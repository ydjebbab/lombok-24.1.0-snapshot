/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : Memento.java
 *
 */
package fr.gouv.finances.lombok.webflow;

import java.io.Serializable;

/**
 * Class Memento An abstract base class for mementos that encapsulate the state of a {@link MementoOriginator}.
 * Basically, a token storing the state of another object.
 * <p>
 * Mementos are expected to be managed by caretakers (clients) without the clents being aware of their internal
 * structure. Only the originator is aware of the internal structure of a concrete Memento implementation.
 * <p>
 * Memento is an abstract class because it's expected implementations are concrete mementos and not part of some other
 * inheritence hierarchy. Mementos are serializable snapshots of data.
 * 
 * @author chouard-cp
 * @version $Revision: 1.2 $ Date: 15 déc. 2009
 */
public abstract class Memento implements Serializable
{ 
    private static final long serialVersionUID = -4889534680542085368L;

    public Memento()
    {
        
    }
}
