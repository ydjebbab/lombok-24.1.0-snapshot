/*
 * Copyright (c) 2017 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.config;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpSessionListener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletListenerRegistrationBean;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.web.session.HttpSessionEventPublisher;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.filter.CharacterEncodingFilter;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.handler.SimpleMappingExceptionResolver;
import org.springframework.web.servlet.handler.SimpleUrlHandlerMapping;
import org.springframework.web.servlet.mvc.UrlFilenameViewController;
import org.springframework.web.servlet.mvc.WebContentInterceptor;

import com.sun.xml.ws.transport.http.servlet.WSSpringServlet;

import fr.gouv.finances.lombok.RGAASessionTimeOutAndTimeWarningCookiesFilter;
import fr.gouv.finances.lombok.mvc.ExceptionResolver;
import fr.gouv.finances.lombok.mvc.WebContentInterceptorAccueil;
import net.sf.jasperreports.j2ee.servlets.ImageServlet;

/**
 * Classe de configuration de la partie dispatcher de Spring. Cette configuration était précedemment réalisée dans le
 * fichier DISPATCHER-SERVLET.XML. Elle ne nécessite plus de déclaration dans WEB.XML (elle est bootstrappée par
 * l'initializer de spring). Elle importe la conf permettant de créer tous les contrôleurs. Reste à traduire en java la
 * partie contenue dans webflow.xml
 */
@Configuration
@EnableWebMvc
// scan entre autres pour le bean WebContentInterceptorAccueil définit dans fr.gouv.finances.lombok.mvc
@ComponentScan(basePackages = {"fr.gouv.finances.lombok.mvc"})

// @Import(value = {LombokWebSharedConfiguration.class, LombokWebFlowConfiguration.class})
public class LombokWebApplicationConfiguration extends WebMvcConfigurerAdapter implements WebApplicationInitializer
{

    /** web content interceptor accueil. (ce component est trouvé grâce au componentscan */
    @Autowired
    private WebContentInterceptorAccueil webContentInterceptorAccueil;

    @Autowired
    LombokWebFlowConfiguration webFlowConfig;

    @Autowired
    private LombokWebSharedConfiguration webSharedConfig;

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter#addResourceHandlers(org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry)
     */
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry)
    {
        registry.addResourceHandler("/composants/**").addResourceLocations("classpath:/META-INF/resources/composants/");
        registry.addResourceHandler("/errors/**").addResourceLocations("classpath:/META-INF/resources/WEB-INF/pages/errors/");
        registry.addResourceHandler("/webjars/**").addResourceLocations("classpath:/META-INF/resources/webjars/");
        registry.addResourceHandler("/resources/**").addResourceLocations("classpath:/META-INF/web-resources/");
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter#configureDefaultServletHandling(org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer)
     */
    @Override
    public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer)
    {
        configurer.enable();
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter#configureHandlerExceptionResolvers(java.util.List)
     */
    @Override
    public void configureHandlerExceptionResolvers(List<HandlerExceptionResolver> exceptionResolvers)
    {
        ExceptionResolver exceptionResolver = new ExceptionResolver();
        exceptionResolvers.add(exceptionResolver);
    }

    /**
     * Methode qui mappe les urls explicites en .ex vers les contrôleurs correspondants
     * 
     * @return simple url handler mapping
     */
    @Bean
    public SimpleUrlHandlerMapping handlerMapping()
    {
        SimpleUrlHandlerMapping simpleUrlHandlerMapping = new SimpleUrlHandlerMapping();
        Map<String, Object> urlMap = new HashMap<>();
        urlMap.put("/sessionexpiree.ex", navigationController());
        urlMap.put("/identification.ex", navigationController());
        // urlMap.put("/messagebloquantdetecteavecportail.ex", messageBloquantDetecteAvecPortailController());
        // urlMap.put("/pagelogin*.ex", redirectiontimeoutcontroller());
        urlMap.put("/**/flux.ex", webFlowConfig.flowcontroller());

        simpleUrlHandlerMapping.setUrlMap(urlMap);
        simpleUrlHandlerMapping.setInterceptors(new Object[] {contentInterceptor()});
        simpleUrlHandlerMapping.setOrder(Integer.MAX_VALUE - 2);

        return simpleUrlHandlerMapping;
    }

    /**
     * Mapping de la page erreuraccesrefuse pour rediriger vers la page d'erreur. Peut-être des mofis à faire (dont le
     * setAlwaysUseFullPath)
     *
     * @return simple url handler mapping
     */
    @Bean
    public SimpleUrlHandlerMapping pagesErreursLombok()
    {
        SimpleUrlHandlerMapping simpleUrlHandlerMapping = new SimpleUrlHandlerMapping();
        Map<String, Object> urlMap = new HashMap<>();
        urlMap.put("/errors/erreuraccesrefuse", navigationController());
        simpleUrlHandlerMapping.setUrlMap(urlMap);
        simpleUrlHandlerMapping.setInterceptors(new Object[] {contentInterceptor()});
        simpleUrlHandlerMapping.setOrder(Integer.MAX_VALUE - 2);
        simpleUrlHandlerMapping.setAlwaysUseFullPath(true);
        return simpleUrlHandlerMapping;
    }

    /**
     * Contrôleur simple qui renverra vers le internal resolver pour le rendu jsp d'une page.
     *
     * @return url filename view controller
     */
    @Bean
    public UrlFilenameViewController navigationController()
    {
        return new UrlFilenameViewController();
    }

    /**
     * methode Max upload exception resolver : pas encore testé si ça marche.
     *
     * @return simple mapping exception resolver
     */
    @Bean
    public SimpleMappingExceptionResolver maxUploadExceptionResolver()
    {
        SimpleMappingExceptionResolver simpleMappingExceptionResolver = new SimpleMappingExceptionResolver();
        Properties propsExceptionMappings = new Properties();
        propsExceptionMappings.setProperty("org.springframework.web.multipart.MaxUploadSizeExceededException", "/errors/erreurmaxupload");
        simpleMappingExceptionResolver.setExceptionMappings(new Properties());
        return simpleMappingExceptionResolver;
    }

    /**
     * Mapping pour l'accueil (accueil.ex). Attention à l'order car la servlet dispatcher est dite par défaut et elle
     * dispose d'un mapping / (alors qu'avant, ce nétait que *.ex, voir doc spring et méthode
     * configureHandlerExceptionResolvers de cette classe)
     * 
     * @return simple url handler mapping
     */
    @Bean
    public SimpleUrlHandlerMapping handlerMappingAccueil()
    {
        SimpleUrlHandlerMapping simpleUrlHandlerMapping = new SimpleUrlHandlerMapping();
        Map<String, Object> urlMap = new HashMap<>();
        urlMap.put("/accueil.ex", webSharedConfig.accueilController());
        simpleUrlHandlerMapping.setUrlMap(urlMap);
        simpleUrlHandlerMapping.setInterceptors(new Object[] {contentInterceptor(), webContentInterceptorAccueil});
        simpleUrlHandlerMapping.setOrder(Integer.MAX_VALUE - 2);
        return simpleUrlHandlerMapping;
    }

    /**
     * Interception web qui place le cache à 0 seconde. à réétudier étant donné que le cache sur les ressources peut
     * être réglé autrement
     *
     * @return web content interceptor
     */
    @Bean
    public WebContentInterceptor contentInterceptor()
    {
        WebContentInterceptor webContentI = new WebContentInterceptor();
        webContentI.setCacheSeconds(0);
        return webContentI;
    }

    /*   *//**
            * methode Error page filter : en attendant de revoir les exceptions et les pages d'erreur, ce bean (et
            * l'autre ci-dessous) permet de désactiver la page filter de spring boot
            *
            * @return error page filter
            */
    /*
     * @Bean public ErrorPageFilter errorPageFilter() { return new ErrorPageFilter(); }
     *//**
       * methode Disable spring boot error filter : voir commentaire du bean ci-dessus.
       *
       * @param filter
       * @return filter registration bean
       *//*
         * @Bean public FilterRegistrationBean disableSpringBootErrorFilter(ErrorPageFilter filter) {
         * FilterRegistrationBean filterRegistrationBean = new FilterRegistrationBean();
         * filterRegistrationBean.setFilter(filter); filterRegistrationBean.setEnabled(false); return
         * filterRegistrationBean; }
         */

    // /**
    // * méthode d'enregistrement du filtre CpExportFilter
    // *
    // * @return
    // */
    // @Bean
    // public FilterRegistrationBean eXtremeExportRegistrationBean()
    // {
    // FilterRegistrationBean filterRegistrationBean = new FilterRegistrationBean();
    // filterRegistrationBean.setName("eXtremeExport");
    // filterRegistrationBean.setFilter(new CpExportFilter());
    // filterRegistrationBean.setUrlPatterns(Collections.singletonList("/*"));
    // return filterRegistrationBean;
    // }

    /**
     * méthode d'enregistrement du filtre RGAASessionTimeOutAndTimeWarningCookiesFilter
     */
    @Bean
    public FilterRegistrationBean rgaaSessionRegistrationBean()
    {
        FilterRegistrationBean filterRegistrationBean = new FilterRegistrationBean();
        filterRegistrationBean.setName("CharacterEncodingFilter");
        filterRegistrationBean.setFilter(new RGAASessionTimeOutAndTimeWarningCookiesFilter());
        filterRegistrationBean.setInitParameters(Collections.singletonMap("sessionTimeWarning", "15"));
        filterRegistrationBean.setUrlPatterns(Collections.singletonList("/*"));
        return filterRegistrationBean;
    }

    /**
     * méthode d'enregistrement du filtre CharacterEncodingFilter
     *
     * @return FilterRegistrationBean
     */
    @Bean
    public FilterRegistrationBean characterEncodingRegistrationBean()
    {
        FilterRegistrationBean filterRegistrationBean = new FilterRegistrationBean();
        filterRegistrationBean.setName("rgaacookie");
        filterRegistrationBean.setFilter(new CharacterEncodingFilter());
        filterRegistrationBean.setInitParameters(Collections.singletonMap("encoding", "UTF-8"));
        filterRegistrationBean.setUrlPatterns(Collections.singletonList("/*"));
        return filterRegistrationBean;
    }

    /**
     * méthode d'enregistrement de la servlet ImageServlet
     *
     * @return bean d'enregistrement de la servlet
     */
    @Bean
    public ServletRegistrationBean imageRegistrationBean()
    {
        ServletRegistrationBean servletRegistrationBean = new ServletRegistrationBean();
        servletRegistrationBean.setServlet(new ImageServlet());
        servletRegistrationBean.setName("ImageServlet");
        servletRegistrationBean.setUrlMappings(Collections.singletonList("/image"));
        return servletRegistrationBean;
    }

    // /**
    // * méthode d'enregistrement de la servlet FichierJointServlet
    // *
    // * @return bean d'enregistrement de la servlet
    // */
    // @Bean
    // public ServletRegistrationBean fichierJointRegistrationBean()
    // {
    // ServletRegistrationBean servletRegistrationBean = new ServletRegistrationBean();
    // servletRegistrationBean.setServlet(new FichierJointServlet());
    // servletRegistrationBean.setName("FichierJointServlet");
    // servletRegistrationBean.setUrlMappings(Collections.singletonList("/fichierjoint/*"));
    // return servletRegistrationBean;
    // }

    /**
     * méthode d'enregistrement de la servlet WSSpringServlet
     *
     * @return bean d'enregistrement de la servlet
     */
    @Bean
    public ServletRegistrationBean serviceRegistrationBean()
    {
        ServletRegistrationBean servletRegistrationBean = new ServletRegistrationBean();
        servletRegistrationBean.setServlet(new WSSpringServlet());
        servletRegistrationBean.setName("ServletWS");
        servletRegistrationBean.setUrlMappings(Collections.singletonList("/services/*"));
        servletRegistrationBean.setLoadOnStartup(1);
        return servletRegistrationBean;
    }

    @Bean
    public ServletListenerRegistrationBean<HttpSessionListener> httpSessionRegistrationBean()
    {
        ServletListenerRegistrationBean<HttpSessionListener> servletListenerRegistrationBean = new ServletListenerRegistrationBean<>();
        servletListenerRegistrationBean.setListener(new HttpSessionEventPublisher());
        return servletListenerRegistrationBean;
    }

    @Override
    public void onStartup(ServletContext servletContext) throws ServletException
    {
        // pour compter nombre de sessions - à ne pas mettre dans projet blanc
        servletContext.addListener(new HttpSessionEventPublisher());
    }

    /**
     * méthode d'enregistrement de la servlet DispatcherServlet
     *
     * @return bean d'enregistrement de la servlet
     */
    // @Bean
    // public ServletRegistrationBean dispatcherRegistration(DispatcherServlet dispatcherServlet)
    // {
    // ServletRegistrationBean registration = new ServletRegistrationBean(dispatcherServlet);
    // registration.setName("dispatcher");
    // registration.setUrlMappings(Arrays.asList("*.ex", "/rest/*", "/async/*"));
    // registration.setLoadOnStartup(1);
    // return registration;
    // }
}
