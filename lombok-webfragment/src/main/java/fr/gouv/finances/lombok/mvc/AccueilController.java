/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : AccueilController.java
 *
 */
package fr.gouv.finances.lombok.mvc;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractController;
import org.springframework.web.servlet.view.RedirectView;

import fr.gouv.finances.lombok.menu.service.MenuService;
import fr.gouv.finances.lombok.securite.service.LectureApplicationService;
import fr.gouv.finances.lombok.securite.springsecurity.PortailAuthentificationSecurityLombok;
import fr.gouv.finances.lombok.securite.springsecurity.UtilisateurSecurityLombokContainer;
import fr.gouv.finances.lombok.securite.techbean.ApplicationAnnuaire;
import fr.gouv.finances.lombok.securite.techbean.HabilitationAnnuaire;
import fr.gouv.finances.lombok.securite.techbean.PersonneAnnuaire;
import fr.gouv.finances.lombok.srm.bean.Codique;
import fr.gouv.finances.lombok.srm.bean.Message;
import fr.gouv.finances.lombok.srm.bean.Profil;
import fr.gouv.finances.lombok.srm.service.SiremeService;
import fr.gouv.finances.lombok.util.cle.ApplicationPropertiesUtil;
import fr.gouv.finances.lombok.utilisateur.mvc.webflow.action.AffichageCaracteristiquesUtilisateurFormAction;

/**
 * Class AccueilController
 * 
 * @author amleplatinec
 * @version $Revision: 1.6 $ Date: 11 déc. 2009
 */
public class AccueilController extends AbstractController
{
    private static final Logger log = LoggerFactory.getLogger(AffichageCaracteristiquesUtilisateurFormAction.class);
    
    private static final String PORTAIL = "portail";

    private MenuService menuso;

    /**
     * Pour pouvoir recupérer l'application métier et ses proprietes.
     */
    private LectureApplicationService lectureapplicationserviceso;

    private SiremeService siremeserviceso;

    private PortailAuthentificationSecurityLombok authenticationPortailProvider;

    public AccueilController()
    {
        super();
    }

    public SiremeService getSiremeserviceso()
    {
        return siremeserviceso;
    }

    public PortailAuthentificationSecurityLombok getAuthenticationPortailProvider()
    {
        return authenticationPortailProvider;
    }

    /**
     * Gets the lectureapplicationserviceso - LectureApplicationService, pour pouvoir recupérer l'application métier et
     * ses proprietes.
     *
     * @return the lectureapplicationserviceso - LectureApplicationService, pour pouvoir recupérer l'application métier
     *         et ses proprietes
     */
    public LectureApplicationService getLectureapplicationserviceso()
    {
        return lectureapplicationserviceso;
    }

    public MenuService getMenuso()
    {
        return menuso;
    }

    public void setSiremeserviceso(SiremeService siremeserviceso)
    {
        this.siremeserviceso = siremeserviceso;
    }

    /**
     * Modificateur de l attribut authentication portail provider.
     *
     * @param authenticationPortailProvider le nouveau authentication portail provider
     */
    public void setAuthenticationPortailProvider(PortailAuthentificationSecurityLombok authenticationPortailProvider)
    {
        this.authenticationPortailProvider = authenticationPortailProvider;

    }

    /**
     * Sets the lectureapplicationserviceso - LectureApplicationService, pour pouvoir recupérer l'application métier et
     * ses proprietes.
     *
     * @param lectureapplicationserviceso the new lectureapplicationserviceso - LectureApplicationService, pour pouvoir
     *        recupérer l'application métier et ses proprietes
     */
    public void setLectureapplicationserviceso(LectureApplicationService lectureapplicationserviceso)
    {
        this.lectureapplicationserviceso = lectureapplicationserviceso;
    }

    public void setMenuso(MenuService menuso)
    {
        this.menuso = menuso;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param request
     * @param respons
     * @return model and view
     * @throws Exception the exception
     * @see org.springframework.web.servlet.mvc.AbstractController#handleRequestInternal(javax.servlet.http.HttpServletRequest,
     *      javax.servlet.http.HttpServletResponse)
     */
    @Override
    protected ModelAndView handleRequestInternal(HttpServletRequest request, HttpServletResponse respons)
        throws Exception
    {
        log.debug(">>> Debut methode handleRequestInternal");
        // Gestion de l'appui sur l'icone de deconnexion
        boolean isDeconnexionDemandee = estDeconnexionDemandee(request);

        // Message SRM deja affiche ?
        boolean isMessageSrmPasEncoreAffiche = estMessageSrmPasEncoreAffiche(request);

        // Est en mode portail
        boolean isModePortail = estEnModePortail();

        List<Message> listMessages = null;

        if (log.isDebugEnabled())
        {
            log.debug("entree handleRequestInternal");
        }

        // cas de la deconnexion
        if (isDeconnexionDemandee)
        {
            if (log.isDebugEnabled())
            {
                log.debug("entree de la deconnexion");
            }

            invaliderSessionUtilisateur(request);

            if (isModePortail)
            {
                // modif amlp 26/10 : quand on se deconnecte en mode portail on retourne sur magellan
                if (log.isDebugEnabled())
                {
                    log.debug("deconnexion mode portail retour ulysse");
                }
                return new ModelAndView(new RedirectView(authenticationPortailProvider.getUlysse()));
            }
            else
            {
                if (log.isDebugEnabled())
                {
                    log.debug("deconnexion mode appli ou appliportail retour login");
                }
                return new ModelAndView(new RedirectView("identification.ex"));
            }
        }

        if (isMessageSrmPasEncoreAffiche)
        {

            listMessages = rechercherMessagesSrmParJaxWS();
            request.getSession().setAttribute("listmessages", listMessages);

            if (listMessages != null && !listMessages.isEmpty())

            {
                if (log.isDebugEnabled())
                {
                    log.debug("affichage listmessages . il y  en a " + listMessages.size());
                }
                return new ModelAndView("listmessages");
            }
        }

        // si pas de deconnexion et pas de message à afficher direction la page d'accueil
        if (log.isDebugEnabled())
        {
            log.debug("avant calculerBarreDeMenusDynamique");
        }
        menuso.calculerBarreDeMenusDynamique(request.getContextPath());

        if (log.isDebugEnabled())
        {
            log.debug("affichage accueil");
        }
        return new ModelAndView("accueil");
    }

    /**
     * un utilisateur a au moins un profil et un codique on regarde si certains messages sont à destination de profils
     * ou de codiques particuliers pour les afficher uniquement aux bons destinataires.
     * 
     * @param lesMessages --
     * @param lesprofils --
     * @param lesCodiques --
     * @return the message[]
     */
    private List<Message> chercherMessagesContenantProfilsEtCodiques(List<Message> lesMessages, String[] lesprofils,
        Set<String> lesCodiques)
    {
        log.debug(">>> Debut methode chercherMessagesContenantProfilsEtCodiques");
        int iii;
        int jjj;
        int lll = 0;

        List<Message> lesMessagesFiltresParProfil = new ArrayList<>();
        for (iii = 0; iii < lesMessages.size(); iii++)

        {
            Message leMessage = (Message) lesMessages.get(iii);
            boolean controleprofilok = false;
            boolean controlecodiqueok = false;

            /* ** controle pour les profils * */

            for (jjj = 0; jjj < lesprofils.length; jjj++)
            {
                Set<Profil> lesProfilsDestiSet = leMessage.getLesProfilsDestinataires();

                if (lesProfilsDestiSet != null && !lesProfilsDestiSet.isEmpty())
                {
                    Iterator<Profil> lesProfilsDestiSetiter = lesProfilsDestiSet.iterator();
                    while (lesProfilsDestiSetiter.hasNext())
                    {
                        Profil unProfil = (Profil) lesProfilsDestiSetiter.next();
                        if (unProfil.getCodeProfil().compareToIgnoreCase(lesprofils[jjj]) == 0)
                        {
                            controleprofilok = true;
                        }
                    }
                }
                else
                {
                    controleprofilok = true;
                }

            }

            Iterator<String> lesCodiquesiter = lesCodiques.iterator();

            while (lesCodiquesiter.hasNext())
            {
                String unCodiqueiter = (String) lesCodiquesiter.next();

                if (unCodiqueiter != null && unCodiqueiter.compareTo("") != 0)
                {
                    Set<Codique> lesCodiquesDestiSet = leMessage.getLesCodiquesDestinataires();
                    if (lesCodiquesDestiSet != null && !lesCodiquesDestiSet.isEmpty())
                    {
                        Iterator<Codique> lesCodiquesDestiSetiter = lesCodiquesDestiSet.iterator();
                        while (lesCodiquesDestiSetiter.hasNext())
                        {
                            Codique unCodique = (Codique) lesCodiquesDestiSetiter.next();
                            if (StringUtils.contains(unCodiqueiter, unCodique.getLibelleCodique()))
                            {
                                controlecodiqueok = true;
                            }
                        }
                    }
                    else
                    {
                        controlecodiqueok = true;

                    }
                }
                else
                {
                    controlecodiqueok = true;
                }

            }

            if (controleprofilok && controlecodiqueok)
            {
                lesMessagesFiltresParProfil.add(lll, leMessage);
                lll = lll + 1;
            }

        }

        if (lesMessagesFiltresParProfil != null && !lesMessagesFiltresParProfil.isEmpty()
            && lesMessagesFiltresParProfil.get(0) != null)
        {
            return lesMessagesFiltresParProfil;
        }
        else
        {
            return null;
        }

    }

    /**
     * methode Est deconnexion demandee : --.
     * 
     * @param request --
     * @return true, si c'est vrai
     */
    private boolean estDeconnexionDemandee(HttpServletRequest request)
    {
        log.debug(">>> Debut methode estDeconnexionDemandee");
        String deconnexion = request.getParameter("deconnecter");
        return (deconnexion != null && "true".compareTo(deconnexion) == 0);
    }

    /**
     * methode Est en mode portail : --.
     * 
     * @return true, si c'est vrai
     */
    private boolean estEnModePortail()
    {
        log.debug(">>> Debut methode estEnModePortail");
        return authenticationPortailProvider.getModeAuthentification().compareTo(PORTAIL) == 0;
    }

    /**
     * methode Est message srm pas encore affiche : --.
     * 
     * @param request --
     * @return true, si c'est vrai
     */
    private boolean estMessageSrmPasEncoreAffiche(HttpServletRequest request)
    {
        Object listmessages = request.getSession().getAttribute("listmessages");
        return (listmessages == null);
    }

    /**
     * à partir des messages , on recherche ceux qui ont des filtres et des profils en accord avec les profils et
     * codiques de l'utilisateur.
     * 
     * @param lesMessages --
     * @return the message[]
     */

    private List<Message> filtrerMessagesAAfficherAUtilisateurParProfilsEtFiltreCodique(List<Message> lesMessages)
    {
        log.debug(">>> Debut methode filtrerMessagesAAfficherAUtilisateurParProfilsEtFiltreCodique");
        List<Message> lesMessagesFiltres = new ArrayList<>();
        PersonneAnnuaire personneannuaire = UtilisateurSecurityLombokContainer.getPersonneAnnuaire();

        String[] lesProfils = personneannuaire.rechercherProfils();
        Set<String> lesCodiques = rechercherCodiquesEtFiltresCodiques(personneannuaire);

        lesMessagesFiltres = this.chercherMessagesContenantProfilsEtCodiques(lesMessages, lesProfils, lesCodiques);

        return lesMessagesFiltres;
    }

    /**
     * methode Invalider session utilisateur : --.
     * 
     * @param request --
     */
    private void invaliderSessionUtilisateur(HttpServletRequest request)
    {
        log.debug(">>> Debut methode invaliderSessionUtilisateur");
        request.getSession().invalidate();
        SecurityContextHolder.clearContext();
    }

    /**
     * Rechercher codiques et filtres codiques.
     * 
     * @param personneannuaire --
     * @return the sets the
     */
    private Set<String> rechercherCodiquesEtFiltresCodiques(PersonneAnnuaire personneannuaire)
    {
        log.debug(">>> Debut methode rechercherCodiquesEtFiltresCodiques");
        Set<HabilitationAnnuaire> lesHabilitations = personneannuaire.getListeHabilitations();
        String unCodique = personneannuaire.getAffectation();
        Set<String> lesCodiques = new HashSet<>();
        for (Iterator<HabilitationAnnuaire> iterHabs = lesHabilitations.iterator(); iterHabs.hasNext();)
        {
            HabilitationAnnuaire habilitationAnnuaire = (HabilitationAnnuaire) iterHabs.next();
            List<String> filtreVals = habilitationAnnuaire.rechercherListeValeursDUnFiltre("CODE_STRUCTURE_SUP");
            List<String> filtreValsSansAnnexe = new ArrayList<>();
            // filtreVals contient en réalité codique +annexe
            // on doit donc transformer pour n avoir que les codiques
            if (filtreVals != null && !filtreVals.isEmpty())
            {

                for (Iterator<String> iterFiltreVals = filtreVals.iterator(); iterFiltreVals.hasNext();)
                {
                    String unIterFiltreVals = (String) iterFiltreVals.next();
                    String uneValeurFiltreSansAnnexeUtils = StringUtils.removeEnd(unIterFiltreVals, "0");
                    filtreValsSansAnnexe.add(uneValeurFiltreSansAnnexeUtils);
                }

            }
            // on recherche application pour connaitre le type gestion structure

            // on doit rechercher l application compléte pour avoir accés au typeGestionStructure
            // Octobre 2012 partie à revoir car dans l'annuaire fusionné les elements typegestionstructure ne sont pas
            // là ...
            ApplicationAnnuaire applicationAnnuaire =
                lectureapplicationserviceso.rechercherApplicationParLibelleCourt(ApplicationPropertiesUtil.getProperty("lombok.aptera.codeappli"));

            if (applicationAnnuaire != null && StringUtils.isNotBlank(applicationAnnuaire.getLibelleCourtAppli()))
            {
                // cette
                String typeGestionStructure = applicationAnnuaire.getTypeGestionStructure();
                // rajout codique uniquement
                if (typeGestionStructure.compareToIgnoreCase("codestructure") == 0)
                {
                    lesCodiques.add(unCodique);
                }
                // rajout codique + ts codes structures supplémentaires
                if (typeGestionStructure.compareToIgnoreCase("codestructureetacl") == 0)
                {
                    lesCodiques.add(unCodique);
                    lesCodiques.addAll(filtreValsSansAnnexe);
                }
                // rajout code structures supplementaires
                if (typeGestionStructure.compareToIgnoreCase("codestructureouacl") == 0)
                {
                    if (filtreValsSansAnnexe != null && !filtreValsSansAnnexe.isEmpty())
                    {
                        lesCodiques.addAll(filtreValsSansAnnexe);
                    }
                    else
                    {
                        lesCodiques.add(unCodique);
                    }
                }
                // rajout code structures supplementaires
                if (typeGestionStructure.compareToIgnoreCase("acl") == 0)
                {
                    if (filtreValsSansAnnexe != null && !filtreValsSansAnnexe.isEmpty())
                    {
                        lesCodiques.addAll(filtreValsSansAnnexe);
                    }
                    else
                    {
                        lesCodiques.add(unCodique);
                    }

                }
            }
            else
            // cas annuaire dgfip on rajoute tous les filtres code structure sans distinction
            {
                lesCodiques.add(unCodique);
                if (filtreValsSansAnnexe != null && !filtreValsSansAnnexe.isEmpty())
                {
                    lesCodiques.addAll(filtreValsSansAnnexe);
                }
            }
        }
        return lesCodiques;
    }

    /**
     * recherche des messages de sireme à afficher (messages valides non bloquant à l heure actuelle).
     * 
     * @return list
     */
    private List<Message> rechercherMessagesSrmParJaxWS()
    {
        List<Message> lesMessagesFiltres = new ArrayList<>();

        List<Message> lesMessages = new ArrayList<>();

        if (siremeserviceso != null)
        {
            lesMessages =
                siremeserviceso.rechercherMessagesALaDateHeureCourantePourLApplication();
        }

        if (lesMessages != null && !lesMessages.isEmpty())
        {
            lesMessagesFiltres = filtrerMessagesAAfficherAUtilisateurParProfilsEtFiltreCodique(lesMessages);

        }
        return lesMessagesFiltres;
    }

}