/*
 * Copyright (c) 2017 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.config;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.webflow.config.AbstractFlowConfiguration;
import org.springframework.webflow.definition.registry.FlowDefinitionRegistry;
import org.springframework.webflow.engine.builder.support.FlowBuilderServices;
import org.springframework.webflow.mvc.builder.MvcViewFactoryCreator;

/**
 * Configuration commune pour le webflow.
 */
@Configuration
public class LombokWebFlowSharedConfiguration extends AbstractFlowConfiguration
{
    @Autowired
    private LombokWebSharedConfiguration webSharedConfig;

    /**
     * Registre de flux pour les composants.
     *
     * @return FlowDefinitionRegistry
     */
    @Bean
    public FlowDefinitionRegistry composantsFlowRegistry()
    {
        return getFlowDefinitionRegistryBuilder()
            .setFlowBuilderServices(flowBuilderServices())
            .setBasePath("/WEB-INF/flows-composants")
            .addFlowLocation("/composantatlas/ajoutdocumentatlas-flow.xml", "ajoutdocumentatlas-flow")
            .addFlowLocation("/composantatlas/recherchedocumentatlas-flow.xml", "recherchedocumentatlas-flow")
            .addFlowLocation("/composantedition/administrationeditions-flow.xml", "administrationeditions-flow")
            .addFlowLocation("/composantedition/administrationpurgeseditions-flow.xml", "administrationpurgeseditions-flow")
            .addFlowLocation("/composantedition/consultationbanette-flow.xml", "consultationbanette-flow")
            .addFlowLocation("/composantedition/statistiqueseditions-flow.xml", "statistiqueseditions-flow")
            .addFlowLocation("/composantinfoutil/affichagecaracteristiquesutilisateur-flow.xml",
                "affichagecaracteristiquesutilisateur-flow")
            .addFlowLocation("/composantjournal/rechercheoperationsjournal-flow.xml", "rechercheoperationsjournal-flow")
            .addFlowLocation("/composantjournal/transfereroperationsjournal-flow.xml", "transfereroperationsjournal-flow")
            // .addFlowLocation("/composantogre/catalogues-flow.xml", "catalogues-flow")
            // .addFlowLocation("/composantogre/classeurs-flow.xml", "classeurs-flow")
            .addFlowLocation("/composantsrm/zf1/creationmessage-flow.xml", "creationmessage-flow")
            .addFlowLocation("/composantsrm/zf1/creationmessageavecrestrictionsprofilsetcodiques-flow.xml",
                "creationmessageavecrestrictionsprofilsetcodiques-flow")
            .addFlowLocation("/composantsrm/zf1/modificationmessage-flow.xml", "modificationmessage-flow")
            .addFlowLocation("/composantsrm/zf1/recherchemessages-flow.xml", "recherchemessages-flow")
            .addFlowLocation("/composantsrm/zf1/recherchemessagestypes-flow.xml", "recherchemessagestypes-flow")
            .addFlowLocation("/composantsrm/zf1/suppressionmessages-flow.xml", "suppressionmessages-flow")
            .addFlowLocation("/composantsrm/zf2/desactivationprofil-flow.xml", "desactivationprofil-flow")
            .addFlowLocation("/composantsrm/zf3/creationmessagetype-flow.xml", "creationmessagetype-flow")
            .addFlowLocation("/composantsrm/zf3/suppressionmessagestype-flow.xml", "suppressionmessagestype-flow")
            .addFlowLocation("/composantsrm/zf4/recherchetousmessages-flow.xml", "recherchetousmessages-flow")
            .addFlowLocation("/composantsrm/zf5/creationprofils-flow.xml", "creationprofils-flow")
            .addFlowLocation("/composantsrm/zf5/suppressionprofils-flow.xml", "suppressionprofils-flow")
            .addFlowLocation("/wsjournal/rechercheoperationsjournalws-flow.xml", "rechercheoperationsjournalws-flow")
            .addFlowLocation("/metriques/actuator-flow.xml", "actuator-flow")
            .build();
    }

    /**
     * Constructeur de flux avec les resolvers JSP et XML injectés depuis la configuration partagée web.
     *
     * @return FlowBuilderServices
     */
    @Bean
    public FlowBuilderServices flowBuilderServices()
    {
        return getFlowBuilderServicesBuilder()
            .setViewFactoryCreator(mvcViewFactoryCreator())
            .build();
    }

    /**
     * Le webflow produit des vues JSP (internal) et XML.
     *
     * @return MvcViewFactoryCreator
     */
    @Bean
    public MvcViewFactoryCreator mvcViewFactoryCreator()
    {
        MvcViewFactoryCreator mvcViewFactoryCreator = new MvcViewFactoryCreator();
        // CF : lombokXMLViewResolver doit être avant lombokInternalResourceViewResolver
        mvcViewFactoryCreator
            .setViewResolvers(Arrays.asList(webSharedConfig.lombokXMLViewResolver(), webSharedConfig.lombokInternalResourceViewResolver()));
        return mvcViewFactoryCreator;
    }

}
