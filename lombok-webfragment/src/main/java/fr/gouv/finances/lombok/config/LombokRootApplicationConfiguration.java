/*
 * Copyright (c) 2017 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

/**
 * Classe montant le contexte applicatif. Les définitions des beans sont contenues dans le fichier app-config.xml
 */
@Configuration
// @Import(LombokCoreConfiguration.class)
@ImportResource(value = {"classpath:/conf/app-config.xml"})
public class LombokRootApplicationConfiguration
{

}
