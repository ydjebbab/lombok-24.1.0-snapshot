/*
 * Copyright (c) 2016 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok;

import java.io.IOException;
import java.util.Calendar;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * A configurer dans le web.xml: {@code
 * <xml>
 * <session-config><session-timeout>sessionEndTimeStamp</session-timeout></session-config> <filter>
 * <filter-name>rgaacookie</filter-name> <filter-class>fr.gouv.finances.lombok.RGAASessionCookieFilter</filter-class>
 * <init-param> <param-name>sessionTimeWarning</param-name><param-value>900</param-value> </init-param> </filter>
 * </xml>
 * }
 */
public class RGAASessionTimeOutAndTimeWarningCookiesFilter implements Filter
{
    /** session time warning. */
    private static String sessionTimeWarning;

    /**
     * Constructeur de la classe RGAASessionTimeOutAndTimeWarningCookiesFilter.java
     */
    public RGAASessionTimeOutAndTimeWarningCookiesFilter()
    {
        super();
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see javax.servlet.Filter#init(javax.servlet.FilterConfig)
     */
    @Override
    public void init(FilterConfig filterConfig) throws ServletException
    {
        sessionTimeWarning = filterConfig.getInitParameter("sessionTimeWarning");
        if (sessionTimeWarning == null || sessionTimeWarning.isEmpty())
        {
            sessionTimeWarning = "30"; // en minutes
        }
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see javax.servlet.Filter#doFilter(javax.servlet.ServletRequest, javax.servlet.ServletResponse,
     *      javax.servlet.FilterChain)
     */
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException
    {

        HttpServletResponse resp = (HttpServletResponse) response;

        HttpServletRequest req = (HttpServletRequest) request;
        if (req.isRequestedSessionIdValid())
        {
            int timeout = req.getSession(false).getMaxInactiveInterval() / 60; // en minutes
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.MINUTE, timeout);
            resp.addCookie(getCookie("sessionEndTimeStamp", String.valueOf(cal.getTimeInMillis())));
            resp.addCookie(getCookie("sessionTimeOut", String.valueOf(timeout)));
            resp.addCookie(getCookie("sessionTimeWarning", sessionTimeWarning));

        }
        chain.doFilter(req, resp);
    }

    /**
     * Méthode permettant de retourner un cookie
     *
     * @param attribut attribut du cookie
     * @param valeur valeur de l'attribue du cookie
     * @return le cookie
     */
    private Cookie getCookie(String attribut, String valeur)
    {
        Cookie cookie = new Cookie(attribut, valeur);
        cookie.setSecure(true);
        cookie.setHttpOnly(true);
        return cookie;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see javax.servlet.Filter#destroy()
     */
    @Override
    public void destroy()
    {
        // Ne rien faire
    }

}
