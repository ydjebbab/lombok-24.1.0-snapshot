/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : MementoFormStatePersister.java
 *
 */
package fr.gouv.finances.lombok.webflow;

import java.util.HashMap;
import java.util.Map;

import org.springframework.util.Assert;
import org.springframework.webflow.action.FormAction;
import org.springframework.webflow.execution.RequestContext;
import org.springframework.webflow.execution.ScopeType;

import fr.gouv.finances.lombok.util.exception.ProgrammationException;

/**
 * Classe a appeler depuis un FormAction pour sauvegarder et restaurer l'etat d'un formulaire.
 * 
 * @author wpetit-cp
 * @version $Revision: 1.2 $ Date: 15 déc. 2009
 */
public class MementoFormStatePersister implements FormStatePersister
{

    /** Niveau de sauvegarde/restauration 1 (permet de differencier differents types de restauration si besoin). */
    public static final int LEVEL1 = 1;

    /** Niveau de sauvegarde/restauration 2 (permet de differencier differents types de restauration si besoin). */
    public static final int LEVEL2 = 2;

    /** Niveau de sauvegarde/restauration 3 (permet de differencier differents types de restauration si besoin). */
    public static final int LEVEL3 = 3;

    /** Niveau de sauvegarde/restauration 4 (permet de differencier differents types de restauration si besoin). */
    public static final int LEVEL4 = 4;

    /** Niveau de sauvegarde/restauration 5 (permet de differencier differents types de restauration si besoin). */
    public static final int LEVEL5 = 5;

    /** Niveau de sauvegarde/restauration 6 (permet de differencier differents types de restauration si besoin). */
    public static final int LEVEL6 = 6;

    /** Le scope dans lequel le memento est stocké. Default is {@link ScopeType#FLOW}. */
    private ScopeType scope = ScopeType.FLOW;

    /**
     * Constructeur de la classe MementoFormStatePersister.java
     *
     */
    public MementoFormStatePersister()
    {
        super();         
    }

    /**
     * Retourne le scope utilisé pour stocker le memento.
     * 
     * @return the scope
     */
    public ScopeType getScope()
    {
        return scope;
    }

    /**
     * Restaure l'état du formulaire pour un niveau donné.
     * 
     * @param form le formulaire
     * @param context contexte du flux
     * @param formaction FormAction
     * @param level défini le niveau de sauvegarde de l'état
     * @return the object
     */
    @Override
    public Object restoreState(Object form, RequestContext context, FormAction formaction, int level)
    {
        assertIsMementoOriginator(form);
        Object statesStorages = getScope().getScope(context).get(getAttributeName(context, formaction));
        if (statesStorages == null || !(statesStorages instanceof Map))
        {
            throw new ProgrammationException("Aucun objet n'est stocké sous la clé "
                + getAttributeName(context, formaction) + " ou cet objet n'est pas de type Map");
        }

        Memento memento = (Memento) ((Map) statesStorages).get(Integer.valueOf (level));

        ((MementoOriginator) form).setMemento(memento, level);

        return form;
    }

    /**
     * Sauvegarde l'état du formulaire pour un niveau donné.
     * 
     * @param form le formulaire
     * @param context contexte du flux
     * @param formaction FormAction
     * @param level défini le niveau de sauvegarde de l'état
     */
    @Override
    public void saveState(Object form, RequestContext context, FormAction formaction, int level)
    {
        assertIsMementoOriginator(form);

        Object statesStorages = getScope().getScope(context).get(getAttributeName(context, formaction));

        if (statesStorages == null)
        {
            statesStorages = new HashMap();
            getScope().getScope(context).put(getAttributeName(context, formaction), statesStorages);
        }
        else if (!(statesStorages instanceof Map))
        {
            throw new ProgrammationException("L'objet stocké sous la clé " + getAttributeName(context, formaction)
                + " n'est pas de type Map");
        }

        ((Map) statesStorages).put(Integer.valueOf (level), ((MementoOriginator) form).createMemento());
    }

    /**
     * Positionne le scope utilisé pour stocker le memento.
     * 
     * @param scope --
     */
    public void setScope(ScopeType scope)
    {
        this.scope = scope;
    }

    /**
     * Return the attribute name of the memento in the context.
     * 
     * @param context --
     * @param formaction --
     * @return the attribute name
     */
    protected String getAttributeName(RequestContext context, FormAction formaction)
    {
        return "memento_form_" + formaction.getFormObjectName();
    }

    /**
     * Make sure given bean is a {@link MementoOriginator}.
     * 
     * @param bean --
     */
    private void assertIsMementoOriginator(Object bean)
    {
        Assert.isInstanceOf(MementoOriginator.class, bean,
            "MementoFormStatePersister requires the persisted bean to be a MementoOriginator");
    }
}
