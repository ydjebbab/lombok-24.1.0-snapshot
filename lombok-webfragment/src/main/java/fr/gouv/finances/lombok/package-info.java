/*
 * Copyright (c) 2017 DGFiP - Tous droits réservés
 * 
*/
// Projet : lombok-webfragment
/**
 * Documentation du paquet fr.gouv.finances.lombok
 * @author chouard
 * @version 1.0
 */
package fr.gouv.finances.lombok;