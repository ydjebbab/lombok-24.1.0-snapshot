/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : SubFlowEnterViewStateListener.java
 *
 */
package fr.gouv.finances.lombok.mvc.webflow.listener;

import java.util.Stack;

import org.springframework.webflow.definition.StateDefinition;
import org.springframework.webflow.engine.EndState;
import org.springframework.webflow.engine.SubflowState;
import org.springframework.webflow.engine.ViewState;
import org.springframework.webflow.execution.Event;
import org.springframework.webflow.execution.FlowExecutionListenerAdapter;
import org.springframework.webflow.execution.FlowSession;
import org.springframework.webflow.execution.RequestContext;

import fr.gouv.finances.lombok.util.exception.ProgrammationException;

/**
 * Un listener pour ecouter les evenements subFlowEnterViewState. * *
 * <p>
 * Cette classe permet de définir dans le contexte de flux une pile qui contient l'historique des vues accédées par
 * l'utilisateur
 * </p>
 * <p>
 * Elle définit une transition standard [gotovueprecedente] pour gérer les transition de type
 * "aller à la page précédente".
 * </p>
 * <p>
 * Lorsque cette transition est appelée :
 * </p>
 * <ul>
 * <li>le dernier identifiant de vue inséré dans la pile est extrait de la pile</li>
 * <li>puis cet identifiant est stocké dans le contexte 'flashScope' sous le nom 'idvueprecedente'</li>
 * </ul>
 * <p>
 * Ce listener permet de stocker les éléments suivants dans le contexte:
 * </p>
 * <ul>
 * <li>sous la clé 'idvueprecedente' dans le flashScope l'identifiant de la vue accédée précédemment dans le même flux</li>
 * <li>sous la clé 'idvuecourante' dans le flashScope l'identifiant de la vue courante"</li>
 * <li>sous la clé 'previousViewStateId dans le conversationScope l'identifiant de la vue précédent l'appel au sous
 * flux'</li>
 * </ul>
 * <p>
 * Exemple d'utilisation dans une transition globale : transition on="gotovueprecedente"
 * to="${flashScope.idvueprecedente}"
 * </p>
 * 
 * @author chouard-cp
 * @version $Revision: 1.2 $
 */
public class SubFlowEnterViewStateListener extends FlowExecutionListenerAdapter
{

    /** Constant : HISTORIQUE_DES_VUES_DU_FLOT. */
    private static final String HISTORIQUE_DES_VUES_DU_FLOT = "historiqueDesVues";

    /** Constant : TRANSITION_ECRAN_PRECEDENT. */
    private static final String TRANSITION_ECRAN_PRECEDENT = "gotovueprecedente";

    /** Constant : ENDSTATE_RETOUR_VUE_APPELANTE. */
    private static final String ENDSTATE_RETOUR_VUE_APPELANTE = "fin";

    /** Constant : ENDSTATE_RETOUR_VUE_PRECEDENT_APPELANTE. */
    private static final String ENDSTATE_RETOUR_VUE_PRECEDENT_APPELANTE = "fin1";

    /** Constant : VUE_PRECEDENTE. */
    private static final String VUE_PRECEDENTE = "idvueprecedente";

    /** Constant : VUE_COURANTE. */
    private static final String VUE_COURANTE = "idvuecourante";

    /** Constant : VUE_PRECEDENT_ENTREE_FLUX. */
    private static final String VUE_PRECEDENT_ENTREE_FLUX = "previousViewStateId";

    /** Constant : MSG_ERREUR_PILE_ABSENTE. */
    private static final String MSG_ERREUR_PILE_ABSENTE =
        "Erreur : la pile maintenant l'historique des vues devrait être présente";

    /**
     * Instanciation de sub flow enter view state listener.
     */
    public SubFlowEnterViewStateListener()
    {
        super();
    }

    /**
     * <p>
     * Appelée quand un évenement est signalé dans l'état en cours, avant la transition entre deux états
     * </p>
     * <p>
     * si l'évènement signalé est 'gotovueprecedente', on retire de la pile le dernier identifiant de vue empilé.
     * </p>
     * 
     * @param context le contexte du flux
     * @param event l'évènement signalé
     */
    public void eventSignaled(RequestContext context, Event event)
    {
        // Si l'évènement en cours de traitement est l'évènement qui permet d'accéder
        // à la vue précécente, on dépile d'un élément la pile des vues consultées
        // par l'utilisateur et on stocke l'id de la vue dépilée dans le flashScope sous la
        // clé 'vueprecedente'
        if (TRANSITION_ECRAN_PRECEDENT.equals(event.getId()) && !getViewStateIds(context).isEmpty())
        {
            Stack<String> pileHistorique = getViewStateIds(context);
            if (pileHistorique != null)
            {
                context.getFlashScope().put(VUE_PRECEDENTE, pileHistorique.pop());
            }
            else
            {
                // Si rien dans la pile, on utilise l'id de la vue courante
                context.getFlashScope().put(VUE_PRECEDENTE, context.getCurrentState().getId());
            }
        }

        // Dans le cas d'un évènement provoqué sur le retour d'un sous flux
        if (event.getSource() instanceof EndState)
        {
            Stack<String> pileHistorique = getViewStateIds(context);
            if (!pileHistorique.isEmpty())
            {
                // On retourne à la vue d'appel
                if (ENDSTATE_RETOUR_VUE_APPELANTE.equals(event.getId()))
                {
                    context.getFlashScope().put(VUE_PRECEDENTE, pileHistorique.pop());
                }
                else if (ENDSTATE_RETOUR_VUE_PRECEDENT_APPELANTE.equals(event.getId()))
                {
                    if (pileHistorique.size() >= 2)
                    {
                        pileHistorique.pop();
                    }
                    context.getFlashScope().put(VUE_PRECEDENTE, pileHistorique.pop());
                }
            }
        }
    }

    /**
     * <p>
     * Dès qu'un nouveau flot est créé, le listener ajoute une nouvelle pile destinée à maintenir l'historique des vues
     * que l'utilisateur a consulté
     * </p>
     * .
     * 
     * @param context --
     * @param session --
     */
    public void sessionStarted(RequestContext context, FlowSession session)
    {
        session.getScope().put(HISTORIQUE_DES_VUES_DU_FLOT, new Stack<String>());

    }

    /**
     * <p>
     * Appelée lors d'une transition entre deux états, une fois que la transition a eu lieu.
     * </p>
     * <p>
     * Stocke les éléments suivants dans le contexte:
     * </p>
     * <ul>
     * <li>sous la clé 'idvueprecedente' dans le flashScope l'identifiant de la vue accédée précédemment dans le même
     * flux</li>
     * <li>sous la clé 'idvuecourante' dans le flashScope l'identifiant de la vue courante"</li>
     * <li>sous la clé 'previousViewStateId dans le conversationScope l'identifiant de la vue précédent l'appel au sous
     * flux'</li>
     * </ul>
     * 
     * @param context le contexte de la requête
     * @param previousState état <i>de départ</i> de la transition
     * @param state état <i>cible</i> de la transition
     */
    public void stateEntered(RequestContext context, StateDefinition previousState, StateDefinition state)
    {

        // Lors de l'entrée dans un sous-flux, on stocke dans le conversationScope l'id de la vue
        // précédent l'appel au sous-flux
        if (state instanceof SubflowState)
        {
            String previousStateId = "";

            // Si l'état précédent est uen vue, on se contente de récupérer l'id de la vue
            if (previousState instanceof ViewState)
            {
                previousStateId = previousState.getId();
            }
            else
            // Sinon, on utilise le dernier élément de la pile
            {
                Stack<String> pileHistorique = getViewStateIds(context);
                if (pileHistorique != null && !pileHistorique.isEmpty())
                {
                    previousStateId = pileHistorique.lastElement();
                }
            }
            context.getConversationScope().put(VUE_PRECEDENT_ENTREE_FLUX, previousStateId);

        }

        // Lors de l'entrée dans une vue, stocke dans le flashScope l'id de la vue en cours
        if (state != null && state.isViewState())
        {
            context.getFlashScope().put(VUE_COURANTE, state.getId());
        }

        // Si le flot a changé d'état et si l'évènement qui a provoqué le changement d'état
        // n'est pas l'évènement qui permet d'accéder à la page précédente
        // on place dans la pile l'id de la vue précédente
        if (previousState != null && state != null
            && previousState.isViewState()
            && (previousState.getId() != null && (!previousState.getId().equals(state.getId())) && (!TRANSITION_ECRAN_PRECEDENT
                .equals(context.getCurrentEvent().getId()))))
        {
            Stack<String> pileHistorique = getViewStateIds(context);
            if (pileHistorique != null)
            {
                pileHistorique.push(previousState.getId());
            }
            else
            {
                throw new ProgrammationException(MSG_ERREUR_PILE_ABSENTE);
            }
        }

    }

    /**
     * Accesseur de l attribut view state ids.
     * 
     * @param context --
     * @return view state ids
     */
    private Stack<String> getViewStateIds(RequestContext context)
    {
        return (Stack<String>) context.getFlowScope().get(HISTORIQUE_DES_VUES_DU_FLOT);
    }

}
