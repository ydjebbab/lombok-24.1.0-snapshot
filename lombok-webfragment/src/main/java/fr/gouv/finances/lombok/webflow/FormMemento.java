/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : FormMemento.java
 *
 */
package fr.gouv.finances.lombok.webflow;

import fr.gouv.finances.lombok.util.ObjectDeepCloner;

/**
 * Class FormMemento --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.2 $ Date: 11 déc. 2009
 */
public class FormMemento extends Memento
{

    /**
     * Classe permettant de sauvegarder un formulaire par clonage de l'arborescence dobjets Est utilisee dans le Pattern
     * Memento.
     */
    private static final long serialVersionUID = 1L;

    /** object deep clone. */
    private Object objectDeepClone;

    /**
     * The Constructor.
     * 
     * @param object --
     */
    public FormMemento(Object object)
    {
        objectDeepClone = this.getDeepClone(object);
    }

    /**
     * Donne une reference sur le formulaire sauvegarde.
     * 
     * @return reference sur le formulaire precedemment sauvegarde
     */
    public Object getObjectDeepClone()
    {
        return objectDeepClone;
    }

    /**
     * Modificateur de l attribut object deep clone.
     * 
     * @param objectDeepClone le nouveau object deep clone
     */
    public void setObjectDeepClone(Object objectDeepClone)
    {
        this.objectDeepClone = objectDeepClone;
    }

    /**
     * Accesseur de l attribut deep clone.
     * 
     * @param object --
     * @return deep clone
     */
    private Object getDeepClone(Object object)
    {
        Object copieDuFormulaire = null;
        copieDuFormulaire = ObjectDeepCloner.deepCopy(object);
        return copieDuFormulaire;
    }
}
