/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : WebContentInterceptorAccueil.java
 *
 */
package fr.gouv.finances.lombok.mvc;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

/**
 * <pre>
 * Intercepteur de la page d'accueil afin de mettre en session des infos 
 * (exemple: verifier que le contexte de formation est chargé ...) cf dispatcher-servlet.xml.
 * Si un bean avantaccueilserviceso est définit dans la configuration alors on appelle les méthode 
 * de l'interface AvantAccueilService qu'il doit implémenter.
 * </pre>
 * 
 * @author amleplatinec-cp
 * @version $Revision: 1.2 $ Date: 15 déc. 2009
 */
@Component
public class WebContentInterceptorAccueil implements HandlerInterceptor
{
    protected final Log log = LogFactory.getLog(getClass());

    @Autowired(required = false)
    private AvantAccueilService avantaccueilserviceso;

    public WebContentInterceptorAccueil()
    {
        super();
    }

    /**
     * s'exécute aprés que la réponse ait été envoyée au client.
     * 
     * @param arg0 --
     * @param arg1 --
     * @param arg2 --
     * @param arg3 --
     * @throws Exception the exception
     */
    @Override
    public void afterCompletion(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2, Exception arg3)
        throws Exception
    {
        if (avantaccueilserviceso != null)
        {
            if (arg0 != null && arg0.isRequestedSessionIdValid())
            {
                if (estDejaPasseParAccueil(arg0))
                {
                    avantaccueilserviceso.mettreAJourSessionApresPassageAccueil(arg0);
                    log.debug("execution du traitement apres passage dans accueil");
                }
            }
        }
    }

    /**
     * methode Est deja passe par accueil : --.
     * 
     * @param arg0 --
     * @return true, si c'est vrai
     */
    public boolean estDejaPasseParAccueil(HttpServletRequest arg0)
    {
        String contextexecuterAvantAccueil = (String) arg0.getSession().getAttribute("contextexecuterAvantAccueil");
        boolean testEstDejaPasseParAccueil =
            (contextexecuterAvantAccueil != null && "contextexecuterAvantAccueil"
                .equalsIgnoreCase(contextexecuterAvantAccueil));
        log.debug("test est deja passe par accueil : " + testEstDejaPasseParAccueil);
        return testEstDejaPasseParAccueil;
    }

    /**
     * s'execute aprés l'objet AccueilController qui va traiter la requete *.
     * 
     * @param arg0 --
     * @param arg1 --
     * @param arg2 --
     * @param arg3 --
     * @throws Exception the exception
     */
    @Override
    public void postHandle(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2, ModelAndView arg3)
        throws Exception
    {

        if (arg3.getViewName() != null && arg3.getViewName().compareToIgnoreCase("accueil") == 0)
        {

            if (avantaccueilserviceso != null)
            {
                // sera exécuté avant chaque accés à accueil.ex
                if (arg0 != null && arg0.isRequestedSessionIdValid())
                {

                    if (!estDejaPasseParAccueil(arg0))
                    {

                        avantaccueilserviceso.executerAvantPremierPassageSurAccueil(arg0);
                        log.debug("execution du traitement avant passage dans accueil");

                        // mise dans la session d'une info disant qu'on vient de faire executerAvantAccueil
                        // (pour ne pas reexécuter quand on clique dans le menu sur accueil)

                        arg0.getSession().setAttribute("contextexecuterAvantAccueil", "contextexecuterAvantAccueil");
                    }
                }

            }
        }
    }

    /**
     * s'execute avant l'objet AccueilController qui va traiter la requete *.
     * 
     * @param arg0 --
     * @param arg1 --
     * @param arg2 --
     * @return true, if pre handle
     */
    @Override
    public boolean preHandle(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2)
    {
        return true;
    }

}
