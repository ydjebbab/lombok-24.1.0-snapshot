/*
 * Copyright (c) 2017 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.config;

import java.util.Properties;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.servlet.mvc.SimpleControllerHandlerAdapter;
import org.springframework.web.servlet.mvc.WebContentInterceptor;
import org.springframework.web.servlet.mvc.annotation.AnnotationMethodHandlerAdapter;
import org.springframework.web.servlet.mvc.annotation.DefaultAnnotationHandlerMapping;

/**
 * Classe définissant la partie contrôleur du module ogre. non testée.
 */
@Configuration
@Profile("ogre")
@ComponentScan(basePackages = {"fr.gouv.finances.dgfip.ogre.mvc.controller"})
public class LombokOgreConfiguration
{

    /**
     * methode Web content interceptor : intercepteur de contenu qui place des infos et qui controle du cache. Il faudra
     * certainement refatorer plus tard...
     *
     * @return web content interceptor
     */
    @Bean
    WebContentInterceptor webContentInterceptor()
    {
        WebContentInterceptor webIntercept = new WebContentInterceptor();
        webIntercept.setCacheSeconds(0);
        webIntercept.setUseExpiresHeader(true);
        webIntercept.setUseCacheControlHeader(true);
        webIntercept.setUseCacheControlNoStore(true);
        Properties propsCacheMappings = new Properties();
        propsCacheMappings.setProperty("/async", "0");
        webIntercept.setCacheMappings(propsCacheMappings);
        return webIntercept;
    }

    /**
     * methode Simplecontrollerhandleradapter
     *
     * @return simple controller handler adapter
     */
    @Bean
    SimpleControllerHandlerAdapter simplecontrollerhandleradapter()
    {
        return new SimpleControllerHandlerAdapter();
    }

    /**
     * methode Defaultannotationhandlermapping .
     *
     * @return default annotation handler mapping
     */
    @Bean
    DefaultAnnotationHandlerMapping defaultannotationhandlermapping()
    {
        DefaultAnnotationHandlerMapping defaultAnnot = new DefaultAnnotationHandlerMapping();
        defaultAnnot.setOrder(0);
        defaultAnnot.setAlwaysUseFullPath(true);
        return defaultAnnot;
    }

    /**
     * methode Jackson message converter
     *
     * @return mapping jackson 2 http message converter
     */
    @Bean
    MappingJackson2HttpMessageConverter jacksonMessageConverter()
    {
        return new MappingJackson2HttpMessageConverter();

    }

    /**
     * methode Annotationmethodhandleradapter 
     *
     * @return annotation method handler adapter
     */
    @Bean
    AnnotationMethodHandlerAdapter annotationmethodhandleradapter()
    {
        AnnotationMethodHandlerAdapter annotAdapter = new AnnotationMethodHandlerAdapter();
        annotAdapter.setMessageConverters(new HttpMessageConverter[] {jacksonMessageConverter()});
        return annotAdapter;
    }

}
