/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
 * Projet ${projet} - lombok.framework
 * 26/10/06
 * quand on est en accés portail, lorsqu'une erreur est décelée par spring security on va vers cette page spécifique pour 
 * l'affichage des erreurs ( soit message bloquant , soit aucune habilitation sur l appli demandee ) 
 * dans applicationcontext-commun-security on 
 * <property name="authenticationFailureUrl">
            <value>/messagebloquantdetecteavecportail.ex</value>
        </property>  
        qui conduit sur ce controller 
 * fichier : MessageBloquantDetecteAvecPortailController.java
 *
 */

package fr.gouv.finances.lombok.mvc;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractController;
import org.springframework.web.servlet.view.RedirectView;

import fr.gouv.finances.lombok.securite.springsecurity.PortailAuthentificationSecurityLombok;

/**
 * Class MessageBloquantDetecteAvecPortailController.
 * 
 * @author amleplatinec-cp Controleur permettant le routage vers l'affichage d'un message bloquant si un message SRM
 *         bloquant concernant l'application a été détecté (l'utilisateur ne doit alors pas avoir acces aux autres pages
 *         de l'application)
 * @author chouard-cp
 * @version $Revision: 1.4 $ Date: 15 déc. 2009
 */
public class MessageBloquantDetecteAvecPortailController extends AbstractController
{
    private static final String PORTAIL = "portail";

    private PortailAuthentificationSecurityLombok authenticationPortailProvider;

    private final Log log = LogFactory.getLog(getClass());

    public MessageBloquantDetecteAvecPortailController()
    {
        super();
    }

    public PortailAuthentificationSecurityLombok getAuthenticationPortailProvider()
    {
        return authenticationPortailProvider;
    }

    public void setAuthenticationPortailProvider(PortailAuthentificationSecurityLombok authenticationPortailProvider)
    {
        this.authenticationPortailProvider = authenticationPortailProvider;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param request
     * @param respons
     * @return model and view
     * @throws Exception the exception
     * @see org.springframework.web.servlet.mvc.AbstractController#handleRequestInternal(javax.servlet.http.HttpServletRequest,
     *      javax.servlet.http.HttpServletResponse)
     */
    @Override
    protected ModelAndView handleRequestInternal(HttpServletRequest request, HttpServletResponse response)
        throws Exception
    {
        // Gestion de l'appui sur l'icone de deconnexion
        String disconnect = request.getParameter("deconnecter");

        if (disconnect != null && "true".compareTo(disconnect) == 0)
        {
            request.getSession().invalidate();
            SecurityContextHolder.clearContext();
            // distinction pour cas ou on est en appliportail (retour page login) ou portail (retour magellan)
            if (authenticationPortailProvider.getModeAuthentification().compareTo(PORTAIL) == 0)
            {
                if (log.isDebugEnabled())
                {
                    log.debug("Déconnexion du mode portail et retour vers Magellan");
                }
                return new ModelAndView(new RedirectView(authenticationPortailProvider.getUlysse()));
            }
            else
            {
                if (log.isDebugEnabled())
                {
                    log.debug("Déconnexion du mode appli ou appliportail  et retour vers page de login");
                }
                return new ModelAndView(new RedirectView("identification.ex"));
            }
        }
        if (log.isDebugEnabled())
        {
            log.debug(" vers messagebloquantdetecteavecportail");
        }
        return new ModelAndView("messagebloquantdetecteavecportail");

    }

}
