/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : AffichageCaracteristiquesUtilisateurFormAction.java
 *
 */
package fr.gouv.finances.lombok.utilisateur.mvc.webflow.action;

import java.beans.PropertyEditor;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.PropertyEditorRegistry;
import org.springframework.validation.Errors;
import org.springframework.webflow.action.FormAction;
import org.springframework.webflow.action.FormObjectAccessor;
import org.springframework.webflow.execution.Event;
import org.springframework.webflow.execution.RequestContext;
import org.springframework.webflow.execution.ScopeType;

import fr.gouv.finances.lombok.securite.springsecurity.UtilisateurSecurityLombokContainer;
import fr.gouv.finances.lombok.securite.techbean.FiltreHabilitationAnnuaire;
import fr.gouv.finances.lombok.securite.techbean.HabilitationAnnuaire;
import fr.gouv.finances.lombok.securite.techbean.PersonneAnnuaire;
import fr.gouv.finances.lombok.util.propertyeditor.CpCustomDateEditor;
import fr.gouv.finances.lombok.util.propertyeditor.SelectEditor;
import fr.gouv.finances.lombok.utilisateur.mvc.form.AffichageCaracteristiquesUtilisateurForm;

/**
 * Class AffichageCaracteristiquesUtilisateurFormAction 
 * 
 * @author amleplatinec
 * @version $Revision: 1.4 $ Date: 11 déc. 2009
 */
public class AffichageCaracteristiquesUtilisateurFormAction extends FormAction
{    
    private static final Logger log = LoggerFactory.getLogger(AffichageCaracteristiquesUtilisateurFormAction.class);

    public AffichageCaracteristiquesUtilisateurFormAction()
    {
        super();
    }

    /**
     * methode Initialiser formulaire
     * 
     * @param request --
     * @return event
     * @throws Exception the exception
     */
    public Event initialiserFormulaire(RequestContext request) throws Exception
    {
        log.debug(">>> Debut methode initialiserFormulaire");
        // Lecture du formulaire
        AffichageCaracteristiquesUtilisateurForm formulaire;
        formulaire = (AffichageCaracteristiquesUtilisateurForm) request.getFlowScope().get(getFormObjectName());

        // Recuperation des infos personnes dans le contexte sécurisé
        PersonneAnnuaire personneannuaire = UtilisateurSecurityLombokContainer.getPersonneAnnuaire();

        // Attachement de la personne au formulaire
        formulaire.setUnePersonneAnnuaire(personneannuaire);

        // Recherche de la première habilitaiton et attachement au
        // formulaire
        Iterator iterhabilitations = personneannuaire.getListeHabilitations().iterator();
        if (iterhabilitations.hasNext())
        {
            formulaire.setUneHabilitation((HabilitationAnnuaire) iterhabilitations.next());
        }

        request.getFlowScope().put("listehabilitations", new ArrayList(personneannuaire.getListeHabilitations()));

        return success();
    }

    /**
     * methode Selectionner filtre par un lien 
     * 
     * @param rc --
     * @return event
     * @throws Exception the exception
     */
    public Event selectionnerFiltreParUnLien(RequestContext rc) throws Exception
    {
        log.debug(">>> Debut methode selectionnerFiltreParUnLien");
        Event retour = success();
        Set lesfiltres = (Set) rc.getFlowScope().get("listefiltres");
        PropertyEditor propertyEditor = new SelectEditor(lesfiltres, "nomDuFiltre");

        propertyEditor.setAsText(rc.getRequestParameters().get("nomDuFiltre"));
        FiltreHabilitationAnnuaire unFiltreSelectionne = (FiltreHabilitationAnnuaire) propertyEditor.getValue();
        if (unFiltreSelectionne != null)
        {
            if (unFiltreSelectionne.getListeValeursDeFiltre() != null)
            {
                rc.getFlowScope().put("listevaleursfiltres", unFiltreSelectionne.getListeValeursDeFiltre());
            }
        }
        else
        {
            Errors errors = new FormObjectAccessor(rc).getFormErrors(getFormObjectName(), ScopeType.REQUEST);
            errors.reject(null, "Problème dans la sélection du profil");
            retour = error();
        }
        return retour;
    }

    /**
     * methode Selectionner habilitation par un lien 
     * 
     * @param rc --
     * @return event
     * @throws Exception the exception
     */
    public Event selectionnerHabilitationParUnLien(RequestContext rc) throws Exception
    {
        log.debug(">>> Debut methode selectionnerHabilitationParUnLien");
        Event retour = success();
        List leshabilitations = (List) rc.getFlowScope().get("listehabilitations");
        PropertyEditor propertyEditor = new SelectEditor(leshabilitations, "nomProfil");

        propertyEditor.setAsText(rc.getRequestParameters().get("nomProfil"));
        HabilitationAnnuaire uneHabilitationSelectionnee = (HabilitationAnnuaire) propertyEditor.getValue();
        if (uneHabilitationSelectionnee != null)
        {
            if (uneHabilitationSelectionnee.getListeFiltres() != null)
            {
                rc.getFlowScope().put("listefiltres", uneHabilitationSelectionnee.getListeFiltres());
            }
            else
            {
                Errors errors = new FormObjectAccessor(rc).getFormErrors(getFormObjectName(), ScopeType.REQUEST);
                errors.reject(null, "Aucun filtre associé à ce profil");
                retour = error();
            }
        }
        else
        {
            Errors errors = new FormObjectAccessor(rc).getFormErrors(getFormObjectName(), ScopeType.REQUEST);
            errors.reject(null, "Problème dans la sélection du profil");
            retour = error();
        }
        return retour;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param context
     * @param registry
     * @see org.springframework.webflow.action.FormAction#registerPropertyEditors(org.springframework.webflow.execution.RequestContext,
     *      org.springframework.beans.PropertyEditorRegistry)
     */
    @Override
    protected void registerPropertyEditors(RequestContext context, PropertyEditorRegistry registry)
    {
        log.debug(">>> Debut methode registerPropertyEditors");
        super.registerPropertyEditors(context, registry);
        registry.registerCustomEditor(Date.class, new CpCustomDateEditor());
    }

}
