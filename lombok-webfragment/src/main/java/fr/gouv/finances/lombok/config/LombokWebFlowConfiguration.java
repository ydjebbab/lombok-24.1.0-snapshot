/*
 * Copyright (c) 2017 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.config;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.access.AccessDecisionManager;
import org.springframework.security.access.AccessDecisionVoter;
import org.springframework.security.access.vote.AffirmativeBased;
import org.springframework.webflow.config.AbstractFlowConfiguration;
import org.springframework.webflow.context.servlet.WebFlow1FlowUrlHandler;
import org.springframework.webflow.definition.registry.FlowDefinitionRegistry;
import org.springframework.webflow.executor.FlowExecutor;
import org.springframework.webflow.mvc.servlet.FlowController;
import org.springframework.webflow.security.SecurityFlowExecutionListener;

import fr.gouv.finances.lombok.mvc.webflow.listener.SubFlowEnterViewStateListener;
import fr.gouv.finances.lombok.securite.springsecurity.VoteurSecurityLombok;

/**
 * <pre>
 * Configuration webflow. Remplace la configuration XML définie initialement
 * dans un fichier webflow.xml
 * </pre>
 */
@Configuration
public class LombokWebFlowConfiguration extends AbstractFlowConfiguration
{
    @Autowired
    private LombokWebFlowSharedConfiguration lombokWebFlowSharedConfig;

    public LombokWebFlowConfiguration()
    {
        super();
    }

    /**
     * Registre des flux applicatifs ayant pour parents les flux composants.
     *
     * @return flow definition registry
     */
    @Bean
    public FlowDefinitionRegistry flowRegistry()
    {
        return getFlowDefinitionRegistryBuilder()
            .setParent(lombokWebFlowSharedConfig.composantsFlowRegistry())
            .setFlowBuilderServices(lombokWebFlowSharedConfig.flowBuilderServices())
            .addFlowLocationPattern("/WEB-INF/flows/**/*-flow.xml")
            .build();
    }

    /**
     * Ajout du listener (nécessaire par exemple pour balises secured du webflow).
     *
     * @return security flow execution listener
     */
    @Bean
    public SecurityFlowExecutionListener securityFlowExecutionListener()
    {
        SecurityFlowExecutionListener secFlowExec = new SecurityFlowExecutionListener();
        secFlowExec.setAccessDecisionManager(accessDecisionManager());
        return secFlowExec;
    }

    /**
     * Ajout du decision manager (utilisé par exemple par webflow, balise secured).
     *
     * @return access decision manager
     */
    @Bean
    public AccessDecisionManager accessDecisionManager()
    {

        List<AccessDecisionVoter<?>> voters = new ArrayList<>();
        VoteurSecurityLombok<Object> voteurLombok = new VoteurSecurityLombok<>();
        voteurLombok.setRolePrefix("");
        voters.add(voteurLombok);
        return new AffirmativeBased(voters);
    }

    /**
     * Executeur de flux.
     *
     * @return flow executor
     */
    @Bean
    public FlowExecutor flowExecutor()
    {
        return getFlowExecutorBuilder(flowRegistry())
            .setAlwaysRedirectOnPause(false)
            .setRedirectInSameState(false)
            .addFlowExecutionListener(previousstatelistener())
            .addFlowExecutionListener(securityFlowExecutionListener())
            .build();
    }

    /**
     * à voir pour le commentaire.
     *
     * @return sub flow enter view state listener
     */
    @Bean
    public SubFlowEnterViewStateListener previousstatelistener()
    {
        return new SubFlowEnterViewStateListener();
    }

    /**
     * Controleur de redirection vers le webflow.
     *
     * @return flow controller
     */
    @Bean
    public FlowController flowcontroller()
    {
        FlowController flowController = new FlowController();
        flowController.setFlowExecutor(flowExecutor());
        flowController.setFlowUrlHandler(webflow1UrlHandler());
        return flowController;
    }

    /**
     * Compatibilité pour les urls webflow 1.
     *
     * @return web flow 1 flow url handler
     */
    @Bean
    public WebFlow1FlowUrlHandler webflow1UrlHandler()
    {

        return new WebFlow1FlowUrlHandler();
    }

}
