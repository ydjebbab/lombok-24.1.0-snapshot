package fr.gouv.finances.lombok.config;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.security.access.AccessDecisionVoter;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.intercept.RunAsManagerImpl;
import org.springframework.security.access.vote.AffirmativeBased;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.security.web.access.AccessDeniedHandlerImpl;
import org.springframework.security.web.access.ExceptionTranslationFilter;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.LoginUrlAuthenticationEntryPoint;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.csrf.InvalidCsrfTokenException;
import org.springframework.security.web.csrf.MissingCsrfTokenException;
import org.springframework.stereotype.Component;

import fr.gouv.finances.lombok.securite.springsecurity.VoteurSecurityLombok;

@Component
public class LombokAuthentificationConfiguration
{
    /** force rediction vers accueil.ex **/
    @Value(value = "${springsecurity.forcerredirectionpageaccueil}")
    private Boolean forcerredirectionpageaccueil;

    @Autowired
    private LombokPortailAuthentificationConfiguration lombokPortailAuthentificationConfiguration;

    @Bean
    public AuthenticationSuccessHandler authenticationSuccessHandler()
    {
        SimpleUrlAuthenticationSuccessHandler simpleUrlAuthenticationSuccessHandler = new SimpleUrlAuthenticationSuccessHandler();
        simpleUrlAuthenticationSuccessHandler.setDefaultTargetUrl("/accueil.ex");
        simpleUrlAuthenticationSuccessHandler.setAlwaysUseDefaultTargetUrl(forcerredirectionpageaccueil);
        return simpleUrlAuthenticationSuccessHandler;
    }

    @Bean
    public AuthenticationFailureHandler authenticationFailureHandler()
    {
        SimpleUrlAuthenticationFailureHandler simpleUrlAuthenticationFailureHandler = new SimpleUrlAuthenticationFailureHandler();
        simpleUrlAuthenticationFailureHandler.setDefaultFailureUrl("/identification.ex?failed=true");
        return simpleUrlAuthenticationFailureHandler;
    }

    @Bean
    public AccessDeniedHandler accessDeniedHandler()
    {
        return new AccessDeniedHandlerImpl() 
        {
            @Override
            public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException accessDeniedException)
                throws IOException, ServletException 
            {
                if (accessDeniedException instanceof MissingCsrfTokenException
                    || accessDeniedException instanceof InvalidCsrfTokenException) 
                {
                    response.sendRedirect(request.getContextPath() + "/errors/erreurcsrf");                                        
                }
                else 
                {
                    response.sendRedirect(request.getContextPath() + "/errors/erreuraccesrefuse");
                }

                super.handle(request, response, accessDeniedException);
            }
        };
    }

    /**
     * <pre>
     * CF: appelé si un utilisateur non authentifié requiert une ressource (url) sécurisée : 
     * permet de renvoyer sur un contrôleur RedirectionTileoutControler (configuré dans dispatcher servler.xml) qui decide 
     * soit de renvoyer vers l'url du portail (à suffixer j_appelportail) soit de renvoyer vers la page de login de l'application.
     * Remplace le bean déclaré dans APPLICATIONCONTEXT-COMMUN-SECURITY.XML
     * </pre>
     *
     * @return LoginUrlAuthenticationEntryPoint
     */
    @Bean
    public LoginUrlAuthenticationEntryPoint authenticationEntryPoint()
    {
        LoginUrlAuthenticationEntryPoint loginUrlAuthenticationEntryPoint = new LoginUrlAuthenticationEntryPoint("/pagelogin*.ex");
        loginUrlAuthenticationEntryPoint.setForceHttps(false);
        return loginUrlAuthenticationEntryPoint;
    }

    /**
     * <pre>
     * CF: 
     * Filtre qui récupère les infos de contexte et oriente vers authentificationEntryPoint si l'utilisateur n'est pas authentifié.
     * Dans le cas contraire il laisse au filtre suivant filtersecurityinterceptor la decision de donner accès ou non à la ressource.
     * Remplace le bean déclaré dans APPLICATIONCONTEXT-COMMUN-SECURITY.XML
     * </pre>
     *
     * @return ExceptionTranslationFilter
     */
    @Bean
    public ExceptionTranslationFilter exceptionTranslationFilter()
    {
        ExceptionTranslationFilter exceptionTranslationFilter = new ExceptionTranslationFilter(authenticationEntryPoint());
        exceptionTranslationFilter.setAccessDeniedHandler(accessDeniedHandler());
        return exceptionTranslationFilter;
    }

    /**
     * <pre>
     * CF: Voteur prenant la décision d'accorder ou refuser l'accès à la ressource.
     * Remplace le bean déclaré dans APPLICATIONCONTEXT-COMMUN-SECURITY.XML
     * </pre>
     *
     * @return VoteurSecurityLombok
     */
    @Bean
    public VoteurSecurityLombok roleVoter()
    {
        return new VoteurSecurityLombok();
    }

    /**
     * <pre>
     * CF: appelé pour la décision d'accorder le droit à la ressource en fonction de l'habilitation de l'utilisateur. 
     * Plusieurs processus peuvent "voter" pour déterminer si le droit est accordé ou non.
     * Le feu vert est donné à partir des "GrantedAuthorities" rattachées à l'objet Authentication et lues 
     * au moment de l'authentification.
     * Remplace le bean déclaré dans APPLICATIONCONTEXT-COMMUN-SECURITY.XML
     * </pre>
     * 
     * @return AffirmativeBased
     */
    @Bean
    public AffirmativeBased accessDecisionManager()
    {
        List<AccessDecisionVoter<? extends Object>> decisionVoters = new ArrayList<>();
        decisionVoters.add(roleVoter());
        return new AffirmativeBased(decisionVoters);
    }

    /**
     * <pre>
     * CF: Remplace le bean déclaré dans APPLICATIONCONTEXT-COMMUN-SECURITY.XML 
     * Utilisé ??
     * </pre>
     *
     * @return RunAsManagerImpl
     */
    @Bean
    public RunAsManagerImpl runAsManager()
    {
        RunAsManagerImpl runAsManagerImpl = new RunAsManagerImpl();
        runAsManagerImpl.setKey("my_run_as_password");
        return runAsManagerImpl;
    }

    /**
     * <pre>
     * CF: Appelé si un utilisateur envoie un formulaire d'authentification (uid/mot de passe).
     * Par défaut, nous renvoyons vers la page accueil.ex
     *  Remplace le bean déclaré dans APPLICATIONCONTEXT-COMMUN-SECURITY.XML
     * </pre>
     *
     * @return UsernamePasswordAuthenticationFilter
     */
    @Bean
    public UsernamePasswordAuthenticationFilter authenticationProcessingFilter()
    {
        UsernamePasswordAuthenticationFilter usernamePasswordAuthenticationFilter = new UsernamePasswordAuthenticationFilter();
        // "L'action" du formulaire d'authentification doit demander l'URL précisée par le filterProcessesUrl
        usernamePasswordAuthenticationFilter.setFilterProcessesUrl("/j_spring_security_check");
        usernamePasswordAuthenticationFilter.setAuthenticationFailureHandler(authenticationFailureHandler());
        usernamePasswordAuthenticationFilter.setAuthenticationSuccessHandler(authenticationSuccessHandler());
        usernamePasswordAuthenticationFilter.setAuthenticationManager(lombokPortailAuthentificationConfiguration.authenticationManager());
        return usernamePasswordAuthenticationFilter;
    }

}
