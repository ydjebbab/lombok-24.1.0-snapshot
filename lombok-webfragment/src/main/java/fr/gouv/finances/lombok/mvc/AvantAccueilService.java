/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : AvantAccueilService.java
 *
 */
package fr.gouv.finances.lombok.mvc;

import javax.servlet.http.HttpServletRequest;

/**
 * Interface AvantAccueilService
 * 
 * @author amleplatinec
 * @version $Revision: 1.2 $ Date: 11 déc. 2009
 */
public interface AvantAccueilService
{

    /**
     * méthode exécutée avant chaque demande de accueil.ex (!!attention : et non accueilController) sinon s'exécute
     * aussi quand on accéde à listmessage ...
     * 
     * @param arg0 --
     */
    public void executerAvantPremierPassageSurAccueil(HttpServletRequest arg0);

    /**
     * methode à implémenter pour supprimer de la session (si besoin) ce qui a été mis dans executerAvantAccueil pour
     * éviter que ca se réaffiche (par exemple...)
     * 
     * @param arg0 --
     */

    public void mettreAJourSessionApresPassageAccueil(HttpServletRequest arg0);
}
