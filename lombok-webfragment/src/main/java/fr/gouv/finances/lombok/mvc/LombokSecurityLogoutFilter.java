/*
 * Copyright (c) 2011 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.mvc;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.context.SecurityContextImpl;

import fr.gouv.finances.lombok.securite.springsecurity.PortailAuthentificationSecurityLombok;

/**
 * Filtre de déconnexion Web.
 * CF: deprecated. On utilise LombokSecurityLogoutHandler à la place.
 * Classe à supprimer !
 */
@Deprecated
public class LombokSecurityLogoutFilter implements Filter
{
    /** authentication portail provider. */
    private PortailAuthentificationSecurityLombok authenticationPortailProvider;

    /** log. */
    private final Log log = LogFactory.getLog(getClass());

    /** logoutPage - String, page de deconnexion en mode appli. */
    private String logoutPage;

    /** Constant : PORTAIL. */
    private static final String PORTAIL = "portail";

    /**
     * Constructeur.
     */
    public LombokSecurityLogoutFilter()
    {
        super();
    }

    /**
     * Accesseur de l attribut portail.
     * 
     * @return portail
     */
    public static String getPortail()
    {
        return PORTAIL;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @see javax.servlet.Filter#destroy()
     */
    @Override
    public void destroy()
    {
         // RAS
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param req
     * @param res
     * @param chain
     * @throws IOException Signal qu'une execption de type I/O s'est produite.
     * @throws ServletException the servlet exception
     * @see javax.servlet.Filter#doFilter(javax.servlet.ServletRequest, javax.servlet.ServletResponse,
     *      javax.servlet.FilterChain)
     */
    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException,
        ServletException
    {
        boolean modeportail = estEnModePortail();

        if (log.isDebugEnabled())
        {
            log.debug("entree handleRequestInternal");
        }

        if (log.isDebugEnabled())
        {
            log.debug("entree de la deconnexion");
        }

        if (modeportail)
        {

            if (log.isDebugEnabled())
            {
                log.debug("deconnexion mode portail retour ulysse");
            }
            if (req instanceof HttpServletRequest)
            {
                HttpServletRequest hReq = (HttpServletRequest) req;
                HttpServletResponse hRes = (HttpServletResponse) res;

                log.debug("déconnexion  :  redirection vers : " + authenticationPortailProvider.getUlysse());

                String targetUrl = authenticationPortailProvider.getUlysse();

                hRes.sendRedirect(hRes.encodeRedirectURL(targetUrl));
                invaliderSessionUtilisateur(req);
            }
        }
        else
        {
            if (log.isDebugEnabled())
            {
                log.debug("deconnexion mode appli ou appliportail retour login");
            }
            if (req instanceof HttpServletRequest)
            {
                HttpServletRequest hReq = (HttpServletRequest) req;
                HttpServletResponse hRes = (HttpServletResponse) res;

                log.debug("déconnexion  :  redirection vers : " + logoutPage);

                String targetUrl = hReq.getContextPath() + logoutPage;

                hRes.sendRedirect(hRes.encodeRedirectURL(targetUrl));
                invaliderSessionUtilisateur(req);
            }
        }
    }

    /**
     * Gets the authentication portail provider.
     * 
     * @return the authentication portail provider
     */
    public PortailAuthentificationSecurityLombok getAuthenticationPortailProvider()
    {
        return authenticationPortailProvider;
    }

    /**
     * Gets the logoutPage - String, page de deconnexion en mode appli.
     * 
     * @return the logoutPage - String, page de deconnexion en mode appli
     */
    public String getLogoutPage()
    {
        return logoutPage;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param arg0
     * @throws ServletException the servlet exception
     * @see javax.servlet.Filter#init(javax.servlet.FilterConfig)
     */
    @Override
    public void init(FilterConfig arg0) throws ServletException
    {
        // RAS
    }

    /**
     * Sets the authentication portail provider.
     * 
     * @param authenticationPortailProvider the new authentication portail provider
     */
    public void setAuthenticationPortailProvider(PortailAuthentificationSecurityLombok authenticationPortailProvider)
    {
        this.authenticationPortailProvider = authenticationPortailProvider;
    }

    /**
     * Sets the logoutPage - String, page de deconnexion en mode appli.
     * 
     * @param logoutPage the new logoutPage - String, page de deconnexion en mode appli
     */
    public void setLogoutPage(String logoutPage)
    {
        this.logoutPage = logoutPage;
    }

    /**
     * methode Est en mode portail : --.
     * 
     * @return true, si c'est vrai
     */
    private boolean estEnModePortail()
    {
        return authenticationPortailProvider.getModeAuthentification().compareTo(PORTAIL) == 0;
    }

    /**
     * methode Invalider session utilisateur : --.
     * 
     * @param req
     */
    private void invaliderSessionUtilisateur(ServletRequest req)
    {
        if (log.isDebugEnabled())
        {
            log.debug("entrée invaliderSessionUtilisateur");
        }

        HttpServletRequest hReq = (HttpServletRequest) req;
        HttpSession session = hReq.getSession(false);
        if (session != null)
        {
            session.invalidate();
        }
        SecurityContextHolder.setContext(new SecurityContextImpl());

        if (log.isDebugEnabled())
        {
            log.debug("sortie invaliderSessionUtilisateur");
        }

    }

}
