package fr.gouv.finances.lombok.config;

import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.ProviderManager;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.ExceptionMappingAuthenticationFailureHandler;

import fr.gouv.finances.lombok.securite.springsecurity.AucunLdapAuthentificationSecurityLombok;
import fr.gouv.finances.lombok.securite.springsecurity.LdapAuthentificationSecurityLombok;
import fr.gouv.finances.lombok.securite.springsecurity.PortailAuthenticationProcessingFilter;
import fr.gouv.finances.lombok.securite.springsecurity.PortailAuthentificationSecurityLombok;
import fr.gouv.finances.lombok.securite.springsecurity.RealAuthentificationSecurityLombok;

@Configuration
@ComponentScan(basePackages = {"fr.gouv.finances.cp.dmo.controller"})
public class LombokPortailAuthentificationConfiguration
{

    @Value(value = "${lombok.aptera.codeappli}")
    private String codeApplicationDansHabilitations;

    @Value(value = "${authentification.modeauthentification}")
    private String modeAuthentification;

    @Value(value = "${annuaire.typeannuaire}")
    private String typeAnnuaire;

    @Value(value = "${authentification.urlportailprefixe}")
    private String urlPortail;

    @Value(value = "${authentification.urlportailprefixe}")
    private String urlPortailPrefixe;

    @Value(value = "${authentification.urlportailappli}")
    private String urlPortailAppli;

    @Value(value = "${authentification.adressesip}")
    private String adressesIP;

    @Value(value = "${authentification.separateurdnmotdepasse}")
    private String separateurdnmotdepasse;

    @Value(value = "${authentification.motdepasseportail}")
    private String motDePassePortail;

    @Value(value = "${authentification.ulysse}")
    private String ulysse;
    
    @Value(value = "${lombok.composant.sireme.inclus}")
    private Boolean siremeInclu;

    @Autowired
    private LombokWebSharedConfiguration lombokSiremeConfig;

    /** commun au mode portail et appli **/
    @Autowired
    private AuthenticationSuccessHandler authenticationSuccessHandler;

    /**
     * Provider pour l'accès avec ldap.
     *
     * @return ldap authentification security lombok
     */
    @Bean
    //@Profile("!sireme") // CF: ajout
    @ConditionalOnProperty(name = "lombok.composant.sireme.inclus", havingValue="false")
    public PortailAuthentificationSecurityLombok authenticationPortailProvider()
    {
        PortailAuthentificationSecurityLombok portailProvider = new PortailAuthentificationSecurityLombok();
        portailProvider
            .setAuthentificationEtLectureHabilitationsService(lombokSiremeConfig.authentificationEtLectureHabilitationsService());
        portailProvider.setCodeApplicationDansHabilitations(codeApplicationDansHabilitations);
        portailProvider.setModeAuthentification(modeAuthentification);
        portailProvider.setUrlPortail(urlPortail);
        portailProvider.setUrlPortailPrefixe(urlPortailPrefixe);
        portailProvider.setUrlPortailAppli(urlPortailAppli);
        portailProvider.setUlysse(ulysse);

        return portailProvider;
    }

    @Bean
    public AuthenticationFailureHandler authenticationFailureHandlerPortail()
    {
        ExceptionMappingAuthenticationFailureHandler failureHandler = new ExceptionMappingAuthenticationFailureHandler();
        failureHandler.setDefaultFailureUrl("/messagebloquantdetecteavecportail.ex");
        failureHandler.setExceptionMappings(Collections.unmodifiableMap(Stream.of(
            new SimpleEntry<>("org.springframework.security.AuthenticationException", "/messagebloquantdetecteavecportail.jsp"))
            .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue))));
        return failureHandler;
    }

    @Bean
    public PortailAuthenticationProcessingFilter authenticationProcessingFilterPortail()
    {
        PortailAuthenticationProcessingFilter portailFilter = new PortailAuthenticationProcessingFilter();
        portailFilter.setFilterProcessesUrl("/j_appelportail");
        List<AuthenticationProvider> listeAuthenticationProvider = new ArrayList<>();        

        // CF : remplacement profil par propriété
        //if (Arrays.stream(environment.getActiveProfiles()).anyMatch(
        //    env -> (env.equalsIgnoreCase("sireme"))))
        if (siremeInclu) {
            listeAuthenticationProvider.add(authenticationPortailProviderSireme());
        } else {
            listeAuthenticationProvider.add(authenticationPortailProvider());
        }
        
        // ProviderManager implements AuthenticationManager
        ProviderManager providerManager = new ProviderManager(listeAuthenticationProvider);
        portailFilter.setAuthenticationManager(providerManager);
        portailFilter.setAuthenticationSuccessHandler(authenticationSuccessHandler);
        portailFilter.setAuthenticationFailureHandler(authenticationFailureHandlerPortail());              
        portailFilter.setMotDePassePortail(motDePassePortail);
        portailFilter.setAdressesIP(adressesIP);
        portailFilter.setSeparateurdnmotdepasse(separateurdnmotdepasse);
        portailFilter.setPostOnly(false);
        portailFilter.setTypeAnnuaire(typeAnnuaire);

        return portailFilter;
    }

    /**
     * <pre>
     * CF :
     * Appelé quand appel demande d'authentification via portail (j_appelportail). 
     * Classe liée à spring-security. Appelée pour la récupération des infos d'habilitation sur un annuaire.
     * Le dn de l'utilisateur authentifié est récupéré dans le header de l'appel http routé par le portail. 
     * Cette classe délégue à une classe authentificationEtLectureHabilitationsService indépendante du framework spring-security.
     * Remplace le bean déclaré dans APPLICATIONCONTEXT-COMMUN-SECURITY.XML
     * </pre>
     *
     * @return PortailAuthentificationSecurityLombok
     */
    @Bean(name = "authenticationPortailProvider")
    //@Profile("sireme")
    @ConditionalOnProperty(name = "lombok.composant.sireme.inclus", havingValue="true")
    public PortailAuthentificationSecurityLombok authenticationPortailProviderSireme()
    {
        PortailAuthentificationSecurityLombok portailProvider = new PortailAuthentificationSecurityLombok();
        portailProvider
            .setAuthentificationEtLectureHabilitationsService(lombokSiremeConfig.authentificationEtLectureHabilitationsService());
        portailProvider.setCodeApplicationDansHabilitations(codeApplicationDansHabilitations);
        portailProvider.setModeAuthentification(modeAuthentification);
        portailProvider.setUrlPortail(urlPortail);
        portailProvider.setUrlPortailPrefixe(urlPortailPrefixe);
        portailProvider.setUrlPortailAppli(urlPortailAppli);
        portailProvider.setUlysse(ulysse);
        portailProvider.setSiremeserviceso(lombokSiremeConfig.getSiremeserviceso());

        return portailProvider;
    }

   

    /**
     * <pre>
     * CF: provider utilisé lorsque annuaire.typeannuaire=AUCUN, l'identifiant de connexion correspond alors au DN et le
     * mot de passe au PROFIL.
     * Remplace le bean déclaré dans APPLICATIONCONTEXT-COMMUN-SECURITY.XML
     * </pre>
     * 
     * @return AucunLdapAuthentificationSecurityLombok
     */
    @Bean
    public AucunLdapAuthentificationSecurityLombok authenticationAucunLdapProvider()
    {

        AucunLdapAuthentificationSecurityLombok aucunLdapAuthentificationSecurityLombok = new AucunLdapAuthentificationSecurityLombok();
        aucunLdapAuthentificationSecurityLombok.setTypeAnnuaire(typeAnnuaire);
        aucunLdapAuthentificationSecurityLombok.setCodeApplicationDansHabilitations(codeApplicationDansHabilitations);
        aucunLdapAuthentificationSecurityLombok.setModeAuthentification(modeAuthentification);

        return aucunLdapAuthentificationSecurityLombok;
    }

    /**
     * CF: Remplace le bean déclaré dans APPLICATIONCONTEXT-COMMUN-SECURITY.XML
     *
     * @return RealAuthentificationSecurityLombok
     */
    @Bean
    public RealAuthentificationSecurityLombok authentificationRealProvider()
    {

        RealAuthentificationSecurityLombok realAuthentificationSecurityLombok = new RealAuthentificationSecurityLombok();
        realAuthentificationSecurityLombok.setTypeAnnuaire(typeAnnuaire);
        realAuthentificationSecurityLombok.setCodeApplicationDansHabilitations(codeApplicationDansHabilitations);
        realAuthentificationSecurityLombok.setModeAuthentification(modeAuthentification);

        return realAuthentificationSecurityLombok;
    }

    /**
     * <pre>
     * CF: classe liée à spring-security, appelée pour l'authentification et la récupération des infos d'habilitation sur un annuaire.
     * Cette classe délègue à une classe authentificationEtLectureHabilitationsService indépendante du frameworkspring-security.
     * Remplace le bean déclaré dans APPLICATIONCONTEXT-COMMUN-SECURITY.XML
     * </pre>
     *
     * @return LdapAuthentificationSecurityLombok
     */
    @Bean(name = "authenticationLdapProvider")
    //@Profile("sireme")
    @ConditionalOnProperty(name = "lombok.composant.sireme.inclus", havingValue="true")
    public LdapAuthentificationSecurityLombok authenticationLdapProviderSireme()
    {
        LdapAuthentificationSecurityLombok ldapAuthentificationSecurityLombok = new LdapAuthentificationSecurityLombok();
        ldapAuthentificationSecurityLombok
            .setAuthentificationEtLectureHabilitationsService(lombokSiremeConfig.authentificationEtLectureHabilitationsService());
        ldapAuthentificationSecurityLombok.setCodeApplicationDansHabilitations(codeApplicationDansHabilitations);
        ldapAuthentificationSecurityLombok.setModeAuthentification(modeAuthentification);
        ldapAuthentificationSecurityLombok.setSiremeserviceso(lombokSiremeConfig.getSiremeserviceso());
        return ldapAuthentificationSecurityLombok;

    }

    /**
     * <pre>
     * CF: classe liée à spring-security, appelée pour l'authentification et la récupération des infos d'habilitation sur un annuaire.
     * Cette classe délègue à une classe authentificationEtLectureHabilitationsService indépendante du frameworkspring-security.
     * Remplace le bean déclaré dans APPLICATIONCONTEXT-COMMUN-SECURITY.XML
     * </pre>
     *
     * @return LdapAuthentificationSecurityLombok
     */
    @Bean
    //@Profile("!sireme")
    @ConditionalOnProperty(name = "lombok.composant.sireme.inclus", havingValue="false")
    public LdapAuthentificationSecurityLombok authenticationLdapProvider()
    {
        LdapAuthentificationSecurityLombok ldapAuthentificationSecurityLombok = new LdapAuthentificationSecurityLombok();
        ldapAuthentificationSecurityLombok
            .setAuthentificationEtLectureHabilitationsService(lombokSiremeConfig.authentificationEtLectureHabilitationsService());
        ldapAuthentificationSecurityLombok.setCodeApplicationDansHabilitations(codeApplicationDansHabilitations);
        ldapAuthentificationSecurityLombok.setModeAuthentification(modeAuthentification);
        return ldapAuthentificationSecurityLombok;
    }

    /**
     * <pre>
     * CF: appelé pour authentifier l'utilisateur et retourner un objet authentication.
     * Référence un ou plusieurs "providers d'authentification. Les providers vont s'enchainer, si retour null au premier 
     * (clé annuaire.typeannuaire != AUCUN) alors on passe au suivant.
     *  Remplace le bean déclaré dans APPLICATIONCONTEXT-COMMUN-SECURITY.XML
     * </pre>
     *
     * @return ProviderManager
     */
    @Bean
    public ProviderManager authenticationManager()
    {
        List<AuthenticationProvider> providers = new ArrayList<>();
        providers.add(authenticationAucunLdapProvider());
        providers.add(authentificationRealProvider());

        // CF : remplacement profil par propriété
        //if (Arrays.stream(environment.getActiveProfiles()).anyMatch(
        //    env -> (env.equalsIgnoreCase("sireme"))))
        if (siremeInclu) 
        {
            providers.add(authenticationLdapProviderSireme());
        }
        else
        {
            providers.add(authenticationLdapProvider());
        }

        return new ProviderManager(providers);

    }

    /**
     * <pre>
     * CF : appelé quand appel demande d'authentification via portail (j_appelportail). 
     * Remplace le bean déclaré dans APPLICATIONCONTEXT-COMMUN-SECURITY.XML
     * 
     * Non utilisé
     * </pre>
     *
     * @return ProviderManager
     */    
    /*
    @Bean
    public ProviderManager authenticationManagerPortail()
    {
        List<AuthenticationProvider> providers = new ArrayList<>();
        if (Arrays.stream(environment.getActiveProfiles()).anyMatch(
            env -> (env.equalsIgnoreCase("sireme"))))
        {
            providers.add(authenticationPortailProviderSireme());
        }
        else
        {
            providers.add(authenticationPortailProvider());
        }

        return new ProviderManager(providers);
    }
    */
    
    


}
