/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : MementoOriginator.java
 *
 */
package fr.gouv.finances.lombok.webflow;

/**
 * Interface implémentée par un formulaire qui peut capturer son propre état dans un Memento. Fourni une méthode pour
 * capturer son état courant (createMemento(level)) et une méthode pour restaurer l'état de l'objet à un niveau donné.
 * 
 * @author amleplatinec
 * @author fernandescn
 * @version $Revision: 1.2 $ Date: 11 déc. 2009
 */
public interface MementoOriginator
{

    /**
     * Crée un memento conservant l'état du formulaire.
     * 
     * @return le memento
     */
    public Memento createMemento();

    /**
     * Restaure l'état du formulaire à partir du memento et du niveau.
     * 
     * @param memento le memento
     * @param level --
     */
    public void setMemento(Memento memento, int level);

}
