/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : RedirectionTimeOutController.java
 *
 */
package fr.gouv.finances.lombok.mvc;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.context.SecurityContextImpl;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractController;
import org.springframework.web.servlet.view.RedirectView;

import fr.gouv.finances.lombok.securite.springsecurity.PortailAuthentificationSecurityLombok;

/**
 * Class RedirectionTimeOutController.
 * 
 * @author amleplatinec-cp controller qui sera apellé quand il y aura un time out en mode portail implémente le
 *         loginFormURL de authenticationEntryPoint dans le cas ou l'on est en mode portail ou appli
 * @version $Revision: 1.3 $ Date: 15 déc. 2009 cas portail : renvoit sur ulysse
 */
public class RedirectionTimeOutController extends AbstractController
{

    /** authentication portail provider. */
    private PortailAuthentificationSecurityLombok authenticationPortailProvider;

    private final Log log = LogFactory.getLog(getClass());

    public RedirectionTimeOutController()
    {
        super();         
    }

    /**
     * Accesseur de l attribut authentication portail provider.
     * 
     * @return authentication portail provider
     */
    public PortailAuthentificationSecurityLombok getAuthenticationPortailProvider()
    {
        return authenticationPortailProvider;
    }

    /**
     * Modificateur de l attribut authentication portail provider.
     * 
     * @param authenticationPortailProvider le nouveau authentication portail provider
     */
    public void setAuthenticationPortailProvider(PortailAuthentificationSecurityLombok authenticationPortailProvider)
    {
        this.authenticationPortailProvider = authenticationPortailProvider;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param request
     * @param respons
     * @return model and view
     * @throws Exception the exception
     * @see org.springframework.web.servlet.mvc.AbstractController#handleRequestInternal(javax.servlet.http.HttpServletRequest,
     *      javax.servlet.http.HttpServletResponse)
     */
    @Override
    protected ModelAndView handleRequestInternal(HttpServletRequest request, HttpServletResponse respons)
        throws Exception
    {
        SecurityContextHolder.setContext(new SecurityContextImpl());
        // on va supprimer de la session le fait que l'on a déjà vu les messages pour qu'ils se réaffichent
        // aprés la déconnexion sur la nouvelle page de login
        request.getSession().removeAttribute("listmessages");
        if (log.isDebugEnabled())
        {
            log.debug("Redirection vers la page de login suite à time-out de la session");
        }
        if (authenticationPortailProvider.getModeAuthentification().compareTo("portail") == 0)
        {
            if (log.isDebugEnabled())
            {
                log.debug("Mode portail, redirection vers : " + authenticationPortailProvider.getUrlPortail());
            }
            return new ModelAndView(new RedirectView(authenticationPortailProvider.getUrlPortail()));
        }
        else
        {
            if (log.isDebugEnabled())
            {
                log.debug("Mode appli , redirection vers : identification.ex");
            }
            return new ModelAndView(new RedirectView("identification.ex"));
        }

    }

}
