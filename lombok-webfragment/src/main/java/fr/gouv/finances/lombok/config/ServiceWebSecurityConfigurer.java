package fr.gouv.finances.lombok.config;

import org.springframework.security.config.annotation.web.builders.HttpSecurity;

/**
 * Interface pour cumuler des WebSecurityConfigurerAdapter::configure(HttpSecurity)
 * L'objet d'une classe implémentant cette interface est ensuite tirée par @Autowired
 * dans la classe de configuration héritant de WebSecurityConfigurerAdapter
 */
@FunctionalInterface
public interface ServiceWebSecurityConfigurer 
{
    void configure(HttpSecurity http) throws Exception;
}