/*
 * Copyright (c) 2017 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.web.access.WebInvocationPrivilegeEvaluator;

import fr.gouv.finances.lombok.menu.service.GenerateurMenuService;
import fr.gouv.finances.lombok.menu.service.MenuService;
import fr.gouv.finances.lombok.menu.service.impl.GenerateurMenuJSCookServiceImpl;
import fr.gouv.finances.lombok.menu.service.impl.MenuServiceImpl;
import fr.gouv.finances.lombok.securite.springsecurity.VerificationDroitAccesSecurityServiceImpl;

/**
 * Contrôleur gérant le menu. Le WebInvocationPrivilegeEvaluator remplace l'utilisation de mockservlet qui était faite
 * dans la classe VerificationDroitAccesSecurityServiceImpl
 */
@Configuration
public class LombokMenuConfiguration
{
    @Autowired
    @Lazy
    private WebInvocationPrivilegeEvaluator webInvocPrivEval;

    /** chemin vers le fichier de définition du menu. */
    @Value(value = "${appli.menu.fichierdefinitionmenu:menudefinition.xml}")
    private String fichierDefinitionMenu;

    /**
     * methode Generateurmenuso : générateur de menu.
     *
     * @return generateur menu service
     */
    @Bean
    public GenerateurMenuService generateurmenuso()
    {
        return new GenerateurMenuJSCookServiceImpl();
    }

    /**
     * <pre>
     * Vérification si accès aux urls du menu.
     * Appelé pour interroger le filtre filterSecurityInterceptor.
     * Notamment pour vérifier la possibilité d'atteindre une url pour un utilisateur authentifié.
     * </pre>
     *
     * @return verification droit acces security service impl
     */
    @Bean
    public VerificationDroitAccesSecurityServiceImpl verificationdroitaccesso()
    {
        VerificationDroitAccesSecurityServiceImpl verificationDroitAccesSecurityServiceImpl = new VerificationDroitAccesSecurityServiceImpl();
        // verifAcces.setFilterSecurityInterceptor(filterSecurityInterceptor);
        verificationDroitAccesSecurityServiceImpl.setWebInvocPrivEval(webInvocPrivEval);
        return verificationDroitAccesSecurityServiceImpl;
    }

    /**
     * Le générateur de menu s'appuie sur un fichier de définition xml qui sera converti en graphe
     * d'objets. Ce fichier doit se trouver dans le classpath.
     *
     * @return menu service
     */
    @Bean
    public MenuService menuso()
    {
        MenuServiceImpl menuServiceImpl = new MenuServiceImpl();
        menuServiceImpl.setVerificationdroitaccesso(verificationdroitaccesso());
        menuServiceImpl.setGenerateurmenuso(generateurmenuso());
        menuServiceImpl.setFichierDefinitionMenu(fichierDefinitionMenu);
        return menuServiceImpl;
    }

}
