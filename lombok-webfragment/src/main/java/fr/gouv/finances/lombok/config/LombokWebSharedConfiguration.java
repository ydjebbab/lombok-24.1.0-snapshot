/*
 * Copyright (c) 2020 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.config;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.io.ClassPathResource;
import org.springframework.security.web.access.AccessDeniedHandlerImpl;
import org.springframework.security.web.context.HttpSessionSecurityContextRepository;
import org.springframework.security.web.context.SecurityContextPersistenceFilter;
import org.springframework.security.web.csrf.CsrfFilter;
import org.springframework.security.web.csrf.HttpSessionCsrfTokenRepository;
import org.springframework.security.web.header.HeaderWriter;
import org.springframework.security.web.header.HeaderWriterFilter;
import org.springframework.security.web.header.writers.CacheControlHeadersWriter;
import org.springframework.security.web.header.writers.XContentTypeOptionsHeaderWriter;
import org.springframework.security.web.header.writers.XXssProtectionHeaderWriter;
import org.springframework.security.web.servlet.support.csrf.CsrfRequestDataValueProcessor;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;
import org.springframework.web.servlet.view.XmlViewResolver;

import fr.gouv.finances.lombok.autoconfig.LombokProperties;
import fr.gouv.finances.lombok.mvc.AccueilController;
import fr.gouv.finances.lombok.mvc.LombokSecurityLogoutSuccessHandler;
import fr.gouv.finances.lombok.securite.service.AuthentificationEtLectureHabilitationsService;
import fr.gouv.finances.lombok.securite.service.LectureApplicationService;
import fr.gouv.finances.lombok.securite.service.LecturePersonneService;
import fr.gouv.finances.lombok.securite.service.RessourceAnnuaireService;
import fr.gouv.finances.lombok.securite.service.impl.AuthentificationEtLectureHabilitationsServiceImpl;
import fr.gouv.finances.lombok.securite.springsecurity.PortailAuthentificationSecurityLombok;
import fr.gouv.finances.lombok.securite.springsecurity.SessionExpirationFilter;
import fr.gouv.finances.lombok.securite.techbean.WebflowRequiresCsrfMatcher;
import fr.gouv.finances.lombok.srm.service.SiremeService;

/**
 * <pre>
 * Configuration des contrôleurs web dont l'accueil. 
 * Différent suivant l'activation ou non du profil SIREME.
 * </pre>
 */
@Configuration
// @Import(value = {LombokMenuConfiguration.class})
@EnableConfigurationProperties(LombokProperties.class)
public class LombokWebSharedConfiguration
{
    private static final Logger log = LoggerFactory.getLogger(LombokWebSharedConfiguration.class);

    private static final String SPRING_SECURITY_LOGOUT_PATH = "/j_spring_security_logout*";

    private static final String SPRING_SECURITY_CHECK_PATH = "/j_spring_security_check*";

    private static final String IDENTIFICATION_PATH = "/identification.ex";

    private static final String APPELPORTAIL_PATH = "/j_appelportail";

    private static final String SESSION_EXPIREE_PATH = "/sessionexpiree.ex";

    private static final String FLUX_PATH = "/**/flux.ex*";

    private static final String RACINE_PATH = "/**";

    private static final String ERREUR_CSRF_PATH = "/WEB-INF/pages/errors/erreurcsrf.jsp";
    
    @Autowired
    LombokProperties lombokProperties;

    //@Value(value = "${lombok.aptera.codeappli}")
    //private String codeApplicationDansHabilitations;

    @Value(value = "${authentification.modeauthentification}")
    private String modeAuthentification;

    @Value(value = "${authentification.urlportail}")
    private String urlPortail;

    @Value(value = "${authentification.urlportailprefixe}")
    private String urlPortailPrefixe;

    @Value(value = "${authentification.urlportailappli}")
    private String urlPortailAppli;

    @Value(value = "${authentification.ulysse}")
    private String ulysse;

    @Value(value = "${annuaire.typeannuaire}")
    private String typeAnnuaire;

    // Taille maximum des fichiers transférés en octets
    @Value(value = "${lombok.composant.upload.maxsize}")
    private int maxUploadSize;

    @Value(value = "${authentification.afficherexpirationsession}")
    private boolean afficherexpirationsession;

    @Autowired
    private LombokMenuConfiguration menuConfig;

    @Autowired
    @Qualifier("lectureapplicationserviceimpl")
    private LectureApplicationService lectureapplicationserviceso;

    @Autowired
    @Qualifier("ressourceannuairedgcpimpl")
    private RessourceAnnuaireService ressourceannuaireso;

    @Autowired
    @Qualifier("lecturepersonneserviceimpl")
    private LecturePersonneService lecturepersonneso;

    /** siremeserviceso vaut null si le profile sireme est absent */
    @Autowired(required = false)
    private SiremeService siremeserviceso;

    @Autowired
    private LombokAuthentificationConfiguration lombokAuthentificationConfiguration;

    @Autowired
    private LombokPortailAuthentificationConfiguration lombokPortailAuthentificationConfiguration;

    public LombokWebSharedConfiguration()
    {
        super();
    }

    /**
     * Contrôleur pour l'accueil en profils sireme et !sireme (le service est alors null).
     *
     * @return accueil controller
     */
    @Bean
    public AccueilController accueilController()
    {
        AccueilController accueilController = new AccueilController();
        accueilController.setMenuso(menuConfig.menuso());
        accueilController.setAuthenticationPortailProvider(authenticationPortailProvider());
        accueilController.setSiremeserviceso(siremeserviceso);
        accueilController.setLectureapplicationserviceso(lectureapplicationserviceso);
        return accueilController;
    }

    /**
     * Provider portail.
     *
     * @return portail authentification security lombok
     */
    @Bean
    public PortailAuthentificationSecurityLombok authenticationPortailProvider()
    {
        PortailAuthentificationSecurityLombok portailAuthentificationSecurityLombok = new PortailAuthentificationSecurityLombok();
        portailAuthentificationSecurityLombok
            .setAuthentificationEtLectureHabilitationsService(authentificationEtLectureHabilitationsService());
        portailAuthentificationSecurityLombok.setCodeApplicationDansHabilitations(lombokProperties.getApteraProperties().getCodeappli());
        portailAuthentificationSecurityLombok.setModeAuthentification(modeAuthentification);
        portailAuthentificationSecurityLombok.setUrlPortail(urlPortail);
        portailAuthentificationSecurityLombok.setUrlPortailPrefixe(urlPortailPrefixe);
        portailAuthentificationSecurityLombok.setUrlPortailAppli(urlPortailAppli);
        portailAuthentificationSecurityLombok.setUlysse(ulysse);
        portailAuthentificationSecurityLombok.setSiremeserviceso(siremeserviceso);
        return portailAuthentificationSecurityLombok;
    }

    /**
     * Lecture des habilitations.
     *
     * @return authentification et lecture habilitations service
     */
    @Bean
    public AuthentificationEtLectureHabilitationsService authentificationEtLectureHabilitationsService()
    {
        AuthentificationEtLectureHabilitationsServiceImpl authentificationEtLectureHabilitationsServiceImpl =
            new AuthentificationEtLectureHabilitationsServiceImpl();
        authentificationEtLectureHabilitationsServiceImpl.setLecturepersonneso(lecturepersonneso);
        authentificationEtLectureHabilitationsServiceImpl.setRessourceannuaireso(ressourceannuaireso);
        authentificationEtLectureHabilitationsServiceImpl.setTypeAnnuaire(typeAnnuaire);
        return authentificationEtLectureHabilitationsServiceImpl;
    }

    /**
     * <pre>
     * Ce résolveur doit être systématiquement traité en dernier
     * </pre>
     * 
     * @return internal resource view resolver
     */
    @Bean
    public InternalResourceViewResolver lombokInternalResourceViewResolver()
    {
        InternalResourceViewResolver internalResourceViewResolver = new InternalResourceViewResolver();
        internalResourceViewResolver.setViewClass(JstlView.class);
        internalResourceViewResolver.setPrefix("/WEB-INF/pages/");
        internalResourceViewResolver.setSuffix(".jsp");
        // internalResourceViewResolver.setOrder(2);
        return internalResourceViewResolver;
    }

    /**
     * <pre>
     * CF: Resolver pour les beans telechargement et atlas
     * </pre>
     * 
     * @return
     */
    @Bean
    public XmlViewResolver lombokXMLViewResolver()
    {
        XmlViewResolver xmlViewResolver = new XmlViewResolver();
        xmlViewResolver.setLocation(new ClassPathResource("conf/applicationContext-views.xml"));
        xmlViewResolver.setOrder(1);
        return xmlViewResolver;
    }

    public SiremeService getSiremeserviceso()
    {
        return siremeserviceso;
    }

    public void setSiremeserviceso(SiremeService siremeserviceso)
    {
        this.siremeserviceso = siremeserviceso;
    }

    /**
     * CF : traitement des requêtes utilisant l'encodage MULTIPART/FORM-DATA utilisé lors de l'upload de fichiers.
     * Remplace le fichier APPLICATIONCONTEXT-MULTIPARTRESOLVER.XML
     * 
     * @return CommonsMultipartResolver
     */
    @Bean
    public CommonsMultipartResolver multipartResolver()
    {
        CommonsMultipartResolver commonsMultipartResolver = new CommonsMultipartResolver();
        commonsMultipartResolver.setDefaultEncoding("utf-8");
        commonsMultipartResolver.setMaxUploadSize(maxUploadSize);
        return commonsMultipartResolver;
    }

    /**
     * CF : Remplace le fichier APPLICATIONCONTEXT-VIEWS.XML
     *
     * @return TelechargementView
     */
    /*
     * @Bean(name = "/telechargement") public TelechargementView telechargement() { return new TelechargementView(); }
     */

    /**
     * CF : bean qui réalise l'authentification et la récupération des infos d'habilitation sur un annuaire LDAP.
     * Remplace le bean déclaré dans APPLICATIONCONTEXT-COMMUN-SECURITY.XML
     *
     * @return AuthentificationEtLectureHabilitationsServiceImpl
     */
    @Bean
    public AuthentificationEtLectureHabilitationsServiceImpl authentificationetlecturehabilitationsserviceimpl()
    {
        AuthentificationEtLectureHabilitationsServiceImpl authentificationEtLectureHabilitationsServiceImpl =
            new AuthentificationEtLectureHabilitationsServiceImpl();
        authentificationEtLectureHabilitationsServiceImpl.setLecturepersonneso(lecturepersonneso);
        authentificationEtLectureHabilitationsServiceImpl.setRessourceannuaireso(ressourceannuaireso);
        authentificationEtLectureHabilitationsServiceImpl.setTypeAnnuaire(typeAnnuaire);
        return authentificationEtLectureHabilitationsServiceImpl;
    }

    /**
     * CF: Filtre pour activer l'écriture dans le header des éléments de sécurité Remplace le bean déclaré dans
     * APPLICATIONCONTEXT-COMMUN-SECURITY.XML
     *
     * @return HeaderWriterFilter
     */
    @Bean
    public HeaderWriterFilter headerWriterFilter()
    {
        List<HeaderWriter> headerWriters = new ArrayList<>();
        headerWriters.add(new CacheControlHeadersWriter());
        headerWriters.add(new XXssProtectionHeaderWriter());
        headerWriters.add(new XContentTypeOptionsHeaderWriter());
        HeaderWriterFilter headerWriterFilter = new HeaderWriterFilter(headerWriters);

        return headerWriterFilter;
    }

    /**
     * CF: filtre pour activer la protection CSRF Remplace le bean déclaré dans APPLICATIONCONTEXT-COMMUN-SECURITY.XML
     *
     * @return HttpSessionCsrfTokenRepository
     */
    @Bean
    public HttpSessionCsrfTokenRepository csrfTokenRepository()
    {
        return new HttpSessionCsrfTokenRepository();
    }

    /**
     * CF: Remplace le bean déclaré dans APPLICATIONCONTEXT-COMMUN-SECURITY.XML
     *
     * @return CsrfFilter
     */
    // @Bean
    // public CsrfFilter csrfFilter()
    // {
    // CsrfFilter csrfFilter = new CsrfFilter(csrfTokenRepository());
    //
    // AccessDeniedHandlerImpl accessDeniedHandlerImpl = new AccessDeniedHandlerImpl();
    // accessDeniedHandlerImpl.setErrorPage(ERREUR_CSRF_PATH);
    // csrfFilter.setAccessDeniedHandler(accessDeniedHandlerImpl);
    // return csrfFilter;
    // }

    /**
     * CF: Remplace le bean déclaré dans APPLICATIONCONTEXT-COMMUN-SECURITY.XML
     *
     * @return CsrfRequestDataValueProcessor
     */
    @Bean
    public CsrfRequestDataValueProcessor requestDataValueProcessor()
    {
        return new CsrfRequestDataValueProcessor();
    }

    /**
     * CF: Filtre utilisé pour détecter l'expiration des sessions utilisateurs. Le cas échéant, il effectue une
     * redirection vers une page informant l'utilisateur de l'expiration de sa session et propose un lien de reconnexion
     * redirigeant vers la page d'authentification ou le portail. <br/>
     * Remplace le bean déclaré dans APPLICATIONCONTEXT-COMMUN-SECURITY.XML
     *
     * @return SessionExpirationFilter
     */
    @Bean
    public SessionExpirationFilter sessionExpirationFilter()
    {
        SessionExpirationFilter sessionExpirationFilter = new SessionExpirationFilter();
        sessionExpirationFilter.setUrlExpiration(SESSION_EXPIREE_PATH);
        sessionExpirationFilter.setAfficherExpiration(afficherexpirationsession);
        return sessionExpirationFilter;
    }

    /**
     * <pre>
     * CF: Filtre Utilisé pour stocker le contexte sécurisé dans la session entre deux requêtes : 
     * - intercepte la requête et copie le contexte depuis la session vers la requête (attaché au threadlocal) 
     * - en fin de traitement de la requête, copie le contenu du contexte dans la session
     * Doit etre activé en premier pour que les autres filtres aient accès aux données du contexte sécurisé.  
     * Remplace le bean déclaré dans APPLICATIONCONTEXT-COMMUN-SECURITY.XML
     * </pre>
     *
     * @return SecurityContextPersistenceFilter
     */
    @Bean
    public SecurityContextPersistenceFilter httpSessionContextIntegrationFilter()
    {
        return new SecurityContextPersistenceFilter();
    }

    /**
     * <pre>
     * CF: Remplace le bean déclaré dans APPLICATIONCONTEXT-COMMUN-SECURITY.XML 
     * FIXME : mais non utilisé ?
     * </pre>
     * 
     * @return HttpSessionSecurityContextRepository
     */
    @Bean
    public HttpSessionSecurityContextRepository securityContextRepository()
    {
        return new HttpSessionSecurityContextRepository();
    }

    /**
     * CF: Remplace le bean déclaré dans APPLICATIONCONTEXT-COMMUN-SECURITY.XML
     *
     * @return LombokSecurityLogoutFilter
     */

    /*
     * @Bean public LombokSecurityLogoutFilter logoutFilter() { log.debug(">>> Debut logoutFilter()");
     * LombokSecurityLogoutFilter lombokSecurityLogoutFilter = new LombokSecurityLogoutFilter();
     * lombokSecurityLogoutFilter.setAuthenticationPortailProvider(authenticationPortailProvider());
     * lombokSecurityLogoutFilter.setLogoutPage(IDENTIFICATION_PATH); return lombokSecurityLogoutFilter; }
     */

    @Bean
    public LombokSecurityLogoutSuccessHandler logoutSuccessHandler()
    {
        log.debug(">>> Debut logoutSuccessHandler()");
        return new LombokSecurityLogoutSuccessHandler();
    }

    /**
     * <pre>
     * CF: Filtre principal : les interceptions de pages réalisées par le filtre tomcat (voir web.xml) sont traitées ici 
     * et redirigées vers les filtres secondaires.
     * Le profile nowebflowcsrf permet de ne pas activer la protection CSRF sur les gets du webflow.
     * Remplace la chaîne de filtre déclarée dans APPLICATIONCONTEXT-COMMUN-SECURITY.XML
     * </pre>
     * 
     * @return FilterChainProxy
     */
    /*
     * @Bean
     * @Profile("nowebflowcsrf") public FilterChainProxy filterChainProxy() { log.debug(">>> Debut filterChainProxy()");
     * List<SecurityFilterChain> filterChains = new ArrayList<>(); filterChains.add(new DefaultSecurityFilterChain(new
     * AntPathRequestMatcher(SPRING_SECURITY_LOGOUT_PATH), // httpSessionContextIntegrationFilter(), logoutFilter()));
     * httpSessionContextIntegrationFilter())); filterChains.add(new DefaultSecurityFilterChain(new
     * AntPathRequestMatcher(SPRING_SECURITY_CHECK_PATH), httpSessionContextIntegrationFilter(),
     * lombokAuthentificationConfiguration.authenticationProcessingFilter())); filterChains.add(new
     * DefaultSecurityFilterChain(new AntPathRequestMatcher(IDENTIFICATION_PATH), httpSessionContextIntegrationFilter(),
     * lombokAuthentificationConfiguration.exceptionTranslationFilter())); filterChains.add(new
     * DefaultSecurityFilterChain(new AntPathRequestMatcher(APPELPORTAIL_PATH), httpSessionContextIntegrationFilter(),
     * lombokPortailAuthentificationConfiguration.authenticationProcessingFilterPortail())); filterChains.add(new
     * DefaultSecurityFilterChain(new AntPathRequestMatcher(SESSION_EXPIREE_PATH),
     * httpSessionContextIntegrationFilter(), lombokAuthentificationConfiguration.exceptionTranslationFilter()));
     * filterChains.add(new DefaultSecurityFilterChain(new AntPathRequestMatcher(RACINE_PATH),
     * sessionExpirationFilter(), httpSessionContextIntegrationFilter(),
     * lombokAuthentificationConfiguration.exceptionTranslationFilter(), csrfFilter(), headerWriterFilter())); return
     * new FilterChainProxy(filterChains); }
     */
    /**
     * <pre>
     * CF: Filtre principal : les interceptions de pages réalisées par le filtre tomcat (voir
     * web.xml) sont traitées ici et redirigées vers les filtres secondaires. 
     * Remplace la chaîne de filtre déclarée dans APPLICATIONCONTEXT-COMMUN-SECURITY.XML
     * </pre>
     * 
     * @return FilterChainProxy
     */
    /*
     * @Bean(name = "filterChainProxy")
     * @Profile("!nowebflowcsrf") public FilterChainProxy filterChainProxySansnowebflowcsrf() {
     * log.debug(">>> Debut filterChainProxySansnowebflowcsrf()"); List<SecurityFilterChain> filterChains = new
     * ArrayList<>(); filterChains.add(new DefaultSecurityFilterChain(new
     * AntPathRequestMatcher(SPRING_SECURITY_LOGOUT_PATH), // httpSessionContextIntegrationFilter(), logoutFilter()));
     * httpSessionContextIntegrationFilter())); filterChains.add(new DefaultSecurityFilterChain(new
     * AntPathRequestMatcher(SPRING_SECURITY_CHECK_PATH), httpSessionContextIntegrationFilter(),
     * lombokAuthentificationConfiguration.authenticationProcessingFilter())); filterChains.add(new
     * DefaultSecurityFilterChain(new AntPathRequestMatcher(IDENTIFICATION_PATH), httpSessionContextIntegrationFilter(),
     * lombokAuthentificationConfiguration.exceptionTranslationFilter())); filterChains.add(new
     * DefaultSecurityFilterChain(new AntPathRequestMatcher(APPELPORTAIL_PATH), httpSessionContextIntegrationFilter(),
     * lombokPortailAuthentificationConfiguration.authenticationProcessingFilterPortail())); filterChains.add(new
     * DefaultSecurityFilterChain(new AntPathRequestMatcher(SESSION_EXPIREE_PATH),
     * httpSessionContextIntegrationFilter(), lombokAuthentificationConfiguration.exceptionTranslationFilter()));
     * filterChains.add(new DefaultSecurityFilterChain(new AntPathRequestMatcher(FLUX_PATH), sessionExpirationFilter(),
     * httpSessionContextIntegrationFilter(), lombokAuthentificationConfiguration.exceptionTranslationFilter(),
     * csrfWebflowFilter(), headerWriterFilter())); filterChains.add(new DefaultSecurityFilterChain(new
     * AntPathRequestMatcher(RACINE_PATH), sessionExpirationFilter(), httpSessionContextIntegrationFilter(),
     * lombokAuthentificationConfiguration.exceptionTranslationFilter(), csrfFilter(), headerWriterFilter())); return
     * new FilterChainProxy(filterChains); }
     */

    /**
     * <pre>
     * CF: ce bean active la protection CSRF pour les méthodes gets du webflow (hors démarrage de flux), voir la classe
     * WebflowRequiresCsrfMatcher. 
     * Remplace le bean déclaré dans APPLICATIONCONTEXT-COMMUN-SECURITY.XML
     * </pre>
     * 
     * @return CsrfFilter
     */
    @Bean
    @Profile("!nowebflowcsrf")
    public CsrfFilter csrfWebflowFilter()
    {
        log.debug(">>> Debut csrfWebflowFilter()");
        CsrfFilter csrfFilter = new CsrfFilter(csrfTokenRepository());
        AccessDeniedHandlerImpl accessDeniedHandlerImpl = new AccessDeniedHandlerImpl();
        accessDeniedHandlerImpl.setErrorPage(ERREUR_CSRF_PATH);
        csrfFilter.setAccessDeniedHandler(accessDeniedHandlerImpl);
        csrfFilter.setRequireCsrfProtectionMatcher(new WebflowRequiresCsrfMatcher());

        return csrfFilter;
    }

}
