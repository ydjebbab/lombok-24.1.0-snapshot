/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : FormStatePersister.java
 *
 */
package fr.gouv.finances.lombok.webflow;

import org.springframework.webflow.action.FormAction;
import org.springframework.webflow.execution.RequestContext;

/**
 * Interface permettant la sauvegarde et la restauration des données d'un formulaire, à un
 * niveau donné.
 * 
 * @author chouard-cp
 * @version $Revision: 1.2 $ Date: 15 déc. 2009
 */
public interface FormStatePersister
{
    /**
     * Restaure l'état du formulaire dans le contexte, à un niveau donné.
     * 
     * @param form --
     * @param context the flow execution request context
     * @param formaction --
     * @param level --
     * @return the formulaire
     */
    public Object restoreState(Object form, RequestContext context, FormAction formaction, int level);

    /**
     * Sauvegarde l'état du formulaire dans le contexte, à un niveau donné.
     * 
     * @param form le formulaire
     * @param context le contexte d'exécution du flux
     * @param formaction --
     * @param level --
     */
    public void saveState(Object form, RequestContext context, FormAction formaction, int level);

}
