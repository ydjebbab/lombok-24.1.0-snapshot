/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 *
 */
package fr.gouv.finances.lombok.mvc;

import java.io.PrintWriter;
import java.io.StringWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.ws.WebServiceException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.core.Ordered;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.multipart.MaxUploadSizeExceededException;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.webflow.conversation.NoSuchConversationException;
import org.springframework.webflow.engine.NoMatchingTransitionException;
import org.springframework.webflow.execution.ActionExecutionException;
import org.springframework.webflow.execution.repository.NoSuchFlowExecutionException;

import fr.gouv.finances.lombok.util.exception.ApplicationException;
import fr.gouv.finances.lombok.util.exception.ApplicationExceptionTransformateur;
import fr.gouv.finances.lombok.util.exception.ExploitationException;

/**
 * <pre>
 * Intercepteur d'exceptions : permet d'éviter que les exceptions ingérables arrivent jusqu'à TOMCAT et soient affichées dans une page sans présentation. 
 * Implémente l'interface HandlerExceptionResolver pour que Spring reroute vers ce bean toutes les exceptions remontées sur la Dispatcher Servlet.
 * On est assuré de récupérer la quasi totalité des exceptions 3 catégories de pages d'erreur : 
 * 1) programmation : pour toute erreur inattendue informer l'utilisateur qu'une erreur nécessitant un correctif (en intégration ou en développement) est apparue :
 * routage du message vers le centre de développement ou d'intégration 
 * 2) exploitation : pour tout problème d'accès à une ressource : informer l'utilisateur qu'il peut tenter de se connecter ultérieurement : routage du message vers
 * l'exploitation (types d'exception à ajouter) 
 * 3) navigation ou flux expiré : l'utilisateur peut se reconnecter pour continuer son traitement.
 * </pre>
 * 
 * @author chouard-cp
 * @version $Revision: 1.2 $ Date: 15 déc. 2009
 */
public class ExceptionResolver implements HandlerExceptionResolver, Ordered
{
    
    /** Initialisation de la journalisation */
    private final Log log = LogFactory.getLog(getClass());

    /** Constante utilisée dans le calcul de l'ordre (méthode {@link #getOrder()}) */
    private static final int CONSTANTE_ORDRE = 2;
    
    // CF: répertoire qui contient les pages JSP des erreurs 
    private static final String CHEMIN_ERROR = "errors/"; 
    

    /**
     * Constructeur de la classe ExceptionResolver.java
     */
    public ExceptionResolver()
    {
        super();
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param rq
     * @param rs
     * @param controller
     * @param exception
     * @return model and view
     * @see org.springframework.web.servlet.HandlerExceptionResolver#resolveException(javax.servlet.http.HttpServletRequest,
     *      javax.servlet.http.HttpServletResponse, java.lang.Object, java.lang.Exception)
     */
    @Override
    public ModelAndView resolveException(HttpServletRequest rq, HttpServletResponse rs, Object controller,
        Exception exception)
    {

        String erreurProgrammation = "oui";
        String vueARetourner = null;

        // Recupération de la pile d'erreur dans une chaine de caractères
        StringWriter sw = new StringWriter();
        exception.printStackTrace(new PrintWriter(sw));
        String pileexception = sw.toString();

        // Recuperation de la pile d'erreur nested
        rq.setAttribute("msgexception", "Pile d'exception :");
        rq.setAttribute("pileexception", pileexception);
        rq.setAttribute("messageexception", exception.getMessage());
        // On trace toutes ces exceptions dans la log
        log.info(sw.toString());

        if (exception instanceof MaxUploadSizeExceededException)
        {
            // controller = null
            vueARetourner = null;
        }
        else if (exception instanceof NoMatchingTransitionException)
        {
            vueARetourner = CHEMIN_ERROR + "erreurnavigation";
        }
        else if (exception instanceof NoSuchFlowExecutionException || exception instanceof NoSuchConversationException)
        {
            vueARetourner = CHEMIN_ERROR + "erreurflowexpired";
        }

        // ajout d'une page spécifique lors de l'indisponibilité d'un web service
        else if (exception instanceof ActionExecutionException && exception.getCause() instanceof WebServiceException)
        {
            vueARetourner = CHEMIN_ERROR + "erreurwebservice";
        }

        // rajout pour erreur rcp pages avec mauvaise url en cache
        else if (exception instanceof IllegalArgumentException
            && exception.getMessage().compareToIgnoreCase("An id is required to lookup a FlowDefinition") == 0)
        {
            vueARetourner = CHEMIN_ERROR + "erreurflowid";
        }
        else if (exception instanceof AccessDeniedException || exception.getCause() instanceof AccessDeniedException)
        {
            vueARetourner = CHEMIN_ERROR + "erreuraccesrefuse";
        }
        else
        {
            ApplicationException ae = ApplicationExceptionTransformateur.transformer(exception);
            if (ae instanceof ExploitationException)
            {
                erreurProgrammation = "non";
                vueARetourner = CHEMIN_ERROR + "erreurexploitation";
            }
            else
            {
                vueARetourner = CHEMIN_ERROR + "erreurprogrammation";
            }
        }

        rq.setAttribute("esterreurprogrammation", erreurProgrammation);

        if (controller == null || vueARetourner == null)
        {
            return null;
        }
        else
        {
            return new ModelAndView(vueARetourner);
        }
    }

    @Override
    public int getOrder()
    {
        return Integer.MAX_VALUE - CONSTANTE_ORDRE;
    }
}