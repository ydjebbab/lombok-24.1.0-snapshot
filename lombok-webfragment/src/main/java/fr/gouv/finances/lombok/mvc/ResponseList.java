/*
 * Copyright (c) 2016 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.mvc;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import fr.gouv.finances.lombok.srm.bean.Message;

/**
 * Class ResponseList
 */
@XmlRootElement
public class ResponseList
{

    private List<Message> list;

    public ResponseList()
    {
        super();         
    }

    /**
     * Accesseur de l attribut list.
     *
     * @return list
     */
    @XmlElement
    public List<Message> getList()
    {
        return list;
    }

    /**
     * Modificateur de l attribut list.
     *
     * @param list le nouveau list
     */
    public void setList(List<Message> list)
    {
        this.list = list;
    }
}
