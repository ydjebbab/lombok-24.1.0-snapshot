/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : TelechargementView.java
 *
 */
package fr.gouv.finances.lombok.mvc;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.servlet.view.AbstractView;

import fr.gouv.finances.lombok.upload.bean.FichierJoint;
import fr.gouv.finances.lombok.util.exception.ApplicationExceptionTransformateur;
import fr.gouv.finances.lombok.util.exception.ProgrammationException;

/**
 * Vue utilisable depuis un enchainement dans un flux pour télécharger un fichier mis dans le
 * contexte.
 * 
 * @author chouard-cp
 * @version $Revision: 1.2 $ Date: 15 déc. 2009
 */
public class TelechargementView extends AbstractView
{

    /**
     * Clé sous laquelle un objet de type FichierJoint ou byte[] doit être stocké dans le model de la vue pour être
     * utilisable par cette vue.
     */
    public static final String FICHIER_A_TELECHARGER = "TelechargementView_fichierATelecharger";

    /** Clé utilisée pour spécifier le nom du fichier à télécharger. */
    public static final String NOM_FICHIER = "TelechargementView_nomFichier";

    /** Type MIME par défaut utilisé quand l'objet FichierJoint ne contient pas de type MIME. */
    public static final String TYPE_MIME_PAR_DEFAULT = "text/plain";

    /**
     * Flag qui indique si l'utilisateur doit se voir proposer l'enregistrement ou l'ouverture du fichier par le
     * navigateur. Vrai par défaut
     */
    public static final String DEMANDE_ENREGISTREMENT_FICHIER = "TelechargementView_Demander_Enregistrement_Fichier";

    /**
     * Nom de fichier par défaut utilisé quand la propriété nomFichierOriginal de l'objet FichierJoint est nulle ou
     * composée de caractères blancs.
     */
    public static final String NOM_FICHIER_PAR_DEFAULT = "telechargement";

    /** Taille du buffer utilisée pour lire le fichier temporaire et écrire sur la réponse de la servlet. */
    private static final int BUFFER_SIZE = 1024;

    /** Constant : log. */
    private static final Log log = LogFactory.getLog(TelechargementView.class);

    public TelechargementView()
    {
        super();      
    }

    /**
     * Production de la vue.
     * 
     * @param model --
     * @param request --
     * @param response --
     */
    @Override
    protected void renderMergedOutputModel(Map model, HttpServletRequest request, HttpServletResponse response)
    {
        FichierJoint fichierJoint = this.getFichierJointFromModel(model);

        response.setContentLength(this.getTailleDuFichierJoint(fichierJoint));

        response.setContentType(this.getTypeMimeDuFichierJoint(fichierJoint));

        if (this.isDemanderEnregistrementFichier(model))
        {
            response.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
            response.setHeader("Pragma", "public");
            response.setHeader("Content-Disposition", "attachment; filename=\""
                + this.getNomFichier(model, fichierJoint) + "\"");
        }

        if (fichierJoint != null && fichierJoint.getLeContenuDuFichier() != null
            && fichierJoint.getLeContenuDuFichier().getData() != null)
        {
            byte[] dataBytes = fichierJoint.getLeContenuDuFichier().getData();
            streamBinaryDataFromArray(dataBytes, response, this.getTailleDuFichierJoint(fichierJoint));
        }
        else if (fichierJoint != null && fichierJoint.getLeContenuDuFichier() != null
            && fichierJoint.getLeContenuDuFichier().getFichierTemporaire() != null)
        {
            File fichierTemporaire = fichierJoint.getLeContenuDuFichier().getFichierTemporaire();
            streamBinaryDataFromFile(fichierTemporaire, response, BUFFER_SIZE);
        }
    }

    /**
     * Lecture du fichier à télécharger stocké dans le model de la vue.
     * 
     * @param model --
     * @return FichierJoint
     */
    private FichierJoint getFichierJointFromModel(Map model)
    {
        FichierJoint result = null;

        if (model.containsKey(TelechargementView.FICHIER_A_TELECHARGER))
        {
            Object object = model.get(TelechargementView.FICHIER_A_TELECHARGER);
            if (object instanceof FichierJoint)
            {
                result = (FichierJoint) model.get(TelechargementView.FICHIER_A_TELECHARGER);
            }
            else if (object instanceof byte[])
            {
                byte[] data = (byte[]) object;
                result = new FichierJoint(data);
            }
            else
            {
                throw new ProgrammationException("LE BEAN STOCKE DANS LE MODEL SOUS LA CLE " + FICHIER_A_TELECHARGER
                    + " DOIT ETRE DE TYPE FICHIERJOINT OU BYTE[]");
            }
        }
        else
        {
            throw new ProgrammationException("AUCUN BEAN DE TYPE FICHIERJOINT N'EST STOCKE DANS LE MODEL SOUS LA CLE "
                + FICHIER_A_TELECHARGER);
        }

        return result;
    }

    /**
     * Lecture du nom de fichier à présenter à l'utilisateur.
     * 
     * @param model --
     * @param fichierJoint --
     * @return the nom fichier
     */
    private String getNomFichier(Map model, FichierJoint fichierJoint)
    {
        String result = null;
        if (model.containsKey(TelechargementView.NOM_FICHIER))
        {
            result = model.get(TelechargementView.NOM_FICHIER).toString();
        }
        else if (!StringUtils.isBlank(fichierJoint.getNomFichierOriginal()))
        {
            result = fichierJoint.getNomFichierOriginal();
        }
        else
        {
            result = NOM_FICHIER_PAR_DEFAULT;
        }
        return result;
    }

    /**
     * Lecture de la taille du fichier associé à l'objet FichierJoint.
     * 
     * @param fichierJoint --
     * @return taille du fichier en octets
     */
    private int getTailleDuFichierJoint(FichierJoint fichierJoint)
    {
        int result;
        long tailleFichier;

        if (fichierJoint != null && fichierJoint.getLeContenuDuFichier() != null
            && fichierJoint.getLeContenuDuFichier().getData() != null)
        {
            if (fichierJoint.getTailleFichier() > 0)
            {
                tailleFichier = fichierJoint.getTailleFichier();
            }
            else
            {
                tailleFichier = fichierJoint.getLeContenuDuFichier().getData().length;
            }
        }
        else if (fichierJoint != null && fichierJoint.getLeContenuDuFichier() != null
            && fichierJoint.getLeContenuDuFichier().getFichierTemporaire() != null)
        {
            if (fichierJoint.getTailleFichier() > 0)
            {
                tailleFichier = fichierJoint.getTailleFichier();
            }
            else
            {
                tailleFichier = fichierJoint.getLeContenuDuFichier().getFichierTemporaire().length();
            }
        }
        else
        {
            throw new ProgrammationException("Le fichier joint ou son contenu est null");
        }

        if (fichierJoint.getTailleFichier() > Integer.MAX_VALUE)
        {
            throw new ProgrammationException("La taille du fichier est trop importante.");
        }
        else
        {
            result = Integer.valueOf("" + tailleFichier).intValue();
        }

        return result;
    }

    /**
     * Lecture du type MIME associé à l'objet FICHIERJOINT.
     * 
     * @param fichierJoint --
     * @return the type mime du fichier joint
     */
    private String getTypeMimeDuFichierJoint(FichierJoint fichierJoint)
    {
        String result;

        if (fichierJoint == null
            || fichierJoint.getLeContenuDuFichier() == null
            || (fichierJoint.getLeContenuDuFichier().getData() == null && fichierJoint.getLeContenuDuFichier()
                .getFichierTemporaire() == null))
        {
            throw new ProgrammationException("Le fichier joint ou son contenu est null");
        }
        else
        {
            if (!StringUtils.isBlank(fichierJoint.getTypeMimeFichier()))
            {
                result = fichierJoint.getTypeMimeFichier();
            }
            else
            {
                result = TYPE_MIME_PAR_DEFAULT;
            }
        }

        return result;
    }

    /**
     * Teste si l'enregistrement du fichier doit être proposé à l'utilisateur.
     * 
     * @param model --
     * @return true, if checks if is demander enregistrement fichier
     */
    private boolean isDemanderEnregistrementFichier(Map model)
    {
        boolean result = true;
        if (model.containsKey(DEMANDE_ENREGISTREMENT_FICHIER))
        {
            Object demandeEnregistrement = model.get(DEMANDE_ENREGISTREMENT_FICHIER);
            if (demandeEnregistrement instanceof Boolean && ((Boolean) demandeEnregistrement).booleanValue() == false)
            {
                result = false;
            }
        }

        return result;
    }

    /**
     * Transfert au client HTTP un fichier à partir d'un tableau de bytes.
     * 
     * @param dataBytes tableau de bytes à transférer au client
     * @param response --
     * @param bufferSize taille du buffer utilisé pour effectuer le transfert
     */
    private void streamBinaryDataFromArray(byte[] dataBytes, HttpServletResponse response, int bufferSize)
    {
        ServletOutputStream ouputStream = null;

        try
        {
            response.setBufferSize(bufferSize);

            ouputStream = response.getOutputStream();
            ouputStream.write(dataBytes, 0, bufferSize);
            ouputStream.flush();
        }
        catch (IOException e)
        {
            log.warn("Erreur d'E/S lors du téléchargement", e);
        }
        finally
        {
            try
            {
                if (ouputStream != null)
                {
                    // ferme le stream
                    ouputStream.close();
                }
            }
            catch (IOException ex)
            {
                log.warn("Impossible de fermer le fichier", ex);
            }
        }

    }

    /**
     * Transfert un fichier au client HTTP à partir d'un fichier temporaire.
     * 
     * @param fichierTemporaire --
     * @param response --
     * @param bufferSize --
     */
    private void streamBinaryDataFromFile(File fichierTemporaire, HttpServletResponse response, int bufferSize)
    {
        BufferedInputStream bufferedInputStream = null;
        ServletOutputStream outputStream = null;
        BufferedOutputStream bufferedOutputStream = null;

        try (InputStream inputStream = new FileInputStream(fichierTemporaire))
        {
            response.setBufferSize(bufferSize);
            outputStream = response.getOutputStream();
            bufferedOutputStream = new BufferedOutputStream(outputStream, bufferSize);
            bufferedInputStream = new BufferedInputStream(inputStream, bufferSize);

            byte[] buff = new byte[bufferSize];
            int bytesRead;

            while (-1 != (bytesRead = bufferedInputStream.read(buff, 0, buff.length)))
            {
                bufferedOutputStream.write(buff, 0, bytesRead);
                bufferedOutputStream.flush();
                outputStream.flush();
            }

        }
        catch (IOException exception)
        {
            log.warn("ERREUR D'E/S", exception);
            throw ApplicationExceptionTransformateur.transformer(exception);
        }
        finally
        {
            if (bufferedInputStream != null)
            {
                try
                {
                    bufferedInputStream.close();
                }
                catch (IOException exception)
                {
                    log.warn("Erreur lors de la fermeture du flux", exception);
                }
            }

            if (bufferedOutputStream != null)
            {
                try
                {
                    bufferedOutputStream.close();
                }
                catch (IOException exception)
                {
                    log.warn("Erreur lors de la fermeture du flux", exception);
                }
            }
            if (outputStream != null)
            {
                try
                {
                    outputStream.flush();
                    outputStream.close();
                }
                catch (IOException exception)
                {
                    log.warn("Erreur lors de la fermeture du flux", exception);
                }
            }
        }
    }

}
