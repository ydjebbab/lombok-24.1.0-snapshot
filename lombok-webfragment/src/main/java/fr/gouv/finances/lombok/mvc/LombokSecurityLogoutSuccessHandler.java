package fr.gouv.finances.lombok.mvc;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;

/**
 * Cette classe gère la déconnexion
 * 
 * @author celfer Date: 7 oct. 2019
 */
public class LombokSecurityLogoutSuccessHandler implements LogoutSuccessHandler
{
    private static final Logger log = LoggerFactory.getLogger(LombokSecurityLogoutSuccessHandler.class);

    // L'URL d'Ulysse : http://www.intranet.dgfip
    @Value(value = "${authentification.ulysse}")
    private String ulysse;

    @Value(value = "${authentification.modeauthentification}")
    private String modeAuthentification;
    
    // Page de login
    private static final String IDENTIFICATION_PATH = "/identification.ex";

    private static final String PORTAIL = "portail";

    @Override
    public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication)
        throws IOException, ServletException
    {
        boolean modeportail = estEnModePortail();

        if (log.isDebugEnabled())
        {
            log.debug("Entrée onLogoutSuccess");
        }

        if (modeportail)
        {
            if (log.isDebugEnabled())
            {
                log.debug("Déconnexion du mode portail et redirection vers Ulysse");
            }
            String targetUrl = ulysse;
            response.sendRedirect(response.encodeRedirectURL(targetUrl));
        }
        else
        {
            if (log.isDebugEnabled())
            {
                log.debug("Déconnexion du mode appli ou appliportail et redirection vers la page de login");
            }
            String targetUrl = request.getContextPath() + IDENTIFICATION_PATH;
            response.sendRedirect(response.encodeRedirectURL(targetUrl));
        }
    }

    private boolean estEnModePortail()
    {
        return modeAuthentification.compareTo(PORTAIL) == 0;
    }

}
