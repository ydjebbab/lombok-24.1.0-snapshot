package fr.gouv.finances.lombok.config;

import org.apache.catalina.Container;
import org.apache.catalina.Context;
import org.apache.catalina.Wrapper;
import org.apache.tomcat.util.scan.StandardJarScanner;
import org.springframework.boot.context.embedded.ConfigurableEmbeddedServletContainer;
import org.springframework.boot.context.embedded.EmbeddedServletContainerCustomizer;
import org.springframework.boot.context.embedded.tomcat.TomcatContextCustomizer;
import org.springframework.boot.context.embedded.tomcat.TomcatEmbeddedServletContainerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Configuration du serveur Tomcat
 */
@Configuration
public class LombokTomcatConfiguration
{
    /**
     * methode Servlet container customizer : Customisation des init parameters du tomcat embedded (trimspaces et
     * development). dans cette version de spring boot les propriétés n'existent pas encore
     * (server.**.init-parameter.developement) Il faudra supprimer cette méthode et la remplacer par des paramètres
     * lorsque nous monterons la version de boot
     *
     * @return embedded servlet container customizer
     */
    @Bean
    public EmbeddedServletContainerCustomizer servletContainerCustomizer()
    {
        return new EmbeddedServletContainerCustomizer()
        {

            @Override
            public void customize(ConfigurableEmbeddedServletContainer container)
            {
                if (container instanceof TomcatEmbeddedServletContainerFactory)
                {
                    customizeTomcat((TomcatEmbeddedServletContainerFactory) container);
                }
            }

            private void customizeTomcat(TomcatEmbeddedServletContainerFactory tomcatFactory)
            {
                tomcatFactory.addContextCustomizers(new TomcatContextCustomizer()
                {

                    @Override
                    public void customize(Context context)
                    {
                        Container jsp = context.findChild("jsp");
                        if (jsp instanceof Wrapper)
                        {
                             //on applique les paramètres sur le tomcat embedded.
                             //avec la version de spring boot, on devrait pouvoir le faire
                             //directement dans le application.properties avec les clés server.jsp-servlet.init-parameters..
                            ((Wrapper) jsp).addInitParameter("development", "false");
                            ((Wrapper) jsp).addInitParameter("trimSpaces", "true");
                        }

                        // La version 1.3.8 de boot tire le tomcat embedded 8.0.37 qui ne permet pas
                        // de désactiver le scan des manifests : du coup au démarrage de tomcat il y
                        // a plein de warnings sur des jars dont le manifest est erronné.
                        // Nous ne pouvons pas désactiver le scan (tlds, fragments).
                        // La solution est d'utiliser tomcat 8.0.48 (surcharge dans dependencymanagement du pom parent),
                        // qui dispose de la méthode setScanManifest que l'on peut mettre à false. Voir ici:
                        // https://bz.apache.org/bugzilla/show_bug.cgi?id=59961
                        StandardJarScanner stdJarScanner = (StandardJarScanner) context.getJarScanner();
                        stdJarScanner.setScanManifest(false);
                    }
                });
            }
        };
    }
}
