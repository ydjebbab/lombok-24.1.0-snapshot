<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java" %>
<%@ include file="/WEB-INF/tags/html/includes.tagf" %>

<!--  Page piedpage.jsp -->

<app:initvar attrname="_imagesrepertoire" attrvalue="${imagesrepertoire}" bundlekey="app.page.imagesrepertoire" defaultvalue="/composants/tagsapp/images/"/>
              
<%-- Pied de page --%>
<app:initvar attrname="_piedboxclass" attrvalue="${piedboxclass}" bundlekey="app.piedpage.piedboxclass" defaultvalue="piedpagecontainer"/>
<app:initvar attrname="_textepiedclass" attrvalue="${classtextepiedpage}" bundlekey="app.piedpage.textepiedclass" defaultvalue="textepiedpage"/>

    <c:choose>
        <c:when test="${param.responsive eq 'false'}">
        	<footer class="${_piedboxclass}" role="contentinfo" id="footerContent" tabindex="0">
        	   <p class="${_textepiedclass}">${param.titrepiedpage}</p>
            </footer>
        </c:when>
        <c:otherwise>
            <footer role="contentinfo" id="footerContent" tabindex="0">
                <p class="${_textepiedclass}">${param.titrepiedpage}</p>
            </footer>
        </c:otherwise>
    </c:choose>



	