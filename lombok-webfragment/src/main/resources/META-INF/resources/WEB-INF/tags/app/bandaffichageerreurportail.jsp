<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java" %>
<%@ include file="/WEB-INF/tags/html/includes.tagf" %>

<!--  Page bandaffichageerreurportail.jsp -->

<app:initvar attrname="_imagesrepertoire" attrvalue="${imagesrepertoire}" bundlekey="app.page.imagesrepertoire" defaultvalue="/composants/tagsapp/images/"/>
              
<%-- Bandeau --%>
<app:initvar attrname="_bndboxclass" attrvalue="${bndboxclass}" bundlekey="app.bandaffichageerreurportail.bndboxclass" defaultvalue="bandeaucontainer"/>
<app:initvar attrname="_bndgaucheclass" attrvalue="${bndgaucheclass}" bundlekey="app.bandaffichageerreurportail.bndgaucheclass" defaultvalue="bandeaugauche"/>
<app:initvar attrname="_bndmilieuclass" attrvalue="${bndmilieuclass}" bundlekey="app.bandaffichageerreurportail.bndmilieuclass" defaultvalue="bandeaumilieu"/>
<app:initvar attrname="_bnddroitclass" attrvalue="${bnddroitclass}" bundlekey="app.bandaffichageerreurportail.bnddroitclass" defaultvalue="bandeaudroit"/>
<app:initvar attrname="_bndfermetureclass" attrvalue="${bndfermetureclass}" bundlekey="app.bandaffichageerreurportail.bndfermetureclass" defaultvalue="popferm"/>
<app:initvar attrname="_bndfermetureurl" attrvalue="${bndfermetureurl}" bundlekey="app.bandaffichageerreurportail.bndfermetureurl" defaultvalue="/messagebloquantdetecteavecportail.ex?deconnecter=true"/>
<app:initvar attrname="_bandfermetureimage" attrvalue="${bandfermetureimage}" bundlekey="app.bandaffichageerreurportail.bandfermetureimage" defaultvalue="imgError13x13.gif"/>
<app:initvar attrname="_textebndclass" attrvalue="${classtextebandeau}" bundlekey="app.bandaffichageerreurportail.textebndclass" defaultvalue="textebandeau"/>

	<div class="${_bndboxclass}">
		<div class="${_bndgaucheclass}">
			<p id="appli-version" class="${_textebndclass}"><fmt:message key="appli.version" /></p>
			<p id="appli-libellelong" class="${_textebndclass}"><fmt:message key="appli.libelle-long"/></p>				
		</div>
		<div class="${_bndmilieuclass}">
			<p class="${_textebndclass}">${param.titrebnd}</p>
		</div>		
		<div class="${_bnddroitclass}">
		<c:if test="${! empty utilisateurSecurityLombokContainer.utilisateurSecurityLombok.personneAnnuaire.cn}">
		    <p id="personneannuaire-cn" class="${_textebndclass}">Nom : ${utilisateurSecurityLombokContainer.utilisateurSecurityLombok.personneAnnuaire.cn}</p>
		</c:if>
	
		<c:if test="${! empty utilisateurSecurityLombokContainer.utilisateurSecurityLombok.personneAnnuaire.fonction}">
		    <p id="personneannuaire-fonction" class="${_textebndclass}">Fonction : ${utilisateurSecurityLombokContainer.utilisateurSecurityLombok.personneAnnuaire.fonction}</p>
		</c:if>
		<c:if test="${! empty utilisateurSecurityLombokContainer.utilisateurSecurityLombok.personneAnnuaire.dateExpirationDerniereHabilitation}">
		    <p id="expirationhabilitation" class="${_textebndclass}">Expiration habilitation : <fmt:formatDate pattern="dd/MM/yyyy" value="${utilisateurSecurityLombokContainer.utilisateurSecurityLombok.personneAnnuaire.dateExpirationDerniereHabilitation}"/> </p>
		</c:if>
        <c:if test="${! empty utilisateurSecurityLombokContainer.utilisateurSecurityLombok.personneAnnuaire.spi}">
            <p id="personneannuaire-spi" class="${_textebndclass}">SPI : ${utilisateurSecurityLombokContainer.utilisateurSecurityLombok.personneAnnuaire.spi} </p>
        </c:if>        
		</div>
		<div class="${_bndfermetureclass}">
			<a href="<c:url value="${_bndfermetureurl}"/>" title="Déconnexion"><img src="<c:url value="${_imagesrepertoire}/${_bandfermetureimage}"/>" alt="Se déconnecter" style="border: none" /></a>
		</div>
	</div>