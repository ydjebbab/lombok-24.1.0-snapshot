<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java" %>
<%@ include file="/WEB-INF/tags/html/includes.tagf" %>

<!--  Page includecssjs.jsp -->

<app:initvar attrname="_composantsrepertoire" attrvalue="${composantsrepertoire}" bundlekey="app.page.composantsrepertoire" defaultvalue="composants"/>
<app:initvar attrname="_csstagsrepertoire" attrvalue="${csstagsrepertoire}" bundlekey="app.page.csstagsrepertoire" defaultvalue="composants/tagsapp/css"/>
<app:initvar attrname="_csstagsrepertoire_rwd" attrvalue="${csstagsrepertoire_rwd}" bundlekey="app.page.rwd.csstagsrepertoire" defaultvalue="composants/tagsapp/css/rwd"/>
<app:initvar attrname="_listecsstags" attrvalue="${listecsstags}" bundlekey="app.page.listecsstags" defaultvalue="communcp.css,Vtheme.css,Htheme.css,VEncadretheme.css,HEncadretheme.css"/>
<app:initvar attrname="_listenewcsstags" attrvalue="${listenewcsstags}" bundlekey="app.page.listenewcsstags" defaultvalue="new_communcp.css,new/menu.css"/>
<app:initvar attrname="_listecsstags_rwd" attrvalue="${listecsstags_rwd}" bundlekey="app.page.rwd.listecsstags"  defaultvalue="bootstrap.min.css,communcp_rwd.css,Vtheme_rwd.css,Htheme_rwd.css,VEncadretheme_rwd.css,HEncadretheme_rwd.css,tablesaw.css,general_rwd.css, form_rwd.css, pagination_rwd.css, icon_rwd.css, new_communcp_rwd.css"/>
<app:initvar attrname="_jstagsrepertoire" attrvalue="${jstagsrepertoire}" bundlekey="app.page.jstagsrepertoire" defaultvalue="composants/tagsapp/js"/>
<app:initvar attrname="_bootstrapdatetimepicker" attrvalue="${bootstrapdatetimepicker}" bundlekey="app.page.rwd.bootstrapdatetimepicker" defaultvalue="false"/>
<app:initvar attrname="_tagjavascript" attrvalue="${tagjavascript}" bundlekey="app.page.tagjavascript" defaultvalue="iepngfix_tilebg.js,jquery.details.js,page.js,tagsapp.js,decoration.js,html5.js,bootstrap-tooltip.js"/> 
<app:initvar attrname="_tagjavascript_rwd" attrvalue="${tagjavascript_rwd}" bundlekey="app.page.rwd.tagjavascript" defaultvalue="iepngfix_tilebg.js,jquery.details.js,page.js,tagsapp.js,decoration.js,html5.js,rwd/bootstrap.min.js,bootstrap-tooltip.js,rwd/bootstrap-accessibility.min.js,rwd/mdetect.js,rwd/ResizeSensor.js,rwd/tablesaw.js,rwd/jquery.rcrumbs.min.js,rwd/bootstrap_add.js"/>
<app:initvar attrname="_tagjavascriptjquery" attrvalue="${tagjavascriptjquery}" bundlekey="app.page.tagjavascriptjquery" defaultvalue="jquery-1.7.1.min.js"/>
<app:initvar attrname="_tagjavascriptjquery_rwd" attrvalue="${tagjavascriptjquery_rwd}" bundlekey="app.page.rwd.tagjavascriptjquery" defaultvalue="jquery-1.11.3.min.js"/>
<app:initvar attrname="_imagesrepertoire" attrvalue="${imagesrepertoire}" bundlekey="app.page.imagesrepertoire" defaultvalue="/composants/tagsapp/images"/>
<app:initvar attrname="_menurepertoire" attrvalue="${menurepertoire}" bundlekey="app.page.menurepertoire" defaultvalue="/composants/jscookmenu"/>
<app:initvar attrname="_menutheme" attrvalue="${menutheme}" bundlekey="app.page.menutheme" defaultvalue="ThemeOffice"/>
<app:initvar attrname="_calendarrepertoire" attrvalue="${calendarrepertoire}" bundlekey="app.calendar.calendarrepertoire" defaultvalue="/jscalendar"/>
<app:initvar attrname="_calendarcss" attrvalue="${calendarcss}" bundlekey="app.calendar.calendarcss" defaultvalue="calendar-blue.css"/>
<app:initvar attrname="_extremecss" attrvalue="${extremecss}" bundlekey="app.extremecomponent.css" defaultvalue="/composants/extremecomponents/css/extremecomponents.css"/>
<app:initvar attrname="_extremejs" attrvalue="${extremejs}" bundlekey="app.extremecomponent.js" defaultvalue="/composants/extremecomponents/js/extremecomponents.js"/>
<app:initvar attrname="_jsapplicationrepertoire" attrvalue="${jsapplicationrepertoire}" bundlekey="app.page.jsapplicationrepertoire" defaultvalue="/application/js"/>
<app:initvar attrname="_cssapplicationrepertoire" attrvalue="${cssapplicationrepertoire}" bundlekey="app.page.cssapplicationrepertoire" defaultvalue="/application/css"/>
<app:initvar attrname="_imagesapplicationrepertoire" attrvalue="${imagesapplicationrepertoire}" bundlekey="app.page.imagesapplicationrepertoire" defaultvalue="/application/img"/>
<app:initvar attrname="_applicationjs" attrvalue="${applicationjs}" bundlekey="app.page.applicationjs" defaultvalue=""/>
<app:initvar attrname="_applicationcss" attrvalue="${applicationcss}" bundlekey="app.page.applicationcss" defaultvalue=""/>
<app:initvar attrname="_appnouvelcharte" attrvalue="${appnouvelcharte}" bundlekey="app.nouvel.charte" defaultvalue="false"/>

<c:choose>
    <c:when test="${(empty param.responsive) or (param.responsive eq 'false')}">

        <%-- Feuille de style associÃ©es aux tags app --%>
        <str:split separator="," var="_tabcss">${_listecsstags}</str:split>
        <c:forEach var="_cssfile"  items="${_tabcss}" > 
            <str:trim var="_cssfiletrim">${_cssfile}</str:trim>
            <style type="text/css">@import url("<c:url value="/${_csstagsrepertoire}/${_cssfiletrim}"/>");</style>
</c:forEach>

        <c:if test="${_appnouvelcharte ne 'false'}">
            <str:split separator="," var="_tabcss">${_listenewcsstags}</str:split>
            <c:forEach var="_cssfile"  items="${_tabcss}" > 
<str:trim var="_cssfiletrim">${_cssfile}</str:trim>
                <style type="text/css">@import url("<c:url value="/${_csstagsrepertoire}/${_cssfiletrim}"/>");</style>
</c:forEach>
        </c:if>

        <%-- Javascript jquery --%>
        <script type="text/javascript"  src="<c:url value="/${_jstagsrepertoire}/${_tagjavascriptjquery}"/>"></script>

        <%-- Javascript associÃ© aux tags app--%>
<str:split separator="," var="_tabjs">${_tagjavascript}</str:split>
<c:forEach var="_jsfile" items="${_tabjs}" > 
                 <script type="text/javascript" src="<c:url value="/${_jstagsrepertoire}/${_jsfile}"/>"></script>
</c:forEach>

     </c:when>
     <c:otherwise>
    
          <%-- Feuille de style associÃ©es aux tags app --%>
        <str:split separator="," var="_tabcss">${_listecsstags_rwd}</str:split>
<c:forEach var="_cssfile"  items="${_tabcss}" > 
 	<str:trim var="_cssfiletrim">${_cssfile}</str:trim>
            <link rel="stylesheet" type="text/css" href="<c:url value="/${_csstagsrepertoire_rwd}/${_cssfiletrim}"/>" />
</c:forEach>

        <%-- Javascript jquery --%>
        <script type="text/javascript"  src="<c:url value="/${_jstagsrepertoire}/${_tagjavascriptjquery_rwd}"/>"></script>   

         <%-- Javascript associÃ© aux tags app responsive --%>
        <str:split separator="," var="_tabjs">${_tagjavascript_rwd}</str:split>
        <c:forEach var="_jsfile" items="${_tabjs}" > 
            <script type="text/javascript" src="<c:url value="/${_jstagsrepertoire}/${_jsfile}"/>"></script>
    </c:forEach>
    
    </c:otherwise>
</c:choose>   

<%-- Feuille de style et javascript JsCookmenu --%>
<c:if test="${param.menu ne 'false'}">
<script type="text/javascript"  src="<c:url value="${_menurepertoire}/JSCookMenu.js"/>"></script>
<link rel="stylesheet" type="text/css" href="<c:url value="${_menurepertoire}/${_menutheme}/theme.css"/>" />
<script type="text/javascript"  src="<c:url value="${_menurepertoire}/${_menutheme}/theme.js"/>"></script>
</c:if>

<%-- Feuille de style eXtremeComponents --%>
<link rel="stylesheet" type="text/css" href="<c:url value="${_extremecss}"/>" />

<%-- Javascript eXtremeComponents --%>
<script type="text/javascript"  src="<c:url value="${_extremejs}"/>"></script>

<%-- Feuille de style et javascript jscalendar --%>
<link rel="stylesheet" type="text/css" href="<c:url value="${_calendarrepertoire}/css/${_calendarcss}"/>" />
<script type="text/javascript" src="<c:url value="${_calendarrepertoire}/js/calendar.js"/>"></script>
<script type="text/javascript" src="<c:url value="${_calendarrepertoire}/js/calendar-fr.js"/>"></script>
<script type="text/javascript" src="<c:url value="${_calendarrepertoire}/js/calendar-setup.js"/>"></script>

<%--bootstrap datetimepicker , on laisse l'ancien calendar sans condition (pour assurer la compatibilitÃ©) --%>
<%--sinon les pages sans responsive ne fonctionneront plus , meme si ca fait doublon--%>
<c:if test="${(!empty param.responsive) and (param.responsive eq 'true') and (_bootstrapdatetimepicker eq 'true')}">
<link rel="stylesheet" type="text/css" href="<c:url value="/${_csstagsrepertoire_rwd}/bootstrap-datetimepicker.min.css" />" />
<script type="text/javascript" src="<c:url value="/${_jstagsrepertoire}/rwd/moment-with-locales.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/${_jstagsrepertoire}/rwd/bootstrap-datetimepicker.min.js"/>"></script>
</c:if>  


<%--Javascript utilisée par le tag app:disperror --%>
<script type="text/javascript" src="<c:url value="/${_composantsrepertoire}/dhtmlpopup/js/popup.js"/>"></script>

<%--Javascript utilisée par le tag app:disppdf --%>
<script type="text/javascript" src="<c:url value="/${_composantsrepertoire}/dhtmlpdfpopup/js/objectpopuppdf.js"/>"></script>


 <%-- Feuille de style de l'application --%>
        <str:split separator="," var="_applicss">${_applicationcss}</str:split>
        <c:forEach var="_cssfile"  items="${_applicss}" > 
            <str:trim var="_cssfiletrim">${_cssfile}</str:trim>
            <c:if test="${! empty _cssfiletrim}">
            <link rel="stylesheet" type="text/css" href="<c:url value="/${_cssapplicationrepertoire}/${_cssfiletrim}"/>" />
            </c:if>
        </c:forEach>
        
    <%-- Javascript Ã  l'application--%>
    <str:split separator="," var="_applijs">${_applicationjs}</str:split>
    <c:forEach var="_jsfile" items="${_applijs}" > 
        <str:trim var="_jsfiletrim">${_jsfile}</str:trim>
        <c:if test="${! empty _jsfiletrim}">
        <script type="text/javascript" src="<c:url value="/${_jsapplicationrepertoire}/${_jsfiletrim}"/>"></script>
        </c:if>
    </c:forEach>


