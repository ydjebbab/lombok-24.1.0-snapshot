<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java" %>
<%@ include file="/WEB-INF/tags/html/includes.tagf" %>

<!--  Page bandeau.jsp -->

<app:initvar attrname="_imagesrepertoire" attrvalue="${imagesrepertoire}" bundlekey="app.page.imagesrepertoire" defaultvalue="/composants/tagsapp/images/"/>
              
<%-- Bandeau --%>
<app:initvar attrname="_bndboxid" attrvalue="${bndboxid}" bundlekey="app.bandeau.bndboxid" defaultvalue="bandeau" />
<app:initvar attrname="_bndboxclass" attrvalue="${bndboxclass}" bundlekey="app.bandeau.bndboxclass" defaultvalue="bandeaucontainer"/>
<app:initvar attrname="_bndgaucheclass" attrvalue="${bndgaucheclass}" bundlekey="app.bandeau.bndgaucheclass" defaultvalue="bandeaugauche"/>
<app:initvar attrname="_bndmilieuclass" attrvalue="${bndmilieuclass}" bundlekey="app.bandeau.bndmilieuclass" defaultvalue="bandeaumilieu"/>
<app:initvar attrname="_bnddroitclass" attrvalue="${bnddroitclass}" bundlekey="app.bandeau.bnddroitclass" defaultvalue="bandeaudroit"/>
<app:initvar attrname="_bndfermetureclass" attrvalue="${bndfermetureclass}" bundlekey="app.bandeau.bndfermetureclass" defaultvalue="popferm"/>
<app:initvar attrname="_bndfermetureurl" attrvalue="${bndfermetureurl}" bundlekey="app.bandeau.bndfermetureurl" defaultvalue="/j_spring_security_logout"/>


<app:initvar attrname="_textebndclass" attrvalue="${classtextebandeau}" bundlekey="app.bandeau.textebndclass" defaultvalue="textebandeau"/>
<app:initvar attrname="_appnouvelcharte" attrvalue="${appnouvelcharte}" bundlekey="app.nouvel.charte" defaultvalue="false"/>
<c:if test="${_appnouvelcharte ne 'true' }">
<app:initvar attrname="_bandfermetureimage" attrvalue="${bandfermetureimage}" bundlekey="app.bandeau.bandfermetureimage" defaultvalue="imgError13x13.gif"/>
</c:if>
<c:if test="${_appnouvelcharte eq 'true' }">
<app:initvar attrname="_bandfermetureimage" attrvalue="${bandfermetureimage}" bundlekey="app.bandeau.bandfermetureimage" defaultvalue="door_in.png"/>
</c:if>

    <c:choose>
        <c:when test="${param.responsive eq 'false'}">
            <header id="${_bndboxid}" class="${_bndboxclass}" role="banner">
        </c:when>
        <c:otherwise>
           <header id="${_bndboxid}" class="${_bndboxclass} row height_equalizer" role="banner">
        </c:otherwise>
    </c:choose>

        <c:if test="${param.responsive eq 'false'}">
    	   <div>
        </c:if>

            <c:choose>
                <c:when test="${param.responsive eq 'false'}">
        			<div class="${_bndgaucheclass}">
                </c:when>
                <c:otherwise>
                    <div class="${_bndgaucheclass} height_eq_item col-md-4 col-sm-4 col-xs-12">
                </c:otherwise>
            </c:choose>
            
                <div id="menu_raccourcis" class="raccourcis menuhidden" >
                    <span>Accès rapides :</span>
                    <div class="linkContainer">
                        <a id="navigationMenu_Link" href="#navigationMenu" tabindex="0">Menu</a>
                        <a id="mainContent_Link" href="#mainContent" tabindex="0">Contenu</a>
                        <a id="footerContent_Link" href="#footerContent" tabindex="0">Infos</a>
                    </div>
                    
                </div>

                <p id="bappli-version" class="${_textebndclass}">
                    <fmt:message key="appli.version" />
                </p>
                <p id="bappli-libellelong" class="${_textebndclass}">
                    <fmt:message key="appli.libelle-long" />
                </p>
                
			</div>

            <c:choose>
                <c:when test="${param.responsive eq 'false'}">
            			<div class="${_bndmilieuclass}">
            				<p class="${_textebndclass}">${param.titrebandeau}</p>
            	            <p class="${_textebndclass}">${param.titrebandeau2}&nbsp;</p>
            			</div>		
            			<div class="${_bnddroitclass}">
                </c:when>
                <c:otherwise>
                    <div class="${_bndmilieuclass}  height_eq_item col-md-5 col-sm-5 col-xs-12">
                </c:otherwise>
            </c:choose>
            
            
    			<c:if test="${! empty utilisateurSecurityLombokContainer.utilisateurSecurityLombok.personneAnnuaire.cn}">
    			    <p id="bpersonneannuaire-cn" class="${_textebndclass}">Nom : ${utilisateurSecurityLombokContainer.utilisateurSecurityLombok.personneAnnuaire.cn}</p>
    			</c:if>
		
    			<c:if test="${! empty utilisateurSecurityLombokContainer.utilisateurSecurityLombok.personneAnnuaire.fonction}">
    			    <p id="bpersonneannuaire-fonction" class="${_textebndclass}">Fonction : ${utilisateurSecurityLombokContainer.utilisateurSecurityLombok.personneAnnuaire.fonction}</p>
    			</c:if>
    			<c:if test="${! empty utilisateurSecurityLombokContainer.utilisateurSecurityLombok.personneAnnuaire.dateExpirationDerniereHabilitation}">
    			    <p id="bexpirationhabilitation" class="${_textebndclass}">Expiration habilitation : <fmt:formatDate pattern="dd/MM/yyyy" value="${utilisateurSecurityLombokContainer.utilisateurSecurityLombok.personneAnnuaire.dateExpirationDerniereHabilitation}"/> </p>
    			</c:if>
                <c:if test="${! empty utilisateurSecurityLombokContainer.utilisateurSecurityLombok.personneAnnuaire.spi}">
                    <p id="personneannuaire-spi" class="${_textebndclass}">SPI : ${utilisateurSecurityLombokContainer.utilisateurSecurityLombok.personneAnnuaire.spi}</p>
                </c:if>                       
			</div>
            <c:choose>
                <c:when test="${param.responsive eq 'false'}">
                    <div class="${_bndfermetureclass}" id="header-action">
                </c:when>
                <c:otherwise>
                    <div class="${_bndfermetureclass} height_eq_item col-md-3 col-sm-3 col-xs-12" id="header-action">
                </c:otherwise>
            </c:choose>

			<c:if test="${_appnouvelcharte eq 'true' }">
                <c:if test="${param.responsive eq 'false'}">

                    <a href="#decfont" id="decfont" class="btn btn-mini header-action" title="Réduire la police de caractère" aria-label="Réduire la police de caractère">-</a>
                    <a href="#resetfont" id="resetfont" class="btn btn-mini header-action" title="Revenir à la taille de police de caractère originale" aria-label="Revenir à la taille de police de caractère originale">A<span>A</span></a>        
                    <a href="#incfont" id="incfont" class="btn btn-mini header-action" title="Agrandir la police de caractère" aria-label="Agrandir la police de caractère">+</a>
                </c:if>
			    <c:if test="${param.responsive eq 'true'}">
                    <ul>
                        <li>
                </c:if>
			
				    <a href="#print" class="btn btn-mini header-action" onclick="javascript:window.print();return false;" id="print"
                        title="Imprimer cette page (ouverture d'une nouvelle fenêtre)"> <img src="<c:url value="${_imagesrepertoire}/icons8/printer.png"/>"
                        alt="Imprimer cette page (ouverture d'une nouvelle fenêtre)" /></a>

                    <c:if test="${param.responsive eq 'true'}">
                        </li>
                    </c:if>
				
				<c:if test="${! empty utilisateurSecurityLombokContainer.utilisateurSecurityLombok}">
                    <c:if test="${param.responsive eq 'true'}">
                        <li>
                    </c:if>
                        <a href="<c:url value="${_bndfermetureurl}"/>" class="btn btn-mini header-action" title="Déconnexion" id="logout"> <img src="<c:url value="${_imagesrepertoire}/icons8/logout.png"/>" alt="Se déconnecter" style="border: none" /></a>
                    <c:if test="${param.responsive eq 'true'}">
                        </li>
                    </c:if>
				</c:if>	
			</c:if>
        			<c:if test="${_appnouvelcharte ne 'true' }">
                        <c:if test="${param.responsive eq 'true'}">
                            <li>
                        </c:if>
                            <a href="<c:url value="${_bndfermetureurl}"/>" title="Déconnexion"><img src="<c:url value="${_imagesrepertoire}/${_bandfermetureimage}"/>" alt="Se déconnecter" style="border: none" /></a>
                        <c:if test="${param.responsive eq 'true'}">
                            </li>
                    	</c:if>
                    </c:if>
                <c:if test="${param.responsive eq 'true'}">
                    </ul>
                </c:if>
            </div>  
            <c:if test="${param.responsive eq 'false'}">
    			</div>
            </c:if>
        <c:if test="${param.responsive eq 'true'}">
            <div class="bandeaubas">
                <p class="textebandeau"></p>
        		</div>
        </c:if>
	</header>