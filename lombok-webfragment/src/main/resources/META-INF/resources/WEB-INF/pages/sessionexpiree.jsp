<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/tags/html/includes.tagf"%>

<!--  Page sessionexpiree.jsp -->

<fmt:message key="appli.libelle-long" var="libellelong" />
<fmt:message key="appli.libelle-court" var="libellecourt" />

<c:set var="responsive" value="true"/>

<app:page   titreecran="${libellecourt}" 
            titrecontainer="${libellelong}" 
            menu="false"
            responsive="${responsive}">

	<app:form   method="post" 
                formboxclass="donnees identification" 
                responsive="${responsive}">
        <div class="boxlblText">
            <label class="labelText">
             Votre session a expiré.<br/> 
             Veuillez cliquer sur le lien suivant pour vous reconnecter :
             <app:link label="Lien de reconnexion" uri="pagelogin.ex" aclass="btn" />
            </label>
        </div>
        
    </app:form>

</app:page>
