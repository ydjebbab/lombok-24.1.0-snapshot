<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/tags/html/includes.tagf"%>

<!--  Page choisirstatistiqueedition.jsp -->

<c:set var="responsive" value="true" />

<app:page titreecran="Statistiques relatives aux éditions" menu="true" responsive="${responsive}">

    <app:chemins action="composantedition/admin/flux.ex" responsive="${responsive}">
        <app:chemin action="accueil.ex" title="Retour à l'accueil" responsive="${responsive}">Accueil</app:chemin>
        <app:chemin title="Éditions" responsive="${responsive}">Éditions</app:chemin>
        <app:chemin title="Administration" responsive="${responsive}">Administration</app:chemin>
        <app:chemin title="Statistiques relatives aux éditions" responsive="${responsive}">Statistiques relatives aux éditions</app:chemin>
    </app:chemins>

    <app:form method="post" action="composantedition/admin/flux.ex" formid="statistiqueseseditions"
        formobjectname="statistiqueseditionsform" onsubmit="return submit_form(500)" formboxid="donnees" formboxclass="donnees sousonglets">

        <table border="1">
            <thead>
                <tr>
                    <td><b>&nbsp;</b></td>
                    <td><b>&nbsp;</b></td>
                </tr>
            </thead>
            <tr>
                <td>Comptage du nombre d'édition par statut</td>
                <td><app:button action="composantedition/admin/flux.ex" transition="statistiqueseditionsparstatus">Consulter</app:button></td>
            </tr>
            <tr>
                <td>Comptage du nombre d'édition par date de demande</td>
                <td><app:button action="composantedition/admin/flux.ex" transition="statistiquestailleeditionsparjour">Consulter</app:button></td>
            </tr>
            <tr>
                <td>Comptage du nombre d'édition par date de purge</td>
                <td><app:button action="composantedition/admin/flux.ex" transition="statistiqueseditionspardatepurge">Consulter</app:button></td>
            </tr>
            <tr>
                <td>Comptage du nombre d'édition par type et statut</td>
                <td><app:button action="composantedition/admin/flux.ex" transition="statistiqueseditionspartypeetstatus">Consulter</app:button></td>
            </tr>
            <tr>
                <td>Taille des éditions par type</td>
                <td><app:button action="composantedition/admin/flux.ex" transition="statistiquestailleeditionspartype">Consulter</app:button></td>
            </tr>
        </table>
        <br />
        <app:boxboutons>
            <app:submit label="Retourner à l'accueil" title="Retourner à l'accueil" transition="annuler" responsive="${responsive}" />
        </app:boxboutons>

    </app:form>
</app:page>