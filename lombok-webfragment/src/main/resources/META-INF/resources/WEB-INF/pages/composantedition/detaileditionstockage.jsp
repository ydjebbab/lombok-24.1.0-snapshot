<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/tags/html/includes.tagf"%>

<!--  Page detaileditionstockage.jsp -->

<c:set var="responsive" value="true" />

<app:page titreecran="Détail d'une édition" responsive="${responsive}">

    <app:chemins action="composantedition/admin/flux.ex" responsive="${responsive}">
        <app:chemin transition="fin" title="Retour à l'accueil" responsive="${responsive}">Accueil</app:chemin>
        <app:chemin title="Éditions" responsive="${responsive}">Éditions</app:chemin>
        <app:chemin title="Administration" responsive="${responsive}">Administration</app:chemin>
        <app:chemin title="Administration des éditions" transition="retourecranrecherche" responsive="${responsive}">Administration des éditions</app:chemin>
        <app:chemin title="Liste des éditions sélectionnées" transition="retourlisteeditions" responsive="${responsive}">Liste des éditions sélectionnées</app:chemin>
        <app:chemin title="Détail d'une édition" responsive="${responsive}">Détail d'une édition</app:chemin>
    </app:chemins>

    <app:onglets cleactive="6" responsive="${responsive}" boxongletsclass="row height_equalizer ongletcontainer">
        <app:onglet cle="1" libelle="Identification" transition="detaileditionidentification" responsive="${responsive}"
            ongletclass="btmodel_2" />
        <app:onglet cle="2" libelle="Destination" transition="detaileditiondestination" responsive="${responsive}" ongletclass="btmodel_2" />
        <app:onglet cle="3" libelle="Ordonnancement" transition="detaileditionordonnancement" responsive="${responsive}"
            ongletclass="btmodel_2" />
        <app:onglet cle="4" libelle="Présentation" transition="detaileditionpresentation" responsive="${responsive}" ongletclass="btmodel_2" />
        <app:onglet cle="5" libelle="Mail" transition="detaileditionmail" responsive="${responsive}" ongletclass="btmodel_2" />
        <app:onglet cle="6" libelle="Stockage" responsive="${responsive}" ongletclass="btmodel_2" />
    </app:onglets>

    <app:form method="post" action="composantedition/admin/flux.ex" formid="administrationedition"
        formobjectname="administrationeditionsform" onsubmit="return submit_form(500)" formboxid="donnees"
        formboxclass="donnees sousonglets" responsive="${responsive}">

        <fieldset>
            <legend>Stockage</legend>
            <app:input attribut="jobHistory.stockageEdition.tailleFichier" libelle="Taille de l'édition (octets) :" maxlength="30" size="30"
                requis="false" readonly="true" responsive="${responsive}" />

            <app:input attribut="jobHistory.stockageEdition.stockageEnBase" libelle="Edition stockée en base de donnée :" maxlength="30"
                size="30" requis="false" readonly="true" responsive="${responsive}" />

            <app:input attribut="jobHistory.stockageEdition.appliEditionRoot" libelle="Racine de stockage  :" maxlength="70" size="70"
                requis="false" readonly="true" responsive="${responsive}" />

            <app:input attribut="jobHistory.stockageEdition.cheminComplet" libelle="Répertoire de stockage :" maxlength="70" size="70"
                requis="false" readonly="true" responsive="${responsive}" />

            <app:input attribut="jobHistory.stockageEdition.indexRepertoire" libelle="Index :" maxlength="30" size="30" requis="false"
                readonly="true" responsive="${responsive}" />

            <app:input attribut="jobHistory.stockageEdition.nomFicStockage" libelle="Nom du fichier de stockage :" maxlength="70" size="70"
                requis="false" readonly="true" responsive="${responsive}" />
        </fieldset>

        <app:boxboutons>
            <c:choose>
                <c:when test="${responsive != 'true'}">
                    <app:submit label="Retour à la liste" transition="retourlisteeditions" />
                </c:when>
                <c:otherwise>
                    <app:submit label="Retour à la liste" transition="retourlisteeditions" responsive="${responsive}"
                         />
                </c:otherwise>
            </c:choose>
        </app:boxboutons>

    </app:form>
    <app:disperror var="${erreurentreflux}" />
</app:page>