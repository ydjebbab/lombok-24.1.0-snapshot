<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java" %>
<%@ include file="/WEB-INF/tags/html/includes.tagf" %>

<!--  Page erreurflowid.jsp -->

<app:page titreecran="Erreur" 
		  titrecontainer="Erreur de navigation" 
		  menu="true" >


<app:disperrorfix  divid="popmessage">
            <p>Une erreur  s'est produite.</p>
            <p>Merci de ne pas mettre de pages de l'application en cache dans le navigateur</p>
            <p>Merci de vous reconnecter à l'application</p>
</app:disperrorfix>

<%@ include file="/WEB-INF/pages/errors/rapporterreur.jspf" %>		  

</app:page>
