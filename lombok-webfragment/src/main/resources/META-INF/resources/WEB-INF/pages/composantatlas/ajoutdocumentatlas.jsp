<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/tags/html/includes.tagf"%>

<!--  Page ajoutdocumentatlas.jsp -->

<c:set var="responsive" value="true" />

<app:page titreecran="Ajouter un document" titrecontainer="Ajouter un document" menu="true" responsive="${responsive}">

    <app:chemins action="composantatlas/flux.ex" responsive="${responsive}">
        <app:chemin action="accueil.ex" title="Retour à l'accueil" responsive="${responsive}">Accueil</app:chemin>
        <app:chemin title="Atlas" responsive="${responsive}">Atlas</app:chemin>
        <app:chemin title="Ajouter d'un document" responsive="${responsive}">Ajouter d'un document</app:chemin>
    </app:chemins>

    <app:form   action="composantatlas/flux.ex?${_csrf.parameterName}=${_csrf.token}" enctype="multipart/form-data" 
                formobjectname="ajoutdocumentatlasform" responsive="${responsive}">

        <fieldset>
            <legend>Ajout d'un document</legend>
            <app:input attribut="documentAtlas.identifiantAtlas" libelle="Identification du document" labelboxwidth="35%" maxlength="50"
                size="50" readonly="true" responsive="${responsive}" />
            <app:input attribut="documentAtlas.nom" libelle="Nom du document" labelboxwidth="35%" maxlength="50" size="50"
                responsive="${responsive}" />
            <%-- 
            <app:date attribut="documentAtlas.dateDePeremption" libelle="Date de péremption" labelboxwidth="35%" responsive="false" />
                --%>
            <div>
                <app:input size="10" maxlength="10" attribut="documentAtlas.dateDePeremption" libelle="Date de péremption" readonly="false"
                    placeholder="JJ/MM/AAAA" arialabel="Format de la date de péremption : JJ barre oblique MM barre oblique AAAA" inputboxwidth="10%" consigne="Veuillez saisir une date de la forme jj/mm/aaaa"
                    responsive="${responsive}">
                </app:input>
                <app:calendar />
            </div>
            <c:if test="${! empty  ajoutdocumentatlasform.fichierJoint}">
                <fieldset class="transferedFileFieldset" tabindex="0">
                    <legend>Fichier transféré</legend>
                    <app:textformate libelle="Fichier" attribut="fichierJoint.nomFichierOriginal" maxlength="50" size="50" />
                    <app:textformate libelle="Taille" attribut="fichierJoint.tailleFichier" maxlength="50" size="50" />
                    <app:textformate libelle="Transféré le " attribut="fichierJoint.dateHeureSoumission" maxlength="50" size="50" />
                    <app:submit label="Supprimer fichier" transition="delete" id="deleteTransferedFile" />
                </fieldset>
            </c:if>
            <c:if test="${ empty  ajoutdocumentatlasform.fichierJoint}">
                
                <app:file attribut="fichierJoint" libelle="Fichier" maxlength="50" size="50" labelboxwidth="7%" inputboxwidth="25%"
                    compboxwidth="15%" requis="false" readonly="true" labelboxclass="boxlblText transferedFileInput">
                    <span class="uploadMaxSizeSpan" tabindex="0">
                        La taille du fichier doit être inférieure à <fmt:formatNumber value="${uploadmaxsize/1000000}" /> Mo
                    </span>
                </app:file>
                <app:submit label="Transferer" transition="transfert" />
            </c:if>
            
            
        </fieldset>

        <c:choose>
            <c:when test="${responsive != 'true'}">
                <app:boxboutons>
                    <app:submit label="Retourner à l'accueil" transition="annuler" title="Retourner à l'accueil" />
                    <app:submit label="Suivant" transition="ajouterdocumentatlas" id="enterButton" title="Créer le document." />
                </app:boxboutons>
            </c:when>
            <c:otherwise>
                <app:boxboutons>
                    <app:submit label="Retourner à l'accueil" transition="annuler" title="Retourner à l'accueil" responsive="${responsive}" />
                    <app:submit label="Suivant" transition="ajouterdocumentatlas" id="enterButton" responsive="${responsive}"
                        inputclass="primary btn btn-default" title="Créer le document." />
                </app:boxboutons>
            </c:otherwise>
        </c:choose>

    </app:form>

    <app:disperror var="${erreurentreflux}" />
</app:page>