<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java" %>
<%@ include file="/WEB-INF/tags/html/includes.tagf" %>
<!--  Page erreurargumentextraction.jsp -->

<app:page titreecran="Erreur" 
		  titrecontainer="Connexion expirée" 
		  menu="true" >

<app:disperrorfix  divid="popmessage">
			<p>Votre connexion a expiré</p>
			<p><app:link action="accueil.ex" label="Reconnectez-vous à l'application"/> </p>
</app:disperrorfix>

<%@ include file="/WEB-INF/pages/errors/rapporterreur.jspf" %>

</app:page>
