<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/tags/html/includes.tagf"%>

<!--  Page detaileditiondestination.jsp -->

<c:set var="responsive" value="true" />

<app:page titreecran="Détail d'une édition" responsive="${responsive}">

    <app:chemins action="composantedition/admin/flux.ex" responsive="${responsive}">
        <app:chemin transition="fin" title="Retour à l'accueil" responsive="${responsive}">Accueil</app:chemin>
        <app:chemin title="Éditions" responsive="${responsive}">Éditions</app:chemin>
        <app:chemin title="Administration" responsive="${responsive}">Administration</app:chemin>
        <app:chemin title="Administration des éditions" transition="retourecranrecherche" responsive="${responsive}">Administration des éditions</app:chemin>
        <app:chemin title="Liste des éditions sélectionnées" transition="retourlisteeditions" responsive="${responsive}">Liste des éditions sélectionnées</app:chemin>
        <app:chemin title="Détail d'une édition" responsive="${responsive}">Détail d'une édition</app:chemin>
    </app:chemins>

    <app:onglets cleactive="2" responsive="${responsive}" boxongletsclass="row height_equalizer ongletcontainer">
        <app:onglet cle="1" libelle="Identification" transition="detaileditionidentification" responsive="${responsive}"
            ongletclass="btmodel_2" />
        <app:onglet cle="2" libelle="Destination" responsive="${responsive}" ongletclass="btmodel_2" />
        <app:onglet cle="3" libelle="Ordonnancement" transition="detaileditionordonnancement" responsive="${responsive}"
            ongletclass="btmodel_2" />
        <app:onglet cle="4" libelle="Présentation" transition="detaileditionpresentation" responsive="${responsive}" ongletclass="btmodel_2" />
        <app:onglet cle="5" libelle="Mail" transition="detaileditionmail" responsive="${responsive}" ongletclass="btmodel_2" />
        <app:onglet cle="6" libelle="Stockage" transition="detaileditionstockage" responsive="${responsive}" ongletclass="btmodel_2" />
    </app:onglets>

    <app:form method="post" action="composantedition/admin/flux.ex" formid="rechercheedition" formobjectname="administrationeditionsform"
        onsubmit="return submit_form(500)" formboxid="donnees" formboxclass="donnees sousonglets" responsive="${responsive}">

        <fieldset>
            <legend>Destination</legend>
            <app:input attribut="jobHistory.destinationEdition.applicationOrigine" libelle="Application productrice :" maxlength="30"
                size="30" requis="false" readonly="true" responsive="${responsive}" />

            <app:input attribut="jobHistory.destinationEdition.monoDestinataire" libelle="Edition destinée au seul utilisateur producteur :"
                maxlength="30" size="30" requis="false" readonly="true" responsive="${responsive}" />

            <app:input attribut="jobHistory.destinationEdition.nouvelleEdition" libelle="Nouvelle édition  :" maxlength="30" size="30"
                requis="false" readonly="true" responsive="${responsive}" />

            <app:input attribut="jobHistory.destinationEdition.uidProprietaire"
                libelle="Utilisateur qui a demandé la production de l'édition :" maxlength="70" size="70" requis="false" readonly="true"
                responsive="${responsive}" />

        </fieldset>

        <c:if test="${! empty administrationeditionsform.jobHistory.destinationEdition.profilsDesti}">
            <c:set var="i" value="0" />
            <c:forEach var="unProfilDesti" items="${administrationeditionsform.jobHistory.destinationEdition.profilsDesti}">
                <fieldset>
                    <legend>Profil autorisé</legend>
                    <app:input attribut="jobHistory.destinationEdition.profilsDesti[${i}].nomProfil" libelle="Nom du profil :"
                        maxlength="70" size="70" requis="false" readonly="true" />

                    <c:set var="j" value="0" />
                    <c:forEach var="unFiltre" items="${unProfilDesti.listeFiltres}">
                        <app:input attribut="jobHistory.destinationEdition.profilsDesti[${i}].listeFiltres[${j}].nomEtValeurs"
                            libelle="Filtre associé :" maxlength="70" size="70" requis="false" readonly="true" />
                        <c:set var="j" value="${j + 1}" />
                    </c:forEach>
                    <c:set var="i" value="${i + 1}" />
                </fieldset>
            </c:forEach>
        </c:if>

        <app:boxboutons>
            <c:choose>
                <c:when test="${responsive != 'true'}">
                    <app:submit label="Retour à la liste" transition="retourlisteeditions" />
                </c:when>
                <c:otherwise>
                    <app:submit label="Retour à la liste" transition="retourlisteeditions" responsive="${responsive}"
                         />
                </c:otherwise>
            </c:choose>
        </app:boxboutons>

    </app:form>
    <app:disperror var="${erreurentreflux}" />
</app:page>