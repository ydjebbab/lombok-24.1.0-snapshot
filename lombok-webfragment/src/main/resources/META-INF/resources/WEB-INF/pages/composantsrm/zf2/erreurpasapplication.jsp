<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java" %>
<%@ include file="/WEB-INF/tags/html/includes.tagf" %>

<!--  Page erreurpasapplication.jsp -->

<app:page titreecran="Gestion des messages" 
		  titrecontainer="Resultats >> Messages >> Création >> Incidents" 
		  menu="true" >
		  
<app:disperrorfix  path="creationmessageform.*" divid="msg" style="position:absolute;left:10% ;top: 10%; width: 80%;height: 20%"/>

<%@ include file="/WEB-INF/pages/errors/rapporterreur.jspf" %>

</app:page>
