<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java" %>
<%@ include file="/WEB-INF/tags/html/includes.tagf" %>

<!--  Page recapitulatifmessageavecprofils.jsp -->

<app:page titreecran="désactivation de profils bienvenue" 
		  titrecontainer=""
		  menu="true" 
		  titrebnd="Récapitulatif du message">

   <app:chemins action="zf2/flux.ex">
    <app:chemin action="accueil.ex" title="Retour à l'accueil">Accueil</app:chemin>
       <app:chemin>Récapitulatif du message créé</app:chemin>
</app:chemins>
	
 <app:form 
	method="post" 	
	action="zf2/flux.ex" 
	formid="creationmessage" 
	formobjectname="creationmessageform" 
	onsubmit="return submit_form(500)"
	formboxid="donnees"
	formboxclass="donnees sousonglets">

 	<c:set var="readonly" value="true"/>
 	
	<div>
	
	<fieldset><legend>Caractéristiques du message</legend>

 	<c:choose> 
        <c:when test="${fn:length(creationmessageform.message.libelleMessage) < 150}">
           <app:textarea attribut="message.libelleMessage" libelle="Libelle :" cols="75" rows="2"  tabindex="2"   readonly="${readonly}"/>
    
        </c:when>
        <c:otherwise>
           <app:textarea attribut="message.libelleMessage" libelle="Libelle :" cols="75" rows="30"  tabindex="2"   readonly="${readonly}"/>
    
        </c:otherwise>
    </c:choose>  </fieldset>
	<fieldset><legend>Période d'affichage du message</legend>
	<app:input attribut="message.dateDebutMessage" format="dd/MM/yyyy HH:mm" libelle="Date de début d'affichage du message :" maxlength="19" size="19" readonly="${readonly}"
	          >		 
	</app:input>
	<app:input attribut="message.dateFinMessage" format="dd/MM/yyyy HH:mm" libelle="Date de fin d'affichage du message :" maxlength="19" size="19" readonly="${readonly}"
	      >
	 </app:input>
	</fieldset>
	<fieldset><legend>Désactivation de profil(s)</legend>
	<ec:table 
  items="listProfilsSel" var="cont"
  autoIncludeParameters="false"
  showPagination="false"
  showStatusBar="false"
  filterable="false"
  sortable="false"
  locale="fr_FR"
  width="50%"
  form="creationmessage"	
  theme="eXtremeTable"	
  view="cphtml"
  imagePath="${pageContext.request.contextPath}/composants/extremecomponents/images/table_fr_FR/*.gif">		
  <ec:row>
   
  <ec:column property="libelleProfil" title="libellé du profil"/>
  <ec:column property="codeProfil" title="code profil"/>
   
  </ec:row>
</ec:table>	
</fieldset>
	<fieldset><legend>Période de désactivation du profil</legend>
	 <app:input attribut="message.dateDebutInactiviteProfil" libelle="Date de début :" format="dd/MM/yyyy HH:mm"   maxlength="19" size="19" readonly="${readonly}"
	      >
	 </app:input>
	 <app:input attribut="message.dateFinInactiviteProfil" libelle="Date de fin :" format="dd/MM/yyyy HH:mm"   maxlength="19" size="19" readonly="${readonly}"
	      >
	 </app:input>
	
	</fieldset>
<app:boxboutons>	
	<app:submit label="Retour" transition="_eventId_retour"/>
</app:boxboutons>

</app:form>
</app:page>