<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/tags/html/includes.tagf"%>

<!--  Page recherchedocumentatlascriteres.jsp -->

<c:set var="responsive" value="true" />

<app:page titreecran="Rechercher un document" menu="true" responsive="${responsive}">

    <app:chemins action="composantatlas/flux.ex" responsive="${responsive}">
        <app:chemin action="accueil.ex" title="Retour à l'accueil" responsive="${responsive}">Accueil</app:chemin>
        <app:chemin title="Atlas" responsive="${responsive}">Atlas</app:chemin>
        <app:chemin title="Rechercher un document" responsive="${responsive}">Rechercher un document</app:chemin>
    </app:chemins>

    <app:onglets cleactive="1" responsive="${responsive}" boxongletsclass="row height_equalizer ongletcontainer">
        <app:onglet cle="1" libelle="Rechercher un document" responsive="${responsive}" ongletclass="btmodel_inv_half" />
    </app:onglets>

    <app:form method="post" action="composantatlas/flux.ex" formid="recherchedocumentatlas" formobjectname="recherchedocumentatlasform"
        onsubmit="return submit_form(500)" formboxid="donnees" formboxclass="donnees sousonglets" responsive="${responsive}">

        <app:input attribut="identifiantAtlas" libelle="Recherche par identifiant :" maxlength="50" size="50" responsive="${responsive}" />

        <c:choose>
            <c:when test="${responsive != 'true'}">
                <app:boxboutons>
                    <app:submit label="Retourner à l'accueil" transition="annuler" title="Retourner à l'accueil" />
                    <app:submit label="Rechercher le document" transition="rechercher" id="enterButton" title="Rechercher le document" />
                </app:boxboutons>
            </c:when>
            <c:otherwise>
                <app:boxboutons>
                    <app:submit label="Retourner à l'accueil" transition="annuler" title="Retourner à l'accueil" responsive="${responsive}" />
                    <app:submit label="Rechercher le document" transition="rechercher" id="enterButton" responsive="${responsive}"
                        inputclass="primary btn btn-default" title="Rechercher le document" />
                </app:boxboutons>
            </c:otherwise>
        </c:choose>

    </app:form>
</app:page>