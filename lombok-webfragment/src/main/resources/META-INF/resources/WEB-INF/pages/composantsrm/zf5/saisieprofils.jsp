<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java" %>
<%@ include file="/WEB-INF/tags/html/includes.tagf" %>

<!--  Page saisieprofils.jsp -->

<app:page titreecran="Saisie de messages bienvenue" 
		  titrecontainer="Publication d'informations"
		  menu="true" 
		  titrebnd="publication d'informations">

<app:chemins action="zf5/flux.ex">
    <app:chemin action="accueil.ex" title="Retour à l'accueil">Accueil</app:chemin>
       <app:chemin>Saisie des profils</app:chemin>
</app:chemins>
 
	
 <app:form 
	method="post" 	
	action="zf5/flux.ex" 
	formid="creationprofils" 
	formobjectname="gestionprofilform" 
	onsubmit="return submit_form(500)"
	formboxid="donnees"
	formboxclass="donnees sousonglets">

 	<c:set var="readonly" value="false"/>
	
 	<div>

	 
	 
	<fieldset><legend>Liste des profils </legend>
	 
	<app:inputmultiple   id="profil1" theme="H"
        libelle="Profil " 
        nbfield="2"
        f1attribut="profil1.codeProfil"   f1libelle="Code profil"  f1size="30" f1maxlength="30"  
        f2attribut="profil1.libelleProfil" f2libelle="Libelle profil" f2size="50" f2maxlength="50" >
             </app:inputmultiple>	
             
     <app:inputmultiple   id="profil2" theme="H"
        libelle="Profil " 
        nbfield="2"
        f1attribut="profil2.codeProfil"   f1libelle="Code profil"   f1size="30" f1maxlength="30" 
        f2attribut="profil2.libelleProfil" f2libelle="Libelle profil" f2size="50" f2maxlength="50"  >
             </app:inputmultiple>   
             
             
             
       <app:inputmultiple   id="profil3" theme="H"
        libelle="Profil " 
        nbfield="2"
        f1attribut="profil3.codeProfil"   f1libelle="Code profil"   f1size="30" f1maxlength="30" 
        f2attribut="profil3.libelleProfil" f2libelle="Libelle profil" f2size="50" f2maxlength="50"   >
             </app:inputmultiple>    
             
             
             
        <app:inputmultiple   id="profil4" theme="H"
        libelle="Profil " 
        nbfield="2"
        f1attribut="profil4.codeProfil"   f1libelle="Code profil"   f1size="30" f1maxlength="30" 
        f2attribut="profil4.libelleProfil" f2libelle="Libelle profil" f2size="50" f2maxlength="50"  >
             </app:inputmultiple>    
             
             
             
        <app:inputmultiple   id="profil5" theme="H"
        libelle="Profil " 
        nbfield="2"
        f1attribut="profil5.codeProfil"   f1libelle="Code profil"   f1size="30" f1maxlength="30" 
        f2attribut="profil5.libelleProfil" f2libelle="Libelle profil" f2size="50" f2maxlength="50"  >
             </app:inputmultiple>   
             
             
             
         <app:inputmultiple   id="profil6" theme="H"
        libelle="Profil " 
        nbfield="2"
        f1attribut="profil6.codeProfil"   f1libelle="Code profil"   f1size="30" f1maxlength="30" 
        f2attribut="profil6.libelleProfil" f2libelle="Libelle profil" f2size="50" f2maxlength="50"  >
             </app:inputmultiple>    
             
             
         <app:inputmultiple   id="profil7" theme="H"
        libelle="Profil " 
        nbfield="2"
        f1attribut="profil7.codeProfil"   f1libelle="Code profil"   f1size="30" f1maxlength="30" 
        f2attribut="profil7.libelleProfil" f2libelle="Libelle profil" f2size="50" f2maxlength="50"  >
             </app:inputmultiple>   
             
             
             
         <app:inputmultiple   id="profil8" theme="H"
        libelle="Profil " 
        nbfield="2"
        f1attribut="profil8.codeProfil"   f1libelle="Code profil"   f1size="30" f1maxlength="30" 
        f2attribut="profil8.libelleProfil" f2libelle="Libelle profil" f2size="50" f2maxlength="50"  >
             </app:inputmultiple>   
             
         <app:inputmultiple   id="profil9" theme="H"
        libelle="Profil " 
        nbfield="2"
        f1attribut="profil9.codeProfil"   f1libelle="Code profil"   f1size="30" f1maxlength="30" 
        f2attribut="profil9.libelleProfil" f2libelle="Libelle profil" f2size="50" f2maxlength="50"  >
             </app:inputmultiple>    
             
             
             
          <app:inputmultiple   id="profil10" theme="H"
        libelle="Profil " 
        nbfield="2"
        f1attribut="profil10.codeProfil"   f1libelle="Code profil"   f1size="30" f1maxlength="30" 
        f2attribut="profil10.libelleProfil" f2libelle="Libelle profil" f2size="50" f2maxlength="50"  >
             </app:inputmultiple>    
             
             
          <app:inputmultiple   id="profil11" theme="H"
        libelle="Profil " 
        nbfield="2"
        f1attribut="profil11.codeProfil"   f1libelle="Code profil"   f1size="30" f1maxlength="30" 
        f2attribut="profil11.libelleProfil" f2libelle="Libelle profil"  f2size="50" f2maxlength="50" >
             </app:inputmultiple>    
             
             
          <app:inputmultiple   id="profil12" theme="H"
        libelle="Profil " 
        nbfield="2"
        f1attribut="profil12.codeProfil"   f1libelle="Code profil"   f1size="30" f1maxlength="30" 
        f2attribut="profil12.libelleProfil" f2libelle="Libelle profil" f2size="50" f2maxlength="50"  >
             </app:inputmultiple>   </fieldset>
	</div>  
<app:boxboutons>
	<app:submit inputclass="annul" label="Annuler" transition="_eventId_annuler"/>
	<app:submit label="Valider la creation" transition="_eventId_creationprofils"/>
</app:boxboutons>

</app:form>
</app:page>