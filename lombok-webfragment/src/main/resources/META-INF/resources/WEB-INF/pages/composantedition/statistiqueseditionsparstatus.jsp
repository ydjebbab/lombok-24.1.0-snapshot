<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/tags/html/includes.tagf"%>

<!--  Page statistiqueseditionsparstatus.jsp -->

<c:set var="responsive" value="true" />

<app:page titreecran="Comptage des éditions par statut" menu="true" responsive="${responsive}">

    <app:chemins action="composantedition/admin/flux.ex" responsive="${responsive}">
        <app:chemin transition="fin" title="Retour à l'accueil" responsive="${responsive}">Accueil</app:chemin>
        <app:chemin title="Éditions" responsive="${responsive}">Éditions</app:chemin>
        <app:chemin title="Administration" responsive="${responsive}">Administration</app:chemin>
        <app:chemin title="Statistiques relatives aux éditions" transition="retourecranchoix" responsive="${responsive}">Statistiques relatives aux éditions</app:chemin>
        <app:chemin title="Comptage des éditions par statut" responsive="${responsive}">Comptage des éditions par statut</app:chemin>
    </app:chemins>

    <app:form action="composantedition/admin/flux.ex" formboxclass="eXtremeTable" formobjectname="statistiqueseditionsform"
        responsive="${responsive}">

        <%--
        <ec:table items="statistiqueseditions"
            action="${pageContext.request.contextPath}/composantedition/admin/flux.ex"
            tableId="statistiqueseditions" 
            autoIncludeParameters="false"
            form="formulaire"
            locale="fr_FR"
            view="cphtml"
            filterable="false"
            showPagination="false" showExports="true"
            var="row"
            sortRowsCallback="cp" filterRowsCallback="cp"
        >
--%>
        <ec:table items="statistiqueseditions" tableId="statistiqueseditions" view="cphtml" var="row"
            action="${pageContext.request.contextPath}/composantedition/admin/flux.ex" autoIncludeParameters="false" form="formulaire"
            locale="fr_FR" filterable="true" sortable="true" sortRowsCallback="cp" filterRowsCallback="cp" rowsDisplayed="10"
            showPagination="true" showNewPagination="true" showExports="true" newPaginationNbPage="5" showStatusBar="true"
            showPaginationBottom="false" showExportsBottom="false" showNewFilter="true" exportsClass="col-md-8 col-sm-6 col-xs-12"
            onInvokeAction="ecupdatectra('tableaunavigation')" positionnoresultsfound="body">

            <ec:row highlightRow="true">
                <%--
    			<ec:column property="status"  alias="status" title="Etat"  sortable="true" style="text-align: center"  headerStyle="text-align: center" > </ec:column>
    			<ec:column property="nombreEditions"  alias="nombreEditions" title="Nombre d'éditions" sortable="true" style="text-align: center" headerStyle="text-align: center" > </ec:column>
            --%>
                <ec:column property="status" alias="status" title="Etat" filterable="true" sortable="true" style="text-align: center"
                    headerStyle="text-align: center" showResponsive="${responsive}"
                    dataAttributes="data-tablesaw-priority=persist scope=col">
                </ec:column>
                <ec:column property="nombreEditions" alias="nombreEditions" title="Nombre d'éditions" filterable="true" sortable="true"
                    style="text-align: center" headerStyle="text-align: center" showResponsive="${responsive}"
                    dataAttributes="data-tablesaw-priority=1 scope=col">
                </ec:column>
            </ec:row>
        </ec:table>
        <p>
            <br>
        </p>

        <app:boxboutons>
            <c:choose>
                <c:when test="${responsive != 'true'}">
                    <app:submit label="Page précédente" transition="retourecranchoix" />
                    <app:submit label="Retour à l'accueil" transition="fin" />
                </c:when>
                <c:otherwise>
                    <app:submit label="Page précédente" transition="retourecranchoix" responsive="${responsive}"
                         />
                    <app:submit label="Retour à l'accueil" transition="fin" responsive="${responsive}"  />
                </c:otherwise>
            </c:choose>
        </app:boxboutons>
    </app:form>
    <app:disperror var="${erreurentreflux}" />
</app:page>