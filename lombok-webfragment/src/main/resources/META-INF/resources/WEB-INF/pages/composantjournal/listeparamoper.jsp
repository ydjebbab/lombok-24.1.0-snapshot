<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/tags/html/includes.tagf"%>

<!--  Page listeparamoper.jsp -->

<c:set var="responsive" value="true" />

<app:page titreecran="Paramètres d'une opération du journal d'activité" menu="true" responsive="${responsive}">

    <app:chemins action="composantjournal/flux.ex" responsive="${responsive}">
        <app:chemin transition="retour" title="Retour à l'accueil" responsive="${responsive}">Accueil</app:chemin>
        <app:chemin title="Divers" responsive="${responsive}">Divers</app:chemin>
        <app:chemin title="Consulter les opérations au journal" transition="saisiedate" responsive="${responsive}">Consulter les opérations au journal</app:chemin>
        <app:chemin title="Opérations du journal d'activité" transition="listeoperations" responsive="${responsive}">Opérations du journal d'activité</app:chemin>
        <app:chemin title="Paramètres d'une opération du journal d'activité" responsive="${responsive}">Paramètres d'une opération du journal d'activité</app:chemin>
    </app:chemins>

    <app:form action="composantjournal/flux.ex" formboxclass="eXtremeTable">

        <ec:table items="lesParamOperation" var="cont" autoIncludeParameters="false" form="formulaire" filterable="false" sortable="false"
            showPagination="false" showStatusBar="false" locale="fr_FR" theme="eXtremeTable" view="cphtml">

            <ec:row>
                <ec:column property="nom" title="Nom du paramètre" />
                <ec:column property="valeur" title="Valeur du paramètre" />
            </ec:row>
        </ec:table>

        <app:boxboutons>
            <c:choose>
                <c:when test="${responsive != 'true'}">
                    <app:submit label="Retourner à l'accueil" transition="retour" title="Retourner à l'accueil" />
                    <app:submit label="Retourner à la page précédente" title="Retourner à la page précédente" transition="listeoperations" />
                </c:when>
                <c:otherwise>
                    <app:submit label="Retourner à l'accueil" title="Retourner à l'accueil" transition="retour" responsive="${responsive}" />
                    <app:submit label="Retouner à la page précédente" title="Retourner à la page précédente" transition="listeoperations"
                        responsive="${responsive}" />
                </c:otherwise>
            </c:choose>
        </app:boxboutons>
    </app:form>
</app:page>
