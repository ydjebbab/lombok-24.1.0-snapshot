<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/tags/html/includes.tagf"%>

<!--  Page recherchepurgeseditioncriteres.jsp -->

<c:set var="responsive" value="true" />

<app:page titreecran="Administration des traces de purges d'éditions" menu="true" responsive="${responsive}">

    <app:chemins action="composantedition/admin/flux.ex" responsive="${responsive}">
        <app:chemin action="accueil.ex" title="Retour à l'accueil" responsive="${responsive}">Accueil</app:chemin>
        <app:chemin title="Éditions" responsive="${responsive}">Éditions</app:chemin>
        <app:chemin title="Administration" responsive="${responsive}">Administration</app:chemin>
        <app:chemin title="Administration des traces de purges d'éditions" responsive="${responsive}">Administration des traces de purges d'éditions</app:chemin>
    </app:chemins>

    <app:form method="post" action="composantedition/admin/flux.ex" formid="recherchepurgeedition"
        formobjectname="administrationpurgeseditionsform" onsubmit="return submit_form(500)" formboxid="donnees"
        formboxclass="donnees sousonglets">

        <fieldset>
            <div>
                <app:input size="30" maxlength="30" attribut="dateDebutPurge" libelle="Date de début de la purge :" requis="false"
                    labelboxwidth="25%" inputboxwidth="13%" compboxwidth="20%" placeholder="JJ/MM/AAAA"
                    consigne="Sélectionnez une date de début de purge" responsive="${responsive}">
                    <app:calendar />
                </app:input>                
            </div>
            <app:submit transition="selectondatedebutpurge" label="Rechercher par date de purge"
                title="Recherche toutes les purges qui ont commencé à la date sélectionnée." responsive="${responsive}" />
        </fieldset>

        <fieldset>
            <div>
                <legend>Période de la purge</legend>
                <app:input size="30" maxlength="30" attribut="debutPeriodePurge" libelle="Début période :" requis="false"
                    labelboxwidth="25%" inputboxwidth="13%" compboxwidth="20%" placeholder="JJ/MM/AAAA"
                    consigne="Sélectionnez le début de la période de recherche" responsive="${responsive}">
                    <app:calendar />
                </app:input>

                <app:input size="30" maxlength="30" attribut="finPeriodePurge" libelle="Fin période :" requis="false" labelboxwidth="25%"
                    inputboxwidth="13%" compboxwidth="20%" placeholder="JJ/MM/AAAA"
                    consigne="Sélectionnez la fin de la période de recherche" responsive="${responsive}">
                    <app:calendar />
                </app:input>
            </div>
            <app:submit transition="selectonperiodedatepurge" label="Rechercher par période de purge"
                title="Recherche toutes les purges qui ont été demandées durant la période sélectionnée." responsive="${responsive}" />
        </fieldset>

        <%--         <app:boxboutons> --%>
        <%--             <app:submit label="Annuler" transition="annuler" responsive="${responsive}" /> --%>
        <%--             <app:submit transition="selectallpurge" label="Afficher toutes les traces de purge" responsive="${responsive}" /> --%>
        <%--         </app:boxboutons> --%>

        <app:boxboutons>
            <c:choose>
                <c:when test="${responsive != 'true'}">
                    <app:submit label="Retourner à l'accueil" transition="annuler" title="Retourner à l'accueil" />
                    <app:submit label="Afficher toutes les traces de purge" transition="selectallpurge"
                        title="Utilisz ce bouton pour afficher toutes les traces de purge."></app:submit>
                </c:when>
                <c:otherwise>
                    <app:submit label="Retourner à l'accueil" transition="annuler" responsive="${responsive}" title="Retourner à l'accueil" />
                    <app:submit label="Afficher toutes les traces de purge" transition="selectallpurge"
                        title="Utilisz ce bouton pour afficher toutes les traces de purge." inputclass="primary btn btn-default"></app:submit>
                </c:otherwise>
            </c:choose>
        </app:boxboutons>

    </app:form>
</app:page>