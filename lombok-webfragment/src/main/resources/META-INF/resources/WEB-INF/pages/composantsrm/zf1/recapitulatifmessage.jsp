<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java" %>
<%@ include file="/WEB-INF/tags/html/includes.tagf" %>

<!--  Page recapitulatifmessage.jsp -->

<app:page titreecran="création des messages bienvenue" 
		  titrecontainer=" "
		  menu="true" 
		  titrebnd="Récapitulatif du message">

  <app:chemins action="zf1/flux.ex">
    <app:chemin action="accueil.ex" title="Retour à l'accueil">Accueil</app:chemin>
       <app:chemin>Récapitulatif du message</app:chemin>
</app:chemins>

	
 <app:form 
	method="post" 	
	action="zf1/flux.ex" 
	formid="creationmessage" 
	formobjectname="creationmessageform" 
	onsubmit="return submit_form(500)"
	formboxid="donnees"
	formboxclass="donnees sousonglets">

 	<c:set var="readonly" value="true"/>
 	
	<div>
	
	<fieldset><legend>Caractéristiques du message</legend>
 
 	<c:choose> 
        <c:when test="${fn:length(creationmessageform.message.libelleMessage) < 150}">
           <app:textarea attribut="message.libelleMessage" libelle="Libelle :" cols="75" rows="2"  tabindex="2"   readonly="${readonly}"/>
    
        </c:when>
        <c:otherwise>
           <app:textarea attribut="message.libelleMessage" libelle="Libelle :" cols="75" rows="30"  tabindex="2"   readonly="${readonly}"/>
    
        </c:otherwise>
    </c:choose>  
 
	<app:textarea attribut="message.libelleTypeMessage" libelle="Type du message :" cols="20" rows="1"  tabindex="3"   readonly="${readonly}"/>
	</fieldset>
	<fieldset><legend>Périodes d'affichage du message</legend>
	
	<app:input attribut="message.dateDebutMessage" format="dd/MM/yyyy HH:mm" libelle="Date de début d'affichage du message :" maxlength="19" size="19" readonly="${readonly}">		 
	</app:input>
	<app:input attribut="message.dateFinMessage" format="dd/MM/yyyy HH:mm" libelle="Date de fin d'affichage du message :" maxlength="19" size="19" readonly="${readonly}">
	 </app:input>
</fieldset>
</div>
 
	
	 
<app:boxboutons>	
	<app:submit label="Retour" transition="_eventId_retour"/>
</app:boxboutons>

</app:form>
</app:page>