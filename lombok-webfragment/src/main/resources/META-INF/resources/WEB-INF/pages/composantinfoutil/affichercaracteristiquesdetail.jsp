<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/tags/html/includes.tagf"%>

<!--  Page affichercaracteristiquesdetail.jsp -->

<c:set var="responsive" value="true" />

<app:page titreecran="Caractéristiques utilisateur" menu="true" responsive="${responsive}">

    <app:chemins action="zf2/flux.ex" responsive="${responsive}">
        <app:chemin action="accueil.ex" title="Retour à l'accueil" responsive="${responsive}">Accueil</app:chemin>
        <app:chemin title="Divers" responsive="${responsive}">Divers</app:chemin>
        <app:chemin title="Caractéristiques utilisateur" responsive="${responsive}">Caractéristiques utilisateur</app:chemin>
    </app:chemins>

    <app:onglets cleactive="2" responsive="${responsive}" boxongletsclass="row height_equalizer ongletcontainer">
        <app:onglet cle="1" libelle="Général" bouton="_eventId_VoirGeneral" responsive="${responsive}" ongletclass="btmodel_inv_third" />
        <app:onglet cle="2" libelle="Détail" responsive="${responsive}" ongletclass="btmodel_inv_third" />
        <app:onglet cle="3" libelle="Habilitation" bouton="_eventId_VoirHabilitations" title="Consulter le détail de l'habilitation"
            responsive="${responsive}" ongletclass="btmodel_inv_third" />
    </app:onglets>

    <app:form action="composantinfoutil/flux.ex" formobjectname="affichagecaracteristiquesutilisateurform"
        formboxclass="donnees sousonglets" responsive="${responsive}">

        <app:input attribut="unePersonneAnnuaire.fonction" libelle="Fonction : " maxlength="40" size="40" readonly="true" requis="false"
            responsive="${responsive}" bootstraplabelclass="btmodel_half" bootstrapinputclass="btmodel_half" />

        <app:input attribut="unePersonneAnnuaire.mail" libelle="Adresse mel :" maxlength="40" size="40" readonly="true" requis="false"
            responsive="${responsive}" bootstraplabelclass="btmodel_half" bootstrapinputclass="btmodel_half" />

        <app:input attribut="unePersonneAnnuaire.telephoneNumber" libelle="Numero de Telephone : " maxlength="40" size="40" readonly="true"
            requis="false" responsive="${responsive}" bootstraplabelclass="btmodel_half" bootstrapinputclass="btmodel_half" />

        <app:boxboutons divboxboutonsclass="boxboutons">
            <app:submit label="Retourner à l'accueil" title="Retourner à l'accueil" transition="RetourAccueil" id="enterButton"
                responsive="${responsive}" />
        </app:boxboutons>
    </app:form>
</app:page>