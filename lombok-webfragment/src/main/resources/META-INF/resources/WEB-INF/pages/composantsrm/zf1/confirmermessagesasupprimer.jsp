<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/tags/html/includes.tagf" %>

<!--  Page confirmermessagesasupprimer.jsp -->

<app:page titreecran="Selectionner un message"
          titrecontainer=" " 
          menu="true" 
          donneescontainerboxclass="tablecontainer">
          
   <app:chemins action="zf1/flux.ex">
    <app:chemin action="accueil.ex" title="Retour à l'accueil">Accueil</app:chemin>
       <app:chemin>Suppression de messages</app:chemin>
</app:chemins>
<app:form method='POST' 
		  formobjectname='suppressionmessagesform'
          action='zf1/flux.ex'>
<spring:bind path="suppressionmessagesform.listeDesMessagesASupprimer">
<ec:table items="status.value"  form="suppressionmessages"
		var="cont"
		autoIncludeParameters="false"
		filterable="false" 
		sortable="false"
		showPagination="false"
		showStatusBar="false"
		locale="fr_FR"		
		theme="eXtremeTable" 
		>
   		<ec:row highlightRow="false">
			<ec:column property="libelleMessage" title="libelle du message" width="15%" sortable="true" filterable="true" > </ec:column>
			<ec:column property="UIDCreateurMessage" title="créateur du message" width="35%" sortable="true" filterable="true"></ec:column>
			<ec:column property="libelleTypeMessage" title="type du message" width="35%" sortable="true" filterable="true"></ec:column>
			<ec:column property="dateDebutMessage" title="date de début d'affichage du message" width="15%" sortable="true" filterable="true" cell="dateheureminute" format="dd/MM/yyyy HH:mm"> </ec:column>
			<ec:column property="dateFinMessage" title="date de fin d'affichage du message" width="15%" sortable="true" filterable="true" cell="dateheureminute" format="dd/MM/yyyy HH:mm"> </ec:column>
			<ec:column property="dateDebutInactiviteProfil" title="date de début d'inactivité du profil" width="15%" sortable="true" filterable="true" cell="dateheureminute" format="dd/MM/yyyy HH:mm"> </ec:column>
			<ec:column property="dateFinInactiviteProfil" title="date de fin d'inactivité du profil" width="15%" sortable="true" filterable="true" cell="dateheureminute" format="dd/MM/yyyy HH:mm"> </ec:column>
			<ec:column property="lesProfilsDesactivesTxt" title="profils désactivés" width="35%" sortable="true" filterable="true"></ec:column>
			<ec:column property="lesProfilsDestinatairesTxt" title="profils destinataires" width="35%" sortable="true" filterable="true"></ec:column>
			<ec:column property="lesCodiquesDestinatairesTxt" title="codiques destinataires" width="35%" sortable="true" filterable="true"></ec:column>
		</ec:row>
</ec:table>
</spring:bind>
<app:boxboutons>
	<app:submit inputclass="annul" label="Annuler" transition="_eventId_Annuler"/>
	<app:submit label="Supprimer" transition="_eventId_Supprimer"/>
</app:boxboutons>

</app:form>
</app:page>
