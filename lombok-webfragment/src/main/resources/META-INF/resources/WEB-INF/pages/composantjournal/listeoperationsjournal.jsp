<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java"%>
<%@ include file="/WEB-INF/tags/html/includes.tagf"%>

<!--  Page listeoperationsjournal.jsp -->

<c:set var="responsive" value="true" />

<app:page titreecran="Opérations du journal d'activité" titrecontainer="" menu="true" responsive="${responsive}">

    <app:chemins action="composantjournal/flux.ex" responsive="${responsive}">
        <app:chemin action="accueil.ex" title="Retour à l'accueil" responsive="${responsive}">Accueil</app:chemin>
        <app:chemin title="Divers" responsive="${responsive}">Divers</app:chemin>
        <app:chemin title="Consulter les opérations au journal" transition="saisiedate" responsive="${responsive}">Consulter les opérations au journal</app:chemin>
        <app:chemin title="Opérations du journal d'activité" responsive="${responsive}">Opérations du journal d'activité</app:chemin>
    </app:chemins>

    <app:form action="composantjournal/flux.ex" formboxclass="eXtremeTable" formobjectname="recherchelesoperationsjournalform"
        responsive="${responsive}">

        <%--
        <ec:table items="listeoperationsjournal" 
            action="${pageContext.request.contextPath}/composantjournal/flux.ex" 
            tableId="opjour"
            autoIncludeParameters="false"
            form="formulaire"
            locale="fr_FR" 
            view="cphtml"
            filterable="true" 
            sortable="true"     
            showPagination="false" showExports="true" 
            var="row" 
            sortRowsCallback="cp" filterRowsCallback="cp"
        >
        --%>
        <ec:table items="listeoperationsjournal" tableId="opjour" view="cphtml" var="row"
            action="${pageContext.request.contextPath}/composantjournal/flux.ex" autoIncludeParameters="false" form="formulaire"
            locale="fr_FR" filterable="true" sortable="true" sortRowsCallback="cp" filterRowsCallback="cp" showPagination="true"
            showNewPagination="true" showExports="true" newPaginationNbPage="5" showStatusBar="true" showPaginationBottom="false"
            showExportsBottom="false" showNewFilter="true" onInvokeAction="ecupdatectra('tableaunavigation')" positionnoresultsfound="body">

            <%--
            <ec:row highlightRow="true"  namePropertyShowIfEqualsLast="identifiantUtilisateurOuBatch">
                <ec:column property="id" title="Identifiant" writeOrBlank="true">
                    <app:link action="composantjournal/flux.ex"
                    transition="afficher"
                    attribute="id"
                    value="${row.id}"
                    label="${row.id}"
                    active="true" 
                    title="Consulter les paramétres opération" />
            </ec:column>
            <ec:column property="dateHeureOperation" title="Date et heure de l'opération" cell="dateheureminuteseconde" />
            <ec:column property="identifiantUtilisateurOuBatch" title="identifiant de l'utilisateur ou du batch"  writeOrBlank="true"/>
            <ec:column property="natureOperation" title="Nature de l'opération"  writeOrBlank="true"/>
            <ec:column property="typeDureeDeConservation" title="Type de la durée de conservation"  writeOrBlank="true"/>
        --%>
            <ec:row highlightRow="true" namePropertyShowIfEqualsLast="identifiantUtilisateurOuBatch">
                <ec:column property="id" title="Identifiant" writeOrBlank="true" sortable="true" filterable="true"
                    showResponsive="${responsive}" dataAttributes="data-tablesaw-priority=1 scope=col">
                    <app:link action="composantjournal/flux.ex" transition="afficher" attribute="id" value="${row.id}" label="${row.id}"
                        active="true" title="Consulter les paramétres opération" />
                </ec:column>
                <ec:column property="dateHeureOperation" title="Date et heure de l'opération" cell="dateheureminuteseconde" sortable="true"
                    filterable="true" showResponsive="${responsive}" dataAttributes="data-tablesaw-priority=2 scope=col" />
                <ec:column property="identifiantUtilisateurOuBatch" title="identifiant de l'utilisateur ou du batch" writeOrBlank="true"
                    sortable="true" filterable="true" showResponsive="${responsive}" dataAttributes="data-tablesaw-priority=3 scope=col" />
                <ec:column property="natureOperation" title="Nature de l'opération" writeOrBlank="true" sortable="true" filterable="true"
                    showResponsive="${responsive}" dataAttributes="data-tablesaw-priority=4 scope=col" />
                <ec:column property="typeDureeDeConservation" title="Type de la durée de conservation" writeOrBlank="true" sortable="true"
                    filterable="true" showResponsive="${responsive}" dataAttributes="data-tablesaw-priority=5 scope=col" />

            </ec:row>
        </ec:table>

        <app:boxboutons>
            <c:choose>
                <c:when test="${responsive != 'true'}">
                    <app:submit label="Retourner à l'accueil" transition="retour" title="Retourner à l'accueil" />
                    <app:submit label="Retourner à la page précédente" title="Retourner à la page précédente" transition="saisiedate" />
                </c:when>
                <c:otherwise>
                    <app:submit label="Retourner à l'accueil" title="Retourner à l'accueil" transition="retour" responsive="${responsive}" />
                    <app:submit label="Retouner à la page précédente" title="Retourner à la page précédente" transition="saisiedate"
                        responsive="${responsive}" />
                </c:otherwise>
            </c:choose>
        </app:boxboutons>
    </app:form>
    <app:disperror var="${erreurentreflux}" />
</app:page>