<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java" %>
<%@ include file="/WEB-INF/tags/html/includes.tagf" %>

<!--  Page erreurprogrammation.jsp -->

<app:page titreecran="Erreur" 
		  titrecontainer="Erreur" 
		  menu="true" >

<app:disperrorfix  divid="popmessage" >
			<p>Erreur dans l'application</p>
            <p>Erreur de programmation</p>
</app:disperrorfix>

	<%@ include file="/WEB-INF/pages/errors/rapporterreur.jspf" %>

</app:page>
