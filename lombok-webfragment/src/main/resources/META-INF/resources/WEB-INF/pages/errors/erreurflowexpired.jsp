<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java" %>
<%@ include file="/WEB-INF/tags/html/includes.tagf" %>

<!--  Page erreurflowexpired.jsp -->

<app:page titreecran="Erreur" 
		  titrecontainer="Erreur de navigation" 
		  menu="true" >

<app:disperrorfix  divid="popmessage">
			<p>Une erreur de navigation s'est produite.</p>
			<p>Votre connexion a expiré. Reconnectez-vous à l'application</p>
</app:disperrorfix>

<%@ include file="/WEB-INF/pages/errors/rapporterreur.jspf" %>

</app:page>
