<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/tags/html/includes.tagf"%>

<!--  Page affichercaracteristiqueshabilitations.jsp -->

<c:set var="responsive" value="true" />

<app:page titreecran="Caractéristiques utilisateur" menu="true" responsive="${responsive}">

    <app:chemins action="zf2/flux.ex" responsive="${responsive}">
        <app:chemin action="accueil.ex" title="Retour à l'accueil" responsive="${responsive}">Accueil</app:chemin>
        <app:chemin title="Divers" responsive="${responsive}">Divers</app:chemin>
        <app:chemin title="Caractéristiques utilisateur" responsive="${responsive}">Caractéristiques utilisateur</app:chemin>
    </app:chemins>

    <app:onglets cleactive="3" responsive="${responsive}" boxongletsclass="row height_equalizer ongletcontainer">
        <app:onglet cle="1" libelle="Général" bouton="_eventId_VoirGeneral" responsive="${responsive}" ongletclass="btmodel_inv_third" />
        <app:onglet cle="2" libelle="Détail" bouton="_eventId_VoirDetail" responsive="${responsive}" ongletclass="btmodel_inv_third" />
        <app:onglet cle="3" libelle="Habilitation" responsive="${responsive}" ongletclass="btmodel_inv_third" />

    </app:onglets>
    <app:form action="composantinfoutil/flux.ex" formboxclass="donnees sousonglets"
        formobjectname="affichagecaracteristiquesutilisateurform" responsive="${responsive}">

        <%--
       
        <ec:table items="listehabilitations"
            action="${pageContext.request.contextPath}composantinfoutil/flux.ex" 
            tableId="habilit" 
            autoIncludeParameters="false"
            form="formulaire"
            locale="fr_FR" 
            view="cphtml"
            filterable="false"  
            showPagination="false" showExports="true" 
            var="row" 
            sortRowsCallback="cp" filterRowsCallback="cp"
        >
        --%>
        <ec:table items="listehabilitations" tableId="habilit" view="cphtml" var="row"
            action="${pageContext.request.contextPath}composantinfoutil/flux.ex" autoIncludeParameters="false" form="formulaire"
            locale="fr_FR" filterable="true" sortable="true" sortRowsCallback="cp" filterRowsCallback="cp" rowsDisplayed="10"
            showPagination="true" showNewPagination="true" showExports="true" newPaginationNbPage="5" showStatusBar="true"
            showPaginationBottom="false" showExportsBottom="false" showNewFilter="true" onInvokeAction="ecupdatectra('tableaunavigation')"
            positionnoresultsfound="body">

            <ec:row highlightRow="true">
                <%--
                <ec:column property="nomProfil" title="Profil">
                    <app:link action="composantinfoutil/flux.ex"
                    transition="afficherfiltres"
                    attribute="nomProfil"
                    value="${row.nomProfil}"
                    label="${row.nomProfil}"    
                    active="true" 
                    title="Consulter les filtres associés"
                     />
                </ec:column>        
                <ec:column property="nbFiltres" title="Filtres" />  
                <ec:column property="nomAdministrateur" title="Administrateur ayant donné le profil" />  
                <ec:column property="dateCreation" title="Date creation" cell="date" format="dd/MM/yyyy hh/mm/ss" />
                <ec:column property="dateDebut" title="Debut de validité" cell="date" format="dd/MM/yyyy hh/mm/ss" />
                <ec:column property="dateFin" title="Fin de validité" cell="date" format="dd/MM/yyyy hh/mm/ss" />
            --%>
                <ec:column property="nomProfil" title="Profil" sortable="true" filterable="true" showResponsive="${responsive}"
                    dataAttributes="data-tablesaw-priority=persist scope=col">
                    <app:link action="composantinfoutil/flux.ex" transition="afficherfiltres" attribute="nomProfil" value="${row.nomProfil}"
                        label="${row.nomProfil}" active="true" title="Consulter les filtres associés" />
                </ec:column>
                <ec:column property="nbFiltres" title="Filtres" sortable="true" filterable="true" showResponsive="${responsive}"
                    dataAttributes="data-tablesaw-priority=1 scope=col" />
                <ec:column property="nomAdministrateur" title="Administrateur ayant donné le profil" sortable="true" filterable="true"
                    showResponsive="${responsive}" dataAttributes="data-tablesaw-priority=2 scope=col" />
                <ec:column property="dateCreation" title="Date creation" cell="date" format="dd/MM/yyyy hh/mm/ss" sortable="true"
                    filterable="true" showResponsive="${responsive}" dataAttributes="data-tablesaw-priority=3 scope=col" />
                <ec:column property="dateDebut" title="Debut de validité" cell="date" format="dd/MM/yyyy hh/mm/ss" sortable="true"
                    filterable="true" showResponsive="${responsive}" dataAttributes="data-tablesaw-priority=4 scope=col" />
                <ec:column property="dateFin" title="Fin de validité" cell="date" format="dd/MM/yyyy hh/mm/ss" sortable="true"
                    filterable="true" showResponsive="${responsive}" dataAttributes="data-tablesaw-priority=5 scope=col" />
            </ec:row>
        </ec:table>

        <app:boxboutons>
            <c:choose>
                <c:when test="${responsive != 'true'}">
                    <app:submit label="Retourner à l'accueil" transition="RetourAccueil"
                        title="Retourner à l'accueil" id="enterButton" />
                </c:when>
                <c:otherwise>
                    <app:submit label="Retourner à l'accueil" transition="RetourAccueil"
                        title="Retourner à l'accueil" id="enterButton" responsive="${responsive}"
                         />
                </c:otherwise>
            </c:choose>
        </app:boxboutons>
    </app:form>
    <app:disperror var="${erreurentreflux}" />
</app:page>