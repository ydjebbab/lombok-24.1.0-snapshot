<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/tags/html/includes.tagf"%>

<!--  Page detaileditionpresentation.jsp -->

<c:set var="responsive" value="true" />

<app:page titreecran="Détail d'une édition" responsive="${responsive}">

    <app:chemins action="composantedition/admin/flux.ex" responsive="${responsive}">
        <app:chemin transition="fin" title="Retour à l'accueil" responsive="${responsive}">Accueil</app:chemin>
        <app:chemin title="Éditions" responsive="${responsive}">Éditions</app:chemin>
        <app:chemin title="Administration" responsive="${responsive}">Administration</app:chemin>
        <app:chemin title="Administration des éditions" transition="retourecranrecherche" responsive="${responsive}">Administration des éditions</app:chemin>
        <app:chemin title="Liste des éditions sélectionnées" transition="retourlisteeditions" responsive="${responsive}">Liste des éditions sélectionnées</app:chemin>
        <app:chemin title="Détail d'une édition" responsive="${responsive}">Détail d'une édition</app:chemin>
    </app:chemins>

    <app:onglets cleactive="4" responsive="${responsive}" boxongletsclass="row height_equalizer ongletcontainer">
        <app:onglet cle="1" libelle="Identification" transition="detaileditionidentification" responsive="${responsive}"
            ongletclass="btmodel_2" />
        <app:onglet cle="2" libelle="Destination" transition="detaileditiondestination" responsive="${responsive}" ongletclass="btmodel_2" />
        <app:onglet cle="3" libelle="Ordonnancement" transition="detaileditionordonnancement" responsive="${responsive}"
            ongletclass="btmodel_2" />
        <app:onglet cle="4" libelle="Présentation" responsive="${responsive}" ongletclass="btmodel_2" />
        <app:onglet cle="5" libelle="Mail" transition="detaileditionmail" responsive="${responsive}" ongletclass="btmodel_2" />
        <app:onglet cle="6" libelle="Stockage" transition="detaileditionstockage" responsive="${responsive}" ongletclass="btmodel_2" />
    </app:onglets>

    <app:form method="post" action="composantedition/admin/flux.ex" formid="rechercheedition" formobjectname="administrationeditionsform"
        onsubmit="return submit_form(500)" formboxid="donnees" formboxclass="donnees sousonglets" responsive="${responsive}">

        <fieldset>
            <legend>Présentation</legend>
            <app:input attribut="jobHistory.presentationEdition.description" libelle="Description :" maxlength="70" size="70" requis="false"
                readonly="true" responsive="${responsive}" />

            <app:input attribut="jobHistory.presentationEdition.characterEncoding" libelle="Encodage :" maxlength="30" size="30"
                requis="false" readonly="true" responsive="${responsive}" />

            <app:input attribut="jobHistory.presentationEdition.extension" libelle="Extension :" maxlength="30" size="30" requis="false"
                readonly="true" responsive="${responsive}" />

            <app:input attribut="jobHistory.presentationEdition.nomFicDownload" libelle="Nom du fichier à télécharger :" maxlength="70"
                size="70" requis="false" readonly="true" responsive="${responsive}" />

            <app:input attribut="jobHistory.presentationEdition.typeMime" libelle="Type MIME :" maxlength="30" size="30" requis="false"
                readonly="true" responsive="${responsive}" />

            <app:textarea attribut="jobHistory.presentationEdition.urlEdition" libelle="URL d'accès à l'édition :" cols="70" rows="3"
                requis="false" readonly="true" responsive="${responsive}" />
        </fieldset>

        <app:boxboutons>
            <c:choose>
                <c:when test="${responsive != 'true'}">
                    <app:submit label="Retour à la liste" transition="retourlisteeditions" />
                </c:when>
                <c:otherwise>
                    <app:submit label="Retour à la liste" transition="retourlisteeditions" responsive="${responsive}"
                         />
                </c:otherwise>
            </c:choose>
        </app:boxboutons>

    </app:form>
    <app:disperror var="${erreurentreflux}" />
</app:page>