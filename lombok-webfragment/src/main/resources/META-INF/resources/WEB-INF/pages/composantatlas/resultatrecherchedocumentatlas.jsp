<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/tags/html/includes.tagf"%>

<!--  Page resultatrecherchedocumentatlas.jsp -->

<c:set var="responsive" value="true" />

<app:page titreecran="Résultats de la recherche" menu="true" responsive="${responsive}">

    <app:chemins action="composantatlas/flux.ex" responsive="${responsive}">
        <app:chemin action="accueil.ex" title="Retour à l'accueil" responsive="${responsive}">Accueil</app:chemin>
        <app:chemin title="Atlas" responsive="${responsive}">Atlas</app:chemin>
        <app:chemin title="Rechercher un document" transition="NouvelleRecherche" responsive="${responsive}">Rechercher un document</app:chemin>
        <app:chemin title="Résultats de la recherche" responsive="${responsive}">Résultats de la recherche</app:chemin>
    </app:chemins>

    <app:form action="composantatlas/flux.ex" formid="formulaire" formboxclass="eXtremeTable" responsive="${responsive}">
        <%--
        
        <ec:table items="listedocument"
            tableId="rechdoc"
            view="cphtml"
        var="doc"  
            action="flux.ex"
            state="notifyToDefault"
            stateAttr="gotofirstpage"
        autoIncludeParameters="false" 
        form="formulaire" 
            locale="fr_FR"
            filterable="true"
            sortable="true"
            sortRowsCallback="cp"
            filterRowsCallback="cpfiltreinfsup"
            rowsDisplayed="10"
            showPagination="true">
 --%>
        <ec:table items="listedocument" tableId="rechdoc" view="cphtml" var="doc" action="flux.ex" state="notifyToDefault"
            stateAttr="gotofirstpage" autoIncludeParameters="false" form="formulaire" locale="fr_FR" filterable="true" sortable="true"
            sortRowsCallback="cp" filterRowsCallback="cpfiltreinfsup" rowsDisplayed="10" showPagination="true" showNewPagination="true"
            showExports="true" newPaginationNbPage="5" showStatusBar="true" showPaginationBottom="false" showExportsBottom="false"
            showNewFilter="true" onInvokeAction="ecupdatectra('tableaunavigation')" positionnoresultsfound="body">

            <ec:row highlightRow="true">
                <%--
                <ec:column property="identifiantAtlas" title="Consultation" width="30%" sortable="true" filterable="true">
                    <app:link action="composantatlas/flux.ex" transition="consulterdocument" attribute="identifiantAtlas"
                        value="${doc.identifiantAtlas}" label="${doc.identifiantAtlas}" active="true" openwindow="false" />
                </ec:column>
                <ec:column property="identifiantAtlas" title="Rupture de flux" width="30%" sortable="true" filterable="true">
                    <app:link action="composantatlas/flux.ex" transition="consulterdocumentrf" attribute="identifiantAtlas"
                        value="${doc.identifiantAtlas}" label="${doc.identifiantAtlas}" active="true" openwindow="false" />
                </ec:column>
                <ec:column property="identifiantAtlas" title="Mel" width="30%" sortable="true" filterable="true">
                    <app:button action="composantatlas/flux.ex" transition="envoyerdocument" attribute="identifiantAtlas"
                        value="${doc.identifiantAtlas}" active="true" openwindow="false">Envoyer par mel</app:button>
                </ec:column>
                <ec:column property="identifiantAtlas" title="Consultation image" width="30%" sortable="true" filterable="true">
                    <app:link action="composantatlas/flux.ex" transition="consulterdocumentimg" attribute="identifiantAtlas"
                        value="${doc.identifiantAtlas}" label="${doc.identifiantAtlas}" active="true" openwindow="false" />
                </ec:column>
                <ec:column property="identifiantAtlas" title="Identifiant Atlas" width="30%" sortable="true" filterable="true">
                </ec:column>
                <ec:column property="nom" title="Nom" width="20%" sortable="true" filterable="true" />
                <ec:column property="dateDePeremption" title="Date de péremption" cell="date" width="15%" sortable="true" filterable="true" />
                <ec:column property="dateDEnvoiAtlas" title="Date d'envoi Atlas" cell="date" width="15%" sortable="true" filterable="true" />
                <ec:column property="datePurgeContenu" title="Date de purge du contenu" cell="date" width="15%" sortable="true"
                    filterable="true" />               
                  --%>

                <ec:column property="identifiantAtlas" title="Consultation" width="30%" sortable="true" filterable="true"
                    showResponsive="${responsive}" dataAttributes="data-tablesaw-priority=persist scope=col">
                    <app:link action="composantatlas/flux.ex" transition="consulterdocument" attribute="identifiantAtlas"
                        value="${doc.identifiantAtlas}" label="${doc.identifiantAtlas}" active="true" openwindow="false" />
                </ec:column>
                <ec:column property="identifiantAtlas" title="Rupture de flux" width="30%" sortable="true" filterable="true"
                    showResponsive="${responsive}" dataAttributes="data-tablesaw-priority=1 scope=col">
                    <app:link action="composantatlas/flux.ex" transition="consulterdocumentrf" attribute="identifiantAtlas"
                        value="${doc.identifiantAtlas}" label="${doc.identifiantAtlas}" active="true" openwindow="false" />
                </ec:column>
                <ec:column property="identifiantAtlas" title="Mel" width="30%" sortable="true" filterable="true"
                    showResponsive="${responsive}" dataAttributes="data-tablesaw-priority=2 scope=col">
                    <app:button action="composantatlas/flux.ex" transition="envoyerdocument" attribute="identifiantAtlas"
                        value="${doc.identifiantAtlas}" active="true" openwindow="false">Envoyer par mel</app:button>
                </ec:column>
                <ec:column property="identifiantAtlas" title="Consultation image" width="30%" sortable="true" filterable="true"
                    showResponsive="${responsive}" dataAttributes="data-tablesaw-priority=3 scope=col">
                    <app:link action="composantatlas/flux.ex" transition="consulterdocumentimg" attribute="identifiantAtlas"
                        value="${doc.identifiantAtlas}" label="${doc.identifiantAtlas}" active="true" openwindow="false" />
                </ec:column>
                <ec:column property="identifiantAtlas" title="Identifiant Atlas" width="30%" sortable="true" filterable="true"
                    showResponsive="${responsive}" dataAttributes="data-tablesaw-priority=4 scope=col" />
                <ec:column property="nom" title="Nom" width="20%" sortable="true" filterable="true" showResponsive="${responsive}"
                    dataAttributes="data-tablesaw-priority=5 scope=col" />
                <ec:column property="dateDePeremption" title="Date de péremption" cell="date" width="15%" sortable="true" filterable="true"
                    showResponsive="${responsive}" dataAttributes="data-tablesaw-priority=6 scope=col" />
                <ec:column property="dateDEnvoiAtlas" title="Date d'envoi Atlas" cell="date" width="15%" sortable="true" filterable="true"
                    showResponsive="${responsive}" dataAttributes="data-tablesaw-priority=7 scope=col" />
                <ec:column property="datePurgeContenu" title="Date de purge du contenu" cell="date" width="15%" sortable="true"
                    filterable="true" showResponsive="${responsive}" dataAttributes="data-tablesaw-priority=8 scope=col" />
            </ec:row>
        </ec:table>

        <c:choose>
            <c:when test="${responsive != 'true'}">
                <app:boxboutons>
                    <app:submit label="Retourner à l'accueil" transition="annuler" title="Retourner à l'accueil" />
                    <app:submit label="Faire une nouvelle recherche de document" transition="NouvelleRecherche" id="enterButton"
                        title="Faire une nouvelle recherche de document" />
                </app:boxboutons>
            </c:when>
            <c:otherwise>
                <app:boxboutons>
                    <app:submit label="Retourner à l'accueil" transition="annuler" title="Retourner à l'accueil" responsive="${responsive}" />
                    <app:submit label="Faire une nouvelle recherche de document" transition="NouvelleRecherche" id="enterButton"
                        responsive="${responsive}" inputclass="primary btn btn-default" title="Faire une nouvelle recherche de document" />
                </app:boxboutons>
            </c:otherwise>
        </c:choose>



    </app:form>
    <app:disperror var="${erreurentreflux}" />
</app:page>