<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/tags/html/includes.tagf"%>

<!--  Page banetteeditions.jsp -->

<c:set var="responsive" value="true" />

<app:page titreecran="Bannette des éditions" menu="true" responsive="${responsive}">
    <app:chemins action="composantedition/flux.ex" responsive="${responsive}">
        <app:chemin transition="back" title="Retourner à l'accueil" responsive="${responsive}">Accueil</app:chemin>
        <app:chemin title="Éditions" responsive="${responsive}">Éditions</app:chemin>
        <app:chemin title="Bannette des éditions" responsive="${responsive}">Bannette des éditions</app:chemin>
    </app:chemins>

    <app:form action="composantedition/banette/flux.ex" formboxclass="eXtremeTable" formobjectname="editionsjasperform"
        responsive="${responsive}">
        <p>${msg}</p>

        <ec:table items="listedeseditions" tableId="editionsjasper" view="cphtml" var="row"
            action="${pageContext.request.contextPath}/composantedition/banette/flux.ex" state="persist" stateAttr="gotofirstpage"
            autoIncludeParameters="false" form="formulaire" locale="fr_FR" filterable="true" sortable="true" sortRowsCallback="cp"
            filterRowsCallback="cpfiltreinfsup" rowsDisplayed="10" showPagination="true" showNewPagination="true" newPaginationNbPage="5"
            showStatusBar="true" showPaginationBottom="false" showNewFilter="true" onInvokeAction="ecupdatectra('tableaunavigation')"
            libellenoresultsfound="Aucune édition n'a été trouvée" positionnoresultsfound="body">

            <ec:row highlightRow="true">
                <ec:column property="presentationEdition.description" alias="description" title="Edition" style="text-align: center"
                    headerStyle="text-align: center" sortable="true" filterable="true" showResponsive="${responsive}"
                    dataAttributes="data-tablesaw-priority=persist scope=col">
                    <app:link action="composantedition/banette/flux.ex" transition="voirEdition" attribute="editionUuid"
                        value="${row.editionUuid}" label="${row.presentationEdition.description}" active="${row.disponible}"
                        title="Voir l'édition" />
                </ec:column>

                <%--
                <ec:column property="ordoEdition.status"  alias="status" title="Etat"  width="10%" sortable="true" style="text-align: center"  headerStyle="text-align: center" > </ec:column>
                --%>
                <ec:column property="ordoEdition.status" alias="status" title="Etat" width="10%" style="text-align: center"
                    headerStyle="text-align: center" sortable="true" filterable="true" showResponsive="${responsive}"
                    dataAttributes="data-tablesaw-priority=1 scope=col" />

                <%--
                <ec:column property="ordoEdition.dateDemande"  alias="dateDemande" title="Date de la demande" cell="dateheureminute" sortable="true" style="text-align: center" headerStyle="text-align: center" > </ec:column>
                --%>
                <ec:column property="ordoEdition.dateDemande" alias="dateDemande" title="Date de la demande" cell="dateheureminute"
                    style="text-align: center" headerStyle="text-align: center" sortable="true" filterable="true"
                    showResponsive="${responsive}" dataAttributes="data-tablesaw-priority=2 scope=col" />

                <%--
                <ec:column property="archivageEdition.datePurge" alias="datePurge" title="Date de suppression prévue" cell="date" sortable="true" style="text-align: center" headerStyle="text-align: center" > </ec:column>
                --%>
                <ec:column property="archivageEdition.datePurge" alias="datePurge" title="Date de suppression prévue" cell="date"
                    style="text-align: center" headerStyle="text-align: center" sortable="true" filterable="true"
                    showResponsive="${responsive}" dataAttributes="data-tablesaw-priority=3 scope=col" />

                <%--
                <ec:column property="destinationEdition.editionDejaConsulteeParUid" alias="editionDejaConsulteeParUid" title="Déjà consultée ?"
                    filterCell="droplist" cell="booleanouinon"
                    style="text-align: center" headerStyle="text-align: center" > </ec:column>
                --%>
                <ec:column property="destinationEdition.editionDejaConsulteeParUid" alias="editionDejaConsulteeParUid"
                    title="Déjà consultée ?" filterCell="droplist" cell="booleanouinon" style="text-align: center"
                    headerStyle="text-align: center" sortable="true" filterable="true" showResponsive="${responsive}"
                    dataAttributes="data-tablesaw-priority=4 scope=col" />
            </ec:row>
        </ec:table>

        <app:boxboutons>
            <c:choose>
                <c:when test="${responsive != 'true'}">
                    <app:submit label="Rafraichir la bannette" transition="rafraichir" />
                    <app:submit label="Afficher toutes les éditions" transition="voirtouteseditions" />
                    <app:submit label="Retourner à l'accueil" transition="retour" />
                </c:when>
                <c:otherwise>
                    <app:submit label="Rafraichir la bannette" transition="rafraichir" responsive="${responsive}"
                         />
                    <app:submit label="Afficher toutes les éditions" transition="voirtouteseditions" responsive="${responsive}"
                         />
                    <app:submit label="Retourner à l'accueil" transition="retour" responsive="${responsive}"  />
                </c:otherwise>
            </c:choose>
        </app:boxboutons>
    </app:form>
    <app:disperror var="${erreurentreflux}" />
</app:page>