<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java" %>
<%@ include file="/WEB-INF/tags/html/includes.tagf" %>

<!--  Page erreuraccesrefuse.jsp -->

<app:page titreecran="Erreur" 
          titrecontainer="Erreur" 
          menu="true"
          bandeau="false">
          
    <app:disperrorfix  divid="popmessage">
            <p>Vous n'avez pas les droits suffisants pour accéder à cette page.</p>
    </app:disperrorfix>

    <%@ include file="/WEB-INF/pages/errors/rapporterreur.jspf" %>

</app:page>
