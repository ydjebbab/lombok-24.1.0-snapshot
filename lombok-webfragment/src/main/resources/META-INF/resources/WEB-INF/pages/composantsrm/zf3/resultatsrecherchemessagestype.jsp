<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/tags/html/includes.tagf" %>

<!--  Page resultatsrecherchemessagestype.jsp -->

<app:page titreecran="Selectionner un message type " 
          titrecontainer="Liste des messages types trouvés selon le  critére  saisi" 
          menu="true">

<app:form
   method="post" 	
	action="zf3/flux.ex" 
	 formboxclass="eXtremeTable">
<ec:table items="listmessagestype" tableId="rechmessty" view="cphtml"   showPagination="false"
  showStatusBar="false"  filterable="false"
  sortable="false"
  var="cont"   autoIncludeParameters="false" form="formulaire" 
locale="fr_FR" width="50%" theme="eXtremeTable centerTable"
		>
    
		<ec:row highlightRow="true">
			<ech:column property="chkbx" rowid="id" title=" " width="5%" filterable="false" sortable="false" viewsAllowed="cphtml"
			cell="cpcheckboxcell"></ech:column>
			<ec:column property="libelleMessageType" title="libelle du message" width="15%" sortable="true" filterable="true" > </ec:column>
			</ec:row>
</ec:table>

<app:boxboutons>
	<app:submit inputclass="annul" label="Annuler" transition="_eventId_Annuler"/>
	<app:submit label="Supprimer" transition="_eventId_Supprimer"/>	  
</app:boxboutons>
</app:form>
<app:disperror var="${erreurentreflux}" style="top: 40%"/>
</app:page>
