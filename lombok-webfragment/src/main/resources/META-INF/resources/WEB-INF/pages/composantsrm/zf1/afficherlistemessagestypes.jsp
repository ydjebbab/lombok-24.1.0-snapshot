<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/tags/html/includes.tagf" %>

<!--  Page afficherlistemessagestypes.jsp -->

<app:page titreecran="Gestion des messages bienvenue" 
		  titrecontainer="Selection d\'un message type " 
		  menu="true"> 
<app:form action="zf1/flux.ex">
<ec:table 
  items="listMessagesTypes" 
  autoIncludeParameters="false"
  showPagination="false"
  showStatusBar="false"
  filterable="false"
  sortable="false"
  locale="fr_FR"
  width="50%"
  form="test"
  theme="eXtremeTable" var="row">
  <ec:row>
  <ec:column property="libelleMessageType" title="libelle du message type">
  	<app:link action="zf1/flux.ex"
  			  transition="selection"
  			  attribute="libelleMessageType"
  			  value="${row.id}"
  			  label="${row.libelleMessageType}">
  </app:link>
  </ec:column>
   
  </ec:row>
</ec:table>	

<app:boxboutons>
	<app:submit inputclass="annul" label="Annuler" transition="_eventId_Annuler"/>
</app:boxboutons>
</app:form>
</app:page>
