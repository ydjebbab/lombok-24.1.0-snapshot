<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/tags/html/includes.tagf"%>

<!--  Page saisiedate.jsp -->

<c:set var="responsive" value="true" />

<app:page titreecran="Consulter les opérations journal par Web Service" titrecontainer="" menu="true" responsive="${responsive}">

    <app:chemins action="wsjournal/flux.ex" responsive="${responsive}">
        <app:chemin transition="annuler" title="Retour à l'accueil" responsive="${responsive}">Accueil</app:chemin>
        <app:chemin title="Divers" responsive="${responsive}">Divers</app:chemin>
        <app:chemin title="Consulter les opérations journal par Web Service" responsive="${responsive}">Consulter les opérations journal par Web Service</app:chemin>
    </app:chemins>

    <app:form method="post" action="wsjournal/flux.ex" formid="date" formobjectname="recherchelesoperationsjournalwsform"
        onsubmit="return submit_form_noenter(500)" formboxid="donnees" formboxclass="donnees sousonglets" responsive="${responsive}">

        <app:input attribut="valeurIdentifiantMetierObjOperations" libelle=" IdentifiantMetier  :" maxlength="20" size="20" readonly="false"
            requis="false" inputboxwidth="10%" responsive="${responsive}" />

        <div>
            <app:input attribut="dateDeRecherche" libelle="Date :" maxlength="10" size="10" readonly="false" requis="false"
                placeholder="JJ/MM/AAAA" inputboxwidth="10%" consigne="Veuillez saisir une date de la forme jj/mm/aaaa"
                onchange="addValidation( 'dateDeNaissance', 'dijit.form.DateTextBox','date incorrect','saisie obligatoire de la date','true','dd/MM/yyyy')"
                responsive="${responsive}">
            </app:input>
            <app:calendar dateouverture="01/01/1995" />
        </div>

        <app:boxboutons>
            <app:submit label="Retourner à l'accueil" title="Retourner à l'accueil" transition="retour" responsive="${responsive}" />
            <app:submit label="Rechercher" title="Rechercher" transition="rechercher" class="primary" responsive="${responsive}"
                inputclass="btn btn-default primary" />
        </app:boxboutons>

    </app:form>
</app:page>