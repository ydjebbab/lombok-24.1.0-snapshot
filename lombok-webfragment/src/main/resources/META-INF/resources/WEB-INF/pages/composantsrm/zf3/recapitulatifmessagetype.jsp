<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java" %>
<%@ include file="/WEB-INF/tags/html/includes.tagf" %>

<!--  Page recapitulatifmessagetype.jsp -->

<app:page titreecran="création des messages type bienvenue" 
		  titrecontainer="Récapitulatif du message type créé"
		  menu="true" 
		  titrebnd="Récapitulatif du message type ">

 
	
 <app:form 
	method="post" 	
	action="zf3/flux.ex" 
	formid="creationmessagetype" 
	formobjectname="creationmessagetypeform" 
	onsubmit="return submit_form(500)"
	formboxid="donnees"
	formboxclass="donnees sousonglets">

 	<c:set var="readonly" value="true"/>
 	
	<div>
	
	<fieldset><legend>Caractéristiques du message type créé</legend>
	 
 
 	<c:choose> 
        <c:when test="${fn:length(creationmessagetypeform.messagetypeform.libelleMessageType) < 150}">
           <app:textarea attribut="messagetypeform.libelleMessageType" libelle="Libelle :" cols="75" rows="2"  tabindex="2"   readonly="${readonly}"/>
    
        </c:when>
        <c:otherwise>
           <app:textarea attribut="messagetypeform.libelleMessageType" libelle="Libelle :" cols="75" rows="30"  tabindex="2"   readonly="${readonly}"/>
    
        </c:otherwise>
    </c:choose>  
	</fieldset>
 
	 
	 
 
</div>
 
	
	 
<app:boxboutons>	
	<app:submit label="Retour" transition="_eventId_retour"/>
</app:boxboutons>

</app:form>
</app:page>