<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java" %>
<%@ include file="/WEB-INF/tags/html/includes.tagf" %>

<!--  Page saisiemessagedesactiveprofil.jsp -->

<app:page titreecran="Saisie de messages bienvenue" 
		  titrecontainer=" "
		  menu="true" 
		  titrebnd="publication d'informations">

     <app:chemins action="zf2/flux.ex">
    <app:chemin action="accueil.ex" title="Retour à l'accueil">Accueil</app:chemin>
       <app:chemin>Saisie du message désactivant des profils</app:chemin>
</app:chemins>
	
 <app:form 
	method="post" 	
	action="zf2/flux.ex" 
	formid="creationmessage" 
	formobjectname="creationmessageform" 
	onsubmit="return submit_form(500)"
	formboxid="donnees"
	formboxclass="donnees sousonglets">

 	<c:set var="readonly" value="false"/>
	
 	<div>
 	<fieldset><legend>Caractéristiques du message </legend>
	<app:textarea attribut="message.libelleMessage" libelle="Libelle :" cols="75" rows="10"  tabindex="1" requis="false"   readonly="${readonly}" inputboxwidth="30%">
		 
	</app:textarea>
	 </fieldset>
	 
	 
	<fieldset><legend>Période d'affichage du message </legend>
	 
	<app:dateheure attribut="message.dateDebutMessage" libelle="Date de début :" readonly="${readonly}" requis="true"/>
	 
	<app:dateheure attribut="message.dateFinMessage" libelle="Date de fin :" readonly="${readonly}" requis="true"/>
	</fieldset>
	</div>  
<app:boxboutons>
	<app:submit inputclass="annul" label="Annuler" transition="_eventId_annuler"/>
	<app:submit label="Désactiver des profils" transition="_eventId_desactiverdesprofils"/>
</app:boxboutons>

</app:form>
</app:page>