<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java" %>
<%@ include file="/WEB-INF/tags/html/includes.tagf" %>

<!--  Page affichermessageamodifier.jsp -->

<app:page titreecran="Modification de messages bienvenue" 
		  titrecontainer=""
		  menu="true" 
		  titrebnd="publication d'informations">

  <app:chemins action="zf1/flux.ex">
    <app:chemin action="accueil.ex" title="Retour à l'accueil">Accueil</app:chemin>
       <app:chemin>Modification de messages</app:chemin>
</app:chemins>
	
 <app:form 
	method="post" 	
	action="zf1/flux.ex" 
	formid="creationmessage" 
	formobjectname="creationmessageform" 
	onsubmit="return submit_form(500)"
	formboxid="donnees"
	formboxclass="donnees sousonglets">

 	<c:set var="readonly" value="false"/>
	
 	<div>
 	<fieldset><legend>Caractéristiques du message</legend>
	 <c:choose> 
        <c:when test="${fn:length(creationmessageform.message.libelleMessage) < 150}">
           <app:textarea attribut="message.libelleMessage" libelle="Libelle :" cols="75" rows="2"  tabindex="2"   readonly="${readonly}"/>
    
        </c:when>
        <c:otherwise>
           <app:textarea attribut="message.libelleMessage" libelle="Libelle :" cols="75" rows="30"  tabindex="2"   readonly="${readonly}"/>
    
        </c:otherwise>
    </c:choose>	 
	 
	 
	 
	<app:radio libelle="Type de messages" pctlibelle="10%" itemsmap="${typemessage}" attribut="message.typeMessage"/>
	</fieldset>
	<fieldset><legend>Période d'affichage du message</legend>
		<app:dateheure attribut="message.dateDebutMessage" libelle="Date de début :" readonly="${readonly}" requis="true"/>
	 
	<app:dateheure attribut="message.dateFinMessage" libelle="Date de fin :" readonly="${readonly}" requis="true"/>
	</fieldset>
	</div>  
<app:boxboutons>
	<app:submit inputclass="annul" label="Annuler" transition="_eventId_annuler"/>
	<app:submit label="Valider" transition="_eventId_valider"/>	 
</app:boxboutons>

</app:form>
</app:page>