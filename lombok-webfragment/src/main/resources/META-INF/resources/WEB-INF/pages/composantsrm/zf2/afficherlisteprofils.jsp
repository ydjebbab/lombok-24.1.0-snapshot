<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/tags/html/includes.tagf" %>

<!--  Page afficherlisteprofils.jsp -->

<app:page titreecran="Gestion des messages bienvenue" 
		  titrecontainer="" 
		  menu="true"> 
		  
  <app:chemins action="zf2/flux.ex">
    <app:chemin action="accueil.ex" title="Retour à l'accueil">Accueil</app:chemin>
       <app:chemin>Désactivation de profils</app:chemin>
</app:chemins>
<app:form   method="post" 	
	action="zf2/flux.ex" 
	formid="creationmessage" 
	formobjectname="creationmessageform" 
	onsubmit="return submit_form(500)"
	formboxid="donnees"
	formboxclass="donnees sousonglets">
 <fieldset><legend>Choisissez un ou plusieurs profils à désactiver dans la liste</legend>
<ec:table 
  items="listProfils"  var="cont"
  action="${pageContext.request.contextPath}/zf1/flux.ex"
  tableId="creepro"
  autoIncludeParameters="false"
  showPagination="false"
  showStatusBar="false"
  filterable="false"
  sortable="false"
  locale="fr_FR"
  width="50%"
  form="creationmessage"	
  theme="eXtremeTable"	
  view="cphtml"
  >		
  <ec:row>
  <ech:column property="chkbx"   title=" " rowid="hashcode" width="5%" filterable="false" sortable="false" viewsAllowed="cphtml"
			cell="cpcheckboxcell"></ech:column>
  <ec:column property="libelleProfil" title="libellé du profil"/>
  <ec:column property="codeProfil" title="code profil"/>
   
  </ec:row>
</ec:table>	
</fieldset>
	<fieldset><legend>Période de désactivation du profil</legend>
	<app:dateheure attribut="message.dateDebutInactiviteProfil" libelle="Date de début :" readonly="${readonly}" requis="true"/>
	 
	<app:dateheure attribut="message.dateFinInactiviteProfil" libelle="Date de fin :" readonly="${readonly}" requis="true"/>
	
	</fieldset>
<app:boxboutons>
	<app:submit inputclass="annul" label="Annuler" transition="_eventId_annuler"/>
	<app:submit label="Valider" transition="_eventId_valider"/>
</app:boxboutons>

</app:form>
</app:page>
