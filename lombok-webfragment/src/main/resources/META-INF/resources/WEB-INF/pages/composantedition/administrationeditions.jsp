<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/tags/html/includes.tagf"%>

<!--  Page administrationeditions.jsp -->

<c:set var="responsive" value="true" />

<app:page titreecran="Administration des éditions" menu="true" responsive="${responsive}">

    <app:chemins action="composantedition/admin/flux.ex" responsive="${responsive}">
        <app:chemin transition="fin" title="Retour à l'accueil" responsive="${responsive}">Accueil</app:chemin>
        <app:chemin title="Éditions" responsive="${responsive}">Éditions</app:chemin>
        <app:chemin title="Administration" responsive="${responsive}">Administration</app:chemin>
        <app:chemin title="Administration des éditions" transition="retourecranrecherche" responsive="${responsive}">Administration des éditions</app:chemin>
        <app:chemin title="Liste des éditions sélectionnées" responsive="${responsive}">Liste des éditions sélectionnées</app:chemin>
    </app:chemins>

    <%--
<app:form
          action="composantedition/admin/flux.ex"
          formboxclass="eXtremeTable"
          formobjectname="administrationeditionsform"
          >
 --%>
    <app:form action="composantedition/admin/flux.ex" formboxclass="eXtremeTable" formobjectname="administrationeditionsform"
        responsive="${responsive}">


        <%--
        <ec:table items="adminlistedeseditions"
                action="${pageContext.request.contextPath}/composantedition/admin/flux.ex"
                tableId="adminlistedeseditions" state="persist"
                autoIncludeParameters="false"
                form="formulaire"
                locale="fr_FR"
                view="cphtml"
                filterable="true"
                showPagination="true" showExports="true"
                var="row"
                sortRowsCallback="cp" filterRowsCallback="cp"
                >
        --%>


        <ec:table items="adminlistedeseditions" tableId="adminlistedeseditions" view="cphtml" var="row"
            action="${pageContext.request.contextPath}/composantedition/admin/flux.ex" state="persist" stateAttr="gotofirstpage"
            autoIncludeParameters="false" form="formulaire" locale="fr_FR" filterable="true" sortable="true" sortRowsCallback="cp"
            filterRowsCallback="cp" rowsDisplayed="10" showPagination="true" showNewPagination="true" newPaginationNbPage="5"
            showStatusBar="true" showNewFilter="true" onInvokeAction="ecupdatectra('tableaunavigation')"
            libellenoresultsfound="Aucune personne ne correspond à vos criteres de recherche" positionnoresultsfound="body">

            <ec:row highlightRow="true">
                <%--
                <ech:column property="chkbx" cell="cpcheckboxcell" rowid="id" viewsAllowed="cphtml"
                    headerCell="cpselectAll" width="5%" filterable="false" sortable="false"  />
                <ec:column property="presentationEdition.description" alias="description" title="Edition" sortable="true" 
                style="text-align: center" headerStyle="text-align: center" >        
                <ec:column property="ordoEdition.status" alias="status" title="Etat" width="10%" sortable="true" 
                    style="text-align: center" headerStyle="text-align: center" > </ec:column>
                <ec:column property="ordoEdition.dateDemande" alias="dateDemande" title="Date de la demande" cell="dateheureminute" 
                    sortable="true" style="text-align: center" headerStyle="text-align: center" > </ec:column>
                <ec:column property="archivageEdition.datePurge" alias="datePurge" title="Date de suppression prévue" cell="date" 
                    sortable="true" style="text-align: center" headerStyle="text-align: center" />                
                --%>
                <ech:column property="chkbx" cell="cpcheckboxcell" viewsAllowed="cphtml" headerCell="cpselectAll" sortable="false"
                    filterable="false" showResponsive="${responsive}" dataAttributes="data-tablesaw-priority=persist scope=col" />
                <ec:column property="presentationEdition.description" alias="description" title="Edition" style="text-align: center"
                    headerStyle="text-align: center" sortable="true" filterable="true" showResponsive="${responsive}"
                    dataAttributes="data-tablesaw-priority=persist scope=col">
                    <app:link action="composantedition/admin/flux.ex" transition="voircontenuedition" attribute="editionUuid"
                        value="${row.editionUuid}" label="${row.presentationEdition.description}" active="${row.disponible}"
                        title="Voir le contenu de l'édition" />
                </ec:column>
                <ec:column property="ordoEdition.status" alias="status" title="Etat" width="10%" style="text-align: center"
                    headerStyle="text-align: center" sortable="true" filterable="true" showResponsive="${responsive}"
                    dataAttributes="data-tablesaw-priority=1 scope=col" />
                <ec:column property="ordoEdition.dateDemande" alias="dateDemande" title="Date de la demande" cell="dateheureminute"
                    style="text-align: center" headerStyle="text-align: center" sortable="true" filterable="true"
                    showResponsive="${responsive}" dataAttributes="data-tablesaw-priority=2 scope=col" />
                <ec:column property="archivageEdition.datePurge" alias="datePurge" title="Date de suppression prévue" cell="date"
                    style="text-align: center" headerStyle="text-align: center" sortable="true" filterable="true"
                    showResponsive="${responsive}" dataAttributes="data-tablesaw-priority=3 scope=col" />
            </ec:row>
        </ec:table>

        <app:boxboutons>
            <c:choose>
                <c:when test="${responsive != 'true'}">
                    <app:submit label="Détail d'une édition" title="Détail d'une édition" transition="detaileditionidentification" />
                    <app:submit label="Supprimer les éditions sélectionnées" title="Supprimer les éditions sélectionnées"
                        transition="supprimer" />
                    <app:submit label="Retourner à l'écran de recherche" title="Retourner à l'écran de recherche"
                        transition="retourecranrecherche" />
                </c:when>
                <c:otherwise>
                    <app:submit label="Détail d'une édition" title="Détail d'une édition" transition="detaileditionidentification"
                        responsive="${responsive}" />
                    <app:submit label="Supprimer les éditions sélectionnées" title="Supprimer les éditions sélectionnées"
                        transition="supprimer" responsive="${responsive}" />
                    <app:submit label="Retourner à l'écran de recherche" title="Retourner à l'écran de recherche"
                        transition="retourecranrecherche" responsive="${responsive}" />
                </c:otherwise>
            </c:choose>
        </app:boxboutons>
    </app:form>
    <app:disperror var="${erreurentreflux}" />
</app:page>