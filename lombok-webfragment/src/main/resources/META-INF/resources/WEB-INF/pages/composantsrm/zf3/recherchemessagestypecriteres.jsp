<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/tags/html/includes.tagf" %>

<!--  Page recherchemessagestypecriteres.jsp -->

<app:page titreecran="Selectionner un message type" 
          titrecontainer="Recherche de messages type" 
          menu="true" >          

 

<app:form 
	method="post" 
	action="zf3/flux.ex" 
	formid="recherchemessagestypes" 
	formobjectname="recherchemessagestypesform" 
	onsubmit="return submit_form(500)"
	formboxid="donnees"
	formboxclass="donnees sousonglets">
		
	<fieldset><legend>Veuillez saisir une partie du libellé que vous recherchez (ex :ess pour message) ou ne rien saisir pour tous les consulter</legend>	
 
 	<app:textarea attribut="libelleMessageType" libelle="Libelle :" cols="20" rows="10"  tabindex="1"   readonly="${readonly}" inputboxwidth="30%">
	</app:textarea>
	
	
 
	</fieldset>
<app:boxboutons>
	<app:submit inputclass="annul" label="Annuler" transition="_eventId_annuler"/>
	<app:submit label="Rechercher" transition="_eventId_rechercher" id="enterButton"/>
</app:boxboutons>

</app:form>
</app:page>