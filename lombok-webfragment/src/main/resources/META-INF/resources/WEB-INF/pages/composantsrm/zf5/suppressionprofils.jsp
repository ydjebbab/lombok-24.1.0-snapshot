<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/tags/html/includes.tagf" %>

<!--  Page suppressionprofils.jsp -->

<app:page titreecran="Gérer les profils" >

<app:chemins action="zf5/flux.ex">
    <app:chemin action="accueil.ex" title="Retour à l'accueil">Accueil</app:chemin>
    <app:chemin>Suppression de profils</app:chemin>
</app:chemins>

<app:form 
          action="zf5/flux.ex"
          formboxclass="eXtremeTable"
          formobjectname="gestionprofilform"
          >
<spring:bind path="gestionprofilform.profil1">
<ec:table items="listprofils" 
        action="${pageContext.request.contextPath}/zf5/flux.ex" 
        title="liste des profils"
        tableId="listprofils" state="persist"
        autoIncludeParameters="false" 
        form="tableform"
        locale="fr_FR" 
        view="cphtml"
        filterable="true"   
        showPagination="true" showExports="true" 
        var="row" 
        sortRowsCallback="cp"  
        >
        
        
    
     
     
        
        <ec:parameter name="_eventId_tableaunavigation" value="tableaunavigation"/> 
        <ec:row highlightRow="true"  >
            <ech:column property="chkbx" cell="cpcheckboxcell" rowid="id" viewsAllowed="cphtml"
                headerCell="cpselectAll" width="5%" filterable="false" sortable="false"  />
            <ec:column property="codeProfil" title="Code" width="20%" sortable="true"  filterable="true" style="text-align: center" headerStyle="text-align: center"
             />  
            <ec:column property="libelleProfil"  title="Libellé"  width="20%" sortable="true" style="text-align: right" headerStyle="text-align: center" cell=""      
             /> 
             
            
            
        </ec:row>
</ec:table>
<p><br/></p>
</spring:bind>
<app:boxboutons>    
    <app:submit label="Supprimer" transition="suppressionprofils"/>
    <app:submit label="Retour" transition="annuler"/>    
</app:boxboutons>
</app:form>
<app:disperror var="${erreurentreflux}" />
</app:page>