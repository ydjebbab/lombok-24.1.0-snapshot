<%@ page contentType="text/html;charset=UTF-8" 
         pageEncoding="UTF-8" 
         language="java" 
         %>
<%@ include file="/WEB-INF/tags/html/includes.tagf" %>

<!--  Page erreurmaxupload.jsp -->

<app:page titreecran="Erreur"  theme="popupcontainer" bandeau="false" menu="false">
<app:disperrorfix  divid="popmessage" >
	<p>Le transfert du fichier a été interrompu.</p>
	<p>Les fichiers de taille supérieure à 10 Mo ne peuvent pas être transférés.</p>
</app:disperrorfix>
</app:page>