<c:forEach items="${flowRequestContext.messageContext.allMessages}" var="message">
    <c:if test="${message.severity eq 'ERROR'}">
       <div class="error-msg"><c:out value="${message.text}"/></div>
    </c:if>
    <c:if test="${message.severity eq 'INFO'}">
       <div class="info-msg"><c:out value="${message.text}"/></div>
    </c:if>
    <c:if test="${message.severity eq 'WARNING'}">
       <div class="warring-msg"><c:out value="${message.text}"/></div>
    </c:if>
</c:forEach>