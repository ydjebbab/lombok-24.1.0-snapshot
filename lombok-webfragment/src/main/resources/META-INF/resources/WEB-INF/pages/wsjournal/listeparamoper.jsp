<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/tags/html/includes.tagf"%>

<!--  Page listeparamoper.jsp -->

<c:set var="responsive" value="true" />

<app:page titreecran="Paramètres d'une opération du journal d'activité" responsive="${responsive}">

    <app:chemins action="wsjournal/flux.ex" responsive="${responsive}">
        <app:chemin transition="annuler" title="Retour à l'accueil" responsive="${responsive}">Accueil</app:chemin>
        <app:chemin title="Divers" responsive="${responsive}">Divers</app:chemin>
        <app:chemin title="Consulter les opérations journal par Web Service" transition="saisiedate" responsive="${responsive}">Consulter les opérations journal par Web Service</app:chemin>
        <app:chemin title="Opérations du journal d'activité" transition="listeoperations" responsive="${responsive}">Opérations du journal d'activité</app:chemin>
        <app:chemin title="Paramètres d'une opération du journal d'activité" responsive="${responsive}">Paramètres d'une opération du journal d'activité</app:chemin>
    </app:chemins>

    <app:form action="wsjournal/flux.ex" formboxclass="eXtremeTable">

        <ec:table items="lesParamOperation" var="cont" autoIncludeParameters="false" form="formulaire" filterable="false" sortable="false"
            showPagination="false" showStatusBar="false" locale="fr_FR" theme="eXtremeTable" view="cphtml">

            <ec:row>
                <ec:column property="nom" title="Nom du paramètre" />
                <ec:column property="valeur" title="Valeur du paramètre" />
            </ec:row>
        </ec:table>

        <app:boxboutons>
            <app:submit label="Page précédente" transition="listeoperations" />
            <app:submit label="Retourner à l'accueil" transition="retour" />
        </app:boxboutons>
    </app:form>
</app:page>
