<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/tags/html/includes.tagf"%>

<!--  Page actuator.jsp -->

<c:set var="responsive" value="true" />

<app:page titreecran="Liste des endpoints" titrecontainer="Liste des endpoints" menu="true" responsive="${responsive}">

    <app:chemins action='zf2/flux.ex' responsive="${responsive}">
        <app:chemin action="accueil.ex" title="Retour à  l'accueil" responsive="${responsive}">Accueil</app:chemin>
        <app:chemin title="Métriques" responsive="${responsive}">Métriques</app:chemin>
        <app:chemin title="Actuator" responsive="${responsive}">Actuator</app:chemin>
    </app:chemins>    

    <fieldset>
        <ul>
        <li><a href="${pageContext.request.contextPath}/health" target="_blank">/health</a>
        Ce endpoint ne nécessite pas de droits.  Fournit des informations sur la santé de l'application.
        </li>        
        
        <li>
        <a href="${pageContext.request.contextPath}/env" target="_blank">/env</a>
        Affiche toutes les variables d'environnement et donc les valeurs des propriétés présentes dans application.properties.
        Nécessite d'avoir un rôle autorisé.
        </li>
        
        <li>
        <a href="${pageContext.request.contextPath}/metrics" target="_blank">/metrics</a>
        Nécessite d'avoir un rôle autorisé.
        Mémoires utilisées, données liées à la JVM, données sur tomcat, URI appelées et leurs statistiques, etc.
        </li>
        
        <li>
        <a href="${pageContext.request.contextPath}/trace" target="_blank">/trace</a>
        Affiche les informations de traces (par défaut les 100 dernières requêtes HTTP)
        </li>
        
        <li>
        <a href="${pageContext.request.contextPath}/beans" target="_blank">/beans</a>
        Liste des beans créés par la BeanFactory, ainsi que leur configuration.
        </li>
        
        <li>
        <a href="${pageContext.request.contextPath}/auditevents" target="_blank">/auditevents</a>
        Fournit des informations sur les évènements d'audit de l'application.
        </li>
        
        <li>
        <a href="${pageContext.request.contextPath}/autoconfig" target="_blank">/autoconfig</a>
        Affiche un rapport sur l'auto-configuration qui montre tous les candidats à l'auto-configuration
        </li>
        
        <li>
        <a href="${pageContext.request.contextPath}/loggers" target="_blank">/loggers</a>
        Montre la configuration des loggers de l'application.
        </li>
        
        <li>
        <a href="${pageContext.request.contextPath}/mappings" target="_blank">/mappings</a>
        Informations sur les mappings de requêtes. 
        </li>
        </ul>
   </fieldset>

   <app:form action="metriques/flux.ex" formboxclass="donnees sousonglets" responsive="${responsive}"> 
       <app:boxboutons  divboxboutonsclass="boxboutons">            
           <app:submit label="Retourner à l'accueil"  transition="RetourAccueil" id="enterButton" responsive="${responsive}" />
       </app:boxboutons>
   </app:form> 
   
</app:page>
