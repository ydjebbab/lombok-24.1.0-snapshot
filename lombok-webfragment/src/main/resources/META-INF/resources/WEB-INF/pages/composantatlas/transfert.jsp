<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/tags/html/includes.tagf" %>
<app:page titreecran="Transférer un fichier" bandeau="false" menu="false" >

<!--  Page transfert.jsp -->

<script type="text/javascript">
 			/*<![CDATA[*/
 			function  validertransfertetfermer()
 				{
 				if (window.opener && ! window.opener.closed)
 					{
					var docactif=window.opener.document;
					doit_on_other_doc(docactif,"flux.ex?${_csrf.parameterName}=${_csrf.token}","validertransfert") ;
					self.close();
					}	
				}
				
				function annulertransfertetfermer()
 				{
 				if (window.opener && ! window.opener.closed)
 					{
					var docactif=window.opener.document;
					doit_on_other_doc(docactif,"flux.ex?${_csrf.parameterName}=${_csrf.token}","annulertransfert") ;
					self.close();
					}	
				}
			/*]]>*/
	</script>

<app:form action="composantatlas/flux.ex?${_csrf.parameterName}=${_csrf.token}"
          formobjectname="ajoutdocumentatlasform" method="post"  enctype="multipart/form-data">

<p><br/></p>
<div>
<fieldset>
<p>1. Cliquez sur Parcourir pour sélectionner le fichier que vous souhaitez transférer.
      <b>(La taille du fichier doit être inférieure à <fmt:formatNumber value="${uploadmaxsize/1000000}"/> Mo)</b></p>
<app:file  attribut="fichierJoint" libelle="Fichier" maxlength="50" size="50"  
            labelboxwidth="7%" inputboxwidth="60%" compboxwidth="15%" requis="false"
            readonly="true">
</app:file>
<p><br/></p>
<p><br/>2. Cliquez sur Transférer
<app:submit label="Transferer" transition="transferer" disabled="true"/>
</p>
<c:if test="${! empty  ajoutdocumentatlasform.fichierJoint}">
<fieldset><legend>Fichier transféré</legend>
<app:textformate libelle="Fichier" attribut="fichierJoint.nomFichierOriginal" maxlength="50" size="50"/>
<app:textformate libelle="Taille" attribut="fichierJoint.tailleFichier" maxlength="50" size="50"/>
<app:textformate libelle="Transféré le " attribut="fichierJoint.dateHeureSoumission" maxlength="50" size="50"/>
</fieldset>
</c:if>
<p>3. Cliquez sur Valider pour accepter le transfert 
<app:button onclick="validertransfertetfermer();" >Valider</app:button>
</p>
<p>4. ou Cliquez sur Annuler pour annuler le transfert 
<app:button onclick="annulertransfertetfermer();" >Annuler</app:button>
</p>
</fieldset>
</div>
</app:form>
</app:page>