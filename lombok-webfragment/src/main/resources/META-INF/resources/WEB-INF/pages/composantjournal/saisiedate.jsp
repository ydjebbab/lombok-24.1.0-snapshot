<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/tags/html/includes.tagf"%>

<!--  Page saisiedate.jsp -->

<c:set var="responsive" value="true" />

<app:page titreecran="Consulter les opérations au journal" titrecontainer="" menu="true" responsive="${responsive}">

    <app:chemins action="composantjournal/flux.ex" responsive="${responsive}">
        <app:chemin action="accueil.ex" title="Retour à l'accueil" responsive="${responsive}">Accueil</app:chemin>
        <app:chemin title="Divers" responsive="${responsive}">Divers</app:chemin>
        <app:chemin title="Consulter les opérations au journal" responsive="${responsive}">Consulter les opérations au journal</app:chemin>
    </app:chemins>

    <app:form method="post" action="composantjournal/flux.ex" formid="date" formobjectname="recherchelesoperationsjournalform"
        onsubmit="return submit_form_noenter(500)" formboxid="donnees" formboxclass="donnees sousonglets" responsive="${responsive}">

        <div>
            <app:input attribut="valeurIdentifiantMetierObjOperations" libelle=" IdentifiantMetier  :" maxlength="20" size="20"
                readonly="false" requis="false" inputboxwidth="10%" responsive="${responsive}">
            </app:input>
        </div>

        <%--
        <app:input attribut="dateDeRecherche" libelle="Date :" maxlength="10" size="10" readonly="false" requis="false" inputboxwidth="10%"
            consigne="Veuillez saisir une date de la forme jj/mm/aaaa" responsive="${responsive}">
            <app:calendar responsive="${responsive}"/>
        </app:input>
        --%>
        <div>
            <app:input attribut="dateDeRecherche" libelle="Date :" maxlength="10" size="10" requis="false" readonly="false"
                inputboxwidth="40%" placeholder="JJ/MM/AAAA" responsive="${responsive}">
                <app:calendar />
            </app:input>
        </div>

        <c:choose>
            <c:when test="${responsive != 'true'}">
                <app:boxboutons>
                    <app:submit label="Retourner à l'accueil" transition="retour" title="Retourner à l'accueil" />
                    <app:submit label="Rechercher l'opération" transition="rechercher" id="enterButton" title="Rechercher l'opération" />
                </app:boxboutons>
            </c:when>
            <c:otherwise>
                <app:boxboutons>
                    <app:submit label="Retourner à l'accueil" transition="retour" title="Retourner à l'accueil" responsive="${responsive}" />
                    <app:submit label="Rechercher l'opération" transition="rechercher" id="enterButton" responsive="${responsive}"
                        inputclass="primary btn btn-default" title="Rechercher l'opération" />
                </app:boxboutons>
            </c:otherwise>
        </c:choose>

    </app:form>
</app:page>