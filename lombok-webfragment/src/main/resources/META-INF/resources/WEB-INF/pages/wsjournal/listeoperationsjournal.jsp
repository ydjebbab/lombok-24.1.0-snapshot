<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java"%>
<%@ include file="/WEB-INF/tags/html/includes.tagf"%>

<!--  Page listeoperationsjournal.jsp -->

<c:set var="responsive" value="true" />

<app:page titreecran="Opérations du journal d'activité" titrecontainer="" menu="true" responsive="${responsive}">

    <app:chemins action="wsjournal/flux.ex" responsive="${responsive}">
        <app:chemin transition="annuler" title="Retour à l'accueil" responsive="${responsive}">Accueil</app:chemin>
        <app:chemin title="Divers" responsive="${responsive}">Divers</app:chemin>
        <app:chemin title="Consulter les opérations journal par Web Service" transition="saisiedate" responsive="${responsive}">Consulter les opérations journal par Web Service</app:chemin>
        <app:chemin title="Opérations du journal d'activité" responsive="${responsive}">Opérations du journal d'activité</app:chemin>
    </app:chemins>

    <app:form action="wsjournal/flux.ex" formboxclass="eXtremeTable" formobjectname="recherchelesoperationsjournalwsform">

        <ec:table items="listeoperationsjournal" action="${pageContext.request.contextPath}/wsjournal/flux.ex" tableId="opjour"
            autoIncludeParameters="false" form="formulaire" locale="fr_FR" view="cphtml" filterable="true" sortable="true"
            showPagination="false" showExports="true" var="row" sortRowsCallback="cp" filterRowsCallback="cp">

            <ec:row highlightRow="true">
                <ec:column property="id" title="Identifiant">
                    <app:link action="wsjournal/flux.ex" transition="afficher" attribute="id" value="${row.id}" label="${row.id}"
                        active="true" title="Consulter les paramétres opération" />
                </ec:column>
                <ec:column property="dateHeureOperation" title="Date et heure de l'opération" cell="dateheureminuteseconde" />
                <ec:column property="identifiantUtilisateurOuBatch" title="identifiant de l'utilisateur ou du batch" />
                <ec:column property="natureOperation" title="Nature de l'opération" />
                <ec:column property="typeDureeDeConservation" title="Type de la durée de conservation" />

            </ec:row>
        </ec:table>

        <app:boxboutons>
            <app:submit label="Retourner à l'accueil" transition="retour" />
            <app:submit label="Page précédente" transition="saisiedate" />
        </app:boxboutons>
    </app:form>
</app:page>

