<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java" %>
<%@ include file="/WEB-INF/tags/html/includes.tagf" %>

<!--  Page erreurnavigation.jsp -->

<app:page titreecran="Erreur" 
		  titrecontainer="Erreur de navigation" 
		  menu="true" >

<app:disperrorfix  divid="popmessage" >
			<p>Une erreur de navigation s'est produite.</p>
			<p>Vous avez essayé de soumettre deux fois la même demande</p>
			<p>Vous avez peut-être :</p>
			<ul>
				<li>utilisé les touches "ACTUALISER","SUIVANT" ou "PRECEDENT" de l'explorateur</li>
				<li>essayé de raffraichir la page avec la touches "F5"</li>		
			</ul>
			<p>Message à transmettre au support si le problème se répète :</p>
			<p><c:out value="${messageexception}"/></p>
</app:disperrorfix>

</app:page>
