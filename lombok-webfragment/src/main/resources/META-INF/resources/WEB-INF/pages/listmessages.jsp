<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/tags/html/includes.tagf" %>

<!--  Page listmessages.jsp -->

<app:page titreecran="Bienvenue" 
          titrecontainer="Merci de prendre connaissance des messages suivants"
          menu="false" 
          donneescontainerboxclass="informationcontainer">
    
    <img data-selector="true" title="Image Sireme." alt="Image Sireme." src="composants/srm/sireme.gif" width="150" height="75"  />
     <app:form action="/accueil.ex">
     
     <c:forEach var="item" items="${listmessages}">
    
        <p>  ${item.libelleMessage}</p>
        </c:forEach>
         
        <app:boxboutons>
            <app:submit label="Valider" transition="ok" id="enterButton"/>
        </app:boxboutons>
         
    </app:form>
    
    
</app:page>