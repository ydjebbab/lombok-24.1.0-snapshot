<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/tags/html/includes.tagf" %>

<!--  Page afficherresultatssuppressionmessagetype.jsp -->

<app:page titreecran="Selectionner un message type"
          titrecontainer="Liste des messages type supprimés" 
          menu="true" 
          donneescontainerboxclass="tablecontainer">
          
<p class="titretableau">Les données suivantes ont été supprimées</p>
<app:form method='POST' 
          action='zf3/flux.ex'
          formid='recherchemessagestypeform'>
 
         
<ec:table items="listmessagestypesel"   form="recherchemessagestypeform" var="cont" 
		autoIncludeParameters="false"
		filterable="false" 
		sortable="false"
		showPagination="false"
		showStatusBar="false"
		locale="fr_FR"
		theme="eXtremeTable centerTable"
		width="50%"  
	>
	<ec:row highlightRow="false">
			<ec:column property="libelleMessageType" title="libelle du message" width="15%" sortable="true" filterable="true" > </ec:column>
		 
 		</ec:row>
</ec:table>
 
<app:boxboutons>
	<app:submit label="Retour" transition="_eventId_Retour"/>
</app:boxboutons>

</app:form>
</app:page>
