<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/tags/html/includes.tagf"%>

<!--  Page arreterunepurgeedition.jsp -->

<app:page titreecran="Administration des traces de purges des éditions">

    <app:chemins action="composantedition/admin/flux.ex">
        <app:chemin action="accueil.ex" title="Retour à l'accueil">Accueil</app:chemin>
        <app:chemin title="Éditions" responsive="${responsive}">Éditions</app:chemin>
        <app:chemin title="Administration" responsive="${responsive}">Administration</app:chemin>
        <app:chemin title="Administration des traces de purges d'éditions" transition="retourecranrecherche" responsive="${responsive}">Administration des traces de purges d'éditions</app:chemin>
        <app:chemin title="Demander l'arrêt d'une tâche de purge des éditions" responsive="${responsive}">Demander l'arrêt d'une tâche de purge des éditions</app:chemin>
    </app:chemins>


    <p>La demande d'arrêt d'une tâche de purge en cours d'exécution indique au batch qu'il doit stopper son exécution dès que possible.
        Si pour une raison quelconque (par exemple arrêt de la jvm par un kill du processus en exploitation) le batch n'est plus en
        exécution alors que sa trace indique qu'il est toujours en cours, le batch ne pourra modifier son état avant de s'interrompre.
        L'état d'une trace marquée "ARRET_DEMANDE" est passé à batch</p>


    <app:form action="composantedition/admin/flux.ex" formboxclass="eXtremeTable" formobjectname="administrationpurgeseditionsform">

        <ec:table items="tracedepurge" action="${pageContext.request.contextPath}/composantedition/admin/flux.ex"
            tableId="tracedepurgetracedepurge" autoIncludeParameters="false" form="formulaire" locale="fr_FR" view="cphtml"
            filterable="false" showPagination="false" showExports="false" var="row" sortRowsCallback="cp" filterRowsCallback="cp">
            <ec:row highlightRow="true">
                <ec:column property="dateDebutPurge" alias="dateDebutPurge" title="Début de la purge" sortable="true"
                    style="text-align: center" headerStyle="text-align: center" cell="dateheureminute" />
                <ec:column property="dateFinPurge" alias="dateFinPurge" title="Fin de la purge" width="10%" sortable="true"
                    style="text-align: center" headerStyle="text-align: center" cell="dateheureminute" />
                <ec:column property="etatPurge" alias="etatPurge" title="Etat de la purge" sortable="true" style="text-align: center"
                    headerStyle="text-align: center" />
                <ec:column property="nbEditionsSupprimees" alias="nbEditionsSupprimees" title="Nombre d'éditions supprimées" sortable="true"
                    style="text-align: right" headerStyle="text-align: center" />
                <ec:column property="messageErreur" alias="messageErreur" title="Message d'erreur" sortable="true" style="text-align: right"
                    headerStyle="text-align: center" />
            </ec:row>
        </ec:table>
        <p>
            <br>
        </p>

        <%--         <app:boxboutons> --%>
        <%--             <app:submit label="Annuler" transition="retourlistepurges" /> --%>
        <%--             <app:submit label="Confirmer la demande d'arrêt" transition="confirmerarretpurge" /> --%>
        <%--         </app:boxboutons> --%>


        <c:choose>
            <c:when test="${responsive != 'true'}">
                <app:boxboutons>
                    <app:submit label="Retourner à la liste des purges" transition="retourlistepurges"
                        title="Retourner à la liste des purges" />
                    <app:submit label="Confirmer la demande d'arrêt" transition="confirmerarretpurge" id="enterButton"
                        title="Confirmer la demande d'arrêt" />
                </app:boxboutons>
            </c:when>
            <c:otherwise>
                <app:boxboutons>
                    <app:submit label="Retourner à la liste des purges" transition="retourlistepurges"
                        title="Retourner à la liste des purges" responsive="${responsive}" />
                    <app:submit label="Confirmer la demande d'arrêt" transition="confirmerarretpurge" id="enterButton"
                        responsive="${responsive}" inputclass="primary btn btn-default" title="Confirmer la demande d'arrêt" />
                </app:boxboutons>
            </c:otherwise>
        </c:choose>
    </app:form>
</app:page>