<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/tags/html/includes.tagf"%>

<!--  Page plansite.jsp -->

<c:set var="responsive" value="true" />

<app:page titreecran="Plan du site" menu="true" responsive="${responsive}">
        <ul>
            <li>Contrats</li>
            <ul>
                <li>Adhérer</li>
                <ul>
                    <li>Prélèvement à l'échéance</li>
                    <li>Prélèvement mensuel</a></li>
                    <ul>
                        <li><a href="dmo/zf1/flux.ex?_flowId=demandeadhesionmensualisation-flow">Immédiat</a></li>
                        <li><a href="dmo/zf1/flux.ex?_flowId=demandeadhesionmensualisation-flow">Après imposition</a></li>
                    </ul>
                </ul>
            </ul>
            <li>Contribuables</li>
            <ul>
                <li>Gestion des contribuables</li>
                <ul>
                    <li><a href="dmo/zf2/flux.ex?_flowId=creationcontribuable-flow">Ajouter un contribuable</a></li>
                    <li><a href="dmo/zf2/flux.ex?_flowId=recherchecontribuable-flow">Rechercher un contribuable</a></li>
                    <li><a href="dmo/zf2/flux.ex?_flowId=recherchecontribuable-flow">Supprimer un contribuable</a></li>
                    <li><a href="dmo/zf2/flux.ex?_flowId=recherchecontribuable-flow">Modifier un contribuable</a></li>
                    <li><a href="dmo/zf2/flux.ex?_flowId=editionlistecontribuables-flow">Édition liste des contribuables</a></li>
                </ul>
                <li><a href="dmo/zf3/flux.ex?_flowId=statistiques-flow">Tableaux et éditions statistiques</a></li>
            </ul>
            <li>Références</li>
            <ul>
                <li><a href="dmo/zf7/flux.ex?_flowId=referencetypesimpots-flow">Gestion des types d'impôts</a></li>
                <li><a href="dmo/zf7/flux.ex?_flowId=referencetypesimpotspouredition-flow">Exemple d'édition de types d'impôt</a></li>
                <li><a href="dmo/zf9/flux.ex?_flowId=recherchelibellestructure-flow">Recherche de structures CP (Nominoe) par
                        codique</a></li>
                <li><a href="dmo/zf9/flux.ex?_flowId=consultationstructures-flow">Consultation des structures CP (Nominoe) via un
                        arbre</a></li>
                <li><a href="dmo/zf9/flux.ex?_flowId=rechercheguichetbancaire-flow">Recherche de guichet bancaire (Tabank) par rib</a></li>
            </ul>
            <li>Éditions</li>
            <ul>
                <li><a href="dmo/composantedition/banette/flux.ex?_flowId=consultationbanette-flow">Banette des éditions</a></li>
                <li>Administration</li>
                <ul>
                    <li><a href="dmo/composantedition/admin/flux.ex?_flowId=administrationeditions-flow">Administration des
                            éditions</a></li>
                    <li><a href="dmo/composantedition/admin/flux.ex?_flowId=administrationpurgeseditions-flow">Administration des
                            traces de purges d'éditions</a></li>
                    <li><a href="dmo/composantedition/admin/flux.ex?_flowId=statistiqueseditions-flow">Statistiques relatives aux
                            éditions</a></li>
                </ul>
            </ul>
            <li>Tests</li>
            <ul>
                <li><a href="dmo/zf8/flux.ex?_flowId=testtags-flow">Test des tags</a></li>
                <li><a href="dmo/zf8/flux.ex?_flowId=testtagtree-flow">Test du tag tree</a></li>
                <li><a href="dmo/zf8/flux.ex?_flowId=lancertachelongue-flow">Test d'un lancement de tâche longue</a></li>
            </ul>
            <li>Divers</li>
            <ul>
                <li><a href="dmo/zf4/flux.ex?_flowId=rechercheoperationsjournal-flow">Consulter les opérations au journal</a></li>
                <li><a href="dmo/zf4/flux.ex?_flowId=transfereroperationsjournal-flow">Transférer les opérations journal sur itm</a></li>
                <li><a href="dmo/wsjournal/flux.ex?_flowId=rechercheoperationsjournalws-flow">Consulter les opérations journal par
                        Web Service</a></li>
                <li><a href="dmo/composantinfoutil/flux.ex?_flowId=affichagecaracteristiquesutilisateur-flow">Afficher les
                        caractéristiques utilisateur</a></li>
                <li><a href="http://www.intranet.dgfip">Intranet DGFIP</a></li>
                <li><a href="http://www.google.com">Google</a></li>
                <li><a href="javascript:aproposde();">À propos de</a></li>
            </ul>
            <li>Atlas</li>
            <ul>
                <li><a href="dmo/composantatlas/flux.ex?_flowId=ajoutdocumentatlas-flow">Ajouter un document</a></li>
                <li><a href="dmo/composantatlas/flux.ex?_flowId=recherchedocumentatlas-flow">Rechercher un document</a></li>
            </ul>
            <li><a href="dmo/composantogre/flux.ex?_flowId=catalogues-flow">Ogre</a></li>
            
             <li>Sireme</li>            
             <ul>
                <li>Gestion des messages</li>
                <ul>
                <li><a href="dmo/composantsrm/zf1/flux.ex?_flowId=creationmessage-flow">Créer un message</a></li>
                <li><a href="dmo/composantsrm/zf2/flux.ex?_flowId=creationmessageavecrestrictionsprofilsetcodiques-flow">Créer un message à destination de profils ou de codiques particuliers</a></li>
                <li><a href="dmo/composantsrm/zf2/flux.ex?_flowId=desactivationprofil-flow">Désactiver des profils</a></li>
                <li><a href="dmo/composantsrm/zf1/flux.ex?_flowId=recherchemessages-flow">Supprimer un message</a></li>
                <li><a href="dmo/composantsrm/zf1/flux.ex?_flowId=creationmessage-flow">Créer un message</a></li>
                <li><a href="dmo/composantsrm/zf1/flux.ex?_flowId=recherchemessages-flow">Modifier un message</a></li>
                <li><a href="dmo/composantsrm/zf1/flux.ex?_flowId=recherchemessages-flow">Rechercher un message</a></li>
                <li><a href="dmo/composantsrm/zf4/flux.ex?_flowId=recherchetousmessages-flow">Voir tous les messages</a></li>
                </ul>

                <li>Gestion des messages type</li>
                <ul>
                <li><a href="dmo/composantsrm/zf1/flux.ex?_flowId=creationmessagetype-flow">Créer un message type</a></li>
                <li><a href="dmo/composantsrm/zf1/flux.ex?_flowId=suppressionmessagestype-flow">Supprimer un message type</a></li>
                </ul>

                <li>Gestion des profils</li>
                <ul>
                <li><a href="dmo/composantsrm/zf5/flux.ex?_flowId=creationprofils-flow">Définir les profils de l'application</a></li>
                <li><a href="dmo/composantsrm/zf5/flux.ex?_flowId=suppressionprofils-flow">Supprimer des profils</a></li>
                </ul>
             </ul>
            
             <li>Métriques</li>            
             <ul>
                <li><a href="dmo/monitoring">Monitoring JavaMelody</a></li>
                <li><a href="dmo/metriques/flux.ex?_flowId=actuator-flow">Endpoints Actuator</a></li>
             </ul>

            
            <li><a href="dmo/plansite/flux.ex?_flowId=plansite-flow">Plan du Site</a></li>
            <li><a href="accueil.ex">Accueil</a></li>
        </ul>
</app:page>