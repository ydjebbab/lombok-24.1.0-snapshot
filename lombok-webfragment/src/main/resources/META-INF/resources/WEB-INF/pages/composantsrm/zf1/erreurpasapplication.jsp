<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java" %>
<%@ include file="/WEB-INF/tags/html/includes.tagf" %>

<!--  Page erreurpasapplication.jsp -->

<app:page titreecran="Gestion des messages" 
		  titrecontainer="Resultats >> Messages >> Création >> Incidents" 
		  menu="true" >
		  
<br>
<br>
Les erreurs suivantes se sont produites : 
<br>
<br><b>soit aucun filtre  application de saisie dans l'habilitation portant sur l'application SIREME . Veuillez contacter votre administrateur MADRHAS . </b></br>
</br>
<br><b>soit l'application sur laquelle vous désirez inscrire un message n'est pas enregistrée dans la base de SIREME , dans ce cas utilisez le menu
Gestion des applications/définir les caractéristiques d'une application pour créer les caractéristiques de votre application (libellé court et long et profils)</b> </br>

<br>
Merci de vous déconnecter de l'application SIREME
</br>
					
</br>
</br>

</app:page>
