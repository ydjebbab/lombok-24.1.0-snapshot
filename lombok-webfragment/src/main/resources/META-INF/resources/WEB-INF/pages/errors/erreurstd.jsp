<%@ page contentType="text/html;charset=UTF-8" 
         pageEncoding="UTF-8" 
         language="java" 
         isErrorPage="true"
         session="false"
         %>
<%@ include file="/WEB-INF/tags/html/includes.tagf" %>

<!--  Page erreurstd.jsp -->

<app:page titreecran="Erreur" 
          bandeau="false"
          piedpage="false">

<%-- Récupération dans le contexte des éléments relatifs à l'erreur traitée --%>          
<c:set var="exception" value="${requestScope[\"javax.servlet.error.exception\"]}"/>
<c:set var="exceptionstack" value="${requestScope[\"javax.servlet.error.exception\"].stackTrace}"/>
<c:set var="exceptiontype" value="${requestScope[\"javax.servlet.error.exception\"].getClass().name}"/>
<c:set var="exceptionmessage" value="${requestScope[\"javax.servlet.error.exception\"].message}"/>
<c:set var="errorcode" value="${requestScope[\"javax.servlet.error.status_code\"]}"/>
<c:set var="requesturi" value="${requestScope[\"javax.servlet.error.request_uri\"]}"/>
<c:set var="servlet_name" value="${requestScope[\"javax.servlet.error.servlet_name\"]}"/>
<c:forEach var="el" items="${exceptionstack}" >
    <%-- <c:set var="pileexception" value="${pileexception}<p>${el}</p>"/>--%>
</c:forEach>

<c:choose>

    <c:when test="${exceptiontype  eq 'java.lang.IllegalStateException'}">
        <p>Trop d'utilisateurs connectés. Réessayez plus tard s'il vous plait.</p>  
    </c:when>
    <c:otherwise>
    
<app:disperrorfix  divid="popmessage" >
<c:choose>

    <c:when test="${errorcode  eq '404'}">
        <p>L'URL demandée (<c:out value="${requesturi}"/>) n'existe pas (Code erreur 404).</p>   
    </c:when>
    <c:when test="${errorcode  eq '403'}">
        <p>L'URL demandée (<c:out value="${requesturi}"/>) n'est pas accessible (Code erreur 403).</p>   
    </c:when>
    <c:when test="${errorcode  eq '503'}">
        <p>Service indisponible (Code erreur 503).</p>   
    </c:when>
    <c:otherwise>
    <p><c:out value="Code erreur : ${errorcode}"/></p>
    <p><c:out value="Message erreur : ${exceptionmessage}"/></p>
    </c:otherwise>
</c:choose>
</app:disperrorfix>

<c:forEach items="${initParam['spring.profiles.active']}" var="profile" varStatus="loop">
    <c:if test="${profile eq 'developpement'}">
        <c:set var="envprofile" value="developpement"/> 
    </c:if>
 
</c:forEach>
<%-- Si le profile developpement a été mis dans la variable spring.profiles.active alors la suite de cette page sera rendue --%>
<c:if test="${not empty envprofile}">



<div class="erreurongletcontainer">
    <ul class="erreuronglet">
        <li class="ongleterreurdesactive"><a href="#popdetail1" class="liendetailerreur">Transmettre un rapport d'erreur</a></li>
        <li class="ongleterreurdesactive"><a href="#popdetail2" class="liendetailerreur">Détail de l'erreur</a></li>
        <li class="ongleterreurdesactive"><a href="#popdetail3" class="liendetailerreur">Information de configuration</a></li>
    </ul>   
</div>

<form id="rapportform" class="appForm"
<c:choose>
<c:when test='${esterreurprogrammation=="oui"}'>
action="mailto:<fmt:message key="mel.encasderreur.developpement"/>?Subject=Erreur application <fmt:message key="appli.libelle-court"/>"
</c:when>
<c:otherwise>
action="mailto:<fmt:message key="mel.encasderreur.exploitation"/>?Subject=Erreur application <fmt:message key="appli.libelle-court"/>"
</c:otherwise>
</c:choose>

method="post"
enctype="text/plain">

<div id="popdetail1" class="erreurdonnees">
<p>Afin de faciliter le diagnostic, merci de décrire le plus précisément possible le contexte dans lequel l'erreur survient.</p>
<div class="Vnormal">
    <div class="boxlblText"  style="width: 35%;">
        <label class="labelText" for="rapportform.contexte" >Contexte dans lequel l'erreur apparaît.</label>
    </div>
    <div class="boxText">
        <textarea name="commentaire" cols="128" rows="10" id="rapportform.contexte" style="width: 60%;"></textarea>
    </div>
</div>
<input name="Transmettre" value="Envoyer" type="submit"/><br/>
<input name="test" type="hidden" value="test"/>
</div>
    
<div id="popdetail2" class="erreurdonnees" >
    <p>Requête : ${requesturi}
    <p>${msgexception}</p>${pileexception}

<input name="msgexception" type="hidden" value="${msgexception}"/>
<input name="pileexception" type="hidden" value="${pileexception}"/>

</div>

<div id="popdetail3" class="erreurdonnees" >
<fieldset>
<legend>Header http</legend>
<div>
<c:forEach var="req" items="${header}">
<div class="Vnormal">
    <div class="boxlblText"  style="width: 25%;"> 
        <label for="${req.key}" class="labelText" >${req.key}</label>
    </div>
    <div class="boxText">
        <input name="${req.key}" id="${req.key}" type="text" class="inputText" readonly="readonly" size="60" value="${req.value}"/>
    </div>
</div>
</c:forEach>
</div>
</fieldset>
</div>
</form>
</c:if>
</c:otherwise>
</c:choose>
</app:page>