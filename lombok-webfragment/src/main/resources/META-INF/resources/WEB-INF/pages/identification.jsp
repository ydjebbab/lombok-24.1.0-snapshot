<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/tags/html/includes.tagf"%>
<spring:eval expression="@environment.getProperty('appli.inea.version')" var="ineaVersion"></spring:eval>
<spring:eval expression="@environment.getProperty('appli.inea.version')!=null?!@environment.getProperty('appli.inea.version').isEmpty():false" var="isIneaStyle" />
<spring:eval expression="@environment.getProperty('appli.inea.jquery-version')" var="jqueryVersion"></spring:eval>
<spring:eval expression="@environment.getProperty('appli.inea.css-url')" var="ineaAppliCssUrl"></spring:eval>
<spring:eval expression="@environment.getProperty('appli.libelle-long')" var="libellelong"></spring:eval>

<c:set var="jqueryUrl" value="webjars/jquery/${jqueryVersion}"></c:set>
<c:set var="ineaUrl" value="webjars/inea/${ineaVersion}"></c:set>



<c:choose>
    <c:when test="${isIneaStyle eq true}">


        <!-- Page identification.jsp format inea -->

        <!DOCTYPE html>
        <html lang="fr" dir="ltr">
<head>
<meta charset="utf-8" />
<title><c:out value="${libellelong}" /></title>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta name="description" content="${title}" />
<meta name="author" content="DGFiP" />
<meta name="msapplication-config" content="none" />
<meta name="format-detection" content="telephone=no" />

<link rel="icon" sizes="16x16 32x32 48x48 64x64" href="${ineaUrl}/img/favicon.ico" />
<!--[if IE]>
    <link rel="shortcut icon" href="${ineaUrl}/img/favicon.ico" />
<![endif] -->
<!-- Optional: Android & iPhone -->
<link rel="icon apple-touch-icon-precomposed" href="${ineaUrl}/img/favicon-152.png" />
<!-- Optional: IE10 Tile. -->
<meta name="msapplication-TileColor" content="#FFFFFF" />
<meta name="msapplication-TileImage" content="${ineaUrl}/img/favicon-144.png" />
<!-- Optional: ipads, androids, iphones, ... -->
<link rel="apple-touch-icon" sizes="152x152" href="${ineaUrl}/img/favicon-152.png" />
<link rel="apple-touch-icon" sizes="144x144" href="${ineaUrl}/img/favicon-144.png" />
<link rel="apple-touch-icon" sizes="120x120" href="${ineaUrl}/img/favicon-120.png" />
<link rel="apple-touch-icon" sizes="114x114" href="${ineaUrl}/img/favicon-114.png" />
<link rel="apple-touch-icon" sizes="72x72" href="${ineaUrl}/img/favicon-72.png" />
<link rel="apple-touch-icon" href="${ineaUrl}/img/favicon-57.png" />

<link href="${ineaAppliCssUrl}" rel="stylesheet" />
<script src="${jqueryUrl}/dist/jquery.min.js"></script>
<script src="${ineaUrl}/js/inea.min.js" async defer></script>
<script src="${ineaUrl}/js/jquery-ui.min.js" async defer></script>
</head>
<body>

    <div class="container contenu-bg">
        <!-- inea:entete:debut -->
        <header class="public" role="banner">
            <div class="row">
                <div
                    class="col-xs-10 col-xs-offset-2 col-sm-6 col-sm-offset-1 col-sm-sans-gouttiere-gauche col-md-6 col-md-offset-0 col-md-avec-gouttiere-gauche">
                    <!-- inea:accueil-entete:debut -->
                    <a class="accueil-entete" href="accueil.html"> <span class="sr-only">Accueil</span>
                        <div class="marianne-entete logo-marianne"></div>
                        <div class="titre-entete">
                            <div class="logo-titre-entete logo-impots-gouv-fr"></div>
                        </div>
                    </a>
                    <!-- inea:accueil-entete:fin -->
                </div>

                <!-- inea:actions-privees:debut -->
                <!-- inea:actions-privees:fin -->
            </div>
            <!-- inea:details-identification:debut -->
            <!-- inea:details-identification:fin -->

            <!-- inea:barre-navigation:debut -->
            <!-- inea:barre-navigation:fin -->
        </header>
    </div>

    <main id="contenu" role="main">
    <div class="container">
        <!-- inea:formulaire-vertical:debut -->
        <form action="j_spring_security_check" method="post">
            <div class="row rubrique">

                <!-- inea:rubrique:debut -->
                <div class="col-xs-12 rubrique">
                    <div class="row titre-rubrique">
                        <div class="col-xs-12">
                            <!-- inea:titre:debut -->
                            <div class="titre">
                                <h2>Formulaire de connexion</h2>
                            </div>
                            <!-- inea:titre:fin -->
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <p aria-hidden="true">
                                <em>Les champs marqués d'un astérisque ( <span class="text-danger">*</span>) doivent être renseignés.
                                </em>
                            </p>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-12">
                            <!-- champ-formulaire:debut -->
                            <div class="form-group">

                                <!-- inea:libelle-champ:debut -->
                                <label for="champ-saisie-vertical" class="col-xs-12 col-sm-3 control-label requis">Identifiant</label>
                                <!-- inea:libelle-champ:fin -->

                                <div class="col-xs-12 col-sm-3 ${status.error ? 'has-error' : ''}">
                                    <!-- inea:champ-saisie:debut -->
                                    <input id="champ-saisie-vertical" type="text" name="username" class="form-control" aria-required="true" />
                                    <!-- inea:champ-saisie:fin -->
                                </div>
                            </div>
                            <!-- champ-formulaire:fin -->
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-12">

                            <!-- champ-formulaire:debut -->
                            <div class="form-group">

                                <!-- inea:libelle-champ:debut -->
                                <label for="champ-saisie-vertical" class="col-xs-12 col-sm-3 control-label requis">Mot de passe</label>
                                <!-- inea:libelle-champ:fin -->

                                <div class="col-xs-12 col-sm-3">
                                    <!-- inea:champ-saisie:debut -->
                                    <input id="champ-saisie-vertical" type="password" name="password" class="form-control"
                                        aria-required="true" />
                                    <!-- inea:champ-saisie:fin -->
                                </div>

                                <!-- champ-formulaire:fin -->
                            </div>
                        </div>
                    </div>

                    <div class="btn-group">
                        <!-- inea:bouton-action:debut -->
                        <button type="button" class="btn btn-default" onclick="this.form.submit();">Se connecter</button>
                        <!-- inea:bouton-action:fin -->

                    </div>
                </div>
                <!-- inea:rubrique:fin -->
            </div>
            <input type="hidden" name="password" value="" /> <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
        </form>
        <!-- inea:formulaire-vertical:fin -->
    </div>
    </main>


    <!-- inea:pied-page:debut -->
    <div class="container">
        <div class="row pieddepage">
            <div class="col-xs-12 pieddepage legal">
                <footer role="contentinfo">
                    <p class="text-center">
                        © Direction générale des Finances publiques <span aria-hidden="true"> - </span> <a
                            href="https://www.impots.gouv.fr/portail/mentions-legales">Mentions légales</a>
                    </p>
                </footer>
            </div>
        </div>
    </div>
    <!-- inea:pied-page:fin -->
</body>
        </html>
    </c:when>
    <c:otherwise>

        <!--  Page identification.jsp format lombok tags-->

        <fmt:message key="appli.libelle-long" var="libellelong" />
        <fmt:message key="appli.libelle-court" var="libellecourt" />

        <fmt:message key="annuaire.typeannuaire" var="typeannuaire" />

        <c:set var="responsive" value="true" />
        <c:set var="labelusername" value="Identifiant" />
        <c:set var="labelpassword" value="Mot de passe" />

        <c:choose>
            <c:when test="${responsive != 'true'}">
                <c:set var="formboxclass_value" value="row donnees identification" />
            </c:when>
            <c:otherwise>
                <c:set var="formboxclass_value" value="row donnees" />
            </c:otherwise>
        </c:choose>

        <app:page titreecran="${libellecourt} - Identification" titrecontainer="${libellelong}" menu="false" responsive="${responsive}">
            <script type="text/javascript">
													var cookieDoShowMessagePrompt = 'showMessagePrompt';

													function isCookieHere(name) {
														return document.cookie
																.indexOf(name) !== -1;
													}

													function rewriteCookie(
															name, newValue) {

														function createCookie(
																name, value,
																days) {
															if (days) {
																var date = new Date();
																date
																		.setTime(date
																				.getTime()
																				+ (days * 24 * 60 * 60 * 1000));
																var expires = "; expires="
																		+ date
																				.toGMTString();
															} else
																var expires = "";
															document.cookie = name
																	+ "="
																	+ value
																	+ expires
																	+ "; path=/";
														}

														function eraseCookie(
																name) {
															createCookie(name,
																	"", -1);
														}

														eraseCookie(name);
														createCookie(name,
																newValue, 10);

													}

													rewriteCookie(
															cookieDoShowMessagePrompt,
															true);
												</script>
            <app:form method="post" enctype="application/x-www-form-urlencoded" action="j_spring_security_check"
                formboxclass="${formboxclass_value}" responsive="${responsive}" bootstrapmodel="half-center"
                rwdformclass="form-horizontal col-md-offset-3 col-md-6 col-sm-offset-2 col-sm-8 col-xs-offset-0 col-xs-12"
                onsubmit="return submit_form(500)" autocomplete="on"
                style="-webkit-box-shadow: 10px 10px 5px 0px rgba(0,0,0,0.75);
-moz-box-shadow: 10px 10px 5px 0px rgba(0,0,0,0.75);
box-shadow: 10px 10px 5px 0px rgba(0,0,0,0.75);">

                <c:choose>
                    <c:when test="${typeannuaire=='REAL'}">
                        <c:set var="labelusername"
                            value="Identifiant REAL (cn=LOMBOK Dev (3134100000),ou=Notaires,o=Professions Reglementées,c=FR)" />
                        <c:set var="labelpassword" value="Profil (Notaires)" />
                        <c:set var="typename" value="text" />
                    </c:when>
                    <c:when test="${typeannuaire=='AUCUN'}">
                        <c:set var="labelpassword" value="Profil" />
                        <c:set var="typename" value="text" />
                    </c:when>
                    <c:otherwise>
                        <c:set var="typename" value="password" />
                    </c:otherwise>
                </c:choose>

                <app:formgroup responsive="${responsive}" formgroupclass="Vrequis" labelname="${labelusername}" labelfor="username"
                    labelclass="col-md-offset-0 col-md-4 col-sm-offset-0 col-sm-4 col-xs-offset-2 col-xs-8" labelboxstyle="width: 35%;"
                    boxclass="col-md-offset-0 col-md-6 col-sm-offset-0 col-sm-6 col-xs-offset-2 col-xs-8" inputname="username"
                    maxlength="40" size="30" id="username" typename="text" classname="inputText" required="required" placeholder="requis" />


                <app:formgroup responsive="${responsive}" formgroupclass="Vrequis" labelname="${labelpassword}" labelfor="password"
                    labelclass="col-md-offset-0 col-md-4 col-sm-offset-0 col-sm-4 col-xs-offset-2 col-xs-8" labelboxstyle="width: 35%;"
                    boxclass="col-md-offset-0 col-md-6 col-sm-offset-0 col-sm-6 col-xs-offset-2 col-xs-8" inputname="password"
                    maxlength="40" size="30" id="password" typename="${typename}" classname="inputText" required="required"
                    placeholder="requis" />




                <app:boxboutons>
                    <app:submit label="Se connecter" title="Se connecter" transition="ok" id="enterButton" />
                </app:boxboutons>
            </app:form>

            <app:disperror var="${SPRING_SECURITY_LAST_EXCEPTION.message}" />

            <c:if test="${! empty SPRING_SECURITY_TARGET_URL}">
                <app:disperror>
                    <li>Vous vous êtes déconnecté</li>
                    <li>ou</li>
                    <li>votre session a expiré.</li>
                </app:disperror>
            </c:if>
        </app:page>
    </c:otherwise>
</c:choose>