<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/tags/html/includes.tagf"%>

<!--  Page affichercaracteristiquesgenerales.jsp -->

<c:set var="responsive" value="true" />

<app:page titreecran="Caractéristiques utilisateur" menu="true" responsive="${responsive}">

    <app:chemins action="zf2/flux.ex" responsive="${responsive}">
        <app:chemin action="accueil.ex" title="Retour à l'accueil" responsive="${responsive}">Accueil</app:chemin>
        <app:chemin title="Divers" responsive="${responsive}">Divers</app:chemin>
        <app:chemin title="Caractéristiques utilisateur" responsive="${responsive}">Caractéristiques utilisateur</app:chemin>
    </app:chemins>

    <app:onglets cleactive="1" responsive="${responsive}" boxongletsclass="row height_equalizer ongletcontainer">
        <app:onglet cle="1" libelle="Général" responsive="${responsive}" ongletclass="btmodel_inv_third" />
        <app:onglet cle="2" libelle="Détail" bouton="_eventId_VoirDetail" responsive="${responsive}" ongletclass="btmodel_inv_third" />
        <app:onglet cle="3" libelle="Habilitation" bouton="_eventId_VoirHabilitations" responsive="${responsive}"
            ongletclass="btmodel_inv_third" />
    </app:onglets>

    <app:form action="composantinfoutil/flux.ex" formobjectname="affichagecaracteristiquesutilisateurform"
        formboxclass="donnees sousonglets" responsive="${responsive}">

        <app:input attribut="unePersonneAnnuaire.cn" libelle="Nom Complet : " maxlength="40" size="40" readonly="true" requis="false"
            responsive="${responsive}" bootstraplabelclass="btmodel_half" bootstrapinputclass="btmodel_half" />

        <app:input attribut="unePersonneAnnuaire.uid" libelle="Identifiant :" maxlength="40" size="40" readonly="true" requis="false"
            responsive="${responsive}" bootstraplabelclass="btmodel_half" bootstrapinputclass="btmodel_half" />
        <app:input attribut="unePersonneAnnuaire.affectation" libelle="Affectation : " maxlength="40" size="40" readonly="true"
            requis="false" responsive="${responsive}" bootstraplabelclass="btmodel_half" bootstrapinputclass="btmodel_half" />

        <app:input attribut="unePersonneAnnuaire.sages" libelle="Sages (Id DGI) : " maxlength="40" size="40" readonly="true" requis="false"
            responsive="${responsive}" bootstraplabelclass="btmodel_half" bootstrapinputclass="btmodel_half" />

        <app:boxboutons divboxboutonsclass="boxboutons">
            <app:submit label="Retourner à l'accueil" title="Retourner à l'accueil" transition="RetourAccueil" id="enterButton"
                responsive="${responsive}" />

            <app:submit label="Voir caractéristiques annuaire dgfip" class="primary"
                transition="afficheraffichercaracteristiquesgeneralesannuairedgfip"
                id="afficheraffichercaracteristiquesgeneralesannuairedgfip" responsive="${responsive}" />
        </app:boxboutons>
    </app:form>
</app:page>