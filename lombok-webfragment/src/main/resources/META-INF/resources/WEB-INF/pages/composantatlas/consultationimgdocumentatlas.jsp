<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/tags/html/includes.tagf"%>

<!--  Page consultationimgdocumentatlas.jsp -->

<c:set var="responsive" value="true" />

<app:page titreecran="Rechercher un document" menu="true" responsive="${responsive}">

    <app:chemins action="composantatlas/flux.ex" responsive="${responsive}">
        <app:chemin action="accueil.ex" title="Retour à l'accueil" responsive="${responsive}">Accueil</app:chemin>
        <app:chemin title="Atlas" responsive="${responsive}">Atlas</app:chemin>
        <app:chemin title="Rechercher un document" responsive="${responsive}">Rechercher un document</app:chemin>
    </app:chemins>

    <app:form method="post" action="composantatlas/flux.ex" formid="recherchedocumentatlas" formobjectname="recherchedocumentatlasform"
        onsubmit="return submit_form(500)" formboxid="donnees" formboxclass="donnees sousonglets" responsive="${responsive}">

        <app:image fichierjoint="fichierimg" />

    </app:form>
</app:page>