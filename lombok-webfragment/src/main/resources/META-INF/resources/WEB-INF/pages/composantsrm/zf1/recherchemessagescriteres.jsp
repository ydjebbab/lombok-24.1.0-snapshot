<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/tags/html/includes.tagf" %>

<!--  Page recherchemessagescriteres.jsp -->

<app:page titreecran="Selectionner un message" 
           
          menu="true" >          

  <app:chemins action="zf1/flux.ex">
    <app:chemin action="accueil.ex" title="Retour à l'accueil">Accueil</app:chemin>
       <app:chemin>Recherche de messages</app:chemin>
</app:chemins>

<app:form 
	method="post" 
	action="zf1/flux.ex" 
	formid="recherchemessages" 
	formobjectname="recherchemessagesform" 
	onsubmit="return submit_form(500)"
	formboxid="donnees"
	formboxclass="donnees sousonglets">

	 <fieldset>
	 <legend>Messages créés entre :</legend> 
		<app:input attribut="crit.dateDebutMessage" libelle="Date de début :" maxlength="10" size="10" readonly="${readonly}" 
	           consigne="Veuillez saisir une date de la forme jj/mm/aaaa" requis="false">
		<app:calendar/>
		</app:input>
		<app:input attribut="crit.dateFinMessage" libelle="Date de fin :" maxlength="10" size="10" readonly="${readonly}"
	           consigne="Veuillez saisir une date de la forme jj/mm/aaaa"  requis="false">
		<app:calendar/>
		</app:input>
		</fieldset>
		 <fieldset>
	<app:radio libelle="Type de message :" pctlibelle="10%" itemsmap="${typemessage}" attribut="crit.typeMessages" requis="false"/>
	
 	<app:input attribut="crit.uidCreateurMessage" libelle="UID du créateur du message :" maxlength="30" size="30" readonly="${readonly}"
	 requis="false" />         
	</fieldset>
<app:boxboutons>
	<app:submit inputclass="annul" label="Annuler" transition="_eventId_annuler"/>
	<app:submit label="Rechercher" transition="_eventId_rechercher" id="enterButton"/>
</app:boxboutons>

</app:form>
</app:page>