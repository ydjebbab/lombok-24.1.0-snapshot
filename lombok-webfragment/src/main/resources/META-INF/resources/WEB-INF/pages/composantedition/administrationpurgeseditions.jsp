<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/tags/html/includes.tagf"%>

<!--  Page administrationpurgeseditions.jsp -->

<c:set var="responsive" value="true" />

<app:page titreecran="Administration des traces des purges d'éditions" menu="true" responsive="${responsive}">

    <app:chemins action="composantedition/admin/flux.ex" responsive="${responsive}">
        <app:chemin transition="fin" title="Retour à l'accueil" responsive="${responsive}">Accueil</app:chemin>
        <app:chemin title="Éditions" responsive="${responsive}">Éditions</app:chemin>
        <app:chemin title="Administration" responsive="${responsive}">Administration</app:chemin>
        <app:chemin title="Administration des traces de purges d'éditions" transition="retourecranrecherche" responsive="${responsive}">Administration des traces de purges d'éditions</app:chemin>
        <app:chemin title="Liste des traces de purge" responsive="${responsive}">Liste des traces de purge</app:chemin>
    </app:chemins>

    <app:form action="composantedition/admin/flux.ex" formboxclass="eXtremeTable" formobjectname="administrationpurgeseditionsform"
        responsive="${responsive}">

        <%--
        <ec:table items="adminlistedestracesdepurge"
                action="${pageContext.request.contextPath}/composantedition/admin/flux.ex"
                tableId="adminlistedestracesdepurge" state="persist"
                autoIncludeParameters="false"
                form="formulaire"
                locale="fr_FR"
                view="cphtml"
                filterable="true"
                showPagination="true" showExports="true"
                var="row"
                sortRowsCallback="cp" filterRowsCallback="cp"
                >
--%>
        <ec:table items="adminlistedestracesdepurge" tableId="adminlistedestracesdepurge" view="cphtml" var="row"
            action="${pageContext.request.contextPath}/composantedition/admin/flux.ex" state="persist" stateAttr="gotofirstpage"
            autoIncludeParameters="false" form="formulaire" locale="fr_FR" filterable="true" sortable="true" sortRowsCallback="cp"
            filterRowsCallback="cp" rowsDisplayed="10" showPagination="true" showNewPagination="true" showExports="false"
            newPaginationNbPage="5" showStatusBar="true" showPaginationBottom="false" showExportsBottom="false" showNewFilter="true"
            onInvokeAction="ecupdatectra('tableaunavigation')" positionnoresultsfound="body">


            <ec:row highlightRow="true">
                <%--
                <ech:column property="chkbx" cell="cpcheckboxcell" rowid="id" viewsAllowed="cphtml"
                    headerCell="cpselectAll" width="5%" filterable="false" sortable="false" />
            --%>
                <ech:column property="chkbx" cell="cpcheckboxcell" rowid="id" viewsAllowed="cphtml" headerCell="cpselectAll" width="5%"
                    filterable="false" sortable="false" showResponsive="${responsive}"
                    dataAttributes="data-tablesaw-priority=persist scope=col" />

                <%--
                <ec:column property="dateDebutPurge" alias="dateDebutPurge"  title="Début de la purge"  
                    sortable="true" style="text-align: center" headerStyle="text-align: center" cell="dateheureminutesecondemilli"/>
            --%>
                <ec:column property="dateDebutPurge" alias="dateDebutPurge" title="Début de la purge" style="text-align: center"
                    headerStyle="text-align: center" cell="dateheureminutesecondemilli" filterable="true" sortable="true"
                    showResponsive="${responsive}" dataAttributes="data-tablesaw-priority=persist scope=col" />

                <%--
                <ec:column property="dateFinPurge"  alias="dateFinPurge" title="Fin de la purge" 
                    width="10%" sortable="true" style="text-align: center"  headerStyle="text-align: center" cell="dateheureminute"/>
            --%>
                <ec:column property="dateFinPurge" alias="dateFinPurge" title="Fin de la purge" width="10%" style="text-align: center"
                    headerStyle="text-align: center" cell="dateheureminute" filterable="true" sortable="true" showResponsive="${responsive}"
                    dataAttributes="data-tablesaw-priority=1 scope=col" />

                <%--
                <ec:column property="etatPurge"  alias="etatPurge" title="Etat de la purge"  sortable="true" 
                    style="text-align: center" headerStyle="text-align: center" /> 
            --%>
                <ec:column property="etatPurge" alias="etatPurge" title="Etat de la purge" style="text-align: center"
                    headerStyle="text-align: center" filterable="true" sortable="true" showResponsive="${responsive}"
                    dataAttributes="data-tablesaw-priority=2 scope=col" />

                <%--
                <ec:column property="nbEditionsSupprimees" alias="nbEditionsSupprimees" title="Nombre d'éditions supprimées"
                    sortable="true" style="text-align: right" headerStyle="text-align: center" /> 
            --%>
                <ec:column property="nbEditionsSupprimees" alias="nbEditionsSupprimees" title="Nombre d'éditions supprimées"
                    style="text-align: right" headerStyle="text-align: center" filterable="true" sortable="true"
                    showResponsive="${responsive}" dataAttributes="data-tablesaw-priority=3 scope=col" />

                <%--
                <ec:column property="messageErreur" alias="messageErreur" title="Message d'erreur"
                    sortable="true" style="text-align: right" headerStyle="text-align: center" />  
            --%>
                <ec:column property="messageErreur" alias="messageErreur" title="Message d'erreur" style="text-align: right"
                    headerStyle="text-align: center" filterable="true" sortable="true" showResponsive="${responsive}"
                    dataAttributes="data-tablesaw-priority=4 scope=col" />
            </ec:row>
        </ec:table>

        <app:boxboutons>
            <c:choose>
                <c:when test="${responsive != 'true'}">
                    <app:submit label="Supprimer les traces de purge sélectionnées" title="Supprimer les traces de purge sélectionnées"
                        transition="supprimer" />
                    <app:submit label="Arrêter les traces de purge sélectionnées" title="Arrêter les traces de purge sélectionnées"
                        transition="arreter" />
                    <app:submit label="Retourner à l'écran de recherche" title="Retourner à l'écran de recherche"
                        transition="retourecranrecherche" />
                </c:when>
                <c:otherwise>
                    <app:submit label="Supprimer les traces de purge sélectionnées" title="Supprimer les traces de purge sélectionnées"
                        transition="supprimer" responsive="${responsive}" />
                    <app:submit label="Arrêter les traces de purge sélectionnées" title="Arrêter les traces de purge sélectionnées"
                        transition="arreter" responsive="${responsive}" />
                    <app:submit label="Retourner à l'écran de recherche" title="Retourner à l'écran de recherche"
                        transition="retourecranrecherche" responsive="${responsive}" />
                </c:otherwise>
            </c:choose>
        </app:boxboutons>
    </app:form>
    <app:disperror var="${erreurentreflux}" />
</app:page>