<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/tags/html/includes.tagf"%>

<!--  Page supprimerdeseditions.jsp -->

<app:page titreecran="Administration des éditions">

    <app:chemins action="composantedition/admin/flux.ex">
        <app:chemin action="accueil.ex" title="Retour à l'accueil">Accueil</app:chemin>
        <app:chemin title="Éditions" responsive="${responsive}">Éditions</app:chemin>
        <app:chemin title="Administration" responsive="${responsive}">Administration</app:chemin>
        <app:chemin title="Administration des éditions" transition="retourecranrecherche" responsive="${responsive}">Administration des éditions</app:chemin>
        <app:chemin title="Liste des éditions sélectionnées" transition="retourlisteeditions" responsive="${responsive}">Editions sélectionnées</app:chemin>
        <app:chemin title="Supprimer des éditions" responsive="${responsive}">Supprimer des éditions</app:chemin>
    </app:chemins>

    <app:form action="composantedition/admin/flux.ex" formboxclass="eXtremeTable" formobjectname="administrationeditionsform">

        <ec:table items="editionsjasperasupprimer" action="${pageContext.request.contextPath}/composantedition/admin/flux.ex"
            tableId="editionsjasperasupprimer" autoIncludeParameters="false" form="formulaire" locale="fr_FR" view="cphtml"
            filterable="true" showPagination="false" showExports="true" var="row" sortRowsCallback="cp" filterRowsCallback="cp">
            <ec:row highlightRow="true">
                <ec:column property="presentationEdition.description" alias="description" title="Edition" sortable="true"
                    style="text-align: center" headerStyle="text-align: center" />
                <ec:column property="ordoEdition.status" alias="status" title="Etat" width="10%" sortable="true" style="text-align: center"
                    headerStyle="text-align: center">
                </ec:column>
                <ec:column property="ordoEdition.dateDemande" alias="dateDemande" title="Date de demande" cell="dateheureminute"
                    sortable="true" style="text-align: center" headerStyle="text-align: center">
                </ec:column>
                <ec:column property="ordoEdition.endTime" alias="endTime" title="Date d'exécution" cell="dateheureminute" sortable="true"
                    style="text-align: center" headerStyle="text-align: center">
                </ec:column>
                <ec:column property="archivageEdition.datePurge" title="Date de purge" cell="dateheureminute" sortable="true"
                    style="text-align: center" headerStyle="text-align: center">
                </ec:column>
            </ec:row>
        </ec:table>
        <p>
            <br>
        </p>

        <c:choose>
            <c:when test="${responsive != 'true'}">
                <app:boxboutons>
                    <app:submit label="Retourner à la liste des éditions" transition="retourlisteeditions"
                        title="Retourner à la liste des éditions" />
                    <app:submit label="Confirmer la suppression" transition="confirmersuppression" title="Confirmer la suppression" />
                </app:boxboutons>
            </c:when>
            <c:otherwise>
                <app:boxboutons>
                    <app:submit label="Retourner à la liste des éditions" transition="retourlisteeditions"
                        title="Retourner à la liste des éditions" responsive="${responsive}" />
                    <app:submit label="Confirmer la suppression" transition="confirmersuppression" responsive="${responsive}"
                        inputclass="primary btn btn-default" title="Confirmer la suppression" />
                </app:boxboutons>
            </c:otherwise>
        </c:choose>
    </app:form>
</app:page>