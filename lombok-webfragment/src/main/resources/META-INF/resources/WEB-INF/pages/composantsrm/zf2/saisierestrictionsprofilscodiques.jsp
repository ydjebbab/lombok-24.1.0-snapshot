<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java" %>
<%@ include file="/WEB-INF/tags/html/includes.tagf" %>

<!--  Page saisierestrictionsprofilscodiques.jsp -->

<app:page titreecran="Saisie de messages bienvenue" 
		  titrecontainer=" "
		  menu="true" 
		  titrebnd="publication d'informations">

  <app:chemins action="zf2/flux.ex">
    <app:chemin action="accueil.ex" title="Retour à l'accueil">Accueil</app:chemin>
       <app:chemin>Saisie des profils ou des codiques</app:chemin>
</app:chemins>
	
 <app:form 
	method="post" 	
	action="zf2/flux.ex" 
	formid="creationmessage" 
	formobjectname="creationmessageform" 
	onsubmit="return submit_form(500)"
	formboxid="donnees"
	formboxclass="donnees sousonglets">
	
	 <fieldset><legend>Profils destinataires du message</legend>	
<ec:table 
  items="listProfils"  var="cont"
  action="${pageContext.request.contextPath}/zf1/flux.ex"
  tableId="creepro"
  autoIncludeParameters="false"
  showPagination="false"
  showStatusBar="false"
  filterable="false"
  sortable="false"
  locale="fr_FR"
  width="50%"
  form="creationmessage"	
  theme="eXtremeTable"	
  view="cphtml"
  >	
 
  <ec:row>
  <ech:column property="chkbx"   title=" " rowid="hashcode" width="5%" filterable="false" sortable="false" viewsAllowed="cphtml"
			cell="cpcheckboxcell"></ech:column>
  <ec:column property="libelleProfil" title="libellé du profil"/>
  <ec:column property="codeProfil" title="code profil"/>
   
  </ec:row>
</ec:table>	
	</fieldset>
	 <fieldset><legend>Codiques destinataires du message (Saisir un codique sur 6 caractéres ou moins .par exemple:
	 001 pour que le message s'applique à tous les codqiues commencant par 001</legend>	
	<app:input attribut="codique1" libelle="codique" maxlength="6" size="6" readonly="${readonly}"
	          >		      
	</app:input>
	<app:input attribut="codique2"   maxlength="6" size="6" readonly="${readonly}"
	          >		   
	</app:input>
	<app:input attribut="codique3"   maxlength="6" size="6" readonly="${readonly}"
	          >		   
	</app:input>
	</fieldset>
<app:boxboutons>
	<app:submit inputclass="annul" label="Annuler" transition="_eventId_annuler"/>
	<app:submit label="Valider" transition="_eventId_valider"/>
</app:boxboutons>

</app:form>
</app:page>