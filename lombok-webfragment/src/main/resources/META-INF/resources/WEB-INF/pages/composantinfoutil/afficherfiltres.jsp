<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/tags/html/includes.tagf"%>

<!--  Page afficherfiltres.jsp -->

<app:page titreecran="Satelit bienvenue" titrecontainer="Affichage caractéristiques utilisateur >> Caractéristiques habilitation"
    menu="true">

    <app:form action="composantinfoutil/flux.ex" formboxclass="donnees" formobjectname="affichagecaracteristiquesutilisateurform">

        <ec:table items="listefiltres" action="${pageContext.request.contextPath}composantinfoutil/flux.ex" tableId="filtres" width="60%"
            autoIncludeParameters="false" form="formulaire" locale="fr_FR" view="cphtml" filterable="false" showPagination="false"
            showExports="false" var="row" sortRowsCallback="cp" filterRowsCallback="cp">
            <ec:row highlightRow="true">
                <ec:column property="nomDuFiltre" title="Nom du filtre">
                    <app:link action="composantinfoutil/flux.ex" transition="affichervaleursfiltres" attribute="nomDuFiltre"
                        value="${row.nomDuFiltre}" label="${row.nomDuFiltreFormate}" active="true" title="Consulter les filtres associés" />
                </ec:column>
                <ec:column property="nbValeursDeFiltre" title="Valeurs associées" />
            </ec:row>
        </ec:table>


        <app:boxboutons divboxboutonsclass="boxboutons">
            <app:submit label="Retour détail des habilitations" transition="RetourHabilitations" id="enterButton" />
        </app:boxboutons>
    </app:form>
</app:page>