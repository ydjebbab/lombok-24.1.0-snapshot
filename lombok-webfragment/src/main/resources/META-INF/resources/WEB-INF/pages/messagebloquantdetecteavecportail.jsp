<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/tags/html/includes.tagf" %>

<!--  Page messagebloquantdetecteavecportail.jsp -->

<fmt:message key="appli.libelle-long" var="libellelong"/>
<fmt:message key="appli.libelle-court" var="libellecourt"/>

<app:page titreecran="${libellecourt}" 
          titrecontainer="${libellelong}" 
          menu="false"
          bandeau="true" 
          bandeaujsp="/WEB-INF/tags/app/bandaffichageerreurportail.jsp">
          
<app:form  method="post" 
           action="j_spring_security_check"
           formboxclass="donnees identification">	
	 
	
	 
	<li>${SPRING_SECURITY_LAST_EXCEPTION.message}</li>

</app:form>

 

<c:if test="${! empty SPRING_SECURITY_TARGET_URL}">
<app:disperror>
	  	<li>Vous vous êtes déconnecté</li>
	  	<li>ou</li>
	  	<li>votre session a expiré.</li>
</app:disperror>
</c:if>
</app:page>