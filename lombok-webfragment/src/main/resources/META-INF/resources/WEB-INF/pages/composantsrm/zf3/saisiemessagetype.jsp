<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java" %>
<%@ include file="/WEB-INF/tags/html/includes.tagf" %>

<!--  Page saisiemessagetype.jsp -->

<app:page titreecran="Saisie de messages bienvenue" 
		  titrecontainer="Publication d'informations"
		  menu="true" 
		  titrebnd="publication d'informations">

 
	
 <app:form 
	method="post" 	
	action="zf3/flux.ex" 
	formid="creationmessagetype" 
	formobjectname="creationmessagetypeform" 
	onsubmit="return submit_form(500)"
	formboxid="donnees"
	formboxclass="donnees sousonglets">

 	<c:set var="readonly" value="false"/>
	
 	<div>
 	<fieldset><legend>Caractéristiques du message type</legend>
 	 
	<app:textarea attribut="messagetypeform.libelleMessageType" libelle="Libelle :" cols="75" rows="10"  tabindex="1"   readonly="${readonly}" inputboxwidth="30%">
		 
	</app:textarea>
	 
	</div>  
<app:boxboutons>
	<app:submit inputclass="annul" label="Annuler" transition="_eventId_annuler"/>
	<app:submit label="Valider" transition="_eventId_valider"/>
</app:boxboutons>

</app:form>
</app:page>