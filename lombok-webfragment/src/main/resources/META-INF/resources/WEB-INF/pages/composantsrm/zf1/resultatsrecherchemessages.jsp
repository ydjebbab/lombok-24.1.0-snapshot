<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/tags/html/includes.tagf" %>

<!--  Page resultatsrecherchemessages.jsp -->

<app:page titreecran="Selectionner un message" 
          titrecontainer="" 
          menu="true">
          
          
  <app:chemins action="zf1/flux.ex">
    <app:chemin action="accueil.ex" title="Retour à l'accueil">Accueil</app:chemin>
       <app:chemin>Résultats de la recherche de messages</app:chemin>
</app:chemins>
          
          
          

<app:form
   method="post" 	
	action="zf1/flux.ex" 
	 formboxclass="eXtremeTable">
	 
	 
	 
	 
	 
	 
<ec:table items="listemessages" tableId="rechmess" view="cphtml" var="cont"    action="flux.ex" state="notifyToDefault" stateAttr="gotofirstpage"
		autoIncludeParameters="false" 
		form="formulaire" 
		locale="fr_FR" 
		sortRowsCallback="cp"  rowsDisplayed="10" showPagination="true"  theme="eXtremeTable"
		>
		
		  <ec:exportPdf 
        fileName="listemessage.pdf" 
         tooltip="Export PDF" 
         headerColor="black" 
         headerBackgroundColor="#b6c2da" 
         headerTitle="Liste des messages"
         imageName="pdf"
         view="cppdf"
       />
    
		<ec:row highlightRow="true">
			<ech:column property="chkbx" rowid="id" title=" " width="5%" filterable="false" sortable="false" viewsAllowed="cphtml"
			cell="cpcheckboxcell"></ech:column>
			<ec:column property="libelleMessage" title="libelle du message" width="15%" sortable="true" filterable="true" > </ec:column>
			<ec:column property="UIDCreateurMessage" title="créateur du message" width="35%" sortable="true" filterable="true"></ec:column>
			<ec:column property="libelleTypeMessage" title="type du message" width="35%" sortable="true" filterable="true"></ec:column>
			<ec:column property="dateDebutMessage" title="date de début d'affichage du message" width="15%" sortable="true" filterable="true" cell="dateheureminute" format="dd/MM/yyyy HH:mm"> </ec:column>
			<ec:column property="dateFinMessage" title="date de fin d'affichage du message" width="15%" sortable="true" filterable="true" cell="dateheureminute" format="dd/MM/yyyy HH:mm"> </ec:column>
			<ec:column property="dateDebutInactiviteProfil" title="date de début d'inactivité du profil" width="15%" sortable="true" filterable="true" cell="dateheureminute" format="dd/MM/yyyy HH:mm"> </ec:column>
			<ec:column property="dateFinInactiviteProfil" title="date de fin d'inactivité du profil" width="15%" sortable="true" filterable="true" cell="dateheureminute" format="dd/MM/yyyy HH:mm"> </ec:column>
			<ec:column property="lesProfilsDesactivesTxt" title="profils désactivés" width="35%" sortable="true" filterable="true"></ec:column>
			<ec:column property="lesProfilsDestinatairesTxt" title="profils destinataires" width="35%" sortable="true" filterable="true"></ec:column>
			<ec:column property="lesCodiquesDestinatairesTxt" title="codiques destinataires" width="35%" sortable="true" filterable="true"></ec:column>
		</ec:row>
</ec:table>

<app:boxboutons>
	<app:submit inputclass="annul" label="Annuler" transition="_eventId_Annuler"/>
	<app:submit label="Supprimer" transition="_eventId_Supprimer"/>
	<app:submit label="Modifier" transition="_eventId_Modifier"/>	 
	<app:submit label="Nouvelle recherche" transition="_eventId_NouvelleRecherche"/>
</app:boxboutons>
</app:form>
<app:disperror var="${erreurentreflux}" style="top: 40%"/>
</app:page>
