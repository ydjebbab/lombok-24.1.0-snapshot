<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/tags/html/includes.tagf"%>

<!--  Page rechercheeditioncriteres.jsp -->

<c:set var="responsive" value="true" />

<app:page titreecran="Administration des éditions" menu="true" responsive="${responsive}">

    <app:chemins action="composantedition/admin/flux.ex" responsive="${responsive}">
        <app:chemin action="accueil.ex" title="Retour à l'accueil" responsive="${responsive}">Accueil</app:chemin>
        <app:chemin title="Éditions" responsive="${responsive}">Éditions</app:chemin>
        <app:chemin title="Administration" responsive="${responsive}">Administration</app:chemin>
        <app:chemin title="Administration des éditions" responsive="${responsive}">Administration des éditions</app:chemin>
    </app:chemins>

    <app:form method="post" action="composantedition/admin/flux.ex" formid="rechercheedition" formobjectname="administrationeditionsform"
        onsubmit="return submit_form(500)" formboxid="donnees" formboxclass="donnees sousonglets" responsive="${responsive}"
        rwdformclass="form-horizontal">

        <app:input attribut="uid" libelle="Code utilisateur :" maxlength="30" size="30" requis="false" labelboxwidth="18%"
            inputboxwidth="37%" compboxwidth="3%"
            consigne="Saisissez l'identifiant (uid) de l'utilisateur qui a demandé la production de l'édition" responsive="${responsive}"
            bootstraplabelclass="btmodel_third" bootstrapinputclass="btmodel_third" bootstrapbodyclass="btmodel_third_sm_right">
            <span/>
        </app:input>
        <app:submit transition="selectonuid" label="Rechercher par code utilisateur"
            title="Recherche toutes les éditions produites par l'utilisateur identifié par le code utilisateur saisi."
            responsive="${responsive}" />

        <app:input size="10" maxlength="10" attribut="dateDemande" libelle="Date de demande :" requis="false" labelboxwidth="18%"
            inputboxwidth="7%" compboxwidth="33%" placeholder="JJ/MM/AAAA" consigne="Sélectionnez une date de demande de l'édition" 
            responsive="${responsive}" >                
            <app:calendar />    
        </app:input>             
        <app:submit transition="selectondatedemande" label="Rechercher par date de demande"
            title="Recherche toutes les éditions qui ont été demandées à la date sélectionnée." responsive="${responsive}" />

        <app:input attribut="editionUuid" libelle="Identifiant de l'édition :" maxlength="50" size="50" requis="false" labelboxwidth="18%"
            inputboxwidth="37%" compboxwidth="3%" consigne="Saisissez l'identifiant de l'édition" responsive="${responsive}"
            bootstraplabelclass="btmodel_third" bootstrapinputclass="btmodel_third">
            <span/>
        </app:input>
        <app:submit transition="selectoneditionuuid" label="Rechercher par l'identifiant de l'édition"
            title="Rechercher l'édition associée à l'identifiant saisi." responsive="${responsive}" />            

        <app:select attribut="profil" itemslist="${listedesprofils}" libelle="Nom du profil :" label="nomProfil" theme="V" 
            requis="false" defautitem="true" labelboxwidth="18%" inputboxwidth="37%" compboxwidth="3%" 
            consigne="Sélectionner un profil" responsive="${responsive}" bootstraplabelclass="btmodel_third"
            bootstrapinputclass="btmodel_third">
            <span style="height:25px"/>
        </app:select>
        <app:submit transition="selectonprofil" label="Rechercher par profil"
            title="Recherche toutes les éditions qui sont accessibles pour le profil sélectionné." responsive="${responsive}" />            

        <app:select attribut="etat" itemslist="${listedesetatspossibles}" libelle="État de l'édition :" theme="V" 
            requis="false" defautitem="true" labelboxwidth="18%" inputboxwidth="37%" compboxwidth="3%"
            consigne="Sélectionnez un état" responsive="${responsive}" bootstraplabelclass="btmodel_third"
            bootstrapinputclass="btmodel_third">
            <span/>
        </app:select>
        <app:submit transition="selectonetat" label="Rechercher par état"
            title="Recherche toutes les éditions qui sont dans l'état sélectionné." responsive="${responsive}" />            

        <app:select attribut="beanEditionId" itemslist="${listedesbeaneditionid}" libelle="Type d'édition :" theme="V"
            requis="false" defautitem="true" labelboxwidth="18%" inputboxwidth="37%" compboxwidth="3%"
            consigne="Sélectionnez un type d&apos;édition" responsive="${responsive}" bootstraplabelclass="btmodel_third"
            bootstrapinputclass="btmodel_third">
            <span/>
        </app:select>
        <app:submit transition="selectonbeaneditionid" label="Rechercher par type d'édition"
            title="Recherche toutes les éditions d'un type donné." responsive="${responsive}" />

        <fieldset style="width:80%">
            <legend>Période de la demande</legend>        
            <app:input attribut="debutPeriodeDemande" libelle="Début période :" maxlength="10" size="10" requis="false" inputboxwidth="8.8%"
                labelboxwidth="10.2%" consigne="Sélectionnez le début de la période de recherche" placeholder="JJ/MM/AAAA" 
                responsive="${responsive}">
                <app:calendar />                    
            </app:input>
        
            <app:input attribut="finPeriodeDemande" libelle="Fin période :" maxlength="10" size="10" requis="false" labelboxwidth="10.2%"
                inputboxwidth="8.8%" compboxwidth="41.3%" consigne="Sélectionnez la fin de la période de recherche" placeholder="JJ/MM/AAAA"
                responsive="${responsive}" >
                <app:calendar />
            </app:input>
            <app:submit transition="selectonperiodedatedemande" label="Rechercher par période de demande"
                title="Recherche toutes les éditions qui ont été demandées durant la période sélectionnée." responsive="${responsive}" />                                   
        </fieldset>
        
        <app:boxboutons>
            <c:choose>
                <c:when test="${responsive != 'true'}">
                    <app:submit label="Retourner à l'accueil" transition="annuler" title="Retourner à l'accueil" />
                </c:when>
                <c:otherwise>
                    <app:submit label="Retourner à l'accueil" transition="annuler" responsive="${responsive}" 
                        title="Retourner à l'accueil" />
                </c:otherwise>
            </c:choose>
        </app:boxboutons>

    </app:form>
    <app:disperror var="${erreurentreflux}" />
</app:page>