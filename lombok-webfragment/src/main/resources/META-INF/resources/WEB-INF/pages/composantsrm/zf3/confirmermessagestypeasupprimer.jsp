<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/tags/html/includes.tagf" %>

<!--  Page confirmermessagestypeasupprimer.jsp -->

<app:page titreecran="Selectionner un message type "
          titrecontainer=" Confirmer la suppression de messages type" 
          menu="true" 
          donneescontainerboxclass="tablecontainer">
          
<p>Les données suivantes vont être supprimées</p>
<app:form method='POST' 
		  formobjectname='recherchemessagestypesform'
          action='zf3/flux.ex'>
 
<ec:table items="listmessagestypesel"   form="recherchemessagestypeform"
		var="cont"
		autoIncludeParameters="false"
		filterable="false" 
		sortable="false"
		showPagination="false"
		showStatusBar="false"
		locale="fr_FR"	
		theme="eXtremeTable centerTable"
		width="50%" 	 
		>
   		<ec:row highlightRow="false">
			<ec:column property="libelleMessageType" title="libelle du message" width="15%" sortable="true" filterable="true" > </ec:column>
			 </ec:row>
</ec:table>
 
<app:boxboutons>
	<app:submit inputclass="annul" label="Annuler" transition="_eventId_Annuler"/>
	<app:submit label="Supprimer" transition="_eventId_Supprimer"/>
</app:boxboutons>

</app:form>
</app:page>
