<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/tags/html/includes.tagf"%>

<!--  Page affichervaleursfiltres.jsp -->

<app:page titreecran="Satelit bienvenue" titrecontainer="Affichage caractéristiques utilisateur >> Caractéristiques habilitation"
    menu="true">

    <app:form action="composantinfoutil/flux.ex" formboxclass="donnees" formobjectname="affichagecaracteristiquesutilisateurform">

        <ec:table items="listevaleursfiltres" action="${pageContext.request.contextPath}composantinfoutil/flux.ex" tableId="filtres"
            autoIncludeParameters="false" form="formulaire" locale="fr_FR" view="cphtml" filterable="false" showPagination="false"
            showExports="false" var="row" sortRowsCallback="cp" filterRowsCallback="cp">
            <ec:row highlightRow="true">
                <ec:column property="valeur" title="Valeurs du filtre" />
            </ec:row>
        </ec:table>


        <app:boxboutons divboxboutonsclass="boxboutons">
            <app:submit label="Retour détail des filtres" transition="RetourFiltres" id="enterButton" />
        </app:boxboutons>
    </app:form>
</app:page>