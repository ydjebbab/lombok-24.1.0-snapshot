function addValidation(elementId,widgetType,invalidMessage,missingMessage,required,regExp){
		 Spring.addDecoration(new Spring.ElementDecoration({
	         elementId : elementId,
	         widgetType : widgetType,
	         widgetAttrs : {invalidMessage   : invalidMessage ,missingMessage : missingMessage ,
	         required :true ,regExp : regExp }}));  
	}


function addAllValidation(elementId,event){
	 Spring.addDecoration(new Spring.ValidateAllDecoration({ elementId:'enterButton', event:'onclick' }));
}