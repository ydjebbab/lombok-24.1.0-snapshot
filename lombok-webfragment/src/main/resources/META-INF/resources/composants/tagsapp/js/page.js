 $(document).ready(function() {
	
    var thisPageOffset = 0;
	var maxDecrement = -5;
	var maxIncrement = 5;
	var $affectedElements = $("body, a, label, input, th, td, div, p, ul, li, select, form, textarea, caption, button, option, span"); // Can be extended, ex. $("div, p, span.someClass")

	// Storing the original size in a data attribute so size can be reset
	$affectedElements.each(function() {
		var $this = $(this);		
		$this.data("orig-size", $this.css("font-size"));
	});

	// Reset Font Size
	$("#resetfont").click(function() {
		$affectedElements.each( function(){
	        var $this = $(this);
	        $this.css("font-size", $this.data("orig-size"));	        
		});
		thisPageOffset = 0;
		setStorage("fontSizeOffset", thisPageOffset);
		return false;
	});
	// Increase Font Size
	$("#incfont").click(function() {
		if (thisPageOffset + 1 <= maxIncrement) {
			changeFontSize(++thisPageOffset);
			setStorage("fontSizeOffset", thisPageOffset);
		}
		return false;
	});
	// Decrease Font Size
	$("#decfont").click(function() {
		if (thisPageOffset - 1 >= maxDecrement)
		{
			changeFontSize(--thisPageOffset);
			setStorage("fontSizeOffset", thisPageOffset);
		}
		return false;
	});
	
	function changeFontSize(offset) {
	    $affectedElements.each(function() {
	        var $this = $(this);
	        $this.css("font-size", parseInt($this.data("orig-size")) + offset);
	    });
	}
	
	/* init font size */
	if (getStorage("fontSizeOffset") != undefined && getStorage("fontSizeOffset") != "") {
		thisPageOffset = parseInt(getStorage("fontSizeOffset"));
		changeFontSize(parseInt(getStorage("fontSizeOffset"))); 
	}
	 
	/*
	 * script jquery pour permettre la navigation au clavier sur le menu
	 */
       $("div.nav li a").focus(function(event){
    	                         $("div.nav ul, div.nav li").removeClass("focus hover");
                                 $(this).parents("div.nav ul, div.nav li").addClass("focus hover");
                               }).hover(function(event){
                            	   $("div.nav ul, div.nav li").removeClass("focus hover");
                                   $(this).parents("ul").addClass("focus");
                                   $(this).parents("li").addClass("hover");
                                 });
       
       $("div.nav").hover(  function(){},                		   
    		                function(){ $("div.nav ul, div.nav li").removeClass("focus hover");
    		                }
        );
                
       
    /* Add conditional classname based on support */
       $('html').addClass($.fn.details.support ? 'details' : 'no-details');

       /* Show a message based on support */
/*
 * $('body').prepend($.fn.details.support ? 'Native support detected; the plugin
 * will only add ARIA annotations and fire custom open/close events.' :
 * 'Emulation active; you are watching the plugin in action!');
 */       
       /*
		 * Emulate <details> where necessary and enable open/close event
		 * handlers
		 */
       $('details').details();
       
       
   /* theme roller */   
    
   
     $("#custom").change(function(){
   		$("link.ui-theme").remove();
   		
   		if($(this).val() != ""){   			
   			var cssLink = $('<link href="'+composantsrepertoire+'tagsapp/css/themes/'+$(this).val()+'.css?'+(new Date().getTime())+'" type="text/css" rel="Stylesheet" class="ui-theme" />');
   	   		$("head").append(cssLink);
   	   		log(cssLink);
   		}   		   		
   		
   		if(supports_local_storage()){
   			localStorage["custom-theme"] = $(this).val();
   		}
     });
       
       
    /* init custom */
    if(supports_local_storage() && localStorage["custom-theme"] != undefined){		
		$("#custom").val(localStorage["custom-theme"]).change();
	} 
   
    
    $("[id$='table_data']").each(function () {
        $('th').each(function () {
            var idColonne = $(this).attr('id');
            var idTemp=idColonne;            
            var  idFiltre= idTemp.replace("_c_", "_f_");            
            var width=$("#"+idColonne).width();            
            $("#"+idFiltre+"_tdfilter").width(width);           
        });
    });
        
    $("[id$='_Link']").focus(function(){
    	$("#menu_raccourcis").removeClass("menuhidden");    	
    });    
    
            
});
 
/* html5 local storage */ 
function supports_local_storage() {
	try {
		return ('localStorage' in window && window['localStorage'] !== null);
	} catch(e) {
		return false;
	}
}

function getChildId(){
	return jQuery("children",this).attr('id');	
}


function IE_CorrectAlpha_PNG(){
	for(i=0; i<document.images.length; i++){
		img    = document.images[i];
		imgExt  = img.src.substring(img.src.length-3, img.src.length);
		imgExt  = imgExt.toUpperCase();
		if (imgExt == "PNG"){
			imgID    = (img.id) ? "id='" + img.id + "' " : "";
			imgClass= (img.className) ? "class='" + img.className + "' " : "";
			imgTitle= (img.title) ? "title='" + img.title + "' " : "title='" + img.alt + "' ";
			imgStyle= "display:inline-block;" + img.style.cssText;
			if (img.align == "left") { imgStyle = "float:left;"  + imgStyle; } else if (img.align == "right"){ imgStyle = "float:right;" + imgStyle; }
			if (img.parentElement.href)   { imgStyle = "cursor:hand;" + imgStyle; }       
			strNewHTML    = '<span '+imgID+imgClass+imgTitle+' style="width:'+img.width+'px; height:'+img.height+'px;'+imgStyle+';'+'filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src=\''+img.src+'\', sizingMethod=\'scale\');"pascontent/span>';
			img.outerHTML = strNewHTML;
			i = i-1;
		}
	}
}

function setStorage(_name,_value){
	if(supports_local_storage()){
		localStorage[_name] = _value;
	}else{
		setCookie(_name, _value);
	}
}

function getStorage(_name){
	if(supports_local_storage()){
		return localStorage[_name];
	}else{
		return getCookie(_name);
	}
}


function supports_local_storage() {
	try {
		return ('localStorage' in window && window['localStorage'] !== null);
	} catch(e) {
		return false;
	}
}

function setCookie(c_name, value, exdays) {
	
	if(exdays == null || exdays == undefined){
		exdays = 365*100;
	}
	
	var exdate = new Date();
	exdate.setDate(exdate.getDate() + exdays);
	var c_value = escape(value) + ((exdays == null) ? "" : "; expires=" + exdate.toUTCString());
	document.cookie = c_name + "=" + c_value;
}

function getCookie(c_name) {
	var i, x, y, ARRcookies = document.cookie.split(";");
	for ( i = 0; i < ARRcookies.length; i++) {
		x = ARRcookies[i].substr(0, ARRcookies[i].indexOf("="));
		y = ARRcookies[i].substr(ARRcookies[i].indexOf("=") + 1);
		x = x.replace(/^\s+|\s+$/g, "");
		if (x == c_name) {
			return unescape(y);
		}
	}
}

function log(logtext){
	try{
		if(typeof(console) != undefined){
			console.log(logtext);
		}		
	}catch(e) {
		return false;
	}
}