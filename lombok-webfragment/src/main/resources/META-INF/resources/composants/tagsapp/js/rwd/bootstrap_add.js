if (navigator.userAgent.match(/IEMobile\/10\.0/)) {
	// Copyright 2014-2015 Twitter, Inc.
	// Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
  var msViewportStyle = document.createElement('style')
  msViewportStyle.appendChild(
    document.createTextNode(
      '@-ms-viewport{width:auto!important}'
    )
  )
  document.querySelector('head').appendChild(msViewportStyle)
}


$(function() {

	var dgfip_debug = false; // option affichage de debug des msg dgfip

	var dgfip_responsive = {};
	dgfip_responsive.detect_device = {};

	// --------- detection mobile avec mdetect.js --------------------
	dgfip_responsive.detect_device.is_mobile = false;
	if (MobileEsp.isIphone || MobileEsp.isAndroid || MobileEsp.isAndroidPhone
			|| MobileEsp.isIphone || MobileEsp.isTierIphone
			|| MobileEsp.isTierTablet || MobileEsp.isMobilePhone
			|| MobileEsp.isTierGenericMobile) {

		dgfip_responsive.detect_device.is_mobile = true;
	}

	/* ---------- affichage de debug ---------------------- */
	var dgfip_console = function(msg) {

		if (!dgfip_debug) {
			return;
		} else if (dgfip_responsive.detect_device.is_mobile) {
			alert(msg);
		} else {
			console.log(msg);
		}

	}

	// -------------------------------------------------------------------------------
	// --------------- MENU RESPONSIVE
	// -----------------------------------------------
	// -------------------------------------------------------------------------------
	// ---- design patterns used : singleton + namespace
	// see : http://addyosmani.com/resources/essentialjsdesignpatterns/book/
	dgfip_responsive.adapt_header_menu = (function() {

		var instance;

		// id_menu pour l'optimisation de selection dans le DOM avec jquery
		function init(id_bandeau, id_menu) {

			var $id_menu = "#" + id_menu;
			var $id_bandeau = "#" + id_bandeau;
			var $toggle_header_menu = $id_menu + " .toggle_header_menu";
			var $header_nav_menu = $id_menu + " .header_nav_menu";
			var $header = "header";
			var menu_state_vert = "hide";
			var menu_state = "horizontal";
			var minWidth = "200px";
			var total_menu_width = 0;
			var item_max_width = 0;
			var bandeau_width = 0;
			var very_small_screen_limit = 480;
			var small_screen_limit = 748;
			var min_height_bandeau = 75;
			var margin_after_bandeau = 20;

			$($toggle_header_menu).hide();

			/*
			 * ---------------- fonction publique appelee au debut et a chaque
			 * redimensionnement ----------
			 */
			var compute_ = function() {

				dgfip_console(" adapt content for rwd largeur:"
						+ parseInt($($toggle_header_menu).css("width"))
						+ "  menu width:" + total_menu_width);

				// adapte la taille du bandeau pour le header action
				// $($id_bandeau).css("min-height", Math.max(
				// parseInt($("#header-action").css("height")) + 10,
				// min_height_bandeau));

				// si la largeur du menu depasse celle du bandeau superieur on
				// switche en menu vertical

				bandeau_width = parseInt($($toggle_header_menu).width());

				if (bandeau_width <= total_menu_width) {

					if (menu_state === "horizontal") {
						$($toggle_header_menu).show();
						// enregistre l'etat vertical
						menu_state = "vertical";
						adapt_menu();
						show_vertical_menu();
					} else {
						adapt_menu();
					}
					$($id_menu).trigger("/menu_orientation", null); // publish
																	// pattern
					check_line_limit();

				} else {

					if (menu_state === "vertical") {

						hide_menu_items();
						show_horizontal_menu();

						$($id_menu).trigger("/menu_orientation", null); // publish
																		// pattern
						$($toggle_header_menu).hide();
						$($header_nav_menu).show();
					}
					// enregistre l'etat vertical
					menu_state = "horizontal";
				}

				// settimeout car il faut attendre le trigger /menu_orientation
				setTimeout(function() {
					$($id_menu).trigger("/size_changed", null);
				}, 10); // publish pattern

			}; /*
				 * ---------------- compute_ end
				 * -------------------------"------------------
				 */

			/*
			 * function publique : dit si le menu est deroulé (all) ou non
			 * (main)
			 */
			var get_type_ = function() {
				return menu_state_vert;
			}

			/* function publique : donne la hauteur du menu */
			var get_height_ = function() {
				return parseInt($($header).height());
			}

			// ========================= private functions
			// =======================================*/

			// calcul de la largeur du menu
			var get_menu_width = function() {
				var total = 0;
				$($header_nav_menu + " > ul > li").each(function() {
					total += parseInt($(this).css("width"));
				});
				return total;
			};

			// calcul de largeur max des items
			var get_item_max_width = function() {
				var max = 0;
				$($header_nav_menu + " > ul > li").each(function() {
					max = Math.max(max, parseInt($(this).css("width")));
				});
				return max + "px";
			};

			// switch entre les differentes options du menu vertical
			var toggle_menu = function() {
				var state = menu_state_vert;
				switch (state) {
				case "all":
				case "main":
					menu_state_vert = "hide";
					break;
				case "hide":
					if (bandeau_width < small_screen_limit) {
						menu_state_vert = "all";
					} else {
						menu_state_vert = "main";
					}
					break;
				}
				adapt_menu();
			}

			// on met a jour le menu
			var adapt_menu = function() {
				$($header_nav_menu + " ul  ul").css("border", "");
				$($header_nav_menu + " ul li > ul").hide();

				if (bandeau_width < small_screen_limit) {
					menu_state_vert = (menu_state_vert !== "hide") ? "all"
							: "hide";
				} else {
					menu_state_vert = (menu_state_vert !== "hide") ? "main"
							: "hide";
				}

				switch (menu_state_vert) {
				case "all":
					set_all_menu();
					$($header_nav_menu).show();
					break;
				case "main":
					set_main_menu();
					$($header_nav_menu).show();
					break;
				case "hide":
					$($header_nav_menu).hide();
					break;
				}
			};

			// affichage du menu vertical
			var show_vertical_menu = function() {

				$(".selected").removeClass("selected");
				$($header_nav_menu + " ul > li > ul ").show();
				$($header_nav_menu).css("width", "25%");

				// modification des proprietes css pour obtenir le menu vertical
				$($header_nav_menu + " > ul").css("right", "0").css("float",
						"left").css("width", "100%");
				$($header_nav_menu + " > ul > li").css("left", "0").css(
						"width", "100%");
				$($header_nav_menu + " ul").css("float", "left").css(
						"position", "initial");
				$($header_nav_menu + " ul ul").css("margin-left", "25px");
				$($header_nav_menu + " ul li").css("clear", "both");

				if (menu_state_vert === "main") {
					set_main_menu();
				} else {
					set_all_menu();
				}

			};

			// menu vertical flottant a gauche
			var set_main_menu = function() {
				$($header_nav_menu + " ul li > ul").hide();
				$($header_nav_menu).css("float", "left").css("position",
						"absolute").css("top", get_height_());
				$($id_menu + " > ul").css("float", "left").css("left", "0")
						.css("right", "0");
				$($id_menu + " > ul > li").css("float", "left")
						.css("left", "0").css("right", "0");
			};

			// menu vertical complet (les données sont affichées en bas de ce
			// menu)
			var set_all_menu = function() {
				$($header_nav_menu + " ul li > ul").show();
				$($header_nav_menu).css("float", "").css("position", "").css(
						"top", "").css("left", "");
				$($id_menu + " > ul").css("float", "").css("left", "").css(
						"right", "");
				$($id_menu + " > ul > li").css("float", "").css("left", "")
						.css("right", "");
			};

			// affichage du menu horizontal
			var show_horizontal_menu = function() {

				$($header_nav_menu).hide();
				$($header_nav_menu + " ul li > ul").hide();
				hide_menu_items();

				$($header_nav_menu).css("float", "").css("position", "").css(
						"top", "").css("left", "");

				// annule la limite de longueur de la ligne
				$($header_nav_menu).css("width", "");
				$($header_nav_menu + " ul").css("width", "");
				$($header_nav_menu + " ul a").css("white-space", "");

				// on remet les proprietes css d'origine pour le menu horizontal
				$($header_nav_menu + " > ul").css("width", "").css("right", "")
						.css("float", "");
				$($header_nav_menu + " > ul > li").css("width", "").css("left",
						"");
				$($header_nav_menu + " ul li").css("clear", "");
				$($header_nav_menu + " ul ul").css("margin-left", "");
				$($header_nav_menu + " ul").css("float", "")
						.css("position", "");
				$($header_nav_menu + " > ul").css("position", "");

			};

			// pour les petits ecrans la longueur de la ligne de menu est
			// limitée
			var check_line_limit = function() {
				if (bandeau_width <= very_small_screen_limit) {
					$($header_nav_menu + " ul a").css("white-space", "initial");
					if (menu_state_vert === "all") {
						$($header_nav_menu + " ul ul").css("min-width",
								minWidth);
					}
				} else {
					// annule la limite de longueur de la ligne
					$($header_nav_menu + " ul a").css("white-space", "");
				}
			}

			// sur un device mobile, l'orientation va changer la taille en
			// largeur du menu
			var doOnOrientationChange = function() {
				dgfip_console(" inner width=" + $(window).innerWidth()
						+ " height=" + $(window).innerHeight());

				compute_();
			}

			// initialisation
			item_max_width = get_item_max_width();
			total_menu_width = get_menu_width();

			var msg = "device is mobile ? ";
			msg += dgfip_responsive.detect_device.is_mobile ? "true" : "false";
			dgfip_console(msg);

			// si detection d'un device mobile
			if (dgfip_responsive.detect_device.is_mobile) {
				doOnOrientationChange();
				$(window).on("orientationchange", doOnOrientationChange);
			}

			// affiche ou non le menu (cas vertical)
			$($toggle_header_menu + " .button_menu-toggle").click(function() {
				toggle_menu();
				compute_();
			});

		
		
			$($header_nav_menu).children().children().addClass("mstatic");

			var affiche_menu_items = function(item) {
				// p = element li superieur
				var p = item.parent();

				// on affiche les enfants de p si ils existent
				p.children().show();
				p.children().children().show();

				// on cache les descendances des freres de p du meme niveau
				p.siblings().find("ul").hide();
				p.siblings().find("li").hide();
			}

			var hide_menu_items = function() {
				$($header_nav_menu).find("ul, li").hide();
				$($header_nav_menu).children().show().children().show();
			}
			hide_menu_items();

			$($header_nav_menu + " a.nourl").click(function() {
				affiche_menu_items($(this));
				var p = $(this).parent();
				if (p.hasClass("mstatic")) {
					if (p.hasClass("selected")) {
						hide_menu_items();
						$(this).parent().siblings().removeClass("selected");
					}
					p.toggleClass("selected");
				}
			});

			$($header_nav_menu + " a.nourl").hover(function() {
				affiche_menu_items($(this))
			});

			$($header_nav_menu).hover(null, function() {
				hide_menu_items();
			});

			// si on detecte un changement de la taille de l'element
			// id=header_menu,
			// alors on appelle a nouveau la fonction d'adaptation du menu
			new ResizeSensor($($id_bandeau), function() {
				dgfip_console("resize sensor detected");
				compute_();
			});

			return {
				compute : compute_,
				get_type : get_type_,
				get_height : get_height_,
			};

		}
		;

		return {
			// Get the Singleton instance if one exists or create one if it
			// doesn't
			getInstance : function(id_bandeau, id_menu) {
				if (!instance) {
					instance = init(id_bandeau, id_menu);
				}
				return instance;
			}
		};

	})();

	// --------------- FIN MENU RESPONSIVE
	// -----------------------------------------------

	// ----------------- height equalization for horizontal menu in donnees
	// --------------------------
	// js pour egaliser les hauteurs de blocs a la volee, car directive css
	// impossible
	// les autres ne fonctionnent pas ou trop de contraintes :
	// http://www.minimit.com/articles/solutions-tutorials/bootstrap-3-responsive-columns-of-same-height
	dgfip_responsive.bootstrap_height_equalizer = function() {

		$(".height_eq_item").css("height", "initial");

		$(".height_equalizer").each(function() {
			var self = $(this);
			self.find(".height_eq_item").addClass("do_equal");
			var maxHeight = Math.max.apply(null, $(".do_equal").map(function() {
				return $(this).height();
			}).get());
			$(".do_equal").height(maxHeight);
			self.find(".height_eq_item").removeClass("do_equal");
		});
	};

	$("#header_menu").on("/size_changed", function() {// subscribe pattern
		dgfip_responsive.bootstrap_height_equalizer();
	});

	dgfip_responsive.bootstrap_height_equalizer();

	// initialisation : appel de la fonction d'adaptation du menu
	dgfip_responsive.header_menu = dgfip_responsive.adapt_header_menu
			.getInstance("bandeau", "header_menu");
	dgfip_responsive.header_menu.compute();

	$(".button_menu-toggle").on("click", function() {
		$("#header_menu").trigger("/menu_orientation", null); // publish
																// pattern
	});

	// initialisation du fil d'ariane
	if ($("#fil_ariane").length) {
		$("#fil_ariane").rcrumbs({
			nbFixedCrumbs : 1
		});
	}

	// ectable pagination
	$("div.toolbar").each(function() {
		var scrollValue = 0;
		$(this).find('.pageScroll li').each(function(index) {
			scrollValue += $(this).width();
			if ($(this).hasClass("currentpage")) {
				return false;
			}
		});

		$(this).find('.pageScroll').animate({
			scrollLeft : scrollValue - $(".pageScroll").width() / 2
		}, 0, null);
	});

});
