package fr.gouv.finances.lombok.mvc;

import fr.gouv.finances.lombok.test.pojo.AbstractCorePojoTest;

/**
 * Tests unitaires du POJO ResponseList.
 * @see fr.gouv.finances.lombok.mvc.ResponseList
 *
 * @author Christophe Breheret-Girardin
 */
public class ResponseListTest extends AbstractCorePojoTest<ResponseList>
{
    /**
     * Constructeur.
     */
    public ResponseListTest()
    {
        super();
    }
}