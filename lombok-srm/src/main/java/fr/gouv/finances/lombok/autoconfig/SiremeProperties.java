package fr.gouv.finances.lombok.autoconfig;

import org.springframework.boot.context.properties.ConfigurationProperties;

import fr.gouv.finances.lombok.autoconfig.LombokComposantProperties;

/**
 * 
 * SiremeProperties.java
 * @author celinio fernandes
 * Date: Feb 19, 2020
 */
@ConfigurationProperties(prefix="lombok.composant.sireme")
public class SiremeProperties  extends LombokComposantProperties
{
    @Override
    public String toString()
    {
        return "SiremeProperties [inclus=" + inclus + "]";
    }



    
}
