package fr.gouv.finances.lombok.autoconfig;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

/**
 * Classe de configuration qui permet de conditionner les ressources à importer
 * 
 * @author celfer Date: 6 févr. 2020
 */
@Configuration
// matchIfMissing : (defaut : false)
// si true alors si la propriété lombok.composant.sireme n'est pas définie, le bean est chargé quand même, les imports
// sont fait
// si false alors si la propriété lombok.composant.sireme n'est pas définie, le bean n'est pas chargé et les imports ne
// sont pas fait
@ConditionalOnProperty(name = "lombok.composant.sireme.inclus", havingValue = "true")
@ImportResource({"classpath*:conf/applicationContext-srm-service.xml", "classpath*:conf/applicationContext-srm-dao.xml"})
@ComponentScan(basePackages = {"fr.gouv.finances.lombok.srm.jpa.dao.impl"})
@EnableConfigurationProperties(SiremeProperties.class)
public class SiremeApplicationConfig
{
    private static final Logger log = LoggerFactory.getLogger(SiremeApplicationConfig.class);

    @Autowired
    private SiremeProperties siremeProperties;

    @PostConstruct
    private void afficherProprietes()
    {

        if (log.isInfoEnabled())
        {
            log.info("Valeurs des propriétés du composant sireme : {}  ", siremeProperties);
        }
    }

}
