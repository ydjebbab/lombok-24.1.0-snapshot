/*
 * Copyright (c) 2017 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.cp.srm.service;

import java.util.List;

import fr.gouv.finances.lombok.securite.techbean.ApplicationAnnuaire;
import fr.gouv.finances.lombok.srm.bean.Profil;

/**
 * Interface ProfilService --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.13 $ Date: 22 dec. 2009
 */
public interface ProfilService
{

    /**
     * enregistre un profil.
     * 
     * @param pProfil le profil
     */
    void enregistrerProfil(Profil pProfil);

    /**
     * recherche dans l'annuaire des profils associes à une application.
     * 
     * @param applicationAnnuaire --
     * @return the list< profil>
     */
    List<Profil> rechercherProfilsAssociesAAppli(ApplicationAnnuaire applicationAnnuaire);

    /**
     * supprime un profil.
     * 
     * @param pProfil le profil
     */
    void supprimerProfil(Profil pProfil);

    /**
     * methode Rechercher tous les profils 
     *
     * @return list
     */
    List<Profil> rechercherTousLesProfils();

    /**
     * methode Rechercher profil par code 
     *
     * @param code DOCUMENTEZ_MOI
     * @return profil
     */
    Profil rechercherProfilParCode(String code);

}
