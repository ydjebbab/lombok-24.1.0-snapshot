/*
 * Copyright : Ministere du budget, des comptes publics et de la fonction publique        France
 *
 */
package fr.gouv.finances.cp.srm.mvc.webflow.action;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.Errors;
import org.springframework.webflow.action.FormAction;
import org.springframework.webflow.action.FormObjectAccessor;
import org.springframework.webflow.execution.Event;
import org.springframework.webflow.execution.RequestContext;
import org.springframework.webflow.execution.ScopeType;

import fr.gouv.finances.cp.srm.mvc.form.SuppressionMessagesForm;
import fr.gouv.finances.cp.srm.service.MessageService;
import fr.gouv.finances.lombok.apptags.util.CheckboxSelectUtil;
import fr.gouv.finances.lombok.util.exception.RegleGestionException;
import fr.gouv.finances.lombok.util.exception.VerrouillageOptimisteException;

/**
 * Classe SuppressionMessagesFormAction --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.17 $ Date: 22 dec. 2009
 */
public class SuppressionMessagesFormAction extends FormAction
{
    /** Initialisation de la journalisation. */
    protected static final Logger LOGGER = LoggerFactory.getLogger(SuppressionMessagesFormAction.class);

    /** messageserviceso. */
    private MessageService messageserviceso;

    public SuppressionMessagesFormAction()
    {
        super();
    }

    public SuppressionMessagesFormAction(Class arg0)
    {
        super(arg0);
    }

    /**
     * suppression effective des messages.
     * 
     * @param context
     *            --
     * @return the event
     */
    public Event executerSupprimerMessages(RequestContext context)
    {
        Event unEvent;

        // Lecture de la liste des messages e supprimer dans le formulaire
        SuppressionMessagesForm supp = (SuppressionMessagesForm) context
            .getFlowScope().getRequired(getFormObjectName(),
                SuppressionMessagesForm.class);
        List listemessagesasupprimer = supp.getListeDesMessagesASupprimer();

        try
        {
            messageserviceso.supprimerListeMessages(listemessagesasupprimer);
            unEvent = success();
        }
        catch (RegleGestionException rge)
        {
            LOGGER.debug("Exception détectée : {}", rge);
            Errors errors = new FormObjectAccessor(context).getFormErrors(
                getFormObjectName(), ScopeType.REQUEST);

            errors.reject(null, rge.getMessage());
            unEvent = result("incorrectdata");
        }
        catch (VerrouillageOptimisteException voe)
        {
            LOGGER.debug("Exception détectée : {}", voe);
            Errors errors = new FormObjectAccessor(context).getFormErrors(
                getFormObjectName(), ScopeType.REQUEST);

            errors.reject(null, voe.getMessage());
            unEvent = error();
        }

        CheckboxSelectUtil.supprimeDesElementsDansLeTableau(context, listemessagesasupprimer, "rechmess");

        return unEvent;
    }

    /**
     * Accesseur de l'attribut messageserviceso.
     * 
     * @return messageserviceso
     */
    public MessageService getMessageserviceso()
    {
        return messageserviceso;
    }

    /**
     * methode Initialiser formulaire : --.
     * 
     * @param context
     *            --
     * @return event
     */
    public Event initialiserFormulaire(RequestContext context)
    {
        Event unEvent;

        SuppressionMessagesForm suppressionMessagesForm;
        suppressionMessagesForm = (SuppressionMessagesForm) context
            .getFlowScope().getRequired(getFormObjectName());

        List listeAsupprimer = (List) CheckboxSelectUtil
            .extraitLesElementsSelectionnesDansLeFluxParent(context,
                "rechmess");

        if (!CheckboxSelectUtil.auMoinsUnElementSelectionne(listeAsupprimer))
        {
            unEvent = error();
        }
        else
        {
            suppressionMessagesForm
                .setListeDesMessagesASupprimer(listeAsupprimer);
            unEvent = success();
        }

        return unEvent;
    }

    /**
     * Modificateur de l'attribut messageserviceso.
     * 
     * @param messageserviceso valeur de messageserviceso
     */
    public void setMessageserviceso(MessageService messageserviceso)
    {
        this.messageserviceso = messageserviceso;
    }

}