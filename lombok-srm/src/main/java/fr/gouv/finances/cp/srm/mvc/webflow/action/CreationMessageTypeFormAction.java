/*
 * Copyright : Ministere du budget, des comptes publics et de la fonction publique        France
 *
 */
package fr.gouv.finances.cp.srm.mvc.webflow.action;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.Errors;
import org.springframework.webflow.action.FormAction;
import org.springframework.webflow.action.FormObjectAccessor;
import org.springframework.webflow.execution.Event;
import org.springframework.webflow.execution.RequestContext;
import org.springframework.webflow.execution.ScopeType;

import fr.gouv.finances.cp.srm.mvc.form.CreationMessageTypeForm;
import fr.gouv.finances.cp.srm.service.MessageTypeService;
import fr.gouv.finances.lombok.srm.bean.MessageType;
import fr.gouv.finances.lombok.util.exception.RegleGestionException;
import fr.gouv.finances.lombok.util.exception.VerrouillageOptimisteException;

/**
 * Classe de creation de messages types
 * 
 * @author amleplatinec
 */
public class CreationMessageTypeFormAction extends FormAction
{
    /** Initialisation de la journalisation. */
    protected static final Logger LOGGER = LoggerFactory.getLogger(CreationMessageTypeFormAction.class);

    /** messagetypeserviceso - MessageTypeService. */
    private MessageTypeService messagetypeserviceso;

    /**
     * Constructeur.
     */
    public CreationMessageTypeFormAction()
    {
        super();
    }

    /**
     * Constructeur.
     * 
     * @param arg0 ARG0
     */
    public CreationMessageTypeFormAction(Class arg0)
    {
        super(arg0);
    }

    /**
     * Creation d'un message type.
     * 
     * @param context
     *            lecontexte
     * @return unevenement
     * @throws desexceptions
     */

    public Event executerCreerMessageType(RequestContext context)
    {
        Event unEvent;
        CreationMessageTypeForm creationMessageTypeForm = (CreationMessageTypeForm) context
            .getFlowScope().getRequired(getFormObjectName(),
                CreationMessageTypeForm.class);
        MessageType messageType = creationMessageTypeForm.getMessagetypeform();

        try
        {
            messagetypeserviceso.enregistrerMessageType(messageType);
            unEvent = success();
        }
        catch (RegleGestionException rge)
        {
            LOGGER.debug("Exception détectée : {}", rge);
            Errors errors = new FormObjectAccessor(context).getFormErrors(
                getFormObjectName(), ScopeType.REQUEST);

            errors.reject(null, rge.getMessage());
            unEvent = result("incorrectdata");
        }
        catch (VerrouillageOptimisteException voe)
        {
            LOGGER.debug("Exception détectée : {}", voe);
            Errors errors = new FormObjectAccessor(context).getFormErrors(
                getFormObjectName(), ScopeType.REQUEST);

            errors.reject(null, voe.getMessage());

            unEvent = error();
        }

        return unEvent;
    }

    /**
     * Accesseur de messagetypeserviceso.
     * 
     * @return the messagetypeserviceso
     */
    public MessageTypeService getMessagetypeserviceso()
    {
        return messagetypeserviceso;
    }

    /**
     * Mutateur de messagetypeserviceso.
     * 
     * @param messagetypeserviceso messagetypeserviceso
     */
    public void setMessagetypeserviceso(MessageTypeService messagetypeserviceso)
    {
        this.messagetypeserviceso = messagetypeserviceso;
    }

    /**
     * Verifie existence du message type.
     * 
     * @param context le contexte
     * @return unevenement
     */

    public Event validerVerifierExistenceMessageType(RequestContext context)
    {
        Event unEvent;
        CreationMessageTypeForm creationMessageTypeForm = (CreationMessageTypeForm) context
            .getFlowScope().getRequired(getFormObjectName(),
                CreationMessageTypeForm.class);
        MessageType messagetype = creationMessageTypeForm.getMessagetypeform();

        try
        {
            messagetypeserviceso.verifierExistenceMessageType(messagetype
                .getLibelleMessageType());
            unEvent = success();
        }
        catch (RegleGestionException rge)
        {
            LOGGER.debug("Exception détectée : {}", rge);
            Errors errors = new FormObjectAccessor(context).getFormErrors(
                getFormObjectName(), ScopeType.REQUEST);

            errors.reject(null, rge.getMessage());
            unEvent = result("incorrectdata");
        }
        return unEvent;
    }

}
