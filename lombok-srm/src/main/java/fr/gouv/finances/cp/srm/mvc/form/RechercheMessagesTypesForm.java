/*
 * Copyright : Ministere du budget, des comptes publics et de la fonction publique        France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - srm
 *
 * fichier : RechercheMessagesTypesForm.java
 *
 */
package fr.gouv.finances.cp.srm.mvc.form;

import java.io.Serializable;
import java.util.List;

import fr.gouv.finances.lombok.srm.bean.Message;

/**
 * Class RechercheMessagesTypesForm --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.11 $ Date: 22 dec. 2009
 */
public class RechercheMessagesTypesForm implements Serializable
{

    private static final long serialVersionUID = 1L;

    /** libelle message type. */
    private String libelleMessageType;

    /** liste des messages type a supprimer. */
    private List<Message> listeDesMessagesTypeASupprimer;

    public RechercheMessagesTypesForm()
    {
        super();
    }

    /**
     * Accesseur de l attribut libelle message type.
     * 
     * @return libelle message type
     */
    public String getLibelleMessageType()
    {
        return libelleMessageType;
    }

    /**
     * Accesseur de l attribut liste des messages type a supprimer.
     * 
     * @return liste des messages type a supprimer
     */
    public List<Message> getListeDesMessagesTypeASupprimer()
    {
        return listeDesMessagesTypeASupprimer;
    }

    /**
     * Modificateur de l attribut libelle message type.
     * 
     * @param libelleMessageType
     *            le nouveau libelle message type
     */
    public void setLibelleMessageType(String libelleMessageType)
    {
        this.libelleMessageType = libelleMessageType;
    }

    /**
     * Modificateur de l attribut liste des messages type a supprimer.
     * 
     * @param listeDesMessagesTypeASupprimer
     *            le nouveau liste des messages type a supprimer
     */
    public void setListeDesMessagesTypeASupprimer(
        List<Message> listeDesMessagesTypeASupprimer)
    {
        this.listeDesMessagesTypeASupprimer = listeDesMessagesTypeASupprimer;
    }

}
