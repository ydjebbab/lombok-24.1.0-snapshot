/*
 * Copyright : Ministere du budget, des comptes publics et de la fonction publique        France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - srm
 *
 * fichier : RechercheMessagesTypesFormAction.java
 *
 */
package fr.gouv.finances.cp.srm.mvc.webflow.action;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.Errors;
import org.springframework.webflow.action.FormAction;
import org.springframework.webflow.action.FormObjectAccessor;
import org.springframework.webflow.execution.Event;
import org.springframework.webflow.execution.RequestContext;
import org.springframework.webflow.execution.ScopeType;

import fr.gouv.finances.cp.srm.mvc.form.RechercheMessagesTypesForm;
import fr.gouv.finances.cp.srm.service.MessageTypeService;
import fr.gouv.finances.lombok.apptags.util.CheckboxSelectUtil;
import fr.gouv.finances.lombok.util.exception.RegleGestionException;
import fr.gouv.finances.lombok.util.exception.VerrouillageOptimisteException;

/**
 * Class RechercheMessagesTypesFormAction --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.16 $ Date: 22 dec. 2009
 */
public class RechercheMessagesTypesFormAction extends FormAction
{
    protected static final Logger LOGGER = LoggerFactory.getLogger(RechercheMessagesTypesFormAction.class);

    /** messagetypeserviceso. */
    private MessageTypeService messagetypeserviceso;

    public RechercheMessagesTypesFormAction()
    {
        super();
    }

    public RechercheMessagesTypesFormAction(Class arg0)
    {
        super(arg0);
    }

    /**
     * *************************************************************************
     * recherche de tous les messages types en base ( attention message type pas
     * // lie e une application mais e toutes)
     * ************************************************************************.
     * 
     * @param context
     *            --
     * @return the event
     */

    public Event executerRechercherListeMessagesTypes(RequestContext context)
    {
        RechercheMessagesTypesForm rechercheMessagesTypesForm = (RechercheMessagesTypesForm) context
            .getFlowScope().getRequired(getFormObjectName(),
                RechercheMessagesTypesForm.class);
        String libelle = rechercheMessagesTypesForm.getLibelleMessageType();

        List listMessagesTypes = messagetypeserviceso
            .rechercherListeMessagesTypesParPartieLibelle(libelle);

        context.getFlowScope().put("listmessagestype", listMessagesTypes);
        // Sauvegarde de l'objet de parametrage des checkboxes dans le
        // FlowScope
        CheckboxSelectUtil
            .parametrerCheckboxes(context, "rechmessty")
            .utiliserRowid("id")
            .utiliserListeElements("listmessagestype")
            .utiliserMsgAucunEltSelectionne(
                "Auncun message type n'est selectionne");
        return success();
    }

    /**
     * suppression des messages types selectionnes dans tableau.
     * 
     * @param context
     *            --
     * @return the event
     */
    public Event executerSupprimerMessagesTypes(RequestContext context)
    {
        Event unEvent;
        List listemessagestypeasupprimer = (List) context.getFlowScope().get("listmessagestypesel");

        try
        {
            messagetypeserviceso.supprimerListeMessagesType(listemessagestypeasupprimer);
            unEvent = success();
        }
        catch (RegleGestionException rge)
        {
            LOGGER.debug("Exception détectée : {}", rge);
            Errors errors = new FormObjectAccessor(context).getFormErrors(
                getFormObjectName(), ScopeType.REQUEST);

            errors.reject(null, rge.getMessage());
            unEvent = result("incorrectdata");
        }
        catch (VerrouillageOptimisteException voe)
        {
            LOGGER.debug("Exception détectée : {}", voe);
            Errors errors = new FormObjectAccessor(context).getFormErrors(
                getFormObjectName(), ScopeType.REQUEST);

            errors.reject(null, voe.getMessage());
            unEvent = error();
        }

        CheckboxSelectUtil.supprimeDesElementsDansLeTableau(context,
            listemessagestypeasupprimer, "rechmessty");

        return unEvent;
    }

    /**
     * Accesseur de l attribut messagetypeserviceso.
     * 
     * @return messagetypeserviceso
     */
    public MessageTypeService getMessagetypeserviceso()
    {
        return messagetypeserviceso;
    }

    /**
     * methode Preparer rechercher liste messages types : --.
     * 
     * @param context
     *            --
     * @return event
     */
    public Event preparerRechercherListeMessagesTypes(RequestContext context)
    {
        List listMessagesTypes = messagetypeserviceso
            .rechercherListeMessagesTypes();

        context.getFlowScope().put("listMessagesTypes", listMessagesTypes);
        return success();
    }

    /**
     * mise en session des messages selectionnes.
     * 
     * @param context
     *            --
     * @return the event
     */
    public Event rechercherMessagesTypesSelectionnes(RequestContext context)
    {
        List listemessagestypeasupprimer = (List) CheckboxSelectUtil
            .extraitLesElementsSelectionnesDansLeFlux(context, "rechmessty");

        context.getFlowScope().put("listmessagestypesel",
            listemessagestypeasupprimer);
        return success();
    }

    /**
     * Modificateur de l attribut messagetypeserviceso.
     * 
     * @param messagetypeserviceso
     *            le nouveau messagetypeserviceso
     */
    public void setMessagetypeserviceso(MessageTypeService messagetypeserviceso)
    {
        this.messagetypeserviceso = messagetypeserviceso;
    }

    /**
     * methode Traiter checkboxes entre pages : --.
     * 
     * @param context
     *            --
     * @return event
     */
    public Event traiterCheckboxesEntrePages(RequestContext context)
    {
        CheckboxSelectUtil.traiterCheckboxesEntrePages(context, "rechmessty");
        return success();
    }

    /**
     * methode Traiter message erreur retourne : --.
     * 
     * @param context
     *            --
     * @return event
     */
    public Event traiterMessageErreurRetourne(RequestContext context)
    {
        CheckboxSelectUtil.traiterMessageErreurRetourne(context);
        return success();
    };
}
