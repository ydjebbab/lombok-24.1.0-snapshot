/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 */
package fr.gouv.finances.cp.srm.dao;

/**
 * Enumération pour manipuler les propriétés du bean Codique
 * 
 * @author fernandescn Date: 11 oct. 2018
 */
public enum ProprieteCodique
{

    /** Libellé codique */
    PROPRIETE_LIBELLE("libelleCodique");

    /** Nom de la propriété */
    private String nom;

    /**
     * Constructeur.
     *
     * @param libelleNom libellé du nom de la propriété
     */
    private ProprieteCodique(String libelleNom)
    {
        nom = libelleNom;
    }

    /**
     * Accesseur de nom
     *
     * @return nom
     */
    public String getNom()
    {
        return nom;
    }
}
