/*
 * Copyright (c) 2017 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.cp.srm.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import fr.gouv.finances.cp.srm.dao.ProfilDao;
import fr.gouv.finances.cp.srm.service.ProfilService;
import fr.gouv.finances.lombok.securite.techbean.ApplicationAnnuaire;
import fr.gouv.finances.lombok.securite.techbean.ProfilAnnuaire;
import fr.gouv.finances.lombok.srm.bean.Profil;
import fr.gouv.finances.lombok.util.base.BaseServiceImpl;

/**
 * Class ProfilServiceImpl --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.16 $ Date: 22 dec. 2009
 */
public class ProfilServiceImpl extends BaseServiceImpl implements ProfilService
{

    private ProfilDao profildao;

    public ProfilServiceImpl()
    {
        super();
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.cp.srm.service.ProfilService#enregistrerProfil(fr.gouv.finances.lombok.srm.bean.Profil)
     */
    @Override
    public void enregistrerProfil(Profil pProfil)
    {
        profildao.saveProfil(pProfil);
    }

    /**
     * Gets the profildao.
     *
     * @return the profildao
     */
    public ProfilDao getProfildao()
    {
        return profildao;
    }

    /**
     * 
     * (methode de remplacement)
     * {@inheritDoc}
     * @see fr.gouv.finances.cp.srm.service.ProfilService#rechercherProfilsAssociesAAppli(fr.gouv.finances.lombok.securite.techbean.ApplicationAnnuaire)
     */
    @Override
    public List<Profil> rechercherProfilsAssociesAAppli(
        ApplicationAnnuaire application)
    {

        Set setProfilsDecodes = application.getDetailProfilDecode();
        List<Profil> listProfils = new ArrayList<>();

        for (Iterator iter = setProfilsDecodes.iterator(); iter.hasNext();)
        {
            ProfilAnnuaire element = (ProfilAnnuaire) iter.next();

            Profil profil = profildao
                .findProfilParCode(element.getCodeProfil());
            if (profil != null)
            {
                listProfils.add(profil);
            }
        }
        return listProfils;
    }

    /** 
     * (methode de remplacement)
     * {@inheritDoc}
     * @see fr.gouv.finances.cp.srm.service.ProfilService#rechercherTousLesProfils()
     */
    @Override
    public List<Profil> rechercherTousLesProfils()
    {
        return profildao.findTousLesProfils();
    }

    /**
     * Sets the profildao.
     *
     * @param profildao the new profildao
     */
    public void setProfildao(ProfilDao profildao)
    {
        this.profildao = profildao;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.cp.srm.service.ProfilService#supprimerProfil(fr.gouv.finances.lombok.srm.bean.Profil)
     */
    @Override
    public void supprimerProfil(Profil pProfil)
    {
        profildao.deleteObject(pProfil);

    }

    /** 
     * (methode de remplacement)
     * {@inheritDoc}
     * @see fr.gouv.finances.cp.srm.service.ProfilService#rechercherProfilParCode(java.lang.String)
     */
    @Override
    public Profil rechercherProfilParCode(String code)
    {
        return profildao.findProfilParCode(code);
    }

}
