/*
 * Copyright (c) 2017 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.cp.srm.mvc.webflow.action;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.webflow.action.FormAction;
import org.springframework.webflow.action.FormObjectAccessor;
import org.springframework.webflow.execution.Event;
import org.springframework.webflow.execution.RequestContext;
import org.springframework.webflow.execution.ScopeType;

import fr.gouv.finances.cp.srm.mvc.form.GestionProfilForm;
import fr.gouv.finances.cp.srm.service.MessageService;
import fr.gouv.finances.cp.srm.service.ProfilService;
import fr.gouv.finances.lombok.apptags.util.CheckboxSelectUtil;
import fr.gouv.finances.lombok.securite.springsecurity.UtilisateurSecurityLombokContainer;
import fr.gouv.finances.lombok.securite.techbean.FiltreHabilitationAnnuaire;
import fr.gouv.finances.lombok.securite.techbean.HabilitationAnnuaire;
import fr.gouv.finances.lombok.securite.techbean.PersonneAnnuaire;
import fr.gouv.finances.lombok.securite.techbean.ValeurFiltreHabilitationAnnuaire;
import fr.gouv.finances.lombok.srm.bean.Profil;

/**
 * Class GestionApplicationFormAction .
 */
public class GestionProfilFormAction extends FormAction
{

    /** filtreANNUSIREME - String,. */
    private static String filtreANNUSIREME = "libcourtapp";

    /** profilserviceso - ProfilService,. */
    private ProfilService profilserviceso;

    /** messageserviceso - MessageService,. */
    private MessageService messageserviceso;

    public GestionProfilFormAction()
    {
        super();
    }

    /**
     * Constructeur de la classe CreationApplicationFormAction.java
     * 
     * @param arg0 ARG0
     */
    public GestionProfilFormAction(Class arg0)
    {
        super(arg0);

    }

    /**
     * methode Creer les profils : .
     *
     * @param context DOCUMENTEZ_MOI
     * @return application
     */
    public Event creerLesProfils(RequestContext context)
    {
        GestionProfilForm creationProfilForm = (GestionProfilForm) context
            .getFlowScope().getRequired(getFormObjectName(),
                GestionProfilForm.class);

        Set lesProfils = new HashSet();

        Event event = success();

        Profil profil1 = creationProfilForm.getProfil1();
        if (!StringUtils.isEmpty(profil1.getCodeProfil()))
        {
            String codeProfilConcatCodeAppli = "";
            codeProfilConcatCodeAppli = codeProfilConcatCodeAppli
                .concat(creationProfilForm.getProfil1()
                    .getCodeProfil());
            profil1.setCodeProfilConcatCodeAppli(codeProfilConcatCodeAppli);

            if (profilserviceso.rechercherProfilParCode(profil1.getCodeProfil()) == null)
            {
                profilserviceso.enregistrerProfil(profil1);
                lesProfils.add(profil1);
            }
            else
            {
                Errors errors = new FormObjectAccessor(context)
                    .getFormErrors(getFormObjectName(),
                        ScopeType.REQUEST);
                errors.reject(null, "Le profil existe déjà en base");
                event = error();
            }
        }

        Profil profil2 = creationProfilForm.getProfil2();
        if (!StringUtils.isEmpty(profil2.getCodeProfil()))
        {
            String codeProfilConcatCodeAppli = "";
            codeProfilConcatCodeAppli = codeProfilConcatCodeAppli
                .concat(creationProfilForm.getProfil2()
                    .getCodeProfil());
            profil2.setCodeProfilConcatCodeAppli(codeProfilConcatCodeAppli);
            if (profilserviceso.rechercherProfilParCode(profil2.getCodeProfil()) == null)
            {
                profilserviceso.enregistrerProfil(profil2);
                lesProfils.add(profil2);
            }
            else
            {
                Errors errors = new FormObjectAccessor(context)
                    .getFormErrors(getFormObjectName(),
                        ScopeType.REQUEST);
                errors.reject(null, "Le profil existe déjà en base");
                event = error();
            }
        }

        Profil profil3 = creationProfilForm.getProfil3();
        if (!StringUtils.isEmpty(profil3.getCodeProfil()))
        {
            String codeProfilConcatCodeAppli = "";
            codeProfilConcatCodeAppli = codeProfilConcatCodeAppli
                .concat(creationProfilForm.getProfil3()
                    .getCodeProfil());
            profil3.setCodeProfilConcatCodeAppli(codeProfilConcatCodeAppli);
            if (profilserviceso.rechercherProfilParCode(profil3.getCodeProfil()) == null)
            {
                profilserviceso.enregistrerProfil(profil3);
                lesProfils.add(profil3);
            }
            else
            {
                Errors errors = new FormObjectAccessor(context)
                    .getFormErrors(getFormObjectName(),
                        ScopeType.REQUEST);
                errors.reject(null, "Le profil existe déjà en base");
                event = error();
            }
        }

        Profil profil4 = creationProfilForm.getProfil4();
        if (!StringUtils.isEmpty(profil4.getCodeProfil()))
        {
            String codeProfilConcatCodeAppli = "";
            codeProfilConcatCodeAppli = codeProfilConcatCodeAppli
                .concat(creationProfilForm.getProfil4()
                    .getCodeProfil());
            profil4.setCodeProfilConcatCodeAppli(codeProfilConcatCodeAppli);
            if (profilserviceso.rechercherProfilParCode(profil4.getCodeProfil()) == null)
            {
                profilserviceso.enregistrerProfil(profil4);
                lesProfils.add(profil4);
            }
            else
            {
                Errors errors = new FormObjectAccessor(context)
                    .getFormErrors(getFormObjectName(),
                        ScopeType.REQUEST);
                errors.reject(null, "Le profil existe déjà en base");
                event = error();
            }
        }

        Profil profil5 = creationProfilForm.getProfil5();
        if (!StringUtils.isEmpty(profil5.getCodeProfil()))
        {
            String codeProfilConcatCodeAppli = "";
            codeProfilConcatCodeAppli = codeProfilConcatCodeAppli
                .concat(creationProfilForm.getProfil5()
                    .getCodeProfil());
            profil5.setCodeProfilConcatCodeAppli(codeProfilConcatCodeAppli);
            if (profilserviceso.rechercherProfilParCode(profil5.getCodeProfil()) == null)
            {
                profilserviceso.enregistrerProfil(profil5);
                lesProfils.add(profil5);
            }
            else
            {
                Errors errors = new FormObjectAccessor(context)
                    .getFormErrors(getFormObjectName(),
                        ScopeType.REQUEST);
                errors.reject(null, "Le profil existe déjà en base");
                event = error();
            }
        }

        Profil profil6 = creationProfilForm.getProfil6();
        if (!StringUtils.isEmpty(profil6.getCodeProfil()))
        {
            String codeProfilConcatCodeAppli = "";
            codeProfilConcatCodeAppli = codeProfilConcatCodeAppli
                .concat(creationProfilForm.getProfil6()
                    .getCodeProfil());
            profil6.setCodeProfilConcatCodeAppli(codeProfilConcatCodeAppli);
            if (profilserviceso.rechercherProfilParCode(profil6.getCodeProfil()) == null)
            {
                profilserviceso.enregistrerProfil(profil6);
                lesProfils.add(profil6);
            }
            else
            {
                Errors errors = new FormObjectAccessor(context)
                    .getFormErrors(getFormObjectName(),
                        ScopeType.REQUEST);
                errors.reject(null, "Le profil existe déjà en base");
                event = error();
            }
        }

        Profil profil7 = creationProfilForm.getProfil7();
        if (!StringUtils.isEmpty(profil7.getCodeProfil()))
        {
            String codeProfilConcatCodeAppli = "";
            codeProfilConcatCodeAppli = codeProfilConcatCodeAppli
                .concat(creationProfilForm.getProfil7()
                    .getCodeProfil());
            profil7.setCodeProfilConcatCodeAppli(codeProfilConcatCodeAppli);
            if (profilserviceso.rechercherProfilParCode(profil7.getCodeProfil()) == null)
            {
                profilserviceso.enregistrerProfil(profil7);
                lesProfils.add(profil7);
            }
            else
            {
                Errors errors = new FormObjectAccessor(context)
                    .getFormErrors(getFormObjectName(),
                        ScopeType.REQUEST);
                errors.reject(null, "Le profil existe déjà en base");
                event = error();
            }
        }

        Profil profil8 = creationProfilForm.getProfil8();
        if (!StringUtils.isEmpty(profil8.getCodeProfil()))
        {
            String codeProfilConcatCodeAppli = "";
            codeProfilConcatCodeAppli = codeProfilConcatCodeAppli
                .concat(creationProfilForm.getProfil8()
                    .getCodeProfil());
            profil8.setCodeProfilConcatCodeAppli(codeProfilConcatCodeAppli);
            if (profilserviceso.rechercherProfilParCode(profil8.getCodeProfil()) == null)
            {
                profilserviceso.enregistrerProfil(profil8);
                lesProfils.add(profil8);
            }
            else
            {
                Errors errors = new FormObjectAccessor(context)
                    .getFormErrors(getFormObjectName(),
                        ScopeType.REQUEST);
                errors.reject(null, "Le profil existe déjà en base");
                event = error();
            }
        }

        Profil profil9 = creationProfilForm.getProfil9();
        if (!StringUtils.isEmpty(profil9.getCodeProfil()))
        {
            String codeProfilConcatCodeAppli = "";
            codeProfilConcatCodeAppli = codeProfilConcatCodeAppli
                .concat(creationProfilForm.getProfil9()
                    .getCodeProfil());
            profil9.setCodeProfilConcatCodeAppli(codeProfilConcatCodeAppli);
            if (profilserviceso.rechercherProfilParCode(profil9.getCodeProfil()) == null)
            {
                profilserviceso.enregistrerProfil(profil9);
                lesProfils.add(profil9);
            }
            else
            {
                Errors errors = new FormObjectAccessor(context)
                    .getFormErrors(getFormObjectName(),
                        ScopeType.REQUEST);
                errors.reject(null, "Le profil existe déjà en base");
                event = error();
            }
        }

        Profil profil10 = creationProfilForm.getProfil10();
        if (!StringUtils.isEmpty(profil10.getCodeProfil()))
        {
            String codeProfilConcatCodeAppli = "";
            codeProfilConcatCodeAppli = codeProfilConcatCodeAppli
                .concat(creationProfilForm.getProfil10()
                    .getCodeProfil());
            profil10.setCodeProfilConcatCodeAppli(codeProfilConcatCodeAppli);
            if (profilserviceso.rechercherProfilParCode(profil10.getCodeProfil()) == null)
            {
                profilserviceso.enregistrerProfil(profil10);
                lesProfils.add(profil10);
            }
            else
            {
                Errors errors = new FormObjectAccessor(context)
                    .getFormErrors(getFormObjectName(),
                        ScopeType.REQUEST);
                errors.reject(null, "Le profil existe déjà en base");
                event = error();
            }
        }

        Profil profil11 = creationProfilForm.getProfil11();
        if (!StringUtils.isEmpty(profil11.getCodeProfil()))
        {
            String codeProfilConcatCodeAppli = "";
            codeProfilConcatCodeAppli = codeProfilConcatCodeAppli
                .concat(creationProfilForm.getProfil11()
                    .getCodeProfil());
            profil11.setCodeProfilConcatCodeAppli(codeProfilConcatCodeAppli);
            if (profilserviceso.rechercherProfilParCode(profil11.getCodeProfil()) == null)
            {
                profilserviceso.enregistrerProfil(profil11);
                lesProfils.add(profil11);
            }
            else
            {
                Errors errors = new FormObjectAccessor(context)
                    .getFormErrors(getFormObjectName(),
                        ScopeType.REQUEST);
                errors.reject(null, "Le profil existe déjà en base");
                event = error();
            }
        }

        Profil profil12 = creationProfilForm.getProfil12();
        if (!StringUtils.isEmpty(profil12.getCodeProfil()))
        {
            String codeProfilConcatCodeAppli = "";
            codeProfilConcatCodeAppli = codeProfilConcatCodeAppli
                .concat(creationProfilForm.getProfil12()
                    .getCodeProfil());
            profil12.setCodeProfilConcatCodeAppli(codeProfilConcatCodeAppli);
            if (profilserviceso.rechercherProfilParCode(profil12.getCodeProfil()) == null)
            {
                profilserviceso.enregistrerProfil(profil12);
                lesProfils.add(profil12);
            }
            else
            {
                Errors errors = new FormObjectAccessor(context)
                    .getFormErrors(getFormObjectName(),
                        ScopeType.REQUEST);
                errors.reject(null, "Le profil existe déjà en base");
                event = error();
            }

        }

        return event;
    }

    /**
     * Gets the messageserviceso - MessageService,.
     *
     * @return the messageserviceso - MessageService,
     */
    public MessageService getMessageserviceso()
    {
        return messageserviceso;
    }

    /**
     * Gets the profilserviceso - ProfilService,.
     *
     * @return the profilserviceso - ProfilService,
     */
    public ProfilService getProfilserviceso()
    {
        return profilserviceso;
    }

    /**
     * methode Parametrer checkboxes : .
     *
     * @param request DOCUMENTEZ_MOI
     * @return event
     */
    public Event parametrerCheckboxes(RequestContext request)
    {
        List lesAppli = (List) request.getFlowScope().get("listprofils");
        // Sauvegarde de l'objet de parametrage des checkboxes dans le
        // FlowScope
        CheckboxSelectUtil
            .parametrerCheckboxes(request, "listprofils")
            .utiliserRowid("id")
            .utiliserListeElements("listprofils")
            .utiliserMsgSelectionneUnSeulElement(
                "Selectionnez un seul profil a la fois.")
            .utiliserMsgAucunEltSelectionne(
                "Aucun profil n'est selectionnee");
        return success();
    }

    /**
     * .
     *
     * @param context DOCUMENTEZ_MOI
     * @return event
     */
    public Event rechercherListCodesApplicationDansPersonne(
        RequestContext context)
    {
        PersonneAnnuaire personne = UtilisateurSecurityLombokContainer
            .getPersonneAnnuaire();
        Set<HabilitationAnnuaire> listHab = personne.getListeHabilitations();
        List listApplications = new ArrayList();
        Iterator itfiltre;
        Iterator itvaleur;
        String valeur = null;
        FiltreHabilitationAnnuaire filtre = null;
        Iterator<HabilitationAnnuaire> lesHab = listHab.iterator();

        while (lesHab.hasNext())
        {
            HabilitationAnnuaire uneHab = lesHab.next();

            if (uneHab != null && uneHab.getListeFiltres() != null)
            {
                itfiltre = (uneHab).getListeFiltres().iterator();

                while (itfiltre.hasNext())
                {
                    filtre = (FiltreHabilitationAnnuaire) itfiltre.next();

                    if ((filtre.getListeValeursDeFiltre() != null)
                        && filtre.getNomDuFiltre().equalsIgnoreCase(
                            filtreANNUSIREME))
                    {
                        itvaleur = filtre.getListeValeursDeFiltre().iterator();

                        while (itvaleur.hasNext())
                        {
                            valeur = ((ValeurFiltreHabilitationAnnuaire) itvaleur
                                .next()).getValeur();

                            if (valeur != null)
                            {
                                listApplications.add(valeur);

                            }

                        }
                    }
                }
            }
        }
        context.getFlowScope().put("listcodesapplications", listApplications);
        return success();
    }

    /**
     * methode Selectionner par une checkbox : DOCUMENTEZ_MOI.
     *
     * @param context DOCUMENTEZ_MOI
     * @return event
     */
    public Event selectionnerParUneCheckbox(RequestContext context)
    {
        Event unEvent;

        GestionProfilForm gestionProfilForm;
        gestionProfilForm = (GestionProfilForm) context
            .getFlowScope().getRequired(getFormObjectName());

        Profil profil = (Profil) CheckboxSelectUtil
            .extraitUnElementSelectionneDansLeFlux(context,
                "listprofils");

        if (profil == null)
        {
            unEvent = error();
        }
        else
        {
            gestionProfilForm.setProfil1(profil);
            unEvent = success();
        }

        return unEvent;

    }

    /**
     * Sets the messageserviceso - MessageService,.
     *
     * @param messageserviceso the new messageserviceso - MessageService,
     */
    public void setMessageserviceso(MessageService messageserviceso)
    {
        this.messageserviceso = messageserviceso;
    }

    /**
     * Sets the profilserviceso - ProfilService,.
     *
     * @param profilserviceso the new profilserviceso - ProfilService,
     */
    public void setProfilserviceso(ProfilService profilserviceso)
    {
        this.profilserviceso = profilserviceso;
    }

    /**
     * methode Supprimer profils : DOCUMENTEZ_MOI.
     *
     * @param context DOCUMENTEZ_MOI
     * @return event
     */
    public Event supprimerProfils(RequestContext context)
    {
        Event unEvent;
        GestionProfilForm supp = (GestionProfilForm) context
            .getFlowScope().getRequired(getFormObjectName(),
                GestionProfilForm.class);
        Profil unprofilasupprimer = supp.getProfil1();

        if (!messageserviceso.verifierSiProfilReferenceParDesMessages(unprofilasupprimer))
        {
            profilserviceso.supprimerProfil(unprofilasupprimer);
            unEvent = success();
        }
        else
        {
            Errors errors = new FormObjectAccessor(context)
                .getFormErrors(getFormObjectName(),
                    ScopeType.REQUEST);
            errors.reject(null, "Suppression impossible, le profil est référencé par des messages.");
            unEvent = error();
        }
        return unEvent;
    }

    /**
     * methode Traiter checkboxes entre pages : .
     *
     * @param context DOCUMENTEZ_MOI
     * @return event
     */
    public Event traiterCheckboxesEntrePages(RequestContext context)
    {
        CheckboxSelectUtil.traiterCheckboxesEntrePages(context,
            "listprofils");
        return success();
    }

    /**
     * methode Rechercher list profils : DOCUMENTEZ_MOI.
     *
     * @param context DOCUMENTEZ_MOI
     * @return event
     */
    public Event rechercherListProfils(RequestContext context)
    {
        List lesProfils = profilserviceso.rechercherTousLesProfils();
        context.getFlowScope().put("listprofils", lesProfils);
        return success();

    }

}
