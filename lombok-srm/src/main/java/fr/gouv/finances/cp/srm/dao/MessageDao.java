/*
 * Copyright : Ministere du budget, des comptes publics et de la fonction publique - France
 */
package fr.gouv.finances.cp.srm.dao;

import java.util.List;

import fr.gouv.finances.cp.srm.techbean.CriteresRechercheMessages;
import fr.gouv.finances.lombok.srm.bean.Message;
import fr.gouv.finances.lombok.util.base.CoreBaseDao;

/**
 * Interface de la gestion des données des messages sireme.
 * 
 * @author amleplatinec
 * @author Christophe Breheret-Girardin
 */
public interface MessageDao extends CoreBaseDao
{

    /**
     * Méthode permettant de supprimer un message sireme.
     * 
     * @param message message à supprimer
     */
    void deleteMessage(Message message);

    /**
     * Méthode permettant de rechercher si un message possède le libellé passé en paramètre.
     *
     * @param message message contenant le libellé à rechercher
     * @return le message s'il existe
     */
    Message findMessageExisteDeja(Message message);

    /**
     * Méthode permettant de rechercher si un message possède le libellé du message existant passé en paramétre.
     *
     * @param message message contenant le libellé à rechercher
     * @return le message s'il existe
     */
    Message findMessageExisteDejaEnModif(Message message);

    /**
     * Méthode permettant de rechercher les messages associés aux critères de recherche.
     * 
     * @param crit critères de recherche à appliquer
     * @return la liste des messages correspondant aux critères
     */
    List<Message> findMessages(CriteresRechercheMessages crit);

    /**
     * Méthode permettant de rechercher les messages dont le codique est le même que celui du message
     * passé en paramètre.
     * 
     * @param libelleCodique libellé du codique à rechercher
     * @param message message à ne pas ramener
     * @return la liste des messages correspondant
     */
    List<Message> findMessagesAyantLibelleCodiqueRattacheAAutreMessage(
        String libelleCodique, Message message);

    /**
     * Méthode permettant de rechercher les messages perimés.
     *
     * @return la liste des messages correspondant
     */
    List<Message> findMessagesPerimes();

    /**
     * Méthode permettant de rechercher tous les messages.
     * 
     * @return la liste de tous les messages
     */
    List<Message> findTousLesMessages();

    /**
     * Méthode permettant d'enregistrer un message.
     * 
     * @param message message à enregistrer
     */
    void saveMessage(Message message);
}
