/*
 * Copyright : Ministere du budget, des comptes publics et de la fonction publique        France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - srm
 *
 * fichier : MessageTypeDao.java
 *
 */
package fr.gouv.finances.cp.srm.dao;

import java.util.List;

import fr.gouv.finances.lombok.srm.bean.MessageType;
import fr.gouv.finances.lombok.util.base.CoreBaseDao;

/**
 * Interface MessageTypeDao --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.14 $ Date: 22 dec. 2009
 */
public interface MessageTypeDao extends CoreBaseDao
{

    /**
     * Delete message type.
     * 
     * @param messagetype
     *            --
     */
    void deleteMessageType(MessageType messagetype);

    /**
     * methode Delete tous messages type : --.
     */
    void deleteTousMessagesType();

    /**
     * Find libelle associe a message type id.
     * 
     * @param messageTypeId
     *            --
     * @return the string
     */
    String findLibelleAssocieAMessageTypeId(String messageTypeId);

    /**
     * methode Find liste messages types : --.
     * 
     * @return list< message type>
     */
    List<MessageType> findListeMessagesTypes();

    /**
     * Find message type par libelle.
     * 
     * @param libelle
     *            --
     * @return the message type
     */
    MessageType findMessageTypeParLibelle(String libelle);

    /**
     * Find message type par partie par libelle.
     * 
     * @param libelle
     *            --
     * @return the list< message type>
     */
    List<MessageType> findMessageTypeParPartieParLibelle(String libelle);

    /**
     * Save message type.
     * 
     * @param message
     *            --
     */
    void saveMessageType(MessageType message);
}
