/*
 * Copyright : Ministere du budget, des comptes publics et de la fonction publique  France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - srm
 *
 * fichier : MessageTypeServiceImpl.java
 *
 */
package fr.gouv.finances.cp.srm.service.impl;

import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import fr.gouv.finances.cp.srm.dao.MessageTypeDao;
import fr.gouv.finances.cp.srm.service.MessageTypeService;
import fr.gouv.finances.lombok.srm.bean.MessageType;
import fr.gouv.finances.lombok.util.base.BaseServiceImpl;
import fr.gouv.finances.lombok.util.exception.ApplicationException;
import fr.gouv.finances.lombok.util.exception.ApplicationExceptionTransformateur;
import fr.gouv.finances.lombok.util.exception.RegleGestionException;

/**
 * Classe MessageTypeServiceImpl.java.
 * 
 * @author amleplatinec
 * @version $Revision: 1.19 $
 */
public class MessageTypeServiceImpl extends BaseServiceImpl implements
    MessageTypeService
{
    private static final Logger log = LoggerFactory.getLogger(MessageTypeServiceImpl.class);

    private MessageTypeDao messagetypedao;

    /** transaction template. */
    private TransactionTemplate transactionTemplate;

    public MessageTypeServiceImpl()
    {
        super();
    }

    /**
     * 
     * (methode de remplacement)
     * {@inheritDoc}
     * @see fr.gouv.finances.cp.srm.service.MessageTypeService#enregistrerMessageType(fr.gouv.finances.lombok.srm.bean.MessageType)
     */
    @Override
    public void enregistrerMessageType(MessageType messageType)
    {
        messagetypedao.saveMessageType(messageType);
    }

    /**
     * Accesseur de l attribut messagetypedao.
     * 
     * @return messagetypedao
     */
    public MessageTypeDao getMessagetypedao()
    {
        return messagetypedao;
    }

    /**
     * Accesseur de l attribut transaction template.
     * 
     * @return transaction template
     */
    public TransactionTemplate getTransactionTemplate()
    {
        return transactionTemplate;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.cp.srm.service.MessageTypeService#rechercherLibelleAssocieAMessageTypeId(java.lang.String)
     */
    @Override
    public String rechercherLibelleAssocieAMessageTypeId(String messageTypeId)
    {
        return messagetypedao.findLibelleAssocieAMessageTypeId(messageTypeId);
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.cp.srm.service.MessageTypeService#rechercherListeMessagesTypes()
     */
    @Override
    public List rechercherListeMessagesTypes()
    {
        return messagetypedao.findListeMessagesTypes();

    }

    /**
     * 
     * (methode de remplacement)
     * {@inheritDoc}
     * @see fr.gouv.finances.cp.srm.service.MessageTypeService#rechercherListeMessagesTypesParPartieLibelle(java.lang.String)
     */
    @Override
    public List<MessageType> rechercherListeMessagesTypesParPartieLibelle(
        String libelle)
    {
        String pourcent = "%";
        String libelleAvecPourcent = "%";

        libelleAvecPourcent = libelleAvecPourcent.concat(libelle);
        libelleAvecPourcent = libelleAvecPourcent.concat(pourcent);
        return messagetypedao
            .findMessageTypeParPartieParLibelle(libelleAvecPourcent);
    }

    /**
     * TODO NE SERT PAS POUR L INSTANT servira si fait une recherche exacte sur
     * le libelle*.
     * 
     * @param libelle
     *            --
     * @return the message type
     */
    @Override
    public MessageType rechercherMessageTypeParLibelle(String libelle)
    {
        return messagetypedao.findMessageTypeParLibelle(libelle);
    }

    /**
     * Modificateur de l attribut messagetypedao.
     * 
     * @param messagetypedao
     *            le nouveau messagetypedao
     */
    public void setMessagetypedao(MessageTypeDao messagetypedao)
    {
        this.messagetypedao = messagetypedao;
    }

    /**
     * Modificateur de l attribut transaction template.
     * 
     * @param transactionTemplate
     *            le nouveau transaction template
     */
    public void setTransactionTemplate(TransactionTemplate transactionTemplate)
    {
        this.transactionTemplate = transactionTemplate;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.cp.srm.service.MessageTypeService#supprimerListeMessagesType(java.util.List)
     */
    @Override
    public void supprimerListeMessagesType(
        final List listemessagestypeasupprimer)
    {
        try
        {
            transactionTemplate.execute(new TransactionCallbackWithoutResult()
            {
                public void doInTransactionWithoutResult(
                    TransactionStatus status)
                {
                    MessageType unmessageasupprimer = null;

                    Iterator uniterator = listemessagestypeasupprimer
                        .iterator();
                    while (uniterator.hasNext())
                    {
                        unmessageasupprimer = (MessageType) uniterator.next();
                        try
                        {
                            supprimerUnMessageType(unmessageasupprimer);

                        }

                        catch (ApplicationException e)
                        {
                            throw ApplicationExceptionTransformateur
                                .transformer(
                                    "Erreur sur suppression message type ",
                                    e);
                        }

                    }
                }
            });
        }
        // Interet du bloc d'exception : identifier les exceptions
        // verouillage optimiste
        // ajouter une info sur le contexte du plantage
        catch (ApplicationException e)
        {
            throw ApplicationExceptionTransformateur
                .transformer(
                    "Erreur sur la validation de la transaction de suppression  de plusieurs messages type",
                    e);
        }
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.cp.srm.service.MessageTypeService#supprimerTousMessagesTypes()
     */
    @Override
    public void supprimerTousMessagesTypes()
    {
        messagetypedao.deleteTousMessagesType();
    }

    /**
     * methode Supprimer un message type : --.
     * 
     * @param messagetype
     *            --
     */
    public void supprimerUnMessageType(MessageType messagetype)
    {
        if (log.isDebugEnabled())
        {
            log.debug("dbut supprimer un message type"
                + messagetype.getLibelleMessageType());
        }
        messagetypedao.deleteMessageType(messagetype);
        if (log.isDebugEnabled())
        {
            log.debug("fin supprimer un message"
                + messagetype.getLibelleMessageType());
        }
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.cp.srm.service.MessageTypeService#verifierExistenceMessageType(java.lang.String)
     */
    @Override
    public void verifierExistenceMessageType(String libelle)
    {
        if (messagetypedao.findMessageTypeParLibelle(libelle) != null)
        {
            throw new RegleGestionException("le message type existe deje");
        }
    }

}
