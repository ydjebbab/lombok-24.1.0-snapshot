/*
 * Copyright : Ministere du budget, des comptes publics et de la fonction publique        France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - srm
 *
 * fichier : CreationMessageValidator.java
 *
 */
package fr.gouv.finances.cp.srm.mvc.validator;

import org.apache.commons.validator.GenericValidator;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import fr.gouv.finances.cp.srm.mvc.form.CreationMessageForm;

/**
 * Classe CreationMessageValidator.java.
 * 
 * @author amleplatinec
 * @version $Revision: 1.11 $
 */
public class CreationMessageValidator implements Validator
{

    public CreationMessageValidator()
    {
        super();
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see org.springframework.validation.Validator#supports(java.lang.Class)
     */
    @Override
    public boolean supports(Class clazz)
    {
        return clazz.equals(CreationMessageForm.class);
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see org.springframework.validation.Validator#validate(java.lang.Object,
     *      org.springframework.validation.Errors)
     */
    @Override
    public void validate(Object command, Errors errors)
    {
        this.validateSurfaceCreationMessage((CreationMessageForm) (command),
            errors);
    }

    /**
     * Regle de validation de surface pour la creation de message simple.
     * 
     * @param command
     *            --
     * @param errors
     *            --
     */
    public void validateSurfaceCreationMessage(CreationMessageForm command,
        Errors errors)
    {
        if (command.getMessage().getDateDebutMessage() == null)
        {
            errors.rejectValue("message.dateDebutMessage",
                "rejected.message.datedebutmessageobligatoire",
                "Champs \"date de debut \" d'affichage du message obligatoire\"");
        }
        if (command.getMessage().getDateFinMessage() == null)
        {
            errors.rejectValue("message.dateFinMessage",
                "rejected.message.datefinmessageobligatoire",
                "Champs \"date de fin \" d'affichage du message obligatoire\"");
        }
        if (GenericValidator.isBlankOrNull(command.getMessage()
            .getLibelleMessage()))
        {
            errors.rejectValue("message.libelleMessage",
                "rejected.message.libellemessageobligatoire",
                "Champs \"libelle \" obligatoire\"");
        }
        if (command.getMessage().getTypeMessage() == null)
        {
            errors.rejectValue("message.typeMessage",
                "rejected.message.typemessageobligatoire",
                "Champs \"type de messages\" obligatoire\"");
        }

    }

    /**
     * regle de validation de surface de message pour la creation de message
     * avec codiques destinataires.
     * 
     * @param command
     *            --
     * @param errors
     *            --
     */
    public void validateSurfaceCreationMessageAvecDestinataires(
        CreationMessageForm command, Errors errors)
    {
        if (command.getMessage().getDateDebutMessage() == null)
        {
            errors.rejectValue("message.dateDebutMessage",
                "rejected.message.datedebutmessageobligatoire",
                "Champs \"date de debut \" d'affichage du message obligatoire\"");
        }
        if (command.getMessage().getDateFinMessage() == null)
        {
            errors.rejectValue("message.dateFinMessage",
                "rejected.message.datefinmessageobligatoire",
                "Champs \"date de fin \" d'affichage du message obligatoire\"");
        }
        if (GenericValidator.isBlankOrNull(command.getMessage()
            .getLibelleMessage()))
        {
            errors.rejectValue("message.libelleMessage",
                "rejected.message.libellemessageobligatoire",
                "Champs \"libelle \" obligatoire\"");
        }

    }

    /**
     * regle de validation de surface de message pour la creation de message
     * avec desactivation de profils premier ecran.
     * 
     * @param command
     *            --
     * @param errors
     *            --
     */
    public void validateSurfaceCreationMessageAvecProfil(
        CreationMessageForm command, Errors errors)
    {
        if (command.getMessage().getDateDebutMessage() == null)
        {
            errors.rejectValue("message.dateDebutMessage",
                "rejected.message.datedebutmessageobligatoire",
                "Champs \"date de debut \" d'affichage du message obligatoire\"");
        }
        if (command.getMessage().getDateFinMessage() == null)
        {
            errors.rejectValue("message.dateFinMessage",
                "rejected.message.datefinmessageobligatoire",
                "Champs \"date de fin \" d'affichage du message obligatoire\"");
        }
        if (GenericValidator.isBlankOrNull(command.getMessage()
            .getLibelleMessage()))
        {
            errors.rejectValue("message.libelleMessage",
                "rejected.message.libellemessageobligatoire",
                "Champs \"libelle \" obligatoire\"");
        }

    }

    /**
     * regle de validation de surface de message pour la creation de message
     * avec desactivation de profils second ecran.
     * 
     * @param command
     *            --
     * @param errors
     *            --
     */
    public void validateSurfaceDesactivationProfils(
        CreationMessageForm command, Errors errors)
    {
        if (command.getMessage().getDateDebutInactiviteProfil() == null)
        {
            errors.rejectValue("message.dateDebutInactiviteProfil",
                "rejected.message.datedebutinactiviteprofilobligatoire",
                "Champs \"date de debut d'inactivite du profil\" obligatoire\"");
        }
        if (command.getMessage().getDateFinInactiviteProfil() == null)
        {
            errors.rejectValue("message.dateFinInactiviteProfil",
                "rejected.message.datefininactiviteprofilobligatoire",
                "Champs \"date de fin d'inactivite du profil\" obligatoire\"");
        }
    }

}
