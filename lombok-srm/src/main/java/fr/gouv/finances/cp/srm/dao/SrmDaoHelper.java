/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 *
 */
package fr.gouv.finances.cp.srm.dao;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.gouv.finances.lombok.util.exception.IngerableException;

/**
 * Utilitaire lié aux DAO sireme.
 *
 * @author Christophe Breheret-Girardin
 */
public final class SrmDaoHelper
{
    /** Initialisation de la journalisation. */
    private static final Logger LOGGER = LoggerFactory.getLogger(SrmDaoHelper.class);

    /**
     * Constructeur.
     *
     */
    private SrmDaoHelper()
    {
        super();
    }

    /**
     * Méthode permettant de vérifier si l'identifiant est utilisable.
     *
     * @param messageTypeId identifiant du type de message sous forme de chaine de caractère
     * @return l'identifiant sous forme numérique
     */
    public static Long getIdentifiant(String messageTypeId)
    {
        // Le paramètre est-il renseigné ?
        if (parametreNonFourni(messageTypeId))
        {
            return null;
        }

        try
        {
            // Conversion de l'identifiant en numérique
            return Long.valueOf(messageTypeId);
        }
        catch (NumberFormatException e)
        {
            // Cas d'un paramètre non numérique
            throw new IngerableException("L'identifiant '" + messageTypeId + "' du type de message sireme"
                + " n'est pas numérique");
        }
    }

    /**
     * Méthode permettant de vérifier si le paramètre de la méthode est fourni, ou non.
     *
     * @param parametre paramétre utilisé dans la recherche
     * @return true si le paramètre de la méthode est fourni, false sinon
     */
    public static boolean parametreNonFourni(String parametre)
    {
        // Le paramètre est-il renseigné ?
        if (StringUtils.isEmpty(parametre))
        {
            LOGGER.info("Aucun parametre n'a été fourni pour la recherche");
            return true;
        }
        return false;
    }
}
