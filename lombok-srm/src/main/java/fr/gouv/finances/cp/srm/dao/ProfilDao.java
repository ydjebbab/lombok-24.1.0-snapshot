/*
 * Copyright (c) 2017 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.cp.srm.dao;

import java.util.List;

import fr.gouv.finances.lombok.srm.bean.Profil;
import fr.gouv.finances.lombok.util.base.CoreBaseDao;

/**
 * Interface ProfilDao --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.12 $ Date: 22 dec. 2009
 */
public interface ProfilDao extends CoreBaseDao
{

    /**
     * recherche un profil par son code.
     * 
     * @param code --
     * @return the profil
     */
    Profil findProfilParCode(String code);

    /**
     * sauve un profil.
     * 
     * @param profil --
     */
    void saveProfil(Profil profil);

    /**
     * methode Find tous les profils : DOCUMENTEZ_MOI.
     *
     * @return list
     */
    List<Profil> findTousLesProfils();

}
