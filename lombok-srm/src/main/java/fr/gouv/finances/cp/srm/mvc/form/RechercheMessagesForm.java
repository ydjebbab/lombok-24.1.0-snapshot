/*
 * Copyright : Ministere du budget, des comptes publics et de la fonction publique        France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - srm
 *
 * fichier : RechercheMessagesForm.java
 *
 */
package fr.gouv.finances.cp.srm.mvc.form;

import java.io.Serializable;

import fr.gouv.finances.cp.srm.techbean.CriteresRechercheMessages;

/**
 * Class RechercheMessagesForm --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.12 $ Date: 22 dec. 2009
 */
public class RechercheMessagesForm implements Serializable
{

    private static final long serialVersionUID = 1L;

    private CriteresRechercheMessages crit = new CriteresRechercheMessages();

    public RechercheMessagesForm()
    {
        super();
    }

    /**
     * Accesseur de l attribut crit.
     * 
     * @return crit
     */
    public CriteresRechercheMessages getCrit()
    {
        return crit;
    }

    /**
     * Modificateur de l attribut crit.
     * 
     * @param crit
     *            le nouveau crit
     */
    public void setCrit(CriteresRechercheMessages crit)
    {
        this.crit = crit;
    }

}
