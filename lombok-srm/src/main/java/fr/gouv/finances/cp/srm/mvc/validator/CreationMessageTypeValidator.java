/*
 * Copyright : Ministere du budget, des comptes publics et de la fonction publique        France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - srm
 *
 * fichier : CreationMessageTypeValidator.java
 *
 */
package fr.gouv.finances.cp.srm.mvc.validator;

import org.apache.commons.validator.GenericValidator;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import fr.gouv.finances.cp.srm.mvc.form.CreationMessageTypeForm;

/**
 * Classe CreationMessageTypeValidator.java.
 * 
 * @author amleplatinec
 * @version $Revision: 1.11 $
 */
public class CreationMessageTypeValidator implements Validator
{

    /**
     * Instanciation de creation message type validator.
     */
    public CreationMessageTypeValidator()
    {
        super();
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see org.springframework.validation.Validator#supports(java.lang.Class)
     */
    @Override
    public boolean supports(Class clazz)
    {
        return clazz.equals(CreationMessageTypeForm.class);
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see org.springframework.validation.Validator#validate(java.lang.Object,
     *      org.springframework.validation.Errors)
     */
    @Override
    public void validate(Object command, Errors errors)
    {
        this.validateSurfaceCreationMessageType(
            (CreationMessageTypeForm) command, errors);
    }

    /**
     * Methode de validation de surface : Regle de nommage debute par
     * validateSurface NomdeVue POur la creation d'un message type libelle
     * obligatoire.
     * 
     * @param command
     *            --
     * @param errors
     *            --
     */
    public void validateSurfaceCreationMessageType(
        CreationMessageTypeForm command, Errors errors)
    {
        if (GenericValidator.isBlankOrNull(command.getMessagetypeform()
            .getLibelleMessageType()))
        {
            errors.rejectValue("message.libelleMessagetype",
                "rejected.message.libellemessageobligatoire",
                "Champs \"libelle \" obligatoire\"");
        }

    }

}
