/*
 * Copyright : Ministere du budget, des comptes publics et de la fonction publique        France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - srm
 *
 * fichier : CreationMessageForm.java
 *
 */
package fr.gouv.finances.cp.srm.mvc.form;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.beanutils.BeanUtils;

import fr.gouv.finances.lombok.srm.bean.Message;
import fr.gouv.finances.lombok.srm.bean.MessageType;
import fr.gouv.finances.lombok.srm.bean.Profil;
import fr.gouv.finances.lombok.util.exception.ProgrammationException;
import fr.gouv.finances.lombok.webflow.FormMemento;
import fr.gouv.finances.lombok.webflow.Memento;
import fr.gouv.finances.lombok.webflow.MementoFormStatePersister;
import fr.gouv.finances.lombok.webflow.MementoOriginator;

/**
 * Class CreationMessageForm --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.20 $ Date: 22 dec. 2009
 */
public class CreationMessageForm implements Serializable, MementoOriginator
{

    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** message. */
    private Message message = new Message();

    /** message type dans form. */
    private MessageType messageTypeDansForm = new MessageType();

    /** list profils. */
    private List<Profil> listProfils = new ArrayList<Profil>();

    /** codique1. */
    private String codique1;

    /** codique2. */
    private String codique2;

    /** codique3. */
    private String codique3;

    /** savemessagetype. */
    private String savemessagetype;

    /**
     * Instanciation de creation message form.
     */
    public CreationMessageForm()
    {
        super();
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.webflow.MementoOriginator#createMemento()
     */
    @Override
    public Memento createMemento()
    {
        FormMemento formMemento = new FormMemento(this);
        return formMemento;
    }

    /**
     * Accesseur de l attribut codique1.
     * 
     * @return codique1
     */
    public String getCodique1()
    {
        return codique1;
    }

    /**
     * Accesseur de l attribut codique2.
     * 
     * @return codique2
     */
    public String getCodique2()
    {
        return codique2;
    }

    /**
     * Accesseur de l attribut codique3.
     * 
     * @return codique3
     */
    public String getCodique3()
    {
        return codique3;
    }

    /**
     * Accesseur de l attribut list profils.
     * 
     * @return list profils
     */
    public List<Profil> getListProfils()
    {
        return listProfils;
    }

    /**
     * Accesseur de l attribut message.
     * 
     * @return message
     */
    public Message getMessage()
    {
        return message;
    }

    /**
     * Accesseur de l attribut message type dans form.
     * 
     * @return message type dans form
     */
    public MessageType getMessageTypeDansForm()
    {
        return messageTypeDansForm;
    }

    /**
     * Accesseur de l attribut savemessagetype.
     * 
     * @return savemessagetype
     */
    public String getSavemessagetype()
    {
        return savemessagetype;
    }

    /**
     * Modificateur de l attribut codique1.
     * 
     * @param codique1
     *            le nouveau codique1
     */
    public void setCodique1(String codique1)
    {
        this.codique1 = codique1;
    }

    /**
     * Modificateur de l attribut codique2.
     * 
     * @param codique2
     *            le nouveau codique2
     */
    public void setCodique2(String codique2)
    {
        this.codique2 = codique2;
    }

    /**
     * Modificateur de l attribut codique3.
     * 
     * @param codique3
     *            le nouveau codique3
     */
    public void setCodique3(String codique3)
    {
        this.codique3 = codique3;
    }

    /**
     * Modificateur de l attribut list profils.
     * 
     * @param listProfils
     *            le nouveau list profils
     */
    public void setListProfils(List<Profil> listProfils)
    {
        this.listProfils = listProfils;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.webflow.MementoOriginator#setMemento(fr.gouv.finances.lombok.webflow.Memento,
     *      int)
     */
    @Override
    public void setMemento(Memento memento, int level)
    {
        FormMemento formMemento = (FormMemento) memento;
        CreationMessageForm formulaireOriginal = (CreationMessageForm) formMemento
            .getObjectDeepClone();

        try
        {
            switch (level)
            {
            // Restauration des informations de l'ecran principal de
            // modification, en cas d'annulation des modifications
                case MementoFormStatePersister.LEVEL1:
                    BeanUtils.copyProperties(this.message,
                        formulaireOriginal.getMessage());
                    break;

            }

        }
        catch (IllegalAccessException e)
        {

            throw new ProgrammationException(e);
        }
        catch (InvocationTargetException e)
        {

            throw new ProgrammationException(e);
        }
    }

    /**
     * Modificateur de l attribut message.
     * 
     * @param message
     *            le nouveau message
     */
    public void setMessage(Message message)
    {
        this.message = message;
    }

    /**
     * Modificateur de l attribut message type dans form.
     * 
     * @param messageType
     *            le nouveau message type dans form
     */
    public void setMessageTypeDansForm(MessageType messageType)
    {
        this.messageTypeDansForm = messageType;
    }

    /**
     * Modificateur de l attribut savemessagetype.
     * 
     * @param savemessagetype
     *            le nouveau savemessagetype
     */
    public void setSavemessagetype(String savemessagetype)
    {
        this.savemessagetype = savemessagetype;
    }

}
