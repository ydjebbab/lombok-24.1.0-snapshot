/*
 * Copyright (c) 2016 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.cp.srm.mvc.form;

import java.io.Serializable;

import fr.gouv.finances.lombok.srm.bean.Profil;

/**
 * Class GestionApplicationForm .
 */
public class GestionProfilForm implements Serializable
{

    private static final long serialVersionUID = 1L;

    /** profil1. */
    private Profil profil1 = new Profil();

    /** profil2. */
    private Profil profil2 = new Profil();

    /** profil3. */
    private Profil profil3 = new Profil();

    /** profil4. */
    private Profil profil4 = new Profil();

    /** profil5. */
    private Profil profil5 = new Profil();

    /** profil6. */
    private Profil profil6 = new Profil();

    /** profil7. */
    private Profil profil7 = new Profil();

    /** profil8. */
    private Profil profil8 = new Profil();

    /** profil9. */
    private Profil profil9 = new Profil();

    /** profil10. */
    private Profil profil10 = new Profil();

    /** profil11. */
    private Profil profil11 = new Profil();

    /** profil12. */
    private Profil profil12 = new Profil();

    /**
     * Accesseur de l attribut profil1.
     *
     * @return profil1
     */
    public Profil getProfil1()
    {
        return profil1;
    }

    /**
     * Accesseur de l attribut profil10.
     *
     * @return profil10
     */
    public Profil getProfil10()
    {
        return profil10;
    }

    /**
     * Accesseur de l attribut profil11.
     *
     * @return profil11
     */
    public Profil getProfil11()
    {
        return profil11;
    }

    /**
     * Accesseur de l attribut profil12.
     *
     * @return profil12
     */
    public Profil getProfil12()
    {
        return profil12;
    }

    /**
     * Accesseur de l attribut profil2.
     *
     * @return profil2
     */
    public Profil getProfil2()
    {
        return profil2;
    }

    /**
     * Accesseur de l attribut profil3.
     *
     * @return profil3
     */
    public Profil getProfil3()
    {
        return profil3;
    }

    /**
     * Accesseur de l attribut profil4.
     *
     * @return profil4
     */
    public Profil getProfil4()
    {
        return profil4;
    }

    /**
     * Accesseur de l attribut profil5.
     *
     * @return profil5
     */
    public Profil getProfil5()
    {
        return profil5;
    }

    /**
     * Accesseur de l attribut profil6.
     *
     * @return profil6
     */
    public Profil getProfil6()
    {
        return profil6;
    }

    /**
     * Accesseur de l attribut profil7.
     *
     * @return profil7
     */
    public Profil getProfil7()
    {
        return profil7;
    }

    /**
     * Accesseur de l attribut profil8.
     *
     * @return profil8
     */
    public Profil getProfil8()
    {
        return profil8;
    }

    /**
     * Accesseur de l attribut profil9.
     *
     * @return profil9
     */
    public Profil getProfil9()
    {
        return profil9;
    }

    /**
     * Modificateur de l attribut profil1.
     *
     * @param profil1 le nouveau profil1
     */
    public void setProfil1(Profil profil1)
    {
        this.profil1 = profil1;
    }

    /**
     * Modificateur de l attribut profil10.
     *
     * @param profil10 le nouveau profil10
     */
    public void setProfil10(Profil profil10)
    {
        this.profil10 = profil10;
    }

    /**
     * Modificateur de l attribut profil11.
     *
     * @param profil11 le nouveau profil11
     */
    public void setProfil11(Profil profil11)
    {
        this.profil11 = profil11;
    }

    /**
     * Modificateur de l attribut profil12.
     *
     * @param profil12 le nouveau profil12
     */
    public void setProfil12(Profil profil12)
    {
        this.profil12 = profil12;
    }

    /**
     * Modificateur de l attribut profil2.
     *
     * @param profil2 le nouveau profil2
     */
    public void setProfil2(Profil profil2)
    {
        this.profil2 = profil2;
    }

    /**
     * Modificateur de l attribut profil3.
     *
     * @param profil3 le nouveau profil3
     */
    public void setProfil3(Profil profil3)
    {
        this.profil3 = profil3;
    }

    /**
     * Modificateur de l attribut profil4.
     *
     * @param profil4 le nouveau profil4
     */
    public void setProfil4(Profil profil4)
    {
        this.profil4 = profil4;
    }

    /**
     * Modificateur de l attribut profil5.
     *
     * @param profil5 le nouveau profil5
     */
    public void setProfil5(Profil profil5)
    {
        this.profil5 = profil5;
    }

    /**
     * Modificateur de l attribut profil6.
     *
     * @param profil6 le nouveau profil6
     */
    public void setProfil6(Profil profil6)
    {
        this.profil6 = profil6;
    }

    /**
     * Modificateur de l attribut profil7.
     *
     * @param profil7 le nouveau profil7
     */
    public void setProfil7(Profil profil7)
    {
        this.profil7 = profil7;
    }

    /**
     * Modificateur de l attribut profil8.
     *
     * @param profil8 le nouveau profil8
     */
    public void setProfil8(Profil profil8)
    {
        this.profil8 = profil8;
    }

    /**
     * Modificateur de l attribut profil9.
     *
     * @param profil9 le nouveau profil9
     */
    public void setProfil9(Profil profil9)
    {
        this.profil9 = profil9;
    }

}
