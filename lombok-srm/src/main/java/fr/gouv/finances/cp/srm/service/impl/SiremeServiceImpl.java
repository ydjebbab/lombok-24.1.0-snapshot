/*
 * Copyright : Ministere du budget, des comptes publics et de la fonction publique        France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - srm
 *
 * fichier : MessageWebServiceImpl.java
 *
 */
package fr.gouv.finances.cp.srm.service.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import fr.gouv.finances.cp.srm.service.MessageService;
import fr.gouv.finances.cp.srm.service.ProfilService;
import fr.gouv.finances.cp.srm.techbean.CriteresRechercheMessages;
import fr.gouv.finances.lombok.securite.service.LectureApplicationService;
import fr.gouv.finances.lombok.securite.techbean.ApplicationAnnuaire;
import fr.gouv.finances.lombok.securite.techbean.ProfilAnnuaire;
import fr.gouv.finances.lombok.srm.bean.Message;
import fr.gouv.finances.lombok.srm.bean.Profil;
import fr.gouv.finances.lombok.srm.service.SiremeService;
import fr.gouv.finances.lombok.util.exception.ApplicationExceptionTransformateur;
import fr.gouv.finances.lombok.util.exception.ExploitationException;

/**
 * Class MessageWebServiceImpl --
 * 
 * @author amleplatinec
 * @version $Revision: 1.5 $ Date: 10 fevr. 2010
 */
public class SiremeServiceImpl implements SiremeService
{
    protected final Log log = LogFactory.getLog(getClass());

     private MessageService messageserviceso;

    /** lectureapplicationserviceso. */
    private LectureApplicationService lectureapplicationserviceso;

    /** profilserviceso. */
    private ProfilService profilserviceso;

    private String typeAnnuaire;

    public SiremeServiceImpl()
    {
        super();
    }
    
    /**
     * methode Creer appli et profils en base : --.
     * 
     * @param pLibelleCourt le libelle court
     */
    public void creerAppliEtProfilsEnBase(String pLibelleCourt)
    {

        try
        {
            if (log.isDebugEnabled())
            {
                log.debug("debut rechercherApplicationParLibelleCourt "
                    + pLibelleCourt);
            }
            ApplicationAnnuaire applicationAnnuaire = lectureapplicationserviceso
                .rechercherApplicationParLibelleCourt(pLibelleCourt);

            if (log.isDebugEnabled())
            {
                log.debug("fin  rechercherApplicationParLibelleCourt "
                    + pLibelleCourt);
            }

            if ((applicationAnnuaire == null)
                || ((applicationAnnuaire != null) && (applicationAnnuaire
                    .getLibelleCourtAppli() == null)))
            {
                if (log.isDebugEnabled())
                {
                    log.debug("alerte l application n existe pas dans le ldap probleme entre"
                        + "le libelle court dans aptera et celui dans le fichier de configuration"
                        + pLibelleCourt);
                }
                throw new ExploitationException(
                    "ALERTE l application n existe pas dans le ldap");

            }
            else
            {
                Set setProfilsDecodes = applicationAnnuaire
                    .getDetailProfilDecode();
                Set setProfils = new HashSet();
                for (Iterator iter = setProfilsDecodes.iterator(); iter
                    .hasNext();)
                {
                    ProfilAnnuaire element = (ProfilAnnuaire) iter.next();
                    Profil profil = new Profil();
                    String codeProfil = element.getCodeProfil();
                    String codeProfilConcatCodeAppli = "";
                    codeProfilConcatCodeAppli = codeProfilConcatCodeAppli
                        .concat(pLibelleCourt);
                    codeProfilConcatCodeAppli = codeProfilConcatCodeAppli
                        .concat("_");
                    codeProfilConcatCodeAppli = codeProfilConcatCodeAppli
                        .concat(codeProfil);
                    profil.setCodeProfilConcatCodeAppli(codeProfilConcatCodeAppli);
                    profil.setCodeProfil(codeProfil);
                    profil.setLibelleProfil(element.getLibelleProfil());
                    setProfils.add(profil);
                    if (log.isDebugEnabled())
                    {
                        log.debug("debut sauvegarde profil" + codeProfil);
                    }
                    profilserviceso.enregistrerProfil(profil);
                    if (log.isDebugEnabled())
                    {
                        log.debug("fin sauvegarde profil" + codeProfil);
                    }
                }
            }
        }
        catch (ExploitationException e)
        {
            throw ApplicationExceptionTransformateur.transformer(
                "Erreur sur creation application et profils en base avec identifiant : "
                    + pLibelleCourt, e);

        }
    }

    /**
     * Accesseur de l attribut lectureapplicationserviceso.
     * 
     * @return lectureapplicationserviceso
     */
    public LectureApplicationService getLectureapplicationserviceso()
    {
        return lectureapplicationserviceso;
    }

    /**
     * Accesseur de l attribut messageserviceso.
     * 
     * @return messageserviceso
     */
    public MessageService getMessageserviceso()
    {
        return messageserviceso;
    }

    /**
     * Accesseur de l attribut profilserviceso.
     * 
     * @return profilserviceso
     */
    public ProfilService getProfilserviceso()
    {
        return profilserviceso;
    }

    public String getTypeAnnuaire()
    {
        return typeAnnuaire;
    }

    /**
     * 
     * (methode de remplacement)
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.srm.service.SiremeService#rechercherListeProfilsADesactiverALaDateHeureCourantePourLApplication()
     */
    @Override
    public List rechercherListeProfilsADesactiverALaDateHeureCourantePourLApplication()
    {
        if (log.isDebugEnabled())
        {
            log.debug("debut rechercherListeProfilsADesactiverALaDateHeureCourantePourLApplication ");
        }
        List listMessages = new ArrayList();
        // annuaire fusionne : la branche application ne contient plus les
        // profils
        // les applications sont gerees dans madhras
        // si on reste sur les applications existantes qui utilisent srm
        // pas besoin de correctif
        // sinon webservice ou creation application via zf5
        CriteresRechercheMessages crit = new CriteresRechercheMessages();
        crit.setRechOnlyMessProfilsADesactNow(true);
        listMessages = messageserviceso.rechercherMessages(crit);

        if (log.isDebugEnabled())
        {
            log.debug("fin rechercherListeProfilsADesactiverALaDateHeureCourantePourLApplication "
                + " nombre de messages trouves: "
                + listMessages.size());
        }
        return listMessages;
    }

    /**
     * 
     * (methode de remplacement)
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.srm.service.SiremeService#rechercherMessageBloquantALaDateHeureCourantePourLApplication()
     */
    @Override
    public Message rechercherMessageBloquantALaDateHeureCourantePourLApplication()
    {
        if (log.isDebugEnabled())
        {
            log.debug("debut rechercherMessageBloquantALaDateHeureCourantePourLApplication ");
        }
        Message unMessage = new Message();
        CriteresRechercheMessages crit = new CriteresRechercheMessages();
        crit.setRechOnlyMessagesActifsNow(true);
        crit.setTypeMessages("2");
        List listMessages = messageserviceso.rechercherMessages(crit);
/*
        crit = new CriteresRechercheMessages();
        crit.setRechOnlyMessagesActifsNow(true);
        crit.setTypeMessages("2");
        List listMessagesPourToutesAppli = messageserviceso.rechercherMessages(crit);

        if ((listMessagesPourToutesAppli != null)
            && (listMessagesPourToutesAppli.size() > 0))
        {
            listMessages.addAll(listMessagesPourToutesAppli);
        }
*/
        if (listMessages != null && !listMessages.isEmpty())
        {
            if (log.isDebugEnabled())
            {
                log.debug("fin rechercherMessageBloquantALaDateHeureCourantePourLApplication  "
                    + "1 message trouve ");
            }
            unMessage = (Message) listMessages.get(0);
        }
        else
        {
            if (log.isDebugEnabled())
            {
                log.debug("fin rechercherMessageBloquantALaDateHeureCourantePourLApplication "
                    + "aucun message trouve ");
            }
            unMessage = null;
        }

        return unMessage;
    }

    /**
     * 
     * (methode de remplacement)
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.srm.service.SiremeService#rechercherMessagesALaDateHeureCourantePourLApplication()
     */
    @Override
    public List rechercherMessagesALaDateHeureCourantePourLApplication()
    {
        if (log.isDebugEnabled())
        {
            log.debug("debut rechercherMessagesALaDateHeureCourantePourLApplication ");
        }
        List listMessages = new ArrayList();

        CriteresRechercheMessages crit = new CriteresRechercheMessages();

        crit.setRechOnlyMessagesActifsNow(true);

        listMessages = messageserviceso.rechercherMessages(crit);
        /*
        crit = new CriteresRechercheMessages();

        crit.setRechOnlyMessagesActifsNow(true);

        List listMessagesPourToutesAppli = messageserviceso
            .rechercherMessages(crit);

        if ((listMessagesPourToutesAppli != null)
            && (listMessagesPourToutesAppli.size() > 0))
        {
            listMessages.addAll(listMessagesPourToutesAppli);
        }
        */
        if (log.isDebugEnabled())
        {
            log.debug("fin rechercherMessagesALaDateHeureCourantePourLApplicationPourLApplication "
                + "nombre de messages trouves "
                + listMessages.size());
        }

        return listMessages;

    }

    /**
     * Modificateur de l attribut lectureapplicationserviceso.
     * 
     * @param lectureapplicationserviceso le nouveau lectureapplicationserviceso
     */
    public void setLectureapplicationserviceso(
        LectureApplicationService lectureapplicationserviceso)
    {
        this.lectureapplicationserviceso = lectureapplicationserviceso;
    }

    /**
     * Modificateur de l attribut messageserviceso.
     * 
     * @param messageserviceso le nouveau messageserviceso
     */
    public void setMessageserviceso(MessageService messageserviceso)
    {
        this.messageserviceso = messageserviceso;
    }

    /**
     * Modificateur de l attribut profilserviceso.
     * 
     * @param profilserviceso le nouveau profilserviceso
     */
    public void setProfilserviceso(ProfilService profilserviceso)
    {
        this.profilserviceso = profilserviceso;
    }

    public void setTypeAnnuaire(String typeAnnuaire)
    {
        this.typeAnnuaire = typeAnnuaire;
    }

    /**
     * methode Est profil absent : --. on recherche si un profil a ete supprime dans l'annuaire
     * 
     * @param listProfilsAnnuaire --
     * @param element --
     * @return profil annuaire
     */
    private ProfilAnnuaire estProfilAbsent(Set listProfilsAnnuaire,
        Profil element)
    {
        ProfilAnnuaire profil = null;
        for (Iterator iter = listProfilsAnnuaire.iterator(); iter.hasNext();)
        {
            ProfilAnnuaire profil1 = (ProfilAnnuaire) iter.next();
            if (profil1.getCodeProfil().compareTo(element.getCodeProfil()) == 0)
            {
                profil = profil1;

            }

        }
        return profil;

    }

    /**
     * methode Est profil present : --. on recherche si un profil est present dans l'annuaire
     * 
     * @param listProfils --
     * @param element --
     * @return profil
     */
//    private Profil estProfilPresent(Set listProfils, ProfilAnnuaire element)
//    {
//        Profil profil = null;
//        for (Iterator iter = listProfils.iterator(); iter.hasNext();)
//        {
//            Profil profil1 = (Profil) iter.next();
//            if (profil1.getCodeProfil().compareTo(element.getCodeProfil()) == 0)
//            {
//                profil = profil1;
//                break;
//            }
//
//        }
//        return profil;
//    }

    /**
     * methode Mise a jour profils : --.
     * 
     * @param application --
     */
    // private void miseAJourProfils()
    // {
    //
    // try
    // {
    // String pLibelleCourtAppli = application.getLibelleCourt();
    // if (log.isDebugEnabled())
    // {
    // log.debug("debut rechercherApplicationParLibelleCourt "
    // + pLibelleCourtAppli);
    // }
    // ApplicationAnnuaire applicationAnnuaire = lectureapplicationserviceso
    // .rechercherApplicationParLibelleCourt(pLibelleCourtAppli);
    // if (log.isDebugEnabled())
    // {
    // log.debug("fin  rechercherApplicationParLibelleCourt "
    // + pLibelleCourtAppli);
    // }
    //
    // if ((applicationAnnuaire == null)
    // || ((applicationAnnuaire != null) && (applicationAnnuaire
    // .getLibelleCourtAppli() == null)))
    // {
    //
    // throw new ExploitationException(
    // "ALERTE l application n existe pas dans le ldap");
    //
    // }
    // else
    //
    // {
    //
    // Set setProfilsDecodes = applicationAnnuaire
    // .getDetailProfilDecode();
    // Set setProfils = new HashSet();
    // Set profilsExistants = application
    // .getListProfilsDeLApplication();
    //
    // boolean miseAJourProfils = false;
    //
    // for (Iterator iter = setProfilsDecodes.iterator(); iter
    // .hasNext();)
    // {
    // ProfilAnnuaire element = (ProfilAnnuaire) iter.next();
    // Profil profil = this.estProfilPresent(profilsExistants,
    // element);
    //
    // if (profil != null)
    // {
    // if (log.isDebugEnabled())
    // {
    // log.debug("profil deja cree . on ne fait rien");
    // }
    // }
    // else
    // {
    // miseAJourProfils = true;
    // }
    // }
    //
    // for (Iterator iter = profilsExistants.iterator(); iter
    // .hasNext();)
    // {
    // Profil element = (Profil) iter.next();
    // ProfilAnnuaire profil = this.estProfilAbsent(
    // setProfilsDecodes, element);
    //
    // if (profil == null)
    // {
    // miseAJourProfils = true;
    // }
    // }
    //
    // if (miseAJourProfils == false)
    // {
    // if (log.isDebugEnabled())
    // {
    // log.debug("les profils en base et dans le ldap sont identiques");
    // }
    // }
    // else
    // {
    // for (Iterator iter = profilsExistants.iterator(); iter
    // .hasNext();)
    // {
    // Profil element = (Profil) iter.next();
    // // on supprime le profil
    //
    // if (log.isDebugEnabled())
    // {
    // log.debug("debut supression profil dans base "
    // + element.getCodeProfil());
    // }
    // profilserviceso.supprimerProfil(element);
    // if (log.isDebugEnabled())
    // {
    // log.debug("fin supression profil dans base "
    // + element.getCodeProfil());
    //
    // }
    // }
    //
    // for (Iterator iter = setProfilsDecodes.iterator(); iter
    // .hasNext();)
    // {
    // ProfilAnnuaire element = (ProfilAnnuaire) iter.next();
    // Profil profil1 = new Profil();
    // String codeProfil = element.getCodeProfil();
    // String codeProfilConcatCodeAppli = "";
    // codeProfilConcatCodeAppli = codeProfilConcatCodeAppli
    // .concat(pLibelleCourtAppli);
    // codeProfilConcatCodeAppli = codeProfilConcatCodeAppli
    // .concat("_");
    // codeProfilConcatCodeAppli = codeProfilConcatCodeAppli
    // .concat(codeProfil);
    // profil1.setCodeProfilConcatCodeAppli(codeProfilConcatCodeAppli);
    // profil1.setCodeProfil(codeProfil);
    // profil1.setLibelleProfil(element.getLibelleProfil());
    // setProfils.add(profil1);
    // if (log.isDebugEnabled())
    // {
    // log.debug("debut sauvegarde profil " + codeProfil);
    // }
    // profilserviceso.enregistrerProfil(profil1);
    // if (log.isDebugEnabled())
    // {
    // log.debug("fin sauvegarde profil " + codeProfil);
    // }
    // }
    //
    // if (setProfils.size() > 0)
    // {
    // application.setListProfilsDeLApplication(setProfils);
    // if (log.isDebugEnabled())
    // {
    // log.debug("debut sauvegarde appli "
    // + pLibelleCourtAppli);
    // }
    // applicationserviceso
    // .enregistrerApplication(application);
    // if (log.isDebugEnabled())
    // {
    // log.debug("fin sauvegarde appli "
    // + pLibelleCourtAppli);
    // }
    //
    // }
    // }
    // }
    // }
    // catch (ExploitationException e)
    // {
    // throw ApplicationExceptionTransformateur.transformer(
    // "Erreur sur mise e jour profils : ", e);
    //
    // }
    // }

}
