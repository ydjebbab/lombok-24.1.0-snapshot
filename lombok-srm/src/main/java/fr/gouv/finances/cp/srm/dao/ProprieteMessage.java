/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 */
package fr.gouv.finances.cp.srm.dao;

/**
 * Propriété liée à la donnée Message
 * @see fr.gouv.finances.lombok.srm.bean.Message
 *
 * @author Christophe Breheret-Girardin
 */
public enum ProprieteMessage
{
    /** Propriété du libellé d'un message */
    PROPRIETE_LIBELLE("libelleMessage"),

    /** Propriété de date de début d'un message */
    PROPRIETE_DATE_DEBUT("dateDebutMessage"),

    /** Propriété de date de fin d'un message */
    PROPRIETE_DATE_FIN("dateFinMessage"),

    /** Propriété de date de début d'inactivité d'un profil */
    PROPRIETE_DATE_DEBUT_INACTIVITE("dateDebutInactiviteProfil"),

    /** Propriété de date de fin d'inactivité d'un profil */
    PROPRIETE_DATE_FIN_INACTIVITE("dateFinInactiviteProfil"),

    /** Propriété des profils désactivés */
    PROPRIETE_PROFILS_DESACTIVES("lesProfilsDesactives"),
    
    /** Propriété des profils destinataires */
    PROPRIETE_PROFILS_DESTINATAIRES("lesProfilsDestinataires"),

    /** Propriété des codiques destinataires */
    PROPRIETE_CODIQUES_DESTINATAIRES("lesCodiquesDestinataires");

    /** Nom de la propriété */
    private String nom;

    /**
     * Constructeur.
     *
     * @param libelleNom libellé du nom de la propriété
     */
    private ProprieteMessage(String libelleNom)
    {
        nom = libelleNom;
    }

    /**
     * Accesseur de nom
     *
     * @return nom
     */
    public String getNom()
    {
        return nom;
    }
    
}
