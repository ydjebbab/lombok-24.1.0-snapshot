/*
 * Copyright : Ministere du budget, des comptes publics et de la fonction publique        France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - srm
 *
 * fichier : MessageTypeService.java
 *
 */
package fr.gouv.finances.cp.srm.service;

import java.util.List;

import fr.gouv.finances.lombok.srm.bean.MessageType;

/**
 * Classe MessageTypeService.java.
 * 
 * @author amleplatinec
 * @version $Revision: 1.12 $
 */
public interface MessageTypeService
{

    /**
     * enregistre un message type.
     * 
     * @param messageType
     *            --
     */
    void enregistrerMessageType(MessageType messageType);

    /**
     * recherhe le libelle associe e unid.
     * 
     * @param messageTypeId
     *            --
     * @return the string
     */
    String rechercherLibelleAssocieAMessageTypeId(String messageTypeId);

    /**
     * recherche tous les messages type.
     * 
     * @return the list< message type>
     */
    List<MessageType> rechercherListeMessagesTypes();

    /**
     * recherche un message type e partir d'une pertie de son libelle.
     * 
     * @param libelle
     *            --
     * @return the list< message type>
     */
    List<MessageType> rechercherListeMessagesTypesParPartieLibelle(
        String libelle);

    /**
     * recherche un message type e partir de son libelle.
     * 
     * @param libelle
     *            --
     * @return the message type
     */
    MessageType rechercherMessageTypeParLibelle(String libelle);

    /**
     * supprime tous les message stypes.
     * 
     * @param listmessagetype
     *            --
     */
    void supprimerListeMessagesType(List<MessageType> listmessagetype);

    /**
     * supprime tous les message stypes.
     */
    void supprimerTousMessagesTypes();

    /**
     * verifie l'existence du mlessage type en base.
     * 
     * @param libelle
     *            --
     */
    void verifierExistenceMessageType(String libelle);

}