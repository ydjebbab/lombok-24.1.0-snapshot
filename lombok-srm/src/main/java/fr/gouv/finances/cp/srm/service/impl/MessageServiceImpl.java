/*
 * Copyright (c) 2017 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.cp.srm.service.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import fr.gouv.finances.cp.srm.dao.CodiqueDao;
import fr.gouv.finances.cp.srm.dao.MessageDao;
import fr.gouv.finances.cp.srm.dao.MessageTypeDao;
import fr.gouv.finances.cp.srm.dao.ProfilDao;
import fr.gouv.finances.cp.srm.service.MessageService;
import fr.gouv.finances.cp.srm.techbean.CriteresRechercheMessages;
import fr.gouv.finances.lombok.srm.bean.Codique;
import fr.gouv.finances.lombok.srm.bean.Message;
import fr.gouv.finances.lombok.srm.bean.MessageType;
import fr.gouv.finances.lombok.srm.bean.Profil;
import fr.gouv.finances.lombok.util.base.BaseServiceImpl;
import fr.gouv.finances.lombok.util.exception.ApplicationException;
import fr.gouv.finances.lombok.util.exception.ApplicationExceptionTransformateur;

/**
 * Classe MessageServiceImpl.java.
 * 
 * @author amleplatinec
 * @version $Revision: 1.22 $
 */
public class MessageServiceImpl extends BaseServiceImpl implements
    MessageService
{
    private static final Logger log = LoggerFactory.getLogger(MessageServiceImpl.class);

    private MessageDao messagedao;

    /** transaction template. */
    private TransactionTemplate transactionTemplate;

    /** profildao. */
    private ProfilDao profildao;

    /** messagetypedao. */
    private MessageTypeDao messagetypedao;

    /** codiquedao. */
    private CodiqueDao codiquedao;

    /**
     * Instanciation de message service impl.
     */
    public MessageServiceImpl()
    {
        super();
    }

    /**
     * creation d'un message simple (sans restrictions profils / codiques) et sans desactivation profils.
     *
     * @param message --
     * @param listCodiquesConcernesParMesage DOCUMENTEZ_MOI
     * @param actionMessageType --
     */

    /**
     * creation d'un message avec comme restrictions utilisateurs : le codique (possibilite de saisie de 3 codiques).
     * 
     * @param message --
     * @param listCodiquesConcernesParMesage --
     * @param actionMessageType --
     */
    @Override
    public void creerUnMessageAvecRestrictionsCodiques(final Message message,
        final Set listCodiquesConcernesParMesage,
        final String actionMessageType)
    {
        try
        {
            transactionTemplate.execute(new TransactionCallbackWithoutResult()
            {
                public void doInTransactionWithoutResult(
                    TransactionStatus status)
                {

                    traiterMessageType(message, actionMessageType);

                    Set lesCodiquesConcernesParMessage = new HashSet();

                    /**
                     * on verifie si le codique est deje cree sinon on le cree
                     */
                    for (Iterator iter = listCodiquesConcernesParMesage
                        .iterator(); iter.hasNext();)
                    {
                        Codique element = (Codique) iter.next();

                        Codique codiqueEnBase = rechercherExistenceCodique(element
                            .getLibelleCodique());

                        if (codiqueEnBase == null)
                        {
                            lesCodiquesConcernesParMessage.add(element);
                            if (log.isDebugEnabled())
                            {
                                log.debug("avant save codique");
                            }
                            codiquedao.saveCodique(element);
                            if (log.isDebugEnabled())
                            {
                                log.debug("apres save codique");
                            }
                        }
                        else
                        {
                            lesCodiquesConcernesParMessage.add(codiqueEnBase);
                        }
                        ;
                    }

                    /**
                     * on rattache e message ses principaux attributs et on sauve le message*
                     */
                    message.setLesCodiquesDestinataires(lesCodiquesConcernesParMessage);
                    if (log.isDebugEnabled())
                    {
                        log.debug("avant sauvegarde message");
                    }
                    messagedao.saveMessage(message);
                    if (log.isDebugEnabled())
                    {
                        log.debug("apres sauvegarde message");
                    }

                }
            });
        }
        // Interet du bloc d'exception : identifier les exception
        // verrouillage
        // optimiste
        // ajouter une info sur le contexte du plantage
        catch (ApplicationException e)
        {
            throw ApplicationExceptionTransformateur.transformer(
                "Erreur surcreation avec identifiant : "
                    + message.getLibelleMessage(),
                e);
        }
        if (log.isDebugEnabled())
        {
            log.debug("commit transaction passe avec succes");
        }

    };

    /**
     * creation d'un message sur une application avec en plus la desactivation de profils.
     *
     * @param message --
     * @param listProfilsConcernParMessage DOCUMENTEZ_MOI
     * @param listCodiquesConcernParMessage DOCUMENTEZ_MOI
     * @param actionMessageType --
     */

    /**
     * creation d'un message avec comme restrictions utilisateurs : leurs profils et/ou le codique (possibilite de
     * saisie de 3 codiques).
     * 
     * @param message --
     * @param listProfilsConcernParMessage --
     * @param listCodiquesConcernParMessage --
     * @param actionMessageType --
     */
    @Override
    public void creerUnMessageAvecRestrictionsProfilsCodiques(
        final Message message,
        final List listProfilsConcernParMessage,
        final Set listCodiquesConcernParMessage,
        final String actionMessageType)
    {
        try
        {
            transactionTemplate.execute(new TransactionCallbackWithoutResult()
            {
                public void doInTransactionWithoutResult(
                    TransactionStatus status)
                {

                    traiterMessageType(message, actionMessageType);
                    Set lesProfilsConcernesParMessage = new HashSet();
                    Set lesCodiquesConcernesParMessage = new HashSet();

                    /**
                     * on verifie si le codique est deje cree sinon on le cree
                     */
                    for (Iterator iter = listCodiquesConcernParMessage
                        .iterator(); iter.hasNext();)
                    {
                        Codique element = (Codique) iter.next();

                        Codique codiqueEnBase = rechercherExistenceCodique(element
                            .getLibelleCodique());

                        if (codiqueEnBase == null)
                        {
                            lesCodiquesConcernesParMessage.add(element);
                            if (log.isDebugEnabled())
                            {
                                log.debug("avant save codique");
                            }
                            codiquedao.saveCodique(element);
                            if (log.isDebugEnabled())
                            {
                                log.debug("apres save codique");
                            }
                        }
                        else
                        {
                            lesCodiquesConcernesParMessage.add(codiqueEnBase);
                        }
                        ;
                    }

                    /**
                     * **on verifie si le profil est deje cree en base sinon on le cree
                     */

                    if (listProfilsConcernParMessage != null
                        && listProfilsConcernParMessage.size() > 0)
                    {
                        for (Iterator iter = listProfilsConcernParMessage
                            .iterator(); iter.hasNext();)
                        {
                            Profil element = (Profil) iter.next();
                            // normallement
                            // profilEnBase doit
                            // toujours exister
                            // car
                            // appli et profils
                            // crees auparavant
                            Profil profilEnBase = rechercherExistenceProfilParCode(element
                                .getCodeProfil());

                            if (profilEnBase == null)
                            {
                                listProfilsConcernParMessage.add(element);
                                if (log.isDebugEnabled())
                                {
                                    log.debug("avant save profil");
                                }
                                profildao.saveProfil(element);
                                if (log.isDebugEnabled())
                                {
                                    log.debug("apres save profil");
                                }
                            }
                            else
                            {
                                lesProfilsConcernesParMessage.add(profilEnBase);
                            }
                            ;
                        }
                    }

                    /**
                     * on rattache e message ses principaux attributs et on sauve le message*
                     */
                    message.setLesProfilsDestinataires(lesProfilsConcernesParMessage);
                    message.setLesCodiquesDestinataires(lesCodiquesConcernesParMessage);
                    if (log.isDebugEnabled())
                    {
                        log.debug("avant sauvegarde message");
                    }
                    messagedao.saveMessage(message);
                    if (log.isDebugEnabled())
                    {
                        log.debug("apres sauvegarde message");
                    }

                }
            });
        }
        // Interet du bloc d'exception : identifier les exception
        // verrouillage
        // optimiste
        // ajouter une info sur le contexte du plantage
        catch (ApplicationException e)
        {
            throw ApplicationExceptionTransformateur.transformer(
                "Erreur surcreation avec identifiant : "
                    + message.getLibelleMessage(),
                e);
        }
        if (log.isDebugEnabled())
        {
            log.debug("commit transaction passe avec succes");
        }

    };

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.cp.srm.service.MessageService#creerUnMessageDesactiveProfil(fr.gouv.finances.lombok.srm.bean.Message,
     *      java.util.List, java.lang.String)
     */
    @Override
    public void creerUnMessageDesactiveProfil(final Message message,
        final List listProfils, final String actionMessageType)
    {
        try
        {
            transactionTemplate.execute(new TransactionCallbackWithoutResult()
            {
                public void doInTransactionWithoutResult(
                    TransactionStatus status)
                {
                    traiterMessageType(message, actionMessageType);
                    Set<Profil> listProfils1 = new HashSet<Profil>();

                    // on verifie que les profils sont bien deje crees en base

                    for (Iterator<Profil> iter = listProfils.iterator(); iter
                        .hasNext();)
                    {
                        Profil element = iter.next();

                        Profil profilEnBase = rechercherExistenceProfilParCode(element
                            .getCodeProfil());

                        if (profilEnBase != null)
                        {
                            listProfils1.add(profilEnBase);
                        }

                    }

                    // for (Iterator<Profil> iter = listProfils1.iterator(); iter
                    // .hasNext();)
                    // {
                    // Profil element = iter.next();
                    //
                    // applicationEnBase.rattacherUnNouveauProfil(element);
                    // }

                    message.setLesProfilsDesactives(listProfils1);
                    if (log.isDebugEnabled())
                    {
                        log.debug("avant sauvegarde message");
                    }
                    messagedao.saveMessage(message);
                    if (log.isDebugEnabled())
                    {
                        log.debug("apres sauvegarde message");
                    }
                }

                // }
            });
        }

        // Interet du bloc d'exception : identifier les exception
        // verrouillage
        // optimiste
        // ajouter une info sur le contexte du plantage
        catch (ApplicationException e)
        {
            throw ApplicationExceptionTransformateur.transformer(
                "Erreur surcreation avec identifiant : "
                    + message.getLibelleMessage(),
                e);
        }
        if (log.isDebugEnabled())
        {
            log.debug("commit transaction passe avec succes");
        }

    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.cp.srm.service.MessageService#creerUnMessageSurAppli(fr.gouv.finances.lombok.srm.bean.Message,
     *      java.lang.String)
     */
    @Override
    public void creerUnMessageSurAppli(final Message message,
        final String actionMessageType)
    {
        try
        {
            transactionTemplate.execute(new TransactionCallbackWithoutResult()
            {
                public void doInTransactionWithoutResult(
                    TransactionStatus status)
                {

                    traiterMessageType(message, actionMessageType);

                    // on sauve le message
                    if (log.isDebugEnabled())
                    {
                        log.debug("avant sauvegarde message");
                    }
                    messagedao.saveMessage(message);
                    if (log.isDebugEnabled())
                    {
                        log.debug("apres sauvegarde message");
                    }
                }
            });
        }
        // Interet du bloc d'exception : identifier les exception
        // verrouillage
        // optimiste
        // ajouter une info sur le contexte du plantage
        catch (ApplicationException e)
        {
            throw ApplicationExceptionTransformateur.transformer(
                "Erreur surcreation avec identifiant : "
                    + message.getLibelleMessage(),
                e);
        }
        if (log.isDebugEnabled())
        {
            log.debug("commit transaction passe avec succes");
        }

    }

    /**
     * Gets the codiquedao.
     *
     * @return the codiquedao
     */
    public CodiqueDao getCodiquedao()
    {
        return codiquedao;
    }

    /**
     * Gets the messagedao.
     *
     * @return the messagedao
     */
    public MessageDao getMessagedao()
    {
        return messagedao;
    }

    /**
     * Gets the messagetypedao.
     *
     * @return the messagetypedao
     */
    public MessageTypeDao getMessagetypedao()
    {
        return messagetypedao;
    }

    /**
     * Gets the profildao.
     *
     * @return the profildao
     */
    public ProfilDao getProfildao()
    {
        return profildao;
    }

    /**
     * Gets the transaction template.
     *
     * @return the transaction template
     */
    public TransactionTemplate getTransactionTemplate()
    {
        return transactionTemplate;
    }

    /**
     * avant de creer un message, on verifie qu il n existe pas en base regle de gestion e faire valider : on regarde
     * sur libelle +application+dates*.
     *
     * @param message --
     * @return the message
     */
    @Override
    public Message rechercherMessage(Message message)
    {
        Message messageTrouve = messagedao.findMessageExisteDeja(message);

        return messageTrouve;

    }

    /**
     * cas de la modification.
     *
     * @param message --
     * @return the message
     */
    @Override
    public Message rechercherMessagePourModif(Message message)
    {
        return messagedao.findMessageExisteDejaEnModif(message);

    }

    /**
     * recherche de messages e partir de criteres (application , dates , uid createur...)
     * 
     * @param crit --
     * @return the list< message>
     */
    @Override
    public List<Message> rechercherMessages(CriteresRechercheMessages crit)
    {
        return messagedao.findMessages(crit);
    }

    /**
     * ************************************************************************* on ramene les MESSAGES qui contiennnet
     * deje les profils desactives ************************************************************************.
     *
     * @param lesProfils --
     * @return the list< message>
     */
    @Override
    public List<Message> rechercherMessagesAvecProfilsDejaDesactives(List lesProfils)
    {
        List<Message> listMessagesASupprimer = new ArrayList<Message>();
        CriteresRechercheMessages crit = new CriteresRechercheMessages();

        List<Message> listMessages = messagedao.findMessages(crit);
        int inti;

        for (inti = 0; inti < listMessages.size(); inti++)
        {
            Message message = listMessages.get(inti);
            Set listProfilsDesactives1 = message.getLesProfilsDesactives();

            if (listProfilsDesactives1.size() > 0)
            {
                for (Iterator iter = lesProfils.iterator(); iter.hasNext();)
                {
                    Profil element = (Profil) iter.next();

                    if (listProfilsDesactives1.contains(element)
                        && (!listMessagesASupprimer.contains(message)))
                    {
                        listMessagesASupprimer.add(message);
                    }

                }
            }
        }
        return listMessagesASupprimer;

    }

    /**
     * ************************************************************************* dans le cadre du batch , on recherche
     * les messages dont la date de fin est depassee
     * ************************************************************************.
     * 
     * @return the list< message>
     */
    @Override
    public List<Message> rechercherMessagesPerimes()
    {
        return messagedao.findMessagesPerimes();

    };

    /**
     * ************************************************************************* avant de desactiver un profil on
     * verifie s il est ou non deje desactive on ramene les profils de LesProfils qui sont desactives
     * ************************************************************************.
     *
     * @param lesProfils --
     * @return the list< profil>
     */
    @Override
    public List<Profil> rechercherProfilsDesactives(List<Profil> lesProfils)
    {
        List<Profil> listProfilsDesactives = new ArrayList<Profil>();
        CriteresRechercheMessages crit = new CriteresRechercheMessages();

        List<Message> listMessages = messagedao.findMessages(crit);
        int inti;

        for (inti = 0; inti < listMessages.size(); inti++)
        {
            Message message = listMessages.get(inti);
            Set<Profil> listProfilsDesactives1 = message
                .getLesProfilsDesactives();

            if (listProfilsDesactives1.size() > 0)
            {
                for (Iterator<Profil> iter = lesProfils.iterator(); iter
                    .hasNext();)
                {
                    Profil element = iter.next();

                    if (listProfilsDesactives1.contains(element))
                    {
                        listProfilsDesactives.add(element);
                    }
                }
            }
        }
        return listProfilsDesactives;

    };

    /**
     * methode Rechercher si codique est rattache d autres messages : --.
     * 
     * @param unCodique --
     * @param unMessage --
     * @return list< message>
     */
    public List<Message> rechercherSiCodiqueEstRattacheDAutresMessages(
        Codique unCodique, Message unMessage)
    {
        List<Message> listMessagesAssociesACodique = new ArrayList<>();

        listMessagesAssociesACodique = messagedao
            .findMessagesAyantLibelleCodiqueRattacheAAutreMessage(
                unCodique.getLibelleCodique(), unMessage);
        return listMessagesAssociesACodique;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.cp.srm.service.MessageService#rechercherTousLesMessages()
     */
    public List<Message> rechercherTousLesMessages()
    {
        List<Message> listMessages = new ArrayList<>();

        listMessages = messagedao.findTousLesMessages();
        return listMessages;
    }

    /**
     * Sets the codiquedao.
     *
     * @param codiquedao the new codiquedao
     */
    public void setCodiquedao(CodiqueDao codiquedao)
    {
        this.codiquedao = codiquedao;
    }

    /**
     * Sets the messagedao.
     *
     * @param messagedao the new messagedao
     */
    public void setMessagedao(MessageDao messagedao)
    {
        this.messagedao = messagedao;
    }

    /**
     * Sets the messagetypedao.
     *
     * @param messagetypedao the new messagetypedao
     */
    public void setMessagetypedao(MessageTypeDao messagetypedao)
    {
        this.messagetypedao = messagetypedao;
    }

    /**
     * Sets the profildao.
     *
     * @param profildao the new profildao
     */
    public void setProfildao(ProfilDao profildao)
    {
        this.profildao = profildao;
    }

    /**
     * Sets the transaction template.
     *
     * @param transactionTemplate the new transaction template
     */
    public void setTransactionTemplate(TransactionTemplate transactionTemplate)
    {
        this.transactionTemplate = transactionTemplate;
    }

    /**
     * suppression de * messages e partir du tableau EC.
     * 
     * @param listMessages --
     */
    @Override
    public void supprimerListeMessages(final List<Message> listMessages)
    {
        {
            try
            {
                transactionTemplate
                    .execute(new TransactionCallbackWithoutResult()
                    {
                        public void doInTransactionWithoutResult(
                            TransactionStatus status)
                        {
                            Message unmessageasupprimer = null;

                            Iterator<Message> uniterator = listMessages
                                .iterator();
                            while (uniterator.hasNext())
                            {
                                unmessageasupprimer = uniterator.next();
                                supprimerUnMessage(unmessageasupprimer);
                            }

                        }
                    });
            }
            // Interet du bloc d'exception : identifier les
            // exceptions
            // verouillage optimiste
            // ajouter une info sur le contexte du plantage
            catch (ApplicationException e)
            {
                throw ApplicationExceptionTransformateur
                    .transformer(
                        "Erreur sur la validation de la transaction de suppression  de plusieurs messages",
                        e);
            }
        }

    }

    /**
     * supression de 1 message (simple ou avec profil) rajout amlp 05/07/07 . il faut supprimer de plus les codiques
     * destinataires associes
     * 
     * @param message --
     */
    @Override
    public void supprimerUnMessage(final Message message)
    {
        {
            try
            {
                transactionTemplate
                    .execute(new TransactionCallbackWithoutResult()
                    {
                        public void doInTransactionWithoutResult(
                            TransactionStatus status)
                        {
                            if (log.isDebugEnabled())
                            {
                                log.debug("debut supprimer un message"
                                    + message.getLibelleMessage());
                            }

                            Set<Codique> lesCodiquesDesti = message
                                .getLesCodiquesDestinataires();
                            Iterator<Codique> uncodiquedestiterator = lesCodiquesDesti
                                .iterator();

                            Codique uncodiquedestasupprimer = null;
                            while (uncodiquedestiterator.hasNext())
                            {
                                Codique codique = (uncodiquedestiterator
                                    .next());
                                List<Message> lesMessagesAssociesACodique = rechercherSiCodiqueEstRattacheDAutresMessages(
                                    codique, message);
                                if (lesMessagesAssociesACodique != null
                                    && lesMessagesAssociesACodique
                                        .size() == 0)
                                {
                                    uncodiquedestasupprimer = codique;
                                }
                            }

                            messagedao.deleteMessage(message);
                            if (log.isDebugEnabled())
                            {
                                log.debug("fin supprimer un message"
                                    + message.getLibelleMessage());
                            }
                            if (uncodiquedestasupprimer != null)
                            {
                                if (log.isDebugEnabled())
                                {
                                    log.debug("codique e supprimer : "
                                        + uncodiquedestasupprimer
                                            .getId());
                                }
                                codiquedao
                                    .deleteObject(uncodiquedestasupprimer);
                                if (log.isDebugEnabled())
                                {
                                    log.debug("sortie de codique e supprimer : "
                                        + uncodiquedestasupprimer
                                            .getId());
                                }
                            }

                        }
                    });
            }
            // Interet du bloc d'exception : identifier les
            // exceptions
            // verouillage optimiste
            // ajouter une info sur le contexte du plantage
            catch (ApplicationException e)
            {
                throw ApplicationExceptionTransformateur
                    .transformer(
                        "Erreur sur la validation de la transaction de suppression  d un message",
                        e);
            }
        }

    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.cp.srm.service.MessageService#verifierSiProfilReferenceParDesMessages(fr.gouv.finances.lombok.srm.bean.Profil)
     */
    @Override
    public boolean verifierSiProfilReferenceParDesMessages(Profil profil)
    {
        boolean estReference = false;
        for (Message message : rechercherTousLesMessages())
        {
            if (message.getLesProfilsDesactives().contains(profil)
                || message.getLesProfilsDestinataires().contains(profil))
            {
                estReference = true;
            }
        }
        return estReference;
    }

    /**
     * recherche existence d'un codique.
     * 
     * @param libelle --
     * @return the codique
     */
    private Codique rechercherExistenceCodique(String libelle)
    {
        return codiquedao.findCodique(libelle);
    }

    /**
     * recherche existence d'un profil.
     * 
     * @param code --
     * @return the profil
     */

    private Profil rechercherExistenceProfilParCode(String code)
    {
        return profildao.findProfilParCode(code);
    }

    /**
     * <pre>
     * Creation d'un message type 
     * 05/10/06 modification: on ne permet plus que la creation et on ne peut plus les supprimer tous (menu special messages types)
     * </pre>
     * 
     * @param message --
     * @param actionMessageType --
     */
    private void traiterMessageType(Message message, String actionMessageType)
    {

        if ((actionMessageType != null)
            && (actionMessageType.compareTo("1") == 0))
        {
            MessageType messageType = new MessageType();
            String libelleMessageType = message.getLibelleMessage();

            messageType.setLibelleMessageType(libelleMessageType);
            if (log.isDebugEnabled())
            {
                log.debug("avant sauvegarde messageType");
            }
            messagetypedao.saveMessageType(messageType);
            if (log.isDebugEnabled())
            {
                log.debug("apres sauvegarde messageType");
            }

        }

    }

}
