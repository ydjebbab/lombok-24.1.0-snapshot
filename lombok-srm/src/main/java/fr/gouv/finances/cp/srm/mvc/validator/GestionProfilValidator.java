/*
 * Copyright (c) 2016 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.cp.srm.mvc.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import fr.gouv.finances.cp.srm.mvc.form.GestionProfilForm;

/**
 * Class GestionApplicationValidator 
 */
public class GestionProfilValidator implements Validator
{
    
    private static final long serialVersionUID = 1L;

    public GestionProfilValidator()
    {
        super(); 
    }

    /** 
     * (methode de remplacement)
     * {@inheritDoc}
     * @see org.springframework.validation.Validator#supports(java.lang.Class)
     */
    @Override
    public boolean supports(Class<?> clazz)
    {
        return clazz.equals(GestionProfilForm.class);
    }

    /** 
     * (methode de remplacement)
     * {@inheritDoc}
     * @see org.springframework.validation.Validator#validate(java.lang.Object, org.springframework.validation.Errors)
     */
    @Override
    public void validate(Object target, Errors errors)
    {
        // RAS
    }

}
