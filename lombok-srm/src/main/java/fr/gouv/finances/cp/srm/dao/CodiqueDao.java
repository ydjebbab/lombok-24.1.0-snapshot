/*
 * Copyright : Ministere du budget, des comptes publics et de la fonction publique        France
 * Contributeur(s) : 
 * - amleplatinec
 *
 *
 *
 * fichier : CodiqueDao.java
 *
 */
package fr.gouv.finances.cp.srm.dao;

import fr.gouv.finances.lombok.srm.bean.Codique;
import fr.gouv.finances.lombok.util.base.CoreBaseDao;

/**
 * Interface CodiqueDao --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.12 $ Date: 22 dec. 2009
 */
public interface CodiqueDao extends CoreBaseDao
{

    /**
     * recherche le codique par le libelle.
     * 
     * @param libelle
     *            --
     * @return the codique
     */
    Codique findCodique(String libelle);

    /**
     * sauve le codique en base.
     * 
     * @param codique
     *            --
     */
    void saveCodique(Codique codique);
}
