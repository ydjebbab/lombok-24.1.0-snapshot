/*
 * Copyright : Ministere du budget, des comptes publics et de la fonction publique        France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - srm
 *
 * fichier : TraitementSuppressionMessagesImpl.java
 *
 */
package fr.gouv.finances.cp.srm.batch;

import java.util.List;
import fr.gouv.finances.cp.srm.service.MessageService;
import fr.gouv.finances.lombok.batch.ServiceBatchCommunImpl;
import fr.gouv.finances.lombok.srm.bean.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

/**
 * Classe TraitementSuppressionMessagesImpl.java.
 *
 * @author amleplatinec
 * @version $Revision: 1.16 $
 */
@Service("traitementsuppressionmessages")
@Profile("batch")
@Lazy(true)
public class TraitementSuppressionMessagesImpl extends ServiceBatchCommunImpl {

    @Autowired()
    @Qualifier("messageserviceso")
    private MessageService messageserviceso;

    public TraitementSuppressionMessagesImpl() {
        super();
    }

    /**
     * Accesseur de l attribut messageserviceso.
     *
     * @return messageserviceso
     */
    public MessageService getMessageserviceso() {
        return messageserviceso;
    }

    /**
     * Modificateur de l attribut messageserviceso.
     *
     * @param messageserviceso
     *            le nouveau messageserviceso
     */
    public void setMessageserviceso(MessageService messageserviceso) {
        this.messageserviceso = messageserviceso;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     *
     * @see fr.gouv.finances.lombok.batch.ServiceBatchCommunImpl#traiterBatch()
     */
    @Override
    public void traiterBatch() {
        int nbArticleErreur = 0;
        List<Message> listoperations = traiterRechercherLesMessagesPerimes();
        if ((listoperations != null) && (!listoperations.isEmpty())) {
            traiterEffacerLesMessagesPerimes(listoperations);
        } else // aucune operation
        {
            log.info("Aucun message  e supprimer e ce jour : ");
            nbArticleErreur += 1;
        }
    }

    /**
     * methode Traiter effacer les messages perimes : --.
     *
     * @param listoperations
     *            --
     */
    private void traiterEffacerLesMessagesPerimes(List<Message> listoperations) {
        log.info("effacement des messages e supprimer");
        messageserviceso.supprimerListeMessages(listoperations);
        if ((listoperations != null) && (!listoperations.isEmpty())) {
            log.info("il y en a" + listoperations.size());
        } else {
            log.info("il y en a 0 ");
        }
    }

    // par defaut , on recherche les operations faites depuis au moins 6 jours
    /**
     * methode Traiter rechercher les messages perimes : --.
     *
     * @return list< message>
     */
    private List<Message> traiterRechercherLesMessagesPerimes() {
        log.info("recherche des messages perimes");
        List<Message> listoperations = messageserviceso.rechercherMessagesPerimes();
        return listoperations;
    }

    @Value("${traitementsuppressionmessages.versionbatch}")
    @Override()
    public void setVersionBatch(String versionBatch) {
        super.setVersionBatch(versionBatch);
    }

    @Value("${traitementsuppressionmessages.nombatch}")
    @Override()
    public void setNomBatch(String nomBatch) {
        super.setNomBatch(nomBatch);
    }
}
