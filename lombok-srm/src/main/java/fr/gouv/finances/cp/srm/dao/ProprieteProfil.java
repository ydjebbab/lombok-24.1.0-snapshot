/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 */
package fr.gouv.finances.cp.srm.dao;

/**
 * Enumération pour manipuler les propriétés du bean Profil
 * 
 * @author fernandescn Date: 11 oct. 2018
 */
public enum ProprieteProfil
{

    /** Libellé profil */
    PROPRIETE_LIBELLE("libelleProfil"),

    /** Code profil */
    PROPRIETE_CODE("codeProfil"),

    /** Code profil */
    PROPRIETE_CODECONCATCODEAPPLI("codeProfilConcatCodeAppli");

    /** Nom de la propriété */
    private String nom;

    /**
     * Constructeur.
     *
     * @param libelleNom libellé du nom de la propriété
     */
    private ProprieteProfil(String libelleNom)
    {
        nom = libelleNom;
    }

    /**
     * Accesseur de nom
     *
     * @return nom
     */
    public String getNom()
    {
        return nom;
    }
}
