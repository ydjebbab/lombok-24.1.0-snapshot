/*
 * Copyright : Ministere du budget, des comptes publics et de la fonction publique        France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - srm
 *
 * fichier : RechercheMessagesFormAction.java
 *
 */
package fr.gouv.finances.cp.srm.mvc.webflow.action;

import java.lang.reflect.InvocationTargetException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import org.apache.commons.beanutils.PropertyUtils;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.DataBinder;
import org.springframework.webflow.action.FormAction;
import org.springframework.webflow.execution.Event;
import org.springframework.webflow.execution.RequestContext;

import fr.gouv.finances.cp.srm.mvc.form.RechercheMessagesForm;
import fr.gouv.finances.cp.srm.service.MessageService;
import fr.gouv.finances.cp.srm.techbean.CriteresRechercheMessages;
import fr.gouv.finances.lombok.apptags.util.CheckboxSelectUtil;
import fr.gouv.finances.lombok.securite.techbean.HabilitationAnnuaire;
import fr.gouv.finances.lombok.securite.techbean.PersonneAnnuaire;
import fr.gouv.finances.lombok.srm.bean.Message;
import fr.gouv.finances.lombok.util.exception.ProgrammationException;

/*
 * author amleplatinec-cp
 * recherche de messages flux principal, ensuite 2 sous flux associes ,modification et suppresssion de messages 
 * flux principal ; recherchemessages-flow
 */
/**
 * Class RechercheMessagesFormAction --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.26 $ Date: 22 dec. 2009
 */
public class RechercheMessagesFormAction extends FormAction
{

    /** Constant : TYPE_DE_MESSAGE. */
    public static final String TYPE_DE_MESSAGE = "typemessage";

    /** EXPLOITANT. */
    private static String exploitant = "exploitant";

    /** messageserviceso. */
    private MessageService messageserviceso;

    public RechercheMessagesFormAction()
    {
        super();
    }

    public RechercheMessagesFormAction(Class arg0)
    {
        super(arg0);
    }

    /**
     * recherche du message correcpondant aux criteres saisis.
     * 
     * @param context
     *            --
     * @return the event
     */

    public Event executerRechercherMessages(RequestContext context)
    {
        RechercheMessagesForm rechercheMessagesForm = (RechercheMessagesForm) context
            .getFlowScope().getRequired(getFormObjectName(),
                RechercheMessagesForm.class);
        CriteresRechercheMessages crit = rechercheMessagesForm.getCrit();

        List<Message> listMessage = messageserviceso.rechercherMessages(crit);

        context.getFlowScope().put("listemessages", listMessage);
        // Sauvegarde de l'objet de parametrage des checkboxes dans le
        // FlowScope
        CheckboxSelectUtil
            .parametrerCheckboxes(context, "rechmess")
            .utiliserRowid("id")
            .utiliserListeElements("listemessages")
            .utiliserMsgSelectionneUnSeulElement(
                "Selectionnez un seul message a la fois.")
            .utiliserMsgAucunEltSelectionne(
                "Aucun message n'est selectionne");
        return success();
    }

    /**
     * Accesseur de l attribut messageserviceso.
     * 
     * @return messageserviceso
     */
    public MessageService getMessageserviceso()
    {
        return messageserviceso;
    }

    /**
     * recherche des types de message pour affichage*.
     * 
     * @param context
     *            --
     * @return the event
     */
    public Event preparerBeanCriteresDeRecherche(RequestContext context)
    {
        // Initialisation bean typesderecherche
        Map typesderecherche = CriteresRechercheMessages.getTypeMessage();

        context.getFlowScope().put(TYPE_DE_MESSAGE, typesderecherche);
        return success();
    }

    /**
     * ATTENTION : code similaire pour la creation de messages ... prepare la
     * liste des applications cas creation le chef de projet voit uniquement les
     * applications mises dans le filtre de l'habilitation sur sireme
     * l'exploitant voit toutes les applications en base
     * 
     * @param context
     *            --
     * @return the event
     */
//    public Event preparerRechercherListeApplications(RequestContext context)
//    {
//        Event unEvent;
//
//        PersonneAnnuaire personne = UtilisateurSecurityLombokContainer
//            .getPersonneAnnuaire();
//
//        List<Application> listApplications = new ArrayList<Application>();
//
//        if (estExploitant(personne))
//        {
//            listApplications = applicationserviceso
//                .rechercherListeApplications();
//
//            // on construit une appli fictive qui permet de selectionner
//            // l' application fictive
//            if (applicationserviceso
//                .rechercherApplicationParLibelleCourt("toutes") == null)
//            {
//                Application appli = new Application();
//
//                appli.setLibelleCourt("toutes");
//                appli.setLibelleLong("toutes");
//                if ((listApplications != null) && (listApplications.size() > 0))
//                {
//                    listApplications.add(appli);
//                }
//                else
//                {
//                    listApplications = new ArrayList<Application>();
//
//                    listApplications.add(appli);
//                }
//            }
//
//        }
//        else
//        {
//
//            List<Application> lesAppliMetiers = applicationserviceso
//                .rechercherListApplicationsDansBase(personne);
//
//            if ((lesAppliMetiers != null) && (lesAppliMetiers.size() > 0))
//            {
//                Iterator<Application> itlesAppliMetiers = lesAppliMetiers
//                    .iterator();
//
//                while (itlesAppliMetiers.hasNext())
//                {
//                    Application applicationEnBase = itlesAppliMetiers.next();
//                    if (applicationEnBase != null)
//                    {
//                        listApplications.add(applicationEnBase);
//                    }
//                }
//            }
//        }
//        if ((listApplications != null) && (listApplications.size() > 0))
//        {
//            context.getFlowScope().put("listapplications", listApplications);
//            unEvent = success();
//        }
//        else
//        {
//            Errors errors = new FormObjectAccessor(context).getFormErrors(
//                getFormObjectName(), ScopeType.REQUEST);
//            errors.reject(
//
//                null,
//                "Les erreurs suivantes se sont produites : soit aucun filtre  application de saisie dans"
//                    + " l'habilitation portant sur l'application SIREME (Veuillez contacter"
//                    + " votre administrateur APTERA)"
//                    + ": soit l'application sur laquelle vous desirez inscrire un message"
//                    + " n'est pas enregistree dans la base de SIREME");
//            unEvent = result("error");
//        }
//
//        return unEvent;
//    };

    /**
     * ATTENTION : code similaire pour la creation de messages ... prepare la
     * liste des applications cas creation le chef de projet voit uniquement les
     * applications mises dans le filtre de l'habilitation sur sireme
     * l'exploitant voit toutes les applications en base
     * 
     * @param context
     *            --
     * @return the event
     */
//    public Event preparerRechercherListeApplicationsMetiers(
//        RequestContext context)
//    {
//        Event unEvent;
//
//        PersonneAnnuaire personne = UtilisateurSecurityLombokContainer
//            .getPersonneAnnuaire();
//
//        List<Application> listApplications = applicationserviceso
//            .rechercherListApplicationsDansBase(personne);
//
//        if ((listApplications != null) && (listApplications.size() > 0))
//        {
//            context.getFlowScope().put("listapplications", listApplications);
//            unEvent = success();
//        }
//        else
//        {
//            Errors errors = new FormObjectAccessor(context).getFormErrors(
//                getFormObjectName(), ScopeType.REQUEST);
//            errors.rejectValue(
//                "application.libelleCourt",
//                null,
//                "Les erreurs suivantes se sont produites : soit aucun filtre  application de saisie"
//                    + " dans l'habilitation portant sur l'application SIREME "
//                    + "(Veuillez contacter votre administrateur APTERA)"
//                    + ": soit l'application sur laquelle vous desirez inscrire un message"
//                    + " n'est pas enregistree dans la base de SIREME");
//            unEvent = result("error");
//        }
//        return unEvent;
//    }

    /**
     * recherche du message correcpondant aux criteres saisis.
     * 
     * @param context
     *            --
     * @return the event
     */

    public Event preparerRechercherListeMessages(RequestContext context)
    {

        List<Message> listMessage = messageserviceso
            .rechercherTousLesMessages();

        context.getFlowScope().put("listemessages", listMessage);
        // Sauvegarde de l'objet de parametrage des checkboxes dans le
        // FlowScope
        CheckboxSelectUtil
            .parametrerCheckboxes(context, "rechmess")
            .utiliserRowid("id")
            .utiliserListeElements("listemessages")
            .utiliserMsgSelectionneUnSeulElement(
                "Selectionnez un seul message a la fois.")
            .utiliserMsgAucunEltSelectionne(
                "Aucun message n'est selectionne");
        return success();
    }

    /**
     * pour reaffichage page de saisie e blanc apres un retour.
     * 
     * @param context
     *            --
     * @return the event
     */
    public Event restaurePageSaisie(RequestContext context)
    {
        RechercheMessagesForm rechercheMessagesForm = (RechercheMessagesForm) context
            .getFlowScope().getRequired(getFormObjectName(),
                RechercheMessagesForm.class);
        CriteresRechercheMessages crit = rechercheMessagesForm.getCrit();
        CriteresRechercheMessages critsav = new CriteresRechercheMessages();

        try
        {
            PropertyUtils.copyProperties(crit, critsav);
        }
        catch (IllegalAccessException e)
        {

            throw new ProgrammationException(e);
        }
        catch (InvocationTargetException e)
        {

            throw new ProgrammationException(e);
        }
        catch (NoSuchMethodException e)
        {
            throw new ProgrammationException(e);
        }
        return success();
    }

    /**
     * Modificateur de l attribut messageserviceso.
     * 
     * @param messageserviceso
     *            le nouveau messageserviceso
     */
    public void setMessageserviceso(MessageService messageserviceso)
    {
        this.messageserviceso = messageserviceso;
    }

    /**
     * methode Traiter checkboxes entre pages : --.
     * 
     * @param context
     *            --
     * @return event
     */
    public Event traiterCheckboxesEntrePages(RequestContext context)
    {
        CheckboxSelectUtil.traiterCheckboxesEntrePages(context, "rechmess");
        return success();
    }

    /**
     * methode Traiter message erreur retourne : --.
     * 
     * @param context
     *            --
     * @return event
     */
    public Event traiterMessageErreurRetourne(RequestContext context)
    {
        CheckboxSelectUtil.traiterMessageErreurRetourne(context);
        return success();
    }

    /**
     * 
     * (methode de remplacement)
     * {@inheritDoc}
     * @see org.springframework.webflow.action.FormAction#initBinder(org.springframework.webflow.execution.RequestContext, org.springframework.validation.DataBinder)
     */
    protected void initBinder(RequestContext request, DataBinder binder)
    {
        binder.registerCustomEditor(Long.class, new CustomNumberEditor(
            Long.class, true));
        // Formatage des dates stricts : les dates qui ne sont pas dans le
        // calendrier sont rejettee
        // Le binder par defaut fournit par Spring decale la date dans le
        // calendrier
        Locale localefr = Locale.FRANCE;

        SimpleDateFormat dateformat1 = new SimpleDateFormat("dd/MM/yyyy",
            localefr);
        dateformat1.setLenient(false);
        binder.registerCustomEditor(Date.class, new CustomDateEditor(
            dateformat1, true));
    }

    /**
     * verifie si une personne est exploitant.
     * 
     * @param personne
     *            --
     * @return true, if est exploitant
     */
    private boolean estExploitant(PersonneAnnuaire personne)
    {
        boolean estExploitant = false;
        Set<HabilitationAnnuaire> listHab = personne.getListeHabilitations();
        Iterator<HabilitationAnnuaire> leshab = listHab.iterator();

        while (leshab.hasNext())
        {
            if ((leshab.next()).getNomProfil().compareTo(exploitant) == 0)
            {
                estExploitant = true;
                break;
            }
        }
        return estExploitant;
    }

}
