/*
 * Copyright : Ministere du budget, des comptes publics et de la fonction publique        France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - srm
 *
 * fichier : CreationMessageTypeForm.java
 *
 */
package fr.gouv.finances.cp.srm.mvc.form;

import java.io.Serializable;

import fr.gouv.finances.lombok.srm.bean.MessageType;

/**
 * Class CreationMessageTypeForm --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.16 $ Date: 22 dec. 2009
 */
public class CreationMessageTypeForm implements Serializable
{

    private static final long serialVersionUID = 1L;

    private MessageType messagetypeform = new MessageType();

    public CreationMessageTypeForm()
    {
        super();
    }

    /**
     * Accesseur de l attribut messagetypeform.
     * 
     * @return messagetypeform
     */
    public MessageType getMessagetypeform()
    {
        return messagetypeform;
    }

    /**
     * Modificateur de l attribut messagetypeform.
     * 
     * @param messagetype
     *            le nouveau messagetypeform
     */
    public void setMessagetypeform(MessageType messagetype)
    {
        this.messagetypeform = messagetype;
    }

}
