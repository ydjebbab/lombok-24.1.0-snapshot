/*
 * Copyright : Ministere du budget, des comptes publics et de la fonction publique        France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - srm
 *
 * fichier : SuppressionMessagesForm.java
 *
 */
package fr.gouv.finances.cp.srm.mvc.form;

import java.io.Serializable;
import java.util.List;

import fr.gouv.finances.lombok.srm.bean.Message;

/**
 * Class SuppressionMessagesForm --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.10 $ Date: 22 dec. 2009
 */
public class SuppressionMessagesForm implements Serializable
{

    private static final long serialVersionUID = 470410828253049636L;

    /** liste des messages a supprimer. */
    private List<Message> listeDesMessagesASupprimer;

    /** liste des messages. */
    private List<Message> listeDesMessages;

    /**
     * Instanciation de suppression messages form.
     */
    public SuppressionMessagesForm()
    {
        super();
    }

    /**
     * Accesseur de l attribut liste des messages.
     * 
     * @return liste des messages
     */
    public List<Message> getListeDesMessages()
    {
        return listeDesMessages;
    }

    /**
     * Accesseur de l attribut liste des messages a supprimer.
     * 
     * @return liste des messages a supprimer
     */
    public List<Message> getListeDesMessagesASupprimer()
    {
        return listeDesMessagesASupprimer;
    }

    /**
     * Modificateur de l attribut liste des messages.
     * 
     * @param listeDesMessages
     *            le nouveau liste des messages
     */
    public void setListeDesMessages(List<Message> listeDesMessages)
    {
        this.listeDesMessages = listeDesMessages;
    }

    /**
     * Modificateur de l attribut liste des messages a supprimer.
     * 
     * @param listeDesMessagesASupprimer
     *            le nouveau liste des messages a supprimer
     */
    public void setListeDesMessagesASupprimer(
        List<Message> listeDesMessagesASupprimer)
    {
        this.listeDesMessagesASupprimer = listeDesMessagesASupprimer;
    }

}
