/*
 * Copyright : Ministere du budget, des comptes publics et de la fonction publique
 *
 */
package fr.gouv.finances.cp.srm.mvc.webflow.action;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.DataBinder;
import org.springframework.validation.Errors;
import org.springframework.webflow.action.FormAction;
import org.springframework.webflow.action.FormObjectAccessor;
import org.springframework.webflow.execution.Event;
import org.springframework.webflow.execution.RequestContext;
import org.springframework.webflow.execution.ScopeType;

import fr.gouv.finances.cp.srm.mvc.form.CreationMessageForm;
import fr.gouv.finances.cp.srm.service.MessageService;
import fr.gouv.finances.cp.srm.service.MessageTypeService;
import fr.gouv.finances.cp.srm.service.ProfilService;
import fr.gouv.finances.cp.srm.techbean.CriteresRechercheMessages;
import fr.gouv.finances.lombok.apptags.util.CheckboxSelectUtil;
import fr.gouv.finances.lombok.securite.springsecurity.UtilisateurSecurityLombokContainer;
import fr.gouv.finances.lombok.securite.techbean.HabilitationAnnuaire;
import fr.gouv.finances.lombok.securite.techbean.PersonneAnnuaire;
import fr.gouv.finances.lombok.srm.bean.Codique;
import fr.gouv.finances.lombok.srm.bean.Message;
import fr.gouv.finances.lombok.srm.bean.Profil;
import fr.gouv.finances.lombok.util.exception.RegleGestionException;
import fr.gouv.finances.lombok.util.exception.VerrouillageOptimisteException;
import fr.gouv.finances.lombok.webflow.FormStatePersister;
import fr.gouv.finances.lombok.webflow.MementoFormStatePersister;

/**
 * 
 /** Classe de creation de messages et
 * modification de messages + desactivation de profils 3 xml associes
 * creationmessage-flow ; desactivationprofils-flow ; modificationmessage-flow
 * creationmessageavecrestrictionsprofilsetcodiques-flow.xml
 * 
 * @author amleplatinec
 */
public class CreationMessageFormAction extends FormAction
{

    /** Initialisation de la journalisation. */
    protected static final Logger LOGGER = LoggerFactory.getLogger(CreationMessageFormAction.class);

    /** TYPE_DE_MESSAGE - String,. */
    public static final String TYPE_DE_MESSAGE = "typemessage";

    /** MESSAGE_SELECTIONNE_SAV - String,. */
    public static final String MESSAGE_SELECTIONNE_SAV = "messageselectionne";

    /** EXPLOITANT - String,. */
    private static String exploitant = "exploitant";

    /** compteur - int,. */
    int compteur = 0;

    /** messageserviceso - MessageService,. */
    private MessageService messageserviceso;

    /** messagetypeserviceso - MessageTypeService,. */
    private MessageTypeService messagetypeserviceso;

    private ProfilService profilserviceso;

    /**
     * Constructeur de la classe CreationMessageFormAction.java
     */
    public CreationMessageFormAction()
    {
        super();
        // Auto-generated constructor stub
    }

    /**
     * Constructeur de la classe CreationMessageFormAction.java
     * 
     * @param arg0
     *            argument
     */
    public CreationMessageFormAction(Class arg0)
    {
        super(arg0);
        // Auto-generated constructor stub
    }

    /**
     * creation d'un message sans profil.
     * 
     * @param context
     *            lecontexte
     * @return unevenement
     * @throws ParseException
     *             uneexception
     */

    public Event executerCreerMessage(RequestContext context)
        throws ParseException
    {
        Event unEvent;
        CreationMessageForm creationMessageForm = (CreationMessageForm) context
            .getFlowScope().getRequired(getFormObjectName(),
                CreationMessageForm.class);
        Message message = creationMessageForm.getMessage();

        message.setDateCreationMessage(Calendar.getInstance().getTime());
        // rajout amlp 12/03/07 . affichage de l uid createur du message
        // lors du
        // resultat de recherche
        PersonneAnnuaire personne = UtilisateurSecurityLombokContainer
            .getPersonneAnnuaire();

        message.setUIDCreateurMessage(personne.getUid());
        String str = creationMessageForm.getSavemessagetype();

        try
        {
            // pour pouvoir str en attribut final dans le service
            if (str == null)
            {
                str = "nonservi";
            }

            messageserviceso.creerUnMessageSurAppli(message, str);

            unEvent = success();
        }
        catch (RegleGestionException rge)
        {
            LOGGER.debug("Exception détectée : {}", rge);
            Errors errors = new FormObjectAccessor(context).getFormErrors(
                getFormObjectName(), ScopeType.REQUEST);

            errors.reject(null, rge.getMessage());
            unEvent = result("incorrectdata");
        }
        catch (VerrouillageOptimisteException voe)
        {
            LOGGER.debug("Exception détectée : {}", voe);
            Errors errors = new FormObjectAccessor(context).getFormErrors(
                getFormObjectName(), ScopeType.REQUEST);

            errors.reject(null, voe.getMessage());
            unEvent = error();
        }

        return unEvent;
    }

    /**
     * creation d'un message avec possiblite de choix de profils pour que les
     * personnes ayant ces profils aient le message et de codique (3 saisies
     * possibles) pour que les personnes ayany ce codique aient le message.
     * 
     * @param context
     *            lecontexte
     * @return unevenement
     */

    public Event executerCreerMessageAvecRestrictions(RequestContext context)
    {
        Event unEvent;
        CreationMessageForm creationMessageForm = (CreationMessageForm) context
            .getFlowScope().getRequired(getFormObjectName(),
                CreationMessageForm.class);
        Message message = creationMessageForm.getMessage();

        // str sert pour creer ou supprimer les messages types quand on cree
        // ou
        // modifie message
        String str = creationMessageForm.getSavemessagetype();

        List listProfilsConcernesParMessage = new ArrayList();

        listProfilsConcernesParMessage = (List) context.getRequestScope().get(
            "listProfilsSel");

        try
        {
            if (str == null)
            {
                str = "nonservi";
            }

            message.setDateCreationMessage(Calendar.getInstance().getTime());

            if (message.getTypeMessage() == null)
            {
                message.setTypeMessage("1");
            }

            // rajout amlp 12/03/07 . affichage de l uid createur du
            // message
            // lors du resultat de recherche
            PersonneAnnuaire personne = UtilisateurSecurityLombokContainer
                .getPersonneAnnuaire();
            message.setUIDCreateurMessage(personne.getUid());
            Set lesCodiquesConcernesParMessage = (Set) context
                .getRequestScope().get("lesCodiquesConcernesParMessage");

            if ((listProfilsConcernesParMessage != null)
                && (listProfilsConcernesParMessage.size() > 0))
            {
                // creation d'un message avec profils et codiques
                // destinataires
                messageserviceso.creerUnMessageAvecRestrictionsProfilsCodiques(
                    message, 
                    listProfilsConcernesParMessage,
                    lesCodiquesConcernesParMessage, str);
            }
            else
            {
                // creation d'un message avec codiques destinataires
                messageserviceso.creerUnMessageAvecRestrictionsCodiques(
                    message, 
                    lesCodiquesConcernesParMessage, str);
            }
            unEvent = success();

        }
        catch (RegleGestionException rge)
        {
            LOGGER.debug("Exception détectée : {}", rge);
            Errors errors = new FormObjectAccessor(context).getFormErrors(
                getFormObjectName(), ScopeType.REQUEST);
            errors.reject(null, rge.getMessage());
            unEvent = result("incorrectdata");
        }
        catch (VerrouillageOptimisteException voe)
        {
            LOGGER.debug("Exception détectée : {}", voe);
            Errors errors = new FormObjectAccessor(context).getFormErrors(
                getFormObjectName(), ScopeType.REQUEST);
            errors.reject(null, voe.getMessage());
            unEvent = error();
        }

        return unEvent;
    }

    /**
     * creation du message avec profils desactives.
     * 
     * @param context
     *            lecontexte
     * @return unevenement
     */

    public Event executerDesactiverProfils(RequestContext context)
    {
        Event unEvent;
        CreationMessageForm creationMessageForm = (CreationMessageForm) context
            .getFlowScope().getRequired(getFormObjectName(),
                CreationMessageForm.class);
        Message message = creationMessageForm.getMessage();

        // str sert pour creer ou supprimer les messages types quand on cree
        // ou
        // modifie message
        String str = creationMessageForm.getSavemessagetype();

        List listProfilsDesactives = (ArrayList) context.getRequestScope().get(
            "listProfilsSel");
        try
        {
            if (str == null)
            {
                str = "nonservi";
            }

            message.setDateCreationMessage(Calendar.getInstance().getTime());

            /*
             * 14/03/07 est impossible le libelle a ete controle avant if (message.getLibelleMessage().compareTo("") ==
             * 0) { message.setLibelleMessage("non servi"); }
             */

            if (message.getTypeMessage() == null)
            {
                message.setTypeMessage("1");
            }

            // rajout amlp 12/03/07 . affichage de l uid createur du
            // message
            // lors du resultat de recherche
            PersonneAnnuaire personne = UtilisateurSecurityLombokContainer
                .getPersonneAnnuaire();
            message.setUIDCreateurMessage(personne.getUid());
            // auparavant on regarder s il existe des messages sur meme
            // appli et
            // meme profil
            // et les supprimer

            List listMessageASupprimer = messageserviceso
                .rechercherMessagesAvecProfilsDejaDesactives(
                    listProfilsDesactives);
            messageserviceso.supprimerListeMessages(listMessageASupprimer);

            // apres on cree un nouveau message
            messageserviceso.creerUnMessageDesactiveProfil(message,
                listProfilsDesactives, str);

            unEvent = success();

        }
        catch (RegleGestionException rge)
        {
            LOGGER.debug("Exception détectée : {}", rge);
            Errors errors = new FormObjectAccessor(context).getFormErrors(
                getFormObjectName(), ScopeType.REQUEST);
            errors.reject(null, rge.getMessage());
            unEvent = result("incorrectdata");
        }
        catch (VerrouillageOptimisteException voe)
        {
            LOGGER.debug("Exception détectée : {}", voe);
            Errors errors = new FormObjectAccessor(context).getFormErrors(
                getFormObjectName(), ScopeType.REQUEST);
            errors.reject(null, voe.getMessage());
            unEvent = error();
        }

        return unEvent;
    }

    /**
     * modification du message.
     * 
     * @param context
     *            lecontexte
     * @return unevenement
     */

    public Event executerModifierMessage(RequestContext context)
    {
        Event unEvent;
        CreationMessageForm creationMessageForm = (CreationMessageForm) context
            .getFlowScope().getRequired(getFormObjectName(),
                CreationMessageForm.class);

        Message message = creationMessageForm.getMessage();
        // rajout amlp 12/03/07 . affichage de l uid createur ( ou
        // modificateur
        // ) du message lors du resultat de recherche
        PersonneAnnuaire personne = UtilisateurSecurityLombokContainer
            .getPersonneAnnuaire();
        message.setUIDCreateurMessage(personne.getUid());

        try
        {
            messageserviceso.creerUnMessageSurAppli(message, "null");
            unEvent = success();
        }
        catch (RegleGestionException rge)
        {
            LOGGER.debug("Exception détectée : {}", rge);
            Errors errors = new FormObjectAccessor(context).getFormErrors(
                getFormObjectName(), ScopeType.REQUEST);
            errors.reject(null, rge.getMessage());
            unEvent = result("incorrectdata");
        }
        catch (VerrouillageOptimisteException voe)
        {
            LOGGER.debug("Exception détectée : {}", voe);
            Errors errors = new FormObjectAccessor(context).getFormErrors(
                getFormObjectName(), ScopeType.REQUEST);
            errors.reject(null, voe.getMessage());
            unEvent = error();
        }
        return unEvent;
    }

    /**
     * Accesseur de l attribut messageserviceso.
     * 
     * @return messageserviceso
     */
    public MessageService getMessageserviceso()
    {
        return messageserviceso;
    }

    /**
     * Accesseur de l attribut messagetypeserviceso.
     * 
     * @return messagetypeserviceso
     */
    public MessageTypeService getMessagetypeserviceso()
    {
        return messagetypeserviceso;
    }

    public ProfilService getProfilserviceso()
    {
        return profilserviceso;
    }

    public void setProfilserviceso(ProfilService profilserviceso)
    {
        this.profilserviceso = profilserviceso;
    }
    
    /**
     * va completer les checkbox.
     * 
     * @param context
     *            lecontexte
     * @return unevenement
     */
    public Event initialiserCheckBox(RequestContext context)
    {
        Map savemessagetype = new HashMap();

        savemessagetype.put("1",
            "enregistrer le message en tant que message type");
        context.getFlowScope().put("savemessagetype", savemessagetype);
        return success();
    }

    /**
     * initialisation du formulaire.
     * 
     * @param context
     *            lecontexte
     * @return unevenement
     */
    public Event initialiserFormulaire(RequestContext context)
    {
        Event unEvent;
        // Lecture du formulaire
        CreationMessageForm creationMessageForm = (CreationMessageForm) context
            .getFlowScope().getRequired(getFormObjectName(),
                CreationMessageForm.class);
        // Recuperation du contribuable selectionne dans le flux parent
        Object original = CheckboxSelectUtil
            .extraitUnElementSelectionneDansLeFluxParent(context,
                "rechmess");

        if (original == null)
        {
            unEvent = error();
        }
        else

        {
            creationMessageForm.setMessage((Message) original);
            // Sauvegarde de l'etat du formulaire
            FormStatePersister mgo = new MementoFormStatePersister();

            mgo.saveState(creationMessageForm, context, this,
                MementoFormStatePersister.LEVEL1);

            unEvent = success();
        }

        return unEvent;
    }

    /**
     * pour gerer le retour sur la page ou il y la check box ; sinon elle reste
     * cochee.
     * 
     * @param context
     *            lecontexte
     * @return unevenement
     */
    public Event initialiserRetourCheckbox(RequestContext context)
    {
        CreationMessageForm creationMessageForm = (CreationMessageForm) context
            .getFlowScope().getRequired(getFormObjectName(),
                CreationMessageForm.class);
        String saveMessageType = creationMessageForm.getSavemessagetype();

        if ((saveMessageType != null) && (saveMessageType.compareTo("1") == 0))
        {
            creationMessageForm.setSavemessagetype("");
        }

        return success();
    }

    /**
     * prepare la liste des applications cas creation le chef de projet voit
     * uniquement les applications mises dans le filtre de l'habilitation sur
     * sireme l'exploitant voit toutes les applications en base.
     * 
     * @param context
     *            lecontexte
     * @return unevenement
     */
//    public Event preparerRechercherListeApplications(RequestContext context)
//    {
//        Event unEvent;

        /**
         * Attention en cas de maintenance : code similaire ds
         * RechercherMessagesFormAction*
         */
//        PersonneAnnuaire personne = UtilisateurSecurityLombokContainer
//            .getPersonneAnnuaire();
//
//        List<Application> listApplications = new ArrayList<Application>();
//
//        if (estExploitant(personne))
//        {
//            listApplications = applicationserviceso
//                .rechercherListeApplications();
            // on construit une appli fictive qui permet de selectionner
            // toutes les applications
//            if (applicationserviceso
//                .rechercherApplicationParLibelleCourt("toutes") == null)
//            {
//                Application appli = new Application();
//                appli.setLibelleCourt("toutes");
//                appli.setLibelleLong("toutes");
//                if ((listApplications != null) && (listApplications.size() > 0))
//                {
//                    listApplications.add(appli);
//                }
//                else
//                {
//                    listApplications = new ArrayList();
//                    listApplications.add(appli);
//                }
//            }
//        }
//        else
//        {
//
//            List lesAppliMetiers = applicationserviceso
//                .rechercherListApplicationsDansBase(personne);
//            if ((lesAppliMetiers != null) && (lesAppliMetiers.size() > 0))
//            {
//                Iterator itlesAppliMetiers = lesAppliMetiers.iterator();
//                while (itlesAppliMetiers.hasNext())
//                {
//                    Application applicationEnBase = (Application) itlesAppliMetiers
//                        .next();
//                    if (applicationEnBase != null)
//                    {
//                        listApplications.add(applicationEnBase);
//                    }
//                }
//            }
//        }
//        if ((listApplications != null) && (listApplications.size() > 0))
//        {
//            context.getFlowScope().put("listapplications", listApplications);
//            unEvent = success();
//        }
//        else
//        {
//            Errors errors = new FormObjectAccessor(context).getFormErrors(
//                getFormObjectName(), ScopeType.REQUEST);
//            errors.rejectValue(
//                "application.libelleCourt",
//                null,
//                "Les erreurs suivantes se sont produites :"
//                    + " soit aucun filtre  application de saisie dans l'habilitation portant"
//                    + " sur l'application SIREME (Veuillez contacter votre administrateur APTERA)"
//
//                    + ": soit l'application sur laquelle vous desirez inscrire un message n'est"
//                    + " pas enregistre dans la base de SIREME");
//            unEvent = result("error");
//        }
//        return unEvent;
//    }

    /**
     * prepare la liste des applications cas creation le chef de projet voit
     * uniquement les applications mises dans le filtre de l'habilitation sur
     * sireme.
     * 
     * @param context
     *            lecontexte
     * @return unevenement
     */
//    public Event preparerRechercherListeApplicationsMetiers(
//        RequestContext context)
//    {
//        Event unEvent;
//
//        /**
//         * Attention en cas de maintenance : code similaire ds
//         * RechercherMessagesFormAction*
//         */
//        PersonneAnnuaire personne = UtilisateurSecurityLombokContainer
//            .getPersonneAnnuaire();
//
//        List listApplications = applicationserviceso
//            .rechercherListApplicationsDansBase(personne);
//
//        if ((listApplications != null) && (listApplications.size() > 0))
//        {
//            context.getFlowScope().put("listapplications", listApplications);
//            unEvent = success();
//        }
//        else
//        {
//            Errors errors = new FormObjectAccessor(context).getFormErrors(
//                getFormObjectName(), ScopeType.REQUEST);
//
//            errors.rejectValue(
//                "application.libelleCourt",
//                null,
//                "Les erreurs suivantes se sont produites : "
//                    + "soit aucun filtre  application de saisie dans l'habilitation portant sur "
//                    + "l'application SIREME "
//                    + "(Veuillez contacter votre administrateur APTERA)"
//                    + ": soit l'application sur laquelle vous desirez inscrire "
//                    + "un message n'est pas enregistre dans la base de SIREME");
//            unEvent = result("error");
//        }
//
//        return unEvent;
//    }

    /**
     * recherche des profils associes e l application selectionnee.
     * 
     * @param context
     *            lecontexte
     * @return unevenement
     */

    public Event preparerRechercherListeProfils(RequestContext context)
    {

        List<Profil> listProfils = profilserviceso.rechercherTousLesProfils();
        context.getFlowScope().put("listProfils", listProfils);
        // Creation de la liste des id e partir de la liste des elements
        CheckboxSelectUtil
            .parametrerCheckboxes(context, "creepro")
            .utiliserRowid("hashcode")
            .utiliserListeElements("listProfils")
            .utiliserMsgAucunEltSelectionne(
                "Aucun profil n'est selectionne");
        return success();

    }

    /**
     * recuperation des profils destinataires coches (par traiterchecboxes).
     * 
     * @param context
     *            lecontexte
     * @return unevenement
     */
    public Event preparerRechercherListeProfilsDestinatairesSelectionnes(
        RequestContext context)
    {
        Event unEvent;

        List listProfilsSel = (List) CheckboxSelectUtil
            .extraitLesElementsSelectionnesDansLeFlux(context, "creepro");
        if ((listProfilsSel != null) && (listProfilsSel.size() > 0))
        {
            context.getRequestScope().put("listProfilsSel", listProfilsSel);
            CheckboxSelectUtil.enregistreRowidsPreSelectionnesDansFlowScope(
                context, listProfilsSel);
            unEvent = success();
        }
        else
        {
            unEvent = success();
        }
        return unEvent;
    }

    /**
     * recuperation des profils coches (par traiterchecboxes).
     * 
     * @param context
     *            lecontexte
     * @return unevenement
     */
    public Event preparerRechercherListeProfilsSelectionnes(
        RequestContext context)
    {
        Event unEvent;

        List listProfilsSel = (List) CheckboxSelectUtil
            .extraitLesElementsSelectionnesDansLeFlux(context, "creepro");
        if ((listProfilsSel != null) && (listProfilsSel.size() > 0))
        {
            context.getRequestScope().put("listProfilsSel", listProfilsSel);
            CheckboxSelectUtil.enregistreRowidsPreSelectionnesDansFlowScope(
                context, listProfilsSel);
            unEvent = success();
        }
        else
        {
            Errors errors = new FormObjectAccessor(context).getFormErrors(
                getFormObjectName(), ScopeType.REQUEST);
            errors.reject(null, "selection d un profil obligatoire");
            unEvent = result("preperror");
        }
        return unEvent;
    }

    /**
     * preparation de l'affichage des types de message.
     * 
     * @param context
     *            lecontexte
     * @return unevenement
     */

    public Event preparerRechercherListeTypeMessage(RequestContext context)
    {
        Map typemessage = CriteresRechercheMessages.getTypeMessage();
        context.getFlowScope().put(TYPE_DE_MESSAGE, typemessage);
        return success();
    }

    /**
     * recherche le libelle du message type associe au message.
     * 
     * @param context
     *            lecontexte
     * @return unevenement
     */
    public Event rechercherLibelleMessageTypeAssocie(RequestContext context)
    {
        CreationMessageForm creationMessageForm = (CreationMessageForm) context
            .getFlowScope().getRequired(getFormObjectName(),
                CreationMessageForm.class);
        if (creationMessageForm.getMessage().getLibelleMessage() != null
            && StringUtils.isNumeric(creationMessageForm.getMessage()
                .getLibelleMessage()))
        {
            String str = messagetypeserviceso
                .rechercherLibelleAssocieAMessageTypeId(creationMessageForm
                    .getMessage().getLibelleMessage());
            Message message = new Message();
            message.setLibelleMessage(str);
            creationMessageForm.setMessage(message);
        }
        return success();
    }

    /**
     * recherche si des profils sur la meme appli sont deje desactives.
     * 
     * @param context
     *            ---
     * @return unevenement
     */

    public Event rechercherProfilsDesactives(RequestContext context)
    {
        Event unEvent;
        CreationMessageForm creationMessageForm = (CreationMessageForm) context
            .getFlowScope().getRequired(getFormObjectName(),
                CreationMessageForm.class);

        // recuperation des profils coches pour etre desactives
        List listProfilsSel = (ArrayList) context.getRequestScope().get(
            "listProfilsSel");
        List listProfilsDesactives = messageserviceso
            .rechercherProfilsDesactives(listProfilsSel);

        if (compteur == 0)
        {
            if ((listProfilsDesactives == null)
                || (listProfilsDesactives.size() == 0))
            {
                unEvent = success();
            }
            else
            {
                // transformation du Set en Txt pour affichage
                String tesxtProfil = "";
                Iterator lesprofils = listProfilsDesactives.iterator();

                while (lesprofils.hasNext())
                {
                    Profil profil = (Profil) lesprofils.next();
                    if (profil != null)
                    {
                        tesxtProfil = tesxtProfil.concat(profil
                            .getLibelleProfil());
                    }
                }

                Errors errors = new FormObjectAccessor(context).getFormErrors(
                    getFormObjectName(), ScopeType.REQUEST);
                errors.reject(
                    null,
                    "Avertissement : les profils suivants sont deja desactives sur cette"
                        + " application .Les messages portant sur les desactivations precedentes "
                        + "seront supprimes . Profil(s) concerne(s)   :  "
                        + tesxtProfil);
                compteur = 1;
                unEvent = result("incorrectdata");
            }
        }
        else
        {

            unEvent = success();
        }
        return unEvent;
    }

    /**
     * cas ou on annule une modification encours . pour retrouver les valeurs
     * originales
     * 
     * @param context
     *            lecontexte
     * @return unevenement
     */

    public Event restaureOriginal(RequestContext context)
    {
        CreationMessageForm creationMessageForm = (CreationMessageForm) context
            .getFlowScope().getRequired(getFormObjectName(),
                CreationMessageForm.class);

        FormStatePersister mgo = new MementoFormStatePersister();

        mgo.restoreState(creationMessageForm, context, this,
            MementoFormStatePersister.LEVEL1);
        return success();
    }

    /**
     * Modificateur de l attribut messageserviceso.
     * 
     * @param messageserviceso
     *            le nouveau messageserviceso
     */
    public void setMessageserviceso(MessageService messageserviceso)
    {
        this.messageserviceso = messageserviceso;
    }

    /**
     * Modificateur de l attribut messagetypeserviceso.
     * 
     * @param messagetypeserviceso
     *            le nouveau messagetypeserviceso
     */
    public void setMessagetypeserviceso(MessageTypeService messagetypeserviceso)
    {
        this.messagetypeserviceso = messagetypeserviceso;
    }

    /**
     * gestion des cases e cocher.
     * 
     * @param context
     *            lecontexte
     * @return unevenement
     */
    public Event traiterCheckboxesEntrePages(RequestContext context)
    {
        CheckboxSelectUtil.traiterCheckboxesEntrePages(context, "creepro");
        return success();
    }

    /**
     * verifie qu'au moins un codique ou profil destinataire ont ete
     * selectionnes.
     * 
     * @param context
     *            lecontexte
     * @return unevenement
     */
    public Event validerVerifierAuMoinsUnCodiqueOuProfilDeSelectionner(
        RequestContext context)
    {
        Event unEvent;
        CreationMessageForm creationMessageForm = (CreationMessageForm) context
            .getFlowScope().getRequired(getFormObjectName(),
                CreationMessageForm.class);
        String libcodique1 = creationMessageForm.getCodique1();
        String libcodique2 = creationMessageForm.getCodique2();
        String libcodique3 = creationMessageForm.getCodique3();

        Set lesCodiquesConcernesParMessage = new HashSet();
        if ((libcodique1 != null) && (libcodique1.compareTo("") != 0))
        {
            Codique codique1 = new Codique(libcodique1);
            lesCodiquesConcernesParMessage.add(codique1);
        }
        if ((libcodique2 != null) && (libcodique2.compareTo("") != 0))
        {
            Codique codique2 = new Codique(libcodique2);
            lesCodiquesConcernesParMessage.add(codique2);
        }
        if ((libcodique3 != null) && (libcodique3.compareTo("") != 0))
        {
            Codique codique3 = new Codique(libcodique3);
            lesCodiquesConcernesParMessage.add(codique3);
        }

        context.getRequestScope().put("lesCodiquesConcernesParMessage",
            lesCodiquesConcernesParMessage);

        List listProfilsConcernesParMessage = (List) context.getRequestScope()
            .get("listProfilsSel");
        if (((lesCodiquesConcernesParMessage != null) && (lesCodiquesConcernesParMessage
            .size() > 0))
            || ((listProfilsConcernesParMessage != null) && (listProfilsConcernesParMessage
                .size() > 0)))
        {
            unEvent = success();
        }
        else
        {
            Errors errors = new FormObjectAccessor(context).getFormErrors(
                getFormObjectName(), ScopeType.REQUEST);
            errors.rejectValue("message.libelleMessage", null,
                "selection obligatoire d un codique ou d un profil");
            unEvent = result("incorrectdata");
        }
        return unEvent;
    }

    /**
     * verifie datedebut message est inferieure datefin message.
     * 
     * @param context
     *            lecontexte
     * @return unevenement
     */

    public Event validerVerifierDatesMessage(RequestContext context)
    {
        Event unEvent;
        CreationMessageForm creationMessageForm = (CreationMessageForm) context
            .getFlowScope().getRequired(getFormObjectName(),
                CreationMessageForm.class);
        Message message = creationMessageForm.getMessage();
        Date dateDebutMessage = message.getDateDebutMessage();
        Date dateFinMessage = message.getDateFinMessage();

        try
        {
            message.verifierDatesMessage(dateDebutMessage, dateFinMessage,
                new Date());
            unEvent = success();
        }
        catch (RegleGestionException rge)
        {
            LOGGER.debug("Exception détectée : {}", rge);
            Errors errors = new FormObjectAccessor(context).getFormErrors(
                getFormObjectName(), ScopeType.REQUEST);
            errors.reject("message.dateDebutMessage", null, rge.getMessage());
            unEvent = result("incorrectdata");
        }
        catch (ParseException pe)
        {
            LOGGER.debug("Exception détectée : {}", pe);
            unEvent = result("incorrectdata");
        }
        return unEvent;
    }

    /**
     * verifie que les dates d'activations des profils sont servies et correcte
     * datefin>datedebut.
     * 
     * @param context
     *            lecontexte
     * @return unevenement
     */

    public Event validerVerifierDatesMessageProfils(RequestContext context)
    {
        Event unEvent;
        CreationMessageForm creationMessageForm = (CreationMessageForm) context
            .getFlowScope().getRequired(getFormObjectName(),
                CreationMessageForm.class);
        Message message = creationMessageForm.getMessage();

        try
        {
            message.verifierDatesMessageProfil(
                message.getDateDebutInactiviteProfil(),
                message.getDateFinInactiviteProfil(), new Date());
            unEvent = success();
        }
        catch (RegleGestionException rge)
        {
            LOGGER.debug("Exception détectée : {}", rge);
            Errors errors = new FormObjectAccessor(context).getFormErrors(
                getFormObjectName(), ScopeType.REQUEST);
            errors.reject(null, rge.getMessage());
            unEvent = result("incorrectdata");
        }
        catch (ParseException pe)
        {
            LOGGER.debug("Exception détectée : {}", pe);
            unEvent = result("incorrectdata");
        }
        return unEvent;
    }

    /**
     * verifie si un message avec les memes caracteristiques existe deje en base
     * (hors message actuellement modifie)(caracteristiques = libelle +
     * application).
     * 
     * @param context
     *            lecontexte
     * @return unevenement
     */

    public Event validerVerifierExistenceMessage(RequestContext context)
    {
        Event unEvent;
        CreationMessageForm creationMessageForm = (CreationMessageForm) context
            .getFlowScope().getRequired(getFormObjectName(),
                CreationMessageForm.class);
        Message message = creationMessageForm.getMessage();

        if (messageserviceso.rechercherMessage(message) == null)
        {
            unEvent = success();
        }
        else
        {
            Errors errors = new FormObjectAccessor(context).getFormErrors(
                getFormObjectName(), ScopeType.REQUEST);
            errors.rejectValue(
                "message.libelleMessage",
                null,
                "un message avec les memes caracteristiques ( application , libelle ) existe dejà");
            unEvent = result("incorrectdata");
        }
        return unEvent;
    }

    /**
     * verifie si un message avec lesmemes caracteristiques existe deje en base
     * (hors message actuellment modifie).
     * 
     * @param context
     *            lecontexte
     * @return unevenement
     */

    public Event validerVerifierExistenceMessageEnModif(RequestContext context)
    {
        Event unEvent;
        CreationMessageForm creationMessageForm = (CreationMessageForm) context
            .getFlowScope().getRequired(getFormObjectName(),
                CreationMessageForm.class);
        Message message = creationMessageForm.getMessage();

        if (messageserviceso.rechercherMessagePourModif(message) == null)
        {
            unEvent = success();
        }
        else
        {
            Errors errors = new FormObjectAccessor(context).getFormErrors(
                getFormObjectName(), ScopeType.REQUEST);
            errors.rejectValue(
                "message.libelleMessage",
                null,
                "un message avec les memes caracteristiques ( application , libelle ) existe dejà");
            unEvent = result("incorrectdata");
        }
        return unEvent;
    }

    /**
     * verifie message type existe.
     * 
     * @param context
     *            lecontexte
     * @return unevenement
     */

    public Event validerVerifierExistenceMessageType(RequestContext context)
    {
        Event unEvent;
        CreationMessageForm creationMessageForm = (CreationMessageForm) context
            .getFlowScope().getRequired(getFormObjectName(),
                CreationMessageForm.class);
        Message message = creationMessageForm.getMessage();
        String str = creationMessageForm.getSavemessagetype();

        // cas ou on a coche la case "enregistrer en tant que message type"
        if ((str != null) && (str.compareTo("1") == 0))
        {
            String libelleMessageType = message.getLibelleMessage();
            try
            {
                messagetypeserviceso
                    .verifierExistenceMessageType(libelleMessageType);
                unEvent = success();
            }
            catch (RegleGestionException rge)
            {
                LOGGER.debug("Exception détectée : {}", rge);
                Errors errors = new FormObjectAccessor(context).getFormErrors(
                    getFormObjectName(), ScopeType.REQUEST);
                errors.reject(null, rge.getMessage());
                unEvent = result("incorrectdata");
            }
        }
        else
        {
            unEvent = success();
        }

        return unEvent;
    }

    /**
     * 
     * (methode de remplacement)
     * {@inheritDoc}
     * @see org.springframework.webflow.action.FormAction#initBinder(org.springframework.webflow.execution.RequestContext, org.springframework.validation.DataBinder)
     */
    @Override
    protected void initBinder(RequestContext request, DataBinder binder)
    {
        super.initBinder(request, binder);
        binder.registerCustomEditor(Long.class, new CustomNumberEditor(
            Long.class, true));
        // Formatage des dates stricts : les dates qui ne sont pas dans le
        // calendrier sont rejettee
        // Le binder par defaut fournit par Spring decale la date dans le
        // calendrier
        Locale localefr = Locale.FRANCE;
        SimpleDateFormat dateformat1 = new SimpleDateFormat("dd/MM/yyyy HH:mm",
            localefr);

        dateformat1.setLenient(false);
        binder.registerCustomEditor(Date.class, new CustomDateEditor(
            dateformat1, true));
    }

    /**
     * **verifie si une personne est exploitant ou non.
     * 
     * @param personne
     *            ---
     * @return unbboleen
     */

    private boolean estExploitant(PersonneAnnuaire personne)
    {
        boolean estExploitant = false;
        Set listHab = personne.getListeHabilitations();
        Iterator leshab = listHab.iterator();

        while (leshab.hasNext())
        {
            if (((HabilitationAnnuaire) leshab.next()).getNomProfil()
                .compareToIgnoreCase(exploitant) == 0)
            {
                estExploitant = true;
                break;
            }
        }
        return estExploitant;
    }

}
