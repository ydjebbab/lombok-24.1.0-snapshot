/*
 * Copyright (c) 2017 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.cp.srm.techbean;

import java.util.Date;
import java.util.Map;
import java.util.TreeMap;

import fr.gouv.finances.lombok.util.base.BaseTechBean;

/**
 * Classe qui va comprendre tous les criteres de recherche d'un message. 
 * 
 * @author amleplatinec
 * @version $Revision: 1.18 $ Date: 22 dec. 2009
 */
public class CriteresRechercheMessages extends BaseTechBean
{
    
    private static final long serialVersionUID = 1L;

    /** Constant : TIDTYPEMESSAGE. */
    private static final int[] TIDTYPEMESSAGE = {1, 2};

    /** Constant : TNAMETYPEMESSAGE. */
    private static final String[] TNAMETYPEMESSAGE = {"message d information",
            "message bloquant"};

    /** type messages. */
    private String typeMessages;

    /** libelle message. */
    private String libelleMessage;

    /** date debut message. */
    private Date dateDebutMessage;

    /** date fin message. */
    private Date dateFinMessage;

    // rajout amlp 12/03/07 on veut faire une recherche avec uid personne ayant
    // cree le message

    /** date heure courante. */
    private Date dateHeureCourante = new Date();

    /** UID createur message. */
    private String uidCreateurMessage;

    /** rechercher uniquement les messages actifs a la date heure courante. */
    private boolean rechOnlyMessagesActifsNow = false;

    /**
     * rechercher uniquement les messages avec profils a desactiver a la date heure courante.
     */
    private boolean rechOnlyMessProfilsADesactNow = false;

    /**
     * rechercher uniquement les messages avec profils a desactiver. rechOnlyMessAvecProfilsADesacti - boolean, aaa
     */
    private boolean rechOnlyMessAvecProfilsADesact = false;

    /**
     * Instanciation de criteres recherche messages.
     */
    public CriteresRechercheMessages()
    {
        super();
    }

    /**
     * Accesseur de l attribut type message.
     *
     * @return type message
     */
    public static final Map<Integer, String> getTypeMessage()
    {
        Map<Integer, String> uneHashMap = new TreeMap<>();

        for (int inti = 0; inti < TIDTYPEMESSAGE.length; inti++)
        {
            uneHashMap.put(Integer.valueOf(TIDTYPEMESSAGE[inti]),
                TNAMETYPEMESSAGE[inti]);
        }
        return uneHashMap;
    }

    /**
     * Gets the date debut message.
     *
     * @return the date debut message
     */
    public Date getDateDebutMessage()
    {
        return dateDebutMessage;
    }

    /**
     * Gets the date fin message.
     *
     * @return the date fin message
     */
    public Date getDateFinMessage()
    {
        return dateFinMessage;
    }

    /**
     * Gets the date heure courante.
     *
     * @return the date heure courante
     */
    public Date getDateHeureCourante()
    {
        return dateHeureCourante;
    }

    /**
     * Gets the libelle message.
     *
     * @return the libelle message
     */
    public String getLibelleMessage()
    {
        return libelleMessage;
    }

    /**
     * faire une recuperation des messages plus jolie
     * 
     * @param tidtypemessage --
     * @return the libelle type message
     */
    public String getLibelleTypeMessage(String tidtypemessage)
    {
        String retour;
        if (tidtypemessage != null && tidtypemessage.compareTo("1") == 0)
        {
            retour = "message d information";
        }
        else
        {
            retour = "message bloquant";
        }
        return retour;
    }

    /**
     * Gets the type messages.
     *
     * @return the type messages
     */
    public String getTypeMessages()
    {
        return typeMessages;
    }

    /**
     * Gets the uID createur message.
     *
     * @return the uID createur message
     */
    public String getUidCreateurMessage()
    {
        return uidCreateurMessage;
    }

    /**
     * Checks if is rechercher uniquement les messages actifs a la date heure courante.
     *
     * @return the rechercher uniquement les messages actifs a la date heure courante
     */
    public boolean isRechOnlyMessagesActifsNow()
    {
        return rechOnlyMessagesActifsNow;
    }

    /**
     * Checks if is rechercher uniquement les messages avec profils a desactiver.
     *
     * @return the rechercher uniquement les messages avec profils a desactiver
     */
    public boolean isRechOnlyMessAvecProfilsADesact()
    {
        return rechOnlyMessAvecProfilsADesact;
    }

    /**
     * Checks if is rechercher uniquement les messages avec profils a desactiver a la date heure courante.
     *
     * @return the rechercher uniquement les messages avec profils a desactiver a la date heure courante
     */
    public boolean isRechOnlyMessProfilsADesactNow()
    {
        return rechOnlyMessProfilsADesactNow;
    }

    /**
     * Sets the date debut message.
     *
     * @param dateDebutMessage the new date debut message
     */
    public void setDateDebutMessage(Date dateDebutMessage)
    {
        this.dateDebutMessage = dateDebutMessage;
    }

    /**
     * Sets the date fin message.
     *
     * @param dateFinMessage the new date fin message
     */
    public void setDateFinMessage(Date dateFinMessage)
    {
        this.dateFinMessage = dateFinMessage;
    }

    /**
     * Sets the date heure courante.
     *
     * @param dateDuJOur the new date heure courante
     */
    public void setDateHeureCourante(Date dateDuJOur)
    {
        this.dateHeureCourante = dateDuJOur;
    }

    /**
     * Sets the libelle message.
     *
     * @param libelleMessage the new libelle message
     */
    public void setLibelleMessage(String libelleMessage)
    {
        this.libelleMessage = libelleMessage;
    }

    /**
     * Sets the rechercher uniquement les messages actifs a la date heure courante.
     *
     * @param rechOnlyMessActifsNow the new rechercher uniquement les messages actifs a la date heure courante
     */
    public void setRechOnlyMessagesActifsNow(boolean rechOnlyMessActifsNow)
    {
        this.rechOnlyMessagesActifsNow = rechOnlyMessActifsNow;
    }

    /**
     * Sets the rechercher uniquement les messages avec profils a desactiver.
     *
     * @param rechOnlyMessAvecProfilsADesact the new rechercher uniquement les messages avec profils a desactiver
     */
    public void setRechOnlyMessAvecProfilsADesact(
        boolean rechOnlyMessAvecProfilsADesact)
    {
        this.rechOnlyMessAvecProfilsADesact = rechOnlyMessAvecProfilsADesact;
    }

    /**
     * Sets the rechercher uniquement les messages avec profils a desactiver a la date heure courante.
     *
     * @param rechOnlyMessProfilsADesactNow the new rechercher uniquement les messages avec profils a desactiver a la
     *        date heure courante
     */
    public void setRechOnlyMessProfilsADesactNow(
        boolean rechOnlyMessProfilsADesactNow)
    {
        this.rechOnlyMessProfilsADesactNow = rechOnlyMessProfilsADesactNow;
    }

    /**
     * Sets the type messages.
     *
     * @param typeMessages the new type messages
     */
    public void setTypeMessages(String typeMessages)
    {
        this.typeMessages = typeMessages;
    }

    /**
     * Sets the uID createur message.
     *
     * @param createurMessage the new uID createur message
     */
    public void setUidCreateurMessage(String createurMessage)
    {
        uidCreateurMessage = createurMessage;
    }

}
