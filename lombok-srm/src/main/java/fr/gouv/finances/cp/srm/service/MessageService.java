/*
 * Copyright (c) 2017 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.cp.srm.service;

import java.util.List;
import java.util.Set;

import fr.gouv.finances.cp.srm.techbean.CriteresRechercheMessages;
import fr.gouv.finances.lombok.srm.bean.Codique;
import fr.gouv.finances.lombok.srm.bean.Message;
import fr.gouv.finances.lombok.srm.bean.Profil;

/**
 * Classe MessageService.java.
 * 
 * @author amleplatinec
 * @version $Revision: 1.12 $
 */
public interface MessageService
{

    /**
     * cree un message avec des restrictions de codiques.
     *
     * @param message --
     * @param lesCodiquesConcernesParMessage --
     * @param actionMessageType --
     */
    void creerUnMessageAvecRestrictionsCodiques(Message message,
        Set<Message> lesCodiquesConcernesParMessage,
        String actionMessageType);

    /**
     * cree un message avec des restrictions de codiques et/ou profils.
     *
     * @param message --
     * @param listProfilsConcernParMessage --
     * @param lesCodiquesConcernParMessage --
     * @param actionMessageType --
     */
    void creerUnMessageAvecRestrictionsProfilsCodiques(Message message,
        List<Profil> listProfilsConcernParMessage,
        Set<Codique> lesCodiquesConcernParMessage, String actionMessageType);

    /**
     * cree un message sur une application donnee qui desactive une liste de profils.
     *
     * @param message --
     * @param listProfils --
     * @param actionMessageType --
     */
    void creerUnMessageDesactiveProfil(Message message,
        List<Message> listProfils, String actionMessageType);

    /**
     * cree un message sur une application donnee.
     *
     * @param message --
     * @param actionMessageType --
     */
    void creerUnMessageSurAppli(Message message, String actionMessageType);

    /**
     * recherche un message e partir du message et de l'aplication.
     *
     * @param message --
     * @return the message
     */
    Message rechercherMessage(Message message);

    /**
     * recherche existence d'un message sur application en vue de transction modification.
     *
     * @param message --
     * @return the message
     */
    Message rechercherMessagePourModif(Message message);

    /**
     * recherche les messages associes au critere de recherche.
     * 
     * @param crit
     *            --
     * @return the list< message>
     */
    List<Message> rechercherMessages(CriteresRechercheMessages crit);

    /**
     * ramene les MESSAGES qui contiennnet deje les profils desactives.
     *
     * @param lesProfils --
     * @return the list< message>
     */
    List<Message> rechercherMessagesAvecProfilsDejaDesactives(List<Profil> lesProfils);

    /**
     * retorne les messages perimes.
     * 
     * @return the list< message>
     */
    List<Message> rechercherMessagesPerimes();

    /**
     * recherche les profils desactives sur des messages associes d'une application (avant de desactiver un profil on
     * verifie si il est deje desactive ou non).
     *
     * @param lesProfils --
     * @return the list< profil>
     */
    List<Profil> rechercherProfilsDesactives(List<Profil> lesProfils);

    /**
     * recherche tous les messages.
     * 
     * @return the list< message>
     */
    List<Message> rechercherTousLesMessages();

    /**
     * supprime une liste de messages.
     * 
     * @param listMessages
     *            --
     */
    void supprimerListeMessages(List<Message> listMessages);

    /**
     * supprime un message.
     * 
     * @param message
     *            --
     */
    void supprimerUnMessage(Message message);

    /**
     * methode Verifier si profil reference par des messages : DOCUMENTEZ_MOI.
     *
     * @param profil DOCUMENTEZ_MOI
     * @return true, si c'est vrai
     */
    boolean verifierSiProfilReferenceParDesMessages(Profil profil);

}