/*
 * Copyright (c) 2020 DGFiP - Tous droits réservés
 * 
 */
package fr.gouv.finances.cp.srm.techbean;

import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.gouv.finances.lombok.test.pojo.AbstractCorePojoTest;

/**
 * Tests unitaires du POJO CriteresRechercheMessages.
 * 
 * @author Christophe Breheret-Girardin
 */
public class CriteresRechercheMessagesTest extends AbstractCorePojoTest<CriteresRechercheMessages>
{

    protected static final Logger LOGGER = LoggerFactory.getLogger(CriteresRechercheMessagesTest.class);
    
    public CriteresRechercheMessagesTest()
    {
        super();
    }

    /**
     * Test de la méthode getLibelleTypeMessage(String)
     */
    @Test
    public void testGetLibelleTypeMessage()
    {
        LOGGER.debug("Methode testGetLibelleTypeMessage");
        CriteresRechercheMessages criteresRechercheMessages = new CriteresRechercheMessages();
        Assert.assertEquals("message bloquant"
            , criteresRechercheMessages.getLibelleTypeMessage(null));
        Assert.assertEquals("message bloquant"
            , criteresRechercheMessages.getLibelleTypeMessage(""));
        Assert.assertEquals("message d information"
            , criteresRechercheMessages.getLibelleTypeMessage("1"));
        Assert.assertEquals("message bloquant"
            , criteresRechercheMessages.getLibelleTypeMessage("2"));
    }

}
