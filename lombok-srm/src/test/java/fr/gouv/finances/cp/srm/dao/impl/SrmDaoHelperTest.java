/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 *
 */
package fr.gouv.finances.cp.srm.dao.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.Test;

import fr.gouv.finances.cp.srm.dao.SrmDaoHelper;
import fr.gouv.finances.lombok.test.controle.VerificationExecution;
import fr.gouv.finances.lombok.util.exception.IngerableException;

/**
 * Tests unitaires de l'utilitaire SrmDaoHelper.
 * @see fr.gouv.finances.cp.srm.dao.impl.SrmDaoHelper
 *
 * @author Christophe Breheret-Girardin
 */
public class SrmDaoHelperTest
{
    /**
     * Test de la méthode fr.gouv.finances.cp.srm.dao.impl.SrmDaoHelper#getIdentifiant(java.lang.String).
     */
    @Test
    public void getIdentifiant()
    {
        assertNull(SrmDaoHelper.getIdentifiant(null));
        assertNull(SrmDaoHelper.getIdentifiant(""));
        VerificationExecution.verifierException(
            IngerableException.class, () -> SrmDaoHelper.getIdentifiant("a"));
        assertEquals(2l, SrmDaoHelper.getIdentifiant("2").longValue());
    }
}