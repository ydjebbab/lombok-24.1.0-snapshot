/*
 * Copyright (c) 2016 DGFiP - Tous droits réservés
 * 
 */
package fr.gouv.finances.cp.srm.mvc.form;

import fr.gouv.finances.lombok.test.pojo.AbstractCorePojoTest;

/**
 * Tests unitaires du POJO RechercheMessagesForm.
 *
 * @param RechercheMessagesForm formulaire lié à la recherche de message sirème
 *
 * @author Christophe Breheret-Girardin
 */
public class RechercheMessagesFormTest extends AbstractCorePojoTest<RechercheMessagesForm>
{   
    /**
     * Constructeur.
     */
    public RechercheMessagesFormTest()
    {
        super();
    }

}
