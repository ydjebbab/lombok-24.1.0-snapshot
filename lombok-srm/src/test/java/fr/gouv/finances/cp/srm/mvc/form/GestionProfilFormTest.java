/*
 * Copyright (c) 2016 DGFiP - Tous droits réservés
 * 
 */
package fr.gouv.finances.cp.srm.mvc.form;

import fr.gouv.finances.lombok.test.pojo.AbstractCorePojoTest;

/**
 * Tests unitaires du POJO GestionProfilForm.
 *
 * @param GestionProfilForm formulaire lié à la gestion des profils
 *
 * @author Christophe Breheret-Girardin
 */
public class GestionProfilFormTest extends AbstractCorePojoTest<GestionProfilForm>
{   
    /**
     * Constructeur.
     */
    public GestionProfilFormTest()
    {
        super();
    }

}
