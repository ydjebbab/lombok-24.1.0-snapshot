/*
 * Copyright (c) 2016 DGFiP - Tous droits réservés
 * 
 */
package fr.gouv.finances.cp.srm.mvc.form;

import fr.gouv.finances.lombok.test.pojo.AbstractCorePojoTest;
import fr.gouv.finances.lombok.webflow.FormMemento;
import fr.gouv.finances.lombok.webflow.Memento;

/**
 * Tests unitaires du POJO CreationMessageForm.
 *
 * @param CreationMessageForm formulaire à la création de message sirème
 *
 * @author Christophe Breheret-Girardin 
 */
public class CreationMessageFormTest extends AbstractCorePojoTest<CreationMessageForm>
{   
    /**
     * Constructeur.
     */
    public CreationMessageFormTest()
    {
        super();

        // Ajout d'une valeur par défaut pour les paramètres de type Memento
        addPrefabValues(Memento.class, new FormMemento(new CreationMessageForm()));
    }

}
