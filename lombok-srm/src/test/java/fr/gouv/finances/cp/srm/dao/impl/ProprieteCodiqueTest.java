/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 */
package fr.gouv.finances.cp.srm.dao.impl;
import fr.gouv.finances.cp.srm.dao.ProprieteCodique;
import fr.gouv.finances.lombok.test.pojo.AbstractCorePojoTest;

/**
 * Tests unitaires de l'énumération ProprieteCodique.
 * 
 * @see fr.gouv.finances.cp.srm.dao.ProprieteCodique
 * @author fernandescn Date: 15 oct. 2018
 */
public class ProprieteCodiqueTest extends AbstractCorePojoTest<ProprieteCodique>
{

    /**
     * Constructeur.
     */
    public ProprieteCodiqueTest()
    {
        super();
    }
}