/*
 * Copyright (c) 2016 DGFiP - Tous droits réservés
 * 
 */
package fr.gouv.finances.cp.srm.mvc.form;

import fr.gouv.finances.lombok.test.pojo.AbstractCorePojoTest;

/**
 * Tests unitaires du POJO CreationMessageTypeForm.
 *
 * @param CreationMessageTypeForm formulaire lié à la création de type de message sirème
 *
 * @author Christophe Breheret-Girardin
 */
public class CreationMessageTypeFormTest extends AbstractCorePojoTest<CreationMessageTypeForm>
{   
    /**
     * Constructeur.
     */
    public CreationMessageTypeFormTest()
    {
        super();
    }

}
