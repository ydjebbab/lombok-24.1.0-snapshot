/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 */
package fr.gouv.finances.cp.srm.dao.impl;

import fr.gouv.finances.cp.srm.dao.ProprieteMessage;
import fr.gouv.finances.lombok.test.pojo.AbstractCorePojoTest;

/**
 * Tests unitaires de l'énumération ProprieteMessage.
 * @see fr.gouv.finances.cp.srm.dao.ProprieteMessage
 *
 * @author Christophe Breheret-Girardin
 */
public class ProprieteMessageTest extends AbstractCorePojoTest<ProprieteMessage>
{ 

    /**
     * Constructeur.
     */
    public ProprieteMessageTest()
    {
        super();
    }
}