/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 */
package fr.gouv.finances.cp.srm.dao.impl;

import fr.gouv.finances.cp.srm.dao.ProprieteProfil;
import fr.gouv.finances.lombok.test.pojo.AbstractCorePojoTest;

/**
 * Tests unitaires de l'énumération ProprieteProfil.
 * 
 * @see fr.gouv.finances.cp.srm.dao.ProprieteProfil
 * @author fernandescn Date: 15 oct. 2018
 */
public class ProprieteProfilTest extends AbstractCorePojoTest<ProprieteProfil>
{ 

    /**
     * Constructeur.
     */
    public ProprieteProfilTest()
    {
        super();
    }
}