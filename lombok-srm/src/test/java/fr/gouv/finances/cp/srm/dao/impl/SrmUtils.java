package fr.gouv.finances.cp.srm.dao.impl;

import fr.gouv.finances.lombok.srm.bean.Codique;
import fr.gouv.finances.lombok.srm.bean.Profil;

/**
 * Utilitaire des tests du composant SRM
 *
 * @author Christophe Breheret-Girardin
 */
public class SrmUtils
{
    /**
     * Constructeur privé.
     */
    private SrmUtils()
    {
        super();
    }

    /**
     * Méthode permettant de générer un profil.
     *
     * @return le profil généré
     */
    public static Profil getProfil(int indice)
    {
        Profil profil = new Profil();
        profil.setCodeProfil(String.valueOf(indice));
        profil.setCodeProfilConcatCodeAppli("code appli " + indice);
        profil.setLibelleProfil("libellé profil");
        return profil;
    }

    /**
     * Méthode permettant de générer un codique.
     *
     * @return le codique généré
     */
    public static Codique getCodique(int indice)
    {
        Codique codique = new Codique();
        codique.setLibelleCodique(String.valueOf(indice));
        return codique;
    }
}
