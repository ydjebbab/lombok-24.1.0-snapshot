/*
 * Copyright (c) 2016 DGFiP - Tous droits réservés
 * 
 */
package fr.gouv.finances.cp.srm.mvc.form;

import fr.gouv.finances.lombok.test.pojo.AbstractCorePojoTest;

/**
 * Tests unitaires du POJO SuppressionMessagesForm.
 *
 * @param SuppressionMessagesForm formulaire lié à la suppression de message sirème
 *
 * @author Christophe Breheret-Girardin
 */
public class SuppressionMessagesFormTest extends AbstractCorePojoTest<SuppressionMessagesForm>
{   
    /**
     * Constructeur.
     */
    public SuppressionMessagesFormTest()
    {
        super();
    }

}
