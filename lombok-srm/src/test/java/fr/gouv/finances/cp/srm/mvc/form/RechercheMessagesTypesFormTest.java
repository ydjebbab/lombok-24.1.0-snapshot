/*
 * Copyright (c) 2016 DGFiP - Tous droits réservés
 * 
 */
package fr.gouv.finances.cp.srm.mvc.form;

import fr.gouv.finances.lombok.test.pojo.AbstractCorePojoTest;

/**
 * Tests unitaires du POJO RechercheMessagesTypesForm.
 *
 * @param RechercheMessagesTypesForm formulaire lié à la recherche de type de message sirème
 *
 * @author Christophe Breheret-Girardin
 */
public class RechercheMessagesTypesFormTest extends AbstractCorePojoTest<RechercheMessagesTypesForm>
{   
    /**
     * Constructeur.
     */
    public RechercheMessagesTypesFormTest()
    {
        super();
    }

}
