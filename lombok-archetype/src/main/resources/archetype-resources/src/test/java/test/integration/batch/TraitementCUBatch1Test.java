package ${package}.test.integration.batch;

import static org.mockito.Mockito.verify;
import org.junit.Test;
import fr.gouv.finances.lombok.util.base.LombokBatchTestSansSpring;

public class TraitementCUBatch1Test extends LombokBatchTestSansSpring {

	//@Test		
	public void testtraiterBatch(){
        testBatch(new String[] {"traitementcubatch1impl"});
        // vérification que le test fini sans erreur
        verify(systemExit).exit(0);
	}
}
