    create table CONTENUFICHIER_COFIC (
        id number(19,0) not null,
        version number(10,0) not null,
        DATA blob,
        CONSTRAINT PK_COFIC primary key (id) USING INDEX TABLESPACE @appli@_INDX
    )
    TABLESPACE @appli@_DATA PCTFREE 10 PCTUSED 80
    ;

    create table FICHIERJOINT_FIJO (
        id number(19,0) not null,
        version number(10,0) not null,
        dateHeureSoumission timestamp,
        nomFichierOriginal varchar2(255 char),
        tailleFichier number(19,0),
        typeMimeFichier varchar2(255 char) not null,
        COFI_ID_LECONTENUDUFICHIER number(19,0),
        CONSTRAINT PK_FIJO primary key (id) USING INDEX TABLESPACE @appli@_INDX
    )
    TABLESPACE @appli@_DATA PCTFREE 10 PCTUSED 80
    ;

	create table DOCUMENTATLAS_DOAT (
        DOAT_ID number(19,0) not null,
        VERSION number(10,0) not null,
        identifiantAtlas varchar2(255 char) not null,
        nom varchar2(255 char) not null,
        nomEnvoi varchar2(255 char),
		ETAT number(10,0) not null,
        dateDePeremption timestamp,
        dateDEnvoiAtlas timestamp,
        dateAccuseReception timestamp,
        datePurgeContenu timestamp,
		FIJO_ID_LEFICHIER number(19,0),
        CONSTRAINT PK_DOAT PRIMARY KEY(DOAT_ID)
            USING INDEX
            TABLESPACE @appli@_INDX,
        CONSTRAINT UK_DOAT_1 UNIQUE(identifiantAtlas)
            USING INDEX
            TABLESPACE @appli@_INDX
    )
        TABLESPACE @appli@_DATA PCTFREE 10 PCTUSED 80 ;

    alter table FICHIERJOINT_FIJO 
        add constraint FK_FIJO_COFI_1 
        foreign key (COFI_ID_LECONTENUDUFICHIER) 
        references CONTENUFICHIER_COFIC;

	alter table DOCUMENTATLAS_DOAT 
        add constraint FK_DOAT_FIJO_1
        foreign key (FIJO_ID_LEFICHIER) 
        references FICHIERJOINT_FIJO;

	create sequence COFIC_ID_SEQUENCE;

	create sequence FIJO_ID_SEQUENCE;

	create sequence DOAT_ID_SEQUENCE;