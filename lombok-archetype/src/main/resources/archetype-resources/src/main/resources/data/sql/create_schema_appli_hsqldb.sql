CREATE TABLE CLASSEANALYSE1(
    CLASSEANALYSE1_ID numeric(19,0) not null,
    VERSION numeric(10,0) not null,
    IDENTIFIANTMETIER numeric(19,0) not null
);

create sequence CLASSEANALYSE1_ID_SEQUENCE;