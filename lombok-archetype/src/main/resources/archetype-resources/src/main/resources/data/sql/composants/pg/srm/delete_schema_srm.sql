---
--- suppression séquences
---
DROP SEQUENCE ZMESSAGE_ID_SEQUENCE 
;
DROP SEQUENCE ZMESSAGETYPE_ID_SEQUENCE 
;
DROP SEQUENCE ZPROFIL_ID_SEQUENCE 
;
DROP SEQUENCE ZCODIQUE_ID_SEQUENCE; 
 
---
--- suppression index
---

DROP INDEX IN_PROF_MESS_1 
;
---
--- suppression clés étrangères
 

alter table  ZTJ_MESS_CODI_TJ         
        drop  constraint FK_TJ_MESS_CODI_TJ_MESS_1  
        ;

alter table  ZTJ_MESS_CODI_TJ         
        drop  constraint FK_TJ_MESS_CODI_TJ_CODI_1 
        ;



alter  table  ZTJ_MESS_PROF_TJ        
        drop constraint   FK_TJ_MESS_PROF_TJ_MESS_1  ;

alter  table  ZTJ_MESS_PROF_TJ        
        drop  constraint FK_TJ_MESS_PROF_TJ_PROF_1   ;

alter table ZPROFIL_PROF   
     drop    constraint FK_PROF_MESS__1 ; 


 

 
 
---
--- suppression tables
---

drop  table  ZTJ_MESS_PROF_TJ  ;

drop table  ZTJ_MESS_CODI_TJ ;

 drop table ZMESSAGE_MESS  ;

 drop table  ZMESSAGETYPE_METY  ;

 drop table ZPROFIL_PROF  ;

drop table ZCODIQUE_CODI ;