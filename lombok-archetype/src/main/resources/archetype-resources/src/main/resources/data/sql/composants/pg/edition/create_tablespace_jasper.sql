---
--- CREATION DES TABLESPACES DU SCHEMA DU COMPOSANT EDITION
---
CREATE TABLESPACE tbs_j@appli@_data1 owner j@appli@ location '/u03/pgsql/9.1/data/PGL@appli@01/pg_tblspc/j@appli@/tbs_j@appli@_data1';
CREATE TABLESPACE tbs_j@appli@_index1 owner j@appli@ location '/u03/pgsql/9.1/data/PGL@appli@01/pg_tblspc/j@appli@/tbs_j@appli@_index1';
