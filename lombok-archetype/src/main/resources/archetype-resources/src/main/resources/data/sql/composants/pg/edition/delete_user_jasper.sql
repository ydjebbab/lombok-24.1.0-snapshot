---
--- SUPPRESSION DE L'UTILISATEUR DU COMPOSANT EDITION
--- VERSION : V13R05
--- 
--- Modifications 
--- wpetit        03/05/2007    creation      
---

--DROP USER J@appli@ CASCADE;

revoke all on tablespace tbs_j@appli@_data1 from j@appli@;
revoke all on tablespace tbs_j@appli@_index1 from j@appli@;

DROP USER j@appli@;
