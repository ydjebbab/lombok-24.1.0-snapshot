    create table ZTYPEDOCUMENTATLAS_ZTDA (
        ZTDA_ID number(19,0) not null,
        VERSION number(10,0) not null,
        code varchar2(80 char) not null,
        libelle varchar2(80 char) not null,
        CONSTRAINT PK_ZTDA PRIMARY KEY(ZTDA_ID)
            USING INDEX
            TABLESPACE @appli@_INDX,
        CONSTRAINT UK_ZTDA_1 UNIQUE(code)
            USING INDEX
            TABLESPACE @appli@_INDX
    )
        TABLESPACE @appli@_DATA PCTFREE 10 PCTUSED 80 ;
        
    alter table ZDOCUMENTATLAS_ZDOA 
        add ZTDA_ID_LETYPEDOCUMENT number(19,0);
        
    alter table ZDOCUMENTATLAS_ZDOA 
        add constraint FK_ZDOA_ZTDA_1
        foreign key (ZTDA_ID_LETYPEDOCUMENT) 
        references ZTYPEDOCUMENTATLAS_ZTDA;

    create sequence ZTDA_ID_SEQUENCE;
