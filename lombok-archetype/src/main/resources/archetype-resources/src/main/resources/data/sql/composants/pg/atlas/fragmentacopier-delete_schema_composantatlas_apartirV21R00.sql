  drop table ZDOCUMENTATLAS_ZDOA;
  
  drop table ZFICHIERJOINT_ZFIJ CASCADE;

  drop table ZCONTENUFICHIER_ZCOF CASCADE;
    
  drop table ZTYPEDOCUMENTATLAS_ZTDA CASCADE;
  
  drop sequence ZDOA_ID_SEQUENCE;

  drop sequence ZFIJ_ID_SEQUENCE;

  drop sequence ZCOF_ID_SEQUENCE;
  
  drop sequence ZTDA_ID_SEQUENCE;