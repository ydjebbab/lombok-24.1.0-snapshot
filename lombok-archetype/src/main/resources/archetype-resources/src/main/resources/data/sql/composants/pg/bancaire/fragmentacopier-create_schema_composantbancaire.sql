create table ZGUICHETBANC_ZGUI (
        ZGUI_ID numeric(19,0) not null,
        VERSION numeric(10,0) not null,
        codeEtablissement varchar(255),
        codeGuichet varchar(255),
        nomGuichet varchar(255),
        adresseGuichet varchar(255),
        codePostal varchar(255),
        villeCodePostal varchar(255),
        denominationComplete varchar(255),
        denominationAbregee varchar(255),
        libelleAbrevDeDomiciliation varchar(255),
        codeReglementDesCredits varchar(255),
        moisDeModification varchar(255),
        anneeDeModification varchar(255),
        codeEnregistrement varchar(255),
        nouveauCodeEtablissement varchar(255),
        nouveauCodeGuichet varchar(255),
        estEnVoieDePeremption numeric(1,0),
        estPerime numeric(1,0),
        dateDerniereMiseAJour timestamp,
        CONSTRAINT PK_ZGUICHETBANC_ZGUI primary key (ZGUI_ID)
        USING INDEX TABLESPACE @appli@_INDX
    ) TABLESPACE @appli@_DATA ;
    
create index IN_ZGUI_2 on ZGUICHETBANC_ZGUI (codeGuichet)
            TABLESPACE @appli@_INDX;

create index IN_ZGUI_3 on ZGUICHETBANC_ZGUI (dateDerniereMiseAJour)
            TABLESPACE @appli@_INDX;

create index IN_ZGUI_1 on ZGUICHETBANC_ZGUI (codeEtablissement)
            TABLESPACE @appli@_INDX;
    
create sequence ZGUI_ID_SEQUENCE;
