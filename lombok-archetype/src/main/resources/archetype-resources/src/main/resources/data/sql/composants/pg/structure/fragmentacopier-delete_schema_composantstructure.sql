    drop table TJ_ZECL_ZSTR_TJ ;

    drop table TJ_ZSTR_ZATT_TJ ;

    drop table TJ_ZSTR_ZFON_TJ ;

    drop table ZADRESSE_ZADR ;

    drop table ZATTRIBUTIONTGCP_ZATT ;

    drop table ZCATEGORIESTRUCTURECP_ZCAS ;

    drop table ZCODEPOSTAL_ZCPO ;

    drop table ZCOMMUNE_ZCOM ;

    drop table ZCOORDBANCAIRECP_ZCOB ;

    drop table ZDEPARTEMENT_ZDEP ;

    drop table ZDOMAINECP_ZDOM ;

    drop table ZECLATEMENTCP_ZECL ;

    drop table ZFONCTIONCP_ZFON ;

    drop table ZGUICHETBANC_ZGUI ;

    drop table ZMODEGESTIONCP_ZMGE ;

    drop table ZPAYSINSEE_ZPIN ;

    drop table ZPAYSISO_ZPIS ;

    drop table ZRATTACHEMENTCP_ZRAT ;

    drop table ZREGION_ZREG ;

    drop table ZREROUTAGECOORDBANCAIRECP_ZRCB ;

    drop table ZRIB_RIB ;

    drop table ZSTRUCTURECP_ZSTR ;

    drop table ZSTR_LESHORAIRESCP ;

    drop table ZSTR_LESPOLESACTIV ;

    drop table ZSTR_LESSERVICESCP ;

    drop table ZTYPEPOLEACTIVITECP_ZTYP ;

    drop table ZTYPESERVICECP_ZTYS ;

    drop table ZTYPESERVICEDIRECTION_ZTSD ;

    drop table ZTYPESTRUCTURECP_ZTSC ;

    drop table ZPROPIMMEUBLE_ZPIM ;

	drop table ZORGAACCUEIL_ZOAC ;

	drop table ZLIENSDEP_ZLID  ;

	drop table ZTYPELIEN_ZTLI  ;

	drop table ZLID_QSTRUCTLIES  ;

	drop sequence ZADR_ID_SEQUENCE;

    drop sequence ZATT_ID_SEQUENCE;

    drop sequence ZCAS_ID_SEQUENCE;

    drop sequence ZCOB_ID_SEQUENCE;

    drop sequence ZCOM_ID_SEQUENCE;

    drop sequence ZCPO_ID_SEQUENCE;

    drop sequence ZDEP_ID_SEQUENCE;

    drop sequence ZDOM_ID_SEQUENCE;

    drop sequence ZECL_ID_SEQUENCE;

    drop sequence ZFON_ID_SEQUENCE;

    drop sequence ZGUI_ID_SEQUENCE;

    drop sequence ZMGE_ID_SEQUENCE;

    drop sequence ZPAY_ID_SEQUENCE;

    drop sequence ZPIN_ID_SEQUENCE;

    drop sequence ZRAT_ID_SEQUENCE;

    drop sequence ZRCB_ID_SEQUENCE;

    drop sequence ZRIB_ID_SEQUENCE;

    drop sequence ZSTR_ID_SEQUENCE;

    drop sequence ZTSC_ID_SEQUENCE;

    drop sequence ZTSD_ID_SEQUENCE;

    drop sequence ZTYP_ID_SEQUENCE;

    drop sequence ZTYS_ID_SEQUENCE;

	drop sequence ZPIM_ID_SEQUENCE;

    drop sequence ZOAC_ID_SEQUENCE;

	drop  sequence ZLID_ID_SEQUENCE;

     drop  sequence ZTLI_ID_SEQUENCE;
