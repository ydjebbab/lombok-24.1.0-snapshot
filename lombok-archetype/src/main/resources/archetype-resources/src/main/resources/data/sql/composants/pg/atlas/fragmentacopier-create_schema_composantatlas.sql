    create table CONTENUFICHIER_COFIC (
        id numeric(19,0) not null,
        version numeric(10,0) not null,
        DATA bytea,
        CONSTRAINT PK_COFIC primary key (id) USING INDEX TABLESPACE @appli@_INDX
    )
    TABLESPACE @appli@_DATA 
    ;

    create table FICHIERJOINT_FIJO (
        id numeric(19,0) not null,
        version numeric(10,0) not null,
        dateHeureSoumission timestamp,
        nomFichierOriginal varchar(255),
        tailleFichier numeric(19,0),
        typeMimeFichier varchar(255) not null,
        COFI_ID_LECONTENUDUFICHIER numeric(19,0),
        CONSTRAINT PK_FIJO primary key (id) USING INDEX TABLESPACE @appli@_INDX
    )
    TABLESPACE @appli@_DATA 
    ;

	create table DOCUMENTATLAS_DOAT (
        DOAT_ID numeric(19,0) not null,
        VERSION numeric(10,0) not null,
        identifiantAtlas varchar(255) not null,
        nom varchar(255) not null,
        nomEnvoi varchar(255),
		ETAT numeric(10,0) not null,
        dateDePeremption timestamp,
        dateDEnvoiAtlas timestamp,
        dateAccuseReception timestamp,
        datePurgeContenu timestamp,
		FIJO_ID_LEFICHIER numeric(19,0),
        CONSTRAINT PK_DOAT PRIMARY KEY(DOAT_ID)
            USING INDEX
            TABLESPACE @appli@_INDX,
        CONSTRAINT UK_DOAT_1 UNIQUE(identifiantAtlas)
            USING INDEX
            TABLESPACE @appli@_INDX
    )
        TABLESPACE @appli@_DATA  ;

    alter table FICHIERJOINT_FIJO 
        add constraint FK_FIJO_COFI_1 
        foreign key (COFI_ID_LECONTENUDUFICHIER) 
        references CONTENUFICHIER_COFIC;

	alter table DOCUMENTATLAS_DOAT 
        add constraint FK_DOAT_FIJO_1
        foreign key (FIJO_ID_LEFICHIER) 
        references FICHIERJOINT_FIJO;

	create sequence COFIC_ID_SEQUENCE;

	create sequence FIJO_ID_SEQUENCE;

	create sequence DOAT_ID_SEQUENCE;