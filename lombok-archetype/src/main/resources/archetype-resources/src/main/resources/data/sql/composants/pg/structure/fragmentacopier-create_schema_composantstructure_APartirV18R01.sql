    create table TJ_ZECL_ZSTR_TJ (
        ZECL_ID numeric(19,0) not null,
        ZSTR_ID numeric(19,0) not null,
        CONSTRAINT PK_TJ_ZECL_ZSTR_TJ PRIMARY KEY(ZECL_ID, ZSTR_ID)
            USING INDEX
            TABLESPACE @appli@_INDX
    )
    TABLESPACE @appli@_DATA 
    ;

    create table TJ_ZSTR_ZATT_TJ (
        ZSTR_ID numeric(19,0) not null,
        ZATT_ID numeric(19,0) not null,
        CONSTRAINT PK_TJ_ZSTR_ZATT_TJ PRIMARY KEY(ZSTR_ID, ZATT_ID)
            USING INDEX
            TABLESPACE @appli@_INDX        
    )
    TABLESPACE @appli@_DATA 
    ;

    create table TJ_ZSTR_ZFON_TJ (
        ZSTR_ID numeric(19,0) not null,
        ZFON_ID numeric(19,0) not null,
        CONSTRAINT PK_TJ_ZSTR_ZFON_TJ PRIMARY KEY(ZSTR_ID, ZFON_ID)
            USING INDEX
            TABLESPACE @appli@_INDX                
    )
    TABLESPACE @appli@_DATA 
    ;

    create table ZADRESSE_ZADR (
        ZADR_ID numeric(19,0) not null,
        version numeric(10,0) not null,
        codePostalEtCedex varchar(255),
        ligne2ServiceAppartEtgEsc varchar(255),
        ligne3BatimentImmeubleResid varchar(255),
        ligne4NumeroLibelleVoie varchar(255),
        ligne5LieuDitMentionSpeciale varchar(255),
        pays varchar(255),
        ville varchar(255),
        CONSTRAINT PK_ZADR PRIMARY KEY(ZADR_ID)
            USING INDEX
            TABLESPACE @appli@_INDX                        
    )
        TABLESPACE @appli@_DATA 
    ;

    create table ZATTRIBUTIONTGCP_ZATT (
        ZATT_ID numeric(19,0) not null,
        VERSION numeric(10,0) not null,
        libelleattributionTG varchar(255) not null,
        codeattributionTG varchar(255),
        CONSTRAINT PK_ZATT PRIMARY KEY(ZATT_ID)
            USING INDEX
            TABLESPACE @appli@_INDX,                       
        CONSTRAINT UK_ZATT_1 UNIQUE(libelleattributionTG)
            USING INDEX
            TABLESPACE @appli@_INDX
    )
        TABLESPACE @appli@_DATA 
    ;

    create table ZCATEGORIESTRUCTURECP_ZCAS (
        ZCAS_ID numeric(19,0) not null,
        VERSION numeric(10,0) not null,
        CODECATEGORIE varchar(255) not null,
        libellecategorie varchar(255),
        CONSTRAINT PK_ZCAS PRIMARY KEY(ZCAS_ID)
            USING INDEX
            TABLESPACE @appli@_INDX,        
        CONSTRAINT UK_ZCAS_1 UNIQUE(CODECATEGORIE)
            USING INDEX
            TABLESPACE @appli@_INDX
    )
        TABLESPACE @appli@_DATA     
    ;

    create table ZCODEPOSTAL_ZCPO (
        ZCPO_ID numeric(19,0) not null,
        version numeric(10,0) not null,
        code varchar(255) not null,
        ville varchar(255) not null,
        CONSTRAINT PK_ZCPO PRIMARY KEY(ZCPO_ID)
            USING INDEX
            TABLESPACE @appli@_INDX,                
        CONSTRAINT UK_ZCPO_1 UNIQUE(code, ville)
            USING INDEX
            TABLESPACE @appli@_INDX
    )
        TABLESPACE @appli@_DATA         
    ;

    create table ZCOMMUNE_ZCOM (
        ZCOM_ID numeric(19,0) not null,
        codecommuneINSEE varchar(255),
        libellecommune varchar(255),
        ZDEP_ID_LESCOMMUNES numeric(19,0),
        CONSTRAINT PK_ZCOM PRIMARY KEY(ZCOM_ID)
            USING INDEX
            TABLESPACE @appli@_INDX,        
        CONSTRAINT UK_ZCOM_1 UNIQUE(codecommuneINSEE)
            USING INDEX
            TABLESPACE @appli@_INDX
    )
        TABLESPACE @appli@_DATA         
    ;

    create table ZCOORDBANCAIRECP_ZCOB (
        ZCOB_ID numeric(19,0) not null,
        VERSION numeric(10,0) not null,
        codeFlux varchar(255),
        codeIC varchar(255),
        biC varchar(255),
        ibAN varchar(255),
        ZRIB_ID_LERIBCLASSIQUE numeric(19,0) unique,
        ZRIB_ID_LERIBAUTOMATISE numeric(19,0) unique,
        ZRCB_ID_LEREROUTAGE numeric(19,0) unique,
        ZSTR_ID_LESCOORDBANCAIRESCP numeric(19,0),
        CONSTRAINT PK_ZCOB PRIMARY KEY(ZCOB_ID)
            USING INDEX
            TABLESPACE @appli@_INDX                
    )
       TABLESPACE @appli@_DATA         
    ;

    create table ZDEPARTEMENT_ZDEP (
        ZDEP_ID numeric(19,0) not null,
        codedepartementINSEE varchar(255),
        libelledepartement varchar(255),
        ZREG_ID_LESDEPARTEMENTS numeric(19,0),
        CONSTRAINT PK_ZDEP PRIMARY KEY(ZDEP_ID)
            USING INDEX
            TABLESPACE @appli@_INDX,
        CONSTRAINT UK_ZDEP_1 UNIQUE(codedepartementINSEE)
            USING INDEX
            TABLESPACE @appli@_INDX
    )
       TABLESPACE @appli@_DATA         
    ;

    create table ZDOMAINECP_ZDOM (
        ZDOM_ID numeric(19,0) not null,
        VERSION numeric(10,0) not null,
        codedomaine varchar(255) not null,
        libelledomaine varchar(255),
        CONSTRAINT PK_ZDOM PRIMARY KEY(ZDOM_ID)
            USING INDEX
            TABLESPACE @appli@_INDX,
        CONSTRAINT UK_ZDOM_1 UNIQUE(codedomaine)
            USING INDEX
            TABLESPACE @appli@_INDX        
    )
       TABLESPACE @appli@_DATA      
    ;

    create table ZECLATEMENTCP_ZECL (
        ZECL_ID numeric(19,0) not null,
        ZECLATEMENTCP_DIS varchar(255) not null,
        VERSION numeric(10,0) not null,
        dateDerniereMaj timestamp,
        dateDecision timestamp,
        dateRealisation timestamp,
        ZSTR_ID_LESECLSTRUBENEF numeric(19,0),
        CONSTRAINT PK_ZECL PRIMARY KEY(ZECL_ID)
            USING INDEX
            TABLESPACE @appli@_INDX        
    )
       TABLESPACE @appli@_DATA          
    ;

    create table ZFONCTIONCP_ZFON (
        ZFON_ID numeric(19,0) not null,
        VERSION numeric(10,0) not null,
        codefonction varchar(255) not null,
        libellefonction varchar(255),
        CONSTRAINT PK_ZFON PRIMARY KEY(ZFON_ID)
            USING INDEX
            TABLESPACE @appli@_INDX,
        CONSTRAINT UK_ZFON_1 UNIQUE(codefonction)
            USING INDEX
            TABLESPACE @appli@_INDX            
    )
       TABLESPACE @appli@_DATA              
    ;

    create table ZGUICHETBANC_ZGUI (
        ZGUI_ID numeric(19,0) not null,
        VERSION numeric(10,0) not null,
        codeEtablissement varchar(255),
        codeGuichet varchar(255),
        nomGuichet varchar(255),
        adresseGuichet varchar(255),
        codePostal varchar(255),
        villeCodePostal varchar(255),
        denominationComplete varchar(255),
        denominationAbregee varchar(255),
        libelleAbrevDeDomiciliation varchar(255),
        codeReglementDesCredits varchar(255),
        moisDeModification varchar(255),
        anneeDeModification varchar(255),
        codeEnregistrement varchar(255),
        nouveauCodeEtablissement varchar(255),
        nouveauCodeGuichet varchar(255),
        estEnVoieDePeremption numeric(1,0),
        estPerime numeric(1,0),
        dateDerniereMiseAJour timestamp,
        CONSTRAINT PK_ZGUI PRIMARY KEY(ZGUI_ID)
            USING INDEX
            TABLESPACE @appli@_INDX 
    )
        TABLESPACE @appli@_DATA              
    ;

    create table ZMODEGESTIONCP_ZMGE (
        ZMGE_ID numeric(19,0) not null,
        VERSION numeric(10,0) not null,
        codemodegestion varchar(255) not null,
        libellemodegestion varchar(255),
        CONSTRAINT PK_ZMGE PRIMARY KEY(ZMGE_ID)
            USING INDEX
            TABLESPACE @appli@_INDX,
        CONSTRAINT UK_ZMGE_1 UNIQUE(codemodegestion)
            USING INDEX
            TABLESPACE @appli@_INDX        
    )
        TABLESPACE @appli@_DATA      
    ;

    create table ZPAYSINSEE_ZPIN (
        ZPIN_ID numeric(19,0) not null,
        codePaysInsee varchar(255),
        nom varchar(255),
        CONSTRAINT PK_ZPIN PRIMARY KEY(ZPIN_ID)
            USING INDEX
            TABLESPACE @appli@_INDX,
        CONSTRAINT UK_ZPIN_1 UNIQUE(codePaysInsee)
            USING INDEX
            TABLESPACE @appli@_INDX
    )
        TABLESPACE @appli@_DATA          
    ;

    create table ZPAYSISO_ZPIS (
        ZPIS_ID varchar(255) not null,
        CODEISO varchar(255),
        nom varchar(255),
        CONSTRAINT PK_ZPIS PRIMARY KEY(ZPIS_ID)
            USING INDEX
            TABLESPACE @appli@_INDX         
    )
        TABLESPACE @appli@_DATA          
    ;

    create table ZRATTACHEMENTCP_ZRAT (
        ZRAT_ID numeric(19,0) not null,
        ZRATTACHEMENTCP_DIS varchar(255) not null,
        VERSION numeric(10,0) not null,
        dateDerniereMaj timestamp,
        dateDecision timestamp,
        dateRealisation timestamp,
        ZSTR_ID_LESRATSTRUABSORBEES numeric(19,0),
        CONSTRAINT PK_ZRAT PRIMARY KEY(ZRAT_ID)
            USING INDEX
            TABLESPACE @appli@_INDX                 
    )
        TABLESPACE @appli@_DATA              
    ;

    create table ZREGION_ZREG (
        ZREG_ID numeric(19,0) not null,
        coderegionINSEE varchar(255),
        libelleregion varchar(255),
        CONSTRAINT PK_ZREG PRIMARY KEY(ZREG_ID)
            USING INDEX
            TABLESPACE @appli@_INDX,
        CONSTRAINT UK_ZREG_1 UNIQUE(coderegionINSEE)
            USING INDEX
            TABLESPACE @appli@_INDX        
    )
        TABLESPACE @appli@_DATA      
    ;

    create table ZREROUTAGECOORDBANCAIRECP_ZRCB (
        ZRCB_ID numeric(19,0) not null,
        VERSION numeric(10,0) not null,
        codeIC varchar(255),
        dateDebutReroutage timestamp,
        dateFinReroutage timestamp,
        identifiantBeneficiaire varchar(255),
        CONSTRAINT PK_ZRCB PRIMARY KEY(ZRCB_ID)
            USING INDEX
            TABLESPACE @appli@_INDX        
    )
        TABLESPACE @appli@_DATA          
    ;    create table ZRIB_RIB (
        id numeric(19,0) not null,
        version numeric(10,0) not null,
        codeBanque varchar(255) not null,
        codeGuichet varchar(255) not null,
        numeroCompte varchar(255) not null,
        cleRib varchar(255),
        estAutomatise boolean,
        CONSTRAINT PK_ZRIB PRIMARY KEY(id)
            USING INDEX
            TABLESPACE @appli@_INDX         
    )
        TABLESPACE @appli@_DATA              
    ;


    create table ZSTRUCTURECP_ZSTR (
        ZSTR_ID numeric(19,0) not null,
        ZSTRUCTURECP_DIS varchar(255) not null,
        VERSION numeric(10,0) not null,
        IDENTITENOMINOE varchar(255) not null,
        dateDerniereMaj timestamp,
        codique varchar(255),
        codeAnnexe varchar(255),
        denominationAbregee varchar(255),
        denominationStandard varchar(255),
        codeLiaison varchar(255),
        libelleLong varchar(255),
        libelleStandard varchar(255),
        libelleAbrege varchar(255),
        siret varchar(255),
        etat varchar(255),
        adresseMelGenerique varchar(255),
        logementFonction varchar(255),
        ZTSC_ID_LETYPESTRUCTURECP numeric(19,0),
        ZTSD_ID_LETYPESERVICEDIRECTION numeric(19,0),
        ZCAS_ID_LACATEGORIESTRUCTURECP numeric(19,0),
        ZPIN_ID_LEPAYS numeric(19,0),
        adresseAbregee varchar(255),
        ZADR_ID_LADRESSEGEOGRAPHIQUE numeric(19,0) unique,
        ZADR_ID_LADRESSEPOSTALE numeric(19,0) unique,
        telStandard varchar(255),
        telPoste1 varchar(255),
        telPoste2 varchar(255),
        telecopie varchar(255),
        horairesAbg varchar(255),
        horairesObsCompl varchar(255),
        remunerationCodeIR varchar(255),
        remunerationEchelleLettre varchar(255),
        codeEtablissmentCEP varchar(255),
        codeGuichetCEP varchar(255),
        codeDICGL varchar(255),
        diCGL varchar(255),
        tenueComptaDateDebut timestamp,
        tenueComptaDateFin timestamp,
        ZSTR_ID_LACENTRALCOMPTABLE numeric(19,0),
        ZSTR_ID_LEMODEGESTION numeric(19,0),
        dateCreation timestamp,
        dateSuppression timestamp,
        dateDecision timestamp,
        dateRealisation timestamp,
        ancienCodique varchar(255),
        ancienCodeAnnexe varchar(255),
        ZRAT_ID_LESSTRUABSORBEES numeric(19,0),
        ZCOM_ID_LACOMMUNE numeric(19,0),
        codeCategTG varchar(255),
        libCategTG varchar(255),
        codeCategRF varchar(255),
        libCategRF varchar(255),
        codeGrRemEtger varchar(255),
        libGrRemEtger varchar(255),
        CONSTRAINT PK_ZSTR PRIMARY KEY(ZSTR_ID)
            USING INDEX
            TABLESPACE @appli@_INDX,
        CONSTRAINT UK_ZSTR_1 UNIQUE(IDENTITENOMINOE)
            USING INDEX
            TABLESPACE @appli@_INDX        
    )
        TABLESPACE @appli@_DATA            
    ;

    create index IN_ZSTR_1 on ZSTRUCTURECP_ZSTR (ZSTRUCTURECP_DIS)
                TABLESPACE @appli@_INDX;

    create index IN_ZSTR_2 on ZSTRUCTURECP_ZSTR (CODIQUE)
                TABLESPACE @appli@_INDX;
                
    create index IN_ZSTR_3 on ZSTRUCTURECP_ZSTR (ZCAS_ID_LACATEGORIESTRUCTURECP)
                TABLESPACE @appli@_INDX;                

    create index IN_ZSTR_4 on ZSTRUCTURECP_ZSTR (ZCOM_ID_LACOMMUNE)
                TABLESPACE @appli@_INDX;   

    create table ZSTR_LESHORAIRESCP (
        ZSTR_ID_LESHORAIRESCP numeric(19,0) not null,
        joursemaine varchar(255),
        matindebut varchar(255),
        matinfin varchar(255),
        apremdebut varchar(255),
        apremfin varchar(255),
        ZSTR_ID_LESHORAIRESCP_INDEX numeric(10,0),
        CONSTRAINT PK_ZSTR_LESHORAIRESCP PRIMARY KEY(ZSTR_ID_LESHORAIRESCP, ZSTR_ID_LESHORAIRESCP_INDEX)
            USING INDEX
            TABLESPACE @appli@_INDX        
    )
        TABLESPACE @appli@_DATA      
    ;

    create table ZSTR_LESPOLESACTIV (
        ZSTR_ID_LESPOLESACTIV numeric(19,0),
        melpoleactivite varchar(255),
        ZTYP_ID_LETYPEPOLEACTIV numeric(19,0),
        ZSTR_ID_LESPOLESACTIV_INDEX numeric(10,0),
        CONSTRAINT PK_ZSTR_LESPOLESACTIV PRIMARY KEY(ZSTR_ID_LESPOLESACTIV, ZSTR_ID_LESPOLESACTIV_INDEX)
            USING INDEX
            TABLESPACE @appli@_INDX                
    )
        TABLESPACE @appli@_DATA          
    ;

    create table ZSTR_LESSERVICESCP (
        ZSTR_ID_LESSERVICESCP numeric(19,0),
        telService varchar(255),
        ZTYS_ID_LETYPESERVICECP numeric(19,0),
        ZSTR_ID_LESSERVICESCP_INDEX numeric(10,0),
        CONSTRAINT PK_ZSTR_LESSERVICESCP PRIMARY KEY(ZSTR_ID_LESSERVICESCP, ZSTR_ID_LESSERVICESCP_INDEX)
            USING INDEX
            TABLESPACE @appli@_INDX        
    )
        TABLESPACE @appli@_DATA              
    ;

    create table ZTYPEPOLEACTIVITECP_ZTYP (
        ZTYP_ID numeric(19,0) not null,
        VERSION numeric(10,0) not null,
        libpoleactivite varchar(255) not null,
        codepoleactivite varchar(255),
        CONSTRAINT PK_ZTYP PRIMARY KEY(ZTYP_ID)
            USING INDEX
            TABLESPACE @appli@_INDX,
        CONSTRAINT UK_ZTYP_1 UNIQUE(libpoleactivite)
            USING INDEX
            TABLESPACE @appli@_INDX                
    )
        TABLESPACE @appli@_DATA                  
    ;

    create table ZTYPESERVICECP_ZTYS (
        ZTYS_ID numeric(19,0) not null,
        VERSION numeric(10,0) not null,
        codeservice varchar(255) not null,
        libelleservice varchar(255),
        ZTYD_ID_UNDOMAINECP numeric(19,0),
        CONSTRAINT PK_ZTYS PRIMARY KEY(ZTYS_ID)
            USING INDEX
            TABLESPACE @appli@_INDX,
        CONSTRAINT UK_ZTYS_1 UNIQUE(codeservice)
            USING INDEX
            TABLESPACE @appli@_INDX         
    )
        TABLESPACE @appli@_DATA                      
    ;

    create table ZTYPESERVICEDIRECTION_ZTSD (
        ZTSD_ID numeric(19,0) not null,
        VERSION numeric(10,0) not null,
        codeTypeServiceDirection varchar(255) not null,
        libelleTypeServiceDirection varchar(255),
        CONSTRAINT PK_ZTSD PRIMARY KEY(ZTSD_ID)
            USING INDEX
            TABLESPACE @appli@_INDX,
        CONSTRAINT UK_ZTSD_1 UNIQUE(codeTypeServiceDirection)
            USING INDEX
            TABLESPACE @appli@_INDX        
    )
        TABLESPACE @appli@_DATA                          
    ;

    create table ZTYPESTRUCTURECP_ZTSC (
        ZTSC_ID numeric(19,0) not null,
        VERSION numeric(10,0) not null,
        codeTypeStructure varchar(255) not null,
        libelleTypeStructure varchar(255),
        CONSTRAINT PK_ZTSC PRIMARY KEY(ZTSC_ID)
            USING INDEX
            TABLESPACE @appli@_INDX,
        CONSTRAINT UK_ZTSC_1 UNIQUE(codeTypeStructure)
            USING INDEX
            TABLESPACE @appli@_INDX        
    )
        TABLESPACE @appli@_DATA                              
    ;


    alter table TJ_ZECL_ZSTR_TJ 
        add constraint FK_TJ_ZECL_ZSTR_TJ_ZSTR_1 
        foreign key (ZSTR_ID) 
        references ZSTRUCTURECP_ZSTR deferrable 
         initially deferred;

    alter table TJ_ZECL_ZSTR_TJ 
        add constraint FK_TJ_ZECL_ZSTR_TJ_ZECL_1 
        foreign key (ZECL_ID) 
        references ZECLATEMENTCP_ZECL deferrable 
         initially deferred;

    alter table TJ_ZSTR_ZATT_TJ 
        add constraint FK_TJ_ZSTR_ZATT_TJ_ZATT_1 
        foreign key (ZATT_ID) 
        references ZATTRIBUTIONTGCP_ZATT deferrable 
         initially deferred;

    alter table TJ_ZSTR_ZATT_TJ 
        add constraint FK_TJ_ZSTR_ZATT_TJ_ZSTR_1 
        foreign key (ZSTR_ID) 
        references ZSTRUCTURECP_ZSTR deferrable 
         initially deferred;

    alter table TJ_ZSTR_ZFON_TJ 
        add constraint FK_TJ_ZSTR_ZFON_TJ_ZSTR_1 
        foreign key (ZSTR_ID) 
        references ZSTRUCTURECP_ZSTR deferrable 
         initially deferred;

    alter table TJ_ZSTR_ZFON_TJ 
        add constraint FK_TJ_ZSTR_ZFON_TJ_ZFON_1 
        foreign key (ZFON_ID) 
        references ZFONCTIONCP_ZFON deferrable 
         initially deferred;

    create index IN_ZCOM_ZDEP_1 on ZCOMMUNE_ZCOM (ZDEP_ID_LESCOMMUNES)
                    TABLESPACE @appli@_INDX;

    alter table ZCOMMUNE_ZCOM 
        add constraint FK_ZCOM_ZDEP_1 
        foreign key (ZDEP_ID_LESCOMMUNES) 
        references ZDEPARTEMENT_ZDEP deferrable 
         initially deferred;

    create index IN_ZCOB_ZSTR_1 on ZCOORDBANCAIRECP_ZCOB (ZSTR_ID_LESCOORDBANCAIRESCP)
                TABLESPACE @appli@_INDX;
    

    alter table ZCOORDBANCAIRECP_ZCOB 
        add constraint FK_ZCOB_ZRIB_1 
        foreign key (ZRIB_ID_LERIBCLASSIQUE) 
        references ZRIB_RIB deferrable 
         initially deferred;

    alter table ZCOORDBANCAIRECP_ZCOB 
        add constraint FK_ZCOB_ZSTR_1 
        foreign key (ZSTR_ID_LESCOORDBANCAIRESCP) 
        references ZSTRUCTURECP_ZSTR deferrable 
         initially deferred;

    alter table ZCOORDBANCAIRECP_ZCOB 
        add constraint FK_ZCOM_ZRCB_1 
        foreign key (ZRCB_ID_LEREROUTAGE) 
        references ZREROUTAGECOORDBANCAIRECP_ZRCB deferrable 
         initially deferred;

    alter table ZCOORDBANCAIRECP_ZCOB 
        add constraint FK_ZCOB_ZRIB_2 
        foreign key (ZRIB_ID_LERIBAUTOMATISE) 
        references ZRIB_RIB deferrable 
         initially deferred;

    create index IN_ZDEP_ZREG_1 on ZDEPARTEMENT_ZDEP (ZREG_ID_LESDEPARTEMENTS)
                TABLESPACE @appli@_INDX;

    alter table ZDEPARTEMENT_ZDEP 
        add constraint FK_ZDEP_ZREG_1 
        foreign key (ZREG_ID_LESDEPARTEMENTS) 
        references ZREGION_ZREG deferrable 
         initially deferred;

    create index IN_ZECL_ZSTR_1 on ZECLATEMENTCP_ZECL (ZSTR_ID_LESECLSTRUBENEF)
                TABLESPACE @appli@_INDX;
    

    create index IN_ZECL_1 on ZECLATEMENTCP_ZECL (ZECLATEMENTCP_DIS)
                TABLESPACE @appli@_INDX;

    alter table ZECLATEMENTCP_ZECL 
        add constraint FK_ZECL_ZSTR_1 
        foreign key (ZSTR_ID_LESECLSTRUBENEF) 
        references ZSTRUCTURECP_ZSTR deferrable 
         initially deferred;

    create index IN_ZGUI_2 on ZGUICHETBANC_ZGUI (codeGuichet) 
                TABLESPACE @appli@_INDX;

    create index IN_ZGUI_1 on ZGUICHETBANC_ZGUI (codeEtablissement) 
                TABLESPACE @appli@_INDX;

    create index IN_ZGUI_3 on ZGUICHETBANC_ZGUI (dateDerniereMiseAJour)
                TABLESPACE @appli@_INDX;

    create index IN_ZRAT_ZSTR_1 on ZRATTACHEMENTCP_ZRAT (ZSTR_ID_LESRATSTRUABSORBEES)
                TABLESPACE @appli@_INDX;

    create index IN_ZDIS_1 on ZRATTACHEMENTCP_ZRAT (ZRATTACHEMENTCP_DIS)
                TABLESPACE @appli@_INDX;

    alter table ZRATTACHEMENTCP_ZRAT 
        add constraint FK_ZRAT_ZSTR_1 
        foreign key (ZSTR_ID_LESRATSTRUABSORBEES) 
        references ZSTRUCTURECP_ZSTR deferrable 
         initially deferred;


    create index IN_ZSTR_ZRAT_1 on ZSTRUCTURECP_ZSTR (ZRAT_ID_LESSTRUABSORBEES)
                TABLESPACE @appli@_INDX
    ;

    alter table ZSTRUCTURECP_ZSTR 
        add constraint FK_ZSTR_ZRAT_1 
        foreign key (ZRAT_ID_LESSTRUABSORBEES) 
        references ZRATTACHEMENTCP_ZRAT deferrable 
         initially deferred;

    alter table ZSTRUCTURECP_ZSTR 
        add constraint FK_ZSTR_ZMGE_1 
        foreign key (ZSTR_ID_LEMODEGESTION) 
        references ZMODEGESTIONCP_ZMGE deferrable 
         initially deferred;

    alter table ZSTRUCTURECP_ZSTR 
        add constraint FK_ZSTR_ZADR_1 
        foreign key (ZADR_ID_LADRESSEGEOGRAPHIQUE) 
        references ZADRESSE_ZADR deferrable 
         initially deferred;

    alter table ZSTRUCTURECP_ZSTR 
        add constraint FK_ZSTR_ZTSD_1 
        foreign key (ZTSD_ID_LETYPESERVICEDIRECTION) 
        references ZTYPESERVICEDIRECTION_ZTSD deferrable 
         initially deferred;

    alter table ZSTRUCTURECP_ZSTR 
        add constraint FK_ZSTR_ZPIN_1 
        foreign key (ZPIN_ID_LEPAYS) 
        references ZPAYSINSEE_ZPIN deferrable 
         initially deferred;

    alter table ZSTRUCTURECP_ZSTR 
        add constraint FK_ZSTR_ZCAS_1 
        foreign key (ZCAS_ID_LACATEGORIESTRUCTURECP) 
        references ZCATEGORIESTRUCTURECP_ZCAS deferrable 
         initially deferred;

    alter table ZSTRUCTURECP_ZSTR 
        add constraint FK_ZSTR_ZTSC_1 
        foreign key (ZTSC_ID_LETYPESTRUCTURECP) 
        references ZTYPESTRUCTURECP_ZTSC deferrable 
         initially deferred;

    alter table ZSTRUCTURECP_ZSTR 
        add constraint FK_ZSTR_ZADR_2 
        foreign key (ZADR_ID_LADRESSEPOSTALE) 
        references ZADRESSE_ZADR deferrable 
         initially deferred;

    alter table ZSTRUCTURECP_ZSTR 
        add constraint FK_ZSTR_ZCOM_1 
        foreign key (ZCOM_ID_LACOMMUNE) 
        references ZCOMMUNE_ZCOM deferrable 
         initially deferred;

    alter table ZSTRUCTURECP_ZSTR 
        add constraint FK_ZSTR_ZSTR_1 
        foreign key (ZSTR_ID_LACENTRALCOMPTABLE) 
        references ZSTRUCTURECP_ZSTR deferrable 
         initially deferred;

    alter table ZSTR_LESHORAIRESCP 
        add constraint FK_ZSTR_LESHORAIRESCP_ZSTR_1 
        foreign key (ZSTR_ID_LESHORAIRESCP) 
        references ZSTRUCTURECP_ZSTR deferrable 
         initially deferred;

    alter table ZSTR_LESPOLESACTIV 
        add constraint FK_ZSTR_LESPOLESACTIV_ZSTR_1 
        foreign key (ZSTR_ID_LESPOLESACTIV) 
        references ZSTRUCTURECP_ZSTR deferrable 
         initially deferred;

    alter table ZSTR_LESPOLESACTIV 
        add constraint FK_ZSTR_ZTYP_1 
        foreign key (ZTYP_ID_LETYPEPOLEACTIV) 
        references ZTYPEPOLEACTIVITECP_ZTYP deferrable 
         initially deferred;

    alter table ZSTR_LESSERVICESCP 
        add constraint FK_ZSTR_LESSERVICESCP_ZSTR_1 
        foreign key (ZSTR_ID_LESSERVICESCP) 
        references ZSTRUCTURECP_ZSTR deferrable 
         initially deferred;

    alter table ZSTR_LESSERVICESCP 
        add constraint FK_ZSTR_ZTYS_1 
        foreign key (ZTYS_ID_LETYPESERVICECP) 
        references ZTYPESERVICECP_ZTYS deferrable 
         initially deferred;

    alter table ZTYPESERVICECP_ZTYS 
        add constraint FK_ZTYS_ZDOM_1 
        foreign key (ZTYD_ID_UNDOMAINECP) 
        references ZDOMAINECP_ZDOM deferrable 
         initially deferred;

    create sequence ZADR_ID_SEQUENCE;

    create sequence ZATT_ID_SEQUENCE;

    create sequence ZCAS_ID_SEQUENCE;

    create sequence ZCOB_ID_SEQUENCE;

    create sequence ZCOM_ID_SEQUENCE;

    create sequence ZCPO_ID_SEQUENCE;

    create sequence ZDEP_ID_SEQUENCE;

    create sequence ZDOM_ID_SEQUENCE;

    create sequence ZECL_ID_SEQUENCE;

    create sequence ZFON_ID_SEQUENCE;

    create sequence ZGUI_ID_SEQUENCE;

    create sequence ZMGE_ID_SEQUENCE;

    create sequence ZPAY_ID_SEQUENCE;

    create sequence ZPIN_ID_SEQUENCE;

    create sequence ZRAT_ID_SEQUENCE;

    create sequence ZRCB_ID_SEQUENCE;

    create sequence ZRIB_ID_SEQUENCE;

    create sequence ZSTR_ID_SEQUENCE;

    create sequence ZTSC_ID_SEQUENCE;

    create sequence ZTSD_ID_SEQUENCE;

    create sequence ZTYP_ID_SEQUENCE;

    create sequence ZTYS_ID_SEQUENCE;

  -- Modifications à appliquer à la V17R25

	create table ZPROPIMMEUBLE_ZPIM (
        ZPIM_ID numeric(19,0) not null,
        VERSION numeric(10,0) not null,
        codePropImmeuble varchar(255) not null,
        libellePropImmeuble varchar(255),
		 CONSTRAINT PK_ZPIM PRIMARY KEY(ZPIM_ID)
            USING INDEX
            TABLESPACE @appli@_INDX,
        CONSTRAINT UK_ZPIM_1 UNIQUE(codePropImmeuble)
            USING INDEX
            TABLESPACE @appli@_INDX        
    )
        TABLESPACE @appli@_DATA                          
    ;

	create table ZORGAACCUEIL_ZOAC (
        ZOAC_ID numeric(19,0) not null,
        VERSION numeric(10,0) not null,
        codeOrgAccueil varchar(255) not null,
        libOrgAccueil varchar(255),
        CONSTRAINT PK_ZOAC PRIMARY KEY(ZOAC_ID)
            USING INDEX
            TABLESPACE @appli@_INDX,
        CONSTRAINT UK_ZOAC_1 UNIQUE(codeOrgAccueil)
            USING INDEX
            TABLESPACE @appli@_INDX        
    )
        TABLESPACE @appli@_DATA                          
    ;

  create sequence ZPIM_ID_SEQUENCE;

  create sequence ZOAC_ID_SEQUENCE;
 
  alter table ZCOORDBANCAIRECP_ZCOB 
		add  ibANAutomatise varchar(255) ;

alter table ZSTRUCTURECP_ZSTR 
		add  ZPIM_ID_LEPROPIMMEUBLE numeric(19,0);

alter table ZSTRUCTURECP_ZSTR 
		add  ZOAC_ID_LEORGAACCUEIL numeric(19,0);


  create index IN_ZCOB_2 on ZCOORDBANCAIRECP_ZCOB (codeIC)
                TABLESPACE @appli@_INDX ; 
--modifications V17R45 (recomandations sql advisor)

CREATE INDEX IN_ ZSTR_LESHORAIRESCP_1 ON ZSTR_LESHORAIRESCP 
(ZSTR_ID_LESHORAIRESCP);

CREATE INDEX IN_ ZSTR_LESSERVICESCP_1 ON ZSTR_LESSERVICESCP 
(ZSTR_ID_LESSERVICESCP);

CREATE INDEX IN_ ZSTR_LESPOLESACTIV_1 ON ZSTR_LESPOLESACTIV 
(ZSTR_ID_LESPOLESACTIV); 


--Modifications pour la V17R45 (ajout de liens de dependance et de typelien ) 
 

alter table ZSTRUCTURECP_ZSTR 
		add  EmailREC varchar(255) ;

alter table ZSTRUCTURECP_ZSTR 
		add  AppliRec varchar(255) ;

alter table ZSTRUCTURECP_ZSTR 
		add  LecteurOpt varchar(255) ;

alter table ZSTRUCTURECP_ZSTR 
		add  CodeIndicateur varchar(255) ;

alter table ZSTRUCTURECP_ZSTR 
		add  LibIndicateur varchar(255) ;

alter table ZSTRUCTURECP_ZSTR 
		add   BICCEP  varchar(255) ;

alter table ZSTRUCTURECP_ZSTR 
		add  domiciliationCEP varchar(255) ;



 

 create table  ZLIENSDEP_ZLID (
        ZLID_ID numeric(19,0) not null,
        ZLIENSDEP_DIS  varchar(255) not null,
        VERSION numeric(10,0) not null,
        ZLTI_ID_LETYPELIEN numeric(19,0) not null,
        ZSTR_ID_LESLIENSDEP numeric(19,0),
     CONSTRAINT PK_ZLID  PRIMARY KEY(ZLID_ID)
            USING INDEX
            TABLESPACE @appli@_INDX                
    )
       TABLESPACE @appli@_DATA     ;


  create table ZTYPELIEN_ZTLI (
        ZTLI_ID numeric(19,0) not null,
        VERSION numeric(10,0) not null,
        codetypelien varchar(255) not null,
        libelletypelien varchar(255),
        CONSTRAINT PK_ZTLI primary key (ZTLI_ID)
             USING INDEX
                  TABLESPACE @appli@_INDX ,
           CONSTRAINT UK_ZTLI_1  unique (codetypelien)
			 USING INDEX
            TABLESPACE @appli@_INDX    
    );

  create table ZLID_QSTRUCTLIES (
        ZLID_ID numeric(19,0) not null,
        STRUCTURELIEEPARID varchar(255)
    )
 TABLESPACE @appli@_DATA  ;

 

 

  
 
 alter table ZLIENSDEP_ZLID 
        add constraint FK_ZLID_ZTLI_1 
        foreign key (ZLTI_ID_LETYPELIEN) 
        references ZTYPELIEN_ZTLI deferrable 
         initially deferred;

    alter table ZLIENSDEP_ZLID
        add constraint FK_ZLID_ZSTR_1 
        foreign key (ZSTR_ID_LESLIENSDEP) 
        references  ZSTRUCTURECP_ZSTR deferrable 
         initially deferred;

  



alter table ZLID_QSTRUCTLIES 
        add constraint FK_ZLID_QSTRUCTLIES_ZLID_1 
        foreign key (ZLID_ID) 
      references ZLIENSDEP_ZLID deferrable 
         initially deferred;

   
  

 create index IN_ZLID_1 on  ZLIENSDEP_ZLID (ZLIENSDEP_DIS)
			TABLESPACE @appli@_INDX;
 


create index IN_ZLID_ZSTR_1 on ZLIENSDEP_ZLID (ZSTR_ID_LESLIENSDEP)
                TABLESPACE @appli@_INDX;

create index IN_FK_ZLID_QSTRUCTLIES_ZLID_1 on ZLID_QSTRUCTLIES (ZLID_ID)
                TABLESPACE @appli@_INDX;

  create sequence ZLID_ID_SEQUENCE;

  create sequence ZTLI_ID_SEQUENCE;

----------------------------------------
   
--rajout des index sur les champs utilisés pour faire des recherches dans structure


 create index IN_ZSTR_5 on ZSTRUCTURECP_ZSTR (CODEANNEXE)
                TABLESPACE @appli@_INDX;


create index IN_ZSTR_7  on ZSTRUCTURECP_ZSTR (ZPIN_ID_LEPAYS)
                TABLESPACE @appli@_INDX;

create index IN_ZSTR_8  on ZSTRUCTURECP_ZSTR (DATEDERNIEREMAJ)
                TABLESPACE @appli@_INDX;

create index IN_ZSTR_9  on ZSTRUCTURECP_ZSTR (ETAT)
                TABLESPACE @appli@_INDX;



create index IN_ZCAS_2 on ZCATEGORIESTRUCTURECP_ZCAS (LIBELLECATEGORIE)
                TABLESPACE @appli@_INDX;


create index IN_ZTSC_2 on ZTYPESTRUCTURECP_ZTSC (LIBELLETYPESTRUCTURE)
                TABLESPACE @appli@_INDX;


create index IN_ZRAT_1  on ZRATTACHEMENTCP_ZRAT (DATEDERNIEREMAJ)
                TABLESPACE @appli@_INDX;


create index IN_ZECL_2  on ZECLATEMENTCP_ZECL (DATEDERNIEREMAJ)
                TABLESPACE @appli@_INDX;

create index IN_ZSTR_10  on ZSTRUCTURECP_ZSTR (ZSTR_ID_LACENTRALCOMPTABLE)
                TABLESPACE @appli@_INDX;

create index IN_ZSTR_11  on ZSTRUCTURECP_ZSTR (ZTSC_ID_LETYPESTRUCTURECP)
                TABLESPACE @appli@_INDX;
----------------------------------------
   
--prise en compte de l attribut sages


 ALTER TABLE ZSTRUCTURECP_ZSTR  ADD (SAGES varchar(10 byte));

