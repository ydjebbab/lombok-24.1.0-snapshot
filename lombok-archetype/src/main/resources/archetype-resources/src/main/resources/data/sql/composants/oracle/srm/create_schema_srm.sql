
    drop table ZMESSAGETYPE_METY cascade constraints;

    drop table ZMESSAGE_MESS cascade constraints;

    drop table ZPROFIL_PROF cascade constraints;

    drop sequence ZMESSAGETYPE_ID_SEQUENCE;

    drop sequence ZMESSAGE_ID_SEQUENCE;

    drop sequence ZPROFIL_ID_SEQUENCE;

    drop table ZCODIQUE_CODI cascade constraints;    
    
    drop table ZTJ_MESS_CODI_TJ cascade constraints;

    drop table ZTJ_MESS_PROF_TJ cascade constraints;       

    drop sequence ZCODIQUE_ID_SEQUENCE;
    
    create table ZMESSAGETYPE_METY (
        MESSAGETYPE_ID number(19,0) not null,
        VERSION number(10,0) not null,
        LIBELLEMESSAGETYPE varchar2(2000 char) not null,
        primary key (MESSAGETYPE_ID),
        unique (LIBELLEMESSAGETYPE)
    );

    create table ZMESSAGE_MESS (
        MESSAGE_ID number(19,0) not null,
        VERSION number(10,0) not null,
        LIBELLEMESSAGE varchar2(2000 char) not null,
        DATECREATIONMESSAGE timestamp not null,
        DATEDEBMESSAGE timestamp,
        DATEFINMESSAGE timestamp,
        DATEDEBINACPROF timestamp,
        DATEFININACPROF timestamp,
        TYPEMESSAGE varchar2(255 char),
        primary key (MESSAGE_ID),
        unique (LIBELLEMESSAGE, DATECREATIONMESSAGE)
    );

    create table ZPROFIL_PROF (
        PROFIL_ID number(19,0) not null,
        VERSION number(10,0) not null,
        CODEPROFILCONCATCODEAPPLI varchar2(255 char) not null,
        LIBELLEPROFIL varchar2(255 char) not null,
        CODEPROFIL varchar2(255 char) not null,
        MESS_ID_LISTPROFDESACT number(19,0),
        primary key (PROFIL_ID),
        unique (CODEPROFILCONCATCODEAPPLI)
    );


    create index IN_PROF_MESS_1 on ZPROFIL_PROF (MESS_ID_LISTPROFDESACT);

    alter table ZPROFIL_PROF 
        add constraint FK_PROF_MESS__1 
        foreign key (MESS_ID_LISTPROFDESACT) 
        references ZMESSAGE_MESS;

    create sequence ZMESSAGETYPE_ID_SEQUENCE;

    create sequence ZMESSAGE_ID_SEQUENCE;

    create sequence ZPROFIL_ID_SEQUENCE;

    create table ZCODIQUE_CODI (
        CODIQUE_ID number(19,0) not null,
        VERSION number(10,0) not null,
        LIBELLECODIQUE varchar2(255 char) not null,
        primary key (CODIQUE_ID),
        unique (LIBELLECODIQUE)
    ); 
    
    create table  ZTJ_MESS_CODI_TJ (
        MESS_ID number(19,0) not null,
        CODI_ID number(19,0) not null,
        primary key (MESS_ID, CODI_ID)
    );

    create table  ZTJ_MESS_PROF_TJ (
        MESS_ID number(19,0) not null,
        PROF_ID number(19,0) not null,
        primary key (MESS_ID, PROF_ID)
    );

    

    alter table ZMESSAGE_MESS 
        add UIDCREATEURMESSAGE varchar2(255 char) ;   

   
    alter table ZTJ_MESS_CODI_TJ 
        add constraint FK_TJ_MESS_CODI_TJ_MESS_1 
        foreign key (MESS_ID) 
        references ZMESSAGE_MESS;

    alter table ZTJ_MESS_CODI_TJ 
        add constraint FK_TJ_MESS_CODI_TJ_CODI_1 
        foreign key (CODI_ID) 
        references ZCODIQUE_CODI;

    alter table ZTJ_MESS_PROF_TJ 
        add constraint FK_TJ_MESS_PROF_TJ_MESS_1 
        foreign key (MESS_ID) 
        references ZMESSAGE_MESS;

    alter table ZTJ_MESS_PROF_TJ 
        add constraint FK_TJ_MESS_PROF_TJ_PROF_1 
        foreign key (PROF_ID) 
        references ZPROFIL_PROF;

   
    create sequence ZCODIQUE_ID_SEQUENCE;
    