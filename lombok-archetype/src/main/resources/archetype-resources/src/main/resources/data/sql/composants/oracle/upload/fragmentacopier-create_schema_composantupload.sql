    create table CONTENUFICHIER_COFIC (
        id number(19,0) not null,
        version number(10,0) not null,
        DATA blob,
        CONSTRAINT PK_COFIC primary key (id) USING INDEX TABLESPACE DMO_INDX
    )
    TABLESPACE @appli@_DATA PCTFREE 10 PCTUSED 80
    ;

    create table FICHIERJOINT_FIJO (
        id number(19,0) not null,
        version number(10,0) not null,
        dateHeureSoumission timestamp,
        nomFichierOriginal varchar2(255 char),
        tailleFichier number(19,0),
        typeMimeFichier varchar2(255 char) not null,
        COFI_ID_LECONTENUDUFICHIER number(19,0),
        CONSTRAINT PK_FIJO primary key (id) USING INDEX TABLESPACE DMO_INDX
    )
    TABLESPACE @appli@_DATA PCTFREE 10 PCTUSED 80
    ;

    alter table FICHIERJOINT_FIJO 
        add constraint FK_FIJO_COFI_1 
        foreign key (COFI_ID_LECONTENUDUFICHIER) 
        references CONTENUFICHIER_COFIC;

	create sequence COFIC_ID_SEQUENCE;

	create sequence FIJO_ID_SEQUENCE;