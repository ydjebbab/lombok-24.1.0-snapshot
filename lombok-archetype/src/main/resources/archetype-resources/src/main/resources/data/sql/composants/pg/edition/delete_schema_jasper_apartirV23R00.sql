---
--- suppresssion de toutes les tables editions, aprés la modification du préfixage

    delete from qrtz_fired_triggers;

    delete from qrtz_simple_triggers;

    delete from qrtz_cron_triggers;

    delete from qrtz_blob_triggers;

    delete from qrtz_triggers;

    delete from qrtz_job_details;

    delete from qrtz_calendars;

    delete from qrtz_paused_trigger_grps;

    delete from qrtz_locks;

    delete from qrtz_scheduler_state;

    drop table qrtz_calendars;

    drop table qrtz_fired_triggers;

    drop table qrtz_blob_triggers;

    drop table qrtz_cron_triggers;

    drop table qrtz_simple_triggers;

    drop table qrtz_simprop_triggers;
    
    drop table qrtz_triggers;

    drop table qrtz_job_details;

    drop table qrtz_paused_trigger_grps;

    drop table qrtz_locks;

    drop table qrtz_scheduler_state;


    drop table ZCONTENUEDITION_ZCOE cascade;

    drop table ZFILTREEDITION_ZFIE cascade;

    drop table ZFILTREVALUE_ZFIV cascade;

    drop table ZJOBHISTORY_ZJOH cascade;

    drop table ZPROFILDESTI_ZPRD cascade;

    drop table ZTRCPURGEEDITION_ZTPE cascade;

    drop table ZUIDCONSULT_ZUCO cascade;

    drop sequence ZCONTENUEDITION_ID_SEQUENCE;

    drop sequence ZFILTREEDITION_ID_SEQUENCE;

    drop sequence ZFILTREVALUE_ID_SEQUENCE;

    drop sequence ZJOBHISTORY_ID_SEQUENCE;

    drop sequence ZPROFILDESTI_ID_SEQUENCE;

    drop sequence ZTRCPURGEEDITION_ID_SEQUENCE;

    drop sequence ZUIDCONSULT_ID_SEQUENCE;
