    drop table if exists ZMESSAGETYPE_METY cascade ;

    drop table if exists ZMESSAGE_MESS cascade ;

    drop table if exists ZPROFIL_PROF cascade;

    drop sequence  if exists ZMESSAGETYPE_ID_SEQUENCE;

    drop sequence  if exists ZMESSAGE_ID_SEQUENCE;

    drop sequence  if exists ZPROFIL_ID_SEQUENCE;

    drop table IF EXISTS ZCODIQUE_CODI cascade ;    
    
    drop table IF EXISTS ZTJ_MESS_CODI_TJ cascade ;

    drop table IF EXISTS ZTJ_MESS_PROF_TJ cascade ;       

    drop sequence IF EXISTS ZCODIQUE_ID_SEQUENCE;
    
    create table ZMESSAGETYPE_METY (
        MESSAGETYPE_ID numeric(19,0) not null,
        VERSION numeric(10,0) not null,
        LIBELLEMESSAGETYPE varchar(2000 ) not null,
        primary key (MESSAGETYPE_ID),
        unique (LIBELLEMESSAGETYPE)
    );

    create table ZMESSAGE_MESS (
        MESSAGE_ID numeric(19,0) not null,
        VERSION numeric(10,0) not null,
        LIBELLEMESSAGE varchar(2000 ) not null,
        DATECREATIONMESSAGE timestamp not null,
        DATEDEBMESSAGE timestamp,
        DATEFINMESSAGE timestamp,
        DATEDEBINACPROF timestamp,
        DATEFININACPROF timestamp,
        TYPEMESSAGE varchar(255 ),
        primary key (MESSAGE_ID),
        unique (LIBELLEMESSAGE, DATECREATIONMESSAGE)
    );

    create table ZPROFIL_PROF (
        PROFIL_ID numeric(19,0) not null,
        VERSION numeric(10,0) not null,
        CODEPROFILCONCATCODEAPPLI varchar(255 ) not null,
        LIBELLEPROFIL varchar(255 ) not null,
        CODEPROFIL varchar(255 ) not null,
        MESS_ID_LISTPROFDESACT numeric(19,0),
        primary key (PROFIL_ID),
        unique (CODEPROFILCONCATCODEAPPLI)
    );

    create index IN_PROF_MESS_1 on ZPROFIL_PROF (MESS_ID_LISTPROFDESACT);

    alter table ZPROFIL_PROF 
        add constraint FK_PROF_MESS__1 
        foreign key (MESS_ID_LISTPROFDESACT) 
        references ZMESSAGE_MESS;

    create sequence ZMESSAGETYPE_ID_SEQUENCE;

    create sequence ZMESSAGE_ID_SEQUENCE;

    create sequence ZPROFIL_ID_SEQUENCE;

    create table ZCODIQUE_CODI (
        CODIQUE_ID numeric(19,0) not null,
        VERSION numeric(10,0) not null,
        LIBELLECODIQUE varchar(255 ) not null,
        primary key (CODIQUE_ID),
        unique (LIBELLECODIQUE)
    ); 
    
    create table  ZTJ_MESS_CODI_TJ (
        MESS_ID numeric(19,0) not null,
        CODI_ID numeric(19,0) not null,
        primary key (MESS_ID, CODI_ID)
    );

    create table  ZTJ_MESS_PROF_TJ (
        MESS_ID numeric(19,0) not null,
        PROF_ID numeric(19,0) not null,
        primary key (MESS_ID, PROF_ID)
    );

    

    alter table ZMESSAGE_MESS 
        add UIDCREATEURMESSAGE varchar(255 ) ;   

   
    alter table ZTJ_MESS_CODI_TJ 
        add constraint FK_TJ_MESS_CODI_TJ_MESS_1 
        foreign key (MESS_ID) 
        references ZMESSAGE_MESS;

    alter table ZTJ_MESS_CODI_TJ 
        add constraint FK_TJ_MESS_CODI_TJ_CODI_1 
        foreign key (CODI_ID) 
        references ZCODIQUE_CODI;

    alter table ZTJ_MESS_PROF_TJ 
        add constraint FK_TJ_MESS_PROF_TJ_MESS_1 
        foreign key (MESS_ID) 
        references ZMESSAGE_MESS;

    alter table ZTJ_MESS_PROF_TJ 
        add constraint FK_TJ_MESS_PROF_TJ_PROF_1 
        foreign key (PROF_ID) 
        references ZPROFIL_PROF;

   
    create sequence ZCODIQUE_ID_SEQUENCE;
    