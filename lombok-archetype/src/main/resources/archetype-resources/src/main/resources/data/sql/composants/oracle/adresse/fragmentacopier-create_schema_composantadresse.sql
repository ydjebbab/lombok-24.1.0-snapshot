create table ZCODEPOSTAL_ZCPO (
        ZCPO_ID number(19,0) not null,
        version number(10,0) not null,
        code varchar2(255 char) not null,
        ville varchar2(255 char) not null,
        CONSTRAINT PK_ZCODEPOSTAL_ZCPO primary key (ZCPO_ID)
            USING INDEX TABLESPACE @appli@_INDX,
        CONSTRAINT UK_ZCODEPOSTAL_ZCPO unique (code, ville)
            USING INDEX TABLESPACE @appli@_INDX
    ) TABLESPACE @appli@_DATA PCTFREE 10 PCTUSED 80;
    
create index IN_ZCPO_1 on ZCODEPOSTAL_ZCPO (code)
            TABLESPACE @appli@_INDX;
create index IN_ZCPO_2 on ZCODEPOSTAL_ZCPO (ville)
            TABLESPACE @appli@_INDX;

create sequence ZCPO_ID_SEQUENCE;
