    create table ZCONTENUFICHIER_ZCOF (
        id number(19,0) not null,
        version number(10,0) not null,
        DATA blob,
        CONSTRAINT PK_ZCOF primary key (id) USING INDEX TABLESPACE @appli@_INDX
    )
    TABLESPACE @appli@_DATA PCTFREE 10 PCTUSED 80
    ;

    create table ZFICHIERJOINT_ZFIJ (
        id number(19,0) not null,
        version number(10,0) not null,
        dateHeureSoumission timestamp,
        nomFichierOriginal varchar2(255 char),
        tailleFichier number(19,0),
        typeMimeFichier varchar2(255 char) not null,
        ZCOF_ID_LECONTENUDUFICHIER number(19,0),
        CONSTRAINT PK_ZFIJ primary key (id) USING INDEX TABLESPACE @appli@_INDX
    )
    TABLESPACE @appli@_DATA PCTFREE 10 PCTUSED 80
    ;

	create table ZDOCUMENTATLAS_ZDOA (
        ZDOA_ID number(19,0) not null,
        VERSION number(10,0) not null,
        identifiantAtlas varchar2(255 char) not null,
        nom varchar2(255 char) not null,
        nomEnvoi varchar2(255 char),
		ETAT number(10,0) not null,
        dateDePeremption timestamp,
        dateDEnvoiAtlas timestamp,
        dateAccuseReception timestamp,
        datePurgeContenu timestamp,
		ZFIJ_ID_LEFICHIER number(19,0),
        CONSTRAINT PK_ZDOA PRIMARY KEY(ZDOA_ID)
            USING INDEX
            TABLESPACE @appli@_INDX,
        CONSTRAINT UK_ZDOA_1 UNIQUE(identifiantAtlas)
            USING INDEX
            TABLESPACE @appli@_INDX
    )
        TABLESPACE @appli@_DATA PCTFREE 10 PCTUSED 80 ;

    alter table ZFICHIERJOINT_ZFIJ 
        add constraint FK_ZFIJ_ZCOF_1 
        foreign key (ZCOF_ID_LECONTENUDUFICHIER) 
        references ZCONTENUFICHIER_ZCOF;

	alter table ZDOCUMENTATLAS_ZDOA 
        add constraint FK_ZDOA_ZFIJ_1
        foreign key (ZFIJ_ID_LEFICHIER) 
        references ZFICHIERJOINT_ZFIJ;

	create sequence ZCOF_ID_SEQUENCE;

	create sequence ZFIJ_ID_SEQUENCE;

	create sequence ZDOA_ID_SEQUENCE;
	
    CREATE INDEX IDX_ZCOF_ID_LECONTENUDUFICHIER 
        ON ZFICHIERJOINT_ZFIJ(ZCOF_ID_LECONTENUDUFICHIER) 
        TABLESPACE @appli@_INDX;
    
    CREATE INDEX IDX_ZFIJ_ID_LEFICHIER 
        ON ZDOCUMENTATLAS_ZDOA(ZFIJ_ID_LEFICHIER) 
        TABLESPACE @appli@_INDX;