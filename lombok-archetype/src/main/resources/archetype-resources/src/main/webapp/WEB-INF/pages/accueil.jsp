<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/tags/html/includes.tagf" %>

<!--  Page accueil.jsp -->

<c:set var="responsive" value="true"/>

<app:page titreecran="DMO" 
          titrecontainer="Bienvenue dans votre projet"
          menu="true" 
          donneescontainerboxclass="informationcontainer"
          responsive="${responsive}">
			   
<img src="application/img/lomboklogo.png">               
			   
  <%-- 
  A ajouter pour forcer l'accès à un flux sans passer par la page d'accueil
   <c:if test="${!empty FLUX_ACCUEIL}">
	   <c:redirect url="/${pageContext.request.contextPath}/zf/flux.ex?_flowId=${FLUX_ACCUEIL}"/>
   </c:if>
   --%>
</app:page>