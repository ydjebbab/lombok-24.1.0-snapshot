<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/tags/html/includes.tagf" %>

<!--  Page vue1.jsp -->

<app:page titreecran="Mon projet bienvenue" 
          titrecontainer="flow1 >> vue1" 
          menu="true"> 

<app:onglets cleactive="1">
  	<app:onglet cle="1" libelle="vue1"/>
  	<app:onglet cle="2" libelle="vue2"/>
</app:onglets>

<app:form 
	action="zf1/flux.ex" 
	formobjectname="cu1form" 
	formboxclass="donnees sousonglets">
	<p>hello</p>
	<app:boxboutons>
		<app:submit label="bouton1" transition="bouton1"/>
	</app:boxboutons>
</app:form>
</app:page>