#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.dao.impl;
import ${package}.dao.ClasseConceptAnalyse1Dao;

#if (${provider_persistance} == 'jpa')
import fr.gouv.finances.lombok.jpa.dao.BaseDaoJpaImpl;
public class ClasseConceptAnalyse1DaoImpl extends BaseDaoJpaImpl implements ClasseConceptAnalyse1Dao {
#else
import fr.gouv.finances.lombok.util.base.BaseDaoImpl;
public class ClasseConceptAnalyse1DaoImpl extends BaseDaoImpl implements ClasseConceptAnalyse1Dao {
#end

    @Override
	public void findEnsembleObjets1ParCritere1() {
	}
}
