#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
/*
 * Copyright (c) 2020 DGFiP - Tous droits réservés
 */
package ${package}.config;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.access.AccessDecisionVoter;
import org.springframework.security.access.intercept.RunAsManagerImpl;
import org.springframework.security.access.vote.AffirmativeBased;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;

import fr.gouv.finances.lombok.config.LombokWebSharedConfiguration;
import fr.gouv.finances.lombok.config.ServiceWebSecurityConfigurer;
import fr.gouv.finances.lombok.securite.springsecurity.AucunLdapAuthentificationSecurityLombok;
import fr.gouv.finances.lombok.securite.springsecurity.LdapAuthentificationSecurityLombok;
import fr.gouv.finances.lombok.securite.springsecurity.RealAuthentificationSecurityLombok;
import fr.gouv.finances.lombok.securite.springsecurity.VoteurSecurityLombok;

/**
 * <pre>
 * Classe de configuration de la sécurité dans l'application. 
 * Configuration à compléter pour les URLS.
 * Cette classe remplace :
 * - la partie servlet déclarée dans le fichier WEB.XML 
 * - les définitions contenues dans les anciens fichiers APPLICATIONCONTEXT-COMMUN-SECURITY.XML et SECURITY.XML
 * Normalement un source de propriétés est créée par SPRING BOOT 
 * (depuis le fichier application.properties pour permettre de résoudre les annotations @Value)
 * </pre>
 */
@Configuration
@EnableWebSecurity
public class LombokWebSecurityConfiguration extends WebSecurityConfigurerAdapter
{
    private static final Logger log = LoggerFactory.getLogger(LombokWebSecurityConfiguration.class);
       
    /** code application dans habilitations. */
    @Value(value = "${lombok.aptera.codeappli}")
    private String codeApplicationDansHabilitations;

    /** mode authentification. */
    @Value(value = "${authentification.modeauthentification}")
    private String modeAuthentification;

    /** type annuaire. */
    @Value(value = "${annuaire.typeannuaire}")
    private String typeAnnuaire;

    /** handler en cas d'authentification réussie **/
    @Autowired
    private AuthenticationSuccessHandler authenticationSuccessHandler;

    /** handler en cas d'authentification en échec **/
    @Autowired
    private AuthenticationFailureHandler authenticationFailureHandler;

    /** lombok sireme config. */
    @Autowired
    private LombokWebSharedConfiguration lombokSiremeConfig;

    /**
     * access denied handler. ce bean est retrouvé grâce à l'annotation de scan
     */
    @Autowired
    private AccessDeniedHandler accessDeniedHandler;

    // CF: Le bean logoutSuccessHandler est défini dans LombokWebSharedConfiguration
    @Autowired
    private LogoutSuccessHandler logoutSuccessHandler;

    /**
     * classes de configuration additionnelles (cumulatives) qui héritent de l'interface ServiceWebSecurityConfigurer
     */
    @Autowired(required = false)
    List<ServiceWebSecurityConfigurer> additionalWebSecurityConfigurer;

    public LombokWebSecurityConfiguration()
    {
        super();
    }

    public LombokWebSecurityConfiguration(boolean disableDefaults)
    {
        super(disableDefaults);
    }

    /**
     * methode Authentication aucun ldap provider : provider pour l'accès sans ldap.
     *
     * @return aucun ldap authentification security lombok
     */
    @Bean
    AucunLdapAuthentificationSecurityLombok authenticationAucunLdapProvider()
    {
        AucunLdapAuthentificationSecurityLombok aucunLdapProvider = new AucunLdapAuthentificationSecurityLombok();
        aucunLdapProvider.setTypeAnnuaire(typeAnnuaire);
        aucunLdapProvider.setCodeApplicationDansHabilitations(codeApplicationDansHabilitations);
        aucunLdapProvider.setModeAuthentification(modeAuthentification);
        return aucunLdapProvider;
    }

    /**
     * methode Authentification real provider : provider pour l'accès avec token REAL.
     *
     * @return real authentification security lombok
     */
    @Bean
    RealAuthentificationSecurityLombok authentificationRealProvider()
    {
        RealAuthentificationSecurityLombok realProvider = new RealAuthentificationSecurityLombok();
        realProvider.setTypeAnnuaire(typeAnnuaire);
        realProvider.setCodeApplicationDansHabilitations(codeApplicationDansHabilitations);
        realProvider.setModeAuthentification(modeAuthentification);
        return realProvider;
    }

    /**
     * methode Authentication ldap provider : provider pour l'accès avec ldap.
     *
     * @return ldap authentification security lombok
     */
    @Bean
    LdapAuthentificationSecurityLombok authenticationLdapProvider()
    {
        LdapAuthentificationSecurityLombok ldapProvider = new LdapAuthentificationSecurityLombok();
        ldapProvider.setAuthentificationEtLectureHabilitationsService(lombokSiremeConfig.authentificationEtLectureHabilitationsService());
        ldapProvider.setCodeApplicationDansHabilitations(codeApplicationDansHabilitations);
        ldapProvider.setModeAuthentification(modeAuthentification);
        ldapProvider.setSiremeserviceso(lombokSiremeConfig.getSiremeserviceso());
        return ldapProvider;
    }

    @Bean
    VoteurSecurityLombok<Object> roleVoter()
    {
        VoteurSecurityLombok<Object> roleVoter = new VoteurSecurityLombok<>();
        roleVoter.setRolePrefix("");
        return roleVoter;
    }

    @Bean
    AffirmativeBased accessDecisionManager()
    {
        List<AccessDecisionVoter<? extends Object>> l = new ArrayList<>();
        l.add(roleVoter());
        return new AffirmativeBased(l);
    }

    @Bean
    RunAsManagerImpl runAsManager()
    {
        RunAsManagerImpl runAsManager = new RunAsManagerImpl();
        runAsManager.setKey("my_run_as_password");
        List<AccessDecisionVoter<? extends Object>> l = new ArrayList<>();
        l.add(roleVoter());
        return runAsManager;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception
    {
        log.debug(">>> Debut methode configure()");
        // @formatter:off
        http
        .csrf()
            .disable()    
        .formLogin()
            .loginPage("/identification.ex")
            .loginProcessingUrl("/j_spring_security_check")
            .successHandler(authenticationSuccessHandler)
            .failureHandler(authenticationFailureHandler)
            .and()
        .logout()
            .logoutUrl("/j_spring_security_logout")
            // CF : ajout
            .logoutSuccessHandler(logoutSuccessHandler)
            .invalidateHttpSession(true)
            .deleteCookies("JSESSIONID")
            .and()
        .exceptionHandling().accessDeniedHandler(accessDeniedHandler)
            .and()
        .authorizeRequests()
                .antMatchers("/webjars/**", "/composants/**", "/application/**", "/", "/identification.ex", 
                    "/errors/**", "/j_appelportail").permitAll()
                .regexMatchers("\\A/accueil.ex.*").hasAnyAuthority("EXPLOITANT", "CHEFDEPROJETAPPLICATIF", "PARTICULIER",
                    "ADMINISTRATEUR", "0", "NOTAIRES")
                .antMatchers("/monitoring").hasAuthority("ADMINISTRATEUR")
                // .antMatchers("/accueil.ex").hasAnyAuthority("EXPLOITANT", "CHEFDEPROJETAPPLICATIF", "PARTICULIER",
                // "ADMINISTRATEUR", "0", "NOTAIRES")
                .regexMatchers(".*/flux.ex.*").authenticated()
                // Toutes les autres requêtes
                .anyRequest().denyAll();
        // @formatter:on

        // configurations supplémentaires (cumulatives) dans des implémentations de ServiceWebSecurityConfigurer
        // (ex: LombokCorsConfiguration)
        if (additionalWebSecurityConfigurer != null)
        {
            for (ServiceWebSecurityConfigurer config : additionalWebSecurityConfigurer)
            {
                config.configure(http);
            }
        }
    }

    @Override
    protected void configure(AuthenticationManagerBuilder authenticationManagerBuilder) throws Exception
    {
        authenticationManagerBuilder.authenticationProvider(authenticationAucunLdapProvider());
        authenticationManagerBuilder.authenticationProvider(authentificationRealProvider());
        authenticationManagerBuilder.authenticationProvider(authenticationLdapProvider());
    }

}
