#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.batch;

import ${package}.service.ClasseConceptAnalyse1Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import fr.gouv.finances.lombok.batch.ServiceBatchCommunImpl;

/** Batch exemple */
@Service("traitementcubatch1")
@Profile({"batch", "exemple"})
@Lazy(true)
public class TraitementCuBatch1Impl extends
        ServiceBatchCommunImpl {
    
    @Autowired()
    @Qualifier("classeconceptanalyse1serviceimpl")
    private ClasseConceptAnalyse1Service classeconceptanalyse1serviceso;

    public void traiterBatch() {
        classeconceptanalyse1serviceso.methodeAnalyse1();
    }

    public ClasseConceptAnalyse1Service getClasseconceptanalyse1serviceso() {
        return classeconceptanalyse1serviceso;
    }

    public void setClasseconceptanalyse1serviceso(
            ClasseConceptAnalyse1Service classeconceptanalyse1serviceso) {
        this.classeconceptanalyse1serviceso = classeconceptanalyse1serviceso;
    }
}
