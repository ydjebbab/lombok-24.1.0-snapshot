#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
/*
 * Copyright (c) 2020 DGFiP - Tous droits réservés
 */
package ${package};

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.jpa.JpaRepositoriesAutoConfiguration;
import org.springframework.boot.autoconfigure.integration.IntegrationAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.JdbcTemplateAutoConfiguration;
import org.springframework.boot.autoconfigure.jmx.JmxAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.boot.autoconfigure.web.MultipartAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * <pre>
 * Cette classe va démarrer l'application avec SPRING BOOT :
 * - soit en mode "TOMCAT EMBEDDED"
 * - soit en mode "TOMCAT SÉPARÉ"
 * </pre>
 */
@Configuration
@SpringBootApplication
@EnableAutoConfiguration(exclude = {DataSourceAutoConfiguration.class, DataSourceTransactionManagerAutoConfiguration.class,
        HibernateJpaAutoConfiguration.class, JpaRepositoriesAutoConfiguration.class, JmxAutoConfiguration.class,
        IntegrationAutoConfiguration.class, MultipartAutoConfiguration.class, JdbcTemplateAutoConfiguration.class
})
#if(${provider_persistance}=='jpa')
@ComponentScan(basePackages = {"fr.gouv.finances.lombok.config", "fr.gouv.finances.lombok.autoconfig", "fr.gouv.finances.lombok.jpa.config", "fr.gouv.finances.lombok.stubs","${package}.config", "${package}.controller"})
#end
#if(${provider_persistance}=='hibernate')
@ComponentScan(basePackages = {"fr.gouv.finances.lombok.config", "fr.gouv.finances.lombok.autoconfig", "fr.gouv.finances.lombok.stubs","${package}.config", "${package}.controller"})
#end
public class WebLauncher extends SpringBootServletInitializer
{

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     */
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder)
    {
        return WebLauncher.build(builder);
    }

    /**
     * Methode main méthode de démarrage en mode embedded.
     *
     * @param args arguments passés en paramètre (pour surcharger les propriétés du fichier application.properties par
     *        exemple)
     */
    public static void main(String[] args)
    {
        WebLauncher.build(new SpringApplicationBuilder()).run(args);
    }

    /**
     * methode Builds the : appelé en mode tomcat séparé.
     *
     * @param builder
     * @return spring application builder
     */
    private static SpringApplicationBuilder build(SpringApplicationBuilder builder)
    {
        return builder.sources(WebLauncher.class);
    }
}
