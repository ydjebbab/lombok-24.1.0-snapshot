#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
/*
 * Copyright (c) 2020 DGFiP - Tous droits réservés
 * 
 */
package ${package}.batch;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.batch.BatchAutoConfiguration;
import org.springframework.boot.autoconfigure.data.jpa.JpaRepositoriesAutoConfiguration;
import org.springframework.boot.autoconfigure.integration.IntegrationAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration;
import org.springframework.boot.autoconfigure.jmx.JmxAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Profile;

import fr.gouv.finances.lombok.config.LombokResourceConfiguration;
import fr.gouv.finances.lombok.config.LombokRootApplicationConfiguration;
import fr.gouv.finances.lombok.batch.AbstractBootBatchStarter;
import fr.gouv.finances.lombok.batch.config.LombokBatchResourcesConfiguration;
import fr.gouv.finances.lombok.jpa.config.PersistenceJPAConfig;

/**
 * BatchLauncher.java Classe de batch sans fichier xml et sans l'ancienne classe batchstarter
 * 
 * @author chouard Date: 18 déc. 2017
 */
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class, DataSourceTransactionManagerAutoConfiguration.class,
        HibernateJpaAutoConfiguration.class, JpaRepositoriesAutoConfiguration.class, JmxAutoConfiguration.class,
        IntegrationAutoConfiguration.class, BatchAutoConfiguration.class
})
@Profile("batch")
@Import({LombokRootApplicationConfiguration.class, LombokBatchResourcesConfiguration.class, LombokResourceConfiguration.class,
    PersistenceJPAConfig.class})
@ComponentScan(basePackages = {
        "${package}.batch",
        "fr.gouv.finances.lombok.adresse.batch", 
        "fr.gouv.finances.lombok.atlas.batch", 
        "fr.gouv.finances.lombok.bancaire.batch",
        "fr.gouv.finances.lombok.edition.batch",
        "fr.gouv.finances.lombok.journal.batch",
        "fr.gouv.finances.cp.srm.batch"})
public class BatchLauncher extends AbstractBootBatchStarter
{

    /**
     * Création du contexte du batch en ajoutant les profils communs aux batchs.
     *
     * @param args arguments du batch
     */
    public static void main(String[] args)
    {

        // va surcharger la variable spring.profiles.active avec les profiles pour les batchs si ils sont differents de
        // l'applicatif
        setActiveProfiles("$spring_profiles_active_batch");
        // construction du contexte avec ajout du profile batch
        SpringApplicationBuilder lombokBatchBuilder = new SpringApplicationBuilder(BatchLauncher.class)
            .profiles("batch")
            .web(false);
        // system.exit pour que l'appelant récupère les codes retour
        System.exit(SpringApplication.exit(lombokBatchBuilder.run(args)));

    }

}
