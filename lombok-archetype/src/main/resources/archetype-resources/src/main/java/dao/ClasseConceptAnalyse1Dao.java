#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.dao;

#if (${provider_persistance} == 'jpa')
import fr.gouv.finances.lombok.jpa.dao.BaseDaoJpa;
public interface ClasseConceptAnalyse1Dao extends BaseDaoJpa {
#else
import fr.gouv.finances.lombok.util.base.BaseDao;
public interface ClasseConceptAnalyse1Dao extends BaseDao{
#end

    void findEnsembleObjets1ParCritere1();
}
