#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )

package ${package}.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Classe pour la configuration de Swagger UI
 * 
 * @author celfer Date: 24 avr. 2019
 */
@ConditionalOnProperty(name="swagger.ui.active", havingValue="true")
@Configuration
@EnableSwagger2
public class SwaggerConfiguration
{    
    @Value("${swagger.api.title}")
    private String title;

    @Value("${swagger.api.description}")
    private String description;

    @Value("${swagger.api.urlprojet}")
    private String urlProjet;

    @Value("${swagger.api.version}")
    private String version;

    @Value("${swagger.api.controller.basepackage}")
    private String basePackage;
    
    @Value("${swagger.api.email}")
    private String email;
    
    
    @Bean
    public Docket productApi()
    {
        return new Docket(DocumentationType.SWAGGER_2)
            .select()
            // .apis(RequestHandlerSelectors.basePackage(basePackage))
            // .apis(RequestHandlerSelectors.basePackage("fr.gouv"))
            .apis(RequestHandlerSelectors.any())
            //.paths(regex("/.*"))
            .paths(PathSelectors.any())
            .build()
            .apiInfo(metaData());
    }

    private ApiInfo metaData()
    {
        return new ApiInfoBuilder()
            .title(title)
            .description(description)
            .version(version)
            .license("Apache License Version 2.0")
            .licenseUrl("https://www.apache.org/licenses/LICENSE-2.0\"")
            .contact(
                new Contact("Equipe Lombok", urlProjet, email))
            .build();
    }
}