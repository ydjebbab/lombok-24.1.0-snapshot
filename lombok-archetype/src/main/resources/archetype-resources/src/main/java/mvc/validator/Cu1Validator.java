#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.mvc.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import ${package}.mvc.form.Cu1Form;

/**TODO : Documenter (javadoc) */
public class Cu1Validator implements Validator {
	
	public boolean supports(Class clazz) {
		return clazz.equals(Cu1Form.class);
	}
	
   public void validate(Object command, Errors errors) {		
}
     
   /**
    * Methode de validation de surface : Règle de nommage debute par validateSurfaceNomdeVue
    * @param Cu1Validator description
    * @param errors description
    */
	public void validateSurfaceVue1(Cu1Form Cu1Validator, Errors errors) {
	}
}
