#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.mvc.webflow.action;

import org.springframework.webflow.execution.Event;
import org.springframework.webflow.execution.RequestContext;
import org.springframework.webflow.action.FormAction;
import ${package}.service.ClasseConceptAnalyse1Service;

/** TODO : Documenter (javadoc) */
public class Cu1FormAction extends FormAction {
	private ClasseConceptAnalyse1Service classeconceptanalyse1serviceso;
	public ClasseConceptAnalyse1Service getClasseconceptanalyse1serviceso() {
		return classeconceptanalyse1serviceso;
	}
	public void setClasseconceptanalyse1serviceso(
			ClasseConceptAnalyse1Service classeconceptanalyse1serviceso) {
		this.classeconceptanalyse1serviceso = classeconceptanalyse1serviceso;
	}
	public Event preparerMethodeAnalyse1(RequestContext context) {
		classeconceptanalyse1serviceso.methodeAnalyse1();
		return success();
	}
}
	