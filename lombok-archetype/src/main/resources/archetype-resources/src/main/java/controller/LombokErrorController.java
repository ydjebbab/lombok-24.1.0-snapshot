#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
/*
 * Copyright (c) 2020 DGFiP - Tous droits réservés
 */
package ${package}.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * <pre>
 * Classe gérant les codes retour HTTP. 
 * Remplace la gestion faite par tomcat pour qu'elle fonctionne en mode tomcat standalone.
 * Il faut créer un paragraphe error-page pointant vers /lombok-status-errors dans le web.xml de l'application 
 * (pour lombok c'est fait dans le fichier web-fragment du module lombo-webfragment).
 * </pre>
 */
@Controller
public class LombokErrorController
{

    /**
     * methode Render error page : méthode appelée pour la résolution des codes erreurs http. Il faut que la ressource
     * (/errors/erreurstd) soit atteignable par spring et qu'elle puisse être rendue (c'est une jsp).
     *
     * @param httpRequest description
     * @return model and view
     */
    @RequestMapping(value = "lombok-status-errors", method = RequestMethod.GET)
    public ModelAndView renderErrorPage(HttpServletRequest httpRequest)
    {

        ModelAndView errorPage = new ModelAndView("errors/erreurstd");
        String errorMsg = "";
        int httpErrorCode = getErrorCode(httpRequest);

        switch (httpErrorCode)
        {
            case 400:
            {
                errorMsg = "Http Error Code: 400. Bad Request";
                break;
            }
            case 401:
            {
                errorMsg = "Http Error Code: 401. Unauthorized";
                break;
            }
            case 404:
            {
                errorMsg = "Http Error Code: 404. Resource not found";
                break;
            }
            case 500:
            {
                errorMsg = "Http Error Code: 500. Internal Server Error";
                break;
            }
        }
        errorPage.addObject("exceptionmessage", errorMsg);
        return errorPage;
    }

    /**
     * Accesseur de l attribut error code.
     *
     * @param httpRequest DOCUMENTEZ_MOI
     * @return error code
     */
    private int getErrorCode(HttpServletRequest httpRequest)
    {
        return (Integer) httpRequest
            .getAttribute("javax.servlet.error.status_code");
    }

}
