#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.service.impl;


import java.util.HashSet;
import java.util.Set;

import ${package}.dao.ClasseConceptAnalyse1Dao;
import ${package}.service.ClasseConceptAnalyse1Service;
import fr.gouv.finances.lombok.journal.bean.ParametreOperation;
import fr.gouv.finances.lombok.util.base.BaseServiceImpl;


/** TODO : Documenter (javadoc) */
public class ClasseConceptAnalyse1ServiceImpl extends BaseServiceImpl implements ClasseConceptAnalyse1Service{
	private ClasseConceptAnalyse1Dao classeconceptanalyse1dao;


	public ClasseConceptAnalyse1Dao getClasseconceptanalyse1dao() {
		return classeconceptanalyse1dao;
	}

	public void setClasseconceptanalyse1dao(ClasseConceptAnalyse1Dao classeanalyse1dao) {
		this.classeconceptanalyse1dao = classeanalyse1dao;
	}

	public void methodeAnalyse1() {
		classeconceptanalyse1dao.findEnsembleObjets1ParCritere1();
		Set lesParamOper = new HashSet();
		ParametreOperation unParamOper = new ParametreOperation();
		unParamOper.setNom("Parametre Operation 1");
		unParamOper.setValeur("valeur");
		lesParamOper.add(unParamOper);

	}

}


