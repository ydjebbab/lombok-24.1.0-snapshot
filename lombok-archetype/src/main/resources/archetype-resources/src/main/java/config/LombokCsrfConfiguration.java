#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )

package ${package}.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.web.csrf.CookieCsrfTokenRepository;

import fr.gouv.finances.lombok.config.ServiceWebSecurityConfigurer;

@Configuration
@ConditionalOnProperty(value="csrfcookie.enabled", havingValue = "true")
public class LombokCsrfConfiguration implements ServiceWebSecurityConfigurer
{
    @Override
    public void configure(HttpSecurity http) throws Exception
    {
        http.csrf().csrfTokenRepository(CookieCsrfTokenRepository.withHttpOnlyFalse());
    }
}
