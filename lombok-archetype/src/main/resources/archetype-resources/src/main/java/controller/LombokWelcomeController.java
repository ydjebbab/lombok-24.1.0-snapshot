#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
/*
 * Copyright (c) 2020 DGFiP - Tous droits réservés
 */
package ${package}.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Class LombokWelcomeController Classe redirigeant les appels sur la racine de la servlet dispatcher (/) vers la page
 * la vue accueil.ex qui est résolue en accueil.jsp (voir controleur AccueilController).
 */
@Controller
public class LombokWelcomeController
{

    /**
     * methode Welcome : redirection vers la page d'accueil.
     *
     * @param model le model
     * @return string
     */
    @RequestMapping("/")
    public String welcome(Model model)
    {
        return "redirect:/accueil.ex";
    }

}
