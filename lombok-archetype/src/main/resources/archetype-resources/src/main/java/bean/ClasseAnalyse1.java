#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.bean;

#if(${provider_persistance}=='jpa')
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;
#end

import fr.gouv.finances.lombok.util.base.BaseBean;

#if(${provider_persistance}=='jpa')
@Table(name = "CLASSEANALYSE1")
@Entity
#end
public class ClasseAnalyse1 extends BaseBean
{
    private static final long serialVersionUID = 1L;

    #if(${provider_persistance}=='jpa')
    @Id
    @Column(name = "CLASSEANALYSE1_ID")
    #end
    private Long id;

    #if(${provider_persistance}=='jpa')
    @Column(name = "IDENTIFIANTMETIER")
    #end
    private Long identifiantMetier;

    #if(${provider_persistance}=='jpa')
    @Column(name = "VERSION")
    @Version
    #end
    private int version;

    public int getVersion()
    {
        return version;
    }

    public void setVersion(int version)
    {
        this.version = version;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getIdentifiantMetier()
    {
        return identifiantMetier;
    }

    public void setIdentifiantMetier(Long identifiantMetier)
    {
        this.identifiantMetier = identifiantMetier;
    }

    public boolean equals(Object object)
    {
        if (this == object)
            return true;

        if (object == null || (object.getClass() != this.getClass()))
            return false;

        ClasseAnalyse1 myObj = (ClasseAnalyse1) object;

        return ((identifiantMetier == null ? myObj.getIdentifiantMetier() == null
            : identifiantMetier.equals(myObj.getIdentifiantMetier())));
    }

    public int hashCode()
    {
        int hash = 7;
        int varCode = 0;
        varCode = (null == identifiantMetier ? 0 : identifiantMetier.hashCode());
        hash = 31 * hash + varCode;
        return hash;
    }
}
