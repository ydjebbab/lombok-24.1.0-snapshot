#!/bin/bash

# Fonction de description des parametres
fonct_usage ()
{
  echo "Usage: $0 version_a_livrer" 1>&2
  echo "Exemple: $0 1.2.3" 1>&2
}

# Nombre d'arguments
if [ $# -ne 1 ]
then
  fonct_usage
  exit 1
fi

rm -fr livrables_intex
mvn -Plivraison_intex -Drelease=${1} clean verify ${EXTRA_ARGS}
