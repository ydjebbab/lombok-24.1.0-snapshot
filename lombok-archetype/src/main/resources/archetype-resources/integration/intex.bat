@echo off

if "%1" == "" GOTO Err

SET REPERTOIRE_COURANT=%cd%

if exist livrables_intex rmdir /s /q livrables_intex
call mvn -Plivraison_intex -Drelease=%1 clean verify %EXTRA_ARGS%
cd %REPERTOIRE_COURANT%

GOTO Fin

:Err
echo Syntaxe: %0 version_a_livrer
echo Exemple: %0 1.2.3
:Fin
