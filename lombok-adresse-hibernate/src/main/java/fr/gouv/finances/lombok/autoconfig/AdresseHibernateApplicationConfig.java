package fr.gouv.finances.lombok.autoconfig;

import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

/**
 * Classe de configuration qui permet de conditionner les ressources à importer
 * @author celfer
 * Date: 18 févr. 2020
 */
@Configuration
@ConditionalOnExpression("'${lombok.orm.jpa}' == 'false'")
@ImportResource({"classpath:conf/applicationContext-adresse-dao.xml"})
public class AdresseHibernateApplicationConfig
{

}
