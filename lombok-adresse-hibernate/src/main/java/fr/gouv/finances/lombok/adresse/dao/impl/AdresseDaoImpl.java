/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 *
 */
package fr.gouv.finances.lombok.adresse.dao.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.FetchMode;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.DistinctRootEntityResultTransformer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.gouv.finances.lombok.adresse.bean.CodePostal;
import fr.gouv.finances.lombok.adresse.bean.Commune;
import fr.gouv.finances.lombok.adresse.bean.Departement;
import fr.gouv.finances.lombok.adresse.bean.PaysInsee;
import fr.gouv.finances.lombok.adresse.bean.PaysIso;
import fr.gouv.finances.lombok.adresse.bean.Region;
import fr.gouv.finances.lombok.adresse.dao.AdresseDao;
import fr.gouv.finances.lombok.adresse.dao.ProprieteAdresse;
import fr.gouv.finances.lombok.util.base.BaseDaoImpl;
import fr.gouv.finances.lombok.util.exception.ProgrammationException;

/**
 * Implémentation Hibernate du DAO de gestion des adresses.
 *
 * @author amleplatinec
 * @author CBRE
 * @author CF
 */
public class AdresseDaoImpl extends BaseDaoImpl implements AdresseDao
{
    protected static final Logger LOG = LoggerFactory.getLogger(AdresseDaoImpl.class);

    public AdresseDaoImpl()
    {
        super();
    }

    /**
     * {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.adresse.dao.AdresseDao#checkExistCodePostal(java.lang.String)
     */
    @Override
    public boolean checkExistCodePostal(String codePostal)
    {
        // Journalisation de la recherche
        LOG.debug("Recherche de l'existence du code postal '{}'", codePostal);

        // Si aucun paramètre n'est fourni
        if (StringUtils.isEmpty(codePostal))
        {
            return false;
        }

        // Exécution de la requête et détermination s'il y a au moins un résultat
        return (Long) getHibernateTemplate().execute(
            session ->
            // Paramétrage de la recherche sur le code postal
            session.createCriteria(CodePostal.class)
                // Initialisation des critères
                .add(Restrictions.ilike(ProprieteAdresse.CODE_POSTAL.getNom(), codePostal.trim()))
                // Recherche du nombre d'élément satisfaisant les critères
                .setProjection(Projections.rowCount())
                // Un seul résultat est attendu
                .uniqueResult()) > 0;
    }

    /**
     * {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.adresse.dao.AdresseDao#checkExistCodePostalXVille(java.lang.String,
     *      java.lang.String)
     */
    @Override
    public boolean checkExistCodePostalXVille(String codePostal, String ville)
    {
        // Journalisation de la recherche
        LOG.debug("Recherche de l'existence du code postal '{}' associé à la ville '{}'", codePostal, ville);

        // Si un paramètre n'est pas fourni
        if (StringUtils.isEmpty(codePostal) || StringUtils.isEmpty(ville))
        {
            return false;
        }

        // Exécution de la requête et détermination s'il y a au moins un résultat
        return (Long) getHibernateTemplate().execute(
            session ->
            // Paramétrage de la recherche sur le code postal
            session.createCriteria(CodePostal.class)
                // Initialisation des critères
                .add(Restrictions.ilike(ProprieteAdresse.CODE_POSTAL.getNom(), codePostal.trim(), MatchMode.START))
                .add(Restrictions.ilike(ProprieteAdresse.VILLE.getNom(), ville.trim(), MatchMode.START))
                // Recherche du nombre d'élément satisfaisant les critères
                .setProjection(Projections.rowCount())
                // Un seul résultat est attendu
                .uniqueResult()) > 0;
    }

    /**
     * {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.adresse.dao.AdresseDao#deleteTousLesCodesPostaux()
     */
    @Override
    public void deleteTousLesCodesPostaux()
    {
        // Journalisation de la mise à jour
        LOG.trace("Suppression de tous les codes postaux");

        // Suppression ensembliste des codes postaux
        getHibernateTemplate().bulkUpdate("delete " + CodePostal.class.getSimpleName());
    }

    /**
     * {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.adresse.dao.AdresseDao#findCodePostaux()
     */
    @Override
    @SuppressWarnings("unchecked")
    public List<CodePostal> findCodePostaux()
    {
        // Journalisation de la recherche
        LOG.trace("Recherche de tous les codes postaux");

        // La requête s'effectuera sur l'objet lié au code postal
        DetachedCriteria detachedCriteria = DetachedCriteria.forClass(CodePostal.class);

        // Ajout d'un critère sur le code postal
        detachedCriteria.addOrder(Order.asc(ProprieteAdresse.CODE_POSTAL.getNom()));

        // Exécution de la requête et récupération des résultats
        return (List<CodePostal>) getHibernateTemplate().findByCriteria(detachedCriteria);
    }

    /**
     * {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.adresse.dao.AdresseDao#findCodesPostauxParCode(java.lang.String)
     */
    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.adresse.dao.AdresseDao#findCodesPostauxParCode(java.lang.String)
     */
    @Override
    @SuppressWarnings("unchecked")
    public List<CodePostal> findCodesPostauxParCode(String codePostal)
    {
        // Journalisation de la recherche
        LOG.debug("Recherche des codes postaux '{}'", codePostal);

        // La requête s'effectuera sur l'objet lié au code postal
        DetachedCriteria detachedCriteria = DetachedCriteria.forClass(CodePostal.class);

        // Si le paramètre n'est pas renseigné, la recherche n'est pas exécutée
        if (StringUtils.isEmpty(codePostal))
        {
            return new ArrayList<>();
        }

        // Ajout d'un critère sur le code postal
        detachedCriteria.add(Restrictions.ilike(ProprieteAdresse.CODE_POSTAL.getNom(), codePostal.trim()));
        // Ajout d'un tri sur le libellé de la ville
        detachedCriteria.addOrder(Order.asc(ProprieteAdresse.VILLE.getNom()));

        // Exécution de la requête et récupération des résultats
        return (List<CodePostal>) getHibernateTemplate().findByCriteria(detachedCriteria);
    }

    /**
     * {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.adresse.dao.AdresseDao#findCodesPostauxParCodeEtOuVille(java.lang.String,
     *      java.lang.String)
     */
    @Override
    @SuppressWarnings("unchecked")
    public List<CodePostal> findCodesPostauxParCodeEtOuVille(String codePostal, String ville)
    {
        // Journalisation de la recherche
        LOG.debug("Recherche des codes postaux '{}' liés à la ville '{}'", codePostal, ville);

        // Si les paramètres ne sont pas renseignés, la recherche n'est pas exécutée
        if (StringUtils.isEmpty(codePostal) && StringUtils.isEmpty(ville))
        {
            return new ArrayList<>();
        }

        // La requête s'effectuera sur l'objet lié au code postal
        DetachedCriteria detachedCriteria = DetachedCriteria.forClass(CodePostal.class);

        // Ajout d'un critère sur le code postal s'il est renseigné
        if (!StringUtils.isEmpty(codePostal))
        {
            detachedCriteria.add(Restrictions.ilike(ProprieteAdresse.CODE_POSTAL.getNom(), codePostal.trim(), MatchMode.START));
        }

        // Ajout d'un critère sur la ville s'il est renseigné
        if (!StringUtils.isEmpty(ville))
        {
            detachedCriteria.add(Restrictions.ilike(ProprieteAdresse.VILLE.getNom(), ville.trim(), MatchMode.START));
        }

        // Exécution de la requête et récupération des résultats
        return (List<CodePostal>) getHibernateTemplate().findByCriteria(detachedCriteria);
    }

    /**
     * {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.adresse.dao.AdresseDao#findCodesPostauxParVille(java.lang.String)
     */
    @Override
    @SuppressWarnings("unchecked")
    public List<CodePostal> findCodesPostauxParVille(String ville)
    {
        // Journalisation de la recherche
        LOG.debug("Recherche des codes postaux liés à la ville '{}'", ville);

        // Si le paramètre n'est pas renseigné, la recherche n'est pas exécutée
        if (StringUtils.isEmpty(ville))
        {
            return new ArrayList<>();
        }

        // La requête s'effectuera sur l'objet lié au code postal
        DetachedCriteria detachedCriteria = DetachedCriteria.forClass(CodePostal.class);

        // Ajout d'un critère sur la ville
        detachedCriteria.add(Restrictions.ilike(ProprieteAdresse.VILLE.getNom(), ville.trim()));
        // Ajout d'un tri sur le libellé de la ville
        detachedCriteria.addOrder(Order.asc(ProprieteAdresse.VILLE.getNom()));

        // Exécution de la requête et récupération des résultats
        return (List<CodePostal>) getHibernateTemplate().findByCriteria(detachedCriteria);
    }

    /**
     * {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.adresse.dao.AdresseDao#findTousLesDepartementsAvecRegion()
     */
    @Override
    @SuppressWarnings("unchecked")
    public List<Departement> findTousLesDepartementsAvecRegion()
    {
        // Journalisation de la recherche
        LOG.trace("Recherche de tous les départements, avec les régions associées");

        // La requête s'effectuera sur l'objet lié au département
        DetachedCriteria detachedCriteria = DetachedCriteria.forClass(Departement.class);

        // Ajout d'une jointure sur la région
        detachedCriteria.setFetchMode(ProprieteAdresse.REGION.getNom(), FetchMode.JOIN);
        // Ajout d'une jointure sur les département de la région
        detachedCriteria.setFetchMode(ProprieteAdresse.REGION.getNom()
            + "." + ProprieteAdresse.DEPARTEMENTS.getNom(), FetchMode.JOIN);

        // Ajout d'un tri sur le code INSEE du département
        detachedCriteria.addOrder(Order.asc(ProprieteAdresse.CODE_DEPARTEMENT.getNom()));
        // Seuls des résultats distinct sont requis
        detachedCriteria.setResultTransformer(DistinctRootEntityResultTransformer.INSTANCE);

        // Exécution de la requête et récupération des résultats
        return (List<Departement>) getHibernateTemplate().findByCriteria(detachedCriteria);
    }

    /**
     * {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.adresse.dao.AdresseDao#findTousLesPaysInsee()
     */
    @Override
    @SuppressWarnings("unchecked")
    public List<PaysInsee> findTousLesPaysInsee()
    {
        // Journalisation de la recherche
        LOG.trace("Recherche de tous les pays selon la codification INSEE");

        // La requête s'effectuera sur l'objet lié au pays, selon la codification INSEE
        DetachedCriteria criteria = DetachedCriteria.forClass(PaysInsee.class);

        // Ajout d'un tri sur le libellé du nom du pays
        criteria.addOrder(Order.asc(ProprieteAdresse.NOM_PAYS.getNom()));
        // Seuls des résultats distinct sont requis
        criteria.setResultTransformer(DistinctRootEntityResultTransformer.INSTANCE);

        // Exécution de la requête et récupération des résultats
        return (List<PaysInsee>) getHibernateTemplate().findByCriteria(criteria);
    }

    /**
     * {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.adresse.dao.AdresseDao#findTousLesPaysIso()
     */
    @Override
    @SuppressWarnings("unchecked")
    public List<PaysIso> findTousLesPaysIso()
    {
        // Journalisation de la recherche
        LOG.trace("Recherche de tous les pays selon la codification ISO");

        // La requête s'effectuera sur l'objet lié au pays, selon la codification ISO
        DetachedCriteria detachedCriteria = DetachedCriteria.forClass(PaysIso.class);

        // Ajout d'un tri sur le libellé du nom du pays
        detachedCriteria.addOrder(Order.asc(ProprieteAdresse.NOM_PAYS.getNom()));
        // Seuls des résultats distinct sont requis
        detachedCriteria.setResultTransformer(DistinctRootEntityResultTransformer.INSTANCE);

        // Exécution de la requête et récupération des résultats
        return (List<PaysIso>) getHibernateTemplate().findByCriteria(detachedCriteria);
    }

    /**
     * {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.adresse.dao.AdresseDao#findToutesLesRegionsAvecDepartements()
     */
    @Override
    @SuppressWarnings("unchecked")
    public List<Region> findToutesLesRegionsAvecDepartements()
    {
        // Journalisation de la recherche
        LOG.trace("Recherche de toutes les régions, et les départements associés");

        // La requête s'effectuera sur l'objet lié à la région
        DetachedCriteria detachedCriteria = DetachedCriteria.forClass(Region.class);

        // Ajout d'une jointure sur les départements
        detachedCriteria.setFetchMode(ProprieteAdresse.DEPARTEMENTS.getNom(), FetchMode.JOIN);
        // Ajout d'un tri sur le libellé de la région
        detachedCriteria.addOrder(Order.asc(ProprieteAdresse.LIBELLE_REGION.getNom()));
        // Seuls des résultats distinct sont requis
        detachedCriteria.setResultTransformer(DistinctRootEntityResultTransformer.INSTANCE);

        // Exécution de la requête et récupération des résultats
        return (List<Region>) getHibernateTemplate().findByCriteria(detachedCriteria);
    }

    /**
     * {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.adresse.dao.AdresseDao#findUnDepartementParCodeDepartementINSEE(java.lang.String)
     */
    @Override
    @SuppressWarnings("unchecked")
    public Departement findUnDepartementParCodeDepartementINSEE(String codedepartementINSEE)
    {
        // Journalisation de la recherche
        LOG.trace("Recherche du département de code INSEE '{}'", codedepartementINSEE);

        // Contrôle de la présence paramètre
        if (StringUtils.isEmpty(codedepartementINSEE))
        {
            throw new ProgrammationException("Le code du département n'a pas été fourni pour la recherche");
        }

        // La requête s'effectuera sur l'objet lié au département, avec ajout d'un critère sur le
        // code du département
        DetachedCriteria detachedCriteria =
            DetachedCriteria.forClass(Departement.class).add(
                Restrictions.eq(ProprieteAdresse.CODE_DEPARTEMENT.getNom(), codedepartementINSEE));

        // Exécution de la requête et récupération des résultats
        List<Departement> resultat = (List<Departement>) getHibernateTemplate().findByCriteria(detachedCriteria);

        // En cas de résultats multiples, récupération du 1er
        if (!resultat.isEmpty())
        {
            return resultat.get(0);
        }

        // Si pas de résultat
        return null;
    }

    /**
     * {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.adresse.dao.AdresseDao#findUnDepartementParCodeDepartementINSEEAvecRegion(java.lang.String)
     */
    @Override
    public Departement findUnDepartementParCodeDepartementINSEEAvecRegion(String codedepartementINSEE)
    {
        // Journalisation de la recherche
        LOG.trace("Recherche du département de code INSEE '{}', avec la région associée", codedepartementINSEE);

        return getDepartementParCodeDepartementINSEEAvecRegionEtCommunes(codedepartementINSEE, false);
    }

    /**
     * {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.adresse.dao.AdresseDao#findUnDepartementParCodeDepartementINSEEAvecRegionEtCommunes(java.lang.String)
     */
    @Override
    public Departement findUnDepartementParCodeDepartementINSEEAvecRegionEtCommunes(String codedepartementINSEE)
    {
        // Journalisation de la recherche
        LOG.trace("Recherche du département de code INSEE '{}', avec la région et la commune associées", codedepartementINSEE);

        return getDepartementParCodeDepartementINSEEAvecRegionEtCommunes(codedepartementINSEE, true);
    }

    /**
     * Recherche un département par code département INSEE.<br/>
     * La propriété laRegion est renseignée.<br/>
     * La propriété lesCommunes est renseignée selon la valeur booléenne du 2ème paramètre.
     *
     * @param codedepartementINSEE code du département INSEE
     * @param isRechercheComune les communes du département doivent-elles être chargées ?
     * @return le département correspondant
     */
    private Departement getDepartementParCodeDepartementINSEEAvecRegionEtCommunes(
        String codedepartementINSEE, boolean isRechercheComune)
    {
        // Journalisation de la recherche
        LOG.trace("Recherche du département de code INSEE '{}', avec la région et éventuellement la commune associées ({})",
            codedepartementINSEE, isRechercheComune);

        // Contrôle de la présence paramètre
        if (StringUtils.isEmpty(codedepartementINSEE))
        {
            throw new ProgrammationException("Le code du département n'a pas été fourni pour la recherche");
        }

        // La requête s'effectuera sur l'objet lié au département, avec ajout d'un critère sur le
        // code INSEE du département
        DetachedCriteria detachedCriteria =
            DetachedCriteria.forClass(Departement.class).add(
                Restrictions.eq(ProprieteAdresse.CODE_DEPARTEMENT.getNom(), codedepartementINSEE));

        // Ajout d'une jointure sur la région
        detachedCriteria.setFetchMode(ProprieteAdresse.REGION.getNom(), FetchMode.JOIN);
        // Ajout d'une jointure sur les département de la région
        detachedCriteria.setFetchMode(ProprieteAdresse.REGION.getNom() + "." + ProprieteAdresse.DEPARTEMENTS.getNom(), FetchMode.JOIN);

        // Si les communes sont requises, ajout d'une jointure sur les communes
        if (isRechercheComune)
        {
            detachedCriteria.setFetchMode(ProprieteAdresse.COMMUNES.getNom(), FetchMode.JOIN);
        }
        // Seuls des résultats distinct sont requis
        detachedCriteria.setResultTransformer(DistinctRootEntityResultTransformer.INSTANCE);

        // Exécution de la requête et récupération des résultats
        @SuppressWarnings("unchecked")
        List<Departement> resultat = (List<Departement>) getHibernateTemplate().findByCriteria(detachedCriteria);

        // En cas de résultats multiples, récupération du 1er
        if (!resultat.isEmpty())
        {
            return resultat.get(0);
        }

        // Si pas de résultat
        return null;
    }

    /**
     * {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.adresse.dao.AdresseDao#findUneCommuneParCodeCommuneINSEE(java.lang.String)
     */
    @Override
    public Commune findUneCommuneParCodeCommuneINSEE(String codecommuneINSEE)
    {
        // Journalisation de la recherche
        LOG.trace("Recherche de la commune de code INSEE '{}'", codecommuneINSEE);

        // Contrôle de la présence paramètre
        if (StringUtils.isEmpty(codecommuneINSEE))
        {
            throw new ProgrammationException("Le code commune n'a pas été fourni pour la recherche");
        }

        // La requête s'effectuera sur l'objet lié à la commune, avec ajout d'un critère sur le
        // code INSEE de la commune
        DetachedCriteria detachedCriteria =
            DetachedCriteria.forClass(Commune.class).add(
                Restrictions.eq(ProprieteAdresse.CODE_COMMUNE.getNom(), codecommuneINSEE));

        // Ajout d'une jointure sur les départements
        detachedCriteria.setFetchMode(ProprieteAdresse.DEPARTEMENT.getNom(), FetchMode.JOIN);
        // Ajout d'une jointure sur les régions du département
        detachedCriteria.setFetchMode(ProprieteAdresse.DEPARTEMENT.getNom() + "." + ProprieteAdresse.REGION.getNom(), FetchMode.JOIN);

        // Exécution de la requête et récupération des résultats
        @SuppressWarnings("unchecked")
        Iterator<Commune> resultat = ((List<Commune>) getHibernateTemplate().findByCriteria(detachedCriteria)).iterator();

        // En cas de résultats multiples, récupération du 1er
        if (resultat.hasNext())
        {
            return resultat.next();
        }

        // Si pas de résultat
        return null;
    }

    /**
     * {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.adresse.dao.AdresseDao#findUneRegionParCodeRegionINSEE(java.lang.String)
     */
    @Override
    public Region findUneRegionParCodeRegionINSEE(String coderegionINSEE)
    {
        // Journalisation de la recherche
        LOG.trace("Recherche de la région de code INSEE '{}'", coderegionINSEE);

        // Contrôle de la présence paramètre
        if (StringUtils.isEmpty(coderegionINSEE))
        {
            throw new ProgrammationException("Le code de la région n'a pas été fourni pour la recherche");
        }

        // La requête s'effectuera sur l'objet lié à la région, avec ajout d'un critère sur le
        // code INSEE de la région
        DetachedCriteria detachedCriteria =
            DetachedCriteria.forClass(Region.class).add(
                Restrictions.eq(ProprieteAdresse.CODE_REGION.getNom(), coderegionINSEE));

        // Exécution de la requête et récupération des résultats
        @SuppressWarnings("unchecked")
        Iterator<Region> resultat = ((List<Region>) getHibernateTemplate().findByCriteria(detachedCriteria))
            .iterator();

        // En cas de résultats multiples, récupération du 1er
        if (resultat.hasNext())
        {
            return resultat.next();
        }

        // Si pas de résultat
        return null;
    }

    /**
     * {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.adresse.dao.AdresseDao#findUnPaysInseeParCodePaysInsee(java.lang.String)
     */
    @Override
    public PaysInsee findUnPaysInseeParCodePaysInsee(String codePaysInsee)
    {
        // Journalisation de la recherche
        LOG.trace("Recherche du pays de code INSEE '{}'", codePaysInsee);

        // Contrôle de la présence paramètre
        if (StringUtils.isEmpty(codePaysInsee))
        {
            throw new ProgrammationException("Le code INSEE du pays n'a pas été fourni pour la recherche");
        }

        // La requête s'effectuera sur l'objet lié au pays selon la codification INSEE, avec
        // ajout d'un critère sur le code INSEE du pays
        DetachedCriteria detachedCriteria =
            DetachedCriteria.forClass(PaysInsee.class)
                .add(Restrictions.eq(ProprieteAdresse.CODE_PAYS_INSEE.getNom(), codePaysInsee));

        // Exécution de la requête et récupération des résultats
        @SuppressWarnings("unchecked")
        Iterator<PaysInsee> resultat = ((List<PaysInsee>) getHibernateTemplate().findByCriteria(detachedCriteria))
            .iterator();

        // En cas de résultats multiples, récupération du 1er
        if (resultat.hasNext())
        {
            return resultat.next();
        }

        // Si pas de résultat
        return null;
    }

    /**
     * {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.adresse.dao.AdresseDao#findUnPaysIsoParCodePaysIso(java.lang.String)
     */
    @Override
    public PaysIso findUnPaysIsoParCodePaysIso(String codeIsoAlpha2)
    {
        // Journalisation de la recherche
        LOG.trace("Recherche du pays de code ISO '{}'", codeIsoAlpha2);

        // Contrôle de la présence paramètre
        if (StringUtils.isEmpty(codeIsoAlpha2))
        {
            throw new ProgrammationException("Le code ISO du pays n'a pas été fourni pour la recherche");
        }

        // La requête s'effectuera sur l'objet lié au pays selon la codification ISO, avec
        // ajout d'un critère sur le code ISO alpha 2 du pays
        DetachedCriteria detachedCriteria =
            DetachedCriteria.forClass(PaysIso.class).add(Restrictions.eq(ProprieteAdresse.CODE_PAYS_ISO.getNom(), codeIsoAlpha2));

        // Exécution de la requête et récupération des résultats
        @SuppressWarnings("unchecked")
        Iterator<PaysIso> resultat = ((List<PaysIso>) getHibernateTemplate().findByCriteria(detachedCriteria))
            .iterator();

        // En cas de résultats multiples, récupération du 1er
        if (resultat.hasNext())
        {
            return resultat.next();
        }

        // Si pas de résultat
        return null;
    }

    /**
     * {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.adresse.dao.AdresseDao#testExistenceCommune(java.lang.String)
     */
    @Override
    public boolean testExistenceCommune(String codeCommuneINSEE)
    {
        // Journalisation de la recherche
        LOG.debug("Recherche de l'existence de la commune de code INSEE '{}'", codeCommuneINSEE);

        // Si le paramètre n'est pas renseigné, la recherche n'est pas exécutée
        if (StringUtils.isEmpty(codeCommuneINSEE))
        {
            return false;
        }

        // Exécution de la requête et détermination s'il y a au moins un résultat
        return (Long) getHibernateTemplate().execute(
            session ->
            // Paramétrage de la recherche
            session.createCriteria(Commune.class)
                // Initialisation des critères
                .add(Restrictions.eq(ProprieteAdresse.CODE_COMMUNE.getNom(), codeCommuneINSEE.trim()).ignoreCase())
                // Recherche du nombre d'élément satisfaisant les critères
                .setProjection(Projections.rowCount())
                // Un seul résultat est attendu
                .uniqueResult()) > 0;
    }

    /**
     * {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.adresse.dao.AdresseDao#testExistenceDepartement(java.lang.String)
     */
    @Override
    public boolean testExistenceDepartement(String codeDepartement)
    {
        // Journalisation de la recherche
        LOG.debug("Recherche de l'existence d'un département de code '{}'", codeDepartement);

        // Si le paramètre n'est pas renseigné, la recherche n'est pas exécutée
        if (StringUtils.isEmpty(codeDepartement))
        {
            return false;
        }

        // Exécution de la requête et détermination s'il y a au moins un résultat
        return (Long) getHibernateTemplate().execute(
            session ->
            // Paramétrage de la recherche
            session.createCriteria(Departement.class)
                // Initialisation des critères
                .add(Restrictions.ilike(ProprieteAdresse.CODE_DEPARTEMENT.getNom(), codeDepartement.trim()))
                // Recherche du nombre d'élément satisfaisant les critères
                .setProjection(Projections.rowCount())
                // Un seul résultat est attendu
                .uniqueResult()) > 0;
    }

    /**
     * {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.adresse.dao.AdresseDao#testExistenceRegion(java.lang.String)
     */
    @Override
    public boolean testExistenceRegion(String codeRegion)
    {
        // Journalisation de la recherche
        LOG.debug("Recherche de l'existence d'une région de code INSEE '{}'", codeRegion);

        // Si le paramètre n'est pas renseigné, la recherche n'est pas exécutée
        if (StringUtils.isEmpty(codeRegion))
        {
            return false;
        }

        // Exécution de la requête et détermination s'il y a au moins un résultat
        return (Long) getHibernateTemplate().execute(
            session ->
            // Paramétrage de la recherche
            session.createCriteria(Region.class)
                // Initialisation des critères
                .add(Restrictions.ilike(ProprieteAdresse.CODE_REGION.getNom(), codeRegion.trim()))
                // Recherche du nombre d'élément satisfaisant les critères
                .setProjection(Projections.rowCount())
                // Un seul résultat est attendu
                .uniqueResult()) > 0;
    }

    /**
     * pour autocompletion communes findLesCommunesParDebutLibelleCommune
     *
     * @param nomcommune
     * @return Liste des communes
     */
    @Override
    public List<String> findLesCommunesParDebutLibelleCommune(String nomcommune)
    {
        LOG.debug(">>> Debut findLesCommunesParDebutLibelleCommune");

        Commune uneCommune;
        List<String> lesCommunes = new ArrayList<>();
        DetachedCriteria detachedCriteria = DetachedCriteria.forClass(Commune.class).add(Restrictions.like("libellecommune",
            nomcommune.trim() + "%").ignoreCase());
        Iterator resultat = getHibernateTemplate().findByCriteria(detachedCriteria).iterator();
        while (resultat.hasNext())
        {
            uneCommune =
                (Commune) resultat.next();
            if (uneCommune != null)
            {
                lesCommunes.add(uneCommune.getLibellecommune());
            }
        }
        return lesCommunes;
    }

    /**
     * pour autocompletion pays
     *
     * @param pays
     * @return Liste des pays
     */
    @Override
    public List<String> findLesPaysParDebutLibellePays(String pays)
    {
        LOG.debug(">>> Debut findLesPaysParDebutLibellePays");
        PaysIso unPays;
        List<String> lesPays = new ArrayList<>();
        DetachedCriteria detachedCriteria =
            DetachedCriteria.forClass(PaysIso.class).add(Restrictions.like("nom", pays.trim() + "%").ignoreCase());
        Iterator resultat = getHibernateTemplate().findByCriteria(detachedCriteria).iterator();
        while (resultat.hasNext())
        {
            unPays =
                (PaysIso) resultat.next();
            if (unPays != null)
            {
                lesPays.add(unPays.getNom());
            }
        }
        return lesPays;
    }
}
