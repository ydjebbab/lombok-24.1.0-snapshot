package fr.gouv.finances.lombok.jpa.util;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;
import javax.persistence.Version;

/**
 * Base d'une entité
 * 
 * @author celfer Date: 27 janv. 2020
 */
@MappedSuperclass
public abstract class BaseEntity implements Serializable
{
    private static final long serialVersionUID = 154542188L;

    @Version
    @Column(name = "VERSION")
    private int version;

    @Transient
    protected final int hashCODEHASHINIT = 7;

    @Transient
    protected final int hashCODEHASHMULT = 31;

    public BaseEntity()
    {
        super();
    }

    public Integer getVersion()
    {
        return version;
    }

    public void setVersion(Integer version)
    {
        this.version = version;
    }

}
