/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 *
 */
package fr.gouv.finances.lombok.jpa.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.LockModeType;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang.StringUtils;
import org.hibernate.ScrollMode;
import org.hibernate.ScrollableResults;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.orm.ObjectRetrievalFailureException;

import fr.gouv.finances.lombok.jpa.util.ScrollIteratorImpl;
import fr.gouv.finances.lombok.util.exception.ProgrammationException;
import fr.gouv.finances.lombok.util.persistance.ModeCritereRecherche;
import fr.gouv.finances.lombok.util.persistance.ScrollIterator;

/**
 * Implémentation du socle des DAO JPA.
 *
 * @author Christophe Breheret-Girardin
 * @author Celinio Fernandes
 */
public class BaseDaoJpaImpl implements BaseDaoJpa
{
    protected static final Logger LOGGER = LoggerFactory.getLogger(BaseDaoJpaImpl.class);

    /** Gestionnaire de persistance */
    //@PersistenceContext(name="entityManagerJPA")
    @PersistenceContext(unitName = "pu-principal", name="entityManager")
    protected EntityManager entityManager;

    /**
     * {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.util.base.CoreBaseDao#clearSession().
     */
    @Override
    public void clearSession()
    {
        entityManager.clear();
    }

    /**
     * {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.util.base.CoreBaseDao#clearPersistenceContext().
     */
    @Override
    public void clearPersistenceContext()
    {
        clearSession();
    }

    /**
     * {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.util.base.CoreBaseDao#deleteObject(java.lang.Class, java.io.Serializable).
     */
    @Override
    public <T> void deleteObject(Class<T> clazz, Serializable identifiant)
    {
        deleteObject(getObject(clazz, identifiant));
    }

    /**
     * {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.util.base.CoreBaseDao#deleteObject(java.lang.Object).
     */
    @Override
    public <T> void deleteObject(T entite)
    {
        LOGGER.debug(">>> Debut methode deleteObject");
        // Si l'entité n'est pas chargée dans le contexte, récupération de celle-ci
        if (!entityManager.contains(entite))
        {
            entite = entityManager.merge(entite);
        }

        // Suppression de l'entité chargée
        entityManager.remove(entite);
    }

    /**
     * {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.util.base.CoreBaseDao#evictObject(java.lang.Object).
     */
    @Override
    public <T> void evictObject(T entite)
    {
        entityManager.detach(entite);
    }

    /**
     * {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.util.base.CoreBaseDao#flush().
     */
    @Override
    public void flush()
    {
        entityManager.flush();
    }

    /**
     * {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.util.base.CoreBaseDao#getObject(java.lang.Class, java.io.Serializable).
     */
    @Override
    public <T> T getObject(Class<T> clazz, Serializable identifiant)
    {
        LOGGER.debug(">>> Debut methode getObject");
        T entite = entityManager.find(clazz, identifiant);

        if (entite == null)
        {
            throw new ObjectRetrievalFailureException(clazz, identifiant);
        }

        return entite;
    }

    /**
     * {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.util.base.CoreBaseDao#loadAllObjects(java.lang.Class).
     */
    @Override
    public <T> List<T> loadAllObjects(Class<T> type)
    {
        LOGGER.debug(">>> Debut methode loadAllObjects");
        // Paramètrage de la recherche
        CriteriaQuery<T> query = createQuery(type);

        // Exécution de la recherche
        return findAll(query);
    }

    /**
     * {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.util.base.CoreBaseDao#saveObject(java.lang.Object).
     */
    @Override
    public <T> void saveObject(T entite)
    {
        LOGGER.debug(">>> Debut methode saveObject");
        entityManager.persist(entite);
    }

    /**
     * {@inheritDoc} 
     * @see fr.gouv.finances.lombok.util.base.CoreBaseDao#modifyObject(java.lang.Object).
     */
    @Override
    public <T> T modifyObject(final T entite)
    {
        LOGGER.debug(">>> Debut methode modifyObject");
        return entityManager.merge(entite);
    }

    /**
     * {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.util.base.CoreBaseDao#refresh(java.lang.Object).
     */
    @Override
    public <T> void refresh(T entite)
    {
        LOGGER.debug(">>> Debut methode refresh");
        entityManager.refresh(entite);
    }

    /**
     * {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.util.base.CoreBaseDao#findbyUniqueCriteria( java.lang.Class, java.lang.String,
     *      java.lang.String).
     */
    @Override
    public <T> T findbyUniqueCriteria(Class<T> classe, String critere, String valeurCritere)
    {
        return findbyUniqueCriteria(classe, critere, valeurCritere, ModeCritereRecherche.EQUAL);
    }

    /**
     * {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.util.base.CoreBaseDao#findbyUniqueCriteria( java.lang.Class, java.lang.String,
     *      java.lang.String, fr.gouv.finances.lombok.util.persistance.ModeCritereRecherche).
     */
    @Override
    public <T> T findbyUniqueCriteria(Class<T> classe, String critere, String valeurCritere, ModeCritereRecherche modeCritereRecherche)
    {
        LOGGER.debug(">>> Debut methode findbyUniqueCriteria");
        // Paramètrage de la recherche
        CriteriaQuery<T> query = createQuerywithUniqueCriteria(classe, critere, valeurCritere, modeCritereRecherche);

        // Exécution de la recherche
        return find(query);
    }

    /**
     * {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.util.base.CoreBaseDao#findAllByUniqueCriteria( java.lang.Class, java.lang.String,
     *      java.lang.String).
     */
    @Override
    public <T> List<T> findAllByUniqueCriteria(Class<T> classe, String critere, String valeurCritere)
    {
        LOGGER.debug(">>> Debut methode findAllByUniqueCriteria");
        // Paramètrage de la recherche
        CriteriaQuery<T> query = createQuerywithUniqueCriteria(classe, critere, valeurCritere);

        // Exécution de la recherche
        return findAll(query);
    }

    /**
     * {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.util.base.CoreBaseDao#findByCriterias( java.lang.Class, java.util.Map,
     *      fr.gouv.finances.lombok.util.persistance.ModeCritereRecherche).
     */
    @Override
    public <T> T findByCriterias(Class<T> classe, Map<String, String> criteres, ModeCritereRecherche modeCritereRecherche)
    {
        LOGGER.debug(">>> Debut methode findByCriterias");
        // Paramètrage de la recherche
        CriteriaQuery<T> query = createQueryWithCriterias(classe, criteres, modeCritereRecherche);

        // Exécution de la recherche
        return find(query);
    }

    /**
     * {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.util.base.CoreBaseDao#findAllByCriterias( java.lang.Class, java.util.Map,
     *      fr.gouv.finances.lombok.util.persistance.ModeCritereRecherche).
     */
    @Override
    public <T> List<T> findAllByCriterias(Class<T> classe, Map<String, String> criteres, ModeCritereRecherche modeCritereRecherche)
    {
        LOGGER.debug(">>> Debut methode findAllByCriterias");
        // Paramètrage de la recherche
        CriteriaQuery<T> query = createQueryWithCriterias(classe, criteres, modeCritereRecherche);

        // Exécution de la recherche
        return findAll(query);
    }

    /**
     * 
     * (methode de remplacement)
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.jpa.dao.BaseDaoJpa#findByUniqueCriteriaWithJoins(java.lang.Class, java.lang.String, java.lang.String, java.util.Map)
     */
    @Override
    public <T> T findByUniqueCriteriaWithJoins(Class<T> classe, String critere, String valeurCritere, Map<String, JoinType> jointures)
    {
        return findByUniqueCriteriaWithJoins(classe, critere, valeurCritere, ModeCritereRecherche.EQUAL, jointures);
    }

    /**
     * 
     * (methode de remplacement)
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.jpa.dao.BaseDaoJpa#findByUniqueCriteriaWithJoins(java.lang.Class, java.lang.String, java.lang.String, fr.gouv.finances.lombok.util.persistance.ModeCritereRecherche, java.util.Map)
     */
    @Override
    public <T> T findByUniqueCriteriaWithJoins(Class<T> classe, String critere, String valeurCritere, ModeCritereRecherche modeCritereRecherche, Map<String, JoinType> jointures)
    {
        LOGGER.debug(">>> Debut methode findByUniqueCriteriaWithJoins");
        // Si le paramètre n'est pas renseigné, la recherche n'est pas exécutée
        if (StringUtils.isEmpty(critere) || StringUtils.isEmpty(valeurCritere) || jointures == null)
        {
            throw new ProgrammationException("Au moins un des paramètres de recherche d'entité n'a pas été fourni");
        }

        // Paramètrage des critères
        Map<String, String> criteres = new HashMap<>();
        criteres.put(critere, valeurCritere);

        // Paramètrage de la recherche
        CriteriaQuery<T> query = createQueryWithCriteriasAndJoins(classe, criteres, modeCritereRecherche, jointures);

        // Exécution de la recherche
        return find(query);
    }
  
    /**
     * 
     * (methode de remplacement)
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.jpa.dao.BaseDaoJpa#findAllByUniqueCriteriaWithJoins(java.lang.Class, java.lang.String, java.lang.String, java.util.Map)
     */
    @Override
    public <T> List<T> findAllByUniqueCriteriaWithJoins(Class<T> classe, String critere, String valeurCritere,
        Map<String, JoinType> jointures)
    {
        // Si le paramètre n'est pas renseigné, la recherche n'est pas exécutée
        if (StringUtils.isEmpty(critere) || StringUtils.isEmpty(valeurCritere) || jointures == null)
        {
            throw new ProgrammationException("Au moins un des paramètres de recherche d'entité n'a pas été fourni");
        }

        // Paramètrage des critères
        Map<String, String> criteres = new HashMap<>();
        criteres.put(critere, valeurCritere);

        // Paramètrage de la recherche
        CriteriaQuery<T> query = createQueryWithCriteriasAndJoins(classe, criteres, jointures).distinct(true);

        // Exécution de la recherche
        return findAll(query);
    }

    /**
     * 
     * (methode de remplacement)
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.jpa.dao.BaseDaoJpa#findByCriteriasWithJoins(java.lang.Class, java.util.Map, fr.gouv.finances.lombok.util.persistance.ModeCritereRecherche, java.util.Map)
     */
    @Override
    public <T> T findByCriteriasWithJoins(Class<T> classe, Map<String, String> criteres, ModeCritereRecherche modeCritereRecherche,
        Map<String, JoinType> jointures)
    {
        // Paramètrage de la recherche
        CriteriaQuery<T> query = createQueryWithCriteriasAndJoins(classe, criteres, modeCritereRecherche, jointures);

        // Exécution de la recherche
        return find(query);
    }

    /**
     * 
     * (methode de remplacement)
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.jpa.dao.BaseDaoJpa#findAllByCriteriasWithJoins(java.lang.Class, java.util.Map, fr.gouv.finances.lombok.util.persistance.ModeCritereRecherche, java.util.Map)
     */
    @Override
    public <T> List<T> findAllByCriteriasWithJoins(Class<T> classe, Map<String, String> criteres, ModeCritereRecherche modeCritereRecherche,
        Map<String, JoinType> jointures)
    {
        // Paramètrage de la recherche
        CriteriaQuery<T> query = createQueryWithCriteriasAndJoins(classe, criteres, modeCritereRecherche, jointures).distinct(true);

        // Exécution de la recherche
        return findAll(query);
    }

    /**
     * 
     * (methode de remplacement)
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.jpa.dao.BaseDaoJpa#findAllWithJoins(java.lang.Class, java.util.Map)
     */
    @Override
    public <T> List<T> findAllWithJoins(Class<T> classe, Map<String, JoinType> jointures)
    {
        // Paramètrage de la recherche
        CriteriaQuery<T> query = createQueryWithCriteriasAndJoins(classe, new HashMap<String, String>(), jointures);

        // Exécution de la recherche
        return findAll(query);
    }


    /**
     *     
     * (methode de remplacement)
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.jpa.dao.BaseDaoJpa#createQueryWithCriteriasAndJoins(java.lang.Class, java.util.Map, fr.gouv.finances.lombok.util.persistance.ModeCritereRecherche, java.util.Map)
     */
    @Override
    public <T> CriteriaQuery<T> createQueryWithCriteriasAndJoins(Class<T> classe, Map<String, String> criteres,
        ModeCritereRecherche modeCritereRecherche, Map<String, JoinType> jointures)
    {
        // Si un paramètre n'est pas renseigné, la recherche n'est pas exécutée
        if (criteres == null || modeCritereRecherche == null || jointures == null)
        {
            throw new ProgrammationException("Au moins un des paramètres de recherche d'entité n'a pas été fourni");
        }

        // Paramètrage de la recherche
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<T> query = criteriaBuilder.createQuery(classe);
        Root<T> root = query.from(classe);

        // Ajout des critères
        addCriterias(criteriaBuilder, query, root, criteres, modeCritereRecherche);

        // Ajout des jointures
        for (Map.Entry<String, JoinType> entryJointure : jointures.entrySet())
        {
            LOGGER.debug("jointure sur '{}' en mode '{}'", entryJointure.getKey(), entryJointure.getValue());
            root.fetch(entryJointure.getKey(), entryJointure.getValue());
        }

        // Pour éviter les produits cartésiens
        if (!jointures.isEmpty())
        {
            query.distinct(true);
        }

        return query;
    }

    /**
     * 
     * (methode de remplacement)
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.util.base.CoreBaseDao#countTotalNumberOfLines(java.lang.Class, java.util.Map, fr.gouv.finances.lombok.util.persistance.ModeCritereRecherche)
     */
    @Override
    public <T> long countTotalNumberOfLines(Class<T> classe, Map<String, String> criteres, ModeCritereRecherche modeCritereRecherche)
    {
        LOGGER.debug("Recherche du nombre d'entité '{}'", classe.getSimpleName());

        // Si un paramètre n'est pas renseigné, la recherche n'est pas exécutée
        if (criteres == null || modeCritereRecherche == null)
        {
            throw new ProgrammationException("Au moins un des paramètres de recherche d'entité n'a pas été fourni");
        }

        // Paramètrage de la recherche
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Long> query = criteriaBuilder.createQuery(Long.class);
        Root<T> root = query.from(classe);

        // Ajout des critères
        addCriterias(criteriaBuilder, query, root, criteres, modeCritereRecherche);

        // La recherche doit ramener le nombre d'éléments satisfaisant les critères
        query.select(criteriaBuilder.count(root));

        return find(query);
    }

    /**
     * 
     * (methode de remplacement)
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.jpa.dao.BaseDaoJpa#createQueryWithCriteriasAndJoins(java.lang.Class, java.util.Map, java.util.Map)
     */
    @Override
    public <T> CriteriaQuery<T> createQueryWithCriteriasAndJoins(Class<T> classe, Map<String, String> criteres,
        Map<String, JoinType> jointures)
    {
        return createQueryWithCriteriasAndJoins(classe, criteres, ModeCritereRecherche.EQUAL, jointures);
    }

    /**
     * 
     * (methode de remplacement)
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.jpa.dao.BaseDaoJpa#createQueryWithCriterias(java.lang.Class, java.util.Map, fr.gouv.finances.lombok.util.persistance.ModeCritereRecherche)
     */
    @Override
    public <T> CriteriaQuery<T> createQueryWithCriterias(Class<T> classe, Map<String, String> criteres,
        ModeCritereRecherche modeCritereRecherche)
    {
        return createQueryWithCriteriasAndJoins(classe, criteres, modeCritereRecherche, new HashMap<String, JoinType>());
    }

    /**
     * 
     * (methode de remplacement)
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.jpa.dao.BaseDaoJpa#createQueryWithCriterias(java.lang.Class, java.util.Map)
     */
    @Override
    public <T> CriteriaQuery<T> createQueryWithCriterias(Class<T> classe, Map<String, String> criteres)
    {
        return createQueryWithCriterias(classe, criteres, ModeCritereRecherche.EQUAL);
    }

    /**
     * 
     * (methode de remplacement)
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.jpa.dao.BaseDaoJpa#createQuerywithUniqueCriteria(java.lang.Class, java.lang.String, java.lang.String, fr.gouv.finances.lombok.util.persistance.ModeCritereRecherche)
     */
    @Override
    public <T> CriteriaQuery<T> createQuerywithUniqueCriteria(Class<T> classe, String critere, String valeurCritere,
        ModeCritereRecherche modeCritereRecherche)
    {
        Map<String, String> criteres = null;

        // Si un paramètre n'est pas renseigné, la recherche ne sera pas exécutée
        if (!StringUtils.isEmpty(critere) && !StringUtils.isEmpty(valeurCritere))
        {
            criteres = new HashMap<>();
            criteres.put(critere, valeurCritere);
        }

        return createQueryWithCriterias(classe, criteres, modeCritereRecherche);
    }

    /**
     * 
     * (methode de remplacement)
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.jpa.dao.BaseDaoJpa#createQuerywithUniqueCriteria(java.lang.Class, java.lang.String, java.lang.String)
     */
    @Override
    public <T> CriteriaQuery<T> createQuerywithUniqueCriteria(Class<T> classe, String critere, String valeurCritere)
    {
        return createQuerywithUniqueCriteria(classe, critere, valeurCritere, ModeCritereRecherche.EQUAL);
    }

    /**
     * 
     * (methode de remplacement)
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.jpa.dao.BaseDaoJpa#createQuery(java.lang.Class)
     */
    @Override
    public <T> CriteriaQuery<T> createQuery(Class<T> classe)
    {
        return createQueryWithCriterias(classe, new HashMap<String, String>());
    }

    /**
     * 
     * (methode de remplacement)
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.jpa.dao.BaseDaoJpa#addCriterias(javax.persistence.criteria.CriteriaBuilder, javax.persistence.criteria.CriteriaQuery, javax.persistence.criteria.Root, java.util.Map, fr.gouv.finances.lombok.util.persistance.ModeCritereRecherche)
     */
    @Override
    public <T> void addCriterias(CriteriaBuilder criteriaBuilder, CriteriaQuery<?> query, Root<T> root, Map<String, String> criteres,
        ModeCritereRecherche modeCritereRecherche)
    {
        List<Predicate> predicates = new ArrayList<>();

        // Parcours des critères à prendre en compte
        for (Map.Entry<String, String> entry : criteres.entrySet())
        {
            LOGGER.debug("Critère de recherche '{}' de valeur '{}', en mode '{}'", entry.getKey(), entry.getValue(), modeCritereRecherche);

            Path<String> path = root.<String> get(entry.getKey());
            switch (modeCritereRecherche)
            {
                case EQUAL:
                    // Critère d'égalité stricte
                    predicates.add(criteriaBuilder.equal(path, entry.getValue()));
                    break;
                case EQUAL_IGNORE_CASSE:
                    // Critère d'égalité stricte
                    predicates.add(criteriaBuilder.equal(criteriaBuilder.lower(path), entry.getValue().toLowerCase(Locale.FRANCE)));
                    break;
                case NOT_EQUAL:
                    // Critère d'égalité stricte
                    predicates.add(criteriaBuilder.notEqual(path, entry.getValue()));
                    break;
                case IS_NULL:
                    // Critère d'égalité stricte
                    predicates.add(criteriaBuilder.isNull(path));
                    break;
                case LIKE_DEBUT:
                    // Critère d'égalité large
                    predicates.add(criteriaBuilder.like(path, entry.getValue() + "%"));
                    break;
                case LIKE_DEBUT_IGNORE_CASSE:
                    // Critère d'égalité large
                    predicates.add(criteriaBuilder.like(
                        criteriaBuilder.lower(path), entry.getValue().toLowerCase(Locale.FRANCE) + "%"));
                    break;
                case LIKE_ANYWHERE:
                    // Critère d'égalité très large
                    predicates.add(criteriaBuilder.like(path, "%" + entry.getValue() + "%"));
                    break;
                case LIKE_ANYWHERE_IGNORE_CASSE:
                    // Critère d'égalité très large
                    predicates.add(criteriaBuilder.like(
                        criteriaBuilder.lower(path), "%" + entry.getValue().toLowerCase(Locale.FRANCE) + "%"));
                    break;
                case LIKE_FIN:
                    // Critère d'égalité large
                    predicates.add(criteriaBuilder.like(path, "%" + entry.getValue()));
                    break;
                case LIKE_FIN_IGNORE_CASSE:
                    // Critère d'égalité large
                    predicates.add(criteriaBuilder.like(
                        criteriaBuilder.lower(path), "%" + entry.getValue().toLowerCase(Locale.FRANCE)));
                    break;
                default:
                    // Non géré
                    throw new UnsupportedOperationException("Critère de recherche non prise en compte");
            }
        }

        // Ajout des critères à la requête
        query.where(predicates.toArray(new Predicate[] {}));
    }

    /**
     * 
     * (methode de remplacement)
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.jpa.dao.BaseDaoJpa#find(javax.persistence.criteria.CriteriaQuery)
     */
    @Override
    public <T> T find(CriteriaQuery<T> query)
    {
        LOGGER.trace("Recherche d'une seule entité");

        try
        {
            // Recherche d'une entité
            return entityManager.createQuery(query).getSingleResult();
        }
        catch (NoResultException nre)
        {
            LOGGER.debug("Aucun élément trouvé ({})", nre);
            return null;
        }
    }

    /**
     * 
     * (methode de remplacement)
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.jpa.dao.BaseDaoJpa#findAll(javax.persistence.criteria.CriteriaQuery)
     */
    @Override
    public <T> List<T> findAll(CriteriaQuery<T> query)
    {
        LOGGER.trace("Recherche de toutes les entités");

        return entityManager.createQuery(query).getResultList();
    }

    /**
     * 
     * (methode de remplacement)
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.jpa.dao.BaseDaoJpa#findAll(javax.persistence.criteria.CriteriaQuery, int)
     */
    @Override
    public <T> List<T> findAll(CriteriaQuery<T> query, int maxResultat)
    {
        LOGGER.debug("Recherche de toutes les entités (max {})", maxResultat);

        return entityManager.createQuery(query).setMaxResults(maxResultat).getResultList();
    }

    @Override
    public boolean isCharged(Object entite)
    {
        return entityManager.getEntityManagerFactory().getPersistenceUnitUtil().isLoaded(entite);
    }

    protected ScrollIterator getScrollIterator(CriteriaQuery<?> jpaCriteriaQuery, Integer nombreOccurences)
    {
        // Le mode curseur n'étant pas encore supporté par JPA, utilisation des possibilités
        // de l'implémentation Hibernate (conversion javax.persistence.Query en org.hibernate.Query)
        org.hibernate.Query hibernateQuery = entityManager.createQuery(jpaCriteriaQuery)
            .setLockMode(LockModeType.NONE)
            .unwrap(org.hibernate.Query.class)
            .setReadOnly(true);
        if (nombreOccurences != 0)
        {
            hibernateQuery.setFetchSize(nombreOccurences);
        }
        // Activation du mode curseur
        ScrollableResults scrollableResults = hibernateQuery.scroll(ScrollMode.FORWARD_ONLY);

        // Encapsulation dans l'utilitaire Lombok des recherches en mode curseur
        return new ScrollIteratorImpl(scrollableResults);
    }
}
