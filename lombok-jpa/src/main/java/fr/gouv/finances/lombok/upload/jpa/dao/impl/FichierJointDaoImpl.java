/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 *
 */
package fr.gouv.finances.lombok.upload.jpa.dao.impl;

import java.util.Collection;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Root;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Repository;

import fr.gouv.finances.lombok.jpa.dao.BaseDaoJpaImpl;
import fr.gouv.finances.lombok.upload.bean.FichierJoint;
import fr.gouv.finances.lombok.upload.dao.FichierJointDao;
import fr.gouv.finances.lombok.util.exception.ProgrammationException;

/**
 * Implémentation JPA du DAO permettant de gérer les données des fichiers joints.
 *
 * @author Christophe Breheret-Girardin
 */
@ConditionalOnProperty(name = "lombok.orm.jpa", havingValue="true")
@Repository("fichierjointdaoimpl")
public class FichierJointDaoImpl  extends BaseDaoJpaImpl implements FichierJointDao
{

    /** 
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.upload.dao.FichierJointDao#deleteCollectionFichiersJoints(java.util.Collection).
     */
    @Override
    public void deleteCollectionFichiersJoints(Collection<FichierJoint> fichiersASupprimer)
    {
        // Une demande de suppression est effectuée sur chaque fichier joint
        fichiersASupprimer.stream().forEach(this::deleteObject);
    }

    /** 
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.upload.dao.FichierJointDao#findFichierJointEtSonContenuParId(java.lang.Long).
     */
    @Override
    public FichierJoint findFichierJointEtSonContenuParId(Long identifiant)
    {
        // Aucun paramètre fourni
        if (identifiant == null)
        {
            throw new ProgrammationException("Aucun identifiant de fichier joint fourni");
        }

        // Paramétrage de la recherche
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<FichierJoint> query = criteriaBuilder.createQuery(FichierJoint.class);
        Root<FichierJoint> from = query.from(FichierJoint.class);

        // Paramétrage des critères de recherche
        query.where(criteriaBuilder.equal(from.get("id"), identifiant));
        
        // Paramètrage des jointures
        from.fetch("leContenuDuFichier", JoinType.LEFT);

        // Exécution de la recherche
        return find(query);
    }
}