/*
 * Copyright (c) 2018 DGFiP - Tous droits réservés
 * 
 */
/**
 * Paquet contenant la gestion JPA des données des fichiers joints.
 *
 * @author Christophe Breheret-Girardin
 */
package fr.gouv.finances.lombok.upload.jpa.dao.impl;