package fr.gouv.finances.lombok.autoconfig;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

@Configuration
public class JpaApplicationConfig
{

    private static final Logger log = LoggerFactory.getLogger(JpaApplicationConfig.class);

    @Configuration
    @ConditionalOnExpression("'${lombok.orm.jpa}' == 'true'")
    @ImportResource({"classpath:conf/applicationContext-monnaie-dao.xml",
            "classpath:conf/applicationContext-commun-jpabatchoptimisation.xml"})
    @ComponentScan(basePackages = {"fr.gouv.finances.lombok.monnaie.jpa.dao.impl"})
    public static class JpaRequisApplicationConfig
    {

        private static final Logger log = LoggerFactory.getLogger(JpaRequisApplicationConfig.class);

        @PostConstruct
        public void init()
        {
            log.debug(">>> Après initialisation de JpaRequisApplicationConfig");
        }
    }

    @Configuration
    @ConditionalOnExpression("'${lombok.composant.atlas.inclus}' == 'true' or '${lombok.composant.upload.inclus}' == 'true'")
    @ImportResource({"classpath:conf/applicationContext-fichier-joint-dao.xml"})
    @ComponentScan(basePackages = {"fr.gouv.finances.lombok.upload.jpa.dao.impl"})
    public static class JpaUploadAtlasApplicationConfig
    {

        private static final Logger log = LoggerFactory.getLogger(JpaUploadAtlasApplicationConfig.class);

        @PostConstruct
        public void init()
        {
            log.debug(">>> Après initialisation de JpaUploadAtlasApplicationConfig");
        }
    }

    @Configuration
    @ConditionalOnExpression("'${lombok.composant.edition.inclus}' == 'true' or '${lombok.composant.batchoptimisation.inclus}' == 'true'")
    @ImportResource({"classpath:conf/applicationContext-commun-jpabatchoptimisation.xml"})
    public static class EditionsBatchOptimisationApplicationConfig
    {

        private static final Logger log = LoggerFactory.getLogger(EditionsBatchOptimisationApplicationConfig.class);

        @PostConstruct
        public void init()
        {
            log.debug(">>> Après initialisation de EditionsBatchOptimisationApplicationConfig");
        }
    }

}
