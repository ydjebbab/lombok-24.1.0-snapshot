/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 *
 */
package fr.gouv.finances.lombok.monnaie.jpa.dao.impl;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Repository;

import fr.gouv.finances.lombok.jpa.dao.BaseDaoJpaImpl;
import fr.gouv.finances.lombok.monnaie.bean.TauxDeChancellerie;
import fr.gouv.finances.lombok.monnaie.dao.TauxDeChancellerieDao;

/**
 * Implémentation JPA du DAO permettant de gérer les données du taux de chancellerie.
 *
 * @author Christophe Breheret-Girardin
 */
@ConditionalOnProperty(name = "lombok.orm.jpa", havingValue="true")
@Repository("tauxdechancelleriedaoimpl")
public class TauxDeChancellerieDaoImpl extends BaseDaoJpaImpl implements TauxDeChancellerieDao
{
    /** 
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.monnaie.dao.TauxDeChancellerieDao#deleteTauxChancellerie(
     * fr.gouv.finances.lombok.monnaie.bean.TauxDeChancellerie).
     */
    @Override
    public void deleteTauxChancellerie(TauxDeChancellerie unTauxDeChancellerie)
    {
        deleteObject(unTauxDeChancellerie);
    }

    /** 
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.monnaie.dao.TauxDeChancellerieDao#modifyTauxChancellerie(
     * fr.gouv.finances.lombok.monnaie.bean.TauxDeChancellerie).
     */
    @Override
    public void modifyTauxChancellerie(TauxDeChancellerie unTauxDeChancellerie)
    {
        modifyObject(unTauxDeChancellerie);
    }

    /** 
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.monnaie.dao.TauxDeChancellerieDao#saveTauxChancellerie(
     * fr.gouv.finances.lombok.monnaie.bean.TauxDeChancellerie).
     */
    @Override
    public void saveTauxChancellerie(TauxDeChancellerie unTauxDeChancellerie)
    {
        saveObject(unTauxDeChancellerie);
    }
}