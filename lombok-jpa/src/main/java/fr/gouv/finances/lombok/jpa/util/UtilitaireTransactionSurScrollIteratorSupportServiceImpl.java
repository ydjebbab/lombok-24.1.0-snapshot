package fr.gouv.finances.lombok.jpa.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.AbstractPlatformTransactionManager;

import fr.gouv.finances.lombok.util.hibernate.UtilitaireTransactionSurScrollIteratorSupportService;

public class UtilitaireTransactionSurScrollIteratorSupportServiceImpl implements UtilitaireTransactionSurScrollIteratorSupportService
{
    private static final Logger log = LoggerFactory.getLogger(UtilitaireTransactionSurScrollIteratorSupportServiceImpl.class);
    
    JpaTransactionManager transactionmanagersanscache;

    JpaTransactionManager transactionmanagerstandard;

    public UtilitaireTransactionSurScrollIteratorSupportServiceImpl()
    {
        super();
    }

    public void setTransactionmanagersanscache(JpaTransactionManager jpaTransactionManager)
    {
        this.transactionmanagersanscache = jpaTransactionManager;
    }

    public void setTransactionmanagerstandard(JpaTransactionManager jpaTransactionManager)
    {
        this.transactionmanagerstandard = jpaTransactionManager;
    }

    @Override
    public AbstractPlatformTransactionManager getTransactionmanagersanscache()
    {
        return transactionmanagersanscache;
    }

    @Override
    public AbstractPlatformTransactionManager getTransactionmanagerstandard()
    {
        return transactionmanagerstandard;
    }

    /**
     * <pre>
     * Permet de fermer la transaction en fin de boucle sur le SCROLLITERATOR, que le traitement soit en erreur ou non
     * (lecture seule pas de ROLLBACK). 
     * A insérer au sein d'une clause FINALLY pour garantir la fin de transaction.
     * </pre>
     * 
     * @param status fourni lors du demarrage de la transaction
     */
    @Override
    public void commiterTransactionEnglobanteDeLectureScrollIterator(TransactionStatus transactionStatus)
    {
        log.debug(">>> Debut commiterTransactionEnglobanteDeLectureScrollIterator()");
        UtilitaireTransactionSurScrollIterator.commiterTransaction(transactionmanagersanscache, transactionStatus);
    }

    /**
     * Permet de fermer la transaction en fin mise à jour d'un lot dans le cas où le traitement s'est fait sans erreur.
     * 
     * @param status fourni lors du demarrage de la transaction
     */
    @Override
    public void commiterTransactionEnglobeeDeMiseAJourDUnLot(TransactionStatus transactionStatus)
    {
        log.debug(">>> Debut commiterTransactionEnglobeeDeMiseAJourDUnLot()");
        UtilitaireTransactionSurScrollIterator.commiterTransaction(transactionmanagerstandard, transactionStatus);
    }

    /**
     * <pre>
     * Permet de démarrer une transaction englobante sur un scrolliterator.
     * La transaction est démarrée sur un transaction manager spécifique sans cache pour permettre l'apurement dans
     * HIBERNATE des caches de type ACTIONQUEUE qui ne sont pas purgés par des commandes CLEAR() et FLUSH().
     * En fin de parcours du SCROLLITERATOR la transaction doit etre fermée par un appel à COMMITERTRANSACTIONENGLOBANTEDELECTURESCROLLITERATOR.
     * Utiliser une clause FINALLY. 
     * Comme il s'agit d'une transaction en lecture, il n'y pas de rollback, on peut commiter même en cas d'exception.
     * </pre>
     * 
     * @return le transaction status retourné doit etre fourni pour le commit
     */
    @Override
    public TransactionStatus demarrerTransactionEnglobanteDeLectureScrollIterator()
    {
        log.debug(">>> Debut demarrerTransactionEnglobanteDeLectureScrollIterator()");
        return UtilitaireTransactionSurScrollIterator.demarrerTransactionEnglobanteDeLectureScrollIterator(transactionmanagersanscache);
    }

    /**
     * <pre>
     * Permet de démarrer une transaction englobée pour faire des mises à jour à l'interieur d'une boucle de scroll
     * iterator. 
     * En fin de traitement de mise à jour la transaction doit etre fermée par un appel à
     * COMMITERTRANSACTIONENGLOBEEENGLOBEEDEMISEAJOURDUNLOT. 
     * Si une exception est captée ou une erreur de traitement détectée, il faut annuler le traitement du lot par un appel à
     * ROLLBACKSURTRANSACTIONENGLOBEEENGLOBEEDEMISEAJOURDUNLOT
     * </pre>
     * 
     * @return le transaction status retourné doit etre fourni pour le commit ou le rollback
     */
    @Override
    public TransactionStatus demarrerTransactionEnglobeeDeMiseAJourDUnLot()
    {
        log.debug(">>> Debut demarrerTransactionEnglobeeDeMiseAJourDUnLot()");
        return UtilitaireTransactionSurScrollIterator.demarrerTransactionEnglobeeDeMiseAJourDUnLot(transactionmanagerstandard);
    }

    /**
     * Permet de fermer la transaction en fin de mise à jour d'un lot dans le cas ou le traitement s'est mal déroulé
     * pour annuler tout le lot de mise à jour.
     * 
     * @param status fourni lors du demarrage de la transaction
     */
    @Override
    public void rollBackSurTransactionEnglobeeDeMiseAJourDUnLot(TransactionStatus transactionStatus)
    {
        log.debug(">>> Debut rollBackSurTransactionEnglobeeDeMiseAJourDUnLot()");
        UtilitaireTransactionSurScrollIterator.rollbackerTransaction(transactionmanagerstandard, transactionStatus);
    }

}