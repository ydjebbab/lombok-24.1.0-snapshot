/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 */
package fr.gouv.finances.lombok.jpa.util;

import org.hibernate.ScrollableResults;

import fr.gouv.finances.lombok.util.persistance.AbstractScrollIteratorCore;

/**
 * Implémentation, JPA mais spécifique Hibernate (non supportée par JPA), des recherches en mode curseur.
 *
 * @author chouard-cp
 * @author Christophe Breheret-Girardin
 */
public class ScrollIteratorImpl extends AbstractScrollIteratorCore
{
    /** L'objet Hibernate ScrollableResults à travers lequel on itère. */
    protected ScrollableResults scrollableResults;

    /**
     * Constructeur.
     *
     * @param scrollableResults utilitaire de recherche en mode curseur
     */
    public ScrollIteratorImpl(ScrollableResults scrollableResults)
    {
        super();
        this.scrollableResults = scrollableResults;
    }

    /** 
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.util.persistance.ScrollIterator#close()
     */
    @Override
    public void close()
    {
        if (!isClosed && scrollableResults != null)
        {
            scrollableResults.close();
            scrollableResults = null;
        }

        isClosed = true;
    }

    /** 
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.util.persistance.ScrollIterator#hasNext()
     */
    @Override
    public boolean hasNext()
    {
        if (nextRow == null && !isClosed)
        {
            if (scrollableResults.next())
            {
                nextRow = scrollableResults.get();
            }
            else
            {
                close();
            }
        }

        return (nextRow != null);
    }
}