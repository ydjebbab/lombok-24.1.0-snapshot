/*
 * Copyright (c) 2018 DGFiP - Tous droits réservés
 * 
 */
/**
 * Paquet regroupant les DAO de la fonctionnalité monnaie
 *
 * @author Christophe Breheret-Girardin
 */
package fr.gouv.finances.lombok.monnaie.jpa.dao.impl;