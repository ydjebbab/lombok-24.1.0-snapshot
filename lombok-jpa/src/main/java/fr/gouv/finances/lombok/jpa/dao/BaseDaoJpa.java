/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 *
 */
package fr.gouv.finances.lombok.jpa.dao;

import java.util.List;
import java.util.Map;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Root;

import fr.gouv.finances.lombok.util.base.CoreBaseDao;
import fr.gouv.finances.lombok.util.persistance.ModeCritereRecherche;

/**
 * Interface du socle des DAO JPA
 *
 * @author Christophe Breheret-Girardin
 * @author Celinio Fernandes
 */
public interface BaseDaoJpa extends CoreBaseDao
{
    /**
     * Méthode pour construire une requête Criteria qui permet de faire une recherche sur une entité donnée. Elle
     * effectue des jointures sur d'autres entités, à partir de critères.
     *
     * @param <T> Entité à rechercher
     * @param classe Type de l'entité à rechercher
     * @param criteres sur lesquels il faut appliquer un filtre (= propriétés de l'entité)
     * @param modeCritereRecherche type de recherche sur les critères (égalité stricte, égalité large, etc.)
     * @param jointures jointures à appliquer (nom et type de jointure)
     * @return la requête Criteria permettant de faire une recherche sur l'entité
     */
    <T> CriteriaQuery<T> createQueryWithCriteriasAndJoins(Class<T> classe, Map<String, String> criteres,
        ModeCritereRecherche modeCritereRecherche, Map<String, JoinType> jointures);

    /**
     * Méthode pour construire une requête Criteria qui permet de faire une recherche sur une entité donnée. Elle
     * effectue des jointures sur d'autres entités, à partir de critères.
     *
     * @param <T> Entité à rechercher
     * @param classe Type de l'entité à rechercher
     * @param criteres sur lesquels il faut appliquer un filtre d'égalité stricte (= propriétés de l'entité)
     * @param jointures jointures à appliquer (nom et type de jointure)
     * @return la requête Criteria permettant de faire une recherche sur l'entité
     */
    <T> CriteriaQuery<T> createQueryWithCriteriasAndJoins(Class<T> classe, Map<String, String> criteres,
        Map<String, JoinType> jointures);

    /**
     * Méthode pour construire une requête Criteria qui permet de faire une recherche sur une entité donnée, à partir de
     * critères.
     *
     * @param <T> Entité à rechercher
     * @param classe Type de l'entité à rechercher
     * @param criteres sur lesquels il faut appliquer un filtre (= propriétés de l'entité)
     * @param modeCritereRecherche type de recherche sur les critères (égalité stricte, égalité large, etc.)
     * @return la requête Criteria permettant de faire une recherche sur l'entité
     */
    <T> CriteriaQuery<T> createQueryWithCriterias(Class<T> classe, Map<String, String> criteres,
        ModeCritereRecherche modeCritereRecherche);

    /**
     * Méthode pour construire une requête Criteria qui permet de faire une recherche sur une entité donnée, à partir de
     * critères.
     *
     * @param <T> Entité à rechercher
     * @param classe Type de l'entité à rechercher
     * @param criteres sur lesquels il faut appliquer un filtre d'égalité stricte (= propriétés de l'entité)
     * @return la requête Criteria permettant de faire une recherche sur l'entité
     */
    <T> CriteriaQuery<T> createQueryWithCriterias(Class<T> classe, Map<String, String> criteres);

    /**
     * Méthode  pour construire une requête Criteria  qui permet de faire une recherche sur une entité donnée, à partir d'un
     * critère.
     *
     * @param <T> Entité à rechercher
     * @param classe Type de l'entité à rechercher
     * @param critere critère sur lequel il faut appliquer un filtre (= propriété de l'entité)
     * @param valeurCritere valeur du critère (= valeur de la propriété de l'entité)
     * @param modeCritereRecherche type de recherche sur le critère (égalité stricte, égalité large, etc.)
     * @return la requête Criteria permettant de faire une recherche sur l'entité
     */
    <T> CriteriaQuery<T> createQuerywithUniqueCriteria(Class<T> classe, String critere, String valeurCritere,
        ModeCritereRecherche modeCritereRecherche);

    /**
     * Méthode pour construire une requête Criteria qui permet de faire une recherche sur une entité donnée, à partir d'un
     * critère.
     *
     * @param <T> Entité à rechercher
     * @param classe Type de l'entité à rechercher
     * @param critere critère sur lequel il faut appliquer un filtre d'égalité stricte (= propriété de l'entité)
     * @param valeurCritere valeur du critère (= valeur de la propriété de l'entité)
     * @return la requête Criteria permettant de faire une recherche sur l'entité
     */
    <T> CriteriaQuery<T> createQuerywithUniqueCriteria(Class<T> classe, String critere, String valeurCritere);

    /**
     * Méthode pour construire une requête Criteria qui permet de faire une recherche sur une entité donnée.
     *
     * @param <T> Entité à rechercher
     * @param classe Type de l'entité à rechercher
     * @return la requête Criteria permettant de faire une recherche sur l'entité
     */
    <T> CriteriaQuery<T> createQuery(Class<T> classe);

    /**
     * Méthode pour rajouter des critères de recherche à une requête Criteria.
     *
     * @param <T> Entité à rechercher
     * @param criteriaBuilder Utilitaire de requêtage via l'API Criteria
     * @param query Requête dont on veut ajouter des critères de recherche
     * @param modeCritereRecherche Type de recherche sur les critères (égalité stricte, égalité large, etc.)
     * @param root Type utilisé dans la clause "FROM" de la requête.
     * @param criteres Critères à ajouter à la requête
     */
    <T> void addCriterias(CriteriaBuilder criteriaBuilder, CriteriaQuery<?> query, Root<T> root,
        Map<String, String> criteres,
        ModeCritereRecherche modeCritereRecherche);

    /**
     * Méthode pour rechercher une entité via une requête Criteria.
     *
     * @param query requête à exécuter
     * @return entité résultat de la requête
     */
    <T> T find(CriteriaQuery<T> query);

    /**
     * Méthode pour rechercher une liste d'entités via une requête Criteria.
     *
     * @param query requête à exécuter
     * @return la liste d'entités résultat de la requête
     */
    <T> List<T> findAll(CriteriaQuery<T> query);

    /**
     * Méthode pour rechercher une liste d'entités via une requête Criteria.
     *
     * @param query Requête à exécuter
     * @param maxResultat Nombre de lignes max à récupérer
     * @return La liste d'entités résultat de la requête
     */
    <T> List<T> findAll(CriteriaQuery<T> query, int maxResultat);
    
    
    /**
     * Méthode permettant de faire une recherche sur une entité donnée, en effectuant des jointures
     * sur d'autres entités, à partir d'un critère.
     *
     * @param <T> Entité à rechercher
     * @param classe Type de l'entité à rechercher
     * @param critere Critere sur lequel il faut appliquer un filtre d'égalité stricte (= propriété de l'entité)
     * @param valeurCritere Valeur du critère (= valeur de la propriété de l'entité)
     * @param jointures Jointures à appliquer (nom et type de jointure)
     * @return l'entité correspondante
     */
    <T> T findByUniqueCriteriaWithJoins(Class<T> classe, String critere, String valeurCritere
        , Map<String, JoinType> jointures);

    /**
     * Méthode permettant de faire une recherche sur une entité donnée, en effectuant des jointures
     * sur d'autres entités, à partir d'un critère.
     *
     * @param <T> Entité à rechercher
     * @param classe Type de l'entité à rechercher
     * @param critere Critere sur lequel il faut appliquer un filtre d'égalité stricte (= propriété de l'entité)
     * @param valeurCritere Valeur du critère (= valeur de la propriété de l'entité)
     * @param jointures Jointures à appliquer (nom et type de jointure)
     * @return l'entité correspondante
     */
    <T> T findByUniqueCriteriaWithJoins(Class<T> classe, String critere, String valeurCritere
        , ModeCritereRecherche modeCritereRecherche, Map<String, JoinType> jointures);
    /**
     * Méthode permettant de faire une recherche sur des entités données, en effectuant des jointures
     * sur d'autres entités, à partir d'un critère.
     *
     * @param <T> Entité à rechercher
     * @param classe Type des entités à rechercher
     * @param critere Critere sur lequel il faut appliquer un filtre d'égalité stricte (= propriété de l'entité)
     * @param valeurCritere Valeur du critère (= valeur de la propriété de l'entité)
     * @param jointures Jointures à appliquer (nom et type de jointure)
     * @return la liste des entités correspondantes
     */
    <T> List<T> findAllByUniqueCriteriaWithJoins(Class<T> classe, String critere, String valeurCritere
        , Map<String, JoinType> jointures);
    
    /**
     * Méthode permettant de faire une recherche sur une entité donnée, en effectuant des jointures
     * sur d'autres entités, à partir de critères.
     *
     * @param <T> Entité à rechercher
     * @param classe Type de l'entité à rechercher
     * @param criteres Criteres sur lesquels il faut appliquer un filtre (= propriétés de l'entité)
     * @param modeCritereRecherche Type de recherche sur les critères (égalité stricte, égalité large, etc.)
     * @param jointures Jointures à appliquer (nom et type de jointure)
     * @return l'entité correspondante
     */
    <T> T findByCriteriasWithJoins(Class<T> classe, Map<String, String> criteres
        , ModeCritereRecherche modeCritereRecherche
        , Map<String, JoinType> jointures);

    /**
     * Méthode permettant de faire une recherche sur des entités données, en effectuant des jointures
     * sur d'autres entités, à partir de critères.
     *
     * @param <T> Entité à rechercher
     * @param classe Type des entités à rechercher
     * @param criteres Criteres sur lesquels il faut appliquer un filtre (= propriétés de l'entité)
     * @param modeCritereRecherche Type de recherche sur les critères (égalité stricte, égalité large, etc.)
     * @param jointures Jointures à appliquer (nom et type de jointure)
     * @return la liste des entités correspondantes
     */
    <T> List<T> findAllByCriteriasWithJoins(Class<T> classe, Map<String, String> criteres
        , ModeCritereRecherche modeCritereRecherche
        , Map<String, JoinType> jointures);

    /**
     * Méthode permettant de faire une recherche sur des entités données, en effectuant des jointures
     * sur d'autres entités.
     *
     * @param <T> Entité à rechercher
     * @param classe Type des entités à rechercher
     * @param jointures Jointures à appliquer (nom et type de jointure)
     * @return la liste des entités correspondantes
     */
    <T> List<T> findAllWithJoins(Class<T> classe, Map<String, JoinType> jointures);
}
