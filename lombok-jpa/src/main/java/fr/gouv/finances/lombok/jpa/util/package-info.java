/*
 * Copyright (c) 2018 DGFiP - Tous droits réservés
 * 
 */
/**
 * Package regroupant les utilitaires JPA.
 *
 * @author Christophe Breheret-Girardin
 */
package fr.gouv.finances.lombok.jpa.util;