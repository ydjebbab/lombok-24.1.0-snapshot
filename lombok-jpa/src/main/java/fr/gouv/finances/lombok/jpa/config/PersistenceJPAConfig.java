package fr.gouv.finances.lombok.jpa.config;

import java.util.Properties;

import javax.sql.DataSource;

import org.apache.tomcat.jdbc.pool.PoolProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaDialect;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.support.TransactionTemplate;

/**
 * <pre>
 * Cette classe reprend l'ancienne configuration XML des fichiers suivants :
 * - /lombok-core/src/main/resources/conf/applicationContext-commun-dao.xml (bean daoproxyabstract)
 * - /lombok-jpa/src/main/resources/conf/applicationContext-datasources-jpa.xml
 * </pre>
 * 
 * @author celfer Date: 11 oct. 2019
 */
@Configuration
@EnableTransactionManagement
@ComponentScan({"fr.gouv.finances.*.*.dao"})
@EnableJpaRepositories(basePackages = "fr.gouv.finances.*.*.dao")
@ConditionalOnProperty(name = "lombok.orm.jpa", havingValue = "true")
public class PersistenceJPAConfig
{
    protected static final Logger LOGGER = LoggerFactory.getLogger(PersistenceJPAConfig.class);

    private static final String UNITE_PERSISTANCE_PRINCIPAL = "pu-principal";

    private static final String UNITE_PERSISTANCE_EDITION = "pu-edition";

    public PersistenceJPAConfig()
    {
        super();
    }

    /**
     * Configuration JPA pour une base embedded HSQL (profil embedded)
     *
     * @author celfer Date: 17 oct. 2019
     */
    @Configuration
    @Profile("embedded")
    public static class InnerEmbeddedConfig
    {
        @Value(value = "${jasperdbpool.initialsize}")
        private int jasperInitialSize;

        @Value(value = "${jasperdbpool.maxactive}")
        private int jasperMaxActive;

        @Value(value = "${jasperdbpool.maxidle}")
        private int jasperMaxIdle;

        @Value(value = "${jasperdbpool.minevictableidletimemillis}")
        private int minEvictableIdleTimeMillis;

        @Value(value = "${jasperdbpool.timebetweenevictionrunsmillis}")
        private int timeBetweenEvictionRunsMillis;

        @Value(value = "${embedded.hsql.chemin.delete.schema}")
        private String embeddedHsqlCheminDeleteSchema;

        @Value(value = "${embedded.hsql.chemin.create.schema}")
        private String embeddedHsqlCheminCreateSchema;

        @Value(value = "${embedded.hsql.chemin.create.data}")
        private String embeddedHsqlCheminCreateData;

        @Value(value = "${embedded.hsql.chemin.create.schemajasper}")
        private String embeddedHsqlCheminCreateSchemajasper;

        /**
         * <pre>
         * Construction de l'EMF à partir de la source de données et du choix d'hibernate. 
         * L'annotation @Primary permet de résoudre l'erreur suivante : 
         * Parameter 1 of constructor in springfox.documentation.spring.web.plugins.WebMvcRequestHandlerProvider required a single bean, but 2 were found : 
         * - entityManager: defined by method 'entityManagerFactoryBean' 
         * - entityManagerEdition: defined by method 'entityManagerEditionFactoryBean'
         * </pre>
         */
        // @Bean(name = "entityManagerJPA")
        @Bean(name = "entityManager")
        @Primary
        public LocalContainerEntityManagerFactoryBean entityManagerFactoryBean()
        {
            LOGGER.debug(">>> Instanciation du bean entityManager");
            final LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
            em.setDataSource(dataSource());
            em.setPersistenceUnitName(UNITE_PERSISTANCE_PRINCIPAL);
            em.setPackagesToScan(new String[] {"fr.gouv.finances.*.*.entite"});

            final HibernateJpaVendorAdapter hibernateJpaVendorAdapter = new HibernateJpaVendorAdapter();
            // CF: false par défaut
            // hibernateJpaVendorAdapter.setShowSql(false);
            hibernateJpaVendorAdapter.setGenerateDdl(false);
            hibernateJpaVendorAdapter.setDatabasePlatform("org.hibernate.dialect.HSQLDialect");

            em.setJpaVendorAdapter(hibernateJpaVendorAdapter);
            em.setJpaProperties(additionalProperties());

            return em;
        }

        /**
         * Définition de la source de données
         */
        @Bean(name = "datasource")
        public DataSource dataSource()
        {
            LOGGER.debug(">>> Instanciation du bean datasource");
            EmbeddedDatabaseBuilder builder = new EmbeddedDatabaseBuilder();
            EmbeddedDatabase db = builder
                .setType(EmbeddedDatabaseType.HSQL)
                // .setName("lombokdbhsql")
                // CF: si aucun nom de base de données n’est défini via EmbeddedDatabaseBuilder.setName(),
                // Spring affectera un nom de base de données par défaut, «testdb».
                .addScript("classpath:" + embeddedHsqlCheminDeleteSchema)
                .addScript("classpath:" + embeddedHsqlCheminCreateSchema)
                .addScript("classpath:" + embeddedHsqlCheminCreateData)
                .build();
            return db;
        }

        /**
         * Construction d'un gestionnaire de transaction en liaison avec l'usine à EM.
         */
        // @Bean(name = "transactionManagerJPA")
        @Bean
        public PlatformTransactionManager transactionManager()
        {
            LOGGER.debug(">>> Instanciation du bean transactionManager");
            JpaTransactionManager jpaTransactionManager = new JpaTransactionManager();
            jpaTransactionManager.setEntityManagerFactory(entityManagerFactoryBean().getObject());
            jpaTransactionManager.setJpaDialect(new HibernateJpaDialect());
            return jpaTransactionManager;
        }

        @Bean
        public PersistenceExceptionTranslationPostProcessor exceptionTranslation()
        {
            return new PersistenceExceptionTranslationPostProcessor();
        }

        // CF: On remplace par @Transactional.
        /*
         * @Bean
         * @Profile("!batchdao") public TransactionProxyFactoryBean daoproxyabstract() { TransactionProxyFactoryBean
         * transactionProxyFactoryBean = new TransactionProxyFactoryBean(); Properties transactionAttributes = new Properties();
         * transactionAttributes.setProperty("*", "PROPAGATION_REQUIRED, ISOLATION_READ_COMMITTED");
         * transactionProxyFactoryBean.setTransactionAttributes(transactionAttributes);
         * transactionProxyFactoryBean.setTransactionManager(transactionManager()); return transactionProxyFactoryBean; }
         */

        @Bean
        // @Profile("!batchdao")
        @ConditionalOnProperty(name = "lombok.composant.batchdao.inclus", havingValue = "false")
        public TransactionTemplate transactionTemplate()
        {
            LOGGER.debug(">>> Instanciation du bean transactionTemplate");
            TransactionTemplate transactionTemplate = new TransactionTemplate();
            transactionTemplate.setTransactionManager(transactionManager());
            return transactionTemplate;
        }

        private final Properties additionalProperties()
        {
            final Properties hibernateProperties = new Properties();
            hibernateProperties.setProperty("hibernate.hbm2ddl.auto", "validate");
            // CF : false par défaut
            // hibernateProperties.setProperty("hibernate.use_sql_comments", "false");
            // TODO : use_sql_comments et show_sql à true ?
            return hibernateProperties;
        }

        // **********************
        // Profil edition
        // **********************

        @Bean(name = "entityManagerEdition")
        // @Profile("edition")
        @ConditionalOnProperty(name = "lombok.composant.edition.inclus", havingValue = "true")
        public LocalContainerEntityManagerFactoryBean entityManagerEditionFactoryBean()
        {
            LOGGER.debug(">>> Instanciation du bean entityManagerEditionFactoryBean");
            final LocalContainerEntityManagerFactoryBean localContainerEntityManagerFactoryBean =
                new LocalContainerEntityManagerFactoryBean();
            localContainerEntityManagerFactoryBean.setDataSource(editionDatasource());
            localContainerEntityManagerFactoryBean.setPersistenceUnitName(UNITE_PERSISTANCE_EDITION);
            // em.setPackagesToScan(new String[] {"fr.gouv.finances.*.*.entite"});

            final HibernateJpaVendorAdapter hibernateJpaVendorAdapter = new HibernateJpaVendorAdapter();
            // CF: false par défaut
            // hibernateJpaVendorAdapter.setShowSql(false);
            hibernateJpaVendorAdapter.setGenerateDdl(false);
            hibernateJpaVendorAdapter.setDatabasePlatform("org.hibernate.dialect.HSQLDialect");

            localContainerEntityManagerFactoryBean.setJpaVendorAdapter(hibernateJpaVendorAdapter);
            localContainerEntityManagerFactoryBean.setJpaProperties(additionalProperties());

            return localContainerEntityManagerFactoryBean;
        }

        @Bean
        // @Profile("edition")
        @ConditionalOnProperty(name = "lombok.composant.edition.inclus", havingValue = "true")
        public PlatformTransactionManager editionTransactionManager()
        {
            LOGGER.debug(">>> Instanciation du bean editionTransactionManager");
            JpaTransactionManager jpaTransactionManager = new JpaTransactionManager();
            jpaTransactionManager.setEntityManagerFactory(entityManagerEditionFactoryBean().getObject());
            jpaTransactionManager.setJpaDialect(new HibernateJpaDialect());
            return jpaTransactionManager;
        }

        @Bean(name = "editionDatasource", destroyMethod = "close")
        // @Profile("edition")
        @ConditionalOnProperty(name = "lombok.composant.edition.inclus", havingValue = "true")
        public DataSource editionDatasource()
        {
            LOGGER.debug(">>> Instanciation du bean editionDatasource");
            org.apache.tomcat.jdbc.pool.DataSource dataSource = new org.apache.tomcat.jdbc.pool.DataSource(editionPool());
            return dataSource;
        }

        @Bean(name = "editionpool")
        // @Profile("edition")
        @ConditionalOnProperty(name = "lombok.composant.edition.inclus", havingValue = "true")
        public org.apache.tomcat.jdbc.pool.PoolProperties editionPool()
        {
            LOGGER.debug(">>> Instanciation du bean editionpool");
            PoolProperties poolProperties = new PoolProperties();
            poolProperties.setInitialSize(jasperInitialSize);
            poolProperties.setMaxActive(jasperMaxActive);
            poolProperties.setMaxIdle(jasperMaxIdle);
            poolProperties.setMinEvictableIdleTimeMillis(minEvictableIdleTimeMillis);
            poolProperties.setTimeBetweenEvictionRunsMillis(timeBetweenEvictionRunsMillis);
            poolProperties.setTestWhileIdle(true);
            poolProperties.setTestOnReturn(true);
            poolProperties.setTestOnBorrow(false);
            poolProperties.setValidationQuery("select 1 from INFORMATION_SCHEMA.SYSTEM_USERS");
            poolProperties.setDefaultReadOnly(false);
            poolProperties.setDefaultAutoCommit(true);
            poolProperties.setDataSource(editionDS());

            return poolProperties;
        }

        @Bean
        // @Profile("edition")
        @ConditionalOnProperty(name = "lombok.composant.edition.inclus", havingValue = "true")
        public DataSource editionDS()
        {
            LOGGER.debug(">>> Instanciation du bean editionDS");
            EmbeddedDatabaseBuilder embeddedDatabaseBuilder = new EmbeddedDatabaseBuilder();
            EmbeddedDatabase embeddedDatabase = embeddedDatabaseBuilder
                .setType(EmbeddedDatabaseType.HSQL)
                // .setName("lombokdbeditionhsql")
                .addScript("classpath:" + embeddedHsqlCheminCreateSchemajasper)
                .build();
            return embeddedDatabase;
        }

        @Bean
        public JdbcTemplate jdbcTemplate()
        {
            LOGGER.debug(">>> Instanciation du bean jdbcTemplate");
            JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource());
            return jdbcTemplate;
        }
    }

    /**
     * Configuration JPA pour une base postgreSQL (profil postgre)
     *
     * @author celfer Date: 17 oct. 2019
     */
    @Configuration
    @Profile("postgre")
    public static class InnerPostgreSQLConfig
    {
        @Value(value = "${dbpool.initialsize}")
        private int initialSize;

        @Value(value = "${dbpool.maxactive}")
        private int maxActive;

        @Value(value = "${dbpool.maxidle}")
        private int maxIdle;

        @Value(value = "${jasperdbpool.initialsize}")
        private int jasperInitialSize;

        @Value(value = "${jasperdbpool.maxactive}")
        private int jasperMaxActive;

        @Value(value = "${jasperdbpool.maxidle}")
        private int jasperMaxIdle;

        @Value(value = "${jasperdbpool.minevictableidletimemillis}")
        private int minEvictableIdleTimeMillis;

        @Value(value = "${jasperdbpool.timebetweenevictionrunsmillis}")
        private int timeBetweenEvictionRunsMillis;

        @Value(value = "${db.driver}")
        private String dataBaseDriverClassName;

        @Value(value = "${db.url}")
        private String databaseURL;

        @Value(value = "${db.username}")
        private String databaseUsername;

        @Value(value = "${db.password}")
        private String databasePassword;

        @Value(value = "${jasperdb.driver}")
        private String jasperDatabaseDriverClassName;

        @Value(value = "${jasperdb.url}")
        private String jasperDatabaseURL;

        @Value(value = "${jasperdb.username}")
        private String jasperDatabaseUsername;

        @Value(value = "${jasperdb.password}")
        private String jasperDatabasePassword;

        // @Bean(name = "entityManagerPostgreSQL")
        @Bean(name = "entityManager")
        public LocalContainerEntityManagerFactoryBean entityManagerFactoryBean()
        {
            LOGGER.debug(">>> Instanciation du bean entityManager");
            final LocalContainerEntityManagerFactoryBean localContainerEntityManagerFactoryBean =
                new LocalContainerEntityManagerFactoryBean();
            localContainerEntityManagerFactoryBean.setDataSource(dataSource());
            localContainerEntityManagerFactoryBean.setPersistenceUnitName(UNITE_PERSISTANCE_PRINCIPAL);
            localContainerEntityManagerFactoryBean.setPackagesToScan(new String[] {"fr.gouv.finances.*.*.entite"});

            final HibernateJpaVendorAdapter hibernateJpaVendorAdapter = new HibernateJpaVendorAdapter();
            // CF: false par défaut
            // hibernateJpaVendorAdapter.setShowSql(false);
            hibernateJpaVendorAdapter.setGenerateDdl(false);
            hibernateJpaVendorAdapter.setDatabasePlatform("org.hibernate.dialect.PostgreSQLDialect");

            localContainerEntityManagerFactoryBean.setJpaVendorAdapter(hibernateJpaVendorAdapter);
            localContainerEntityManagerFactoryBean.setJpaProperties(additionalProperties());

            return localContainerEntityManagerFactoryBean;
        }

        @Bean(name = "datasource", destroyMethod = "close")
        public DataSource dataSource()
        {
            LOGGER.debug(">>> Instanciation du bean datasource");
            org.apache.tomcat.jdbc.pool.DataSource dataSource = new org.apache.tomcat.jdbc.pool.DataSource(pool());
            return dataSource;
        }

        @Bean
        public PlatformTransactionManager transactionManager()
        {
            LOGGER.debug(">>> Instanciation du bean transactionManager");
            JpaTransactionManager jpaTransactionManager = new JpaTransactionManager();
            jpaTransactionManager.setEntityManagerFactory(entityManagerFactoryBean().getObject());
            jpaTransactionManager.setJpaDialect(new HibernateJpaDialect());
            return jpaTransactionManager;
        }

        @Bean
        public org.apache.tomcat.jdbc.pool.PoolProperties pool()
        {
            LOGGER.debug(">>> Instanciation du bean pool()");
            PoolProperties poolProperties = new PoolProperties();
            poolProperties.setInitialSize(initialSize);
            poolProperties.setMaxActive(maxActive);
            poolProperties.setMaxIdle(maxIdle);
            poolProperties.setMinEvictableIdleTimeMillis(minEvictableIdleTimeMillis);
            poolProperties.setTimeBetweenEvictionRunsMillis(timeBetweenEvictionRunsMillis);
            poolProperties.setTestWhileIdle(true);
            poolProperties.setTestOnReturn(true);
            poolProperties.setTestOnBorrow(false);
            poolProperties.setValidationQuery("select 1");
            poolProperties.setDefaultReadOnly(false);
            poolProperties.setDefaultAutoCommit(true);
            poolProperties.setDriverClassName(dataBaseDriverClassName);
            poolProperties.setUrl(databaseURL);
            poolProperties.setUsername(databaseUsername);
            poolProperties.setPassword(databasePassword);
            return poolProperties;
        }

        // **********************
        // Profil edition
        // **********************

        @Bean(name = "entityManagerEdition")
        // @Profile("edition")
        @ConditionalOnProperty(name = "lombok.composant.edition.inclus", havingValue = "true")
        public LocalContainerEntityManagerFactoryBean entityManagerEditionFactoryBean()
        {
            LOGGER.debug(">>> Instanciation du bean entityManagerEditionFactoryBean");
            final LocalContainerEntityManagerFactoryBean localContainerEntityManagerFactoryBean =
                new LocalContainerEntityManagerFactoryBean();
            localContainerEntityManagerFactoryBean.setDataSource(editionDatasource());
            localContainerEntityManagerFactoryBean.setPersistenceUnitName(UNITE_PERSISTANCE_EDITION);
            localContainerEntityManagerFactoryBean.setPackagesToScan(new String[] {"fr.gouv.finances.*.*.entite"});

            final HibernateJpaVendorAdapter hibernateJpaVendorAdapter = new HibernateJpaVendorAdapter();
            // CF: false par défaut
            // hibernateJpaVendorAdapter.setShowSql(false);
            hibernateJpaVendorAdapter.setGenerateDdl(false);
            hibernateJpaVendorAdapter.setDatabasePlatform("org.hibernate.dialect.PostgreSQLDialect");

            localContainerEntityManagerFactoryBean.setJpaVendorAdapter(hibernateJpaVendorAdapter);
            localContainerEntityManagerFactoryBean.setJpaProperties(additionalProperties());

            return localContainerEntityManagerFactoryBean;
        }

        @Bean
        // @Profile("edition")
        @ConditionalOnProperty(name = "lombok.composant.edition.inclus", havingValue = "true")
        public PlatformTransactionManager editionTransactionManager()
        {
            LOGGER.debug(">>> Instanciation du bean transactionManager");
            JpaTransactionManager jpaTransactionManager = new JpaTransactionManager();
            jpaTransactionManager.setEntityManagerFactory(entityManagerEditionFactoryBean().getObject());
            jpaTransactionManager.setJpaDialect(new HibernateJpaDialect());
            return jpaTransactionManager;
        }

        @Bean
        // @Profile("edition")
        @ConditionalOnProperty(name = "lombok.composant.edition.inclus", havingValue = "true")
        public DataSource editionDatasource()
        {
            LOGGER.debug(">>> Instanciation du bean editionDatasource()");
            org.apache.tomcat.jdbc.pool.DataSource dataSource = new org.apache.tomcat.jdbc.pool.DataSource(editionpool());
            return dataSource;
        }

        @Bean
        // @Profile("edition")
        @ConditionalOnProperty(name = "lombok.composant.edition.inclus", havingValue = "true")
        public org.apache.tomcat.jdbc.pool.PoolProperties editionpool()
        {
            LOGGER.debug(">>> Instanciation du bean editionpool()");
            PoolProperties poolProperties = new PoolProperties();
            poolProperties.setInitialSize(jasperInitialSize);
            poolProperties.setMaxActive(jasperMaxActive);
            poolProperties.setMaxIdle(jasperMaxIdle);
            poolProperties.setMinEvictableIdleTimeMillis(minEvictableIdleTimeMillis);
            poolProperties.setTimeBetweenEvictionRunsMillis(timeBetweenEvictionRunsMillis);
            poolProperties.setTestWhileIdle(true);
            poolProperties.setTestOnReturn(true);
            poolProperties.setTestOnBorrow(false);
            poolProperties.setValidationQuery("select 1");
            poolProperties.setDefaultReadOnly(false);
            poolProperties.setDefaultAutoCommit(true);
            poolProperties.setDriverClassName(jasperDatabaseDriverClassName);
            poolProperties.setUrl(jasperDatabaseURL);
            poolProperties.setUsername(jasperDatabaseUsername);
            poolProperties.setPassword(jasperDatabasePassword);
            return poolProperties;
        }

        @Bean(destroyMethod = "close")
        // @Profile("edition")
        @ConditionalOnProperty(name = "lombok.composant.edition.inclus", havingValue = "true")
        public DataSource editionDS()
        {
            LOGGER.debug(">>> Instanciation du bean editionDS()");
            org.apache.tomcat.jdbc.pool.DataSource dataSource = new org.apache.tomcat.jdbc.pool.DataSource();
            dataSource.setDriverClassName(jasperDatabaseDriverClassName);
            dataSource.setUrl(jasperDatabaseURL);
            dataSource.setUsername(jasperDatabaseUsername);
            dataSource.setPassword(jasperDatabasePassword);

            return dataSource;
        }

        private final Properties additionalProperties()
        {
            final Properties hibernateProperties = new Properties();
            // hibernateProperties.setProperty("hibernate.hbm2ddl.auto", "validate");
            // CF : false par défaut
            // hibernateProperties.setProperty("hibernate.use_sql_comments", "false");

            // Pour que le démarrage soit plus rapide :
            // https://stackoverflow.com/questions/40678758/spring-boot-hibernate-slow-startup
            hibernateProperties.setProperty("hibernate.temp.use_jdbc_metadata_defaults", "false");
            return hibernateProperties;
        }

        @Bean
        public JdbcTemplate jdbcTemplate()
        {
            LOGGER.debug(">>> Instanciation du bean jdbcTemplate");
            JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource());
            return jdbcTemplate;
        }
    }

}
