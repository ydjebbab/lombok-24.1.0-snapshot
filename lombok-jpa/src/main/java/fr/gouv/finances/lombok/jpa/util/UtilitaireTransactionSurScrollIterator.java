package fr.gouv.finances.lombok.jpa.util;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.AbstractPlatformTransactionManager;
import org.springframework.transaction.support.DefaultTransactionDefinition;

public class UtilitaireTransactionSurScrollIterator
{
    protected static final Log log = LogFactory.getLog(UtilitaireTransactionSurScrollIterator.class);

    /**
     * methode Commit transaction 
     * 
     * @param jpaTransactionManager 
     * @param transactionStatus
     */
    public static void commiterTransaction(JpaTransactionManager jpaTransactionManager, TransactionStatus transactionStatus)
    {
        jpaTransactionManager.commit(transactionStatus);
        log.debug("Commit d'une transaction effectué");
    }

    /**
     * Demarrer une transaction
     * 
     * @param jpaTransactionManager 
     * @param name 
     * @param propagationBehavior 
     * @param readonly 
     * @return transactionStatus
     */
    public static TransactionStatus demarrerTransaction(JpaTransactionManager jpaTransactionManager, String name,
        int propagationBehavior, boolean readonly)
    {
        DefaultTransactionDefinition def = new DefaultTransactionDefinition();
        def.setName(name);
        def.setPropagationBehavior(propagationBehavior);
        def.setReadOnly(readonly);
        TransactionStatus transactionStatus = jpaTransactionManager.getTransaction(def);
        log.debug("Démarrage d'une transaction");
        return transactionStatus;
    }

    /**
     * Demarrer une transaction englobante de lecture scroll iterator 
     * 
     * @param jpaTransactionManager 
     * @return transactionStatus
     */
    public static TransactionStatus demarrerTransactionEnglobanteDeLectureScrollIterator(
        JpaTransactionManager jpaTransactionManager)
    {
        TransactionStatus transactionStatus =
            demarrerTransaction(jpaTransactionManager, "Transaction englobante lecture seule",
                TransactionDefinition.PROPAGATION_NESTED, true);
        log.debug("Démarrage d'une transaction englobante effectué");
        return transactionStatus;
    }

    /**
     * Demarrer une transaction englobee de mise a jour d un lot 
     * 
     * @param jpaTransactionManager 
     * @return transactionStatus
     */
    public static TransactionStatus demarrerTransactionEnglobeeDeMiseAJourDUnLot(
        JpaTransactionManager jpaTransactionManager)
    {
        TransactionStatus transactionStatus =
            demarrerTransaction(jpaTransactionManager, "transaction englobee mise à jour",
                TransactionDefinition.PROPAGATION_REQUIRES_NEW, false);
        log.debug("Démarrage d'une transaction englobée effectué");
        return transactionStatus;
    }

    /**
     * Rollback transaction 
     * 
     * @param abstractPlatformTransactionManager 
     * @param transactionStatus 
     */
    public static void rollbackerTransaction(AbstractPlatformTransactionManager abstractPlatformTransactionManager,
        TransactionStatus transactionStatus)
    {
        abstractPlatformTransactionManager.rollback(transactionStatus);
        log.debug("Rollback d'une transaction effectué");
    }

}
