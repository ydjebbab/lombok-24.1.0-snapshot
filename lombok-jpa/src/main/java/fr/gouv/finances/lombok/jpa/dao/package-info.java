/*
 * Copyright (c) 2017 DGFiP - Tous droits réservés
 * 
 */
/**
 * Paquet contenant le socle JPA.
 *
 * @author Christophe Breheret-Girardin
 */
package fr.gouv.finances.lombok.jpa.dao;