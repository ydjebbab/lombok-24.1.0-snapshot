/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 *
 */
package fr.gouv.finances.lombok.monnaie.jpa.dao.impl;

import java.util.Collections;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Root;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Repository;

import fr.gouv.finances.lombok.jpa.dao.BaseDaoJpaImpl;
import fr.gouv.finances.lombok.monnaie.bean.Monnaie;
import fr.gouv.finances.lombok.monnaie.dao.MonnaieDao;

/**
 * Implémentation JPA du DAO permettant de gérer les données monnaie.
 *
 * @author Christophe Breheret-Girardin
 */
@ConditionalOnProperty(name = "lombok.orm.jpa", havingValue="true")
@Repository("monnaiedaoimpl")
public class MonnaieDaoImpl extends BaseDaoJpaImpl implements MonnaieDao
{

    /** 
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.monnaie.dao.MonnaieDao#deleteMonnaie(
     * fr.gouv.finances.lombok.monnaie.bean.Monnaie)
     */
    @Override
    public void deleteMonnaie(Monnaie uneMonnaie)
    {
        deleteObject(uneMonnaie);
    }

    /** 
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.monnaie.dao.MonnaieDao#findAllMonnaies()
     */
    @Override
    public List<Monnaie> findAllMonnaies()
    {
        // Récupération de toutes les monnaies et des taux de chancellerie associés
        return findAllWithJoins(Monnaie.class, Collections.singletonMap("listTauxChancel", JoinType.LEFT));
    }

    /** 
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.monnaie.dao.MonnaieDao#findAllMonnaiesTriSurLibelle()
     */
    @Override
    public List<Monnaie> findAllMonnaiesTriSurLibelle()
    {
        // Paramétrage de la recherche
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Monnaie> query = criteriaBuilder.createQuery(Monnaie.class);
        Root<Monnaie> root = query.from(Monnaie.class);

        // Paramètrage des jointures
        root.fetch("listTauxChancel", JoinType.LEFT);
        
        // Paramétrage du tri
        query.orderBy(criteriaBuilder.asc(root.get("libelleMonnaie")));
        
        // Exécution de la recherche
        return findAll(query.distinct(true));
    }

    /** 
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.monnaie.dao.MonnaieDao#findMonnaieParCode(java.lang.String)
     */
    @Override
    public Monnaie findMonnaieParCode(String code)
    {
        return findbyUniqueCriteria(Monnaie.class, "codeBDF", code);
    }

    /** 
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.monnaie.dao.MonnaieDao#modifyMonnaie(fr.gouv.finances.lombok.monnaie.bean.Monnaie)
     */
    @Override
    public void modifyMonnaie(Monnaie uneMonnaie)
    {
        modifyObject(uneMonnaie);
    }

    /** 
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.monnaie.dao.MonnaieDao#saveMonnaie(fr.gouv.finances.lombok.monnaie.bean.Monnaie)
     */
    @Override
    public boolean saveMonnaie(Monnaie uneMonnaie)
    {
        return saveMonnaieEtTaux(uneMonnaie);
    }

    /** 
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.monnaie.dao.MonnaieDao#saveMonnaieEtTaux(fr.gouv.finances.lombok.monnaie.bean.Monnaie)
     */
    @Override
    public boolean saveMonnaieEtTaux(Monnaie uneMonnaie)
    {
        if (this.findMonnaieParCode(uneMonnaie.getCodeBDF()) == null)
        {
            saveObject(uneMonnaie);
            return true;
        }
        else
        {
            // Pas de sauvegarde pour un code BDF
            return false;
        }
    }
}
