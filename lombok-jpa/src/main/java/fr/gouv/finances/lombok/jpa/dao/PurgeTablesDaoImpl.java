/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 */
package fr.gouv.finances.lombok.jpa.dao;

import java.util.Arrays;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import fr.gouv.finances.lombok.util.base.PurgeTablesDao;

/**
 * Implémentation JPA de la purge de tables de la base de données
 *
 * @author Christophe Breheret-Girardin
 */
@ConditionalOnProperty(name = "lombok.orm.jpa", havingValue="true")
@Repository("purgetablesdao")
@Transactional
public class PurgeTablesDaoImpl extends BaseDaoJpaImpl implements PurgeTablesDao
{

    /**
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.util.base.PurgeTablesDao#purgerTables(java.lang.String[])
     */
    @Override
    public void purgerTables(String... nomsTable)
    {
        // Exécution d'un ordre pour tronquer chaque table
        // (instruction immédiate par rapport à une suppression, et qui ne nécessite pas d'instruction "commit")
        Arrays.asList(nomsTable).stream().forEach(
            nomTable -> entityManager.createNativeQuery("TRUNCATE TABLE " + nomTable).executeUpdate());
    }

}