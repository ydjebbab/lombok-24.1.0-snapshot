/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 *
 */
package fr.gouv.finances.lombok.test.dao;

import java.util.List;

import javax.persistence.criteria.CriteriaQuery;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import fr.gouv.finances.lombok.jpa.dao.BaseDaoJpaImpl;
import fr.gouv.finances.lombok.util.persistance.ScrollIterator;

/**
 * Implémentation JPA d'un DAO sur des classes d'élèves.
 *
 * @author Christophe Breheret-Girardin
 */
@ConditionalOnProperty(name = "lombok.orm.jpa", havingValue="true")
@Repository("classedaoimpl")
@Transactional
class ClasseDaoImpl extends BaseDaoJpaImpl implements ClasseDao
{

    /** 
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.test.dao.ClasseDao#getScrollIterator().
     */
    @Override
    public ScrollIterator getScrollIterator(int nombreOccurences)
    {
        // Paramètrage de la recherche
        CriteriaQuery<Classe> query = createQuery(Classe.class);
        return getScrollIterator(query, nombreOccurences);
    }

    /** 
     * {@inheritDoc}<br/>
     * <u>Remarque</u> : avec l'implémentation JPA, cette méthode fait appel à la méthode
     * fr.gouv.finances.lombok.test.dao.ClasseDao#getMaxClassesViaContextePersistance(int).
     * @see fr.gouv.finances.lombok.test.dao.ClasseDao#getMaxClassesEtanche(int).
     */
    @Override
    public List<Classe> getMaxClassesEtanche(int maxResultat)
    {
        return getMaxClassesViaContextePersistance(maxResultat);
    }

    /** 
     * {@inheritDoc}<br/>
     * <u>Remarque</u> : n'est pas possible avec l'implémentation JPA
     * @see fr.gouv.finances.lombok.test.dao.ClasseDao#getMaxClassesNonEtanche(int).
     * 
     */
    @Override
    public List<Classe> getMaxClassesNonEtanche(int maxResultat)
    {
        throw new UnsupportedOperationException("Fonctionnalité non implémentée");
    }

    /** 
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.test.dao.ClasseDao#getMaxClassesViaContextePersistance(int).
     */
    @Override
    public List<Classe> getMaxClassesViaContextePersistance(int maxResultat)
    {
        return findAll(createQuery(Classe.class), maxResultat);
    }
    
    /** 
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.test.dao.ClasseDao#getClasses().
     */
    @Override
    public List<Classe> getClasses()
    {
        return findAll(createQuery(Classe.class));
    }

    /** 
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.test.dao.ClasseDao#setMaxResultat(int).
     */
    @Override
    public void setMaxResultat(int maxResultat)
    {
        throw new UnsupportedOperationException("Fonctionnalité non implémentée");
    }
}