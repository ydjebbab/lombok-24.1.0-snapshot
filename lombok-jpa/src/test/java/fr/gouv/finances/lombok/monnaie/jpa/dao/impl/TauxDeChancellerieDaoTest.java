package fr.gouv.finances.lombok.monnaie.jpa.dao.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.data.jpa.JpaRepositoriesAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import fr.gouv.finances.lombok.monnaie.bean.TauxDeChancellerie;
import fr.gouv.finances.lombok.monnaie.config.PersistenceJPAConfigTest;
import fr.gouv.finances.lombok.monnaie.dao.TauxDeChancellerieDao;
import fr.gouv.finances.lombok.monnaie.dao.TauxDeChancellerieUtils;
import fr.gouv.finances.lombok.test.controle.ControleDonnees;
import fr.gouv.finances.lombok.util.base.PurgeTablesDao;

/**
 * Tests unitaires automatisés du DAO JPA, gérant les données de taux de chancellerie.
 *
 * @author Christophe Breheret-Girardin
 * @author CF
 */
@ComponentScan(basePackages = {"fr.gouv.finances.lombok.jpa.dao", "fr.gouv.finances.lombok.monnaie.jpa.dao.impl"})
@EnableAutoConfiguration(exclude = JpaRepositoriesAutoConfiguration.class)
@SpringBootTest(classes = {PersistenceJPAConfigTest.class}, properties = {"lombok.orm.jpa=true"})
@ActiveProfiles(profiles = {"embedded"})
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class TauxDeChancellerieDaoTest
{

    public TauxDeChancellerieDaoTest()
    {
        super();
    }
        
    protected static final Logger LOGGER = LoggerFactory.getLogger(TauxDeChancellerieDaoTest.class);

    /** Déclaration du DAO de gestion des données du taux de chancellerie. */
    @Autowired
    private TauxDeChancellerieDao tauxdechancelleriedao;

    /** Déclaration du DAO de gestion des purges de la base. */
    @Autowired
    private PurgeTablesDao purgetablesdao;

    /** Map contenant les paramètres de taux de chancellerie présents en base. */
    private Map<Integer, TauxDeChancellerie> mapTauxDeChancellerie = new HashMap<>();

    /**
     * Initialisation des données en base avant chaque test.
     */
    @Before
    public void creerDonnees()
    {
        // Purge des données de la base
        purgetablesdao.purgerTables("TAUX_TAUX", "MONNAIE_MONN");

        // Vérification de l'absence de données
        ControleDonnees.verifierElements(tauxdechancelleriedao.loadAllObjects(TauxDeChancellerie.class));

        // Génération et enregistrement des taux de chancellerie
        mapTauxDeChancellerie = IntStream.rangeClosed(1, 5)
            .mapToObj(TauxDeChancellerieUtils::getTauxDeChancellerie)
            .peek(tauxdechancelleriedao::saveObject)
            .collect(Collectors.toMap(t -> t.getValeurTauxChancel().intValue(), t -> t));

        // Vérification de la présence des données avant exécution du test
        ControleDonnees.verifierElements(tauxdechancelleriedao.loadAllObjects(TauxDeChancellerie.class)
            , mapTauxDeChancellerie.get(1), mapTauxDeChancellerie.get(2), mapTauxDeChancellerie.get(3)
            , mapTauxDeChancellerie.get(4), mapTauxDeChancellerie.get(5));

        // Suppression du cache pour forcer les requêtes des tests
        tauxdechancelleriedao.clearPersistenceContext();
    }

    /**
     * Méthode permettant de tester l'injection du DAO.
     */
    @Test
    public void injectionDao()
    {
        assertNotNull(tauxdechancelleriedao);
    }

    /**
     * Test de la méthode
     * fr.gouv.finances.lombok.monnaie.dao.TauxDeChancellerieDao#deleteTauxChancellerie(
     * fr.gouv.finances.lombok.monnaie.bean.TauxDeChancellerie).
     */
    @Test
    public void deleteTauxChancellerie()
    {
        // Exécution de la méthode à tester et vérifications
        tauxdechancelleriedao.deleteTauxChancellerie(mapTauxDeChancellerie.get(2));
        ControleDonnees.verifierElements(tauxdechancelleriedao.loadAllObjects(TauxDeChancellerie.class)
            , mapTauxDeChancellerie.get(1), mapTauxDeChancellerie.get(3)
            , mapTauxDeChancellerie.get(4), mapTauxDeChancellerie.get(5));
    }

    /**
     * Test de la méthode
     * fr.gouv.finances.lombok.monnaie.dao.TauxDeChancellerieDao#saveTauxChancellerie(
     * fr.gouv.finances.lombok.monnaie.bean.TauxDeChancellerie).
     */
    @Test
    public void saveTauxChancellerie()
    {
        // Exécution de la méthode à tester
        TauxDeChancellerie tauxDeChancellerie = TauxDeChancellerieUtils.getTauxDeChancellerie(6);
        tauxdechancelleriedao.saveTauxChancellerie(tauxDeChancellerie);

        // Vérifications
        ControleDonnees.verifierElements(tauxdechancelleriedao.loadAllObjects(TauxDeChancellerie.class)
            , mapTauxDeChancellerie.get(1), mapTauxDeChancellerie.get(2), mapTauxDeChancellerie.get(3)
            , mapTauxDeChancellerie.get(4), mapTauxDeChancellerie.get(5)
            , tauxDeChancellerie);
    }

    /**
     * Test de la méthode
     * fr.gouv.finances.lombok.monnaie.dao.TauxDeChancellerieDao#modifyTauxChancellerie(
     * fr.gouv.finances.lombok.monnaie.bean.TauxDeChancellerie).
     */
    @Test
    public void modifyTauxChancellerie()
    {
        TauxDeChancellerie tauxDeChancellerie = mapTauxDeChancellerie.get(2);
        assertEquals(2d, tauxDeChancellerie.getValeurTauxChancel().doubleValue(), 0);

        // Exécution de la méthode à tester
        tauxDeChancellerie.setValeurTauxChancel(10d);
        tauxdechancelleriedao.modifyTauxChancellerie(tauxDeChancellerie);

        // Vérifications
        assertEquals(10d
            , tauxdechancelleriedao.getObject(TauxDeChancellerie.class, tauxDeChancellerie.getId())
                .getValeurTauxChancel().doubleValue(), 0);
    }

}