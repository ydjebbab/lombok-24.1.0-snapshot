package fr.gouv.finances.lombok.monnaie.config;

import java.util.Properties;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaDialect;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.support.TransactionTemplate;

/**
 * 
 * @author celfer Date: 11 oct. 2019
 */
@Configuration
@EnableTransactionManagement
@ComponentScan({"fr.gouv.finances.*.*.dao"})
@EnableJpaRepositories(basePackages = "fr.gouv.finances.*.*.dao")
@ConditionalOnProperty(name = "lombok.orm.jpa", havingValue="true")
public class PersistenceJPAConfigTest
{
    protected static final Logger LOGGER = LoggerFactory.getLogger(PersistenceJPAConfigTest.class);

    private static final String UNITE_PERSISTANCE_PRINCIPAL = "pu-principal";

    public PersistenceJPAConfigTest()
    {
        super();
    }

    /**
     * Configuration JPA pour une base embedded HSQL (profil embedded)
     *
     * @author celfer Date: 17 oct. 2019
     */
    @Configuration
    @Profile("embedded")
    public static class InnerEmbeddedConfig
    {
        /**
         * Construction de l'EMF à partir de la source de données et du choix d'hibernate. 
         */
        // @Bean(name = "entityManagerJPA")
        @Bean(name = "entityManager")
        public LocalContainerEntityManagerFactoryBean entityManagerFactoryBean()
        {
            LOGGER.debug(">>> Instanciation du bean entityManager");
            final LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
            em.setDataSource(dataSource());
            em.setPersistenceUnitName(UNITE_PERSISTANCE_PRINCIPAL);
            em.setPackagesToScan(new String[] {"fr.gouv.finances.*.*.entite"});
            em.setPersistenceXmlLocation("classpath:META-INF/persistence-test.xml");

            final HibernateJpaVendorAdapter hibernateJpaVendorAdapter = new HibernateJpaVendorAdapter();
            // CF: false par défaut
            // hibernateJpaVendorAdapter.setShowSql(false);
            hibernateJpaVendorAdapter.setGenerateDdl(false);
            hibernateJpaVendorAdapter.setDatabasePlatform("org.hibernate.dialect.HSQLDialect");

            em.setJpaVendorAdapter(hibernateJpaVendorAdapter);
            em.setJpaProperties(additionalProperties());

            return em;
        }

        /**
         * Définition de la source de données
         */
        @Bean(name = "datasource")
        public DataSource dataSource()
        {
            LOGGER.debug(">>> Instanciation du bean datasource");
            EmbeddedDatabaseBuilder builder = new EmbeddedDatabaseBuilder();
            EmbeddedDatabase db = builder
                .setType(EmbeddedDatabaseType.HSQL)
                .setName("lombokdbtest")
                .build();
            return db;
        }

        
        /**
         * Construction d'un gestionnaire de transaction en liaison avec l'usine à EM.
         */        
        @Bean
        public PlatformTransactionManager transactionManager()
        {
            LOGGER.debug(">>> Instanciation du bean transactionManager");
            JpaTransactionManager transactionManager = new JpaTransactionManager();
            transactionManager.setEntityManagerFactory(entityManagerFactoryBean().getObject());
            transactionManager.setJpaDialect(new HibernateJpaDialect());
            return transactionManager;
        }

        @Bean
        public PersistenceExceptionTranslationPostProcessor exceptionTranslation()
        {
            return new PersistenceExceptionTranslationPostProcessor();
        }

        @Bean
        @Profile("!batchdao")
        public TransactionTemplate transactionTemplate()
        {
            LOGGER.debug(">>> Instanciation du bean transactionTemplate");
            TransactionTemplate transactionTemplate = new TransactionTemplate();
            transactionTemplate.setTransactionManager(transactionManager());
            return transactionTemplate;
        }

        private final Properties additionalProperties()
        {
            final Properties hibernateProperties = new Properties();
            hibernateProperties.setProperty("hibernate.hbm2ddl.auto", "create");
            // CF : false par défaut
            // hibernateProperties.setProperty("hibernate.use_sql_comments", "false");
            // TODO : use_sql_comments et show_sql à true ?
            return hibernateProperties;
        }
    }
}
