/*
 * Copyright (c) 2018 DGFiP - Tous droits réservés
 * 
 */
/**
 * Documentation du paquet fr.gouv.finances.lombok.test.dao
 *
 * @author Christophe Breheret-Girardin
 */
package fr.gouv.finances.lombok.test.dao;