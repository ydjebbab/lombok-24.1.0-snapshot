/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : FiltreValue.java
 *
 */
package fr.gouv.finances.lombok.edition.bean;

import fr.gouv.finances.lombok.util.base.BaseBean;

/**
 * Class FiltreValue --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.2 $ Date: 11 déc. 2009
 */
public class FiltreValue extends BaseBean
{

    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** Identifiant technique utilisé par Hibernate. */
    private Long id;

    /** Flag utilisé pour la gestion du verrouillage optimiste par Hibernate. */
    private int version;

    /** value. */
    private String value;

    /**
     * Instanciation de filtre value.
     */
    public FiltreValue()
    {
        super();
    }

    /**
     * Instanciation de filtre value.
     * 
     * @param value --
     */
    public FiltreValue(String value)
    {
        this.value = value;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param obj
     * @return true, si c'est vrai
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
        {
            return true;
        }
        if (obj == null)
        {
            return false;
        }
        if (getClass() != obj.getClass())
        {
            return false;
        }
        final FiltreValue other = (FiltreValue) obj;
        if (value == null)
        {
            if (other.value != null)
            {
                return false;
            }
        }
        else if (!value.equals(other.value))
        {
            return false;
        }
        return true;
    }

    /**
     * Accesseur de l attribut id.
     * 
     * @return id
     */
    public Long getId()
    {
        return id;
    }

    /**
     * Accesseur de l attribut value.
     * 
     * @return value
     */
    public String getValue()
    {
        return value;
    }

    /**
     * Accesseur de l attribut version.
     * 
     * @return version
     */
    public int getVersion()
    {
        return version;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @return int
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;

        result = prime * result + ((value == null) ? 0 : value.hashCode());
        return result;
    }

    /**
     * Modificateur de l attribut id.
     * 
     * @param id le nouveau id
     */
    public void setId(Long id)
    {
        this.id = id;
    }

    /**
     * Modificateur de l attribut value.
     * 
     * @param value le nouveau value
     */
    public void setValue(String value)
    {
        this.value = value;
    }

    /**
     * Modificateur de l attribut version.
     * 
     * @param version le nouveau version
     */
    public void setVersion(int version)
    {
        this.version = version;
    }

}
