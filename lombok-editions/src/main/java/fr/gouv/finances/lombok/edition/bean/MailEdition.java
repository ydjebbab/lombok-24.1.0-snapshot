/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : MailEdition.java
 *
 */
package fr.gouv.finances.lombok.edition.bean;

import fr.gouv.finances.lombok.util.base.BaseBean;

/**
 * Class MailEdition --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.2 $ Date: 11 déc. 2009
 */
public class MailEdition extends BaseBean
{

    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** Flag qui indique si l'utilisateur doit être averti par mail. */
    private Boolean notificationMail = Boolean.FALSE;

    /** Adresse mail du destinataire du mail. */
    private String mailA;

    /** Adresse mail de l'expéditeur du mail. */
    private String mailDe;

    /** Objet du mail. */
    private String mailObjet;

    /** Message du mail. */
    private String mailMessage;

    /** Flag qui indique si le mail peut être transmis. */
    private Boolean envoiPieceJointe = Boolean.FALSE;

    public MailEdition()
    {
        super();
    }

    /**
     * Instanciation de mail edition.
     * 
     * @param notificationMail --
     * @param envoiPieceJointe --
     */
    public MailEdition(Boolean notificationMail, Boolean envoiPieceJointe)
    {
        super();
        this.notificationMail = notificationMail;
        this.envoiPieceJointe = envoiPieceJointe;
    }

    /**
     * Gets the envoi piece jointe.
     * 
     * @return the envoiPieceJointe
     */
    public Boolean getEnvoiPieceJointe()
    {
        return envoiPieceJointe;
    }

    /**
     * Gets the mail a.
     * 
     * @return the mailA
     */
    public String getMailA()
    {
        return mailA;
    }

    /**
     * Gets the mail de.
     * 
     * @return the mailDe
     */
    public String getMailDe()
    {
        return mailDe;
    }

    /**
     * Gets the mail message.
     * 
     * @return the mailMessage
     */
    public String getMailMessage()
    {
        return mailMessage;
    }

    /**
     * Gets the mail objet.
     * 
     * @return the mailObjet
     */
    public String getMailObjet()
    {
        return mailObjet;
    }

    /**
     * Gets the notification mail.
     * 
     * @return the notificationMail
     */
    public Boolean getNotificationMail()
    {
        return notificationMail;
    }

    /**
     * Indique si l'édition est notifiée par mail.
     * 
     * @return true, if checks if is notifiee par mail
     */
    public boolean isNotifieeParMail()
    {
        boolean result;
        if (this.getNotificationMail() == null)
        {
            result = false;
        }
        else
        {
            result = this.getNotificationMail().booleanValue();
        }
        return result;
    }

    /**
     * Indique si l'édition est transmise en pièce jointe du mail.
     * 
     * @return true, if checks if is piece a joindre
     */
    public boolean isPieceAJoindre()
    {
        boolean result;
        if (this.getEnvoiPieceJointe() == null)
        {
            result = false;
        }
        else
        {
            result = this.getEnvoiPieceJointe().booleanValue();
        }
        return result;
    }

    /**
     * Sets the envoi piece jointe.
     * 
     * @param envoiPieceJointe the envoiPieceJointe to set
     */
    public void setEnvoiPieceJointe(Boolean envoiPieceJointe)
    {
        this.envoiPieceJointe = envoiPieceJointe;
    }

    /**
     * Sets the mail a.
     * 
     * @param mailA the mailA to set
     */
    public void setMailA(String mailA)
    {
        this.mailA = mailA;
    }

    /**
     * Sets the mail de.
     * 
     * @param mailDe the mailDe to set
     */
    public void setMailDe(String mailDe)
    {
        this.mailDe = mailDe;
    }

    /**
     * Sets the mail message.
     * 
     * @param mailMessage the mailMessage to set
     */
    public void setMailMessage(String mailMessage)
    {
        this.mailMessage = mailMessage;
    }

    /**
     * Sets the mail objet.
     * 
     * @param mailObjet the mailObjet to set
     */
    public void setMailObjet(String mailObjet)
    {
        this.mailObjet = mailObjet;
    }

    /**
     * Sets the notification mail.
     * 
     * @param notificationMail the notificationMail to set
     */
    public void setNotificationMail(Boolean notificationMail)
    {
        this.notificationMail = notificationMail;
    }

}
