/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : JobHistory.java
 *
 */
package fr.gouv.finances.lombok.edition.bean;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.Objects;

import fr.gouv.finances.lombok.util.base.BaseBean;
import fr.gouv.finances.lombok.util.date.TemporaliteUtils;

/**
 * Class JobHistory --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.3 $ Date: 11 déc. 2009
 */
public class JobHistory extends BaseBean implements EditionAsynchroneNotifieeParMail, EditionSynchroneNotifieeParMail,
    EditionAsynchroneNonNotifieeParMail, EditionSynchroneNonNotifieeParMail
{

    /** Constant : STATUS_INITIALISE. */
    public static final String STATUS_INITIALISE = "INITIALISE - NON DECLENCHE";

    /** Constant : STATUS_EN_ATTENTE_EXECUTION. */
    public static final String STATUS_EN_ATTENTE_EXECUTION = "EN ATTENTE D'EXECUTION";

    /** Constant : STATUS_EN_COURS_EXECUTION. */
    public static final String STATUS_EN_COURS_EXECUTION = "EN COURS D'EXECUTION";

    /** Constant : STATUS_DISPONIBLE. */
    public static final String STATUS_DISPONIBLE = "DISPONIBLE";

    /** Constant : STATUS_DISPONIBLE_NON_ENVOYE. */
    public static final String STATUS_DISPONIBLE_NON_ENVOYE = "DISPONIBLE NON ENVOYE";

    /** Constant : STATUS_ECHEC. */
    public static final String STATUS_ECHEC = "EN ECHEC";

    /** Constant : MODE_SOUM_SYNCH. */
    public static final String MODE_SOUM_SYNCH = "SYNCHRONE";

    /** Constant : MODE_SOUM_ASYNC_IMDIAT. */
    public static final String MODE_SOUM_ASYNC_IMDIAT = "ASYNCHRONE IMMEDIAT";

    /** Constant : MODE_SOUM_ASYNC_DIFF. */
    public static final String MODE_SOUM_ASYNC_DIFF = "ASYNCHRONE DIFFERE";

    /** Constant : ATTR_EDITION_UUID. */
    public static final String ATTR_EDITION_UUID = "editionUuid";

    /** Constant : ATTR_USER_UID. */
    public static final String ATTR_USER_UID = "uid";

    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /*
     * ----------------------------------------------------------------------- identifiants de l'édition
     * ----------------------------------------------------------------------
     */

    /** Identifiant technique utilisé par Hibernate. */
    private Long id;

    /** Flag utilisé pour la gestion du verrouillage optimiste par Hibernate. */
    private int version;

    /** Id du bean représentant l'édition dans le contexte Spring. */
    private String beanEditionId;

    /** UUID de l'édition. */
    private String editionUuid;

    /** Stockage de l'édition dans le SF. */
    private StockageEdition stockageEdition;

    /** Ordonnancement de l'édition. */
    private OrdoEdition ordoEdition;

    /** Information par mail de la production de l'édition. */
    private MailEdition mailEdition;

    /** Archivage des éditions. */
    private ArchivageEdition archivageEdition;

    /** Présentation de l'édition à l'utilisateur. */
    private PresentationEdition presentationEdition;

    /** Utilisateurs destinataires de l'édition. */
    private DestinationEdition destinationEdition;

    /** Propriété non persistante Jour de la demande (heures, minutes, secondes à zéro). */
    private Date jourDemande;

    /**
     * Instanciation de job history.
     */
    public JobHistory()
    {
        this.ordoEdition = new OrdoEdition(Integer.valueOf("0"));
        this.stockageEdition = new StockageEdition(Integer.valueOf("-1"));
        this.presentationEdition = new PresentationEdition();
        this.archivageEdition = new ArchivageEdition(Integer.valueOf("0"));
        this.mailEdition = new MailEdition();
        this.destinationEdition = new DestinationEdition();
    }

    /**
     * Instanciation de job history.
     * 
     * @param beanEditionId --
     * @param editionUuid --
     * @param delaiLancement --
     * @param notificationMail --
     * @param envoiPieceJointe --
     * @param nbJStock --
     */
    public JobHistory(String beanEditionId, String editionUuid, Integer delaiLancement, Boolean notificationMail,
        Boolean envoiPieceJointe, Integer nbJStock)
    {
        this.beanEditionId = beanEditionId;
        this.editionUuid = editionUuid;
        this.ordoEdition = new OrdoEdition(delaiLancement);
        if (nbJStock == null)
        {
            this.archivageEdition = new ArchivageEdition(Integer.valueOf("0"));
        }
        else
        {
            this.archivageEdition = new ArchivageEdition(nbJStock);
        }
        this.mailEdition = new MailEdition(notificationMail, envoiPieceJointe);
        this.stockageEdition = new StockageEdition(Integer.valueOf("-1"));
        this.presentationEdition = new PresentationEdition();
        this.destinationEdition = new DestinationEdition();
    }

    /**
     * Méthode permettant de calculer le temps d'exécution d'une édition et le jour demandé.
     */
    public void caluler()
    {
        // Calcul du temps d'exécution
        if (getOrdoEdition() != null
            && getOrdoEdition().getEndTime() != null && getOrdoEdition().getStartTime() != null)
        {
            getOrdoEdition().setTempsExecution(
                String.valueOf(getOrdoEdition().getEndTime().getTime() - getOrdoEdition().getStartTime().getTime()));
        }

        // Récupération du jour demandé à partir de la date demandée, en ne tenant pas compte
        // de la partie horaire
        if (getOrdoEdition() != null && getOrdoEdition().getDateDemande() != null)
        {
            LocalDateTime localDateTime = TemporaliteUtils.convert(getOrdoEdition().getDateDemande());
            jourDemande = TemporaliteUtils.convert(TemporaliteUtils.supprimerHoraire(localDateTime));
        }
    }

    /** 
     * {@inheritDoc}
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode()
    {
        return Objects.hash(editionUuid);
    }
    
    /** 
     * {@inheritDoc}
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj)
    {
        if (obj == null)
        {
            return false;
        }

        if (this.getClass() != obj.getClass())
        {
            return false;
        }

        JobHistory jobHistory = (JobHistory) obj;
        return Objects.equals(editionUuid, jobHistory.getEditionUuid());
    }

    /**
     * Gets the archivage edition.
     * 
     * @return the archivageEdition
     */
    @Override
    public ArchivageEdition getArchivageEdition()
    {
        return archivageEdition;
    }

    /**
     * Gets the bean edition id.
     * 
     * @return the beanEditionId
     */
    @Override
    public String getBeanEditionId()
    {
        return beanEditionId;
    }

    /**
     * Gets the destination edition.
     * 
     * @return the destinationEdition
     */
    @Override
    public DestinationEdition getDestinationEdition()
    {
        return destinationEdition;
    }

    /**
     * Gets the edition uuid.
     * 
     * @return the editionUuid
     */
    @Override
    public String getEditionUuid()
    {
        return editionUuid;
    }

    /**
     * Gets the id.
     * 
     * @return the id
     */
    public Long getId()
    {
        return id;
    }

    /**
     * Accesseur de l attribut jour demande.
     * 
     * @return jour demande
     */
    public Date getJourDemande()
    {
        return jourDemande;
    }

    /**
     * Gets the mail edition.
     * 
     * @return the mailEdition
     */
    @Override
    public MailEdition getMailEdition()
    {
        return mailEdition;
    }

    /**
     * Gets the ordo edition.
     * 
     * @return the ordoEdition
     */
    @Override
    public OrdoEdition getOrdoEdition()
    {
        return ordoEdition;
    }

    /**
     * Gets the presentation edition.
     * 
     * @return the presentationEdition
     */
    public PresentationEdition getPresentationEdition()
    {
        return presentationEdition;
    }

    /**
     * Retourne le statut de l'édition.
     * 
     * @return the statut edition
     */
    @Override
    public String getStatutEdition()
    {
        String result;
        if (this.getOrdoEdition() != null)
        {
            result = this.getOrdoEdition().getStatus();
        }
        else
        {
            result = "";
        }
        return result;
    }

    /**
     * Gets the stockage edition.
     * 
     * @return the stockageEdition
     */
    public StockageEdition getStockageEdition()
    {
        return stockageEdition;
    }

    /**
     * Accesseur de l attribut version.
     * 
     * @return version
     */
    public int getVersion()
    {
        return version;
    }

    /**
     * Flag qui indique si l'état de l'édition lui permet d'être consultée.
     * 
     * @return true, if checks if is disponible
     */
    @Override
    public boolean isDisponible()
    {
        boolean result = false;
        if (this.getOrdoEdition() != null
            && (JobHistory.STATUS_DISPONIBLE.equals(this.getOrdoEdition().getStatus()) || JobHistory.STATUS_DISPONIBLE_NON_ENVOYE
                .equals(this.getOrdoEdition().getStatus())))
        {
            result = true;
        }

        return result;
    }

    /**
     * Retourne true si l'édition est en attente ou en cours d'exécution.
     * 
     * @return true, if checks if is en cours ou attente execution
     */
    public boolean isEnCoursOuAttenteExecution()
    {
        return (JobHistory.STATUS_EN_ATTENTE_EXECUTION.equals(this.getOrdoEdition().getStatus()) || JobHistory.STATUS_EN_COURS_EXECUTION
            .equals(this.getOrdoEdition().getStatus()));
    }

    /**
     * Retourne true si l'édition est asynchrone, false sinon.
     * 
     * @return true, if checks if is job asynchrone
     */
    public boolean isJobAsynchrone()
    {
        return (JobHistory.MODE_SOUM_ASYNC_DIFF.equals(this.getOrdoEdition().getModeSoumission()) || JobHistory.MODE_SOUM_ASYNC_IMDIAT
            .equals(this.getOrdoEdition().getModeSoumission()));
    }

    /**
     * Sets the archivage edition.
     * 
     * @param archivageEdition the archivageEdition to set
     */
    public void setArchivageEdition(ArchivageEdition archivageEdition)
    {
        this.archivageEdition = archivageEdition;
    }

    /**
     * Sets the bean edition id.
     * 
     * @param beanEditionId the beanEditionId to set
     */
    public void setBeanEditionId(String beanEditionId)
    {
        this.beanEditionId = beanEditionId;
    }

    /**
     * Sets the destination edition.
     * 
     * @param destinationEdition the destinationEdition to set
     */
    public void setDestinationEdition(DestinationEdition destinationEdition)
    {
        this.destinationEdition = destinationEdition;
    }

    /**
     * Sets the edition uuid.
     * 
     * @param editionUuid the editionUuid to set
     */
    public void setEditionUuid(String editionUuid)
    {
        this.editionUuid = editionUuid;
    }

    /**
     * Sets the id.
     * 
     * @param id the id to set
     */
    public void setId(Long id)
    {
        this.id = id;
    }

    /**
     * Modificateur de l attribut jour demande.
     * 
     * @param jourDemande le nouveau jour demande
     */
    public void setJourDemande(Date jourDemande)
    {
        this.jourDemande = jourDemande;
    }

    /**
     * Sets the mail edition.
     * 
     * @param mailEdition the mailEdition to set
     */
    @Override
    public void setMailEdition(MailEdition mailEdition)
    {
        this.mailEdition = mailEdition;
    }

    /**
     * Sets the ordo edition.
     * 
     * @param ordoEdition the ordoEdition to set
     */
    public void setOrdoEdition(OrdoEdition ordoEdition)
    {
        this.ordoEdition = ordoEdition;
    }

    /**
     * Sets the presentation edition.
     * 
     * @param presentationEdition the presentationEdition to set
     */
    public void setPresentationEdition(PresentationEdition presentationEdition)
    {
        this.presentationEdition = presentationEdition;
    }

    /**
     * Sets the stockage edition.
     * 
     * @param stockageEdition the stockageEdition to set
     */
    public void setStockageEdition(StockageEdition stockageEdition)
    {
        this.stockageEdition = stockageEdition;
    }

    /**
     * Modificateur de l attribut version.
     * 
     * @param version le nouveau version
     */
    public void setVersion(int version)
    {
        this.version = version;
    }

}
