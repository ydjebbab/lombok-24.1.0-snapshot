/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : EditionProducerService.java
 *
 */
package fr.gouv.finances.lombok.edition.service;

import java.util.Collection;
import java.util.Map;

import fr.gouv.finances.lombok.edition.bean.EditionAsynchroneNonNotifieeParMail;
import fr.gouv.finances.lombok.edition.bean.EditionAsynchroneNotifieeParMail;
import fr.gouv.finances.lombok.edition.bean.EditionSynchroneNonNotifieeParMail;
import fr.gouv.finances.lombok.edition.bean.EditionSynchroneNotifieeParMail;
import fr.gouv.finances.lombok.edition.bean.JobHistory;
import fr.gouv.finances.lombok.upload.bean.FichierJoint;

/**
 * Interface EditionProducerService --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.2 $ Date: 11 déc. 2009
 */
public interface EditionProducerService
{

    /**
     * Execute une edition de façon synchrone.
     * 
     * @param editionBeanId --
     * @param parametresEdition --
     * @return FichierJoint contenant l'édition produite
     */
    public FichierJoint creerEdition(String editionBeanId, Map parametresEdition);

    /**
     * Execute une edition de façon synchrone, puis la compresse.
     * 
     * @param editionBeanId --
     * @param parametresEdition --
     * @return FichierJoint contenant l'édition produite
     */
    public FichierJoint creerEditionCompressee(String editionBeanId, Map parametresEdition);

    /**
     * methode Creer edition from collection et parametres : --.
     * 
     * @param editionBeanId --
     * @param collection --
     * @param mapParametres --
     * @return fichier joint
     */
    public FichierJoint creerEditionFromCollectionEtParametres(String editionBeanId, Collection collection,
        Map mapParametres);

    /**
     * Crée une édition HTML.
     * 
     * @param editionBeanId --
     * @param mapParametres --
     * @return Map contenant : - le fichier de l'édition sous la clé CpJasperReportsHtmlView.FICHIER_JOINT_KEY - un
     *         objet jasperprint sous la clé CpJasperReportsHtmlView.JASPER_PRINT_KEY
     */
    public Map creerEditionHtml(String editionBeanId, Map mapParametres);

    /**
     * methode Creer edition html from collection et parametres : --.
     * 
     * @param editionBeanId --
     * @param collection --
     * @param mapParametres --
     * @return map
     */
    public Map creerEditionHtmlFromCollectionEtParametres(String editionBeanId, Collection collection, Map mapParametres);

    /**
     * Crée et stocke une édition.
     * 
     * @param jobHistory --
     * @param parametresEdition --
     */
    public void creerEtStockerEdition(JobHistory jobHistory, Map parametresEdition);

    /**
     * Crée, compresse et stocke une édition.
     * 
     * @param jobHistory --
     * @param parametresEdition --
     */
    public void creerEtStockerEditionCompressee(JobHistory jobHistory, Map parametresEdition);

    /**
     * Enregistre une demande d'édition lors de son déclenchement par l'utilisateur.
     * 
     * @param jobHistory --
     * @param status --
     * @return jobHistory
     */
    public JobHistory enregistrerDeclenchement(JobHistory jobHistory, String status);

    /**
     * Initialise une édition asynchrone exécutée après un délai,non notifiée par mail.
     * 
     * @param beanIdEdition identifiant de l'édition
     * @param uidProprietaire uid du propiétaire de l'édition
     * @param delai délai (en minutes) avant l'exécution de l'édition
     * @return EditionSynchroneNonNotifieeParMail
     */
    public EditionAsynchroneNonNotifieeParMail initEditionAsynchroneAvecDelaiNonNotifieeParMail(String beanIdEdition,
        String uidProprietaire, Integer delai);

    /**
     * Initialise une édition asynchrone exécutée après un délai, notifiée par mail.
     * 
     * @param beanIdEdition identifiant de l'édition
     * @param uidProprietaire uid du propiétaire de l'édition
     * @param mailDestinataire adresse mail du propriétaire de l'édition
     * @param delai délai (en minutes) avant l'exécution de l'édition
     * @return EditionSynchroneNotifieeParMail
     */
    public EditionAsynchroneNotifieeParMail initEditionAsynchroneAvecDelaiNotifieeParMail(String beanIdEdition,
        String uidProprietaire, String mailDestinataire, Integer delai);

    /**
     * Initialise une édition asynchrone avec exécution immédiate dès son déclenchement, non notifiée par mail.
     * 
     * @param beanIdEdition identifiant de l'édition
     * @param uidProprietaire uid du propiétaire de l'édition
     * @return EditionSynchroneNonNotifieeParMail
     */
    public EditionAsynchroneNonNotifieeParMail initEditionAsynchroneNonNotifieeParMail(String beanIdEdition,
        String uidProprietaire);

    /**
     * Initialise une édition asynchrone avec exécution immédiate dès son déclenchement, notifiée par mail.
     * 
     * @param beanIdEdition identifiant de l'édition
     * @param uidProprietaire uid du propiétaire de l'édition
     * @param mailDestinataire adresse mail du propriétaire de l'édition
     * @return EditionSynchroneNotifieeParMail
     */
    public EditionAsynchroneNotifieeParMail initEditionAsynchroneNotifieeParMail(String beanIdEdition,
        String uidProprietaire, String mailDestinataire);

    /**
     * Initialise une édition synchrone non notifiée par mail.
     * 
     * @param beanIdEdition identifiant de l'édition
     * @param uidProprietaire uid du propiétaire de l'édition
     * @return EditionSynchroneNonNotifieeParMail
     */
    public EditionSynchroneNonNotifieeParMail initEditionSynchroneNonNotifieeParMail(String beanIdEdition,
        String uidProprietaire);

    /**
     * Initialise une édition synchrone notifiée par mail.
     * 
     * @param beanIdEdition identifiant de l'édition
     * @param uidProprietaire uid du propiétaire de l'édition
     * @param mailDestinataire adresse mail du propriétaire de l'édition
     * @return EditionSynchroneNotifieeParMail
     */
    public EditionSynchroneNotifieeParMail initEditionSynchroneNotifieeParMail(String beanIdEdition,
        String uidProprietaire, String mailDestinataire);

    /**
     * Initialise le message mail d'accès à l'édition pour permettre un accès via le portail intranet ou le portail
     * internet Par défaut, l'accès proposé dans le mail dépend du mode d'authentification défini dans le fichier
     * application.properties : - appli : accès sans passer par le portail intranet - appliportail : accès par le portail
     * intranet ou en direct - portail : accès par le portail
     * 
     * @param jobHistory --
     */
    public void initMessageMailPourAccesAvecOuSansPortailIntranet(JobHistory jobHistory);

    /**
     * Initialise le message mail d'accès à l'édition pour permettre un accès via le portail intrane Par défaut, l'accès
     * proposé dans le mail dépend du mode d'authentification défini dans le fichier application.properties : - appli : accès
     * sans passer par le portail intranet - appliportail : accès par le portail intranet ou en direct - portail : accès
     * par le portail
     * 
     * @param jobHistory --
     */
    public void initMessageMailPourAccesViaPortailIntranet(JobHistory jobHistory);

    /**
     * Initialise le message mail d'accès à l'édition pour permettre un accès via le portail intranet ou le portail
     * internet Par défaut, l'accès proposé dans le mail dépend du mode d'authentification défini dans le fichier
     * application.properties : - appli : accès sans passer par le portail intranet - appliportail : accès par le portail
     * intranet ou en direct - portail : accès par le portail
     * 
     * @param jobHistory --
     */
    public void initMessageMailPourAccesViaPortailIntranetOuInternet(JobHistory jobHistory);

}
