/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : FichierJointServlet.java
 *
 */
package fr.gouv.finances.lombok.edition.mvc;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.boot.autoconfigure.web.DispatcherServletAutoConfiguration;
import org.springframework.context.ApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;
import org.springframework.webflow.context.ExternalContext;
import org.springframework.webflow.context.ExternalContextHolder;
import org.springframework.webflow.context.servlet.ServletExternalContext;
import org.springframework.webflow.core.collection.AttributeMap;
import org.springframework.webflow.execution.FlowExecution;
import org.springframework.webflow.execution.FlowExecutionKey;
import org.springframework.webflow.execution.repository.FlowExecutionRepository;
import org.springframework.webflow.executor.FlowExecutorImpl;
import org.springframework.webflow.mvc.servlet.FlowController;
import org.springframework.webflow.mvc.servlet.FlowHandlerAdapter;

import fr.gouv.finances.lombok.upload.bean.FichierJoint;
import fr.gouv.finances.lombok.util.exception.ProgrammationException;

/**
 * Servlet qui permet d'aller chercher des objets de type fichiers joints dans le contexte de flux sans passer par le
 * mécanisme de webflow pour ne pas changer l'état (appel par un tag app:image par exemple).
 * 
 * @author chouard-cp
 * @version $Revision: 1.2 $ Date: 15 déc. 2009
 */
public class FichierJointServlet extends HttpServlet
{
    private static final long serialVersionUID = 1L;

    public static final String FICHIER_JOINT_PARAMETER = "fichierjoint";

    public static final String MIME_TYPE_PARAMETER = "mimetype";

    /** Id du controleur défini dans le fichier dispatcher-servlet.xml */
    private static final String FLOW_CONTROLLER_BEAN = "flowcontroller";

    public FichierJointServlet()
    {
        super();
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param request
     * @param response
     * @throws IOException Signal qu'une execption de type I/O s'est produite.
     * @throws ServletException the servlet exception
     * @see javax.servlet.http.HttpServlet#service(javax.servlet.http.HttpServletRequest,
     *      javax.servlet.http.HttpServletResponse)
     */
    @Override
    public void service(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
    {
        if (!writeFichierJointToClient(request, response))
        {
            this.writebyteArrayToClient(request, response);
        }
    }

    /**
     * Lecture du nom de l'image passé en paramètre.
     * 
     * @param request --
     * @return Le nom de l'image en parametre
     */
    private String recupererNomImage(HttpServletRequest request)
    {
        String imageName = request.getParameter(FICHIER_JOINT_PARAMETER);

        if (imageName == null || StringUtils.isBlank(imageName))
        {
            throw new ProgrammationException("La requete ne contient pas le parametre " + FICHIER_JOINT_PARAMETER
                + " qui permet de lire l'image dans le contexte de flux");
        }
        return imageName;
    }

    /**
     * Lecture du nom du type MIME passé en paramètre.
     * 
     * @param request --
     * @return Le nom de l'image en parametre
     */
    private String recupererTypeMime(HttpServletRequest request)
    {
        return request.getParameter(MIME_TYPE_PARAMETER);
    }

    /**
     * Lit dans le contexte de flux le tableau de byte qui représente l'image recherchée.
     * 
     * @param imageName --
     * @param request --
     * @param response --
     * @return the byte[]
     */
    private byte[] readArrayByteInScope(String imageName, HttpServletRequest request, HttpServletResponse response)
    {
        byte[] data = null;
        AttributeMap scope = skotchToFlowExecutionContext(request, response);
        Object imageObject = scope.get(imageName);

        if ((imageObject instanceof byte[]))
        {
            data = (byte[]) imageObject;
        }
        return data;
    }

    /**
     * Lit dans le contexte du flux l'objet FichierJoint qui représente l'image recherchée.
     * 
     * @param imageName --
     * @param request --
     * @param response --
     * @return FichierJoint
     */
    private FichierJoint readFichierJointInScope(String imageName, HttpServletRequest request, HttpServletResponse response)
    {
        FichierJoint fichierJoint = null;
        AttributeMap scope = skotchToFlowExecutionContext(request, response);
        Object imageObject = scope.get(imageName);

        if ((imageObject instanceof FichierJoint))
        {
            fichierJoint = (FichierJoint) imageObject;
        }
        return fichierJoint;
    }

    /**
     * Accède au contexte de flux en utilisant le paramète flowexecutionkey transmis dans la requête.
     * 
     * @param request --
     * @param response --
     * @return le contexte de flux
     */
    private AttributeMap skotchToFlowExecutionContext(HttpServletRequest request, HttpServletResponse response)
    {
        FlowExecution flowExecution = null;
        AttributeMap scope = null;

        // La servlet spring DispatcherServlet est stockée dans le
        // contexte de la servlet
        ApplicationContext context =
            (ApplicationContext) getServletContext().getAttribute(
                DispatcherServlet.SERVLET_CONTEXT_PREFIX + DispatcherServletAutoConfiguration.DEFAULT_DISPATCHER_SERVLET_BEAN_NAME);

        FlowController controller = (FlowController) context.getBean(FLOW_CONTROLLER_BEAN);

        FlowExecutionRepository repository = ((FlowExecutorImpl) controller.getFlowExecutor()).getExecutionRepository();

        ExternalContext externalContext =
            new ServletExternalContext(request.getSession().getServletContext(), request, response);
        ExternalContextHolder.setExternalContext(externalContext);
        FlowHandlerAdapter handler = controller.getFlowHandlerAdapter();

        FlowExecutionKey flowExecutionKey =
            repository.parseFlowExecutionKey(handler.getFlowUrlHandler().getFlowExecutionKey(request));

        try
        {
            flowExecution = repository.getFlowExecution(flowExecutionKey);

            if (flowExecution == null || flowExecution.getActiveSession() == null
                || flowExecution.getActiveSession().getScope() == null)
            {
                throw new ProgrammationException("Aucun contexte de flux n'est accessible");
            }
            else
            {
                scope = flowExecution.getActiveSession().getScope();
            }
        }
        finally
        {
            ExternalContextHolder.setExternalContext(null);
        }
        return scope;
    }

    /**
     * Lit dans le contexte de flux si un objet de type byte[] est disponible dans le contexte de flux. Si c'est le cas,
     * le transfère au client. Sinon retourne false
     * 
     * @param request --
     * @param response --
     * @return true si un fichier a été transféré au client, sinon false
     * @throws IOException Signal qu'une execption de type I/O s'est produite.
     */
    private boolean writebyteArrayToClient(HttpServletRequest request, HttpServletResponse response) throws IOException
    {
        boolean result = true;

        String imageName = recupererNomImage(request);
        byte[] imageData = readArrayByteInScope(imageName, request, response);
        String imageMimeType = recupererTypeMime(request);

        if (imageData == null)
        {
            result = false;
        }
        else
        {
            writeFileToClient(imageData, imageMimeType, response);
        }

        return result;
    }

    /**
     * Lit dans le contexte de flux si un objet de type FichierJoint est disponible dans le contexte de flux. Si c'est
     * le cas, le transfère au client. Sinon retourne false
     * 
     * @param request --
     * @param response --
     * @return true si un fichier a été transféré au client, sinon false
     * @throws IOException Signal qu'une execption de type I/O s'est produite.
     */
    private boolean writeFichierJointToClient(HttpServletRequest request, HttpServletResponse response)
        throws IOException
    {
        boolean result = true;
        byte[] imageData = null;
        String imageMimeType = null;

        String imageName = recupererNomImage(request);

        FichierJoint fichierJoint = readFichierJointInScope(imageName, request, response);

        if (fichierJoint == null || fichierJoint.getLeContenuDuFichier() == null)
        {
            result = false;
        }
        else
        {
            imageData = fichierJoint.getLeContenuDuFichier().getData();
            imageMimeType = fichierJoint.getTypeMimeFichier();

            writeFileToClient(imageData, imageMimeType, response);
        }
        return result;
    }

    /**
     * methode Write file to client
     * 
     * @param imageData --
     * @param imageMimeType --
     * @param request --
     * @param response --
     * @throws IOException Signal qu'une execption de type I/O s'est produite.
     */
    private void writeFileToClient(byte[] imageData, String imageMimeType, HttpServletResponse response) throws IOException
    {
        if (imageData != null && imageData.length > 0)
        {
            if (imageMimeType != null)
            {
                response.setHeader("Content-Type", imageMimeType);
            }
            response.setContentLength(imageData.length);
            ServletOutputStream ouputStream = response.getOutputStream();
            ouputStream.write(imageData, 0, imageData.length);
            ouputStream.flush();
            ouputStream.close();
        }
    }

}
