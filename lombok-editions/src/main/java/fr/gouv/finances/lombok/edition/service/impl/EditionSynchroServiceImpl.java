/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : EditionSynchroServiceImpl.java
 *
 */
package fr.gouv.finances.lombok.edition.service.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.TransactionStatus;

import fr.gouv.finances.lombok.edition.bean.JobHistory;
import fr.gouv.finances.lombok.edition.bean.TrcPurgeEdition;
import fr.gouv.finances.lombok.edition.dao.JobHistoryBatchDao;
import fr.gouv.finances.lombok.edition.dao.JobHistoryDao;
import fr.gouv.finances.lombok.edition.dao.TrcPurgeEditionDao;
import fr.gouv.finances.lombok.edition.service.EditionStorageService;
import fr.gouv.finances.lombok.edition.service.EditionSynchroService;
import fr.gouv.finances.lombok.edition.util.Control;
import fr.gouv.finances.lombok.util.base.BaseServiceImpl;
import fr.gouv.finances.lombok.util.exception.ApplicationException;
import fr.gouv.finances.lombok.util.exception.ApplicationExceptionTransformateur;
import fr.gouv.finances.lombok.util.exception.ExploitationException;
import fr.gouv.finances.lombok.util.exception.RegleGestionException;
import fr.gouv.finances.lombok.util.hibernate.UtilitaireTransactionSurScrollIteratorSupportService;
import fr.gouv.finances.lombok.util.persistance.ScrollIterator;

/**
 * Class EditionSynchroServiceImpl
 * 
 * @author amleplatinec
 * @version $Revision: 1.2 $ Date: 11 déc. 2009
 */
public class EditionSynchroServiceImpl extends BaseServiceImpl implements EditionSynchroService
{
    private static final Logger log = LoggerFactory.getLogger(EditionSynchroServiceImpl.class);

    /** Nom de l'application à l'origine de l'édition. */
    private String appliorigine;

    /** Référence vers le Dao d'accès à l'historique des éditions. */
    private JobHistoryDao jobhistorydao;

    /** Référence vers le dao dédié aux batch de l'historique des éditions. */
    private JobHistoryBatchDao jobhistorybatchdao;

    /** trcpurgeeditiondao. */
    private TrcPurgeEditionDao trcpurgeeditiondao;

    /** Réference du service de stockage des édition. */
    private EditionStorageService editionstorageserviceso;

    /** Gestionnaire de transaction. */
    private UtilitaireTransactionSurScrollIteratorSupportService transactionScrollIterator;

    /**
     * Constructeur de la classe EditionSynchroServiceImpl.java
     */
    public EditionSynchroServiceImpl()
    {
        super();

    }

    /**
     * Place toutes les tâches de purge dans l'état ARRET_DEMANDE.
     */
    @Override
    public void demanderArretDesPurgesEnCoursExecution()
    {
        List trcPurgesEditionsEnCours = trcpurgeeditiondao.findTrcPurgeEditionEnCours(this.appliorigine);
        for (Iterator iterTrcPurgeEdition = trcPurgesEditionsEnCours.iterator(); iterTrcPurgeEdition.hasNext();)
        {
            TrcPurgeEdition trcPurgeEdition = (TrcPurgeEdition) iterTrcPurgeEdition.next();
            enregistreDemandeArretPurge(trcPurgeEdition);
        }
    }

    /**
     * Demande l'arrêt d'une tâche de purge en cours d'exécution.
     * 
     * @param trcPurgeEdition --
     */
    @Override
    public void enregistreDemandeArretPurge(TrcPurgeEdition trcPurgeEdition)
    {
        trcpurgeeditiondao.refresh(trcPurgeEdition);
        trcPurgeEdition.demandeArret();
        trcpurgeeditiondao.saveTrcPurgeEdition(trcPurgeEdition);
    }

    /**
     * Place dans l'état ARRET_DEMANDE les Objets TrcPurgeEdition qui sont dans l'état EN_COURS_EXECUTION qui ont
     * démarrées avant la date passée en paramètre.
     * 
     * @param dateDebutPurgeCourante --
     */
    @Override
    public void enregistreDemandeArretPurgesAnterieures(Date dateDebutPurgeCourante)
    {
        List purgesEnCoursAnterieures =
            trcpurgeeditiondao.findLesTrcPurgeEditionEnCoursAnterieuresAUneDate(dateDebutPurgeCourante,
                this.appliorigine);

        if (purgesEnCoursAnterieures != null && purgesEnCoursAnterieures.size() > 0)
        {
            log.warn("Au moins une tâche de purge est déjà en cours d'exécution.\n");

            for (Iterator iterTrcPurgeEdition = purgesEnCoursAnterieures.iterator(); iterTrcPurgeEdition.hasNext();)
            {
                TrcPurgeEdition trcPurgeEdition = (TrcPurgeEdition) iterTrcPurgeEdition.next();
                if (log.isDebugEnabled())
                {
                    log.debug(trcPurgeEdition.toString());
                }

                enregistreDemandeArretPurge(trcPurgeEdition);
                log.warn("Demande d'arrêt des tâches de purge en cours d'exécution.\n");
            }
        }
    }

    /**
     * Enregistre le démarrage d'une tâche de purge des éditions. Si une autre édition est démarrée dans la même
     * milliseconde, on incrémente le moment du démarrage d'une milliseconde et on teste de nouveau si une autre tâche
     * possède la même date de début. L'opération est répétée jusqu'à 10 fois. A la 11eme tentative, on lève une
     * exception
     * 
     * @param applicationOrigine --
     * @param trcPurgeEdition --
     */
    @Override
    public void enregistreDemarragePurge(String applicationOrigine, TrcPurgeEdition trcPurgeEdition)
    {
        Control.assertTrcPurgeEditionNotNull(trcPurgeEdition);

        trcPurgeEdition.demarreExecution();

        // On contrôle qu'aucun autre batch de purge n'a été démarré au même instant
        // Si c'est le cas, on essai 10 fois d'ajouter 1 milliseconde à la date
        // de démarrage.
        int maxEssai = 0;
        while (trcpurgeeditiondao.checkExistencePurgeDateIdentique(trcPurgeEdition) && maxEssai < 10)
        {
            Calendar cal = Calendar.getInstance(Locale.FRANCE);
            cal.setTime(trcPurgeEdition.getDateDebutPurge());
            cal.add(Calendar.MILLISECOND, 1);
            maxEssai++;
        }

        if (!trcpurgeeditiondao.checkExistencePurgeDateIdentique(trcPurgeEdition))
        {
            trcpurgeeditiondao.saveTrcPurgeEdition(trcPurgeEdition);
        }
        else
        {
            StringBuilder msgErreur = new StringBuilder();
            msgErreur.append("Impossible d'enregistrer le démarrage d'une tâche de purge.");
            msgErreur.append("Plusieurs tâches de purges ont été démarrées dans la même milliseconde.");
            throw new ExploitationException(msgErreur.toString());
        }
    }

    /**
     * Enregistre la fin d'une tache de purge avec erreur.
     * 
     * @param trcPurgeEdition --
     * @param messageErreur --
     */
    public void enregistreFinPurgeAvecErreur(TrcPurgeEdition trcPurgeEdition, String messageErreur)
    {
        trcpurgeeditiondao.refresh(trcPurgeEdition);
        trcPurgeEdition.termineAvecErreur(messageErreur);
        trcpurgeeditiondao.saveTrcPurgeEdition(trcPurgeEdition);
    }

    /**
     * Enregistre la fin d'une tache de purge sans erreur.
     * 
     * @param trcPurgeEdition --
     */
    public void enregistreFinPurgeSansErreur(TrcPurgeEdition trcPurgeEdition)
    {
        if (trcPurgeEdition.isArretDemande())
        {
            trcPurgeEdition.interrompreSuiteDemandeAdministrateur();
        }
        else
        {
            trcPurgeEdition.termineSansErreur();
        }
        trcpurgeeditiondao.saveTrcPurgeEdition(trcPurgeEdition);
    }

    /**
     * Enregistre une tache de purge dans l'état interrompu.
     * 
     * @param trcPurgeEdition --
     */
    public void enregistreInterruptionPurge(TrcPurgeEdition trcPurgeEdition)
    {
        trcpurgeeditiondao.refresh(trcPurgeEdition);
        trcPurgeEdition.interrompreSuiteDemandeAdministrateur();
        trcpurgeeditiondao.saveTrcPurgeEdition(trcPurgeEdition);
    }

    /**
     * Enregistre l'état d'une tache de purge dans l'état non démarré.
     * 
     * @param trcPurgeEdition --
     */
    public void enregistrePurgeNonDemarree(TrcPurgeEdition trcPurgeEdition)
    {
        trcpurgeeditiondao.refresh(trcPurgeEdition);
        trcPurgeEdition.nePasDemarrerExecution();
        trcpurgeeditiondao.saveTrcPurgeEdition(trcPurgeEdition);
    }

    /**
     * Recherche l'objet TcPurgeEdition correspondant à un instant de démarrage donné.
     * 
     * @param instantDemarrage --
     * @param applicationOrigine --
     * @return the trc purge edition
     */
    @Override
    public TrcPurgeEdition findUnTrcPurgeEditionPourUnInstantDemarrage(Date instantDemarrage, String applicationOrigine)
    {
        TrcPurgeEdition trcPurgeEdition = null;
        List<TrcPurgeEdition> lesTrcPurgeEdition;

        lesTrcPurgeEdition =
            trcpurgeeditiondao.findLesTrcPurgeEditionPourUnInstantDemarrage(instantDemarrage, applicationOrigine);

        if (lesTrcPurgeEdition != null && lesTrcPurgeEdition.size() > 1)
        {
            throw new RegleGestionException("A un instant donné doit correspondre une seul objet TrcPurgeEdition");
        }
        else if (lesTrcPurgeEdition != null && lesTrcPurgeEdition.size() == 1)
        {
            trcPurgeEdition = lesTrcPurgeEdition.get(0);
        }
        return trcPurgeEdition;
    }

    /**
     * Gets the trcpurgeeditiondao.
     * 
     * @return the trcpurgeeditiondao
     */
    public TrcPurgeEditionDao getTrcpurgeeditiondao()
    {
        return trcpurgeeditiondao;
    }

    /**
     * Supprime les éditions dont la date de purge est atteinte. Supprime le fichier dans le SF et l'entrée dans
     * l'historique des éditions
     * 
     * @param nombreOccurences --
     * @param dateCourante --
     * @param trcPurgeEdition --
     */
    public void purgeEditions(final int nombreOccurences, final Date dateCourante, TrcPurgeEdition trcPurgeEdition)
    {
        final String applicationOrigine = this.appliorigine;
        long compteur = 0;
        long nombreEditionsSupprimees = 0;
        ScrollIterator editionsAPurgerIterator = null;

        // Ouverture (en lecture seule) de la transaction de lecture des éléments à purger
        TransactionStatus statustransactionenglobantelecture =
            transactionScrollIterator.demarrerTransactionEnglobanteDeLectureScrollIterator();

        try
        {
            JobHistory jobHistory;
            List<JobHistory> lesEditionsSupprimees = new ArrayList<>();

            // Ouverture du curseur
            editionsAPurgerIterator =
                jobhistorybatchdao.listeEditionAPurgerIterator(nombreOccurences, dateCourante, applicationOrigine);

            boolean arretDemande = isPurgeAInterrompre(trcPurgeEdition);

            while (editionsAPurgerIterator.hasNext() && (!arretDemande))
            {
                // debut boucle sur
                // editionsAPurgerIterator

                jobHistory = (JobHistory) ((Object[]) editionsAPurgerIterator.next())[0];

                // On vide la session une fois l'objet lu pour éviter une exception levée
                // par Hibernate lorsqu'un collection est associée à deux sessions
                this.jobhistorybatchdao.clearPersistenceContext();

                if (jobHistory != null)
                {
                    editionstorageserviceso.effacerFichierEdition(jobHistory);
                    lesEditionsSupprimees.add(jobHistory);
                }

                if (compteur++ % nombreOccurences == 0 && lesEditionsSupprimees != null && !lesEditionsSupprimees.isEmpty())
                {
                    arretDemande = isPurgeAInterrompre(trcPurgeEdition);
                    if (!arretDemande)
                    {
                        lesEditionsSupprimees =
                            supprimeLotEditions(compteur, lesEditionsSupprimees, nombreEditionsSupprimees);
                        lesEditionsSupprimees = new ArrayList<>();
                    }
                }
            }
            // fin boucle sur editionsAPurgerIterator

            if (lesEditionsSupprimees != null && !lesEditionsSupprimees.isEmpty())
            {
                lesEditionsSupprimees = supprimeLotEditions(compteur, lesEditionsSupprimees, nombreEditionsSupprimees);
            }

            if (arretDemande)
            {
                log.warn("La tâche de purge a été interrompue suite à la demande d'un administrateur "
                    + "ou au démarrage d'une autre tâche de purge");
            }

            log.warn("Suppression d'un total de " + compteur + " éditions ");

        }
        catch (RuntimeException exception)
        {
            log.warn("ERREUR LORS DE LA PURGE ", exception);
            throw ApplicationExceptionTransformateur.transformer(exception);
        }
        finally
        {
            if (editionsAPurgerIterator != null)
            {
                // Fermeture du curseur
                editionsAPurgerIterator.close();
            }

            // On ferme la transaction en lecture seule
            if (!statustransactionenglobantelecture.isCompleted())
            {
                transactionScrollIterator.commiterTransactionEnglobanteDeLectureScrollIterator(
                    statustransactionenglobantelecture);
            }

            trcPurgeEdition.enregistreDureeEtNbEltsPurges(nombreEditionsSupprimees);
        }
    }

    /**
     * Démarre la tache de purge des éditions, si possible.
     * 
     * @param nombreOccurences --
     * @param dateCourante --
     */
    @Override
    public void purgeEditionsSiAutorise(final int nombreOccurences, final Date dateCourante)
    {
        TrcPurgeEdition trcPurgeEdition = null;

        try
        {
            trcPurgeEdition = TrcPurgeEdition.creationTachePurgeEnAttenteDemarrage(this.appliorigine);

            // Demande l'arrêt des purges précédentes en cours d'exécution
            enregistreDemandeArretPurgesAnterieures(trcPurgeEdition.getDateDebutPurge());
            enregistreDemarragePurge(this.appliorigine, trcPurgeEdition);
            purgeEditions(nombreOccurences, dateCourante, trcPurgeEdition);
            enregistreFinPurgeSansErreur(trcPurgeEdition);
        }
        catch (ApplicationException exception)
        {
            log.warn("ERREUR", exception);
            enregistreFinPurgeAvecErreur(trcPurgeEdition, exception.getMessage());
            throw exception;
        }
    }

    /**
     * Sets the appliorigine.
     * 
     * @param appliorigine the appliorigine to set
     */
    public void setAppliorigine(String appliorigine)
    {
        this.appliorigine = appliorigine;
    }

    /**
     * Sets the editionstorageserviceso.
     * 
     * @param editionstorageserviceso the editionstorageserviceso to set
     */
    public void setEditionstorageserviceso(EditionStorageService editionstorageserviceso)
    {
        this.editionstorageserviceso = editionstorageserviceso;
    }

    /**
     * Modificateur de l attribut jobhistorybatchdao.
     * 
     * @param jobhistorybatchdao le nouveau jobhistorybatchdao
     */
    public void setJobhistorybatchdao(JobHistoryBatchDao jobhistorybatchdao)
    {
        this.jobhistorybatchdao = jobhistorybatchdao;
    }

    /**
     * Sets the jobhistorydao.
     * 
     * @param jobhistorydao the jobhistorydao to set
     */
    public void setJobhistorydao(JobHistoryDao jobhistorydao)
    {
        this.jobhistorydao = jobhistorydao;
    }

    /**
     * Sets the trcpurgeeditiondao.
     * 
     * @param trcpurgeeditiondao the trcpurgeeditiondao to set
     */
    public void setTrcpurgeeditiondao(TrcPurgeEditionDao trcpurgeeditiondao)
    {
        this.trcpurgeeditiondao = trcpurgeeditiondao;
    }

    /**
     * Supprimes tous les objets TrcPurgeEdition.
     * 
     * @return the int
     */
    @Override
    public int supprimeToutesLesTracesDePurge()
    {
        List lesTrcPurgesEditionSupp = trcpurgeeditiondao.loadAllObjects(TrcPurgeEdition.class);
        trcpurgeeditiondao.deleteTrcPurgeEditions(lesTrcPurgesEditionSupp);
        return (lesTrcPurgesEditionSupp == null ? 0 : lesTrcPurgesEditionSupp.size());
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param nombreOccurences
     * @see fr.gouv.finances.lombok.edition.service.EditionSynchroService#synchroniseHistoriqueAvecSf(int)
     */
    @Override
    public void synchroniseHistoriqueAvecSf(int nombreOccurences)
    {
        editionstorageserviceso.synchroniseHistoriqueAvecSf(nombreOccurences, this.appliorigine);
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @see fr.gouv.finances.lombok.edition.service.EditionSynchroService#synchroniseSfAvecHistorique()
     */
    @Override
    public void synchroniseSfAvecHistorique()
    {
        editionstorageserviceso.synchroniseSfAvecHistorique(this.appliorigine);
    }

    /**
     * Teste si l'interruption d'une purge a été demandée.
     * 
     * @param trcPurgeEdition --
     * @return true, if checks if is purge a interrompre
     */
    private boolean isPurgeAInterrompre(TrcPurgeEdition trcPurgeEdition)
    {
        Control.assertTrcPurgeEditionNotNull(trcPurgeEdition);
        trcpurgeeditiondao.modifyObject(trcPurgeEdition);
        // trcpurgeeditiondao.refresh(trcPurgeEdition);
        return trcPurgeEdition.isArretDemande();
    }

    /**
     * Ouvre une transaction et supprime un lot d'éditions.
     * 
     * @param compteur --
     * @param lesEditionsSupprimees --
     * @param nombreEditionsSupprimees --
     * @return the list< job history>
     */
    private List<JobHistory> supprimeLotEditions(long compteur, List<JobHistory> lesEditionsSupprimees,
        long nombreEditionsSupprimees)
    {
        TransactionStatus statustransactionenglobeebouclemaj = null;
        JobHistory jobHistory = null;
        try
        {
            // Début de la transaction de suppression d'un lot d'édition

            // ** demarrer la transaction en debut de mise à jour
            statustransactionenglobeebouclemaj =
                transactionScrollIterator.demarrerTransactionEnglobeeDeMiseAJourDUnLot();

            for (Iterator iterjob = lesEditionsSupprimees.iterator(); iterjob.hasNext();)
            {
                jobHistory = (JobHistory) iterjob.next();
                jobhistorydao.deleteJobHistory(jobHistory);
                jobhistorydao.flush();
                nombreEditionsSupprimees++;
            }

            // fermeture de la transaction de suppression des éditions
            transactionScrollIterator.commiterTransactionEnglobeeDeMiseAJourDUnLot(
                statustransactionenglobeebouclemaj);

            log.warn("Suppression des éditions " + (compteur - lesEditionsSupprimees.size()) + " à " + compteur + "\n");

        }
        catch (RuntimeException exception)
        {
            StringBuilder msg = new StringBuilder();
            msg.append("ERREUR ");
            if (jobHistory != null)
            {
                msg.append(" LORS DE LA SUPPRESSION DE L'EDITION (");
                msg.append(jobHistory.getId());
                msg.append(')');
            }
            log.warn(msg.toString(), exception);
            transactionScrollIterator.rollBackSurTransactionEnglobeeDeMiseAJourDUnLot(statustransactionenglobeebouclemaj);
            throw ApplicationExceptionTransformateur.transformer(exception);
        }
        return lesEditionsSupprimees;
    }

    /**
     * Mutateur de transactionScrollIterator
     *
     * @param transactionScrollIterator transactionScrollIterator
     */
    public void setTransactionScrollIterator(UtilitaireTransactionSurScrollIteratorSupportService transactionScrollIterator)
    {
        this.transactionScrollIterator = transactionScrollIterator;
    }

}
