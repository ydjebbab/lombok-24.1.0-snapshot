/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 */
package fr.gouv.finances.lombok.edition.bean;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.Resource;

import fr.gouv.finances.lombok.util.exception.ApplicationExceptionTransformateur;
import fr.gouv.finances.lombok.util.exception.ExploitationException;
import fr.gouv.finances.lombok.util.exception.ProgrammationException;

/**
 * Paramétrage Jasper de la vue HTML.
 * 
 * @author amleplatinec
 * @author Christophe Breheret-Girardin
 */
public class ParametrageVueJasperHtml extends AbstractParametrageVueJasper
{
    /** Initialisation de la journalisation. */
    protected static final Logger LOGGER = LoggerFactory.getLogger(ParametrageVueJasperHtml.class);

    /** Initialisation de l'UID. */
    private static final long serialVersionUID = 1L;

    /** Constant : DEF_FORMAT_EDITION_VALUE. */
    private static final String DEF_FORMAT_EDITION_VALUE = "html";

    /** Constant : DEF_EXTENSION. */
    private static final String DEF_EXTENSION = "html";

    /** Constant : DEF_CHARACTER_ENCODING. */
    private static final String DEF_CHARACTER_ENCODING = "UTF-8";

    /** Constant : DEF_CONTENT_TYPE. */
    private static final String DEF_CONTENT_TYPE = "text/html";

    /** Constant : DEF_MIME_TYPE. */
    private static final String DEF_MIME_TYPE = "text/html";

    /** Constant : DEF_IMAGE_URI. */
    private static final String DEF_IMAGE_URI = "";

    /** Constant : DEF_OUTPUT_IMAGES_TO_DIR. */
    private static final Boolean DEF_OUTPUT_IMAGES_TO_DIR = Boolean.TRUE;

    /** Constant : DEF_REM_EMPTY_SPACE_BET_ROWS. */
    private static final Boolean DEF_REM_EMPTY_SPACE_BET_ROWS = Boolean.TRUE;

    /** Constant : DEF_USING_IMAGES_TO_ALIGN. */
    private static final Boolean DEF_USING_IMAGES_TO_ALIGN = Boolean.TRUE;

    /** Constant : DEF_WHITE_PAGE_BACKGROUND. */
    private static final Boolean DEF_WHITE_PAGE_BACKGROUND = Boolean.TRUE;

    /** Constant : DEF_WRAP_BREAK_WORD. */
    private static final Boolean DEF_WRAP_BREAK_WORD = Boolean.TRUE;

    /** Constant : DEF_SIZE_UNIT. */
    private static final String DEF_SIZE_UNIT = "px";

    /** Constant : IMAGES_SOURCE_DIR. */
    private static final String IMAGES_SOURCE_DIR = "IMAGES_SOURCE_DIR";

    /** Constant : IMAGES_DIR_NAME. */
    private static final String IMAGES_DIR_NAME =
        "net.sf.jasperreports.engine.export.JRHtmlExporterParameter.IMAGES_DIR_NAME";

    /** Constant : IMAGES_URI. */
    private static final String IMAGES_URI = "net.sf.jasperreports.engine.export.JRHtmlExporterParameter.IMAGES_URI";

    /** Constant : IS_OUTPUT_IMAGES_TO_DIR. */
    private static final String IS_OUTPUT_IMAGES_TO_DIR =
        "net.sf.jasperreports.engine.export.JRHtmlExporterParameter.IS_OUTPUT_IMAGES_TO_DIR";

    /** Constant : HTML_HEADER. */
    private static final String HTML_HEADER = "net.sf.jasperreports.engine.export.JRHtmlExporterParameter.HTML_HEADER";

    /** Constant : BETWEEN_PAGES_HTML. */
    private static final String BETWEEN_PAGES_HTML =
        "net.sf.jasperreports.engine.export.JRHtmlExporterParameter.BETWEEN_PAGES_HTML";

    /** Constant : HTML_FOOTER. */
    private static final String HTML_FOOTER = "net.sf.jasperreports.engine.export.JRHtmlExporterParameter.HTML_FOOTER";

    /** Constant : IS_REM_EMPTY_SPACE_BET_ROWS. */
    private static final String IS_REM_EMPTY_SPACE_BET_ROWS =
        "net.sf.jasperreports.engine.export.JRHtmlExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS";

    /** Constant : IS_WHITE_PAGE_BACKGROUND. */
    private static final String IS_WHITE_PAGE_BACKGROUND =
        "net.sf.jasperreports.engine.export.JRHtmlExporterParameter.IS_WHITE_PAGE_BACKGROUND";

    /** Constant : IS_USING_IMAGES_TO_ALIGN. */
    private static final String IS_USING_IMAGES_TO_ALIGN =
        "net.sf.jasperreports.engine.export.JRHtmlExporterParameter.IS_USING_IMAGES_TO_ALIGN";

    /** Constant : IS_WRAP_BREAK_WORD. */
    private static final String IS_WRAP_BREAK_WORD =
        "net.sf.jasperreports.engine.export.JRHtmlExporterParameter.IS_WRAP_BREAK_WORD";

    /** Constant : SIZE_UNIT. */
    private static final String SIZE_UNIT = "net.sf.jasperreports.engine.export.JRHtmlExporterParameter.SIZE_UNIT";

    /** images source dir. */
    private String imagesSourceDir;

    /** images dir name. */
    private String imagesDirName;

    /** images dir name absolute path. */
    private String imagesDirNameAbsolutePath;

    /** image uri. */
    private String imageURI;

    /** output images to dir. */
    private Boolean outputImagesToDir;

    /** html header. */
    private String htmlHeader;

    /** between pages html. */
    private String betweenPagesHtml;

    /** html footer. */
    private String htmlFooter;

    /** remove empty space betweens rows. */
    private Boolean removeEmptySpaceBetweensRows;

    /** white page background. */
    private Boolean whitePageBackground;

    /** using images to align. */
    private Boolean usingImagesToAlign;

    /** wrap break word. */
    private Boolean wrapBreakWord;

    /** size unit. */
    private String sizeUnit;

    /**
     * Construit un objet ParametrageVueJasperHtml et lui affecte les paramètres par défaut.
     */
    public ParametrageVueJasperHtml()
    {
        super();

        this.formatEdition = DEF_FORMAT_EDITION_VALUE;
        this.characterEncoding = DEF_CHARACTER_ENCODING;
        this.extension = DEF_EXTENSION;
        this.contentType = DEF_CONTENT_TYPE;
        this.typeMimeEdition = DEF_MIME_TYPE;

        // Spécifique html
        this.imageURI = DEF_IMAGE_URI;
        this.outputImagesToDir = DEF_OUTPUT_IMAGES_TO_DIR;
        this.removeEmptySpaceBetweensRows = DEF_REM_EMPTY_SPACE_BET_ROWS;
        this.usingImagesToAlign = DEF_USING_IMAGES_TO_ALIGN;
        this.whitePageBackground = DEF_WHITE_PAGE_BACKGROUND;
        this.wrapBreakWord = DEF_WRAP_BREAK_WORD;
        this.sizeUnit = DEF_SIZE_UNIT;
    }

    /**
     * Accesseur de l attribut between pages html.
     * 
     * @return between pages html
     */
    public String getBetweenPagesHtml()
    {
        return betweenPagesHtml;
    }

    /**
     * Accesseur de l attribut html footer.
     * 
     * @return html footer
     */
    public String getHtmlFooter()
    {
        return htmlFooter;
    }

    /**
     * Accesseur de l attribut html header.
     * 
     * @return html header
     */
    public String getHtmlHeader()
    {
        return htmlHeader;
    }

    /**
     * Accesseur de l attribut images dir name.
     * 
     * @return images dir name
     */
    public String getImagesDirName()
    {
        return imagesDirName;
    }

    /**
     * Accesseur de l attribut images dir name absolute path.
     * 
     * @return images dir name absolute path
     */
    public String getImagesDirNameAbsolutePath()
    {
        return imagesDirNameAbsolutePath;
    }

    /**
     * Accesseur de l attribut images source dir.
     * 
     * @return images source dir
     */
    public String getImagesSourceDir()
    {
        return imagesSourceDir;
    }

    /**
     * Accesseur de l attribut image uri.
     * 
     * @return image uri
     */
    public String getImageURI()
    {
        return imageURI;
    }

    /**
     * Accesseur de l attribut output images to dir.
     * 
     * @return output images to dir
     */
    public Boolean getOutputImagesToDir()
    {
        return outputImagesToDir;
    }

    /**
     * Accesseur de l attribut removes the empty space betweens rows.
     * 
     * @return removes the empty space betweens rows
     */
    public Boolean getRemoveEmptySpaceBetweensRows()
    {
        return removeEmptySpaceBetweensRows;
    }

    /**
     * Accesseur de l attribut size unit.
     * 
     * @return size unit
     */
    public String getSizeUnit()
    {
        return sizeUnit;
    }

    /**
     * Accesseur de l attribut using images to align.
     * 
     * @return using images to align
     */
    public Boolean getUsingImagesToAlign()
    {
        return usingImagesToAlign;
    }

    /**
     * Accesseur de l attribut white page background.
     * 
     * @return white page background
     */
    public Boolean getWhitePageBackground()
    {
        return whitePageBackground;
    }

    /**
     * Accesseur de l attribut wrap break word.
     * 
     * @return wrap break word
     */
    public Boolean getWrapBreakWord()
    {
        return wrapBreakWord;
    }

    /**
     * Produit une Map contenant le paramétrage de l'exporter jasper.
     * 
     * @return the map
     */
    public Map<String, Object> parametrageJRExporter()
    {
        // Déclaration d'une Map qui contiendra le paramétrage
        Map<String, Object> mapModele = new HashMap<>();

        // Paramétrage général de l'exporteur Jasper
        putIfNotNull(mapModele, CHARACTER_ENCODING, this.characterEncoding);

        // Récupération de l'URI du répertoire contenant les images
        try
        {
            if (this.imagesDirName != null)
            {
                URI imageDirNameURI = new URI(this.imagesDirName);
                this.imagesDirNameAbsolutePath = imageDirNameURI.getPath();

                // Controle de la présence du répertoire et création si nécessaire
                this.controlOrCreateDirectory(this.imagesDirName);
            }

        }
        catch (URISyntaxException exception)
        {
            throw ApplicationExceptionTransformateur.transformer("ERREUR LORS DE L'ACCES À L'URI CONTENANT LES IMAGES",
                exception);
        }

        // Paramétrage spécifique HTML
        putIfNotNull(mapModele, IMAGES_DIR_NAME, this.imagesDirNameAbsolutePath);
        putIfNotNull(mapModele, IMAGES_URI, this.imageURI);
        putIfNotNull(mapModele, IS_OUTPUT_IMAGES_TO_DIR, this.outputImagesToDir);
        putIfNotNull(mapModele, HTML_HEADER, this.htmlHeader);
        putIfNotNull(mapModele, HTML_FOOTER, this.htmlFooter);
        putIfNotNull(mapModele, BETWEEN_PAGES_HTML, this.betweenPagesHtml);
        putIfNotNull(mapModele, IS_REM_EMPTY_SPACE_BET_ROWS, this.removeEmptySpaceBetweensRows);
        putIfNotNull(mapModele, IS_WHITE_PAGE_BACKGROUND, this.whitePageBackground);
        putIfNotNull(mapModele, IS_USING_IMAGES_TO_ALIGN, this.usingImagesToAlign);
        putIfNotNull(mapModele, IS_WRAP_BREAK_WORD, this.wrapBreakWord);
        putIfNotNull(mapModele, SIZE_UNIT, this.sizeUnit);

        // Journalisation du paramétrage
        journaliserParametrageJRExporter(mapModele);

        return mapModele;
    }

    /**
     * Produit une Map contenant le paramétrage de la vue jasper en utilisant : - soit les valeurs par défaut, - soit
     * les valeurs contenues dans le propriétés de l'objet ParametrageVueJasperHtml (valeurs injectée dans le fichier de
     * configuration des éditions).
     * 
     * @return le paramétrage de la vue Jasper
     */
    public Map<String, String> parametrerVue()
    {
        // Déclaration d'une Map qui contiendra le paramétrage
        Map<String, String> mapModele = new HashMap<>();

        // Initialisation du paramétrage
        putIfNotNull(mapModele, REPORT_VIEW_KEY, this.formatEdition);
        putIfNotNull(mapModele, EXTENSION_KEY, this.extension);
        putIfNotNull(mapModele, CONTENT_TYPE_KEY, this.contentType);
        putIfNotNull(mapModele, MIME_TYPE_KEY, this.typeMimeEdition);
        putIfNotNull(mapModele, IMAGES_SOURCE_DIR, this.imagesSourceDir);

        // Journalisation du paramétrage
        journaliserParametrageVue(mapModele);

        return mapModele;
    }

    /**
     * Modificateur de l attribut between pages html.
     * 
     * @param betweenPagesHtml le nouveau between pages html
     */
    public void setBetweenPagesHtml(String betweenPagesHtml)
    {
        this.betweenPagesHtml = betweenPagesHtml;
    }

    /**
     * Modificateur de l attribut html footer.
     * 
     * @param htmlFooter le nouveau html footer
     */
    public void setHtmlFooter(String htmlFooter)
    {
        this.htmlFooter = htmlFooter;
    }

    /**
     * Modificateur de l attribut html header.
     * 
     * @param htmlHeader le nouveau html header
     */
    public void setHtmlHeader(String htmlHeader)
    {
        this.htmlHeader = htmlHeader;
    }

    /**
     * Modificateur de l attribut images dir name.
     * 
     * @param imageDirName le nouveau images dir name
     */
    public void setImagesDirName(String imageDirName)
    {
        this.imagesDirName = imageDirName;
    }

    /**
     * Modificateur de l attribut images dir name absolute path.
     * 
     * @param imagesDirNameAbsolutePath le nouveau images dir name absolute path
     */
    public void setImagesDirNameAbsolutePath(String imagesDirNameAbsolutePath)
    {
        this.imagesDirNameAbsolutePath = imagesDirNameAbsolutePath;
    }

    /**
     * Modificateur de l attribut images source dir.
     * 
     * @param imageSourceDir le nouveau images source dir
     */
    public void setImagesSourceDir(String imageSourceDir)
    {
        this.imagesSourceDir = imageSourceDir;
    }

    /**
     * Modificateur de l attribut image uri.
     * 
     * @param imageURI le nouveau image uri
     */
    public void setImageURI(String imageURI)
    {
        this.imageURI = imageURI;
    }

    /**
     * Modificateur de l attribut output images to dir.
     * 
     * @param outputImagesToDir le nouveau output images to dir
     */
    public void setOutputImagesToDir(Boolean outputImagesToDir)
    {
        this.outputImagesToDir = outputImagesToDir;
    }

    /**
     * Modificateur de l attribut removes the empty space betweens rows.
     * 
     * @param removeEmptySpaceBetweensRows le nouveau removes the empty space betweens rows
     */
    public void setRemoveEmptySpaceBetweensRows(Boolean removeEmptySpaceBetweensRows)
    {
        this.removeEmptySpaceBetweensRows = removeEmptySpaceBetweensRows;
    }

    /**
     * Modificateur de l attribut size unit.
     * 
     * @param sizeUnit le nouveau size unit
     */
    public void setSizeUnit(String sizeUnit)
    {
        this.sizeUnit = sizeUnit;
    }

    /**
     * Modificateur de l attribut using images to align.
     * 
     * @param usingImagesToAlign le nouveau using images to align
     */
    public void setUsingImagesToAlign(Boolean usingImagesToAlign)
    {
        this.usingImagesToAlign = usingImagesToAlign;
    }

    /**
     * Modificateur de l attribut white page background.
     * 
     * @param whitePageBackground le nouveau white page background
     */
    public void setWhitePageBackground(Boolean whitePageBackground)
    {
        this.whitePageBackground = whitePageBackground;
    }

    /**
     * Modificateur de l attribut wrap break word.
     * 
     * @param wrapBreakWord le nouveau wrap break word
     */
    public void setWrapBreakWord(Boolean wrapBreakWord)
    {
        this.wrapBreakWord = wrapBreakWord;
    }

    /**
     * Contrôle qu'un répertoire existe. Si ce n'est pas le cas, crée les répertoires nécessaires
     * 
     * @param dir --
     * @return File
     */
    private File controlOrCreateDirectory(String dir)
    {
        DefaultResourceLoader drl = new DefaultResourceLoader();
        Resource resdirectory = drl.getResource(dir);
        File file;

        try
        {
            file = resdirectory.getFile();
        }
        catch (IOException e)
        {
            throw ApplicationExceptionTransformateur.transformer(e);
        }

        // Création du répertoire si nécessaire
        if (!file.exists())
        {
            boolean result;

            result = file.mkdirs();
            // Si le répertoire n'a pas pu être créé et
            // si le répertoire a été créé entre temps par un autre
            // thread => OK
            if ((!result) && (!file.exists()))
            {
                throw new ExploitationException("IMPOSSIBLE DE CREER LE REPERTOIRE " + dir);
            }
        }

        // On teste qu'il s'agit bien d'un répertoire
        if (!file.isDirectory())
        {
            throw new ProgrammationException("LE CHEMIN " + dir + " NE POINTE PAS VERS UN REPERTOIRE");
        }

        // On teste que le répertoire est bien accessible en lecture
        // /écriture
        if (!(file.canRead() && file.canRead()))
        {
            throw new ExploitationException("LE REPERTOIRE " + dir + " N'EST PAS ACCESSIBLE EN LECTURE / ECRITURE");
        }

        return file;
    }

}
