/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : EditionJob.java
 *
 */
package fr.gouv.finances.lombok.edition.service.impl;

import java.util.Map;

import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.SchedulerContext;
import org.quartz.SchedulerException;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.context.ApplicationContext;

import fr.gouv.finances.lombok.edition.bean.JobHistory;
import fr.gouv.finances.lombok.edition.service.EditionProducerService;
import fr.gouv.finances.lombok.util.exception.ApplicationExceptionTransformateur;
import fr.gouv.finances.lombok.util.exception.ProgrammationException;

/**
 * Class EditionJob --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.2 $ Date: 11 déc. 2009
 */
public class EditionJob implements Job
{

    /** Constant : PARAMETRES_EDITION. */
    public static final String PARAMETRES_EDITION = "parametresEdition";

    /** Constant : JOB_HISTORY. */
    public static final String JOB_HISTORY = "JobHistory";

    /** Constant : EDITION_COMPRESSION. */
    public static final String EDITION_COMPRESSION = "FlagEditionCompression";

    /** Constant : APPLICATION_CONTEXT_KEY. */
    public static final String APPLICATION_CONTEXT_KEY = "applicationContext";
    
    public EditionJob()
    {
        super();
    }

    /**
     * methode Extract edition compression : --.
     * 
     * @param jobDataMap --
     * @return boolean
     */
    public static Boolean extractEditionCompression(Map jobDataMap)
    {
        Boolean result = null;

        if (jobDataMap.get(EditionJob.EDITION_COMPRESSION) instanceof Boolean)
        {
            result = (Boolean) jobDataMap.get(EditionJob.EDITION_COMPRESSION);
        }
        return result;
    }

    /**
     * methode Extract job history : --.
     * 
     * @param jobDataMap --
     * @return job history
     */
    public static JobHistory extractJobHistory(Map jobDataMap)
    {
        JobHistory result = null;

        if (jobDataMap.get(EditionJob.JOB_HISTORY) instanceof JobHistory)
        {
            result = (JobHistory) jobDataMap.get(EditionJob.JOB_HISTORY);
        }
        return result;
    }

    /**
     * methode Extract parametres edition : --.
     * 
     * @param jobDataMap --
     * @return map
     */
    public static Map extractParametresEdition(Map jobDataMap)
    {
        Map result = null;

        if (jobDataMap.get(EditionJob.PARAMETRES_EDITION) instanceof Map)
        {
            result = (Map) jobDataMap.get(EditionJob.PARAMETRES_EDITION);
        }
        return result;
    }

    /**
     * methode Fill edition compression : --.
     * 
     * @param jobDataMap --
     * @param compression --
     */
    @SuppressWarnings("unchecked")
    public static void fillEditionCompression(Map jobDataMap, Boolean compression)
    {
        if (jobDataMap != null)
        {
            jobDataMap.put(EditionJob.EDITION_COMPRESSION, compression);
        }
    }

    /**
     * methode Fill job history : --.
     * 
     * @param jobDataMap --
     * @param jobHistory --
     */
    @SuppressWarnings("unchecked")
    public static void fillJobHistory(Map jobDataMap, JobHistory jobHistory)
    {
        if (jobDataMap != null)
        {
            jobDataMap.put(EditionJob.JOB_HISTORY, jobHistory);
        }
    }

    /**
     * methode Fill parametres edition : --.
     * 
     * @param jobDataMap --
     * @param parametresEdition --
     */
    @SuppressWarnings("unchecked")
    public static void fillParametresEdition(Map jobDataMap, Map parametresEdition)
    {
        if (jobDataMap != null)
        {
            jobDataMap.put(EditionJob.PARAMETRES_EDITION, parametresEdition);
        }
    }

    /** application context. */
    private ApplicationContext applicationContext;



    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param jobExecutionContext
     * @throws JobExecutionException the job execution exception
     * @see org.quartz.Job#execute(org.quartz.JobExecutionContext)
     */
    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException
    {
        // Récupération de la référence au service producteur d'éditions
        EditionProducerService editionProducerService = this.getEditionProducerService(jobExecutionContext);

        // Lecture des paramètres de l'édition dans le contexte du Job
        JobDataMap jobDataMap = jobExecutionContext.getJobDetail().getJobDataMap();
        Map parametresEdition = EditionJob.extractParametresEdition(jobDataMap);
        JobHistory jobHistory = EditionJob.extractJobHistory(jobDataMap);
        Boolean compression = EditionJob.extractEditionCompression(jobDataMap);

        if (compression != null && compression.booleanValue() == true)
        {
            // Exécution et compression de l'édition
            editionProducerService.creerEtStockerEditionCompressee(jobHistory, parametresEdition);
        }
        else
        {
            // Exécution de l'édition
            editionProducerService.creerEtStockerEdition(jobHistory, parametresEdition);
        }

    }

    /**
     * Accesseur de l attribut application context.
     * 
     * @return application context
     */
    public ApplicationContext getApplicationContext()
    {
        return applicationContext;
    }

    /**
     * Modificateur de l attribut application context.
     * 
     * @param applicationContext le nouveau application context
     */
    public void setApplicationContext(ApplicationContext applicationContext)
    {
        this.applicationContext = applicationContext;
    }

    /**
     * Retourne la référence du service de production et de stockage des éditions.
     * 
     * @param jobExecutionContext --
     * @return EditionProducerService
     */
    private EditionProducerService getEditionProducerService(JobExecutionContext jobExecutionContext)
    {
        EditionProducerService editionProducerService = null;
        SchedulerContext schedulerContext = null;

        try
        {
            schedulerContext = jobExecutionContext.getScheduler().getContext();
        }
        catch (SchedulerException e)
        {
            throw ApplicationExceptionTransformateur.transformer(e);
        }
        this.applicationContext = (ApplicationContext) schedulerContext.get(APPLICATION_CONTEXT_KEY);

        String[] beanTab = this.applicationContext.getBeanNamesForType(EditionProducerService.class);

        if (beanTab == null || beanTab.length == 0)
        {
            throw new ProgrammationException("AUCUN BEAN DE TYPE " + EditionProducerService.class.getName()
                + " N'EST PRESENT DANS LE CONTEXTE");
        }
        else
        {
            String producerServiceSoName = beanTab[0];

            if (this.applicationContext.containsBean(producerServiceSoName))
            {
                try
                {
                    editionProducerService =
                        (EditionProducerService) this.applicationContext.getBean(producerServiceSoName,
                            EditionProducerService.class);
                }
                catch (NoSuchBeanDefinitionException e)
                {
                    throw new ProgrammationException("LE BEAN " + producerServiceSoName
                        + " N'EST PAS DE TYPE EDITIONPRODUCERSERVICE", e);
                }
            }
            else
            {
                throw new ProgrammationException("LE BEAN " + producerServiceSoName
                    + " N'EST PAS PRESENT DANS LE CONTEXTE");
            }
        }
        return editionProducerService;
    }

}
