/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 */
package fr.gouv.finances.lombok.edition.techbean;

import java.util.Date;

import fr.gouv.finances.lombok.util.base.BaseBean;

/**
 * Statistique des travaux d'édition.
 *
 * @author amleplatinec
 * @author Christophe Breheret-Girardin
 */
public class StatJobHistory extends BaseBean
{
    /** Initialisation de l'UID. */
    private static final long serialVersionUID = 1L;

    /** Statut. */
    private String status;

    /** nombre editions. */
    private long nombreEditions;

    /** date purge. */
    private Date datePurge;

    /** jour demande. */
    private Date jourDemande;

    /** bean edition id. */
    private String beanEditionId;

    /** description. */
    private String description;

    /** taille totale. */
    private Long tailleTotale;

    /** taille moyenne. */
    private Double tailleMoyenne;

    /**
     * Constructeur.
     */
    public StatJobHistory()
    {
        super();
    }

    /**
     * Constructeur.
     * @param status statut des éditions
     * @param nombreEditions nombre d'éditions
     */
    public StatJobHistory(String status, long nombreEditions)
    {
        super();
        this.status = status;
        this.nombreEditions = nombreEditions;
    }

    /**
     * Constructeur.
     * @param datePurge date de demande de purge
     * @param nombreEditions nombre d'éditions
     */
    public StatJobHistory(Date datePurge, long nombreEditions)
    {
        super();
        this.datePurge = datePurge;
        this.nombreEditions = nombreEditions;
    }

    /**
     * Constructeur.
     * @param nombreEditions nombre d'éditions
     * @param jourDemande jour de demande de l'édition
     */
    public StatJobHistory(long nombreEditions, Date jourDemande)
    {
        super();
        this.jourDemande = jourDemande;
        this.nombreEditions = nombreEditions;
    }

    /**
     * Constructeur.
     * @param status statut des éditions
     * @param beanEditionId identifiant de l'édition
     * @param nombreEditions nombre d'éditions
     */
    public StatJobHistory(String status, String beanEditionId, long nombreEditions)
    {
        super();
        this.status = status;
        this.beanEditionId = beanEditionId;
        this.nombreEditions = nombreEditions;
    }

    /**
     * Constructeur.
     * @param status statut des éditions
     * @param beanEditionId identifiant de l'édition
     * @param nombreEditions nombre d'éditions
     * @param tailleTotale taille totale des éditions
     * @param tailleMoyenne taille moyenne des éditions
     */
    public StatJobHistory(String status, String beanEditionId, long nombreEditions, Long tailleTotale
        , Double tailleMoyenne)
    {
        super();
        this.status = status;
        this.beanEditionId = beanEditionId;
        this.nombreEditions = nombreEditions;
        this.tailleTotale = tailleTotale;
        this.tailleMoyenne = tailleMoyenne;
    }

    /**
     * Accesseur de l'attribut bean edition id.
     * 
     * @return bean edition id
     */
    public String getBeanEditionId()
    {
        return beanEditionId;
    }

    /**
     * Accesseur de l'attribut date purge.
     * 
     * @return date purge
     */
    public Date getDatePurge()
    {
        return datePurge;
    }

    /**
     * Accesseur de l'attribut description.
     * 
     * @return description
     */
    public String getDescription()
    {
        return description;
    }

    /**
     * Accesseur de l'attribut jour demande.
     * 
     * @return jour demande
     */
    public Date getJourDemande()
    {
        return jourDemande;
    }

    /**
     * Accesseur de l'attribut nombre editions.
     * 
     * @return nombre editions
     */
    public long getNombreEditions()
    {
        return nombreEditions;
    }

    /**
     * Accesseur de l'attribut status.
     * 
     * @return status
     */
    public String getStatus()
    {
        return status;
    }

    /**
     * Accesseur de l'attribut taille moyenne.
     * 
     * @return taille moyenne
     */
    public Double getTailleMoyenne()
    {
        return tailleMoyenne;
    }

    /**
     * Accesseur de l'attribut taille totale.
     * 
     * @return taille totale
     */
    public Long getTailleTotale()
    {
        return tailleTotale;
    }

    /**
     * Modificateur de l'attribut bean edition id.
     * 
     * @param beanEditionId le nouveau bean edition id
     */
    public void setBeanEditionId(String beanEditionId)
    {
        this.beanEditionId = beanEditionId;
    }

    /**
     * Modificateur de l'attribut date purge.
     * 
     * @param datePurge le nouveau date purge
     */
    public void setDatePurge(Date datePurge)
    {
        this.datePurge = datePurge;
    }

    /**
     * Modificateur de l'attribut description.
     * 
     * @param description le nouveau description
     */
    public void setDescription(String description)
    {
        this.description = description;
    }

    /**
     * Modificateur de l'attribut jour demande.
     * 
     * @param jourDemande le nouveau jour demande
     */
    public void setJourDemande(Date jourDemande)
    {
        this.jourDemande = jourDemande;
    }

    /**
     * Modificateur de l'attribut nombre editions.
     * 
     * @param nombreEditions le nouveau nombre editions
     */
    public void setNombreEditions(long nombreEditions)
    {
        this.nombreEditions = nombreEditions;
    }

    /**
     * Modificateur de l'attribut status.
     * 
     * @param status le nouveau status
     */
    public void setStatus(String status)
    {
        this.status = status;
    }

    /**
     * Modificateur de l'attribut taille moyenne.
     * 
     * @param tailleMoyenne le nouveau taille moyenne
     */
    public void setTailleMoyenne(Double tailleMoyenne)
    {
        this.tailleMoyenne = tailleMoyenne;
    }

    /**
     * Modificateur de l'attribut taille totale.
     * 
     * @param tailleTotale le nouveau taille totale
     */
    public void setTailleTotale(Long tailleTotale)
    {
        this.tailleTotale = tailleTotale;
    }

}
