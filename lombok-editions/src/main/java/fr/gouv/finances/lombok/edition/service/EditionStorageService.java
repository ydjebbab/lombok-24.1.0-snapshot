/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : EditionStorageService.java
 *
 */
package fr.gouv.finances.lombok.edition.service;

import fr.gouv.finances.lombok.edition.bean.JobHistory;
import fr.gouv.finances.lombok.upload.bean.FichierJoint;
import fr.gouv.finances.lombok.util.exception.RegleGestionException;

/**
 * Interface EditionStorageService --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.2 $ Date: 11 déc. 2009
 */
public interface EditionStorageService
{

    /**
     * Charge le contenu d'une édition identifiée par son objet jobHistory.
     * 
     * @param jobHistory --
     * @return Fichierjoint associé à l'édition
     * @throws RegleGestionException the regle gestion exception
     */
    public FichierJoint chargerFichierEdition(JobHistory jobHistory) throws RegleGestionException;

    /**
     * Charge le contenu d'une édition identifiée par son objet jobHistory Met à jour la liste des utilisateur qui ont
     * consulté l'édition.
     * 
     * @param jobHistory --
     * @param uid utilisateur qui consulte l'édition
     * @return Fichierjoint associé à l'édition
     * @throws RegleGestionException the regle gestion exception
     */
    public FichierJoint chargerFichierEdition(JobHistory jobHistory, String uid) throws RegleGestionException;

    /**
     * Efface une édition.
     * 
     * @param jobHistory --
     */
    public void effacerFichierEdition(JobHistory jobHistory);

    /**
     * Stocke une édition dans un système de fichier.
     * 
     * @param jobHistory --
     * @param fichierJoint --
     */
    public void stockerEdition(JobHistory jobHistory, FichierJoint fichierJoint);

    /**
     * Supprime toutes les entrées de l'historique dans l'état DISPONIBLE dont le fichier dans le SF n'existe plus.
     * 
     * @param nombreOccurences --
     * @param applicationOrigine --
     */
    public void synchroniseHistoriqueAvecSf(final int nombreOccurences, final String applicationOrigine);

    /**
     * Supprime tous les fichiers de l'arborescence qui ne correspondent à aucune entrée dans l'historique des édition.
     * 
     * @param applicationOrigine --
     */
    public void synchroniseSfAvecHistorique(final String applicationOrigine);
}
