/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 */
package fr.gouv.finances.lombok.edition.util;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.gouv.finances.lombok.util.exception.ApplicationExceptionTransformateur;
import fr.gouv.finances.lombok.util.exception.ProgrammationException;

/**
 * Utilitaire de gestion de fichier.
 *
 * @author Christophe Breheret-Girardin
 */
public final class FileUtils
{
    /** Initialisation de la journalisation. */
    private static final Logger LOGGER = LoggerFactory.getLogger(FileUtils.class);

    /** Nom utilisé pour la création d'un fichier temporaire. */
    private static final String FICHIER_TEMPORAIRE_PREFIX = "chargement";

    /** Extension utilisée pour la création d'un fichier temporaire. */
    private static final String FICHIER_TEMPORAIRE_EXTENSION = ".tmp";

    /**
     * Constructeur.
     *
     */
    private FileUtils()
    {
        super();
    }

    /**
     * Méthode permettant de convertir un tableau d'octet en fichier.
     *
     * @param data données sous forme de tableau d'octet
     * @return un fichier temporaire contenant les données contenues dans le tableau d'octet
     */
    public static File getFichier(byte[] data)
    {
        // Si pas de données, mauvaise utilisation de l'utilitaire
        if (data == null)
        {
            throw new ProgrammationException("Aucune donnée n'a été fournie pour une conversion en fichier");
        }

        // Création d'un fichier temporaire à partir des données fournies
        File fichierTemporaire = getFichierTemporaire();
        try
        {
            Files.write(fichierTemporaire.toPath(), data);
            return fichierTemporaire;
        }
        catch (IOException exception)
        {
            LOGGER.warn("Erreur d'écriture au sein du fichier '{}' : {}"
                , fichierTemporaire.getAbsolutePath(), exception);
            throw ApplicationExceptionTransformateur.transformer(exception);
        }
    }

    /**
     * Méthode permettant de convertir un fichier en tableau d'octet.
     *
     * @param fichier fichier à lire
     * @return le tableau d'octet correspondant au contenu du fichier
     */
    public static byte[] getFichier(File fichier)
    {
        // Si pas de fichier, mauvaise utilisation de l'utilitaire
        if (fichier == null)
        {
            throw new ProgrammationException("Aucun fichier n'a été fourni pour une conversion en tableau d'octet");
        }
        
        try
        {
            // Lecture des données du fichier
            return Files.readAllBytes(fichier.toPath());
        }
        catch (IOException exception)
        {
            LOGGER.warn("Erreur de leur du fichier '{}' : {}"
                , fichier.getAbsolutePath(), exception);
            throw ApplicationExceptionTransformateur.transformer(exception);
        }
    }

    /**
     * Méthode permettant de récupérer un fichier temporaire vide.
     *
     * @return un fichier temporaire
     */
    public static File getFichierTemporaire()
    {
        try
        {
            // Récupération d'un fichier temporaire
            File fichierTemporaire = File.createTempFile(
                String.join("", FICHIER_TEMPORAIRE_PREFIX, String.valueOf(System.currentTimeMillis()))
                , FICHIER_TEMPORAIRE_EXTENSION);
            fichierTemporaire.deleteOnExit();

            // Retour du fichier
            return fichierTemporaire;
        }
        catch (IOException exception)
        {
            // Gestion des erreurs
            LOGGER.warn("Erreur de création d'un fichier temporaire : {}", exception);
            throw ApplicationExceptionTransformateur.transformer(exception);
        }
    }

    /**
     * Méthode permettant de récupérer un fichier temporaire renseigné.
     *
     * @param contenu contenu à insérer dans le fichier
     * @return un fichier temporaire
     */
    public static File getFichierTemporaire(String contenu)
    {
        try
        {
            // Récupération d'un fichier temporaire
            File fichierTemporaire = getFichierTemporaire();

            // Ecriture du contenu au sein du fichier
            org.apache.commons.io.FileUtils.writeStringToFile(fichierTemporaire, contenu);

            // Retour du fichier
            return fichierTemporaire;
        }
        catch (IOException exception)
        {
            // Gestion des erreurs
            LOGGER.warn("Erreur de création d'un fichier temporaire : {}", exception);
            throw ApplicationExceptionTransformateur.transformer(exception);
        }
    }
}