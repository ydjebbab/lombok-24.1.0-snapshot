/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : AdministrationPurgesEditionsFormAction.java
 *
 */
package fr.gouv.finances.lombok.edition.mvc.webflow.action;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.springframework.beans.PropertyEditorRegistry;
import org.springframework.validation.Errors;
import org.springframework.webflow.action.FormAction;
import org.springframework.webflow.action.FormObjectAccessor;
import org.springframework.webflow.execution.Event;
import org.springframework.webflow.execution.RequestContext;
import org.springframework.webflow.execution.ScopeType;

import fr.gouv.finances.lombok.apptags.util.CheckboxSelectUtil;
import fr.gouv.finances.lombok.apptags.util.MaxFetchTableUtil;
import fr.gouv.finances.lombok.edition.bean.TrcPurgeEdition;
import fr.gouv.finances.lombok.edition.mvc.form.AdministrationPurgesEditionsForm;
import fr.gouv.finances.lombok.edition.service.EditionAdministrationPurgesService;
import fr.gouv.finances.lombok.util.exception.ApplicationException;
import fr.gouv.finances.lombok.util.propertyeditor.CpCustomDateEditor;

/**
 * Class AdministrationPurgesEditionsFormAction --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.4 $ Date: 11 déc. 2009
 */
public class AdministrationPurgesEditionsFormAction extends FormAction
{

    /** ELEMENT s_ dan s_ l e_ flux. */
    private static String elEMENTSDANSLEFLUX = "adminlistedestracesdepurge";

    /** ELEMEN t_ selectionn e_ dan s_ l e_ flux. */
    private static String elEMENTSELECTIONNEDANSLEFLUX = "tracedepurge";

    /** ELT s_ selectionne s_ dan s_ flux. */
    private static String elTSSELECTIONNESDANSFLUX = "tracesdepurgeasupprimer";

    /** TABLEID. */
    private static String taBLEID = "adminlistedestracesdepurge";

    /** MA x_ ligne. */
    private static int maXLIGNE = 1000;

    /** editionadministrationpurgesserviceso. */
    private EditionAdministrationPurgesService editionadministrationpurgesserviceso;

    /**
     * Instanciation de administration purges editions form action.
     */
    public AdministrationPurgesEditionsFormAction()
    {
        super();
    }

    /**
     * Instanciation de administration purges editions form action.
     * 
     * @param arg0 --
     */
    public AdministrationPurgesEditionsFormAction(Class arg0)
    {
        super(arg0);
    }

    /**
     * methode Demander arret purge edition : --.
     * 
     * @param requestContext --
     * @return event
     */
    public Event demanderArretPurgeEdition(RequestContext requestContext)
    {
        Event result = success();
        try
        {
            Collection purgesAArreter = requestContext.getFlowScope().getCollection(elEMENTSELECTIONNEDANSLEFLUX);

            if (purgesAArreter.iterator().hasNext())
            {
                TrcPurgeEdition trcPurgeEdition = (TrcPurgeEdition) purgesAArreter.iterator().next();
                this.editionadministrationpurgesserviceso.demanderArretPurgeEdition(trcPurgeEdition);
            }
        }
        catch (ApplicationException exception)
        {
            logger.debug("Exception détectée : ", exception);
            Errors errors =
                new FormObjectAccessor(requestContext).getFormErrors(getFormObjectName(), ScopeType.REQUEST);
            errors.reject(null, exception.getMessage());
            result = error();
        }

        return result;
    }

    /**
     * methode Parametrer checkboxes : --.
     * 
     * @param request --
     * @return event
     */
    public Event parametrerCheckboxes(RequestContext request)
    {
        // Sauvegarde de l'objet de paramétrage des checkboxes dans le
        // FlowScope
        CheckboxSelectUtil.parametrerCheckboxes(request, taBLEID).utiliserRowid("id").utiliserListeElements(
            elEMENTSDANSLEFLUX).utiliserMsgSelectionneUnSeulElement("Sélectionnez une seule trace de purge à la fois.")
            .utiliserMsgAucunEltSelectionne("Aucune trace de purge n'est sélectionnée")
            .stockerLesElementsSelectionnesDansFlowScope().stockerLesMessagesDansRequestScope();
        return success();
    }

    /**
     * methode Preparer liste purges par periode date purge : --.
     * 
     * @param requestContext --
     * @return event
     */
    public Event preparerListePurgesParPeriodeDatePurge(RequestContext requestContext)
    {
        AdministrationPurgesEditionsForm administrationPurgeEditionsForm =
            getAdministrationPurgeEditionsForm(requestContext);
        List listetrcPurgeEdition =
            editionadministrationpurgesserviceso.rechercherPurgesEditionsParPeriodePurge(
                administrationPurgeEditionsForm.getDebutPeriodePurge(), administrationPurgeEditionsForm
                    .getFinPeriodePurge(), maXLIGNE);

        requestContext.getFlowScope().put(elEMENTSDANSLEFLUX, listetrcPurgeEdition);
        MaxFetchTableUtil.gererMaxFetch(requestContext, taBLEID, elEMENTSDANSLEFLUX, maXLIGNE + 1);

        return success();
    }

    /**
     * methode Preparer liste toutes trc purge : --.
     * 
     * @param requestContext --
     * @return event
     */
    public Event preparerListeToutesTrcPurge(RequestContext requestContext)
    {
        List listetrcPurgeEdition = editionadministrationpurgesserviceso.rechercherPurgesEditions(maXLIGNE);

        requestContext.getFlowScope().put(elEMENTSDANSLEFLUX, listetrcPurgeEdition);
        MaxFetchTableUtil.gererMaxFetch(requestContext, taBLEID, elEMENTSDANSLEFLUX, maXLIGNE + 1);
        return success();
    };

    /**
     * methode Preparer liste trc purge par date debut : --.
     * 
     * @param requestContext --
     * @return event
     */
    public Event preparerListeTrcPurgeParDateDebut(RequestContext requestContext)
    {
        AdministrationPurgesEditionsForm administrationPurgeEditionsForm =
            getAdministrationPurgeEditionsForm(requestContext);
        List listetrcPurgeEdition =
            editionadministrationpurgesserviceso.rechercherPurgesEditionsParDateDemande(administrationPurgeEditionsForm
                .getDateDebutPurge(), maXLIGNE);

        requestContext.getFlowScope().put(elEMENTSDANSLEFLUX, listetrcPurgeEdition);
        MaxFetchTableUtil.gererMaxFetch(requestContext, taBLEID, elEMENTSDANSLEFLUX, maXLIGNE + 1);

        return success();
    }

    /**
     * methode Reinitialiser le formulaire : --.
     * 
     * @param requestContext --
     * @return event
     */
    public Event reinitialiserLeFormulaire(RequestContext requestContext)
    {
        AdministrationPurgesEditionsForm administrationPurgeEditionsForm =
            getAdministrationPurgeEditionsForm(requestContext);
        administrationPurgeEditionsForm.setDateDebutPurge(null);
        administrationPurgeEditionsForm.setDebutPeriodePurge(null);
        administrationPurgeEditionsForm.setFinPeriodePurge(null);
        return success();
    }

    /**
     * methode Selectionner purge edition a arreter : --.
     * 
     * @param requestContext --
     * @return event
     */
    public Event selectionnerPurgeEditionAArreter(RequestContext requestContext)
    {
        Event result;
        Collection elementsSelectionnes =
            CheckboxSelectUtil.extraitLesElementsSelectionnesDansLeFlux(requestContext, taBLEID);

        if (CheckboxSelectUtil.unEtUnSeulElementSelectionne(elementsSelectionnes))
        {
            requestContext.getFlowScope().put(elEMENTSELECTIONNEDANSLEFLUX, elementsSelectionnes);
            result = success();
        }
        else
        {
            result = error();
        }
        return result;
    }

    /**
     * methode Selectionner purges editions a supprimer : --.
     * 
     * @param requestContext --
     * @return event
     */
    public Event selectionnerPurgesEditionsASupprimer(RequestContext requestContext)
    {
        Event result;
        Collection elementsSelectionnes =
            CheckboxSelectUtil.extraitLesElementsSelectionnesDansLeFlux(requestContext, taBLEID);

        if (CheckboxSelectUtil.auMoinsUnElementSelectionne(elementsSelectionnes))
        {
            requestContext.getFlowScope().put(elTSSELECTIONNESDANSFLUX, elementsSelectionnes);
            result = success();
        }
        else
        {
            result = error();
        }
        return result;
    }

    /**
     * Modificateur de l attribut editionadministrationpurgesserviceso.
     * 
     * @param editionadministrationpurgesserviceso le nouveau editionadministrationpurgesserviceso
     */
    public void setEditionadministrationpurgesserviceso(
        EditionAdministrationPurgesService editionadministrationpurgesserviceso)
    {
        this.editionadministrationpurgesserviceso = editionadministrationpurgesserviceso;
    }

    /**
     * methode Supprimertrc purge : --.
     * 
     * @param requestContext --
     * @return event
     */
    @SuppressWarnings("unchecked")
    public Event supprimertrcPurge(RequestContext requestContext)
    {
        Event result = success();
        Collection elementsASupprimer = requestContext.getFlowScope().getCollection(elTSSELECTIONNESDANSFLUX);

        try
        {
            this.editionadministrationpurgesserviceso.supprimerTrcPurgeEdition(elementsASupprimer);
            Collection listeDesTrcPurge = requestContext.getFlowScope().getCollection(elEMENTSDANSLEFLUX);
            listeDesTrcPurge.removeAll(elementsASupprimer);
        }
        catch (ApplicationException exception)
        {
            logger.debug("Exception détectée : ", exception);
            Errors errors =
                new FormObjectAccessor(requestContext).getFormErrors(getFormObjectName(), ScopeType.REQUEST);
            errors.reject(null, exception.getMessage());
            result = error();
        }

        return result;
    }

    /**
     * methode Traiter checkboxes entre pages : --.
     * 
     * @param context --
     * @return event
     */
    public Event traiterCheckboxesEntrePages(RequestContext context)
    {
        CheckboxSelectUtil.traiterCheckboxesEntrePages(context, taBLEID);
        return success();
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param context
     * @param registry
     * @see org.springframework.webflow.action.FormAction#registerPropertyEditors(org.springframework.webflow.execution.RequestContext,
     *      org.springframework.beans.PropertyEditorRegistry)
     */
    @Override
    protected void registerPropertyEditors(RequestContext context, PropertyEditorRegistry registry)
    {
        super.registerPropertyEditors(context, registry);
        registry.registerCustomEditor(Date.class, new CpCustomDateEditor());
    }

    /**
     * Accesseur de l attribut administration purge editions form.
     * 
     * @param requestContext --
     * @return administration purge editions form
     */
    private AdministrationPurgesEditionsForm getAdministrationPurgeEditionsForm(RequestContext requestContext)
    {
        return (AdministrationPurgesEditionsForm) requestContext.getFlowScope().getRequired(getFormObjectName(),
            AdministrationPurgesEditionsForm.class);
    }
}