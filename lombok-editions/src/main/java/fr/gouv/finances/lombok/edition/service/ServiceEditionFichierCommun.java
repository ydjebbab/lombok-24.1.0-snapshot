/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : ServiceEditionFichierCommun.java
 *
 */
package fr.gouv.finances.lombok.edition.service;

/**
 * Interface ServiceEditionFichierCommun --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.2 $ Date: 11 déc. 2009
 */
public interface ServiceEditionFichierCommun
{

}
