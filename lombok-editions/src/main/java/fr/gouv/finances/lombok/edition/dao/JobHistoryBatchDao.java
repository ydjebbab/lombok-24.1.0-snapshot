/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 */
package fr.gouv.finances.lombok.edition.dao;

import java.util.Date;

import fr.gouv.finances.lombok.util.base.CoreBaseDao;
import fr.gouv.finances.lombok.util.persistance.ScrollIterator;

/**
 * Interface du DAO de gestion des données, d'historique des travaux d'édition, en batch.
 *
 * @author amleplatinec
 * @author Christophe Breheret-Girardin
 */
public interface JobHistoryBatchDao extends CoreBaseDao
{

    /**
     * Méthode permettant de rechercher des éditions qui doivent être purgées, en mode curseur.
     *
     * @param nombreOccurences taille d'extraction des données (important pour les résultats volumineux)
     * @param dateCourante restriction aux éditions antérieures à cette date
     * @param applicationOrigine application sur laquelle porte les éditions recherchées
     * @return un itérateur permettant de parcourir les résultats en mode curseur
     */
    public ScrollIterator listeEditionAPurgerIterator(int nombreOccurences, final Date dateCourante,
        final String applicationOrigine);

    /**
     * Méthode permettant de rechercher des éditions au statut "disponible" (ou "disponible non envoyé"),
     * en mode curseur.
     *
     * @param nombreOccurences taille d'extraction des données (important pour les résultats volumineux)
     * @param applicationOrigine application sur laquelle porte les éditions recherchées
     * @return un itérateur permettant de parcourir les résultats en mode curseur
     */
    public ScrollIterator listeEditionsDisponiblesIterator(int nombreOccurences, final String applicationOrigine);

    /**
     * Méthode permettant de rechercher des éditions au statut "échec", en mode curseur.
     *
     * @param nombreOccurences taille d'extraction des données (important pour les résultats volumineux)
     * @param applicationOrigine application sur laquelle porte les éditions recherchées
     * @return un itérateur permettant de parcourir les résultats en mode curseur
     */
    public ScrollIterator listeEditionsEnEchecIterator(int nombreOccurences, final String applicationOrigine);
}