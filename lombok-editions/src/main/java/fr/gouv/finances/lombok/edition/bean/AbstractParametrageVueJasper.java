/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 */
package fr.gouv.finances.lombok.edition.bean;

import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.view.jasperreports.JasperReportsMultiFormatView;

import fr.gouv.finances.lombok.util.base.BaseBean;

/**
 * Classe mère du paramétrage des vues Jasper
 * 
 * @author amleplatinec
 * @author Christophe Breheret-Girardin
 */
public abstract class AbstractParametrageVueJasper extends BaseBean
{
    /** Initialisation de la journalisation. */
    protected static final Logger LOGGER = LoggerFactory.getLogger(AbstractParametrageVueJasper.class);

    /* Clé de stockage de la source de données principale (datasource) -- */
    /** Constant : REPORT_DATA_KEY. */
    public static final String REPORT_DATA_KEY = "reportDataKey";

    /* Clé de stockage du nom du répertoire contenant les sous-rapports */
    /** Constant : SUBREPORT_DIR. */
    public static final String SUBREPORT_DIR = "SUBREPORT_DIR";

    /* Clé de stockage du nom du répertoire contenant les sous-rapports commun */
    /** Constant : SUBREPORT_DIR. */
    public static final String COMMUN_SUBREPORT_DIR = "COMMUN_SUBREPORT_DIR";

    /*
     * Cle de stockage du nom du répertoire contenant les images à inclure dans le rapport
     */
    /** Constant : IMAGES_SOURCE_DIR. */
    public static final String IMAGES_SOURCE_DIR = "IMAGES_SOURCE_DIR";

    /* Clé de stockage du type de vue (pdf,csv,xls,html) */
    /** Constant : REPORT_VIEW_KEY. */
    public static final String REPORT_VIEW_KEY = JasperReportsMultiFormatView.DEFAULT_FORMAT_KEY;

    /** Constant : REPORT_FILENAME_KEY. */
    public static final String REPORT_FILENAME_KEY = "reportFilenameKey";

    /** Constant : CONTENT_TYPE_KEY. */
    public static final String CONTENT_TYPE_KEY = "contentTypeKey";

    /** Constant : MIME_TYPE_KEY. */
    public static final String MIME_TYPE_KEY = "mimeTypeKey";

    /** Constant : EXTENSION_KEY. */
    public static final String EXTENSION_KEY = "extensionKey";

    /** Constant : NOM_FICHIER_PAR_DEFAUT. */
    public static final String NOM_FICHIER_PAR_DEFAUT = "edition";

    /** Constant : CHARACTER_ENCODING. */
    protected static final String CHARACTER_ENCODING =
        "net.sf.jasperreports.engine.JRExporterParameter.CHARACTER_ENCODING";

    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** format edition. */
    protected String formatEdition;

    /** type mime edition. */
    protected String typeMimeEdition;

    /** extension. */
    protected String extension;

    /** content type. */
    protected String contentType;

    /** character encoding. */
    protected String characterEncoding;

    /**
     * Instanciation de abstract parametrage vue jasper.
     */
    public AbstractParametrageVueJasper()
    {
        super();
    }

    /**
     * Accesseur de l attribut character encoding.
     * 
     * @return character encoding
     */
    public String getCharacterEncoding()
    {
        return characterEncoding;
    }

    /**
     * Accesseur de l attribut content type.
     * 
     * @return content type
     */
    public String getContentType()
    {
        return contentType;
    }

    /**
     * Accesseur de l attribut extension.
     * 
     * @return extension
     */
    public String getExtension()
    {
        return extension;
    }

    /**
     * Accesseur de l attribut format edition.
     * 
     * @return format edition
     */
    public String getFormatEdition()
    {
        return formatEdition;
    }

    /**
     * Accesseur de l attribut type mime edition.
     * 
     * @return type mime edition
     */
    public String getTypeMimeEdition()
    {
        return typeMimeEdition;
    }

    /**
     * methode Parametrage jr exporter : --.
     * 
     * @return map
     */
    public abstract Map parametrageJRExporter();

    /**
     * methode Parametrer vue : --.
     * 
     * @return map
     */
    public abstract Map parametrerVue();

    /**
     * Modificateur de l attribut character encoding.
     * 
     * @param characterEncoding le nouveau character encoding
     */
    public void setCharacterEncoding(String characterEncoding)
    {
        this.characterEncoding = characterEncoding;
    }

    /**
     * Modificateur de l attribut content type.
     * 
     * @param contentType le nouveau content type
     */
    public void setContentType(String contentType)
    {
        this.contentType = contentType;
    }

    /**
     * Modificateur de l attribut extension.
     * 
     * @param extension le nouveau extension
     */
    public void setExtension(String extension)
    {
        this.extension = extension;
    }

    /**
     * Modificateur de l attribut format edition.
     * 
     * @param formatEdition le nouveau format edition
     */
    public void setFormatEdition(String formatEdition)
    {
        this.formatEdition = formatEdition;
    }

    /**
     * Modificateur de l attribut type mime edition.
     * 
     * @param typeMimeEdition le nouveau type mime edition
     */
    public void setTypeMimeEdition(String typeMimeEdition)
    {
        this.typeMimeEdition = typeMimeEdition;
    }

    /**
     * Insere un couple parameterName x parameterValue dans une Map s'ils sont non nulls.
     * 
     * @param parameters --
     * @param parameterName --
     * @param parameterValue --
     */
    @SuppressWarnings("unchecked")
    protected void putIfNotNull(Map parameters, String parameterName, Object parameterValue)
    {
        if (parameters != null && parameterName != null && parameterValue != null
            && StringUtils.isNotBlank(parameterName))
        {
            if (parameterValue instanceof String)
            {
                if (StringUtils.isNotBlank((String) parameterValue))
                {
                    parameters.put(parameterName, parameterValue);
                }
            }
            else
            {
                parameters.put(parameterName, parameterValue);
            }
        }
    }

    /**
     * Méthode permettant de journaliser le paramétrage de JRExporter
     *
     * @param parametrage paramétrage à journaliser
     */
    protected void journaliserParametrageJRExporter(Map<String, Object> parametrage)
    {
        parametrage.entrySet().stream().forEach(
            entry -> LOGGER.debug(" paramètre JRExporter '{}' : {}", entry.getKey(), entry.getValue()));
    }

    /**
     * Méthode permettant de journaliser le paramétrage la vue Jasper
     *
     * @param parametrage paramétrage à journaliser
     */
    protected void journaliserParametrageVue(Map<String, String> parametrage)
    {
        parametrage.entrySet().stream().forEach(
            entry -> LOGGER.debug(" paramètre vue '{}' : {}", entry.getKey(), entry.getValue()));
    }
}
