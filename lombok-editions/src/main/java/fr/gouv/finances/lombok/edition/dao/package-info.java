/*
 * Copyright (c) 2017 DGFiP - Tous droits réservés
 * 
*/
/**
 * Paquet contenant les interfaces des DAO de gestion des données liées aux éditions.
 * @author Christophe Breheret-Girardin
 */
package fr.gouv.finances.lombok.edition.dao;