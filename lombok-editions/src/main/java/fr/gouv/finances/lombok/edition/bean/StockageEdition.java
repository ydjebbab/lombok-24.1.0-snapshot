/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 */
package fr.gouv.finances.lombok.edition.bean;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import fr.gouv.finances.lombok.util.base.BaseBean;
import fr.gouv.finances.lombok.util.exception.ProgrammationException;

/**
 * Entité StockageEdition
 *
 * @author amleplatinec
 * @author Christophe Breheret-Girardin
 */
public class StockageEdition extends BaseBean
{
    /** Initialisation de l'UID de la classe. */
    private static final long serialVersionUID = 1L;

    /** Constant : LOT_JOUR_PREFIX. */
    public static final String LOT_JOUR_PREFIX = "LOT";

    /** Constant : SEP. */
    private static final String SEP = "/";

    /** Flag qui indique si l'édition est stockée en base de données ou dans le système de fichiers. */
    private Boolean stockageEnBase;

    /** Chemin d'accès au répertoire servant de base pour le stockage des éditions. */
    private String appliEditionRoot;

    /** Sous répertoire du répertoire journalier contenant les éditions par groupe de N. */
    private Integer indexRepertoire = Integer.valueOf("-1");

    /** Chemin complet de l'édition dans le gestionnaire de fichier. */
    private String cheminComplet;

    /** Nom de stockage du fichier dans le SF. */
    private String nomFicStockage;

    /** Taille de l'édition en octets. */
    private Long tailleFichier;

    /** Contenu de l'édition - quand elle est stockée en base de donnée. */
    private ContenuEdition leContenuEdition;

    /**
     * Constructeur.
     */
    public StockageEdition()
    {
        super();
    }

    /**
     * Constructeur.
     *
     * @param indexRepertoire index du répertoire lié au stockage
     */
    public StockageEdition(Integer indexRepertoire)
    {
        super();
        this.indexRepertoire = indexRepertoire;
    }

    /**
     * Méthode permettant de calculer le répertoire de stockage N6.
     *
     * @param datejour date du jour
     * @param applicationOrigine application liée à l'édition
     * @param beanEditionId identifiant de l'édition
     * @param applicationEdiRoot racine
     * @return le répertoire de stockage N6
     */
    public static String calculeRepertoireStockageN6(Date datejour, String applicationOrigine, String beanEditionId,
        String applicationEdiRoot)
    {
        if (datejour == null)
        {
            throw new ProgrammationException("La date n'a pas été fournie");
        }

        return String.join(SEP, applicationEdiRoot, applicationOrigine, beanEditionId
                , repertoireAnnee(datejour), repertoireMois(datejour), repertoireJour(datejour));
    }

    /**
     * Méthode permettant de calculer le répertoire de stockage N7.
     *
     * @param datejour date du jour
     * @param applicationOrigine application liée à l'édition
     * @param beanEditionId identifiant de l'édition
     * @param index index
     * @param applicationEdiRoot racine
     * @return le répertoire de stockage N7
     */
    public static String calculeRepertoireStockageN7(Date datejour, String applicationOrigine, String beanEditionId,
        Integer index, String applicationEdiRoot)
    {
        //TODO L'index n'est pas utilisé, que faut-il faire ?
        return calculeRepertoireStockageN6(datejour, applicationOrigine, beanEditionId, applicationEdiRoot);
    }

    /**
     * Méthode permettant de récupérer la partie année d'une date
     *
     * @param date date
     * @return la partie année de la date
     */
    public static String repertoireAnnee(Date date)
    {
        DateFormat unJourFormat = new SimpleDateFormat("yyyy", Locale.FRANCE);
        return unJourFormat.format(date);
    }

    /**
     * Méthode permettant de récupérer la partie jour d'une date
     *
     * @param date date
     * @return la partie jour de la date
     */
    public static String repertoireJour(Date date)
    {
        DateFormat unJourFormat = new SimpleDateFormat("dd", Locale.FRANCE);
        return unJourFormat.format(date);
    }

    /**
     * Méthode permettant de récupérer la partie mois d'une date
     *
     * @param date date
     * @return la partie mois de la date
     */
    public static String repertoireMois(Date date)
    {
        DateFormat unJourFormat = new SimpleDateFormat("MM", Locale.FRANCE);
        return unJourFormat.format(date);
    }

    /**
     * Méthode permettant de récupérer un nom de répertoire correspondant au lot
     *
     * @param index index
     * @return le nom de répertoire
     */
    public static String repertoireLot(Integer index)
    {
        if (index == null)
        {
            throw new ProgrammationException("L'index n'a pas été fourni");
        }

        return String.join(".", LOT_JOUR_PREFIX, String.valueOf(index));
    }

    /**
     * Méthode permettant de fournir un nom de chemin absolu.
     *
     * @return nom de chemin absolu
     */
    public String fournirCheminAbsoluFichier()
    {
        return String.join(SEP, cheminComplet, nomFicStockage);
    }

    /**
     * Accesseur de appli edition root.
     *
     * @return valeur de appliEditionRoot
     */
    public String getAppliEditionRoot()
    {
        return appliEditionRoot;
    }

    /**
     * Accesseur de chemin complet.
     *
     * @return valeur de cheminComplet
     */
    public String getCheminComplet()
    {
        return cheminComplet;
    }

    /**
     * Accesseur de index repertoire.
     *
     * @return valeur de indexRepertoire
     */
    public Integer getIndexRepertoire()
    {
        return indexRepertoire;
    }

    /**
     * Accesseur de le contenu edition.
     *
     * @return valeur de leContenuEdition
     */
    public ContenuEdition getLeContenuEdition()
    {
        return leContenuEdition;
    }

    /**
     * Accesseur de nomFicStockage.
     *
     * @return valeur de nomFicStockage
     */
    public String getNomFicStockage()
    {
        return nomFicStockage;
    }

    /**
     * Accesseur de stockageEnBase.
     *
     * @return valeur de stockageEnBase
     */
    public Boolean getStockageEnBase()
    {
        return stockageEnBase;
    }

    /**
     * Accesseur de l attribut taille fichier.
     *
     * @return taille fichier
     */
    public Long getTailleFichier()
    {
        return tailleFichier;
    }

    /**
     * Mutateur de appliEditionRoot.
     *
     * @param appliEditionRoot à initialiser
     */
    public void setAppliEditionRoot(String appliEditionRoot)
    {
        this.appliEditionRoot = appliEditionRoot;
    }

    /**
     * Mutateur de cheminComplet.
     *
     * @param cheminComplet à initialiser
     */
    public void setCheminComplet(String cheminComplet)
    {
        this.cheminComplet = cheminComplet;
    }

    /**
     * Mutateur de indexRepertoire.
     *
     * @param indexRepertoire à initialiser
     */
    public void setIndexRepertoire(Integer indexRepertoire)
    {
        this.indexRepertoire = indexRepertoire;
    }

    /**
     * Mutateur de leContenuEdition.
     *
     * @param leContenuEdition à initialiser
     */
    public void setLeContenuEdition(ContenuEdition leContenuEdition)
    {
        this.leContenuEdition = leContenuEdition;
    }

    /**
     * Mutateur de nomFicStockage.
     *
     * @param nomFicStockage à initialiser
     */
    public void setNomFicStockage(String nomFicStockage)
    {
        this.nomFicStockage = nomFicStockage;
    }

    /**
     * Mutateur de stockageEnBase.
     *
     * @param stockageEnBase stockageEnBase à initialiser
     */
    public void setStockageEnBase(Boolean stockageEnBase)
    {
        this.stockageEnBase = stockageEnBase;
    }

    /**
     * Modificateur de l'attribut taille fichier.
     *
     * @param tailleFichier la nouvelle taille fichier
     */
    public void setTailleFichier(Long tailleFichier)
    {
        this.tailleFichier = tailleFichier;
    }

}
