/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : AbstractServiceEditionCsvCommunImpl.java
 *
 */
package fr.gouv.finances.lombok.edition.service.impl;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.context.support.ApplicationObjectSupport;

import fr.gouv.finances.lombok.edition.service.ServiceEditionCommun;
import fr.gouv.finances.lombok.upload.bean.FichierJoint;
import fr.gouv.finances.lombok.util.exception.ApplicationExceptionTransformateur;
import fr.gouv.finances.lombok.util.exception.ProgrammationException;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JRDesignBand;
import net.sf.jasperreports.engine.design.JRDesignExpression;
import net.sf.jasperreports.engine.design.JRDesignField;
import net.sf.jasperreports.engine.design.JRDesignQuery;
import net.sf.jasperreports.engine.design.JRDesignSection;
import net.sf.jasperreports.engine.design.JRDesignStaticText;
import net.sf.jasperreports.engine.design.JRDesignTextField;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.export.JRCsvExporter;
import net.sf.jasperreports.engine.export.JRCsvExporterParameter;
import net.sf.jasperreports.engine.fill.JRFileVirtualizer;
import net.sf.jasperreports.engine.query.JRHibernateQueryExecuterFactory;

/**
 * Class AbstractServiceEditionCsvCommunImpl.
 * 
 * @author chouard-cp
 * @version $Revision: 1.3 $ Date: 15 déc. 2009
 */
public abstract class AbstractServiceEditionCsvCommunImpl extends ApplicationObjectSupport implements
    ServiceEditionCommun
{

    /** Constant : UNIX_LINE_DELIMITER. */
    protected static final String UNIX_LINE_DELIMITER = "\n";

    /** Constant : WINDOWS_LINE_DELIMITER. */
    protected static final String WINDOWS_LINE_DELIMITER = "\r\n";

    /** Constant : DEF_EXTENSION. */
    private static final String DEF_EXTENSION = "txt";

    /** Constant : DEF_CHARACTER_ENCODING. */
    private static final String DEF_CHARACTER_ENCODING = "ISO-8859-1";

    /** Constant : DEF_CONTENT_TYPE. */
    private static final String DEF_CONTENT_TYPE = "text/plain;charset=ISO-8859-1";

    /** Constant : DEF_MIME_TYPE. */
    private static final String DEF_MIME_TYPE = "text/plain";

    /** Constant : DEF_FIELD_DELIMITER. */
    private static final String DEF_FIELD_DELIMITER = ";";

    /** Constant : DEF_RECORD_DELIMITER. */
    private static final String DEF_RECORD_DELIMITER = UNIX_LINE_DELIMITER;

    /** Constant : NOM_FICHIER_PAR_DEFAUT. */
    private static final String NOM_FICHIER_PAR_DEFAUT = "edition";

    protected Logger log = LoggerFactory.getLogger(getClass());

    /** jasper design. */
    protected JasperDesign jasperDesign = new JasperDesign();

    /** columns. */
    protected List<JRDesignTextField> columns = new ArrayList<JRDesignTextField>();

    /** columns names. */
    protected List<String> columnsNames = new ArrayList<String>();

    /** Flag qui indique si la description des champs doit être placée dans la première ligne de l'export cvs. */
    protected boolean inclureDescriptionChamp = true;

    /** Flag qui indique si l'édition utilise une connection jdbc vers la source de données. */
    private boolean modeconnecte = true;

    /** Extension utilisée dans le nom du fichier produit. */
    private String extension;

    /** Encodage du fichier. */
    private String characterEncoding;

    /** Valeur de l'attribut contentType. */
    private String contentType;

    /** Type Mime. */
    private String typeMime;

    /** Caractère de séparation des champs. */
    private String fieldDelimiter;

    /** Caractère de séparation des enregistrements. */
    private String recordDelimiter;

    /** Nombre de jours de conservation de l'édition après sa production. */
    private Integer nombreJoursConservation;

    /** Nom de l'édition. */
    private String nomEdition;

    /**
     * Instanciation de abstract service edition csv commun impl.
     */
    public AbstractServiceEditionCsvCommunImpl()
    {
        super();
        this.characterEncoding = DEF_CHARACTER_ENCODING;
        this.contentType = DEF_CONTENT_TYPE;
        this.extension = DEF_EXTENSION;
        this.fieldDelimiter = DEF_FIELD_DELIMITER;
        this.recordDelimiter = DEF_RECORD_DELIMITER;
        this.typeMime = DEF_MIME_TYPE;

        this.jasperDesign.setName("productioncsv");
        this.jasperDesign.setBottomMargin(0);
        this.jasperDesign.setTopMargin(0);
        this.jasperDesign.setLeftMargin(0);
        this.jasperDesign.setRightMargin(0);
        this.jasperDesign.setPageHeight(900);

        JRDesignBand detailBand = new JRDesignBand();
        detailBand.setHeight(40);
        // this.jasperDesign.setDetail(detailBand);
        ((JRDesignSection) jasperDesign.getDetailSection()).addBand(detailBand);

        JRDesignBand titleHeaderband = new JRDesignBand();
        titleHeaderband.setHeight(40);
        this.jasperDesign.setTitle(titleHeaderband);

    }
    
    /**
     * Accesseur de l attribut bytes from file.
     * 
     * @param file --
     * @return bytes from file
     * @throws IOException Signal qu'une execption de type I/O s'est produite.
     */
    public static byte[] getBytesFromFile(File file) throws IOException
    {
        try (InputStream iss = new FileInputStream(file))
        {
            // Get the size of the file
            long length = file.length();
    
            /*
             * if (length > Integer.MAX_VALUE) { // File is too large }
             */
    
            // Create the byte array to hold the data
            byte[] bytes = new byte[(int) length];
    
            // Read in the bytes
            int offset = 0;
            int numRead = 0;
    
            while (offset < bytes.length && (numRead = iss.read(bytes, offset, bytes.length - offset)) >= 0)
            {
                offset += numRead;
            }
    
            // Ensure all the bytes have been read in
            if (offset < bytes.length)
            {
                throw new IOException("Could not completely read file " + file.getName());
            }
    
            return bytes;
        }
    }
    
    /**
     * methode Ajouter une colonne : --.
     * 
     * @param nomDuChamp --
     * @param expressionZoneTexte --
     * @param classeExpressionZoneTexte --
     * @param classeDuChamp --
     * @param descriptionDuChamp --
     * @param nullIsBlank --
     */
    public void ajouterUneColonne(String nomDuChamp, String expressionZoneTexte, Class classeExpressionZoneTexte,
        Class classeDuChamp, String descriptionDuChamp, boolean nullIsBlank)
    {
        ajouterUneColonne(nomDuChamp, expressionZoneTexte, null, classeExpressionZoneTexte, classeDuChamp,
            descriptionDuChamp, true);
    }

    /**
     * methode Ajouter une colonne : --.
     * 
     * @param nomDuChamp --
     * @param expressionZoneTexte --
     * @param modeleFormatZoneTexte --
     * @param classeExpressionZoneTexte --
     * @param classeDuChamp --
     * @param descriptionColonne --
     * @param nullIsBlank --
     */
    public void ajouterUneColonne(String nomDuChamp, String expressionZoneTexte, String modeleFormatZoneTexte,
        Class classeExpressionZoneTexte, Class classeDuChamp, String descriptionColonne, boolean nullIsBlank)
    {

        if (inclureDescriptionChamp)
        {
            this.ajouterLaDescriptionDeLaColonne(descriptionColonne, nomDuChamp);
        }

        this.definirUnChampDansLeRapport(this.jasperDesign, nomDuChamp, classeDuChamp);

        this.ajouterUneZoneDeTexte(expressionZoneTexte, modeleFormatZoneTexte, classeExpressionZoneTexte);

    }

    /**
     * Ouvre une session hibernate.
     * 
     * @param parametresEdition --
     * @return the session
     */
    public Session associerHibernateConnection(Map parametresEdition)
    {
        SessionFactory hibernateSessionFactory = getLocalSessionFactory("hibernatesessionfactorysanscache");
        return hibernateSessionFactory.openSession();
    }

    /**
     * methode Associer jdbc data source : --.
     * 
     * @param parametresEdition --
     * @return data source
     */
    public DataSource associerJdbcDataSource(Map parametresEdition)
    {
        return getJdbcDataSource("datasource");
    }

    /**
     * Création de la collection contenant les données principales utilisées pour constituer l'édition. Les beans de
     * cette collection sont utilisés pour remplir les champs placès dans le section DETAIL du modèle jasperreports.
     * 
     * @param parametresEdition paramètres utilisés pour constituer les données de l'édition
     * @return collection contenant les données de l'édition
     */
    public abstract Collection creerDatasource(Map parametresEdition);

    /**
     * methode Creer hql : --.
     * 
     * @param parametresEdition --
     * @return string
     */
    public abstract String creerHql(Map parametresEdition);

    /**
     * Création du modèle de l'édition.
     * 
     * @param parametresEdition --
     */
    public abstract void creerJasperDesign(Map parametresEdition);

    /**
     * Paramétrage du nom de fichier à utiliser pour nommer l'édition Par défaut null. A surcharger si nécessaire. sinon
     * le nom du fichier est généré
     * 
     * @param parametresEdition --
     * @return le nom du fichier à utiliser pour stocker l'édition
     */
    public abstract String creerNomDuFichier(Map parametresEdition);

    /**
     * Exécute une édition japerreport.
     * 
     * @param parameters --
     * @param jasperDesign --
     * @param jrDataSource --
     * @return tableau de byte contenant l'édition produite
     */
    @SuppressWarnings("unchecked")
    public byte[] executeJasperreports(Map parameters, JasperDesign jasperDesign, JRDataSource jrDataSource)
    {
        byte[] result = null;

        try
        {
            JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);
            parameters.put(JRParameter.IS_IGNORE_PAGINATION, Boolean.TRUE);
            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrDataSource);

            result = exportReportToCsv(jasperPrint);
        }
        catch (JRException exception)
        {
            log.warn("ERREUR LORS DE LA PRODUCTION DE L'EDITION", exception);
            throw ApplicationExceptionTransformateur.transformer(exception);
        }

        return result;
    }

    /**
     * Exécute une édition japerreport.
     * 
     * @param parameters --
     * @param jasperDesign --
     * @param hibernateSession --
     * @param dataSource --
     * @param fileEditionOutputStream --
     */
    @SuppressWarnings("unchecked")
    public void executeJasperreportsWithConnection(Map parameters, JasperDesign jasperDesign, Session hibernateSession,
        DataSource dataSource, OutputStream fileEditionOutputStream)
    {
        Connection con = null;
        try
        {
            JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);
            parameters.put(JRHibernateQueryExecuterFactory.PROPERTY_HIBERNATE_QUERY_RUN_TYPE, "scroll");
            parameters.put(JRHibernateQueryExecuterFactory.PROPERTY_HIBERNATE_CLEAR_CACHE, Boolean.TRUE);
            parameters.put(JRHibernateQueryExecuterFactory.PARAMETER_HIBERNATE_SESSION, hibernateSession);
            parameters.put(JRParameter.REPORT_VIRTUALIZER, new JRFileVirtualizer(10));
            con = dataSource.getConnection();
            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, con);
            exportReportToCsvFromFileToFile(jasperPrint, fileEditionOutputStream);
        }
        catch (JRException exception)
        {
            log.warn("ERREUR LORS DE LA PRODUCTION DE L'EDITION", exception);
            throw ApplicationExceptionTransformateur.transformer(exception);
        }
        catch (SQLException exception)
        {
            log.warn("ERREUR LORS DE LA PRODUCTION DE L'EDITION", exception);
            throw ApplicationExceptionTransformateur.transformer(exception);
        }

        finally
        {
            try
            {
                if (con != null)
                {
                    con.close();
                }
            }
            catch (SQLException ex)
            {
                logger.warn("IMPOSSIBLE DE FERMER LA CONNEXION JDBC", ex);
            }
        }

    }

    /**
     * Produit l'édition passée en paramètres au format csv et la retourne sous la forme d'un tableau de bytes.
     * 
     * @param jasperPrint l'objet rapport à exporter
     * @return tableau de byte représentant l'édition au format csv
     * @throws JRException the JR exception
     * @see net.sf.jasperreports.engine.export.JRCsvExporter
     */
    public byte[] exportReportToCsv(JasperPrint jasperPrint) throws JRException
    {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        JRCsvExporter exporter = new JRCsvExporter();

        exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
        exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, baos);
        exporter.setParameter(JRExporterParameter.CHARACTER_ENCODING, this.characterEncoding);

        exporter.setParameter(JRCsvExporterParameter.FIELD_DELIMITER, this.fieldDelimiter);
        exporter.setParameter(JRCsvExporterParameter.RECORD_DELIMITER, this.recordDelimiter);

        exporter.exportReport();

        return baos.toByteArray();
    }

    /**
     * Produit l'édition passée en paramètres au format csv et la retourne sous la forme d'un tableau de bytes.
     * 
     * @param jasperPrint l'objet rapport à exporter
     * @param editionOutputStream --
     * @throws JRException the JR exception
     * @see net.sf.jasperreports.engine.export.JRCsvExporter
     */
    public void exportReportToCsvFromFileToFile(JasperPrint jasperPrint, OutputStream editionOutputStream)
        throws JRException
    {
        JRCsvExporter exporter = new JRCsvExporter();

        exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
        exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, editionOutputStream);
        exporter.setParameter(JRExporterParameter.CHARACTER_ENCODING, this.characterEncoding);

        exporter.setParameter(JRCsvExporterParameter.FIELD_DELIMITER, this.fieldDelimiter);
        exporter.setParameter(JRCsvExporterParameter.RECORD_DELIMITER, this.recordDelimiter);

        exporter.exportReport();

    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @return the encodage du fichier
     * @see fr.gouv.finances.lombok.edition.service.ServiceEditionCommun#getCharacterEncoding()
     */
    @Override
    public String getCharacterEncoding()
    {
        return characterEncoding;
    }

    /**
     * Accesseur de l attribut content type.
     * 
     * @return content type
     */
    public String getContentType()
    {
        return contentType;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @return description
     * @see fr.gouv.finances.lombok.edition.service.ServiceEditionCommun#getDescription()
     */
    @Override
    public String getDescription()
    {
        // Date datejour = new Date();
        // DateFormat unDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale.FRANCE);
        StringBuilder   desc = new StringBuilder();

        desc.append(this.nomEdition);
        // desc.append(" (");
        // desc.append(unDateFormat.format(datejour));
        // desc.append(')');

        return desc.toString();
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param parametresEdition
     * @return description
     * @see fr.gouv.finances.lombok.edition.service.ServiceEditionCommun#getDescription(java.util.Map)
     */
    @Override
    public String getDescription(Map parametresEdition)
    {
        return this.getDescription();
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @return the extension utilisée dans le nom du fichier produit
     * @see fr.gouv.finances.lombok.edition.service.ServiceEditionCommun#getExtension()
     */
    @Override
    public String getExtension()
    {
        return extension;
    }

    /**
     * Accesseur de l attribut field delimiter.
     * 
     * @return field delimiter
     */
    public String getFieldDelimiter()
    {
        return fieldDelimiter;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @return the nombre de jours de conservation de l'édition après sa production
     * @see fr.gouv.finances.lombok.edition.service.ServiceEditionCommun#getNombreJoursConservation()
     */
    @Override
    public Integer getNombreJoursConservation()
    {
        return nombreJoursConservation;
    }

    /**
     * Accesseur de l attribut nom edition.
     * 
     * @return nom edition
     */
    public String getNomEdition()
    {
        return nomEdition;
    }

    /**
     * Accesseur de l attribut record delimiter.
     * 
     * @return record delimiter
     */
    public String getRecordDelimiter()
    {
        return recordDelimiter;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @return the type Mime
     * @see fr.gouv.finances.lombok.edition.service.ServiceEditionCommun#getTypeMime()
     */
    @Override
    public String getTypeMime()
    {
        return typeMime;
    }

    /**
     * Verifie si modeconnecte.
     * 
     * @return true, si c'est modeconnecte
     */
    public boolean isModeconnecte()
    {
        return modeconnecte;
    }

    /**
     * Produit un fichier d'édition.
     * 
     * @param parametresEdition Map contenant les paramètres de l'édition
     * @return un objet de type FichierJoint contenant l'édition
     */
    @Override
    public FichierJoint produireFichierEdition(Map parametresEdition)
    {
        FichierJoint result = null;

        long start = traceDebutEdition();

        if (modeconnecte)
        {
            result = produitFichierEditionFromHql(parametresEdition);
            JRDesignQuery query = new JRDesignQuery();
            query.setLanguage("hql");
            query.setText(this.creerHql(parametresEdition));
            this.jasperDesign.setQuery(query);

            result = produitFichierEditionFromHql(parametresEdition);
        }
        else
        {
            // -----------------------------------------------------------------------
            // -- Production des données source de l'édition : méthode à
            // surcharger --
            // -----------------------------------------------------------------------
            Collection datasource = this.creerDatasource(parametresEdition);

            result = produitFichierEdition(parametresEdition, datasource);

            traceFinExecution(start);
        }

        return result;
    }

    /**
     * Produit une édition HTML à partir d'une collection de beans destinée à être vue "en ligne".
     * 
     * @param data --
     * @param parametresEdition --
     * @return the fichier joint
     */
    public FichierJoint produireFichierEditionFromCollection(Collection data, Map parametresEdition)
    {
        FichierJoint result = null;

        long start = traceDebutEdition();

        // -----------------------------------------------------------------------
        // -- Datasource
        // -----------------------------------------------------------------------
        Collection datasource = data;

        result = produitFichierEdition(parametresEdition, datasource);

        this.traceFinExecution(start);

        return result;
    }

    /**
     * methode Produire fichier edition hql : --.
     * 
     * @param parametresEdition --
     * @return fichier joint
     */
    public FichierJoint produireFichierEditionHql(Map parametresEdition)
    {
        // JRHibernateQueryExecuterFactory.PARAMETER_HIBERNATE_SESSION, session

        return null;
    }

    /**
     * Produit une édition à partir d'une collection de beans.
     * 
     * @param data --
     * @param parametresEdition --
     * @return the map
     */
    public Map produitEditionHtmlFromCollection(Collection data, Map parametresEdition)
    {
        return null;
    }

    /**
     * Modificateur de l attribut character encoding.
     * 
     * @param characterEncoding le nouveau character encoding
     */
    public void setCharacterEncoding(String characterEncoding)
    {
        this.characterEncoding = characterEncoding;
    }

    /**
     * Modificateur de l attribut content type.
     * 
     * @param contentType le nouveau content type
     */
    public void setContentType(String contentType)
    {
        this.contentType = contentType;
    }

    /**
     * Modificateur de l attribut extension.
     * 
     * @param fileExtension le nouveau extension
     */
    public void setExtension(String fileExtension)
    {
        this.extension = fileExtension;
    }

    /**
     * Modificateur de l attribut field delimiter.
     * 
     * @param fieldDelimiter le nouveau field delimiter
     */
    public void setFieldDelimiter(String fieldDelimiter)
    {
        this.fieldDelimiter = fieldDelimiter;
    }

    /**
     * Modificateur de l attribut modeconnecte.
     * 
     * @param modeconnecte le nouveau modeconnecte
     */
    public void setModeconnecte(boolean modeconnecte)
    {
        this.modeconnecte = modeconnecte;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @param nombreJoursConservation the new nombre de jours de conservation de l'édition après sa production
     * @see fr.gouv.finances.lombok.edition.service.ServiceEditionCommun#setNombreJoursConservation(java.lang.Integer)
     */
    @Override
    public void setNombreJoursConservation(Integer nombreJoursConservation)
    {
        this.nombreJoursConservation = nombreJoursConservation;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @param nomEdition the new nom de l'édition
     * @see fr.gouv.finances.lombok.edition.service.ServiceEditionCommun#setNomEdition(java.lang.String)
     */
    @Override
    public void setNomEdition(String nomEdition)
    {
        this.nomEdition = nomEdition;
    }

    /**
     * Modificateur de l attribut record delimiter.
     * 
     * @param recordDelimiter le nouveau record delimiter
     */
    public void setRecordDelimiter(String recordDelimiter)
    {
        this.recordDelimiter = recordDelimiter;
    }

    /**
     * Modificateur de l attribut type edition par defaut.
     * 
     * @param typeEditionParDefaut le nouveau type edition par defaut
     */
    public void setTypeEditionParDefaut(String typeEditionParDefaut)
    {
        // RAS
    }

    /**
     * Modificateur de l attribut type mime.
     * 
     * @param mimeType le nouveau type mime
     */
    public void setTypeMime(String mimeType)
    {
        this.typeMime = mimeType;
    }

    /**
     * methode Ajouter la description de la colonne : --.
     * 
     * @param descriptionColonne --
     * @param nomDuChamp --
     */
    private void ajouterLaDescriptionDeLaColonne(String descriptionColonne, String nomDuChamp)
    {
        String descriptColonne = descriptionColonne;
        if (descriptionColonne == null || StringUtils.isBlank(descriptionColonne))
        {
            descriptColonne = nomDuChamp;
        }
        this.columnsNames.add(descriptColonne);

        JRDesignStaticText textStatic = new JRDesignStaticText();
        textStatic.setHeight(40);
        textStatic.setWidth(10000);
        textStatic.setX(this.columnsNames.size() - 1);
        textStatic.setY(0);
        textStatic.setText(descriptColonne);

        ((JRDesignBand) this.jasperDesign.getTitle()).addElement(textStatic);
    }

    /**
     * methode Ajouter une zone de texte : --.
     * 
     * @param expressionZoneTexte --
     * @param modeleFormatZoneTexte --
     * @param classeExpressionZoneTexte --
     */
    private void ajouterUneZoneDeTexte(String expressionZoneTexte, String modeleFormatZoneTexte,
        Class classeExpressionZoneTexte)
    {
        JRDesignTextField textField = new JRDesignTextField();

        this.columns.add(textField);

        textField.setX(this.columns.size() - 1);
        textField.setY(0);
        textField.setHeight(20);
        textField.setWidth(10000);

        if (modeleFormatZoneTexte != null && StringUtils.isNotBlank(modeleFormatZoneTexte))
        {
            textField.setPattern(modeleFormatZoneTexte);
        }
        JRDesignExpression expre = new JRDesignExpression();
        expre.setValueClass(classeExpressionZoneTexte);
        expre.setText(expressionZoneTexte);
        textField.setExpression(expre);

        ((JRDesignBand) this.jasperDesign.getDetailSection()).addElement(textField);
    }

    /**
     * Création de la source de données Jasper à partir d'une collection de bean.
     * 
     * @param beanCollection --
     * @return the JR data source
     */
    private JRDataSource creerJRdataSource(Collection beanCollection)
    {
        return new JRBeanCollectionDataSource(beanCollection);
    }

    /**
     * methode Definir un champ dans le rapport : --.
     * 
     * @param jasperDesign --
     * @param nomDuChamp --
     * @param classeDuChamp --
     */
    private void definirUnChampDansLeRapport(JasperDesign jasperDesign, String nomDuChamp, Class classeDuChamp)
    {
        JRDesignField field = new JRDesignField();

        field.setName(nomDuChamp);
        field.setValueClass(classeDuChamp);

        Map reportFields = jasperDesign.getFieldsMap();
        if (reportFields == null || (!reportFields.containsKey(field.getName())))
        {
            try
            {
                jasperDesign.addField(field);
            }
            catch (JRException exception)
            {
                ApplicationExceptionTransformateur.transformer(exception);
            }
        }
    }

    /**
     * Place le tableau de byte représentant l'édition dans un objet de type FichierJoint et affecte le type MIME du
     * fichier ainsi que son nom.
     * 
     * @param parametresEdition --
     * @param baos le tableau de byte constituant l'édition
     * @return objet de type FichierJoint représentant une édition
     */
    private FichierJoint getFichierJoint(Map parametresEdition, byte[] baos)
    {
        FichierJoint result;

        result = new FichierJoint(baos);
        result.setTypeMimeFichier(this.typeMime);
        result.setNomFichierOriginal(getNomFichier(parametresEdition));
        return result;
    }

    /**
     * Accesseur de l attribut jdbc data source.
     * 
     * @param datasourceID --
     * @return jdbc data source
     */
    private DataSource getJdbcDataSource(String datasourceID)
    {
        DataSource dataSource = null;

        if (getApplicationContext() == null)
        {
            throw new ProgrammationException("LE CONTEXTE DE L'APPLICATION EST NULL");
        }
        else
        {
            if (getApplicationContext().containsBean(datasourceID))
            {
                try
                {
                    dataSource = (DataSource) getApplicationContext().getBean(datasourceID);
                    // , DataSource.class);
                }
                catch (NoSuchBeanDefinitionException exception)
                {
                    throw ApplicationExceptionTransformateur.transformer("LE BEAN " + datasourceID
                        + " N'EST PAS DE TYPE DataSource", exception);
                }
            }
            else
            {
                throw new ProgrammationException("LE BEAN " + datasourceID
                    + " N'EST PAS DEFINI DANS LE CONTEXTE D'APPLICATION.");
            }
        }
        return dataSource;

    }

    /**
     * Lit dans le contexte la référence de la fabrique de sessions Hibernate.
     * 
     * @param factoryId --
     * @return ServiceEditionCommun
     */
    private SessionFactory getLocalSessionFactory(String factoryId)
    {
        SessionFactory localSessionFactory = null;

        if (getApplicationContext() == null)
        {
            throw new ProgrammationException("LE CONTEXTE DE L'APPLICATION EST NULL");
        }
        else
        {
            if (getApplicationContext().containsBean(factoryId))
            {
                try
                {
                    localSessionFactory =
                        (SessionFactory) getApplicationContext().getBean(factoryId, SessionFactory.class);
                    // , LocalSessionFactoryBean.class);
                }
                catch (NoSuchBeanDefinitionException exception)
                {
                    throw ApplicationExceptionTransformateur.transformer("LE BEAN " + factoryId
                        + " N'EST PAS DE TYPE LocalSessionFactoryBean", exception);
                }
            }
            else
            {
                throw new ProgrammationException("LE BEAN " + factoryId
                    + " N'EST PAS DEFINI DANS LE CONTEXTE D'APPLICATION.");
            }
        }
        return localSessionFactory;
    }

    /**
     * Récupère dans la Map model le nom à utiliser pour nommer le fichier contenant l'édition et lui affecte
     * éventuellement une extension. Si le nom du fichier n'est pas transmis dans les paramètres, génère un nom de
     * fichier.
     * 
     * @param parametresEdition Map contenant les paramètres de l'édition
     * @return Nom du fichier à utiliser pour stocker une édition
     */
    private String getNomFichier(Map parametresEdition)
    {
        StringBuilder   nomFichier = new StringBuilder();

        String nomFichierCree = this.creerNomDuFichier(parametresEdition);
        // TT du nom du fichier
        if (nomFichierCree != null && StringUtils.isNotBlank(nomFichierCree))
        {
            nomFichier.append(nomFichierCree);
        }
        else
        {
            nomFichier.append(NOM_FICHIER_PAR_DEFAUT);
            nomFichier.append((new Date()).getTime());
        }

        // TT de l'extension du fichier
        if (nomFichier != null && nomFichier.lastIndexOf(".") == -1)
        {
            nomFichier.append('.');
            nomFichier.append(this.extension);
        }
        return nomFichier.toString();
    }

    /**
     * Produit l'édition et la retourne sous forme d'objet FichierJoint.
     * 
     * @param parametresEdition --
     * @param datasource --
     * @return the fichier joint
     */
    private FichierJoint produitFichierEdition(Map parametresEdition, Collection datasource)
    {
        FichierJoint result;

        // -----------------------------------------------------------------------
        // -- Création le datasource de l'édition
        // -----------------------------------------------------------------------
        JRDataSource jrDataSource = this.creerJRdataSource(datasource);

        // -----------------------------------------------------------------------
        // -- Création le modèle de l'édition
        // -----------------------------------------------------------------------
        this.creerJasperDesign(parametresEdition);

        // -----------------------------------------------------------------------
        // -- Exécution de jasperreport --
        // -----------------------------------------------------------------------
        byte[] baos = this.executeJasperreports(parametresEdition, this.jasperDesign, jrDataSource);

        // -----------------------------------------------------------------------
        // -- Stockage de l'édition dans un objet de type FichierJoint
        // -----------------------------------------------------------------------

        result = getFichierJoint(parametresEdition, baos);

        return result;
    }

    /**
     * Produit l'édition et la retourne sous forme d'objet FichierJoint.
     * 
     * @param parametresEdition --
     * @return the fichier joint
     */
    private FichierJoint produitFichierEditionFromHql(Map parametresEdition)
    {
        FichierJoint result = null;

        // -----------------------------------------------------------------------
        // -- Ouverture de la session Hibernate
        // -----------------------------------------------------------------------
        Session hibernateSession = this.associerHibernateConnection(parametresEdition);

        DataSource dataSource = this.associerJdbcDataSource(parametresEdition);

        // -----------------------------------------------------------------------
        // -- Création le modèle de l'édition
        // -----------------------------------------------------------------------
        this.creerJasperDesign(parametresEdition);

        // -----------------------------------------------------------------------
        // Création du fichier temporaire de stockage de l'édition
        // -----------------------------------------------------------------------
        try
        {
            File tempFileEdition = File.createTempFile("pattern", ".suffix");
            try (OutputStream fileEditionOutputStream = new FileOutputStream(tempFileEdition))
            {
                try (BufferedOutputStream bufFileEditionOutputStream = new BufferedOutputStream(fileEditionOutputStream))
                {
                    // -----------------------------------------------------------------------
                    // -- Exécution de jasperreport --
                    // -----------------------------------------------------------------------
                    this.executeJasperreportsWithConnection(parametresEdition, this.jasperDesign, hibernateSession, dataSource,
                        bufFileEditionOutputStream);
        
                    // -----------------------------------------------------------------------
                    // -- Stockage de l'édition dans un objet de type FichierJoint
                    // -----------------------------------------------------------------------
        
                    byte[] baos = getBytesFromFile(tempFileEdition);
                    result = getFichierJoint(parametresEdition, baos);
                }
            }
        }
        catch (IOException exception)
        {
            log.warn("ERREUR", exception);
            throw new ProgrammationException(exception);
        }

        return result;
    }

    /**
     * Trace le début d'une édition.
     * 
     * @return le moment où la produFileOutputStreamction de l'édition a commencé
     */
    private long traceDebutEdition()
    {
        long start = 0;

        if (log.isTraceEnabled())
        {
            // ----------------------
            // -- Nom de l'édition --
            // ----------------------

            StringBuilder unMsg = new StringBuilder("NOM DE L'EDITION : ");
            unMsg.append(this.nomEdition);

            // -----------------------------------------------------------------------
            // -- Date du lancement de l'édition --
            // -----------------------------------------------------------------------
            Calendar cal = Calendar.getInstance();

            unMsg = new StringBuilder("DEBUT DU TRAITEMENT DE L'EDITION : ");
            unMsg.append(cal.getTime().toString());
            start = cal.getTimeInMillis();
            log.trace(unMsg.toString());
        }
        return start;
    }

    /**
     * Trace la fin de l'exécution.
     * 
     * @param start --
     */
    private void traceFinExecution(long start)
    {
        if (log.isTraceEnabled())
        {
            StringBuilder   unMsg;
            Date uneDateCourante;

            // --------------------------------------
            // -- Date de fin d'exécution de l'édition --
            // --------------------------------------
            unMsg = new StringBuilder();
            unMsg.append("FIN DU TRAITEMENT DE  L'EDITION : ");
            uneDateCourante = Calendar.getInstance().getTime();
            unMsg.append(uneDateCourante.toString());
            log.trace(unMsg.toString());

            // ------------------------------------------------
            // -- Duree de l'exécution de l'édition --
            // -----------------------------------
            unMsg = new StringBuilder();
            unMsg.append("DUREE D'EXECUTION DE L'EDITION : ");
            long elapsedTime = Calendar.getInstance().getTimeInMillis() - start;

            unMsg.append(elapsedTime);
            unMsg.append(" MILLI SECONDES");
        }
    }

}
