/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 */
package fr.gouv.finances.lombok.edition.dao;

import java.util.List;

import fr.gouv.finances.lombok.edition.techbean.StatJobHistory;
import fr.gouv.finances.lombok.util.base.CoreBaseDao;

/**
 * Interface du DAO de gestion des données liées aux statistiques des travaux d'édition.
 *
 * @author amleplatinec
 * @author Christophe Breheret-Girardin
 */
public interface StatistiqueJobHistoryDao extends CoreBaseDao
{

    /**
     * Méthode permettant de récupérer les statistiques sur les historiques d'édition, par date de purge.
     *
     * @param applicationOrigine application sur laquelle se porte les statistiques demandée
     * @return la liste des statistiques correspondant, triée sur la date de purge
     */
    public List<StatJobHistory> compteJobHistoryParDatePurge(final String applicationOrigine);

    /**
     * Méthode permettant de récupérer les statistiques sur les historiques d'édition, par date de demande
     * d'édition.
     *
     * @param applicationOrigine application sur laquelle se porte les statistiques demandée
     * @return la liste des statistiques correspondant, triée sur la date de demande d'édition
     */
    public List<StatJobHistory> compteJobHistoryParJourDemande(final String applicationOrigine);

    /**
     * Méthode permettant de récupérer les statistiques sur les historiques d'édition, par statut
     * d'édition.
     *
     * @param applicationOrigine application sur laquelle se porte les statistiques demandée
     * @return la liste des statistiques correspondant
     */
    public List<StatJobHistory> compteJobHistoryParStatus(String applicationOrigine);

    /**
     * Méthode permettant de récupérer les statistiques sur les historiques d'édition, par type et statut
     * d'édition.
     *
     * @param applicationOrigine application sur laquelle se porte les statistiques demandée
     * @return la liste des statistiques correspondant
     */
    public List<StatJobHistory> compteJobHistoryParTypeEtStatus(final String applicationOrigine);

    /**
     * Méthode permettant de récupérer les statistiques sur les historiques d'édition sur les tailles
     * d'édition.
     *
     * @param applicationOrigine application sur laquelle se porte les statistiques demandée
     * @return la liste des statistiques correspondant
     */
    public List<StatJobHistory> compteTailleEditionParType(final String applicationOrigine);
}
