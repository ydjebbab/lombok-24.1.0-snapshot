/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 */
package fr.gouv.finances.lombok.edition.dao;

/**
 * Propriétés liées aux traces de purge d'édition.
 * @see fr.gouv.finances.lombok.edition.bean.TrcPurgeEdition
 *
 * @author Christophe Breheret-Girardin
 */
public enum ProprieteTrcPurgeEdition
{
    /** Application à laquelle est rattachée l'édition. */
    APPLICATION_ORIGINE("applicationOrigine"),

    /** Date de début de la purge d'une édition. */
    DATE_DEBUT_PURGE("dateDebutPurge"),

    /** Statut de la purge d'une édition. */
    ETAT_PURGE("etatPurge");

    /** Nom de la propriété */
    private String nom;

    /**
     * Constructeur.
     *
     * @param libelleNom libellé du nom de la propriété
     */
    private ProprieteTrcPurgeEdition(String libelleNom)
    {
        nom = libelleNom;
    }

    /**
     * Accesseur de nom
     *
     * @return nom de la propriété
     */
    public String getNom()
    {
        return nom;
    }
    
}
