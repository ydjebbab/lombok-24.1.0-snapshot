/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 */
package fr.gouv.finances.lombok.edition.dao;

import java.io.File;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import fr.gouv.finances.lombok.edition.bean.ContenuEdition;
import fr.gouv.finances.lombok.edition.bean.FiltreEdition;
import fr.gouv.finances.lombok.edition.bean.JobHistory;
import fr.gouv.finances.lombok.edition.bean.ProfilDesti;
import fr.gouv.finances.lombok.edition.bean.StockageEdition;
import fr.gouv.finances.lombok.util.base.CoreBaseDao;

/**
 * Interface du DAO d'historique des travaux d'édition.
 *
 * @author amleplatinec
 * @author Christophe Breheret-Girardin
 */
public interface JobHistoryDao extends CoreBaseDao
{
    /**
     * Méthode permettant de charger le contenu de l'édition, avec initialisation en cas d'absence.
     *
     * @param jobHistory historique des travaux d'édition dont il faut charger le contenu de l'édition
     */
    default void loadSafeData(JobHistory jobHistory)
    {
        // Récupération des informations de stockage
        StockageEdition stockageEdition = jobHistory.getStockageEdition();
        // En cas d'absence, initialisation de celles-ci
        if (jobHistory.getStockageEdition() == null)
        {
            stockageEdition = new StockageEdition();
            jobHistory.setStockageEdition(stockageEdition);
        }

        // Récupération du contenu de l'édition
        ContenuEdition contenuEdition = jobHistory.getStockageEdition().getLeContenuEdition();
        // En cas d'absence, initialisation de celui-ci
        if (contenuEdition == null)
        {
            contenuEdition = new ContenuEdition();
            stockageEdition.setLeContenuEdition(contenuEdition);
        }
    }

    /**
     * Méthode permettant de charge le contenu d'une édition dans un tableau d'octet.
     *
     * @param jobHistory historique des travaux d'édition dont il faut charger le contenu
     */
    JobHistory chargerContenuEditionToBytes(JobHistory jobHistory);

    /**
     * Méthode permettant de charge le contenu d'une édition dans un fichier temporaire.
     *
     * @param jobHistory historique des travaux d'édition dont il faut charger le contenu
     */
    void chargerContenuEditionToFile(JobHistory jobHistory);

    /**
     * Méthode permettant de supprimer un historique de travaux d'édition.
     *
     * @param jobHistory historique des travaux d'édition à supprimer
     */
    void deleteJobHistory(JobHistory jobHistory);

    /**
     * Méthode permettant de supprimer des historiques de travaux d'édition.
     *
     * @param lesEditionsSupprimees les historiques des travaux d'édition à supprimer
     */
    void deleteJobsHistories(List<JobHistory> lesEditionsSupprimees);

    /**
     * Méthode permettant de rechercher les identifiants des éditions.
     *
     * @param applicationOrigine application sur laquelle porte les éditions recherchées
     * @return les identifiants d'édition satisfaisant les critères
     */
    List<String> findBeanEditionIds(String applicationOrigine);

    /**
     * Méthode permettant de rechercher les historiques de travaux d'édition dont les éditions
     * ont les temps d'exécution les plus longs.
     *
     * @param applicationOrigine application sur laquelle porte les éditions recherchées
     * @param nbeARetourner nombre d'édition à récupérer
     * @return les historiques de travaux d'édition, triés par temps d'exécution décroissant
     */
    List<JobHistory> findEditionsLesPlusLongues(String applicationOrigine, int nbeARetourner);

    /**
     * Méthode permettant de rechercher un historique de travaux d'édition.
     *
     * @param editionUuid UUID de l'édition
     * @return l'historique de travaux d'édition satisfaisant les critères
     */
    JobHistory findJobHistory(String editionUuid);

    /**
     * Fonctionnalité non implémentée.
     *
     * @param profil profil lié aux éditions recherchées
     * @param structureAffectation structure d'affectation lié aux éditions recherchées
     * @param applicationOrigine application sur laquelle porte les éditions recherchées
     * @return les historiques de travaux d'édition satisfaisant les critères
     */
    List<JobHistory> findJobHistoryParProfilStructureAffectationAppli(String profil,
        String structureAffectation, String applicationOrigine);

    /**
     * Méthode permettant de rechercher un historique de travaux d'édition portant
     * sur la dernière édition produite.
     *
     * @param beanEditionId identifiant de l'édition recherchée
     * @param appliOrigine application sur laquelle porte l'édition recherchée
     * @return l'historique de travaux d'édition satisfaisant les critères
     */
    JobHistory findLaDerniereEditionParBeanIdEtParAppliOrigine(String beanEditionId, String appliOrigine);

    /**
     * Méthode permettant de rechercher un historique de travaux d'édition portant
     * sur la dernière édition produite.
     *
     * @param beanEditionId identifiant de l'édition recherchée
     * @param appliOrigine application sur laquelle porte l'édition recherchée
     * @param utilisateur propriétaire de l'édition recherchée
     * @return l'historique de travaux d'édition satisfaisant les critères
     */
    JobHistory findLaDerniereEditionParBeanIdEtParAppliOrigineEtParUtilisateur(String beanEditionId,
        String appliOrigine, String utilisateur);

    /**
     * Méthode permettant de rechercher les historiques des nouveaux travaux d'édition.
     *
     * @param uid identifiant de l'utilisateur n'ayant pas consulté les éditions recherchées
     * @param mapFiltresEdition filtre à appliquer à la recherche
     * @param applicationOrigine application sur laquelle porte les éditions recherchées
     * @return les historiques des nouveaux travaux d'édition satisfaisant les critères
     */
    List<JobHistory> findNouvellesEditionsParProfilFiltreAppli(String uid
        , Map<String, Set<FiltreEdition>> mapFiltresEdition, String applicationOrigine);

    /**
     * Méthode permettant de rechercher les historiques des nouveaux travaux d'édition.
     *
     * @param uidProprietaire propriétaire de l'édition
     * @param applicationOrigine application sur laquelle porte les éditions recherchées
     * @return les historiques des nouveaux travaux d'édition satisfaisant les critères
     */
    List<JobHistory> findNouvellesEditionsParUidEtAppli(String uidProprietaire, String applicationOrigine);

    /**
     * Méthode permettant de rechercher les profils destinataires des éditions produites.
     *
     * @param applicationOrigine application sur laquelle porte les profils recherchés
     * @return l'historique de travaux d'édition satisfaisant les critères
     */
    List<ProfilDesti> findProfils(String applicationOrigine);

    /**
     * Méthode permettant de rechercher les historiques des travaux d'édition.
     *
     * @param beanEditionId identifiant des éditions recherchées
     * @param applicationOrigine application sur laquelle porte les éditions recherchées
     * @return les historiques de travaux d'édition satisfaisant les critères
     */
    List<JobHistory> findToutesEditionsParBeanEditionId(String beanEditionId, String applicationOrigine);

    /**
     * Méthode permettant de rechercher les historiques des travaux d'édition.
     *
     * @param dateDemande restriction sur la date de demande de l'édition
     * @param applicationOrigine application sur laquelle porte les éditions recherchées
     * @return les historiques de travaux d'édition satisfaisant les critères
     */
    List<JobHistory> findToutesEditionsParDateDemande(Date dateDemande, String applicationOrigine);

    /**
     * Méthode permettant de rechercher les historiques des travaux d'édition.
     *
     * @param etat statut des éditions recherchées
     * @param applicationOrigine application sur laquelle porte les éditions recherchées
     * @return les historiques de travaux d'édition satisfaisant les critères
     */
    List<JobHistory> findToutesEditionsParEtat(String etat, String applicationOrigine);

    /**
     * Méthode permettant de rechercher les historiques des travaux d'édition.
     *
     * @param debutPeriodeDemande date de début de période de demande
     * @param finPeriodeDemande date de fin de période de demande
     * @param applicationOrigine application sur laquelle porte les éditions recherchées
     * @return les historiques de travaux d'édition satisfaisant les critères
     */
    List<JobHistory> findToutesEditionsParPeriodeDateDemande(Date debutPeriodeDemande, Date finPeriodeDemande,
        String applicationOrigine);

    /**
     * Méthode permettant de rechercher les historiques des travaux d'édition.
     *
     * @param mapFiltresEdition filtre à appliquer à la recherche
     * @param applicationOrigine application sur laquelle porte les éditions recherchées
     * @return les historiques de travaux d'édition satisfaisant les critères
     */
    List<JobHistory> findToutesEditionsParProfilFiltreAppli(Map<String, Set<FiltreEdition>> mapFiltresEdition,
        String applicationOrigine);

    /**
     * Méthode permettant de rechercher les historiques des travaux d'édition.
     *
     * @param uidProprietaire propriétaire de l'édition
     * @param applicationOrigine application sur laquelle porte les éditions recherchées
     * @return les historiques de travaux d'édition satisfaisant les critères
     */
    List<JobHistory> findToutesEditionsParUidEtAppli(String uidProprietaire, String applicationOrigine);

    /**
     * Méthode permettant de savoir si un historique de travaux d'édition est valide, c'est-à-dire
     * si l'édition associée existe pour l'UUID présent dans la propriété "editionUuid".
     *
     * @param jobHistory historique des travaux d'édition à contrôler
     * @return true si l'édition est valide, false sinon
     */
    boolean isValidEdition(JobHistory jobHistory);

    /**
     * Méthode permettant d'enregistrer le contenu d'une édition, sous la forme d'un tableau d'octet,
     * ainsi que des éventuelles modifications de l'historique des travaux d'édition.
     *
     * @param jobHistory historique des travaux d'édition dont il faut enregistrer le contenu
     * @param dataBytes contenu de l'édition
     */
    void saveContenuEditionFromByteArray(JobHistory jobHistory, byte[] dataBytes);

    /**
     * Méthode permettant d'enregistrer le contenu d'une édition, sous la forme d'un fichier temporaire,
     * ainsi que des éventuelles modifications de l'historique des travaux d'édition.
     *
     * @param jobHistory historique des travaux d'édition dont il faut enregistrer le contenu
     * @param fichierTemporaire contenu de l'édition
     */
    void saveContenuEditionFromFile(JobHistory jobHistory, File fichierTemporaire);

    /**
     * Méthode permettant d'enregistrer un historique de travaux d'édition.
     *
     * @param jobHistory historique des travaux d'édition
     */
    void saveJobHistory(JobHistory jobHistory);

    /**
     * Méthode permettant de récupérer un historique de travaux d'édition.
     *
     * @param editionUuid UUID l'édition
     * @return l'historique de travaux d'édition satisfaisant les critères
     */
    JobHistory findAndLoadJobHistory(String editionUuid);

    /**
     * Méthode permettant de charger le contenu d'un historique de travaux d'édition
     *
     * @param jobHistory historique de travaux d'édition dont il faut charger le contenu
     * @return l'historique chargé avec son contenu
     */
    JobHistory chargerContenuEdition(JobHistory jobHistory);
}
