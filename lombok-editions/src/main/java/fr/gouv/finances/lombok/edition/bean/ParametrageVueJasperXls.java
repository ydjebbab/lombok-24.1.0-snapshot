/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 */
package fr.gouv.finances.lombok.edition.bean;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Paramétrage Jasper de la vue Excel.
 * 
 * @author amleplatinec
 * @author Christophe Breheret-Girardin
 */
public class ParametrageVueJasperXls extends AbstractParametrageVueJasperJExcel
{
    /** Initialisation de la journalisation. */
    protected static final Logger LOGGER = LoggerFactory.getLogger(ParametrageVueJasperXls.class);

    /** Initialisation de l'UID. */
    private static final long serialVersionUID = 1L;

    /**
     * Construit un objet ParametrageVueJasperXls et lui affecte les paramètres par défaut.
     */
    public ParametrageVueJasperXls()
    {
        super();

        this.formatEdition = DEF_FORMAT_EDITION_VALUE;
        this.characterEncoding = DEF_CHARACTER_ENCODING;
        this.extension = DEF_EXTENSION;
        this.contentType = DEF_CONTENT_TYPE;
        this.typeMimeEdition = DEF_MIME_TYPE;

        // Spécifique xls
        this.onePagePerSheet = DEF_ONE_PAGE_PER_SHEET;
        this.detectCellType = DEF_DETECT_CELL_TYPE;
        this.fontSizeFixEnabled = DEF_FONT_SIZE_FIX_ENABLED;
        this.removeEmptySpaceBetweenRows = DEF_REM_EMPTY_SPACE_BET_ROWS;
        this.whitePageBackground = DEF_WHITE_PAGE_BACKGROUND;
    }

    /**
     * Produit une Map contenant le paramétrage de l'exporteur Jasper.
     *
     * @return le paramétrage de l'exporteur Jasper.
     */
    @Override
    public Map<String, Object> parametrageJRExporter()
    {
        // Déclaration d'une Map qui contiendra le paramétrage
        Map<String, Object> mapModele = new HashMap<>();

        // Paramétrage général de l'exporteur Jasper
        putIfNotNull(mapModele, CHARACTER_ENCODING, this.characterEncoding);

        // Paramétrage spécifique XLS
        putIfNotNull(mapModele, IS_ONE_PAGE_PER_SHEET, this.onePagePerSheet);
        putIfNotNull(mapModele, IS_DETECT_CELL_TYPE, this.detectCellType);
        putIfNotNull(mapModele, IS_FONT_SIZE_FIX_ENABLED, this.fontSizeFixEnabled);
        putIfNotNull(mapModele, IS_REM_EMPTY_SPACE_BET_ROWS, this.removeEmptySpaceBetweenRows);
        putIfNotNull(mapModele, IS_WHITE_PAGE_BACKGROUND, this.whitePageBackground);

        // Journalisation du paramétrage
        journaliserParametrageJRExporter(mapModele);

        return mapModele;
    }

    /**
     * Produit une Map contenant le paramétrage de la vue jasper en utilisant : - soit les valeurs par défaut, - soit
     * les valeurs contenues dans le propriétés de l'objet ParametrageVueJasperXls (valeurs injectée dans le fichier de
     * configuration des éditions).
     * 
     * @return the map
     */
    @Override
    public Map<String, String> parametrerVue()
    {
        // Déclaration d'une Map qui contiendra le paramétrage
        Map<String, String> mapModele = new HashMap<>();

        // Initialisation du paramétrage
        putIfNotNull(mapModele, REPORT_VIEW_KEY, this.formatEdition);
        putIfNotNull(mapModele, EXTENSION_KEY, this.extension);
        putIfNotNull(mapModele, CONTENT_TYPE_KEY, this.contentType);
        putIfNotNull(mapModele, MIME_TYPE_KEY, this.typeMimeEdition);

        // Journalisation du paramétrage
        journaliserParametrageVue(mapModele);

        return mapModele;
    }
}
