/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : PresentationEdition.java
 *
 */
package fr.gouv.finances.lombok.edition.bean;

import fr.gouv.finances.lombok.util.base.BaseBean;

/**
 * Class PresentationEdition --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.2 $ Date: 11 déc. 2009
 */
public class PresentationEdition extends BaseBean
{

    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** Url permettant d'accéder à l'édition produite. */
    private String urlEdition;

    /** Url permettant d'accéder à l'édition via le portail intranet. */
    private String urlPIntranet;

    /** Url permettant d'accéder à l'édition via le portail internet. */
    private String urlPInternet;

    /** Description du job, utilisable dans l'interface présentée à l'utilisateur pour lui indiquer l'état de son job. */
    private String description;

    /** Nom du fichier pour présentation à l'utilisateur lors du téléchargement. */
    private String nomFicDownload;

    /** Type MIME de l'édition produite. */
    private String typeMime;

    /** Encodage de l'édition. */
    private String characterEncoding;

    /** Extension utilisée pour présenter le fichier. */
    private String extension;

    /**
     * Instanciation de presentation edition.
     */
    public PresentationEdition()
    {
        super();
    }

    /**
     * Gets the character encoding.
     * 
     * @return the characterEncoding
     */
    public String getCharacterEncoding()
    {
        return characterEncoding;
    }

    /**
     * Gets the description.
     * 
     * @return the description
     */
    public String getDescription()
    {
        return description;
    }

    /**
     * Gets the extension.
     * 
     * @return the extension
     */
    public String getExtension()
    {
        return extension;
    }

    /**
     * Gets the nom fic download.
     * 
     * @return the nomFicDownload
     */
    public String getNomFicDownload()
    {
        return nomFicDownload;
    }

    /**
     * Gets the type mime.
     * 
     * @return the typeMime
     */
    public String getTypeMime()
    {
        return typeMime;
    }

    /**
     * Gets the url edition.
     * 
     * @return the urlEdition
     */
    public String getUrlEdition()
    {
        return urlEdition;
    }

    /**
     * Gets the url p internet.
     * 
     * @return the urlPInternet
     */
    public String getUrlPInternet()
    {
        return urlPInternet;
    }

    /**
     * Gets the url p intranet.
     * 
     * @return the urlPIntranet
     */
    public String getUrlPIntranet()
    {
        return urlPIntranet;
    }

    /**
     * Sets the character encoding.
     * 
     * @param characterEncoding the characterEncoding to set
     */
    public void setCharacterEncoding(String characterEncoding)
    {
        this.characterEncoding = characterEncoding;
    }

    /**
     * Sets the description.
     * 
     * @param description the description to set
     */
    public void setDescription(String description)
    {
        this.description = description;
    }

    /**
     * Sets the extension.
     * 
     * @param extension the extension to set
     */
    public void setExtension(String extension)
    {
        this.extension = extension;
    }

    /**
     * Sets the nom fic download.
     * 
     * @param nomFicDownload the nomFicDownload to set
     */
    public void setNomFicDownload(String nomFicDownload)
    {
        this.nomFicDownload = nomFicDownload;
    }

    /**
     * Sets the type mime.
     * 
     * @param typeMime the typeMime to set
     */
    public void setTypeMime(String typeMime)
    {
        this.typeMime = typeMime;
    }

    /**
     * Sets the url edition.
     * 
     * @param urlEdition the urlEdition to set
     */
    public void setUrlEdition(String urlEdition)
    {
        this.urlEdition = urlEdition;
    }

    /**
     * Sets the url p internet.
     * 
     * @param urlPInternet the urlPInternet to set
     */
    public void setUrlPInternet(String urlPInternet)
    {
        this.urlPInternet = urlPInternet;
    }

    /**
     * Sets the url p intranet.
     * 
     * @param urlPIntranet the urlPIntranet to set
     */
    public void setUrlPIntranet(String urlPIntranet)
    {
        this.urlPIntranet = urlPIntranet;
    }

}
