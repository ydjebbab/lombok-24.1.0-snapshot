/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : EditionSynchroService.java
 *
 */
package fr.gouv.finances.lombok.edition.service;

import java.util.Date;

import fr.gouv.finances.lombok.edition.bean.TrcPurgeEdition;

/**
 * Interface EditionSynchroService 
 * 
 * @author amleplatinec
 * @version $Revision: 1.2 $ Date: 11 déc. 2009
 */
public interface EditionSynchroService
{

    /**
     * Placer toutes les tâches de purge dans l'état ARRET_DEMANDE.
     */
    public void demanderArretDesPurgesEnCoursExecution();

    /**
     * Demander l'arrêt d'une tâche de purge en cours d'exécution.
     * 
     * @param trcPurgeEdition --
     */
    public void enregistreDemandeArretPurge(TrcPurgeEdition trcPurgeEdition);

    /**
     * Placer dans l'état ARRET_DEMANDE les Objets TrcPurgeEdition qui sont dans l'état EN_COURS_EXECUTION qui ont
     * débutées avant date passée en paramètres.
     * 
     * @param datePurgeCourante --
     */
    public void enregistreDemandeArretPurgesAnterieures(Date datePurgeCourante);

    /**
     * Enregistrer le démarrage d'une tâche de purge des éditions. Si une autre édition est démarrée dans la même
     * milliseconde, on incrémente le moment du démarrage d'une milliseconde et on teste de nouveau si une autre tâche
     * possède la même date de début. L'opération est répétée jusqu'à 10 fois. A la 11eme tentative, on lève une
     * exception
     * 
     * @param applicationOrigine --
     * @param trcPurgeEdition --
     */
    public void enregistreDemarragePurge(String applicationOrigine, TrcPurgeEdition trcPurgeEdition);

    /**
     * Rechercher l'objet TcPurgeEdition correspondant à un instant de démarrage donné.
     * 
     * @param instantDemarrage --
     * @param applicationOrigine --
     * @return the trc purge edition
     */
    public TrcPurgeEdition findUnTrcPurgeEditionPourUnInstantDemarrage(Date instantDemarrage, String applicationOrigine);

    /**
     * Exécuter une tache de purge.
     * 
     * @param nombreOccurences --
     * @param dateCourante --
     */
    public void purgeEditionsSiAutorise(final int nombreOccurences, final Date dateCourante);

    /**
     * Supprimer tous les objets TrcPurgeEdition.
     * 
     * @return the int
     */
    public int supprimeToutesLesTracesDePurge();

    /**
     * Supprimer toutes les entrées de l'historique dans l'état DISPONIBLE dont le fichier dans le SF n'existe plus.
     * 
     * @param nombreOccurences --
     */
    public void synchroniseHistoriqueAvecSf(final int nombreOccurences);

    /**
     * Supprimer tous les fichiers de l'arborescence qui ne correspondent à aucune entrée dans l'historique des édition.
     */
    public void synchroniseSfAvecHistorique();
}
