/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
*
 *
 * fichier : AbstractServiceVirtualEditionCommunImpl.java
 *
 */
package fr.gouv.finances.lombok.edition.service.impl;

import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.web.servlet.DispatcherServlet;
import org.springframework.web.servlet.i18n.AcceptHeaderLocaleResolver;
import org.springframework.web.servlet.view.jasperreports.JasperReportsMultiFormatView;

import fr.gouv.finances.lombok.edition.bean.AbstractParametrageVueJasper;
import fr.gouv.finances.lombok.edition.bean.ParametrageVueJasperCsv;
import fr.gouv.finances.lombok.edition.bean.ParametrageVueJasperHtml;
import fr.gouv.finances.lombok.edition.bean.ParametrageVueJasperJxl;
import fr.gouv.finances.lombok.edition.bean.ParametrageVueJasperOdt;
import fr.gouv.finances.lombok.edition.bean.ParametrageVueJasperPdf;
import fr.gouv.finances.lombok.edition.bean.ParametrageVueJasperPng;
import fr.gouv.finances.lombok.edition.bean.ParametrageVueJasperRtf;
import fr.gouv.finances.lombok.edition.bean.ParametrageVueJasperXls;
import fr.gouv.finances.lombok.edition.service.ServiceEditionCommun;
import fr.gouv.finances.lombok.edition.service.ServiceEditionJasperCommun;
import fr.gouv.finances.lombok.mvc.jasperreports.CpJasperReportsHtmlView;
import fr.gouv.finances.lombok.upload.bean.FichierJoint;
import fr.gouv.finances.lombok.util.exception.ApplicationExceptionTransformateur;
import fr.gouv.finances.lombok.util.exception.ProgrammationException;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.fill.JRSwapFileVirtualizer;
import net.sf.jasperreports.engine.util.JRSwapFile;

/**
 * Class AbstractServiceVirtualEditionCommunImpl 
 * 
 * @author chouard-cp
 * @version $Revision: 1.5 $ Date: 11 déc. 2009
 */
public abstract class AbstractServiceVirtualEditionCommunImpl implements ServiceEditionCommun,
    ServiceEditionJasperCommun
{
    protected Logger log = LoggerFactory.getLogger(getClass());

    private JasperReportsMultiFormatView editionview;

    private String nomEdition;

    private String typeEditionParDefaut;

    private String subreportdir;

    private String communsubreportdir;

    private String imagesSourceDir;

    private Integer nombreJoursConservation;

    private AbstractParametrageVueJasper parametrageVueJasper;

    public AbstractServiceVirtualEditionCommunImpl()
    {
        super();
    }

    /**
     * Création de la collection contenant les données principales utilisées pour constituer l'édition. Les beans de
     * cette collection sont utilisés pour remplir les champs placès dans le section DETAIL du modèle jasperreports.
     * 
     * @param parametresEdition paramètres utilisés pour constituer les données de l'édition
     * @return collection contenant les données de l'édition
     */
    public abstract Collection creerDatasource(Map parametresEdition);

    /**
     * Paramétrage du nom de fichier à utiliser pour nommer l'édition Par défaut null. A surcharger si nécessaire. sinon
     * le nom du fichier est généré
     * 
     * @param parametresEdition param
     * @return le nom du fichier à utiliser pour stocker l'édition
     */
    public abstract String creerNomDuFichier(Map parametresEdition);

    /**
     * Créer une Map contenant les paramètres spécifiques à une instance de l'édition.
     * 
     * @param parametresEdition paramètres utilisés pour constituer les parametres utilisés pour remplir les
     *        'parametres' de l'édition
     * @return map contenant les parametres de l'édition
     */
    public abstract Map creerParametresJasperPourLEntete(Map parametresEdition);

    /**
     * Cette méthode permet de préciser à quel endroit devront être stockés les fichiers de swap créés lors de la
     * génération de l'édition.
     * 
     * @return le répertoire de stockage des fichiers de swap
     */
    public abstract String determinerLocalisationRepertoireTemporaire();

    /**
     * Exécute une édition japerreport.
     * 
     * @param model param
     * @return tableau de byte contenant l'édition produite
     */
    public byte[] executeJasperreports(Map model)
    {
        byte[] result = null;
        MockHttpServletRequest request = new MockHttpServletRequest();

        request.getSession(true);
        MockHttpServletResponse response = new MockHttpServletResponse();

        request.setAttribute(DispatcherServlet.LOCALE_RESOLVER_ATTRIBUTE, new AcceptHeaderLocaleResolver());
        request.addPreferredLocale(Locale.FRANCE);

        try
        {
            editionview.render(model, request, response);
        }
        catch (Exception exception)
        {
            log.warn("ERREUR LORS DE LA PRODUCTION DE L'EDITION", exception);
            throw ApplicationExceptionTransformateur.transformer(exception);
        }

        result = response.getContentAsByteArray();
        return result;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @return character encoding
     * @see fr.gouv.finances.lombok.edition.service.ServiceEditionCommun#getCharacterEncoding()
     */
    @Override
    public String getCharacterEncoding()
    {
        this.assertParametrageVuejasperNotNull();
        return this.parametrageVueJasper.getCharacterEncoding();
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @return the communsubreportdir
     * @see fr.gouv.finances.lombok.edition.service.ServiceEditionJasperCommun#getCommunsubreportdir()
     */
    @Override
    public String getCommunsubreportdir()
    {
        return communsubreportdir;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @return description
     * @see fr.gouv.finances.lombok.edition.service.ServiceEditionCommun#getDescription()
     */
    @Override
    public String getDescription()
    {
        // Date datejour = new Date();
        // DateFormat unDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale.FRANCE);
        StringBuilder desc = new StringBuilder();

        desc.append(this.nomEdition);
        // desc.append(" (");
        // desc.append(unDateFormat.format(datejour));
        // desc.append(')');

        return desc.toString();
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param parmetresEdition
     * @return description
     * @see fr.gouv.finances.lombok.edition.service.ServiceEditionCommun#getDescription(java.util.Map)
     */
    @Override
    public String getDescription(Map parmetresEdition)
    {
        return getDescription();
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @return the editionview
     * @see fr.gouv.finances.lombok.edition.service.ServiceEditionJasperCommun#getEditionview()
     */
    @Override
    public JasperReportsMultiFormatView getEditionview()
    {
        return editionview;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @return extension
     * @see fr.gouv.finances.lombok.edition.service.ServiceEditionCommun#getExtension()
     */
    @Override
    public String getExtension()
    {
        this.assertParametrageVuejasperNotNull();
        return this.parametrageVueJasper.getExtension();
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @return the images source dir
     * @see fr.gouv.finances.lombok.edition.service.ServiceEditionJasperCommun#getImagesSourceDir()
     */
    @Override
    public String getImagesSourceDir()
    {
        return imagesSourceDir;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @return the nombre jours conservation
     * @see fr.gouv.finances.lombok.edition.service.ServiceEditionCommun#getNombreJoursConservation()
     */
    @Override
    public Integer getNombreJoursConservation()
    {
        return nombreJoursConservation;
    }

    /**
     * Accesseur de l attribut nom edition.
     * 
     * @return nom edition
     */
    public String getNomEdition()
    {
        return nomEdition;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @return the parametrage vue jasper
     * @see fr.gouv.finances.lombok.edition.service.ServiceEditionJasperCommun#getParametrageVueJasper()
     */
    @Override
    public AbstractParametrageVueJasper getParametrageVueJasper()
    {
        return parametrageVueJasper;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @return the subreportdir
     * @see fr.gouv.finances.lombok.edition.service.ServiceEditionJasperCommun#getSubreportdir()
     */
    @Override
    public String getSubreportdir()
    {
        return subreportdir;
    }

    /**
     * Accesseur de l attribut type edition par defaut.
     * 
     * @return type edition par defaut
     */
    public String getTypeEditionParDefaut()
    {
        return typeEditionParDefaut;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @return type mime
     * @see fr.gouv.finances.lombok.edition.service.ServiceEditionCommun#getTypeMime()
     */
    @Override
    public String getTypeMime()
    {
        this.assertParametrageVuejasperNotNull();
        return this.parametrageVueJasper.getTypeMimeEdition();
    }

    /**
     * Produit un fichier d'édition.
     * 
     * @param parametresEdition Map contenant les paramètres de l'édition
     * @return un objet de type FichierJoint contenant l'édition
     */
    @Override
    public FichierJoint produireFichierEdition(Map parametresEdition)
    {
        FichierJoint result = null;

        long start = traceDebutEdition();

        // -----------------------------------------------------------------------
        // -- Production des données source de l'édition : méthode à
        // surcharger --
        // -----------------------------------------------------------------------
        Collection datasource = this.creerDatasource(parametresEdition);

        result = produitFichierEdition(parametresEdition, datasource);

        traceFinExecution(start);

        return result;
    }

    /**
     * Produit une édition HTML à partir d'une collection de beans destinée à être vue "en ligne".
     * 
     * @param data param
     * @param parametresEdition param
     * @return the fichier joint
     */
    @Override
    public FichierJoint produireFichierEditionFromCollection(Collection data, Map parametresEdition)
    {
        return null;
    }

    /**
     * Produit une édition HTML.
     * 
     * @param parametresEdition param
     * @return Map contenant : - le fichier de l'édition sous la clé CpJasperReportsHtmlView.FICHIER_JOINT_KEY - un
     *         objet jasperprint sous la clé CpJasperReportsHtmlView.JASPER_PRINT_KEY
     */
    @SuppressWarnings("unchecked")
    @Override
    public Map produitEditionHtml(Map parametresEdition)
    {
        Map result = null;

        long start = traceDebutEdition();

        Collection datasource = this.creerDatasource(parametresEdition);

        result = produitFichierEditionHtml(parametresEdition, datasource);

        traceFinExecution(start);

        return result;

    }

    /**
     * Produit une édition à partir d'une collection de beans.
     * 
     * @param data param
     * @param parametresEdition param
     * @return the map
     */
    @Override
    public Map produitEditionHtmlFromCollection(Collection data, Map parametresEdition)
    {
        return null;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @param communsubreportdir the new communsubreportdir
     * @see fr.gouv.finances.lombok.edition.service.ServiceEditionJasperCommun#setCommunsubreportdir(java.lang.String)
     */
    @Override
    public void setCommunsubreportdir(String communsubreportdir)
    {
        this.communsubreportdir = communsubreportdir;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @param editionview the new editionview
     * @see fr.gouv.finances.lombok.edition.service.ServiceEditionJasperCommun#setEditionview(org.springframework.web.servlet.view.jasperreports.JasperReportsMultiFormatView)
     */
    @Override
    public void setEditionview(JasperReportsMultiFormatView editionview)
    {
        this.editionview = editionview;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @param imagesSourceDir the new images source dir
     * @see fr.gouv.finances.lombok.edition.service.ServiceEditionJasperCommun#setImagesSourceDir(java.lang.String)
     */
    @Override
    public void setImagesSourceDir(String imagesSourceDir)
    {
        this.imagesSourceDir = imagesSourceDir;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @param nombreJoursConservation the new nombre jours conservation
     * @see fr.gouv.finances.lombok.edition.service.ServiceEditionCommun#setNombreJoursConservation(java.lang.Integer)
     */
    @Override
    public void setNombreJoursConservation(Integer nombreJoursConservation)
    {
        this.nombreJoursConservation = nombreJoursConservation;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @param nomEdition the new nom edition
     * @see fr.gouv.finances.lombok.edition.service.ServiceEditionCommun#setNomEdition(java.lang.String)
     */
    @Override
    public void setNomEdition(String nomEdition)
    {
        this.nomEdition = nomEdition;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @param parametrageVueJasper the new parametrage vue jasper
     * @see fr.gouv.finances.lombok.edition.service.ServiceEditionJasperCommun#setParametrageVueJasper(fr.gouv.finances.lombok.edition.bean.AbstractParametrageVueJasper)
     */
    @Override
    public void setParametrageVueJasper(AbstractParametrageVueJasper parametrageVueJasper)
    {
        this.parametrageVueJasper = parametrageVueJasper;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @param subreportdir the new subreportdir
     * @see fr.gouv.finances.lombok.edition.service.ServiceEditionJasperCommun#setSubreportdir(java.lang.String)
     */
    @Override
    public void setSubreportdir(String subreportdir)
    {
        this.subreportdir = subreportdir;
    }

    /**
     * Modificateur de l attribut type edition par defaut.
     * 
     * @param typeEditionParDefaut le nouveau type edition par defaut
     */
    public void setTypeEditionParDefaut(String typeEditionParDefaut)
    {
        this.typeEditionParDefaut = typeEditionParDefaut;
    }

    /**
     * Configure dynamiquement la vue Csv utilisée par jasperreports.
     * 
     * @param parametresEdition param
     * @return ParametrageVueJasperCsv
     */
    protected ParametrageVueJasperCsv configurerLaVueCsv(Map parametresEdition)
    {
        return (ParametrageVueJasperCsv) this.getParametrageVueJasper();
    }

    /**
     * Configure dynamiquement la vue Html utilisée par jasperreports.
     * 
     * @param parametresEdition param
     * @return ParametrageVueJasperHtml
     */
    protected ParametrageVueJasperHtml configurerLaVueHtml(Map parametresEdition)
    {
        return (ParametrageVueJasperHtml) this.getParametrageVueJasper();
    }

    /**
     * Configure dynamiquement la vue Jxl utilisée par jasperreports.
     * 
     * @param parametresEdition param
     * @return ParametrageVueJasperJxl
     */
    protected ParametrageVueJasperJxl configurerLaVueJxl(Map parametresEdition)
    {
        return (ParametrageVueJasperJxl) this.getParametrageVueJasper();
    }

    /**
     * Configure dynamiquement la vue Odt utilisée par jasperreports.
     * 
     * @param parametresEdition param
     * @return ParametrageVueJasperOdt
     */
    protected ParametrageVueJasperOdt configurerLaVueOdt(Map parametresEdition)
    {
        return (ParametrageVueJasperOdt) this.getParametrageVueJasper();
    }

    /**
     * Configure dynamiquement la vue PDF utilisée par jasperreports.
     * 
     * @param parametresEdition param
     * @return ParametrageVueJasperPdf
     */
    protected ParametrageVueJasperPdf configurerLaVuePdf(Map parametresEdition)
    {
        return (ParametrageVueJasperPdf) this.getParametrageVueJasper();
    }

    /**
     * Configure dynamiquement la vue Png utilisée par jasperreports.
     * 
     * @param parametresEdition param
     * @return ParametrageVueJasperOdt
     */
    protected ParametrageVueJasperPng configurerLaVuePng(Map parametresEdition)
    {
        return (ParametrageVueJasperPng) this.getParametrageVueJasper();
    }

    /**
     * Configure dynamiquement la vue Rtf utilisée par jasperreports.
     * 
     * @param parametresEdition param
     * @return ParametrageVueJasperRtf
     */
    protected ParametrageVueJasperRtf configurerLaVueRtf(Map parametresEdition)
    {
        return (ParametrageVueJasperRtf) this.getParametrageVueJasper();
    }

    /**
     * Configure dynamiquement la vue Xls utilisée par jasperreports.
     * 
     * @param parametresEdition param
     * @return ParametrageVueJasperXls
     */
    protected ParametrageVueJasperXls configurerLaVueXls(Map parametresEdition)
    {
        return (ParametrageVueJasperXls) this.getParametrageVueJasper();
    }

    /**
     * methode Assert edition vuejasper not null : DGFiP.
     */
    private void assertEditionVuejasperNotNull()
    {
        if (this.editionview == null)
        {
            throw new ProgrammationException("AUCUNE VUE N'EST ASSOCIEE A L'EDITION : " + nomEdition);
        }
    }

    /**
     * methode Assert parametrage vuejasper not null : DGFiP.
     */
    private void assertParametrageVuejasperNotNull()
    {
        if (this.parametrageVueJasper == null)
        {
            throw new ProgrammationException("LA VUE JASPER '" + nomEdition + "' N'EST PAS CONFIGUREE CORRECTEMENT.");
        }
    }

    /**
     * Configure le modèle de l'édition.
     * 
     * @param parametresEdition param
     * @param datasource param
     * @return the map
     */
    @SuppressWarnings("unchecked")
    private Map configureModelEdition(Map parametresEdition, Collection datasource)
    {
        // -----------------------------------------------------------------------
        // Stocke le datasource dans le modèle --
        // -----------------------------------------------------------------------
        Map model = new HashMap();

        model.put(AbstractParametrageVueJasper.REPORT_DATA_KEY, datasource);

        JRSwapFileVirtualizer virtualizer =
            new JRSwapFileVirtualizer(2, new JRSwapFile(determinerLocalisationRepertoireTemporaire(), 1024, 1024), true);
        model.put(JRParameter.REPORT_VIRTUALIZER, virtualizer);

        // -----------------------------------------------------------------------
        // -- Production des paramètres de l'édition : méthode à
        // surcharger --
        // -----------------------------------------------------------------------
        Map paramSpecUneEdition = this.creerParametresJasperPourLEntete(parametresEdition);

        if (paramSpecUneEdition != null)
        {
            model.putAll(paramSpecUneEdition);
        }

        // -----------------------------------------------------------------------
        // Place le paramètre qui stocke le répertoire où le(s) sous
        // rapport(s) sont stockés
        // -----------------------------------------------------------------------
        model.put(AbstractParametrageVueJasper.SUBREPORT_DIR, this.getSubreportdir());

        // -----------------------------------------------------------------------
        // Place le paramètre qui stocke le répertoire où le(s) images
        // sont stockées
        // -----------------------------------------------------------------------
        model.put(AbstractParametrageVueJasper.IMAGES_SOURCE_DIR, this.getImagesSourceDir());

        // -----------------------------------------------------------------------
        // -- paramètre le type de vue à utiliser --
        // -----------------------------------------------------------------------
        Map parametresDeLaVue = this.parametrageVueJasper.parametrerVue();

        if (parametresDeLaVue != null)
        {
            model.putAll(parametresDeLaVue);
        }

        // -----------------------------------------------------------------------
        // -- Configuration de l'exporteur utilisé pour la vue
        // -----------------------------------------------------------------------
        this.configurerLaVue(parametresEdition);

        // -----------------------------------------------------------------------
        // -- paramètre le nom du fichier contenant l'édition --
        // -----------------------------------------------------------------------
        String nomFichier = this.creerNomDuFichier(parametresEdition);

        if (nomFichier != null)
        {
            model.put(AbstractParametrageVueJasper.REPORT_FILENAME_KEY, nomFichier);
        }
        return model;
    }

    /**
     * Configure la vue utilisée par JasperReports.
     * 
     * @param parametresEdition param
     */
    @SuppressWarnings("unchecked")
    private void configurerLaVue(Map parametresEdition)
    {
        this.assertParametrageVuejasperNotNull();
        this.assertEditionVuejasperNotNull();

        // Configuration dynamique d'une vue pdf
        if (this.getParametrageVueJasper() instanceof ParametrageVueJasperPdf)
        {
            this.parametrageVueJasper = this.configurerLaVuePdf(parametresEdition);
        }
        // Configuration dynamique d'une vue html
        else if (this.getParametrageVueJasper() instanceof ParametrageVueJasperHtml)
        {
            this.parametrageVueJasper = this.configurerLaVueHtml(parametresEdition);
        }

        // Configuration dynamique d'une vue xls
        else if (this.getParametrageVueJasper() instanceof ParametrageVueJasperXls)
        {
            this.parametrageVueJasper = this.configurerLaVueXls(parametresEdition);
        }

        // Configuration dynamique d'une vue jxl
        else if (this.getParametrageVueJasper() instanceof ParametrageVueJasperJxl)
        {
            this.parametrageVueJasper = this.configurerLaVueJxl(parametresEdition);
        }

        // Configuration dynamique d'une vue csv
        else if (this.getParametrageVueJasper() instanceof ParametrageVueJasperCsv)
        {
            this.parametrageVueJasper = this.configurerLaVueCsv(parametresEdition);
        }

        // Configuration dynamique d'une vue rtf
        else if (this.getParametrageVueJasper() instanceof ParametrageVueJasperRtf)
        {
            this.parametrageVueJasper = this.configurerLaVueRtf(parametresEdition);
        }

        // Configuration dynamique d'une vue odt
        else if (this.getParametrageVueJasper() instanceof ParametrageVueJasperOdt)
        {
            this.parametrageVueJasper = this.configurerLaVueOdt(parametresEdition);
        }

        // Configuration dynamique d'une vue png
        else if (this.getParametrageVueJasper() instanceof ParametrageVueJasperPng)
        {
            this.parametrageVueJasper = this.configurerLaVuePng(parametresEdition);
        }

        Map parametresConfigExporter = this.getParametrageVueJasper().parametrageJRExporter();
        Map exporterParameters = this.getEditionview().getExporterParameters();

        exporterParameters.putAll(parametresConfigExporter);

    }

    /**
     * Place le tableau de byte représentant l'édition dans un objet de type FichierJoint et affecte le type MIME du
     * fichier ainsi que son nom.
     * 
     * @param parametrageEditionJasper paramétrage de l'édition
     * @param model l'objet Map contenant les paramètres de l'édition
     * @param baos le tableau de byte constituant l'édition
     * @return objet de type FichierJoint représentant une édition
     */
    private FichierJoint getFichierJoint(AbstractParametrageVueJasper parametrageEditionJasper, Map model, byte[] baos)
    {
        FichierJoint result;

        result = new FichierJoint(baos);
        result.setTypeMimeFichier(parametrageEditionJasper.getTypeMimeEdition());
        result.setNomFichierOriginal(getNomFichier(parametrageEditionJasper, model));
        return result;
    }

    /**
     * Récupère dans la Map model le nom à utiliser pour nommer le fichier contenant l'édition et lui affecte
     * éventuellement une extension. Si le nom du fichier n'est pas transmis dans les paramètres, génère un nom de
     * fichier.
     * 
     * @param parametrageEditionJasper paramétrage de l'édition
     * @param model L'objet Map contenant les paramètres de l'édition
     * @return Nom du fichier à utiliser pour stocker une édition
     */
    private String getNomFichier(AbstractParametrageVueJasper parametrageEditionJasper, Map model)
    {
        StringBuilder nomFichier = new StringBuilder();

        // TT du nom du fichier
        if (parametrageEditionJasper != null && model.containsKey(AbstractParametrageVueJasper.REPORT_FILENAME_KEY)
            && model.get(AbstractParametrageVueJasper.REPORT_FILENAME_KEY) instanceof String)
        {
            nomFichier.append((String) model.get(AbstractParametrageVueJasper.REPORT_FILENAME_KEY));
        }
        else
        {
            nomFichier.append(AbstractParametrageVueJasper.NOM_FICHIER_PAR_DEFAUT);
            nomFichier.append((new Date()).getTime());
        }

        // TT de l'extension du fichier
        if (parametrageEditionJasper != null && nomFichier != null && nomFichier.lastIndexOf(".") == -1)
        {
            String extentionFichier = parametrageEditionJasper.getExtension();
            nomFichier.append('.');
            nomFichier.append(extentionFichier);
        }
        return nomFichier.toString();
    }

    /**
     * Produit l'édition et la retourne sous forme d'objet FichierJoint.
     * 
     * @param parametresEdition param
     * @param datasource param
     * @return the fichier joint
     */
    private FichierJoint produitFichierEdition(Map parametresEdition, Collection datasource)
    {
        FichierJoint result;

        // -----------------------------------------------------------------------
        // -- Configure le modèle de l'édition
        // -----------------------------------------------------------------------
        Map model = configureModelEdition(parametresEdition, datasource);

        // -----------------------------------------------------------------------
        // -- Exécution de jasperreport --
        // -----------------------------------------------------------------------
        byte[] baos = this.executeJasperreports(model);

        // -----------------------------------------------------------------------
        // -- Stockage de l'édition dans un objet de type FichierJoint
        // -----------------------------------------------------------------------
        result = getFichierJoint(this.parametrageVueJasper, model, baos);

        return result;
    }

    /**
     * Produit une édition HTML.
     * 
     * @param parametresEdition param
     * @param datasource param
     * @return Map contenant : - le fichier de l'édition sous la clé CpJasperReportsHtmlView.FICHIER_JOINT_KEY - un
     *         objet jasperprint sous la clé CpJasperReportsHtmlView.JASPER_PRINT_KEY
     */
    @SuppressWarnings("unchecked")
    private Map produitFichierEditionHtml(Map parametresEdition, Collection datasource)
    {
        Map result = new HashMap(2);
        FichierJoint fichierJoint = null;

        long start = traceDebutEdition();

        // -----------------------------------------------------------------------
        // -- Configure le modèle de l'édition
        // -----------------------------------------------------------------------
        Map model = configureModelEdition(parametresEdition, datasource);

        byte[] baos = this.executeJasperreports(model);

        JasperPrint jasperPrint = (JasperPrint) model.get(CpJasperReportsHtmlView.JASPER_PRINT_KEY);

        // -----------------------------------------------------------------------
        // -- Stockage de l'édition dans un objet de type FichierJoint
        // -----------------------------------------------------------------------
        fichierJoint = getFichierJoint(this.parametrageVueJasper, model, baos);

        result.put(CpJasperReportsHtmlView.JASPER_PRINT_KEY, jasperPrint);
        result.put(CpJasperReportsHtmlView.FICHIER_JOINT_KEY, fichierJoint);

        this.traceFinExecution(start);

        return result;

    }

    /**
     * Trace le début d'une édition.
     * 
     * @return le moment où la production de l'édition a commencé
     */
    private long traceDebutEdition()
    {
        long start = 0;

        if (log.isTraceEnabled())
        {
            // ----------------------
            // -- Nom de l'édition --
            // ----------------------

            StringBuilder  unMsg = new StringBuilder("NOM DE L'EDITION : ");
            unMsg.append(this.getEditionview());

            // -----------------------------------------------------------------------
            // -- Date du lancement de l'édition --
            // -----------------------------------------------------------------------
            Calendar cal = Calendar.getInstance();

            unMsg = new StringBuilder("DEBUT DU TRAITEMENT DE L'EDITION : ");
            unMsg.append(cal.getTime().toString());
            start = cal.getTimeInMillis();
            log.trace(unMsg.toString());
        }
        return start;
    }

    /**
     * Trace la fin de l'exécution.
     * 
     * @param start param
     */
    private void traceFinExecution(long start)
    {
        if (log.isTraceEnabled())
        {
            StringBuilder unMsg;
            Date uneDateCourante;

            // --------------------------------------
            // -- Date de fin d'exécution de l'édition --
            // --------------------------------------
            unMsg = new StringBuilder();
            unMsg.append("FIN DU TRAITEMENT DE  L'EDITION : ");
            uneDateCourante = Calendar.getInstance().getTime();
            unMsg.append(uneDateCourante.toString());
            log.trace(unMsg.toString());

            // ------------------------------------------------
            // -- Duree de l'exécution de l'édition --
            // -----------------------------------
            unMsg = new StringBuilder();
            unMsg.append("DUREE D'EXECUTION DE L'EDITION : ");
            long elapsedTime = Calendar.getInstance().getTimeInMillis() - start;

            unMsg.append(elapsedTime);
            unMsg.append(" MILLI SECONDES");
        }
    }

}
