/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : StatistiquesEditionsFormAction.java
 *
 */
package fr.gouv.finances.lombok.edition.mvc.webflow.action;

import java.util.Date;
import java.util.List;

import org.springframework.beans.PropertyEditorRegistry;
import org.springframework.webflow.action.FormAction;
import org.springframework.webflow.execution.Event;
import org.springframework.webflow.execution.RequestContext;

import fr.gouv.finances.lombok.edition.mvc.form.StatistiquesEditionsForm;
import fr.gouv.finances.lombok.edition.service.EditionStatistiqueService;
import fr.gouv.finances.lombok.util.propertyeditor.CpCustomDateEditor;

/**
 * Class StatistiquesEditionsFormAction --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.3 $ Date: 11 déc. 2009
 */
public class StatistiquesEditionsFormAction extends FormAction
{

    /** ELEMENT s_ dan s_ l e_ flux. */
    private static String ELEMENTS_DANS_LE_FLUX = "statistiqueseditions";

    /** editionstatistiqueserviceso. */
    private EditionStatistiqueService editionstatistiqueserviceso;

    /**
     * Instanciation de statistiques editions form action.
     */
    public StatistiquesEditionsFormAction()
    {
        super();
    }

    /**
     * Instanciation de statistiques editions form action.
     * 
     * @param arg0 --
     */
    public StatistiquesEditionsFormAction(Class arg0)
    {
        super(arg0);
    }

    /**
     * methode Preparer statistiques editions par date purge : --.
     * 
     * @param requestContext --
     * @return event
     */
    public Event preparerStatistiquesEditionsParDatePurge(RequestContext requestContext)
    {

        List statistiquesEditionsParStatus = editionstatistiqueserviceso.compteJobHistoryParDatePurge();

        requestContext.getFlowScope().put(ELEMENTS_DANS_LE_FLUX, statistiquesEditionsParStatus);

        return success();
    }

    /**
     * methode Preparer statistiques editions par jour demande : --.
     * 
     * @param requestContext --
     * @return event
     */
    public Event preparerStatistiquesEditionsParJourDemande(RequestContext requestContext)
    {

        List statistiquesEditionsJourDemande = editionstatistiqueserviceso.compteJobHistoryParJourDemande();

        requestContext.getFlowScope().put(ELEMENTS_DANS_LE_FLUX, statistiquesEditionsJourDemande);

        return success();
    }

    /**
     * methode Preparer statistiques editions par status : --.
     * 
     * @param requestContext --
     * @return event
     */
    public Event preparerStatistiquesEditionsParStatus(RequestContext requestContext)
    {
        getStatistiquesEditionsForm(requestContext);

        List statistiquesEditionsParStatus = editionstatistiqueserviceso.compteJobHistoryParStatus();

        requestContext.getFlowScope().put(ELEMENTS_DANS_LE_FLUX, statistiquesEditionsParStatus);

        return success();
    }

    /**
     * methode Preparer statistiques editions par type et status : --.
     * 
     * @param requestContext --
     * @return event
     */
    public Event preparerStatistiquesEditionsParTypeEtStatus(RequestContext requestContext)
    {

        List statistiquesEditionsParTypeEtStatus = editionstatistiqueserviceso.compteJobHistoryParTypeEtStatus();

        requestContext.getFlowScope().put(ELEMENTS_DANS_LE_FLUX, statistiquesEditionsParTypeEtStatus);

        return success();
    }

    /**
     * methode Preparer statistiques taille editions par type : --.
     * 
     * @param requestContext --
     * @return event
     */
    public Event preparerStatistiquesTailleEditionsParType(RequestContext requestContext)
    {

        List statistiquesTailleEditionsParType = editionstatistiqueserviceso.compteTailleEditionParType();

        requestContext.getFlowScope().put(ELEMENTS_DANS_LE_FLUX, statistiquesTailleEditionsParType);

        return success();
    }

    /**
     * methode Reinitialiser le formulaire : --.
     * 
     * @param requestContext --
     * @return event
     */
    public Event reinitialiserLeFormulaire(RequestContext requestContext)
    {
        getStatistiquesEditionsForm(requestContext);

        return success();
    }

    /**
     * Modificateur de l attribut editionstatistiqueserviceso.
     * 
     * @param editionstatistiqueserviceso le nouveau editionstatistiqueserviceso
     */
    public void setEditionstatistiqueserviceso(EditionStatistiqueService editionstatistiqueserviceso)
    {
        this.editionstatistiqueserviceso = editionstatistiqueserviceso;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param context
     * @param registry
     * @see org.springframework.webflow.action.FormAction#registerPropertyEditors(org.springframework.webflow.execution.RequestContext,
     *      org.springframework.beans.PropertyEditorRegistry)
     */
    @Override
    protected void registerPropertyEditors(RequestContext context, PropertyEditorRegistry registry)
    {
        super.registerPropertyEditors(context, registry);
        registry.registerCustomEditor(Date.class, new CpCustomDateEditor());
    }

    /**
     * Accesseur de l attribut statistiques editions form.
     * 
     * @param requestContext --
     * @return statistiques editions form
     */
    private StatistiquesEditionsForm getStatistiquesEditionsForm(RequestContext requestContext)
    {
        return (StatistiquesEditionsForm) requestContext.getFlowScope().getRequired(getFormObjectName(),
            StatistiquesEditionsForm.class);
    }

}
