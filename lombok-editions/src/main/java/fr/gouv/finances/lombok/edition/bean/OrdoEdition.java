/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : OrdoEdition.java
 *
 */
package fr.gouv.finances.lombok.edition.bean;

import java.util.Date;

import fr.gouv.finances.lombok.util.base.BaseBean;

/**
 * Class OrdoEdition --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.3 $ Date: 11 déc. 2009
 */
public class OrdoEdition extends BaseBean
{

    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** Date de la demande d'édition. */
    private Date dateDemande;

    /** Délai de lancement ( en minutes ). */
    private Integer delaiLancement = Integer.valueOf("0");

    /** Début de la réalisation de l'édition. */
    private Date startTime;

    /** Fin de l'exécution de l'édition. */
    private Date endTime;

    /** Statut de l'édition. */
    private String status;

    /** Nom du trigger qui déclenche le job. */
    private String triggerName;

    /** Groupe auquel appartient le trigger qui déclenche le job. */
    private String triggerGroup;

    /**
     * Message associé au job exécuté. Utilisé principalement pour stocker le message d'une exception levée par le job
     * asynchrone
     */
    private String message;

    /**
     * Stockage du résultat du batch asynchrone. Dans le cas des éditions : - soit l'url d'accès à l'édition si celle-ci
     * est stockée dans un système de fichiers - soit son id si celle-ci est stockée en base de donnée
     */
    private String resultat;

    /** Mode de soumission de l'édition SYNCHRONE, ASYNCHRONE_IMMEDIAT, ... */
    private String modeSoumission;

    /** tempsExecution - CALCUL de la durée d'exécution. */
    private String tempsExecution;

    /**
     * Instanciation de ordo edition.
     */
    public OrdoEdition()
    {
        super();
    }

    /**
     * Instanciation de ordo edition.
     * 
     * @param delaiLancement --
     */
    public OrdoEdition(Integer delaiLancement)
    {
        super();
        this.delaiLancement = delaiLancement;
    }

    /**
     * Gets the date demande.
     * 
     * @return the dateDemande
     */
    public Date getDateDemande()
    {
        return dateDemande;
    }

    /**
     * Gets the delai lancement.
     * 
     * @return the delaiLancement
     */
    public Integer getDelaiLancement()
    {
        return delaiLancement;
    }

    /**
     * Gets the end time.
     * 
     * @return the endTime
     */
    public Date getEndTime()
    {
        return endTime;
    }

    /**
     * Gets the message.
     * 
     * @return the message
     */
    public String getMessage()
    {
        return message;
    }

    /**
     * Gets the mode soumission.
     * 
     * @return the modeSoumission
     */
    public String getModeSoumission()
    {
        return modeSoumission;
    }

    /**
     * Gets the resultat.
     * 
     * @return the resultat
     */
    public String getResultat()
    {
        return resultat;
    }

    /**
     * Gets the start time.
     * 
     * @return the startTime
     */
    public Date getStartTime()
    {
        return startTime;
    }

    /**
     * Gets the status.
     * 
     * @return the status
     */
    public String getStatus()
    {
        return status;
    }

    /**
     * get TempsExecution
     * 
     * @return the tempsExecution - CALCUL de la durée d'exécution
     */
    public String getTempsExecution()
    {
        return tempsExecution;
    }

    /**
     * Gets the trigger group.
     * 
     * @return the triggerGroup
     */
    public String getTriggerGroup()
    {
        return triggerGroup;
    }

    /**
     * Gets the trigger name.
     * 
     * @return the triggerName
     */
    public String getTriggerName()
    {
        return triggerName;
    }

    /**
     * Sets the date demande.
     * 
     * @param dateDemande the dateDemande to set
     */
    public void setDateDemande(Date dateDemande)
    {
        this.dateDemande = dateDemande;
    }

    /**
     * Sets the delai lancement.
     * 
     * @param delaiLancement the delaiLancement to set
     */
    public void setDelaiLancement(Integer delaiLancement)
    {
        this.delaiLancement = delaiLancement;
    }

    /**
     * Sets the end time.
     * 
     * @param endTime the endTime to set
     */
    public void setEndTime(Date endTime)
    {
        this.endTime = endTime;
    }

    /**
     * Sets the message.
     * 
     * @param message the message to set
     */
    public void setMessage(String message)
    {
        this.message = message;
    }

    /**
     * Sets the mode soumission.
     * 
     * @param modeSoumission the modeSoumission to set
     */
    public void setModeSoumission(String modeSoumission)
    {
        this.modeSoumission = modeSoumission;
    }

    /**
     * Sets the resultat.
     * 
     * @param resultat the resultat to set
     */
    public void setResultat(String resultat)
    {
        this.resultat = resultat;
    }

    /**
     * Sets the start time.
     * 
     * @param startTime the startTime to set
     */
    public void setStartTime(Date startTime)
    {
        this.startTime = startTime;
    }

    /**
     * Sets the status.
     * 
     * @param status the status to set
     */
    public void setStatus(String status)
    {
        this.status = status;
    }

    /**
     * set tempsExecution
     * 
     * @param tempsExecution the new tempsExecution - CALCUL de la durée d'exécution
     */
    public void setTempsExecution(String tempsExecution)
    {
        this.tempsExecution = tempsExecution;
    }

    /**
     * Sets the trigger group.
     * 
     * @param triggerGroup the triggerGroup to set
     */
    public void setTriggerGroup(String triggerGroup)
    {
        this.triggerGroup = triggerGroup;
    }

    /**
     * Sets the trigger name.
     * 
     * @param triggerName the triggerName to set
     */
    public void setTriggerName(String triggerName)
    {
        this.triggerName = triggerName;
    }

}
