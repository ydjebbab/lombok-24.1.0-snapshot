/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 */
package fr.gouv.finances.lombok.edition.util;

import java.io.File;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import fr.gouv.finances.lombok.edition.bean.JobHistory;
import fr.gouv.finances.lombok.edition.bean.TrcPurgeEdition;
import fr.gouv.finances.lombok.upload.bean.FichierJoint;
import fr.gouv.finances.lombok.util.exception.ProgrammationException;
import fr.gouv.finances.lombok.util.exception.RegleGestionException;

/**
 * Utilitaire de contrôle des éditions.
 *
 * @author amleplatinec
 * @author Christophe Breheret-Girardin
 */
public class Control
{

    /** Limite de 4000 caractères. */
    public static final int MAX_VARCHAR_4000 = 4000;

    /** Limite de 2000 caractères. */
    public static final int MAX_VARCHAR_2000 = 2000;

    /** Limite de 255 caractères. */
    public static final int MAX_VARCHAR_255 = 255;

    /**
     * Teste si un tableau de byte est null.
     * 
     * @param databytes --
     * @throws ProgrammationException the programmation exception
     */
    public static void assertdatabytesNotNull(byte[] databytes) throws ProgrammationException
    {
        if (databytes == null || databytes.length == 0)
        {
            throw new ProgrammationException("Le contenu des données liées à une édition n'ont pas été fournies");
        }
    }

    /**
     * Teste si une édition est disponible.
     * 
     * @param jobHistory --
     * @throws RegleGestionException the regle gestion exception
     */
    public static void assertEditionDisponible(JobHistory jobHistory) throws RegleGestionException
    {
        assertJobHistoryNotNull(jobHistory);

        if (!jobHistory.isDisponible())
        {
            StringBuilder   msg = new StringBuilder();
            msg.append("L'EDITION (");
            msg.append(jobHistory.getEditionUuid());
            msg.append(')');
            msg.append("N'EST PAS DISPONIBLE.");
            msg.append("SON STATUT EST : ");
            msg.append(jobHistory.getStatutEdition());

            throw new RegleGestionException(msg.toString());
        }

        if (jobHistory.getStockageEdition() == null || jobHistory.getStockageEdition().getTailleFichier() == null
            || jobHistory.getStockageEdition().getTailleFichier().longValue() <= 0)
        {
            StringBuilder   msg = new StringBuilder();
            msg.append("IL EST IMPOSSIBLE DE DETERMINER LA TAILLE DE L'EDITION (");
            msg.append(jobHistory.getEditionUuid());
            msg.append("). UNE ERREUR EST SURVENUE LORS DE SA PRODUCTION.");
            throw new RegleGestionException(msg.toString());
        }
    }

    /**
     * Teste si un fichier existe et est accessible en lecture.
     * 
     * @param fichier --
     */
    public static void assertFichierAccessibleEnLecture(File fichier)
    {
        if (fichier == null)
        {
            throw new ProgrammationException("Aucun fichier n'a été fourni");
        }
        if (!fichier.exists())
        {
            throw new ProgrammationException("Le fichier '" + fichier.getAbsolutePath() + "' n'a pas été trouvé");
        }
        if (!fichier.canRead())
        {
            throw new ProgrammationException("Le fichier '" + fichier.getAbsolutePath()
                + "' n'est pas accessible en lecture");
        }
    }

    /**
     * Teste si un objet FichierJoint est null.
     * 
     * @param fichierJoint --
     * @throws ProgrammationException the programmation exception
     */
    public static void assertFichierJointNotNull(FichierJoint fichierJoint)
    {
        if (fichierJoint == null)
        {
            throw new ProgrammationException("L'OBJET FICHIERJOINT NE DOIT PAS ETRE NULL");
        }
    }

    /**
     * Teste si un objet JobHistory est null.
     * 
     * @param jobHistory --
     */
    public static void assertJobHistoryNotNull(JobHistory jobHistory)
    {
        // L'historique de l'édition doit être présente
        if (jobHistory == null)
        {
            throw new ProgrammationException("Aucun historique d'édition n'a été fourni");
        }
    }

    /**
     * Méthode permettant de tester si une liste d'historique de travaux d'édition est initialisée (même à vide).
     *
     * @param historiques liste à tester
     */
    public static void assertListJobHistoryNotNull(List<JobHistory> historiques)
    {
        if (historiques == null)
        {
            throw new ProgrammationException("La liste des historiques de travaux d'édition "
                + " n'a pas été fournie");
        }
    }
    
    /**
     * Méthode permettant de s'assurer que l'historique de l'édition a été chargé au préalable
     *
     * @param jobHistory historique de l'édition à vérifier
     */
    public static void assertJobHistoryCharge(JobHistory jobHistory)
    {
        if (jobHistory.getId() == null)
        {
            throw new ProgrammationException("L'historique d'édition fournir doit avoir été chargé "
                + "au préalable " + "(son identifiant n'est pas renseigné)");
        }
    }

    /**
     * Méthode permettant de s'assurer qu'une application a été fournie.
     *
     * @param application application à vérifier
     */
    public static void assertApplicationNonRenseignee(String application)
    {
        if (StringUtils.isEmpty(application))
        {
            throw new ProgrammationException("L'application liée à l'historique des travaux d'édition"
                + " n'a pas été fourni ");
        }
    }

    /**
     * Méthode permettant de s'assurer qu'un identifiant a été fourni.
     *
     * @param identifiant identifiant à vérifier
     */
    public static void assertIdNonRenseigne(String identifiant)
    {
        if (StringUtils.isEmpty(identifiant))
        {
            throw new ProgrammationException("L'identifiant de l'historique des travaux d'édition n'a pas été"
                + " fourni ");
        }
    }

    /**
     * Méthode permettant de s'assurer qu'un uuid a été fourni.
     *
     * @param uuid uuid à vérifier
     */
    public static void assertUuidNonRenseigne(String uuid)
    {
        if (StringUtils.isEmpty(uuid))
        {
            throw new ProgrammationException("L'UUID de l'historique des travaux d'édition"
                + " n'a pas été fourni ");
        }
    }

    /**
     * Méthode permettant de s'assurer qu'un utilisateur a été fourni.
     *
     * @param utilisateur utilisateur à vérifier
     */
    public static void assertUtilisateurNonRenseigne(String utilisateur)
    {
        if (StringUtils.isEmpty(utilisateur))
        {
            throw new ProgrammationException("L'utilisateur n'a pas été fourni ");
        }
    }
    
    /**
     * Teste si la référence à un fichier temporaire est null dans objet FichierJoint.
     * 
     * @param fichierJoint --
     * @throws ProgrammationException the programmation exception
     */
    public static void assertRefFichierTemporaireNotNull(FichierJoint fichierJoint) throws ProgrammationException
    {
        assertFichierJointNotNull(fichierJoint);

        if (fichierJoint.getLeContenuDuFichier() == null
            || fichierJoint.getLeContenuDuFichier().getFichierTemporaire() == null)
        {
            throw new ProgrammationException("AUCUN FICHIER TEMPORAIRE N'EST REFERENCE DANS L'OBJET FICHIERJOINT");
        }
    }

    /**
     * Teste si un objet TrcPurgeEdition est null.
     * 
     * @param trcPurgeEdition --
     * @throws ProgrammationException the programmation exception
     */
    public static void assertTrcPurgeEditionNotNull(TrcPurgeEdition trcPurgeEdition) throws ProgrammationException
    {
        if (trcPurgeEdition == null)
        {
            throw new ProgrammationException("L'OBJET TRCPURGEEDITION NE DOIT PAS ETRE NULL");
        }
    }

    /**
     * Retourne l'instant courant.
     * 
     * @return Timestamp
     */
    public static Timestamp instantCourant()
    {
        return new Timestamp((new Date()).getTime());
    }

    /**
     * Limite la longueur d'une chaîne de caractères.
     * 
     * @param chaine --
     * @param longueurMaxChaine --
     * @return la chaîne éventuellement tronquée
     */
    public static String limiterLaLongeurDeLaChaine(String chaine, int longueurMaxChaine)
    {
        String msg = null;
        if (chaine != null)
        {
            msg = (chaine.length() <= longueurMaxChaine ? chaine : chaine.substring(0, longueurMaxChaine - 1));
        }
        return msg;
    }

}