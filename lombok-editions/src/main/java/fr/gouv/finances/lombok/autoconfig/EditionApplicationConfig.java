package fr.gouv.finances.lombok.autoconfig;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

/**
 * Classe de configuration qui permet de conditionner les ressources à importer
 * 
 * @author celfer Date: 20 févr. 2020
 */
@Configuration
@EnableConfigurationProperties(EditionProperties.class)
public class EditionApplicationConfig
{
    private static final Logger log = LoggerFactory.getLogger(EditionApplicationConfig.class);

    @Configuration
    @ConditionalOnExpression("'${lombok.composant.edition.inclus}' == 'true'")
    @ImportResource({"classpath:conf/applicationContext-commun-edition.xml",
            "classpath:conf/applicationContext-edition.xml",
            "classpath:conf/applicationContext-edition-dao-batch.xml",
            "classpath*:conf/applicationContext-edition-dao.xml"})
    @ComponentScan(basePackages = {"fr.gouv.finances.lombok.edition.jpa.dao.impl"})
    public static class EditionRequisApplicationConfig
    {
        private static final Logger log = LoggerFactory.getLogger(EditionRequisApplicationConfig.class);

        @PostConstruct
        public void init()
        {
            log.debug(">>> Après initialisation de EditionRequisApplicationConfig");
        }

    }

    @Configuration
    @ConditionalOnExpression("'${lombok.composant.editionsansbase.inclus}' == 'true'")
    @ImportResource({"classpath:conf/applicationContext-commun-edition.xml",
            "classpath:conf/applicationContext-editionsansbase.xml"})
    // @ComponentScan(basePackages = {"fr.gouv.finances.lombok.journal.jpa.dao.impl"})
    public static class EditionSansBaseRequisApplicationConfig
    {
        private static final Logger log = LoggerFactory.getLogger(EditionRequisApplicationConfig.class);

        @PostConstruct
        public void init()
        {
            log.debug(">>> Après initialisation de EditionSansBaseRequisApplicationConfig");
        }

    }

}
