/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : CpJasperReportsCsvView.java
 *
 */
package fr.gouv.finances.lombok.mvc.jasperreports;

import org.springframework.web.servlet.view.jasperreports.AbstractJasperReportsSingleFormatView;

import fr.gouv.finances.lombok.edition.exporter.CpJRCsvExporter;
import net.sf.jasperreports.engine.JRExporter;

/**
 * Implementation de <code>AbstractJasperReportsSingleFormatView</code> qui permet de fournir
 * un rapport au format CSV.
 * 
 * @author chouard-cp
 * @author Christophe Breheret-Girardin
 */
public class CpJasperReportsCsvView extends AbstractJasperReportsSingleFormatView
{

    public CpJasperReportsCsvView()
    {
        super();
        this.setContentType("text/csv");
    }

    /**
     * Surcharge de la méthode pour la rendre finale, pour que l'appel, au sein du constructeur,
     * ne soit plus une mauvaise pratique puisqu'elle ne peut plus ainsi être surchargée dans les
     * classes dérivées.
     *
     * @see org.springframework.web.servlet.view.AbstractView#setContentType(java.lang.String)
     */
    @Override
    public final void setContentType(String contentType)
    {
        super.setContentType(contentType);
    }
    
    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @return jR exporter
     * @see org.springframework.web.servlet.view.jasperreports.AbstractJasperReportsSingleFormatView#createExporter()
     */
    @Override
    protected JRExporter createExporter()
    {
        return new CpJRCsvExporter();
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @return true, si c'est vrai
     * @see org.springframework.web.servlet.view.jasperreports.AbstractJasperReportsSingleFormatView#useWriter()
     */
    @Override
    protected boolean useWriter()
    {
        return true;
    }

}