/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 */
package fr.gouv.finances.lombok.edition.util;

/**
 * Enumération sur le nombre maximum d'édition retournées dans la bannette.
 *
 * @author Christophe Breheret-Girardin
 */
public enum NombreMaxEdition
{
    /** Nombre maximum d'éditions retournées dans la bannette pour un UID donné. */
    MAX_LIGNE_PAR_UID(101),
    /** Nombre maximum d'éditions retournées dans la bannette pour un profil donné. */
    MAX_LIGNE_PAR_PROFIL(202);

    /** nombre max d'éditions */
    int nbreMax;

    /**
     * Constructeur.
     *
     * @param nbre nombre maximum d'éditions
     */
    private NombreMaxEdition(int nbre)
    {
        nbreMax = nbre;
    }

    /**
     * Accesseur de nbreMax
     *
     * @return nbreMax
     */
    public int getNbreMax()
    {
        return nbreMax;
    }

}
