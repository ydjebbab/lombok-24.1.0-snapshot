/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : ConsultationBanetteFormAction.java
 *
 */
package fr.gouv.finances.lombok.edition.mvc.webflow.action;

import java.beans.PropertyEditor;
import java.util.List;

import org.springframework.webflow.action.FormAction;
import org.springframework.webflow.execution.Event;
import org.springframework.webflow.execution.RequestContext;

import fr.gouv.finances.lombok.apptags.util.MaxFetchTableUtil;
import fr.gouv.finances.lombok.edition.bean.JobHistory;
import fr.gouv.finances.lombok.edition.mvc.form.EditionsJasperForm;
import fr.gouv.finances.lombok.edition.service.EditionDemandeService;
import fr.gouv.finances.lombok.mvc.TelechargementView;
import fr.gouv.finances.lombok.securite.springsecurity.UtilisateurSecurityLombokContainer;
import fr.gouv.finances.lombok.securite.techbean.PersonneAnnuaire;
import fr.gouv.finances.lombok.upload.bean.FichierJoint;
import fr.gouv.finances.lombok.util.propertyeditor.SelectEditor;

/**
 * Class ConsultationBanetteFormAction --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.3 $ Date: 11 déc. 2009
 */
public class ConsultationBanetteFormAction extends FormAction
{

    /** Constant : ELEMENTS_DANS_LE_FLUX. */
    private static final String ELEMENTS_DANS_LE_FLUX = "listedeseditions";

    /** Constant : TABLEID. */
    private static final String TABLEID = "editionsjasper";

    /** Constant : MAX_LIGNE. */
    private static final int MAX_LIGNE = 100;

    /** EVEN t_ banette. */
    private static String evENTBANETTE = "banette";

    /** Constant : EVENT_TELECHARGER. */
    private static final String EVENT_TELECHARGER = "telecharger";

    /** Constant : ATTRIBUT_INPUT_EDITION_UUID. */
    private static final String ATTRIBUT_INPUT_EDITION_UUID = "editionUuid";

    /** Constant : ATTRIBUT_INPUT_UID. */
    private static final String ATTRIBUT_INPUT_UID = "uid";

    /** editiondemandeserviceso. */
    private EditionDemandeService editiondemandeserviceso;

    /**
     * Instanciation de consultation banette form action.
     */
    public ConsultationBanetteFormAction()
    {
        super();
    }

    /**
     * Instanciation de consultation banette form action.
     * 
     * @param arg0 --
     */
    public ConsultationBanetteFormAction(Class arg0)
    {
        super(arg0);
    }

    /**
     * Choix action.
     * 
     * @param requestContext --
     * @return the event
     */
    public Event choixAction(RequestContext requestContext)
    {

        Event eventResult;
        StringBuilder   msg = new StringBuilder(100);

        String editionUuid = (String) requestContext.getFlowScope().get(ATTRIBUT_INPUT_EDITION_UUID);
        String uid = (String) requestContext.getFlowScope().get(ATTRIBUT_INPUT_UID);

        if (editionUuid != null && uid != null)
        {
            JobHistory jobHistory = editiondemandeserviceso.rechercherEditionParUuid(editionUuid);

            // L'édition n'est plus référencée dans la table
            // JobHistory
            if (jobHistory == null)
            {
                msg.append("L'EDITION DEMANDEE ( " + editionUuid + " ) N'EXISTE PLUS.");
                msg
                    .append("LES EDITIONS DISPONIBLES OU EN ATTENTE D'EXECUTION SONT LISTEES DANS LE TABLEAU CI-DESSOUS");

                requestContext.getRequestScope().put("msg", msg);
                eventResult = result(evENTBANETTE);
            }

            // L'édition n'est pas encore disponible
            else if (!(JobHistory.STATUS_DISPONIBLE.equals(jobHistory.getStatutEdition()) || JobHistory.STATUS_DISPONIBLE_NON_ENVOYE
                .equals(jobHistory.getStatutEdition())))
            {
                msg.append("L'EDITION DEMANDEE ( " + editionUuid + " ) N'EST PAS ENCORE DISPONIBLE.");
                msg
                    .append("LES EDITIONS DISPONIBLES OU EN ATTENTE D'EXECUTION SONT LISTEES DANS LE TABLEAU CI-DESSOUS");
                requestContext.getRequestScope().put("msg", msg);
                eventResult = result(evENTBANETTE);
            }
            // L'édition est disponible
            else
            {
                FichierJoint fichierJoint = editiondemandeserviceso.rechercherEdition(jobHistory, uid);

                if (fichierJoint == null)
                {
                    msg.append("AUCUN FICHIER N'EST ASSOCIE À L'EDITION DEMANDEE ( " + editionUuid + " ).");
                    msg
                        .append("LES EDITIONS DISPONIBLES OU EN ATTENTE D'EXECUTION SONT LISTEES DANS LE TABLEAU CI-DESSOUS");

                    eventResult = result(evENTBANETTE);
                    requestContext.getRequestScope().put("msg", msg);
                }
                // L'édition est disponible et le fichier
                // associé aussi
                else
                {
                    requestContext.getFlowScope().put(TelechargementView.FICHIER_A_TELECHARGER, fichierJoint);
                    eventResult = result(EVENT_TELECHARGER);
                }
            }
        }
        else
        {
            eventResult = result(evENTBANETTE);
        }

        return eventResult;
    }

    /**
     * methode Preparer liste nouvelles editions accessibles a un utilisateur : --.
     * 
     * @param requestContext --
     * @return event
     */
    public Event preparerListeNouvellesEditionsAccessiblesAUnUtilisateur(RequestContext requestContext)
    {
        /*
         * Récupération des caractéristiques de la personne connectée dans le contexte sécurisé pour retrouver l'uid
         */
        PersonneAnnuaire personne = UtilisateurSecurityLombokContainer.getPersonneAnnuaire();

        List listedeseditionsutilisateur = null;

        listedeseditionsutilisateur =
            editiondemandeserviceso.rechercheNouvellesEditionsAccessiblesAUnUtilisateur(personne);

        requestContext.getConversationScope().put(ELEMENTS_DANS_LE_FLUX, listedeseditionsutilisateur);
        MaxFetchTableUtil.gererMaxFetch(requestContext, TABLEID, ELEMENTS_DANS_LE_FLUX, MAX_LIGNE + 1);
        return success();
    }

    /**
     * methode Preparer liste toutes editions accessibles a un utilisateur : --.
     * 
     * @param requestContext --
     * @return event
     */
    public Event preparerListeToutesEditionsAccessiblesAUnUtilisateur(RequestContext requestContext)
    {
        /*
         * Récupération des caractéristiques de la personne connectée dans le contexte sécurisé pour retrouver l'uid
         */
        PersonneAnnuaire personne = UtilisateurSecurityLombokContainer.getPersonneAnnuaire();

        List listedeseditionsutilisateur = null;

        listedeseditionsutilisateur =
            editiondemandeserviceso.rechercheToutesEditionsAccessiblesAUnUtilisateur(personne);

        requestContext.getConversationScope().put(ELEMENTS_DANS_LE_FLUX, listedeseditionsutilisateur);
        MaxFetchTableUtil.gererMaxFetch(requestContext, TABLEID, ELEMENTS_DANS_LE_FLUX, MAX_LIGNE + 1);
        return success();
    }

    /**
     * Sets the editiondemandeserviceso.
     * 
     * @param editiondemandeserviceso the editiondemandeserviceso to set
     */
    public void setEditiondemandeserviceso(EditionDemandeService editiondemandeserviceso)
    {
        this.editiondemandeserviceso = editiondemandeserviceso;
    }

    /**
     * methode Voir edition : --.
     * 
     * @param requestContext --
     * @return event
     */
    public Event voirEdition(RequestContext requestContext)
    {
        Event result = error();
        FichierJoint fichierJoint = null;

        List editionsjasper = (List) requestContext.getConversationScope().get(ELEMENTS_DANS_LE_FLUX);
        PropertyEditor propertyEditor = new SelectEditor(editionsjasper, "editionUuid");

        propertyEditor.setAsText(requestContext.getRequestParameters().get("editionUuid"));
        JobHistory jobHistory = (JobHistory) propertyEditor.getValue();

        if (jobHistory != null)
        {
            EditionsJasperForm form = (EditionsJasperForm) requestContext.getFlowScope().get(getFormObjectName());
            form.setJobHistory(jobHistory);
            PersonneAnnuaire personne = UtilisateurSecurityLombokContainer.getPersonneAnnuaire();

            if (personne != null)
            {
                fichierJoint = editiondemandeserviceso.rechercherEdition(jobHistory, personne.getUid());
            }
            else
            {
                fichierJoint = editiondemandeserviceso.rechercherEdition(jobHistory);
            }

            if (fichierJoint != null)
            {
                form.setFichierJoint(fichierJoint);
                requestContext.getConversationScope().put(TelechargementView.FICHIER_A_TELECHARGER, fichierJoint);
                result = success();
            }
        }

        return result;
    }

}
