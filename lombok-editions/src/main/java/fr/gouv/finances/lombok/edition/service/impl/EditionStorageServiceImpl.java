/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : EditionStorageServiceImpl.java
 *
 */
package fr.gouv.finances.lombok.edition.service.impl;

import fr.gouv.finances.lombok.edition.bean.JobHistory;
import fr.gouv.finances.lombok.edition.service.EditionStorageService;
import fr.gouv.finances.lombok.edition.util.Control;
import fr.gouv.finances.lombok.upload.bean.FichierJoint;
import fr.gouv.finances.lombok.util.base.BaseServiceImpl;
import fr.gouv.finances.lombok.util.exception.RegleGestionException;

/**
 * Class EditionStorageServiceImpl --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.2 $ Date: 11 déc. 2009
 */
public class EditionStorageServiceImpl extends BaseServiceImpl implements EditionStorageService
{

    /** stockage en base de donnees. */
    private Boolean stockageEnBaseDeDonnees;

    /** editionsfstorageserviceso. */
    private EditionStorageService editionsfstorageserviceso;

    /** editiondbstorageserviceso. */
    private EditionStorageService editiondbstorageserviceso;

    public EditionStorageServiceImpl()
    {
        super();
    }

    /**
     * Charge le contenu d'une édition identifiée par son objet jobHistory.
     * 
     * @param jobHistory --
     * @return Fichierjoint associé à l'édition
     * @throws RegleGestionException the regle gestion exception
     */
    @Override
    public FichierJoint chargerFichierEdition(JobHistory jobHistory) throws RegleGestionException
    {
        FichierJoint result = null;

        Control.assertEditionDisponible(jobHistory);

        if (this.stockageEnBaseDeDonnees.booleanValue())
        {
            result = this.editiondbstorageserviceso.chargerFichierEdition(jobHistory);
        }
        else
        {
            result = this.editionsfstorageserviceso.chargerFichierEdition(jobHistory);
        }

        return result;
    }

    /**
     * Charge le contenu d'une édition identifiée par son objet jobHistory Met à jour la liste des utilisateur qui ont
     * consulté l'édition.
     * 
     * @param jobHistory --
     * @param uid utilisateur qui consulte l'édition
     * @return Fichierjoint associé à l'édition
     * @throws RegleGestionException the regle gestion exception
     */
    @Override
    public FichierJoint chargerFichierEdition(JobHistory jobHistory, String uid) throws RegleGestionException
    {
        FichierJoint result = null;

        Control.assertJobHistoryNotNull(jobHistory);

        if (stockageEnBaseDeDonnees.booleanValue())
        {
            result = this.editiondbstorageserviceso.chargerFichierEdition(jobHistory, uid);
        }
        else
        {
            result = this.editionsfstorageserviceso.chargerFichierEdition(jobHistory, uid);
        }
        return result;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param jobHistory
     * @see fr.gouv.finances.lombok.edition.service.EditionStorageService#effacerFichierEdition(fr.gouv.finances.lombok.edition.bean.JobHistory)
     */
    @Override
    public void effacerFichierEdition(JobHistory jobHistory)
    {
        Control.assertJobHistoryNotNull(jobHistory);

        if (this.stockageEnBaseDeDonnees.booleanValue())
        {
            this.editiondbstorageserviceso.effacerFichierEdition(jobHistory);
        }
        else
        {
            this.editionsfstorageserviceso.effacerFichierEdition(jobHistory);
        }
    }

    /**
     * Sets the editiondbstorageserviceso.
     * 
     * @param editiondbstorageserviceso the editiondbstorageserviceso to set
     */
    public void setEditiondbstorageserviceso(EditionStorageService editiondbstorageserviceso)
    {
        this.editiondbstorageserviceso = editiondbstorageserviceso;
    }

    /**
     * Sets the editionsfstorageserviceso.
     * 
     * @param editionsfstorageserviceso the editionsfstorageserviceso to set
     */
    public void setEditionsfstorageserviceso(EditionStorageService editionsfstorageserviceso)
    {
        this.editionsfstorageserviceso = editionsfstorageserviceso;
    }

    /**
     * Sets the stockage en base de donnees.
     * 
     * @param stockageEnBaseDeDonnees the stockageEnBaseDeDonnees to set
     */
    public void setStockageEnBaseDeDonnees(Boolean stockageEnBaseDeDonnees)
    {
        this.stockageEnBaseDeDonnees = stockageEnBaseDeDonnees;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param jobHistory
     * @param fichierJoint
     * @see fr.gouv.finances.lombok.edition.service.EditionStorageService#stockerEdition(fr.gouv.finances.lombok.edition.bean.JobHistory,
     *      fr.gouv.finances.lombok.upload.bean.FichierJoint)
     */
    @Override
    public void stockerEdition(JobHistory jobHistory, FichierJoint fichierJoint)
    {
        Control.assertJobHistoryNotNull(jobHistory);

        jobHistory.getStockageEdition().setStockageEnBase(this.stockageEnBaseDeDonnees);

        if (stockageEnBaseDeDonnees.booleanValue())
        {
            editiondbstorageserviceso.stockerEdition(jobHistory, fichierJoint);
        }
        else
        {
            editionsfstorageserviceso.stockerEdition(jobHistory, fichierJoint);
        }

    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param nombreOccurences
     * @param applicationOrigine
     * @see fr.gouv.finances.lombok.edition.service.EditionStorageService#synchroniseHistoriqueAvecSf(int,
     *      java.lang.String)
     */
    @Override
    public void synchroniseHistoriqueAvecSf(int nombreOccurences, String applicationOrigine)
    {

        if (this.stockageEnBaseDeDonnees.booleanValue())
        {
            this.editiondbstorageserviceso.synchroniseHistoriqueAvecSf(nombreOccurences, applicationOrigine);
        }
        else
        {
            this.editionsfstorageserviceso.synchroniseHistoriqueAvecSf(nombreOccurences, applicationOrigine);
        }
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param applicationOrigine
     * @see fr.gouv.finances.lombok.edition.service.EditionStorageService#synchroniseSfAvecHistorique(java.lang.String)
     */
    @Override
    public void synchroniseSfAvecHistorique(String applicationOrigine)
    {
        if (this.stockageEnBaseDeDonnees.booleanValue())
        {
            this.editiondbstorageserviceso.synchroniseSfAvecHistorique(applicationOrigine);
        }
        else
        {
            this.editionsfstorageserviceso.synchroniseSfAvecHistorique(applicationOrigine);
        }
    }

}
