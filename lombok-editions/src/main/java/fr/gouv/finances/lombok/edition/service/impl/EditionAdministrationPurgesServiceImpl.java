/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : EditionAdministrationPurgesServiceImpl.java
 *
 */
package fr.gouv.finances.lombok.edition.service.impl;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import fr.gouv.finances.lombok.edition.bean.TrcPurgeEdition;
import fr.gouv.finances.lombok.edition.dao.TrcPurgeEditionDao;
import fr.gouv.finances.lombok.edition.service.EditionAdministrationPurgesService;
import fr.gouv.finances.lombok.edition.service.EditionSynchroService;
import fr.gouv.finances.lombok.util.base.BaseServiceImpl;

/**
 * Class EditionAdministrationPurgesServiceImpl --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.2 $ Date: 11 déc. 2009
 */
public class EditionAdministrationPurgesServiceImpl extends BaseServiceImpl implements
    EditionAdministrationPurgesService
{

    /** trcpurgeeditiondao. */
    private TrcPurgeEditionDao trcpurgeeditiondao;

    /** editionsynchroserviceso. */
    private EditionSynchroService editionsynchroserviceso;

    /** Nom de l'application à l'origine de l'édition. */
    private String appliorigine;

    /**
     * Constructeur de la classe EditionAdministrationPurgesServiceImpl.java
     *
     */
    public EditionAdministrationPurgesServiceImpl()
    {
        super(); 
        
    }

    /**
     * Demande l'arrêt d'une tâche de purge.
     * 
     * @param trcPurgeEdition --
     */
    @Override
    public void demanderArretPurgeEdition(TrcPurgeEdition trcPurgeEdition)
    {
        this.editionsynchroserviceso.enregistreDemandeArretPurge(trcPurgeEdition);
    }

    /**
     * Accesseur de l attribut editionsynchroserviceso.
     * 
     * @return editionsynchroserviceso
     */
    public EditionSynchroService getEditionsynchroserviceso()
    {
        return editionsynchroserviceso;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param max
     * @return list
     * @see fr.gouv.finances.lombok.edition.service.EditionAdministrationPurgesService#rechercherPurgesEditions(int)
     */
    @Override
    public List rechercherPurgesEditions(int max)
    {
        return this.trcpurgeeditiondao.findTousLesTrcPurgeEdition(this.appliorigine, max);
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param dateDebutPurge
     * @param max
     * @return list
     * @see fr.gouv.finances.lombok.edition.service.EditionAdministrationPurgesService#rechercherPurgesEditionsParDateDemande(java.util.Date,
     *      int)
     */
    @Override
    public List rechercherPurgesEditionsParDateDemande(Date dateDebutPurge, int max)
    {
        return this.trcpurgeeditiondao.findLesTrcPurgeEditionPourUneDate(dateDebutPurge, this.appliorigine, max);
    }

    /**
     * Retourne une liste d'objets de type TrcPurgeEdition pour une période donnée.
     * 
     * @param debutPeriodePurge --
     * @param finPeriodePurge --
     * @param max --
     * @return the list
     */
    @Override
    public List rechercherPurgesEditionsParPeriodePurge(Date debutPeriodePurge, Date finPeriodePurge, int max)
    {
        return this.trcpurgeeditiondao.findTrcPurgeEditionParPeriodePurge(debutPeriodePurge, finPeriodePurge,
            this.appliorigine, max);
    }

    /**
     * Retourne une liste d'objets de type TrcPurgeEdition associé à un purge en cours d'exécution.
     * 
     * @return the list
     */
    @Override
    public List rechercherPurgesEnCoursExecution()
    {
        return this.trcpurgeeditiondao.findTrcPurgeEditionEnCours(this.appliorigine);
    }

    /**
     * Modificateur de l attribut appliorigine.
     * 
     * @param appliorigine le nouveau appliorigine
     */
    public void setAppliorigine(String appliorigine)
    {
        this.appliorigine = appliorigine;
    }

    /**
     * Modificateur de l attribut editionsynchroserviceso.
     * 
     * @param editionsynchroserviceso le nouveau editionsynchroserviceso
     */
    public void setEditionsynchroserviceso(EditionSynchroService editionsynchroserviceso)
    {
        this.editionsynchroserviceso = editionsynchroserviceso;
    }

    /**
     * Modificateur de l attribut trcpurgeeditiondao.
     * 
     * @param trcpurgeeditiondao le nouveau trcpurgeeditiondao
     */
    public void setTrcpurgeeditiondao(TrcPurgeEditionDao trcpurgeeditiondao)
    {
        this.trcpurgeeditiondao = trcpurgeeditiondao;
    }

    /**
     * Supprime la liste des traces de purge passée en paramètres.
     * 
     * @param trcPurgeASupprimer --
     */
    @Override
    public void supprimerTrcPurgeEdition(Collection trcPurgeASupprimer)
    {
        this.trcpurgeeditiondao.deleteTrcPurgeEditions(trcPurgeASupprimer);
    }

}
