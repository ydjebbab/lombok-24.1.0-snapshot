/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : AdministrationEditionsValidator.java
 *
 */
package fr.gouv.finances.lombok.edition.mvc.validator;

import org.apache.commons.validator.GenericValidator;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import fr.gouv.finances.lombok.edition.mvc.form.AdministrationEditionsForm;

/**
 * Class AdministrationEditionsValidator --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.2 $ Date: 11 déc. 2009
 */
public class AdministrationEditionsValidator implements Validator
{

    private static String notSELECTED = "-999999";

    public AdministrationEditionsValidator()
    {
        super();         
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param clazz
     * @return true, si c'est vrai
     * @see org.springframework.validation.Validator#supports(java.lang.Class)
     */
    @Override
    public boolean supports(Class clazz)
    {
        return clazz.equals(AdministrationEditionsForm.class);

    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param arg0
     * @param arg1
     * @see org.springframework.validation.Validator#validate(java.lang.Object, org.springframework.validation.Errors)
     */
    @Override
    public void validate(Object arg0, Errors arg1)
    {
        // pas de validation globale
    }

    /**
     * methode Validate surface recherche par bean edition id : --.
     * 
     * @param command --
     * @param errors --
     */
    public void validateSurfaceRechercheParBeanEditionId(AdministrationEditionsForm command, Errors errors)
    {
        // Contrôle de surface de l'identifiant
        if (GenericValidator.isBlankOrNull(command.getBeanEditionId())
            || notSELECTED.equals(command.getBeanEditionId()))
        {
            errors.rejectValue("beanEditionId", null,
                "La saisie d'un type d'édition est obligatoire pour effectuer ce type de recherche.");
        }
    }

    /**
     * methode Validate surface recherche par date demande : --.
     * 
     * @param command --
     * @param errors --
     */
    public void validateSurfaceRechercheParDateDemande(AdministrationEditionsForm command, Errors errors)
    {
        // Contrôle de surface de l'identifiant
        if (command.getDateDemande() == null)
        {
            errors.rejectValue("dateDemande", null,
                "La sélection d'une date est obligatoire pour effectuer ce type de recherche");
        }
    }

    /**
     * methode Validate surface recherche par edition uuid : --.
     * 
     * @param command --
     * @param errors --
     */
    public void validateSurfaceRechercheParEditionUuid(AdministrationEditionsForm command, Errors errors)
    {
        // Contrôle de surface de l'identifiant
        if (GenericValidator.isBlankOrNull(command.getEditionUuid()))
        {
            errors.rejectValue("editionUuid", null,
                "La saisie de l'identifiant d'une édition est obligatoire pour effectuer ce type de recherche.");
        }
    }

    /**
     * methode Validate surface recherche par etat : --.
     * 
     * @param command --
     * @param errors --
     */
    public void validateSurfaceRechercheParEtat(AdministrationEditionsForm command, Errors errors)
    {
        // Contrôle de surface de l'identifiant
        if (GenericValidator.isBlankOrNull(command.getEtat()) || notSELECTED.equals(command.getEtat()))
        {
            errors
                .rejectValue("etat", null, "La saisie d'un état est obligatoire pour effectuer ce type de recherche.");
        }
    }

    /**
     * methode Validate surface recherche par periode demande : --.
     * 
     * @param command --
     * @param errors --
     */
    public void validateSurfaceRechercheParPeriodeDemande(AdministrationEditionsForm command, Errors errors)
    {
        // Contrôle de surface de l'identifiant
        if (command.getDebutPeriodeDemande() == null || command.getFinPeriodeDemande() == null)
        {
            errors.rejectValue("debutPeriodeDemande", null,
                "La sélection de dates de début et de fin de période est obligatoire"
                    + " pour effectuer ce type de recherche.");
        }
    }

    /**
     * methode Validate surface recherche par profil : --.
     * 
     * @param command --
     * @param errors --
     */
    public void validateSurfaceRechercheParProfil(AdministrationEditionsForm command, Errors errors)
    {
        // Contrôle de surface de l'identifiant
        if (GenericValidator.isBlankOrNull(command.getProfil()) || notSELECTED.equals(command.getProfil()))
        {
            errors.rejectValue("profil", null,
                "La saisie d'un profil est obligatoire pour effectuer ce type de recherche.");
        }
    }

    /**
     * methode Validate surface recherche par uid : --.
     * 
     * @param command --
     * @param errors --
     */
    public void validateSurfaceRechercheParUid(AdministrationEditionsForm command, Errors errors)
    {
        // Contrôle de surface de l'identifiant
        if (GenericValidator.isBlankOrNull(command.getUid()))
        {
            errors.rejectValue("uid", null,
                "La saisie d'un code utilisateur est obligatoire pour effectuer ce type de recherche.");
        }
    }
}
