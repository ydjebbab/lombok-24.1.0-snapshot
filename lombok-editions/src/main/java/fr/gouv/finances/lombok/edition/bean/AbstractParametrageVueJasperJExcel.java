/*
 * Copyright (c) 2016 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.edition.bean;

/**
 * Class AbstractParametrageVueJasperJExcel factorisation des attributs utilisés dans Les deux vues
 * (ParametrageVueJasperXls et ParametrageVueJasperJxl
 */
public abstract class AbstractParametrageVueJasperJExcel extends AbstractParametrageVueJasper
{

    /** serialVersionUID - long */
    private static final long serialVersionUID = 1L;

    /** Constant : IS_ONE_PAGE_PER_SHEET. */
    public static final String IS_ONE_PAGE_PER_SHEET = "net.sf.jasperreports.engine.export.JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET";

    /** Constant : IS_DETECT_CELL_TYPE. */
    public static final String IS_DETECT_CELL_TYPE = "net.sf.jasperreports.engine.export.JRXlsExporterParameter.IS_DETECT_CELL_TYPE";

    /** Constant : IS_FONT_SIZE_FIX_ENABLED. */
    public static final String IS_FONT_SIZE_FIX_ENABLED =
        "net.sf.jasperreports.engine.export.JRXlsExporterParameter.IS_FONT_SIZE_FIX_ENABLED";

    /** Constant : IS_REM_EMPTY_SPACE_BET_ROWS. */
    public static final String IS_REM_EMPTY_SPACE_BET_ROWS =
        "net.sf.jasperreports.engine.export.JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS";

    /** Constant : IS_WHITE_PAGE_BACKGROUND. */
    public static final String IS_WHITE_PAGE_BACKGROUND =
        "net.sf.jasperreports.engine.export.JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND";

    /** Constant : DEF_FORMAT_EDITION_VALUE. */
    protected static final String DEF_FORMAT_EDITION_VALUE = "jxl";

    /** Constant : DEF_EXTENSION. */
    protected static final String DEF_EXTENSION = "xls";

    /** Constant : DEF_CHARACTER_ENCODING. */
    protected static final String DEF_CHARACTER_ENCODING = "UTF-8";

    /** Constant : DEF_CONTENT_TYPE. */
    protected static final String DEF_CONTENT_TYPE = "application/vnd.ms-excel";

    /** Constant : DEF_MIME_TYPE. */
    protected static final String DEF_MIME_TYPE = "application/vnd.ms-excel";

    /** Constant : DEF_ONE_PAGE_PER_SHEET. */
    protected static final Boolean DEF_ONE_PAGE_PER_SHEET = Boolean.TRUE;

    /** Constant : DEF_DETECT_CELL_TYPE. */
    protected static final Boolean DEF_DETECT_CELL_TYPE = Boolean.TRUE;

    /** Constant : DEF_FONT_SIZE_FIX_ENABLED. */
    protected static final Boolean DEF_FONT_SIZE_FIX_ENABLED = Boolean.TRUE;

    /** Constant : DEF_REM_EMPTY_SPACE_BET_ROWS. */
    protected static final Boolean DEF_REM_EMPTY_SPACE_BET_ROWS = Boolean.TRUE;

    /** Constant : DEF_WHITE_PAGE_BACKGROUND. */
    protected static final Boolean DEF_WHITE_PAGE_BACKGROUND = Boolean.TRUE;

    /** one page per sheet. */
    protected Boolean onePagePerSheet;

    /** detect cell type. */
    protected Boolean detectCellType;

    /** font size fix enabled. */
    protected Boolean fontSizeFixEnabled;

    /** remove empty space between rows. */
    protected Boolean removeEmptySpaceBetweenRows;

    /** white page background. */
    protected Boolean whitePageBackground;

    /**
     * Instanciation de abstract parametrage vue jasper j excel.
     */
    public AbstractParametrageVueJasperJExcel()
    {
        super();
    }

    /**
     * Accesseur de onePagePerSheet
     *
     * @return onePagePerSheet
     */
    public Boolean getOnePagePerSheet()
    {
        return onePagePerSheet;
    }

    /**
     * Mutateur de onePagePerSheet
     *
     * @param onePagePerSheet onePagePerSheet
     */
    public void setOnePagePerSheet(Boolean onePagePerSheet)
    {
        this.onePagePerSheet = onePagePerSheet;
    }

    /**
     * Accesseur de detectCellType
     *
     * @return detectCellType
     */
    public Boolean getDetectCellType()
    {
        return detectCellType;
    }

    /**
     * Mutateur de detectCellType
     *
     * @param detectCellType detectCellType
     */
    public void setDetectCellType(Boolean detectCellType)
    {
        this.detectCellType = detectCellType;
    }

    /**
     * Accesseur de fontSizeFixEnabled
     *
     * @return fontSizeFixEnabled
     */
    public Boolean getFontSizeFixEnabled()
    {
        return fontSizeFixEnabled;
    }

    /**
     * Mutateur de fontSizeFixEnabled
     *
     * @param fontSizeFixEnabled fontSizeFixEnabled
     */
    public void setFontSizeFixEnabled(Boolean fontSizeFixEnabled)
    {
        this.fontSizeFixEnabled = fontSizeFixEnabled;
    }

    /**
     * Accesseur de removeEmptySpaceBetweenRows
     *
     * @return removeEmptySpaceBetweenRows
     */
    public Boolean getRemoveEmptySpaceBetweenRows()
    {
        return removeEmptySpaceBetweenRows;
    }

    /**
     * Mutateur de removeEmptySpaceBetweenRows
     *
     * @param removeEmptySpaceBetweenRows removeEmptySpaceBetweenRows
     */
    public void setRemoveEmptySpaceBetweenRows(Boolean removeEmptySpaceBetweenRows)
    {
        this.removeEmptySpaceBetweenRows = removeEmptySpaceBetweenRows;
    }

    /**
     * Accesseur de whitePageBackground
     *
     * @return whitePageBackground
     */
    public Boolean getWhitePageBackground()
    {
        return whitePageBackground;
    }

    /**
     * Mutateur de whitePageBackground
     *
     * @param whitePageBackground whitePageBackground
     */
    public void setWhitePageBackground(Boolean whitePageBackground)
    {
        this.whitePageBackground = whitePageBackground;
    }

}