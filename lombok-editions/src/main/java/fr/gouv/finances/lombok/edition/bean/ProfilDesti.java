/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 */
package fr.gouv.finances.lombok.edition.bean;

import java.util.HashSet;
import java.util.Set;

import fr.gouv.finances.lombok.util.base.BaseBean;

/**
 * Profil destinataire des éditions.
 * 
 * @author amleplatinec
 * @author Christophe Breheret-Girardin
 */
public class ProfilDesti extends BaseBean
{

    /** Initialisation d'un UID. */
    private static final long serialVersionUID = 1L;

    /** Identifiant technique utilisé par Hibernate. */
    private Long id;

    /** Flag utilisé pour la gestion du verrouillage optimiste par Hibernate. */
    private int version;

    /** Nom du profil. */
    private String nomProfil;

    /** Liste des filtres. */
    private Set<FiltreEdition> listeFiltres;

    /**
     * Constructeur.
     */
    public ProfilDesti()
    {
        super();
    }

    /**
     * Constructeur.
     * @param nomProfil nom du profil destinataire
     */
    public ProfilDesti(String nomProfil)
    {
        super();
        this.nomProfil = nomProfil;
    }
    
    /**
     * Ajoute un Filtre à un profil.
     * 
     * @param nomDuFiltre --
     * @return Set destiné à contenir les valeurs autorisée pour le filtre
     */
    public FiltreEdition ajouterUnFiltre(String nomDuFiltre)
    {
        // Initialise la liste des filtres
        if (this.listeFiltres == null)
        {
            this.listeFiltres = new HashSet<>();
        }

        // Crée un nouveau filtre
        FiltreEdition filtreEdition = new FiltreEdition();
        filtreEdition.setNomDuFiltre(nomDuFiltre);

        // Ajoute le filtre à la liste
        this.listeFiltres.add(filtreEdition);

        filtreEdition.setValsFiltre(new HashSet<FiltreValue>());

        return filtreEdition;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param obj
     * @return true, si c'est vrai
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
        {
            return true;
        }
        if (obj == null)
        {
            return false;
        }
        if (getClass() != obj.getClass())
        {
            return false;
        }
        final ProfilDesti other = (ProfilDesti) obj;
        if (nomProfil == null)
        {
            if (other.nomProfil != null)
            {
                return false;
            }
        }
        else if (!nomProfil.equals(other.nomProfil))
        {
            return false;
        }
        return true;
    }

    /**
     * Accesseur de l attribut id.
     * 
     * @return id
     */
    public Long getId()
    {
        return id;
    }

    /**
     * Gets the liste filtres.
     * 
     * @return the listeFiltres
     */
    public Set<FiltreEdition> getListeFiltres()
    {
        return listeFiltres;
    }

    /**
     * Accesseur de l attribut nom profil.
     * 
     * @return nom profil
     */
    public String getNomProfil()
    {
        return nomProfil;
    }

    /**
     * Accesseur de l attribut version.
     * 
     * @return version
     */
    public int getVersion()
    {
        return version;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @return int
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode()
    {
        final int PRIME = 31;
        int result = 1;

        result = PRIME * result + ((nomProfil == null) ? 0 : nomProfil.hashCode());
        return result;
    }

    /**
     * Modificateur de l attribut id.
     * 
     * @param id le nouveau id
     */
    public void setId(Long id)
    {
        this.id = id;
    }

    /**
     * Sets the liste filtres.
     * 
     * @param listeFiltres the listeFiltres to set
     */
    public void setListeFiltres(Set<FiltreEdition> listeFiltres)
    {
        this.listeFiltres = listeFiltres;
    }

    /**
     * Modificateur de l attribut nom profil.
     * 
     * @param nomProfil le nouveau nom profil
     */
    public void setNomProfil(String nomProfil)
    {
        this.nomProfil = nomProfil;
    }

    /**
     * Modificateur de l attribut version.
     * 
     * @param version le nouveau version
     */
    public void setVersion(int version)
    {
        this.version = version;
    }

}
