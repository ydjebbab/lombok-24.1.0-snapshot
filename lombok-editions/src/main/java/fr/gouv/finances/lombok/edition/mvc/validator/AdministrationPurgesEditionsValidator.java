/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : AdministrationPurgesEditionsValidator.java
 *
 */
package fr.gouv.finances.lombok.edition.mvc.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import fr.gouv.finances.lombok.edition.mvc.form.AdministrationPurgesEditionsForm;

/**
 * Class AdministrationPurgesEditionsValidator --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.2 $ Date: 11 déc. 2009
 */
public class AdministrationPurgesEditionsValidator implements Validator
{

    public AdministrationPurgesEditionsValidator()
    {
        super();
        
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param clazz
     * @return true, si c'est vrai
     * @see org.springframework.validation.Validator#supports(java.lang.Class)
     */
    @Override
    public boolean supports(Class clazz)
    {
        return clazz.equals(AdministrationPurgesEditionsForm.class);

    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param arg0
     * @param arg1
     * @see org.springframework.validation.Validator#validate(java.lang.Object, org.springframework.validation.Errors)
     */
    @Override
    public void validate(Object arg0, Errors arg1)
    {
        // pas de validation globale
    }

    /**
     * methode Validate surface recherche par date debut purge : --.
     * 
     * @param command --
     * @param errors --
     */
    public void validateSurfaceRechercheParDateDebutPurge(AdministrationPurgesEditionsForm command, Errors errors)
    {
        // Contrôle de surface de l'identifiant
        if (command.getDateDebutPurge() == null)
        {
            errors.rejectValue("dateDebutPurge", null,
                "La sélection d'une date est obligatoire pour effectuer ce type de recherche");
        }
    }

    /**
     * methode Validate surface recherche par periode purge : --.
     * 
     * @param command --
     * @param errors --
     */
    public void validateSurfaceRechercheParPeriodePurge(AdministrationPurgesEditionsForm command, Errors errors)
    {
        // Contrôle de surface de l'identifiant
        if (command.getDebutPeriodePurge() == null || command.getFinPeriodePurge() == null)
        {
            errors.rejectValue("debutPeriodePurge", null,
                "La sélection de dates de début et de fin de période est obligatoire pour effectuer ce type de recherche");
        }
    }
}
