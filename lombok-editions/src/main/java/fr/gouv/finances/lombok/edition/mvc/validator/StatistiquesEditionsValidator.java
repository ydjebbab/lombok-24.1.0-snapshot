/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : StatistiquesEditionsValidator.java
 *
 */
package fr.gouv.finances.lombok.edition.mvc.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import fr.gouv.finances.lombok.edition.mvc.form.StatistiquesEditionsForm;

/**
 * Class StatistiquesEditionsValidator --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.2 $ Date: 11 déc. 2009
 */
public class StatistiquesEditionsValidator implements Validator
{

    public StatistiquesEditionsValidator()
    {
        super();
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param clazz
     * @return true, si c'est vrai
     * @see org.springframework.validation.Validator#supports(java.lang.Class)
     */
    @Override
    public boolean supports(Class clazz)
    {
        return clazz.equals(StatistiquesEditionsForm.class);

    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param arg0
     * @param arg1
     * @see org.springframework.validation.Validator#validate(java.lang.Object, org.springframework.validation.Errors)
     */
    @Override
    public void validate(Object arg0, Errors arg1)
    {
        // pas de validation globale
    }

    /*
     * public void validateSurfaceRechercheParUid(AdministrationEditionsForm command, Errors errors) { // Contrôle de
     * surface de l'identifiant if (GenericValidator.isBlankOrNull(command.getUid())) { errors.rejectValue("uid",null,
     * "La saisie d'un code utilisateur est obligatoire pour effectuer ce type de recherche."); } } public void
     * validateSurfaceRechercheParEtat(AdministrationEditionsForm command, Errors errors) { // Contrôle de surface de
     * l'identifiant if (GenericValidator.isBlankOrNull(command.getEtat()) || NOT_SELECTED.equals(command.getEtat()) ) {
     * errors.rejectValue("etat",null, "La saisie d'un état est obligatoire pour effectuer ce type de recherche."); } }
     * public void validateSurfaceRechercheParProfil(AdministrationEditionsForm command, Errors errors) { // Contrôle de
     * surface de l'identifiant if (GenericValidator.isBlankOrNull(command.getProfil()) ||
     * NOT_SELECTED.equals(command.getProfil())) { errors.rejectValue("profil",null,
     * "La saisie d'un profil est obligatoire pour effectuer ce type de recherche."); } } public void
     * validateSurfaceRechercheParBeanEditionId(AdministrationEditionsForm command, Errors errors) { // Contrôle de
     * surface de l'identifiant if (GenericValidator.isBlankOrNull(command.getBeanEditionId()) ||
     * NOT_SELECTED.equals(command.getBeanEditionId())) { errors.rejectValue("beanEditionId",null,
     * "La saisie d'un type d'édition est obligatoire pour effectuer ce type de recherche."); } } public void
     * validateSurfaceRechercheParEditionUuid(AdministrationEditionsForm command, Errors errors) { // Contrôle de
     * surface de l'identifiant if (GenericValidator.isBlankOrNull(command.getEditionUuid())) {
     * errors.rejectValue("editionUuid",null,
     * "La saisie de l'identifiant d'une édition est obligatoire pour effectuer ce type de recherche."); } } public void
     * validateSurfaceRechercheParDateDemande(AdministrationEditionsForm command, Errors errors) { // Contrôle de
     * surface de l'identifiant if (command.getDateDemande() == null) { errors.rejectValue("dateDemande",null,
     * "La sélection d'une date est obligatoire pour effectuer ce type de recherche"."); } } public void
     * validateSurfaceRechercheParPeriodeDemande(AdministrationEditionsForm command, Errors errors) { // Contrôle de
     * surface de l'identifiant if (command.getDebutPeriodeDemande() == null || command.getFinPeriodeDemande() == null)
     * { errors.rejectValue("debutPeriodeDemande",null,
     * "La sélection de dates de début et de fin de période est obligatoire pour effectuer ce type de recherche."); } }
     */
}
