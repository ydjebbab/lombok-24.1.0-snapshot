/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : UidConsult.java
 *
 */
package fr.gouv.finances.lombok.edition.bean;

import fr.gouv.finances.lombok.util.base.BaseBean;

/**
 * Class UidConsult --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.2 $ Date: 11 déc. 2009
 */
public class UidConsult extends BaseBean
{
    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** Identifiant technique utilisé par Hibernate. */
    private Long id;

    /** Flag utilisé pour la gestion du verrouillage optimiste par Hibernate. */
    private int version;

    /** UID de l'utilisateur qui a consulté l'édition. */
    private String uidUtilisateur;

    /**
     * Instanciation de uid consult.
     */
    public UidConsult()
    {
        super();
    }

    /**
     * Instanciation de uid consult.
     * 
     * @param uidUtilisateur --
     */
    public UidConsult(String uidUtilisateur)
    {
        super();
        this.uidUtilisateur = uidUtilisateur;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param obj
     * @return true, si c'est vrai
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
        {
            return true;
        }
        if (obj == null)
        {
            return false;
        }
        if (getClass() != obj.getClass())
        {
            return false;
        }
        final UidConsult other = (UidConsult) obj;
        if (uidUtilisateur == null)
        {
            if (other.uidUtilisateur != null)
            {
                return false;
            }
        }
        else if (!uidUtilisateur.equals(other.uidUtilisateur))
        {
            return false;
        }
        return true;
    }

    /**
     * Accesseur de l attribut id.
     * 
     * @return id
     */
    public Long getId()
    {
        return id;
    }

    /**
     * Gets the uid utilisateur.
     * 
     * @return the uidUtilisateur
     */
    public String getUidUtilisateur()
    {
        return uidUtilisateur;
    }

    /**
     * Accesseur de l attribut version.
     * 
     * @return version
     */
    public int getVersion()
    {
        return version;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @return int
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode()
    {
        final int PRIME = 31;
        int result = 1;

        result = PRIME * result + ((uidUtilisateur == null) ? 0 : uidUtilisateur.hashCode());
        return result;
    }

    /**
     * Modificateur de l attribut id.
     * 
     * @param id le nouveau id
     */
    public void setId(Long id)
    {
        this.id = id;
    }

    /**
     * Sets the uid utilisateur.
     * 
     * @param uid --
     */
    public void setUidUtilisateur(String uid)
    {
        this.uidUtilisateur = uid;
    }

    /**
     * Modificateur de l attribut version.
     * 
     * @param version le nouveau version
     */
    public void setVersion(int version)
    {
        this.version = version;
    }

}
