/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : EditionSchedulerServiceImpl.java
 *
 */
package fr.gouv.finances.lombok.edition.service.impl;

import java.util.Calendar;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.impl.JobDetailImpl;
import org.quartz.impl.triggers.SimpleTriggerImpl;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ApplicationObjectSupport;

import fr.gouv.finances.lombok.edition.bean.JobHistory;
import fr.gouv.finances.lombok.edition.service.EditionSchedulerService;
import fr.gouv.finances.lombok.edition.service.ServiceEditionCommun;
import fr.gouv.finances.lombok.edition.util.Control;
import fr.gouv.finances.lombok.util.exception.ApplicationExceptionTransformateur;
import fr.gouv.finances.lombok.util.exception.ProgrammationException;

/**
 * Class EditionSchedulerServiceImpl --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.3 $ Date: 11 déc. 2009
 */
public class EditionSchedulerServiceImpl extends ApplicationObjectSupport implements EditionSchedulerService
{

    public static final String SEP = "/";

    protected final Log log = LogFactory.getLog(getClass());

    /** Ordonnanceur Quartz. */
    private Scheduler scheduler;

    public EditionSchedulerServiceImpl()
    {
        super();
    }

    /**
     * Plannifie une édition qui doit être déclenchée immédiatemnt.
     * 
     * @param jobHistory --
     * @param parametresEdition --
     * @param compression --
     * @return the job history
     */
    @Override
    public JobHistory declencherEdition(JobHistory jobHistory, Map parametresEdition, Boolean compression)
    {
        Control.assertJobHistoryNotNull(jobHistory);

        // Identifiant du JOB = editionUuid
        // Groupe de l'édition = application qui a demandé l'édition
        JobDetailImpl jobDetail =
            new JobDetailImpl(jobHistory.getEditionUuid(), jobHistory.getDestinationEdition().getApplicationOrigine(),
                EditionJob.class);

        /**
         * pour demande compas /psax/sepa compléter la descptio par la date de la demande modif amlp 07/01/09 dans le
         * cas d'une edition asynchrone , la description n'était pas affectée au job history comme dans
         * creerEtStockerEdition dans EditionProducerSErvice (on ne pouvait donc pas la modifier dans ce cas )
         */

        // jobDetail.setDescription(jobHistory.getPresentationEdition().getDescription());
        this.affecterDescriptionToJobHistory(jobHistory, parametresEdition);
        jobDetail.setDurability(false);
        // jobDetail.setVolatility(false);

        // Paramétrage du job
        JobDataMap jobDataMap = jobDetail.getJobDataMap();

        EditionJob.fillParametresEdition(jobDataMap, parametresEdition);
        EditionJob.fillJobHistory(jobDataMap, jobHistory);
        EditionJob.fillEditionCompression(jobDataMap, compression);

        Trigger trigger;
        // Déclenchement différé d'une tâche asynchrone
        if (JobHistory.MODE_SOUM_ASYNC_DIFF.equals(jobHistory.getOrdoEdition().getModeSoumission()))
        {
            Calendar cal = Calendar.getInstance(Locale.FRANCE);

            cal.setTime(jobHistory.getOrdoEdition().getDateDemande());
            cal.add(Calendar.MINUTE, jobHistory.getOrdoEdition().getDelaiLancement().intValue());

            trigger =
                new SimpleTriggerImpl(jobHistory.getEditionUuid(), jobHistory.getDestinationEdition()
                    .getApplicationOrigine(), cal.getTime());

        }
        // Déclenchement immédiat d'une tâche asynchrone
        else
        {
            trigger =
                new SimpleTriggerImpl(jobHistory.getEditionUuid(), jobHistory.getDestinationEdition()
                    .getApplicationOrigine(), 0, 0);
        }

        try
        {
            scheduler.scheduleJob(jobDetail, trigger);
        }
        catch (SchedulerException scex)
        {
            log.warn("ERREUR SCHEDULER", scex);
            throw ApplicationExceptionTransformateur.transformer(scex);
        }

        return jobHistory;
    }

    /**
     * Modificateur de l attribut scheduler.
     * 
     * @param scheduler le nouveau scheduler
     */
    public void setScheduler(Scheduler scheduler)
    {
        this.scheduler = scheduler;
    }

    /**
     * Supprime un job.
     * 
     * @param jobHistory --
     */
    @Override
    public void supprimerJob(JobHistory jobHistory)
    {
        try
        {
            Control.assertJobHistoryNotNull(jobHistory);

            this.scheduler.deleteJob(new JobKey(jobHistory.getEditionUuid(), jobHistory.getDestinationEdition()
                .getApplicationOrigine()));
        }
        catch (SchedulerException scex)
        {
            log.warn("ERREUR SCHEDULER", scex);
            throw ApplicationExceptionTransformateur.transformer(scex);
        }
    }

    /**
     * Teste si une édition est encore planifiée dans l'ordonnanceur quartz.
     * 
     * @param jobHistory --
     * @return true, if tester existence job edition
     */
    @Override
    public boolean testerExistenceJobEdition(JobHistory jobHistory)
    {
        boolean result = false;

        Control.assertJobHistoryNotNull(jobHistory);

        try
        {
            JobDetail jobDetail =
                scheduler.getJobDetail(new JobKey(jobHistory.getEditionUuid(), jobHistory.getDestinationEdition()
                    .getApplicationOrigine()));

            result = (jobDetail != null ? true : false);
        }
        catch (SchedulerException scex)
        {
            log.warn("ERREUR SCHEDULER", scex);
            throw ApplicationExceptionTransformateur.transformer(scex);
        }

        return result;
    }

    /**
     * rajout pour compléter la descrption du jobhistory *.
     * 
     * @param jobHistory --
     * @param parametresEdition --
     */

    /**
     * Affecte la description d'une édition en utilisant éventuellement les paramètres de l'édition passés en paramètres
     * 
     * @param jobHistory
     * @param parametresEdition
     */
    private void affecterDescriptionToJobHistory(JobHistory jobHistory, Map parametresEdition)
    {
        ServiceEditionCommun serviceEdition = this.getReferenceServiceEditionCommun(jobHistory.getBeanEditionId());

        String description =
            Control.limiterLaLongeurDeLaChaine(serviceEdition.getDescription(parametresEdition),
                Control.MAX_VARCHAR_255);

        jobHistory.getPresentationEdition().setDescription(description);
    }

    /**
     * Lecture dans le contexte d'application de la référence identifiée l'id du bean.
     * 
     * @param editionBeanId --
     * @return the reference bean id
     */
    private Object getReferenceBeanId(String editionBeanId)
    {
        Object editionBeanObject = null;
        ApplicationContext appliContext = this.getApplicationContext();

        if (appliContext == null)
        {
            throw new ProgrammationException("LE CONTEXTE DE L'APPLICATION EST NULL");
        }
        else
        {
            if (appliContext.containsBean(editionBeanId))
            {

                editionBeanObject = appliContext.getBean(editionBeanId);
            }
            else
            {
                throw new ProgrammationException("LE BEAN " + editionBeanId
                    + " N'EST PAS DEFINI DANS LE CONTEXTE D'EDITION.");
            }
        }
        return editionBeanObject;
    }

    /**
     * Lit dans le contexte d'édition la référence du bean editionBeanId et la retourne dans le type
     * ServiceEditionCommun.
     * 
     * @param editionBeanId --
     * @return ServiceEditionCommun
     */
    private ServiceEditionCommun getReferenceServiceEditionCommun(String editionBeanId)
    {
        ServiceEditionCommun serviceEdition = null;

        Object object = this.getReferenceBeanId(editionBeanId);

        if (object instanceof ServiceEditionCommun)
        {
            serviceEdition = (ServiceEditionCommun) object;
        }
        else
        {
            throw new ProgrammationException("LE BEAN " + editionBeanId + " N'EST PAS DE TYPE SERVICEEDITIONCOMMUN");
        }
        return serviceEdition;
    }

    /* fin rajout pour compléter la descrption du jobhistory */

}
