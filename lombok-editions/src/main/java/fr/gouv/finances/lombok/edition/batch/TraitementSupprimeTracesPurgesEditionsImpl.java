/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : TraitementSupprimeTracesPurgesEditionsImpl.java
 *
 */
package fr.gouv.finances.lombok.edition.batch;

import fr.gouv.finances.lombok.batch.ServiceBatchCommunImpl;
import fr.gouv.finances.lombok.edition.service.EditionSynchroService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

/**
 * Class TraitementSupprimeTracesPurgesEditionsImpl --.
 *
 * @author amleplatinec
 * @version $Revision: 1.2 $ Date: 11 déc. 2009
 */
@Service("traitementsupprimetracespurgeseditions")
@Profile("batch")
@Lazy(true)
public class TraitementSupprimeTracesPurgesEditionsImpl extends ServiceBatchCommunImpl {

    /**
     * editionsynchroserviceso.
     */
    @Autowired()
    @Qualifier("editionsynchroserviceso")
    private EditionSynchroService editionsynchroserviceso;

    /**
     * Instanciation de traitement supprime traces purges editions impl.
     */
    public TraitementSupprimeTracesPurgesEditionsImpl() {
        super();
    }

    /**
     * Modificateur de l attribut editionsynchroserviceso.
     *
     * @param editionsynchroserviceso le nouveau editionsynchroserviceso
     */
    public void setEditionsynchroserviceso(EditionSynchroService editionsynchroserviceso) {
        this.editionsynchroserviceso = editionsynchroserviceso;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     *
     * @see fr.gouv.finances.lombok.batch.ServiceBatchCommunImpl#traiterBatch()
     */
    @Override
    public void traiterBatch()
    {
        editionsynchroserviceso.supprimeToutesLesTracesDePurge();
    }

    @Value("${traitementsupprimetracespurgeseditions.versionbatch}")
    @Override()
    public void setVersionBatch(String versionBatch) {
        super.setVersionBatch(versionBatch);
    }

    @Value("${traitementsupprimetracespurgeseditions.nombatch}")
    @Override()
    public void setNomBatch(String nomBatch) {
        super.setNomBatch(nomBatch);
    }
}
