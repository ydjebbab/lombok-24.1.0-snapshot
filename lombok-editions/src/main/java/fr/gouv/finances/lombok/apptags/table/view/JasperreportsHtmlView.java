/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : JasperreportsHtmlView.java
 *
 */
package fr.gouv.finances.lombok.apptags.table.view;

import java.util.Collection;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.jsp.PageContext;

import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.j2ee.servlets.ImageServlet;

import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import fr.gouv.finances.lombok.apptags.table.bean.Column;
import fr.gouv.finances.lombok.apptags.table.core.TableModel;
import fr.gouv.finances.lombok.apptags.table.view.jasper.CpJasperReportsHtmlView;
import fr.gouv.finances.lombok.apptags.util.EcTableException;
import fr.gouv.finances.lombok.edition.service.EditionProducerService;

/**
 * Class JasperreportsHtmlView --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.2 $ Date: 11 déc. 2009
 */
public class JasperreportsHtmlView implements View
{

    /**
     * Instanciation de jasperreports html view.
     */
    public JasperreportsHtmlView()
    {
        super();
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param model
     * @return object
     * @see fr.gouv.finances.lombok.apptags.table.view.View#afterBody(fr.gouv.finances.lombok.apptags.table.core.TableModel)
     */
    @Override
    public Object afterBody(TableModel model)
    {
        // Lecture de l'id du bean de l'édition contenu dans l'attribut
        // fileName
        String editionBeanId = model.getExportHandler().getCurrentExport().getFileName();

        // Lecture de la collection de données qui sert à produire
        // l'édition
        Collection collection = model.getCollectionOfBeans();

        // Lecture du nom de l'attribut qui pointe (dans le contexte de
        // requête)
        // vers la Map de paramètres utilisée pour produire l'édition
        String parametresAttr = model.getExportHandler().getCurrentExport().getText();

        Map mapParametres = null;

        if (model.getContext().getRequestAttribute(parametresAttr) instanceof Map)
        {
            mapParametres = (Map) model.getContext().getRequestAttribute(parametresAttr);
        }

        Map result = null;
        EditionProducerService editionProducerService = null;

        if (model != null && model.getContext() != null && model.getContext().getContextObject() instanceof PageContext)
        {
            PageContext pageContext = (PageContext) model.getContext().getContextObject();
            ServletContext sc = pageContext.getServletContext();
            WebApplicationContext applicationContext = WebApplicationContextUtils.getWebApplicationContext(sc);

            String[] beanTab = applicationContext.getBeanNamesForType(EditionProducerService.class);

            if (beanTab == null || beanTab.length == 0)
            {
                throw new EcTableException("AUCUN BEAN DE TYPE " + EditionProducerService.class.getName()
                    + " N'EST PAS PRESENT DANS LE CONTEXTE");
            }
            else
            {
                String producerServiceSoName = beanTab[0];

                if (applicationContext.containsBean(producerServiceSoName))
                {
                    try
                    {
                        editionProducerService =
                            (EditionProducerService) applicationContext.getBean(producerServiceSoName,
                                EditionProducerService.class);
                    }
                    catch (NoSuchBeanDefinitionException exception)
                    {
                        throw new EcTableException("LE BEAN " + producerServiceSoName
                            + " N'EST PAS DE TYPE EDITIONPRODUCERSERVICE", exception);
                    }
                }
                else
                {
                    throw new EcTableException("LE BEAN " + producerServiceSoName
                        + " N'EST PAS PRESENT DANS LE CONTEXTE");
                }
            }

            result =
                editionProducerService.creerEditionHtmlFromCollectionEtParametres(editionBeanId, collection,
                    mapParametres);
            JasperPrint jasperPrint = (JasperPrint) result.get(CpJasperReportsHtmlView.JASPER_PRINT_KEY);

            model.getContext().setSessionAttribute(ImageServlet.DEFAULT_JASPER_PRINT_SESSION_ATTRIBUTE, jasperPrint);
        }
        else
        {
            throw new EcTableException("Impossible de lire le contexte de l'application");
        }

        return result;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param model
     * @see fr.gouv.finances.lombok.apptags.table.view.View#beforeBody(fr.gouv.finances.lombok.apptags.table.core.TableModel)
     */
    @Override
    public void beforeBody(TableModel model)
    {
        // RAS
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param model
     * @param column
     * @see fr.gouv.finances.lombok.apptags.table.view.View#body(fr.gouv.finances.lombok.apptags.table.core.TableModel,
     *      fr.gouv.finances.lombok.apptags.table.bean.Column)
     */
    @Override
    public void body(TableModel model, Column column)
    {
        // RAS
    }
}
