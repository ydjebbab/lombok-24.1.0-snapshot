/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : AdministrationEditionsForm.java
 *
 */
package fr.gouv.finances.lombok.edition.mvc.form;

import java.util.Date;

import fr.gouv.finances.lombok.edition.bean.JobHistory;
import fr.gouv.finances.lombok.util.base.BaseBean;

/**
 * Class AdministrationEditionsForm --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.2 $ Date: 11 déc. 2009
 */
public class AdministrationEditionsForm extends BaseBean
{

    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** uid. */
    private String uid;

    /** edition uuid. */
    private String editionUuid;

    /** date demande. */
    private Date dateDemande;

    /** debut periode demande. */
    private Date debutPeriodeDemande;

    /** fin periode demande. */
    private Date finPeriodeDemande;

    /** profil. */
    private String profil;

    /** etat. */
    private String etat;

    /** bean edition id. */
    private String beanEditionId;

    /** job history. */
    private JobHistory jobHistory;

    /**
     * Constructeur de la classe AdministrationEditionsForm.java
     *
     */
    public AdministrationEditionsForm()
    {
        super();
        
    }

    /**
     * Gets the bean edition id.
     * 
     * @return the beanEditionId
     */
    public String getBeanEditionId()
    {
        return beanEditionId;
    }

    /**
     * Gets the date demande.
     * 
     * @return the dateDemande
     */
    public Date getDateDemande()
    {
        return dateDemande;
    }

    /**
     * Gets the debut periode demande.
     * 
     * @return the debutPeriodeDemande
     */
    public Date getDebutPeriodeDemande()
    {
        return debutPeriodeDemande;
    }

    /**
     * Gets the edition uuid.
     * 
     * @return the editionuuid
     */
    public String getEditionUuid()
    {
        return editionUuid;
    }

    /**
     * Gets the etat.
     * 
     * @return the etat
     */
    public String getEtat()
    {
        return etat;
    }

    /**
     * Gets the fin periode demande.
     * 
     * @return the finPeriodeDemande
     */
    public Date getFinPeriodeDemande()
    {
        return finPeriodeDemande;
    }

    /**
     * Gets the job history.
     * 
     * @return the jobHistory
     */
    public JobHistory getJobHistory()
    {
        return jobHistory;
    }

    /**
     * Gets the profil.
     * 
     * @return the profil
     */
    public String getProfil()
    {
        return profil;
    }

    /**
     * Gets the uid.
     * 
     * @return the uid
     */
    public String getUid()
    {
        return uid;
    }

    /**
     * Sets the bean edition id.
     * 
     * @param beanEditionId the beanEditionId to set
     */
    public void setBeanEditionId(String beanEditionId)
    {
        this.beanEditionId = beanEditionId;
    }

    /**
     * Sets the date demande.
     * 
     * @param dateDemande the dateDemande to set
     */
    public void setDateDemande(Date dateDemande)
    {
        this.dateDemande = dateDemande;
    }

    /**
     * Sets the debut periode demande.
     * 
     * @param debutPeriodeDemande the debutPeriodeDemande to set
     */
    public void setDebutPeriodeDemande(Date debutPeriodeDemande)
    {
        this.debutPeriodeDemande = debutPeriodeDemande;
    }

    /**
     * Sets the edition uuid.
     * 
     * @param editionuuid the editionuuid to set
     */
    public void setEditionUuid(String editionuuid)
    {
        this.editionUuid = editionuuid;
    }

    /**
     * Sets the etat.
     * 
     * @param etat the etat to set
     */
    public void setEtat(String etat)
    {
        this.etat = etat;
    }

    /**
     * Sets the fin periode demande.
     * 
     * @param finPeriodeDemande the finPeriodeDemande to set
     */
    public void setFinPeriodeDemande(Date finPeriodeDemande)
    {
        this.finPeriodeDemande = finPeriodeDemande;
    }

    /**
     * Sets the job history.
     * 
     * @param jobHistory the jobHistory to set
     */
    public void setJobHistory(JobHistory jobHistory)
    {
        this.jobHistory = jobHistory;
    }

    /**
     * Sets the profil.
     * 
     * @param profil the profil to set
     */
    public void setProfil(String profil)
    {
        this.profil = profil;
    }

    /**
     * Sets the uid.
     * 
     * @param uid the uid to set
     */
    public void setUid(String uid)
    {
        this.uid = uid;
    }

}
