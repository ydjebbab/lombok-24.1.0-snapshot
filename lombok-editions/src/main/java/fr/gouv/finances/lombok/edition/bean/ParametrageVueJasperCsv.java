/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 */
package fr.gouv.finances.lombok.edition.bean;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class ParametrageVueJasperCsv --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.2 $ Date: 11 déc. 2009
 */
public class ParametrageVueJasperCsv extends AbstractParametrageVueJasper
{
    /** Initialisation de la journalisation. */
    protected static final Logger LOGGER = LoggerFactory.getLogger(ParametrageVueJasperRtf.class);

    /** Initialisation de l'UID. */
    private static final long serialVersionUID = 1L;

    /** Constant : DEF_FORMAT_EDITION_VALUE. */
    private static final String DEF_FORMAT_EDITION_VALUE = "csv";

    /** Constant : DEF_EXTENSION. */
    private static final String DEF_EXTENSION = "txt";

    /** Constant : DEF_CHARACTER_ENCODING. */
    private static final String DEF_CHARACTER_ENCODING = "ISO-8859-1";

    /** Constant : DEF_CONTENT_TYPE. */
    private static final String DEF_CONTENT_TYPE = "text/plain;charset=ISO-8859-1";

    /** Constant : DEF_MIME_TYPE. */
    private static final String DEF_MIME_TYPE = "text/plain";

    /** Constant : DEF_FIELD_DELIMITER. */
    private static final String DEF_FIELD_DELIMITER = ";";

    /** Constant : DEF_RECORD_DELIMITER. */
    private static final String DEF_RECORD_DELIMITER = "\n";

    /** Constant : FIELD_DELIMITER. */
    private static final String FIELD_DELIMITER =
        "net.sf.jasperreports.engine.export.JRCsvExporterParameter.FIELD_DELIMITER";

    /** Constant : RECORD_DELIMITER. */
    private static final String RECORD_DELIMITER =
        "net.sf.jasperreports.engine.export.JRCsvExporterParameter.RECORD_DELIMITER";

    /** field delimiter. */
    private String fieldDelimiter;

    /** record delimiter. */
    private String recordDelimiter;

    /**
     * Construit un objet ParametrageVueJasperCsv et lui affecte les paramètres par défaut.
     */
    public ParametrageVueJasperCsv()
    {
        super();

        this.formatEdition = DEF_FORMAT_EDITION_VALUE;
        this.characterEncoding = DEF_CHARACTER_ENCODING;
        this.extension = DEF_EXTENSION;
        this.contentType = DEF_CONTENT_TYPE;
        this.typeMimeEdition = DEF_MIME_TYPE;

        // Spécifique csv
        this.fieldDelimiter = DEF_FIELD_DELIMITER;
        this.recordDelimiter = DEF_RECORD_DELIMITER;
    }

    /**
     * Accesseur de l attribut field delimiter.
     * 
     * @return field delimiter
     */
    public String getFieldDelimiter()
    {
        return fieldDelimiter;
    }

    /**
     * Accesseur de l attribut record delimiter.
     * 
     * @return record delimiter
     */
    public String getRecordDelimiter()
    {
        return recordDelimiter;
    }

    /**
     * Produit une Map contenant le paramétrage de l'exporteur Jasper.
     *
     * @return le paramétrage de l'exporteur Jasper
     */
    public Map<String, Object> parametrageJRExporter()
    {
        // Déclaration d'une Map qui contiendra le paramétrage
        Map<String, Object> mapModele = new HashMap<>();

        // Paramétrage général de l'exporteur Jasper
        mapModele.put(CHARACTER_ENCODING, this.characterEncoding);

        // Paramétrage spécifique CSV
        putIfNotNull(mapModele, FIELD_DELIMITER, this.fieldDelimiter);
        putIfNotNull(mapModele, RECORD_DELIMITER, this.recordDelimiter);

        // Journalisation du paramétrage
        journaliserParametrageJRExporter(mapModele);

        return mapModele;
    }

    /**
     * Produit une Map contenant le paramétrage de la vue Jasper en utilisant : <br/>
     * - soit les valeurs par défaut,<br/>
     * - soit les valeurs contenues dans le propriétés de l'objet ParametrageVueJasperCsv
     * (valeurs injectée dans le fichier de configuration des éditions).
     * 
     * @return le paramétrage de la vue Jasper
     */
    public Map<String, String> parametrerVue()
    {
        // Déclaration d'une Map qui contiendra le paramétrage
        Map<String, String> mapModele = new HashMap<>();
        
        // Initialisation du paramétrage
        putIfNotNull(mapModele, REPORT_VIEW_KEY, this.formatEdition);
        putIfNotNull(mapModele, EXTENSION_KEY, this.extension);
        putIfNotNull(mapModele, CONTENT_TYPE_KEY, this.contentType);
        putIfNotNull(mapModele, MIME_TYPE_KEY, this.typeMimeEdition);

        // Journalisation du paramétrage
        journaliserParametrageVue(mapModele);

        return mapModele;
    }

    /**
     * Modificateur de l attribut field delimiter.
     * 
     * @param fieldDelimiter le nouveau field delimiter
     */
    public void setFieldDelimiter(String fieldDelimiter)
    {
        this.fieldDelimiter = fieldDelimiter;
    }

    /**
     * Modificateur de l attribut record delimiter.
     * 
     * @param recordDelimiter le nouveau record delimiter
     */
    public void setRecordDelimiter(String recordDelimiter)
    {
        this.recordDelimiter = recordDelimiter;
    }
}
