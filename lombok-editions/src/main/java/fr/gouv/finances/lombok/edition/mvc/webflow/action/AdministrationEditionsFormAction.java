/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : AdministrationEditionsFormAction.java
 *
 */
package fr.gouv.finances.lombok.edition.mvc.webflow.action;

import java.beans.PropertyEditor;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.springframework.beans.PropertyEditorRegistry;
import org.springframework.validation.Errors;
import org.springframework.webflow.action.FormAction;
import org.springframework.webflow.action.FormObjectAccessor;
import org.springframework.webflow.execution.Event;
import org.springframework.webflow.execution.RequestContext;
import org.springframework.webflow.execution.ScopeType;

import fr.gouv.finances.lombok.apptags.util.CheckboxSelectUtil;
import fr.gouv.finances.lombok.apptags.util.MaxFetchTableUtil;
import fr.gouv.finances.lombok.edition.bean.JobHistory;
import fr.gouv.finances.lombok.edition.mvc.form.AdministrationEditionsForm;
import fr.gouv.finances.lombok.edition.mvc.form.EditionsJasperForm;
import fr.gouv.finances.lombok.edition.service.EditionAdministrationService;
import fr.gouv.finances.lombok.edition.service.EditionDemandeService;
import fr.gouv.finances.lombok.mvc.TelechargementView;
import fr.gouv.finances.lombok.securite.springsecurity.UtilisateurSecurityLombokContainer;
import fr.gouv.finances.lombok.securite.techbean.PersonneAnnuaire;
import fr.gouv.finances.lombok.upload.bean.FichierJoint;
import fr.gouv.finances.lombok.util.exception.ApplicationException;
import fr.gouv.finances.lombok.util.propertyeditor.CpBooleanEditor;
import fr.gouv.finances.lombok.util.propertyeditor.CpCustomDateEditor;
import fr.gouv.finances.lombok.util.propertyeditor.SelectEditor;

/**
 * Class AdministrationEditionsFormAction --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.3 $ Date: 11 déc. 2009
 */
public class AdministrationEditionsFormAction extends FormAction
{

    /** ELEMENT s_ dan s_ l e_ flux. */
    private static String elementsDANSLEFLUX = "adminlistedeseditions";

    /** ELT s_ a_ supprime r_ dan s_ flux. */
    private static String eltsASUPPRIMERDANSFLUX = "editionsjasperasupprimer";

    /** TABLEID. */
    private static String tableID = "adminlistedeseditions";

    /** MA x_ ligne. */
    private static int maxLIGNE = 100;

    /** editiondemandeserviceso. */
    private EditionDemandeService editiondemandeserviceso;

    /** editionadministrationserviceso. */
    private EditionAdministrationService editionadministrationserviceso;

    /**
     * Instanciation de administration editions form action.
     */
    public AdministrationEditionsFormAction()
    {
        super();
    }

    /**
     * Instanciation de administration editions form action.
     * 
     * @param arg0 --
     */
    public AdministrationEditionsFormAction(Class arg0)
    {
        super(arg0);
    }

    /**
     * methode Parametrer checkboxes : --.
     * 
     * @param request --
     * @return event
     */
    public Event parametrerCheckboxes(RequestContext request)
    {
        // Sauvegarde de l'objet de paramétrage des checkboxes dans le
        // FlowScope
        CheckboxSelectUtil.parametrerCheckboxes(request, tableID).utiliserRowid("id").utiliserListeElements(
            elementsDANSLEFLUX).utiliserMsgSelectionneUnSeulElement("Sélectionnez une seule édition à la fois.")
            .utiliserMsgAucunEltSelectionne("Aucune édition n'est sélectionnée")
            .stockerLesElementsSelectionnesDansFlowScope().stockerLesMessagesDansRequestScope();
        return success();
    }

    /**
     * methode Preparation liste bean edition id : --.
     * 
     * @param requestContext --
     * @return event
     */
    public Event preparationListeBeanEditionId(RequestContext requestContext)
    {
        List descriptions = editionadministrationserviceso.rechercherEditionBeanIds();
        requestContext.getFlowScope().put("listedesbeaneditionid", descriptions);
        return success();
    }

    /**
     * methode Preparation liste etats : --.
     * 
     * @param requestContext --
     * @return event
     */
    public Event preparationListeEtats(RequestContext requestContext)
    {
        List<String> etats = new ArrayList<String>();
        etats.add(JobHistory.STATUS_DISPONIBLE);
        etats.add(JobHistory.STATUS_DISPONIBLE_NON_ENVOYE);
        etats.add(JobHistory.STATUS_EN_ATTENTE_EXECUTION);
        etats.add(JobHistory.STATUS_ECHEC);
        etats.add(JobHistory.STATUS_EN_COURS_EXECUTION);
        etats.add(JobHistory.STATUS_INITIALISE);

        requestContext.getFlowScope().put("listedesetatspossibles", etats);
        return success();
    }

    /**
     * methode Preparation liste profils : --.
     * 
     * @param requestContext --
     * @return event
     */
    public Event preparationListeProfils(RequestContext requestContext)
    {
        List profils = editionadministrationserviceso.rechercherProfils();
        requestContext.getFlowScope().put("listedesprofils", profils);
        return success();
    }

    /**
     * methode Preparer liste editions par bean id : --.
     * 
     * @param requestContext --
     * @return event
     */
    public Event preparerListeEditionsParBeanId(RequestContext requestContext)
    {
        AdministrationEditionsForm administrationEditionsForm = getAdministrationEditionForm(requestContext);
        List listedeseditionsuid =
            editionadministrationserviceso.rechercherEditionParBeanId(administrationEditionsForm.getBeanEditionId());

        requestContext.getFlowScope().put(elementsDANSLEFLUX, listedeseditionsuid);
        MaxFetchTableUtil.gererMaxFetch(requestContext, tableID, elementsDANSLEFLUX, maxLIGNE + 1);

        return success();
    };

    /**
     * methode Preparer liste editions par date demande : --.
     * 
     * @param requestContext --
     * @return event
     */
    public Event preparerListeEditionsParDateDemande(RequestContext requestContext)
    {
        AdministrationEditionsForm administrationEditionsForm = getAdministrationEditionForm(requestContext);
        List listedeseditionsuid =
            editionadministrationserviceso.rechercherEditionParDateDemande(administrationEditionsForm.getDateDemande());

        requestContext.getFlowScope().put(elementsDANSLEFLUX, listedeseditionsuid);
        MaxFetchTableUtil.gererMaxFetch(requestContext, tableID, elementsDANSLEFLUX, maxLIGNE + 1);

        return success();
    }

    /**
     * methode Preparer liste editions par edition uuid : --.
     * 
     * @param requestContext --
     * @return event
     */
    public Event preparerListeEditionsParEditionUuid(RequestContext requestContext)
    {
        AdministrationEditionsForm administrationEditionsForm = getAdministrationEditionForm(requestContext);
        List listedeseditionsuuid =
            editionadministrationserviceso.rechercherEditionParEditionUuid(administrationEditionsForm.getEditionUuid());

        requestContext.getFlowScope().put(elementsDANSLEFLUX, listedeseditionsuuid);

        return success();

    }

    /**
     * methode Preparer liste editions par etat : --.
     * 
     * @param requestContext --
     * @return event
     */
    public Event preparerListeEditionsParEtat(RequestContext requestContext)
    {
        AdministrationEditionsForm administrationEditionsForm = getAdministrationEditionForm(requestContext);
        List listedeseditionsuid =
            editionadministrationserviceso.rechercherEditionParEtat(administrationEditionsForm.getEtat());

        requestContext.getFlowScope().put(elementsDANSLEFLUX, listedeseditionsuid);
        MaxFetchTableUtil.gererMaxFetch(requestContext, tableID, elementsDANSLEFLUX, maxLIGNE + 1);

        return success();
    }

    /**
     * methode Preparer liste editions par periode date demande : --.
     * 
     * @param requestContext --
     * @return event
     */
    public Event preparerListeEditionsParPeriodeDateDemande(RequestContext requestContext)
    {
        AdministrationEditionsForm administrationEditionsForm = getAdministrationEditionForm(requestContext);
        List listedeseditionsuid =
            editionadministrationserviceso.rechercherEditionParPeriodeDateDemande(administrationEditionsForm
                .getDebutPeriodeDemande(), administrationEditionsForm.getFinPeriodeDemande());

        requestContext.getFlowScope().put(elementsDANSLEFLUX, listedeseditionsuid);
        MaxFetchTableUtil.gererMaxFetch(requestContext, tableID, elementsDANSLEFLUX, maxLIGNE + 1);

        return success();
    }

    /**
     * methode Preparer liste editions par profil : --.
     * 
     * @param requestContext --
     * @return event
     */
    public Event preparerListeEditionsParProfil(RequestContext requestContext)
    {
        AdministrationEditionsForm administrationEditionsForm = getAdministrationEditionForm(requestContext);
        List listedeseditionsuid =
            editionadministrationserviceso.rechercherEditionParProfil(administrationEditionsForm.getProfil());

        requestContext.getFlowScope().put(elementsDANSLEFLUX, listedeseditionsuid);
        MaxFetchTableUtil.gererMaxFetch(requestContext, tableID, elementsDANSLEFLUX, maxLIGNE + 1);

        return success();
    }

    /**
     * methode Preparer liste editions par uid : --.
     * 
     * @param requestContext --
     * @return event
     */
    public Event preparerListeEditionsParUid(RequestContext requestContext)
    {
        AdministrationEditionsForm administrationEditionsForm = getAdministrationEditionForm(requestContext);
        List listedeseditionsuid =
            editionadministrationserviceso.rechercherEditionParUid(administrationEditionsForm.getUid());

        requestContext.getFlowScope().put(elementsDANSLEFLUX, listedeseditionsuid);
        MaxFetchTableUtil.gererMaxFetch(requestContext, tableID, elementsDANSLEFLUX, maxLIGNE + 1);

        return success();
    }

    /**
     * methode Preparer liste nouvelles editions accessibles a un utilisateur : --.
     * 
     * @param requestContext --
     * @return event
     */
    public Event preparerListeNouvellesEditionsAccessiblesAUnUtilisateur(RequestContext requestContext)
    {
        /*
         * Récupération des caractéristiques de la personne connectée dans le contexte sécurisé pour retrouver l'uid
         */
        PersonneAnnuaire personne = UtilisateurSecurityLombokContainer.getPersonneAnnuaire();

        List listedeseditionsutilisateur = null;

        listedeseditionsutilisateur =
            editiondemandeserviceso.rechercheNouvellesEditionsAccessiblesAUnUtilisateur(personne);

        requestContext.getFlowScope().put(elementsDANSLEFLUX, listedeseditionsutilisateur);
        MaxFetchTableUtil.gererMaxFetch(requestContext, tableID, elementsDANSLEFLUX, maxLIGNE + 1);
        return success();
    }

    /**
     * methode Preparer liste toutes editions accessibles a un utilisateur : --.
     * 
     * @param requestContext --
     * @return event
     */
    public Event preparerListeToutesEditionsAccessiblesAUnUtilisateur(RequestContext requestContext)
    {
        /*
         * Récupération des caractéristiques de la personne connectée dans le contexte sécurisé pour retrouver l'uid
         */
        PersonneAnnuaire personne = UtilisateurSecurityLombokContainer.getPersonneAnnuaire();

        List listedeseditionsutilisateur = null;

        listedeseditionsutilisateur =
            editiondemandeserviceso.rechercheToutesEditionsAccessiblesAUnUtilisateur(personne);

        requestContext.getFlowScope().put(elementsDANSLEFLUX, listedeseditionsutilisateur);
        MaxFetchTableUtil.gererMaxFetch(requestContext, tableID, elementsDANSLEFLUX, maxLIGNE + 1);
        return success();
    }

    /**
     * methode Reinitialiser le formulaire : --.
     * 
     * @param requestContext --
     * @return event
     */
    public Event reinitialiserLeFormulaire(RequestContext requestContext)
    {
        AdministrationEditionsForm administrationEditionsForm = getAdministrationEditionForm(requestContext);
        administrationEditionsForm.setBeanEditionId(null);
        administrationEditionsForm.setDateDemande(null);
        administrationEditionsForm.setDebutPeriodeDemande(null);
        administrationEditionsForm.setFinPeriodeDemande(null);
        administrationEditionsForm.setEtat(null);
        administrationEditionsForm.setProfil(null);
        administrationEditionsForm.setUid(null);
        administrationEditionsForm.setEditionUuid(null);
        return success();
    }

    /**
     * methode Selectionner edition a telecharger : --.
     * 
     * @param requestContext --
     * @return event
     */
    public Event selectionnerEditionATelecharger(RequestContext requestContext)
    {
        Event result = error();

        List listeeditions = (List) requestContext.getFlowScope().get(elementsDANSLEFLUX);
        PropertyEditor propertyEditor = new SelectEditor(listeeditions, "editionUuid");

        propertyEditor.setAsText(requestContext.getRequestParameters().get("editionUuid"));
        JobHistory jobHistoryElSelected = (JobHistory) propertyEditor.getValue();

        FichierJoint fichierJoint = editiondemandeserviceso.rechercherEdition(jobHistoryElSelected);
        if (fichierJoint != null)
        {
            requestContext.getFlowScope().put(TelechargementView.FICHIER_A_TELECHARGER, fichierJoint);
            result = success();
        }
        return result;
    }

    /**
     * methode Selectionner editions a supprimer : --.
     * 
     * @param requestContext --
     * @return event
     */
    public Event selectionnerEditionsASupprimer(RequestContext requestContext)
    {
        Event result;
        Collection elementsSelectionnes =
            CheckboxSelectUtil.extraitLesElementsSelectionnesDansLeFlux(requestContext, tableID);

        if (CheckboxSelectUtil.auMoinsUnElementSelectionne(elementsSelectionnes))
        {
            requestContext.getFlowScope().put(eltsASUPPRIMERDANSFLUX, elementsSelectionnes);
            result = success();
        }
        else
        {
            result = error();
        }
        return result;
    }

    /**
     * methode Selectionner par une checkbox : --.
     * 
     * @param requestContext --
     * @return event
     */
    public Event selectionnerParUneCheckbox(RequestContext requestContext)
    {
        Event result;

        Object elementSelectionne = CheckboxSelectUtil.extraitUnElementSelectionneDansLeFlux(requestContext, tableID);
        if (elementSelectionne != null)
        {
            AdministrationEditionsForm administrationEditionsForm = getAdministrationEditionForm(requestContext);
            administrationEditionsForm.setJobHistory((JobHistory) elementSelectionne);
            result = success();
        }
        else
        {
            result = error();
        }
        return result;
    }

    /**
     * Sets the editionadministrationserviceso.
     * 
     * @param editionadministrationserviceso the editionadministrationserviceso to set
     */
    public void setEditionadministrationserviceso(EditionAdministrationService editionadministrationserviceso)
    {
        this.editionadministrationserviceso = editionadministrationserviceso;
    }

    /**
     * Sets the editiondemandeserviceso.
     * 
     * @param editiondemandeserviceso the editiondemandeserviceso to set
     */
    public void setEditiondemandeserviceso(EditionDemandeService editiondemandeserviceso)
    {
        this.editiondemandeserviceso = editiondemandeserviceso;
    }

    /**
     * methode Supprimer editions : --.
     * 
     * @param requestContext --
     * @return event
     */
    @SuppressWarnings("unchecked")
    public Event supprimerEditions(RequestContext requestContext)
    {
        Event result = success();
        Collection elementsASupprimer = requestContext.getFlowScope().getCollection(eltsASUPPRIMERDANSFLUX);

        try
        {
            Collection editionsSupprimees = editiondemandeserviceso.supprimerEditions(elementsASupprimer);
            Collection listeDesJobHistory = requestContext.getFlowScope().getCollection(elementsDANSLEFLUX);

            listeDesJobHistory.removeAll(editionsSupprimees);
        }
        catch (ApplicationException exception)
        {
            logger.debug("Exception détectée : ", exception);
            Errors errors =
                new FormObjectAccessor(requestContext).getFormErrors(getFormObjectName(), ScopeType.REQUEST);
            errors.reject(null, exception.getMessage());
            result = error();
        }

        return result;
    }

    /**
     * methode Traiter checkboxes entre pages : --.
     * 
     * @param context --
     * @return event
     */
    public Event traiterCheckboxesEntrePages(RequestContext context)
    {
        CheckboxSelectUtil.traiterCheckboxesEntrePages(context, tableID);
        return success();
    }

    /**
     * methode Voir edition : --.
     * 
     * @param requestContext --
     * @return event
     */
    public Event voirEdition(RequestContext requestContext)
    {
        Event result = error();
        FichierJoint fichierJoint = null;

        List editionsjasper = (List) requestContext.getFlowScope().get(elementsDANSLEFLUX);
        PropertyEditor propertyEditor = new SelectEditor(editionsjasper, "editionUuid");

        propertyEditor.setAsText(requestContext.getRequestParameters().get("editionUuid"));
        JobHistory jobHistory = (JobHistory) propertyEditor.getValue();

        if (jobHistory != null)
        {
            EditionsJasperForm form = (EditionsJasperForm) requestContext.getFlowScope().get(getFormObjectName());
            form.setJobHistory(jobHistory);
            PersonneAnnuaire personne = UtilisateurSecurityLombokContainer.getPersonneAnnuaire();

            if (personne != null)
            {
                fichierJoint = editiondemandeserviceso.rechercherEdition(jobHistory, personne.getUid());
            }
            else
            {
                fichierJoint = editiondemandeserviceso.rechercherEdition(jobHistory);
            }

            if (fichierJoint != null)
            {
                form.setFichierJoint(fichierJoint);
                requestContext.getFlowScope().put(TelechargementView.FICHIER_A_TELECHARGER, fichierJoint);
                result = success();
            }
        }

        return result;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param context
     * @param registry
     * @see org.springframework.webflow.action.FormAction#registerPropertyEditors(org.springframework.webflow.execution.RequestContext,
     *      org.springframework.beans.PropertyEditorRegistry)
     */
    @Override
    protected void registerPropertyEditors(RequestContext context, PropertyEditorRegistry registry)
    {
        super.registerPropertyEditors(context, registry);
        registry.registerCustomEditor(Date.class, new CpCustomDateEditor());
        registry.registerCustomEditor(Boolean.class, new CpBooleanEditor());
    }

    /**
     * Accesseur de l attribut administration edition form.
     * 
     * @param requestContext --
     * @return administration edition form
     */
    private AdministrationEditionsForm getAdministrationEditionForm(RequestContext requestContext)
    {
        return (AdministrationEditionsForm) requestContext.getFlowScope().getRequired(getFormObjectName(),
            AdministrationEditionsForm.class);
    }

}
