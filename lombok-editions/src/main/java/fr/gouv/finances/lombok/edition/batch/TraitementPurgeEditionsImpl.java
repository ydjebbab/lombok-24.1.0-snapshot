/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : TraitementPurgeEditionsImpl.java
 *
 */
package fr.gouv.finances.lombok.edition.batch;

import fr.gouv.finances.lombok.batch.ServiceBatchCommunImpl;
import fr.gouv.finances.lombok.edition.service.EditionDemandeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

/**
 * Class TraitementPurgeEditionsImpl --.
 *
 * @author amleplatinec
 * @version $Revision: 1.2 $ Date: 11 déc. 2009
 */
@Service("traitementpurgeeditions")
@Profile("batch")
@Lazy(true)
public class TraitementPurgeEditionsImpl extends ServiceBatchCommunImpl {

    /**
     * editiondemandeserviceso.
     */
    @Autowired()
    @Qualifier("editiondemandeserviceimpl")
    private EditionDemandeService editiondemandeserviceso;

    /**
     * Instanciation de traitement purge editions impl.
     */
    public TraitementPurgeEditionsImpl() {
        super();
    }

    /**
     * Gets the editiondemandeserviceso.
     *
     * @return the editiondemandeserviceso
     */
    public EditionDemandeService getEditiondemandeserviceso() {
        return editiondemandeserviceso;
    }

    /**
     * Sets the editiondemandeserviceso.
     *
     * @param editiondemandeserviceso the editiondemandeserviceso to set
     */
    public void setEditiondemandeserviceso(EditionDemandeService editiondemandeserviceso) {
        this.editiondemandeserviceso = editiondemandeserviceso;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     *
     * @see fr.gouv.finances.lombok.batch.ServiceBatchCommunImpl#traiterBatch()
     */
    @Override
    public void traiterBatch()
    {
        editiondemandeserviceso.purgeEtSynchronise();
    }

    @Value("${traitementpurgeeditions.versionbatch}")
    @Override()
    public void setVersionBatch(String versionBatch) {
        super.setVersionBatch(versionBatch);
    }

    @Value("${traitementpurgeeditions.nombatch}")
    @Override()
    public void setNomBatch(String nomBatch) {
        super.setNomBatch(nomBatch);
    }
}
