/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 */
package fr.gouv.finances.lombok.edition.dao;

/**
 * Propriétés liées aux historiques des travaux d'édition.
 * @see fr.gouv.finances.lombok.edition.bean.JobHistory
 *
 * @author Christophe Breheret-Girardin
 */
public enum ProprieteJobHistory
{
    /** Destination de l'édition. */
    DESTINATION_EDITION("destinationEdition"),
    
    /** Ordonnancement de l'édition */
    ORDONNANCEMENT_EDITION("ordoEdition"),
    
    /** Archivage liée à l'édition. */
    ARCHIVAGE_EDITION("archivageEdition"),
    
    /** Application liée à l'édition. */
    APPLICATION_ORIGINE("applicationOrigine"),

    /** Identifiant de l'édition. */
    IDENTIFIANT_EDITION("beanEditionId"),

    /** Statut de l'édition. */
    STATUT("status"),

    /** Propriété du temps d'exécution. */
    TEMPS_EXECUTION("ordoEdition.tempsExecution"),

    /** UUID de l'édition. */
    UID_EDITION("editionUuid"),

    /** Date de demande de l'édition. */
    DATE_DEMANDE("dateDemande"),

    /** Propriétaire de l'édition. */
    UID_PROPRIETAIRE("uidProprietaire"),

    /** Est-ce que l'édition est nouvelle ou non ? */
    NOUVELLE_EDITION("nouvelleEdition"),

    /** Est-ce que l'édition est mono destinataire ou non ? */
    MONO_DESTINATAIRE("monoDestinataire"),

    /** Profils destinataires de l'édition. */
    PROFILS_DESTINATAIRES("destinationEdition.profilsDesti"),

    /** Nom du profil. */
    NOM_PROFIL("nomProfil"),

    /** Liste des filtres rattachés à l'édition. */
    LISTE_FILTRE("destinationEdition.profilsDesti.listeFiltres"),

    /** Valeurs de la liste des filtres rattachés à l'édition. */
    VALEUR_FILTRE("destinationEdition.profilsDesti.listeFiltres.valsFiltre"),

    /** Nom du filtre. */
    NOM_FILTRE("nomDuFiltre"),

    /** Valeur du filtre. */
    VAL_FILTRE("value"),

    /** UID de consultation. */
    UID_CONSULTATION("destinationEdition.uidConsult"),

    /** UID du propriétaire de l'édition. */
    UID_UTILISATEUR("uidUtilisateur"),

    /** Contenu de l'édition. */
    CONTENU_EDITION("stockageEdition.leContenuEdition"),

    /** Date de purge de l'édition. */
    DATE_PURGE("datePurge"),

    /** Alias d'accès au profil. */
    ALIAS_PROFILS("profils"),

    /** Alias d'accès au nom du profil. */
    ALIAS_NOM_PROFIL("nomProfil"),

    /** Alias d'accès à la liste de filtre. */
    ALIAS_LISTE_FILTRE("listeFiltres"),

    /** Alias d'accès à la valeur du filtre. */
    ALIAS_VALEUR_FILTRE("valsFiltre"),

    /** Alias d'accès à l'UID de consultation. */
    ALIAS_UID_CONSULTATION("uidConsult");
    
    /** Nom de la propriété */
    private String nom;

    /**
     * Constructeur.
     *
     * @param libelleNom libellé du nom de la propriété
     */
    private ProprieteJobHistory(String libelleNom)
    {
        nom = libelleNom;
    }

    /**
     * Accesseur de nom
     *
     * @return nom de la propriété
     */
    public String getNom()
    {
        return nom;
    }
    
}
