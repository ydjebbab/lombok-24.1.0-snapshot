/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 */
package fr.gouv.finances.lombok.edition.bean;

import java.util.HashMap;
import java.util.Map;

import net.sf.jasperreports.engine.export.JRPdfExporterParameter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.lowagie.text.pdf.PdfWriter;

/**
 * Paramétrage Jasper de la vue PDF.
 * 
 * @author amleplatinec
 * @author Christophe Breheret-Girardin
 */
public class ParametrageVueJasperPdf extends AbstractParametrageVueJasper
{
    /** Initialisation de la journalisation. */
    protected static final Logger LOGGER = LoggerFactory.getLogger(ParametrageVueJasperPdf.class);

    /** Initialisation de l'UID. */
    private static final long serialVersionUID = 1L;

    /** Constant : DEF_FORMAT_EDITION_VALUE. */
    private static final String DEF_FORMAT_EDITION_VALUE = "pdf";

    /** Constant : DEF_EXTENSION. */
    private static final String DEF_EXTENSION = "pdf";

    /** Constant : DEF_CHARACTER_ENCODING. */
    private static final String DEF_CHARACTER_ENCODING = "UTF-8";

    /** Constant : DEF_CONTENT_TYPE. */
    private static final String DEF_CONTENT_TYPE = "application/pdf";

    /** Constant : DEF_MIME_TYPE. */
    private static final String DEF_MIME_TYPE = "application/pdf";

    /** Constant : DEF_PDF_VERSION. */
    private static final Character DEF_PDF_VERSION = JRPdfExporterParameter.PDF_VERSION_1_5;

    /** Constant : DEf_COMPRESSED. */
    private static final Boolean DEf_COMPRESSED = Boolean.TRUE;

    /** Constant : DEF_CRE_BATCH_MODE_BOOKMARKS. */
    private static final Boolean DEF_CRE_BATCH_MODE_BOOKMARKS = Boolean.TRUE;

    /** Constant : DEF_FORCE_LINEBREAK_POLICY. */
    private static final Boolean DEF_FORCE_LINEBREAK_POLICY = Boolean.TRUE;

    /** Constant : DEF_ALLOW_ASSEMBLY. */
    private static final Boolean DEF_ALLOW_ASSEMBLY = Boolean.FALSE;

    /** Constant : DEF_ALLOW_COPY. */
    private static final Boolean DEF_ALLOW_COPY = Boolean.FALSE;

    /** Constant : DEF_ALLOW_DEGRADED_PRINTING. */
    private static final Boolean DEF_ALLOW_DEGRADED_PRINTING = Boolean.TRUE;

    /** Constant : DEF_ALLOW_PRINTING. */
    private static final Boolean DEF_ALLOW_PRINTING = Boolean.TRUE;

    /** Constant : DEF_ALLOW_SCREEN_READERS. */
    private static final Boolean DEF_ALLOW_SCREEN_READERS = Boolean.FALSE;

    /** Constant : DEF_ENCRYPTED. */
    private static final Boolean DEF_ENCRYPTED = Boolean.FALSE;

    /** Constant : DEF_CENTVINGTHUITBITKEY. */
    private static final Boolean DEF_CENTVINGTHUITBITKEY = Boolean.FALSE;

    /** Constant : PDF_VERSION. */
    private static final String PDF_VERSION = "net.sf.jasperreports.engine.export.JRPdfExporterParameter.PDF_VERSION";

    /** Constant : IS_COMPRESSED. */
    private static final String IS_COMPRESSED =
        "net.sf.jasperreports.engine.export.JRPdfExporterParameter.IS_COMPRESSED";

    /** Constant : IS_CREA_BATCH_MODE_BOOKMAR. */
    private static final String IS_CREA_BATCH_MODE_BOOKMAR =
        "net.sf.jasperreports.engine.export.JRPdfExporterParameter.IS_CREATING_BATCH_MODE_BOOKMARKS";

    /** Constant : FORCE_LINEBREAK_POLICY. */
    private static final String FORCE_LINEBREAK_POLICY =
        "net.sf.jasperreports.engine.export.JRPdfExporterParameter.FORCE_LINEBREAK_POLICY";

    /** Constant : PERMISSIONS. */
    private static final String PERMISSIONS = "net.sf.jasperreports.engine.export.JRPdfExporterParameter.PERMISSIONS";

    /** Constant : IS_ENCRYPTED. */
    private static final String IS_ENCRYPTED = "net.sf.jasperreports.engine.export.JRPdfExporterParameter.IS_ENCRYPTED";

    /** Constant : IS_128_BIT_KEY. */
    private static final String IS_128_BIT_KEY =
        "net.sf.jasperreports.engine.export.JRPdfExporterParameter.IS_128_BIT_KEY";

    /** Constant : USER_PASSWORD. */
    private static final String USER_PASSWORD =
        "net.sf.jasperreports.engine.export.JRPdfExporterParameter.USER_PASSWORD";

    /** Constant : OWNER_PASSWORD. */
    private static final String OWNER_PASSWORD =
        "net.sf.jasperreports.engine.export.JRPdfExporterParameter.OWNER_PASSWORD";

    /** Constant : METADATA_AUTHOR. */
    private static final String METADATA_AUTHOR =
        "net.sf.jasperreports.engine.export.JRPdfExporterParameter.METADATA_AUTHOR";

    /** Constant : METADATA_CREATOR. */
    private static final String METADATA_CREATOR =
        "net.sf.jasperreports.engine.export.JRPdfExporterParameter.METADATA_CREATOR";

    /** Constant : METADATA_TITLE. */
    private static final String METADATA_TITLE =
        "net.sf.jasperreports.engine.export.JRPdfExporterParameter.METADATA_TITLE";

    /** Constant : METADATA_SUBJECT. */
    private static final String METADATA_SUBJECT =
        "net.sf.jasperreports.engine.export.JRPdfExporterParameter.METADATA_SUBJECT";

    /** Constant : METADATA_KEYWORDS. */
    private static final String METADATA_KEYWORDS =
        "net.sf.jasperreports.engine.export.JRPdfExporterParameter.METADATA_KEYWORDS";

    /** Entier qui indique la version de PDF utilisée (3->1.3, ... , 6->1.6) */
    private Character pdfVersion;

    /** Booléen qui indique si le document doit être compressé ou non. */
    private Boolean compressed;

    /** Booléen qui indique si le document PDF doit contenir une section 'outline'. */
    private Boolean creatingBatchModeBookmarks;

    /**
     * Booléen qui définit si l'implémentation (voir SplitCharacter) qui assure que le découpage d'un texte en ligne est
     * effectué de la même façon par iText et par le processus de remplissage de jasperreports. La logique de découpe en
     * ligne utilisée par AWT lors du processus de remplissage est différente de celle mise en oeuvre par iText (lors de
     * l'export en PDF).
     */
    private Boolean forceLinebreakPolicy;

    /** Booléen qui indique si le document est crypté. */
    private Boolean encrypted;

    /** cent vingt huit bit key. */
    private Boolean centVingtHuitBitKey;

    /** allow assembly. */
    private Boolean allowAssembly;

    /** allow copy. */
    private Boolean allowCopy;

    /** allow degraded printing. */
    private Boolean allowDegradedPrinting;

    /** allow printing. */
    private Boolean allowPrinting;

    /** allow screen readers. */
    private Boolean allowScreenReaders;

    /** userpassword. */
    private String userpassword;

    /** ownerpassword. */
    private String ownerpassword;

    /** author. */
    private String author;

    /** creator. */
    private String creator;

    /** title. */
    private String title;

    /** subject. */
    private String subject;

    /** keywords. */
    private String keywords;

    /**
     * Construit un objet ParametrageVueJasperPdf et lui affecte les paramètres par défaut.
     */
    public ParametrageVueJasperPdf()
    {
        super();
        this.formatEdition = DEF_FORMAT_EDITION_VALUE;
        this.characterEncoding = DEF_CHARACTER_ENCODING;
        this.extension = DEF_EXTENSION;
        this.contentType = DEF_CONTENT_TYPE;
        this.typeMimeEdition = DEF_MIME_TYPE;

        // Spécifique pdf
        this.pdfVersion = DEF_PDF_VERSION;
        this.compressed = DEf_COMPRESSED;
        this.creatingBatchModeBookmarks = DEF_CRE_BATCH_MODE_BOOKMARKS;
        this.forceLinebreakPolicy = DEF_FORCE_LINEBREAK_POLICY;
        this.allowAssembly = DEF_ALLOW_ASSEMBLY;
        this.allowCopy = DEF_ALLOW_COPY;
        this.allowDegradedPrinting = DEF_ALLOW_DEGRADED_PRINTING;
        this.allowPrinting = DEF_ALLOW_PRINTING;
        this.allowScreenReaders = DEF_ALLOW_SCREEN_READERS;
        this.encrypted = DEF_ENCRYPTED;
        this.centVingtHuitBitKey = DEF_CENTVINGTHUITBITKEY;
    }

    /**
     * Accesseur de l attribut allow assembly.
     * 
     * @return allow assembly
     */
    public Boolean getAllowAssembly()
    {
        return allowAssembly;
    }

    /**
     * Accesseur de l attribut allow copy.
     * 
     * @return allow copy
     */
    public Boolean getAllowCopy()
    {
        return allowCopy;
    }

    /**
     * Accesseur de l attribut allow degraded printing.
     * 
     * @return allow degraded printing
     */
    public Boolean getAllowDegradedPrinting()
    {
        return allowDegradedPrinting;
    }

    /**
     * Accesseur de l attribut allow printing.
     * 
     * @return allow printing
     */
    public Boolean getAllowPrinting()
    {
        return allowPrinting;
    }

    /**
     * Accesseur de l attribut allow screen readers.
     * 
     * @return allow screen readers
     */
    public Boolean getAllowScreenReaders()
    {
        return allowScreenReaders;
    }

    /**
     * Accesseur de l attribut author.
     * 
     * @return author
     */
    public String getAuthor()
    {
        return author;
    }

    /**
     * Accesseur de l attribut cent vingt huit bit key.
     * 
     * @return cent vingt huit bit key
     */
    public Boolean getCentVingtHuitBitKey()
    {
        return centVingtHuitBitKey;
    }

    /**
     * Accesseur de l attribut compressed.
     * 
     * @return compressed
     */
    public Boolean getCompressed()
    {
        return compressed;
    }

    /**
     * Accesseur de l attribut creating batch mode bookmarks.
     * 
     * @return creating batch mode bookmarks
     */
    public Boolean getCreatingBatchModeBookmarks()
    {
        return creatingBatchModeBookmarks;
    }

    /**
     * Accesseur de l attribut creator.
     * 
     * @return creator
     */
    public String getCreator()
    {
        return creator;
    }

    /**
     * Accesseur de l attribut encrypted.
     * 
     * @return encrypted
     */
    public Boolean getEncrypted()
    {
        return encrypted;
    }

    /**
     * Accesseur de l attribut force linebreak policy.
     * 
     * @return force linebreak policy
     */
    public Boolean getForceLinebreakPolicy()
    {
        return forceLinebreakPolicy;
    }

    /**
     * Accesseur de l attribut keywords.
     * 
     * @return keywords
     */
    public String getKeywords()
    {
        return keywords;
    }

    /**
     * Accesseur de l attribut ownerpassword.
     * 
     * @return ownerpassword
     */
    public String getOwnerpassword()
    {
        return ownerpassword;
    }

    /**
     * Accesseur de l attribut pdf version.
     * 
     * @return pdf version
     */
    public Character getPdfVersion()
    {
        return pdfVersion;
    }

    /**
     * Accesseur de l attribut subject.
     * 
     * @return subject
     */
    public String getSubject()
    {
        return subject;
    }

    /**
     * Accesseur de l attribut title.
     * 
     * @return title
     */
    public String getTitle()
    {
        return title;
    }

    /**
     * Accesseur de l attribut userpassword.
     * 
     * @return userpassword
     */
    public String getUserpassword()
    {
        return userpassword;
    }

    /**
     * Produit une Map contenant le paramétrage de l'exporteur Jasper.
     *
     * @return le paramétrage de l'exporteur Jasper.
     */ 
    @Override
    public Map<String, Object> parametrageJRExporter()
    {
        // Déclaration d'une Map qui contiendra le paramétrage
        Map<String, Object> mapModele = new HashMap<>();

        // Paramétrage général de l'exporteur Jasper
        putIfNotNull(mapModele, CHARACTER_ENCODING, this.characterEncoding);

        // Paramétrage spécifique PDF
        putIfNotNull(mapModele, PDF_VERSION, this.pdfVersion);
        putIfNotNull(mapModele, IS_COMPRESSED, this.compressed);
        putIfNotNull(mapModele, IS_CREA_BATCH_MODE_BOOKMAR, this.creatingBatchModeBookmarks);
        putIfNotNull(mapModele, FORCE_LINEBREAK_POLICY, this.forceLinebreakPolicy);

        int permission =
            (this.allowAssembly.booleanValue() ? PdfWriter.AllowAssembly : 0)
                | (this.allowCopy.booleanValue() ? PdfWriter.AllowCopy : 0)
                | (this.allowDegradedPrinting.booleanValue() ? PdfWriter.AllowDegradedPrinting : 0)
                | (this.allowPrinting.booleanValue() ? PdfWriter.AllowPrinting : 0)
                | (this.allowScreenReaders.booleanValue() ? PdfWriter.AllowScreenReaders : 0);

        putIfNotNull(mapModele, PERMISSIONS, Integer.valueOf (permission));
        putIfNotNull(mapModele, IS_ENCRYPTED, this.encrypted);
        putIfNotNull(mapModele, IS_128_BIT_KEY, this.centVingtHuitBitKey);
        putIfNotNull(mapModele, USER_PASSWORD, this.userpassword);
        putIfNotNull(mapModele, OWNER_PASSWORD, this.ownerpassword);
        putIfNotNull(mapModele, METADATA_AUTHOR, this.author);
        putIfNotNull(mapModele, METADATA_CREATOR, this.creator);
        putIfNotNull(mapModele, METADATA_TITLE, this.title);
        putIfNotNull(mapModele, METADATA_SUBJECT, this.subject);
        putIfNotNull(mapModele, METADATA_KEYWORDS, this.keywords);

        // Journalisation du paramétrage
        journaliserParametrageJRExporter(mapModele);
        
        return mapModele;
    }

    /**
     * Produit une Map contenant le paramétrage de la vue jasper en utilisant : - soit les valeurs par défaut, - soit
     * les valeurs contenues dans le propriétés de l'objet ParametrageVueJasperPdf (valeurs injectée dans le fichier de
     * configuration des éditions).
     * 
     * @return le paramétrage de la vue Jasper
     */
    @Override
    public Map<String, String> parametrerVue()
    {
        // Déclaration d'une Map qui contiendra le paramétrage
        Map<String, String> mapModele = new HashMap<>();

        // Initialisation du paramétrage
        putIfNotNull(mapModele, REPORT_VIEW_KEY, this.formatEdition);
        putIfNotNull(mapModele, EXTENSION_KEY, this.extension);
        putIfNotNull(mapModele, CONTENT_TYPE_KEY, this.contentType);
        putIfNotNull(mapModele, MIME_TYPE_KEY, this.typeMimeEdition);

        // Journalisation du paramétrage
        journaliserParametrageVue(mapModele);

        return mapModele;
    }

    /**
     * Modificateur de l attribut allow assembly.
     * 
     * @param allowAssembly le nouveau allow assembly
     */
    public void setAllowAssembly(Boolean allowAssembly)
    {
        this.allowAssembly = allowAssembly;
    }

    /**
     * Modificateur de l attribut allow copy.
     * 
     * @param allowCopy le nouveau allow copy
     */
    public void setAllowCopy(Boolean allowCopy)
    {
        this.allowCopy = allowCopy;
    }

    /**
     * Modificateur de l attribut allow degraded printing.
     * 
     * @param allowDegradedPrinting le nouveau allow degraded printing
     */
    public void setAllowDegradedPrinting(Boolean allowDegradedPrinting)
    {
        this.allowDegradedPrinting = allowDegradedPrinting;
    }

    /**
     * Modificateur de l attribut allow printing.
     * 
     * @param allowPrinting le nouveau allow printing
     */
    public void setAllowPrinting(Boolean allowPrinting)
    {
        this.allowPrinting = allowPrinting;
    }

    /**
     * Modificateur de l attribut allow screen readers.
     * 
     * @param allowScreenReaders le nouveau allow screen readers
     */
    public void setAllowScreenReaders(Boolean allowScreenReaders)
    {
        this.allowScreenReaders = allowScreenReaders;
    }

    /**
     * Modificateur de l attribut author.
     * 
     * @param author le nouveau author
     */
    public void setAuthor(String author)
    {
        this.author = author;
    }

    /**
     * Modificateur de l attribut cent vingt huit bit key.
     * 
     * @param centVingtHuitBitKey le nouveau cent vingt huit bit key
     */
    public void setCentVingtHuitBitKey(Boolean centVingtHuitBitKey)
    {
        this.centVingtHuitBitKey = centVingtHuitBitKey;
    }

    /**
     * Modificateur de l attribut compressed.
     * 
     * @param compressed le nouveau compressed
     */
    public void setCompressed(Boolean compressed)
    {
        this.compressed = compressed;
    }

    /**
     * Modificateur de l attribut creating batch mode bookmarks.
     * 
     * @param creatingBatchModeBookmarks le nouveau creating batch mode bookmarks
     */
    public void setCreatingBatchModeBookmarks(Boolean creatingBatchModeBookmarks)
    {
        this.creatingBatchModeBookmarks = creatingBatchModeBookmarks;
    }

    /**
     * Modificateur de l attribut creator.
     * 
     * @param creator le nouveau creator
     */
    public void setCreator(String creator)
    {
        this.creator = creator;
    }

    /**
     * Modificateur de l attribut encrypted.
     * 
     * @param encrypted le nouveau encrypted
     */
    public void setEncrypted(Boolean encrypted)
    {
        this.encrypted = encrypted;
    }

    /**
     * Modificateur de l attribut force linebreak policy.
     * 
     * @param forceLinebreakPolicy le nouveau force linebreak policy
     */
    public void setForceLinebreakPolicy(Boolean forceLinebreakPolicy)
    {
        this.forceLinebreakPolicy = forceLinebreakPolicy;
    }

    /**
     * Modificateur de l attribut keywords.
     * 
     * @param keywords le nouveau keywords
     */
    public void setKeywords(String keywords)
    {
        this.keywords = keywords;
    }

    /**
     * Modificateur de l attribut ownerpassword.
     * 
     * @param ownerpassword le nouveau ownerpassword
     */
    public void setOwnerpassword(String ownerpassword)
    {
        this.ownerpassword = ownerpassword;
    }

    /**
     * Modificateur de l attribut pdf version.
     * 
     * @param pdfVersion le nouveau pdf version
     */
    public void setPdfVersion(Character pdfVersion)
    {
        this.pdfVersion = pdfVersion;
    }

    /**
     * Modificateur de l attribut subject.
     * 
     * @param subject le nouveau subject
     */
    public void setSubject(String subject)
    {
        this.subject = subject;
    }

    /**
     * Modificateur de l attribut title.
     * 
     * @param title le nouveau title
     */
    public void setTitle(String title)
    {
        this.title = title;
    }

    /**
     * Modificateur de l attribut userpassword.
     * 
     * @param userpassword le nouveau userpassword
     */
    public void setUserpassword(String userpassword)
    {
        this.userpassword = userpassword;
    }

}
