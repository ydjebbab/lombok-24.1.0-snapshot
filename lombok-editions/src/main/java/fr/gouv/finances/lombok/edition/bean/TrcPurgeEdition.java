/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : TrcPurgeEdition.java
 *
 */
package fr.gouv.finances.lombok.edition.bean;

import java.sql.Timestamp;
import java.util.Date;

import fr.gouv.finances.lombok.util.base.BaseBean;
import fr.gouv.finances.lombok.util.exception.RegleGestionException;
import fr.gouv.finances.lombok.util.format.FormaterDate;

/**
 * Class TrcPurgeEdition --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.3 $ Date: 11 déc. 2009
 */
public class TrcPurgeEdition extends BaseBean
{

    /** Constant : EN_ATTENTE_DEMARRAGE. */
    public static final String EN_ATTENTE_DEMARRAGE = "EN ATTENTE DEMARRAGE";

    /** Constant : EN_COURS_EXECUTION. */
    public static final String EN_COURS_EXECUTION = "EN COURS";

    /** Constant : TERMINE_SANS_ERREUR. */
    public static final String TERMINE_SANS_ERREUR = "TERMINE SANS ERREUR";

    /** Constant : TERMINE_AVEC_ERREUR. */
    public static final String TERMINE_AVEC_ERREUR = "TERMINE AVEC ERREUR";

    /** Constant : ARRET_DEMANDE. */
    public static final String ARRET_DEMANDE = "ARRET DE LA PURGE DEMANDE";

    /** Constant : PURGE_INTERROMPUE. */
    public static final String PURGE_INTERROMPUE = "PURGE INTERROMPUE";

    /** Constant : AUTRE_PURGE_EN_COURS. */
    public static final String AUTRE_PURGE_EN_COURS = "UNE AUTRE PURGE EST ACTIVE";

    /** Constant : MSG_AUTRE_PURGE_EN_COURS. */
    public static final String MSG_AUTRE_PURGE_EN_COURS =
        "UNE AUTRE TACHE DE PURGE DATANT DE MOINS DE 24H EST EN COURS D'EXECUTION";

    /** Constant : MSG_PURGE_INTERROMPUE_ADMINISTRATEUR. */
    public static final String MSG_PURGE_INTERROMPUE_ADMINISTRATEUR =
        "LA TACHE DE PURGE A ETE INTERROMPUE SUITE A UNE DEMANDE D'ARRET DE L'ADMINISTRATEUR";

    /** Constant : MSG_PURGE_INTERROMPUE_AUTRE. */
    public static final String MSG_PURGE_INTERROMPUE_AUTRE =
        "LA TACHE DE PURGE A ETE INTERROMPUE SUITE A UN EVENEMENT INATTENDU";

    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** Constant : MAX_VARCHAR_LENGTH. */
    private static final int MAX_VARCHAR_LENGTH = 4000;

    /** Identifiant technique utilisé par Hibernate. */
    private Long id;

    /** Flag utilisé pour la gestion du verrouillage optimiste par Hibernate. */
    private int version;

    /** Instant correspondant au début de la purge. */
    private Date dateDebutPurge;

    /** Instant correspondant à la fin de la purge. */
    private Date dateFinPurge;

    /** Etat du traitement de purge. */
    private String etatPurge;

    /** Message d'erreur si le traitement de purge ne s'est pas terminé correctement. */
    private String messageErreur;

    /** Nombre d'éditions supprimées. */
    private Long nbEditionsSupprimees;

    /** Durée d'exécution du traitement de putge. */
    private Long dureeExecution;

    /** Application concernée par l'opération de purge. */
    private String applicationOrigine;

    /**
     * Instanciation de trc purge edition.
     */
    public TrcPurgeEdition()
    {
        super();
    }

    /**
     * Enregistre une tache de purge en la plaçant dans l'etat EN_ATTENTE_DEMARRAGE.
     * 
     * @param applicationOrigine --
     * @return the trc purge edition
     */
    public static TrcPurgeEdition creationTachePurgeEnAttenteDemarrage(String applicationOrigine)
    {
        TrcPurgeEdition trcPurgeEdition = new TrcPurgeEdition();
        trcPurgeEdition.applicationOrigine = applicationOrigine;
        trcPurgeEdition.etatPurge = TrcPurgeEdition.EN_ATTENTE_DEMARRAGE;
        trcPurgeEdition.dateDebutPurge = new Timestamp((new Date()).getTime());

        return trcPurgeEdition;
    }

    /**
     * methode Demande arret : --.
     */
    public void demandeArret()
    {
        this.etatPurge = TrcPurgeEdition.ARRET_DEMANDE;
    }

    /**
     * Place une tache de purge dans l'état EN_COURS_EXECUTION.
     */
    public void demarreExecution()
    {
        if (this.isEnAttenteDemarrage())
        {
            this.etatPurge = TrcPurgeEdition.EN_COURS_EXECUTION;
        }
        else
        {
            throw new RegleGestionException("Une tâche de purge ne peut être placée dans l'état EN_COURS_EXECUTION"
                + " que si elle est dans l'état EN_ATTENTE_DEMARRAGE");
        }
    }

    /**
     * Enregistre la durée et le nombre d'éléments purgés par une tache de purge.
     * 
     * @param compteur --
     */
    public void enregistreDureeEtNbEltsPurges(long compteur)
    {
        this.dateFinPurge = new Timestamp((new Date()).getTime());
        long duree = this.dateFinPurge.getTime() - this.dateDebutPurge.getTime();
        duree = (duree / 1000) / 60;
        this.dureeExecution = new Long(duree);
        this.nbEditionsSupprimees = new Long(compteur);
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param obj
     * @return true, si c'est vrai
     * @see java.lang.Object#equals(java.lang.Object)
     */
    public boolean equals(Object obj)
    {

        if (this == obj)
        {
            return true;
        }
        if (obj == null)
        {
            return false;
        }
        if (getClass() != obj.getClass())
        {
            return false;
        }
        final TrcPurgeEdition other = (TrcPurgeEdition) obj;
        if (applicationOrigine == null)
        {
            if (other.applicationOrigine != null)
            {
                return false;
            }
        }
        else if (!applicationOrigine.equals(other.applicationOrigine))
        {
            return false;
        }
        if (dateDebutPurge == null)
        {
            if (other.dateDebutPurge != null)
            {
                return false;
            }
        }
        else if (!dateDebutPurge.equals(other.dateDebutPurge))
        {
            return false;
        }
        return true;
    }

    /**
     * Accesseur de l attribut application origine.
     * 
     * @return application origine
     */
    public String getApplicationOrigine()
    {
        return applicationOrigine;
    }

    /**
     * Gets the date debut purge.
     * 
     * @return the dateDebutPurge
     */
    public Date getDateDebutPurge()
    {
        return dateDebutPurge;
    }

    /**
     * Gets the date fin purge.
     * 
     * @return the dateFinPurge
     */
    public Date getDateFinPurge()
    {
        return dateFinPurge;
    }

    /**
     * Gets the duree execution.
     * 
     * @return the dureeExecution
     */
    public Long getDureeExecution()
    {
        return dureeExecution;
    }

    /**
     * Gets the etat purge.
     * 
     * @return the etatPurge
     */
    public String getEtatPurge()
    {
        return etatPurge;
    }

    /**
     * Gets the id.
     * 
     * @return the id
     */
    public Long getId()
    {
        return id;
    }

    /**
     * Gets the message erreur.
     * 
     * @return the messageErreur
     */
    public String getMessageErreur()
    {
        return messageErreur;
    }

    /**
     * Gets the nb editions supprimees.
     * 
     * @return the nbEditionsSupprimees
     */
    public Long getNbEditionsSupprimees()
    {
        return nbEditionsSupprimees;
    }

    /**
     * Gets the version.
     * 
     * @return the version
     */
    public int getVersion()
    {
        return version;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @return int
     * @see java.lang.Object#hashCode()
     */
    public int hashCode()
    {
        final int PRIME = 31;
        int result = 1;

        result = PRIME * result + ((applicationOrigine == null) ? 0 : applicationOrigine.hashCode());
        result = PRIME * result + ((dateDebutPurge == null) ? 0 : dateDebutPurge.hashCode());
        return result;
    }

    /**
     * methode Interrompre suite demande administrateur : --.
     */
    public void interrompreSuiteDemandeAdministrateur()
    {
        if (this.isArretDemande())
        {
            this.etatPurge = TrcPurgeEdition.PURGE_INTERROMPUE;
            this.messageErreur = TrcPurgeEdition.MSG_PURGE_INTERROMPUE_ADMINISTRATEUR;
        }
        else
        {
            throw new RegleGestionException(
                "Une tâche de purge ne peut être interrompue que si elle est dans l'état ARRET_DEMANDE");
        }
    }

    /**
     * methode Interrompre suite evt inattendu : --.
     */
    public void interrompreSuiteEvtInattendu()
    {
        if (this.isArretDemande())
        {
            this.etatPurge = TrcPurgeEdition.PURGE_INTERROMPUE;
            this.messageErreur = TrcPurgeEdition.MSG_PURGE_INTERROMPUE_AUTRE;
        }
        else
        {
            throw new RegleGestionException(
                "Une tâche de purge ne peut être interrompue que si elle est dans l'état ARRET_DEMANDE");
        }
    }

    /**
     * Retourne vrai si l'objet trcPurgeEdition est dans l'état ARRET_DEMANDE.
     * 
     * @return true, if checks if is arret demande
     */
    public boolean isArretDemande()
    {
        return TrcPurgeEdition.ARRET_DEMANDE.equals(this.etatPurge);
    }

    /**
     * Retourne vrai si l'objet trcPurgeEdition est dans l'état EN_ATTENTE_DEMARRAGE.
     * 
     * @return true, if checks if is en attente demarrage
     */
    public boolean isEnAttenteDemarrage()
    {
        return TrcPurgeEdition.EN_ATTENTE_DEMARRAGE.equals(this.etatPurge);
    }

    /**
     * Retourne vrai si la tache de purge dans l'état EN_COURS_EXECUTION.
     * 
     * @return true, if checks if is en cours execution
     */
    public boolean isEnCoursExecution()
    {
        return TrcPurgeEdition.EN_COURS_EXECUTION.equals(this.etatPurge);
    }

    /**
     * Retourne vrai si l'objet trcPurgeEdition est dans l'état PURGE_INTERROMPUE.
     * 
     * @return true, if checks if is purge interrompue
     */
    public boolean isPurgeInterrompue()
    {
        return TrcPurgeEdition.PURGE_INTERROMPUE.equals(this.etatPurge);
    }

    /**
     * Retourne vrai si l'objet trcPurgeEdition est dans l'état TERMINE_AVEC_ERREUR.
     * 
     * @return true, if checks if is termine avec erreur
     */
    public boolean isTermineAvecErreur()
    {
        return TrcPurgeEdition.TERMINE_AVEC_ERREUR.equals(this.etatPurge);
    }

    /**
     * Retourne vrai si l'objet trcPurgeEdition est dans l'état TERMINE_SANS_ERREUR.
     * 
     * @return true, if checks if is termine sans erreur
     */
    public boolean isTermineSansErreur()
    {
        return TrcPurgeEdition.TERMINE_SANS_ERREUR.equals(this.etatPurge);
    }

    /**
     * methode Ne pas demarrer execution : --.
     */
    public void nePasDemarrerExecution()
    {
        this.etatPurge = TrcPurgeEdition.AUTRE_PURGE_EN_COURS;
        this.messageErreur = TrcPurgeEdition.MSG_AUTRE_PURGE_EN_COURS;
    }

    /**
     * Modificateur de l attribut application origine.
     * 
     * @param applicationOrigine le nouveau application origine
     */
    public void setApplicationOrigine(String applicationOrigine)
    {
        this.applicationOrigine = applicationOrigine;
    }

    /**
     * Sets the date debut purge.
     * 
     * @param dateDebutPurge the dateDebutPurge to set
     */
    public void setDateDebutPurge(Date dateDebutPurge)
    {
        this.dateDebutPurge = dateDebutPurge;
    }

    /**
     * Sets the date fin purge.
     * 
     * @param dateFinPurge the dateFinPurge to set
     */
    public void setDateFinPurge(Date dateFinPurge)
    {
        this.dateFinPurge = dateFinPurge;
    }

    /**
     * Sets the duree execution.
     * 
     * @param dureeExecution the dureeExecution to set
     */
    public void setDureeExecution(Long dureeExecution)
    {
        this.dureeExecution = dureeExecution;
    }

    /**
     * Sets the etat purge.
     * 
     * @param etatPurge the etatPurge to set
     */
    public void setEtatPurge(String etatPurge)
    {
        this.etatPurge = etatPurge;
    }

    /**
     * Sets the id.
     * 
     * @param id the id to set
     */
    public void setId(Long id)
    {
        this.id = id;
    }

    /**
     * Sets the message erreur.
     * 
     * @param messageErreur the messageErreur to set
     */
    public void setMessageErreur(String messageErreur)
    {
        this.messageErreur = messageErreur;
    }

    /**
     * Sets the nb editions supprimees.
     * 
     * @param nbEditionsSupprimees the nbEditionsSupprimees to set
     */
    public void setNbEditionsSupprimees(Long nbEditionsSupprimees)
    {
        this.nbEditionsSupprimees = nbEditionsSupprimees;
    }

    /**
     * Sets the version.
     * 
     * @param version the version to set
     */
    public void setVersion(int version)
    {
        this.version = version;
    }

    /**
     * Enregistre la fin d'une tache de purge avec erreur.
     * 
     * @param msg --
     */
    public void termineAvecErreur(String msg)
    {
        if (this.isEnCoursExecution() || this.isArretDemande() || this.isEnAttenteDemarrage()
            || this.isPurgeInterrompue())
        {
            this.etatPurge = TrcPurgeEdition.TERMINE_AVEC_ERREUR;
            if (msg != null)
            {
                String messageTronque =
                    (msg.length() <= MAX_VARCHAR_LENGTH ? msg : msg.substring(0, MAX_VARCHAR_LENGTH - 1));
                this.messageErreur = messageTronque;
            }
        }
        else
        {
            throw new RegleGestionException("Une tâche de purge ne peut être 'terminée avec erreur' que si elle est "
                + "dans un des états EN_COURS_EXECUTION, ARRET_DEMANDE,EN_ATTENTE_DEMARRAGE, PURGE_INTERROMPUE");
        }
    }

    /**
     * Enregistre la fin d'une tache de purge sans erreur.
     */
    public void termineSansErreur()
    {
        if (this.isEnCoursExecution() || this.isArretDemande() || this.isPurgeInterrompue())
        {
            this.etatPurge = TrcPurgeEdition.TERMINE_SANS_ERREUR;
        }
        else
        {
            throw new RegleGestionException("Une tâche de purge ne peut être 'terminée sans erreur' "
                + "que si elle est dans un des états EN_COURS_EXECUTION, ARRET_DEMANDE, PURGE_INTERROMPUE");
        }
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @return string
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        StringBuilder tos = new StringBuilder();
        tos.append("applicationOrigine :");
        tos.append(this.applicationOrigine);
        tos.append('\n');
        tos.append("dateDebutPurge :");
        tos.append(FormaterDate.formatDateHeureMinuteSecondeMillisecondeCourt(this.dateDebutPurge));
        tos.append('\n');
        tos.append("dateFinPurge :");
        tos.append(FormaterDate.formatDateHeureMinuteSecondeMillisecondeCourt(this.dateFinPurge));
        tos.append('\n');
        tos.append("etatPurge :");
        tos.append(this.etatPurge);
        tos.append('\n');
        tos.append("dureeExecution :");
        tos.append(this.dureeExecution);
        tos.append('\n');
        tos.append("nbEditionsSupprimees :");
        tos.append(this.nbEditionsSupprimees);
        tos.append('\n');
        tos.append("messageErreur :");
        tos.append(this.messageErreur);
        tos.append('\n');
        return tos.toString();
    }

}
