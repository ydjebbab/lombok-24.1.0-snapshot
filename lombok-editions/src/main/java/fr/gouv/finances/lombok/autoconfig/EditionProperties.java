package fr.gouv.finances.lombok.autoconfig;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author celinio fernandes Date: Feb 25, 2020
 */
@ConfigurationProperties(prefix = "lombok.composant.edition")
public class EditionProperties  extends LombokComposantProperties
{

    @Override
    public String toString()
    {
        return "EditionProperties [inclus=" + inclus + "]";
    }

}