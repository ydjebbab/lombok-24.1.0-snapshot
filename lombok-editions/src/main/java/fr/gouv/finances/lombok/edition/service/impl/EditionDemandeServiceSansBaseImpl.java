/*
 * Copyright (c) 2016 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.edition.service.impl;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import fr.gouv.finances.lombok.edition.bean.EditionAsynchroneNonNotifieeParMail;
import fr.gouv.finances.lombok.edition.bean.EditionAsynchroneNotifieeParMail;
import fr.gouv.finances.lombok.edition.bean.EditionSynchroneNonNotifieeParMail;
import fr.gouv.finances.lombok.edition.bean.EditionSynchroneNotifieeParMail;
import fr.gouv.finances.lombok.edition.bean.JobHistory;
import fr.gouv.finances.lombok.edition.service.EditionDemandeService;
import fr.gouv.finances.lombok.edition.service.EditionProducerService;
import fr.gouv.finances.lombok.securite.techbean.PersonneAnnuaire;
import fr.gouv.finances.lombok.upload.bean.FichierJoint;
import fr.gouv.finances.lombok.util.base.BaseServiceImpl;

/**
 * Remplace la classe fr.gouv.finances.lombok.edition.service.impl.EditionDemandeServiceImpl pour ne plus avoir de lien
 * avec le schéma jasper
 */
public class EditionDemandeServiceSansBaseImpl extends BaseServiceImpl implements EditionDemandeService
{

    /** Service de production des éditions (synchrone). */
    private EditionProducerService editionproducerserviceso;

    /**
     * Constructeur
     */
    public EditionDemandeServiceSansBaseImpl()
    {
        super();
    }

    /**
     * Gets the service de production des éditions (synchrone).
     *
     * @return the service de production des éditions (synchrone)
     */
    public EditionProducerService getEditionproducerserviceso()
    {
        return editionproducerserviceso;
    }

    /**
     * Sets the service de production des éditions (synchrone).
     *
     * @param editionproducerserviceso the new service de production des éditions (synchrone)
     */
    public void setEditionproducerserviceso(EditionProducerService editionproducerserviceso)
    {
        this.editionproducerserviceso = editionproducerserviceso;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.edition.service.EditionDemandeService#declencherEdition(fr.gouv.finances.lombok.edition.bean.EditionAsynchroneNonNotifieeParMail,
     *      java.util.Map)
     */
    @Override
    public EditionAsynchroneNonNotifieeParMail declencherEdition(EditionAsynchroneNonNotifieeParMail editAsynchNonNotifieeParMail,
        Map parametresEdition)
    {
        return null;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.edition.service.EditionDemandeService#declencherEdition(fr.gouv.finances.lombok.edition.bean.EditionAsynchroneNotifieeParMail,
     *      java.util.Map)
     */
    @Override
    public EditionAsynchroneNotifieeParMail declencherEdition(EditionAsynchroneNotifieeParMail editAsynchNotifieeParMail,
        Map parametresEdition)
    {
        return null;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.edition.service.EditionDemandeService#declencherEdition(fr.gouv.finances.lombok.edition.bean.EditionSynchroneNonNotifieeParMail,
     *      java.util.Map)
     */
    @Override
    public EditionSynchroneNonNotifieeParMail declencherEdition(EditionSynchroneNonNotifieeParMail editSynchNonNotifieeParMail,
        Map parametresEdition)
    {
        return null;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.edition.service.EditionDemandeService#declencherEdition(fr.gouv.finances.lombok.edition.bean.EditionSynchroneNotifieeParMail,
     *      java.util.Map)
     */
    @Override
    public EditionSynchroneNotifieeParMail declencherEdition(EditionSynchroneNotifieeParMail editSynchNotifieeParMail,
        Map parametresEdition)
    {
        return null;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.edition.service.EditionDemandeService#declencherEditionCompressee(fr.gouv.finances.lombok.edition.bean.EditionAsynchroneNonNotifieeParMail,
     *      java.util.Map)
     */
    @Override
    public EditionAsynchroneNonNotifieeParMail declencherEditionCompressee(
        EditionAsynchroneNonNotifieeParMail editAsynchNonNotifieeParMail, Map parametresEdition)
    {
        return null;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.edition.service.EditionDemandeService#declencherEditionCompressee(fr.gouv.finances.lombok.edition.bean.EditionAsynchroneNotifieeParMail,
     *      java.util.Map)
     */
    @Override
    public EditionAsynchroneNotifieeParMail declencherEditionCompressee(EditionAsynchroneNotifieeParMail editAsynchNotifieeParMail,
        Map parametresEdition)
    {
        return null;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.edition.service.EditionDemandeService#declencherEditionCompressee(fr.gouv.finances.lombok.edition.bean.EditionSynchroneNonNotifieeParMail,
     *      java.util.Map)
     */
    @Override
    public EditionSynchroneNonNotifieeParMail declencherEditionCompressee(EditionSynchroneNonNotifieeParMail editSynchNonNotifieeParMail,
        Map parametresEdition)
    {
        return null;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.edition.service.EditionDemandeService#declencherEditionCompressee(fr.gouv.finances.lombok.edition.bean.EditionSynchroneNotifieeParMail,
     *      java.util.Map)
     */
    @Override
    public EditionSynchroneNotifieeParMail declencherEditionCompressee(EditionSynchroneNotifieeParMail editSynchNotifieeParMail,
        Map parametresEdition)
    {
        return null;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.edition.service.EditionDemandeService#declencherProductionEditionCompresseeNonStockee(java.lang.String,
     *      java.util.Map)
     */
    @Override
    public FichierJoint declencherProductionEditionCompresseeNonStockee(String editionBeanId, Map parametresEdition)
    {
        return null;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.edition.service.EditionDemandeService#declencherProductionEditionNonStockee(java.lang.String,
     *      java.util.Map)
     */
    @Override
    public FichierJoint declencherProductionEditionNonStockee(String editionBeanId, Map parametresEdition)
    {
        return editionproducerserviceso.creerEdition(editionBeanId, parametresEdition);
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.edition.service.EditionDemandeService#declencherProductionHtmlEditionNonStockee(java.lang.String,
     *      java.util.Map)
     */
    @Override
    public Map declencherProductionHtmlEditionNonStockee(String editionBeanId, Map parametresEdition)
    {
        return null;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.edition.service.EditionDemandeService#initEditionAsynchroneNonNotifieeParMail(java.lang.String,
     *      java.lang.String)
     */
    @Override
    public EditionAsynchroneNonNotifieeParMail initEditionAsynchroneNonNotifieeParMail(String beanIdEdition, String uidProprietaire)
    {
        return null;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.edition.service.EditionDemandeService#initEditionAsynchroneNotifieeParMail(java.lang.String,
     *      java.lang.String, java.lang.String)
     */
    @Override
    public EditionAsynchroneNotifieeParMail initEditionAsynchroneNotifieeParMail(String beanIdEdition, String uidProprietaire,
        String mailDestinataire)
    {
        return null;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.edition.service.EditionDemandeService#initEditionSynchroneNonNotifieeParMail(java.lang.String,
     *      java.lang.String)
     */
    @Override
    public EditionSynchroneNonNotifieeParMail initEditionSynchroneNonNotifieeParMail(String beanIdEdition, String uidProprietaire)
    {
        return null;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.edition.service.EditionDemandeService#initEditionSynchroneNotifieeParMail(java.lang.String,
     *      java.lang.String, java.lang.String)
     */
    @Override
    public EditionSynchroneNotifieeParMail initEditionSynchroneNotifieeParMail(String beanIdEdition, String uidProprietaire,
        String mailDestinataire)
    {
        return null;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.edition.service.EditionDemandeService#initMessageMailPourAccesAvecOuSansPortailIntranet(fr.gouv.finances.lombok.edition.bean.JobHistory)
     */
    @Override
    public void initMessageMailPourAccesAvecOuSansPortailIntranet(JobHistory jobHistory)
    {

    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.edition.service.EditionDemandeService#initMessageMailPourAccesViaPortailIntranetOuInternet(fr.gouv.finances.lombok.edition.bean.JobHistory)
     */
    @Override
    public void initMessageMailPourAccesViaPortailIntranetOuInternet(JobHistory jobHistory)
    {

    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.edition.service.EditionDemandeService#purgeEtSynchronise()
     */
    @Override
    public void purgeEtSynchronise()
    {

    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.edition.service.EditionDemandeService#rechercheNouvellesEditionsAccessiblesAUnUtilisateur(fr.gouv.finances.lombok.securite.techbean.PersonneAnnuaire)
     */
    @Override
    public List rechercheNouvellesEditionsAccessiblesAUnUtilisateur(PersonneAnnuaire personne)
    {
        return null;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.edition.service.EditionDemandeService#rechercherEdition(fr.gouv.finances.lombok.edition.bean.JobHistory)
     */
    @Override
    public FichierJoint rechercherEdition(JobHistory jobHistory)
    {
        return null;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.edition.service.EditionDemandeService#rechercherEdition(fr.gouv.finances.lombok.edition.bean.JobHistory,
     *      java.lang.String)
     */
    @Override
    public FichierJoint rechercherEdition(JobHistory jobHistory, String uid)
    {
        return null;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.edition.service.EditionDemandeService#rechercherEditionParUuid(java.lang.String)
     */
    @Override
    public JobHistory rechercherEditionParUuid(String editionUuid)
    {
        return null;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.edition.service.EditionDemandeService#rechercherLaDerniereEditionParBeanIdEtParAppliOrigine(java.lang.String,
     *      java.lang.String)
     */
    @Override
    public JobHistory rechercherLaDerniereEditionParBeanIdEtParAppliOrigine(String beanEditionId, String appliOrigine)
    {
        return null;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.edition.service.EditionDemandeService#rechercherLaDerniereEditionParBeanIdEtParAppliOrigineEtParUtilisateur(java.lang.String,
     *      java.lang.String, java.lang.String)
     */
    @Override
    public JobHistory rechercherLaDerniereEditionParBeanIdEtParAppliOrigineEtParUtilisateur(String beanEditionId, String appliOrigine,
        String utilisateur)
    {
        return null;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.edition.service.EditionDemandeService#rechercheToutesEditionsAccessiblesAUnUtilisateur(fr.gouv.finances.lombok.securite.techbean.PersonneAnnuaire)
     */
    @Override
    public List rechercheToutesEditionsAccessiblesAUnUtilisateur(PersonneAnnuaire personne)
    {
        return null;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.edition.service.EditionDemandeService#supprimerEditions(java.util.Collection)
     */
    @Override
    public Collection<JobHistory> supprimerEditions(Collection<JobHistory> elementsASupprimer)
    {
        return null;
    }

}
