/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : EditionsJasperForm.java
 *
 */
package fr.gouv.finances.lombok.edition.mvc.form;

import java.io.Serializable;

import fr.gouv.finances.lombok.edition.bean.JobHistory;
import fr.gouv.finances.lombok.upload.bean.FichierJoint;

/**
 * Class EditionsJasperForm --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.2 $ Date: 11 déc. 2009
 */
public class EditionsJasperForm implements Serializable
{

    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** job history. */
    private JobHistory jobHistory;

    /** fichier joint. */
    private FichierJoint fichierJoint;

    /**
     * Instanciation de editions jasper form.
     */
    public EditionsJasperForm()
    {
        super();
    }

    /**
     * Accesseur de l attribut fichier joint.
     * 
     * @return fichier joint
     */
    public FichierJoint getFichierJoint()
    {
        return fichierJoint;
    }

    /**
     * Accesseur de l attribut job history.
     * 
     * @return job history
     */
    public JobHistory getJobHistory()
    {
        return jobHistory;
    }

    /**
     * Modificateur de l attribut fichier joint.
     * 
     * @param fichierJoint le nouveau fichier joint
     */
    public void setFichierJoint(FichierJoint fichierJoint)
    {
        this.fichierJoint = fichierJoint;
    }

    /**
     * Modificateur de l attribut job history.
     * 
     * @param jobHistory le nouveau job history
     */
    public void setJobHistory(JobHistory jobHistory)
    {
        this.jobHistory = jobHistory;
    }

}
