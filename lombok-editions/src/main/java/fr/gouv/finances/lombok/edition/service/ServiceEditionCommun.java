/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : ServiceEditionCommun.java
 *
 */
package fr.gouv.finances.lombok.edition.service;

import java.util.Map;

import fr.gouv.finances.lombok.upload.bean.FichierJoint;

/**
 * Interface ServiceEditionCommun.
 * 
 * @author wpetit-cp
 * @version $Revision: 1.2 $ Date: 15 déc. 2009
 */
public interface ServiceEditionCommun
{

    /**
     * Retourne l'encodage utilisé pour produire l'édition.
     * 
     * @return the character encoding
     */
    public String getCharacterEncoding();

    /**
     * Construit une description de l'édition qui est utilisée pour la présenter à l'utilisateur dans la bannette.
     * 
     * @return the description
     */
    public String getDescription();

    /**
     * Construit une description de l'édition qui est utilisée pour la présenter à l'utilisateur dans la bannette.
     * 
     * @param parametresEdition Map contenant les paramètres de l"édition
     * @return the description
     */
    public String getDescription(Map parametresEdition);

    /**
     * Retourne l'extension utilisée dans le nom du fichier.
     * 
     * @return the extension
     */
    public String getExtension();

    /**
     * Retourne le nombre de jours de conservation de l'édition après sa production.
     * 
     * @return the nombre jours conservation
     */
    public Integer getNombreJoursConservation();

    /**
     * Retourne le type Mime de l'édition.
     * 
     * @return the type mime
     */
    public String getTypeMime();

    /**
     * Produit une édition.
     * 
     * @param parametres paramètres de l'édition
     * @return the fichier joint
     */
    public FichierJoint produireFichierEdition(final Map parametres);

    /**
     * Produit une édition HTML.
     * 
     * @param parametresEdition --
     * @return Map contenant : - le fichier de l'édition sous la clé CpJasperReportsHtmlView.FICHIER_JOINT_KEY - un
     *         objet jasperprint sous la clé CpJasperReportsHtmlView.JASPER_PRINT_KEY
     */
    public Map produitEditionHtml(Map parametresEdition);

    /**
     * Nombre de jours de conservation de l'édition.
     * 
     * @param nombreJoursConservation --
     */
    public void setNombreJoursConservation(Integer nombreJoursConservation);

    /**
     * Affecte le nom de l'édition.
     * 
     * @param nomEdition --
     */
    public void setNomEdition(String nomEdition);
}
