/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : EditionFinderService.java
 *
 */
package fr.gouv.finances.lombok.edition.service;

import java.util.Date;
import java.util.List;

import fr.gouv.finances.lombok.edition.bean.JobHistory;
import fr.gouv.finances.lombok.edition.bean.ProfilDesti;
import fr.gouv.finances.lombok.securite.techbean.PersonneAnnuaire;

/**
 * Interface EditionFinderService --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.3 $ Date: 11 déc. 2009
 */
public interface EditionFinderService
{

    /**
     * Recherche la liste des descriptions d'éditions disponibles.
     * 
     * @return the list< string>
     */
    public List<String> rechercheBeanEditionIds();

    /**
     * Recherche les nouvelles éditions accessibles à un utilisateur.
     * 
     * @param personne --
     * @return the list< job history>
     */
    public List<JobHistory> rechercheNouvellesEditionsAccessiblesAUnUtilisateur(PersonneAnnuaire personne);

    /**
     * Recherche la liste des profils des éditions produites.
     * 
     * @return the list< profil desti>
     */
    public List<ProfilDesti> rechercheProfils();

    /**
     * Recherche les éditions qui correspondent à un beanId donné.
     * 
     * @param beanId --
     * @return the list< job history>
     */
    public List<JobHistory> rechercherEditionParBeanId(String beanId);

    /**
     * Recherche les éditions dont la demande a été effectuée à une date donnée.
     * 
     * @param dateDemande --
     * @return the list< job history>
     */
    public List<JobHistory> rechercherEditionParDateDemande(Date dateDemande);

    /**
     * Recherche les éditions qui sont dans un état donné.
     * 
     * @param etat --
     * @return the list< job history>
     */
    public List<JobHistory> rechercherEditionParEtat(String etat);

    /**
     * Recherche les éditions dont la demande a été effectuée durant la période passée en paramètres.
     * 
     * @param debutPeriodeDemande --
     * @param finPeriodeDemande --
     * @return the list< job history>
     */
    public List<JobHistory> rechercherEditionParPeriodeDateDemande(Date debutPeriodeDemande, Date finPeriodeDemande);

    /**
     * Recherche toutes les éditions accessibles à un profil donné.
     * 
     * @param profil --
     * @return the list< job history>
     */
    public List<JobHistory> rechercherEditionParProfil(String profil);

    /**
     * Recherche la liste des éditions appartenant à un utilisateur identifié par son uid.
     * 
     * @param uid --
     * @return the list< job history>
     */
    public List<JobHistory> rechercherEditionParUid(String uid);

    /**
     * Recherche une édition par son editionUuid.
     * 
     * @param editionUuid --
     * @return the job history
     */
    public JobHistory rechercherEditionParUuid(String editionUuid);

    /**
     * recehrche les nbeARetourner éditions les plus longues sur l application applicationOrigine.
     * 
     * @param nbeARetourner
     * @return list
     */
    public List<JobHistory> rechercherEditionsLesPlusLongues(int nbeARetourner);

    /**
     * Recherche la dernière édition produite pour un beanEditionId(type d'édition) et pour une application d'origine.
     * 
     * @param beanEditionId --
     * @param appliOrigine --
     * @return the job history
     */
    public JobHistory rechercherLaDerniereEditionParBeanIdEtParAppliOrigine(String beanEditionId, String appliOrigine);

    /**
     * Recherche la dernière édition produite pour un beanEditionId(type d'édition) et pour une application d'origine.
     * 
     * @param beanEditionId --
     * @param appliOrigine --
     * @param utilisateur --
     * @return the job history
     */
    public JobHistory rechercherLaDerniereEditionParBeanIdEtParAppliOrigineEtParUtilisateur(String beanEditionId,
        String appliOrigine, String utilisateur);

    /**
     * Recherche les toutes les éditions accessibles à un utilisateur.
     * 
     * @param personne --
     * @return the list< job history>
     */
    public List<JobHistory> rechercheToutesEditionsAccessiblesAUnUtilisateur(PersonneAnnuaire personne);

}
