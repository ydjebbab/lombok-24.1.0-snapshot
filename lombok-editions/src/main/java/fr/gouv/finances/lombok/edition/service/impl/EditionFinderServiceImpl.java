/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : EditionFinderServiceImpl.java
 *
 */
package fr.gouv.finances.lombok.edition.service.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.beanutils.BeanComparator;
import org.apache.commons.collections.comparators.ComparableComparator;
import org.apache.commons.collections.comparators.ComparatorChain;
import org.apache.commons.collections.comparators.ReverseComparator;
import org.apache.commons.lang.StringUtils;

import fr.gouv.finances.lombok.edition.bean.FiltreEdition;
import fr.gouv.finances.lombok.edition.bean.JobHistory;
import fr.gouv.finances.lombok.edition.bean.ProfilDesti;
import fr.gouv.finances.lombok.edition.dao.JobHistoryDao;
import fr.gouv.finances.lombok.edition.service.EditionFinderService;
import fr.gouv.finances.lombok.securite.techbean.FiltreHabilitationAnnuaire;
import fr.gouv.finances.lombok.securite.techbean.HabilitationAnnuaire;
import fr.gouv.finances.lombok.securite.techbean.PersonneAnnuaire;
import fr.gouv.finances.lombok.securite.techbean.ValeurFiltreHabilitationAnnuaire;
import fr.gouv.finances.lombok.util.base.BaseServiceImpl;
import fr.gouv.finances.lombok.util.comparators.NullSafeBeanComparator;
import fr.gouv.finances.lombok.util.comparators.NullorNotComparableComparator;

/**
 * Class EditionFinderServiceImpl --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.3 $ Date: 11 déc. 2009
 */
public class EditionFinderServiceImpl extends BaseServiceImpl implements EditionFinderService
{

    /** Nom de l'application à l'origine de l'édition. */
    private String appliorigine;

    /** Référence vers le Dao d'accès à l'historique des éditions. */
    private JobHistoryDao jobhistorydao;

    /**
     * Instanciation de edition finder service impl.
     */
    public EditionFinderServiceImpl()
    {
        super();
    }

    /**
     * Recherche la liste des descriptions d'éditions disponibles.
     * 
     * @return the list< string>
     */
    @Override
    public List<String> rechercheBeanEditionIds()
    {
        return jobhistorydao.findBeanEditionIds(this.appliorigine);
    }

    /**
     * Recherche les nouvelles éditions accessibles à un utilisateur.
     * 
     * @param personne --
     * @return the list< job history>
     */
    @Override
    public List<JobHistory> rechercheNouvellesEditionsAccessiblesAUnUtilisateur(PersonneAnnuaire personne)
    {
        List<JobHistory> editionsAccessibles = new ArrayList<>();

        this.rechercheNouvellesEditionsPourUidUtilisateur(personne, editionsAccessibles);

        this.rechercheNouvellesEditionsPourProfilsUtilisateur(personne, editionsAccessibles);

        this.metAJourEtatConsultationDesEditions(personne, editionsAccessibles);

        this.triEditionsParEtatConsultationEtDateDemande(editionsAccessibles);

        return editionsAccessibles;
    }

    /**
     * Recherche la liste des profils des éditions produites.
     * 
     * @return the list< profil desti>
     */
    @Override
    public List<ProfilDesti> rechercheProfils()
    {
        return jobhistorydao.findProfils(this.appliorigine);
    }

    /**
     * Recherche les éditions qui correspondent à une description donnée.
     * 
     * @param editionBeanId --
     * @return the list< job history>
     */
    @Override
    public List<JobHistory> rechercherEditionParBeanId(String editionBeanId)
    {
        return jobhistorydao.findToutesEditionsParBeanEditionId(editionBeanId, this.appliorigine);
    }

    /**
     * Recherche les éditions dont la demande a été effectuée à une date donnée.
     * 
     * @param dateDemande --
     * @return the list< job history>
     */
    @Override
    public List<JobHistory> rechercherEditionParDateDemande(Date dateDemande)
    {
        return jobhistorydao.findToutesEditionsParDateDemande(dateDemande, this.appliorigine);
    }

    /**
     * Recherche les éditions qui sont dans un état donné.
     * 
     * @param etat --
     * @return the list< job history>
     */
    @Override
    public List<JobHistory> rechercherEditionParEtat(String etat)
    {
        return jobhistorydao.findToutesEditionsParEtat(etat, this.appliorigine);
    }

    /**
     * Recherche les éditions dont la demande a été effectuée durant la période passée en paramètres.
     * 
     * @param debutPeriodeDemande --
     * @param finPeriodeDemande --
     * @return the list< job history>
     */
    @Override
    public List<JobHistory> rechercherEditionParPeriodeDateDemande(Date debutPeriodeDemande, Date finPeriodeDemande)
    {
        return jobhistorydao.findToutesEditionsParPeriodeDateDemande(debutPeriodeDemande, finPeriodeDemande,
            this.appliorigine);
    }

    /**
     * Recherche toutes les éditions accessibles par un profil donné.
     * 
     * @param profil --
     * @return the list< job history>
     */
    @Override
    public List<JobHistory> rechercherEditionParProfil(String profil)
    {
        List<JobHistory> result = null;

        Map<String, Set<FiltreEdition>> mapFiltresEdition = new HashMap<>(1);

        mapFiltresEdition.put(profil, null);
        result = jobhistorydao.findToutesEditionsParProfilFiltreAppli(mapFiltresEdition, this.appliorigine);
        return result;
    }

    /**
     * Retourne la liste des éditions qui appartiennent à l'UID.
     * 
     * @param uid identifiant du propriétaire des édition
     * @return list des JobHistory qui appartiennent à l'uid passé en paramètre
     */
    @Override
    public List<JobHistory> rechercherEditionParUid(String uid)
    {
        List<JobHistory> result = null;

        result = jobhistorydao.findToutesEditionsParUidEtAppli(uid, this.appliorigine);

        // Met à jour l'état de consultation de chaque édition pour
        // l'uid en cours
        for (Iterator jobHistoriesIter = result.iterator(); jobHistoriesIter.hasNext();)
        {
            JobHistory jobHistory = (JobHistory) jobHistoriesIter.next();
            jobHistory.getDestinationEdition().isEditionDejaConsulteeParCetUid(uid);
        }

        return result;
    }

    /**
     * Recherche de l'historique d'une édition par son identifiant.
     * 
     * @param editionUuid --
     * @return the job history
     */
    @Override
    public JobHistory rechercherEditionParUuid(String editionUuid)
    {
        JobHistory result = null;

        result = jobhistorydao.findJobHistory(editionUuid);
        return result;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.edition.service.EditionFinderService#rechercherEditionsLesPlusLongues(int)
     */
    @Override
    public List<JobHistory> rechercherEditionsLesPlusLongues(int nbeARetourner)
    {
        return jobhistorydao.findEditionsLesPlusLongues(this.appliorigine, nbeARetourner);
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param beanEditionId
     * @param appliOrigine
     * @return job history
     * @see fr.gouv.finances.lombok.edition.service.EditionFinderService#rechercherLaDerniereEditionParBeanIdEtParAppliOrigine(java.lang.String,
     *      java.lang.String)
     */
    @Override
    public JobHistory rechercherLaDerniereEditionParBeanIdEtParAppliOrigine(String beanEditionId, String appliOrigine)
    {
        return jobhistorydao.findLaDerniereEditionParBeanIdEtParAppliOrigine(beanEditionId, appliOrigine);
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param beanEditionId
     * @param appliOrigine
     * @param utilisateur
     * @return job history
     * @see fr.gouv.finances.lombok.edition.service.EditionFinderService#rechercherLaDerniereEditionParBeanIdEtParAppliOrigineEtParUtilisateur(java.lang.String,
     *      java.lang.String, java.lang.String)
     */
    @Override
    public JobHistory rechercherLaDerniereEditionParBeanIdEtParAppliOrigineEtParUtilisateur(String beanEditionId,
        String appliOrigine, String utilisateur)
    {
        return jobhistorydao.findLaDerniereEditionParBeanIdEtParAppliOrigineEtParUtilisateur(beanEditionId,
            appliOrigine, utilisateur);
    }

    /**
     * Recherche les nouvelles éditions accessibles à un utilisateur.
     * 
     * @param personne --
     * @return the list< job history>
     */
    @Override
    public List<JobHistory> rechercheToutesEditionsAccessiblesAUnUtilisateur(PersonneAnnuaire personne)
    {
        List<JobHistory> editionsAccessibles = new ArrayList<>();

        this.rechercheToutesEditionsPourUidUtilisateur(personne, editionsAccessibles);

        this.rechercheToutesEditionsPourProfilsUtilisateur(personne, editionsAccessibles);

        this.metAJourEtatConsultationDesEditions(personne, editionsAccessibles);

        this.triEditionsParEtatConsultationEtDateDemande(editionsAccessibles);

        return editionsAccessibles;

    }

    /**
     * Sets the appliorigine.
     * 
     * @param appliorigine the appliorigine to set
     */
    public void setAppliorigine(String appliorigine)
    {
        this.appliorigine = appliorigine;
    }

    /**
     * Sets the jobhistorydao.
     * 
     * @param jobhistorydao the jobhistorydao to set
     */
    public void setJobhistorydao(JobHistoryDao jobhistorydao)
    {
        this.jobhistorydao = jobhistorydao;
    }

    /**
     * Met à jour la collection filtresEditionsDuProfils pour un objet HabilitationAnnuaire donné.
     * 
     * @param filtresEditionsDuProfil --
     * @param habilitationAnnuaire --
     */
    private void affecteLesFiltreEditionPourUneHabilitationAnnuaire(Set<FiltreEdition> filtresEditionsDuProfil,
        HabilitationAnnuaire habilitationAnnuaire)
    {
        if (habilitationAnnuaire != null)
        {
            Set habis = habilitationAnnuaire.getListeFiltres();
            if (habis != null)
            {
                for (Iterator iterHabis = habis.iterator(); iterHabis.hasNext();)
                {
                    creeFiltreEditionAPartirDeFiltreHabilitation(filtresEditionsDuProfil, iterHabis);
                }
            }
        }
    }

    /**
     * Construit une map association à chaque habilitation une liste de filtres.
     * 
     * @param personne --
     * @return the map< string, set>
     */
    private Map<String, Set<FiltreEdition>> construitMapDesFiltreEditionParProfil(PersonneAnnuaire personne)
    {
        Map<String, Set<FiltreEdition>> result = new HashMap<>();

        Collection profils = personne.getListeHabilitations();
        if (profils != null && profils.size() > 0)
        {
            for (Iterator iterProfils = profils.iterator(); iterProfils.hasNext();)
            {
                HabilitationAnnuaire habilitationAnnuaire = (HabilitationAnnuaire) iterProfils.next();
                if (this.isNomProfilHabilitationAnnuaireValide(habilitationAnnuaire)
                    && this.isPeriodeHabilitationAnnuaireValide(habilitationAnnuaire))
                {
                    Set<FiltreEdition> filtresEditionsDuProfil = new HashSet<>();
                    result.put(habilitationAnnuaire.getNomProfil(), filtresEditionsDuProfil);
                    this.affecteLesFiltreEditionPourUneHabilitationAnnuaire(filtresEditionsDuProfil,
                        habilitationAnnuaire);
                }
            }
        }
        else
        {
            // Aucun profil
        }
        return result;
    }

    /**
     * Copie les valeurs affectées à un objet FiltreHabilitationAnnuaire dans un objet FiltreEdition.
     * 
     * @param filtre --
     * @param filtreEdition --
     */
    private void copieLesValeursDeFiltreAnnuaireDansUnFiltreEdition(FiltreHabilitationAnnuaire filtre,
        FiltreEdition filtreEdition)
    {
        Set listeValeursDeFiltre = filtre.getListeValeursDeFiltre();

        if (listeValeursDeFiltre != null && listeValeursDeFiltre.size() > 0)
        {
            for (Iterator iterResult = listeValeursDeFiltre.iterator(); iterResult.hasNext();)
            {
                ValeurFiltreHabilitationAnnuaire valeurFiltreHabi =
                    (ValeurFiltreHabilitationAnnuaire) iterResult.next();
                if (valeurFiltreHabi != null)
                {
                    filtreEdition.affecterLaValeur(valeurFiltreHabi.getValeur());
                }
            }
        }
    }

    /**
     * Copie le nom d'un filtre contenu dans un objet FiltreEdition.
     * 
     * @param filtresEditionsDuProfil --
     * @param iterHabis --
     */
    private void creeFiltreEditionAPartirDeFiltreHabilitation(Set<FiltreEdition> filtresEditionsDuProfil,
        Iterator iterHabis)
    {
        FiltreHabilitationAnnuaire filtre = (FiltreHabilitationAnnuaire) iterHabis.next();

        if (filtre != null && filtre.getNomDuFiltre() != null && StringUtils.isNotBlank(filtre.getNomDuFiltre()))
        {
            FiltreEdition filtreEdition = new FiltreEdition(filtre.getNomDuFiltre());
            filtreEdition.setAllow(filtre.isAllow());
            filtreEdition.setDeny(filtre.isDeny());
            filtresEditionsDuProfil.add(filtreEdition);

            copieLesValeursDeFiltreAnnuaireDansUnFiltreEdition(filtre, filtreEdition);

        }
    }

    /**
     * Vérifie que la propriété nomProfil d'un objet PersonneAnnuaire est valide.
     * 
     * @param habilitationAnnuaire --
     * @return true, if checks if is nom profil habilitation annuaire valide
     */
    private boolean isNomProfilHabilitationAnnuaireValide(HabilitationAnnuaire habilitationAnnuaire)
    {
        boolean result = false;

        if (habilitationAnnuaire != null && habilitationAnnuaire.getNomProfil() != null
            && StringUtils.isNotBlank(habilitationAnnuaire.getNomProfil()))
        {
            result = true;
        }
        return result;
    }

    /**
     * Teste si une HabilitationAnnuaire est valide pour la date du jour.
     * 
     * @param habilitationAnnuaire --
     * @return true, if checks if is periode habilitation annuaire valide
     */
    private boolean isPeriodeHabilitationAnnuaireValide(HabilitationAnnuaire habilitationAnnuaire)
    {
        boolean result = false;
        Date dateJour = new Date();

        if ((habilitationAnnuaire != null)
            && (habilitationAnnuaire.getDateDebut() != null && (habilitationAnnuaire.getDateDebut().before(dateJour) || habilitationAnnuaire
                .getDateDebut().compareTo(dateJour) == 0))
            && (habilitationAnnuaire.getDateFin() != null && (habilitationAnnuaire.getDateFin().after(dateJour) || habilitationAnnuaire
                .getDateFin().compareTo(dateJour) == 0))

        )
        {
            result = true;
        }

        return result;
    }

    /**
     * Met à jour l'attribut non persistent "editionDejaConsulteeParCetUid" qui.
     * 
     * @param personne --
     * @param editionsAccessibles --
     */
    private void metAJourEtatConsultationDesEditions(PersonneAnnuaire personne, List<JobHistory> editionsAccessibles)
    {
        // Met à jour l'état de consultation de chaque édition pour
        // l'uid en cours
        for (Iterator jobHistoriesIter = editionsAccessibles.iterator(); jobHistoriesIter.hasNext();)
        {
            JobHistory jobHistory = (JobHistory) jobHistoriesIter.next();
            jobHistory.getDestinationEdition().isEditionDejaConsulteeParCetUid(personne.getUid());
        }
    }

    /**
     * Ajoute à la liste editionsAccessibles toutes les nouvelles éditions qui sont accessibles à un utilisateur en
     * raison des profils dont il dispose.
     * 
     * @param personne --
     * @param editionsAccessibles --
     */
    private void rechercheNouvellesEditionsPourProfilsUtilisateur(PersonneAnnuaire personne,
        List<JobHistory> editionsAccessibles)
    {
        List<JobHistory> listedeseditionspourunprofil = null;
        Map<String, Set<FiltreEdition>> mapFiltresEdition = this.construitMapDesFiltreEditionParProfil(personne);

        listedeseditionspourunprofil =
            jobhistorydao.findNouvellesEditionsParProfilFiltreAppli(personne.getUid(), mapFiltresEdition,
                this.appliorigine);

        if (listedeseditionspourunprofil != null)
        {
            editionsAccessibles.addAll(listedeseditionspourunprofil);
        }
    }

    /**
     * Ajoute à la liste editionsAccessibles toutes les éditions qui sont accessibles à un utilisateur en raison de son
     * uid.
     * 
     * @param personne --
     * @param editionsAccessibles --
     */
    private void rechercheNouvellesEditionsPourUidUtilisateur(PersonneAnnuaire personne,
        List<JobHistory> editionsAccessibles)
    {
        // Récupération des éditions dont l'utilisateur est propriétaire
        List<JobHistory> nouvEditProdParUti =
            jobhistorydao.findNouvellesEditionsParUidEtAppli(personne.getUid(), this.appliorigine);

        if (nouvEditProdParUti != null)
        {
            editionsAccessibles.addAll(nouvEditProdParUti);
        }
    }

    /**
     * Ajoute à la liste editionsAccessibles toutes les éditions qui sont accessibles à un utilisateur en raison des
     * profils dont il dispose.
     * 
     * @param personne --
     * @param editionsAccessibles --
     */
    private void rechercheToutesEditionsPourProfilsUtilisateur(PersonneAnnuaire personne,
        List<JobHistory> editionsAccessibles)
    {
        List<JobHistory> listedeseditionspourunprofil = null;
        Map<String, Set<FiltreEdition>> mapFiltresEdition = this.construitMapDesFiltreEditionParProfil(personne);

        listedeseditionspourunprofil =
            jobhistorydao.findToutesEditionsParProfilFiltreAppli(mapFiltresEdition, this.appliorigine);

        if (listedeseditionspourunprofil != null)
        {
            editionsAccessibles.addAll(listedeseditionspourunprofil);
        }
    }

    /**
     * Ajoute à la liste editionsAccessibles toutes les éditions qui sont accessibles à un utilisateur en raison de son
     * uid.
     * 
     * @param personne --
     * @param editionsAccessibles --
     */
    private void rechercheToutesEditionsPourUidUtilisateur(PersonneAnnuaire personne,
        List<JobHistory> editionsAccessibles)
    {
        // Récupération des éditions dont l'utilisateur est propriétaire
        List<JobHistory> toutesEditProdParUti =
            jobhistorydao.findToutesEditionsParUidEtAppli(personne.getUid(), this.appliorigine);

        if (toutesEditProdParUti != null)
        {
            editionsAccessibles.addAll(toutesEditProdParUti);
        }
    }

    /**
     * Tri des éditions par état de la consultation et date de la demande.
     * 
     * @param editionsAccessibles --
     */
    @SuppressWarnings("unchecked")
    private void triEditionsParEtatConsultationEtDateDemande(List<JobHistory> editionsAccessibles)
    {
        // TRi des éditions par ordre suivant :
        // 1. Editions non consultée par l'utilisateur
        // 2. Date de demande de l'édition
        List<Comparator> sortFields = new ArrayList<Comparator>();

        sortFields.add(new NullSafeBeanComparator("destinationEdition.editionDejaConsulteeParUid",
            new NullorNotComparableComparator()));
        sortFields
            .add(new BeanComparator("ordoEdition.dateDemande", new ReverseComparator(new ComparableComparator())));
        ComparatorChain multiSort = new ComparatorChain(sortFields);
        Collections.sort(editionsAccessibles, multiSort);
    }

}
