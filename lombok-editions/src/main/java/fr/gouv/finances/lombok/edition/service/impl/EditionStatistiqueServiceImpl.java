/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : EditionStatistiqueServiceImpl.java
 *
 */
package fr.gouv.finances.lombok.edition.service.impl;

import java.util.Iterator;
import java.util.List;

import fr.gouv.finances.lombok.edition.dao.StatistiqueJobHistoryDao;
import fr.gouv.finances.lombok.edition.service.EditionStatistiqueService;
import fr.gouv.finances.lombok.edition.techbean.StatJobHistory;
import fr.gouv.finances.lombok.util.base.BaseServiceImpl;

/**
 * Class EditionStatistiqueServiceImpl --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.2 $ Date: 11 déc. 2009
 */
public class EditionStatistiqueServiceImpl extends BaseServiceImpl implements EditionStatistiqueService
{

    private StatistiqueJobHistoryDao statistiquejobhistorydao;

    private String appliorigine;

    public EditionStatistiqueServiceImpl()
    {
        super();       
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @return list
     * @see fr.gouv.finances.lombok.edition.service.EditionStatistiqueService#compteJobHistoryParDatePurge()
     */
    @Override
    public List compteJobHistoryParDatePurge()
    {
        return statistiquejobhistorydao.compteJobHistoryParDatePurge(this.appliorigine);
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @return list
     * @see fr.gouv.finances.lombok.edition.service.EditionStatistiqueService#compteJobHistoryParJourDemande()
     */
    @Override
    public List compteJobHistoryParJourDemande()
    {
        return statistiquejobhistorydao.compteJobHistoryParJourDemande(this.appliorigine);
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @return list
     * @see fr.gouv.finances.lombok.edition.service.EditionStatistiqueService#compteJobHistoryParStatus()
     */
    @Override
    public List compteJobHistoryParStatus()
    {
        return statistiquejobhistorydao.compteJobHistoryParStatus(this.appliorigine);
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @return list
     * @see fr.gouv.finances.lombok.edition.service.EditionStatistiqueService#compteJobHistoryParTypeEtStatus()
     */
    @Override
    public List compteJobHistoryParTypeEtStatus()
    {
        return statistiquejobhistorydao.compteJobHistoryParTypeEtStatus(this.appliorigine);
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @return list
     * @see fr.gouv.finances.lombok.edition.service.EditionStatistiqueService#compteTailleEditionParType()
     */
    @Override
    public List compteTailleEditionParType()
    {
        List stat = statistiquejobhistorydao.compteTailleEditionParType(this.appliorigine);
        for (Iterator iterStat = stat.iterator(); iterStat.hasNext();)
        {
            StatJobHistory statJobHistory = (StatJobHistory) iterStat.next();
            statJobHistory.setTailleMoyenne(new Double(statJobHistory.getTailleMoyenne().doubleValue() / 1024));
            statJobHistory.setTailleTotale(new Long(statJobHistory.getTailleTotale().longValue() / 1024 / 1024));
        }
        return stat;
    }

    /**
     * Modificateur de l attribut appliorigine.
     * 
     * @param appliorigine le nouveau appliorigine
     */
    public void setAppliorigine(String appliorigine)
    {
        this.appliorigine = appliorigine;
    }

    /**
     * Modificateur de l attribut statistiquejobhistorydao.
     * 
     * @param statistiquejobhistorydao le nouveau statistiquejobhistorydao
     */
    public void setStatistiquejobhistorydao(StatistiqueJobHistoryDao statistiquejobhistorydao)
    {
        this.statistiquejobhistorydao = statistiquejobhistorydao;
    }
}
