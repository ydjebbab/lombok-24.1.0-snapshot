/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : CpJRCsvExporter.java
 *
 */
package fr.gouv.finances.lombok.edition.exporter;

import java.util.StringTokenizer;

import net.sf.jasperreports.engine.JasperReportsContext;
import net.sf.jasperreports.engine.export.JRCsvExporter;

/**
 * Class CpJRCsvExporter --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.3 $ Date: 11 déc. 2009
 */
public class CpJRCsvExporter extends JRCsvExporter
{

    /**
     * Constructeur de la classe CpJRCsvExporter.java
     *
     */
    public CpJRCsvExporter()
    {
        super();
        
    }

    /**
     * Constructeur de la classe CpJRCsvExporter.java
     *
     * @param jasperReportsContext
     */
    public CpJRCsvExporter(JasperReportsContext jasperReportsContext)
    {
        super(jasperReportsContext);    // DOCUMENTEZ_MOI Raccord de constructeur auto-généré
        
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param source
     * @return string
     * @see net.sf.jasperreports.engine.export.JRCsvExporter#prepareText(java.lang.String)
     */
    protected String prepareText(String source)
    {
        String str = null;

        if (source != null)
        {
            boolean putQuotes = false;

            // On ne protège pas les champs qui contiennent un délimiteur
            // On considère qu'il s'agit de plusieurs champs

            /*
             * if (source.indexOf(delimiter) >= 0) { putQuotes = true; }
             */

            StringBuilder   sbuffer = new StringBuilder();
            StringTokenizer tkzer = new StringTokenizer(source, "\"\n", true);
            String token = null;

            while (tkzer.hasMoreTokens())
            {
                token = tkzer.nextToken();
                if ("\"".equals(token))
                {
                    putQuotes = true;
                    sbuffer.append("\"\"");
                }
                else if ("\n".equals(token))
                {
                    // sbuffer.append(' ');
                    putQuotes = true;
                    sbuffer.append('\n');
                }
                else
                {
                    sbuffer.append(token);
                }
            }

            str = sbuffer.toString();

            if (putQuotes)
            {
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append("\"");
                stringBuilder.append(str);
                stringBuilder.append("\"");
                str = stringBuilder.toString();
            }
        }

        return str;
    }
}
