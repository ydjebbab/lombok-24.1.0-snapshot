/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 */
package fr.gouv.finances.lombok.edition.bean;

import java.util.HashSet;
import java.util.Set;

import fr.gouv.finances.lombok.util.base.BaseBean;

/**
 * Données sur la destination d'une édition.
 * 
 * @author amleplatinec
 * @author Christophe Breheret-Girardin
 */
public class DestinationEdition extends BaseBean
{
    /** Initialisation de l'UID. */
    private static final long serialVersionUID = 1L;

    /** Trigramme de l'application qui a demandé l'édition. */
    private String applicationOrigine;

    /** Flag qui indique si l'édition peut être accédée par un seul utilisateur ou par un groupe d'utilisateurs. */
    private Boolean monoDestinataire = Boolean.TRUE;

    /** UID de l'utilisateur ou identifiant du batch qui a déclenché le job. */
    private String uidProprietaire;

    /** Liste des Habilitations auxquelles l'édition produite est destinée. */
    private Set<ProfilDesti> profilsDesti = new HashSet<>();

    /** Flag qui indique si l'édition a déjà été consultée par l'utilisateur destinataire. */
    private Boolean nouvelleEdition = Boolean.TRUE;

    /** Liste contenant les UID des utilisateurs qui ont consulté l'édition. */
    private Set<UidConsult> uidConsult = new HashSet<>();

    /** Flag non persistent qui permet de savoir si l'édition a déjà été consultée par un utilisateur. */
    private Boolean editionDejaConsulteeParUid = Boolean.FALSE;

    /**
     * Constructeur.
     */
    public DestinationEdition()
    {
        super();
    }

    /**
     * Ajouter un profil aux destinataire de l'application.
     *
     * @param nomProfil --
     * @return the profil desti
     */
    public ProfilDesti ajouterUnProfilDestinataire(String nomProfil)
    {
        // L'édition est destinée à plusieurs destinataires
        this.monoDestinataire = Boolean.FALSE;

        // Création d'un nouveau profil destinataire
        ProfilDesti profilDesti = new ProfilDesti();
        profilDesti.setNomProfil(nomProfil);

        // Initialisation de la liste des profils si nécessaire
        if (this.profilsDesti == null)
        {
            this.profilsDesti = new HashSet<>();
        }

        // Ajout du nouveau profil à la liste
        this.profilsDesti.add(profilDesti);

        return profilDesti;
    }

    /**
     * Gets the application origine.
     * 
     * @return the applicationOrigine
     */
    public String getApplicationOrigine()
    {
        return applicationOrigine;
    }

    /**
     * Gets the edition deja consultee par uid.
     * 
     * @return the editionDejaConsulteeParUid
     */
    public Boolean getEditionDejaConsulteeParUid()
    {
        return editionDejaConsulteeParUid;
    }

    /**
     * Gets the mono destinataire.
     * 
     * @return the monoDestinataire
     */
    public Boolean getMonoDestinataire()
    {
        return monoDestinataire;
    }

    /**
     * Gets the nouvelle edition.
     * 
     * @return the nouvelleEdition
     */
    public Boolean getNouvelleEdition()
    {
        return nouvelleEdition;
    }

    /**
     * Gets the profils desti.
     * 
     * @return the profilsDesti
     */
    public Set<ProfilDesti> getProfilsDesti()
    {
        return profilsDesti;
    }

    /**
     * Gets the uid consult.
     * 
     * @return the uidConsult
     */
    public Set<UidConsult> getUidConsult()
    {
        return uidConsult;
    }

    /**
     * Gets the uid proprietaire.
     * 
     * @return the uidProprietaire
     */
    public String getUidProprietaire()
    {
        return uidProprietaire;
    }

    /**
     * Indique si une édition a déjà été consultée par un uid donné Met à jour le flag porté par l'objet permettant de
     * savoir si l'utilisateur a déjà consulté l'objet (editionDejaConsulteeParUid).
     * 
     * @param uid --
     * @return true, if checks if is edition deja consultee par cet uid
     */
    public Boolean isEditionDejaConsulteeParCetUid(String uid)
    {
        Boolean result = Boolean.FALSE;
        
        if (this.uidConsult == null)
        {
            this.uidConsult = new HashSet<>();
        }
        
        // Edition qui ne peut être consultée que par son propriétaire
        if (this.monoDestinataire == null || this.monoDestinataire.booleanValue())
        {
            // L'UID demandant la consultation doit être le propriétaire de l'édition
            if (uid != null && uid.equals(this.uidProprietaire) && this.nouvelleEdition != null
                && (!this.nouvelleEdition.booleanValue()))
            {
                result = Boolean.TRUE;
            }
        }
        // Edition consultable par un ensemble d'utilisateurs
        else
        {
            // L'utilisateur a-t-il déjà consulté l'édition ?
            if (this.uidConsult.contains(new UidConsult(uid)))
            {
                result = Boolean.TRUE;
            }
        }

        this.editionDejaConsulteeParUid = result;
        return result;
    }

    /**
     * Met à jour les utilisateurs ayant consulté l'édition.
     * 
     * @param uid --
     */
    public void metAJourLaListeDesConsultations(String uid)
    {
        UidConsult uidUtilisateur = new UidConsult(uid);
        if (this.uidConsult == null)
        {
            this.uidConsult = new HashSet<>();
        }
        if (this.nouvelleEdition == null)
        {
            this.nouvelleEdition = Boolean.TRUE;
        }

        // L'edition ne peut être consultée que par son propriétaire
        if (this.monoDestinataire == null || this.monoDestinataire.booleanValue())
        {
            // L'UID demandant la consultation doit être le propriétaire de l'édition
            if (uid != null && uid.equals(this.uidProprietaire) && this.nouvelleEdition.booleanValue())
            {
                this.nouvelleEdition = Boolean.FALSE;
            }
        }
        // Edition consultable par un ensemble d'utilisateurs
        else
        {
            // L'utilisateur a-t-il déjà consulté l'édition ?
            if (!this.uidConsult.contains(uidUtilisateur))
            {
                this.uidConsult.add(uidUtilisateur);
            }
        }
    }

    /**
     * Mutateur de application origine.
     * 
     * @param applicationOrigine the applicationOrigine to set
     */
    public void setApplicationOrigine(String applicationOrigine)
    {
        this.applicationOrigine = applicationOrigine;
    }

    /**
     * Mutateur de edition deja consultee par uid.
     * 
     * @param editionDejaConsulteeParUid the editionDejaConsulteeParUid to set
     */
    public void setEditionDejaConsulteeParUid(Boolean editionDejaConsulteeParUid)
    {
        this.editionDejaConsulteeParUid = editionDejaConsulteeParUid;
    }

    /**
     * Mutateur de mono destinataire.
     * 
     * @param monoDestinataire the monoDestinataire to set
     */
    public void setMonoDestinataire(Boolean monoDestinataire)
    {
        this.monoDestinataire = monoDestinataire;
    }

    /**
     * Mutateur de nouvelle edition.
     * 
     * @param nouvelleEdition the nouvelleEdition to set
     */
    public void setNouvelleEdition(Boolean nouvelleEdition)
    {
        this.nouvelleEdition = nouvelleEdition;
    }

    /**
     * Mutateur de profils desti.
     * 
     * @param profilsDesti the profilsDesti to set
     */
    public void setProfilsDesti(Set<ProfilDesti> profilsDesti)
    {
        this.profilsDesti = profilsDesti;
    }

    /**
     * Mutateur de uid consult.
     * 
     * @param uidConsult the uidConsult to set
     */
    public void setUidConsult(Set<UidConsult> uidConsult)
    {
        this.uidConsult = uidConsult;
    }

    /**
     * Mutateur de uid proprietaire.
     * 
     * @param uidProprietaire the uidProprietaire to set
     */
    public void setUidProprietaire(String uidProprietaire)
    {
        this.uidProprietaire = uidProprietaire;
    }

}
