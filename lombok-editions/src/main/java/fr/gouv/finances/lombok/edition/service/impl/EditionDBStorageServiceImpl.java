/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : EditionDBStorageServiceImpl.java
 *
 */
package fr.gouv.finances.lombok.edition.service.impl;

import java.io.File;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.orm.hibernate5.HibernateOptimisticLockingFailureException;

import fr.gouv.finances.lombok.edition.bean.ContenuEdition;
import fr.gouv.finances.lombok.edition.bean.JobHistory;
import fr.gouv.finances.lombok.edition.dao.JobHistoryDao;
import fr.gouv.finances.lombok.edition.service.EditionStorageService;
import fr.gouv.finances.lombok.edition.util.Control;
import fr.gouv.finances.lombok.upload.bean.FichierJoint;
import fr.gouv.finances.lombok.util.base.BaseServiceImpl;
import fr.gouv.finances.lombok.util.exception.ExploitationException;
import fr.gouv.finances.lombok.util.exception.ProgrammationException;
import fr.gouv.finances.lombok.util.exception.RegleGestionException;
import fr.gouv.finances.lombok.util.format.FormaterDate;

/**
 * Class EditionDBStorageServiceImpl --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.2 $ Date: 11 déc. 2009
 */
public class EditionDBStorageServiceImpl extends BaseServiceImpl implements EditionStorageService
{
    private static final Logger log = LoggerFactory.getLogger(EditionDBStorageServiceImpl.class);

    /** Taille maximale d'un fichier qui peut être stocké en mémoire dans un tableau de bytes. */
    private static final int MAX_SIZE_FILE_IN_BYTE_ARRAY = 5 * 1024 * 1024;

    /**
     * Délai d'attente (en ms) entre le chargement de la même édition, décale les chargements pour permettre la mise à
     * jour de la liste des utilisateurs qui ont consultés l'édition.
     */
    private static final int DELAI_ATTENTE_CHARGEMENT_MEME_EDITION = 1000;

    /** Nombre de tentatives de rechargement d'une édition après un échec sur une erreur de verrouillage optimiste. */
    private static final int MAX_TENTATIVES_CHARGEMENT = 3;
    
    /** Delai au dela duquel une demande est purgée, en minutes **/
    private static final int DELAI_PURGE_DEMANDE = 20;

    /** Référence vers le Dao d'accès à l'historique des éditions. */
    private JobHistoryDao jobhistorydao;

    /** Maintient la liste des editions qui sont en cours de chargement. */
    private final ConcurrentMap<String, ConcurrentMap<String, Date>> editionsEnCoursChargement =
        new ConcurrentHashMap<>();

    /**
     * Instanciation de edition db storage service impl.
     */
    public EditionDBStorageServiceImpl()
    {
        super();
    }

    /**
     * Charge un fichier edition soit dans un fichier temporaire soit dans un tableau de byte[] en mémoire selon la
     * taille du fichier. Les fichiers dont la taille est inférieure à 5Mo sont stockés dans un tableau de byte, les
     * autres sont stockés dans un fichier temporaire
     * 
     * @param jobHistory --
     * @return the fichier joint
     * @throws RegleGestionException the regle gestion exception
     */
    @Override
    public FichierJoint chargerFichierEdition(JobHistory jobHistory) throws RegleGestionException
    {
        Control.assertEditionDisponible(jobHistory);

        FichierJoint result;

        if (jobHistory.getStockageEdition().getTailleFichier().longValue() < MAX_SIZE_FILE_IN_BYTE_ARRAY)
        {
            result = this.chargerFichierEditionToByte(jobHistory);
        }
        else
        {
            result = this.chargerFichierEditionToFile(jobHistory);
        }

        return result;
    }

    /**
     * Lit une édition stockée et la retourne sous forme d'un objet FichierJoint Met à jour l'état de consultation de
     * l'édition.
     * 
     * @param jobHistory qui représente l'édition
     * @param uid --
     * @return Fichierjoint associé à l'édition
     * @throws RegleGestionException the regle gestion exception
     */
    @Override
    public FichierJoint chargerFichierEdition(JobHistory jobHistory, String uid) throws RegleGestionException
    {
        Control.assertEditionDisponible(jobHistory);

        Date instantCourant = Control.instantCourant();

        String cleDemandeChargement = calculeCleDemandeChargement(uid, instantCourant);

        int delai = this.calculDelaiAvantChargement(jobHistory.getEditionUuid(), cleDemandeChargement, instantCourant);

        this.attendre(delai);

        FichierJoint result = null;
        int nbTentative = 0;

        while (nbTentative < MAX_TENTATIVES_CHARGEMENT && result == null)
        {
            nbTentative++;
            result = rechargeFichierEdition(jobHistory, uid, delai);
        }

        if (result == null)
        {
            StringBuilder   msg = new StringBuilder();
            msg.append("ECHEC APRES ");
            msg.append(MAX_TENTATIVES_CHARGEMENT);
            msg.append(" DE CHARGEMENT DE L'EDITION");
            msg.append(jobHistory.getEditionUuid());
            log.warn(msg.toString());
            throw new ProgrammationException(msg.toString());
        }

        this.supprimeEntreeApresChargement(jobHistory.getEditionUuid(), cleDemandeChargement);
        return result;
    }

    /**
     * Efface une édition.
     * 
     * @param jobHistory --
     */
    @Override
    public void effacerFichierEdition(JobHistory jobHistory)
    {
        // ne rien faire - le contenu de l'édition est supprimé
        // en même temps que la suppresion de l'entrée représentant l'édition
        // dans l'historique des éditions
    }

    /**
     * Sets the jobhistorydao.
     * 
     * @param jobhistorydao the jobhistorydao to set
     */
    public void setJobhistorydao(JobHistoryDao jobhistorydao)
    {
        this.jobhistorydao = jobhistorydao;
    }

    /**
     * Stocke une édition.
     * 
     * @param jobHistory --
     * @param fichierJoint --
     */
    @Override
    public void stockerEdition(JobHistory jobHistory, FichierJoint fichierJoint)
    {
        Control.assertJobHistoryNotNull(jobHistory);
        Control.assertFichierJointNotNull(fichierJoint);

        jobHistory.getStockageEdition().setTailleFichier(Long.valueOf(fichierJoint.getTailleFichier()));
        jobHistory.getStockageEdition().setLeContenuEdition(new ContenuEdition());

        if (fichierJoint.isDataInByteArray())
        {
            this.stockerEditionFromBytesArray(jobHistory, fichierJoint.getLeContenuDuFichier().getData());
        }
        else
        {
            this.stockerEditionFromFile(jobHistory, fichierJoint);
        }
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param nombreOccurences
     * @param applicationOrigine
     * @see fr.gouv.finances.lombok.edition.service.EditionStorageService#synchroniseHistoriqueAvecSf(int,
     *      java.lang.String)
     */
    @Override
    public void synchroniseHistoriqueAvecSf(int nombreOccurences, String applicationOrigine)
    {
        // ne rien faire
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param applicationOrigine
     * @see fr.gouv.finances.lombok.edition.service.EditionStorageService#synchroniseSfAvecHistorique(java.lang.String)
     */
    @Override
    public void synchroniseSfAvecHistorique(String applicationOrigine)
    {
        // ne rien faire
    }

    /**
     * Attend une durée définie en ms.
     * 
     * @param delai --
     */
    private void attendre(int delai)
    {
        try
        {
            Thread.sleep(delai);
        }
        catch (InterruptedException interrupe)
        {
            throw new ProgrammationException("ERREUR LORS DE L'ATTENTE ENTRE DEUX CHARGEMENTS DE LA MEME EDITION",
                interrupe);
        }
    }

    /**
     * Calcule pour chaque chargement un temps d'attente rn ms dépendant du nombre d'utilisateurs qui demande la même
     * édition.
     * 
     * @param editionUuid --
     * @param cleDemandeChargement --
     * @param instantCourant --
     * @return the int
     */
    private int calculDelaiAvantChargement(String editionUuid, String cleDemandeChargement, Date instantCourant)
    {
        int result;
        int nbLecturesActives;

        ConcurrentMap<String, Date> mapUserEdit;

        if (this.editionsEnCoursChargement.containsKey(editionUuid))
        {
            mapUserEdit = this.editionsEnCoursChargement.get(editionUuid);
            this.supprimeDemandeEditionDePlusDeVingtMinutes(mapUserEdit, instantCourant);
        }
        else
        {
            // On ajoute l'édition dans la liste des éditions en cours de chargement
            mapUserEdit = new ConcurrentHashMap<>();
            this.editionsEnCoursChargement.put(editionUuid, mapUserEdit);
        }

        mapUserEdit.put(cleDemandeChargement, instantCourant);

        nbLecturesActives = mapUserEdit.size();

        result = (nbLecturesActives - 1) * DELAI_ATTENTE_CHARGEMENT_MEME_EDITION;
        return result;
    }

    /**
     * Calcule la clé d'une demande de chargement pour un uid et un instant donné.
     * 
     * @param uid --
     * @param instantCourant --
     * @return the string
     */
    private String calculeCleDemandeChargement(String uid, Date instantCourant)
    {
        StringBuilder   cleDemandeChargement = new StringBuilder();

        cleDemandeChargement.append(uid);
        cleDemandeChargement.append(FormaterDate.formatDateHeureMinuteSecondeMillisecondeCourt(instantCourant));

        return cleDemandeChargement.toString();
    }

    /**
     * Lit une édition stockée et la retourne sous forme d'un objet FichierJoint.
     * 
     * @param jobHistory qui représente l'édition
     * @return Fichijoint associé à l'édition
     */
    private FichierJoint chargerFichierEditionToByte(JobHistory jobHistory)
    {
        FichierJoint fichierJoint;

        Control.assertJobHistoryNotNull(jobHistory);

        // lecture des données de l'édition
        jobHistory = jobhistorydao.chargerContenuEditionToBytes(jobHistory);

        if (jobHistory.getStockageEdition() != null
            && jobHistory.getStockageEdition().getLeContenuEdition() != null)
        {
            byte[] data = jobHistory.getStockageEdition().getLeContenuEdition().getDataByte();

            fichierJoint = new FichierJoint(data);

            fichierJoint.setNomFichierOriginal(jobHistory.getPresentationEdition().getNomFicDownload());
            fichierJoint.setTypeMimeFichier(jobHistory.getPresentationEdition().getTypeMime());
            fichierJoint.setDateHeureSoumission(jobHistory.getOrdoEdition().getDateDemande());
        }
        else
        {
            throw new ProgrammationException("INCOHERENCE DANS LE MODE DE STOCKAGE DES EDITIONS");
        }

        return fichierJoint;
    }

    /**
     * Lit une édition stockée et la retourne sous forme d'un objet FichierJoint - Les données de l'édition sont
     * stockées dans un fichier temporaire référencé dans l'objet FichierJoint.
     * 
     * @param jobHistory qui représente l'édition
     * @return Fichijoint associé à l'édition
     */
    private FichierJoint chargerFichierEditionToFile(JobHistory jobHistory)
    {
        Control.assertJobHistoryNotNull(jobHistory);

        FichierJoint fichierJoint;

        // lecture des données de l'édition
        this.jobhistorydao.chargerContenuEditionToFile(jobHistory);

        if (jobHistory.getStockageEdition() != null
            && jobHistory.getStockageEdition().getLeContenuEdition() != null)
        {
            File dataFile = jobHistory.getStockageEdition().getLeContenuEdition().getDataFile();

            fichierJoint = new FichierJoint(dataFile);

            fichierJoint.setNomFichierOriginal(jobHistory.getPresentationEdition().getNomFicDownload());
            fichierJoint.setTypeMimeFichier(jobHistory.getPresentationEdition().getTypeMime());
            fichierJoint.setDateHeureSoumission(jobHistory.getOrdoEdition().getDateDemande());
        }
        else
        {
            throw new ProgrammationException("INCOHERENCE DANS LE MODE DE STOCKAGE DES EDITIONS");
        }

        return fichierJoint;
    }

    /**
     * Lit une édition stockée et la retourne sous forme d'un objet FichierJoint Met à jour l'état de consultation de
     * l'édition Gestion des exceptions de verrouillage optimiste.
     * 
     * @param jobHistory --
     * @param uid --
     * @param delai --
     * @return the fichier joint
     */
    private FichierJoint rechargeFichierEdition(JobHistory jobHistory, String uid, int delai)
    {
        log.debug(">>> Debut methode rechargeFichierEdition");
        FichierJoint result;

        JobHistory jobHistoryRelu = jobhistorydao.findJobHistory(jobHistory.getEditionUuid());

        if (jobHistoryRelu == null)
        {
            StringBuilder   msg = new StringBuilder();
            msg.append("ACCES A L'EDITION ");
            msg.append(jobHistory.getEditionUuid());
            msg.append(" IMPOSSIBLE");
            throw new ProgrammationException(msg.toString());
        }
        else
        {
            try
            {
                if (!jobHistoryRelu.getDestinationEdition().isEditionDejaConsulteeParCetUid(uid).booleanValue())
                {
                    jobHistoryRelu.getDestinationEdition().metAJourLaListeDesConsultations(uid);
                    this.jobhistorydao.saveJobHistory(jobHistoryRelu);
                }
                result = this.chargerFichierEdition(jobHistoryRelu);

                if (result == null)
                {
                    StringBuilder   msg = new StringBuilder();
                    msg.append("L'EDITION ");
                    msg.append(jobHistoryRelu.getEditionUuid());
                    msg.append(" N'EXISTE PLUS");
                    throw new ExploitationException(msg.toString());
                }
            }
            catch (HibernateOptimisticLockingFailureException optimisticException)
            {
                log.debug("Exception détectée : ", optimisticException);
                this.attendre(delai);
                return null;
            }
        }

        return result;
    }

    /**
     * Stocke une édition à partir d'un tableau de bytes en mémoire.
     * 
     * @param jobHistory --
     * @param dataBytes --
     */
    private void stockerEditionFromBytesArray(JobHistory jobHistory, byte[] dataBytes)
    {
        Control.assertJobHistoryNotNull(jobHistory);
        Control.assertdatabytesNotNull(dataBytes);

        this.jobhistorydao.saveContenuEditionFromByteArray(jobHistory, dataBytes);
    }

    /**
     * Stocke une édition à partir d'un fichier sur disque.
     * 
     * @param jobHistory --
     * @param fichierJoint --
     */
    private void stockerEditionFromFile(JobHistory jobHistory, FichierJoint fichierJoint)
    {
        Control.assertJobHistoryNotNull(jobHistory);
        Control.assertRefFichierTemporaireNotNull(fichierJoint);
        jobhistorydao.saveContenuEditionFromFile(jobHistory, fichierJoint.getLeContenuDuFichier()
            .getFichierTemporaire());
    }

    /**
     * Supprime de la map des utilisateurs qui sont en cours de chargement d'une édition ceux dont la demande a été
     * effectuée il y a plus de 20 minutes.
     * 
     * @param mapUserEdit --
     * @param instantCourant --
     */
    private void supprimeDemandeEditionDePlusDeVingtMinutes(Map<String, Date> mapUserEdit, Date instantCourant)
    {
        // On supprime les entrées qui correspondent à des chargements
        // démarrés il y a plus de DELAI_PURGE_DEMANDE minutes
        Calendar cal = Calendar.getInstance(Locale.FRANCE);
        cal.setTime(instantCourant);
        cal.add(Calendar.MINUTE, -DELAI_PURGE_DEMANDE);
        Date timeMaxDemande = cal.getTime();

        Set<String> cles = mapUserEdit.keySet();
        for (Iterator<String> iter = cles.iterator(); iter.hasNext();)
        {
            String cleChargement = iter.next();
            Date timeDemande = mapUserEdit.get(cleChargement);

            if (timeMaxDemande.after(timeDemande))
            {
                mapUserEdit.remove(cleChargement);
            }
        }
    }

    /**
     * Supprime une entre dans la map des chargements en cours.
     * 
     * @param editionUuid --
     * @param cleDemandeChargement --
     */
    private void supprimeEntreeApresChargement(String editionUuid, String cleDemandeChargement)
    {
        if (this.editionsEnCoursChargement.containsKey(editionUuid))
        {
            ConcurrentMap<String, Date> mapUserEdit = this.editionsEnCoursChargement.get(editionUuid);

            if (mapUserEdit != null && mapUserEdit.containsKey(cleDemandeChargement))
            {
                mapUserEdit.remove(cleDemandeChargement);
            }
        }
    }

}
