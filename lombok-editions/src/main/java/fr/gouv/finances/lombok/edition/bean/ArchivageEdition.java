/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : ArchivageEdition.java
 *
 */
package fr.gouv.finances.lombok.edition.bean;

import java.util.Date;

import fr.gouv.finances.lombok.util.base.BaseBean;

/**
 * Class ArchivageEdition --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.2 $ Date: 11 déc. 2009
 */
public class ArchivageEdition extends BaseBean
{

    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** Indique le nombre de jours de conservation de l'édition. */
    private Integer nbJStock = Integer.valueOf("0");

    /** Date de purge de l'édition. */
    private Date datePurge;

    /**
     * Instanciation de archivage edition.
     */
    public ArchivageEdition()
    {
        super();
    }

    /**
     * Instanciation de archivage edition.
     * 
     * @param nbJStock --
     */
    public ArchivageEdition(Integer nbJStock)
    {
        super();
        this.nbJStock = nbJStock;
    }

    /**
     * Gets the date purge.
     * 
     * @return the datePurge
     */
    public Date getDatePurge()
    {
        return datePurge;
    }

    /**
     * Gets the nb j stock.
     * 
     * @return the nbJStock
     */
    public Integer getNbJStock()
    {
        return nbJStock;
    }

    /**
     * Sets the date purge.
     * 
     * @param datePurge the datePurge to set
     */
    public void setDatePurge(Date datePurge)
    {
        this.datePurge = datePurge;
    }

    /**
     * Sets the nb j stock.
     * 
     * @param nbJStock the nbJStock to set
     */
    public void setNbJStock(Integer nbJStock)
    {
        this.nbJStock = nbJStock;
    }

}
