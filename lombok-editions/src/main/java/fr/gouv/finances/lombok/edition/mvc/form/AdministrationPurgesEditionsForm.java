/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : AdministrationPurgesEditionsForm.java
 *
 */
package fr.gouv.finances.lombok.edition.mvc.form;

import java.util.Date;

import fr.gouv.finances.lombok.util.base.BaseBean;

/**
 * Class AdministrationPurgesEditionsForm --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.2 $ Date: 11 déc. 2009
 */
public class AdministrationPurgesEditionsForm extends BaseBean
{

    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** date debut purge. */
    private Date dateDebutPurge;

    /** debut periode purge. */
    private Date debutPeriodePurge;

    /** fin periode purge. */
    private Date finPeriodePurge;

    /**
     * Constructeur de la classe AdministrationPurgesEditionsForm.java
     *
     */
    public AdministrationPurgesEditionsForm()
    {
        super();
        
    }

    /**
     * Accesseur de l attribut date debut purge.
     * 
     * @return date debut purge
     */
    public Date getDateDebutPurge()
    {
        return dateDebutPurge;
    }

    /**
     * Accesseur de l attribut debut periode purge.
     * 
     * @return debut periode purge
     */
    public Date getDebutPeriodePurge()
    {
        return debutPeriodePurge;
    }

    /**
     * Accesseur de l attribut fin periode purge.
     * 
     * @return fin periode purge
     */
    public Date getFinPeriodePurge()
    {
        return finPeriodePurge;
    }

    /**
     * Modificateur de l attribut date debut purge.
     * 
     * @param dateDebutPurge le nouveau date debut purge
     */
    public void setDateDebutPurge(Date dateDebutPurge)
    {
        this.dateDebutPurge = dateDebutPurge;
    }

    /**
     * Modificateur de l attribut debut periode purge.
     * 
     * @param debutPeriodePurge le nouveau debut periode purge
     */
    public void setDebutPeriodePurge(Date debutPeriodePurge)
    {
        this.debutPeriodePurge = debutPeriodePurge;
    }

    /**
     * Modificateur de l attribut fin periode purge.
     * 
     * @param finPeriodePurge le nouveau fin periode purge
     */
    public void setFinPeriodePurge(Date finPeriodePurge)
    {
        this.finPeriodePurge = finPeriodePurge;
    }

}
