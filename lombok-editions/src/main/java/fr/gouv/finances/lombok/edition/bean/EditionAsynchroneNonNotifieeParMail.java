/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : EditionAsynchroneNonNotifieeParMail.java
 *
 */
package fr.gouv.finances.lombok.edition.bean;

/**
 * Interface EditionAsynchroneNonNotifieeParMail --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.2 $ Date: 11 déc. 2009
 */
public interface EditionAsynchroneNonNotifieeParMail
{

    /**
     * Accesseur de l attribut archivage edition.
     * 
     * @return archivage edition
     */
    public abstract ArchivageEdition getArchivageEdition();

    /**
     * Accesseur de l attribut bean edition id.
     * 
     * @return bean edition id
     */
    public abstract String getBeanEditionId();

    /**
     * Accesseur de l attribut destination edition.
     * 
     * @return destination edition
     */
    public abstract DestinationEdition getDestinationEdition();

    /**
     * Accesseur de l attribut edition uuid.
     * 
     * @return edition uuid
     */
    public abstract String getEditionUuid();

    /**
     * Accesseur de l attribut ordo edition.
     * 
     * @return ordo edition
     */
    public abstract OrdoEdition getOrdoEdition();

    /**
     * Retourne le statut de l'édition.
     * 
     * @return the statut edition
     */
    public abstract String getStatutEdition();

    /**
     * Indique si l'édition est disponible.
     * 
     * @return boolean
     */
    public abstract boolean isDisponible();

}