/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : AbstractServiceEditionCommunStubImpl.java
 *
 */
package fr.gouv.finances.lombok.edition.service.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import fr.gouv.finances.lombok.edition.service.ServiceEditionCommunStub;

/**
 * Class AbstractServiceEditionCommunStubImpl SuperClasse pour les stubs des datasource jasper.
 * 
 * @author chouard-cp
 * @version $Revision: 1.2 $ Date: 15 déc. 2009
 */
public abstract class AbstractServiceEditionCommunStubImpl implements ServiceEditionCommunStub
{
    /**
     * Instanciation de abstract service edition commun stub impl.
     */
    public AbstractServiceEditionCommunStubImpl()
    {
        super();
    }

    
    /**
     * methode Creates the bean collection : --.
     * 
     * @return collection
     */
    protected static Collection createBeanCollection()
    {
        return new ArrayList();
    }

 
    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @return jR data source
     * @see fr.gouv.finances.lombok.edition.service.ServiceEditionCommunStub#createBeanCollectionDatasource()
     */
    @Override
    public JRDataSource createBeanCollectionDatasource()
    {
        return new JRBeanCollectionDataSource(createBeanCollection());
    }

    /**
     * methode Creer datasource : --.
     * 
     * @param parametresEdition --
     * @return collection
     */
    public Collection creerDatasource(Map parametresEdition)
    {
        return createBeanCollection();
    }

}
