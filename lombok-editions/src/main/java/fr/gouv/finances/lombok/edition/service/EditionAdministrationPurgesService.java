/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : EditionAdministrationPurgesService.java
 *
 */
package fr.gouv.finances.lombok.edition.service;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import fr.gouv.finances.lombok.edition.bean.TrcPurgeEdition;

/**
 * Interface EditionAdministrationPurgesService --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.2 $ Date: 11 déc. 2009
 */
public interface EditionAdministrationPurgesService
{

    /**
     * Demande l'arrêt d'une tâche de purge.
     * 
     * @param trcPurgeEdition --
     */
    public void demanderArretPurgeEdition(TrcPurgeEdition trcPurgeEdition);

    /**
     * Retourne la liste des objets TrcPurgeEdition présents en base.
     * 
     * @param max --
     * @return the list
     */
    public List rechercherPurgesEditions(int max);

    /**
     * Retourne une liste d'objets de type TrcPurgeEdition pour une date donnée.
     * 
     * @param dateDemande --
     * @param max --
     * @return the list
     */
    public List rechercherPurgesEditionsParDateDemande(Date dateDemande, int max);

    /**
     * Retourne une liste d'objets de type TrcPurgeEdition pour une période donnée.
     * 
     * @param debutPeriodePurge --
     * @param finPeriodePurge --
     * @param max --
     * @return the list
     */
    public List rechercherPurgesEditionsParPeriodePurge(Date debutPeriodePurge, Date finPeriodePurge, int max);

    /**
     * Retourne une liste d'objets de type TrcPurgeEdition associé à un purge en cours d'exécution.
     * 
     * @return the list
     */
    public List rechercherPurgesEnCoursExecution();

    /**
     * Supprime la liste des traces de purge passée en paramètres.
     * 
     * @param trcPurgeASupprimer --
     */
    public void supprimerTrcPurgeEdition(Collection trcPurgeASupprimer);
}
