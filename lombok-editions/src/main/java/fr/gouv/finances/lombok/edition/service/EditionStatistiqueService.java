/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : EditionStatistiqueService.java
 *
 */
package fr.gouv.finances.lombok.edition.service;

import java.util.List;

/**
 * Interface EditionStatistiqueService --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.2 $ Date: 11 déc. 2009
 */
public interface EditionStatistiqueService
{

    /**
     * methode Compte job history par date purge : --.
     * 
     * @return list
     */
    public List compteJobHistoryParDatePurge();

    /**
     * methode Compte job history par jour demande : --.
     * 
     * @return list
     */
    public List compteJobHistoryParJourDemande();

    /**
     * methode Compte job history par status : --.
     * 
     * @return list
     */
    public List compteJobHistoryParStatus();

    /**
     * methode Compte job history par type et status : --.
     * 
     * @return list
     */
    public List compteJobHistoryParTypeEtStatus();

    /**
     * methode Compte taille edition par type : --.
     * 
     * @return list
     */
    public List compteTailleEditionParType();
}
