/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : EditionSchedulerService.java
 *
 */
package fr.gouv.finances.lombok.edition.service;

import java.util.Map;

import fr.gouv.finances.lombok.edition.bean.JobHistory;

/**
 * Interface EditionSchedulerService --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.2 $ Date: 11 déc. 2009
 */
public interface EditionSchedulerService
{

    /**
     * Déclenche l'ordre de production d'une édition.
     * 
     * @param jobHistory --
     * @param parametresEdition --
     * @param compression --
     * @return the job history
     */
    public JobHistory declencherEdition(JobHistory jobHistory, Map parametresEdition, Boolean compression);

    /**
     * Supprime un job de l'ordonnanceur.
     * 
     * @param jobHistory --
     */
    public void supprimerJob(JobHistory jobHistory);

    /**
     * Teste l'existence d'un job dans l'ordonnanceur.
     * 
     * @param jobHistory --
     * @return true, if tester existence job edition
     */
    public boolean testerExistenceJobEdition(JobHistory jobHistory);
}
