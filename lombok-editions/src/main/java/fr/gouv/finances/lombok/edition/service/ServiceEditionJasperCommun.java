/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : ServiceEditionJasperCommun.java
 *
 */
package fr.gouv.finances.lombok.edition.service;

import java.util.Collection;
import java.util.Map;

import org.springframework.web.servlet.view.jasperreports.JasperReportsMultiFormatView;

import fr.gouv.finances.lombok.edition.bean.AbstractParametrageVueJasper;
import fr.gouv.finances.lombok.upload.bean.FichierJoint;

/**
 * Interface ServiceEditionJasperCommun --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.3 $ Date: 11 déc. 2009
 */
public interface ServiceEditionJasperCommun
{

    /**
     * Retourne le répertoire de stockage des sous-rapports communs à plusieurs rapports.
     * 
     * @return communsubreportdir
     */
    public String getCommunsubreportdir();

    /**
     * Retourne la vue jasperreport.
     * 
     * @return the editionview
     */
    public JasperReportsMultiFormatView getEditionview();

    /**
     * Retourne le répertoire de stockage des images.
     * 
     * @return the images source dir
     */
    public String getImagesSourceDir();

    /**
     * Affecte le paramétrage de la vue jasper utilisée.
     * 
     * @return the parametrage vue jasper
     */
    public AbstractParametrageVueJasper getParametrageVueJasper();

    /**
     * Retourne le répertoire de stockage des sous-rapports.
     * 
     * @return the subreportdir
     */
    public String getSubreportdir();

    /**
     * Produit une édition à partir d'une collection passée en paramètres.
     * 
     * @param data --
     * @param parametresEdition --
     * @return the fichier joint
     */
    public FichierJoint produireFichierEditionFromCollection(Collection data, Map parametresEdition);

    /**
     * Produit une édition au format HTML à partir d'une collection de données passée en paramètres.
     * 
     * @param data --
     * @param parametresEdition --
     * @return the map
     */
    public Map produitEditionHtmlFromCollection(Collection data, Map parametresEdition);

    /**
     * Affecte le répertoire de stockage des sous-rapports communs à plusieurs rapports.
     * 
     * @param communsubreportdir le nouveau communsubreportdir
     */
    public void setCommunsubreportdir(String communsubreportdir);

    /**
     * Affecte la vue jasperreport.
     * 
     * @param editionview --
     */
    public void setEditionview(JasperReportsMultiFormatView editionview);

    /**
     * Affecte le répertoire de stockage des images.
     * 
     * @param imagesSourceDir --
     */
    public void setImagesSourceDir(String imagesSourceDir);

    /**
     * Retourne le paramétrage de la vue Jasper utilisée.
     * 
     * @param parametrageVueJasper --
     */
    public void setParametrageVueJasper(AbstractParametrageVueJasper parametrageVueJasper);

    /**
     * Affecte le répertoire de stockage des sous-rapports.
     * 
     * @param subreportdir --
     */
    public void setSubreportdir(String subreportdir);

}
