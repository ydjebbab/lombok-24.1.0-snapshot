/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 */
package fr.gouv.finances.lombok.edition.dao;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import fr.gouv.finances.lombok.edition.bean.TrcPurgeEdition;
import fr.gouv.finances.lombok.util.base.CoreBaseDao;

/**
 * Interface du DAO de gestion des données des traces de purge d'édition.
 *
 * @author amleplatinec
 * @author Christophe Breheret-Girardin
 */
public interface TrcPurgeEditionDao extends CoreBaseDao
{

    /**
     * Méthode permettant de savoir si une trace de purge d'édition est déjà présente à la même date que celle
     * fournie en paramètre.
     * 
     * @param trcPurgeEdition trace de purge d'édition à comparer
     * @return true si une autre trace de purge d'édition existe à cette date, false sinon
     */
    public boolean checkExistencePurgeDateIdentique(TrcPurgeEdition trcPurgeEdition);

    /**
     * Méthode permettant de supprimer une trace de purge d'édition.
     * 
     * @param trcPurgeEdition trace de purge d'édition à supprimer
     */
    public void deleteTrcPurgeEdition(TrcPurgeEdition trcPurgeEdition);

    /**
     * Méthode permettant de supprimer une liste de trace de purge d'édition.
     *
     * @param lesTrcPurgesEditionSupp liste de trace de purge d'édition à supprimer
     */
    public void deleteTrcPurgeEditions(Collection<TrcPurgeEdition> lesTrcPurgesEditionSupp);

    /**
     * Méthode permettant de récupérer les traces de purge d'édition, en cours d'exécution, associée
     * à une application donnée, pour une date antérieure à celle fournie.
     * 
     * @param dateDebutPurge critère de date
     * @param applicationOrigine application associée
     * @return la liste des traces de purge  satisfaisant les critères
     */
    public List<TrcPurgeEdition> findLesTrcPurgeEditionEnCoursAnterieuresAUneDate(Date dateDebutPurge,
        String applicationOrigine);

    /**
     * Méthode permettant de récupérer les traces de purge d'édition, associées à une application
     * et une date données.
     * 
     * @param dateDemande date de la demande
     * @param applicationOrigine application associée
     * @param max nombre de trace maximum à récupérer
     * @return la liste des traces de purge d'édition satisfaisant les critères, triée sur la date
     */
    public List<TrcPurgeEdition> findLesTrcPurgeEditionPourUneDate(Date dateDemande, String applicationOrigine
        , int max);

    /**
     * Méthode permettant de récupérer les traces de purge d'édition, associées à une application et une
     * date de démarrage données.
     * 
     * @param instantDemarrage date de démarrage
     * @param applicationOrigine application associée
     * @return la liste des traces de purge d'édition satisfaisant les critères
     */
    public List<TrcPurgeEdition> findLesTrcPurgeEditionPourUnInstantDemarrage(Date instantDemarrage,
        String applicationOrigine);

    /**
     * Méthode permettant de récupérer toutes les traces de purge des éditions, associée à une application.
     * 
     * @param applicationOrigine application associée
     * @param max nombre de trace maximum à récupérer
     * @return la liste des traces de purge d'édition satisfaisant les critères, triée sur la date
     */
    public List<TrcPurgeEdition> findTousLesTrcPurgeEdition(String applicationOrigine, int max);

    /**
     * Méthode permettant de récupérer les traces de purge des éditions, en cours d'exécution,
     * associée à une application.
     * 
     * @param applicationOrigine application associée
     * @return la liste des traces de purge d'édition satisfaisant les critères
     */
    public List<TrcPurgeEdition> findTrcPurgeEditionEnCours(String applicationOrigine);

    /**
     * Méthode permettant de récupérer les traces de purge des éditions, associée à une application,
     * dont la période se situe entre deux dates fournies.
     * 
     * @param debutPeriodePurge date de début de période de purge
     * @param finPeriodePurge date de fin de période de purge
     * @param applicationOrigine application associée
     * @param max nombre de trace maximum à récupérer
     * @return la liste des traces de purge d'édition satisfaisant les critères, triée par date 
     */
    public List<TrcPurgeEdition> findTrcPurgeEditionParPeriodePurge(Date debutPeriodePurge, Date finPeriodePurge,
        String applicationOrigine, int max);

    /**
     * Méthode permettant de savoir si un batch de purge est en cours d'exécution, associé à une application.
     *
     * @param applicationOrigine application associée
     * @return true si un batch de purge est en cours d'exécution, false sinon
     */
    public boolean isUnBatchPurgeEnCoursExecution(String applicationOrigine);

    /**
     * Méthode permettant de créer une trace de purge d'édition.
     * 
     * @param trcPurgeEdition trace de purge d'édition à créer
     */
    public void saveTrcPurgeEdition(TrcPurgeEdition trcPurgeEdition);
}
