/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : EditionAdministrationService.java
 *
 */
package fr.gouv.finances.lombok.edition.service;

import java.util.Date;
import java.util.List;

/**
 * Interface EditionAdministrationService --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.2 $ Date: 11 déc. 2009
 */
public interface EditionAdministrationService
{

    /**
     * Retourne la liste des descriptions des éditions disponibles.
     * 
     * @return the list
     */
    public List rechercherEditionBeanIds();

    /**
     * Retourne la liste des éditions pour une description donnée.
     * 
     * @param description --
     * @return the list
     */
    public List rechercherEditionParBeanId(String description);

    /**
     * Retourne la liste des éditions dont la demande a été effectuée à une date donnée.
     * 
     * @param dateDemande --
     * @return the list
     */
    public List rechercherEditionParDateDemande(Date dateDemande);

    /**
     * Retourne une liste contenant l'édition identifiée par le paramètre editionuuid, si elle existe.
     * 
     * @param editionuuid --
     * @return the list
     */
    public List rechercherEditionParEditionUuid(String editionuuid);

    /**
     * Retourne la liste des éditions qui sont dans un état donné.
     * 
     * @param etat --
     * @return the list
     */
    public List rechercherEditionParEtat(String etat);

    /**
     * Retourne la liste des éditions dont la demande a été effectuée durant la période délimitée par les paramètres de
     * début et de fin de période.
     * 
     * @param debutPeriodeDemande --
     * @param finPeriodeDemande --
     * @return the list
     */
    public List rechercherEditionParPeriodeDateDemande(Date debutPeriodeDemande, Date finPeriodeDemande);

    /**
     * Retourne la liste des éditions accessibles à un profil donné.
     * 
     * @param profil --
     * @return the list
     */
    public List rechercherEditionParProfil(String profil);

    /**
     * Retourne la liste des éditions appartenant à l'utilisateur identifié par son uid.
     * 
     * @param uid --
     * @return List de JobHistory
     */
    public List rechercherEditionParUid(String uid);

    /**
     * Retourne la liste des profils autorisant l'accès à des éditions produites.
     * 
     * @return the list
     */
    public List rechercherProfils();

}
