/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : ContenuEdition.java
 *
 */
package fr.gouv.finances.lombok.edition.bean;

import java.io.File;
import java.util.Arrays;

import fr.gouv.finances.lombok.util.base.BaseBean;

/**
 * Class ContenuEdition --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.3 $ Date: 11 déc. 2009
 */
public class ContenuEdition extends BaseBean
{

    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** id. */
    private Long id;

    /** version. */
    private int version;

    /** Blob contenant les données de l'édition. */
    private byte[] data;

    /**
     * Sert à stocker temporairement en mémoire les données de l'attribut dataBlob lorsque est suffisamment petit.
     * Propriété non persistante en base de données.
     */
    private byte[] dataByte;

    /**
     * Sert à stocker temporairement dans le système de fichier les données de l'attribut dataBlob lorsque celui-ci est
     * trop grand pour être stocké en mémoire Propriété non persistante en base de données.
     */
    private File dataFile;

    /**
     * Constructeur.
     */
    public ContenuEdition()
    {
        super();
    }

    /** 
     * {@inheritDoc}
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object object)
    {
        if (object == this)
        {
            return true;
        }
        if (object == null || this.getClass() != object.getClass())
        {
            return false;
        }
        ContenuEdition contenuEdition = (ContenuEdition) object;
        return Arrays.equals(dataByte, contenuEdition.dataByte);
    }

    /** 
     * {@inheritDoc}
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode()
    {
        return Arrays.hashCode(dataByte);
    }

    /**
     * Accesseur de l attribut data.
     * 
     * @return data
     */
    public byte[] getData()
    {
        return data;
    }

    /**
     * Accesseur de l attribut data byte.
     * 
     * @return data byte
     */
    public byte[] getDataByte()
    {
        return dataByte;
    }

    /**
     * Accesseur de l attribut data file.
     * 
     * @return data file
     */
    public File getDataFile()
    {
        return dataFile;
    }

    /**
     * Accesseur de l attribut id.
     * 
     * @return id
     */
    public Long getId()
    {
        return id;
    }

    /**
     * Accesseur de l attribut version.
     * 
     * @return version
     */
    public int getVersion()
    {
        return version;
    }

    /**
     * Modificateur de l attribut data.
     * 
     * @param dataBlob le nouveau data
     */
    public void setData(byte[] dataBlob)
    {
        this.data = dataBlob;
    }

    /**
     * Modificateur de l attribut data byte.
     * 
     * @param data le nouveau data byte
     */
    public void setDataByte(byte[] data)
    {
        this.dataByte = data;
    }

    /**
     * Modificateur de l attribut data file.
     * 
     * @param dataFile le nouveau data file
     */
    public void setDataFile(File dataFile)
    {
        this.dataFile = dataFile;
    }

    /**
     * Modificateur de l attribut id.
     * 
     * @param id le nouveau id
     */
    public void setId(Long id)
    {
        this.id = id;
    }

    /**
     * Modificateur de l attribut version.
     * 
     * @param version le nouveau version
     */
    public void setVersion(int version)
    {
        this.version = version;
    }

}
