/*
 * Copyright (c) 2016 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.edition.service.impl;

import java.util.Collection;
import java.util.Map;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ApplicationObjectSupport;

import fr.gouv.finances.lombok.edition.bean.EditionAsynchroneNonNotifieeParMail;
import fr.gouv.finances.lombok.edition.bean.EditionAsynchroneNotifieeParMail;
import fr.gouv.finances.lombok.edition.bean.EditionSynchroneNonNotifieeParMail;
import fr.gouv.finances.lombok.edition.bean.EditionSynchroneNotifieeParMail;
import fr.gouv.finances.lombok.edition.bean.JobHistory;
import fr.gouv.finances.lombok.edition.service.EditionProducerService;
import fr.gouv.finances.lombok.edition.service.ServiceEditionCommun;
import fr.gouv.finances.lombok.upload.bean.FichierJoint;
import fr.gouv.finances.lombok.util.exception.ProgrammationException;

/**
 * Class EditionProducerServiceEaiImpl. Remplace la classe
 * fr.gouv.finances.lombok.edition.service.impl.EditionProducerServiceImpl
 */
public class EditionProducerServiceSansBaseImpl extends ApplicationObjectSupport implements EditionProducerService
{

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.edition.service.EditionProducerService#creerEdition(java.lang.String, java.util.Map)
     */
    @Override
    public FichierJoint creerEdition(String editionBeanId, Map parametresEdition)
    {
        FichierJoint result = null;
        ServiceEditionCommun serviceEdition = this.getReferenceServiceEditionCommun(editionBeanId);

        result = serviceEdition.produireFichierEdition(parametresEdition);
        return result;
    }

    /**
     * Lit dans le contexte d'édition la référence du bean editionBeanId et la retourne dans le type
     * ServiceEditionCommun.
     *
     * @param editionBeanId editionBeanId
     * @return reference service edition commun
     */
    private ServiceEditionCommun getReferenceServiceEditionCommun(String editionBeanId)
    {
        ServiceEditionCommun serviceEdition = null;

        Object object = this.getReferenceBeanId(editionBeanId);

        if (object instanceof ServiceEditionCommun)
        {
            serviceEdition = (ServiceEditionCommun) object;
        }
        else
        {
            throw new ProgrammationException("LE BEAN " + editionBeanId + " N'EST PAS DE TYPE SERVICEEDITIONCOMMUN");
        }
        return serviceEdition;
    }

    /**
     * Lecture dans le contexte d'application de la référence identifiée l'id du bean.
     *
     * @param editionBeanId editionBeanId
     * @return reference bean id
     */
    private Object getReferenceBeanId(String editionBeanId)
    {
        Object editionBeanObject = null;
        ApplicationContext appliContext = this.getApplicationContext();

        if (appliContext == null)
        {
            throw new ProgrammationException("LE CONTEXTE DE L'APPLICATION EST NULL");
        }
        else
        {
            if (appliContext.containsBean(editionBeanId))
            {

                editionBeanObject = appliContext.getBean(editionBeanId);
            }
            else
            {
                throw new ProgrammationException("LE BEAN " + editionBeanId
                    + " N'EST PAS DEFINI DANS LE CONTEXTE D'EDITION.");
            }
        }
        return editionBeanObject;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.edition.service.EditionProducerService#creerEditionCompressee(java.lang.String,
     *      java.util.Map)
     */
    @Override
    public FichierJoint creerEditionCompressee(String editionBeanId, Map parametresEdition)
    {
        return null;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.edition.service.EditionProducerService#creerEditionFromCollectionEtParametres(java.lang.String,
     *      java.util.Collection, java.util.Map)
     */
    @Override
    public FichierJoint creerEditionFromCollectionEtParametres(String editionBeanId, Collection collection, Map mapParametres)
    {
        return null;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.edition.service.EditionProducerService#creerEditionHtml(java.lang.String,
     *      java.util.Map)
     */
    @Override
    public Map creerEditionHtml(String editionBeanId, Map mapParametres)
    {
        return null;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.edition.service.EditionProducerService#creerEditionHtmlFromCollectionEtParametres(java.lang.String,
     *      java.util.Collection, java.util.Map)
     */
    @Override
    public Map creerEditionHtmlFromCollectionEtParametres(String editionBeanId, Collection collection, Map mapParametres)
    {
        return null;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.edition.service.EditionProducerService#creerEtStockerEdition(fr.gouv.finances.lombok.edition.bean.JobHistory,
     *      java.util.Map)
     */
    @Override
    public void creerEtStockerEdition(JobHistory jobHistory, Map parametresEdition)
    {
        // RAS
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.edition.service.EditionProducerService#creerEtStockerEditionCompressee(fr.gouv.finances.lombok.edition.bean.JobHistory,
     *      java.util.Map)
     */
    @Override
    public void creerEtStockerEditionCompressee(JobHistory jobHistory, Map parametresEdition)
    {
        // RAS
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.edition.service.EditionProducerService#enregistrerDeclenchement(fr.gouv.finances.lombok.edition.bean.JobHistory,
     *      java.lang.String)
     */
    @Override
    public JobHistory enregistrerDeclenchement(JobHistory jobHistory, String status)
    {
        return null;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.edition.service.EditionProducerService#initEditionAsynchroneAvecDelaiNonNotifieeParMail(java.lang.String,
     *      java.lang.String, java.lang.Integer)
     */
    @Override
    public EditionAsynchroneNonNotifieeParMail initEditionAsynchroneAvecDelaiNonNotifieeParMail(String beanIdEdition,
        String uidProprietaire, Integer delai)
    {
        return null;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.edition.service.EditionProducerService#initEditionAsynchroneAvecDelaiNotifieeParMail(java.lang.String,
     *      java.lang.String, java.lang.String, java.lang.Integer)
     */
    @Override
    public EditionAsynchroneNotifieeParMail initEditionAsynchroneAvecDelaiNotifieeParMail(String beanIdEdition, String uidProprietaire,
        String mailDestinataire, Integer delai)
    {
        return null;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.edition.service.EditionProducerService#initEditionAsynchroneNonNotifieeParMail(java.lang.String,
     *      java.lang.String)
     */
    @Override
    public EditionAsynchroneNonNotifieeParMail initEditionAsynchroneNonNotifieeParMail(String beanIdEdition, String uidProprietaire)
    {
        return null;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.edition.service.EditionProducerService#initEditionAsynchroneNotifieeParMail(java.lang.String,
     *      java.lang.String, java.lang.String)
     */
    @Override
    public EditionAsynchroneNotifieeParMail initEditionAsynchroneNotifieeParMail(String beanIdEdition, String uidProprietaire,
        String mailDestinataire)
    {
        return null;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.edition.service.EditionProducerService#initEditionSynchroneNonNotifieeParMail(java.lang.String,
     *      java.lang.String)
     */
    @Override
    public EditionSynchroneNonNotifieeParMail initEditionSynchroneNonNotifieeParMail(String beanIdEdition, String uidProprietaire)
    {
        return null;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.edition.service.EditionProducerService#initEditionSynchroneNotifieeParMail(java.lang.String,
     *      java.lang.String, java.lang.String)
     */
    @Override
    public EditionSynchroneNotifieeParMail initEditionSynchroneNotifieeParMail(String beanIdEdition, String uidProprietaire,
        String mailDestinataire)
    {
        return null;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.edition.service.EditionProducerService#initMessageMailPourAccesAvecOuSansPortailIntranet(fr.gouv.finances.lombok.edition.bean.JobHistory)
     */
    @Override
    public void initMessageMailPourAccesAvecOuSansPortailIntranet(JobHistory jobHistory)
    {
        // RAS
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.edition.service.EditionProducerService#initMessageMailPourAccesViaPortailIntranet(fr.gouv.finances.lombok.edition.bean.JobHistory)
     */
    @Override
    public void initMessageMailPourAccesViaPortailIntranet(JobHistory jobHistory)
    {
        // RAS
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.edition.service.EditionProducerService#initMessageMailPourAccesViaPortailIntranetOuInternet(fr.gouv.finances.lombok.edition.bean.JobHistory)
     */
    @Override
    public void initMessageMailPourAccesViaPortailIntranetOuInternet(JobHistory jobHistory)
    {
        // RAS
    }
}
