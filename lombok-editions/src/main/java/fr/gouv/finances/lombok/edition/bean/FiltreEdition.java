/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 */
package fr.gouv.finances.lombok.edition.bean;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import fr.gouv.finances.lombok.util.base.BaseBean;

/**
 * Les méthodes hashCode et equals s'appuient uniquement sur le nom du filtre.<br/>
 * Les objets "FiltreEdition" ne peuvent être utilisés que dans le contexte d'un objet ProfilDesti.<br/>
 * Ils doivent se comporter comme un composant de ProfilDesti.<br/>
 * En raison d'une limitation de l'interface criteria de l'ORM, Le lien entre ProfilDesti<br/>
 * et la collection de FiltreEdition est défini sous forme d'une association alors que l'on souhaiterait<br>
 * utiliser une collection de composants.
 * 
 * @author wpetit-cp
 * @author Christophe Breheret-Girardin
 */
public class FiltreEdition extends BaseBean
{
    /** Initialisation de l'UID. */
    private static final long serialVersionUID = 1L;

    /** Identifiant technique utilisé par l'ORM. */
    private Long id;

    /** Flag utilisé pour la gestion du verrouillage optimiste par l'ORM. */
    private int version;

    /** Nom du filtre. */
    private String nomDuFiltre;

    /** Autorisation. */
    private boolean allow;

    /** Refus. */
    private boolean deny;

    /** Valeurs du filtre. */
    private Set<FiltreValue> valsFiltre;

    /**
     * Constructeur.
     */
    public FiltreEdition()
    {
        super();
    }

    /**
     * Instanciation de filtre edition.
     * 
     * @param nomDuFiltre --
     */
    public FiltreEdition(String nomDuFiltre)
    {
        super();
        this.nomDuFiltre = nomDuFiltre;
    }

    /**
     * Affecte une valeur à un filtre.
     * 
     * @param uneValeurDuFiltre --
     * @return FiltreEdition
     */
    public FiltreEdition affecterLaValeur(String uneValeurDuFiltre)
    {
        FiltreValue filtreValue = new FiltreValue(uneValeurDuFiltre);
        if (this.valsFiltre == null)
        {
            this.valsFiltre = new HashSet<FiltreValue>();
        }

        if (uneValeurDuFiltre != null)
        {
            this.valsFiltre.add(filtreValue);
        }

        return this;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param obj
     * @return true, si c'est vrai
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
        {
            return true;
        }
        if (obj == null)
        {
            return false;
        }
        if (getClass() != obj.getClass())
        {
            return false;
        }
        final FiltreEdition other = (FiltreEdition) obj;
        if (nomDuFiltre == null)
        {
            if (other.nomDuFiltre != null)
            {
                return false;
            }
        }
        else if (!nomDuFiltre.equals(other.nomDuFiltre))
        {
            return false;
        }
        return true;
    }

    /**
     * Accesseur de l'attribut id.
     * 
     * @return id
     */
    public Long getId()
    {
        return id;
    }

    /**
     * Accesseur de l'attribut nom du filtre.
     * 
     * @return nom du filtre
     */
    public String getNomDuFiltre()
    {
        return nomDuFiltre;
    }

    /**
     * Accesseur de l'attribut nom et valeurs.
     * 
     * @return nom et valeurs
     */
    public String getNomEtValeurs()
    {
        StringBuilder   nomEtvaleurs = new StringBuilder();
        nomEtvaleurs.append(this.nomDuFiltre);
        if (this.valsFiltre != null)
        {
            nomEtvaleurs.append(" (");
            int compteur = 0;
            for (Iterator iterValsFiltre = valsFiltre.iterator(); iterValsFiltre.hasNext();)
            {
                FiltreValue filtreValue = (FiltreValue) iterValsFiltre.next();
                nomEtvaleurs.append(filtreValue.getValue());
                compteur++;
                if (compteur < valsFiltre.size())
                {
                    nomEtvaleurs.append(" , ");
                }
            }
            nomEtvaleurs.append(" )");
        }
        return nomEtvaleurs.toString();
    }

    /**
     * Accesseur des valeurs du filtre.
     * 
     * @return les valeurs du filtre
     */
    public Set<FiltreValue> getValsFiltre()
    {
        return valsFiltre;
    }

    /**
     * Accesseur de l'attribut vals filtre string.
     * 
     * @return vals filtre string
     */
    public Set<String> getValsFiltreString()
    {
        Set<String> filtreValsString = new HashSet<>();
        if (valsFiltre != null)
        {
            for (Iterator iterfiltreVals = this.valsFiltre.iterator(); iterfiltreVals.hasNext();)
            {
                FiltreValue filtreValue = (FiltreValue) iterfiltreVals.next();
                filtreValsString.add(filtreValue.getValue());
            }
        }
        return filtreValsString;
    }

    /**
     * Accesseur de l'attribut version.
     * 
     * @return version
     */
    public int getVersion()
    {
        return version;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @return int
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;

        result = prime * result + ((nomDuFiltre == null) ? 0 : nomDuFiltre.hashCode());
        return result;
    }

    /**
     * Verifie si allow.
     * 
     * @return true, si c'est allow
     */
    public boolean isAllow()
    {
        return allow;
    }

    /**
     * Verifie si deny.
     * 
     * @return true, si c'est deny
     */
    public boolean isDeny()
    {
        return deny;
    }

    /**
     * Modificateur de l'attribut allow.
     * 
     * @param allow le nouveau allow
     */
    public void setAllow(boolean allow)
    {
        this.allow = allow;
    }

    /**
     * Modificateur de l'attribut deny.
     * 
     * @param deny le nouveau deny
     */
    public void setDeny(boolean deny)
    {
        this.deny = deny;
    }

    /**
     * Modificateur de l'attribut id.
     * 
     * @param id le nouveau id
     */
    public void setId(Long id)
    {
        this.id = id;
    }

    /**
     * Modificateur de l'attribut nom du filtre.
     * 
     * @param nomDuFiltre le nouveau nom du filtre
     */
    public void setNomDuFiltre(String nomDuFiltre)
    {
        this.nomDuFiltre = nomDuFiltre;
    }

    /**
     * Modification de la valeur du filtre.
     * 
     * @param valsFiltre valeur du filtre à modifier
     */
    public void setValsFiltre(Set<FiltreValue> valsFiltre)
    {
        this.valsFiltre = valsFiltre;
    }

    /**
     * Modificateur de l'attribut version.
     * 
     * @param version le nouveau version
     */
    public void setVersion(int version)
    {
        this.version = version;
    }
}
