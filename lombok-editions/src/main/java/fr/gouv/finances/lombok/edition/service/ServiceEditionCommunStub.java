/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : ServiceEditionCommunStub.java
 *
 */
package fr.gouv.finances.lombok.edition.service;

import net.sf.jasperreports.engine.JRDataSource;

/**
 * Interface ServiceEditionCommunStub --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.2 $ Date: 11 déc. 2009
 */
public interface ServiceEditionCommunStub
{

    /**
     * methode Creates the bean collection datasource : --.
     * 
     * @return jR data source
     */
    public JRDataSource createBeanCollectionDatasource();
}
