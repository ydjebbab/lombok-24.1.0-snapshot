/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 */
package fr.gouv.finances.lombok.edition.dao;

/**
 * Propriétés liées aux statistiques des travaux d'édition.
 * @see fr.gouv.finances.lombok.edition.techbean.StatJobHistory
 *
 * @author Christophe Breheret-Girardin
 */
public enum ProprieteStatistiqueJobHistoryEdition
{
    /** Application à laquelle est rattachée l'édition. */
    APPLICATION_ORIGINE("applicationOrigine"),
    /** Date de purge de l'édition. */
    DATE_PURGE("datePurge"),
    /** UUID de l'édition. */
    UID("editionUuid"),
    /** Nombre d'édition. */
    NOMBRE_EDITIONS("nombreEditions"),
    /** Date de demande de l'édition. */
    JOUR_DEMANDE_EDITION("jourDemande"),
    /** Statut de l'édition. */
    STATUT_EDITION("status"),
    /** Identifiant de l'édition. */
    ID_EDITION("beanEditionId"),
    /** Taille du fichier de l'édition. */
    TAILLE_FICHIER_EDITION("tailleFichier"),
    /** Taille du fichier de l'édition. */
    TAILLE_TOTALE_FICHIER("tailleTotale"),
    /** Taille du fichier de l'édition. */
    TAILLE_MOYENNE_FICHIER("tailleMoyenne"),
    /** Informations sur la destination de l'édition. */
    DESTINATION_EDITION("destinationEdition"),
    /** Informations d'archivage de l'édition. */
    ARCHIVAGE_EDITION("archivageEdition"),
    /** Informations d'ordonnancement de l'édition. */
    ORDONNANCEMENT_EDITION("ordoEdition"),
    /** Informations de stockage de l'édition. */
    STOCKAGE_EDITION("stockageEdition");

    /** Nom de la propriété */
    private String nom;

    /**
     * Constructeur.
     *
     * @param libelleNom libellé du nom de la propriété
     */
    private ProprieteStatistiqueJobHistoryEdition(String libelleNom)
    {
        nom = libelleNom;
    }

    /**
     * Accesseur de nom
     *
     * @return nom de la propriété
     */
    public String getNom()
    {
        return nom;
    }
}
