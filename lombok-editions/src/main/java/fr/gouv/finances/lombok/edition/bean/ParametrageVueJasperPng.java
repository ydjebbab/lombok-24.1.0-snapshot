/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 */
package fr.gouv.finances.lombok.edition.bean;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Paramétrage Jasper de la vue PNG.
 * 
 * @author amleplatinec
 * @author Christophe Breheret-Girardin
 */
public class ParametrageVueJasperPng extends AbstractParametrageVueJasper
{
    /** Initialisation de la journalisation. */
    protected static final Logger LOGGER = LoggerFactory.getLogger(ParametrageVueJasperPng.class);

    /** Initialisation de l'UID. */
    private static final long serialVersionUID = 1L;

    /** Constant : DEF_FORMAT_EDITION_VALUE. */
    private static final String DEF_FORMAT_EDITION_VALUE = "png";

    /** Constant : DEF_EXTENSION. */
    private static final String DEF_EXTENSION = "png";

    /** Constant : DEF_CHARACTER_ENCODING. */
    private static final String DEF_CHARACTER_ENCODING = "UTF-8";

    /** Constant : DEF_CONTENT_TYPE. */
    private static final String DEF_CONTENT_TYPE = "image/png";

    /** Constant : DEF_MIME_TYPE. */
    private static final String DEF_MIME_TYPE = "image/png";

    /** Constant : ZOOM_RATIO. */
    private static final String ZOOM_RATIO =
        "net.sf.jasperreports.engine.export.JRGraphics2DExporterParameter.ZOOM_RATIO";

    /** Constant : MINI_PRINT_JOB_SIZE. */
    private static final String MINI_PRINT_JOB_SIZE =
        "net.sf.jasperreports.engine.export.JRGraphics2DExporterParameter.MINIMIZE_PRINTER_JOB_SIZE";

    /** Constant : DEF_ZOOM_RATIO. */
    private static final Float DEF_ZOOM_RATIO = new Float(3);

    /** Constant : DEF_MINI_PRINT_JOB_SIZE. */
    private static final Boolean DEF_MINI_PRINT_JOB_SIZE = Boolean.FALSE;

    /** zoom ratio. */
    private Float zoomRatio;

    /** minimize printer job size. */
    private Boolean minimizePrinterJobSize;

    /**
     * Construit un objet ParametrageVueJasperPng et lui affecte les paramètres par défaut.
     */
    public ParametrageVueJasperPng()
    {
        super();
        this.formatEdition = DEF_FORMAT_EDITION_VALUE;
        this.characterEncoding = DEF_CHARACTER_ENCODING;
        this.extension = DEF_EXTENSION;
        this.contentType = DEF_CONTENT_TYPE;
        this.typeMimeEdition = DEF_MIME_TYPE;

        // Spécifique png
        this.zoomRatio = DEF_ZOOM_RATIO;
        this.minimizePrinterJobSize = DEF_MINI_PRINT_JOB_SIZE;
    }

    /**
     * Gets the minimize printer job size.
     * 
     * @return the minimizePrinterJobSize
     */
    public Boolean getMinimizePrinterJobSize()
    {
        return minimizePrinterJobSize;
    }

    /**
     * Gets the zoom ratio.
     * 
     * @return the zoomRatio
     */
    public Float getZoomRatio()
    {
        return zoomRatio;
    }

    /**
     * Produit une Map contenant le paramétrage de l'exporteur Jasper.
     *
     * @return le paramétrage de l'exporteur Jasper.
     */  
    @Override
    public Map<String, Object> parametrageJRExporter()
    {
        // Déclaration d'une Map qui contiendra le paramétrage
        Map<String, Object> mapModele = new HashMap<>();

        // Paramétrage général de l'exporteur Jasper
        mapModele.put(CHARACTER_ENCODING, this.characterEncoding);

        // Paramétrage spécifique PNG
        putIfNotNull(mapModele, ZOOM_RATIO, this.zoomRatio);
        putIfNotNull(mapModele, MINI_PRINT_JOB_SIZE, this.minimizePrinterJobSize);

        // Journalisation du paramétrage
        journaliserParametrageJRExporter(mapModele);

        return mapModele;
    }

    /**
     * Produit une Map contenant le paramétrage de la vue jasper en utilisant : - soit les valeurs par défaut, - soit
     * les valeurs contenues dans le propriétés de l'objet ParametrageVueJasperPng (valeurs injectée dans le fichier de
     * configuration des éditions).
     * 
     * @return le paramétrage de la vue Jasper
     */
    @Override
    public Map<String, String> parametrerVue()
    {
        // Déclaration d'une Map qui contiendra le paramétrage
        Map<String, String> mapModele = new HashMap<>();

        // Initialisation du paramétrage
        putIfNotNull(mapModele, REPORT_VIEW_KEY, this.formatEdition);
        putIfNotNull(mapModele, EXTENSION_KEY, this.extension);
        putIfNotNull(mapModele, CONTENT_TYPE_KEY, this.contentType);
        putIfNotNull(mapModele, MIME_TYPE_KEY, this.typeMimeEdition);

        // Journalisation du paramétrage
        journaliserParametrageVue(mapModele);

        return mapModele;
    }

    /**
     * Sets the minimize printer job size.
     * 
     * @param minimizePrinterJobSize the minimizePrinterJobSize to set
     */
    public void setMinimizePrinterJobSize(Boolean minimizePrinterJobSize)
    {
        this.minimizePrinterJobSize = minimizePrinterJobSize;
    }

    /**
     * Sets the zoom ratio.
     * 
     * @param zoomRatio the zoomRatio to set
     */
    public void setZoomRatio(Float zoomRatio)
    {
        this.zoomRatio = zoomRatio;
    }
}
