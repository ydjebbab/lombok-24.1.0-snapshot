/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 */
package fr.gouv.finances.lombok.edition.bean;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class ParametrageVueJasperOdt --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.2 $ Date: 11 déc. 2009
 */
public class ParametrageVueJasperOdt extends AbstractParametrageVueJasper
{
    /** Initialisation de la journalisation. */
    protected static final Logger LOGGER = LoggerFactory.getLogger(ParametrageVueJasperOdt.class);

    /** Initialisation de l'UID. */
    private static final long serialVersionUID = 1L;

    /** Constant : DEF_FORMAT_EDITION_VALUE. */
    private static final String DEF_FORMAT_EDITION_VALUE = "odt";

    /** Constant : DEF_EXTENSION. */
    private static final String DEF_EXTENSION = "ods";

    /** Constant : DEF_CHARACTER_ENCODING. */
    private static final String DEF_CHARACTER_ENCODING = "UTF-8";

    /** Constant : DEF_CONTENT_TYPE. */
    private static final String DEF_CONTENT_TYPE = "application/vnd.oasis.opendocument.spreadsheet;charset=UTF-8";

    /** Constant : DEF_MIME_TYPE. */
    private static final String DEF_MIME_TYPE = "application/vnd.oasis.opendocument.spreadsheet";

    /**
     * Construit un objet ParametrageVueJasperOdt et lui affecte les paramètres par défaut.
     */
    public ParametrageVueJasperOdt()
    {
        super();

        this.formatEdition = DEF_FORMAT_EDITION_VALUE;
        this.characterEncoding = DEF_CHARACTER_ENCODING;
        this.extension = DEF_EXTENSION;
        this.contentType = DEF_CONTENT_TYPE;
        this.typeMimeEdition = DEF_MIME_TYPE;
    }

    /**
     * Produit une Map contenant le paramétrage de l'exporteur Jasper.
     *
     * @return le paramétrage de l'exporteur Jasper.
     */   
    public Map<String, Object> parametrageJRExporter()
    {
        // Déclaration d'une Map qui contiendra le paramétrage
        Map<String, Object> mapModele = new HashMap<>();

        // Paramétrage général de l'exporteur Jasper
        mapModele.put(CHARACTER_ENCODING, this.characterEncoding);

        // Journalisation du paramétrage
        journaliserParametrageJRExporter(mapModele);

        return mapModele;
    }

    /**
     * Produit une Map contenant le paramétrage de la vue jasper en utilisant : - soit les valeurs par défaut, - soit
     * les valeurs contenues dans le propriétés de l'objet ParametrageVueJasperOdt (valeurs injectée dans le fichier de
     * configuration des éditions).
     * 
     * @return le paramétrage de la vue Jasper
     */
    public Map<String, String> parametrerVue()
    {
        // Déclaration d'une Map qui contiendra le paramétrage
        Map<String, String> mapModele = new HashMap<>();

        // Initialisation du paramétrage
        putIfNotNull(mapModele, REPORT_VIEW_KEY, this.formatEdition);
        putIfNotNull(mapModele, EXTENSION_KEY, this.extension);
        putIfNotNull(mapModele, CONTENT_TYPE_KEY, this.contentType);
        putIfNotNull(mapModele, MIME_TYPE_KEY, this.typeMimeEdition);

        // Journalisation du paramétrage
        journaliserParametrageVue(mapModele);

        return mapModele;
    }
}
