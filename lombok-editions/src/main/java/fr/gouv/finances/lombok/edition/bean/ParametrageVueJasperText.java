/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 */
package fr.gouv.finances.lombok.edition.bean;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Paramétrage Jasper de la vue Text.
 * 
 * @author amleplatinec
 * @author Christophe Breheret-Girardin
 */
public class ParametrageVueJasperText extends AbstractParametrageVueJasper
{
    /** Initialisation de la journalisation. */
    protected static final Logger LOGGER = LoggerFactory.getLogger(ParametrageVueJasperText.class);

    /** Initialisation de l'UID. */
    private static final long serialVersionUID = 1L;

    /** Constant : DEF_FORMAT_EDITION_VALUE. */
    private static final String DEF_FORMAT_EDITION_VALUE = "text";

    /** Constant : DEF_EXTENSION. */
    private static final String DEF_EXTENSION = "txt";

    /** Constant : DEF_CHARACTER_ENCODING. */
    private static final String DEF_CHARACTER_ENCODING = "ISO-8859-1";

    /** Constant : DEF_CONTENT_TYPE. */
    private static final String DEF_CONTENT_TYPE = "text/plain;charset=ISO-8859-1";

    /** Constant : DEF_MIME_TYPE. */
    private static final String DEF_MIME_TYPE = "text/plain";

    /** Constant : DEF_CHARACTER_WIDTH. */
    private static final int DEF_CHARACTER_WIDTH = 10;

    /** Constant : DEF_CHARACTER_HEIGHT. */
    private static final int DEF_CHARACTER_HEIGHT = 10;

    /** Constant : DEF_PAGE_WIDTH. */
    private static final int DEF_PAGE_WIDTH = 80;

    /** Constant : DEF_PAGE_HEIGHT. */
    private static final int DEF_PAGE_HEIGHT = 400;

    /** Constant : DEF_BETWEEN_PAGES_TEXT. */
    private static final String DEF_BETWEEN_PAGES_TEXT = "";

    /** Constant : DEF_LINE_SEPARATOR. */
    private static final String DEF_LINE_SEPARATOR = System.getProperty("line.separator");

    /** An integer representing the pixel/character horizontal ratio. */
    private static final String CHARACTER_WIDTH =
        "net.sf.jasperreports.engine.export.JRTextExporterParameter.CHARACTER_WIDTH";

    /** An integer representing the pixel/character vertical ratio. */
    private static final String CHARACTER_HEIGHT =
        "net.sf.jasperreports.engine.export.JRTextExporterParameter.CHARACTER_HEIGHT";

    /** An integer representing the page width in characters. */
    private static final String PAGE_WIDTH = "net.sf.jasperreports.engine.export.JRTextExporterParameter.PAGE_WIDTH";

    /** An integer representing the page height in characters. */
    private static final String PAGE_HEIGHT = "net.sf.jasperreports.engine.export.JRTextExporterParameter.PAGE_HEIGHT";

    /**
     * A string representing text that will be inserted between pages of the generated report. By default, JasperReports
     * separates pages by two empty lines, but this behaviour can be overriden by this parameter.
     */
    private static final String BETWEEN_PAGES_TEXT =
        "net.sf.jasperreports.engine.export.JRTextExporterParameter.BETWEEN_PAGES_TEXT";

    /**
     * A string representing the separator between two lines of text. This parameter is useful since line separators can
     * vary from one operating system to another. The default value is the system "line.separator" property.
     */
    private static final String LINE_SEPARATOR =
        "net.sf.jasperreports.engine.export.JRTextExporterParameter.LINE_SEPARATOR";

    /** page width. */
    private int pageWidth;

    /** page height. */
    private int pageHeight;

    /** character height. */
    private int characterHeight;

    /** character width. */
    private int characterWidth;

    /** between pages text. */
    private String betweenPagesText;

    /** line separator. */
    private String lineSeparator;

    /**
     * Construit un objet ParametrageVueJasperCsv et lui affecte les paramètres par défaut.
     */
    public ParametrageVueJasperText()
    {
        super();

        this.formatEdition = DEF_FORMAT_EDITION_VALUE;
        this.characterEncoding = DEF_CHARACTER_ENCODING;
        this.extension = DEF_EXTENSION;
        this.contentType = DEF_CONTENT_TYPE;
        this.typeMimeEdition = DEF_MIME_TYPE;

        // Spécifique text
        this.betweenPagesText = DEF_BETWEEN_PAGES_TEXT;
        this.lineSeparator = DEF_LINE_SEPARATOR;
        this.pageHeight = DEF_PAGE_HEIGHT;
        this.pageWidth = DEF_PAGE_WIDTH;
        this.characterHeight = DEF_CHARACTER_HEIGHT;
        this.characterWidth = DEF_CHARACTER_WIDTH;
    }

    /**
     * Accesseur de l attribut between pages text.
     * 
     * @return between pages text
     */
    public String getBetweenPagesText()
    {
        return betweenPagesText;
    }

    /**
     * Accesseur de l attribut character height.
     * 
     * @return character height
     */
    public int getCharacterHeight()
    {
        return characterHeight;
    }

    /**
     * Accesseur de l attribut character width.
     * 
     * @return character width
     */
    public int getCharacterWidth()
    {
        return characterWidth;
    }

    /**
     * Accesseur de l attribut line separator.
     * 
     * @return line separator
     */
    public String getLineSeparator()
    {
        return lineSeparator;
    }

    /**
     * Accesseur de l attribut page height.
     * 
     * @return page height
     */
    public int getPageHeight()
    {
        return pageHeight;
    }

    /**
     * Accesseur de l attribut page width.
     * 
     * @return page width
     */
    public int getPageWidth()
    {
        return pageWidth;
    }

    /**
     * Produit une Map contenant le paramétrage de l'exporteur Jasper.
     *
     * @return le paramétrage de l'exporteur Jasper.
     */ 
    @Override
    public Map<String, Object> parametrageJRExporter()
    {
        // Déclaration d'une Map qui contiendra le paramétrage
        Map<String, Object> mapModele = new HashMap<>();

        // Paramétrage général de l'exporteur Jasper
        mapModele.put(CHARACTER_ENCODING, this.characterEncoding);

        // Paramétrage spécifique texte
        putIfNotNull(mapModele, BETWEEN_PAGES_TEXT, this.betweenPagesText);
        putIfNotNull(mapModele, LINE_SEPARATOR, this.lineSeparator);
        putIfNotNull(mapModele, CHARACTER_HEIGHT, Integer.valueOf (this.characterHeight));
        putIfNotNull(mapModele, CHARACTER_WIDTH, Integer.valueOf (this.characterWidth));
        putIfNotNull(mapModele, PAGE_WIDTH, Integer.valueOf (this.pageWidth));
        putIfNotNull(mapModele, PAGE_HEIGHT, Integer.valueOf (this.pageHeight));

        // Journalisation du paramétrage
        journaliserParametrageJRExporter(mapModele);

        return mapModele;
    }

    /**
     * Produit une Map contenant le paramétrage de la vue jasper en utilisant : - soit les valeurs par défaut, - soit
     * les valeurs contenues dans le propriétés de l'objet ParametrageVueJasperCsv (valeurs injectée dans le fichier de
     * configuration des éditions).
     * 
     * @return the map
     */
    @Override
    public Map<String, String> parametrerVue()
    {
        // Déclaration d'une Map qui contiendra le paramétrage
        Map<String, String> mapModele = new HashMap<>();

        // Initialisation du paramétrage
        putIfNotNull(mapModele, REPORT_VIEW_KEY, this.formatEdition);
        putIfNotNull(mapModele, EXTENSION_KEY, this.extension);
        putIfNotNull(mapModele, CONTENT_TYPE_KEY, this.contentType);
        putIfNotNull(mapModele, MIME_TYPE_KEY, this.typeMimeEdition);

        // Journalisation du paramétrage
        journaliserParametrageVue(mapModele);

        return mapModele;
    }

    /**
     * Modificateur de l attribut between pages text.
     * 
     * @param betweenPagesText le nouveau between pages text
     */
    public void setBetweenPagesText(String betweenPagesText)
    {
        this.betweenPagesText = betweenPagesText;
    }

    /**
     * Modificateur de l attribut character height.
     * 
     * @param characterHeight le nouveau character height
     */
    public void setCharacterHeight(int characterHeight)
    {
        this.characterHeight = characterHeight;
    }

    /**
     * Modificateur de l attribut character width.
     * 
     * @param characterWidth le nouveau character width
     */
    public void setCharacterWidth(int characterWidth)
    {
        this.characterWidth = characterWidth;
    }

    /**
     * Modificateur de l attribut line separator.
     * 
     * @param lineSeparator le nouveau line separator
     */
    public void setLineSeparator(String lineSeparator)
    {
        this.lineSeparator = lineSeparator;
    }

    /**
     * Modificateur de l attribut page height.
     * 
     * @param pageHeight le nouveau page height
     */
    public void setPageHeight(int pageHeight)
    {
        this.pageHeight = pageHeight;
    }

    /**
     * Modificateur de l attribut page width.
     * 
     * @param pageWidth le nouveau page width
     */
    public void setPageWidth(int pageWidth)
    {
        this.pageWidth = pageWidth;
    }

}
