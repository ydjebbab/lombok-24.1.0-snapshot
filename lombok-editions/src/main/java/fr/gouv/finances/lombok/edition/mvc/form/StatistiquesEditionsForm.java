/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : StatistiquesEditionsForm.java
 *
 */
package fr.gouv.finances.lombok.edition.mvc.form;

import fr.gouv.finances.lombok.util.base.BaseBean;

/**
 * Class StatistiquesEditionsForm --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.2 $ Date: 11 déc. 2009
 */
public class StatistiquesEditionsForm extends BaseBean
{

    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = 1L;
    
    
    /**
     * Constructeur de la classe StatistiquesEditionsForm.java
     *
     */
    public StatistiquesEditionsForm()
    {
        super();
    }
    
    

}
