/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : EditionDemandeServiceImpl.java
 *
 */
package fr.gouv.finances.lombok.edition.service.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.gouv.finances.lombok.edition.bean.EditionAsynchroneNonNotifieeParMail;
import fr.gouv.finances.lombok.edition.bean.EditionAsynchroneNotifieeParMail;
import fr.gouv.finances.lombok.edition.bean.EditionSynchroneNonNotifieeParMail;
import fr.gouv.finances.lombok.edition.bean.EditionSynchroneNotifieeParMail;
import fr.gouv.finances.lombok.edition.bean.JobHistory;
import fr.gouv.finances.lombok.edition.dao.JobHistoryDao;
import fr.gouv.finances.lombok.edition.service.EditionDemandeService;
import fr.gouv.finances.lombok.edition.service.EditionFinderService;
import fr.gouv.finances.lombok.edition.service.EditionProducerService;
import fr.gouv.finances.lombok.edition.service.EditionSchedulerService;
import fr.gouv.finances.lombok.edition.service.EditionStorageService;
import fr.gouv.finances.lombok.edition.service.EditionSynchroService;
import fr.gouv.finances.lombok.edition.util.Control;
import fr.gouv.finances.lombok.securite.techbean.PersonneAnnuaire;
import fr.gouv.finances.lombok.upload.bean.FichierJoint;
import fr.gouv.finances.lombok.util.base.BaseServiceImpl;
import fr.gouv.finances.lombok.util.exception.RegleGestionException;

/**
 * Class EditionDemandeServiceImpl
 * 
 * @author amleplatinec
 * @version $Revision: 1.2 $ Date: 11 déc. 2009
 */
public class EditionDemandeServiceImpl extends BaseServiceImpl implements EditionDemandeService
{
    protected static final Logger log = LoggerFactory.getLogger(EditionDemandeServiceImpl.class);

    /** Dao permettant d'accéder à l'historique des éditions. */
    private JobHistoryDao jobhistorydao;

    /** Service de production des éditions (synchrone). */
    private EditionProducerService editionproducerserviceso;

    /** Service de plannification des éditions asynchrones. */
    private EditionSchedulerService editionschedulerserviceso;

    /** Service de recherche des éditions. */
    private EditionFinderService editionfinderserviceso;

    /** Service de stockage des éditions. */
    private EditionStorageService editionstorageserviceso;

    /** Service de purge et de synchronisation des éditions. */
    private EditionSynchroService editionsynchroserviceso;

    /**
     * Constructeur
     */
    public EditionDemandeServiceImpl()
    {
        super();
    }

    /**
     * Déclenche une édition asynchrone non notifiée par mail.
     * 
     * @param editAsynchNonNotifieeParMail --
     * @param parametresEdition --
     * @return the edition asynchrone non notifiee par mail
     */
    @Override
    public EditionAsynchroneNonNotifieeParMail declencherEdition(
        EditionAsynchroneNonNotifieeParMail editAsynchNonNotifieeParMail, Map parametresEdition)
    {
        return declencherEdition((JobHistory) editAsynchNonNotifieeParMail, parametresEdition, Boolean.FALSE);
    }

    /**
     * Déclenche une édition asynchrone notifiée par mail.
     * 
     * @param editAsynchNotifieeParMail --
     * @param parametresEdition --
     * @return the edition asynchrone notifiee par mail
     */
    @Override
    public EditionAsynchroneNotifieeParMail declencherEdition(
        EditionAsynchroneNotifieeParMail editAsynchNotifieeParMail, Map parametresEdition)
    {
        return declencherEdition((JobHistory) editAsynchNotifieeParMail, parametresEdition, Boolean.FALSE);
    }

    /**
     * Déclenche une édition synchrone non notifiée par mail.
     * 
     * @param editSynchNonNotifieeParMail --
     * @param parametresEdition --
     * @return the edition synchrone non notifiee par mail
     */
    @Override
    public EditionSynchroneNonNotifieeParMail declencherEdition(
        EditionSynchroneNonNotifieeParMail editSynchNonNotifieeParMail, Map parametresEdition)
    {
        return declencherEdition((JobHistory) editSynchNonNotifieeParMail, parametresEdition, Boolean.FALSE);
    }

    /**
     * Déclenche une édition synchrone notifiée par mail.
     * 
     * @param editSynchNotifieeParMail --
     * @param parametresEdition --
     * @return the edition synchrone notifiee par mail
     */
    @Override
    public EditionSynchroneNotifieeParMail declencherEdition(EditionSynchroneNotifieeParMail editSynchNotifieeParMail,
        Map parametresEdition)
    {
        return declencherEdition((JobHistory) editSynchNotifieeParMail, parametresEdition, Boolean.FALSE);
    }

    /**
     * Déclenche une édition asynchrone compressee non notifiée par mail.
     * 
     * @param editAsynchNonNotifieeParMail --
     * @param parametresEdition --
     * @return the edition asynchrone non notifiee par mail
     */
    @Override
    public EditionAsynchroneNonNotifieeParMail declencherEditionCompressee(
        EditionAsynchroneNonNotifieeParMail editAsynchNonNotifieeParMail, Map parametresEdition)
    {
        return declencherEdition((JobHistory) editAsynchNonNotifieeParMail, parametresEdition, Boolean.TRUE);
    }

    /**
     * Déclenche une édition asynchrone compressee notifiée par mail.
     * 
     * @param editAsynchNotifieeParMail --
     * @param parametresEdition --
     * @return the edition asynchrone notifiee par mail
     */
    @Override
    public EditionAsynchroneNotifieeParMail declencherEditionCompressee(
        EditionAsynchroneNotifieeParMail editAsynchNotifieeParMail, Map parametresEdition)
    {
        return declencherEdition((JobHistory) editAsynchNotifieeParMail, parametresEdition, Boolean.TRUE);
    }

    /**
     * Déclenche une édition synchrone compressee non notifiée par mail.
     * 
     * @param editSynchNonNotifieeParMail --
     * @param parametresEdition --
     * @return the edition synchrone non notifiee par mail
     */
    @Override
    public EditionSynchroneNonNotifieeParMail declencherEditionCompressee(
        EditionSynchroneNonNotifieeParMail editSynchNonNotifieeParMail, Map parametresEdition)
    {
        return declencherEdition((JobHistory) editSynchNonNotifieeParMail, parametresEdition, Boolean.TRUE);
    }

    /**
     * Déclenche une édition synchrone compressee notifiée par mail.
     * 
     * @param editSynchNotifieeParMail --
     * @param parametresEdition --
     * @return the edition synchrone notifiee par mail
     */
    @Override
    public EditionSynchroneNotifieeParMail declencherEditionCompressee(
        EditionSynchroneNotifieeParMail editSynchNotifieeParMail, Map parametresEdition)
    {
        return declencherEdition((JobHistory) editSynchNotifieeParMail, parametresEdition, Boolean.TRUE);
    }

    /**
     * Déclenche la production immédiate d'une édition et la compresse, sans stockage dans le repository des éditions.
     * 
     * @param editionBeanId --
     * @param parametresEdition --
     * @return FichierJoint représentant l'édition
     */
    @Override
    public FichierJoint declencherProductionEditionCompresseeNonStockee(String editionBeanId, Map parametresEdition)
    {
        return editionproducerserviceso.creerEditionCompressee(editionBeanId, parametresEdition);
    }

    /**
     * Déclenche la production immédiate de l'édition, sans stockage dans le repository des éditions.
     * 
     * @param editionBeanId --
     * @param parametresEdition --
     * @return FichierJoint représentant l'édition
     */
    @Override
    public FichierJoint declencherProductionEditionNonStockee(String editionBeanId, Map parametresEdition)
    {
        return editionproducerserviceso.creerEdition(editionBeanId, parametresEdition);
    }

    /**
     * Déclenche la production immédiate de l'édition html, sans stockage dans le repository des éditions.
     * 
     * @param editionBeanId --
     * @param parametresEdition --
     * @return Map contenant : - le fichier de l'édition sous la clé CpJasperReportsHtmlView.FICHIER_JOINT_KEY - un
     *         objet jasperprint sous la clé CpJasperReportsHtmlView.JASPER_PRINT_KEY
     */
    @Override
    public Map declencherProductionHtmlEditionNonStockee(String editionBeanId, Map parametresEdition)
    {
        return editionproducerserviceso.creerEditionHtml(editionBeanId, parametresEdition);
    }

    /**
     * Initialise une edition asynchrone dont la réalisation ne sera pas notifiée par mail.
     * 
     * @param beanIdEdition --
     * @param uidProprietaire --
     * @return EditionAsynchroneNonNotifieeParMail
     */
    @Override
    public EditionAsynchroneNonNotifieeParMail initEditionAsynchroneNonNotifieeParMail(String beanIdEdition,
        String uidProprietaire)
    {
        return editionproducerserviceso.initEditionAsynchroneNonNotifieeParMail(beanIdEdition, uidProprietaire);
    }

    /**
     * Initialise une edition asynchrone dont la réalisation sera notifiée par mail.
     * 
     * @param beanIdEdition --
     * @param uidProprietaire --
     * @param mailDestinataire --
     * @return EditionAsynchroneNotifieeParMail
     */
    @Override
    public EditionAsynchroneNotifieeParMail initEditionAsynchroneNotifieeParMail(String beanIdEdition,
        String uidProprietaire, String mailDestinataire)
    {
        return editionproducerserviceso.initEditionAsynchroneNotifieeParMail(beanIdEdition, uidProprietaire,
            mailDestinataire);
    }

    /**
     * Initialise une edition asynchrone dont la réalisation ne sera pas notifiée par mail.
     * 
     * @param beanIdEdition --
     * @param uidProprietaire --
     * @return EditionAsynchroneNonNotifieeParMail
     */
    @Override
    public EditionSynchroneNonNotifieeParMail initEditionSynchroneNonNotifieeParMail(String beanIdEdition,
        String uidProprietaire)
    {
        return editionproducerserviceso.initEditionSynchroneNonNotifieeParMail(beanIdEdition, uidProprietaire);
    }

    /**
     * Initialise une edition asynchrone dont la réalisation sera notifiée par mail.
     * 
     * @param beanIdEdition --
     * @param uidProprietaire --
     * @param mailDestinataire --
     * @return EditionAsynchroneNotifieeParMail
     */
    @Override
    public EditionSynchroneNotifieeParMail initEditionSynchroneNotifieeParMail(String beanIdEdition,
        String uidProprietaire, String mailDestinataire)
    {
        return editionproducerserviceso.initEditionSynchroneNotifieeParMail(beanIdEdition, uidProprietaire,
            mailDestinataire);
    }

    /**
     * Initialise le message mail d'accès à l'édition pour permettre un accès via le portail intranet ou le portail
     * internet Par défaut, l'accès est proposé seulement via le portail intranet.
     * 
     * @param jobHistory --
     */
    @Override
    public void initMessageMailPourAccesAvecOuSansPortailIntranet(JobHistory jobHistory)
    {
        editionproducerserviceso.initMessageMailPourAccesAvecOuSansPortailIntranet(jobHistory);
    }

    /**
     * Initialise le message mail d'accès à l'édition pour permettre un accès via le portail intranet ou le portail
     * internet Par défaut, l'accès est proposé seulement via le portail intranet.
     * 
     * @param jobHistory --
     */
    @Override
    public void initMessageMailPourAccesViaPortailIntranetOuInternet(JobHistory jobHistory)
    {
        editionproducerserviceso.initMessageMailPourAccesViaPortailIntranetOuInternet(jobHistory);
    }

    /**
     * Purge les éditions en fonction de leurs durée de rétention.
     */
    @Override
    public void purgeEtSynchronise()
    {
        editionsynchroserviceso.purgeEditionsSiAutorise(100, Control.instantCourant());
        editionsynchroserviceso.synchroniseSfAvecHistorique();
        editionsynchroserviceso.synchroniseHistoriqueAvecSf(100);
    }

    /**
     * Recherche toutes les nouvelles éditions accessibles à un utilisateur représenté par un objet PersonneAnnuaire.
     * 
     * @param personne --
     * @return the list
     */
    @Override
    public List rechercheNouvellesEditionsAccessiblesAUnUtilisateur(PersonneAnnuaire personne)
    {
        return editionfinderserviceso.rechercheNouvellesEditionsAccessiblesAUnUtilisateur(personne);
    }

    /**
     * Recherche une édition à partir d'un objet JobHistory Retourne l'objet de type FichierJoint associé.
     * 
     * @param jobHistory --
     * @return FichierJoint contenant l'édition ou null si l'édition n'est pas disponible
     */
    @Override
    public FichierJoint rechercherEdition(JobHistory jobHistory)
    {
        FichierJoint fichierJoint = null;

        try
        {
            fichierJoint = editionstorageserviceso.chargerFichierEdition(jobHistory);
        }
        catch (RegleGestionException rgex)
        {
            log.warn("ERREUR :", rgex);
        }

        return fichierJoint;
    }

    /**
     * Recherche une édition à partir d'un objet JobHistory Met à jour la liste des utilisateurs qui ont consulté
     * l'édition.
     * 
     * @param jobHistory JbHistory qui représente l'édition recherchée
     * @param uid Identifiant de l'utilisateur qui demande à consulter l'édition
     * @return FichierJoint contenant l'édition ou null si l'édition n'est pas disponible
     */
    @Override
    public FichierJoint rechercherEdition(JobHistory jobHistory, String uid)
    {
        FichierJoint fichierJoint = null;
        try
        {
            fichierJoint = editionstorageserviceso.chargerFichierEdition(jobHistory, uid);
        }
        catch (RegleGestionException rgex)
        {
            log.warn("ERREUR :", rgex);
        }

        return fichierJoint;
    }

    /**
     * Recherche de l'historique d'une édition par son identifiant.
     * 
     * @param editionUuid --
     * @return the job history
     */
    @Override
    public JobHistory rechercherEditionParUuid(String editionUuid)
    {
        return editionfinderserviceso.rechercherEditionParUuid(editionUuid);
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param beanEditionId
     * @param appliOrigine
     * @return job history
     * @see fr.gouv.finances.lombok.edition.service.EditionDemandeService#rechercherLaDerniereEditionParBeanIdEtParAppliOrigine(java.lang.String,
     *      java.lang.String)
     */
    @Override
    public JobHistory rechercherLaDerniereEditionParBeanIdEtParAppliOrigine(String beanEditionId, String appliOrigine)
    {
        return editionfinderserviceso
            .rechercherLaDerniereEditionParBeanIdEtParAppliOrigine(beanEditionId, appliOrigine);
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param beanEditionId
     * @param appliOrigine
     * @param utilisateur
     * @return job history
     * @see fr.gouv.finances.lombok.edition.service.EditionDemandeService#rechercherLaDerniereEditionParBeanIdEtParAppliOrigineEtParUtilisateur(java.lang.String,
     *      java.lang.String, java.lang.String)
     */
    @Override
    public JobHistory rechercherLaDerniereEditionParBeanIdEtParAppliOrigineEtParUtilisateur(String beanEditionId,
        String appliOrigine, String utilisateur)
    {
        return editionfinderserviceso.rechercherLaDerniereEditionParBeanIdEtParAppliOrigineEtParUtilisateur(
            beanEditionId, appliOrigine, utilisateur);
    }

    /**
     * Recherche toutes les éditions accessibles à un utilisateur représenté par un objet PersonneAnnuaire.
     * 
     * @param personne --
     * @return the list
     */
    @Override
    public List rechercheToutesEditionsAccessiblesAUnUtilisateur(PersonneAnnuaire personne)
    {
        return editionfinderserviceso.rechercheToutesEditionsAccessiblesAUnUtilisateur(personne);
    }

    /**
     * Sets the editionfinderserviceso.
     * 
     * @param editionfinderserviceso the editionfinderserviceso to set
     */
    public void setEditionfinderserviceso(EditionFinderService editionfinderserviceso)
    {
        this.editionfinderserviceso = editionfinderserviceso;
    }

    /**
     * Modificateur de l attribut editionproducerserviceso.
     * 
     * @param editionproducerserviceso le nouveau editionproducerserviceso
     */
    public void setEditionproducerserviceso(EditionProducerService editionproducerserviceso)
    {
        this.editionproducerserviceso = editionproducerserviceso;
    }

    /**
     * Modificateur de l attribut editionschedulerserviceso.
     * 
     * @param editionschedulerserviceso le nouveau editionschedulerserviceso
     */
    public void setEditionschedulerserviceso(EditionSchedulerService editionschedulerserviceso)
    {
        this.editionschedulerserviceso = editionschedulerserviceso;
    }

    /**
     * Sets the editionstorageserviceso.
     * 
     * @param editionstorageserviceso the editionstorageserviceso to set
     */
    public void setEditionstorageserviceso(EditionStorageService editionstorageserviceso)
    {
        this.editionstorageserviceso = editionstorageserviceso;
    }

    /**
     * Sets the editionsynchroserviceso.
     * 
     * @param editionsynchroserviceso the editionsynchroserviceso to set
     */
    public void setEditionsynchroserviceso(EditionSynchroService editionsynchroserviceso)
    {
        this.editionsynchroserviceso = editionsynchroserviceso;
    }

    /**
     * Modificateur de l attribut jobhistorydao.
     * 
     * @param jobhistorydao le nouveau jobhistorydao
     */
    public void setJobhistorydao(JobHistoryDao jobhistorydao)
    {
        this.jobhistorydao = jobhistorydao;
    }

    /**
     * Supprime une collection d'éditions.
     * 
     * @param elementsASupprimer --
     * @return the collection< job history>
     */
    @Override
    public Collection<JobHistory> supprimerEditions(Collection<JobHistory> elementsASupprimer)
    {
        Collection<JobHistory> editionsSupprimees = new ArrayList<>();

        for (Iterator iter = elementsASupprimer.iterator(); iter.hasNext();)
        {
            supprimerUneEdition(editionsSupprimees, iter);
        }
        return editionsSupprimees;
    }

    /**
     * Demande de production d'une édition asynchrone ou synchrone.
     * 
     * @param jobHistory --
     * @param parametresEdition --
     * @param compression --
     * @return the job history
     */
    private JobHistory declencherEdition(JobHistory jobHistory, Map parametresEdition, Boolean compression)
    {
        log.debug(">>> Debut methode declencherEdition");
        Control.assertJobHistoryNotNull(jobHistory);
        JobHistory jobHistoryEnregistre;

        // Production de l'édition de façon asynchrone
        if (jobHistory.isJobAsynchrone())
        {
            log.debug("Production de l'édition de façon asynchrone");
            jobHistoryEnregistre =
                editionproducerserviceso.enregistrerDeclenchement(jobHistory, JobHistory.STATUS_EN_ATTENTE_EXECUTION);
            editionschedulerserviceso.declencherEdition(jobHistory, parametresEdition, compression);
        }
        // Production de l'édition de façon synchrone
        else
        {
            log.debug("Production de l'édition de façon synchrone");
            jobHistoryEnregistre =
                editionproducerserviceso.enregistrerDeclenchement(jobHistory, JobHistory.STATUS_EN_COURS_EXECUTION);

            if (compression != null && compression.booleanValue() == true)
            {
                log.debug("creation et stockage d'une edition compressee");
                editionproducerserviceso.creerEtStockerEditionCompressee(jobHistoryEnregistre, parametresEdition);
            }
            else
            {
                log.debug("creation et stockage d'une edition non compressee");
                editionproducerserviceso.creerEtStockerEdition(jobHistoryEnregistre, parametresEdition);
            }
        }
        return jobHistoryEnregistre;
    }

    /**
     * Supprim une édition.
     * 
     * @param editionsSupprimees --
     * @param iter --
     */
    private void supprimerUneEdition(Collection<JobHistory> editionsSupprimees, Iterator iter)
    {
        JobHistory jobHistory = (JobHistory) iter.next();

        if (JobHistory.MODE_SOUM_SYNCH.equals(jobHistory.getOrdoEdition().getModeSoumission()))
        {
            supprimerUneEditionSynchrone(editionsSupprimees, jobHistory);
        }
        else
        {
            supprimerUneEditionAsynchrone(editionsSupprimees, jobHistory);
        }
    }

    /**
     * Supprime une édition produite de façon asynchrone.
     * 
     * @param editionsSupprimees --
     * @param jobHistory --
     */
    private void supprimerUneEditionAsynchrone(Collection<JobHistory> editionsSupprimees, JobHistory jobHistory)
    {
        Control.assertJobHistoryNotNull(jobHistory);
        if (!jobHistory.isEnCoursOuAttenteExecution())
        {
            jobhistorydao.deleteJobHistory(jobHistory);

            if (jobHistory.isDisponible())
            {
                editionstorageserviceso.effacerFichierEdition(jobHistory);
            }
            editionschedulerserviceso.supprimerJob(jobHistory);
        }

        else
        {
            if (editionschedulerserviceso.testerExistenceJobEdition(jobHistory))
            {
                editionschedulerserviceso.supprimerJob(jobHistory);
            }
            jobhistorydao.deleteJobHistory(jobHistory);
        }
        editionsSupprimees.add(jobHistory);
    }

    /**
     * Supprime une édition produite de façon synchrone.
     * 
     * @param editionsSupprimees --
     * @param jobHistory --
     */
    private void supprimerUneEditionSynchrone(Collection<JobHistory> editionsSupprimees, JobHistory jobHistory)
    {
        Control.assertJobHistoryNotNull(jobHistory);

        if (!jobHistory.isEnCoursOuAttenteExecution())
        {
            jobhistorydao.deleteJobHistory(jobHistory);
            editionsSupprimees.add(jobHistory);
            if (jobHistory.isDisponible())
            {
                editionstorageserviceso.effacerFichierEdition(jobHistory);
            }
        }
    }
}
