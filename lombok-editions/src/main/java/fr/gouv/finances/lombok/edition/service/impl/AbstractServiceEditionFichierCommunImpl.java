/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : AbstractServiceEditionFichierCommunImpl.java
 *
 */
package fr.gouv.finances.lombok.edition.service.impl;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.ApplicationObjectSupport;

import fr.gouv.finances.lombok.edition.service.ServiceEditionCommun;
import fr.gouv.finances.lombok.upload.bean.FichierJoint;
import fr.gouv.finances.lombok.util.exception.ApplicationExceptionTransformateur;

/**
 * Class AbstractServiceEditionFichierCommunImpl SuperClasse pour le stockage de fichiers binaires dont la production
 * est à implémenter.
 * 
 * @author chouard-cp
 * @version $Revision: 1.3 $ Date: 15 déc. 2009
 */
public abstract class AbstractServiceEditionFichierCommunImpl extends ApplicationObjectSupport implements
    ServiceEditionCommun
{

    /** Constant : DEF_EXTENSION. */
    private static final String DEF_EXTENSION = "txt";

    /** Constant : DEF_CHARACTER_ENCODING. */
    private static final String DEF_CHARACTER_ENCODING = "ISO-8859-1";

    /** Constant : DEF_CONTENT_TYPE. */
    private static final String DEF_CONTENT_TYPE = "text/plain;charset=ISO-8859-1";

    /** Constant : DEF_MIME_TYPE. */
    private static final String DEF_MIME_TYPE = "text/plain";

    /** Constant : NOM_FICHIER_PAR_DEFAUT. */
    private static final String NOM_FICHIER_PAR_DEFAUT = "edition";

    /** Constant : FICHIER_TEMPORAIRE_SUFFIXE. */
    private static final String FICHIER_TEMPORAIRE_SUFFIXE = ".temp";

    protected Logger log = LoggerFactory.getLogger(getClass());

    /** Extension utilisée dans le nom du fichier produit. */
    private String extension;

    /** Encodage du fichier. */
    private String characterEncoding;

    /** Valeur de l'attribut contentType. */
    private String contentType;

    /** Type Mime. */
    private String typeMime;

    /** Nombre de jours de conservation de l'édition après sa production. */
    private Integer nombreJoursConservation;

    /** Nom de l'édition. */
    private String nomEdition;

    /**
     * Instanciation de abstract service edition fichier commun impl.
     */
    public AbstractServiceEditionFichierCommunImpl()
    {
        super();
        this.characterEncoding = DEF_CHARACTER_ENCODING;
        this.contentType = DEF_CONTENT_TYPE;
        this.extension = DEF_EXTENSION;
        this.typeMime = DEF_MIME_TYPE;
    }

    /**
     * Production du fichier contenant l'édition en utilisant la stream passée en paramètres. La stream ne doit pas être
     * fermée par cette méthode.
     * 
     * @param parametresEdition --
     * @param out --
     */
    public abstract void creerFichierEdition(Map parametresEdition, OutputStream out);

    /**
     * Paramétrage du nom de fichier à utiliser pour nommer l'édition Par défaut null. A surcharger si nécessaire. sinon
     * le nom du fichier est généré
     * 
     * @param parametresEdition --
     * @return le nom du fichier à utiliser pour stocker l'édition
     */
    public abstract String creerNomDuFichier(Map parametresEdition);

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @return the encodage du fichier
     * @see fr.gouv.finances.lombok.edition.service.ServiceEditionCommun#getCharacterEncoding()
     */
    public String getCharacterEncoding()
    {
        return characterEncoding;
    }

    /**
     * Accesseur de l attribut content type.
     * 
     * @return content type
     */
    public String getContentType()
    {
        return contentType;
    }

    /**
     * Retourne la description de l'édition qui sera affichée dans la bannette.
     * 
     * @return the description
     */
    @Override
    public String getDescription()
    {
        // Date datejour = new Date();
        // DateFormat unDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale.FRANCE);
        StringBuilder desc = new StringBuilder();

        desc.append(this.nomEdition);
        // desc.append(" (");
        // desc.append(unDateFormat.format(datejour));
        // desc.append(')');

        return desc.toString();
    }

    /**
     * Retourne la description de l'édition qui sera affichée dans la bannette.
     * 
     * @param parametresEdition --
     * @return the description
     */
    @Override
    public String getDescription(Map parametresEdition)
    {
        return this.getDescription();
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @return the extension utilisée dans le nom du fichier produit
     * @see fr.gouv.finances.lombok.edition.service.ServiceEditionCommun#getExtension()
     */
    @Override
    public String getExtension()
    {
        return extension;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @return the nombre de jours de conservation de l'édition après sa production
     * @see fr.gouv.finances.lombok.edition.service.ServiceEditionCommun#getNombreJoursConservation()
     */
    @Override
    public Integer getNombreJoursConservation()
    {
        return nombreJoursConservation;
    }

    /**
     * Accesseur de l attribut nom edition.
     * 
     * @return nom edition
     */
    public String getNomEdition()
    {
        return nomEdition;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @return the type Mime
     * @see fr.gouv.finances.lombok.edition.service.ServiceEditionCommun#getTypeMime()
     */
    @Override
    public String getTypeMime()
    {
        return typeMime;
    }

    /**
     * Produit l'édition et la retourne sous forme d'objet FichierJoint.
     * 
     * @param parametresEdition --
     * @return the fichier joint
     */
    @Override
    public FichierJoint produireFichierEdition(Map parametresEdition)
    {
        FichierJoint result = null;

        long start = this.traceDebutEdition();

        File tempFileEdition = null;
        OutputStream fileEditionOutputStream = null;
        BufferedOutputStream out = null;

        try
        {
            tempFileEdition = File.createTempFile(this.nomEdition, FICHIER_TEMPORAIRE_SUFFIXE);
            tempFileEdition.deleteOnExit();
            fileEditionOutputStream = new FileOutputStream(tempFileEdition);
            out = new BufferedOutputStream(fileEditionOutputStream);

            // -----------------------------------------------------------------------
            // -- Production du fichier contenant l'édition : méthode à surcharger --
            // -----------------------------------------------------------------------
            this.creerFichierEdition(parametresEdition, out);

            result = new FichierJoint(tempFileEdition);
            result.setTypeMimeFichier(this.typeMime);
            result.setNomFichierOriginal(getNomFichier(parametresEdition));
        }
        catch (IOException exception)
        {
            log.warn("Erreur lors de production de l'édition", exception);
            throw ApplicationExceptionTransformateur.transformer(exception);
        }
        finally
        {
            if (out != null)
            {
                try
                {
                    out.close();
                }
                catch (IOException exception)
                {
                    log.warn("Erreur lors de la fermeture du flux", exception);
                    // Aucune exception n'est levée, on suppose le stream a été
                    // fermé dans la méthode creerFichierEdition
                }
            }

            if (fileEditionOutputStream != null)
            {
                try
                {
                    fileEditionOutputStream.close();
                }
                catch (IOException exception)
                {
                    log.warn("ERREUR LORS DE LA FERMETURE DU STREAM", exception);
                }
            }
        }

        this.traceFinExecution(start);

        return result;
    }

    /**
     * Méthode non utilisable.
     * 
     * @param parametresEdition --
     * @return the map
     */
    @Override
    public Map produitEditionHtml(Map parametresEdition)
    {
        return null;
    }

    /**
     * Modificateur de l attribut character encoding.
     * 
     * @param characterEncoding le nouveau character encoding
     */
    public void setCharacterEncoding(String characterEncoding)
    {
        this.characterEncoding = characterEncoding;
    }

    /**
     * Modificateur de l attribut content type.
     * 
     * @param contentType le nouveau content type
     */
    public void setContentType(String contentType)
    {
        this.contentType = contentType;
    }

    /**
     * Modificateur de l attribut extension.
     * 
     * @param fileExtension le nouveau extension
     */
    public void setExtension(String fileExtension)
    {
        this.extension = fileExtension;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @param nombreJoursConservation the new nombre de jours de conservation de l'édition après sa production
     * @see fr.gouv.finances.lombok.edition.service.ServiceEditionCommun#setNombreJoursConservation(java.lang.Integer)
     */
    @Override
    public void setNombreJoursConservation(Integer nombreJoursConservation)
    {
        this.nombreJoursConservation = nombreJoursConservation;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @param nomEdition the new nom de l'édition
     * @see fr.gouv.finances.lombok.edition.service.ServiceEditionCommun#setNomEdition(java.lang.String)
     */
    @Override
    public void setNomEdition(String nomEdition)
    {
        this.nomEdition = nomEdition;
    }

    /**
     * Modificateur de l attribut type mime.
     * 
     * @param mimeType le nouveau type mime
     */
    public void setTypeMime(String mimeType)
    {
        this.typeMime = mimeType;
    }

    /**
     * Récupère dans la Map model le nom à utiliser pour nommer le fichier contenant l'édition et lui affecte
     * éventuellement une extension. Si le nom du fichier n'est pas transmis dans les paramètres, génère un nom de
     * fichier.
     * 
     * @param parametresEdition Map contenant les paramètres de l'édition
     * @return Nom du fichier à utiliser pour stocker une édition
     */
    private String getNomFichier(Map parametresEdition)
    {
        StringBuilder nomFichier = new StringBuilder();

        String nomFichierCree = this.creerNomDuFichier(parametresEdition);
        // TT du nom du fichier
        if (nomFichierCree != null && StringUtils.isNotBlank(nomFichierCree))
        {
            nomFichier.append(nomFichierCree);
        }
        else
        {
            nomFichier.append(NOM_FICHIER_PAR_DEFAUT);
            nomFichier.append((new Date()).getTime());
        }

        // TT de l'extension du fichier
        if (nomFichier != null && nomFichier.lastIndexOf(".") == -1)
        {
            nomFichier.append('.');
            nomFichier.append(this.extension);
        }
        return nomFichier.toString();
    }

    /**
     * Trace le début d'une édition.
     * 
     * @return le moment où la produFileOutputStreamction de l'édition a commencé
     */
    private long traceDebutEdition()
    {
        long start = 0;

        if (log.isTraceEnabled())
        {
            // ----------------------
            // -- Nom de l'édition --
            // ----------------------

            StringBuilder unMsg = new StringBuilder("NOM DE L'EDITION : ");
            unMsg.append(this.nomEdition);

            // -----------------------------------------------------------------------
            // -- Date du lancement de l'édition --
            // -----------------------------------------------------------------------
            Calendar cal = Calendar.getInstance();

            unMsg = new StringBuilder("DEBUT DU TRAITEMENT DE L'EDITION : ");
            unMsg.append(cal.getTime().toString());
            start = cal.getTimeInMillis();
            log.trace(unMsg.toString());
        }
        return start;
    }

    /**
     * Trace la fin de l'exécution.
     * 
     * @param start --
     */
    private void traceFinExecution(long start)
    {
        if (log.isTraceEnabled())
        {
            StringBuilder unMsg;
            Date uneDateCourante;

            // --------------------------------------
            // -- Date de fin d'exécution de l'édition --
            // --------------------------------------
            unMsg = new StringBuilder();
            unMsg.append("FIN DU TRAITEMENT DE  L'EDITION : ");
            uneDateCourante = Calendar.getInstance().getTime();
            unMsg.append(uneDateCourante.toString());
            log.trace(unMsg.toString());

            // ------------------------------------------------
            // -- Duree de l'exécution de l'édition --
            // -----------------------------------
            unMsg = new StringBuilder();
            unMsg.append("DUREE D'EXECUTION DE L'EDITION : ");
            long elapsedTime = Calendar.getInstance().getTimeInMillis() - start;

            unMsg.append(elapsedTime);
            unMsg.append(" MILLI SECONDES");
        }
    }
}
