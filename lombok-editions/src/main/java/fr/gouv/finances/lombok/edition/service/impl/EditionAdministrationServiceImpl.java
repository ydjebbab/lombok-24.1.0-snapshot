/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : EditionAdministrationServiceImpl.java
 *
 */
package fr.gouv.finances.lombok.edition.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import fr.gouv.finances.lombok.edition.bean.JobHistory;
import fr.gouv.finances.lombok.edition.service.EditionAdministrationService;
import fr.gouv.finances.lombok.edition.service.EditionFinderService;
import fr.gouv.finances.lombok.util.base.BaseServiceImpl;

/**
 * Class EditionAdministrationServiceImpl --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.2 $ Date: 11 déc. 2009
 */
public class EditionAdministrationServiceImpl extends BaseServiceImpl implements EditionAdministrationService
{

    /** editionfinderserviceso. */
    private EditionFinderService editionfinderserviceso;

    /**
     * Constructeur de la classe EditionAdministrationServiceImpl.java
     *
     */
    public EditionAdministrationServiceImpl()
    {
        super(); 
        
    }

    /**
     * Gets the editionfinderserviceso.
     * 
     * @return the editionfinderserviceso
     */
    public EditionFinderService getEditionfinderserviceso()
    {
        return editionfinderserviceso;
    }

    /**
     * Retourne la liste des descriptions des éditions disponibles.
     * 
     * @return the list
     */
    @Override
    public List rechercherEditionBeanIds()
    {
        return editionfinderserviceso.rechercheBeanEditionIds();
    }

    /**
     * Retourne la liste des éditions pour une description donnée.
     * 
     * @param description --
     * @return the list
     */
    @Override
    public List rechercherEditionParBeanId(String description)
    {
        return editionfinderserviceso.rechercherEditionParBeanId(description);
    }

    /**
     * Retourne la liste des éditions dont la demande a été effectuée à une date donnée.
     * 
     * @param dateDemande --
     * @return the list
     */
    @Override
    public List rechercherEditionParDateDemande(Date dateDemande)
    {
        return editionfinderserviceso.rechercherEditionParDateDemande(dateDemande);
    }

    /**
     * Recherche de l'historique d'une édition identifiée par son editionUuid.
     * 
     * @param editionUuid --
     * @return List
     */
    @Override
    public List<JobHistory> rechercherEditionParEditionUuid(String editionUuid)
    {
        List<JobHistory> result = new ArrayList<>();
        JobHistory jobHistory = editionfinderserviceso.rechercherEditionParUuid(editionUuid);

        if (jobHistory != null)
        {
            result.add(jobHistory);
        }
        return result;
    }

    /**
     * Retourne la liste des éditions qui sont dans un état donné.
     * 
     * @param etat --
     * @return the list
     */
    @Override
    public List rechercherEditionParEtat(String etat)
    {
        return editionfinderserviceso.rechercherEditionParEtat(etat);
    }

    /**
     * Retourne la liste des éditions dont la demande a été effectuée durant la période délimitée par les paramètres de
     * début et de fin de période.
     * 
     * @param debutPeriodeDemande --
     * @param finPeriodeDemande --
     * @return the list
     */
    @Override
    public List rechercherEditionParPeriodeDateDemande(Date debutPeriodeDemande, Date finPeriodeDemande)
    {
        return editionfinderserviceso.rechercherEditionParPeriodeDateDemande(debutPeriodeDemande, finPeriodeDemande);
    }

    /**
     * Recherche de l'historique des éditions accessibles à un profil donné.
     * 
     * @param profil --
     * @return the list< job history>
     */
    @Override
    public List<JobHistory> rechercherEditionParProfil(String profil)
    {
        return editionfinderserviceso.rechercherEditionParProfil(profil);
    }

    /**
     * Recherche de l'historique des éditions d'un utilisateur donné par son uid.
     * 
     * @param uid --
     * @return the list
     */
    @Override
    public List rechercherEditionParUid(String uid)
    {
        return editionfinderserviceso.rechercherEditionParUid(uid);
    }

    /**
     * Retourne la liste des profils autorisant l'accès à des éditions produites.
     * 
     * @return the list
     */
    @Override
    public List rechercherProfils()
    {
        return editionfinderserviceso.rechercheProfils();
    }

    /**
     * Sets the editionfinderserviceso.
     * 
     * @param editionfinderserviceso the editionfinderserviceso to set
     */
    public void setEditionfinderserviceso(EditionFinderService editionfinderserviceso)
    {
        this.editionfinderserviceso = editionfinderserviceso;
    }
}
