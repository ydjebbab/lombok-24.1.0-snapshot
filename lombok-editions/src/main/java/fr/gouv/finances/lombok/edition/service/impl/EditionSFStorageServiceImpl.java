/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : EditionSFStorageServiceImpl.java
 *
 */
package fr.gouv.finances.lombok.edition.service.impl;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import org.apache.commons.io.filefilter.DirectoryFileFilter;
import org.apache.commons.io.filefilter.FileFilterUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.Resource;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import fr.gouv.finances.lombok.edition.bean.JobHistory;
import fr.gouv.finances.lombok.edition.bean.StockageEdition;
import fr.gouv.finances.lombok.edition.dao.JobHistoryBatchDao;
import fr.gouv.finances.lombok.edition.dao.JobHistoryDao;
import fr.gouv.finances.lombok.edition.service.EditionStorageService;
import fr.gouv.finances.lombok.edition.util.Control;
import fr.gouv.finances.lombok.upload.bean.FichierJoint;
import fr.gouv.finances.lombok.util.base.BaseServiceImpl;
import fr.gouv.finances.lombok.util.exception.ApplicationExceptionTransformateur;
import fr.gouv.finances.lombok.util.exception.ExploitationException;
import fr.gouv.finances.lombok.util.exception.ProgrammationException;
import fr.gouv.finances.lombok.util.persistance.ScrollIterator;

/**
 * Class EditionSFStorageServiceImpl --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.3 $ Date: 11 déc. 2009
 */
public class EditionSFStorageServiceImpl extends BaseServiceImpl implements EditionStorageService
{
    /** Initialisation de la journalisation. */
    protected static final Logger log = LoggerFactory.getLogger(EditionSFStorageServiceImpl.class);

    /** Constante : LOT_JOUR_PREFIX. */
    public static final String LOT_JOUR_PREFIX = "LOT";

    /** Constante : SEP. */
    private static final String SEP = "/";

    /** Référence vers le Dao d'accès à l'historique des éditions. */
    private JobHistoryDao jobhistorydao;

    /** Nombre maximum de fichiers stockés dans un répertoire. Au delà, un nouveau répertoire est créé. */
    private long maxEditionParRepertoire;

    /** Chemin du répertoire de base de stockage des éditions produites. */
    private String appliEditionRoot;

    /** Référence vers le dao dédié aux batch de l'historique des éditions. */
    private JobHistoryBatchDao jobhistorybatchdao;

    /** transaction template. */
    private TransactionTemplate transactionTemplate;

    /**
     * Instanciation de edition sf storage service impl.
     */
    public EditionSFStorageServiceImpl()
    {
        super();
    }

    /**
     * methode Repertoire lot : --.
     * 
     * @param index --
     * @return string
     */
    public static String repertoireLot(Integer index)
    {
        StringBuilder lotDir = new StringBuilder();

        lotDir.append(LOT_JOUR_PREFIX);
        lotDir.append('.');
        lotDir.append(index.intValue());

        return lotDir.toString();
    }

    /**
     * Lit une édition stockée dans le SF et la retourne sous forme d'un objet FichierJoint.
     * 
     * @param jobHistory qui représente l'édition
     * @return Fichijoint associé à l'édition
     */
    public FichierJoint chargerFichierEdition(JobHistory jobHistory)
    {
        Control.assertJobHistoryNotNull(jobHistory);
        FichierJoint fichierJoint = null;

        if (JobHistory.STATUS_DISPONIBLE.equals(jobHistory.getOrdoEdition().getStatus())
            || JobHistory.STATUS_DISPONIBLE_NON_ENVOYE.equals(jobHistory.getOrdoEdition().getStatus()))

        {
            DefaultResourceLoader drl = new DefaultResourceLoader();
            Resource resfile =
                drl.getResource(jobHistory.getStockageEdition().getCheminComplet() + "/"
                    + jobHistory.getStockageEdition().getNomFicStockage());
            File file;

            try
            {
                file = resfile.getFile();
            }
            catch (IOException exception)
            {
                throw ApplicationExceptionTransformateur.transformer(exception);
            }

            if (!file.exists())
            {
                throw new ExploitationException("L'EDITION N'EXISTE PLUS");
            }
            else
            {
                ByteArrayOutputStream byteArrayOutput = this.loadFile(file);
                if (byteArrayOutput != null)
                {
                    fichierJoint = new FichierJoint(byteArrayOutput.toByteArray());
                    fichierJoint.setNomFichierOriginal(jobHistory.getPresentationEdition().getNomFicDownload());
                    fichierJoint.setTypeMimeFichier(jobHistory.getPresentationEdition().getTypeMime());
                    fichierJoint.setDateHeureSoumission(jobHistory.getOrdoEdition().getDateDemande());
                }
                else
                {
                    throw new ExploitationException("Aucune édition à charger pour le fichier " + file.getName());
                }
            }
        }
        return fichierJoint;
    }

    /**
     * Lit une édition stockée dans le SF et la retourne sous forme d'un objet FichierJoint Met à jour l'état de
     * consultation de l'édition.
     * 
     * @param jobHistory qui représente l'édition
     * @param uid --
     * @return Fichijoint associé à l'édition
     */
    @Override
    public FichierJoint chargerFichierEdition(JobHistory jobHistory, String uid)
    {
        log.debug(">>> Debut methode chargerFichierEdition");
        Control.assertJobHistoryNotNull(jobHistory);
        FichierJoint result = this.chargerFichierEdition(jobHistory);

        if (result == null)
        {
            throw new ExploitationException("L'EDITION N'EXISTE PLUS");
        }
        else
        {
            jobHistory.getDestinationEdition().metAJourLaListeDesConsultations(uid);
            jobhistorydao.saveJobHistory(jobHistory);
        }
        return result;
    }

    /**
     * methode Construit la liste des annees : --.
     * 
     * @param beanEditionId --
     * @return list< date>
     */
    public List<Date> construitLaListeDesAnnees(String beanEditionId)
    {
        // Contruit la liste des années
        List<Date> listeAnnees = new ArrayList<Date>();
        DefaultResourceLoader drl = new DefaultResourceLoader();
        Resource resdirectory =
            drl.getResource(this.appliEditionRoot + SEP + this.appliEditionRoot + SEP + beanEditionId);
        File file;

        try
        {
            file = resdirectory.getFile();
            if (file.isDirectory())
            {
                File[] fileTab = file.listFiles((FileFilter) FileFilterUtils.directoryFileFilter());

                for (int i = 0; i < fileTab.length; i++)
                {
                    Date annee = this.parseRepertoireAnnee(fileTab[i].getName());

                    if (annee != null)
                    {
                        listeAnnees.add(annee);
                    }
                }
            }
        }
        catch (IOException exception)
        {
            throw ApplicationExceptionTransformateur.transformer(exception);
        }

        return listeAnnees;
    }

    /**
     * methode Construit la liste des jours : --.
     * 
     * @param beanEditionId --
     * @param annee --
     * @param mois --
     * @param applicationOrigine --
     * @return list< date>
     */
    public List<Date> construitLaListeDesJours(String beanEditionId, Date annee, Date mois,
        final String applicationOrigine)
    {
        // Contruit la liste des jours
        List<Date> listeJours = new ArrayList<Date>();
        DefaultResourceLoader drl = new DefaultResourceLoader();
        Resource resdirectory =
            drl.getResource(this.appliEditionRoot + SEP + applicationOrigine + SEP + beanEditionId + SEP
                + StockageEdition.repertoireAnnee(annee) + SEP + StockageEdition.repertoireMois(mois));
        File file;

        try
        {
            file = resdirectory.getFile();
            if (file.isDirectory())
            {
                File[] fileTab = file.listFiles((FileFilter) FileFilterUtils.directoryFileFilter());

                for (int i = 0; i < fileTab.length; i++)
                {
                    Date jour = this.parseRepertoireJour(fileTab[i].getName());

                    if (jour != null)
                    {
                        listeJours.add(jour);
                    }
                }
            }
        }
        catch (IOException exception)
        {
            throw ApplicationExceptionTransformateur.transformer(exception);
        }

        return listeJours;
    }

    /**
     * methode Construit la liste des mois : --.
     * 
     * @param beanEditionId --
     * @param annee --
     * @param applicationOrigine --
     * @return list< date>
     */
    public List<Date> construitLaListeDesMois(String beanEditionId, Date annee, final String applicationOrigine)
    {
        // Contruit la liste des mois
        List<Date> listeMois = new ArrayList<Date>();
        DefaultResourceLoader drl = new DefaultResourceLoader();
        Resource resdirectory =
            drl.getResource(this.appliEditionRoot + SEP + applicationOrigine + SEP + beanEditionId + SEP
                + StockageEdition.repertoireAnnee(annee));
        File file;

        try
        {
            file = resdirectory.getFile();
            if (file.isDirectory())
            {
                File[] fileTab = file.listFiles((FileFilter) FileFilterUtils.directoryFileFilter());

                for (int i = 0; i < fileTab.length; i++)
                {
                    Date mois = this.parseRepertoireMois(fileTab[i].getName());

                    if (mois != null)
                    {
                        listeMois.add(mois);
                    }
                }
            }
        }
        catch (IOException exception)
        {
            throw ApplicationExceptionTransformateur.transformer(exception);
        }

        return listeMois;
    }

    /**
     * Efface une édition.
     * 
     * @param jobHistory --
     */
    public void effacerFichierEdition(JobHistory jobHistory)
    {
        Control.assertJobHistoryNotNull(jobHistory);
        String nomCompFicStock =
            StockageEdition.calculeRepertoireStockageN7(jobHistory.getOrdoEdition().getDateDemande(), jobHistory
                .getDestinationEdition().getApplicationOrigine(), jobHistory.getBeanEditionId(), jobHistory
                .getStockageEdition().getIndexRepertoire(), jobHistory.getStockageEdition().getAppliEditionRoot());

        jobHistory.getStockageEdition().setCheminComplet(nomCompFicStock);
        this.supprimerFichier(jobHistory.getStockageEdition().fournirCheminAbsoluFichier());
    }

    /**
     * methode Parses the date production : --.
     * 
     * @param date --
     * @return date
     */
    public Date parseDateProduction(String date)
    {
        Date result;
        DateFormat unJourFormat = new SimpleDateFormat("yyyyMMdd", Locale.FRANCE);

        try
        {
            result = unJourFormat.parse(date);
        }
        catch (ParseException exception)
        {
            result = null;
        }
        return result;
    }

    /**
     * methode Parses the repertoire annee : --.
     * 
     * @param repertoire --
     * @return date
     */
    public Date parseRepertoireAnnee(String repertoire)
    {
        Date result;
        DateFormat unJourFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.FRANCE);

        try
        {
            StringBuilder temp = new StringBuilder();

            temp.append("01/01/");
            temp.append(repertoire);
            result = unJourFormat.parse(temp.toString());
        }
        catch (ParseException exception)
        {
            result = null;
        }
        return result;
    }

    /**
     * methode Parses the repertoire jour : --.
     * 
     * @param repertoire --
     * @return date
     */
    public Date parseRepertoireJour(String repertoire)
    {
        Date result;
        DateFormat unJourFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.FRANCE);

        try
        {
            StringBuilder temp = new StringBuilder();

            temp.append(repertoire);
            temp.append("/01/1970");
            result = unJourFormat.parse(temp.toString());
        }
        catch (ParseException exception)
        {
            result = null;
        }
        return result;
    }

    /**
     * methode Parses the repertoire mois : --.
     * 
     * @param repertoire --
     * @return date
     */
    public Date parseRepertoireMois(String repertoire)
    {
        Date result;
        DateFormat unMoisFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.FRANCE);

        try
        {
            StringBuilder temp = new StringBuilder();

            temp.append("01/");
            temp.append(repertoire);
            temp.append("/1970");
            result = unMoisFormat.parse(temp.toString());
        }
        catch (ParseException exception)
        {
            result = null;
        }
        return result;
    }

    /**
     * Sets the appli edition root.
     * 
     * @param appliEditionRoot the appliEditionRoot to set
     */
    public void setAppliEditionRoot(String appliEditionRoot)
    {
        this.appliEditionRoot = appliEditionRoot;
    }

    /**
     * Modificateur de l attribut jobhistorybatchdao.
     * 
     * @param jobhistorybatchdao le nouveau jobhistorybatchdao
     */
    public void setJobhistorybatchdao(JobHistoryBatchDao jobhistorybatchdao)
    {
        this.jobhistorybatchdao = jobhistorybatchdao;
    }

    /**
     * Sets the jobhistorydao.
     * 
     * @param jobhistorydao the jobhistorydao to set
     */
    public void setJobhistorydao(JobHistoryDao jobhistorydao)
    {
        this.jobhistorydao = jobhistorydao;
    }

    /**
     * Sets the max edition par repertoire.
     * 
     * @param maxEditionParRepertoire the maxEditionParRepertoire to set
     */
    public void setMaxEditionParRepertoire(long maxEditionParRepertoire)
    {
        this.maxEditionParRepertoire = maxEditionParRepertoire;
    }

    /**
     * Modificateur de l attribut transaction template.
     * 
     * @param transactionTemplate le nouveau transaction template
     */
    public void setTransactionTemplate(TransactionTemplate transactionTemplate)
    {
        this.transactionTemplate = transactionTemplate;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param jobHistory
     * @param fichierJoint
     * @see fr.gouv.finances.lombok.edition.service.EditionStorageService#stockerEdition(fr.gouv.finances.lombok.edition.bean.JobHistory,
     *      fr.gouv.finances.lombok.upload.bean.FichierJoint)
     */
    public void stockerEdition(JobHistory jobHistory, FichierJoint fichierJoint)
    {
        Control.assertJobHistoryNotNull(jobHistory);

        this.construitRepertoireStockage(jobHistory);
        jobHistory.getStockageEdition().setTailleFichier(Long.valueOf(fichierJoint.getTailleFichier()));

        DefaultResourceLoader drl = new DefaultResourceLoader();

        String cheminStockage = jobHistory.getStockageEdition().fournirCheminAbsoluFichier();

        Resource resource = drl.getResource(cheminStockage);

        FileOutputStream output = null;
        try
        {
            output = new FileOutputStream(resource.getFile());
            output.write(fichierJoint.getLeContenuDuFichier().getData());
            output.flush();
        }
        catch (FileNotFoundException exce)
        {
            throw ApplicationExceptionTransformateur.transformer(exce);
        }
        catch (IOException ioex)
        {
            throw ApplicationExceptionTransformateur.transformer(ioex);
        }
        finally
        {
            try
            {
                if (output != null)
                {
                    // ferme le stream
                    output.close();
                }
            }
            catch (IOException ioex)
            {
                log.warn("Erreur lors de la fermeture du flux", ioex);
            }
        }
    }

    /**
     * Supprime toutes les entrées de l'historique dans l'état DISPONIBLE dont le fichier dans le SF n'existe plus.
     * 
     * @param nombreOccurences --
     * @param applicationOrigine --
     */    
    public void synchroniseHistoriqueAvecSf(final int nombreOccurences, final String applicationOrigine)
    {

        transactionTemplate.execute(new TransactionCallbackWithoutResult()
        {
            public void doInTransactionWithoutResult(TransactionStatus status)
            {
                JobHistory jobHistory;
                int compteur = 0;
                List<JobHistory> lesEditionsASupprimer = new ArrayList<JobHistory>();

                ScrollIterator editionsDisponiblesIterator =
                    jobhistorybatchdao.listeEditionsDisponiblesIterator(nombreOccurences, applicationOrigine);

                while (editionsDisponiblesIterator.hasNext())
                {
                    // debut boucle sur
                    // editionsDisponiblesIterator
                    jobHistory = (JobHistory) ((Object[]) editionsDisponiblesIterator.next())[0];
                    if (jobHistory != null)
                    {
                        if (!testExistenceFichierEdition(jobHistory))
                        {
                            lesEditionsASupprimer.add(jobHistory);
                        }
                        if (compteur++ % nombreOccurences == 0)
                        {
                            jobhistorydao.deleteJobsHistories(lesEditionsASupprimer);
                            lesEditionsASupprimer = new ArrayList<JobHistory>();
                        }
                    }

                }
                // fin boucle sur
                // editionsDisponiblesIterator
                jobhistorydao.deleteJobsHistories(lesEditionsASupprimer);
                editionsDisponiblesIterator.close();
            }
        });
    }

    /**
     * Supprime tous les fichiers de l'arborescence qui ne correspondent à aucune entrée dans l'historique des édition.
     * 
     * @param applicationOrigine --
     */
    public void synchroniseSfAvecHistorique(final String applicationOrigine)
    {

        // On récupère la liste des répertoires qui sont placés dans
        // appliEditionRoot/appliEditionOrigine
        // Le nom de chacun de ces répertoires correspond à un nom
        // d'édition IdBeanEdition
        List<String> listeDesEditions = this.contruitListeDesEditionsDansLeSf(applicationOrigine);

        // On itère sur les répertoires de chaque édition
        for (Iterator<String> beanEditionIter = listeDesEditions.iterator(); beanEditionIter.hasNext();)
        {
            String beanEditionId = beanEditionIter.next();
            // On récupère la liste des dates de production
            // d'édition
            // à partir de chaque triplet de sous-répertoire qui
            // doivent
            // correspondre à yyyy/mm/dd
            List<Date> datesDeProduction = this.construitLaListeDesDatesDeProduction(beanEditionId, applicationOrigine);

            // On itère sur la liste des dates de production
            for (Iterator<Date> datesDeProductionIter = datesDeProduction.iterator(); datesDeProductionIter.hasNext();)
            {
                Date dateDeProduction = datesDeProductionIter.next();

                // On récupère la liste des indexs de lots
                // d'édition
                List<Integer> listeDesIndexDeLots =
                    this.construitLaListeDesIndexDeLots(beanEditionId, dateDeProduction, applicationOrigine);

                // On itère sur la liste des indexs de lots

                for (Iterator<Integer> indexDeLotIter = listeDesIndexDeLots.iterator(); indexDeLotIter.hasNext();)
                {
                    Integer index = indexDeLotIter.next();

                    // On récupère la liste des fichiers
                    // correspondants à une édition
                    String repertoireStockage =
                        StockageEdition.calculeRepertoireStockageN7(dateDeProduction, applicationOrigine,
                            beanEditionId, index, this.appliEditionRoot);

                    List<String> listeDesFichiers = this.construitLaListeDesFichiers(repertoireStockage);

                    // On itère sur la liste des fichiers
                    for (Iterator<String> fichiersIter = listeDesFichiers.iterator(); fichiersIter.hasNext();)
                    {
                        String nomFichier = fichiersIter.next();
                        String editionUuid = this.extraitUuid(nomFichier);
                        String suffixe = this.extraitSuffixe(nomFichier);

                        JobHistory jobHistorySf = new JobHistory();

                        jobHistorySf.setEditionUuid(editionUuid);
                        jobHistorySf.setBeanEditionId(beanEditionId);
                        jobHistorySf.getOrdoEdition().setDateDemande(dateDeProduction);
                        jobHistorySf.getDestinationEdition().setApplicationOrigine(applicationOrigine);
                        jobHistorySf.getStockageEdition().setCheminComplet(repertoireStockage);
                        jobHistorySf.getStockageEdition().setNomFicStockage(nomFichier);
                        jobHistorySf.getPresentationEdition().setExtension(suffixe);
                        jobHistorySf.getStockageEdition().setAppliEditionRoot(this.appliEditionRoot);
                        jobHistorySf.getStockageEdition().setIndexRepertoire(index);

                        this.synchronise(jobHistorySf);
                    }
                }

            }
        }
    }

    /**
     * Calcule un numéro d'index de lot dans le répertoire destockage des éditions d'un jour.
     * 
     * @param repertoireStockageFile --
     * @return the integer
     */
    private Integer calculeIndexRepertoire(File repertoireStockageFile)
    {
        Integer result = null;

        // Liste les répertoires des lots de la journée en cours pour
        // l'application et l'édition donnée
        FileFilter fileFilter =
            FileFilterUtils.andFileFilter(DirectoryFileFilter.INSTANCE, FileFilterUtils
                .prefixFileFilter(StockageEdition.LOT_JOUR_PREFIX));
        File[] tabDirJour = repertoireStockageFile.listFiles(fileFilter);

        int lotEnCours = 0;
        File fileEnCours = null;

        for (int i = 0; i < tabDirJour.length; i++)
        {
            // Les répertoires à utiliser sont de la forme LOT.X
            File file = tabDirJour[i];
            String fileName = file.getName();
            int indexpoint = fileName.indexOf('.');

            if (indexpoint == -1)
            {
                continue;
            }
            else
            {
                String suffix = fileName.substring(indexpoint + 1, fileName.length());
                int numeroLot;

                try
                {
                    numeroLot = Integer.parseInt(suffix);
                }
                catch (NumberFormatException exception)
                {
                    continue;
                }
                if (numeroLot > lotEnCours)
                {
                    lotEnCours = numeroLot;
                    fileEnCours = file;
                }
            }
        }

        int nombreFichiersDansLot = 0;

        // Si aucun lot n'a encore été créé
        if (lotEnCours == 0)
        {
            result = Integer.valueOf(1);
        }
        else
        {
            // On compte le nombre de fichiers dans le dernier lot
            if (fileEnCours != null)
            {
                String[] fichiersDuRepertoire = fileEnCours.list();

                if (fichiersDuRepertoire != null)
                {
                    nombreFichiersDansLot = fichiersDuRepertoire.length;
                }

                if (nombreFichiersDansLot > this.maxEditionParRepertoire)
                {
                    result = Integer.valueOf(lotEnCours + 1);
                }
                else
                {
                    result = Integer.valueOf(lotEnCours);
                }
            }
        }

        return result;
    }

    /**
     * methode Construit la liste des dates de production : --.
     * 
     * @param beanEditionId --
     * @param applicationOrigine --
     * @return list< date>
     */
    private List<Date> construitLaListeDesDatesDeProduction(String beanEditionId, final String applicationOrigine)
    {
        List<Date> result = new ArrayList<Date>();

        List<Date> listeDesAnnees = this.construitLaListeDesAnnees(beanEditionId);

        for (Iterator<Date> anneesIter = listeDesAnnees.iterator(); anneesIter.hasNext();)
        {
            Date annee = anneesIter.next();
            List<Date> listeDesMois = this.construitLaListeDesMois(beanEditionId, annee, applicationOrigine);

            for (Iterator<Date> moisIter = listeDesMois.iterator(); moisIter.hasNext();)
            {
                Date mois = moisIter.next();
                List<Date> listeDesJours =
                    this.construitLaListeDesJours(beanEditionId, annee, mois, applicationOrigine);

                for (Iterator<Date> jourIter = listeDesJours.iterator(); jourIter.hasNext();)
                {
                    Date jour = jourIter.next();
                    if (annee != null && mois != null && jour != null)
                    {
                        String dateProductionS =
                            StockageEdition.repertoireAnnee(annee) + StockageEdition.repertoireMois(mois)
                                + StockageEdition.repertoireJour(jour);

                        Date dateProduction = this.parseDateProduction(dateProductionS);

                        if (dateProduction != null)
                        {
                            result.add(dateProduction);
                        }
                    }
                }
            }
        }

        return result;
    }

    /**
     * methode Construit la liste des fichiers : --.
     * 
     * @param repertoireStockage --
     * @return list< string>
     */
    private List<String> construitLaListeDesFichiers(String repertoireStockage)
    {
        // Contruit la liste des fichiers dans un lot
        List<String> listeFichiers = new ArrayList<String>();
        DefaultResourceLoader drl = new DefaultResourceLoader();

        Resource resdirectory = drl.getResource(repertoireStockage);

        File file;

        try
        {
            file = resdirectory.getFile();
            if (file.isDirectory())
            {
                File[] fileTab =
                    file.listFiles((FileFilter) FileFilterUtils.notFileFilter(FileFilterUtils.directoryFileFilter()));

                for (int i = 0; i < fileTab.length; i++)
                {
                    String fileName = fileTab[i].getName();

                    if (fileName != null)
                    {
                        listeFichiers.add(fileName);
                    }
                }
            }
        }
        catch (IOException exception)
        {
            throw ApplicationExceptionTransformateur.transformer(exception);
        }

        return listeFichiers;
    }

    /**
     * methode Construit la liste des index de lots : --.
     * 
     * @param beanEditionId --
     * @param dateDeProduction --
     * @param applicationOrigine --
     * @return list< integer>
     */
    private List<Integer> construitLaListeDesIndexDeLots(String beanEditionId, Date dateDeProduction,
        final String applicationOrigine)
    {
        // Contruit la liste des index de lots
        List<Integer> listeLots = new ArrayList<Integer>();
        DefaultResourceLoader drl = new DefaultResourceLoader();
        Resource resdirectory =
            drl.getResource(appliEditionRoot + SEP + applicationOrigine + SEP + beanEditionId + SEP
                + StockageEdition.repertoireAnnee(dateDeProduction) + SEP
                + StockageEdition.repertoireMois(dateDeProduction) + SEP
                + StockageEdition.repertoireJour(dateDeProduction));
        File file;

        try
        {
            file = resdirectory.getFile();
            if (file.isDirectory())
            {
                FileFilter fileFilter =
                    FileFilterUtils.andFileFilter(DirectoryFileFilter.INSTANCE, FileFilterUtils
                        .prefixFileFilter(LOT_JOUR_PREFIX));
                File[] fileTab = file.listFiles(fileFilter);

                for (int i = 0; i < fileTab.length; i++)
                {
                    File dirLot = fileTab[i];
                    String fileName = dirLot.getName();
                    int indexpoint = fileName.indexOf('.');

                    if (indexpoint == -1)
                    {
                        continue;
                    }
                    else
                    {
                        String suffix = fileName.substring(indexpoint + 1, fileName.length());
                        Integer numeroLot;

                        try
                        {
                            numeroLot = Integer.valueOf(suffix);
                            listeLots.add(numeroLot);
                        }
                        catch (NumberFormatException e)
                        {
                            continue;
                        }
                    }
                }
            }
        }
        catch (IOException exception)
        {
            throw ApplicationExceptionTransformateur.transformer(exception);
        }

        return listeLots;
    }

    /**
     * Construit si nécessaire le répertoire de stockage d'une édition et met à jour l'objet JobHistory avec les
     * éléments de stockage calculés.
     * 
     * @param jobHistory --
     * @return jobHistory
     */
    private JobHistory construitRepertoireStockage(JobHistory jobHistory)
    {
        Control.assertJobHistoryNotNull(jobHistory);
        Date datejour = new Date();

        jobHistory.getStockageEdition().setAppliEditionRoot(this.appliEditionRoot);
        jobHistory.getStockageEdition().setNomFicStockage(
            jobHistory.getEditionUuid() + "." + jobHistory.getPresentationEdition().getExtension());

        String repertoireStockageJour =
            StockageEdition.calculeRepertoireStockageN6(datejour, jobHistory.getDestinationEdition()
                .getApplicationOrigine(), jobHistory.getBeanEditionId(), jobHistory.getStockageEdition()
                .getAppliEditionRoot());

        File repertoireStockageFile = this.controlOrCreateDirectory(repertoireStockageJour);
        Integer indexRep = this.calculeIndexRepertoire(repertoireStockageFile);

        String cheminComplet =
            StockageEdition.calculeRepertoireStockageN7(datejour, jobHistory.getDestinationEdition()
                .getApplicationOrigine(), jobHistory.getBeanEditionId(), indexRep, jobHistory.getStockageEdition()
                .getAppliEditionRoot());

        this.controlOrCreateDirectory(cheminComplet);

        jobHistory.getStockageEdition().setIndexRepertoire(indexRep);
        jobHistory.getStockageEdition().setCheminComplet(cheminComplet);

        return jobHistory;
    }

    /**
     * Contrôle qu'un répertoire existe. Si ce n'est pas le cas, crée les répertoires nécessaires
     * 
     * @param dir --
     * @return File
     */
    private File controlOrCreateDirectory(String dir)
    {
        DefaultResourceLoader drl = new DefaultResourceLoader();
        Resource resdirectory = drl.getResource(dir);
        File file;

        try
        {
            file = resdirectory.getFile();
        }
        catch (IOException ioe)
        {
            throw ApplicationExceptionTransformateur.transformer(ioe);
        }

        // Création du répertoire si nécessaire
        if (!file.exists())
        {
            boolean result;

            result = file.mkdirs();
            // Si le répertoire n'a pas pu être créé et
            // si le répertoire a été créé entre temps par un autre
            // thread => OK
            if ((!result) && (!file.exists()))
            {
                throw new ExploitationException("IMPOSSIBLE DE CREER LE REPERTOIRE " + dir);
            }
        }

        // On teste qu'il s'agit bien d'un répertoire
        if (!file.isDirectory())
        {
            throw new ProgrammationException("LE CHEMIN " + dir + " NE POINTE PAS VERS UN REPERTOIRE");
        }

        // On teste que le répertoire est bien accessible en lecture
        // /écriture
        if (!(file.canRead() && file.canRead()))
        {
            throw new ExploitationException("LE REPERTOIRE " + dir + " N'EST PAS ACCESSIBLE EN LECTURE / ECRITURE");
        }

        return file;
    }

    /**
     * Contruit à partir la liste des sous-répertoires du répertoire appliEditionRoot/appliEditionOrigine la liste des
     * editionBeanId.
     * 
     * @param applicationOrigine --
     * @return the list< string>
     */
    private List<String> contruitListeDesEditionsDansLeSf(final String applicationOrigine)
    {
        List<String> result = new ArrayList<String>();
        DefaultResourceLoader drl = new DefaultResourceLoader();
        Resource resdirectory = drl.getResource(this.appliEditionRoot + SEP + applicationOrigine);
        File file;

        try
        {
            file = resdirectory.getFile();
            if (file.isDirectory())
            {
                File[] fileTab = file.listFiles((FileFilter) FileFilterUtils.directoryFileFilter());

                for (int i = 0; i < fileTab.length; i++)
                {
                    result.add(fileTab[i].getName());
                }
            }
        }
        catch (IOException exception)
        {
            throw ApplicationExceptionTransformateur.transformer(exception);
        }

        return result;
    }

    /**
     * methode Extrait suffixe : --.
     * 
     * @param fichier --
     * @return string
     */
    private String extraitSuffixe(String fichier)
    {
        String suffix;

        if (fichier == null)
        {
            suffix = null;
        }
        else
        {
            int indexpoint = fichier.indexOf('.');

            if (indexpoint == -1)
            {
                suffix = null;
            }
            else
            {
                suffix = fichier.substring(indexpoint + 1, fichier.length());
            }
        }
        return suffix;
    }

    /**
     * methode Extrait uuid : --.
     * 
     * @param fichier --
     * @return string
     */
    private String extraitUuid(String fichier)
    {
        String uuid;

        if (fichier == null)
        {
            uuid = null;
        }
        else
        {
            int indexpoint = fichier.indexOf('.');
            if (indexpoint == -1)
            {
                uuid = null;
            }
            else
            {
                uuid = fichier.substring(0, indexpoint);
            }
        }
        return uuid;
    }

    /**
     * Chargement en mémoire d'un fichier.
     * 
     * @param file --
     * @return ByteArrayOutputStream
     */
    private ByteArrayOutputStream loadFile(File file)
    {
        byte[] buffer = new byte[1024];
        FileInputStream input = null;
        ByteArrayOutputStream byteArrayOutput = null;

        try
        {
            input = new FileInputStream(file);

            byteArrayOutput = new ByteArrayOutputStream(input.available());
            int byteRead;

            do
            {
                byteRead = input.read(buffer);
                if (byteRead > 0)
                {
                    byteArrayOutput.write(buffer, 0, byteRead);
                }
            }
            while (byteRead > 0);

        }
        catch (FileNotFoundException fnof)
        {
            throw ApplicationExceptionTransformateur.transformer(fnof);
        }
        catch (IOException ioe)
        {
            throw ApplicationExceptionTransformateur.transformer(ioe);
        }
        finally
        {
            try
            {
                if (input != null)
                {
                    // ferme le stream
                    input.close();
                }
            }
            catch (IOException ioex)
            {
                log.warn("Erreur lors de la fermeture du flux", ioex);
            }
        }
        return byteArrayOutput;
    }

    /**
     * Supprime un fichier du SF à partir du chemin absolu du fichier.
     * 
     * @param cheminCompletFichier --
     */
    private void supprimerFichier(String cheminCompletFichier)
    {
        DefaultResourceLoader drl = new DefaultResourceLoader();
        Resource resfile = drl.getResource(cheminCompletFichier);
        File file;

        try
        {
            file = resfile.getFile();
            boolean suppressionOk = file.delete();
            if (!suppressionOk)
            {
                log.warn("Le fichier '{}' n'a pas été supprimé", file.getAbsolutePath());
            }
        }
        catch (FileNotFoundException fnfe)
        {
            log.warn("LE FICHIER '{}' A DEJA ETE SUPPRIME.", cheminCompletFichier, fnfe);
            // Rien à faire, le fichier a été supprimé par un autre moyen
        }
        catch (IOException ioe)
        {
            log.warn("Exception détectée", ioe);
            throw ApplicationExceptionTransformateur.transformer(ioe);
        }
    }

    /**
     * Teste si les paramètres passés correspondent à une entrée dans l'historique des éditions Si le test est négatif,
     * le fichier est supprimé du SF.
     * 
     * @param jobHistory --
     */
    private void synchronise(JobHistory jobHistory)
    {
        if (!jobhistorydao.isValidEdition(jobHistory))
        {
            this.effacerFichierEdition(jobHistory);
        }
    }

    /**
     * methode Test existence fichier edition : --.
     * 
     * @param jobHistory --
     * @return true, si c'est vrai
     */
    private boolean testExistenceFichierEdition(JobHistory jobHistory)
    {
        boolean fichierExiste = false;

        DefaultResourceLoader drl = new DefaultResourceLoader();
        String nomFichier = jobHistory.getStockageEdition().fournirCheminAbsoluFichier();
        Resource resfile = drl.getResource(nomFichier);
        File file;

        try
        {
            file = resfile.getFile();
            if (file.exists() && file.isFile())
            {
                fichierExiste = true;
            }
        }
        catch (IOException exception)
        {
            StringBuilder msg = new StringBuilder();
            msg.append("Le fichier ");
            msg.append(nomFichier);
            msg.append(" n'existe pas");
            log.warn(msg.toString(), exception);
        }
        return fichierExiste;
    }

}
