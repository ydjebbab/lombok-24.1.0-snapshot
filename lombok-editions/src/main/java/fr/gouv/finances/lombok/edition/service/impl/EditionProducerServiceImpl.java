/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : EditionProducerServiceImpl.java
 *
 */
package fr.gouv.finances.lombok.edition.service.impl;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ApplicationObjectSupport;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.transaction.support.TransactionTemplate;

import fr.gouv.finances.lombok.edition.bean.EditionAsynchroneNonNotifieeParMail;
import fr.gouv.finances.lombok.edition.bean.EditionAsynchroneNotifieeParMail;
import fr.gouv.finances.lombok.edition.bean.EditionSynchroneNonNotifieeParMail;
import fr.gouv.finances.lombok.edition.bean.EditionSynchroneNotifieeParMail;
import fr.gouv.finances.lombok.edition.bean.JobHistory;
import fr.gouv.finances.lombok.edition.bean.MailEdition;
import fr.gouv.finances.lombok.edition.bean.OrdoEdition;
import fr.gouv.finances.lombok.edition.dao.JobHistoryDao;
import fr.gouv.finances.lombok.edition.service.EditionProducerService;
import fr.gouv.finances.lombok.edition.service.EditionStorageService;
import fr.gouv.finances.lombok.edition.service.ServiceEditionCommun;
import fr.gouv.finances.lombok.edition.service.ServiceEditionJasperCommun;
import fr.gouv.finances.lombok.edition.util.Control;
import fr.gouv.finances.lombok.mel.service.MelService;
import fr.gouv.finances.lombok.mel.techbean.Mel;
import fr.gouv.finances.lombok.mel.techbean.PieceJointe;
import fr.gouv.finances.lombok.securite.springsecurity.UtilisateurSecurityLombokContainer;
import fr.gouv.finances.lombok.upload.bean.FichierJoint;
import fr.gouv.finances.lombok.util.exception.ApplicationException;
import fr.gouv.finances.lombok.util.exception.ApplicationExceptionTransformateur;
import fr.gouv.finances.lombok.util.exception.ProgrammationException;
import fr.gouv.finances.lombok.util.format.FormaterDate;

/**
 * Class EditionProducerServiceImpl --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.3 $ Date: 11 déc. 2009
 */
public class EditionProducerServiceImpl extends ApplicationObjectSupport implements EditionProducerService
{

    protected static final Logger log = LoggerFactory.getLogger(EditionProducerServiceImpl.class);
    
    /** Suffixe du fichier temporaire utilisé pour compresser l'édition. */
    private static final String FICHIER_TEMPORAIRE_ZIP_SUFFIXE = ".zip";

    /** transaction template. */
    protected TransactionTemplate transactionTemplate;

    /** Référence de la fabrique d'éditions. */
    protected ApplicationContext editionContext;

    /** Taille max en octet des fichiers qui peuvent être transmis en pièce jointe. */
    private long tailleMaxPieceJointe;

    /** Nom de l'application à l'origine de l'édition. */
    private String appliorigine;

    /** Mode d'authentification utilisé par l'application Peut prendre comme valeur portail, appli ou appliportail. */
    private String modeauthentification;

    /** base de l'url permettant d'accéder au flux de présentation des éditions asynchrones. */
    private String applibaseurl;

    /** base de l'url permettant d'accéder via internet au flux de présentation des éditions asynchrones. */
    private String internetbaseurl;

    /** base de l'url permettant d'accéder via intranet au flux de présentation des éditions asynchrones. */
    private String intranetbaseurl;

    /** Morceau d'url permettant d'accéder au flux de présentation des éditions asynchrones. */
    private String fluxUrl;

    /** Expéditeur du mail. */
    private String mailDe;

    /** Référence du serveur de mail. */
    private MelService serveurmelserviceso;

    /** Réference du service de stockage des édition. */
    private EditionStorageService editionstorageserviceso;

    /** Référence vers le Dao d'accès à l'historique des éditions. */
    private JobHistoryDao jobhistorydao;

    /**
     * Instanciation de edition producer service impl.
     */
    public EditionProducerServiceImpl()
    {
        super();
    }

    /**
     * Produit un fichier d'édition compressé en mode synchrone.
     * 
     * @param parametresEdition --
     * @param fichierJoint --
     * @return the fichier joint
     */
    public FichierJoint compresserEdition(Map parametresEdition, final FichierJoint fichierJoint)
    {
        FichierJoint result = null;
        File tempZipFile = null;
        OutputStream outputStream = null;
        ZipOutputStream zipOutputStream = null;
        InputStream inputStream = null;
        BufferedInputStream bufferedInputStream = null;

        try
        {
            // Création d'un stream en lecture sur les données du FichierJoint
            if (this.testContenuFichierJoint(fichierJoint))
            {
                if (fichierJoint.isDataInByteArray())
                {
                    outputStream = new ByteArrayOutputStream();
                    inputStream = new ByteArrayInputStream(fichierJoint.getLeContenuDuFichier().getData());
                }
                else
                {
                    tempZipFile =
                        File.createTempFile(fichierJoint.getNomFichierOriginal(), FICHIER_TEMPORAIRE_ZIP_SUFFIXE);
                    tempZipFile.deleteOnExit();
                    outputStream = new FileOutputStream(tempZipFile);
                    inputStream = new FileInputStream(fichierJoint.getLeContenuDuFichier().getFichierTemporaire());
                }

                zipOutputStream = new ZipOutputStream(outputStream);

                // Création at ajout d'une entrée dans le fichier zip
                ZipEntry zipEntry = new ZipEntry(fichierJoint.getNomFichierOriginal());
                zipOutputStream.putNextEntry(zipEntry);

                // Création d'un BufferedStream
                bufferedInputStream = new BufferedInputStream(inputStream);

                int len;
                byte[] buff = new byte[1024];

                // Compression des données
                while ((len = bufferedInputStream.read(buff)) >= 0)
                {
                    zipOutputStream.write(buff, 0, len);
                }
                zipOutputStream.flush();

                // Fermeture de l'entrée zip
                zipOutputStream.closeEntry();

                // Fin d'écriture sur le stream de compression sans
                // fermer le stream sous jacent
                zipOutputStream.finish();

                // Affectation du fichier compresse au fichierJoint
                if (fichierJoint.isDataInByteArray())
                {
                    result = new FichierJoint(((ByteArrayOutputStream) outputStream).toByteArray());
                }
                else
                {
                    result = new FichierJoint(tempZipFile);
                }

                // Modification du nom du fichier (ajout de .zip)
                result.setNomFichierOriginal(fichierJoint.getNomFichierOriginal() + ".zip");
                result.setTypeMimeFichier("application/zip");

            }

        }
        catch (IOException exception)
        {
            log.warn("ERREUR D'ENTREE / SORTIE LORS DE LA COMPRESSION DU FICHIER");
            throw ApplicationExceptionTransformateur.transformer(exception);
        }
        finally
        {
            close(bufferedInputStream, zipOutputStream, outputStream, inputStream);
        }

        return result;
    }

    /**
     * Méthode permettant de fermer les flux ouverts
     *
     * @param bufferedInputStream flux 1
     * @param zipOutputStream flux 2
     * @param outputStream flux 3
     * @param inputStream flux 4
     */
    private void close(BufferedInputStream bufferedInputStream, ZipOutputStream zipOutputStream
        , OutputStream outputStream, InputStream inputStream)
    {
        if (bufferedInputStream != null)
        {
            try
            {
                bufferedInputStream.close();
            }
            catch (IOException exception)
            {
                log.warn("ERREUR LORS DE LA FERMETURE DU STREAM EN ENTREE", exception);
            }
        }

        if (zipOutputStream != null)
        {
            try
            {
                zipOutputStream.close();
            }
            catch (IOException exception)
            {
                log.warn("ERREUR LORS DE LA FERMETURE DU STREAM EN SORTIE", exception);
            }
        }

        if (outputStream != null)
        {
            try
            {
                outputStream.close();
            }
            catch (IOException exception)
            {
                log.warn("ERREUR LORS DE LA FERMETURE  FLUX", exception);
            }
        }
        if (inputStream != null)
        {
            try
            {
                inputStream.close();
            }
            catch (IOException exception)
            {
                log.warn("ERREUR LORS DE LA FERMETURE  FLUX", exception);
            }
        }
    }
    
    /**
     * Exécute une édition de façon synchrone.
     * 
     * @param editionBeanId --
     * @param parametresEdition --
     * @return FichierJoint contenant l'édition
     */
    @Override
    public FichierJoint creerEdition(String editionBeanId, Map parametresEdition)
    {
        FichierJoint result = null;
        ServiceEditionCommun serviceEdition = this.getReferenceServiceEditionCommun(editionBeanId);

        result = serviceEdition.produireFichierEdition(parametresEdition);
        return result;
    }

    /**
     * Execute une edition de façon synchrone, puis la compresse.
     * 
     * @param editionBeanId --
     * @param parametresEdition --
     * @return the fichier joint
     */
    @Override
    public FichierJoint creerEditionCompressee(String editionBeanId, Map parametresEdition)
    {
        return this.compresserEdition(parametresEdition, this.creerEdition(editionBeanId, parametresEdition));
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param editionBeanId
     * @param collection
     * @param mapParametres
     * @return fichier joint
     * @see fr.gouv.finances.lombok.edition.service.EditionProducerService#creerEditionFromCollectionEtParametres(java.lang.String,
     *      java.util.Collection, java.util.Map)
     */
    @Override
    public FichierJoint creerEditionFromCollectionEtParametres(String editionBeanId, Collection collection,
        Map mapParametres)
    {
        ServiceEditionJasperCommun serviceEdition = this.getReferenceServiceEditionJasperCommun(editionBeanId);
        return serviceEdition.produireFichierEditionFromCollection(collection, mapParametres);
    }

    /**
     * Crée une édition HTML.
     * 
     * @param editionBeanId --
     * @param mapParametres --
     * @return Map contenant : - le fichier de l'édition sous la clé CpJasperReportsHtmlView.FICHIER_JOINT_KEY-un objet
     *         jasperprint sous la clé CpJasperReportsHtmlView.JASPER_PRINT_KEY
     */
    @Override
    public Map creerEditionHtml(String editionBeanId, Map mapParametres)
    {
        ServiceEditionCommun serviceEdition = this.getReferenceServiceEditionCommun(editionBeanId);
        return serviceEdition.produitEditionHtml(mapParametres);
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param editionBeanId
     * @param collection
     * @param mapParametres
     * @return map
     * @see fr.gouv.finances.lombok.edition.service.EditionProducerService#creerEditionHtmlFromCollectionEtParametres(java.lang.String,
     *      java.util.Collection, java.util.Map)
     */
    @Override
    public Map creerEditionHtmlFromCollectionEtParametres(String editionBeanId, Collection collection, Map mapParametres)
    {
        ServiceEditionJasperCommun serviceEdition = this.getReferenceServiceEditionJasperCommun(editionBeanId);
        return serviceEdition.produitEditionHtmlFromCollection(collection, mapParametres);
    }

    /**
     * Crée et stocke une édition.
     * 
     * @param jobHistory --
     * @param parametresEdition --
     */
    @Override
    public void creerEtStockerEdition(JobHistory jobHistory, Map parametresEdition)
    {
        this.creerEtStockerEdition(jobHistory, parametresEdition, Boolean.FALSE);
    }

    /**
     * Crée, compresse et stocke une édition.
     * 
     * @param jobHistory --
     * @param parametresEdition --
     */
    @Override
    public void creerEtStockerEditionCompressee(JobHistory jobHistory, Map parametresEdition)
    {
        this.creerEtStockerEdition(jobHistory, parametresEdition, Boolean.TRUE);
    }

    /**
     * Enregistre une demande d'édition lors de son déclenchement par l'utilisateur.
     * 
     * @param jobHistory --
     * @param status --
     * @return jobHistory
     */
    @Override
    public JobHistory enregistrerDeclenchement(JobHistory jobHistory, String status)
    {
        log.debug(">>> Debut methode enregistrerDeclenchement");
        jobHistory.getOrdoEdition().setStatus(status);
        // Stockage de la demande dans l'historique des éditions
        jobhistorydao.saveJobHistory(jobHistory);
        return jobHistory;
    }

    /**
     * Gets the intranetbaseurl.
     * 
     * @return the intranetbaseurl
     */
    public String getIntranetbaseurl()
    {
        return intranetbaseurl;
    }

    /**
     * Initialise une édition asynchrone exécutée après un délai, non notifiée par mail.
     * 
     * @param beanIdEdition identifiant de l'édition
     * @param uidProprietaire uid du propiétaire de l'édition
     * @param delai délai (en minutes) avant l'exécution de l'édition
     * @return EditionSynchroneNonNotifieeParMail
     */
    @Override
    public EditionAsynchroneNonNotifieeParMail initEditionAsynchroneAvecDelaiNonNotifieeParMail(String beanIdEdition,
        String uidProprietaire, Integer delai)
    {
        // Affectation d'un identifiant unique à l'édition
        String editionUuid = genererUnUuid();
        ServiceEditionCommun serviceEdition = getReferenceServiceEditionCommun(beanIdEdition);

        JobHistory jobHistory =
            new JobHistory(beanIdEdition, editionUuid, delai, Boolean.FALSE, Boolean.FALSE, serviceEdition
                .getNombreJoursConservation());

        // maj du mode de soumission
        if (delai.intValue() > 0)
        {
            jobHistory.getOrdoEdition().setModeSoumission(JobHistory.MODE_SOUM_ASYNC_DIFF);
        }
        else
        {
            jobHistory.getOrdoEdition().setModeSoumission(JobHistory.MODE_SOUM_ASYNC_IMDIAT);
        }

        // maj des éléments communs à toutes les demandes
        jobHistory = this.majElementsCommunsDemande(jobHistory, uidProprietaire, serviceEdition);

        return jobHistory;

    }

    /**
     * Initialise une édition asynchrone exécutée après un délai, notifiée par mail.
     * 
     * @param beanIdEdition identifiant de l'édition
     * @param uidProprietaire uid du propiétaire de l'édition
     * @param mailDestinataire adresse mail du propriétaire de l'édition
     * @param delai délai (en minutes) avant l'exécution de l'édition
     * @return EditionSynchroneNotifieeParMail
     */
    @Override
    public EditionAsynchroneNotifieeParMail initEditionAsynchroneAvecDelaiNotifieeParMail(String beanIdEdition,
        String uidProprietaire, String mailDestinataire, Integer delai)
    {
        // Affectation d'un identifiant unique à l'édition
        String editionUuid = genererUnUuid();
        ServiceEditionCommun serviceEdition = getReferenceServiceEditionCommun(beanIdEdition);

        JobHistory jobHistory =
            new JobHistory(beanIdEdition, editionUuid, delai, Boolean.TRUE, Boolean.TRUE, serviceEdition
                .getNombreJoursConservation());

        // maj du mode de soumission
        if (delai.intValue() > 0)
        {
            jobHistory.getOrdoEdition().setModeSoumission(JobHistory.MODE_SOUM_ASYNC_DIFF);
        }
        else
        {
            jobHistory.getOrdoEdition().setModeSoumission(JobHistory.MODE_SOUM_ASYNC_IMDIAT);
        }

        // maj des éléments communs à toutes les demandes
        jobHistory = this.majElementsCommunsDemande(jobHistory, uidProprietaire, serviceEdition);

        /**
         * modif amlp
         */
        jobHistory = this.majElementsMail(mailDestinataire, jobHistory);
        // maj les éléments relatifs au mail à envoyer
        // jobHistory = this.majElementsMailPourSelonConfig(mailDestinataire, jobHistory);

        return jobHistory;

    }

    /**
     * Initialise une édition asynchrone avec exécution immédiate dès son déclenchement, non notifiée par mail.
     * 
     * @param beanIdEdition identifiant de l'édition
     * @param uidProprietaire uid du propiétaire de l'édition
     * @return EditionSynchroneNonNotifieeParMail
     */
    @Override
    public EditionAsynchroneNonNotifieeParMail initEditionAsynchroneNonNotifieeParMail(String beanIdEdition,
        String uidProprietaire)
    {
        return this.initEditionAsynchroneAvecDelaiNonNotifieeParMail(beanIdEdition, uidProprietaire, Integer
            .valueOf("0"));
    }

    /**
     * Initialise une édition asynchrone avec exécution immédiate dès son déclenchement, notifiée par mail.
     * 
     * @param beanIdEdition identifiant de l'édition
     * @param uidProprietaire uid du propiétaire de l'édition
     * @param mailDestinataire adresse mail du propriétaire de l'édition
     * @return EditionSynchroneNotifieeParMail
     */
    @Override
    public EditionAsynchroneNotifieeParMail initEditionAsynchroneNotifieeParMail(String beanIdEdition,
        String uidProprietaire, String mailDestinataire)
    {
        return this.initEditionAsynchroneAvecDelaiNotifieeParMail(beanIdEdition, uidProprietaire, mailDestinataire,
            Integer.valueOf("0"));
    }

    /**
     * Initialise la production d'une édition non stockée.
     * 
     * @param beanIdEdition identifiant de l'édition
     * @return EditionSynchroneNonNotifieeParMail
     */
    public EditionSynchroneNonNotifieeParMail initEditionNonStockee(String beanIdEdition)
    {
        ServiceEditionCommun serviceEdition = getReferenceServiceEditionCommun(beanIdEdition);

        JobHistory jobHistory =
            new JobHistory(beanIdEdition, null, Integer.valueOf("0"), Boolean.FALSE, Boolean.FALSE, serviceEdition
                .getNombreJoursConservation());

        // maj des éléments communs à toutes les demandes
        jobHistory = majElementsCommunsDemande(jobHistory, null, serviceEdition);

        return jobHistory;
    }

    /**
     * Initialise une édition synchrone non notifiée par mail.
     * 
     * @param beanIdEdition identifiant de l'édition
     * @param uidProprietaire uid du propiétaire de l'édition
     * @return EditionSynchroneNonNotifieeParMail
     */
    @Override
    public EditionSynchroneNonNotifieeParMail initEditionSynchroneNonNotifieeParMail(String beanIdEdition,
        String uidProprietaire)
    {
        // Affectation d'un identifiant unique à l'édition
        String editionUuid = genererUnUuid();

        ServiceEditionCommun serviceEdition = getReferenceServiceEditionCommun(beanIdEdition);

        JobHistory jobHistory =
            new JobHistory(beanIdEdition, editionUuid, Integer.valueOf("0"), Boolean.FALSE, Boolean.FALSE,
                serviceEdition.getNombreJoursConservation());

        // maj du mode de soumission
        jobHistory.getOrdoEdition().setModeSoumission(JobHistory.MODE_SOUM_SYNCH);

        // maj des éléments communs à toutes les demandes
        jobHistory = majElementsCommunsDemande(jobHistory, uidProprietaire, serviceEdition);

        return jobHistory;
    }

    /**
     * Initialise une édition synchrone notifiée par mail.
     * 
     * @param beanIdEdition identifiant de l'édition
     * @param uidProprietaire uid du propiétaire de l'édition
     * @param mailDestinataire adresse mail du propriétaire de l'édition
     * @return EditionSynchroneNotifieeParMail
     */
    @Override
    public EditionSynchroneNotifieeParMail initEditionSynchroneNotifieeParMail(String beanIdEdition,
        String uidProprietaire, String mailDestinataire)
    {
        // Affectation d'un identifiant unique à l'édition
        String editionUuid = genererUnUuid();

        ServiceEditionCommun serviceEdition = getReferenceServiceEditionCommun(beanIdEdition);

        JobHistory jobHistory =
            new JobHistory(beanIdEdition, editionUuid, Integer.valueOf("0"), Boolean.TRUE, Boolean.TRUE, serviceEdition
                .getNombreJoursConservation());

        // maj du mode de soumission
        jobHistory.getOrdoEdition().setModeSoumission(JobHistory.MODE_SOUM_SYNCH);

        // maj des éléments communs à toutes les demandes
        jobHistory = majElementsCommunsDemande(jobHistory, uidProprietaire, serviceEdition);
        /**
         * modif amlp
         */
        jobHistory = this.majElementsMail(mailDestinataire, jobHistory);

        /*
         * 09/01/09 modification amlp -pour rajouter des elements (mis dans paramDescription dans la description -
         * demande sepa - on le fait directement quand on envoie le mel ou si soucis cf notifierEchecEditionParMail et
         * notifierSuccesEditionParMail
         */

        // maj les éléments relatifs au mail à envoyer
        // jobHistory = this.majElementsMailPourSelonConfig(mailDestinataire, jobHistory);
        return jobHistory;
    }

    /**
     * Initialise le message mail d'accès à l'édition pour permettre un accès via le portail intranet ou le portail
     * internet Par défaut, l'accès est proposé seulement via le portail intranet.
     * 
     * @param jobHistory --
     */
    @Override
    public void initMessageMailPourAccesAvecOuSansPortailIntranet(JobHistory jobHistory)
    {
        Control.assertJobHistoryNotNull(jobHistory);
        this.majElementsMailPourAvecOuSansPortail(jobHistory.getMailEdition().getMailA(), jobHistory);
    }

    /**
     * Initialise le message mail d'accès à l'édition pour permettre un accès via le portail intrane Par défaut, l'accès
     * est proposé seulement via le portail intranet.
     * 
     * @param jobHistory --
     */
    @Override
    public void initMessageMailPourAccesViaPortailIntranet(JobHistory jobHistory)
    {
        Control.assertJobHistoryNotNull(jobHistory);
        this.majElementsMailPourPortailIntranet(jobHistory.getMailEdition().getMailA(), jobHistory);
    }

    /**
     * Initialise le message mail d'accès à l'édition pour permettre un accès via le portail intranet ou le portail
     * internet Par défaut, l'accès est proposé seulement via le portail intranet.
     * 
     * @param jobHistory --
     */
    @Override
    public void initMessageMailPourAccesViaPortailIntranetOuInternet(JobHistory jobHistory)
    {
        Control.assertJobHistoryNotNull(jobHistory);
        this.majElementsMailPourPortailIntranetOuInternet(jobHistory.getMailEdition().getMailA(), jobHistory);
    }

    /**
     * Met à jour l'objet JobHistory à partir de données associées au service associé au bean de l'édition référencée
     * dans l'objet JobHistory.
     * 
     * @param jobHistory --
     * @return the job history
     */
    public JobHistory majJobHistoryFromParametresEdition(JobHistory jobHistory)
    {
        log.debug(">>> Debut methode majJobHistoryFromParametresEdition");
        ServiceEditionCommun serviceEdition = getReferenceServiceEditionCommun(jobHistory.getBeanEditionId());

        jobHistory.getPresentationEdition().setTypeMime(
            Control.limiterLaLongeurDeLaChaine(serviceEdition.getTypeMime(), Control.MAX_VARCHAR_255));
        jobHistory.getPresentationEdition().setCharacterEncoding(
            Control.limiterLaLongeurDeLaChaine(serviceEdition.getCharacterEncoding(), Control.MAX_VARCHAR_255));
        jobHistory.getPresentationEdition().setExtension(
            Control.limiterLaLongeurDeLaChaine(serviceEdition.getExtension(), Control.MAX_VARCHAR_255));
        jobHistory.getPresentationEdition().setDescription(
            Control.limiterLaLongeurDeLaChaine(serviceEdition.getDescription(), Control.MAX_VARCHAR_255));
        jobHistory.getArchivageEdition().setNbJStock(serviceEdition.getNombreJoursConservation());

        if (jobHistory.getOrdoEdition() == null)
        {
            jobHistory.setOrdoEdition(new OrdoEdition());
        }
        jobHistory.getOrdoEdition().setStatus(JobHistory.STATUS_INITIALISE);

        jobhistorydao.saveJobHistory(jobHistory);

        return jobHistory;
    }

    /**
     * Modificateur de l attribut applibaseurl.
     * 
     * @param baseUrl le nouveau applibaseurl
     */
    public void setApplibaseurl(String baseUrl)
    {
        this.applibaseurl = baseUrl;
    }

    /**
     * Modificateur de l attribut appliorigine.
     * 
     * @param appliorigine le nouveau appliorigine
     */
    public void setAppliorigine(String appliorigine)
    {
        this.appliorigine = appliorigine;
    }

    /**
     * Modificateur de l attribut edition context.
     * 
     * @param editionContext le nouveau edition context
     */
    public void setEditionContext(ApplicationContext editionContext)
    {
        this.editionContext = editionContext;
    }

    /**
     * Sets the editionstorageserviceso.
     * 
     * @param editionstorageserviceso the editionstorageserviceso to set
     */
    public void setEditionstorageserviceso(EditionStorageService editionstorageserviceso)
    {
        this.editionstorageserviceso = editionstorageserviceso;
    }

    /**
     * Modificateur de l attribut flux url.
     * 
     * @param fluxUrl le nouveau flux url
     */
    public void setFluxUrl(String fluxUrl)
    {
        this.fluxUrl = fluxUrl;
    }

    /**
     * Modificateur de l attribut internetbaseurl.
     * 
     * @param internetbaseurl le nouveau internetbaseurl
     */
    public void setInternetbaseurl(String internetbaseurl)
    {
        this.internetbaseurl = internetbaseurl;
    }

    /**
     * Sets the intranetbaseurl.
     * 
     * @param intranetbaseurl the intranetbaseurl to set
     */
    public void setIntranetbaseurl(String intranetbaseurl)
    {
        this.intranetbaseurl = intranetbaseurl;
    }

    /**
     * Modificateur de l attribut jobhistorydao.
     * 
     * @param jobhistorydao le nouveau jobhistorydao
     */
    public void setJobhistorydao(JobHistoryDao jobhistorydao)
    {
        this.jobhistorydao = jobhistorydao;
    }

    /**
     * Modificateur de l attribut mail de.
     * 
     * @param mailDe le nouveau mail de
     */
    public void setMailDe(String mailDe)
    {
        this.mailDe = mailDe;
    }

    /**
     * Sets the modeauthentification.
     * 
     * @param modeauthentification the modeauthentification to set
     */
    public void setModeauthentification(String modeauthentification)
    {
        this.modeauthentification = modeauthentification;
    }

    /**
     * Modificateur de l attribut serveurmelserviceso.
     * 
     * @param serveurmelserviceso le nouveau serveurmelserviceso
     */
    public void setServeurmelserviceso(MelService serveurmelserviceso)
    {
        this.serveurmelserviceso = serveurmelserviceso;
    }

    /**
     * Modificateur de l attribut taille max piece jointe.
     * 
     * @param tailleMaxPieceJointe le nouveau taille max piece jointe
     */
    public void setTailleMaxPieceJointe(long tailleMaxPieceJointe)
    {
        this.tailleMaxPieceJointe = tailleMaxPieceJointe;
    }

    /**
     * Modificateur de l attribut transaction template.
     * 
     * @param transactionTemplate le nouveau transaction template
     */
    public void setTransactionTemplate(TransactionTemplate transactionTemplate)
    {
        this.transactionTemplate = transactionTemplate;
    }

    /**
     * methode Affecte message erreur to job history : --.
     * 
     * @param jobHistory --
     * @param exception --
     */
    private void affecteMessageErreurToJobHistory(JobHistory jobHistory, Exception exception)
    {
        StringBuilder   msgErr = new StringBuilder();

        if (exception != null)
        {
            msgErr.append(exception.getMessage());
            msgErr.append('\n');

            ByteArrayOutputStream out = new ByteArrayOutputStream();
            PrintWriter pwriter = new PrintWriter(out);

            //exception.printStackTrace(pwriter); modif rapide pour ne pas utiliser la console brute mais un logger
            // à voir si il n'y a pas de regressions sur l'analyse des logs au delà du dev.
            log.error(exception.getMessage(),exception);
            pwriter.flush();

            try
            {
                msgErr.append(out.toString("UTF8"));
            }
            catch (UnsupportedEncodingException exce)
            {
                log.warn("ERREUR LORS DE LA LECTURE DU STACKTRACE", exce);
                throw new ProgrammationException(exce);
            }

        }

        String msg = Control.limiterLaLongeurDeLaChaine(msgErr.toString(), Control.MAX_VARCHAR_4000);
        jobHistory.getOrdoEdition().setMessage(msg);
        jobHistory.getOrdoEdition().setStatus(JobHistory.STATUS_ECHEC);
        this.notifierEchecEditionParMail(jobHistory);
    }

    /**
     * Affecte la description d'une édition en utilisant éventuellement les paramètres de l'édition passés en
     * paramètres.
     * 
     * @param jobHistory --
     * @param parametresEdition --
     */
    private void affecterDescriptionToJobHistory(JobHistory jobHistory, Map parametresEdition)
    {
        ServiceEditionCommun serviceEdition = this.getReferenceServiceEditionCommun(jobHistory.getBeanEditionId());

        String description =
            Control.limiterLaLongeurDeLaChaine(serviceEdition.getDescription(parametresEdition),
                Control.MAX_VARCHAR_255);

        jobHistory.getPresentationEdition().setDescription(description);
    }

    /**
     * Attacher une pièce jointe à un objet Mel.
     * 
     * @param jobHistory --
     * @param mel --
     * @return mel
     */
    private Mel attacherPieceJointe(JobHistory jobHistory, Mel mel)
    {
        if (jobHistory.getMailEdition().isPieceAJoindre())
        {
            FichierJoint fichierJoint = editionstorageserviceso.chargerFichierEdition(jobHistory);

            // Les éditions stockées dans des fichiers temporaires ne peuvent pas
            // être envoyées par mail
            if (fichierJoint.getTailleFichier() < this.tailleMaxPieceJointe
                && fichierJoint.getLeContenuDuFichier().getData() != null)
            {
                ByteArrayResource byArray = new ByteArrayResource(fichierJoint.getLeContenuDuFichier().getData());
                PieceJointe pieceJointe =
                    new PieceJointe(jobHistory.getPresentationEdition().getNomFicDownload(), byArray);
                mel.ajouterPieceJointe(pieceJointe);
            }
        }

        return mel;
    }

    /**
     * Calcule la date à laquelle l'édition sera supprimée.
     * 
     * @param nombreJoursConservation --
     * @return date de suppression de l'édition
     */
    private Date calculDatePurge(Integer nombreJoursConservation)
    {
        Calendar cal = Calendar.getInstance(Locale.FRANCE);

        cal.setTime(instantCourant());
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);

        cal.add(Calendar.DATE, nombreJoursConservation.intValue());
        return new Timestamp(cal.getTimeInMillis());
    }

    /**
     * Construit le message mail transmis à l'utilisateur lors d'un accès avec ou sans portail intranet.
     * 
     * @param jobHistory --
     * @return the string
     */
    private String construitMessageMailAvecOuSansPortail(JobHistory jobHistory)
    {
        StringBuilder   msg = new StringBuilder(200);
        msg.append("Le document \"");
        msg.append(jobHistory.getPresentationEdition().getDescription());
        msg.append("\" est disponible à l'adresse suivante : \n\n");
        msg.append(jobHistory.getPresentationEdition().getUrlPIntranet());
        msg.append("\n\n\n\nSi vous vous connectez sans passer par le portail intranet,");
        msg.append(" veuillez utiliser l'adresse ci-dessous.\n\n");
        msg.append(jobHistory.getPresentationEdition().getUrlEdition());
        msg.append("\n\n");
        msg.append(this.contruitInfosGeneralesEditionMessageMail(jobHistory));
        return msg.toString();
    }

    /**
     * Construit le message mail transmis à l'utilisateur lors d'un accès par le portail intranet.
     * 
     * @param jobHistory --
     * @return the string
     */
    private String construitMessageMailPortailIntranet(JobHistory jobHistory)
    {
        StringBuilder   msg = new StringBuilder(200);
        msg.append("Le document \"");
        msg.append(jobHistory.getPresentationEdition().getDescription());
        msg.append("\" est disponible à l'adresse suivante : \n\n");
        msg.append(jobHistory.getPresentationEdition().getUrlPIntranet());
        msg.append("\n\n");
        msg.append(this.contruitInfosGeneralesEditionMessageMail(jobHistory));
        return msg.toString();
    }

    /**
     * Construit le message mail transmis à l'utilisateur lors d'un accès par le portail intranet ou intranet.
     * 
     * @param jobHistory --
     * @return the string
     */
    private String construitMessageMailPortailIntranetOuInternet(JobHistory jobHistory)
    {
        StringBuilder   msg = new StringBuilder(200);
        msg.append("Le document \"");
        msg.append(jobHistory.getPresentationEdition().getDescription());
        msg.append("\" est disponible à l'adresse suivante : ");
        msg.append("\n\n");
        msg.append(jobHistory.getPresentationEdition().getUrlPIntranet());
        msg.append("\n\n\n\nSi vous vous connectez par l'intermédiaire du portail internet,");
        msg.append(" veuillez utiliser l'adresse ci-dessous.");
        msg.append("\n\n");
        msg.append(jobHistory.getPresentationEdition().getUrlPInternet());
        msg.append("\n\n");
        msg.append(this.contruitInfosGeneralesEditionMessageMail(jobHistory));
        return msg.toString();
    }

    /**
     * Construit le message mail transmis à l'utilisateur lors d'un accès sans portail.
     * 
     * @param jobHistory --
     * @return the string
     */
    private String construitMessageMailSansPortail(JobHistory jobHistory)
    {
        StringBuilder   msg = new StringBuilder(200);
        msg.append("Le document \"");
        msg.append(jobHistory.getPresentationEdition().getDescription());
        msg.append("\" est disponible à l'adresse suivante : \n\n");
        msg.append(jobHistory.getPresentationEdition().getUrlEdition());
        msg.append("\n\n");
        msg.append(this.contruitInfosGeneralesEditionMessageMail(jobHistory));
        return msg.toString();
    }

    /**
     * Contruit l'URL qui sera transmise par mail à l'utilisateur pour accéder à l'édition qu'il vient de produire.
     * 
     * @param jobHistory --
     * @param urlb --
     * @return String
     */
    private String construitUrlAccesAUneEdition(JobHistory jobHistory, String urlb)
    {
        StringBuilder   url = new StringBuilder(40);
        String editionUuid = "";
        StringBuilder   uid = new StringBuilder();

        if (jobHistory != null)
        {
            editionUuid = jobHistory.getEditionUuid();
            uid = uid.append(jobHistory.getDestinationEdition().getUidProprietaire());
        }

        url.append(urlb);
        url.append('/');
        url.append(this.fluxUrl);
        url.append('&');
        url.append(JobHistory.ATTR_EDITION_UUID);
        url.append('=');
        url.append(editionUuid);
        url.append('&');
        url.append(JobHistory.ATTR_USER_UID);
        url.append('=');
        url.append(uid);

        return url.toString();
    }

    /**
     * Contruit l'URL qui sera transmise par mail à l'utilisateur pour accéder à l'édition sans passer par un portail.
     * 
     * @param jobHistory --
     * @return String
     */
    private String construitUrlAccessAUneEditionSansPortail(JobHistory jobHistory)
    {
        return construitUrlAccesAUneEdition(jobHistory, this.applibaseurl);
    }

    /**
     * Contruit l'URL qui sera transmise par mail à l'utilisateur pour accéder à l'édition via le portail internet.
     * 
     * @param jobHistory --
     * @return String
     */
    private String construitUrlAccessAUneEditionViaPortailInternet(JobHistory jobHistory)
    {
        return construitUrlAccesAUneEdition(jobHistory, this.internetbaseurl);
    }

    /**
     * Contruit l'URL qui sera transmise par mail à l'utilisateur pour accéder à l'édition via le portail intranet.
     * 
     * @param jobHistory --
     * @return String
     */
    private String construitUrlAccessAUneEditionViaPortailIntranet(JobHistory jobHistory)
    {
        String urlPIntranet = UtilisateurSecurityLombokContainer.getUrl();

        return construitUrlAccesAUneEdition(jobHistory, urlPIntranet);
    }

    /**
     * Construit un message contenant des informations générales sur une édition.
     * 
     * @param jobHistory --
     * @return the string
     */
    private String contruitInfosGeneralesEditionMessageMail(JobHistory jobHistory)
    {
        StringBuilder   mailMessage = new StringBuilder(100);
        mailMessage.append("\nApplication à l'origine de l'édition : ");
        mailMessage.append(jobHistory.getDestinationEdition().getApplicationOrigine());
        mailMessage.append('\n');
        if (jobHistory.getDestinationEdition().getUidProprietaire() != null
            && StringUtils.isNotBlank(jobHistory.getDestinationEdition().getUidProprietaire()))
        {
            mailMessage.append("Utilisateur ayant demandé la production de l'édition : ");
            mailMessage.append(jobHistory.getDestinationEdition().getUidProprietaire());
            mailMessage.append('\n');
        }
        mailMessage.append("Date de demande de l'édition : ");
        mailMessage.append(FormaterDate.formatDateHeureMinuteCourt(jobHistory.getOrdoEdition().getDateDemande()));
        mailMessage.append('\n');
        mailMessage.append("Date de suppression de l'édition : ");
        mailMessage.append(FormaterDate.formatDateCourt(jobHistory.getArchivageEdition().getDatePurge()));
        return mailMessage.toString();
    }

    /**
     * Crée et stocke une édition.
     * 
     * @param jobHistory --
     * @param parametresEdition --
     * @param compression --
     */
    private void creerEtStockerEdition(JobHistory jobHistory, Map parametresEdition, Boolean compression)
    {
        log.debug(">>> Debut methode creerEtStockerEdition");
        try
        {
            jobHistory.getOrdoEdition().setStartTime(new Timestamp((new Date()).getTime()));

            this.affecterDescriptionToJobHistory(jobHistory, parametresEdition);

            FichierJoint fichierJoint = this.creerEdition(jobHistory.getBeanEditionId(), parametresEdition);

            if (jobHistory.getPresentationEdition().getNomFicDownload() == null
                || StringUtils.isBlank(jobHistory.getPresentationEdition().getNomFicDownload()))
            {
                jobHistory.getPresentationEdition().setNomFicDownload(fichierJoint.getNomFichierOriginal());
            }

            if (compression != null && compression.booleanValue() == true)
            {
                FichierJoint fichierJointCompresse = this.compresserEdition(parametresEdition, fichierJoint);

                StringBuilder   newNomFicDownload = new StringBuilder();
                String nomFicDownload = jobHistory.getPresentationEdition().getNomFicDownload();

                if (nomFicDownload != null && StringUtils.isNotBlank(nomFicDownload))
                {
                    newNomFicDownload.append(nomFicDownload);
                    newNomFicDownload.append(".zip");
                }
                else
                {
                    newNomFicDownload.append("edition.zip");
                }

                jobHistory.getPresentationEdition().setNomFicDownload(newNomFicDownload.toString());
                jobHistory.getPresentationEdition().setTypeMime("application/zip");
                jobHistory.getPresentationEdition().setExtension("zip");

                editionstorageserviceso.stockerEdition(jobHistory, fichierJointCompresse);
            }
            else
            {
                editionstorageserviceso.stockerEdition(jobHistory, fichierJoint);
            }

            jobHistory.getOrdoEdition().setEndTime(new Timestamp((new Date()).getTime()));
            jobHistory.getOrdoEdition().setStatus(JobHistory.STATUS_DISPONIBLE);

            jobHistory = this.notifierSuccesEditionParMail(jobHistory);

        }
        catch (ApplicationException exception)
        {
            // Comme l'édition n'a pas été produite, on supprime le lien
            // vers le contenu de l'édition - sinon, on ne peut pas
            // enregistrer l'erreur produite
            jobHistory.getStockageEdition().setLeContenuEdition(null);
            this.affecteMessageErreurToJobHistory(jobHistory, exception);
        }
        catch (RuntimeException exception)
        {
            // Comme l'édition n'a pas été produite, on supprime le lien
            // vers le contenu de l'édition - sinon, on ne peut pas
            // enregistrer l'erreur produite
            jobHistory.getStockageEdition().setLeContenuEdition(null);
            log.warn("EXCEPTION NON TRAITEE : ", exception);
            this.affecteMessageErreurToJobHistory(jobHistory, exception);
        }
        finally
        {
            jobhistorydao.saveJobHistory(jobHistory);
        }
    }

    /**
     * Lecture dans le contexte d'application de la référence identifiée l'id du bean.
     * 
     * @param editionBeanId --
     * @return the reference bean id
     */
    private Object getReferenceBeanId(String editionBeanId)
    {
        Object editionBeanObject = null;
        ApplicationContext appliContext = this.getApplicationContext();

        if (appliContext == null)
        {
            throw new ProgrammationException("LE CONTEXTE DE L'APPLICATION EST NULL");
        }
        else
        {
            if (appliContext.containsBean(editionBeanId))
            {

                editionBeanObject = appliContext.getBean(editionBeanId);
            }
            else
            {
                throw new ProgrammationException("LE BEAN " + editionBeanId
                    + " N'EST PAS DEFINI DANS LE CONTEXTE D'EDITION.");
            }
        }
        return editionBeanObject;
    }

    /**
     * Lit dans le contexte d'édition la référence du bean editionBeanId et la retourne dans le type
     * ServiceEditionCommun.
     * 
     * @param editionBeanId --
     * @return ServiceEditionCommun
     */
    private ServiceEditionCommun getReferenceServiceEditionCommun(String editionBeanId)
    {
        ServiceEditionCommun serviceEdition = null;

        Object object = this.getReferenceBeanId(editionBeanId);

        if (object instanceof ServiceEditionCommun)
        {
            serviceEdition = (ServiceEditionCommun) object;
        }
        else
        {
            throw new ProgrammationException("LE BEAN " + editionBeanId + " N'EST PAS DE TYPE SERVICEEDITIONCOMMUN");
        }
        return serviceEdition;
    }

    /**
     * Lit dans le contexte d'édition la référence du bean editionBeanId et la retourne dans le type
     * ServiceEditionEcCommun.
     * 
     * @param editionBeanId --
     * @return ServiceEditionCommun
     */
    private ServiceEditionJasperCommun getReferenceServiceEditionJasperCommun(String editionBeanId)
    {
        ServiceEditionJasperCommun serviceEdition = null;

        Object object = this.getReferenceBeanId(editionBeanId);

        if (object instanceof ServiceEditionJasperCommun)
        {
            serviceEdition = (ServiceEditionJasperCommun) object;
        }
        else
        {
            throw new ProgrammationException("LE BEAN " + editionBeanId + " N'EST PAS DE TYPE SERVICEEDITIONECCOMMUN");
        }
        return serviceEdition;
    }

    /**
     * Retourne le moment courant.
     * 
     * @return the timestamp
     */
    private Timestamp instantCourant()
    {
        return new Timestamp((new Date()).getTime());
    }

    /**
     * Met à jour les éléments communs à toutes les demandes.
     * 
     * @param jobHistory --
     * @param uidProprietaire --
     * @param serviceEdition --
     * @return the job history
     */
    private JobHistory majElementsCommunsDemande(JobHistory jobHistory, String uidProprietaire,
        ServiceEditionCommun serviceEdition)
    {
        jobHistory.getDestinationEdition().setUidProprietaire(
            Control.limiterLaLongeurDeLaChaine(uidProprietaire, Control.MAX_VARCHAR_255));

        jobHistory.getPresentationEdition().setTypeMime(
            Control.limiterLaLongeurDeLaChaine(serviceEdition.getTypeMime(), Control.MAX_VARCHAR_255));

        jobHistory.getPresentationEdition().setCharacterEncoding(
            Control.limiterLaLongeurDeLaChaine(serviceEdition.getCharacterEncoding(), Control.MAX_VARCHAR_255));
        jobHistory.getPresentationEdition().setExtension(
            Control.limiterLaLongeurDeLaChaine(serviceEdition.getExtension(), Control.MAX_VARCHAR_255));

        jobHistory.getPresentationEdition().setDescription(
            Control.limiterLaLongeurDeLaChaine(serviceEdition.getDescription(), Control.MAX_VARCHAR_255));

        // Enregistrement de la date de la demande
        jobHistory.getOrdoEdition().setDateDemande(this.instantCourant());
        jobHistory.getDestinationEdition().setApplicationOrigine(this.appliorigine);

        // Affectation de la date de purge
        jobHistory.getArchivageEdition().setDatePurge(
            this.calculDatePurge(jobHistory.getArchivageEdition().getNbJStock()));

        // Calcul de l'URL permettant de récupérer l'édition
        jobHistory.getPresentationEdition().setUrlEdition(
            Control.limiterLaLongeurDeLaChaine(this.construitUrlAccessAUneEditionSansPortail(jobHistory),
                Control.MAX_VARCHAR_2000));

        // Calcul de l'URL permettant de récupérer l'édition via le portail intranet
        jobHistory.getPresentationEdition().setUrlPIntranet(
            Control.limiterLaLongeurDeLaChaine(this.construitUrlAccessAUneEditionViaPortailIntranet(jobHistory),
                Control.MAX_VARCHAR_2000));

        // Calcul de l'URL permettant de récupérer l'édition via le portail internet
        jobHistory.getPresentationEdition().setUrlPInternet(
            Control.limiterLaLongeurDeLaChaine(this.construitUrlAccessAUneEditionViaPortailInternet(jobHistory),
                Control.MAX_VARCHAR_2000));

        return jobHistory;
    }

    /**
     * Met à jour les éléments suivants du mail à envoyer : - destinataire - expéditeur - objet.
     * 
     * @param mailDestinataire --
     * @param jobHistory --
     * @return jobHistory
     */
    private JobHistory majElementsMail(String mailDestinataire, JobHistory jobHistory)
    {
        Control.assertJobHistoryNotNull(jobHistory);

        if (jobHistory.getMailEdition() == null)
        {
            jobHistory.setMailEdition(new MailEdition(Boolean.TRUE, Boolean.FALSE));
        }

        jobHistory.getMailEdition().setMailA(mailDestinataire);
        jobHistory.getMailEdition().setMailDe(this.mailDe);
        /**
         * 09/01/09 amlp modif pour tenir compte de l'eventuelle modification de la description (demande psax sepa)
         * remplacer par majObjetMail qui sera appellée dans notifierEchecEditionParMail et notifierSuccesEditionParMail
         */

        /*
         * StringBuilder   objetMail = new StringBuilder(); objetMail.append("Edition demandée : ");
         * objetMail.append(jobHistory.getPresentationEdition().getDescription());
         * jobHistory.getMailEdition().setMailObjet(objetMail.toString());
         */

        return jobHistory;
    }

    /**
     * Met à jour le message du mail à envoyer, pour un accès à l'édition avec ou sans portail.
     * 
     * @param mailDestinataire --
     * @param jobHistory --
     * @return the job history
     */
    private JobHistory majElementsMailPourAvecOuSansPortail(String mailDestinataire, JobHistory jobHistory)
    {
        this.majElementsMail(mailDestinataire, jobHistory);

        // Construit le message du mail
        jobHistory.getMailEdition().setMailMessage(
            Control.limiterLaLongeurDeLaChaine(this.construitMessageMailAvecOuSansPortail(jobHistory),
                Control.MAX_VARCHAR_4000));

        return jobHistory;
    }

    /**
     * Met à jour le message du mail à envoyer, pour un accès à l'édition via le portail intranet.
     * 
     * @param mailDestinataire --
     * @param jobHistory --
     * @return the job history
     */
    private JobHistory majElementsMailPourPortailIntranet(String mailDestinataire, JobHistory jobHistory)
    {
        this.majElementsMail(mailDestinataire, jobHistory);

        // Construit le message du mail
        jobHistory.getMailEdition().setMailMessage(
            Control.limiterLaLongeurDeLaChaine(this.construitMessageMailPortailIntranet(jobHistory),
                Control.MAX_VARCHAR_4000));

        return jobHistory;
    }

    /**
     * Met à jour le message du mail à envoyer, pour un accès à l'édition via le portail intranet ou le portail
     * internet.
     * 
     * @param mailDestinataire --
     * @param jobHistory --
     * @return the job history
     */
    private JobHistory majElementsMailPourPortailIntranetOuInternet(String mailDestinataire, JobHistory jobHistory)
    {
        this.majElementsMail(mailDestinataire, jobHistory);

        // Construit le message du mail
        jobHistory.getMailEdition().setMailMessage(
            Control.limiterLaLongeurDeLaChaine(this.construitMessageMailPortailIntranetOuInternet(jobHistory),
                Control.MAX_VARCHAR_4000));

        return jobHistory;
    }

    /**
     * Met à jour le message du mail à envoyer selon le mode d'authentificatiton avec lequel l'application est démarrée.
     * 
     * @param mailDestinataire --
     * @param jobHistory --
     * @return the job history
     */
    private JobHistory majElementsMailPourSelonConfig(String mailDestinataire, JobHistory jobHistory)
    {
        this.majElementsMail(mailDestinataire, jobHistory);

        // Application exécutée en mode appli
        if ("appli".equalsIgnoreCase(this.modeauthentification))
        {
            // Construit le message du mail
            jobHistory.getMailEdition().setMailMessage(
                Control.limiterLaLongeurDeLaChaine(this.construitMessageMailSansPortail(jobHistory),
                    Control.MAX_VARCHAR_4000));
        }
        // Application exécutée en mode portail
        else if ("appliportail".equalsIgnoreCase(this.modeauthentification))
        {
            // Construit le message du mail
            jobHistory.getMailEdition().setMailMessage(
                Control.limiterLaLongeurDeLaChaine(this.construitMessageMailAvecOuSansPortail(jobHistory),
                    Control.MAX_VARCHAR_4000));
        }
        // Application exécutée en mode portail
        else
        {
            // Construit le message du mail
            jobHistory.getMailEdition().setMailMessage(
                Control.limiterLaLongeurDeLaChaine(this.construitMessageMailPortailIntranet(jobHistory),
                    Control.MAX_VARCHAR_4000));
        }

        return jobHistory;
    }

    /**
     * Met à jour les éléments suivants du mail à envoyer : - objet.
     * 
     * @param jobHistory --
     * @return jobHistory
     */
    private JobHistory majObjetMail(JobHistory jobHistory)
    {
        Control.assertJobHistoryNotNull(jobHistory);

        StringBuilder   objetMail = new StringBuilder();
        objetMail.append("Edition demandée : ");
        objetMail.append(jobHistory.getPresentationEdition().getDescription());
        jobHistory.getMailEdition().setMailObjet(objetMail.toString());

        return jobHistory;
    }

    /**
     * Envoyer un mail indiquant que l'édition est en echec.
     * 
     * @param jobHistory --
     */
    private void notifierEchecEditionParMail(JobHistory jobHistory)
    {
        if (jobHistory.getMailEdition().isNotifieeParMail())
        {
            try
            {
                /*
                 * 09/01/09 modification amlp -pour rajouter des elements (mis dans paramDescription dans la description
                 * - demande sepa - on le fait ici car avant le job history n' a pas été mis à jour dans
                 * presendationedition.description
                 */

                this.majObjetMail(jobHistory);
                this.majElementsMailPourSelonConfig(jobHistory.getMailEdition().getMailA(), jobHistory);

                Mel mel = new Mel();
                mel.ajouterDestinataireA(jobHistory.getMailEdition().getMailA());
                mel.setDe(jobHistory.getMailEdition().getMailDe());
                mel.setObjet(jobHistory.getMailEdition().getMailObjet());
                mel.setMesssage(jobHistory.getOrdoEdition().getMessage());
                serveurmelserviceso.envoyerMel(mel);
            }
            catch (ApplicationException exception)
            {
                log.debug("Exception détectée", exception);

                StringBuilder   msg = new StringBuilder();
                msg.append("UNE ERREUR EST SURVENUE LORS DE LA PRODUCTION D'UNE EDITION");
                msg.append('\n');
                msg.append("PAR L'APPLICATION ");
                msg.append(jobHistory.getDestinationEdition().getApplicationOrigine());
                msg.append("\n\n");
                msg.append("IDENTIFIANT DE L'EDITION EN ERREUR : ");
                msg.append(jobHistory.getEditionUuid());
                msg.append('\n');
                msg.append("DATE DE DEMANDE DE L'EDITION :");
                msg.append(FormaterDate.formatDateHeureMinuteCourt((jobHistory.getJourDemande())));
                msg.append('\n');
                msg.append(exception.getMessage());
                msg.append(jobHistory.getOrdoEdition().getMessage());

                String msgTrunc = Control.limiterLaLongeurDeLaChaine(msg.toString(), Control.MAX_VARCHAR_4000);
                jobHistory.getOrdoEdition().setMessage(msgTrunc);
            }
        }
    }

    /**
     * Envoyer une mail indiquant que l'édition a abouti correctement.
     * 
     * @param jobHistory --
     * @return the job history
     */
    private JobHistory notifierSuccesEditionParMail(JobHistory jobHistory)
    {
        if (jobHistory.getMailEdition().isNotifieeParMail())
        {
            try
            {
                /*
                 * 09/01/09 modification amlp -pour rajouter des elements (mis dans paramDescription dans la description
                 * - demande sepa - on le fait ici car avant le job history n' a pas été mis à jour dans
                 * presendationedition.description
                 */

                this.majObjetMail(jobHistory);
                this.majElementsMailPourSelonConfig(jobHistory.getMailEdition().getMailA(), jobHistory);

                /* fin modification amlp */

                Mel mel = new Mel();
                mel.ajouterDestinataireA(jobHistory.getMailEdition().getMailA());
                mel.setDe(jobHistory.getMailEdition().getMailDe());
                mel.setObjet(jobHistory.getMailEdition().getMailObjet());
                mel.setMesssage(jobHistory.getMailEdition().getMailMessage());

                mel = attacherPieceJointe(jobHistory, mel);
                serveurmelserviceso.envoyerMel(mel);
            }
            catch (ApplicationException exception)
            {
                log.debug("Exception détectée", exception);

                jobHistory.getOrdoEdition().setMessage(
                    Control.limiterLaLongeurDeLaChaine(exception.getMessage(), Control.MAX_VARCHAR_4000));
                jobHistory.getOrdoEdition().setStatus(JobHistory.STATUS_DISPONIBLE_NON_ENVOYE);
            }
        }

        return jobHistory;
    }

    /**
     * Teste si le contenu d'un FichierJoint est acessible.
     * 
     * @param fichierJoint --
     * @return true, if test contenu fichier joint
     */
    private boolean testContenuFichierJoint(FichierJoint fichierJoint)
    {
        boolean result = true;

        if (fichierJoint == null)
        {
            throw new ProgrammationException("LE FICHIERJOINT NE DOIT PAS ETRE NULL");
        }
        else if (fichierJoint.getLeContenuDuFichier() == null)
        {
            throw new ProgrammationException("LE CONTENU DU FICHIER NE DOIT PAS ETRE NULL");
        }
        else
        {
            if (fichierJoint.isDataInByteArray())
            {
                if (fichierJoint.getLeContenuDuFichier().getData() == null)
                {
                    throw new ProgrammationException("LE CONTENU DU FICHIER CONTIENT UN TABLEAU DE BYTES NULL");
                }
            }
            else
            {
                File fichierTemporaire = fichierJoint.getLeContenuDuFichier().getFichierTemporaire();
                if (fichierTemporaire == null)
                {
                    throw new ProgrammationException("LE CONTENU DU FICHIER CONTIENT UN FICHIER TEMPORAIRE NULL");
                }
                else
                {
                    if (!fichierTemporaire.exists())
                    {
                        throw new ProgrammationException("LE FICHIER " + fichierTemporaire.getAbsolutePath()
                            + " N'EXISTE PAS.");
                    }

                    if (!fichierTemporaire.canRead())
                    {
                        throw new ProgrammationException("LE FICHIER " + fichierTemporaire.getAbsolutePath()
                            + " N'EST PAS ACCESSIBLE EN LECTUER.");
                    }
                }
            }
        }
        return result;
    }


    /**
     * Méthode permettant de générer un uuid
     *
     * @return le uuid généré
     */
    private String genererUnUuid()
    {
        String uuidstring = UUID.randomUUID().toString();
        if (log.isDebugEnabled())
        {
            log.debug("nouveau uuid généré par composant édition:" + uuidstring);
        }
        return uuidstring;
    }
    
}
