/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : EditionDemandeService.java
 *
 */
package fr.gouv.finances.lombok.edition.service;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import fr.gouv.finances.lombok.edition.bean.EditionAsynchroneNonNotifieeParMail;
import fr.gouv.finances.lombok.edition.bean.EditionAsynchroneNotifieeParMail;
import fr.gouv.finances.lombok.edition.bean.EditionSynchroneNonNotifieeParMail;
import fr.gouv.finances.lombok.edition.bean.EditionSynchroneNotifieeParMail;
import fr.gouv.finances.lombok.edition.bean.JobHistory;
import fr.gouv.finances.lombok.securite.techbean.PersonneAnnuaire;
import fr.gouv.finances.lombok.upload.bean.FichierJoint;

/**
 * Interface EditionDemandeService --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.2 $ Date: 11 déc. 2009
 */
public interface EditionDemandeService
{

    /**
     * Déclenche une édition asynchrone non notifiée par mail.
     * 
     * @param editAsynchNonNotifieeParMail --
     * @param parametresEdition --
     * @return the edition asynchrone non notifiee par mail
     */
    public EditionAsynchroneNonNotifieeParMail declencherEdition(
        EditionAsynchroneNonNotifieeParMail editAsynchNonNotifieeParMail, Map parametresEdition);

    /**
     * Déclenche une édition asynchrone notifiée par mail.
     * 
     * @param editAsynchNotifieeParMail --
     * @param parametresEdition --
     * @return the edition asynchrone notifiee par mail
     */
    public EditionAsynchroneNotifieeParMail declencherEdition(
        EditionAsynchroneNotifieeParMail editAsynchNotifieeParMail, Map parametresEdition);

    /**
     * Déclenche une édition synchrone non notifiée par mail.
     * 
     * @param editSynchNonNotifieeParMail --
     * @param parametresEdition --
     * @return the edition synchrone non notifiee par mail
     */
    public EditionSynchroneNonNotifieeParMail declencherEdition(
        EditionSynchroneNonNotifieeParMail editSynchNonNotifieeParMail, Map parametresEdition);

    /**
     * Déclenche une édition synchrone notifiée par mail.
     * 
     * @param editSynchNotifieeParMail --
     * @param parametresEdition --
     * @return the edition synchrone notifiee par mail
     */
    public EditionSynchroneNotifieeParMail declencherEdition(EditionSynchroneNotifieeParMail editSynchNotifieeParMail,
        Map parametresEdition);

    /**
     * Déclenche une édition asynchrone compressee non notifiée par mail.
     * 
     * @param editAsynchNonNotifieeParMail --
     * @param parametresEdition --
     * @return the edition asynchrone non notifiee par mail
     */
    public EditionAsynchroneNonNotifieeParMail declencherEditionCompressee(
        EditionAsynchroneNonNotifieeParMail editAsynchNonNotifieeParMail, Map parametresEdition);

    /**
     * Déclenche une édition asynchrone compressee notifiée par mail.
     * 
     * @param editAsynchNotifieeParMail --
     * @param parametresEdition --
     * @return the edition asynchrone notifiee par mail
     */
    public EditionAsynchroneNotifieeParMail declencherEditionCompressee(
        EditionAsynchroneNotifieeParMail editAsynchNotifieeParMail, Map parametresEdition);

    /**
     * Déclenche une édition synchrone compressee non notifiée par mail.
     * 
     * @param editSynchNonNotifieeParMail --
     * @param parametresEdition --
     * @return the edition synchrone non notifiee par mail
     */
    public EditionSynchroneNonNotifieeParMail declencherEditionCompressee(
        EditionSynchroneNonNotifieeParMail editSynchNonNotifieeParMail, Map parametresEdition);

    /**
     * Déclenche une édition synchrone compressee notifiée par mail.
     * 
     * @param editSynchNotifieeParMail --
     * @param parametresEdition --
     * @return the edition synchrone notifiee par mail
     */
    public EditionSynchroneNotifieeParMail declencherEditionCompressee(
        EditionSynchroneNotifieeParMail editSynchNotifieeParMail, Map parametresEdition);

    /**
     * Déclenche la production immédiate d'une édition et la compresse, sans stockage dans le repository des éditions.
     * 
     * @param editionBeanId --
     * @param parametresEdition --
     * @return FichierJoint représentant l'édition
     */
    public FichierJoint declencherProductionEditionCompresseeNonStockee(String editionBeanId, Map parametresEdition);

    /**
     * Déclenche la production immédiate de l'édition, sans stockage dans le repository des éditions.
     * 
     * @param editionBeanId --
     * @param parametresEdition --
     * @return FichierJoint représentant l'édition
     */
    public FichierJoint declencherProductionEditionNonStockee(String editionBeanId, Map parametresEdition);

    /**
     * Déclenche la production immédiate de l'édition html, sans stockage dans le repository des éditions.
     * 
     * @param editionBeanId --
     * @param parametresEdition --
     * @return Map contenant : - le fichier de l'édition sous la clé CpJasperReportsHtmlView.FICHIER_JOINT_KEY - un
     *         objet jasperprint sous la clé CpJasperReportsHtmlView.JASPER_PRINT_KEY
     */
    public Map declencherProductionHtmlEditionNonStockee(String editionBeanId, Map parametresEdition);

    /**
     * Initialise une edition asynchrone dont la réalisation ne sera pas notifiée par mail.
     * 
     * @param beanIdEdition id du bean de l'édition
     * @param uidProprietaire uid du propriétaire de l'édition
     * @return EditionAsynchroneNonNotifieeParMail
     */
    public EditionAsynchroneNonNotifieeParMail initEditionAsynchroneNonNotifieeParMail(String beanIdEdition,
        String uidProprietaire);

    /**
     * Initialise une edition asynchrone dont la réalisation sera notifiée par mail.
     * 
     * @param beanIdEdition id du bean de l'édition
     * @param uidProprietaire uid du propriétaire de l'édition
     * @param mailDestinataire --
     * @return EditionAsynchroneNotifieeParMail
     */
    public EditionAsynchroneNotifieeParMail initEditionAsynchroneNotifieeParMail(String beanIdEdition,
        String uidProprietaire, String mailDestinataire);

    /**
     * Initialise une edition asynchrone dont la réalisation ne sera pas notifiée par mail.
     * 
     * @param beanIdEdition id du bean de l'édition
     * @param uidProprietaire uid du propriétaire de l'édition
     * @return EditionAsynchroneNonNotifieeParMail
     */
    public EditionSynchroneNonNotifieeParMail initEditionSynchroneNonNotifieeParMail(String beanIdEdition,
        String uidProprietaire);

    /**
     * Initialise une edition asynchrone dont la réalisation sera notifiée par mail.
     * 
     * @param beanIdEdition id du bean de l'édition
     * @param uidProprietaire uid du propriétaire de l'édition
     * @param mailDestinataire --
     * @return EditionAsynchroneNotifieeParMail
     */
    public EditionSynchroneNotifieeParMail initEditionSynchroneNotifieeParMail(String beanIdEdition,
        String uidProprietaire, String mailDestinataire);

    /**
     * Initialise le message mail d'accès à l'édition pour permettre un accès via le portail intranet ou en accédant
     * directement à l'application Par défaut, l'accès est proposé seulement via le portail intranet.
     * 
     * @param jobHistory --
     */
    public void initMessageMailPourAccesAvecOuSansPortailIntranet(JobHistory jobHistory);

    /**
     * Initialise le message mail d'accès à l'édition pour permettre un accès via le portail intranet ou le portail
     * internet Par défaut, l'accès est proposé seulement via le portail intranet.
     * 
     * @param jobHistory --
     */
    public void initMessageMailPourAccesViaPortailIntranetOuInternet(JobHistory jobHistory);

    /**
     * Exécute le traitement de purge des éditions et synchronise le repository des éditions stockées dans le SF avec
     * l'historique des éditions en base de donnée.
     */
    public void purgeEtSynchronise();

    /**
     * Recherche toutes les nouvelles éditions accessibles à un utilisateur représenté par un objet PersonneAnnuaire.
     * 
     * @param personne --
     * @return the list
     */
    public List rechercheNouvellesEditionsAccessiblesAUnUtilisateur(PersonneAnnuaire personne);

    /**
     * Recherche un fichier édition à partir de l'objet JobHistory qui le représente.
     * 
     * @param jobHistory --
     * @return FichierJoint
     */
    public FichierJoint rechercherEdition(JobHistory jobHistory);

    /**
     * Recherche une édition à partir d'un objet JobHistory Met à jour la liste des utilisateurs qui ont consulté
     * l'édition.
     * 
     * @param jobHistory JbHistory qui représente l'édition recherchée
     * @param uid Identifiant de l'utilisateur qui demande à consulter l'édition
     * @return FichierJoint contenant l'édition ou null si l'édition n'est pas disponible
     */
    public FichierJoint rechercherEdition(JobHistory jobHistory, String uid);

    /**
     * Recherche une édition par son Uuid.
     * 
     * @param editionUuid --
     * @return une édition représentée par un objet JobHistory
     */
    public JobHistory rechercherEditionParUuid(String editionUuid);

    /**
     * Recherche la dernière édition produite pour un beanEditionId(type d'édition) et pour une application d'origine.
     * 
     * @param beanEditionId --
     * @param appliOrigine --
     * @return the job history
     */
    public JobHistory rechercherLaDerniereEditionParBeanIdEtParAppliOrigine(String beanEditionId, String appliOrigine);

    /**
     * Recherche la dernière édition produite pour un beanEditionId(type d'édition) et pour une application d'origine.
     * 
     * @param beanEditionId --
     * @param appliOrigine --
     * @param utilisateur --
     * @return the job history
     */
    public JobHistory rechercherLaDerniereEditionParBeanIdEtParAppliOrigineEtParUtilisateur(String beanEditionId,
        String appliOrigine, String utilisateur);

    /**
     * Recherche toutes les éditions accessibles à un utilisateur représenté par un objet PersonneAnnuaire.
     * 
     * @param personne --
     * @return the list
     */
    public List rechercheToutesEditionsAccessiblesAUnUtilisateur(PersonneAnnuaire personne);

    /**
     * Supprime une collection d'édition (JobHistory) de l'historique des éditions, ainsi que le fichier associé dans
     * l'arborescence de stockage des éditions.
     * 
     * @param elementsASupprimer --
     * @return the collection< job history>
     */
    public Collection<JobHistory> supprimerEditions(Collection<JobHistory> elementsASupprimer);

}
