<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xmlns:context="http://www.springframework.org/schema/context"
  xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd">

  <!-- *********************************************************************************** -->
  <!-- Configuration des éléments communs aux éditions (liés ou non à une base de données) -->
  <!-- *********************************************************************************** -->

  <!-- Contrôle que les paramètres d'appel à un service ne sont pas NULL -->
  <bean id="editionparametercontroladvice" class="fr.gouv.finances.lombok.util.spring.ParameterControlAdvice" />
  
  <!-- auto proxy pour application de l'aspect editionparametercontroladvice  -->
  <bean id="editionserviceautoproxy" class="org.springframework.aop.framework.autoproxy.BeanNameAutoProxyCreator">
    <property name="beanNames">
      <list>
        <value>edition*service*impl</value>
      </list>
    </property>
    <property name="interceptorNames">
      <list>
        <value>editionparametercontroladvice</value>
      </list>
    </property>
  </bean>

  <!-- Paramétres des éditions CSV -->
  <bean id="parametragevue.csv" class="fr.gouv.finances.lombok.edition.bean.ParametrageVueJasperCsv">
    <property name="formatEdition">
      <value>${jasper.csv.formatEdition}</value>
    </property>
    <property name="typeMimeEdition">
      <value>${jasper.csv.typeMimeEdition}</value>
    </property>
    <property name="extension">
      <value>${jasper.csv.extension}</value>
    </property>
    <property name="contentType">
      <value>${jasper.csv.contentType}</value>
    </property>
    <property name="characterEncoding">
      <value>${jasper.csv.characterEncoding}</value>
    </property>
    <property name="fieldDelimiter">
      <value>${jasper.csv.fieldDelimiter}</value>
    </property>
    <property name="recordDelimiter">
      <value>${jasper.csv.recordDelimiter}</value>
    </property>
  </bean>

  <!-- Paramétres des éditions XLS -->
  <bean id="parametragevue.xls" class="fr.gouv.finances.lombok.edition.bean.ParametrageVueJasperXls">
    <property name="formatEdition">
      <value>${jasper.xls.formatEdition}</value>
    </property>
    <property name="typeMimeEdition">
      <value>${jasper.xls.typeMimeEdition}</value>
    </property>
    <property name="extension">
      <value>${jasper.xls.extension}</value>
    </property>
    <property name="contentType">
      <value>${jasper.xls.contentType}</value>
    </property>
    <property name="characterEncoding">
      <value>${jasper.xls.characterEncoding}</value>
    </property>
    <property name="onePagePerSheet">
      <value>${jasper.xls.onePagePerSheet}</value>
    </property>
    <property name="detectCellType">
      <value>${jasper.xls.detectCellType}</value>
    </property>
    <property name="fontSizeFixEnabled">
      <value>${jasper.xls.fontSizeFixEnabled}</value>
    </property>
    <property name="removeEmptySpaceBetweenRows">
      <value>${jasper.xls.removeEmptySpaceBetweenRows}</value>
    </property>
    <property name="whitePageBackground">
      <value>${jasper.xls.whitePageBackground}</value>
    </property>
  </bean>

  <!-- Paramétres des éditions liées à JXL -->
  <bean id="parametragevue.jxl" class="fr.gouv.finances.lombok.edition.bean.ParametrageVueJasperJxl">
    <property name="formatEdition">
      <value>${jasper.jxl.formatEdition}</value>
    </property>
    <property name="typeMimeEdition">
      <value>${jasper.jxl.typeMimeEdition}</value>
    </property>
    <property name="extension">
      <value>${jasper.jxl.extension}</value>
    </property>
    <property name="contentType">
      <value>${jasper.jxl.contentType}</value>
    </property>
    <property name="characterEncoding">
      <value>${jasper.jxl.characterEncoding}</value>
    </property>
    <property name="onePagePerSheet">
      <value>${jasper.jxl.onePagePerSheet}</value>
    </property>
    <property name="detectCellType">
      <value>${jasper.jxl.detectCellType}</value>
    </property>
    <property name="fontSizeFixEnabled">
      <value>${jasper.jxl.fontSizeFixEnabled}</value>
    </property>
    <property name="removeEmptySpaceBetweenRows">
      <value>${jasper.jxl.removeEmptySpaceBetweenRows}</value>
    </property>
    <property name="whitePageBackground">
      <value>${jasper.jxl.whitePageBackground}</value>
    </property>
  </bean>

  <!-- Paramétres des éditions PDF -->
  <bean id="parametragevue.pdf" class="fr.gouv.finances.lombok.edition.bean.ParametrageVueJasperPdf">
    <property name="formatEdition">
      <value>${jasper.pdf.formatEdition}</value>
    </property>
    <property name="typeMimeEdition">
      <value>${jasper.pdf.typeMimeEdition}</value>
    </property>
    <property name="extension">
      <value>${jasper.pdf.extension}</value>
    </property>
    <property name="contentType">
      <value>${jasper.pdf.contentType}</value>
    </property>
    <property name="characterEncoding">
      <value>${jasper.pdf.characterEncoding}</value>
    </property>
    <property name="pdfVersion">
      <value>${jasper.pdf.pdfVersion}</value>
    </property>
    <property name="compressed">
      <value>${jasper.pdf.compressed}</value>
    </property>
    <property name="creatingBatchModeBookmarks">
      <value>${jasper.pdf.creatingBatchModeBookmarks}</value>
    </property>
    <property name="forceLinebreakPolicy">
      <value>${jasper.pdf.forceLinebreakPolicy}</value>
    </property>
    <property name="allowAssembly">
      <value>${jasper.pdf.allowAssembly}</value>
    </property>
    <property name="allowCopy">
      <value>${jasper.pdf.allowCopy}</value>
    </property>
    <property name="allowDegradedPrinting">
      <value>${jasper.pdf.allowDegradedPrinting}</value>
    </property>
    <property name="allowPrinting">
      <value>${jasper.pdf.allowPrinting}</value>
    </property>
    <property name="allowScreenReaders">
      <value>${jasper.pdf.allowScreenReaders}</value>
    </property>
    <property name="encrypted">
      <value>${jasper.pdf.encrypted}</value>
    </property>
    <property name="centVingtHuitBitKey">
      <value>${jasper.pdf.centVingtHuitBitKey}</value>
    </property>
    <property name="userpassword">
      <value>${jasper.pdf.userpassword}</value>
    </property>
    <property name="ownerpassword">
      <value>${jasper.pdf.ownerpassword}</value>
    </property>
    <property name="author">
      <value>${jasper.pdf.author}</value>
    </property>
    <property name="creator">
      <value>${jasper.pdf.creator}</value>
    </property>
    <property name="subject">
      <value>${jasper.pdf.subject}</value>
    </property>
    <property name="title">
      <value>${jasper.pdf.title}</value>
    </property>
    <property name="keywords">
      <value>${jasper.pdf.keywords}</value>
    </property>
  </bean>

  <!-- Paramétres des éditions HTML -->
  <bean id="parametragevue.html" class="fr.gouv.finances.lombok.edition.bean.ParametrageVueJasperHtml">
    <property name="formatEdition">
      <value>${jasper.html.formatEdition}</value>
    </property>
    <property name="typeMimeEdition">
      <value>${jasper.html.typeMimeEdition}</value>
    </property>
    <property name="extension">
      <value>${jasper.html.extension}</value>
    </property>
    <property name="contentType">
      <value>${jasper.html.contentType}</value>
    </property>
    <property name="characterEncoding">
      <value>${jasper.html.characterEncoding}</value>
    </property>
    <property name="imagesSourceDir">
      <value>${jasper.html.imagesSourceDir}</value>
    </property>
    <property name="imageURI">
      <value>${jasper.html.imageURI}</value>
    </property>
    <property name="imagesDirName">
      <value>${jasper.html.imagesDirName}</value>
    </property>
    <property name="outputImagesToDir">
      <value>${jasper.html.outputImagesToDir}</value>
    </property>
    <property name="htmlHeader">
      <value>${jasper.html.htmlHeader}</value>
    </property>
    <property name="betweenPagesHtml">
      <value>${jasper.html.betweenPagesHtml}</value>
    </property>
    <property name="htmlFooter">
      <value>${jasper.html.htmlFooter}</value>
    </property>
    <property name="removeEmptySpaceBetweensRows">
      <value>${jasper.html.removeEmptySpaceBetweensRows}</value>
    </property>
    <property name="whitePageBackground">
      <value>${jasper.html.whitePageBackground}</value>
    </property>
    <property name="usingImagesToAlign">
      <value>${jasper.html.usingImagesToAlign}</value>
    </property>
    <property name="sizeUnit">
      <value>${jasper.html.sizeUnit}</value>
    </property>
  </bean>

  <!-- Paramétres des éditions RTF  -->
  <bean id="parametragevue.rtf" class="fr.gouv.finances.lombok.edition.bean.ParametrageVueJasperRtf">
    <property name="formatEdition">
      <value>${jasper.rtf.formatEdition}</value>
    </property>
    <property name="typeMimeEdition">
      <value>${jasper.rtf.typeMimeEdition}</value>
    </property>
    <property name="extension">
      <value>${jasper.rtf.extension}</value>
    </property>
    <property name="contentType">
      <value>${jasper.rtf.contentType}</value>
    </property>
    <property name="characterEncoding">
      <value>${jasper.rtf.characterEncoding}</value>
    </property>
  </bean>

  <!-- Paramétres des éditions ODT -->
  <bean id="parametragevue.odt" class="fr.gouv.finances.lombok.edition.bean.ParametrageVueJasperOdt">
    <property name="formatEdition">
      <value>${jasper.odt.formatEdition}</value>
    </property>
    <property name="typeMimeEdition">
      <value>${jasper.odt.typeMimeEdition}</value>
    </property>
    <property name="extension">
      <value>${jasper.odt.extension}</value>
    </property>
    <property name="contentType">
      <value>${jasper.odt.contentType}</value>
    </property>
    <property name="characterEncoding">
      <value>${jasper.odt.characterEncoding}</value>
    </property>
  </bean>

  <!-- Paramétres des éditions ODS -->
  <bean id="parametragevue.ods" class="fr.gouv.finances.lombok.edition.bean.ParametrageVueJasperOds">
    <property name="formatEdition">
      <value>${jasper.ods.formatEdition}</value>
    </property>
    <property name="typeMimeEdition">
      <value>${jasper.ods.typeMimeEdition}</value>
    </property>
    <property name="extension">
      <value>${jasper.ods.extension}</value>
    </property>
    <property name="contentType">
      <value>${jasper.ods.contentType}</value>
    </property>
    <property name="characterEncoding">
      <value>${jasper.ods.characterEncoding}</value>
    </property>
  </bean>

  <!-- Paramétres des éditions PNG -->
  <bean id="parametragevue.png" class="fr.gouv.finances.lombok.edition.bean.ParametrageVueJasperPng">
    <property name="formatEdition">
      <value>${jasper.png.formatEdition}</value>
    </property>
    <property name="typeMimeEdition">
      <value>${jasper.png.typeMimeEdition}</value>
    </property>
    <property name="extension">
      <value>${jasper.png.extension}</value>
    </property>
    <property name="contentType">
      <value>${jasper.png.contentType}</value>
    </property>
    <property name="characterEncoding">
      <value>${jasper.png.characterEncoding}</value>
    </property>
    <property name="zoomRatio">
      <value>${jasper.png.zoomRatio}</value>
    </property>
  </bean>

  <!-- Paramétres des éditions SVG -->
  <bean id="parametragevue.svg" class="fr.gouv.finances.lombok.edition.bean.ParametrageVueJasperSvg">
    <property name="formatEdition">
      <value>${jasper.svg.formatEdition}</value>
    </property>
    <property name="typeMimeEdition">
      <value>${jasper.svg.typeMimeEdition}</value>
    </property>
    <property name="extension">
      <value>${jasper.svg.extension}</value>
    </property>
    <property name="contentType">
      <value>${jasper.svg.contentType}</value>
    </property>
    <property name="characterEncoding">
      <value>${jasper.svg.characterEncoding}</value>
    </property>
  </bean>

  <!-- Paramétres des éditions CSV -->
  <bean id="parametragevue.cpcsv" class="fr.gouv.finances.lombok.edition.bean.ParametrageVueJasperCsv">
    <property name="formatEdition">
      <value>${jasper.cpcsv.formatEdition}</value>
    </property>
    <property name="typeMimeEdition">
      <value>${jasper.cpcsv.typeMimeEdition}</value>
    </property>
    <property name="extension">
      <value>${jasper.cpcsv.extension}</value>
    </property>
    <property name="contentType">
      <value>${jasper.cpcsv.contentType}</value>
    </property>
    <property name="characterEncoding">
      <value>${jasper.cpcsv.characterEncoding}</value>
    </property>
    <property name="fieldDelimiter">
      <value>${jasper.cpcsv.fieldDelimiter}</value>
    </property>
    <property name="recordDelimiter">
      <value>${jasper.cpcsv.recordDelimiter}</value>
    </property>
  </bean>

  <bean id="cpmultiformatview" class="fr.gouv.finances.lombok.mvc.jasperreports.CpJasperReportsMultiFormatView">
    <!-- mise en commentaire sinon jasper5 ne fonctionne pas<property name="formatMappings"> <props> <prop key="html">fr.gouv.finances.lombok.mvc.jasperreports.CpJasperReportsHtmlView.class</prop> 
      <prop key="csv">org.springframework.web.servlet.view.jasperreports.JasperReportsCsvView.class</prop> <prop key="xls">org.springframework.web.servlet.view.jasperreports.JasperReportsXlsView.class</prop> 
      <prop key="jxl">fr.gouv.finances.lombok.mvc.jasperreports.JasperReportsJxlView.class</prop> <prop key="pdf">fr.gouv.finances.lombok.mvc.jasperreports.CpJasperReportsPdfView.class</prop> 
      <prop key="rtf">fr.gouv.finances.lombok.mvc.jasperreports.CpJasperReportsRtfView.class</prop> <prop key="odt">fr.gouv.finances.lombok.mvc.jasperreports.CpJasperReportsOdtView.class</prop> 
      <prop key="png">fr.gouv.finances.lombok.mvc.jasperreports.CpJasperReportsPngView.class</prop> <prop key="svg">fr.gouv.finances.lombok.mvc.jasperreports.CpJasperReportsSvgView.class</prop> 
      <prop key="cpcsv">fr.gouv.finances.lombok.mvc.jasperreports.CpJasperReportsCsvView.class</prop> <prop key="ods">fr.gouv.finances.lombok.mvc.jasperreports.CpJasperReportsOdsView.class</prop> 
      </props> </property> -->
  </bean>

</beans>