/*
 * Copyright (c) 2016 DGFiP - Tous droits réservés
 * 
 */
package fr.gouv.finances.lombok.edition.bean;

import static org.junit.Assert.*;

import java.util.Map;

import org.junit.Test;

import fr.gouv.finances.lombok.test.pojo.AbstractCorePojoTest;

/**
 * Tests unitaires du POJO ParametrageVueJasperRtf.
 *
 * @author Christophe Breheret-Girardin
 */
public class ParametrageVueJasperRtfTest extends AbstractCorePojoTest<ParametrageVueJasperRtf>
{   
    /**
     * Constructeur.
     */
    public ParametrageVueJasperRtfTest()
    {
        super();
    }

    /**
     * Test de fr.gouv.finances.lombok.edition.bean.ParametrageVueJasperRtf#parametrageJRExporter().
     */
    @Test
    public void parametrageJRExporter()
    {
        Map<String, Object> params = new ParametrageVueJasperRtf().parametrageJRExporter();
        assertEquals(1, params.size());
        assertNotNull(params.get(AbstractParametrageVueJasper.CHARACTER_ENCODING));
    }

    /**
     * Test de fr.gouv.finances.lombok.edition.bean.ParametrageVueJasperRtf#parametrerVue().
     */
    @Test
    public void parametrerVue()
    {
        Map<String, String> params = new ParametrageVueJasperRtf().parametrerVue();
        assertEquals(4, params.size());
        assertNotNull(params.get(ParametrageVueJasperRtf.REPORT_VIEW_KEY));
        assertNotNull(params.get(ParametrageVueJasperRtf.EXTENSION_KEY));
        assertNotNull(params.get(ParametrageVueJasperRtf.CONTENT_TYPE_KEY));
        assertNotNull(params.get(ParametrageVueJasperRtf.MIME_TYPE_KEY));
    }
}