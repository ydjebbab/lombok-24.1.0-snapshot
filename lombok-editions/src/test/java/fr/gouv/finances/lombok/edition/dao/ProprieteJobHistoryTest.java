package fr.gouv.finances.lombok.edition.dao;

import fr.gouv.finances.lombok.test.pojo.AbstractCorePojoTest;

/**
 * Tests unitaires de l'énumération ProprieteJobHistory.
 * @see fr.gouv.finances.lombok.edition.dao.ProprieteJobHistory
 *
 * @author Christophe Breheret-Girardin
 */
public class ProprieteJobHistoryTest extends AbstractCorePojoTest<ProprieteJobHistory>
{
    /**
     * Constructeur.
     */
    public ProprieteJobHistoryTest()
    {
        super();
    }
}
