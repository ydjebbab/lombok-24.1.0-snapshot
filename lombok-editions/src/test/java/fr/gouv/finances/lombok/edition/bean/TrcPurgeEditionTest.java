/*
 * Copyright (c) 2016 DGFiP - Tous droits réservés
 * 
 */
package fr.gouv.finances.lombok.edition.bean;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.Calendar;
import java.util.Date;

import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;

import fr.gouv.finances.lombok.test.pojo.AbstractCorePojoTest;
import fr.gouv.finances.lombok.util.exception.RegleGestionException;

/**
 * Tests unitaires du POJO TrcPurgeEdition.
 * 
 * @author Christophe Breheret-Girardin
 */
public class TrcPurgeEditionTest extends AbstractCorePojoTest<TrcPurgeEdition>
{   
    /** Constante donnant la propriété sur l'application associée à la trace de purge d'édition. */
    private static final String APPLICATION = "application";

    /**
     * Constructeur.
     */
    public TrcPurgeEditionTest()
    {
        super();
    }

    /**
     * Méthode permettant de tester l'égalité du POJO.
     */
    @Test
    public final void testEqualsHashCode()
    {
        EqualsVerifier.forClass(getParameterizedType())
            .suppress(Warning.NONFINAL_FIELDS, Warning.STRICT_INHERITANCE)
            .withOnlyTheseFields("applicationOrigine", "dateDebutPurge")
            .usingGetClass()
            .verify();
    }

    /**
     * Test de fr.gouv.finances.lombok.edition.bean.TrcPurgeEdition#creationTachePurgeEnAttenteDemarrage().
     */
    @Test
    public void creationTachePurgeEnAttenteDemarrage()
    {
        TrcPurgeEdition trcPurgeEdition
            = TrcPurgeEdition.creationTachePurgeEnAttenteDemarrage(APPLICATION);
        assertEquals(APPLICATION, trcPurgeEdition.getApplicationOrigine());
        assertEquals(TrcPurgeEdition.EN_ATTENTE_DEMARRAGE, trcPurgeEdition.getEtatPurge());
        assertNotNull(trcPurgeEdition.getDateDebutPurge());
    }

    /**
     * Test de fr.gouv.finances.lombok.edition.bean.TrcPurgeEdition#demandeArret().
     */
    @Test
    public void demandeArret()
    {
        TrcPurgeEdition trcPurgeEdition = new TrcPurgeEdition();
        trcPurgeEdition.setEtatPurge(TrcPurgeEdition.EN_COURS_EXECUTION);
        trcPurgeEdition.demandeArret();
        assertEquals(TrcPurgeEdition.ARRET_DEMANDE, trcPurgeEdition.getEtatPurge());
    }

    /**
     * Test de fr.gouv.finances.lombok.edition.bean.demarreExecution#demarreExecution().
     */
    @Test
    public void demarreExecution()
    {
        // Appel de la méthode à tester sur un état "en attente de démarrage", et vérifications
        TrcPurgeEdition trcPurgeEdition = new TrcPurgeEdition();
        trcPurgeEdition.setEtatPurge(TrcPurgeEdition.EN_ATTENTE_DEMARRAGE);
        trcPurgeEdition.demarreExecution();
        assertEquals(TrcPurgeEdition.EN_COURS_EXECUTION, trcPurgeEdition.getEtatPurge());
        
        // Appel de la méthode à tester sur un état non "purge interrompue", et vérifications
        final TrcPurgeEdition trcPurgeEdition2 = new TrcPurgeEdition();
        trcPurgeEdition2.setEtatPurge(TrcPurgeEdition.PURGE_INTERROMPUE);
        verifierException(RegleGestionException.class, () -> trcPurgeEdition2.demarreExecution());
    }

    /**
     * Test de fr.gouv.finances.lombok.edition.bean.TrcPurgeEdition#interrompreSuiteDemandeAdministrateur().
     */
    @Test
    public void interrompreSuiteDemandeAdministrateur()
    {
        // Appel de la méthode à tester sur un état "arrêt demandé", et vérifications
        TrcPurgeEdition trcPurgeEdition = new TrcPurgeEdition();
        trcPurgeEdition.setEtatPurge(TrcPurgeEdition.ARRET_DEMANDE);
        trcPurgeEdition.interrompreSuiteDemandeAdministrateur();
        assertEquals(TrcPurgeEdition.PURGE_INTERROMPUE, trcPurgeEdition.getEtatPurge());
        assertEquals(TrcPurgeEdition.MSG_PURGE_INTERROMPUE_ADMINISTRATEUR, trcPurgeEdition.getMessageErreur());
        
        // Appel de la méthode à tester sur un état non "arrêt demandé", et vérifications
        final TrcPurgeEdition trcPurgeEdition2 = new TrcPurgeEdition();
        trcPurgeEdition2.setEtatPurge(TrcPurgeEdition.EN_COURS_EXECUTION);
        verifierException(RegleGestionException.class
            , () -> trcPurgeEdition2.interrompreSuiteDemandeAdministrateur());
    }

    /**
     * Test de fr.gouv.finances.lombok.edition.bean.TrcPurgeEdition#interrompreSuiteEvtInattendu().
     */
    @Test
    public void interrompreSuiteEvtInattendu()
    {
        // Appel de la méthode à tester sur un état "arrêt demandé", et vérifications
        TrcPurgeEdition trcPurgeEdition = new TrcPurgeEdition();
        trcPurgeEdition.setEtatPurge(TrcPurgeEdition.ARRET_DEMANDE);
        trcPurgeEdition.interrompreSuiteEvtInattendu();
        assertEquals(TrcPurgeEdition.PURGE_INTERROMPUE, trcPurgeEdition.getEtatPurge());
        assertEquals(TrcPurgeEdition.MSG_PURGE_INTERROMPUE_AUTRE, trcPurgeEdition.getMessageErreur());
        
        // Appel de la méthode à tester sur un état non "arrêt demandé", et vérifications
        final TrcPurgeEdition trcPurgeEdition2 = new TrcPurgeEdition();
        trcPurgeEdition2.setEtatPurge(TrcPurgeEdition.EN_COURS_EXECUTION);
        verifierException(RegleGestionException.class, () -> trcPurgeEdition2.interrompreSuiteEvtInattendu());
    }

    /**
     * Test de fr.gouv.finances.lombok.edition.bean.TrcPurgeEdition#nePasDemarrerExecution().
     */
    @Test
    public void nePasDemarrerExecution()
    {
        TrcPurgeEdition trcPurgeEdition = new TrcPurgeEdition();
        trcPurgeEdition.nePasDemarrerExecution();
        assertEquals(TrcPurgeEdition.AUTRE_PURGE_EN_COURS, trcPurgeEdition.getEtatPurge());
        assertEquals(TrcPurgeEdition.MSG_AUTRE_PURGE_EN_COURS, trcPurgeEdition.getMessageErreur());
    }

    /**
     * Test de fr.gouv.finances.lombok.edition.bean.TrcPurgeEdition#termineAvecErreur().
     */
    @Test
    public void termineAvecErreur()
    {
        // Appel de la méthode à tester sur un état "arrêt demandé", et vérifications
        TrcPurgeEdition trcPurgeEdition = new TrcPurgeEdition();
        trcPurgeEdition.setEtatPurge(TrcPurgeEdition.ARRET_DEMANDE);
        trcPurgeEdition.termineAvecErreur(null);
        assertEquals(TrcPurgeEdition.TERMINE_AVEC_ERREUR, trcPurgeEdition.getEtatPurge());
        assertNull(trcPurgeEdition.getMessageErreur());

        trcPurgeEdition.setEtatPurge(TrcPurgeEdition.EN_COURS_EXECUTION);
        trcPurgeEdition.termineAvecErreur(null);
        assertEquals(TrcPurgeEdition.TERMINE_AVEC_ERREUR, trcPurgeEdition.getEtatPurge());
        assertNull(trcPurgeEdition.getMessageErreur());

        trcPurgeEdition.setEtatPurge(TrcPurgeEdition.EN_ATTENTE_DEMARRAGE);
        trcPurgeEdition.termineAvecErreur(null);
        assertEquals(TrcPurgeEdition.TERMINE_AVEC_ERREUR, trcPurgeEdition.getEtatPurge());
        assertNull(trcPurgeEdition.getMessageErreur());

        trcPurgeEdition.setEtatPurge(TrcPurgeEdition.PURGE_INTERROMPUE);
        trcPurgeEdition.termineAvecErreur(null);
        assertEquals(TrcPurgeEdition.TERMINE_AVEC_ERREUR, trcPurgeEdition.getEtatPurge());
        assertNull(trcPurgeEdition.getMessageErreur());

        trcPurgeEdition.setEtatPurge(TrcPurgeEdition.ARRET_DEMANDE);
        trcPurgeEdition.termineAvecErreur("ff");
        assertEquals(TrcPurgeEdition.TERMINE_AVEC_ERREUR, trcPurgeEdition.getEtatPurge());
        assertEquals("ff", trcPurgeEdition.getMessageErreur());

        trcPurgeEdition.setEtatPurge(TrcPurgeEdition.ARRET_DEMANDE);
        trcPurgeEdition.termineAvecErreur(StringUtils.rightPad("ff", 5000, "a"));
        assertEquals(TrcPurgeEdition.TERMINE_AVEC_ERREUR, trcPurgeEdition.getEtatPurge());
        assertEquals(StringUtils.rightPad("ff", 3999, "a"), trcPurgeEdition.getMessageErreur());

        // Appel de la méthode à tester sur un état non permis, et vérifications
        final TrcPurgeEdition trcPurgeEdition2 = new TrcPurgeEdition();
        trcPurgeEdition2.setEtatPurge(TrcPurgeEdition.TERMINE_SANS_ERREUR);
        verifierException(RegleGestionException.class, () -> trcPurgeEdition2.termineAvecErreur(null));
    }

    /**
     * Test de fr.gouv.finances.lombok.edition.bean.TrcPurgeEdition#termineSansErreur().
     */
    @Test
    public void termineSansErreur()
    {
        // Appel de la méthode à tester sur un état "arrêt demandé", et vérifications
        TrcPurgeEdition trcPurgeEdition = new TrcPurgeEdition();
        trcPurgeEdition.setEtatPurge(TrcPurgeEdition.ARRET_DEMANDE);
        trcPurgeEdition.termineSansErreur();
        assertEquals(TrcPurgeEdition.TERMINE_SANS_ERREUR, trcPurgeEdition.getEtatPurge());
        assertNull(trcPurgeEdition.getMessageErreur());

        // Appel de la méthode à tester sur un état "en cours d'exécution", et vérifications
        trcPurgeEdition = new TrcPurgeEdition();
        trcPurgeEdition.setEtatPurge(TrcPurgeEdition.EN_COURS_EXECUTION);
        trcPurgeEdition.termineSansErreur();
        assertEquals(TrcPurgeEdition.TERMINE_SANS_ERREUR, trcPurgeEdition.getEtatPurge());
        assertNull(trcPurgeEdition.getMessageErreur());
        
        trcPurgeEdition = new TrcPurgeEdition();
        trcPurgeEdition.setEtatPurge(TrcPurgeEdition.PURGE_INTERROMPUE);
        trcPurgeEdition.termineSansErreur();
        assertEquals(TrcPurgeEdition.TERMINE_SANS_ERREUR, trcPurgeEdition.getEtatPurge());
        assertNull(trcPurgeEdition.getMessageErreur());
        
        // Appel de la méthode à tester sur un état non permis, et vérifications
        final TrcPurgeEdition trcPurgeEdition2 = new TrcPurgeEdition();
        trcPurgeEdition2.setEtatPurge(TrcPurgeEdition.TERMINE_SANS_ERREUR);
        verifierException(RegleGestionException.class, () -> trcPurgeEdition2.termineSansErreur());
    }
    
    /**
     * Test de fr.gouv.finances.lombok.edition.bean.TrcPurgeEdition#enregistreDureeEtNbEltsPurges(
     * java.lang.long).
     */
    @Test
    public void enregistreDureeEtNbEltsPurges()
    {
        Date date = getDate(2018, 2, 1);
        TrcPurgeEdition trcPurgeEdition = new TrcPurgeEdition();
        assertNull(trcPurgeEdition.getDateFinPurge());
        assertNull(trcPurgeEdition.getDureeExecution());
        assertNull(trcPurgeEdition.getNbEditionsSupprimees());

        trcPurgeEdition.setDateDebutPurge(date);
        trcPurgeEdition.enregistreDureeEtNbEltsPurges(10);
        assertEquals(date, trcPurgeEdition.getDateDebutPurge());
        assertNotNull(trcPurgeEdition.getDateFinPurge());
        long duree = trcPurgeEdition.getDateFinPurge().getTime() - date.getTime();
        assertEquals((duree / 1000) / 60, trcPurgeEdition.getDureeExecution().longValue());
        assertEquals(10l, trcPurgeEdition.getNbEditionsSupprimees().longValue());
    }
    
    /**
     * Méthode permettant de générer une date.
     *
     * @param annee année de la date
     * @param mois mois de la date
     * @param jour jour de la date
     * @return la date générée
     */
    protected static Date getDate(int annee, int mois, int jour)
    {
        Calendar calendar = Calendar.getInstance();
        calendar.set(annee, mois, jour, 0, 0, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
    }
}