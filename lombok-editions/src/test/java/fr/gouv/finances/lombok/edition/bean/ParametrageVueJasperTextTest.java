/*
 * Copyright (c) 2016 DGFiP - Tous droits réservés
 * 
 */
package fr.gouv.finances.lombok.edition.bean;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Map;

import org.junit.Test;

import fr.gouv.finances.lombok.test.pojo.AbstractCorePojoTest;

/**
 * Tests unitaires du POJO ParametrageVueJasperText.
 * 
 * @author Christophe Breheret-Girardin
 */
public class ParametrageVueJasperTextTest extends AbstractCorePojoTest<ParametrageVueJasperText>
{   
    /**
     * Constructeur.
     */
    public ParametrageVueJasperTextTest()
    {
        super();
    }

    /**
     * Test de fr.gouv.finances.lombok.edition.bean.ParametrageVueJasperText#parametrageJRExporter().
     */
    @Test
    public void parametrageJRExporter()
    {
        Map<String, Object> params = new ParametrageVueJasperText().parametrageJRExporter();
        assertEquals(5, params.size());
    }

    /**
     * Test de fr.gouv.finances.lombok.edition.bean.ParametrageVueJasperText#parametrerVue().
     */
    @Test
    public void parametrerVue()
    {
        Map<String, String> params = new ParametrageVueJasperText().parametrerVue();
        assertEquals(4, params.size());
        assertNotNull(params.get(ParametrageVueJasperText.REPORT_VIEW_KEY));
        assertNotNull(params.get(ParametrageVueJasperText.EXTENSION_KEY));
        assertNotNull(params.get(ParametrageVueJasperText.CONTENT_TYPE_KEY));
        assertNotNull(params.get(ParametrageVueJasperText.MIME_TYPE_KEY));
    }
}