/*
 * Copyright (c) 2016 DGFiP - Tous droits réservés
 * 
 */
package fr.gouv.finances.lombok.edition.bean;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Map;

import org.junit.Test;

import fr.gouv.finances.lombok.test.pojo.AbstractCorePojoTest;

/**
 * Tests unitaires du POJO ParametrageVueJasperHtml.
 * 
 * @author Christophe Breheret-Girardin
 */
public class ParametrageVueJasperHtmlTest extends AbstractCorePojoTest<ParametrageVueJasperHtml>
{   
    /**
     * Constructeur.
     */
    public ParametrageVueJasperHtmlTest()
    {
        super();
    }

    /**
     * Test de fr.gouv.finances.lombok.edition.bean.ParametrageVueJasperHtml#parametrageJRExporter().
     */
    @Test
    public void parametrageJRExporter()
    {
        Map<String, Object> params = new ParametrageVueJasperHtml().parametrageJRExporter();
        assertEquals(7, params.size());
        assertNotNull(params.get(ParametrageVueJasperHtml.CHARACTER_ENCODING));
    }
    
    /**
     * Test de fr.gouv.finances.lombok.edition.bean.ParametrageVueJasperHtml#parametrerVue().
     */
    @Test
    public void parametrerVue()
    {
        Map<String, String> params = new ParametrageVueJasperHtml().parametrerVue();
        assertEquals(4, params.size());
        assertNotNull(params.get(ParametrageVueJasperHtml.REPORT_VIEW_KEY));
        assertNotNull(params.get(ParametrageVueJasperHtml.EXTENSION_KEY));
        assertNotNull(params.get(ParametrageVueJasperHtml.CONTENT_TYPE_KEY));
        assertNotNull(params.get(ParametrageVueJasperHtml.MIME_TYPE_KEY));
    }
}