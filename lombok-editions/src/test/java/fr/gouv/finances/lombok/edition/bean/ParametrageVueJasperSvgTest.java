/*
 * Copyright (c) 2016 DGFiP - Tous droits réservés
 * 
 */
package fr.gouv.finances.lombok.edition.bean;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Map;

import org.junit.Test;

import fr.gouv.finances.lombok.test.pojo.AbstractCorePojoTest;

/**
 * Tests unitaires du POJO ParametrageVueJasperSvg.
 * 
 * @author Christophe Breheret-Girardin
 */
public class ParametrageVueJasperSvgTest extends AbstractCorePojoTest<ParametrageVueJasperSvg>
{   
    /**
     * Constructeur.
     */
    public ParametrageVueJasperSvgTest()
    {
        super();
    }

    /**
     * Test de fr.gouv.finances.lombok.edition.bean.ParametrageVueJasperSvg#parametrageJRExporter().
     */
    @Test
    public void parametrageJRExporter()
    {
        Map<String, Object> params = new ParametrageVueJasperSvg().parametrageJRExporter();
        assertEquals(1, params.size());
        assertNotNull(params.get(AbstractParametrageVueJasper.CHARACTER_ENCODING));
    }

    /**
     * Test de fr.gouv.finances.lombok.edition.bean.ParametrageVueJasperSvg#parametrerVue().
     */
    @Test
    public void parametrerVue()
    {
        Map<String, String> params = new ParametrageVueJasperSvg().parametrerVue();
        assertEquals(4, params.size());
        assertNotNull(params.get(ParametrageVueJasperSvg.REPORT_VIEW_KEY));
        assertNotNull(params.get(ParametrageVueJasperSvg.EXTENSION_KEY));
        assertNotNull(params.get(ParametrageVueJasperSvg.CONTENT_TYPE_KEY));
        assertNotNull(params.get(ParametrageVueJasperSvg.MIME_TYPE_KEY));
    }
}