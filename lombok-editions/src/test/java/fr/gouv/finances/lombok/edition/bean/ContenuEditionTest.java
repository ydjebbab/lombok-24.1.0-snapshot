/*
 * Copyright (c) 2016 DGFiP - Tous droits réservés
 * 
 */
package fr.gouv.finances.lombok.edition.bean;

import java.io.File;
import java.net.URL;

import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;

import org.junit.Test;

import fr.gouv.finances.lombok.test.pojo.AbstractCorePojoTest;

/**
 * Tests unitaires du POJO ContenuEdition.
 * 
 * @author Christophe Breheret-Girardin
 */
public class ContenuEditionTest extends AbstractCorePojoTest<ContenuEdition>
{   
    /**
     * Constructeur permettant d'initialiser des valeurs préfabriquées utilisées automatiquement dans les tests.
     */
    public ContenuEditionTest()
    {
        super();
        // Ajout d'une valeur par défaut pour les paramètres de type byte[]
        addPrefabValues(Byte.TYPE, new byte[]{});
        // Ajout d'une valeur par défaut pour les paramètres de type File
        URL url = getClass().getResource(this.getClass().getSimpleName() + ".class");
        addPrefabValues(File.class, new File(url.getFile()));
    }

    /**
     * Méthode permettant de tester l'égalité du POJO.
     */
    @Test
    public final void testEqualsHashCode()
    {
        EqualsVerifier.forClass(getParameterizedType())
            .suppress(Warning.NONFINAL_FIELDS, Warning.NULL_FIELDS, Warning.STRICT_INHERITANCE)
            .withOnlyTheseFields("dataByte")
            .usingGetClass()
            .verify();
    }
}
