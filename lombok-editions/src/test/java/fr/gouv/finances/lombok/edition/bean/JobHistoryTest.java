/*
 * Copyright (c) 2016 DGFiP - Tous droits réservés
 * 
 */
package fr.gouv.finances.lombok.edition.bean;

import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;

import org.junit.Test;

import fr.gouv.finances.lombok.test.pojo.AbstractCorePojoTest;

/**
 * Tests unitaires du POJO JobHistory.
 * 
 * @author Christophe Breheret-Girardin
 */
public class JobHistoryTest extends AbstractCorePojoTest<JobHistory>
{   
    /**
     * Constructeur.
     */
    public JobHistoryTest()
    {
        super();
    }

    /**
     * Méthode permettant de tester l'égalité des contenus d'édition.
     */
    @Test
    public final void testEqualsHashCode()
    {
        EqualsVerifier.forClass(getParameterizedType())
            .suppress(Warning.NONFINAL_FIELDS, Warning.STRICT_INHERITANCE)
            .withOnlyTheseFields("editionUuid")
            .usingGetClass()
            .verify();
    }
}
