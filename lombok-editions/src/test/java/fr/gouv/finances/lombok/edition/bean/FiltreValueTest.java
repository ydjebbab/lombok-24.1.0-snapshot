/*
 * Copyright (c) 2016 DGFiP - Tous droits réservés
 * 
 */
package fr.gouv.finances.lombok.edition.bean;

import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;

import org.junit.Test;

import fr.gouv.finances.lombok.test.pojo.AbstractCorePojoTest;

/**
 * Tests unitaires du POJO FiltreValue.
 * 
 * @author Christophe Breheret-Girardin
 */
public class FiltreValueTest extends AbstractCorePojoTest<FiltreValue>
{   
    /**
     * Constructeur.
     */
    public FiltreValueTest()
    {
        super();
    }

    /**
     * Méthode permettant de tester l'égalité des contenus d'édition.
     */
    @Test
    public final void testEqualsHashCode()
    {
        EqualsVerifier.forClass(getParameterizedType())
            .suppress(Warning.NONFINAL_FIELDS, Warning.STRICT_INHERITANCE)
            .withOnlyTheseFields("value")
            .usingGetClass()
            .verify();
    }
}
