/*
 * Copyright (c) 2016 DGFiP - Tous droits réservés
 * 
 */
package fr.gouv.finances.lombok.edition.bean;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.Iterator;

import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;

import org.junit.Test;

import fr.gouv.finances.lombok.test.pojo.AbstractCorePojoTest;

/**
 * Tests unitaires du POJO FiltreEdition.
 * 
 * @author Christophe Breheret-Girardin
 */
public class FiltreEditionTest extends AbstractCorePojoTest<FiltreEdition>
{   
    /**
     * Constructeur.
     */
    public FiltreEditionTest()
    {
        super();
    }

    /**
     * Méthode permettant de tester l'égalité du POJO.
     */
    @Test
    public final void testEqualsHashCode()
    {
        EqualsVerifier.forClass(getParameterizedType())
            .suppress(Warning.NONFINAL_FIELDS, Warning.STRICT_INHERITANCE)
            .withOnlyTheseFields("nomDuFiltre")
            .usingGetClass()
            .verify();
    }

    /**
     * Test de fr.gouv.finances.lombok.edition.bean.FiltreEdition#affecterLaValeur().
     */
    @Test
    public void affecterLaValeur()
    {
        FiltreEdition filtreEdition = new FiltreEdition();
        FiltreEdition filtreEdition2 = filtreEdition.affecterLaValeur(null);
        assertTrue(filtreEdition.getValsFiltre().isEmpty());
        assertTrue(filtreEdition2.getValsFiltre().isEmpty());

        FiltreEdition filtreEdition3 = filtreEdition.affecterLaValeur("cbre");
        assertEquals(1, filtreEdition.getValsFiltre().size());
        assertEquals(1, filtreEdition3.getValsFiltre().size());
        assertEquals("cbre", filtreEdition.getValsFiltre().iterator().next().getValue());
        assertEquals("cbre", filtreEdition3.getValsFiltre().iterator().next().getValue());
        
        FiltreEdition filtreEdition4 = filtreEdition.affecterLaValeur("dmod");
        assertEquals(2, filtreEdition.getValsFiltre().size());
        assertEquals(2, filtreEdition4.getValsFiltre().size());
        Iterator<FiltreValue> iterator = filtreEdition.getValsFiltre().iterator();
        Iterator<FiltreValue> iterator4 = filtreEdition.getValsFiltre().iterator();
        assertEquals("dmod", iterator.next().getValue());
        assertEquals("dmod", iterator4.next().getValue());
        assertEquals("cbre", iterator.next().getValue());
        assertEquals("cbre", iterator4.next().getValue());
    }
}
