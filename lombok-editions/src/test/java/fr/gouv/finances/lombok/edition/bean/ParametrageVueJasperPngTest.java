/*
 * Copyright (c) 2016 DGFiP - Tous droits réservés
 * 
 */
package fr.gouv.finances.lombok.edition.bean;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Map;

import org.junit.Test;

import fr.gouv.finances.lombok.test.pojo.AbstractCorePojoTest;

/**
 * Tests unitaires du POJO ParametrageVueJasperPng.
 * 
 * @author Christophe Breheret-Girardin
 */
public class ParametrageVueJasperPngTest extends AbstractCorePojoTest<ParametrageVueJasperPng>
{   
    /**
     * Constructeur.
     */
    public ParametrageVueJasperPngTest()
    {
        super();
    }

    /**
     * Test de fr.gouv.finances.lombok.edition.bean.ParametrageVueJasperPng#parametrageJRExporter().
     */
    @Test
    public void parametrageJRExporter()
    {
        Map<String, Object> params = new ParametrageVueJasperPng().parametrageJRExporter();
        assertEquals(3, params.size());
        assertNotNull(params.get(ParametrageVueJasperPng.CHARACTER_ENCODING));
    }

    /**
     * Test de fr.gouv.finances.lombok.edition.bean.ParametrageVueJasperPng#parametrerVue().
     */
    @Test
    public void parametrerVue()
    {
        Map<String, String> params = new ParametrageVueJasperPng().parametrerVue();
        assertEquals(4, params.size());
        assertNotNull(params.get(ParametrageVueJasperPng.REPORT_VIEW_KEY));
        assertNotNull(params.get(ParametrageVueJasperPng.EXTENSION_KEY));
        assertNotNull(params.get(ParametrageVueJasperPng.CONTENT_TYPE_KEY));
        assertNotNull(params.get(ParametrageVueJasperPng.MIME_TYPE_KEY));
    }
}