package fr.gouv.finances.lombok.edition.dao;

import fr.gouv.finances.lombok.test.pojo.AbstractCorePojoTest;

/**
 * Tests unitaires de l'énumération ProprieteStatistiqueJobHistoryEdition.
 * @see fr.gouv.finances.lombok.edition.dao.ProprieteStatistiqueJobHistoryEdition
 *
 * @author Christophe Breheret-Girardin
 */
public class ProprieteStatistiqueJobHistoryTest extends AbstractCorePojoTest<ProprieteStatistiqueJobHistoryEdition>
{
    /**
     * Constructeur.
     */
    public ProprieteStatistiqueJobHistoryTest()
    {
        super();
    }
}
