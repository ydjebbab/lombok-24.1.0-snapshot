/*
 * Copyright (c) 2016 DGFiP - Tous droits réservés
 * 
 */
package fr.gouv.finances.lombok.edition.bean;

import fr.gouv.finances.lombok.test.pojo.AbstractCorePojoTest;

/**
 * Tests unitaires du POJO ArchivageEdition.
 * 
 * @author Christophe Breheret-Girardin
 */
public class ArchivageEditionTest extends AbstractCorePojoTest<ArchivageEdition>
{   
    /**
     * Constructeur.
     */
    public ArchivageEditionTest()
    {
        super();
    }
}
