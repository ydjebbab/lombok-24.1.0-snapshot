/*
 * Copyright (c) 2016 DGFiP - Tous droits réservés
 * 
 */
package fr.gouv.finances.lombok.edition.bean;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Map;

import org.junit.Test;

import fr.gouv.finances.lombok.test.pojo.AbstractCorePojoTest;

/**
 * Tests unitaires du POJO ParametrageVueJasperOdt.
 * 
 * @author Christophe Breheret-Girardin
 */
public class ParametrageVueJasperOdtTest extends AbstractCorePojoTest<ParametrageVueJasperOdt>
{   
    /**
     * Constructeur.
     */
    public ParametrageVueJasperOdtTest()
    {
        super();
    }

    /**
     * Test de fr.gouv.finances.lombok.edition.bean.ParametrageVueJasperOdt#parametrageJRExporter().
     */
    @Test
    public void parametrageJRExporter()
    {
        Map<String, Object> params = new ParametrageVueJasperOdt().parametrageJRExporter();
        assertEquals(1, params.size());
        assertNotNull(params.get(ParametrageVueJasperOdt.CHARACTER_ENCODING));
    }

    /**
     * Test de fr.gouv.finances.lombok.edition.bean.ParametrageVueJasperOdt#parametrerVue().
     */
    @Test
    public void parametrerVue()
    {
        Map<String, String> params = new ParametrageVueJasperOdt().parametrerVue();
        assertEquals(4, params.size());
        assertNotNull(params.get(ParametrageVueJasperOdt.REPORT_VIEW_KEY));
        assertNotNull(params.get(ParametrageVueJasperOdt.EXTENSION_KEY));
        assertNotNull(params.get(ParametrageVueJasperOdt.CONTENT_TYPE_KEY));
        assertNotNull(params.get(ParametrageVueJasperOdt.MIME_TYPE_KEY));
    }
}
