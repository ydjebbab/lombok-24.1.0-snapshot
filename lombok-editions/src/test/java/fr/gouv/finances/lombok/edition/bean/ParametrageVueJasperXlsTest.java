/*
 * Copyright (c) 2016 DGFiP - Tous droits réservés
 * 
 */
package fr.gouv.finances.lombok.edition.bean;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Map;

import org.junit.Test;

import fr.gouv.finances.lombok.test.pojo.AbstractCorePojoTest;

/**
 * Tests unitaires du POJO ParametrageVueJasperXls.
 * 
 * @author Christophe Breheret-Girardin
 */
public class ParametrageVueJasperXlsTest extends AbstractCorePojoTest<ParametrageVueJasperXls>
{   
    /**
     * Constructeur.
     */
    public ParametrageVueJasperXlsTest()
    {
        super();
    }

    /**
     * Test de fr.gouv.finances.lombok.edition.bean.ParametrageVueJasperXls#parametrageJRExporter().
     */
    @Test
    public void parametrageJRExporter()
    {
        Map<String, Object> params = new ParametrageVueJasperXls().parametrageJRExporter();
        assertEquals(6, params.size());
    }

    /**
     * Test de fr.gouv.finances.lombok.edition.bean.ParametrageVueJasperXls#parametrerVue().
     */
    @Test
    public void parametrerVue()
    {
        Map<String, String> params = new ParametrageVueJasperXls().parametrerVue();
        assertEquals(4, params.size());
        assertNotNull(params.get(ParametrageVueJasperXls.REPORT_VIEW_KEY));
        assertNotNull(params.get(ParametrageVueJasperXls.EXTENSION_KEY));
        assertNotNull(params.get(ParametrageVueJasperXls.CONTENT_TYPE_KEY));
        assertNotNull(params.get(ParametrageVueJasperXls.MIME_TYPE_KEY));
    }
}