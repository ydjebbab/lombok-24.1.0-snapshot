/*
 * Copyright (c) 2016 DGFiP - Tous droits réservés
 * 
 */
package fr.gouv.finances.lombok.edition.bean;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Map;

import org.junit.Test;

import fr.gouv.finances.lombok.test.pojo.AbstractCorePojoTest;

/**
 * Tests unitaires du POJO ParametrageVueJasperPdf.
 * 
 * @author Christophe Breheret-Girardin
 */
public class ParametrageVueJasperPdfTest extends AbstractCorePojoTest<ParametrageVueJasperPdf>
{   
    /**
     * Constructeur.
     */
    public ParametrageVueJasperPdfTest()
    {
        super();
    }

    /**
     * Test de fr.gouv.finances.lombok.edition.bean.ParametrageVueJasperPdf#parametrageJRExporter().
     */
    @Test
    public void parametrageJRExporter()
    {
        Map<String, Object> params = new ParametrageVueJasperPdf().parametrageJRExporter();
        assertEquals(8, params.size());
        assertNotNull(params.get(ParametrageVueJasperOds.CHARACTER_ENCODING));
    }

    /**
     * Test de fr.gouv.finances.lombok.edition.bean.ParametrageVueJasperPdf#parametrerVue().
     */
    @Test
    public void parametrerVue()
    {
        Map<String, String> params = new ParametrageVueJasperPdf().parametrerVue();
        assertEquals(4, params.size());
        assertNotNull(params.get(ParametrageVueJasperPdf.REPORT_VIEW_KEY));
        assertNotNull(params.get(ParametrageVueJasperPdf.EXTENSION_KEY));
        assertNotNull(params.get(ParametrageVueJasperPdf.CONTENT_TYPE_KEY));
        assertNotNull(params.get(ParametrageVueJasperPdf.MIME_TYPE_KEY));
    }
}
