/*
 * Copyright (c) 2016 DGFiP - Tous droits réservés
 * 
 */
package fr.gouv.finances.lombok.edition.bean;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import fr.gouv.finances.lombok.test.pojo.AbstractCorePojoTest;

/**
 * Tests unitaires du POJO DestinationEdition.
 * 
 * @author Christophe Breheret-Girardin
 */
public class DestinationEditionTest extends AbstractCorePojoTest<DestinationEdition>
{
    /** Constante donnant la propriété sur l'information mono destinataire ou non. */
    private static final String PROPRIETE_MONO_DESTINATAIRE = "monoDestinataire";
    
    /** Constante donnant la propriété sur les profils destination. */
    private static final String PROPRIETE_PROFILS_DESTINATION = "profilsDesti";

    /** Constante donnant la valeur d'un profil utilisable dans les tests. */
    private static final String VALEUR_PROFIL = "profil";
    
    /** Constante d'une 1ère valeur d'un UUID utilisable dans les tests. */
    private static final String UUID_1 = "UUID 1";
    
    /** Constante d'une 2ème valeur d'un UUID utilisable dans les tests. */
    private static final String UUID_2 = "UUID 2";

    /** Constante d'un UUID inconnu. */
    private static final String UUID_INCONNU = "UUID INCONNU";

    /**
     * Constructeur.
     */
    public DestinationEditionTest()
    {
        super();
    }

    /**
     * Test de fr.gouv.finances.lombok.edition.bean.DestinationEdition#ajouterUnProfilDestinataire(
     * java.lang.String)
     */
    @Test
    public void ajouterUnProfilDestinataire()
    {
        // Génération d'un nouvel historique des travaux d'édition
        JobHistory jobHistorySansProfil = new JobHistory();
        // Mise à null de la propriété à renseigner pour tester l'absence de NullPointerException
        ReflectionTestUtils.setField(
            jobHistorySansProfil.getDestinationEdition(), PROPRIETE_PROFILS_DESTINATION, null);
        assertNull(jobHistorySansProfil.getDestinationEdition().getProfilsDesti());
        // Exécution de la méthode à tester
        executerEtcontrolerProfilsDesti(jobHistorySansProfil);

        // Génération d'un nouvel historique des travaux d'édition
        JobHistory jobHistory = new JobHistory();
        // Contrôle des données de destination
        assertTrue((Boolean) ReflectionTestUtils.getField(jobHistory.getDestinationEdition()
            , PROPRIETE_MONO_DESTINATAIRE));
        assertTrue(jobHistory.getDestinationEdition().getProfilsDesti().isEmpty());
        executerEtcontrolerProfilsDesti(jobHistory);
    }

    private void executerEtcontrolerProfilsDesti(JobHistory jobHistory)
    {
        // Exécution de la méthode à tester
        jobHistory.getDestinationEdition().ajouterUnProfilDestinataire(VALEUR_PROFIL);
        // Contrôle des données de destination modifiées
        assertFalse((Boolean) ReflectionTestUtils.getField(jobHistory.getDestinationEdition()
            , PROPRIETE_MONO_DESTINATAIRE));
        assertFalse(jobHistory.getDestinationEdition().getProfilsDesti().isEmpty());
        assertEquals(1, jobHistory.getDestinationEdition().getProfilsDesti().size());
        assertEquals(VALEUR_PROFIL
            , jobHistory.getDestinationEdition().getProfilsDesti().iterator().next().getNomProfil());
    }
    
    /**
     * Test de fr.gouv.finances.lombok.edition.bean.DestinationEdition#isEditionDejaConsulteeParCetUid(
     * java.lang.String)
     */
    @Test
    public void isEditionDejaConsulteeParCetUid()
    {
        // Appel de la méthode à tester sur des paramétres non initialisés et/ou vides, et vérifications
        IntStream.rangeClosed(1, 23).forEach(indice ->
            assertFalse("Le JobHistory #" + indice + " ne devrait pas déjà être consulté"
                , getJobHistory(indice).getDestinationEdition().isEditionDejaConsulteeParCetUid(null)));

        // Appel de la méthode à tester sans résultat, et vérifications
        IntStream.rangeClosed(1, 23).forEach(indice ->
            assertFalse("Le JobHistory #" + indice + " ne devrait pas déjà être consulté"
                , getJobHistory(indice).getDestinationEdition().isEditionDejaConsulteeParCetUid(UUID_INCONNU)));

        // Appel de la méthode à tester sur le propriétaire, et vérifications
        IntStream.of(1, 2, 3, 4, 6, 7, 8, 9, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23).forEach(indice ->
            assertFalse("Le JobHistory #" + indice + " ne devrait pas déjà être consulté"
                , getJobHistory(indice).getDestinationEdition().isEditionDejaConsulteeParCetUid(UUID_1)));
        IntStream.of(5, 10).forEach(indice ->
            assertTrue("Le JobHistory #" + indice + " devrait déjà être consulté"
                , getJobHistory(indice).getDestinationEdition().isEditionDejaConsulteeParCetUid(UUID_1)));

        // Appel de la méthode à tester sur un autre utilisateur, et vérifications
        IntStream.of(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 21, 22, 23).forEach(indice ->
            assertFalse("Le JobHistory #" + indice + " ne devrait pas déjà être consulté"
                , getJobHistory(indice).getDestinationEdition().isEditionDejaConsulteeParCetUid(UUID_2)));
        IntStream.of(16, 17, 18, 19, 20).forEach(indice ->
            assertTrue("Le JobHistory #" + indice + " devrait déjà être consulté"
                , getJobHistory(indice).getDestinationEdition().isEditionDejaConsulteeParCetUid(UUID_2)));
    }

    /**
     * Test de fr.gouv.finances.lombok.edition.bean.DestinationEdition#metAJourLaListeDesConsultations(
     * java.lang.String)
     */
    @Test
    public void metAJourLaListeDesConsultations()
    {
        // Initialisation des jeux d'essai
        Map<Integer, JobHistory> mapJobHistoryNonInit = getJdd();
        
        // Appel de la méthode à tester sur des paramétres non initialisés et/ou vides, et vérifications
        mapJobHistoryNonInit.entrySet().stream().forEach(
            entry -> entry.getValue().getDestinationEdition().metAJourLaListeDesConsultations(null));
        IntStream.rangeClosed(1, 23).forEach(indice ->
            assertEquals("Le JobHistory #" + indice + " ne devrait pas avoir modifié l'indicateur de nouveauté"
                , getJobHistory(indice).getDestinationEdition().getNouvelleEdition()
                , mapJobHistoryNonInit.get(indice).getDestinationEdition().getNouvelleEdition()));

        // Initialisation des jeux d'essai
        Map<Integer, JobHistory> mapJobHistoryVide = getJdd();
        
        // Appel de la méthode à tester sans résultat, et vérifications
        mapJobHistoryVide.entrySet().stream().forEach(
            entry -> entry.getValue().getDestinationEdition().metAJourLaListeDesConsultations(UUID_INCONNU));
        IntStream.rangeClosed(1, 23).forEach(indice ->
            assertEquals("Le JobHistory #" + indice + " ne devrait pas avoir modifié l'indicateur de nouveauté"
                , getJobHistory(indice).getDestinationEdition().getNouvelleEdition()
                , mapJobHistoryVide.get(indice).getDestinationEdition().getNouvelleEdition()));

        // Initialisation des jeux d'essai
        Map<Integer, JobHistory> mapJobHistoryUuid1 = getJdd();
        
        // Appel de la méthode à tester sur le propriétaire, avec résultat
        mapJobHistoryUuid1.entrySet().stream().forEach(
            entry -> entry.getValue().getDestinationEdition().metAJourLaListeDesConsultations(UUID_1));

        // Vérification de la nouveauté de l'édition
        IntStream.of(1, 6, 11, 12, 13, 16, 17, 18, 22, 23)
            .forEach(indice -> assertTrue("Le JobHistory #" + indice + " devrait être nouveau"
                , mapJobHistoryUuid1.get(indice).getDestinationEdition().getNouvelleEdition()));
        IntStream.of(2, 3, 4, 5, 7, 8, 9, 10, 14, 15, 19, 20, 21)
            .forEach(indice -> assertFalse("Le JobHistory #" + indice + " ne devrait pas être nouveau"
                , mapJobHistoryUuid1.get(indice).getDestinationEdition().getNouvelleEdition()));

        // Vérification des UUID consultés ou non
        IntStream.of(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 22, 23)
            .forEach(indice ->
                assertEquals("Le JobHistory #" +indice + " n'a pas le bon uidConsult"
                , new HashSet<>(Arrays.asList()), mapJobHistoryUuid1.get(indice).getDestinationEdition().getUidConsult()));
        IntStream.of(11, 12, 13, 14, 15, 21)
            .forEach(indice ->
            assertEquals("Le JobHistory #" +indice + " n'a pas le bon uidConsult"
                , new HashSet<>(Arrays.asList(new UidConsult(UUID_1)))
                , mapJobHistoryUuid1.get(indice).getDestinationEdition().getUidConsult()));
        IntStream.of(16, 17, 18, 19, 20)
            .forEach(indice ->
                assertEquals("Le JobHistory #" +indice + " n'a pas le bon uidConsult"
                , new HashSet<>(Arrays.asList(new UidConsult(UUID_1), new UidConsult(UUID_2)))
                , mapJobHistoryUuid1.get(indice).getDestinationEdition().getUidConsult()));
        
        // Initialisation des jeux d'essai
        Map<Integer, JobHistory> mapJobHistoryUuid2 = getJdd();

        // Appel de la méthode à tester sur un autre utilisateur, avec résultat
        mapJobHistoryUuid2.entrySet().stream().forEach(
            entry -> entry.getValue().getDestinationEdition().metAJourLaListeDesConsultations(UUID_2));

        // Vérification de la nouveauté de l'édition
        IntStream.of(1, 2, 3, 6, 7, 8, 11, 12, 13, 16, 17, 18, 22, 23)
            .forEach(indice -> assertTrue("Le JobHistory #" + indice + " devrait être nouveau"
                , mapJobHistoryUuid2.get(indice).getDestinationEdition().getNouvelleEdition()));
        IntStream.of(4, 5, 9, 10, 14, 15, 19, 20, 21)
            .forEach(indice -> assertFalse("Le JobHistory #" + indice + " ne devrait pas être nouveau"
                , mapJobHistoryUuid2.get(indice).getDestinationEdition().getNouvelleEdition()));

        // Vérification des UUID consultés ou non
        IntStream.of(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 22, 23)
            .forEach(indice ->
                assertEquals("Le JobHistory #" +indice + " n'a pas le bon uidConsult"
                , new HashSet<>(Arrays.asList()), mapJobHistoryUuid1.get(indice).getDestinationEdition().getUidConsult()));
        IntStream.of(11, 12, 13, 14, 15, 21)
            .forEach(indice ->
            assertEquals("Le JobHistory #" +indice + " n'a pas le bon uidConsult"
                , new HashSet<>(Arrays.asList(new UidConsult(UUID_1)))
                , mapJobHistoryUuid1.get(indice).getDestinationEdition().getUidConsult()));
        IntStream.of(16, 17, 18, 19, 20)
            .forEach(indice ->
                assertEquals("Le JobHistory #" +indice + " n'a pas le bon uidConsult"
                , new HashSet<>(Arrays.asList(new UidConsult(UUID_1), new UidConsult(UUID_2)))
                , mapJobHistoryUuid1.get(indice).getDestinationEdition().getUidConsult()));
    }

    /**
     * Méthode permettant de renvoyer un jeu d'essai d'historique de travaux d'édition sous forme de Map.
     *
     * @return le jeu d'essai sous forme de Map
     */
    private Map<Integer, JobHistory> getJdd()
    {
        return IntStream.rangeClosed(1, 23)
            .mapToObj(indice -> getJobHistory(indice)).collect(Collectors.toMap(
                jobHistory -> Integer.parseInt(jobHistory.getEditionUuid()), jobHistory -> jobHistory));
    }

    /**
     * Méthode permettant de récupérer un historique de travaux d'édition.
     *
     * @param indice UUID d'édition de l'historique de travaux d'édition
     * @return un historique de travaux d'édition
     */
    private JobHistory getJobHistory(int indice)
    {
        JobHistory jobHistory = new JobHistory();
        jobHistory.setEditionUuid(String.valueOf(indice));

        switch (indice)
        {
            case 1 :
                // Mono-destinaire (implicite), sans UID, nouvelle édition (implicite)
                break;
            case 2 :
                // Mono-destinaire (implicite), avec UID, nouvelle édition (implicite)
                jobHistory.getDestinationEdition().setUidProprietaire(UUID_1);
                break;
            case 3 :
                // Mono-destinaire (implicite), avec UID, nouvelle édition (explicite)
                jobHistory.getDestinationEdition().setUidProprietaire(UUID_1);
                jobHistory.getDestinationEdition().setNouvelleEdition(true);
                break;
            case 4 :
                // Mono-destinaire (implicite), sans UID, ancienne édition
                jobHistory.getDestinationEdition().setNouvelleEdition(false);
                break;
            case 5 :
                // Mono-destinaire (implicite), avec UID, ancienne édition
                jobHistory.getDestinationEdition().setUidProprietaire(UUID_1);
                jobHistory.getDestinationEdition().setNouvelleEdition(false);
                break;
            case 6 :
                // Mono-destinaire (explicite), sans UID, nouvelle édition (implicite)
                jobHistory.getDestinationEdition().setMonoDestinataire(true);
                break;
            case 7 :
                // Mono-destinaire (explicite), avec UID, nouvelle édition (implicite)
                jobHistory.getDestinationEdition().setUidProprietaire(UUID_1);
                jobHistory.getDestinationEdition().setMonoDestinataire(true);
                break;
            case 8 :
                // Mono-destinaire (explicite), avec UID, nouvelle édition (explicite)
                jobHistory.getDestinationEdition().setUidProprietaire(UUID_1);
                jobHistory.getDestinationEdition().setMonoDestinataire(true);
                jobHistory.getDestinationEdition().setNouvelleEdition(true);
                break;
            case 9 :
                // Mono-destinaire (explicite), sans UID, ancienne édition
                jobHistory.getDestinationEdition().setNouvelleEdition(false);
                jobHistory.getDestinationEdition().setMonoDestinataire(true);
                break;
            case 10 :
                // Mono-destinaire (explicite), avec UID, ancienne édition
                jobHistory.getDestinationEdition().setUidProprietaire(UUID_1);
                jobHistory.getDestinationEdition().setMonoDestinataire(true);
                jobHistory.getDestinationEdition().setNouvelleEdition(false);
                break;
            case 11 :
                // Multi-destinaires, sans UID, nouvelle édition (implicite)
                jobHistory.getDestinationEdition().setMonoDestinataire(false);
                break;
            case 12 :
                // Multi-destinaires, avec UID, nouvelle édition (implicite)
                jobHistory.getDestinationEdition().setMonoDestinataire(false);
                jobHistory.getDestinationEdition().setUidProprietaire(UUID_1);
                break;
            case 13 :
                // Multi-destinaires, avec UID, nouvelle édition (explicite)
                jobHistory.getDestinationEdition().setMonoDestinataire(false);
                jobHistory.getDestinationEdition().setUidProprietaire(UUID_1);
                jobHistory.getDestinationEdition().setNouvelleEdition(true);
                break;
            case 14 :
                // Multi-destinaires, sans UID, ancienne édition (explicite)
                jobHistory.getDestinationEdition().setMonoDestinataire(false);
                jobHistory.getDestinationEdition().setNouvelleEdition(false);
                break;
            case 15 :
                // Multi-destinaires, avec UID, ancienne édition (explicite)
                jobHistory.getDestinationEdition().setMonoDestinataire(false);
                jobHistory.getDestinationEdition().setUidProprietaire(UUID_1);
                jobHistory.getDestinationEdition().setNouvelleEdition(false);
                break;
            case 16 :
                // Multi-destinaires, sans UID, nouvelle édition (implicite), déjà consultée
                jobHistory.getDestinationEdition().setMonoDestinataire(false);
                jobHistory.getDestinationEdition().getUidConsult().add(new UidConsult(UUID_2));
                break;
            case 17 :
                // Multi-destinaires, avec UID, nouvelle édition (implicite)
                jobHistory.getDestinationEdition().setMonoDestinataire(false);
                jobHistory.getDestinationEdition().setUidProprietaire(UUID_1);
                jobHistory.getDestinationEdition().getUidConsult().add(new UidConsult(UUID_2));
                break;
            case 18 :
                // Multi-destinaires, avec UID, nouvelle édition (explicite)
                jobHistory.getDestinationEdition().setMonoDestinataire(false);
                jobHistory.getDestinationEdition().setUidProprietaire(UUID_1);
                jobHistory.getDestinationEdition().setNouvelleEdition(true);
                jobHistory.getDestinationEdition().getUidConsult().add(new UidConsult(UUID_2));
                break;
            case 19 :
                // Multi-destinaires, sans UID, ancienne édition (explicite)
                jobHistory.getDestinationEdition().setMonoDestinataire(false);
                jobHistory.getDestinationEdition().setNouvelleEdition(false);
                jobHistory.getDestinationEdition().getUidConsult().add(new UidConsult(UUID_2));
                break;
            case 20 :
                // Multi-destinaires, avec UID, ancienne édition (explicite)
                jobHistory.getDestinationEdition().setMonoDestinataire(false);
                jobHistory.getDestinationEdition().setUidProprietaire(UUID_1);
                jobHistory.getDestinationEdition().setNouvelleEdition(false);
                jobHistory.getDestinationEdition().getUidConsult().add(new UidConsult(UUID_2));
                break;
            case 21 :
                // Multi-destinaires, avec UID, ancienne édition (explicite)
                jobHistory.getDestinationEdition().setMonoDestinataire(false);
                jobHistory.getDestinationEdition().setUidProprietaire(UUID_1);
                jobHistory.getDestinationEdition().setNouvelleEdition(false);
                jobHistory.getDestinationEdition().setUidConsult(null);
                break;
            case 22 :
                // Mono-destinaire non initialisé
                jobHistory.getDestinationEdition().setMonoDestinataire(null);
                break;
            case 23 :
                // Nouvelle édition non initialisée
                jobHistory.getDestinationEdition().setNouvelleEdition(true);
                break;
        }

        return jobHistory;
    }
}
