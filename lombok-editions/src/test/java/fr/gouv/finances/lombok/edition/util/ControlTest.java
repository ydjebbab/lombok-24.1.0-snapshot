package fr.gouv.finances.lombok.edition.util;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.io.File;
import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import fr.gouv.finances.lombok.test.controle.VerificationExecution;
import fr.gouv.finances.lombok.upload.bean.FichierJoint;
import fr.gouv.finances.lombok.util.exception.ProgrammationException;
import fr.gouv.finances.lombok.util.exception.RegleGestionException;
import fr.gouv.finances.lombok.edition.bean.JobHistory;
import fr.gouv.finances.lombok.edition.bean.TrcPurgeEdition;

/**
 * Tests unitaires de l'utilitaire de contrôle d'édition.
 * 
 * @author Christophe Breheret-Girardin
 *
 */
public class ControlTest
{
    private static final String VALEUR = "kamoulox";

    @Mock
    File fichierIllisible = FileUtils.getFichierTemporaire("contenu temporaire");

    @Before
    public void before()
    {
        // Initialisation des bouchons
        MockitoAnnotations.initMocks(this);

        // Paramétrage du comportement du bouchon pour rendre un fichier non lisible
        when(fichierIllisible.canRead()).thenReturn(false);
    }
    
    /**
     * Test de fr.gouv.finances.lombok.edition.util.Control#assertdatabytesNotNull(byte[]).
     */
    @Test
    public void assertdatabytesNotNull()
    {
        // Cas de la non fourniture de valeur
        VerificationExecution.verifierException(ProgrammationException.class
            , () -> Control.assertdatabytesNotNull((byte[]) null));
        VerificationExecution.verifierException(ProgrammationException.class
            , () -> Control.assertdatabytesNotNull("".getBytes()));

        // Cas d'une valeur renseigné et vérification
        Control.assertdatabytesNotNull(VALEUR.getBytes());
    }

    /**
     * Test de fr.gouv.finances.lombok.edition.util.Control#assertdatabytesNotNull(byte[]).
     */
    @Test
    public void assertEditionDisponible()
    {
        VerificationExecution.verifierException(ProgrammationException.class
            , () -> Control.assertEditionDisponible(null));
        VerificationExecution.verifierException(RegleGestionException.class
            , () -> Control.assertEditionDisponible(new JobHistory()));
        
        JobHistory jobHistorySansOrdonnancement = new JobHistory();
        jobHistorySansOrdonnancement.setOrdoEdition(null);
        VerificationExecution.verifierException(RegleGestionException.class
            , () -> Control.assertEditionDisponible(jobHistorySansOrdonnancement));

        JobHistory jobHistoryEchec = new JobHistory();
        jobHistoryEchec.getOrdoEdition().setStatus(JobHistory.STATUS_ECHEC);
        VerificationExecution.verifierException(RegleGestionException.class
            , () -> Control.assertEditionDisponible(jobHistoryEchec));

        JobHistory jobHistoryDispoSansStockage = new JobHistory();
        jobHistoryDispoSansStockage.getOrdoEdition().setStatus(JobHistory.STATUS_DISPONIBLE);
        jobHistoryDispoSansStockage.setStockageEdition(null);
        VerificationExecution.verifierException(RegleGestionException.class
            , () -> Control.assertEditionDisponible(jobHistoryDispoSansStockage));
        
        JobHistory jobHistoryDispo = new JobHistory();
        jobHistoryDispo.getOrdoEdition().setStatus(JobHistory.STATUS_DISPONIBLE);
        VerificationExecution.verifierException(RegleGestionException.class
            , () -> Control.assertEditionDisponible(jobHistoryDispo));
        
        jobHistoryDispo.getStockageEdition().setTailleFichier(10L);
        Control.assertEditionDisponible(jobHistoryDispo);
        
        JobHistory jobHistoryDispoNonEnvoye = new JobHistory();
        jobHistoryDispoNonEnvoye.getOrdoEdition().setStatus(JobHistory.STATUS_DISPONIBLE_NON_ENVOYE);
        VerificationExecution.verifierException(RegleGestionException.class
            , () -> Control.assertEditionDisponible(jobHistoryDispoNonEnvoye));
        
        jobHistoryDispoNonEnvoye.getStockageEdition().setTailleFichier(10L);
        Control.assertEditionDisponible(jobHistoryDispoNonEnvoye);
    }

    /**
     * Test de fr.gouv.finances.lombok.edition.util.Control#assertFichierAccessibleEnLecture(java.io.File).
     */
    @Test
    public void assertFichierAccessibleEnLecture()
    {
        VerificationExecution.verifierException(ProgrammationException.class
            , () -> Control.assertFichierAccessibleEnLecture(null));

        VerificationExecution.verifierException(ProgrammationException.class
            , () -> Control.assertFichierAccessibleEnLecture(new File("cbre.txt")));

        VerificationExecution.verifierException(ProgrammationException.class
            , () -> Control.assertFichierAccessibleEnLecture(fichierIllisible));
        
        Control.assertFichierAccessibleEnLecture(FileUtils.getFichierTemporaire("contenu temporaire"));
    }
    
    /**
     * Test de fr.gouv.finances.lombok.edition.util.Control#assertFichierJointNotNull(
     * fr.gouv.finances.lombok.upload.bean.FichierJoint).
     */
    @Test
    public void assertFichierJointNotNull()
    {
        VerificationExecution.verifierException(ProgrammationException.class
            , () -> Control.assertFichierJointNotNull(null));

        Control.assertFichierJointNotNull(new FichierJoint());
    }
    
    /**
     * Test de fr.gouv.finances.lombok.edition.util.Control#assertJobHistoryNotNull().
     */
    @Test
    public void assertJobHistoryNotNull()
    {
        VerificationExecution.verifierException(ProgrammationException.class
            , () -> Control.assertJobHistoryNotNull(null));

        Control.assertJobHistoryNotNull(new JobHistory ());
    }

    /**
     * Test de fr.gouv.finances.lombok.edition.util.Control#assertListJobHistoryNotNull(java.util.List).
     */
    @Test
    public void assertListJobHistoryNotNull()
    {
        VerificationExecution.verifierException(ProgrammationException.class
            , () -> Control.assertListJobHistoryNotNull(null));

        Control.assertListJobHistoryNotNull(new ArrayList<>());
    }

    /**
     * Test de fr.gouv.finances.lombok.edition.util.Control#assertJobHistoryCharge(
     * fr.gouv.finances.lombok.edition.bean.JobHistory).
     */
    @Test
    public void assertJobHistoryCharge()
    {
        VerificationExecution.verifierException(ProgrammationException.class
            , () -> Control.assertJobHistoryCharge(new JobHistory()));

        JobHistory jobHistory = new JobHistory();
        jobHistory.setId(1L);
        Control.assertJobHistoryCharge(jobHistory);
    }

    /**
     * Test de fr.gouv.finances.lombok.edition.util.Control#assertJobHistoryCharge(java.lang.String).
     */
    @Test
    public void assertApplicationNonRenseignee()
    {
        VerificationExecution.verifierException(ProgrammationException.class
            , () -> Control.assertApplicationNonRenseignee(null));
        VerificationExecution.verifierException(ProgrammationException.class
            , () -> Control.assertApplicationNonRenseignee(""));

        Control.assertApplicationNonRenseignee(VALEUR);
    }

    /**
     * Test de fr.gouv.finances.lombok.edition.util.Control#assertIdNonRenseigne(java.lang.String).
     */
    @Test
    public void assertIdNonRenseigne()
    {
        VerificationExecution.verifierException(ProgrammationException.class
            , () -> Control.assertIdNonRenseigne(null));
        VerificationExecution.verifierException(ProgrammationException.class
            , () -> Control.assertIdNonRenseigne(""));

        Control.assertIdNonRenseigne(VALEUR);
    }

    /**
     * Test de fr.gouv.finances.lombok.edition.util.Control#assertUuidNonRenseigne(java.lang.String).
     */
    @Test
    public void assertUuidNonRenseigne()
    {
        VerificationExecution.verifierException(ProgrammationException.class
            , () -> Control.assertUuidNonRenseigne(null));
        VerificationExecution.verifierException(ProgrammationException.class
            , () -> Control.assertUuidNonRenseigne(""));

        Control.assertUuidNonRenseigne(VALEUR);
    }

    /**
     * Test de fr.gouv.finances.lombok.edition.util.Control#assertUtilisateurNonRenseigne(java.lang.String).
     */
    @Test
    public void assertUtilisateurNonRenseigne()
    {
        VerificationExecution.verifierException(ProgrammationException.class
            , () -> Control.assertUtilisateurNonRenseigne(null));
        VerificationExecution.verifierException(ProgrammationException.class
            , () -> Control.assertUtilisateurNonRenseigne(""));

        Control.assertUtilisateurNonRenseigne(VALEUR);
    }
    
    /**
     * Test de fr.gouv.finances.lombok.edition.util.Control#assertRefFichierTemporaireNotNull(
     * fr.gouv.finances.lombok.upload.bean.FichierJoint).
     */
    @Test
    public void assertRefFichierTemporaireNotNull()
    {
        VerificationExecution.verifierException(ProgrammationException.class
            , () -> Control.assertRefFichierTemporaireNotNull(null));
        
        FichierJoint fichierJointSansContenu = new FichierJoint();
        fichierJointSansContenu.setLeContenuDuFichier(null);
        VerificationExecution.verifierException(ProgrammationException.class
            , () -> Control.assertRefFichierTemporaireNotNull(fichierJointSansContenu));

        VerificationExecution.verifierException(ProgrammationException.class
            , () -> Control.assertRefFichierTemporaireNotNull(new FichierJoint()));
        
        FichierJoint fichierJoint = new FichierJoint();
        fichierJoint.getLeContenuDuFichier().setFichierTemporaire(FileUtils.getFichierTemporaire("contenu temporaire"));
        Control.assertRefFichierTemporaireNotNull(fichierJoint);
    }

    /**
     * Test de fr.gouv.finances.lombok.edition.util.Control#assertTrcPurgeEditionNotNull(
     * fr.gouv.finances.lombok.edition.bean.TrcPurgeEdition).
     */
    @Test
    public void assertTrcPurgeEditionNotNull()
    {
        VerificationExecution.verifierException(ProgrammationException.class
            , () -> Control.assertTrcPurgeEditionNotNull(null));

        Control.assertTrcPurgeEditionNotNull(new TrcPurgeEdition());
    }

    /**
     * Test de fr.gouv.finances.lombok.edition.util.Control#limiterLaLongeurDeLaChaine(
     * java.lang.String, int).
     */
    @Test
    public void limiterLaLongeurDeLaChaine()
    {
        assertNull(Control.limiterLaLongeurDeLaChaine(null, 0));
        assertNull(Control.limiterLaLongeurDeLaChaine(null, 10));
        assertEquals(VALEUR, Control.limiterLaLongeurDeLaChaine(VALEUR, 10));
        assertEquals("ka", Control.limiterLaLongeurDeLaChaine(VALEUR, 3));
    }
}