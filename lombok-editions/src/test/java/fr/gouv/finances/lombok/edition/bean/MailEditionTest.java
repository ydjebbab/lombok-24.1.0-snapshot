/*
 * Copyright (c) 2016 DGFiP - Tous droits réservés
 * 
 */
package fr.gouv.finances.lombok.edition.bean;

import fr.gouv.finances.lombok.test.pojo.AbstractCorePojoTest;

/**
 * Tests unitaires du POJO MailEdition.
 * 
 * @author Christophe Breheret-Girardin
 */
public class MailEditionTest extends AbstractCorePojoTest<MailEdition>
{   
    /**
     * Constructeur.
     */
    public MailEditionTest()
    {
        super();
    }

}
