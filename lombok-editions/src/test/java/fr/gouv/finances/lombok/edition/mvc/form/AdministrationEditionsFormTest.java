/*
 * Copyright (c) 2016 DGFiP - Tous droits réservés
 * 
 */
package fr.gouv.finances.lombok.edition.mvc.form;

import fr.gouv.finances.lombok.test.pojo.AbstractCorePojoTest;

/**
 * Tests unitaires du POJO AdministrationEditionsForm.
 *
 * @param AdministrationEditionsForm formulaire d'administration des éditions
 *
 * @author Christophe Breheret-Girardin
 */
public class AdministrationEditionsFormTest extends AbstractCorePojoTest<AdministrationEditionsForm>
{   
    /**
     * Constructeur.
     */
    public AdministrationEditionsFormTest()
    {
        super();
    }

}
