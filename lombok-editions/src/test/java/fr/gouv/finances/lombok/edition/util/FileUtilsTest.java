package fr.gouv.finances.lombok.edition.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.Test;

import fr.gouv.finances.lombok.test.controle.VerificationExecution;
import fr.gouv.finances.lombok.util.exception.IngerableException;
import fr.gouv.finances.lombok.util.exception.ProgrammationException;

/**
 * Tests unitaires de l'utilitaire de gestion des fichiers.
 * 
 * @author Christophe Breheret-Girardin
 *
 */
public class FileUtilsTest
{
    /** Valeur lambda à utiliser au sein des tests */
    private static final String VALEUR_1 = "kamoulox";

    /** 2ème valeur lambda à utiliser au sein des tests */
    private static final String VALEUR_2 = "42";

    /** Concaténation des valeurs lambda */
    private static final String VALEUR = VALEUR_1 + "\n" + VALEUR_2;

    /**
     * Test de fr.gouv.finances.lombok.edition.util#getFichier(byte[]).
     */
    @Test
    public void getFichier()
    {
        // Cas de la non fourniture de valeur
        VerificationExecution.verifierException(ProgrammationException.class
            , () -> FileUtils.getFichier((byte[]) null));

        // Cas d'une valeur vide et vérification
        File fichier = FileUtils.getFichier("".getBytes());
        assertTrue(fichier.exists());
        assertEquals(0, getContenu(fichier).size());
        
        // Cas d'une valeur sur 2 lignes et vérifications
        fichier = FileUtils.getFichier(VALEUR.getBytes());
        assertTrue(fichier.exists());
        List<String> infos = getContenu(fichier);
        assertEquals(2, infos.size());
        assertEquals(VALEUR_1, infos.get(0));
        assertEquals(VALEUR_2, infos.get(1));
    }

    /**
     * Test de fr.gouv.finances.lombok.edition.util#getFichier(java.io.File).
     */
    @Test
    public void getByteArray()
    {
        // Cas de la non fourniture de valeur
        VerificationExecution.verifierException(ProgrammationException.class
            , () -> FileUtils.getFichier((File) null));

        // Création d'un fichier à partir de données puis opération inverse, et vérification
        assertEquals(VALEUR, new String(FileUtils.getFichier(FileUtils.getFichier(VALEUR.getBytes()))));
    }

    /**
     * Test de fr.gouv.finances.lombok.edition.util#getFichierTemporaire().
     */
    @Test
    public void getFichierTemporaire()
    {
        // Création d'un fichier temporaire et vérification
        File fichier = FileUtils.getFichierTemporaire();
        assertTrue(fichier.exists());
    }

    /**
     * Test de fr.gouv.finances.lombok.edition.util#getFichierTemporaireRenseigne(java.lang.String).
     */
    @Test
    public void getFichierTemporaireRenseigne()
    {
        // Création d'un fichier temporaire et vérification
        File fichier = FileUtils.getFichierTemporaire(VALEUR);
        assertTrue(fichier.exists());
        List<String> infos = getContenu(fichier);
        assertEquals(2, infos.size());
        assertEquals(VALEUR_1, infos.get(0));
        assertEquals(VALEUR_2, infos.get(1));
    }

    /**
     * Récupération d'une liste de toutes les lignes du fichier
     *
     * @param fichier fichier à lire
     * @return les lignes du fichier
     */
    private List<String> getContenu(File fichier)
    {
        List<String> infos = new ArrayList<>();
        try (Stream<String> stream = Files.lines(Paths.get(fichier.getAbsolutePath()))) {
            infos = stream.collect(Collectors.toList());
        } catch (IOException ioe) {
            throw new IngerableException(ioe);
        }
        return infos;
    }
}