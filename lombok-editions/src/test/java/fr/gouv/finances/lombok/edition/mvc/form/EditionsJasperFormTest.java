/*
 * Copyright (c) 2016 DGFiP - Tous droits réservés
 * 
 */
package fr.gouv.finances.lombok.edition.mvc.form;

import fr.gouv.finances.lombok.test.pojo.AbstractCorePojoTest;

/**
 * Tests unitaires du POJO AdministrationEditionsForm.
 *
 * @param EditionsJasperForm formulaire lié aux éditions Jasper
 *
 * @author Christophe Breheret-Girardin
 */
public class EditionsJasperFormTest extends AbstractCorePojoTest<EditionsJasperForm>
{   
    /**
     * Constructeur.
     */
    public EditionsJasperFormTest()
    {
        super();
    }

}
