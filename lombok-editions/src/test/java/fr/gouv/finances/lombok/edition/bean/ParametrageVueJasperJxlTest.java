/*
 * Copyright (c) 2016 DGFiP - Tous droits réservés
 * 
 */
package fr.gouv.finances.lombok.edition.bean;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Map;

import org.junit.Test;

import fr.gouv.finances.lombok.test.pojo.AbstractCorePojoTest;

/**
 * Tests unitaires du POJO ParametrageVueJasperJxl.
 * 
 * @author Christophe Breheret-Girardin
 */
public class ParametrageVueJasperJxlTest extends AbstractCorePojoTest<ParametrageVueJasperJxl>
{   
    /**
     * Constructeur.
     */
    public ParametrageVueJasperJxlTest()
    {
        super();
    }

    /**
     * Test de fr.gouv.finances.lombok.edition.bean.ParametrageVueJasperJxl#parametrageJRExporter().
     */
    @Test
    public void parametrageJRExporter()
    {
        Map<String, Object> params = new ParametrageVueJasperJxl().parametrageJRExporter();
        assertEquals(6, params.size());
        assertNotNull(params.get(ParametrageVueJasperXls.CHARACTER_ENCODING));
        assertNotNull(params.get(ParametrageVueJasperXls.IS_ONE_PAGE_PER_SHEET));
        assertNotNull(params.get(ParametrageVueJasperXls.IS_DETECT_CELL_TYPE));
        assertNotNull(params.get(ParametrageVueJasperXls.IS_FONT_SIZE_FIX_ENABLED));
        assertNotNull(params.get(ParametrageVueJasperXls.IS_REM_EMPTY_SPACE_BET_ROWS));
        assertNotNull(params.get(ParametrageVueJasperXls.IS_WHITE_PAGE_BACKGROUND));
    }

    /**
     * Test de fr.gouv.finances.lombok.edition.bean.ParametrageVueJasperJxl#parametrerVue().
     */
    @Test
    public void parametrerVue()
    {
        Map<String, String> params = new ParametrageVueJasperJxl().parametrerVue();
        assertEquals(4, params.size());
        assertNotNull(params.get(ParametrageVueJasperJxl.REPORT_VIEW_KEY));
        assertNotNull(params.get(ParametrageVueJasperJxl.EXTENSION_KEY));
        assertNotNull(params.get(ParametrageVueJasperJxl.CONTENT_TYPE_KEY));
        assertNotNull(params.get(ParametrageVueJasperJxl.MIME_TYPE_KEY));
    }
}