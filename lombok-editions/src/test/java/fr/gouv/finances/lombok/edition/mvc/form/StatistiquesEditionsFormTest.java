/*
 * Copyright (c) 2016 DGFiP - Tous droits réservés
 * 
 */
package fr.gouv.finances.lombok.edition.mvc.form;

import fr.gouv.finances.lombok.test.pojo.AbstractCorePojoTest;

/**
 * Tests unitaires du POJO AdministrationEditionsForm.
 *
 * @param StatistiquesEditionsForm formulaire lié aux statistiques d'édition
 *
 * @author Christophe Breheret-Girardin
 */
public class StatistiquesEditionsFormTest extends AbstractCorePojoTest<StatistiquesEditionsForm>
{   
    /**
     * Constructeur.
     */
    public StatistiquesEditionsFormTest()
    {
        super();
    }

}
