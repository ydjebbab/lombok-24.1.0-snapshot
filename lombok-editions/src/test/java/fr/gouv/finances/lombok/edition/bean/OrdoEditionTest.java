/*
 * Copyright (c) 2016 DGFiP - Tous droits réservés
 * 
 */
package fr.gouv.finances.lombok.edition.bean;

import fr.gouv.finances.lombok.test.pojo.AbstractCorePojoTest;

/**
 * Tests unitaires du POJO OrdoEdition.
 * 
 * @author Christophe Breheret-Girardin
 */
public class OrdoEditionTest extends AbstractCorePojoTest<OrdoEdition>
{   
    /**
     * Constructeur.
     */
    public OrdoEditionTest()
    {
        super();
    }

}
