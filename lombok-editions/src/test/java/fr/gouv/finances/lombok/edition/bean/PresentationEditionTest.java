/*
 * Copyright (c) 2016 DGFiP - Tous droits réservés
 * 
 */
package fr.gouv.finances.lombok.edition.bean;

import fr.gouv.finances.lombok.test.pojo.AbstractCorePojoTest;

/**
 * Tests unitaires du POJO PresentationEdition.
 * 
 * @author Christophe Breheret-Girardin
 */
public class PresentationEditionTest extends AbstractCorePojoTest<PresentationEdition>
{   
    /**
     * Constructeur.
     */
    public PresentationEditionTest()
    {
        super();
    }

}
