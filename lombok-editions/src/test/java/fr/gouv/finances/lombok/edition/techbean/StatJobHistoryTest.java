/*
 * Copyright (c) 2016 DGFiP - Tous droits réservés
 * 
 */
package fr.gouv.finances.lombok.edition.techbean;

import fr.gouv.finances.lombok.test.pojo.AbstractCorePojoTest;

/**
 * Tests unitaires du POJO d'historique des travaux d'édition.
 * 
 * @author Christophe Breheret-Girardin
 *
 * @param StatJobHistory historique des travaux d'édition
 * @see fr.gouv.finances.lombok.edition.techbean.StatJobHistory
 */
public class StatJobHistoryTest extends AbstractCorePojoTest<StatJobHistory>
{   
    /**
     * Constructeur.
     */
    public StatJobHistoryTest()
    {
        super();
    }
}
