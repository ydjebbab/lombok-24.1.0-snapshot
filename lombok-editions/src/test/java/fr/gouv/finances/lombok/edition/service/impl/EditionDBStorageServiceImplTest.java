package fr.gouv.finances.lombok.edition.service.impl;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Date;
import java.util.UUID;

import org.hibernate.dialect.lock.OptimisticEntityLockException;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.orm.hibernate5.HibernateOptimisticLockingFailureException;

import fr.gouv.finances.lombok.edition.bean.ContenuEdition;
import fr.gouv.finances.lombok.edition.bean.JobHistory;
import fr.gouv.finances.lombok.edition.bean.OrdoEdition;
import fr.gouv.finances.lombok.edition.bean.PresentationEdition;
import fr.gouv.finances.lombok.edition.bean.StockageEdition;
import fr.gouv.finances.lombok.edition.dao.JobHistoryDao;
import fr.gouv.finances.lombok.upload.bean.ContenuFichier;
import fr.gouv.finances.lombok.upload.bean.FichierJoint;
import fr.gouv.finances.lombok.util.exception.ExploitationException;
import fr.gouv.finances.lombok.util.exception.ProgrammationException;
import fr.gouv.finances.lombok.util.exception.RegleGestionException;

public class EditionDBStorageServiceImplTest
{
    /** Initialisation de la journalisation. */
    protected static final Logger LOGGER = LoggerFactory.getLogger(EditionDBStorageServiceImplTest.class);

    /** Reprise de champs de même nom dans la classe EditionDBStorageServiceImpl */
    private static final int MAX_SIZE_FILE_IN_BYTE_ARRAY = 5 * 1024 * 1024;

    /** Constante contenu d'edition bidon */
    private static final byte[] BIG_DATA_DUMMY = new byte[MAX_SIZE_FILE_IN_BYTE_ARRAY + 1];
    private static final byte[] SMALL_DATA_DUMMY = new byte[MAX_SIZE_FILE_IN_BYTE_ARRAY - 1];

    private static enum TYPE_DATA {BIG, SMALL}; 

    /** Constante nom du fichier de l'édition */
    private static final String NOM_FICHIER_EDITION = "nom_fichier_edition";

    /** Constante nom du fichier de l'édition */
    private static final String TYPE_MIME_EDITION = "application/pdf";

    /** Constante nom du fichier de l'édition */
    private static final String UID_DUMMY = "207796";

    /** Constante nom du fichier de l'édition */
    private static final Date DATE_DEMANDE_EDITION = new Date();

    /**
     * Test de la méthode {@link EditionDBStorageServiceImpl#chargerFichierEdition(JobHistory)}
     * Gestion des exceptions
     */
    @Test
    public void testChargerFichierEditionJobHistory_Ano()
    {
        EditionDBStorageServiceImpl editionDBStorageService = new EditionDBStorageServiceImpl();
        JobHistoryDao jobHistoryDaoMock = mock(JobHistoryDao.class);
        editionDBStorageService.setJobhistorydao(jobHistoryDaoMock);
        
        // test : chargerFichierEdition(null) -> RegleGestionException
        try 
        {           
            editionDBStorageService.chargerFichierEdition(null);
            fail("Une exception ProgrammationException aurait du être levée");
        }
        catch (ProgrammationException e) 
        {
            LOGGER.debug("L'exception attendue a bien été levée");
        }

        // test : !jobHistory.isDisponible() -> RegleGestionException
        JobHistory jobHistorySpy = spy(JobHistory.class);
        when(jobHistorySpy.isDisponible()).thenReturn(false);
        try 
        {           
            editionDBStorageService.chargerFichierEdition(jobHistorySpy);
            fail("Une exception RegleGestionException aurait du être levée");
        }
        catch (RegleGestionException e) 
        {
            LOGGER.debug("L'exception attendue a bien été levée");
        }

        // test : jobHistory.getSockageEdition() == null -> RegleGestionException
        when(jobHistorySpy.isDisponible()).thenReturn(true);
        jobHistorySpy.setStockageEdition(null);
        try 
        {           
            editionDBStorageService.chargerFichierEdition(jobHistorySpy);
            fail("Une exception RegleGestionException aurait du être levée");
        }
        catch (RegleGestionException e) 
        {
            LOGGER.debug("L'exception attendue a bien été levée");
        }

        // test : jobHistory.getStockageEdition().getTailleFichier() == null -> RegleGestionException
        StockageEdition stockageEdition = new StockageEdition();
        stockageEdition.setTailleFichier(null);
        jobHistorySpy.setStockageEdition(stockageEdition);
        try 
        {           
            editionDBStorageService.chargerFichierEdition(jobHistorySpy);
            fail("Une exception RegleGestionException aurait du être levée");
        }
        catch (RegleGestionException e) 
        {
            LOGGER.debug("L'exception attendue a bien été levée");
        }

        // test : jobHistory.getStockageEdition().getTailleFichier() <= 0 -> RegleGestionException
        stockageEdition.setTailleFichier(-1L);
        try 
        {           
            editionDBStorageService.chargerFichierEdition(jobHistorySpy);
            fail("Une exception RegleGestionException aurait du être levée");
        }
        catch (RegleGestionException e) 
        {
            LOGGER.debug("L'exception attendue a bien été levée");
        }
        
        // test : jobHistory.getStockageEdition().getLeContenuEdition() == null 0 -> ProgrammationException
        stockageEdition.setTailleFichier(Long.valueOf(SMALL_DATA_DUMMY.length));
        stockageEdition.setLeContenuEdition(null);
        doNothing().when(jobHistoryDaoMock).chargerContenuEditionToBytes(any(JobHistory.class));
        try 
        {           
            editionDBStorageService.chargerFichierEdition(jobHistorySpy);
            fail("Une exception ProgrammationException aurait du être levée");
        }
        catch (ProgrammationException e) 
        {
            LOGGER.debug("L'exception attendue a bien été levée");
        }        
        
        // test : jobHistory.getStockageEdition().getLeContenuEdition() == null 0 -> ProgrammationException
        stockageEdition.setTailleFichier(Long.valueOf(BIG_DATA_DUMMY.length));
        try 
        {           
            editionDBStorageService.chargerFichierEdition(jobHistorySpy);
            fail("Une exception ProgrammationException aurait du être levée");
        }
        catch (ProgrammationException e) 
        {
            LOGGER.debug("L'exception attendue a bien été levée");
        }
    }

    /**
     * Test de la méthode {@link EditionDBStorageServiceImpl#chargerFichierEdition(JobHistory)}
     * avec data dans un byte[]
     *
     * @throws IOException
     */
    @Test
    public void testChargerFichierEditionJobHistory_BytesArray() throws IOException
    {             
        // cas nominal lorsque taille de fichier < MAX_SIZE_FILE_IN_BYTE_ARRAY :
        // on charge l'édition dans un tableau de byte
        EditionDBStorageServiceImpl editionDBStorageService = spy(EditionDBStorageServiceImpl.class); 
        JobHistory jobHistory = prepareJobHistoryPourTestChargerFichierEditionJobHistoryString(TYPE_DATA.SMALL);
        JobHistoryDao jobHistoryDaoMock = mock(JobHistoryDao.class);
        editionDBStorageService.setJobhistorydao(jobHistoryDaoMock);

        // Mock de JobHistoryDao.chargerContenuEditionToBytes car on ne teste pas la couche persistance
        doNothing().when(jobHistoryDaoMock).chargerContenuEditionToBytes(any(JobHistory.class));       
        editionDBStorageService.setJobhistorydao(jobHistoryDaoMock);

        // appel de la méthode testée 
        FichierJoint result = editionDBStorageService.chargerFichierEdition(jobHistory);

        // verifier que jobhistorydao.chargerContenuEditionToBytes a été appelée une et une seule fois
        verify(jobHistoryDaoMock, times(1)).chargerContenuEditionToBytes(jobHistory);

        // vérification : chargerFichierEdition ne renvoie pas null
        assertNotNull("chargeFichierEdition renvoie null", result);

        // vérification result est correctement rempli
        assertEquals("Nom de fichier édition non attendu", NOM_FICHIER_EDITION, result.getNomFichierOriginal());
        assertEquals("Type MIME édition non attendu", TYPE_MIME_EDITION, result.getTypeMimeFichier());
        assertEquals("Date demande édition non attendu", DATE_DEMANDE_EDITION, result.getDateHeureSoumission());
        assertArrayEquals("Data édition non attendu", SMALL_DATA_DUMMY, result.getLeContenuDuFichier().getData());
        assertEquals("Taille data édition non attendu", SMALL_DATA_DUMMY.length, result.getTailleFichier());
    }

    /**
     * Test de la méthode {@link EditionDBStorageServiceImpl#chargerFichierEdition(JobHistory)}
     * avec data dans un File
     * 
     * @throws IOException
     */
    @Test
    public void testChargerFichierEditionJobHistory_ToFile()
        throws IOException
    {             
        // cas nominal lorsque taille de fichier > MAX_SIZE_FILE_IN_BYTE_ARRAY :
        // on charge l'édition dans un fichier temporaire        
        EditionDBStorageServiceImpl editionDBStorageService = spy(EditionDBStorageServiceImpl.class); 
        JobHistory jobHistory = prepareJobHistoryPourTestChargerFichierEditionJobHistoryString(TYPE_DATA.BIG);
        JobHistoryDao jobHistoryDaoMock = mock(JobHistoryDao.class);
        editionDBStorageService.setJobhistorydao(jobHistoryDaoMock);

        // Mock de JobHistoryDao.chargerContenuEditionToBytes car on ne teste pas la couche persistance
        doNothing().when(jobHistoryDaoMock).chargerContenuEditionToBytes(any(JobHistory.class));       
        editionDBStorageService.setJobhistorydao(jobHistoryDaoMock);

        // appel de la méthode testée 
        FichierJoint result = editionDBStorageService.chargerFichierEdition(jobHistory);

        // verifier que jobhistorydao.chargerContenuEditionToBytes a été appelée une et une seule fois
        verify(jobHistoryDaoMock, times(1)).chargerContenuEditionToFile(jobHistory);

        // vérification : chargerFichierEdition ne renvoie pas null
        assertNotNull("chargeFichierEdition renvoie null", result);

        // vérification result est correctement rempli
        assertEquals("Nom de fichier édition non attendu", NOM_FICHIER_EDITION, result.getNomFichierOriginal());
        assertEquals("Type MIME édition non attendu", TYPE_MIME_EDITION, result.getTypeMimeFichier());
        assertEquals("Date demande édition non attendu", DATE_DEMANDE_EDITION, result.getDateHeureSoumission());
        byte[] resultData = Files.readAllBytes(result.getLeContenuDuFichier().getFichierTemporaire().toPath());
        assertArrayEquals("Data édition non attendu", BIG_DATA_DUMMY, resultData);
        assertEquals("Taille data édition non attendu", BIG_DATA_DUMMY.length, result.getTailleFichier());
    }

    /**
     * Test de la méthode {@link EditionDBStorageServiceImpl#chargerFichierEdition(JobHistory, String)}
     * Gestion des exceptions
     *
     * @throws IOException
     * @throws InterruptedException 
     */
    @Test
    public void testChargerFichierEditionJobHistoryString_Ano() throws IOException, InterruptedException
    {        
        EditionDBStorageServiceImpl editionDBStorageServiceSpy = spy(EditionDBStorageServiceImpl.class); 
        JobHistory jobHistory = prepareJobHistoryPourTestChargerFichierEditionJobHistoryString(TYPE_DATA.SMALL);
        JobHistoryDao jobHistoryDaoMock = mock(JobHistoryDao.class);
        editionDBStorageServiceSpy.setJobhistorydao(jobHistoryDaoMock);

        // test du comportement lorsqu'on simule un échec lors de la recharge du jobHistory en base
        // à partir de son uuid 
        doReturn(null).when(jobHistoryDaoMock).findJobHistory(anyString());                
        try 
        {           
            editionDBStorageServiceSpy.chargerFichierEdition(jobHistory, UID_DUMMY);
            fail("Une exception ProgrammationException aurait du être levée");
        }
        catch (ProgrammationException e) 
        {
            LOGGER.debug("L'exception attendue a bien été levée");
        }

        // test du comportement lorsqu'on simule un échec au chargement de l'édition en base
        doReturn(jobHistory).when(jobHistoryDaoMock).findJobHistory(anyString());
        doReturn(null).when(editionDBStorageServiceSpy).chargerFichierEdition(any(JobHistory.class));
        jobHistory.setEditionUuid(UUID.randomUUID().toString());
        try
        {
            // appel de la méthode testée         
            editionDBStorageServiceSpy.chargerFichierEdition(jobHistory, UID_DUMMY);
            fail("Une exception ExploitationException aurait du être levée");            
        }
        catch (ExploitationException e) 
        {
            LOGGER.debug("L'exception attendue a bien été levée");
        }

        // test du comportement lorsqu'on simule une exception lors de l'enregistrement de jobHistory
        // ceci permet également de tester un nombre max de tentative de chargement de fichiers (or quand on a un échec de chargement,
        // on lève une exception donc on ne fait qu'une itération de toute façon 
        doThrow(new HibernateOptimisticLockingFailureException(new OptimisticEntityLockException(jobHistory, "test")))
            .when(jobHistoryDaoMock).saveJobHistory(jobHistory);
        jobHistory.setEditionUuid(UUID.randomUUID().toString());
        try
        {
            // appel de la méthode testée         
            editionDBStorageServiceSpy.chargerFichierEdition(jobHistory, UID_DUMMY);
            fail("Une exception ProgrammationException aurait du être levée");
        }
        catch (ProgrammationException e) 
        {
            LOGGER.debug("L'exception attendue a bien été levée");
        }       
        
        // Test InterruptedException dans EditionDBServiceImpl.attendre()
        Thread.currentThread().interrupt();
        try
        {
            // appel de la méthode testée         
            editionDBStorageServiceSpy.chargerFichierEdition(jobHistory, UID_DUMMY);
            fail("Une exception ProgrammationException aurait du être levée");
        }
        catch (ProgrammationException e) 
        {
            LOGGER.debug("L'exception attendue a bien été levée");
        }          
    }

    /**
     * Test de la méthode {@link EditionDBStorageServiceImpl#chargerFichierEdition(JobHistory, String)}
     *
     * @throws IOException
     */
    @Test
    public void testChargerFichierEditionJobHistoryString() throws IOException
    {
        EditionDBStorageServiceImpl editionDBStorageService = new EditionDBStorageServiceImpl(); 
        JobHistory jobHistory = prepareJobHistoryPourTestChargerFichierEditionJobHistoryString(TYPE_DATA.SMALL);
        JobHistoryDao jobHistoryDaoMock = mock(JobHistoryDao.class);
        editionDBStorageService.setJobhistorydao(jobHistoryDaoMock);
        
        // on mock findJobHistory pour ne pas utiliser la couche persistance
        doReturn(jobHistory).when(jobHistoryDaoMock).findJobHistory(anyString());

        // appel de la méthode testée         
        FichierJoint result = editionDBStorageService.chargerFichierEdition(jobHistory, UUID.randomUUID().toString());

        // verifier que jobhistorydao.chargerContenuEditionToBytes a été appelée une et une seule fois
        verify(jobHistoryDaoMock, times(1)).chargerContenuEditionToBytes(jobHistory);

        // vérification : chargerFichierEdition ne renvoie pas null
        assertNotNull("chargeFichierEdition renvoie null", result);

        // vérification result est correctement rempli
        assertEquals("Nom de fichier édition non attendu", NOM_FICHIER_EDITION, result.getNomFichierOriginal());
        assertEquals("Type MIME édition non attendu", TYPE_MIME_EDITION, result.getTypeMimeFichier());
        assertEquals("Date demande édition non attendu", DATE_DEMANDE_EDITION, result.getDateHeureSoumission());
        assertArrayEquals("Data édition non attendu", SMALL_DATA_DUMMY, result.getLeContenuDuFichier().getData());
        assertEquals("Taille data édition non attendu", SMALL_DATA_DUMMY.length, result.getTailleFichier());
    }

    /**
     * Test de la méthode {@link EditionDBStorageServiceImpl#effacerFichierEdition(JobHistory)}
     * Ne fait rien
     */
    @Test
    public void testEffacerFichierEdition()
    {
        EditionDBStorageServiceImpl editionDBStorageService = new EditionDBStorageServiceImpl();
        editionDBStorageService.effacerFichierEdition(new JobHistory());
    }

    /**
     * Test de la méthode {@link EditionDBStorageServiceImpl#stockerEdition(JobHistory, FichierJoint)}
     * Gestion des exceptions
     */
    @Test
    public void testStockerEdition_Ano()
    {
        FichierJoint fichierJoint = new FichierJoint();
        JobHistory jobHistory = new JobHistory();
        EditionDBStorageServiceImpl editionDBStorageService = new EditionDBStorageServiceImpl();
        ContenuFichier contenuFichier = new ContenuFichier();

        // test : jobHistory null
        try
        {
            editionDBStorageService.stockerEdition(null, fichierJoint);
            fail("Une exception ProgrammationException aurait du être levée");
        }
        catch (ProgrammationException e) 
        {
            LOGGER.debug("L'exception attendue a bien été levée");
        }

        // test : fichierJoint null
        try
        {
            editionDBStorageService.stockerEdition(jobHistory, null);
            fail("Une exception ProgrammationException aurait du être levée");
        }
        catch (ProgrammationException e) 
        {
            LOGGER.debug("L'exception attendue a bien été levée");
        }

        // test : data dans data byte[], data de contenuFichier de fichierJoint null
        fichierJoint.setDataInByteArray(true);
        contenuFichier.setData(null);
        fichierJoint.setLeContenuDuFichier(contenuFichier);
        try
        {
            editionDBStorageService.stockerEdition(jobHistory, fichierJoint);
            fail("Une exception ProgrammationException aurait du être levée");
        }
        catch (ProgrammationException e) 
        {
            LOGGER.debug("L'exception attendue a bien été levée");
        }

        // test : data dans fichierTemporaire, contenuFichier de fichierJoint null
        fichierJoint.setDataInByteArray(false);
        fichierJoint.setLeContenuDuFichier(null);
        try
        {
            editionDBStorageService.stockerEdition(jobHistory, fichierJoint);
            fail("Une exception ProgrammationException aurait du être levée");
        }
        catch (ProgrammationException e) 
        {
            LOGGER.debug("L'exception attendue a bien été levée");
        }

        // test : data dans fichierTemporaire, fichierTemporaire de contenuFichier de fichierJoint null
        fichierJoint.setDataInByteArray(false);
        contenuFichier.setFichierTemporaire(null);
        fichierJoint.setLeContenuDuFichier(null);
        try
        {
            editionDBStorageService.stockerEdition(jobHistory, fichierJoint);
            fail("Une exception ProgrammationException aurait du être levée");
        }
        catch (ProgrammationException e) 
        {
            LOGGER.debug("L'exception attendue a bien été levée");
        }
    }
    
    /**
     * Test de la méthode {@link EditionDBStorageServiceImpl#stockerEdition(JobHistory, FichierJoint)}
     * Cas avec un byte[]
     */
    @Test
    public void testStockerEdition_BytesArray()
    {
        FichierJoint fichierJoint = new FichierJoint(SMALL_DATA_DUMMY);        
        JobHistory jobHistory = new JobHistory();
        JobHistoryDao jobHistoryDaoMock = mock(JobHistoryDao.class);
        EditionDBStorageServiceImpl editionDBStorageService = new EditionDBStorageServiceImpl();
        
        // préparation des objets 
        editionDBStorageService.setJobhistorydao(jobHistoryDaoMock);
        
        // mock du dao pour ne pas le tester
        doNothing().when(jobHistoryDaoMock)
            .saveContenuEditionFromByteArray(jobHistory, fichierJoint.getLeContenuDuFichier().getData());
        
        // appel de la méthode testée
        editionDBStorageService.stockerEdition(jobHistory, fichierJoint);
        
        // vérification qu'on a appelé la bonne méthode de la dao 
        verify(jobHistoryDaoMock, times(1)).saveContenuEditionFromByteArray(jobHistory, 
            fichierJoint.getLeContenuDuFichier().getData());
        
        // vérification des modifications sur jobHistory
        assertEquals("Taille de fichier non attendu", fichierJoint.getTailleFichier(), 
            jobHistory.getStockageEdition().getTailleFichier().longValue());
        assertNotNull("contenuEdition est null", jobHistory.getStockageEdition().getLeContenuEdition());
    }

    /**
     * Test de la méthode {@link EditionDBStorageServiceImpl#stockerEdition(JobHistory, FichierJoint)}
     * Cas avec un File
     * 
     * @throws IOException
     */
    @Test
    public void testStockerEdition_File() throws IOException
    {
        FichierJoint fichierJoint = new FichierJoint(createTempFileFromBigData());        
        JobHistory jobHistory = new JobHistory();
        JobHistoryDao jobHistoryDaoMock = mock(JobHistoryDao.class);
        EditionDBStorageServiceImpl editionDBStorageService = new EditionDBStorageServiceImpl();
        
        // préparation des objets 
        editionDBStorageService.setJobhistorydao(jobHistoryDaoMock);
        
        // mock du dao pour ne pas le tester
        doNothing().when(jobHistoryDaoMock)
            .saveContenuEditionFromFile(jobHistory, 
                fichierJoint.getLeContenuDuFichier().getFichierTemporaire());
        
        // appel de la méthode testée
        editionDBStorageService.stockerEdition(jobHistory, fichierJoint);
        
        // vérification qu'on a appelé la bonne méthode de la dao 
        verify(jobHistoryDaoMock, times(1))
            .saveContenuEditionFromFile(jobHistory, 
                fichierJoint.getLeContenuDuFichier().getFichierTemporaire());
        
        // vérification des modifications sur jobHistory
        assertEquals("Taille de fichier non attendu", fichierJoint.getTailleFichier(), 
            jobHistory.getStockageEdition().getTailleFichier().longValue());
        assertNotNull("contenuEdition est null", jobHistory.getStockageEdition().getLeContenuEdition());
    }
    
    /**
     * Test de la méthode {@link EditionDBStorageServiceImpl#synchroniseHistoriqueAvecSf(int, String)}
     * Cas avec un byte[]
     */
    @Test
    public void testSynchroniseHistoriqueAvecSf()
    {
        EditionDBStorageServiceImpl editionDBStorageService = new EditionDBStorageServiceImpl();
        editionDBStorageService.synchroniseHistoriqueAvecSf(0, "");
    }

    /**
     * Test de la méthode {@link EditionDBStorageServiceImpl#synchroniseSfAvecHistorique(String)}
     * Cas avec un byte[]
     */
    @Test
    public void testSynchroniseSfAvecHistorique()
    {
        EditionDBStorageServiceImpl editionDBStorageService = new EditionDBStorageServiceImpl();
        editionDBStorageService.synchroniseSfAvecHistorique("");
    }

    /**
     * Prépare un objet JobHistory pour les tests.
     *
     * @param typeData : BIG pour les datas en File ou SMALL pour les datas en byte[]
     * @return objet JobHistory préparé pour les tests
     * @throws IOException
     */
    private JobHistory prepareJobHistoryPourTestChargerFichierEditionJobHistoryString(TYPE_DATA typeData) throws IOException
    {
        JobHistory jobHistory = new JobHistory();
        OrdoEdition ordoEdition = new OrdoEdition();
        StockageEdition stockageEdition = new StockageEdition();
        PresentationEdition presentationEdition = new PresentationEdition();
        ContenuEdition contenuEdition = new ContenuEdition();

        // jobHistory
        jobHistory.setEditionUuid(UUID.randomUUID().toString());

        // présentationEdition
        presentationEdition.setNomFicDownload(NOM_FICHIER_EDITION);
        presentationEdition.setTypeMime(TYPE_MIME_EDITION);
        jobHistory.setPresentationEdition(presentationEdition);

        // ordoEdition
        ordoEdition.setDateDemande(DATE_DEMANDE_EDITION);
        ordoEdition.setStatus(JobHistory.STATUS_DISPONIBLE);
        jobHistory.setOrdoEdition(ordoEdition);

        // contenuEdition
        if (typeData == TYPE_DATA.SMALL)
        {
            contenuEdition.setDataByte(SMALL_DATA_DUMMY);
            stockageEdition.setLeContenuEdition(contenuEdition);
            stockageEdition.setTailleFichier(new Long(SMALL_DATA_DUMMY.length));
            jobHistory.setStockageEdition(stockageEdition);
        }
        else 
        {           
            contenuEdition.setDataFile(createTempFileFromBigData());
            stockageEdition.setLeContenuEdition(contenuEdition);
            stockageEdition.setTailleFichier(contenuEdition.getDataFile().length());
            jobHistory.setStockageEdition(stockageEdition);
        }

        return jobHistory;
    }
    
    /**
     * Crée un fichier temporaire  
     *
     * @return fichier temporaire
     * @throws IOException
     */
    private File createTempFileFromBigData() throws IOException
    {
        File fichierTemp = File.createTempFile("JUNIT_TEST", "TXT");
        try (FileOutputStream fos = new FileOutputStream(fichierTemp))
        {
            fos.write(BIG_DATA_DUMMY);
        }
        catch (IOException e)
        {
            throw e;
        }
        fichierTemp.deleteOnExit();
        
        return fichierTemp;
    }
}
