/*
 * Copyright (c) 2016 DGFiP - Tous droits réservés
 * 
 */
package fr.gouv.finances.lombok.edition.bean;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Map;

import org.junit.Test;

import fr.gouv.finances.lombok.test.pojo.AbstractCorePojoTest;

/**
 * Tests unitaires du POJO ParametrageVueJasperCsv.
 * 
 * @author Christophe Breheret-Girardin
 */
public class ParametrageVueJasperCsvTest extends AbstractCorePojoTest<ParametrageVueJasperCsv>
{   
    /**
     * Constructeur.
     */
    public ParametrageVueJasperCsvTest()
    {
        super();
    }

    /**
     * Test de fr.gouv.finances.lombok.edition.bean.ParametrageVueJasperCsv#parametrageJRExporter().
     */
    @Test
    public void parametrageJRExporter()
    {
        Map<String, Object> params = new ParametrageVueJasperCsv().parametrageJRExporter();
        assertEquals(2, params.size());
        assertNotNull(params.get(ParametrageVueJasperCsv.CHARACTER_ENCODING));
    }
    
    /**
     * Test de fr.gouv.finances.lombok.edition.bean.ParametrageVueJasperCsv#parametrerVue().
     */
    @Test
    public void parametrerVue()
    {
        Map<String, String> params = new ParametrageVueJasperCsv().parametrerVue();
        assertEquals(4, params.size());
        assertNotNull(params.get(ParametrageVueJasperRtf.REPORT_VIEW_KEY));
        assertNotNull(params.get(ParametrageVueJasperRtf.EXTENSION_KEY));
        assertNotNull(params.get(ParametrageVueJasperRtf.CONTENT_TYPE_KEY));
        assertNotNull(params.get(ParametrageVueJasperRtf.MIME_TYPE_KEY));
    }
}