/*
 * Copyright (c) 2016 DGFiP - Tous droits réservés
 * 
 */
package fr.gouv.finances.lombok.edition.bean;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;
import java.time.Month;

import org.junit.Test;

import fr.gouv.finances.lombok.test.controle.VerificationExecution;
import fr.gouv.finances.lombok.util.exception.ProgrammationException;

/**
 * Tests unitaires des utilitaires du POJO StockageEdition.
 * 
 * @author Christophe Breheret-Girardin
 */
public class StockageEditionUtilitaireTest
{   
    /**
     * Constructeur.
     */
    public StockageEditionUtilitaireTest()
    {
        super();
    }

    /**
     * Test de fr.gouv.finances.lombok.edition.bean.StockageEdition#calculeRepertoireStockageN6(
     * java.util.Date, java.lang.String, java.lang.String, java.lang.String)
     */
    @Test
    public void calculeRepertoireStockageN6()
    {
        // Contrôle des paramètres non renseignés
        VerificationExecution.verifierException(ProgrammationException.class
            , () -> StockageEdition.calculeRepertoireStockageN6(null, null, null, null));

        // Contrôle avec certains paramètres non renseignés
        LocalDate lodalDate = LocalDate.of(2018, Month.FEBRUARY, 14);
        assertEquals("null/null/null/2018/02/14"
            , StockageEdition.calculeRepertoireStockageN6(java.sql.Date.valueOf(lodalDate), null, null, null));
        assertEquals("null/a/null/2018/02/14"
            , StockageEdition.calculeRepertoireStockageN6(java.sql.Date.valueOf(lodalDate), "a", null, null));
        assertEquals("null/null/b/2018/02/14"
            , StockageEdition.calculeRepertoireStockageN6(java.sql.Date.valueOf(lodalDate), null, "b", null));
        assertEquals("c/null/null/2018/02/14"
            , StockageEdition.calculeRepertoireStockageN6(java.sql.Date.valueOf(lodalDate), null, null, "c"));

        VerificationExecution.verifierException(ProgrammationException.class
            , () -> StockageEdition.calculeRepertoireStockageN6(null, "a", "b", "c"));

        // Contrôle avec tous les paramètres renseignés
        assertEquals("c/a/b/2018/02/14"
            , StockageEdition.calculeRepertoireStockageN6(java.sql.Date.valueOf(lodalDate), "a", "b", "c"));
    }

    /**
     * Test de fr.gouv.finances.lombok.edition.bean.StockageEdition#calculeRepertoireStockageN7(
     * java.util.Date, java.lang.String, java.lang.String, java.lang.Integer, java.lang.String)
     */
    @Test
    public void calculeRepertoireStockageN7()
    {
        // Contrôle des paramètres non renseignés
        VerificationExecution.verifierException(ProgrammationException.class
            , () -> StockageEdition.calculeRepertoireStockageN7(null, null, null, null, null));

        // Contrôle avec certains paramètres non renseignés
        LocalDate lodalDate = LocalDate.of(2018, Month.FEBRUARY, 14);
        assertEquals("null/null/null/2018/02/14"
            , StockageEdition.calculeRepertoireStockageN7(
                java.sql.Date.valueOf(lodalDate), null, null, null, null));
        assertEquals("null/a/null/2018/02/14"
            , StockageEdition.calculeRepertoireStockageN7(
                java.sql.Date.valueOf(lodalDate), "a", null, null, null));
        assertEquals("null/null/b/2018/02/14"
            , StockageEdition.calculeRepertoireStockageN7(
                java.sql.Date.valueOf(lodalDate), null, "b", null, null));
        assertEquals("c/null/null/2018/02/14"
            , StockageEdition.calculeRepertoireStockageN7(
                java.sql.Date.valueOf(lodalDate), null, null, null, "c"));
        assertEquals("null/null/null/2018/02/14"
            , StockageEdition.calculeRepertoireStockageN7(
                java.sql.Date.valueOf(lodalDate), null, null, 1, null));
        VerificationExecution.verifierException(ProgrammationException.class
            , () -> StockageEdition.calculeRepertoireStockageN7(null, "a", "b", 1, "c"));

        // Contrôle avec tous les paramètres renseignés
        assertEquals("c/a/b/2018/02/14"
            , StockageEdition.calculeRepertoireStockageN7(java.sql.Date.valueOf(lodalDate), "a", "b", null, "c"));
        assertEquals("c/a/b/2018/02/14"
            , StockageEdition.calculeRepertoireStockageN7(java.sql.Date.valueOf(lodalDate), "a", "b", 1, "c"));
    }
    
    /**
     * Test de fr.gouv.finances.lombok.edition.bean.StockageEdition#repertoireLot(java.lang.Integer)
     */
    @Test
    public void repertoireLot()
    {
        // Contrôle du paramètre non renseigné
        VerificationExecution.verifierException(ProgrammationException.class, () -> StockageEdition.repertoireLot(null));

        // Contrôle du paramètre renseigné
        assertEquals("LOT.1", StockageEdition.repertoireLot(1));
    }

    /**
     * Test de fr.gouv.finances.lombok.edition.bean.StockageEdition#fournirCheminAbsoluFichier()
     */
    @Test
    public void fournirCheminAbsoluFichier()
    {
        StockageEdition stockageEdition = new StockageEdition();

        // Contrôle des paramètres non renseignés
        assertEquals("null/null", stockageEdition.fournirCheminAbsoluFichier());
        stockageEdition.setCheminComplet("a");
        assertEquals("a/null", stockageEdition.fournirCheminAbsoluFichier());
        stockageEdition.setCheminComplet(null);
        stockageEdition.setNomFicStockage("b");
        assertEquals("null/b", stockageEdition.fournirCheminAbsoluFichier());

        // Contrôle des paramètres renseignés
        stockageEdition.setCheminComplet("a");
        stockageEdition.setNomFicStockage("b");
        assertEquals("a/b", stockageEdition.fournirCheminAbsoluFichier());
    }
}