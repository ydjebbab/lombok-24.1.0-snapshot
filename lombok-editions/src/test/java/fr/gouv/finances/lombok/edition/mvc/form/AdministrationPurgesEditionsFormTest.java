/*
 * Copyright (c) 2016 DGFiP - Tous droits réservés
 * 
 */
package fr.gouv.finances.lombok.edition.mvc.form;

import fr.gouv.finances.lombok.test.pojo.AbstractCorePojoTest;

/**
 * Tests unitaires du POJO AdministrationEditionsForm.
 *
 * @param  AdministrationPurgesEditionsForm formulaire lié à l'administration des purges d'édition
 *
 * @author Christophe Breheret-Girardin
 */
public class AdministrationPurgesEditionsFormTest extends AbstractCorePojoTest< AdministrationPurgesEditionsForm>
{   
    /**
     * Constructeur.
     */
    public AdministrationPurgesEditionsFormTest()
    {
        super();
    }

}
