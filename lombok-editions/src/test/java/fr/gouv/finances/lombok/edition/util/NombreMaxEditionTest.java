/*
 * Copyright (c) 2016 DGFiP - Tous droits réservés
 * 
 */
package fr.gouv.finances.lombok.edition.util;

import fr.gouv.finances.lombok.test.pojo.AbstractCorePojoTest;

/**
 * Tests unitaires du POJO NombreMaxEdition.
 * 
 * @author Christophe Breheret-Girardin
 *
 * @param NombreMaxEdition énumération sur les nombres maximum d'édition à récupérer
 * @see fr.gouv.finances.lombok.edition.util.NombreMaxEdition
 */
public class NombreMaxEditionTest extends AbstractCorePojoTest<NombreMaxEdition>
{
    /**
     * Constructeur.
     */
    public NombreMaxEditionTest()
    {
        super();
    }
}
