package fr.gouv.finances.lombok.edition.dao;

import fr.gouv.finances.lombok.test.pojo.AbstractCorePojoTest;

/**
 * Tests unitaires de l'énumération ProprieteTrcPurgeEdition.
 * @see fr.gouv.finances.lombok.edition.dao.ProprieteTrcPurgeEdition
 *
 * @author Christophe Breheret-Girardin
 */
public class ProprieteTrcPurgeEditionTest extends AbstractCorePojoTest<ProprieteTrcPurgeEdition>
{
    /**
     * Constructeur.
     */
    public ProprieteTrcPurgeEditionTest()
    {
        super();
    }
}
