/*
 * Copyright (c) 2016 DGFiP - Tous droits réservés
 * 
 */
package fr.gouv.finances.lombok.edition.bean;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Map;

import org.junit.Test;

import fr.gouv.finances.lombok.test.pojo.AbstractCorePojoTest;

/**
 * Tests unitaires du POJO ParametrageVueJasperOds.
 * 
 * @author Christophe Breheret-Girardin
 */
public class ParametrageVueJasperOdsTest extends AbstractCorePojoTest<ParametrageVueJasperOds>
{   
    /**
     * Constructeur.
     */
    public ParametrageVueJasperOdsTest()
    {
        super();
    }

    /**
     * Test de fr.gouv.finances.lombok.edition.bean.ParametrageVueJasperOds#parametrageJRExporter().
     */
    @Test
    public void parametrageJRExporter()
    {
        Map<String, Object> params = new ParametrageVueJasperOds().parametrageJRExporter();
        assertEquals(1, params.size());
        assertNotNull(params.get(ParametrageVueJasperOds.CHARACTER_ENCODING));
    }

    /**
     * Test de fr.gouv.finances.lombok.edition.bean.ParametrageVueJasperOds#parametrerVue().
     */
    @Test
    public void parametrerVue()
    {
        Map<String, String> params = new ParametrageVueJasperOds().parametrerVue();
        assertEquals(4, params.size());
        assertNotNull(params.get(ParametrageVueJasperOds.REPORT_VIEW_KEY));
        assertNotNull(params.get(ParametrageVueJasperOds.EXTENSION_KEY));
        assertNotNull(params.get(ParametrageVueJasperOds.CONTENT_TYPE_KEY));
        assertNotNull(params.get(ParametrageVueJasperOds.MIME_TYPE_KEY));
    }
}