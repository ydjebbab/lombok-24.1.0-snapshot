/*
 * Copyright (c) 2011 DGFiP - Tous droits réservés
 */
package fr.gouv.impots.appli.itmtgenmas.services.offresservices;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import fr.gouv.impots.appli.itmtgenmas.services.offresservices.objetsvaleurs.service.commun.infosinterface.InformationsInterfaceValeur;

/**
 * <p>
 * Java class for obtenirInformationsResponse complex type.
 * <p>
 * The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="obtenirInformationsResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="return" type="{http://services.itmtgenmas.appli.impots.gouv.fr/offresServices/objetsValeurs/service/commun/infosinterface}informationsInterfaceValeur" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "obtenirInformationsResponse", propOrder = {
        "_return"
})
public class ObtenirInformationsResponse
{

    /** _return. */
    @XmlElement(name = "return")
    protected InformationsInterfaceValeur _return;

    /**
     * Constructeur de la classe ObtenirInformationsResponse.java
     *
     */
    public ObtenirInformationsResponse()
    {
        super();   
        
    }

    /**
     * Gets the value of the return property.
     * 
     * @return possible object is {@link InformationsInterfaceValeur }
     */
    public InformationsInterfaceValeur getReturn()
    {
        return _return;
    }

    /**
     * Sets the value of the return property.
     * 
     * @param value allowed object is {@link InformationsInterfaceValeur }
     */
    public void setReturn(InformationsInterfaceValeur value)
    {
        this._return = value;
    }

}
