/*
 * Copyright (c) 2011 DGFiP - Tous droits réservés
 */
package fr.gouv.impots.appli.itmtgenmas.services.offresservices;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for TechDysfonctionnementErreur complex type.
 * <p>
 * The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TechDysfonctionnementErreur">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="code" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="descriptif" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *         &lt;element name="libelle" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="message" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="severite" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TechDysfonctionnementErreur", propOrder = {
        "code",
        "descriptif",
        "id",
        "libelle",
        "message",
        "severite"
})
public class TechDysfonctionnementErreur
{

    /** code. */
    protected Integer code;

    /** descriptif. */
    protected String descriptif;

    /** id. */
    protected byte[] id;

    /** libelle. */
    protected String libelle;

    /** message. */
    protected String message;

    /** severite. */
    protected Integer severite;

    /**
     * Gets the value of the code property.
     * 
     * @return possible object is {@link Integer }
     */
    public Integer getCode()
    {
        return code;
    }

    /**
     * Gets the value of the descriptif property.
     * 
     * @return possible object is {@link String }
     */
    public String getDescriptif()
    {
        return descriptif;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return possible object is byte[]
     */
    public byte[] getId()
    {
        return id;
    }

    /**
     * Gets the value of the libelle property.
     * 
     * @return possible object is {@link String }
     */
    public String getLibelle()
    {
        return libelle;
    }

    /**
     * Gets the value of the message property.
     * 
     * @return possible object is {@link String }
     */
    public String getMessage()
    {
        return message;
    }

    /**
     * Gets the value of the severite property.
     * 
     * @return possible object is {@link Integer }
     */
    public Integer getSeverite()
    {
        return severite;
    }

    /**
     * Sets the value of the code property.
     * 
     * @param value allowed object is {@link Integer }
     */
    public void setCode(Integer value)
    {
        this.code = value;
    }

    /**
     * Sets the value of the descriptif property.
     * 
     * @param value allowed object is {@link String }
     */
    public void setDescriptif(String value)
    {
        this.descriptif = value;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value allowed object is byte[]
     */
    public void setId(byte[] value)
    {
        this.id = ((byte[]) value);
    }

    /**
     * Sets the value of the libelle property.
     * 
     * @param value allowed object is {@link String }
     */
    public void setLibelle(String value)
    {
        this.libelle = value;
    }

    /**
     * Sets the value of the message property.
     * 
     * @param value allowed object is {@link String }
     */
    public void setMessage(String value)
    {
        this.message = value;
    }

    /**
     * Sets the value of the severite property.
     * 
     * @param value allowed object is {@link Integer }
     */
    public void setSeverite(Integer value)
    {
        this.severite = value;
    }

}
