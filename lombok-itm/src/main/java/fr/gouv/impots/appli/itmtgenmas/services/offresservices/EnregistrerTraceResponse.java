/*
 * Copyright (c) 2011 DGFiP - Tous droits réservés
 */
package fr.gouv.impots.appli.itmtgenmas.services.offresservices;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

import fr.gouv.impots.appli.itmtgenmas.services.offresservices.objetsvaleurs.service.enregistrementtraces.reponse.ErreurReponseSrvValeur;

/**
 * <p>
 * Java class for enregistrerTraceResponse complex type.
 * <p>
 * The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="enregistrerTraceResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="result" type="{http://services.itmtgenmas.appli.impots.gouv.fr/offresServices/objetsValeurs/service/enregistrementtraces/reponse}ErreurReponseSrvValeur" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "enregistrerTraceResponse", propOrder = {
        "result"
})
public class EnregistrerTraceResponse
{

    /** result. */
    protected ErreurReponseSrvValeur result;

    /**
     * Constructeur de la classe EnregistrerTraceResponse.java
     *
     */
    public EnregistrerTraceResponse()
    {
        super(); 
    }

    /**
     * Gets the value of the result property.
     * 
     * @return possible object is {@link ErreurReponseSrvValeur }
     */
    public ErreurReponseSrvValeur getResult()
    {
        return result;
    }

    /**
     * Sets the value of the result property.
     * 
     * @param value allowed object is {@link ErreurReponseSrvValeur }
     */
    public void setResult(ErreurReponseSrvValeur value)
    {
        this.result = value;
    }

}
