/*
 * Copyright (c) 2011 DGFiP - Tous droits réservés
 */
package fr.gouv.impots.appli.itmtgenmas.services.offresservices;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;

import fr.gouv.impots.appli.itmtgenmas.services.offresservices.objetsvaleurs.service.enregistrementtraces.requete.NtcSRequeteSrvValeur;

/**
 * <p>
 * Java class for enregistrerTraces complex type.
 * <p>
 * The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="enregistrerTraces">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="arrayOfbyte_1" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *         &lt;element name="arrayOfNtcSRequeteSrvValeur_2" type="{http://services.itmtgenmas.appli.impots.gouv.fr/offresServices/objetsValeurs/service/enregistrementtraces/requete}NtcSRequeteSrvValeur" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "enregistrerTraces", propOrder = {
        "arrayOfbyte1",
        "arrayOfNtcSRequeteSrvValeur2"
})
public class EnregistrerTraces
{

    /** array ofbyte1. */
    @XmlElementRef(name = "arrayOfbyte_1", type = JAXBElement.class)
    protected JAXBElement<byte[]> arrayOfbyte1;

    /** array of ntc s requete srv valeur2. */
    @XmlElement(name = "arrayOfNtcSRequeteSrvValeur_2", nillable = true)
    protected List<NtcSRequeteSrvValeur> arrayOfNtcSRequeteSrvValeur2;

    public EnregistrerTraces()
    {
        super();           
    }

    /**
     * Gets the value of the arrayOfbyte1 property.
     * 
     * @return possible object is {@link JAXBElement }
     */
    public JAXBElement<byte[]> getArrayOfbyte1()
    {
        return arrayOfbyte1;
    }

    /**
     * Gets the value of the arrayOfNtcSRequeteSrvValeur2 property.
     * <p>
     * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to
     * the returned list will be present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for
     * the arrayOfNtcSRequeteSrvValeur2 property.
     * <p>
     * For example, to add a new item, do as follows:
     * 
     * <pre>
     * getArrayOfNtcSRequeteSrvValeur2().add(newItem);
     * </pre>
     * <p>
     * Objects of the following type(s) are allowed in the list {@link NtcSRequeteSrvValeur }
     * 
     * @return array of ntc s requete srv valeur2
     */
    public List<NtcSRequeteSrvValeur> getArrayOfNtcSRequeteSrvValeur2()
    {
        if (arrayOfNtcSRequeteSrvValeur2 == null)
        {
            arrayOfNtcSRequeteSrvValeur2 = new ArrayList<>();
        }
        return this.arrayOfNtcSRequeteSrvValeur2;
    }

    /**
     * Sets the value of the arrayOfbyte1 property.
     * 
     * @param value allowed object is {@link JAXBElement }
     */
    public void setArrayOfbyte1(JAXBElement<byte[]> value)
    {
        this.arrayOfbyte1 = ((JAXBElement<byte[]>) value);
    }

}
