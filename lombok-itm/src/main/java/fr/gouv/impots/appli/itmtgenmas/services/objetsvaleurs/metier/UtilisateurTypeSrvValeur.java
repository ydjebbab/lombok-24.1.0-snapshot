/*
 * Copyright (c) 2011 DGFiP - Tous droits réservés
 */
package fr.gouv.impots.appli.itmtgenmas.services.objetsvaleurs.metier;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for UtilisateurTypeSrvValeur complex type.
 * <p>
 * The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="UtilisateurTypeSrvValeur">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="nom" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="valeur" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UtilisateurTypeSrvValeur", propOrder = {
        "nom",
        "valeur"
})
public class UtilisateurTypeSrvValeur
{

    /** nom. */
    protected String nom;

    /** valeur. */
    protected String valeur;

    /**
     * Constructeur de la classe UtilisateurTypeSrvValeur.java
     *
     */
    public UtilisateurTypeSrvValeur()
    {
        super();  
        
    }

    /**
     * Gets the value of the nom property.
     * 
     * @return possible object is {@link String }
     */
    public String getNom()
    {
        return nom;
    }

    /**
     * Gets the value of the valeur property.
     * 
     * @return possible object is {@link String }
     */
    public String getValeur()
    {
        return valeur;
    }

    /**
     * Sets the value of the nom property.
     * 
     * @param value allowed object is {@link String }
     */
    public void setNom(String value)
    {
        this.nom = value;
    }

    /**
     * Sets the value of the valeur property.
     * 
     * @param value allowed object is {@link String }
     */
    public void setValeur(String value)
    {
        this.valeur = value;
    }

}
