/*
 * Copyright (c) 2011 DGFiP - Tous droits réservés
 */
package fr.gouv.impots.appli.itmtgenmas.services.offresservices;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import fr.gouv.impots.appli.itmtgenmas.services.offresservices.objetsvaleurs.service.enregistrementtraces.reponse.ErreurReponseSrvValeur;

/**
 * <p>
 * Java class for enregistrerTracesResponse complex type.
 * <p>
 * The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="enregistrerTracesResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="result" type="{http://services.itmtgenmas.appli.impots.gouv.fr/offresServices/objetsValeurs/service/enregistrementtraces/reponse}ErreurReponseSrvValeur" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "enregistrerTracesResponse", propOrder = {
        "result"
})
public class EnregistrerTracesResponse
{

    /** result. */
    @XmlElement(nillable = true)
    protected List<ErreurReponseSrvValeur> result;

    /**
     * Constructeur de la classe EnregistrerTracesResponse.java
     *
     */
    public EnregistrerTracesResponse()
    {
        super();  
        
    }

    /**
     * Gets the value of the result property.
     * <p>
     * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to
     * the returned list will be present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for
     * the result property.
     * <p>
     * For example, to add a new item, do as follows:
     * 
     * <pre>
     * getResult().add(newItem);
     * </pre>
     * <p>
     * Objects of the following type(s) are allowed in the list {@link ErreurReponseSrvValeur }
     * 
     * @return result
     */
    public List<ErreurReponseSrvValeur> getResult()
    {
        if (result == null)
        {
            result = new ArrayList<ErreurReponseSrvValeur>();
        }
        return this.result;
    }

}
