/*
 * Copyright (c) 2011 DGFiP - Tous droits réservés
 */
package fr.gouv.impots.appli.itmtgenmas.services.offresservices;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;

import fr.gouv.impots.appli.itmtgenmas.services.offresservices.objetsvaleurs.service.enregistrementtraces.requete.NtcSRequeteSrvValeur;

/**
 * <p>
 * Java class for enregistrerTrace complex type.
 * <p>
 * The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="enregistrerTrace">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="arrayOfbyte_1" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *         &lt;element name="NtcSRequeteSrvValeur_2" type="{http://services.itmtgenmas.appli.impots.gouv.fr/offresServices/objetsValeurs/service/enregistrementtraces/requete}NtcSRequeteSrvValeur" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "enregistrerTrace", propOrder = {
        "arrayOfbyte1",
        "ntcSRequeteSrvValeur2"
})
public class EnregistrerTrace
{

    /** array ofbyte1. */
    @XmlElementRef(name = "arrayOfbyte_1", type = JAXBElement.class)
    protected JAXBElement<byte[]> arrayOfbyte1;

    /** ntc s requete srv valeur2. */
    @XmlElement(name = "NtcSRequeteSrvValeur_2")
    protected NtcSRequeteSrvValeur ntcSRequeteSrvValeur2;

    /**
     * Constructeur de la classe EnregistrerTrace.java
     *
     */
    public EnregistrerTrace()
    {
        super();   
        
    }

    /**
     * Gets the value of the arrayOfbyte1 property.
     * 
     * @return possible object is {@link JAXBElement }
     */
    public JAXBElement<byte[]> getArrayOfbyte1()
    {
        return arrayOfbyte1;
    }

    /**
     * Gets the value of the ntcSRequeteSrvValeur2 property.
     * 
     * @return possible object is {@link NtcSRequeteSrvValeur }
     */
    public NtcSRequeteSrvValeur getNtcSRequeteSrvValeur2()
    {
        return ntcSRequeteSrvValeur2;
    }

    /**
     * Sets the value of the arrayOfbyte1 property.
     * 
     * @param value allowed object is {@link JAXBElement }
     */
    public void setArrayOfbyte1(JAXBElement<byte[]> value)
    {
        this.arrayOfbyte1 = ((JAXBElement<byte[]>) value);
    }

    /**
     * Sets the value of the ntcSRequeteSrvValeur2 property.
     * 
     * @param value allowed object is {@link NtcSRequeteSrvValeur }
     */
    public void setNtcSRequeteSrvValeur2(NtcSRequeteSrvValeur value)
    {
        this.ntcSRequeteSrvValeur2 = value;
    }

}
