/*
 * Copyright (c) 2011 DGFiP - Tous droits réservés
 */
package fr.gouv.impots.appli.itmtgenmas.services.offresservices;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;

/**
 * This object contains factory methods for each Java content interface and Java element interface generated in the
 * fr.gouv.impots.appli.itmtgenmas.services.offresservices package.
 * <p>
 * An ObjectFactory allows you to programatically construct new instances of the Java representation for XML content.
 * The Java representation of XML content can consist of schema derived interfaces and classes representing the binding
 * of schema type definitions, element declarations and model groups. Factory methods for each of these are provided in
 * this class.
 */
@XmlRegistry
public class ObjectFactory
{

    /** Constant : _EnregistrerTracesArrayOfbyte1_QNAME. */
    private static final QName _EnregistrerTracesArrayOfbyte1_QNAME = new QName("", "arrayOfbyte_1");

    /** Constant : _EnregistrerTracesResponse_QNAME. */
    private static final QName _EnregistrerTracesResponse_QNAME = new QName(
        "http://services.itmtgenmas.appli.impots.gouv.fr/offresServices", "enregistrerTracesResponse");

    /** Constant : _TechIndisponibiliteErreur_QNAME. */
    private static final QName _TechIndisponibiliteErreur_QNAME = new QName(
        "http://services.itmtgenmas.appli.impots.gouv.fr/offresServices", "TechIndisponibiliteErreur");

    /** Constant : _ObtenirInformations_QNAME. */
    private static final QName _ObtenirInformations_QNAME = new QName("http://services.itmtgenmas.appli.impots.gouv.fr/offresServices",
        "obtenirInformations");

    /** Constant : _TechDysfonctionnementErreur_QNAME. */
    private static final QName _TechDysfonctionnementErreur_QNAME = new QName(
        "http://services.itmtgenmas.appli.impots.gouv.fr/offresServices", "TechDysfonctionnementErreur");

    /** Constant : _EnregistrerTraceResponse_QNAME. */
    private static final QName _EnregistrerTraceResponse_QNAME = new QName(
        "http://services.itmtgenmas.appli.impots.gouv.fr/offresServices", "enregistrerTraceResponse");

    /** Constant : _ObtenirInformationsResponse_QNAME. */
    private static final QName _ObtenirInformationsResponse_QNAME = new QName(
        "http://services.itmtgenmas.appli.impots.gouv.fr/offresServices", "obtenirInformationsResponse");

    /** Constant : _EnregistrerTraces_QNAME. */
    private static final QName _EnregistrerTraces_QNAME = new QName("http://services.itmtgenmas.appli.impots.gouv.fr/offresServices",
        "enregistrerTraces");

    /** Constant : _TechProtocolaireErreur_QNAME. */
    private static final QName _TechProtocolaireErreur_QNAME = new QName("http://services.itmtgenmas.appli.impots.gouv.fr/offresServices",
        "TechProtocolaireErreur");

    /** Constant : _EnregistrerTrace_QNAME. */
    private static final QName _EnregistrerTrace_QNAME = new QName("http://services.itmtgenmas.appli.impots.gouv.fr/offresServices",
        "enregistrerTrace");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package:
     * fr.gouv.impots.appli.itmtgenmas.services.offresservices
     */
    public ObjectFactory()
    {
    }

    /**
     * Create an instance of {@link EnregistrerTrace }.
     * 
     * @return the enregistrer trace
     */
    public EnregistrerTrace createEnregistrerTrace()
    {
        return new EnregistrerTrace();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EnregistrerTrace }{@code >}.
     * 
     * @param value
     * @return the JAXB element< enregistrer trace>
     */
    @XmlElementDecl(namespace = "http://services.itmtgenmas.appli.impots.gouv.fr/offresServices", name = "enregistrerTrace")
    public JAXBElement<EnregistrerTrace> createEnregistrerTrace(EnregistrerTrace value)
    {
        return new JAXBElement<EnregistrerTrace>(_EnregistrerTrace_QNAME, EnregistrerTrace.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }
     * 
     * @param value
     * @return the JAXB element<byte[]>
     */
    @XmlElementDecl(namespace = "", name = "arrayOfbyte_1", scope = EnregistrerTrace.class)
    public JAXBElement<byte[]> createEnregistrerTraceArrayOfbyte1(byte[] value)
    {
        return new JAXBElement<byte[]>(_EnregistrerTracesArrayOfbyte1_QNAME, byte[].class, EnregistrerTrace.class, ((byte[]) value));
    }

    /**
     * Create an instance of {@link EnregistrerTraceResponse }.
     * 
     * @return the enregistrer trace response
     */
    public EnregistrerTraceResponse createEnregistrerTraceResponse()
    {
        return new EnregistrerTraceResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EnregistrerTraceResponse }{@code >}.
     * 
     * @param value
     * @return the JAXB element< enregistrer trace response>
     */
    @XmlElementDecl(namespace = "http://services.itmtgenmas.appli.impots.gouv.fr/offresServices", name = "enregistrerTraceResponse")
    public JAXBElement<EnregistrerTraceResponse> createEnregistrerTraceResponse(EnregistrerTraceResponse value)
    {
        return new JAXBElement<EnregistrerTraceResponse>(_EnregistrerTraceResponse_QNAME, EnregistrerTraceResponse.class, null, value);
    }

    /**
     * Create an instance of {@link EnregistrerTraces }.
     * 
     * @return the enregistrer traces
     */
    public EnregistrerTraces createEnregistrerTraces()
    {
        return new EnregistrerTraces();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EnregistrerTraces }{@code >}.
     * 
     * @param value
     * @return the JAXB element< enregistrer traces>
     */
    @XmlElementDecl(namespace = "http://services.itmtgenmas.appli.impots.gouv.fr/offresServices", name = "enregistrerTraces")
    public JAXBElement<EnregistrerTraces> createEnregistrerTraces(EnregistrerTraces value)
    {
        return new JAXBElement<EnregistrerTraces>(_EnregistrerTraces_QNAME, EnregistrerTraces.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }
     * 
     * @param value
     * @return the JAXB element<byte[]>
     */
    @XmlElementDecl(namespace = "", name = "arrayOfbyte_1", scope = EnregistrerTraces.class)
    public JAXBElement<byte[]> createEnregistrerTracesArrayOfbyte1(byte[] value)
    {
        return new JAXBElement<byte[]>(_EnregistrerTracesArrayOfbyte1_QNAME, byte[].class, EnregistrerTraces.class, ((byte[]) value));
    }

    /**
     * Create an instance of {@link EnregistrerTracesResponse }.
     * 
     * @return the enregistrer traces response
     */
    public EnregistrerTracesResponse createEnregistrerTracesResponse()
    {
        return new EnregistrerTracesResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EnregistrerTracesResponse }{@code >}.
     * 
     * @param value
     * @return the JAXB element< enregistrer traces response>
     */
    @XmlElementDecl(namespace = "http://services.itmtgenmas.appli.impots.gouv.fr/offresServices", name = "enregistrerTracesResponse")
    public JAXBElement<EnregistrerTracesResponse> createEnregistrerTracesResponse(EnregistrerTracesResponse value)
    {
        return new JAXBElement<EnregistrerTracesResponse>(_EnregistrerTracesResponse_QNAME, EnregistrerTracesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link Erreur }.
     * 
     * @return the erreur
     */
    public Erreur createErreur()
    {
        return new Erreur();
    }

    /**
     * Create an instance of {@link ObtenirInformations }.
     * 
     * @return the obtenir informations
     */
    public ObtenirInformations createObtenirInformations()
    {
        return new ObtenirInformations();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObtenirInformations }{@code >}.
     * 
     * @param value
     * @return the JAXB element< obtenir informations>
     */
    @XmlElementDecl(namespace = "http://services.itmtgenmas.appli.impots.gouv.fr/offresServices", name = "obtenirInformations")
    public JAXBElement<ObtenirInformations> createObtenirInformations(ObtenirInformations value)
    {
        return new JAXBElement<ObtenirInformations>(_ObtenirInformations_QNAME, ObtenirInformations.class, null, value);
    }

    /**
     * Create an instance of {@link ObtenirInformationsResponse }.
     * 
     * @return the obtenir informations response
     */
    public ObtenirInformationsResponse createObtenirInformationsResponse()
    {
        return new ObtenirInformationsResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObtenirInformationsResponse }{@code >}.
     * 
     * @param value
     * @return the JAXB element< obtenir informations response>
     */
    @XmlElementDecl(namespace = "http://services.itmtgenmas.appli.impots.gouv.fr/offresServices", name = "obtenirInformationsResponse")
    public JAXBElement<ObtenirInformationsResponse> createObtenirInformationsResponse(ObtenirInformationsResponse value)
    {
        return new JAXBElement<ObtenirInformationsResponse>(_ObtenirInformationsResponse_QNAME, ObtenirInformationsResponse.class, null,
            value);
    }

    /**
     * Create an instance of {@link TechDysfonctionnementErreur }.
     * 
     * @return the tech dysfonctionnement erreur
     */
    public TechDysfonctionnementErreur createTechDysfonctionnementErreur()
    {
        return new TechDysfonctionnementErreur();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TechDysfonctionnementErreur }{@code >}.
     * 
     * @param value
     * @return the JAXB element< tech dysfonctionnement erreur>
     */
    @XmlElementDecl(namespace = "http://services.itmtgenmas.appli.impots.gouv.fr/offresServices", name = "TechDysfonctionnementErreur")
    public JAXBElement<TechDysfonctionnementErreur> createTechDysfonctionnementErreur(TechDysfonctionnementErreur value)
    {
        return new JAXBElement<TechDysfonctionnementErreur>(_TechDysfonctionnementErreur_QNAME, TechDysfonctionnementErreur.class, null,
            value);
    }

    /**
     * Create an instance of {@link TechIndisponibiliteErreur }.
     * 
     * @return the tech indisponibilite erreur
     */
    public TechIndisponibiliteErreur createTechIndisponibiliteErreur()
    {
        return new TechIndisponibiliteErreur();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TechIndisponibiliteErreur }{@code >}.
     * 
     * @param value
     * @return the JAXB element< tech indisponibilite erreur>
     */
    @XmlElementDecl(namespace = "http://services.itmtgenmas.appli.impots.gouv.fr/offresServices", name = "TechIndisponibiliteErreur")
    public JAXBElement<TechIndisponibiliteErreur> createTechIndisponibiliteErreur(TechIndisponibiliteErreur value)
    {
        return new JAXBElement<TechIndisponibiliteErreur>(_TechIndisponibiliteErreur_QNAME, TechIndisponibiliteErreur.class, null, value);
    }

    /**
     * Create an instance of {@link TechProtocolaireErreur }.
     * 
     * @return the tech protocolaire erreur
     */
    public TechProtocolaireErreur createTechProtocolaireErreur()
    {
        return new TechProtocolaireErreur();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TechProtocolaireErreur }{@code >}.
     * 
     * @param value
     * @return the JAXB element< tech protocolaire erreur>
     */
    @XmlElementDecl(namespace = "http://services.itmtgenmas.appli.impots.gouv.fr/offresServices", name = "TechProtocolaireErreur")
    public JAXBElement<TechProtocolaireErreur> createTechProtocolaireErreur(TechProtocolaireErreur value)
    {
        return new JAXBElement<TechProtocolaireErreur>(_TechProtocolaireErreur_QNAME, TechProtocolaireErreur.class, null, value);
    }

}
