/*
 * Copyright (c) 2011 DGFiP - Tous droits réservés
 */
package fr.gouv.impots.appli.itmtgenmas.services.offresservices.objetsvaleurs.service.enregistrementtraces.requete;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;

import fr.gouv.impots.appli.itmtgenmas.services.objetsvaleurs.metier.UtilisateurTypeSrvValeur;
import fr.gouv.impots.appli.itmtgenmas.services.objetsvaleurs.metier.enregistrementtraces.ExploitationSrvValeur;

/**
 * <p>
 * Java class for NtcSRequeteSrvValeur complex type.
 * <p>
 * The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="NtcSRequeteSrvValeur">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="emetteur" type="{http://services.itmtgenmas.appli.impots.gouv.fr/objetsvaleurs/metier}UtilisateurTypeSrvValeur" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="exploitation" type="{http://services.itmtgenmas.appli.impots.gouv.fr/objetsvaleurs/metier/enregistrementtraces}ExploitationSrvValeur" minOccurs="0"/>
 *         &lt;element name="horodatage" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="metier" type="{http://services.itmtgenmas.appli.impots.gouv.fr/objetsvaleurs/metier}UtilisateurTypeSrvValeur" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="typeTrace" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="typeTraceInitiateur" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "NtcSRequeteSrvValeur", propOrder = {
        "emetteur",
        "exploitation",
        "horodatage",
        "metier",
        "typeTrace",
        "typeTraceInitiateur"
})
public class NtcSRequeteSrvValeur
{

    /** emetteur. */
    @XmlElement(nillable = true)
    protected List<UtilisateurTypeSrvValeur> emetteur;

    /** exploitation. */
    protected ExploitationSrvValeur exploitation;

    /** horodatage. */
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar horodatage;

    /** metier. */
    @XmlElement(nillable = true)
    protected List<UtilisateurTypeSrvValeur> metier;

    /** type trace. */
    protected String typeTrace;

    /** type trace initiateur. */
    protected String typeTraceInitiateur;

    /**
     * Constructeur de la classe NtcSRequeteSrvValeur.java
     *
     */
    public NtcSRequeteSrvValeur()
    {
        super(); 
        
    }

    /**
     * Gets the value of the emetteur property.
     * <p>
     * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to
     * the returned list will be present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for
     * the emetteur property.
     * <p>
     * For example, to add a new item, do as follows:
     * 
     * <pre>
     * getEmetteur().add(newItem);
     * </pre>
     * <p>
     * Objects of the following type(s) are allowed in the list {@link UtilisateurTypeSrvValeur }
     * 
     * @return emetteur
     */
    public List<UtilisateurTypeSrvValeur> getEmetteur()
    {
        if (emetteur == null)
        {
            emetteur = new ArrayList<UtilisateurTypeSrvValeur>();
        }
        return this.emetteur;
    }

    /**
     * Gets the value of the exploitation property.
     * 
     * @return possible object is {@link ExploitationSrvValeur }
     */
    public ExploitationSrvValeur getExploitation()
    {
        return exploitation;
    }

    /**
     * Gets the value of the horodatage property.
     * 
     * @return possible object is {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getHorodatage()
    {
        return horodatage;
    }

    /**
     * Gets the value of the metier property.
     * <p>
     * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to
     * the returned list will be present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for
     * the metier property.
     * <p>
     * For example, to add a new item, do as follows:
     * 
     * <pre>
     * getMetier().add(newItem);
     * </pre>
     * <p>
     * Objects of the following type(s) are allowed in the list {@link UtilisateurTypeSrvValeur }
     * 
     * @return metier
     */
    public List<UtilisateurTypeSrvValeur> getMetier()
    {
        if (metier == null)
        {
            metier = new ArrayList<UtilisateurTypeSrvValeur>();
        }
        return this.metier;
    }

    /**
     * Gets the value of the typeTrace property.
     * 
     * @return possible object is {@link String }
     */
    public String getTypeTrace()
    {
        return typeTrace;
    }

    /**
     * Gets the value of the typeTraceInitiateur property.
     * 
     * @return possible object is {@link String }
     */
    public String getTypeTraceInitiateur()
    {
        return typeTraceInitiateur;
    }

    /**
     * Sets the value of the exploitation property.
     * 
     * @param value allowed object is {@link ExploitationSrvValeur }
     */
    public void setExploitation(ExploitationSrvValeur value)
    {
        this.exploitation = value;
    }

    /**
     * Sets the value of the horodatage property.
     * 
     * @param value allowed object is {@link XMLGregorianCalendar }
     */
    public void setHorodatage(XMLGregorianCalendar value)
    {
        this.horodatage = value;
    }

    /**
     * Sets the value of the typeTrace property.
     * 
     * @param value allowed object is {@link String }
     */
    public void setTypeTrace(String value)
    {
        this.typeTrace = value;
    }

    /**
     * Sets the value of the typeTraceInitiateur property.
     * 
     * @param value allowed object is {@link String }
     */
    public void setTypeTraceInitiateur(String value)
    {
        this.typeTraceInitiateur = value;
    }

}
