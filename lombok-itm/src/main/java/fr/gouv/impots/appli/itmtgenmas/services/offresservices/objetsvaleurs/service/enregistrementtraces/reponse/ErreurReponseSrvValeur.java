/*
 * Copyright (c) 2011 DGFiP - Tous droits réservés
 */
package fr.gouv.impots.appli.itmtgenmas.services.offresservices.objetsvaleurs.service.enregistrementtraces.reponse;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

import fr.gouv.impots.appli.itmtgenmas.services.offresservices.Erreur;

/**
 * <p>
 * Java class for ErreurReponseSrvValeur complex type.
 * <p>
 * The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ErreurReponseSrvValeur">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="erreur" type="{http://services.itmtgenmas.appli.impots.gouv.fr/offresServices}erreur" minOccurs="0"/>
 *         &lt;element name="rejeuIeip" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ErreurReponseSrvValeur", propOrder = {
        "erreur",
        "rejeuIeip"
})
public class ErreurReponseSrvValeur
{

    /** erreur. */
    protected Erreur erreur;

    /** rejeu ieip. */
    protected Boolean rejeuIeip;

    /**
     * Constructeur de la classe ErreurReponseSrvValeur.java
     *
     */
    public ErreurReponseSrvValeur()
    {
        super();  
        
    }

    /**
     * Gets the value of the erreur property.
     * 
     * @return possible object is {@link Erreur }
     */
    public Erreur getErreur()
    {
        return erreur;
    }

    /**
     * Gets the value of the rejeuIeip property.
     * 
     * @return possible object is {@link Boolean }
     */
    public Boolean isRejeuIeip()
    {
        return rejeuIeip;
    }

    /**
     * Sets the value of the erreur property.
     * 
     * @param value allowed object is {@link Erreur }
     */
    public void setErreur(Erreur value)
    {
        this.erreur = value;
    }

    /**
     * Sets the value of the rejeuIeip property.
     * 
     * @param value allowed object is {@link Boolean }
     */
    public void setRejeuIeip(Boolean value)
    {
        this.rejeuIeip = value;
    }

}
