/*
 * Copyright (c) 2011 DGFiP - Tous droits réservés
 */
package fr.gouv.impots.appli.itmtgenmas.services.offresservices.objetsvaleurs.service.commun.infosinterface;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for informationsInterfaceValeur complex type.
 * <p>
 * The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="informationsInterfaceValeur">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="descriptif" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nomContratApplicatif" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nomInterface" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nomMas" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "informationsInterfaceValeur", propOrder = {
        "descriptif",
        "nomContratApplicatif",
        "nomInterface",
        "nomMas"
})
public class InformationsInterfaceValeur
{

    /** descriptif. */
    protected String descriptif;

    /** nom contrat applicatif. */
    protected String nomContratApplicatif;

    /** nom interface. */
    protected String nomInterface;

    /** nom mas. */
    protected String nomMas;

    /**
     * Constructeur de la classe InformationsInterfaceValeur.java
     *
     */
    public InformationsInterfaceValeur()
    {
        super();  
        
    }

    /**
     * Gets the value of the descriptif property.
     * 
     * @return possible object is {@link String }
     */
    public String getDescriptif()
    {
        return descriptif;
    }

    /**
     * Gets the value of the nomContratApplicatif property.
     * 
     * @return possible object is {@link String }
     */
    public String getNomContratApplicatif()
    {
        return nomContratApplicatif;
    }

    /**
     * Gets the value of the nomInterface property.
     * 
     * @return possible object is {@link String }
     */
    public String getNomInterface()
    {
        return nomInterface;
    }

    /**
     * Gets the value of the nomMas property.
     * 
     * @return possible object is {@link String }
     */
    public String getNomMas()
    {
        return nomMas;
    }

    /**
     * Sets the value of the descriptif property.
     * 
     * @param value allowed object is {@link String }
     */
    public void setDescriptif(String value)
    {
        this.descriptif = value;
    }

    /**
     * Sets the value of the nomContratApplicatif property.
     * 
     * @param value allowed object is {@link String }
     */
    public void setNomContratApplicatif(String value)
    {
        this.nomContratApplicatif = value;
    }

    /**
     * Sets the value of the nomInterface property.
     * 
     * @param value allowed object is {@link String }
     */
    public void setNomInterface(String value)
    {
        this.nomInterface = value;
    }

    /**
     * Sets the value of the nomMas property.
     * 
     * @param value allowed object is {@link String }
     */
    public void setNomMas(String value)
    {
        this.nomMas = value;
    }

}
