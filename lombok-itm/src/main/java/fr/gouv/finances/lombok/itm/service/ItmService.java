/*
 * Copyright (c) 2011 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.itm.service;

import java.util.List;

import fr.gouv.impots.appli.itmtgenmas.services.offresservices.objetsvaleurs.service.commun.infosinterface.InformationsInterfaceValeur;
import fr.gouv.impots.appli.itmtgenmas.services.offresservices.objetsvaleurs.service.enregistrementtraces.reponse.ErreurReponseSrvValeur;
import fr.gouv.impots.appli.itmtgenmas.services.offresservices.objetsvaleurs.service.enregistrementtraces.requete.NtcSRequeteSrvValeur;

/**
 * Interface ItmService .
 */
public interface ItmService
{

    /**
     * methode Enregistrer trace : .
     * 
     * @param arrayOfbyte
     * @param arrayOfNtcSRequeteSrvValeur
     * @return erreur reponse srv valeur
     */
    ErreurReponseSrvValeur enregistrerTrace(byte[] arrayOfbyte, NtcSRequeteSrvValeur arrayOfNtcSRequeteSrvValeur);

    /**
     * methode Enregistrer traces : .
     * 
     * @param arrayOfbyte
     * @param arrayOfNtcSRequeteSrvValeur
     * @return list
     */
    List<ErreurReponseSrvValeur> enregistrerTraces(byte[] arrayOfbyte, List<NtcSRequeteSrvValeur> arrayOfNtcSRequeteSrvValeur);

    /**
     * methode Obtenir informations : .
     * 
     * @return informations interface valeur
     */
    InformationsInterfaceValeur obtenirInformations();

}
