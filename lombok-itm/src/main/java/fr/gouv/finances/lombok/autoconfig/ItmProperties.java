package fr.gouv.finances.lombok.autoconfig;

import org.springframework.boot.context.properties.ConfigurationProperties;

import fr.gouv.finances.lombok.autoconfig.LombokComposantProperties;

/**
 * @author celinio fernandes Date: Feb 20, 2020
 */
@ConfigurationProperties(prefix = "lombok.composant.itm")
public class ItmProperties extends LombokComposantProperties
{

    @Override
    public String toString()
    {
        return "ItmProperties [inclus=" + inclus + "]";
    }
}
