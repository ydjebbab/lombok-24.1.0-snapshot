/*
 * Copyright (c) 2011 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.itm.service.impl;

import java.util.List;

import fr.gouv.finances.lombok.itm.service.ItmService;
import fr.gouv.finances.lombok.itm.ws.service.FabriqueClientWSItmService;
import fr.gouv.finances.lombok.util.exception.ApplicationExceptionTransformateur;
import fr.gouv.impots.appli.commun.frameworks.itmtgenmas.services.itmtgenmas.contrat_lombok_enreg_v3.enregistrementtracessynchroneinterface.EnregistrementTracesSynchroneInterface;
import fr.gouv.impots.appli.commun.frameworks.itmtgenmas.services.itmtgenmas.contrat_lombok_enreg_v3.enregistrementtracessynchroneinterface.TechDysfonctionnementErreur;
import fr.gouv.impots.appli.commun.frameworks.itmtgenmas.services.itmtgenmas.contrat_lombok_enreg_v3.enregistrementtracessynchroneinterface.TechIndisponibiliteErreur;
import fr.gouv.impots.appli.commun.frameworks.itmtgenmas.services.itmtgenmas.contrat_lombok_enreg_v3.enregistrementtracessynchroneinterface.TechProtocolaireErreur;
import fr.gouv.impots.appli.itmtgenmas.services.offresservices.objetsvaleurs.service.commun.infosinterface.InformationsInterfaceValeur;
import fr.gouv.impots.appli.itmtgenmas.services.offresservices.objetsvaleurs.service.enregistrementtraces.reponse.ErreurReponseSrvValeur;
import fr.gouv.impots.appli.itmtgenmas.services.offresservices.objetsvaleurs.service.enregistrementtraces.requete.NtcSRequeteSrvValeur;

/**
 * Class ItmServiceImpl
 */
public class ItmServiceImpl implements ItmService
{
    private FabriqueClientWSItmService fabriqueclientwsitmserviceso;

    public ItmServiceImpl()
    {
        super();
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.itm.service.ItmService#enregistrerTrace(byte[],
     *      fr.gouv.impots.appli.itmtgenmas.services.offresservices.objetsvaleurs.service.enregistrementtraces.requete.NtcSRequeteSrvValeur)
     */
    @Override
    public ErreurReponseSrvValeur enregistrerTrace(byte[] arrayOfbyte, NtcSRequeteSrvValeur ntcSRequeteSrvValeur)
    {
        try
        {
            EnregistrementTracesSynchroneInterface enregistrementTracesSynchroneInterface =
                fabriqueclientwsitmserviceso.creerItmWebServiceClient();
            return enregistrementTracesSynchroneInterface.enregistrerTrace(arrayOfbyte, ntcSRequeteSrvValeur);
        }
        catch (TechDysfonctionnementErreur | TechIndisponibiliteErreur | TechProtocolaireErreur e)
        {
            throw ApplicationExceptionTransformateur.transformer(e);
        }
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.itm.service.ItmService#enregistrerTraces(byte[], java.util.List)
     */
    @Override
    public List<ErreurReponseSrvValeur> enregistrerTraces(byte[] arrayOfbyte, List<NtcSRequeteSrvValeur> arrayOfNtcSRequeteSrvValeur)
    {
        try
        {
            EnregistrementTracesSynchroneInterface enregistrementTracesSynchroneInterface =
                fabriqueclientwsitmserviceso.creerItmWebServiceClient();
            return enregistrementTracesSynchroneInterface.enregistrerTraces(arrayOfbyte, arrayOfNtcSRequeteSrvValeur);
        }
        catch (TechDysfonctionnementErreur | TechIndisponibiliteErreur | TechProtocolaireErreur e)
        {
            throw ApplicationExceptionTransformateur.transformer(e);
        }
    }

    /**
     * Accesseur de l attribut fabriqueclientwsitmserviceso.
     * 
     * @return fabriqueclientwsitmserviceso
     */
    public FabriqueClientWSItmService getFabriqueclientwsitmserviceso()
    {
        return fabriqueclientwsitmserviceso;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.itm.service.ItmService#obtenirInformations()
     */
    @Override
    public InformationsInterfaceValeur obtenirInformations()
    {
        try
        {
            EnregistrementTracesSynchroneInterface enregistrementTracesSynchroneInterface =
                fabriqueclientwsitmserviceso.creerItmWebServiceClient();
            return enregistrementTracesSynchroneInterface.obtenirInformations();
        }
        catch (TechDysfonctionnementErreur | TechIndisponibiliteErreur | TechProtocolaireErreur e)
        {
            throw ApplicationExceptionTransformateur.transformer(e);
        }
    }

    /**
     * Modificateur de l attribut fabriqueclientwsitmserviceso.
     * 
     * @param fabriqueclientwsitmserviceso le nouveau fabriqueclientwsitmserviceso
     */
    public void setFabriqueclientwsitmserviceso(FabriqueClientWSItmService fabriqueclientwsitmserviceso)
    {
        this.fabriqueclientwsitmserviceso = fabriqueclientwsitmserviceso;
    }

}
