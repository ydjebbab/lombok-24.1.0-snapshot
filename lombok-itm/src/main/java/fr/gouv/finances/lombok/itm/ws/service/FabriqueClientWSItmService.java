/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : FabriqueClientWSJournalService.java
 *
 */
package fr.gouv.finances.lombok.itm.ws.service;

import fr.gouv.impots.appli.commun.frameworks.itmtgenmas.services.itmtgenmas.contrat_lombok_enreg_v3.enregistrementtracessynchroneinterface.EnregistrementTracesSynchroneInterface;

/**
 * Class FabriqueClientWSJournalService --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.1 $ Date: 11 déc. 2009
 */
public interface FabriqueClientWSItmService
{

    /**
     * methode Creer journal web service client : --.
     * 
     * @return journal web service
     */
    public EnregistrementTracesSynchroneInterface creerItmWebServiceClient();

}