/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : FabriqueClientWSJournalServiceImpl.java
 *
 */
package fr.gouv.finances.lombok.itm.ws.service.impl;

import java.net.URL;

import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;

import fr.gouv.finances.lombok.itm.ws.service.FabriqueClientWSItmService;
import fr.gouv.finances.lombok.util.ws.SlpaUtil;
import fr.gouv.impots.appli.commun.frameworks.itmtgenmas.services.itmtgenmas.contrat_lombok_enreg_v3.enregistrementtracessynchroneinterface.EnregistrementTracesSynchroneInterface;
import fr.gouv.impots.appli.commun.frameworks.itmtgenmas.services.itmtgenmas.contrat_lombok_enreg_v3.enregistrementtracessynchroneinterface.EnregistrementTracesSynchroneInterface_Service;

/**
 * Class FabriqueClientWSJournalServiceImpl
 * 
 * @author amleplatinec
 * @version $Revision: 1.1 $ Date: 11 déc. 2009
 */
public class FabriqueClientWSItmServiceImpl implements FabriqueClientWSItmService
{

    static String SERVICE = "EnregistrementTracesSynchroneInterface";

    static String NAMESPACE =
        "http://services.itmtgenmas.frameworks.commun.appli.impots.gouv.fr/itmtgenmas/CONTRAT_LOMBOK_ENREG_V3/EnregistrementTracesSynchroneInterface";

    private String contrat;

    public FabriqueClientWSItmServiceImpl()
    {
        super();
    }

    /**
     * methode Creer journal web service client : --.
     * 
     * @return journal web service
     */
    @Override
    public EnregistrementTracesSynchroneInterface creerItmWebServiceClient()
    {
        // Récupération de l' url d'accès au service
        URL urlAccessPoint = SlpaUtil.rechercherLocalisationService(contrat, SERVICE);
        // Récupération de l' url d'accès au wsdl
        URL urlWsdl = SlpaUtil.rechercherLocalisationWSDL(contrat, SERVICE);

        QName qName = new QName(NAMESPACE, SERVICE);

        // Récupération d'une instance cliente
        EnregistrementTracesSynchroneInterface client =
            new EnregistrementTracesSynchroneInterface_Service(urlWsdl, qName).getEnregistrementTracesSynchroneInterfacePort();

        // Paramétrage de l'url d'accès au service: pour l'exécution des opérations
        ((BindingProvider) client).getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY,
            urlAccessPoint.toString());

        return client;
    }

    /**
     * Accesseur de l'attribut contrat.
     * 
     * @return contrat
     */
    public String getContrat()
    {
        return contrat;
    }

    /**
     * Modificateur de l'attribut contrat.
     * 
     * @param contrat le nouveau contrat
     */
    public void setContrat(String contrat)
    {
        this.contrat = contrat;
    }
}
