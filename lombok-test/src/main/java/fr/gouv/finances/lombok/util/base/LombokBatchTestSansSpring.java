/*
 * Copyright (c) 2016 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.util.base;

import static org.mockito.Mockito.mock;

import org.junit.Before;

import fr.gouv.finances.lombok.batch.BatchStarter;
import fr.gouv.finances.lombok.batch.SystemExitWrapper;

/**
 * LombokBatchTest.java Cette classe abstraite reprend la configuration par annotation de lombokservicetest ce qui
 * permet dans une sous classe de rajouter des configurations en plus de celles héritées par cette classe. Ce sera à
 * revoir... permet pour l'instant la migration des classes de batch.
 * 
 * @author chouard Date: 24 oct. 2014
 */
public abstract class LombokBatchTestSansSpring
{
    /** system exit. */
    protected SystemExitWrapper systemExit;
    
    /** batch starter. */
    private BatchStarter batchStarter;
    
    /**
     * methode Sets the up : mock le wrapper autour de System.exit
     */
    @Before
    public void setUp()
    {
        systemExit = mock(SystemExitWrapper.class);
        batchStarter = new BatchStarter(systemExit);
    }

    /**
     * Méthode permettant d'excuter un batch.
     *
     * @param args nom du batch
     */
    public void testBatch(String[] args)
    {
        batchStarter.executeAndExit(args);
    }

}