/*
 * Copyright (c) 2016 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.util.base;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

/**
 * Interface LombokServiceTest.
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@ActiveProfiles(profiles = {"embedded", "hibernate","edition", "atlas", "journal", "structure", "bancaire","jdbc"})
@ContextConfiguration(name = "application", locations = "/conf/app-config.xml")
public @interface LombokServiceTest
{

}
