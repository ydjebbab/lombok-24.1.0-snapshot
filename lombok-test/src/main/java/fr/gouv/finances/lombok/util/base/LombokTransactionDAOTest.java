/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - chouard
 *
*
 *
 * fichier : LombokTransactionDAOTest.java
 *
 */
package fr.gouv.finances.lombok.util.base;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.transaction.annotation.Transactional;

/**
 * Interface LombokTransactionDAOTest.
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
//@ActiveProfiles(profiles = {"embedded", "hibernate","edition", "atlas", "journal", "structure", "bancaire", "jdbc"})
@ActiveProfiles(profiles = {"embedded", "jpa","edition", "atlas", "journal", "journal-ws","itm","bancaire","sireme","clamav-simulation","springbatch","jdbc","nowebflowcsrf","adresse"})
@ContextConfiguration(name = "application", locations = "classpath:conf/app-config.xml")
@Rollback(true)
// @WebAppConfiguration
@Transactional
public @interface LombokTransactionDAOTest
{

    /**
     * methode Supprimer dirty check
     *
     * @return true, si c'est vrai
     */
    boolean supprimerDirtyCheck() default false;

}
