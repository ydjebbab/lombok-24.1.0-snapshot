/*
 * Copyright (c) 2016 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.util.base;

import java.io.FileNotFoundException;

import org.junit.BeforeClass;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Log4jConfigurer;

import fr.gouv.finances.lombok.util.spring.ResolveurDeLocalisations;

/**
 * Cette classe devra être modifiée lorsque le log4.properties aura été déplacé dans un emplacement
 * indépendant d'un module web.
 * 
 * @author chouard Date: 22 oct. 2014
 * @deprecated cette classe ne doit plus être utilisée. Il faut désormais créer un fichier log4j.properties dans le
 *  classpath des tests
 */
@Deprecated
public abstract class LombokBeanTest
{
    
    /** Constant : LOCALISATION_LOG4J_PROPERTIES. */
    private static final String LOCALISATION_LOG4J_PROPERTIES = "$localisations{appli.web.root}/WEB-INF/properties/log4j.properties";

    /**
     * methode Inits the :.
     */
    @BeforeClass
    public static void init()
    {
        String log4jurl =
            ResolveurDeLocalisations
                .resoudreURLLocalisationEtTransformerEnPath(LOCALISATION_LOG4J_PROPERTIES);

        try
        {
            Log4jConfigurer.initLogging(log4jurl);
        }
        catch (FileNotFoundException exc)
        {
            Logger logger = LoggerFactory.getLogger(LombokBeanTest.class);
            String sformat = String.format("Impossible d'initiliser log4j, fichier : %s non trouvé !", LOCALISATION_LOG4J_PROPERTIES);
            logger.error(sformat,exc);
            

        }

    }

}
