/*
 * Copyright (c) 2016 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.util.base;

import java.io.FileNotFoundException;

import org.jfree.util.Log;
import org.junit.runners.model.InitializationError;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Log4jConfigurer;

import fr.gouv.finances.lombok.util.spring.ResolveurDeLocalisations;

/**
 * Class LombokJunit4ClassRunner
 */
public class LombokJunit4ClassRunner extends SpringJUnit4ClassRunner
{

    /**
     * Instanciation de lombok junit4 class runner.
     *
     * @param clazz 
     * @throws InitializationError the initialization error
     */
    public LombokJunit4ClassRunner(Class<?> clazz) throws InitializationError
    {
        super(clazz); // "documenté" Raccord de constructeur auto-généré

    }

    static
    {
        String log4jurl = null;
        try
        {
            log4jurl =
                ResolveurDeLocalisations
                    .resoudreURLLocalisationEtTransformerEnPath("$localisations{appli.web.root}/WEB-INF/properties/log4j2.properties");
            Log4jConfigurer.initLogging(log4jurl);
        }
        catch (FileNotFoundException ex)
        {
            Log.debug("Exception détectée", ex);
        }
    }

}
