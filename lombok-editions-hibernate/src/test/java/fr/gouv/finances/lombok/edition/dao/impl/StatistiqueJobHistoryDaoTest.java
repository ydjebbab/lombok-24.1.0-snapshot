package fr.gouv.finances.lombok.edition.dao.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.sql.Timestamp;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.gouv.finances.lombok.edition.bean.JobHistory;
import fr.gouv.finances.lombok.edition.dao.StatistiqueJobHistoryDao;
import fr.gouv.finances.lombok.edition.techbean.StatJobHistory;
import fr.gouv.finances.lombok.test.controle.ControleDonnees;
import fr.gouv.finances.lombok.test.controle.VerificationExecution;
import fr.gouv.finances.lombok.util.base.PurgeTablesDao;
import fr.gouv.finances.lombok.util.date.TemporaliteUtils;
import fr.gouv.finances.lombok.util.exception.ProgrammationException;

/**
 * Tests unitaires automatisés du DAO Hibernate gérant les données des statistiques de l'historique des travaux
 * d'édition.
 *
 * @author Christophe Breheret-Girardin
 * @author CF
 */
@ContextConfiguration({"classpath:/conf/applicationContext-edition-hibernate-test.xml",
        "classpath*:/conf/applicationContext-core-persistance-test.xml",
        "classpath*:/conf/applicationContext-core-orm-test.xml",
        "classpath*:/conf/applicationContext-commun-edition-hibernate.xml",
        "classpath*:/conf/applicationContext-edition-dao.xml"})
@ActiveProfiles(profiles = {"edition", "hibernate"})
@RunWith(SpringJUnit4ClassRunner.class)
public class StatistiqueJobHistoryDaoTest
{
    public StatistiqueJobHistoryDaoTest()
    {
        super();
    }

    protected static final Logger LOGGER = LoggerFactory.getLogger(StatistiqueJobHistoryDaoTest.class);

    /** Déclaration du DAO gérant les données des statistiques liées aux historiques des travaux d'édition. */
    @Autowired
    private StatistiqueJobHistoryDao statistiquejobhistorydao;

    /** Déclaration du DAO de gestion des purges de la base. */
    @Autowired
    private PurgeTablesDao purgetablesdao;

    /** Map contenant les historiques des travaux d'édition présents en base */
    private Map<Integer, JobHistory> mapJobHistory;

    /** Constante donnant le suffixe d'une libellé d'une application d'origine */
    private static final String SUFFIXE_APPLICATION_ORIGINE = "APPLICATION ORIGINE ";

    /** Constante donnant le libellé d'une 1ère application d'origine */
    private static final String APPLICATION_ORIGINE_1 = SUFFIXE_APPLICATION_ORIGINE + 1;

    /** Constante donnant le libellé d'une 2ème application d'origine */
    private static final String APPLICATION_ORIGINE_2 = SUFFIXE_APPLICATION_ORIGINE + 2;

    /** Constante donnant le libellé d'une application d'origine non utilisée dans les jeux d'essai */
    private static final String APPLICATION_ORIGINE_INCONNUE = SUFFIXE_APPLICATION_ORIGINE + 3;

    /**
     * Initialisation des données en base avant les tests.
     */
    @Before
    public void creerDonnees()
    {
        // Purge des données de la base
        purgetablesdao.purgerTables("ZFILTREVALUE_ZFIV", "ZFILTREEDITION_ZFIE", "ZPROFILDESTI_ZPRD", "ZJOBHISTORY_ZJOH");

        // Vérification de l'absence de données
        ControleDonnees.verifierElements(statistiquejobhistorydao.loadAllObjects(JobHistory.class));

        // Génération des historiques de travaux d'édition, insertion en base de données et sauvegarde dans une Map
        mapJobHistory = IntStream.rangeClosed(1, 13).mapToObj(this::getJobHistory)
            .peek(statistiquejobhistorydao::saveObject)
            .collect(Collectors.toMap(j -> Integer.parseInt(j.getEditionUuid()), j -> j));

        // Vérification de la présence des données avant exécution du test
        ControleDonnees.verifierElements(statistiquejobhistorydao.loadAllObjects(JobHistory.class), mapJobHistory.get(1),
            mapJobHistory.get(2), mapJobHistory.get(3), mapJobHistory.get(4), mapJobHistory.get(5), mapJobHistory.get(6),
            mapJobHistory.get(7), mapJobHistory.get(8), mapJobHistory.get(9), mapJobHistory.get(10), mapJobHistory.get(11),
            mapJobHistory.get(12), mapJobHistory.get(13));
    }

    /**
     * Méthode permettant de tester l'injection du DAO.
     */
    @Test
    public final void injectionDao()
    {
        // Vérification
        assertNotNull("DAO non injecté dans le test", statistiquejobhistorydao);
    }

    /**
     * Test de la méthode fr.gouv.finances.lombok.edition.dao.StatistiqueJobHistoryDao#compteJobHistoryParDatePurge(
     * java.lang.String).
     */
    @Test
    public void compteJobHistoryParDatePurge()
    {
        // Exécution de la méthode à tester avec des paramètres non initialisés ou vides
        VerificationExecution.verifierException(ProgrammationException.class,
            () -> statistiquejobhistorydao.compteJobHistoryParDatePurge(null));
        VerificationExecution.verifierException(ProgrammationException.class,
            () -> statistiquejobhistorydao.compteJobHistoryParDatePurge(""));

        // Exécution de la méthode à tester sans résultat et vérifications
        ControleDonnees.verifierElements(
            statistiquejobhistorydao.compteJobHistoryParDatePurge(APPLICATION_ORIGINE_INCONNUE));

        // Exécution de la méthode à tester avec résultat et vérifications (avec ordre)
        verifierJobHistoryDatePurge(statistiquejobhistorydao.compteJobHistoryParDatePurge(APPLICATION_ORIGINE_1),
            getStatJobHistory(TemporaliteUtils.getTimestamp(2017, 1, 18), 9),
            getStatJobHistory(TemporaliteUtils.getTimestamp(2016, 1, 18), 1));
        verifierJobHistoryDatePurge(statistiquejobhistorydao.compteJobHistoryParDatePurge(APPLICATION_ORIGINE_2),
            getStatJobHistory((Timestamp) null, 1), getStatJobHistory(TemporaliteUtils.getTimestamp(2016, 1, 18), 2));
    }

    /**
     * Test de la méthode fr.gouv.finances.lombok.edition.dao.StatistiqueJobHistoryDao#compteJobHistoryParJourDemande(
     * java.lang.String.
     */
    @Test
    public void compteJobHistoryParJourDemande()
    {
        // Exécution de la méthode à tester avec des paramètres non initialisés ou vides
        VerificationExecution.verifierException(ProgrammationException.class,
            () -> statistiquejobhistorydao.compteJobHistoryParJourDemande(null));
        VerificationExecution.verifierException(ProgrammationException.class,
            () -> statistiquejobhistorydao.compteJobHistoryParJourDemande(""));

        // Exécution de la méthode à tester sans résultat et vérifications (avec ordre)
        ControleDonnees.verifierElements(
            statistiquejobhistorydao.compteJobHistoryParJourDemande(APPLICATION_ORIGINE_INCONNUE));

        // Exécution de la méthode à tester avec résultat et vérifications
        verifierJobHistoryJourDemande(statistiquejobhistorydao.compteJobHistoryParJourDemande(APPLICATION_ORIGINE_1),
            getStatJobHistory(TemporaliteUtils.getTimestamp(2017, 1, 18), 9),
            getStatJobHistory(TemporaliteUtils.getTimestamp(2016, 1, 18), 1));
        verifierJobHistoryJourDemande(statistiquejobhistorydao.compteJobHistoryParJourDemande(APPLICATION_ORIGINE_2),
            getStatJobHistory((Timestamp) null, 1), getStatJobHistory(TemporaliteUtils.getTimestamp(2016, 1, 18), 2));
    }

    /**
     * Test de la méthode fr.gouv.finances.lombok.edition.dao.StatistiqueJobHistoryDao#compteJobHistoryParStatus(
     * java.lang.String.
     */
    @Test
    public void compteJobHistoryParStatus()
    {
        // Exécution de la méthode à tester avec des paramètres non initialisés ou vides
        VerificationExecution.verifierException(ProgrammationException.class,
            () -> statistiquejobhistorydao.compteJobHistoryParStatus(null));
        VerificationExecution.verifierException(ProgrammationException.class, () -> statistiquejobhistorydao.compteJobHistoryParStatus(""));

        // Exécution de la méthode à tester sans résultat et vérifications
        ControleDonnees.verifierElements(
            statistiquejobhistorydao.compteJobHistoryParStatus(APPLICATION_ORIGINE_INCONNUE));

        // Exécution de la méthode à tester avec résultat et vérifications
        verifierJobHistoryParStatus(statistiquejobhistorydao.compteJobHistoryParStatus(APPLICATION_ORIGINE_1),
            getStatJobHistory(JobHistory.STATUS_DISPONIBLE, 5l), getStatJobHistory(JobHistory.STATUS_DISPONIBLE_NON_ENVOYE, 1l),
            getStatJobHistory(JobHistory.STATUS_ECHEC, 4l));
        verifierJobHistoryParStatus(statistiquejobhistorydao.compteJobHistoryParStatus(APPLICATION_ORIGINE_2),
            getStatJobHistory(JobHistory.STATUS_DISPONIBLE_NON_ENVOYE, 2l), getStatJobHistory((String) null, 1l));
    }

    /**
     * Test de la méthode fr.gouv.finances.lombok.edition.dao.StatistiqueJobHistoryDao#compteJobHistoryParTypeEtStatus(
     * java.lang.String.
     */
    @Test
    public void compteJobHistoryParTypeEtStatus()
    {
        // Exécution de la méthode à tester avec des paramètres non initialisés ou vides
        VerificationExecution.verifierException(ProgrammationException.class,
            () -> statistiquejobhistorydao.compteJobHistoryParTypeEtStatus(null));
        VerificationExecution.verifierException(ProgrammationException.class,
            () -> statistiquejobhistorydao.compteJobHistoryParTypeEtStatus(""));

        // Exécution de la méthode à tester sans résultat et vérifications
        ControleDonnees.verifierElements(
            statistiquejobhistorydao.compteJobHistoryParTypeEtStatus(APPLICATION_ORIGINE_INCONNUE));

        // Exécution de la méthode à tester avec résultat et vérifications
        verifierJobHistoryTypeEtStatus(statistiquejobhistorydao.compteJobHistoryParTypeEtStatus(APPLICATION_ORIGINE_1),
            getStatJobHistory(JobHistory.STATUS_DISPONIBLE, "1", 3), getStatJobHistory(JobHistory.STATUS_DISPONIBLE, "2", 2),
            getStatJobHistory(JobHistory.STATUS_DISPONIBLE_NON_ENVOYE, "1", 1), getStatJobHistory(JobHistory.STATUS_ECHEC, "1", 4));
        verifierJobHistoryTypeEtStatus(statistiquejobhistorydao.compteJobHistoryParTypeEtStatus(APPLICATION_ORIGINE_2),
            getStatJobHistory(JobHistory.STATUS_DISPONIBLE_NON_ENVOYE, "2", 2), getStatJobHistory((String) null, (String) null, 1));
    }

    /**
     * Test de la méthode fr.gouv.finances.lombok.edition.dao.StatistiqueJobHistoryDao#compteTailleEditionParType(
     * java.lang.String.
     */
    @Test
    public void compteTailleEditionParType()
    {
        // Exécution de la méthode à tester avec des paramètres non initialisés ou vides
        VerificationExecution.verifierException(ProgrammationException.class,
            () -> statistiquejobhistorydao.compteTailleEditionParType(null));
        VerificationExecution.verifierException(ProgrammationException.class,
            () -> statistiquejobhistorydao.compteTailleEditionParType(""));

        // Exécution de la méthode à tester sans résultat et vérifications
        ControleDonnees.verifierElements(statistiquejobhistorydao.compteTailleEditionParType(APPLICATION_ORIGINE_INCONNUE));

        // Exécution de la méthode à tester avec résultat et vérifications
        verifierJobHistoryType(statistiquejobhistorydao.compteTailleEditionParType(APPLICATION_ORIGINE_1),
            new StatJobHistory(JobHistory.STATUS_DISPONIBLE, "1", 3l, 6L, 2D),
            new StatJobHistory(JobHistory.STATUS_DISPONIBLE, "2", 2l, 9L, 4.5D),
            new StatJobHistory(JobHistory.STATUS_DISPONIBLE_NON_ENVOYE, "1", 1l, 6L, 6D));
        verifierJobHistoryType(statistiquejobhistorydao.compteTailleEditionParType(APPLICATION_ORIGINE_2),
            new StatJobHistory(JobHistory.STATUS_DISPONIBLE_NON_ENVOYE, "2", 2l, 15L, 7.5D));
    }

    /**
     * Méthode permettant de verifier une liste des statistiques des travaux d'édition, par rapport à la date de purge
     * des éditions, suivant un attendu.
     *
     * @param statsJobHistory statistiques des travaux d'édition à vérifier
     * @param statsJobHistoryPrevus statistiques des travaux d'édition attendues
     */
    private void verifierJobHistoryDatePurge(List<StatJobHistory> statsJobHistory, StatJobHistory... statsJobHistoryPrevus)
    {
        // Vérification du nombre de résultat
        verifierNombre(statsJobHistory, statsJobHistoryPrevus);

        // Vérifications des données dans l'ordre
        AtomicInteger index = new AtomicInteger();
        statsJobHistory.stream().forEach(statJobHistory -> {
            StatJobHistory statJobHistoryPrevu = statsJobHistoryPrevus[index.getAndIncrement()];
            assertNotNull("La statistique n'est pas prévue avec un nombre d'édition de " + statJobHistory.getNombreEditions(),
                statJobHistoryPrevu);
            assertEquals("La date de purge n'est pas correcte", statJobHistoryPrevu.getDatePurge(), statJobHistory.getDatePurge());
        });
    }

    /**
     * Méthode permettant de verifier une liste des statistiques des travaux d'édition, par rapport au jour de demande
     * des éditions, suivant un attendu.
     *
     * @param statsJobHistory statistiques des travaux d'édition à vérifier
     * @param statsJobHistoryPrevus statistiques des travaux d'édition attendues
     */
    private void verifierJobHistoryJourDemande(List<StatJobHistory> statsJobHistory, StatJobHistory... statsJobHistoryPrevus)
    {
        // Vérification du nombre de résultat
        verifierNombre(statsJobHistory, statsJobHistoryPrevus);

        // Vérifications des données dans l'ordre
        AtomicInteger index = new AtomicInteger();
        statsJobHistory.stream().forEach(statJobHistory -> {
            StatJobHistory statJobHistoryPrevu = statsJobHistoryPrevus[index.getAndIncrement()];
            assertEquals("Le nombre d'édition n'est pas correct", new Long(statJobHistoryPrevu.getNombreEditions()),
                new Long(statJobHistory.getNombreEditions()));
            assertNotNull("La statistique n'est pas prévue avec un nombre d'édition de " + statJobHistory.getNombreEditions(),
                statJobHistoryPrevu);
            assertEquals("Le jour demandé n'est pas correct", statJobHistoryPrevu.getJourDemande(), statJobHistory.getJourDemande());
        });
    }

    /**
     * Méthode permettant de verifier une liste des statistiques des travaux d'édition, par rapport à un statut
     * d'édition, suivant un attendu.
     *
     * @param statsJobHistory statistiques des travaux d'édition à vérifier
     * @param statsJobHistoryPrevus statistiques des travaux d'édition attendues
     */
    private void verifierJobHistoryParStatus(List<StatJobHistory> statsJobHistory, StatJobHistory... statsJobHistoryPrevus)
    {
        // Vérification du nombre de résultat
        verifierNombre(statsJobHistory, statsJobHistoryPrevus);

        // Transformation de la liste des attendus en Map (clé : status, valeur : objet)
        Map<Long, StatJobHistory> mapStat = Arrays.asList(statsJobHistoryPrevus).stream()
            .collect(Collectors
                .toMap(statJobHistory -> statJobHistory.getNombreEditions(), statJobHistory -> statJobHistory));

        // Vérifications des données
        statsJobHistory.stream().forEach(statJobHistory -> {
            StatJobHistory statJobHistoryPrevu = mapStat.get(statJobHistory.getNombreEditions());
            assertNotNull("La statistique n'est pas prévue avec un nombre d'édition de " + statJobHistory.getNombreEditions(),
                statJobHistoryPrevu);
            assertEquals("Le statut n'est pas correct", statJobHistoryPrevu.getStatus(), statJobHistory.getStatus());
        });
    }

    /**
     * Méthode permettant de verifier une liste des statistiques des travaux d'édition, par type et statut, suivant un
     * attendu.
     *
     * @param statsJobHistory statistiques des travaux d'édition à vérifier
     * @param statsJobHistoryPrevus statistiques des travaux d'édition attendues
     */
    private void verifierJobHistoryTypeEtStatus(List<StatJobHistory> statsJobHistory, StatJobHistory... statsJobHistoryPrevus)
    {
        // Vérification du nombre de résultat
        verifierNombre(statsJobHistory, statsJobHistoryPrevus);

        // Transformation de la liste des attendus en Map (clé : status, valeur : objet)
        Map<Long, StatJobHistory> mapStat = Arrays.asList(statsJobHistoryPrevus).stream()
            .collect(Collectors
                .toMap(statJobHistory -> statJobHistory.getNombreEditions(), statJobHistory -> statJobHistory));

        // Vérifications des données
        statsJobHistory.stream().forEach(statJobHistory -> {
            StatJobHistory statJobHistoryPrevu = mapStat.get(statJobHistory.getNombreEditions());
            assertNotNull("La statistique n'est pas prévue avec un nombre d'édition de " + statJobHistory.getNombreEditions(),
                statJobHistoryPrevu);
            assertEquals("L'id n'est pas correct", statJobHistoryPrevu.getBeanEditionId(), statJobHistory.getBeanEditionId());
            assertEquals("Le statut n'est pas correct", statJobHistoryPrevu.getStatus(), statJobHistory.getStatus());
        });
    }

    /**
     * Méthode permettant de verifier une liste des statistiques des travaux d'édition, par type, suivant un attendu.
     *
     * @param statsJobHistory statistiques des travaux d'édition à vérifier
     * @param statsJobHistoryPrevus statistiques des travaux d'édition attendues
     */
    private void verifierJobHistoryType(List<StatJobHistory> statsJobHistory, StatJobHistory... statsJobHistoryPrevus)
    {
        // Vérification du nombre de résultat
        verifierNombre(statsJobHistory, statsJobHistoryPrevus);

        // Transformation de la liste des attendus en Map (clé : status, valeur : objet)
        Map<Long, StatJobHistory> mapStat = Arrays.asList(statsJobHistoryPrevus).stream()
            .collect(Collectors
                .toMap(statJobHistory -> statJobHistory.getNombreEditions(), statJobHistory -> statJobHistory));

        // Vérifications des données
        statsJobHistory.stream().forEach(statJobHistory -> {
            StatJobHistory statJobHistoryPrevu = mapStat.get(statJobHistory.getNombreEditions());
            assertNotNull("La statistique n'est pas prévue avec un nombre d'édition de " + statJobHistory.getNombreEditions(),
                statJobHistoryPrevu);
            assertEquals("L'id n'est pas correct", statJobHistoryPrevu.getBeanEditionId(), statJobHistory.getBeanEditionId());
            assertEquals("Le statut n'est pas correct", statJobHistoryPrevu.getStatus(), statJobHistory.getStatus());
            assertEquals("Le total n'est pas correct", statJobHistoryPrevu.getTailleTotale(), statJobHistory.getTailleTotale());
            assertEquals("La moyenne n'est pas correcte", statJobHistoryPrevu.getTailleMoyenne(), statJobHistory.getTailleMoyenne());
        });
    }

    /**
     * Méthode permettant de verifier le nombre de statistiques des travaux d'édition obtenu, par rapport à celui
     * attendu.
     *
     * @param statsJobHistory statistiques des travaux d'édition à vérifier
     * @param statsJobHistoryPrevus statistiques des travaux d'édition attendues
     */
    private void verifierNombre(List<StatJobHistory> statsJobHistory, StatJobHistory... statsJobHistoryPrevus)
    {
        assertEquals("Le nombre d'élément récupéré par rapport à l'attendu est eronné", statsJobHistoryPrevus.length,
            statsJobHistory.size());
    }

    /**
     * Méthode permettant de générer un historique de travaux d'édition.
     *
     * @param indice indice à utiliser dans les informations de l'historique
     * @return historique de travaux d'édition généré
     */
    private JobHistory getJobHistory(int indice)
    {
        JobHistory jobHistory = new JobHistory();

        // L'UID de l'édition est l'indice fourni
        jobHistory.setEditionUuid(String.valueOf(indice));

        // Affectation de différentes valeurs suivant l'indice
        switch (indice)
        {
            case 1:
                jobHistory.getDestinationEdition().setApplicationOrigine(APPLICATION_ORIGINE_1);
                jobHistory.getArchivageEdition().setDatePurge(TemporaliteUtils.getTimestamp(2017, 1, 18));
                jobHistory.getOrdoEdition().setDateDemande(TemporaliteUtils.getTimestamp(2017, 1, 18, 1, 2, 3));
                jobHistory.getOrdoEdition().setStatus(JobHistory.STATUS_DISPONIBLE);
                jobHistory.setBeanEditionId("1");
                jobHistory.getStockageEdition().setTailleFichier(1l);
                break;
            case 2:
                jobHistory.getDestinationEdition().setApplicationOrigine(APPLICATION_ORIGINE_1);
                jobHistory.getArchivageEdition().setDatePurge(TemporaliteUtils.getTimestamp(2017, 1, 18));
                jobHistory.getOrdoEdition().setDateDemande(TemporaliteUtils.getTimestamp(2017, 1, 18, 1, 2, 3));
                jobHistory.getOrdoEdition().setStatus(JobHistory.STATUS_DISPONIBLE);
                jobHistory.setBeanEditionId("1");
                jobHistory.getStockageEdition().setTailleFichier(2l);
                break;
            case 3:
                jobHistory.getDestinationEdition().setApplicationOrigine(APPLICATION_ORIGINE_1);
                jobHistory.getArchivageEdition().setDatePurge(TemporaliteUtils.getTimestamp(2017, 1, 18));
                jobHistory.getOrdoEdition().setDateDemande(TemporaliteUtils.getTimestamp(2017, 1, 18, 1, 2, 3));
                jobHistory.getOrdoEdition().setStatus(JobHistory.STATUS_DISPONIBLE);
                jobHistory.setBeanEditionId("1");
                jobHistory.getStockageEdition().setTailleFichier(3l);
                break;
            case 4:
                jobHistory.getDestinationEdition().setApplicationOrigine(APPLICATION_ORIGINE_1);
                jobHistory.getArchivageEdition().setDatePurge(TemporaliteUtils.getTimestamp(2017, 1, 18));
                jobHistory.getOrdoEdition().setDateDemande(TemporaliteUtils.getTimestamp(2017, 1, 18, 1, 2, 3));
                jobHistory.getOrdoEdition().setStatus(JobHistory.STATUS_DISPONIBLE);
                jobHistory.setBeanEditionId("2");
                jobHistory.getStockageEdition().setTailleFichier(4l);
                break;
            case 5:
                jobHistory.getDestinationEdition().setApplicationOrigine(APPLICATION_ORIGINE_1);
                jobHistory.getArchivageEdition().setDatePurge(TemporaliteUtils.getTimestamp(2017, 1, 18));
                jobHistory.getOrdoEdition().setDateDemande(TemporaliteUtils.getTimestamp(2017, 1, 18, 1, 2, 3));
                jobHistory.getOrdoEdition().setStatus(JobHistory.STATUS_DISPONIBLE);
                jobHistory.setBeanEditionId("2");
                jobHistory.getStockageEdition().setTailleFichier(5l);
                break;
            case 6:
                jobHistory.getDestinationEdition().setApplicationOrigine(APPLICATION_ORIGINE_1);
                jobHistory.getArchivageEdition().setDatePurge(TemporaliteUtils.getTimestamp(2016, 1, 18));
                jobHistory.getOrdoEdition().setDateDemande(TemporaliteUtils.getTimestamp(2016, 1, 18, 1, 2, 3));
                jobHistory.getOrdoEdition().setStatus(JobHistory.STATUS_DISPONIBLE_NON_ENVOYE);
                jobHistory.setBeanEditionId("1");
                jobHistory.getStockageEdition().setTailleFichier(6l);
                break;
            case 7:
                jobHistory.getDestinationEdition().setApplicationOrigine(APPLICATION_ORIGINE_2);
                jobHistory.getArchivageEdition().setDatePurge(TemporaliteUtils.getTimestamp(2016, 1, 18));
                jobHistory.getOrdoEdition().setDateDemande(TemporaliteUtils.getTimestamp(2016, 1, 18, 1, 2, 3));
                jobHistory.getOrdoEdition().setStatus(JobHistory.STATUS_DISPONIBLE_NON_ENVOYE);
                jobHistory.setBeanEditionId("2");
                jobHistory.getStockageEdition().setTailleFichier(7l);
                break;
            case 8:
                jobHistory.getDestinationEdition().setApplicationOrigine(APPLICATION_ORIGINE_2);
                jobHistory.getArchivageEdition().setDatePurge(TemporaliteUtils.getTimestamp(2016, 1, 18));
                jobHistory.getOrdoEdition().setDateDemande(TemporaliteUtils.getTimestamp(2016, 1, 18, 1, 2, 3));
                jobHistory.getOrdoEdition().setStatus(JobHistory.STATUS_DISPONIBLE_NON_ENVOYE);
                jobHistory.setBeanEditionId("2");
                jobHistory.getStockageEdition().setTailleFichier(8l);
                break;
            case 9:
                jobHistory.getDestinationEdition().setApplicationOrigine(APPLICATION_ORIGINE_2);
                break;
            default:
                jobHistory.getDestinationEdition().setApplicationOrigine(APPLICATION_ORIGINE_1);
                jobHistory.getArchivageEdition().setDatePurge(TemporaliteUtils.getTimestamp(2017, 1, 18));
                jobHistory.getOrdoEdition().setDateDemande(TemporaliteUtils.getTimestamp(2017, 1, 18, 1, 2, 3));
                jobHistory.getOrdoEdition().setStatus(JobHistory.STATUS_ECHEC);
                jobHistory.setBeanEditionId("1");
                jobHistory.getStockageEdition().setTailleFichier(1l);
        }

        return jobHistory;
    }

    /**
     * Méthode permettant de générer une statistique d'historique de travaux d'édition.
     *
     * @param timeStamp donnée calendaire à utiliser dans les informations de l'historique
     * @return statistique d'historique de travaux d'édition générée
     */
    private StatJobHistory getStatJobHistory(Timestamp timeStamp, long nbre)
    {
        StatJobHistory statJobHistory = new StatJobHistory();

        statJobHistory.setDatePurge(timeStamp);
        statJobHistory.setJourDemande(timeStamp);
        statJobHistory.setNombreEditions(nbre);

        return statJobHistory;
    }

    /**
     * Méthode permettant de générer une statistique d'historique de travaux d'édition.
     *
     * @param status statut à utiliser dans les informations de statistique
     * @param beanId identifiant d'édition à utiliser dans les informations de statistique
     * @param nbre nombre d'édition à insérer dans les statistiques
     * @return statistique d'historique de travaux d'édition générée
     */
    private StatJobHistory getStatJobHistory(String status, String beanId, long nbre)
    {
        return new StatJobHistory(status, beanId, nbre);
    }

    /**
     * Méthode permettant de générer une statistique d'historique de travaux d'édition.
     *
     * @param status statut à utiliser dans les informations de statistique
     * @param nbre nombre d'édition à insérer dans les statistiques
     * @return statistique d'historique de travaux d'édition générée
     */
    private StatJobHistory getStatJobHistory(String status, long nbre)
    {
        StatJobHistory statJobHistory = new StatJobHistory();
        statJobHistory.setStatus(status);
        statJobHistory.setNombreEditions(nbre);
        return statJobHistory;
    }
}