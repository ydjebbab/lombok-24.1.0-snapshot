/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 */
package fr.gouv.finances.lombok.edition.dao.impl;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.AliasToBeanResultTransformer;

import fr.gouv.finances.lombok.edition.bean.JobHistory;
import fr.gouv.finances.lombok.edition.dao.ProprieteStatistiqueJobHistoryEdition;
import fr.gouv.finances.lombok.edition.dao.StatistiqueJobHistoryDao;
import fr.gouv.finances.lombok.edition.techbean.StatJobHistory;
import fr.gouv.finances.lombok.util.base.BaseDaoImpl;
import fr.gouv.finances.lombok.util.exception.ProgrammationException;

/**
 * Implémentation Hibernate du DAO de gestion des données liées aux statistiques des travaux d'édition.
 *
 * @author amleplatinec
 * @author Christophe Breheret-Girardin
 */
public class StatistiqueJobHistoryDaoImpl extends BaseDaoImpl implements StatistiqueJobHistoryDao
{
    /**
     * Constructeur.
     */
    public StatistiqueJobHistoryDaoImpl()
    {
        super();
    }

    /** 
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.edition.dao.StatistiqueJobHistoryDao#compteJobHistoryParDatePurge(
     * java.lang.String)
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<StatJobHistory> compteJobHistoryParDatePurge(final String applicationOrigine)
    {
        // Cas des paramètres non fournis
        controlerParametres(applicationOrigine);

        // Initialisation de la recherche
        DetachedCriteria criteria = DetachedCriteria.forClass(JobHistory.class);

        // Ajout des critères
        criteria.add(Restrictions.eq(ProprieteStatistiqueJobHistoryEdition.DESTINATION_EDITION.getNom()
            + "." + ProprieteStatistiqueJobHistoryEdition.APPLICATION_ORIGINE.getNom(), applicationOrigine));

        // Création de la projection
        ProjectionList uneprojection = Projections.projectionList();
        uneprojection.add(Projections.groupProperty(
                ProprieteStatistiqueJobHistoryEdition.ARCHIVAGE_EDITION.getNom() + "." 
                    + ProprieteStatistiqueJobHistoryEdition.DATE_PURGE.getNom())
                    , ProprieteStatistiqueJobHistoryEdition.DATE_PURGE.getNom())
            .add(Projections.count(ProprieteStatistiqueJobHistoryEdition.UID.getNom())
                , ProprieteStatistiqueJobHistoryEdition.NOMBRE_EDITIONS.getNom());

        // Affectation de la projection à la requête
        criteria.setProjection(uneprojection);

        // Stratégie de transformation du résultat
        criteria.setResultTransformer(new AliasToBeanResultTransformer(StatJobHistory.class));

        // Ajout du tri
        criteria.addOrder(Order.desc(ProprieteStatistiqueJobHistoryEdition.ARCHIVAGE_EDITION.getNom()
            + "." + ProprieteStatistiqueJobHistoryEdition.DATE_PURGE.getNom()));

        // Exécution de la requête
        return (List<StatJobHistory>) getHibernateTemplate().findByCriteria(criteria);
    }

    /** 
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.edition.dao.StatistiqueJobHistoryDao#compteJobHistoryParJourDemande(
     * java.lang.String)
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<StatJobHistory> compteJobHistoryParJourDemande(final String applicationOrigine)
    {
        // Cas des paramètres non fournis
        controlerParametres(applicationOrigine);

        // Initialisation de la recherche
        DetachedCriteria criteria = DetachedCriteria.forClass(JobHistory.class);

        // Ajout des critères
        criteria.add(Restrictions.eq(ProprieteStatistiqueJobHistoryEdition.DESTINATION_EDITION.getNom()
            + "." + ProprieteStatistiqueJobHistoryEdition.APPLICATION_ORIGINE.getNom(), applicationOrigine));

        // Création de la projection
        ProjectionList uneprojection = Projections.projectionList();
        uneprojection.add(
            Projections.groupProperty(ProprieteStatistiqueJobHistoryEdition.JOUR_DEMANDE_EDITION.getNom())
                , ProprieteStatistiqueJobHistoryEdition.JOUR_DEMANDE_EDITION.getNom())
            .add(Projections.count(ProprieteStatistiqueJobHistoryEdition.UID.getNom())
                , ProprieteStatistiqueJobHistoryEdition.NOMBRE_EDITIONS.getNom());

        // Affectation de la projection à la requête
        criteria.setProjection(uneprojection);

        // Stratégie de transformation du résultat
        criteria.setResultTransformer(new AliasToBeanResultTransformer(StatJobHistory.class));

        // Ajout du tri
        criteria.addOrder(Order.desc(ProprieteStatistiqueJobHistoryEdition.JOUR_DEMANDE_EDITION.getNom()));

        // Exécution de la requête
        return (List<StatJobHistory>) getHibernateTemplate().findByCriteria(criteria);
    }

    /** 
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.edition.dao.StatistiqueJobHistoryDao#compteJobHistoryParStatus(
     * java.lang.String)
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<StatJobHistory> compteJobHistoryParStatus(final String applicationOrigine)
    {
        // Cas des paramètres non fournis
        controlerParametres(applicationOrigine);

        // Initialisation de la recherche
        DetachedCriteria criteria = DetachedCriteria.forClass(JobHistory.class);

        // Ajout des critères
        criteria.add(Restrictions.eq(ProprieteStatistiqueJobHistoryEdition.DESTINATION_EDITION.getNom()
            + "." + ProprieteStatistiqueJobHistoryEdition.APPLICATION_ORIGINE.getNom(), applicationOrigine));

        // Création de la projection
        ProjectionList uneprojection = Projections.projectionList();
        uneprojection.add(Projections.groupProperty(
                ProprieteStatistiqueJobHistoryEdition.ORDONNANCEMENT_EDITION.getNom()
                + "." + ProprieteStatistiqueJobHistoryEdition.STATUT_EDITION.getNom())
                , ProprieteStatistiqueJobHistoryEdition.STATUT_EDITION.getNom())
            .add(Projections.count(ProprieteStatistiqueJobHistoryEdition.UID.getNom())
                , ProprieteStatistiqueJobHistoryEdition.NOMBRE_EDITIONS.getNom());

        // Affectation de la projection à la requête
        criteria.setProjection(uneprojection);

        // Stratégie de transformation du résultat
        criteria.setResultTransformer(new AliasToBeanResultTransformer(StatJobHistory.class));

        // Exécution de la requête
        return (List<StatJobHistory>) getHibernateTemplate().findByCriteria(criteria);
    }

    /** 
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.edition.dao.StatistiqueJobHistoryDao#compteJobHistoryParTypeEtStatus(
     * java.lang.String)
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<StatJobHistory> compteJobHistoryParTypeEtStatus(final String applicationOrigine)
    {
        // Cas des paramètres non fournis
        controlerParametres(applicationOrigine);

        // Initialisation de la recherche
        DetachedCriteria criteria = DetachedCriteria.forClass(JobHistory.class);

        // Ajout des critères
        criteria.add(Restrictions.eq(ProprieteStatistiqueJobHistoryEdition.DESTINATION_EDITION.getNom()
            + "." + ProprieteStatistiqueJobHistoryEdition.APPLICATION_ORIGINE.getNom(), applicationOrigine));

        // Création de la projection
        ProjectionList projections = Projections.projectionList();
        projections.add(Projections.groupProperty(
            ProprieteStatistiqueJobHistoryEdition.ORDONNANCEMENT_EDITION.getNom()
                + "." + ProprieteStatistiqueJobHistoryEdition.STATUT_EDITION.getNom())
                ,  ProprieteStatistiqueJobHistoryEdition.STATUT_EDITION.getNom())
            .add(Projections.groupProperty(ProprieteStatistiqueJobHistoryEdition.ID_EDITION.getNom())
                    , ProprieteStatistiqueJobHistoryEdition.ID_EDITION.getNom())
            .add(Projections.count(ProprieteStatistiqueJobHistoryEdition.UID.getNom()),
                ProprieteStatistiqueJobHistoryEdition.NOMBRE_EDITIONS.getNom());

        // Affectation dess projection à la requête
        criteria.setProjection(projections);

        // Stratégie de transformation du résultat
        criteria.setResultTransformer(new AliasToBeanResultTransformer(StatJobHistory.class));

        // Exécution de la requête
        return (List<StatJobHistory>) getHibernateTemplate().findByCriteria(criteria);
    }

    /** 
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.edition.dao.StatistiqueJobHistoryDao#compteTailleEditionParType(
     * java.lang.String)
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<StatJobHistory> compteTailleEditionParType(final String applicationOrigine)
    {
        // Cas des paramètres non fournis
        controlerParametres(applicationOrigine);

        // Initialisation de la recherche
        DetachedCriteria criteria = DetachedCriteria.forClass(JobHistory.class);

        // Ajout des critères
        criteria.add(Restrictions.eq(ProprieteStatistiqueJobHistoryEdition.DESTINATION_EDITION.getNom()
            + "." + ProprieteStatistiqueJobHistoryEdition.APPLICATION_ORIGINE.getNom(), applicationOrigine));
        criteria.add(Restrictions.or(Restrictions.eq(ProprieteStatistiqueJobHistoryEdition.ORDONNANCEMENT_EDITION.getNom()
            + "." + ProprieteStatistiqueJobHistoryEdition.STATUT_EDITION.getNom(), JobHistory.STATUS_DISPONIBLE)
            , Restrictions.eq(ProprieteStatistiqueJobHistoryEdition.ORDONNANCEMENT_EDITION.getNom()
                + "." + ProprieteStatistiqueJobHistoryEdition.STATUT_EDITION.getNom()
                , JobHistory.STATUS_DISPONIBLE_NON_ENVOYE)));

        // Création de la projection
        ProjectionList uneprojection = Projections.projectionList();
        uneprojection.add(Projections.groupProperty(
                ProprieteStatistiqueJobHistoryEdition.ORDONNANCEMENT_EDITION.getNom()
                + "." + ProprieteStatistiqueJobHistoryEdition.STATUT_EDITION.getNom())
                , ProprieteStatistiqueJobHistoryEdition.STATUT_EDITION.getNom())
            .add(Projections.groupProperty(ProprieteStatistiqueJobHistoryEdition.ID_EDITION.getNom())
                , ProprieteStatistiqueJobHistoryEdition.ID_EDITION.getNom())
            .add(Projections.count(ProprieteStatistiqueJobHistoryEdition.UID.getNom()),
                    ProprieteStatistiqueJobHistoryEdition.NOMBRE_EDITIONS.getNom())
            .add(Projections.sum(ProprieteStatistiqueJobHistoryEdition.STOCKAGE_EDITION.getNom()
                + "." + ProprieteStatistiqueJobHistoryEdition.TAILLE_FICHIER_EDITION.getNom())
                , ProprieteStatistiqueJobHistoryEdition.TAILLE_TOTALE_FICHIER.getNom())
            .add(Projections.avg(ProprieteStatistiqueJobHistoryEdition.STOCKAGE_EDITION.getNom()
                + "." + ProprieteStatistiqueJobHistoryEdition.TAILLE_FICHIER_EDITION.getNom())
                , ProprieteStatistiqueJobHistoryEdition.TAILLE_MOYENNE_FICHIER.getNom());

        // Affectation de la projection à la requête
        criteria.setProjection(uneprojection);

        // Stratégie de transformation du résultat
        criteria.setResultTransformer(new AliasToBeanResultTransformer(StatJobHistory.class));

        // Exécution de la requête
        return (List<StatJobHistory>) getHibernateTemplate().findByCriteria(criteria);
    }

    private void controlerParametres(final String applicationOrigine)
    {
        if (StringUtils.isEmpty(applicationOrigine))
        {
            throw new ProgrammationException("Aucune application d'origine n'a été fournie");
        }
    }
}