/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 */
package fr.gouv.finances.lombok.edition.dao.impl;

import java.util.Date;

import org.hibernate.CacheMode;
import org.hibernate.ScrollMode;
import org.hibernate.criterion.Restrictions;

import fr.gouv.finances.lombok.edition.bean.JobHistory;
import fr.gouv.finances.lombok.edition.dao.JobHistoryBatchDao;
import fr.gouv.finances.lombok.edition.dao.ProprieteJobHistory;
import fr.gouv.finances.lombok.edition.util.Control;
import fr.gouv.finances.lombok.util.base.BaseDaoImpl;
import fr.gouv.finances.lombok.util.exception.ProgrammationException;
import fr.gouv.finances.lombok.util.hibernate.DgcpScrollIteratorImpl;
import fr.gouv.finances.lombok.util.persistance.ScrollIterator;

/**
 * Implémentation Hibernate du DAO de gestion des données, d'historique des travaux d'édition, en batch.
 *
 * @author amleplatinec
 * @author Christophe Breheret-Girardin
 */
public class JobHistoryBatchDaoImpl extends BaseDaoImpl implements JobHistoryBatchDao
{
    /** Constante de la propriété permettant d'accéder à l'application liée à l'édition */
    private static final String APPLICATION_ORIGINE
        = String.join(".", ProprieteJobHistory.DESTINATION_EDITION.getNom()
            , ProprieteJobHistory.APPLICATION_ORIGINE.getNom());

    /** Constante de la propriété permettant d'accéder à la nouveauté de l'édition */
    private static final String STATUT
        = String.join(".", ProprieteJobHistory.ORDONNANCEMENT_EDITION.getNom()
            , ProprieteJobHistory.STATUT.getNom());

    /** Constante de la propriété permettant d'accéder à la date de purge de l'édition */
    private static final String DATE_PURGE
        = String.join(".", ProprieteJobHistory.ARCHIVAGE_EDITION.getNom()
            , ProprieteJobHistory.DATE_PURGE.getNom());

    /**
     * Constructeur.
     */
    public JobHistoryBatchDaoImpl()
    {
        super();
    }

    /** 
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.edition.dao.JobHistoryBatchDao#listeEditionAPurgerIterator(int, java.util.Date, java.lang.String)
     */
    @Override
    public ScrollIterator listeEditionAPurgerIterator(int nombreOccurences, final Date dateCourante,
        final String applicationOrigine)
    {
        // Contrôle des paramètres
        Control.assertApplicationNonRenseignee(applicationOrigine);
        if (dateCourante == null)
        {
            throw new ProgrammationException("La date de recherche de l'historique des travaux d'édition"
                + " n'a pas été fournie");
        }

        // Ouverture d'une recherche en mode curseur
        // sur les éditions qui ne sont pas en cours ou en attente, dont la date de purge est antérieure à celle fournie
        // et liées à l'application fournie
        return new DgcpScrollIteratorImpl(getHibernateTemplate().execute(
            session -> session.createCriteria(JobHistory.class)
                        .add(Restrictions.eq(APPLICATION_ORIGINE, applicationOrigine))
                        .add(Restrictions.le(DATE_PURGE, dateCourante))
                        .add(Restrictions.ne(STATUT, JobHistory.STATUS_EN_COURS_EXECUTION))
                        .add(Restrictions.ne(STATUT, JobHistory.STATUS_EN_ATTENTE_EXECUTION))
                        .setCacheMode(CacheMode.IGNORE)
                        .setFetchSize(nombreOccurences)
                        .scroll(ScrollMode.FORWARD_ONLY)));
    }

    /** 
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.edition.dao.JobHistoryBatchDao#listeEditionsDisponiblesIterator(
     * int, java.lang.String)
     */
    @Override
    public ScrollIterator listeEditionsDisponiblesIterator(int nombreOccurences, final String applicationOrigine)
    {
        // Contrôle des paramètres
        Control.assertApplicationNonRenseignee(applicationOrigine);

        // Ouverture d'une recherche en mode curseur
        // sur les éditions en disponibles ou non envoyés, liées à l'application fournie
        return new DgcpScrollIteratorImpl(getHibernateTemplate().execute(
            session -> session.createCriteria(JobHistory.class)
                        .add(Restrictions.eq(APPLICATION_ORIGINE, applicationOrigine))
                        .add(Restrictions.or(Restrictions.eq(STATUT, JobHistory.STATUS_DISPONIBLE),
                            Restrictions.eq(STATUT, JobHistory.STATUS_DISPONIBLE_NON_ENVOYE)))
                        .setCacheMode(CacheMode.IGNORE)
                        .setFetchSize(nombreOccurences)
                        .scroll(ScrollMode.FORWARD_ONLY)));
    }

    /** 
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.edition.dao.JobHistoryBatchDao#listeEditionsEnEchecIterator(
     * int, java.lang.String)
     */
    @Override
    public ScrollIterator listeEditionsEnEchecIterator(int nombreOccurences, final String applicationOrigine)
    {
        // Contrôle des paramètres
        Control.assertApplicationNonRenseignee(applicationOrigine);

        // Ouverture d'une recherche en mode curseur
        // sur les éditions en échec, liées à l'application fournie
        return new DgcpScrollIteratorImpl(getHibernateTemplate().execute(
            session -> session.createCriteria(JobHistory.class)
                        .add(Restrictions.eq(APPLICATION_ORIGINE, applicationOrigine))
                        .add(Restrictions.eq(STATUT, JobHistory.STATUS_ECHEC))
                        .setCacheMode(CacheMode.IGNORE)
                        .setFetchSize(nombreOccurences)
                        .scroll(ScrollMode.FORWARD_ONLY)));
    }
}