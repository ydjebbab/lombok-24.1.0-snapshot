/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 */
package fr.gouv.finances.lombok.edition.dao.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.CacheMode;
import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.orm.hibernate5.HibernateTemplate;

import fr.gouv.finances.lombok.edition.bean.TrcPurgeEdition;
import fr.gouv.finances.lombok.edition.dao.ProprieteTrcPurgeEdition;
import fr.gouv.finances.lombok.edition.dao.TrcPurgeEditionDao;
import fr.gouv.finances.lombok.edition.util.Control;
import fr.gouv.finances.lombok.util.base.BaseDaoImpl;

/**
 * Implémentation Hibernate du DAO de gestion des données des traces de purge d'édition
 *
 * @author amleplatinec
 * @author Christophe Breheret-Girardin
 */
public class TrcPurgeEditionDaoImpl extends BaseDaoImpl implements TrcPurgeEditionDao
{
    /** Initialisation de la journalisation. */
    protected static final Logger LOGGER = LoggerFactory.getLogger(TrcPurgeEditionDaoImpl.class);

    /**
     * Constructeur.
     */
    public TrcPurgeEditionDaoImpl()
    {
        super();
    }

    /**
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.edition.dao.TrcPurgeEditionDao#checkExistencePurgeDateIdentique(fr.gouv.finances.lombok.edition.bean.TrcPurgeEdition)
     */
    @Override
    public boolean checkExistencePurgeDateIdentique(TrcPurgeEdition trcPurgeEdition)
    {
        // Journalisation de la recherche
        LOGGER.debug("Recherche d'existence d'une trace de purge d'édition avec une date identique que '{}'"
            , trcPurgeEdition);

        // L'entité doit être renseignée
        Control.assertTrcPurgeEditionNotNull(trcPurgeEdition);

        // Exécution de la requête et retour du résultat d'existence
        return (Long) getHibernateTemplate().execute
            (
                session -> 
                {
                    // Paramétrage de la recherche
                    Criteria criteria = session.createCriteria(TrcPurgeEdition.class);

                    // Ajout des critères
                    criteria.add(Restrictions.eq(ProprieteTrcPurgeEdition.DATE_DEBUT_PURGE.getNom()
                            , trcPurgeEdition.getDateDebutPurge()))
                        .add(Restrictions.eq(ProprieteTrcPurgeEdition.APPLICATION_ORIGINE.getNom()
                            , trcPurgeEdition.getApplicationOrigine()));
                    if (trcPurgeEdition.getId() != null)
                    {
                        criteria.add(Restrictions.not(Restrictions.idEq(trcPurgeEdition.getId())));
                    }

                    // Ajout d'un paramétrage pour compter les résultats
                    criteria.setProjection(Projections.rowCount());
                    // Récupération du résultat
                    return criteria.uniqueResult();
                }
            // Existe-il au moins une purge d'édition satisfaisant les critères ?
            ) > 0;
    }

    /**
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.edition.dao.TrcPurgeEditionDao#deleteTrcPurgeEdition(fr.gouv.finances.lombok.edition.bean.TrcPurgeEdition)
     */
    @Override
    public void deleteTrcPurgeEdition(TrcPurgeEdition trcPurgeEdition)
    {
        // Journalisation de la suppression
        LOGGER.debug("Suppression de la trace de purge d'édition '{}'", trcPurgeEdition);

        // Exécution de la suppression
        getHibernateTemplate().delete(trcPurgeEdition);
    }

    /**
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.edition.dao.TrcPurgeEditionDao#deleteTrcPurgeEditions(java.util.Collection)
     */
    @Override
    public void deleteTrcPurgeEditions(Collection<TrcPurgeEdition> lesTrcPurgesEditionSupp)
    {
        // Journalisation de la suppression
        LOGGER.trace("Suppression de plusieurs trace de purge d'édition");

        // La requête n'est exécutée que si le paramétre est renseigné
        if (lesTrcPurgesEditionSupp != null && !lesTrcPurgesEditionSupp.isEmpty())
        {
            getSessionFactory().getCurrentSession().setCacheMode(CacheMode.IGNORE);

            // Récupération d'une nouvelle instance de l'utilitaire Spring permettant de faciliter l'utilisation
            // d'Hibernate, afin de ne pas conserver les modifications du paramétrage Hibernate, ci-dessous, après
            // la fin du traitement
            HibernateTemplate hibernateTemplate = getHibernateTemplate(true);
            hibernateTemplate.setCacheQueries(false);

            // Exécution des suppressions
            hibernateTemplate.deleteAll(lesTrcPurgesEditionSupp);
            hibernateTemplate.flush();
            hibernateTemplate.clear();
        }
    }

    /**
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.edition.dao.TrcPurgeEditionDao#findLesTrcPurgeEditionEnCoursAnterieuresAUneDate(java.util.Date,
     *      java.lang.String)
     */
    @Override
    @SuppressWarnings("unchecked")
    public List<TrcPurgeEdition> findLesTrcPurgeEditionEnCoursAnterieuresAUneDate(Date dateDebutPurge,
        String applicationOrigine)
    {
        // Journalisation de la recherche
        LOGGER.debug("Recherche de trace de purge d'édition sur l'application '{}' antérieure à la date '{}'"
            , applicationOrigine, dateDebutPurge);

        // Paramétrage de la recherche
        DetachedCriteria criteria = DetachedCriteria.forClass(TrcPurgeEdition.class);

        // Paramétrage des critères
        criteria.add(Restrictions.eq(ProprieteTrcPurgeEdition.APPLICATION_ORIGINE.getNom(), applicationOrigine));
        criteria.add(Restrictions.eq(ProprieteTrcPurgeEdition.ETAT_PURGE.getNom()
            , TrcPurgeEdition.EN_COURS_EXECUTION));
        criteria.add(Restrictions.lt(ProprieteTrcPurgeEdition.DATE_DEBUT_PURGE.getNom(), dateDebutPurge));

        // Exécution de la recherche
        return (List<TrcPurgeEdition>) getHibernateTemplate().findByCriteria(criteria);
    }

    /**
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.edition.dao.TrcPurgeEditionDao#findLesTrcPurgeEditionPourUneDate(java.util.Date,
     *      java.lang.String, int)
     */
    @Override
    @SuppressWarnings("unchecked")
    public List<TrcPurgeEdition> findLesTrcPurgeEditionPourUneDate(Date dateDebutPurge
        , String applicationOrigine, int max)
    {
        // Journalisation de la recherche
        LOGGER.debug("Recherche de trace de purge d'édition sur l'application '{}' pour la date '{}' (max '{}' éditions)"
            , applicationOrigine, dateDebutPurge, max);

        // Si un des paramétres est manquant, la requête n'est pas exécutée
        if (dateDebutPurge == null || StringUtils.isEmpty(applicationOrigine))
        {
            return new ArrayList<>();
        }

        // Calcul de la borne supérieure à appliquer à la recherche
        Calendar cal = Calendar.getInstance(Locale.FRANCE);
        cal.setTime(dateDebutPurge);
        cal.add(Calendar.DATE, 1);
        Date dateDebutPurgePlusUn = cal.getTime();

        // Récupération d'une nouvelle instance de l'utilitaire Spring permettant de faciliter l'utilisation
        // d'Hibernate, afin de ne pas conserver les modifications du paramétrage Hibernate, ci-dessous, après
        // la fin du traitement
        HibernateTemplate hibernateTemplate = getHibernateTemplate(true);

        // Modification du paramétrage Hibernate pour limiter le nombre d'éditions retournées
        hibernateTemplate.setMaxResults(max);

        // Paramétrage de la recherche
        DetachedCriteria criteria = DetachedCriteria.forClass(TrcPurgeEdition.class);

        // Paramétrage des critères
        criteria.add(Restrictions.eq(ProprieteTrcPurgeEdition.APPLICATION_ORIGINE.getNom(), applicationOrigine));
        criteria.add(Restrictions.ge(ProprieteTrcPurgeEdition.DATE_DEBUT_PURGE.getNom(), dateDebutPurge));
        criteria.add(Restrictions.lt(ProprieteTrcPurgeEdition.DATE_DEBUT_PURGE.getNom(), dateDebutPurgePlusUn));

        // Paramétrage du tri
        criteria.addOrder(Order.desc(ProprieteTrcPurgeEdition.DATE_DEBUT_PURGE.getNom()));

        // Exécution de la recherche
        return (List<TrcPurgeEdition>) hibernateTemplate.findByCriteria(criteria);
    }

    /**
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.edition.dao.TrcPurgeEditionDao#findLesTrcPurgeEditionPourUnInstantDemarrage(java.util.Date,
     *      java.lang.String)
     */
    @Override
    @SuppressWarnings("unchecked")
    public List<TrcPurgeEdition> findLesTrcPurgeEditionPourUnInstantDemarrage(Date instantDemarrage,
        String applicationOrigine)
    {
        // Journalisation de la recherche
        LOGGER.debug("Recherche de trace de purge d'édition sur l'application '{}' pour un instant de démarrage '{}'"
            , applicationOrigine, instantDemarrage);

        // Paramétrage de la recherche
        DetachedCriteria criteria = DetachedCriteria.forClass(TrcPurgeEdition.class);

        // Paramétrage des critères
        criteria.add(Restrictions.eq(ProprieteTrcPurgeEdition.APPLICATION_ORIGINE.getNom(), applicationOrigine));
        criteria.add(Restrictions.eq(ProprieteTrcPurgeEdition.DATE_DEBUT_PURGE.getNom(), instantDemarrage));

        // Exécution de la recherche
        return (List<TrcPurgeEdition>) getHibernateTemplate().findByCriteria(criteria);
    }

    /**
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.edition.dao.TrcPurgeEditionDao#findTousLesTrcPurgeEdition(java.lang.String, int)
     */
    @Override
    @SuppressWarnings("unchecked")
    public List<TrcPurgeEdition> findTousLesTrcPurgeEdition(String applicationOrigine, int max)
    {
        // Journalisation de la recherche
        LOGGER.debug("Recherche de toutes les traces de purge d'éditionn sur l'application '{}' "
            + "(max '{}' éditions)", applicationOrigine, max);

        // Si le paramétre est manquant, la requête n'est pas exécutée
        if (StringUtils.isEmpty(applicationOrigine))
        {
            return new ArrayList<>();
        }

        // Récupération d'une nouvelle instance de l'utilitaire Spring permettant de faciliter l'utilisation
        // d'Hibernate, afin de ne pas conserver les modifications du paramétrage Hibernate, ci-dessous, après
        // la fin du traitement
        HibernateTemplate hibernateTemplate = getHibernateTemplate(true);

        // Modification du paramétrage Hibernate pour limiter le nombre d'éditions retournée
        hibernateTemplate.setMaxResults(max);

        // Paramétrage de la recherche
        DetachedCriteria criteria = DetachedCriteria.forClass(TrcPurgeEdition.class);

        // Paramétrage des critères
        criteria.add(Restrictions.eq(ProprieteTrcPurgeEdition.APPLICATION_ORIGINE.getNom(), applicationOrigine));
        criteria.addOrder(Order.desc(ProprieteTrcPurgeEdition.DATE_DEBUT_PURGE.getNom()));

        // Exécution de la recherche
        return (List<TrcPurgeEdition>) hibernateTemplate.findByCriteria(criteria);
    }

    /**
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.edition.dao.TrcPurgeEditionDao#findTrcPurgeEditionEnCours(java.lang.String)
     */
    @Override
    @SuppressWarnings("unchecked")
    public List<TrcPurgeEdition> findTrcPurgeEditionEnCours(String applicationOrigine)
    {
        // Journalisation de la recherche
        LOGGER.debug("Recherche de toutes les traces de purge d'édition sur l'application '{}' en cours"
            , applicationOrigine);

        // Paramétrage de la recherche
        DetachedCriteria criteria = DetachedCriteria.forClass(TrcPurgeEdition.class);

        // Paramétrage des critères
        criteria.add(Restrictions.eq(ProprieteTrcPurgeEdition.APPLICATION_ORIGINE.getNom(), applicationOrigine));
        criteria.add(Restrictions.eq(ProprieteTrcPurgeEdition.ETAT_PURGE.getNom()
            , TrcPurgeEdition.EN_COURS_EXECUTION));

        // Exécution de la recherche
        return (List<TrcPurgeEdition>) getHibernateTemplate().findByCriteria(criteria);
    }

    /**
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.edition.dao.TrcPurgeEditionDao#findTrcPurgeEditionParPeriodePurge(java.util.Date,
     *      java.util.Date, java.lang.String, int)
     */
    @Override
    @SuppressWarnings("unchecked")
    public List<TrcPurgeEdition> findTrcPurgeEditionParPeriodePurge(Date debutPeriodePurge, Date finPeriodePurge,
        String applicationOrigine, int max)
    {
        // Journalisation de la recherche
        LOGGER.debug("Recherche des traces de purge d'édition sur l'application '{}' entre '{}' et '{}' (max '{}' éditions)"
            , applicationOrigine, debutPeriodePurge, finPeriodePurge, max);

        // Récupération d'une nouvelle instance de l'utilitaire Spring permettant de faciliter l'utilisation
        // d'Hibernate, afin de ne pas conserver les modifications du paramétrage Hibernate, ci-dessous, après
        // la fin du traitement
        HibernateTemplate hibernateTemplate = getHibernateTemplate(true);

        // Modification du paramétrage Hibernate pour limiter le nombre d'éditions retournée
        hibernateTemplate.setMaxResults(max);

        // Paramétrage de la recherche
        DetachedCriteria criteria = DetachedCriteria.forClass(TrcPurgeEdition.class);

        // Paramétrage des critères
        criteria.add(Restrictions.ge(ProprieteTrcPurgeEdition.DATE_DEBUT_PURGE.getNom(), debutPeriodePurge));
        criteria.add(Restrictions.le(ProprieteTrcPurgeEdition.DATE_DEBUT_PURGE.getNom(), finPeriodePurge));
        criteria.add(Restrictions.eq(ProprieteTrcPurgeEdition.APPLICATION_ORIGINE.getNom(), applicationOrigine));

        // Paramétrage du tri
        criteria.addOrder(Order.desc(ProprieteTrcPurgeEdition.DATE_DEBUT_PURGE.getNom()));

        // Exécution de la recherche
        return (List<TrcPurgeEdition>) hibernateTemplate.findByCriteria(criteria);
    }

    /**
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.edition.dao.TrcPurgeEditionDao#isUnBatchPurgeEnCoursExecution(java.lang.String)
     */
    @Override
    public boolean isUnBatchPurgeEnCoursExecution(String applicationOrigine)
    {
        // Journalisation de la recherche
        LOGGER.debug("Recherche si un  batch concernant les traces de purge d'édition sur l'application '{}'"
            + "est encore d'exécution", applicationOrigine);

        // Exécution de la recherche
        return getHibernateTemplate().execute
            (
                session -> (Long) session.createCriteria(TrcPurgeEdition.class)
                        .add(Restrictions.eq(ProprieteTrcPurgeEdition.APPLICATION_ORIGINE.getNom()
                            , applicationOrigine))
                        .add(Restrictions.eq(ProprieteTrcPurgeEdition.ETAT_PURGE.getNom()
                            , TrcPurgeEdition.EN_COURS_EXECUTION))
                        .setProjection(Projections.rowCount())
                        .uniqueResult()
            ) > 0;
    }

    /**
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.edition.dao.TrcPurgeEditionDao#saveTrcPurgeEdition(fr.gouv.finances.lombok.edition.bean.TrcPurgeEdition)
     */
    @Override
    public void saveTrcPurgeEdition(TrcPurgeEdition trcPurgeEdition)
    {
        // Journalisation de la mise à jour
        LOGGER.debug("Insertion de la trace de purge d'édition '{}'", trcPurgeEdition);

        // Exécution de la mise à jour
        this.saveObject(trcPurgeEdition);
    }
}
