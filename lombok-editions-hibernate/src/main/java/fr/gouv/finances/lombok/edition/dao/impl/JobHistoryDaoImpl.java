/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 */
package fr.gouv.finances.lombok.edition.dao.impl;

import java.io.File;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.hibernate.CacheMode;
import org.hibernate.FetchMode;
import org.hibernate.Hibernate;
import org.hibernate.LockOptions;
import org.hibernate.criterion.Conjunction;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.hibernate.transform.AliasToBeanResultTransformer;
import org.hibernate.transform.DistinctRootEntityResultTransformer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.orm.hibernate5.HibernateTemplate;

import fr.gouv.finances.lombok.edition.bean.ContenuEdition;
import fr.gouv.finances.lombok.edition.bean.FiltreEdition;
import fr.gouv.finances.lombok.edition.bean.JobHistory;
import fr.gouv.finances.lombok.edition.bean.ProfilDesti;
import fr.gouv.finances.lombok.edition.dao.JobHistoryDao;
import fr.gouv.finances.lombok.edition.dao.ProprieteJobHistory;
import fr.gouv.finances.lombok.edition.util.Control;
import fr.gouv.finances.lombok.edition.util.FileUtils;
import fr.gouv.finances.lombok.edition.util.NombreMaxEdition;
import fr.gouv.finances.lombok.util.exception.ProgrammationException;

/**
 * Implémentation Hibernate du DAO d'historique des travaux d'édition.
 *
 * @author amleplatinec
 * @author Christophe Breheret-Girardin
 */
public class JobHistoryDaoImpl extends JobHistoryBatchDaoImpl implements JobHistoryDao
{
    /** Initialisation de la journalisation. */
    protected static final Logger LOGGER = LoggerFactory.getLogger(JobHistoryDaoImpl.class);

    /** Constante du message de journalisation des recherches */
    protected static final String JOURNALISATION_RECHERCHE = "Recherche des historiques des travaux d'édition, ";

    /** Constante de la propriété permettant d'accéder à l'application liée à l'édition */
    private static final String APPLICATION_ORIGINE
        = String.join(".", ProprieteJobHistory.DESTINATION_EDITION.getNom()
            , ProprieteJobHistory.APPLICATION_ORIGINE.getNom());

    /** Constante de la propriété permettant d'accéder au propriétaire de l'édition */
    private static final String UID_PROPRIETAIRE
        = String.join(".", ProprieteJobHistory.DESTINATION_EDITION.getNom()
            , ProprieteJobHistory.UID_PROPRIETAIRE.getNom());

    /** Constante de la propriété permettant d'accéder au type de destinataire de l'édition */
    private static final String MONO_DESTINATAIRE
        = String.join(".", ProprieteJobHistory.DESTINATION_EDITION.getNom()
            , ProprieteJobHistory.MONO_DESTINATAIRE.getNom());

    /** Constante de la propriété permettant d'accéder à la date demandée de l'édition */
    private static final String DATE_DEMANDE
        = String.join(".", ProprieteJobHistory.ORDONNANCEMENT_EDITION.getNom()
            , ProprieteJobHistory.DATE_DEMANDE.getNom());

    /** Constante de la propriété permettant d'accéder à la nouveauté de l'édition */
    private static final String NOUVELLE_EDITION
        = String.join(".", ProprieteJobHistory.DESTINATION_EDITION.getNom()
            , ProprieteJobHistory.NOUVELLE_EDITION.getNom());
    
    /** Constante de la propriété permettant d'accéder à la nouveauté de l'édition */
    private static final String STATUT
        = String.join(".", ProprieteJobHistory.ORDONNANCEMENT_EDITION.getNom()
            , ProprieteJobHistory.STATUT.getNom());

    /**
     * Constructeur.
     */
    public JobHistoryDaoImpl()
    {
        super();
    }

    /** 
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.edition.dao.JobHistoryDao#saveJobHistory(fr.gouv.finances.lombok.edition.bean.JobHistory)
     */
    @Override
    public void saveJobHistory(JobHistory jobHistory)
    {
        // Journalisation de la sauvegarde
        LOGGER.trace("Sauvegarde d'un historique des travaux d'édition");

        // Exécution de la sauvegarde
        if (jobHistory.getStockageEdition() != null
            && jobHistory.getStockageEdition().getLeContenuEdition() != null)
        {
            this.saveObject(jobHistory.getStockageEdition().getLeContenuEdition());
        }
        this.saveObject(jobHistory);
    }

    /** 
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.edition.dao.JobHistoryDao#chargerContenuEditionToBytes(
     * fr.gouv.finances.lombok.edition.bean.JobHistory)
     */
    @Override
    public JobHistory chargerContenuEditionToBytes(JobHistory jobHistory)
    {
        // Exécution du chargement du contenu de l'édition
        chargerContenu(jobHistory);

        // Récupération du contenu sous forme de tableau d'octet et alimentation de l'historique
        jobHistory.getStockageEdition().getLeContenuEdition().setDataByte(
            jobHistory.getStockageEdition().getLeContenuEdition().getData());
        return jobHistory;
    }

    /** 
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.edition.dao.JobHistoryDao#chargerContenuEditionToFile(
     * fr.gouv.finances.lombok.edition.bean.JobHistory)
     */
    @Override
    public void chargerContenuEditionToFile(JobHistory jobHistory)
    {
        // Exécution du chargement du contenu de l'édition
        chargerContenu(jobHistory);

        // Récupération du contenu sous forme de fichier et alimentation de l'historique
        jobHistory.getStockageEdition().getLeContenuEdition().setDataFile(
            FileUtils.getFichier(jobHistory.getStockageEdition().getLeContenuEdition().getData()));
    }

    /**
     * Méthode permettant de charger le contenu d'une édition.
     *
     * @param jobHistory Historique des travaux d'édition dont il faut charger le contenu
     */
    private void chargerContenu(JobHistory jobHistory)
    {
        // L'historique des travaux d'édition doit avoir été fourni
        Control.assertJobHistoryNotNull(jobHistory);

        // L'historique des travaux d'édition doit avoir été chargé
        Control.assertJobHistoryCharge(jobHistory);

        // L'historique des travaux d'édition doit être au statut disponible
        Control.assertEditionDisponible(jobHistory);

        // Journalisation du chargement
        LOGGER.debug("Chargement du contenu de l'historique des travaux d'édition (UUID #{})"
            , jobHistory.getEditionUuid());
        
        // Exécution du chargement du contenu de l'édition
        chargerContenuEdition(jobHistory);
    }

    /** 
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.edition.dao.JobHistoryDao#chargerContenuEdition(fr.gouv.finances.lombok.edition.bean.JobHistory)
     */
    @Override
    public JobHistory chargerContenuEdition(JobHistory jobHistory)
    {
        // Journalisation du chargement
        LOGGER.trace("Chargement du contenu de l'historique des travaux d'édition");

        // L'historique des travaux d'édition doit avoir été fourni
        Control.assertJobHistoryNotNull(jobHistory);

        // L'historique des travaux d'édition doit avoir été chargé
        Control.assertJobHistoryCharge(jobHistory);

        // Des informations de stockage doivent être présentes
        if (jobHistory.getStockageEdition() == null)
        {
            throw new ProgrammationException("Pas d'information de stockage liée à "
                + "l'historique d'édition fournie (UUID #" + jobHistory.getEditionUuid() + ") est vide.");
        }

        // Gestion du contenu de l'édition
        return getHibernateTemplate().execute(
            session -> 
            {
                // Récupération du contenu
                ContenuEdition contenu = jobHistory.getStockageEdition().getLeContenuEdition();
                session.buildLockRequest(LockOptions.READ).lock(jobHistory);

                // Chargement du contenu si celui-ci n'est pas initialisé
                if (!Hibernate.isInitialized(contenu))
                {
                    Hibernate.initialize(contenu);
                }
                if (contenu == null || contenu.getData() == null)
                {
                    throw new ProgrammationException("Le contenu de l'historique d'édition fournie "
                        + "(UUID #" + jobHistory.getEditionUuid() + ") est vide.");
                }

                return jobHistory;
            }
        );
    }

    /** 
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.edition.dao.JobHistoryDao#deleteJobHistory(
     * fr.gouv.finances.lombok.edition.bean.JobHistory)
     */
    @Override
    public void deleteJobHistory(JobHistory jobHistory)
    {
        // Le paramètre doit être présent
        Control.assertJobHistoryNotNull(jobHistory);

        // L'historique des travaux d'édition doit avoir été chargé
        Control.assertJobHistoryCharge(jobHistory);

        // Journalisation de la mise à jour
        LOGGER.debug("Suppression de l'historique des travaux d'édition (UUID #{})"
            , jobHistory.getEditionUuid());
        
        // Exécution de la suppression
        getHibernateTemplate().delete(jobHistory);
    }

    /** 
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.edition.dao.JobHistoryDao#deleteJobsHistories(java.util.List)
     */
    @Override
    public void deleteJobsHistories(List<JobHistory> historiques)
    {
        // La liste doit avoir été fournie
        Control.assertListJobHistoryNotNull(historiques);

        // Journalisation de la mise à jour
        LOGGER.debug("Suppression de {} historique(s) des travaux d'édition", historiques.size());
        
        // Chaque historique des travaux d'édition doit avoir été chargé
        historiques.stream().forEach(Control::assertJobHistoryCharge);

        // Récupération d'une nouvelle instance de l'utilitaire Spring permettant de faciliter l'utilisation
        // d'Hibernate, afin de ne pas conserver les modifications du paramétrage, ci-dessous, après
        // la fin du traitement
        HibernateTemplate hibernateTemplate = getHibernateTemplate(true);

        // Paramétrage du cache
        getSessionFactory().getCurrentSession().setCacheMode(CacheMode.IGNORE);
        hibernateTemplate.setCacheQueries(false);

        // Suppression des historiques de travaux d'édition
        hibernateTemplate.deleteAll(historiques);
        hibernateTemplate.flush();
        hibernateTemplate.clear();
    }

    /** 
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.edition.dao.JobHistoryDao#findBeanEditionIds(java.lang.String)
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<String> findBeanEditionIds(String applicationOrigine)
    {
        // Cas du paramétre absent
        Control.assertApplicationNonRenseignee(applicationOrigine);

        // Journalisation de la recherche
        LOGGER.debug(JOURNALISATION_RECHERCHE + " lié à l'application '{}'"
            , applicationOrigine);
        
        // Initialisation de la recherche
        DetachedCriteria criteria = DetachedCriteria.forClass(JobHistory.class);

        // Ajout des critères
        criteria.add(Restrictions.eq(APPLICATION_ORIGINE, applicationOrigine));

        // Création d'une projection sur l'identifiant
        ProjectionList uneprojection = Projections.projectionList();
        uneprojection.add(Projections.groupProperty(ProprieteJobHistory.IDENTIFIANT_EDITION.getNom())
            , ProprieteJobHistory.IDENTIFIANT_EDITION.getNom());
        criteria.setProjection(uneprojection);

        // Exécution de la recherche
        return (List<String>) getHibernateTemplate().findByCriteria(criteria);
    }

    /** 
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.edition.dao.JobHistoryDao#findEditionsLesPlusLongues(java.lang.String, int)
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<JobHistory> findEditionsLesPlusLongues(String applicationOrigine, int nbeARetourner)
    {
        // Cas du paramétre absent
        Control.assertApplicationNonRenseignee(applicationOrigine);

        // Journalisation de la recherche
        LOGGER.debug(JOURNALISATION_RECHERCHE + " ayant les temps d'exécution les plus "
            + "longs, liés à l'application '{}' (max : {})", applicationOrigine, nbeARetourner);

        // Initialisation de la recherche
        DetachedCriteria criteria = DetachedCriteria.forClass(JobHistory.class);

        // Ajout des critères
        criteria.add(Restrictions.eq(APPLICATION_ORIGINE, applicationOrigine));
        criteria.add(Restrictions.eq(STATUT, JobHistory.STATUS_DISPONIBLE));

        // Ajout du tri
        criteria.addOrder(Order.desc(ProprieteJobHistory.TEMPS_EXECUTION.getNom()));

        // Récupération d'une nouvelle instance de l'utilitaire Spring permettant de faciliter l'utilisation
        // d'Hibernate, afin de ne pas conserver les modifications du paramétrage, ci-dessous, après
        // la fin du traitement
        HibernateTemplate hibernateTemplate = getHibernateTemplate(true);

        // Modification du paramétrage pour limiter le nombre de résultat
        hibernateTemplate.setMaxResults(nbeARetourner);

        // Exécution de la recherche
        return (List<JobHistory>) hibernateTemplate.findByCriteria(criteria);
    }

    /** 
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.edition.dao.JobHistoryDao#findJobHistory(java.lang.String)
     */
    @Override
    public JobHistory findJobHistory(String editionUuid)
    {
        // Cas du paramétre absent
        Control.assertUuidNonRenseigne(editionUuid);

        // Journalisation de la recherche
        LOGGER.debug(JOURNALISATION_RECHERCHE + " d'UUID '{}'", editionUuid);

        // Récupération du résultat de la recherche
        return getHibernateTemplate().execute(
                // Initialisation de la recherche
                session -> (JobHistory) session.createCriteria(JobHistory.class)
                            // Ajout des critères
                            .add(Restrictions.eq(ProprieteJobHistory.UID_EDITION.getNom(), editionUuid))
                            // Exécution de la recherche
                            .uniqueResult()
            );
    }

    /** 
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.edition.dao.JobHistoryDao#findJobHistoryParProfilStructureAffectationAppli(
     * java.lang.String, java.lang.String, java.lang.String)
     */
    @Override
    public List<JobHistory> findJobHistoryParProfilStructureAffectationAppli(String profil,
        String structureAffectation, String applicationOrigine)
    {
        throw new UnsupportedOperationException("Fonctionnalité non disponible");
    }

    /** 
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.edition.dao.JobHistoryDao#findLaDerniereEditionParBeanIdEtParAppliOrigine(
     * java.lang.String, java.lang.String)
     */
    @Override
    public JobHistory findLaDerniereEditionParBeanIdEtParAppliOrigine(String beanEditionId, String appliOrigine)
    {
        // Cas des paramétres absents
        Control.assertIdNonRenseigne(beanEditionId);
        Control.assertApplicationNonRenseignee(appliOrigine);

        // Journalisation de la recherche
        LOGGER.debug(JOURNALISATION_RECHERCHE + " d'id '{}', lié à l'application '{}'"
            , beanEditionId, appliOrigine);

        // Exécution de la recherche
        return findLaDerniereEdition(beanEditionId, appliOrigine);
    }

    /** 
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.edition.dao.JobHistoryDao
     * #findLaDerniereEditionParBeanIdEtParAppliOrigineEtParUtilisateur(
     * java.lang.String, java.lang.String, java.lang.String)
     */
    @Override
    public JobHistory findLaDerniereEditionParBeanIdEtParAppliOrigineEtParUtilisateur(String beanEditionId,
        String appliOrigine, String utilisateur)
    {
        // Cas des paramétres absents
        Control.assertIdNonRenseigne(beanEditionId);
        Control.assertApplicationNonRenseignee(appliOrigine);
        Control.assertUtilisateurNonRenseigne(utilisateur);

        // Journalisation de la recherche
        LOGGER.debug(JOURNALISATION_RECHERCHE + " d'identifiant '{}', par l'utilisateur '{}', "
            + "lié à l'application '{}'", beanEditionId, utilisateur, appliOrigine);

        // Exécution de la recherche
        return findLaDerniereEdition(beanEditionId, appliOrigine, utilisateur);
    }

    /**
     * Méthode permettant de récupérer le dernier historique de travaux d'édition
     *
     * @param beanEditionId identifiant de l'édition
     * @param appliOrigine application liée à l'édition
     * @return l'historique de travaux d'édition correspondant
     */
    private JobHistory findLaDerniereEdition(String beanEditionId, String appliOrigine)
    {
        return findLaDerniereEdition(beanEditionId, appliOrigine, null);
    }

    /**
     * Méthode permettant de réupérer le dernier historique de travaux d'édition
     *
     * @param beanEditionId identifiant de l'édition
     * @param appliOrigine application liée à l'édition
     * @param utilisateur propriétaire de l'édition
     * @return l'historique de travaux d'édition correspondant
     */
    private JobHistory findLaDerniereEdition(String beanEditionId, String appliOrigine, String utilisateur)
    {
        // Déclaration d'un critère sur la date maximum
        DetachedCriteria lastJobHistory = DetachedCriteria.forClass(JobHistory.class);
        lastJobHistory.setProjection(Property.forName(DATE_DEMANDE).max())
            .add(Restrictions.eq(ProprieteJobHistory.IDENTIFIANT_EDITION.getNom(), beanEditionId))
            .add(Restrictions.eq(APPLICATION_ORIGINE, appliOrigine));
        if (utilisateur != null)
        {
            lastJobHistory.add(Restrictions.eq(UID_PROPRIETAIRE, utilisateur));
        }
                    
        DetachedCriteria criteria = DetachedCriteria.forClass(JobHistory.class);
        criteria.add(Property.forName(DATE_DEMANDE).eq(lastJobHistory))
            .add(Restrictions.eq(ProprieteJobHistory.IDENTIFIANT_EDITION.getNom(), beanEditionId))
            .add(Restrictions.eq(APPLICATION_ORIGINE, appliOrigine));
        if (utilisateur != null)
        {
            criteria.add(Restrictions.eq(UID_PROPRIETAIRE, utilisateur));
        }

        // Exécution de la recherche
        @SuppressWarnings("unchecked")
        List<JobHistory> listeJobHistory = (List<JobHistory>) getHibernateTemplate().findByCriteria(criteria);

        // Récupération du résultat s'il existe
        if (!listeJobHistory.isEmpty())
        {
            // Récupération du 1er des historiques existent pour la même date, le même identifiant,
            // voire le même propriétaire (même si c'est hautement improbable)
            return listeJobHistory.get(0);
        }

        return null;
    }


    /** 
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.edition.dao.JobHistoryDao#findNouvellesEditionsParUidEtAppli(
     * java.lang.String, java.lang.String)
     */
    @Override
    @SuppressWarnings("unchecked")
    public List<JobHistory> findNouvellesEditionsParUidEtAppli(String uidProprietaire, String applicationOrigine)
    {
        // Cas des paramétres absents
        Control.assertApplicationNonRenseignee(applicationOrigine);
        Control.assertUtilisateurNonRenseigne(uidProprietaire);

        // Journalisation de la recherche
        LOGGER.debug(JOURNALISATION_RECHERCHE + " du propriétaire '{}', liés à l'application '{}'"
            , uidProprietaire, applicationOrigine);
        
        // Récupération d'une nouvelle instance de l'utilitaire Spring permettant de faciliter l'utilisation
        // d'Hibernate, afin de ne pas conserver les modifications du paramétrage, ci-dessous, après
        // la fin du traitement
        HibernateTemplate hibernateTemplate = getHibernateTemplate(true);

         // Modification du paramétrage pour limiter le nombre de résultat
        hibernateTemplate.setMaxResults(NombreMaxEdition.MAX_LIGNE_PAR_UID.getNbreMax());

        // Initialisation de la recherche
        DetachedCriteria criteria = DetachedCriteria.forClass(JobHistory.class);

        // Paramétrage des critères
        criteria.add(Restrictions.eq(UID_PROPRIETAIRE, uidProprietaire));
        criteria.add(Restrictions.eq(APPLICATION_ORIGINE, applicationOrigine));
        criteria.add(Restrictions.eq(MONO_DESTINATAIRE, Boolean.TRUE));
        criteria.add(Restrictions.eq(NOUVELLE_EDITION, Boolean.TRUE));

        // Paramétrage du tri
        criteria.addOrder(Order.desc(DATE_DEMANDE));

        // Exécution de la recherche
        return (List<JobHistory>) hibernateTemplate.findByCriteria(criteria);
    }

    /** 
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.edition.dao.JobHistoryDao#findProfils(java.lang.String)
     */
    @Override
    @SuppressWarnings("unchecked")
    public List<ProfilDesti> findProfils(String applicationOrigine)
    {
        // Cas du paramétre absent
        Control.assertApplicationNonRenseignee(applicationOrigine);

        // Journalisation de la recherche
        LOGGER.debug("Recherche des profils destinataires liés aux historiques des travaux d'édition, "
            + "liés à l'application '{}'", applicationOrigine);
        
        // Initialisation de la recherche
        DetachedCriteria criteria = DetachedCriteria.forClass(JobHistory.class);

        // Ajout des critères
        criteria.add(Restrictions.eq(APPLICATION_ORIGINE, applicationOrigine));
        // Jointure sur les profils
        criteria.createCriteria(ProprieteJobHistory.PROFILS_DESTINATAIRES.getNom()
            , ProprieteJobHistory.ALIAS_PROFILS.getNom(), JoinType.INNER_JOIN);
        // Paramétrage d'une projection
        ProjectionList uneprojection = Projections.projectionList();
        uneprojection.add(Projections.groupProperty(ProprieteJobHistory.ALIAS_PROFILS.getNom()
            + "." + ProprieteJobHistory.NOM_PROFIL.getNom())
            , ProprieteJobHistory.ALIAS_NOM_PROFIL.getNom());
        criteria.setProjection(uneprojection);

        // Définition d'une stratégie de transformation du résultat, pour obtenir des profils
        criteria.setResultTransformer(new AliasToBeanResultTransformer(ProfilDesti.class));

        // Exécution de la requête
        return (List<ProfilDesti>) getHibernateTemplate().findByCriteria(criteria);
    }

    /** 
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.edition.dao.JobHistoryDao#findToutesEditionsParBeanEditionId(
     * java.lang.String, java.lang.String)
     */
    @Override
    @SuppressWarnings("unchecked")
    public List<JobHistory> findToutesEditionsParBeanEditionId(String beanEditionId, String applicationOrigine)
    {
        // Cas des paramétres absents
        Control.assertIdNonRenseigne(beanEditionId);
        Control.assertApplicationNonRenseignee(applicationOrigine);

        // Journalisation de la recherche
        LOGGER.debug(JOURNALISATION_RECHERCHE + " d'identifiant '{}'"
            + "liés à l'application '{}'", beanEditionId, applicationOrigine);
        
        // Récupération d'une nouvelle instance de l'utilitaire Spring permettant de faciliter l'utilisation
        // d'Hibernate, afin de ne pas conserver les modifications du paramétrage, ci-dessous, après
        // la fin du traitement
        HibernateTemplate hibernateTemplate = getHibernateTemplate(true);

        // Modification du paramétrage pour limiter le nombre de résultat
        hibernateTemplate.setMaxResults(NombreMaxEdition.MAX_LIGNE_PAR_UID.getNbreMax());

        // Initialisation de la recherche
        DetachedCriteria criteria = DetachedCriteria.forClass(JobHistory.class);

        // Paramétrage des critères de recherche
        criteria.add(Restrictions.eq(ProprieteJobHistory.IDENTIFIANT_EDITION.getNom(), beanEditionId));
        criteria.add(Restrictions.eq(APPLICATION_ORIGINE, applicationOrigine));

        // Paramétrage du tri
        criteria.addOrder(Order.desc(DATE_DEMANDE));

        // Exécution de la recherche
        return (List<JobHistory>) hibernateTemplate.findByCriteria(criteria);
    }

    /** 
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.edition.dao.JobHistoryDao#findToutesEditionsParDateDemande(
     * java.util.Date, java.lang.String)
     */
    @Override
    @SuppressWarnings("unchecked")
    public List<JobHistory> findToutesEditionsParDateDemande(Date dateDemande, String applicationOrigine)
    {
        // Cas des paramètres non renseignés
        Control.assertApplicationNonRenseignee(applicationOrigine);
        if (dateDemande == null)
        {
            throw new ProgrammationException("La date de demande doit être renseignée");
        }

        // Journalisation de la recherche
        LOGGER.debug(JOURNALISATION_RECHERCHE + "liés à l'application '{}' pour la date '{}'"
            , applicationOrigine, dateDemande);
        
        // Récupération d'une nouvelle instance de l'utilitaire Spring permettant de faciliter l'utilisation
        // d'Hibernate, afin de ne pas conserver les modifications du paramétrage, ci-dessous, après
        // la fin du traitement
        HibernateTemplate hibernateTemplate = getHibernateTemplate(true);

        // Modification du paramétrage pour limiter le nombre de résultat
        hibernateTemplate.setMaxResults(NombreMaxEdition.MAX_LIGNE_PAR_UID.getNbreMax());

        // Initialisation de la recherche
        DetachedCriteria criteria = DetachedCriteria.forClass(JobHistory.class);

        // Détermination de la borne fin de la date de demande
        Calendar cal = Calendar.getInstance(Locale.FRANCE);
        cal.setTime(dateDemande);
        cal.add(Calendar.DATE, 1);
        Date dateDemandePlusUn = cal.getTime();

        // Paramétrage des critères de recherche
        criteria.add(Restrictions.ge(DATE_DEMANDE, dateDemande));
        criteria.add(Restrictions.lt(DATE_DEMANDE, dateDemandePlusUn));
        criteria.add(Restrictions.eq(APPLICATION_ORIGINE, applicationOrigine));

        // Paramétrage du tri
        criteria.addOrder(Order.desc(DATE_DEMANDE));

        // Exécution de la recherche
        return (List<JobHistory>) hibernateTemplate.findByCriteria(criteria);
    }

    /** 
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.edition.dao.JobHistoryDao#findToutesEditionsParEtat(
     * java.lang.String, java.lang.String)
     */
    @Override
    @SuppressWarnings("unchecked")
    public List<JobHistory> findToutesEditionsParEtat(String etat, String applicationOrigine)
    {
        // Cas des paramètres non renseignés
        Control.assertApplicationNonRenseignee(applicationOrigine);
        if (StringUtils.isEmpty(etat))
        {
            throw new ProgrammationException("L'état doit être renseignée");
        }

        // Journalisation de la recherche
        LOGGER.debug(JOURNALISATION_RECHERCHE + " liés à l'application '{}' pour l'état '{}'"
            , applicationOrigine, etat);
        
        // Récupération d'une nouvelle instance de l'utilitaire Spring permettant de faciliter l'utilisation
        // d'Hibernate, afin de ne pas conserver les modifications du paramétrage, ci-dessous, après
        // la fin du traitement
        HibernateTemplate hibernateTemplate = getHibernateTemplate(true);

        // Modification du paramétrage pour limiter le nombre de résultat
        hibernateTemplate.setMaxResults(NombreMaxEdition.MAX_LIGNE_PAR_UID.getNbreMax());

        // Initialisation de la recherche
        DetachedCriteria criteria = DetachedCriteria.forClass(JobHistory.class);

        // Paramétrage des critères de recherche
        criteria.add(Restrictions.eq(STATUT, etat));
        criteria.add(Restrictions.eq(APPLICATION_ORIGINE, applicationOrigine));
        criteria.addOrder(Order.desc(DATE_DEMANDE));

        // Exécution de la recherche
        return (List<JobHistory>) hibernateTemplate.findByCriteria(criteria);
    }

    /** 
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.edition.dao.JobHistoryDao#findToutesEditionsParPeriodeDateDemande(
     * java.util.Date, java.util.Date, java.lang.String)
     */
    @Override
    @SuppressWarnings("unchecked")
    public List<JobHistory> findToutesEditionsParPeriodeDateDemande(
        Date debutPeriodeDemande, Date finPeriodeDemande, String applicationOrigine)
    {
        // Cas des paramètres non renseignés
        Control.assertApplicationNonRenseignee(applicationOrigine);
        if (debutPeriodeDemande == null || finPeriodeDemande == null)
        {
            throw new ProgrammationException("La date de début et fin de période doit être renseignée");
        }

        // Journalisation de la recherche
        LOGGER.debug(JOURNALISATION_RECHERCHE + "liés à l'application '{}' pour période '{}' -> '{}'"
            , applicationOrigine, debutPeriodeDemande, finPeriodeDemande);

        // Récupération d'une nouvelle instance de l'utilitaire Spring permettant de faciliter l'utilisation
        // d'Hibernate, afin de ne pas conserver les modifications du paramétrage, ci-dessous, après
        // la fin du traitement
        HibernateTemplate hibernateTemplate = getHibernateTemplate(true);

        // Modification du paramétrage pour limiter le nombre de résultat
        hibernateTemplate.setMaxResults(NombreMaxEdition.MAX_LIGNE_PAR_UID.getNbreMax());

        // Initialisation de la recherche
        DetachedCriteria criteria = DetachedCriteria.forClass(JobHistory.class);

        // Paramétrage des critères de recherche
        criteria.add(Restrictions.ge(DATE_DEMANDE, debutPeriodeDemande));
        criteria.add(Restrictions.le(DATE_DEMANDE, finPeriodeDemande));
        criteria.add(Restrictions.eq(APPLICATION_ORIGINE, applicationOrigine));

        // Paramétrage du tri 
        criteria.addOrder(Order.desc(DATE_DEMANDE));

        // Exécution de la recherche
        return (List<JobHistory>) hibernateTemplate.findByCriteria(criteria);
    }

    /** 
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.edition.dao.JobHistoryDao#findNouvellesEditionsParProfilFiltreAppli(
     * java.lang.String, java.util.Map, java.lang.String)
     */
    @Override
    public List<JobHistory> findNouvellesEditionsParProfilFiltreAppli(String uid
        , Map<String, Set<FiltreEdition>> mapFiltresEdition, String applicationOrigine)
    {
        // Cas du paramétre UID absent
        Control.assertUtilisateurNonRenseigne(uid);

        // Exécution de la recherche
        return findToutesEditions(uid, mapFiltresEdition, applicationOrigine);
    }

    /** 
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.edition.dao.JobHistoryDao#findToutesEditionsParProfilFiltreAppli(
     * java.util.Map, java.lang.String)
     */
    @Override
    public List<JobHistory> findToutesEditionsParProfilFiltreAppli(
        Map<String, Set<FiltreEdition>> mapFiltresEdition, String applicationOrigine)
    {
        // Exécution de la recherche
        return findToutesEditions(mapFiltresEdition, applicationOrigine);
    }
    
    private List<JobHistory> findToutesEditions(Map<String, Set<FiltreEdition>> mapFiltresEdition
        , String applicationOrigine)
    {
        return findToutesEditions(null, mapFiltresEdition, applicationOrigine);
    }

    @SuppressWarnings("unchecked")
    private List<JobHistory> findToutesEditions(String uid, Map<String, Set<FiltreEdition>> mapFiltresEdition
        , String applicationOrigine)
    {
        // Contrôle des paramétres
        Control.assertApplicationNonRenseignee(applicationOrigine);
        if (mapFiltresEdition == null)
        {
            throw new ProgrammationException("Les filtres de profil doivent être renseignés");
        }

        // Journalisation de la recherche
        LOGGER.debug(JOURNALISATION_RECHERCHE + " liés à l'application '{}' pour un certain "
            + "filtre de profil, non consultés par l'utilisateur '{}'", applicationOrigine, uid);
        
        List<JobHistory> listeJobParNom;
        // Récupération d'une nouvelle instance de l'utilitaire Spring permettant de faciliter l'utilisation
        // d'Hibernate, afin de ne pas conserver les modifications du paramétrage, ci-dessous, après
        // la fin du traitement
        HibernateTemplate hibernateTemplate = getHibernateTemplate(true);
        // Modification du paramétrage pour limiter le nombre de résultat
        hibernateTemplate.setMaxResults(NombreMaxEdition.MAX_LIGNE_PAR_PROFIL.getNbreMax());

        DetachedCriteria criteria = DetachedCriteria.forClass(JobHistory.class);
        criteria.add(Restrictions.eq(APPLICATION_ORIGINE, applicationOrigine));
        criteria.add(Restrictions.eq(MONO_DESTINATAIRE, Boolean.FALSE));
        criteria.createAlias(ProprieteJobHistory.PROFILS_DESTINATAIRES.getNom(), ProprieteJobHistory.ALIAS_PROFILS.getNom());
        criteria.createAlias(ProprieteJobHistory.LISTE_FILTRE.getNom()
            , ProprieteJobHistory.ALIAS_LISTE_FILTRE.getNom(), JoinType.LEFT_OUTER_JOIN);
        criteria.createAlias(ProprieteJobHistory.VALEUR_FILTRE.getNom()
            , ProprieteJobHistory.ALIAS_VALEUR_FILTRE.getNom(), JoinType.LEFT_OUTER_JOIN);

        Set<String> keyProfils = mapFiltresEdition.keySet();
        Disjunction profilsDisjunction = Restrictions.disjunction();

        for (Iterator<String> iterKeyProfils = keyProfils.iterator(); iterKeyProfils.hasNext();)
        {
            Conjunction unProfilConjunction = Restrictions.conjunction();
            String profil = iterKeyProfils.next();

            Set<FiltreEdition> filtresEditions = mapFiltresEdition.get(profil);
            if (filtresEditions == null)
            {

                throw new ProgrammationException("Le filtre du profil '" + profil + "' n'est pas renseigné");
            }

            // Sélection des éditions qui sont accessibles par un profil
            unProfilConjunction.add(Restrictions.eq(ProprieteJobHistory.ALIAS_PROFILS.getNom()
                + "." + ProprieteJobHistory.NOM_PROFIL.getNom(), profil));

            for (Iterator<FiltreEdition> iterFiltresEditions = filtresEditions.iterator(); iterFiltresEditions.hasNext();)
            {
                Conjunction unFiltreConjunction = Restrictions.conjunction();
                FiltreEdition filtreEdition = iterFiltresEditions.next();
                // Ajout d'une restriction en fonction du filtre associé au profil
                if (StringUtils.isNotBlank(filtreEdition.getNomDuFiltre()))
                {
                    unFiltreConjunction
                        .add(Restrictions.eq(ProprieteJobHistory.ALIAS_LISTE_FILTRE.getNom()
                            + "." + ProprieteJobHistory.NOM_FILTRE.getNom(), filtreEdition.getNomDuFiltre()));
                    unFiltreConjunction.add(Restrictions
                            .in(ProprieteJobHistory.ALIAS_VALEUR_FILTRE.getNom() 
                                + "." + ProprieteJobHistory.VAL_FILTRE.getNom()
                                , filtreEdition.getValsFiltreString()));
                }
                unProfilConjunction.add(unFiltreConjunction);
            }
            profilsDisjunction.add(unProfilConjunction);
        }

        criteria.add(profilsDisjunction);
        
        if (uid != null)
        {
            // Sélectionne uniquement les éditions que l'utilisateur n'a pas encore consulté
            criteria.createAlias(ProprieteJobHistory.UID_CONSULTATION.getNom()
                , ProprieteJobHistory.ALIAS_UID_CONSULTATION.getNom(), JoinType.LEFT_OUTER_JOIN);
            criteria.add(Restrictions.disjunction().add(Restrictions.ne(
                ProprieteJobHistory.ALIAS_UID_CONSULTATION.getNom()
                + "." + ProprieteJobHistory.UID_UTILISATEUR.getNom(), uid)).add(
                Restrictions.isEmpty(ProprieteJobHistory.UID_CONSULTATION.getNom())));
        }

        criteria.setResultTransformer(DistinctRootEntityResultTransformer.INSTANCE);
        criteria.addOrder(Order.desc(DATE_DEMANDE));

        // Exécution de la requête
        listeJobParNom = (List<JobHistory>) hibernateTemplate.findByCriteria(criteria);
        return listeJobParNom;
    }

    /** 
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.edition.dao.JobHistoryDao#findToutesEditionsParUidEtAppli(
     * java.lang.String, java.lang.String)
     */
    @Override
    @SuppressWarnings("unchecked")
    public List<JobHistory> findToutesEditionsParUidEtAppli(String uidProprietaire, String applicationOrigine)
    {
        // Contrôle des paramétres
        Control.assertApplicationNonRenseignee(applicationOrigine);
        Control.assertUtilisateurNonRenseigne(uidProprietaire);

        // Journalisation de la recherche
        LOGGER.debug(JOURNALISATION_RECHERCHE + " liés à l'application '{}' pour l'utilisateur '{}' "
            , applicationOrigine, uidProprietaire);

        // Récupération d'une nouvelle instance de l'utilitaire Spring permettant de faciliter l'utilisation
        // d'Hibernate, afin de ne pas conserver les modifications du paramétrage, ci-dessous, après
        // la fin du traitement
        HibernateTemplate hibernateTemplate = getHibernateTemplate(true);

        // Modification du paramétrage pour limiter le nombre de résultat
        hibernateTemplate.setMaxResults(NombreMaxEdition.MAX_LIGNE_PAR_UID.getNbreMax());

        // Initialisation de la recherche
        DetachedCriteria criteria = DetachedCriteria.forClass(JobHistory.class);

        // Paramétrage des critères de recherche
        criteria.add(Restrictions.eq(UID_PROPRIETAIRE, uidProprietaire));
        criteria.add(Restrictions.eq(APPLICATION_ORIGINE, applicationOrigine));
        criteria.add(Restrictions.eq(MONO_DESTINATAIRE, Boolean.TRUE));

        // Paramétrage du tri
        criteria.addOrder(Order.desc(DATE_DEMANDE));

        // Exécution de la recherche
        return (List<JobHistory>) hibernateTemplate.findByCriteria(criteria);
    }

    /** 
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.edition.dao.JobHistoryDao#isValidEdition(
     * fr.gouv.finances.lombok.edition.bean.JobHistory)
     */
    @Override
    public boolean isValidEdition(JobHistory jobHistory)
    {
        Control.assertJobHistoryNotNull(jobHistory);
        Control.assertUuidNonRenseigne(jobHistory.getEditionUuid());

        // Journalisation de la recherche
        LOGGER.debug("Détermination si un historique de travaux d'édition, d'identifiant '{}', existe ou non"
            , jobHistory.getEditionUuid());

        // Recherche s'il y a plus d'une édition pour un UID donné
        return getHibernateTemplate().execute(
            session -> (Long) session.createCriteria(JobHistory.class)
                            .setProjection(Projections.rowCount())
                            .add(Restrictions.eq(ProprieteJobHistory.UID_EDITION.getNom(), jobHistory.getEditionUuid()))
                            .uniqueResult()
        )  > 0;
    }

    /** 
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.edition.dao.JobHistoryDao#saveContenuEditionFromByteArray(
     * fr.gouv.finances.lombok.edition.bean.JobHistory, byte[])
     */
    @Override
    public void saveContenuEditionFromByteArray(JobHistory jobHistory, byte[] dataBytes)
    {
        // Contrôles des paramétres
        Control.assertJobHistoryNotNull(jobHistory);
        Control.assertJobHistoryCharge(jobHistory);
        Control.assertdatabytesNotNull(dataBytes);

        // Journalisation de la mise à jour
        LOGGER.debug("Sauvegarde du contenu au sein de l'historique de travaux d'édition d'identifiant '{}'"
            , jobHistory.getEditionUuid());

        getHibernateTemplate().execute(
                session ->
                {
                    // Mise à jour des éventuelles modifications
                    session.update(jobHistory);
                    // Chargement du contenu de l'édition, avec initialisation en cas d'absence
                    loadSafeData(jobHistory);

                    // Association du nouveau contenu de l'édition avec l'historique
                    jobHistory.getStockageEdition().getLeContenuEdition().setData(dataBytes);

                    // Sauvegarde en base
                    session.save(jobHistory.getStockageEdition().getLeContenuEdition());
                    session.update(jobHistory);

                    // L'historique ayant directement été modifié, pas de retour requis
                    return null;
                }
            );
    }

    /** 
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.edition.dao.JobHistoryDao#saveContenuEditionFromFile(
     * fr.gouv.finances.lombok.edition.bean.JobHistory, java.io.File)
     */
    @Override
    public void saveContenuEditionFromFile(JobHistory jobHistory, File fichierTemporaire)
    {
        // Contrôle des paramètres
        Control.assertJobHistoryNotNull(jobHistory);
        Control.assertJobHistoryCharge(jobHistory);
        Control.assertFichierAccessibleEnLecture(fichierTemporaire);
        if (fichierTemporaire.length() == 0)
        {
            throw new ProgrammationException("Le fichier '" + fichierTemporaire.getName() + "' est vide");
        }

        // Journalisation de la mise à jour
        LOGGER.debug("Sauvegarde du contenu au sein de l'historique de travaux d'édition d'identifiant '{}'"
            , jobHistory.getEditionUuid());
        
        getHibernateTemplate().execute(
            session ->
            {
                // Mise à jour des éventuelles modifications
                session.update(jobHistory);
                // Chargement du contenu de l'édition, avec initialisation en cas d'absence
                loadSafeData(jobHistory);

                // Association du nouveau contenu de l'édition avec l'historique
                jobHistory.getStockageEdition().getLeContenuEdition().setData(
                    FileUtils.getFichier(fichierTemporaire));

                // Sauvegarde en base
                session.save(jobHistory.getStockageEdition().getLeContenuEdition());
                session.update(jobHistory);

                // L'historique ayant directement été modifié, pas de retour requis
                return null;
            }
        );
    }
    
    /** 
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.edition.dao.JobHistoryDao#findAndLoadJobHistory(java.lang.String)
     */
    @Override
    public JobHistory findAndLoadJobHistory(String editionUuid)
    {
        // Cas du paramétre absent
        Control.assertUuidNonRenseigne(editionUuid);

        // Journalisation de la recherche
        LOGGER.debug("Chargement de l'historique de travaux d'édition d'identifiant '{}'", editionUuid);
        
        // Exécution de la requête
        return (JobHistory) getHibernateTemplate().execute(
                // Initialisation de la recherche
                session -> session.createCriteria(JobHistory.class)
                    // Ajout des critères de recherche
                    .add(Restrictions.eq(ProprieteJobHistory.UID_EDITION.getNom(), editionUuid))
                    // Récupération des associations
                    .setFetchMode(ProprieteJobHistory.CONTENU_EDITION.getNom(), FetchMode.SELECT)
                    .setFetchMode(ProprieteJobHistory.PROFILS_DESTINATAIRES.getNom(), FetchMode.JOIN)
                    .setFetchMode(ProprieteJobHistory.LISTE_FILTRE.getNom(), FetchMode.JOIN)
                    // Le résulat est unique
                    .uniqueResult()
            );
    }
}
