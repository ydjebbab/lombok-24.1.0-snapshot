package fr.gouv.finances.lombok.autoconfig;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

/**
 * Classe de configuration qui permet de conditionner les ressources à importer
 * 
 * @author celfer Date: 18 févr. 2020
 */
@Configuration
@ConditionalOnProperty(name = "lombok.orm.jpa", havingValue = "false")
@ImportResource({"classpath:conf/applicationContext-commun-edition-hibernate.xml",
        "classpath:conf/applicationContext-commun-edition-hibernate-batch.xml"})
public class EditionsHibernateApplicationConfig
{

}
