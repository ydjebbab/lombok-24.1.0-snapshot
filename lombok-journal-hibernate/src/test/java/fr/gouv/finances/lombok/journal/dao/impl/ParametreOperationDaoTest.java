package fr.gouv.finances.lombok.journal.dao.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.gouv.finances.lombok.journal.bean.ParametreOperation;
import fr.gouv.finances.lombok.journal.dao.ParametreOperationDao;
import fr.gouv.finances.lombok.test.controle.ControleDonnees;
import fr.gouv.finances.lombok.util.base.PurgeTablesDao;

/**
 * Tests unitaires automatisés du DAO Hibernate gérant les données des paramètres d'opération de journalisation.
 *
 * @author Christophe Breheret-Girardin
 */
@ContextConfiguration({"classpath:/conf/applicationContext-journal-hibernate-test.xml",
        "classpath*:/conf/applicationContext-core-persistance-test.xml",
        "classpath*:/conf/applicationContext-core-orm-test.xml",
        "classpath*:/conf/applicationContext-journal-dao.xml"})
@ActiveProfiles(profiles = {"journal", "hibernate"})
@RunWith(SpringJUnit4ClassRunner.class)
public class ParametreOperationDaoTest
{
    public ParametreOperationDaoTest()
    {
        super();
    }

    protected static final Logger LOGGER = LoggerFactory.getLogger(ParametreOperationDaoTest.class);

    /** Déclaration du DAO de gestion des données des paramètres d'opération de journalisation. */
    @Autowired
    private ParametreOperationDao paramoperdao;

    /** Déclaration du DAO de gestion des purges de la base. */
    @Autowired
    private PurgeTablesDao purgetablesdao;

    /** Map contenant les paramètres d'opération de journalisation présents en base */
    Map<Integer, ParametreOperation> mapParametreOperation = new HashMap<>();

    /** Valeur inconnue en base utilisée dans les tests */
    private static final String VALEUR_INCONNUE = "kamoulox";

    /**
     * Initialisation des données en base avant chaque test.
     */
    @Before
    public void creerDonnees()
    {
        // Purge des données de la base
        purgetablesdao.purgerTables("TJ_ZOPE_ZPAO_TJ", "ZOPERATIONJOURNAL_ZOPE", "ZPARAMETREOPERATION_ZPAO");

        // Vérification de l'absence de données
        ControleDonnees.verifierElements(paramoperdao.loadAllObjects(ParametreOperation.class));

        // Génération des paramètres de journalisation, insertion en base de données et sauvegarde dans une Map
        mapParametreOperation = IntStream.rangeClosed(1, 5)
            .mapToObj(ParametreOperationUtils::getParametreOperation).peek(paramoperdao::saveObject)
            .collect(Collectors.toMap(p -> p.getNumIncrement().intValue(), p -> p));

        // Vérification des données à partir de l'image de la base
        ControleDonnees.verifierElements(paramoperdao.loadAllObjects(ParametreOperation.class), mapParametreOperation.get(1),
            mapParametreOperation.get(2), mapParametreOperation.get(3), mapParametreOperation.get(4), mapParametreOperation.get(5));

        // Suppression du cache pour forcer les requêtes des tests
        paramoperdao.clearPersistenceContext();
    }

    @Test
    public void injectionDao()
    {
        assertNotNull(paramoperdao);
    }

    @Test
    public void contraintes()
    {
        ParametreOperation param1 = ParametreOperationUtils.getParametreOperation(1);
        paramoperdao.saveObject(param1);

        ControleDonnees.verifierElements(paramoperdao.loadAllObjects(ParametreOperation.class), mapParametreOperation.get(1),
            mapParametreOperation.get(2), mapParametreOperation.get(3), mapParametreOperation.get(4), mapParametreOperation.get(5), param1);
    }

    /**
     * Test de la méthode xxx.
     */
    @Test
    public void saveUnParamOper()
    {
        // Exécution de la méthode à tester
        ParametreOperation parametreOperation = ParametreOperationUtils.getParametreOperation(6);
        paramoperdao.saveParametreOperation(parametreOperation);

        // Vérification des données
        ControleDonnees.verifierElements(paramoperdao.loadAllObjects(ParametreOperation.class), mapParametreOperation.get(1),
            mapParametreOperation.get(2), mapParametreOperation.get(3), mapParametreOperation.get(4), mapParametreOperation.get(5),
            parametreOperation);
    }

    /**
     * Test de la méthode xxx.
     */
    @Test
    public void deleteUnParamOper()
    {
        // Exécution de la méthode à tester
        paramoperdao.deleteParametreOperation(mapParametreOperation.get(2));

        // Vérification des données
        ControleDonnees.verifierElements(paramoperdao.loadAllObjects(ParametreOperation.class), mapParametreOperation.get(1),
            mapParametreOperation.get(3), mapParametreOperation.get(4), mapParametreOperation.get(5));
    }

    /**
     * Test de la méthode xxx.
     */
    @Test
    public void findUnParamOperParNom()
    {
        // Exécution de la méthode à tester sans résultat
        assertNull(paramoperdao.findParametreOperationParNom(VALEUR_INCONNUE));

        // Exécution de la méthode à tester sur élément avec nom
        assertEquals(mapParametreOperation.get(1), paramoperdao.findParametreOperationParNom(mapParametreOperation.get(1).getNom()));
        assertEquals(mapParametreOperation.get(2), paramoperdao.findParametreOperationParNom(mapParametreOperation.get(2).getNom()));
        assertEquals(mapParametreOperation.get(4), paramoperdao.findParametreOperationParNom(mapParametreOperation.get(4).getNom()));

        // Exécution de la méthode à tester sur élément sans nom
        assertEquals(mapParametreOperation.get(3), paramoperdao.findParametreOperationParNom(null));
    }

    /**
     * Test de la méthode xxx.
     */
    @Test
    public void findUnParamOperParNomEtValeur()
    {
        // Exécution de la méthode à tester sans résultat
        assertNull(paramoperdao.findParametreOperationParNomEtValeur(VALEUR_INCONNUE, VALEUR_INCONNUE));
        assertNull(paramoperdao.findParametreOperationParNomEtValeur(
            mapParametreOperation.get(2).getNom(), VALEUR_INCONNUE));
        assertNull(paramoperdao.findParametreOperationParNomEtValeur(
            VALEUR_INCONNUE, mapParametreOperation.get(2).getValeur()));

        // Exécution de la méthode à tester avec résultat
        assertEquals(mapParametreOperation.get(1), paramoperdao.findParametreOperationParNomEtValeur(
            mapParametreOperation.get(1).getNom(), mapParametreOperation.get(1).getValeur()));
        assertEquals(mapParametreOperation.get(2), paramoperdao.findParametreOperationParNomEtValeur(
            mapParametreOperation.get(2).getNom(), mapParametreOperation.get(2).getValeur()));
        assertEquals(mapParametreOperation.get(3), paramoperdao.findParametreOperationParNomEtValeur(
            null, mapParametreOperation.get(3).getValeur()));
        assertEquals(mapParametreOperation.get(4), paramoperdao.findParametreOperationParNomEtValeur(
            mapParametreOperation.get(4).getNom(), null));
        assertEquals(mapParametreOperation.get(5), paramoperdao.findParametreOperationParNomEtValeur(
            mapParametreOperation.get(5).getNom(), mapParametreOperation.get(5).getValeur()));
    }
}