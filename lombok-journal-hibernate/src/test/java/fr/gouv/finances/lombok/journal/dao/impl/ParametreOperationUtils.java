package fr.gouv.finances.lombok.journal.dao.impl;

import fr.gouv.finances.lombok.journal.bean.ParametreOperation;

final class ParametreOperationUtils
{
    /** Préfix de valeurs du nom du paramètre */
    public static final String PREFIX_NOM = "Nom ";

    /**
     * Constructeur.
     */
    public ParametreOperationUtils()
    {
        super();
    }

    /**
     * Méthode permettant de générer un paramètre d'opération de journalisation.
     *
     * @param indice indice permettant d'instaurer de la généricité
     * @return le paramètre d'opération de journalisation généré
     */
    public static ParametreOperation getParametreOperation(Integer indice)
    {
        ParametreOperation parametreOperation = new ParametreOperation();
        parametreOperation.setNumIncrement(indice.longValue());
        /**
         * paramètre :
         * 1 -> nom1 | 1
         * 2 -> nom2 | 2
         * 3 ->      | 3
         * 4 -> nom4 |
         * 5 -> nom5 | 5
         * 6 -> nom6 | 6
         * 7 -> nom7 | 7
         */
        switch (indice)
        {
            case 3 :
                parametreOperation.setValeur(String.valueOf(indice));
                break;
            case 4:
                parametreOperation.setNom(PREFIX_NOM + indice);
                break;
            case 5:
                break;
            default :
                parametreOperation.setNom(PREFIX_NOM + indice);
                parametreOperation.setValeur(String.valueOf(indice));
        }
        parametreOperation.setValeurPrecedente("valeur precedente " + indice);

        return parametreOperation;
    }
}
