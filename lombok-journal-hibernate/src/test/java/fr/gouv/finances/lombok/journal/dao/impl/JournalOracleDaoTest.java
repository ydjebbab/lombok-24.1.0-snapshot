package fr.gouv.finances.lombok.journal.dao.impl;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.support.TransactionTemplate;

import fr.gouv.finances.lombok.journal.bean.OperationJournal;
import fr.gouv.finances.lombok.journal.dao.JournalDao;
import fr.gouv.finances.lombok.test.controle.ControleDonnees;
import fr.gouv.finances.lombok.test.controle.VerificationExecution;
import fr.gouv.finances.lombok.util.base.PurgeTablesDao;

/**
 * Tests unitaires automatisés du DAO Hibernate gérant les données de journalisation.
 *
 * @author Christophe Breheret-Girardin
 */
@ContextConfiguration({"classpath:/conf/applicationContext-journal-hibernate-test.xml",
        "classpath*:/conf/applicationContext-core-persistance-test.xml",
        "classpath*:/conf/applicationContext-core-orm-test.xml",
        "classpath*:/conf/applicationContext-journal-dao.xml",
        "classpath*:/conf/applicationContext-commun-transaction.xml"})
@ActiveProfiles(profiles = {"journal", "hibernate"})
@RunWith(SpringJUnit4ClassRunner.class)
public class JournalOracleDaoTest
{
    public JournalOracleDaoTest()
    {
        super();
    }

    protected static final Logger LOGGER = LoggerFactory.getLogger(JournalOracleDaoTest.class);

    /** Déclaration du DAO de gestion des données de journalisation. */
    @Autowired
    private JournalDao journaldao;

    /** Déclaration du DAO de gestion des purges de la base. */
    @Autowired
    private PurgeTablesDao purgetablesdao;

    /** Déclaration du template de transaction. */
    @Autowired
    private TransactionTemplate transactionTemplate;

    /**
     * Initialisation des données en base avant chaque test.
     */
    @Before
    public void creerDonnees()
    {
        // Purge des données de la base
        purgetablesdao.purgerTables("TJ_ZOPE_ZPAO_TJ", "ZOPERATIONJOURNAL_ZOPE", "ZPARAMETREOPERATION_ZPAO");

        // Vérification de l'absence de données
        ControleDonnees.verifierElements(journaldao.loadAllObjects(OperationJournal.class));

        // Suppression du cache pour forcer les requêtes des tests
        journaldao.clearPersistenceContext();
    }

    @Test
    public void testExistenceContrainteUniquePAOP()
    {
        VerificationExecution.verifierException(Throwable.class, () -> journaldao.testerExistenceContrainteUniquePAOP());
    }

    @Test
    public void sauvegardeTablesOperationJournalDansTablesTemporaires()
    {
        VerificationExecution.verifierException(Throwable.class, () -> journaldao.sauvegardeTablesOperationJournalDansTablesTemporaires());
    }

    @Test
    public void suppressionCopieTablesOperationJournal()
    {
        VerificationExecution.verifierException(Throwable.class, () -> journaldao.suppressionCopieTablesOperationJournal());
    }
}