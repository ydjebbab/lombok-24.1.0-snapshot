/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 */
package fr.gouv.finances.lombok.journal.dao.impl;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.SQLQuery;
import org.hibernate.Transaction;
import org.hibernate.type.LongType;
import org.hibernate.type.StringType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.gouv.finances.lombok.journal.dao.JournalDao;
import fr.gouv.finances.lombok.util.base.BaseDaoImpl;
import fr.gouv.finances.lombok.util.exception.ProgrammationException;

/**
 * Implémentation Hibernate de la gestion des données des opérations de journalisation propres à Oracle.
 *
 * @author chouard-cp
 * @author Christophe Breheret-Girardin
 */
abstract class AbstractJournalOracleDaoImpl extends BaseDaoImpl implements JournalDao
{
    protected static final Logger LOG = LoggerFactory.getLogger(AbstractJournalOracleDaoImpl.class);
    /**
     * Copie les tables du composant journal dans des tables temporaires.
     */
    @Override
    public void sauvegardeTablesOperationJournalDansTablesTemporaires()
    {
        // Sauvegarde dans une table temporaire
        sauvegarderUneTableDansTableTemporaire("ZOPERATIONJOURNAL_ZOPE");
        sauvegarderUneTableDansTableTemporaire("ZPARAMETREOPERATION_ZPAO");
        sauvegarderUneTableDansTableTemporaire("TJ_ZOPE_ZPAO_TJ");
    }

    /**
     * Suppression des copies des tables du composant journal.
     */
    @Override
    public void suppressionCopieTablesOperationJournal()
    {
        // Sauvegarde dans une table temporaire
        supprimerUneTableTemporaire("TJ_ZOPE_ZPAO_TJ");
        supprimerUneTableTemporaire("ZPARAMETREOPERATION_ZPAO");
        supprimerUneTableTemporaire("ZOPERATIONJOURNAL_ZOPE");
    }

    /** 
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.journal.dao.JournalDao#ajusterSequencesOracle()
     */
    @Override
    public void ajusterSequencesOracle()
    {
        Transaction trans = null;
        try 
        {
            trans = this.getSessionFactory().getCurrentSession().beginTransaction();
        } 
        catch (HibernateException e) 
        {
            LOG.debug(e.getMessage(), e);
            trans = this.getSessionFactory().openSession().beginTransaction();
        }

        try
        {
            // Création des séquences en ajustant les valeurs initiales
            // aux tables cibles
            ajusterSequenceToTableMax("ZOPE_ID_SEQUENCE", "ZOPERATIONJOURNAL_ZOPE", "ZOPE_ID", 1);
            ajusterSequenceToTableMax("ZPAO_ID_SEQUENCE", "ZPARAMETREOPERATION_ZPAO", "ZPAO_ID", 1);
        }
        catch (HibernateException | ProgrammationException exception)
        {
            LOG.warn("ERREUR LORS DE LA MIGRATION DES DONNEES", exception);
            if (trans != null)
            {
                trans.rollback();
            }
        }

        // en cas d'exception, rollback et restauration des données
        // sauvegardées au début de la transation
    }

    /**
     * Teste si la table PARAMETREOPERATION_PAOP possède une contrainte unique.
     * 
     * @return true, if test existence contrainte unique paop
     */
    @Override
    public boolean testerExistenceContrainteUniquePAOP()
    {
        boolean result = false;
        String sqlTestExistenceObjet =
            "SELECT COUNT(*) AS NBCONSTRAINT FROM USER_CONSTRAINTS WHERE TABLE_NAME=:TABLE_NAME AND CONSTRAINT_TYPE=:CONSTRAINT_TYPE";

        SQLQuery sqlTestExistenceObjetQuery = this.getSessionFactory().getCurrentSession().createSQLQuery(sqlTestExistenceObjet);
        sqlTestExistenceObjetQuery.setParameter("TABLE_NAME", "ZPARAMETREOPERATION_ZPAO", new StringType());
        sqlTestExistenceObjetQuery.setParameter("CONSTRAINT_TYPE", "U", new StringType());
        sqlTestExistenceObjetQuery.addScalar("NBCONSTRAINT", new LongType());
        List resultList = sqlTestExistenceObjetQuery.list();

        long nbobject = 0;
        if (resultList != null && resultList.size() == 1)
        {
            nbobject = ((Long) resultList.get(0)).longValue();
        }

        LOG.debug("NOMBRE DE CONTRAINTES UNIQUES SUR LA TABLE ZPARAMETREOPERATION_ZPAO = " + nbobject);

        result = (nbobject >= 1 ? true : false);

        LOG.debug("TEST EXISTENCE CONTRAINTE UNIQUE SUR LA TABLE ZPARAMETREOPERATION_ZPAO = " + result);
        // Si une contrainte existe, on teste si elle est active ou non
        if (result)
        {
            String sqlStatutContrainte =
                "SELECT STATUS AS STATUS FROM USER_CONSTRAINTS WHERE TABLE_NAME=:TABLE_NAME AND CONSTRAINT_TYPE=:CONSTRAINT_TYPE";
            SQLQuery sqlTestStatutContrainteQuery = this.getSessionFactory().getCurrentSession().createSQLQuery(sqlStatutContrainte);
            sqlTestStatutContrainteQuery.setParameter("TABLE_NAME", "ZPARAMETREOPERATION_ZPAO", new StringType());
            sqlTestStatutContrainteQuery.setParameter("CONSTRAINT_TYPE", "U", new StringType());
            sqlTestStatutContrainteQuery.addScalar("STATUS", new StringType());
            List resultConstraintList = sqlTestStatutContrainteQuery.list();

            String statutContrainte = "";
            if (resultConstraintList != null && resultConstraintList.size() == 1)
            {
                statutContrainte = ((String) resultConstraintList.get(0));
            }

            LOG.warn("STATUT DE LA CONTRAINTE " + statutContrainte);

            result = ("ENABLED".equals(statutContrainte) ? true : false);

        }

        // on vérifie si un index unique est présent sur les colonnes valeur et nom de
        // la table parametreoperation_paop - si c'est le cas, on le supprime

        String sqlIndexUnique =
            "SELECT INDEX_NAME AS INDEX_NAME FROM user_IND_COLUMNS WHERE table_name='ZPARAMETREOPERATION_ZPAO' AND column_name='VALEUR'";

        SQLQuery sqlIndexUniqueQuery = this.getSessionFactory().getCurrentSession().createSQLQuery(sqlIndexUnique);
        sqlIndexUniqueQuery.addScalar("INDEX_NAME", new StringType());
        List resultIndexUniqueList = sqlIndexUniqueQuery.list();

        String indexUniqueName = "";
        if (resultIndexUniqueList != null && resultIndexUniqueList.size() == 1)
        {
            indexUniqueName = ((String) resultIndexUniqueList.get(0));
            LOG.warn("NOM DE l'INDEX UNIQUE SUR LES COLONNES NOM ET VALEUR DE LA TABLE ZPARAMETREOPERATION_ZPAO "
                + indexUniqueName);

            // On supprime l'index associé à la clé unique si il y en a un
            LOG.warn("SUPPRESSION DE l'INDEX " + indexUniqueName);
            String sqlDropIndex = "DROP INDEX :indexName";
            SQLQuery sqlDropIndexQuery = this.getSessionFactory().getCurrentSession().createSQLQuery(sqlDropIndex);
            sqlDropIndexQuery.setParameter("indexName", indexUniqueName, new StringType());
            sqlDropIndexQuery.executeUpdate();
        }

        LOG.debug("TEST EXISTENCE D'UNE CONTRAINTE UNIQUE SUR LA TABLE ZPARAMETREOPERATION_ZPAO  : " + result);

        return result;
    }

    
    private void ajusterSequenceToTableMax(String sequenceName, String tableName, String columnIdName,
        long sequenceIncrement)
    {
        // Lecture de la valeur maximale de l'id dans la table
        long maxIdTableValue = this.findTableMaxIdValue(tableName, columnIdName);
        long valueForNewSequence = maxIdTableValue + 50;

        // Suppression de la séquence en cours
        supprimerSequenceOracle(sequenceName);

        // Recréation de la séquence
        creerSequenceOracle(sequenceName, valueForNewSequence, sequenceIncrement);
    }

    /**
     * Méthode permettant de récupérer l'identifiant maximum, en cours, dans une colonne.
     * 
     * @param tableName nom de la table
     * @param columnId identifiant de la colonne
     * @return l'identifiant maximum dans une colonne
     */
    private long findTableMaxIdValue(String tableName, String columnId)
    {
        StringBuilder sqlFindMaxId = new StringBuilder(27);
        sqlFindMaxId.append("SELECT MAX(");
        sqlFindMaxId.append(columnId);
        sqlFindMaxId.append(") AS MAXID FROM ");
        sqlFindMaxId.append(tableName);

        SQLQuery sqlFindMaxIdQuery = this.getSessionFactory().getCurrentSession().createSQLQuery(sqlFindMaxId.toString());
        sqlFindMaxIdQuery.addScalar("MAXID", new LongType());

        long maxId = 0;
        List maxIdList = sqlFindMaxIdQuery.list();

        if (maxIdList != null && !maxIdList.isEmpty() && maxIdList.get(0) != null)
        {
            maxId = ((Long) maxIdList.get(0)).longValue();
        }
        else
        {
            maxId = 0;
        }

        StringBuilder msg = new StringBuilder(51);
        msg.append("LA VALEUR MAXIMALE DE LA COLONNE ");
        msg.append(columnId);
        msg.append(" DE LA TABLE ");
        msg.append(tableName);
        msg.append(" EST ");
        msg.append(maxId);

        LOG.warn(msg.toString());

        return maxId;
    }
    
    /**
     * Création d'une copie temporaire d'une table.
     * 
     * @param tableName --
     */
    private void sauvegarderUneTableDansTableTemporaire(String tableName)
    {
        String nomTableTemporaire = creerNomTableTemporaire(tableName);

        // On contrôle que la table temporaire n'est pas déjà présente dans
        // le
        // schéma
        if (testerExistenceObjetOracle("TABLE", nomTableTemporaire))
        {
            StringBuilder msg = new StringBuilder(27);
            msg.append("LA TABLE ");
            msg.append(nomTableTemporaire);
            msg.append(" EXISTE DEJA DANS LE SCHEMA");
            throw new ProgrammationException(msg.toString());
        }

        // Compte le nombre de lignes de la table source
        long nbLignesTableSrc = compterNbLignesDansUneTable(tableName);

        StringBuilder sqlCreatetemporaryTable = new StringBuilder(32);
        sqlCreatetemporaryTable.append("CREATE TABLE  ");
        sqlCreatetemporaryTable.append(nomTableTemporaire);
        sqlCreatetemporaryTable.append(" AS SELECT * FROM ");
        sqlCreatetemporaryTable.append(tableName);

        SQLQuery sqlCreatetemporaryTableQuery =
            this.getSessionFactory().getCurrentSession().createSQLQuery(sqlCreatetemporaryTable.toString());

        // Création de la table
        sqlCreatetemporaryTableQuery.executeUpdate();

        // Compte le nombre de ligne de la table de sauvegarde
        long nbLignesTableSav = compterNbLignesDansUneTable(nomTableTemporaire);

        if (nbLignesTableSrc != nbLignesTableSav)
        {
            StringBuilder msg = new StringBuilder();
            msg.append("LA TABLE ");
            msg.append(nomTableTemporaire);
            msg.append(" CONTIENT ");
            msg.append(nbLignesTableSav);
            msg.append(" ALORS QUE LA TABLE ");
            msg.append(tableName);
            msg.append(nbLignesTableSav);
            msg.append(" CONTIENT ");
            msg.append(nbLignesTableSrc);
            msg.append(" LIGNES");
            throw new ProgrammationException(msg.toString());
        }

        StringBuilder msg = new StringBuilder(64);
        msg.append("LA TABLE ");
        msg.append(tableName);
        msg.append(" (");
        msg.append(nbLignesTableSrc);
        // ancien code
        // msg.append(" lignes) ");
        // msg.append(" A ETE SAUVEGARDEE DANS LA TABLE ");
        // nouveau code
        msg.append(" lignes) A ETE SAUVEGARDEE DANS LA TABLE ");
        msg.append(nomTableTemporaire);
        msg.append(" (");
        msg.append(nbLignesTableSav);
        msg.append(" lignes) ");
        LOG.warn(msg.toString());

    }

    /**
     * Supprime une séquence d'un schéma Oracle.
     * 
     * @param sequenceName --
     */
    private void supprimerSequenceOracle(String sequenceName)
    {
        // Si la séquence existe, on la supprime sinon on efait rien
        if (testerExistenceObjetOracle("SEQUENCE", sequenceName))
        {
            StringBuilder sqlSuppressionSequence = new StringBuilder();
            sqlSuppressionSequence.append("DROP SEQUENCE ");
            sqlSuppressionSequence.append(sequenceName);

            SQLQuery sqlSuppressionSequenceQuery =
                this.getSessionFactory().getCurrentSession().createSQLQuery(sqlSuppressionSequence.toString());

            // Suppression de la séquence
            sqlSuppressionSequenceQuery.executeUpdate();

            LOG.debug(sqlSuppressionSequence.toString());

            // Contrôle de la suppression de la séquence
            if (testerExistenceObjetOracle("SEQUENCE", sequenceName))
            {
                StringBuilder msg = new StringBuilder();
                msg.append("LA SUPPRESSION DE LA SEQUENCE ");
                msg.append(sequenceName);
                msg.append("A ECHOUE");
                LOG.warn(msg.toString());
                throw new ProgrammationException(msg.toString());
            }
            else
            {
                StringBuilder msg = new StringBuilder();
                msg.append("LA SEQUENCE ");
                msg.append(sequenceName);
                msg.append(" A ETE SUPPRIMEE");
                LOG.warn(msg.toString());
            }

        }
        else
        {
            StringBuilder msg = new StringBuilder();
            msg.append("LA SEQUENCE ");
            msg.append(sequenceName);
            msg.append(" N'EXISTE PAS");
            LOG.warn(msg.toString());
        }
    }
    
    /**
     * Crée une séquence Oracle.
     * 
     * @param sequenceName --
     * @param sequenceFirstValue --
     * @param sequenceIncrement --
     */
    private void creerSequenceOracle(String sequenceName, long sequenceFirstValue, long sequenceIncrement)
    {
        // On contrôle que la séquence n'existe pas encore
        if (testerExistenceObjetOracle("SEQUENCE", sequenceName))
        {
            StringBuilder msg = new StringBuilder();
            msg.append("LA SEQUENCE ");
            msg.append(sequenceName);
            msg.append("EXISTE DEJA");

            throw new ProgrammationException(msg.toString());
        }

        StringBuilder sqlCreationSequence = new StringBuilder(43);
        sqlCreationSequence.append(" CREATE SEQUENCE ");
        sqlCreationSequence.append(sequenceName);
        sqlCreationSequence.append(" START WITH ");
        sqlCreationSequence.append(sequenceFirstValue);
        sqlCreationSequence.append(" INCREMENT BY ");
        sqlCreationSequence.append(sequenceIncrement);

        LOG.debug(sqlCreationSequence.toString());

        SQLQuery sqlCreationSequenceQuery = getSessionFactory().getCurrentSession().createSQLQuery(sqlCreationSequence.toString());

        // Création de la séquence
        sqlCreationSequenceQuery.executeUpdate();

        // Contrôle de l'existence de la séquence créee
        if (!testerExistenceObjetOracle("SEQUENCE", sequenceName))
        {
            StringBuilder msg = new StringBuilder();
            msg.append("LA CREATION DE LA SEQUENCE ");
            msg.append(sequenceName);
            msg.append(" A ECHOUEE");
            LOG.warn(msg.toString());
            throw new ProgrammationException(msg.toString());
        }
        else
        {
            StringBuilder msg = new StringBuilder();
            msg.append("LA SEQUENCE ");
            msg.append(sequenceName);
            msg.append(" (START VALUE = ");
            msg.append(sequenceFirstValue);
            msg.append("  INCREMENT = ");
            msg.append(sequenceIncrement);
            msg.append(" )");
            msg.append(" A ETE CREEE");
            LOG.warn(msg.toString());
        }
    }
    
    /**
     * suppression dune table temporaire.
     * 
     * @param tableName --
     */
    private void supprimerUneTableTemporaire(String tableName)
    {
        String nomTableTemporaire = creerNomTableTemporaire(tableName);

        // On contrôle que la table temporaire est présente dans le schéma
        if (testerExistenceObjetOracle("TABLE", nomTableTemporaire))
        {
            StringBuilder sqlDroptemporaryTable = new StringBuilder();
            sqlDroptemporaryTable.append("DROP TABLE  ");
            sqlDroptemporaryTable.append(nomTableTemporaire);

            SQLQuery sqlCreatetemporaryTableQuery =
                this.getSessionFactory().getCurrentSession().createSQLQuery(sqlDroptemporaryTable.toString());

            // Suppression de la table
            sqlCreatetemporaryTableQuery.executeUpdate();

            // On controle que la table a bien été supprimée
            if (!testerExistenceObjetOracle("TABLE", nomTableTemporaire))
            {
                StringBuilder msg = new StringBuilder(17);
                msg.append("LA TABLE ");
                msg.append(nomTableTemporaire);
                msg.append(" A ETE SUPPRIMEE ");
                LOG.warn(msg.toString());
            }
            else
            {
                StringBuilder msg = new StringBuilder(23);
                msg.append("ERREUR : LA TABLE ");
                msg.append(nomTableTemporaire);
                msg.append(" N'A PAS ETE SUPPRIMEE ");
                LOG.warn(msg.toString());
                throw new ProgrammationException(msg.toString());
            }
        }
        else
        {
            StringBuilder msg = new StringBuilder(30);
            msg.append("LA TABLE ");
            msg.append(nomTableTemporaire);
            msg.append(" N'EXISTE PAS DANS LE SCHEMA. ");
            LOG.warn(msg.toString());
        }
    }

    /**
     * teste si un objet est présent dans un schéma Oracle.
     * 
     * @param objectType --
     * @param objectName --
     * @return true, if test existence objet oracle
     */
    private boolean testerExistenceObjetOracle(String objectType, String objectName)
    {
        boolean result = false;
        String sqlTestExistenceObjet =
            "SELECT COUNT(*) AS NBOBJECT FROM USER_OBJECTS WHERE OBJECT_TYPE=:OBJECT_TYPE AND OBJECT_NAME=:OBJECT_NAME";

        SQLQuery sqlTestExistenceObjetQuery = this.getSessionFactory().getCurrentSession().createSQLQuery(sqlTestExistenceObjet);
        sqlTestExistenceObjetQuery.setParameter("OBJECT_TYPE", objectType, new StringType());
        sqlTestExistenceObjetQuery.setParameter("OBJECT_NAME", objectName, new StringType());
        sqlTestExistenceObjetQuery.addScalar("NBOBJECT", new LongType());
        List resultList = sqlTestExistenceObjetQuery.list();

        long nbobject = 0;
        if (resultList != null && resultList.size() == 1 && resultList.get(0) != null)
        {
            nbobject = ((Long) resultList.get(0)).longValue();
        }

        result = (nbobject == 1 ? true : false);
        LOG.debug("TEST EXISTENCE DE L'OBJET '" + objectName + " : " + result);

        return result;
    }
    
    /**
     * Définition du nom de la table temporaire.
     * 
     * @param tableName --
     * @return the string
     */
    private String creerNomTableTemporaire(String tableName)
    {
        StringBuilder nomTableTemporaire = new StringBuilder();
        nomTableTemporaire.append(tableName);
        nomTableTemporaire.append("_SAV");
        return nomTableTemporaire.toString();
    }
    
    /**
     * Compte le nombre de ligne d'une table.
     * 
     * @param tableName --
     * @return nombre de lignes de la table
     */
    private long compterNbLignesDansUneTable(String tableName)
    {
        long nbLignes = 0;

        StringBuilder sqlCompteLigne = new StringBuilder(33);
        sqlCompteLigne.append("SELECT COUNT(*) AS NBLIGNES FROM ");
        sqlCompteLigne.append(tableName);

        SQLQuery sqlCompteLigneQuery = this.getSessionFactory().getCurrentSession().createSQLQuery(sqlCompteLigne.toString());
        sqlCompteLigneQuery.addScalar("NBLIGNES", new LongType());

        List resultList = sqlCompteLigneQuery.list();

        if (resultList != null && resultList.size() == 1 && resultList.get(0) != null)
        {
            nbLignes = ((Long) resultList.get(0)).longValue();

            StringBuilder msg = new StringBuilder();
            msg.append("LA TABLE ");
            msg.append(tableName);
            msg.append(" contient ");
            msg.append(nbLignes);
            msg.append(" LIGNES");
            LOG.debug(msg.toString());
        }
        else
        {
            nbLignes = 0;
        }

        return nbLignes;
    }
    
   
}