/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 */
package fr.gouv.finances.lombok.journal.dao.impl;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.StringJoiner;

import org.apache.commons.lang.StringUtils;
import org.hibernate.CacheMode;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Hibernate;
import org.hibernate.SQLQuery;
import org.hibernate.ScrollMode;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.DistinctRootEntityResultTransformer;
import org.hibernate.type.LongType;
import org.joda.time.DateTime;
import org.joda.time.Period;

import fr.gouv.finances.lombok.journal.bean.CriteresRecherchesJournal;
import fr.gouv.finances.lombok.journal.bean.IdentifiantsMetierObjOperation;
import fr.gouv.finances.lombok.journal.bean.OperationJournal;
import fr.gouv.finances.lombok.journal.bean.ParametreOperation;
import fr.gouv.finances.lombok.journal.dao.JournalDao;
import fr.gouv.finances.lombok.journal.dao.ProprieteJournal;
import fr.gouv.finances.lombok.util.date.TemporaliteUtils;
import fr.gouv.finances.lombok.util.exception.ProgrammationException;
import fr.gouv.finances.lombok.util.hibernate.DgcpScrollIteratorImpl;
import fr.gouv.finances.lombok.util.persistance.ScrollIterator;

/**
 * Implémentation Hibernate de la gestion des données des opérations de journalisation.
 *
 * @author chouard-cp
 * @author Christophe Breheret-Girardin
 */
public class JournalDaoImpl extends AbstractJournalOracleDaoImpl implements JournalDao
{
    /**
     * Constructeur.
     */
    public JournalDaoImpl()
    {
        super();
    }

    /**
     * {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.journal.dao.JournalDao#countOperationJournalAssocieAUnParametreOperation(fr.gouv.finances.lombok.journal.bean.ParametreOperation)
     *      .
     */
    @Override
    public int countOperationJournalAssocieAUnParametreOperation(ParametreOperation paramOper)
    {
        // Contrôle du paramètre
        if (paramOper == null)
        {
            throw new ProgrammationException("Aucun paramètre d'opération de journalisation fourni");
        }
        if (paramOper.getId() == null)
        {
            throw new ProgrammationException("Le paramètre d'opération de journalisation n'a pas d'identifiant");
        }

        return getHibernateTemplate().execute(session ->
        {
            // Paramétrage de la cible de recherche
            Criteria criteria = session.createCriteria(OperationJournal.class);

            // Paramétrage des restrictions de recherche
            criteria.createCriteria(ProprieteJournal.PARAMETRES.getNom())
                .add(Restrictions.idEq(paramOper.getId()));

            // Le résultat est le nombre d'élément trouvé
            criteria.setProjection(Projections.rowCount());

            // Exécution de la recherche
            return ((Long) criteria.uniqueResult()).intValue();
        });
    }

    /**
     * {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.journal.dao.JournalDao#deleteOperationJournal(fr.gouv.finances.lombok.journal.bean.OperationJournal)
     */
    @Override
    public void deleteOperationJournal(OperationJournal operationJournal)
    {
        if (operationJournal != null)
        {
            deleteObject(operationJournal);
        }
    }

    /**
     * {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.journal.dao.JournalDao#findOperationsJournalParCriteresRecherche(fr.gouv.finances.lombok.journal.bean.CriteresRecherchesJournal)
     *      .
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<OperationJournal> findOperationsJournalParCriteresRecherche(final CriteresRecherchesJournal crit)
    {
        // Contrôle du paramètre
        if (crit == null)
        {
            throw new ProgrammationException("Aucun critère de recherche fourni");
        }

        // Exécution de la recherche
        return getHibernateTemplate().execute(session ->
        {
            Criteria criteria = session.createCriteria(OperationJournal.class);

            // recherche avec identifiant structure
            String identifiantstructure = crit.getIdentifiantStructure();
            if ((StringUtils.isNotBlank(identifiantstructure)))
            {
                criteria.add(Restrictions.eq(ProprieteJournal.IDENTIFIANT_STRUCTURE.getNom(), identifiantstructure));
            }

            // Recherche avec nature operation
            if (crit.getNaturesOperations() != null && !crit.getNaturesOperations().isEmpty())
            {
                // Si la liste de nature contient plus d'un élément
                if (crit.getNaturesOperations().size() > 1)
                {
                    criteria.add(Restrictions.in(ProprieteJournal.NATURE.getNom(), crit.getNaturesOperations()));
                }
                else
                {
                    // Si la liste de nature contient un seul élément
                    if (StringUtils.isNotBlank(crit.getNaturesOperations().get(0)))
                    {
                        criteria.add(Restrictions.eq(ProprieteJournal.NATURE.getNom(), crit.getNaturesOperations().get(0)));
                    }
                }
            }

            // Recherche avec un code opération
            String codeoperation = null;
            if (crit.getCodesOperation() != null && !crit.getCodesOperation().isEmpty())
            {
                // Si la liste de code contient plus d'un élément
                if (crit.getCodesOperation().size() > 1)
                {
                    criteria.add(Restrictions.in(ProprieteJournal.CODE.getNom(), crit.getCodesOperation()));
                }
                else
                {
                    // Si la liste de code contient un seul élément
                    codeoperation = crit.getCodesOperation().get(0);
                }
            }
            else
            {
                // Si le critère de recherche ne contient qu'un seul code opération
                codeoperation = crit.getCodeOperation();
            }
            if (StringUtils.isNotBlank(codeoperation))
            {
                criteria.add(Restrictions.eq(ProprieteJournal.CODE.getNom(), codeoperation));
            }

            // Recherche sur identifiant
            String identifiantUtilisateurOuBatch = crit.getIdentifiantUtilisateurOuBatch();
            if (StringUtils.isNotBlank(identifiantUtilisateurOuBatch))
            {
                criteria.add(Restrictions.eq(ProprieteJournal.IDENTIFIANT.getNom(), identifiantUtilisateurOuBatch));
            }

            // Recherche sur la temporalité
            Date dateHeureOperation = crit.getDateHeureOperation();
            if (dateHeureOperation != null)
            {
                criteria.add(Restrictions.eq(ProprieteJournal.TEMPORALITE.getNom(), dateHeureOperation));
            }

            // recherche avec la date maximale
            Date dateMax = crit.getDateMax();
            if (dateMax != null)
            {
                DateTime dMax = new DateTime(dateMax).plus(Period.hours(24));
                criteria.add(Restrictions.le(ProprieteJournal.TEMPORALITE.getNom(), dMax.toDate()));

            }
            // recherche avec la date inférieure
            Date dateMin = crit.getDateMin();
            if (dateMin != null)
            {
                criteria.add(Restrictions.ge(ProprieteJournal.TEMPORALITE.getNom(), dateMin));
            }

            // recherche avec les parametres
            String valeurParamOperMax = crit.getValeurParamOperMax();
            String nomParamOper = crit.getNomParamOper();
            String valeurParamOperMin = crit.getValeurParamOperMin();
            String valeurParamOper = crit.getValeurParamOper();

            // Critère sur le nom
            Criteria criteriaParam = null;
            if (StringUtils.isNotBlank(nomParamOper))
            {
                criteriaParam = criteria.createCriteria(ProprieteJournal.PARAMETRES.getNom())
                    .add(Restrictions.eq(ProprieteJournal.NOM_PARAM.getNom(), nomParamOper));
            }

            // Critère sur la valeur max
            if (StringUtils.isNotBlank(valeurParamOperMax) && StringUtils.isNotBlank(nomParamOper))
            {
                criteriaParam.add(Restrictions.le(ProprieteJournal.VALEUR_PARAM.getNom(), valeurParamOperMax));
            }

            // Critère sur laleur min
            if (StringUtils.isNotBlank(valeurParamOperMin) && StringUtils.isNotBlank(nomParamOper))
            {
                criteriaParam.add(Restrictions.ge(ProprieteJournal.VALEUR_PARAM.getNom(), valeurParamOperMin));
            }

            // Critère sur la valeur exacte
            if (StringUtils.isNotBlank(valeurParamOper) && StringUtils.isNotBlank(nomParamOper))
            {
                criteriaParam.add(Restrictions.eq(ProprieteJournal.VALEUR_PARAM.getNom(), valeurParamOper));
            }

            // recherche à partir de la date avec heures et secondes à 0
            Date dateJJMMAAAA = crit.getDateJJMMAAAA();
            if (dateJJMMAAAA != null)
            {
                DateTime dtt = new DateTime(dateJJMMAAAA).plus(Period.hours(24));
                criteria.add(Restrictions.between(ProprieteJournal.TEMPORALITE.getNom(), dateJJMMAAAA, dtt.toDate()));
            }

            // recherche / apurement
            Boolean apurementPossible = crit.getApurementPossible();
            if (apurementPossible != null)
            {
                criteria.add(Restrictions.eq(ProprieteJournal.APUREMENT.getNom(), apurementPossible));
            }

            // Chargement des paramètres ne fonctionne pas sur une recherche de ParamOper !!??
            // criteria.setFetchMode((ProprieteJournal.PARAMETRES.getNom(), FetchMode.JOIN);
            // criteria.setResultTransformer(DistinctRootEntityResultTransformer.INSTANCE);

            // Exécution de la recherche
            List<OperationJournal> journaux = criteria.list();
            for (OperationJournal journal : journaux)
            {
                Hibernate.initialize(journal.getLesParamOperation());
            }
            return journaux;
        });
    }

    /**
     * {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.journal.dao.JournalDao#findOperationsJournalParIdentifiantMetierObjOperation(fr.gouv.finances.lombok.journal.bean.IdentifiantsMetierObjOperation)
     *      .
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<OperationJournal> findOperationsJournalParIdentifiantMetierObjOperation(
        final IdentifiantsMetierObjOperation unIdentifiantMetierObjOperation)
    {
        // Cas du paramètre non fourni
        if (unIdentifiantMetierObjOperation == null)
        {
            throw new ProgrammationException("identifiant métier obj opération non fourni");
        }

        return getHibernateTemplate().execute(session ->
        {
            Criteria criteria = session.createCriteria(OperationJournal.class);

            String valeurIdentifiantMetier1 = unIdentifiantMetierObjOperation.getValeurIdentifiantMetier1();
            if (StringUtils.isNotBlank(valeurIdentifiantMetier1))
            {
                criteria.add(Restrictions.eq(
                    new StringJoiner(".")
                        .add(ProprieteJournal.IDENTIFIANT_METIER.getNom())
                        .add("valeurIdentifiantMetier1").toString(),
                    valeurIdentifiantMetier1));
            }
            String valeurIdentifiantMetier2 = unIdentifiantMetierObjOperation.getValeurIdentifiantMetier2();
            if (StringUtils.isNotBlank(valeurIdentifiantMetier2))
            {
                criteria.add(Restrictions.eq(
                    new StringJoiner(".")
                        .add(ProprieteJournal.IDENTIFIANT_METIER.getNom())
                        .add("valeurIdentifiantMetier2").toString(),
                    valeurIdentifiantMetier2));
            }
            String valeurIdentifiantMetier3 = unIdentifiantMetierObjOperation.getValeurIdentifiantMetier3();
            if (StringUtils.isNotBlank(valeurIdentifiantMetier3))
            {
                criteria.add(Restrictions.eq(
                    new StringJoiner(".")
                        .add(ProprieteJournal.IDENTIFIANT_METIER.getNom())
                        .add("valeurIdentifiantMetier3").toString(),
                    valeurIdentifiantMetier3));
            }
            String valeurIdentifiantMetier4 = unIdentifiantMetierObjOperation.getValeurIdentifiantMetier4();
            if (StringUtils.isNotBlank(valeurIdentifiantMetier4))
            {
                criteria.add(Restrictions.eq(
                    new StringJoiner(".")
                        .add(ProprieteJournal.IDENTIFIANT_METIER.getNom())
                        .add("valeurIdentifiantMetier4").toString(),
                    valeurIdentifiantMetier4));
            }
            String valeurIdentifiantMetier5 = unIdentifiantMetierObjOperation.getValeurIdentifiantMetier5();
            if (StringUtils.isNotBlank(valeurIdentifiantMetier5))
            {
                criteria.add(Restrictions.eq(
                    new StringJoiner(".")
                        .add(ProprieteJournal.IDENTIFIANT_METIER.getNom())
                        .add("valeurIdentifiantMetier5").toString(),
                    valeurIdentifiantMetier5));
            }
            String libelleIdentifiantMetier1 = unIdentifiantMetierObjOperation.getLibelleIdentifiantMetier1();
            if (StringUtils.isNotBlank(libelleIdentifiantMetier1))
            {
                criteria.add(Restrictions.eq(
                    new StringJoiner(".")
                        .add(ProprieteJournal.IDENTIFIANT_METIER.getNom())
                        .add("libelleIdentifiantMetier1").toString(),
                    libelleIdentifiantMetier1));
            }

            String libelleIdentifiantMetier2 = unIdentifiantMetierObjOperation.getLibelleIdentifiantMetier2();
            if (StringUtils.isNotBlank(libelleIdentifiantMetier2))
            {
                criteria.add(Restrictions.eq(
                    new StringJoiner(".")
                        .add(ProprieteJournal.IDENTIFIANT_METIER.getNom())
                        .add("libelleIdentifiantMetier2").toString(),
                    libelleIdentifiantMetier2));
            }
            String libelleIdentifiantMetier3 = unIdentifiantMetierObjOperation.getLibelleIdentifiantMetier3();
            if (StringUtils.isNotBlank(libelleIdentifiantMetier3))
            {
                criteria.add(Restrictions.eq(
                    new StringJoiner(".")
                        .add(ProprieteJournal.IDENTIFIANT_METIER.getNom())
                        .add("libelleIdentifiantMetier3").toString(),
                    libelleIdentifiantMetier3));
            }
            String libelleIdentifiantMetier4 = unIdentifiantMetierObjOperation.getLibelleIdentifiantMetier4();
            if (StringUtils.isNotBlank(libelleIdentifiantMetier4))
            {
                criteria.add(Restrictions.eq(
                    new StringJoiner(".")
                        .add(ProprieteJournal.IDENTIFIANT_METIER.getNom())
                        .add("libelleIdentifiantMetier4").toString(),
                    libelleIdentifiantMetier4));
            }
            String libelleIdentifiantMetier5 = unIdentifiantMetierObjOperation.getLibelleIdentifiantMetier5();
            if (StringUtils.isNotBlank(libelleIdentifiantMetier5))
            {
                criteria.add(Restrictions.eq(
                    new StringJoiner(".")
                        .add(ProprieteJournal.IDENTIFIANT_METIER.getNom())
                        .add("libelleIdentifiantMetier5").toString(),
                    libelleIdentifiantMetier5));
            }

            // Chargement des paramètres
            criteria.setFetchMode(ProprieteJournal.PARAMETRES.getNom(), FetchMode.JOIN);
            criteria.setResultTransformer(DistinctRootEntityResultTransformer.INSTANCE);

            // Exécution de la recherche
            return criteria.list();
        });
    }

    /**
     * {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.journal.dao.JournalDao#findOperationsPerimees(java.lang.String).
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<OperationJournal> findOperationsPerimees(String datelimite)
    {
        if (StringUtils.isEmpty(datelimite))
        {
            throw new ProgrammationException("La date limite n'a pas été fournie");
        }
        DetachedCriteria criteria = DetachedCriteria.forClass(OperationJournal.class);
        criteria.add(Restrictions.lt(ProprieteJournal.TEMPORALITE.getNom(), TemporaliteUtils.getDate(datelimite, "MM/dd/yyyy")));
        criteria.add(Restrictions.eq(ProprieteJournal.APUREMENT.getNom(), true));
        return (List<OperationJournal>) getHibernateTemplate().findByCriteria(criteria);
    }

    /**
     * {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.journal.dao.JournalDao#findParametresOperations(fr.gouv.finances.lombok.journal.bean.OperationJournal)
     *      .
     */
    @Override
    public Set<ParametreOperation> findParametresOperations(OperationJournal operationJournal)
    {
        // Contrôle du paramètre
        if (operationJournal == null)
        {
            throw new ProgrammationException("Aucune opération de journalisation fournie");
        }
        if (operationJournal.getId() == null)
        {
            throw new ProgrammationException("L'opération de journalisation n'a pas d'identifiant");
        }

        return getHibernateTemplate().execute(session ->
        {
            // Paramétrage de la cible de recherche
            Criteria criteria = session.createCriteria(OperationJournal.class);

            // Paramétrage des restrictions de recherche
            criteria.add(Restrictions.idEq(operationJournal.getId()));

            // Paramétrage de la récupération des paramètres associés à l'opération de journalisation
            criteria.setFetchMode(ProprieteJournal.PARAMETRES.getNom(), FetchMode.JOIN);
            criteria.setResultTransformer(DistinctRootEntityResultTransformer.INSTANCE);

            // Exécution de la recherche
            OperationJournal journal = (OperationJournal) criteria.uniqueResult();

            // Récupération des paramètres d'opération de journalisation
            if (journal != null)
            {
                return journal.getLesParamOperation();
            }
            return new HashSet<>();
        });
    }

    /**
     * {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.journal.dao.JournalDao#findUneOperationJournalParIdUtilEtDateHeure(java.lang.String,
     *      java.util.Date).
     */
    @Override
    public OperationJournal findUneOperationJournalParIdUtilEtDateHeure(
        String idUtilOuBatch, Date dateHeureOperation)
    {
        return getHibernateTemplate().execute(session ->
        {
            // Paramétrage de la cible à rechercher
            Criteria criteria = session.createCriteria(OperationJournal.class);

            // Paramétrage des restrictions de recherche
            criteria.add(Restrictions.eq(ProprieteJournal.IDENTIFIANT.getNom(), idUtilOuBatch));
            criteria.add(Restrictions.eq(ProprieteJournal.TEMPORALITE.getNom(), dateHeureOperation));

            // Paramétrage de la récupération des paramètres associés à l'opération de journalisation
            criteria.setFetchMode(ProprieteJournal.PARAMETRES.getNom(), FetchMode.JOIN);
            criteria.setResultTransformer(DistinctRootEntityResultTransformer.INSTANCE);

            // Exécution de la recherche
            return (OperationJournal) criteria.uniqueResult();
        });
    }

    /**
     * {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.journal.dao.JournalDao#findUneOperationParId(java.lang.Long).
     */
    @Override
    public OperationJournal findUneOperationParId(Long id)
    {
        return getHibernateTemplate().execute(session ->
        {
            // Paramétrage de la cible de recherche
            Criteria criteria = session.createCriteria(OperationJournal.class);

            // Paramétrage des restrictions de recherche
            criteria.add(Restrictions.idEq(id));

            // Paramétrage de la récupération des paramètres associés à l'opération de journalisation
            criteria.setFetchMode(ProprieteJournal.PARAMETRES.getNom(), FetchMode.JOIN);
            criteria.setResultTransformer(DistinctRootEntityResultTransformer.INSTANCE);

            // Exécution de la recherche
            return (OperationJournal) criteria.uniqueResult();
        });
    }

    /**
     * {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.journal.dao.JournalDao#initialiseParametresOperation(fr.gouv.finances.lombok.journal.bean.OperationJournal)
     *      .
     */
    @Override
    public void initialiseParametresOperation(OperationJournal operationJournal)
    {
        if (operationJournal != null)
        {
            getHibernateTemplate().initialize(operationJournal.getLesParamOperation());
        }
    }

    /**
     * {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.journal.dao.JournalDao#saveNouvelleOperationAuJournal(fr.gouv.finances.lombok.journal.bean.OperationJournal)
     */
    @Override
    public void saveNouvelleOperationAuJournal(OperationJournal uneoperationjournal)
    {
        getHibernateTemplate().saveOrUpdate(uneoperationjournal);
    }

    /**
     * {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.journal.dao.JournalDao#countNombreParametresOrphelins().
     */
    @Override
    public Long countNombreParametresOrphelins()
    {
        // Initialisation de la requête SQL
        StringBuilder sqlComptage = new StringBuilder(140)
            .append("SELECT COUNT(*) AS NBLIGNES FROM  ZPARAMETREOPERATION_ZPAO A ")
            .append(" WHERE NOT EXISTS (SELECT * FROM TJ_ZOPE_ZPAO_TJ B WHERE A.ZPAO_ID = B.ZPAO_ID)");

        // Exécution de la requête
        return getHibernateTemplate().execute(session ->
            (Long) session.createSQLQuery(sqlComptage.toString())
                .addScalar("NBLIGNES", new LongType())
                .uniqueResult());
    }

    /**
     * {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.journal.dao.JournalDao#supprimerParametresOperationsOrphelins().
     */
    @Override
    public void supprimerParametresOperationsOrphelins()
    {
        StringBuilder sqlDelParamsOpesOrphs = new StringBuilder(118);
        sqlDelParamsOpesOrphs.append(
            "DELETE FROM ZPARAMETREOPERATION_ZPAO A  WHERE NOT EXISTS (SELECT * FROM TJ_ZOPE_ZPAO_TJ B WHERE A.ZPAO_ID = B.ZPAO_ID)");

        SQLQuery sqlDeleteParametresOrphelins =
            this.getSessionFactory().getCurrentSession().createSQLQuery(sqlDelParamsOpesOrphs.toString());

        // Suppression des lignes orphelines
        sqlDeleteParametresOrphelins.executeUpdate();
    }

    /**
     * {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.journal.dao.JournalDao#findOperationsPerimeesAUneDatePourUneDureeDeRetention(java.util.Date,
     *      java.lang.String, int).
     */
    @Override
    public ScrollIterator findOperationsPerimeesAUneDatePourUneDureeDeRetention(final Date dateCourante,
        final String typeDureeDeConservation, final int joursDeRetention)
    {
        return getHibernateTemplate().execute(session ->
            new DgcpScrollIteratorImpl(
                session.createCriteria(OperationJournal.class).add(
                    Restrictions.eq(ProprieteJournal.TYPE_DUREE_CONSERVATION.getNom(), typeDureeDeConservation))
                    .add(Restrictions
                        .lt(ProprieteJournal.TEMPORALITE.getNom(), TemporaliteUtils.ajouterJour(dateCourante, -joursDeRetention)))
                    .add(Restrictions.or(Restrictions.isNull(ProprieteJournal.APUREMENT.getNom())
                        , Restrictions.eq(ProprieteJournal.APUREMENT.getNom(), true)))
                    .setCacheMode(CacheMode.IGNORE).scroll(ScrollMode.FORWARD_ONLY)));
    }

    /**
     * {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.journal.dao.JournalDao#findAllOperationsJournal().
     */
    @Override
    public ScrollIterator findAllOperationsJournal()
    {
        return getHibernateTemplate().execute(session ->
            new DgcpScrollIteratorImpl(
                session.createCriteria(OperationJournal.class).setCacheMode(CacheMode.IGNORE)
                    .scroll(ScrollMode.FORWARD_ONLY)));
    }
}
