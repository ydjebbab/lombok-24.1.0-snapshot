/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : ParametreOperationDaoImpl.java
 *
 */
package fr.gouv.finances.lombok.journal.dao.impl;

/**
 * 
 * @author amleplatinec-cp
 *
 */

import java.util.Iterator;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;

import fr.gouv.finances.lombok.journal.bean.ParametreOperation;
import fr.gouv.finances.lombok.journal.dao.ParametreOperationDao;
import fr.gouv.finances.lombok.util.base.BaseDaoImpl;

/**
 * Class ParametreOperationDaoImpl.
 * 
 * @author amleplatinec
 * @version $Revision: 1.3 $ Date: 14 déc. 2009
 */
public class ParametreOperationDaoImpl extends BaseDaoImpl implements ParametreOperationDao
{

    public ParametreOperationDaoImpl()
    {
        super();
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param unParametreOperation
     * @see fr.gouv.finances.lombok.journal.dao.ParametreOperationDao#deleteParametreOperation(fr.gouv.finances.lombok.journal.bean.ParametreOperation)
     */
    @Override
    public void deleteParametreOperation(ParametreOperation unParametreOperation)
    {
        getHibernateTemplate().delete(unParametreOperation);
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param nom
     * @return parametre operation
     * @see fr.gouv.finances.lombok.journal.dao.ParametreOperationDao#findParametreOperationParNom(java.lang.String)
     */
    @Override
    public ParametreOperation findParametreOperationParNom(String nom)
    {

        DetachedCriteria detachedCriteria = DetachedCriteria.forClass(ParametreOperation.class);

        if (StringUtils.isEmpty(nom))
        {
            detachedCriteria.add(Restrictions.isNull("nom"));
        }
        else
        {
            detachedCriteria.add(Restrictions.eq("nom", nom));
        }

        Iterator iterParametresOperations = getHibernateTemplate().findByCriteria(detachedCriteria).iterator();

        ParametreOperation parametreOperation = null;
        if (iterParametresOperations.hasNext())
        {
            parametreOperation = (ParametreOperation) iterParametresOperations.next();

        }
        return parametreOperation;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param nom
     * @param valeur
     * @return parametre operation
     * @see fr.gouv.finances.lombok.journal.dao.ParametreOperationDao#findParametreOperationParNomEtValeur(java.lang.String,
     *      java.lang.String)
     */
    @Override
    public ParametreOperation findParametreOperationParNomEtValeur(String nom, String valeur)
    {

        DetachedCriteria detachedCriteria = DetachedCriteria.forClass(ParametreOperation.class);

        /*
         * Les caractères composés uniquement de blancs sont enregistrés en base de données comme une valeur nulle, si
         * la colonne cible est de type varchar
         */
        if (StringUtils.isEmpty(valeur))
        {
            detachedCriteria.add(Restrictions.isNull("valeur"));
        }
        else
        {
            detachedCriteria.add(Restrictions.eq("valeur", valeur));
        }

        if (StringUtils.isEmpty(nom))
        {
            detachedCriteria.add(Restrictions.isNull("nom"));
        }
        else
        {
            detachedCriteria.add(Restrictions.eq("nom", nom));
        }

        Iterator iterParametresOperations = getHibernateTemplate().findByCriteria(detachedCriteria).iterator();

        ParametreOperation parametreOperation = null;
        if (iterParametresOperations.hasNext())
        {
            parametreOperation = (ParametreOperation) iterParametresOperations.next();

        }
        return parametreOperation;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param unParametreOperation
     * @see fr.gouv.finances.lombok.journal.dao.ParametreOperationDao#saveParametreOperation(fr.gouv.finances.lombok.journal.bean.ParametreOperation)
     */
    @Override
    public void saveParametreOperation(ParametreOperation unParametreOperation)
    {
        getHibernateTemplate().saveOrUpdate(unParametreOperation);
    }

}
