/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 */
package fr.gouv.finances.lombok.atlas.dao.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.FetchMode;
import org.hibernate.ScrollMode;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.orm.hibernate5.HibernateTemplate;

import fr.gouv.finances.lombok.atlas.bean.DocumentAtlas;
import fr.gouv.finances.lombok.atlas.bean.EtatDocumentAtlas;
import fr.gouv.finances.lombok.atlas.bean.TypeDocumentAtlas;
import fr.gouv.finances.lombok.atlas.dao.DocumentAtlasDao;
import fr.gouv.finances.lombok.atlas.dao.ProprieteDocumentAtlas;
import fr.gouv.finances.lombok.util.base.BaseDaoImpl;
import fr.gouv.finances.lombok.util.exception.ProgrammationException;
import fr.gouv.finances.lombok.util.hibernate.DgcpScrollIteratorImpl;
import fr.gouv.finances.lombok.util.persistance.ScrollIterator;

/**
 * Implémentation Hibernate de la gestion des données Atlas.
 *
 * @author chouard-cp
 * @author Christophe Breheret-Girardin
 */
public class DocumentAtlasDaoImpl extends BaseDaoImpl implements DocumentAtlasDao
{

    /** Initialisation de la journalisation. */
    protected static final Logger LOGGER = LoggerFactory.getLogger(DocumentAtlasDaoImpl.class);

    /**
     * Constructeur.
     */
    public DocumentAtlasDaoImpl()
    {
        super();
    }

    /** 
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.atlas.dao.DocumentAtlasDao#findDocumentAtlasParIdentifiantAtlas(
     * java.lang.String).
     */
    @Override
    public DocumentAtlas findDocumentAtlasParIdentifiantAtlas(String identifiantAtlas)
    {
        // Journalisation de la recherche
        LOGGER.debug("Recherche du document Atlas d'identifiant '{}'", identifiantAtlas);

        // Si aucun paramètre, la recherche n'est pas exécutée
        if (StringUtils.isEmpty(identifiantAtlas))
        {
            return null;
        }

        // Exécution de la requête
        return getHibernateTemplate().execute
            (
                session -> (DocumentAtlas) session.createCriteria(DocumentAtlas.class)
                            // Critère sur l'identifiant Atlas unique
                            .add(Restrictions.eq(ProprieteDocumentAtlas.IDENTIFIANT.getNom(), identifiantAtlas))
                            // Récupération du type de document associé
                            .setFetchMode(ProprieteDocumentAtlas.PROPRIETE_TYPE_DOCUMENT.getNom(), FetchMode.JOIN)
                            // L'identifiant étant unique, un seul document correspond à la demande
                            .uniqueResult()
            );
    }

    /** 
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.atlas.dao.DocumentAtlasDao#findListeDocumentAtlasParIdentifiantAtlas(
     * java.util.Collection).
     */
    @Override
    @SuppressWarnings("unchecked")
    public List<DocumentAtlas> findListeDocumentAtlasParIdentifiantAtlas(Collection<String> colIdentifiantAtlas)
    {
        // Journalisation de la recherche
        LOGGER.debug("Recherche des documents Atlas d'identifiant '{}'", colIdentifiantAtlas);

        // Si aucun paramètre, la recherche n'est pas exécutée
        if (colIdentifiantAtlas == null || colIdentifiantAtlas.isEmpty())
        {
            return new ArrayList<>();
        }

        DetachedCriteria criteria = DetachedCriteria.forClass(DocumentAtlas.class);
        // Critères sur les identifiants uniques
        criteria.add(Restrictions.in(ProprieteDocumentAtlas.IDENTIFIANT.getNom(), colIdentifiantAtlas));
        // Récupération du type de document associé
        criteria.setFetchMode(ProprieteDocumentAtlas.PROPRIETE_TYPE_DOCUMENT.getNom(), FetchMode.JOIN);
        // Récupération de tous les documents correspondant aux critères
        return (List<DocumentAtlas>) getHibernateTemplate().findByCriteria(criteria);
    }

    /** 
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.atlas.dao.DocumentAtlasDao#findListeDocumentAtlasParType(
     * fr.gouv.finances.lombok.atlas.bean.TypeDocumentAtlas).
     */
    @Override
    @SuppressWarnings("unchecked")
    public List<DocumentAtlas> findListeDocumentAtlasParType(TypeDocumentAtlas typeDocument)
    {
        // Journalisation de la recherche
        LOGGER.debug("Recherche des documents Atlas de type '{}'", typeDocument);

        // Si aucun paramètre, la recherche n'est pas exécutée
        if (typeDocument == null)
        {
            return new ArrayList<>();
        }
        
        DetachedCriteria criteria = DetachedCriteria.forClass(DocumentAtlas.class);
        // Critère sur le type de document
        criteria.add(Restrictions.eq(ProprieteDocumentAtlas.PROPRIETE_TYPE_DOCUMENT.getNom(), typeDocument));
        // Récupération du type de document associé
        criteria.setFetchMode(ProprieteDocumentAtlas.PROPRIETE_TYPE_DOCUMENT.getNom(), FetchMode.JOIN);
        // Récupération de tous les documents correspondant aux critères
        return (List<DocumentAtlas>) getHibernateTemplate().findByCriteria(criteria);
    }

    /** 
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.atlas.dao.DocumentAtlasDao#findListeDocumentAtlasParTypeEtDates(
     * fr.gouv.finances.lombok.atlas.bean.TypeDocumentAtlas, java.util.Date, java.util.Date).
     */
    @Override
    @SuppressWarnings("unchecked")
    public List<DocumentAtlas> findListeDocumentAtlasParTypeEtDates(
        TypeDocumentAtlas typeDocument, Date dateDebut, Date dateFin)
    {
        // Journalisation de la recherche
        LOGGER.debug("Recherche des documents Atlas de type '{}', de dates comprises entre '{}' et '{}'"
            , typeDocument, dateDebut, dateFin);

        DetachedCriteria criteria = DetachedCriteria.forClass(DocumentAtlas.class);
        // Critère sur la date de péremption
        criteria.add(Restrictions.between(ProprieteDocumentAtlas.PROPRIETE_DATE_PEREMPTION.getNom(), dateDebut, dateFin));
        // Critère sur le type de document
        criteria.add(Restrictions.eq(ProprieteDocumentAtlas.PROPRIETE_TYPE_DOCUMENT.getNom(), typeDocument));
        // Récupération du type de document associé
        criteria.setFetchMode(ProprieteDocumentAtlas.PROPRIETE_TYPE_DOCUMENT.getNom(), FetchMode.JOIN);
        // Récupération de tous les documents correspondant aux critères
        return (List<DocumentAtlas>) getHibernateTemplate().findByCriteria(criteria);
    }

    /** 
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.atlas.dao.DocumentAtlasDao#findTousLesTypesDocument().
     */
    @Override
    public List<TypeDocumentAtlas> findTousLesTypesDocument()
    {
        // Journalisation de la recherche
        LOGGER.trace("Recherche de tous les types de document Atlas");

        // Récupération de tous les documents sans critère
        return loadAllObjects(TypeDocumentAtlas.class);
    }

    /** 
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.atlas.dao.DocumentAtlasDao#findTypeDocumentParCode(java.lang.String).
     */
    @Override
    public TypeDocumentAtlas findTypeDocumentParCode(String code)
    {
        // Journalisation de la recherche
        LOGGER.debug("Recherche du document Atlas de code '{}'", code);

        // Gestion des paramètres non fournis
        if (StringUtils.isEmpty(code))
        {
            throw new ProgrammationException("Le code du type de document n'a pas été fourni pour la recherche");
        }

        // Exécution de la requête
        return getHibernateTemplate().execute
            (
                session -> (TypeDocumentAtlas) session.createCriteria(TypeDocumentAtlas.class)
                            // Critère sur le code du type de document
                            .add(Restrictions.eq(ProprieteDocumentAtlas.PROPRIETE_CODE_TYPE_DOCUMENT.getNom(), code))
                            // Le code étant unique, un seul type de document correspond à la demande
                            .uniqueResult()
            );
    }

    /** 
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.atlas.dao.DocumentAtlasDao#findDocumentsAtlasEtContenusAArchiverParIterateur(
     * java.lang.Integer).
     */
    @Override
    public ScrollIterator findDocumentsAtlasEtContenusAArchiverParIterateur(Integer nombreOccurences)
    {
        // Journalisation de la recherche
        LOGGER.trace("Recherche des documents Atlas à archiver");

        // Récupération de l'utilitaire Spring facilitant les échanges Hibernate
        HibernateTemplate hibernateTemplate = getHibernateTemplate(true);
        // Modification de la taille d'extraction des données (important pour les résultats volumineux)
        hibernateTemplate.setFetchSize(nombreOccurences);

        // Exécution de la requête
        return hibernateTemplate.execute
            (
                session -> new DgcpScrollIteratorImpl(
                            session.createCriteria(DocumentAtlas.class)
                            // Critère sur l'état des documents à archiver
                            .add(Restrictions.eq(ProprieteDocumentAtlas.PROPRIETE_ETAT.getNom(), EtatDocumentAtlas.AARCHIVER))
                            // Récupération des informations du fichier associé
                            .setFetchMode(ProprieteDocumentAtlas.PROPRIETE_FICHIER.getNom(), FetchMode.JOIN)
                            // Récupération du contenu du fichier associé
                            .setFetchMode(ProprieteDocumentAtlas.PROPRIETE_FICHIER.getNom()
                                + "." + ProprieteDocumentAtlas.PROPRIETE_CONTENU_FICHIER.getNom(), FetchMode.JOIN)
                            // Activation du mode curseur
                            .scroll(ScrollMode.FORWARD_ONLY))
            );
    }

    /** 
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.atlas.dao.DocumentAtlasDao#findDocumentsAtlasEtContenusAPurgerParIterateur(
     * java.lang.Integer).
     */
    @Override
    public ScrollIterator findDocumentsAtlasEtContenusAPurgerParIterateur(Integer nombreOccurences)
    {
        // Journalisation de la recherche
        LOGGER.trace("Recherche des documents Atlas à purger");

        // Récupération de l'utilitaire Spring facilitant les échanges Hibernate
        HibernateTemplate hibernateTemplate = getHibernateTemplate(true);
        // Modification de la taille d'extraction des données (important pour les résultats volumineux)
        hibernateTemplate.setFetchSize(nombreOccurences);

        // Exécution de la requête
        return hibernateTemplate.execute
            (
                session -> new DgcpScrollIteratorImpl(
                            session.createCriteria(DocumentAtlas.class)
                            // Critère sur l'état des documents
                            .add(Restrictions.eq(ProprieteDocumentAtlas.PROPRIETE_ETAT.getNom()
                                , EtatDocumentAtlas.ARCHIVE))
                            // Un fichier doit être rattaché au document
                            .add(Restrictions.isNotNull(ProprieteDocumentAtlas.PROPRIETE_FICHIER.getNom()))
                            // Récupération des informations du fichier associé
                            .setFetchMode(ProprieteDocumentAtlas.PROPRIETE_FICHIER.getNom(), FetchMode.JOIN)
                            // Activation du mode curseur
                            .scroll(ScrollMode.FORWARD_ONLY))
            );
    }

    /** 
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.atlas.dao.DocumentAtlasDao#findDocumentsAtlasPerimesParIterateur(
     * java.lang.Integer).
     */
    @Override
    public ScrollIterator findDocumentsAtlasPerimesParIterateur(Integer nombreOccurences)
    {
        // Journalisation de la recherche
        LOGGER.trace("Recherche des documents Atlas périmés");

        // Récupération de l'utilitaire Spring facilitant les échanges Hibernate
        HibernateTemplate hibernateTemplate = getHibernateTemplate(true);
        // Modification de la taille d'extraction des données (important pour les résultats volumineux)
        hibernateTemplate.setFetchSize(nombreOccurences);

        // Exécution de la requête
        return hibernateTemplate.execute
            (
                session -> new DgcpScrollIteratorImpl(
                            session.createCriteria(DocumentAtlas.class)
                            // Critère sur l'état des documents
                            .add(Restrictions.lt(ProprieteDocumentAtlas.PROPRIETE_DATE_PEREMPTION.getNom()
                                , Calendar.getInstance().getTime()))
                            // Activation du mode curseur
                            .scroll(ScrollMode.FORWARD_ONLY))
            );
    }

    /** 
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.atlas.dao.DocumentAtlasDao#findDocumentsEnvoyesAAtlasParIterateur(
     * java.lang.Integer, java.sql.Timestamp).
     */
    @Override
    public ScrollIterator findDocumentsEnvoyesAAtlasParIterateur(Integer nombreOccurences,
        final Timestamp dateDEnvoiAtlas)
    {
        // Journalisation de la recherche
        LOGGER.trace("Recherche des documents Atlas à archiver à la date '{}'", dateDEnvoiAtlas);

        // Récupération de l'utilitaire Spring facilitant les échanges Hibernate
        HibernateTemplate hibernateTemplate = getHibernateTemplate(true);
        // Modification de la taille d'extraction des données (important pour les résultats volumineux)
        hibernateTemplate.setFetchSize(nombreOccurences);

        // Exécution de la requête
        return hibernateTemplate.execute
            (
                session -> new DgcpScrollIteratorImpl(
                            session.createCriteria(DocumentAtlas.class)
                            // Critère sur la date d'envoi (égalité stricte donc attention à la partie horaire)
                            .add(Restrictions.eq(ProprieteDocumentAtlas.PROPRIETE_DATE_ENVOI.getNom()
                                , dateDEnvoiAtlas))
                            // Critère sur l'état des documents
                            .add(Restrictions.eq(ProprieteDocumentAtlas.PROPRIETE_ETAT.getNom()
                                , EtatDocumentAtlas.ENVOYEPOURARCHIVAGE))
                            // Activation du mode curseur
                            .scroll(ScrollMode.FORWARD_ONLY))
            );
    }
}
