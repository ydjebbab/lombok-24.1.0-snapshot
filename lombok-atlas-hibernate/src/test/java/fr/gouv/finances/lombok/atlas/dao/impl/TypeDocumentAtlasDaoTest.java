package fr.gouv.finances.lombok.atlas.dao.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.gouv.finances.lombok.atlas.bean.TypeDocumentAtlas;
import fr.gouv.finances.lombok.test.controle.ControleDonnees;
import fr.gouv.finances.lombok.test.controle.VerificationExecution;
import fr.gouv.finances.lombok.util.exception.ProgrammationException;

/**
 * Tests unitaires automatisés du DAO gérant les données des types de document Atlas.
 *
 * @author Christophe Breheret-Girardin
 * @author CF
 */
@ContextConfiguration({"classpath:/conf/applicationContext-atlas-hibernate-test.xml",
        "classpath*:/conf/applicationContext-core-persistance-test.xml",
        "classpath*:/conf/applicationContext-core-orm-test.xml",
        "classpath*:/conf/applicationContext-atlas-dao.xml"})
@ActiveProfiles(profiles = {"atlas", "hibernate"})
@RunWith(SpringJUnit4ClassRunner.class)
public class TypeDocumentAtlasDaoTest extends AtlasDaoCore
{
    public TypeDocumentAtlasDaoTest()
    {
        super();
    }

    protected static final Logger LOGGER = LoggerFactory.getLogger(TypeDocumentAtlasDaoTest.class);

    /**
     * Initialisation des données en base avant chaque test.
     */
    @Before
    public void creerDonnees()
    {
        // Purge des données de la base
        purgetablesdao.purgerTables("ZDOCUMENTATLAS_ZDOA", "ZTYPEDOCUMENTATLAS_ZTDA", "ZFICHIERJOINT_ZFIJ");

        // Vérification de l'absence de données
        ControleDonnees.verifierElements(documentatlasdao.loadAllObjects(TypeDocumentAtlas.class));

        // Génération des types de document Atlas, insertion en base de données et sauvegarde dans une Map
        mapTypeDocumentAtlas = IntStream.rangeClosed(1, 2).mapToObj(this::getTypeDocument)
            .peek(documentatlasdao::saveObject)
            .collect(Collectors.toMap(t -> Integer.parseInt(t.getCode()), t -> t));

        // Vérification des données à partir de l'image de la base
        ControleDonnees.verifierElements(documentatlasdao.loadAllObjects(TypeDocumentAtlas.class), mapTypeDocumentAtlas.get(1),
            mapTypeDocumentAtlas.get(2));

        // Suppression du cache pour forcer les requêtes des tests
        documentatlasdao.clearPersistenceContext();
    }

    /**
     * Test de la méthode {@link fr.gouv.finances.lombok.atlas.dao.DocumentAtlasDao#findTousLesTypesDocument()}.
     */
    @Test
    public void findTousLesTypesDocument()
    {
        ControleDonnees.verifierElements(documentatlasdao.findTousLesTypesDocument(), mapTypeDocumentAtlas.get(1),
            mapTypeDocumentAtlas.get(2));
    }

    /**
     * Test de la méthode
     * {@link fr.gouv.finances.lombok.atlas.dao.DocumentAtlasDao#findTypeDocumentParCode(java.lang.String)}.
     */
    @Test
    public void findTypeDocumentParCode()
    {
        // Exécution de la méthode à tester avec des paramètres vides ou non initialisés
        VerificationExecution.verifierException(ProgrammationException.class, () -> documentatlasdao.findTypeDocumentParCode(null));
        VerificationExecution.verifierException(ProgrammationException.class, () -> documentatlasdao.findTypeDocumentParCode(""));

        // Exécution de la méthode à tester sans résultat et vérification
        assertNull(documentatlasdao.findTypeDocumentParCode(VALEUR_INCONNUE));

        // Exécution de la méthode à tester sur toutes les données et vérifications
        mapTypeDocumentAtlas.entrySet().stream()
            .forEach(e -> assertEquals(e.getValue(), documentatlasdao.findTypeDocumentParCode(e.getValue().getCode())));
    }
}