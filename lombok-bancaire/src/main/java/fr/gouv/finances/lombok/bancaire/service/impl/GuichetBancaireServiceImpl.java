/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 */
package fr.gouv.finances.lombok.bancaire.service.impl;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.support.TransactionTemplate;

import fr.gouv.finances.lombok.bancaire.bean.GuichetBancaire;
import fr.gouv.finances.lombok.bancaire.dao.GuichetBancaireDao;
import fr.gouv.finances.lombok.bancaire.service.GuichetBancaireBatchService;
import fr.gouv.finances.lombok.bancaire.service.GuichetBancaireService;
import fr.gouv.finances.lombok.util.base.BaseServiceImpl;
import fr.gouv.finances.lombok.util.exception.ApplicationExceptionTransformateur;
import fr.gouv.finances.lombok.util.exception.RegleGestionException;

/**
 * Implémentation des services métiers de gestion des guichets bancaires.
 * 
 * @author amleplatinec
 * @author Christophe Breheret-Girardin
 */
public class GuichetBancaireServiceImpl extends BaseServiceImpl implements GuichetBancaireService, GuichetBancaireBatchService
{
    private static final Logger log = LoggerFactory.getLogger(GuichetBancaireServiceImpl.class);

    /** Déclaration du DAO permettant la gestion des données bancaires. */
    private GuichetBancaireDao guichetbancairedao;

    /** Déclaration du template de transaction. */
    private TransactionTemplate transactionTemplate;

    /** Une synchronisation avec la BDD doit avoir lieu au bout d'un nombre max de guichets */
    private static final int NBRE_GUICHET_FLUSH = 20;

    /** Valeur à recevoir pour exécuter un écrasement */
    private static final String VALEUR_ECRASEMENT = "ecrasement";

    public GuichetBancaireServiceImpl()
    {
        super();
    }

    /**
     * Méthode principale du batch de chargement des guichets bancaires.
     *
     * @param guichetsBancaires liste des guichets bancaires à créer
     * @param horodatageDuTraitement horodatage du traitement
     * @param modetraitement mode de traitement
     */
    @Override
    public void creerLesGuichetsBancaires(final List<GuichetBancaire> guichetsBancaires, final Date horodatageDuTraitement,
        final String modetraitement)
    {
        log.trace("Debut de mise à jour d'un lot de guichets bancaires");

        try
        {
            transactionTemplate.execute(transactionStatus -> {
                // Détermination s'il y a écrasement à appliquer ou non
                final boolean isEcrasement = isEcrasement(modetraitement);

                // Dans le cas de l'écrasement, suppression de tous les guichets bancaires
                suppressionGuichetBancaire(isEcrasement);

                // Journalisation du nombre de guichets à traiter
                log.debug("Début de la mise à jour des {} guichets bancaires", guichetsBancaires.size());

                // Initialisation d'un index de parcours des guichets bancaires
                AtomicInteger index = new AtomicInteger();

                // Gestion de tous les guichets bancaires
                guichetsBancaires.stream()
                    .forEach(guichetBancaire -> gererGuichetBancaire(guichetBancaire, index, horodatageDuTraitement, isEcrasement));

                // Indication du statut périmé pour les guichets bancaires, dans le cas d'un non écrasement
                indicationStatutPerime(isEcrasement, horodatageDuTraitement);

                log.trace("Fin de la mise à jour des guichets bancaires");
                return null;
            });
        }
        catch (RuntimeException e)
        {
            // Gestion des erreurs
            log.error(
                "Une erreur s'est produite (voir ci-après): la mise à jour du lot de guichets bancaires est annulée");
            log.error("Rollback effectué : aucune mise a jour n'a été prise en compte");
            throw ApplicationExceptionTransformateur.transformer("Erreur sur création des guichets bancaires ", e);
        }
        log.trace("Fin de mise à jour d'un lot de guichets bancaires");
    }

    /**
     * Méthode permettant de supprimer tous les guichets bancaires existants si le mode écrasement est demandé.
     *
     * @param ecrasement faut-il écraser les données ?
     */
    private void suppressionGuichetBancaire(boolean ecrasement)
    {
        if (ecrasement)
        {
            log.trace("Mode écrasement : suppression de tous les guichets existants");
            guichetbancairedao.deleteTousLesGuichetsBancaires();
        }
    }

    /**
     * Méthode permettant d'indiquer un statut périmé pour tous les guichets existants à une date donnée (= pas en mode
     * écrasement).
     *
     * @param ecrasement faut-il écraser les données ?
     * @param horodatageDuTraitement date de dernière mise à jour
     */
    private void indicationStatutPerime(boolean ecrasement, Date horodatageDuTraitement)
    {
        if (!ecrasement)
        {
            log.trace("Mode sans ecrasement : marquage des guichets bancaires périmés");
            guichetbancairedao.marqueLesGuichetsBancairesPerimesADate(horodatageDuTraitement);
        }
    }

    /**
     * Méthode permettant de gérer la mise à jour d'un guichet bancaire.
     *
     * @param guichetBancaire guichet bancaire à traiter
     * @param index index du guichet bancaire dans la liste des guichers à gérer
     * @param horodatageDuTraitement horodatage du traitement
     * @param ecrasement les données existantes ont-elles été supprimées ?
     */
    private void gererGuichetBancaire(GuichetBancaire guichetBancaire, AtomicInteger index, Date horodatageDuTraitement, boolean ecrasement)
    {
        log.debug("Traitement du guichet bancaire n°{}", index);

        // Calcul permettant de savoir si le guichet est en voie de péremption
        guichetBancaire.calculerAttributEstEnVoieDePeremption();

        // Initialisation de la date de dernière mise à jour du guichet bancaire
        setDateDerniereMaj(guichetBancaire, horodatageDuTraitement);

        // Journalisation du guichet bancaire
        log.debug("Traitement du guichet : {}", guichetBancaire);

        // Mis à jour du guichet bancaire
        majGuichet(guichetBancaire, ecrasement);

        // Synchronisation avec la base de données à intervalle régulier
        synchronisationBdd(index);
    }

    /**
     * Synchronisation avec la base de données à interval régulier.
     *
     * @param index index du guichet bancaire en cours de traitement
     */
    private void synchronisationBdd(AtomicInteger index)
    {
        // Tous les "NBRE_GUICHET_FLUSH", un flush est effectué
        if (index.incrementAndGet() % NBRE_GUICHET_FLUSH == 0)
        {
            guichetbancairedao.flush();
            guichetbancairedao.clearPersistenceContext();
        }
    }

    /**
     * Méthode permettant d'initialiser la date de dernière mise à jour.
     *
     * @param guichetBancaire guichet bancaire à gérer
     * @param horodatageDuTraitement horodatage du traitement
     */
    private void setDateDerniereMaj(GuichetBancaire guichetBancaire, Date horodatageDuTraitement)
    {
        // Si pas d'horodatage, utilisation de l'instant présent
        if (horodatageDuTraitement == null)
        {
            guichetBancaire.setDateDerniereMiseAJour(new Timestamp(new Date().getTime()));
        }
        else
        {
            guichetBancaire.setDateDerniereMiseAJour(new Timestamp(horodatageDuTraitement.getTime()));
        }
    }

    /**
     * Récupération de l'information sur une demande d'écrasement ou non.
     *
     * @param modetraitement mode de traitement demandé
     * @return l'information sur une demande d'écrasement ou non
     */
    private boolean isEcrasement(String modetraitement)
    {
        boolean isEcrasement = false;
        if (modetraitement != null)
        {
            isEcrasement = modetraitement.compareTo(VALEUR_ECRASEMENT) == 0;
        }
        log.info("Mode ecrasement activé (true/false?) : {}", isEcrasement);
        return isEcrasement;
    }

    /**
     * Méthode permettant de persister un guichet bancaire.
     *
     * @param guichetBancaire guichet bancaire à persister
     * @param isEcrasement les données existantes ont-elles été supprimées ?
     */
    private void majGuichet(GuichetBancaire guichetBancaire, boolean isEcrasement)
    {
        // Si les données ont été supprimées, création d'un guichet bancaire, sinon modification de celui-ci
        if (isEcrasement)
        {
            creerGuichetBancaire(guichetBancaire);
        }
        else
        {
            creerOuMettreAJourUnGuichetBancaire(guichetBancaire);
        }
    }

    /**
     * Méthode permettant de mettre à jour un guichet bancaire.
     *
     * @param guichetBancaire données bancaires
     */
    @Override
    public void modifierGuichetBancaire(final GuichetBancaire guichetBancaire)
    {
        log.debug("Modification du guichet bancaire - identifiant : '{}' - libelle = '{}'", guichetBancaire.getId(),
            guichetBancaire.getDenominationComplete());

        // Exécution de la modification
        guichetbancairedao.saveObject(guichetBancaire);

        log.trace("Modification du guichet bancaire effectuée");
    }

    /**
     * Recherche le GuichetBancaire non périmé correspondant à un couple codeguichet, codeetablissement.
     *
     * @param codeGuichet code du guichet
     * @param codeEtablissement code de l'établissement
     * @return un {@link GuichetBancaire} ou null si aucun guichet bancaire ne correspond
     */
    @Override
    public GuichetBancaire rechercherGuichetBancaireNonPerimeParCodeGuichetEtCodeEtablissement(String codeGuichet,
        String codeEtablissement)
    {
        return guichetbancairedao.findGuichetBancaireNonPerimeParCodeGuichetEtCodeEtablissement(codeGuichet,
            codeEtablissement);
    }

    /**
     * Recherche les GuichetBancaires correspondant par code commune et code etablissement.
     * 
     * @param commune --
     * @param codeEtablissement --
     * @return liste de {@link GuichetBancaire}
     */
    @Override
    public List<GuichetBancaire> rechercherGuichetsBancaireParCommuneEtCodeEtablissement(String commune, String codeEtablissement)
    {
        return guichetbancairedao.findGuichetsBancaireParCommuneEtCodeEtablissement(commune, codeEtablissement);
    }

    /**
     * Recherche les GuichetBancaires correspondant par dénomination et code etablissement.
     * 
     * @param denominationComplete --
     * @param codeEtablissement --
     * @return liste de {@link GuichetBancaire}
     */
    @Override
    public List<GuichetBancaire> rechercherGuichetsBancaireParDenominationCompleteBanqueEtCodeEtablissement(String denominationComplete,
        String codeEtablissement)
    {
        return guichetbancairedao.findGuichetsBancaireParDenominationCompleteBanqueEtCodeEtablissement(
            denominationComplete, codeEtablissement);

    }

    /**
     * Retourne la liste des guichets bancaires non périmés.
     * 
     * @return liste de {@link GuichetBancaire}
     */
    @Override
    public List<GuichetBancaire> rechercherGuichetsBancairesNonPerimes()
    {
        return guichetbancairedao.findGuichetsBancairesNonPerimes();
    }

    /**
     * Recherche les GuichetBancaires par code etablissement.
     * 
     * @param codeEtablissement --
     * @return liste de {@link GuichetBancaire}
     */
    @Override
    public List<GuichetBancaire> rechercherGuichetsBancairesParCodeEtablissement(String codeEtablissement)
    {
        return guichetbancairedao.findGuichetsBancairesParCodeEtablissement(codeEtablissement);
    }

    /**
     * Recherche les GuichetBancaires correspondant à un couple codeguichet, codeetablissement.
     * 
     * @param codeGuichet --
     * @param codeEtablissement --
     * @return liste de {@link GuichetBancaire}
     */
    @Override
    public List<GuichetBancaire> rechercherGuichetsBancairesParCodeGuichetEtCodeEtablissement(String codeGuichet,
        String codeEtablissement)
    {
        return guichetbancairedao
            .findGuichetsBancairesParCodeGuichetEtCodeEtablissement(codeGuichet, codeEtablissement);
    }

    /**
     * Recherche les GuichetBancaires par code postal.
     * 
     * @param codePostal --
     * @return liste de {@link GuichetBancaire}
     */
    @Override
    public List<GuichetBancaire> rechercherGuichetsBancairesParCodePostal(String codePostal)
    {
        return guichetbancairedao.findGuichetsBancairesParCodePostal(codePostal);

    }

    /**
     * Recherche les GuichetBancaires par commune.
     * 
     * @param commune --
     * @return liste de {@link GuichetBancaire}
     */
    @Override
    public List<GuichetBancaire> rechercherGuichetsBancairesParCommune(String commune)
    {
        return guichetbancairedao.findGuichetsBancairesParCommune(commune);
    }

    /**
     * Recherche les GuichetBancaires par commune.
     * 
     * @param denominationComplete --
     * @return liste de {@link GuichetBancaire}
     */
    @Override
    public List<GuichetBancaire> rechercherGuichetsBancairesParDenominationCompleteBanque(String denominationComplete)
    {
        return guichetbancairedao.findGuichetsBancairesParDenominationCompleteBanque(denominationComplete);
    }

    /**
     * Retourne la liste des guichets bancaires périmés ou non.
     * 
     * @return liste de {@link GuichetBancaire}
     */
    @Override
    public List<GuichetBancaire> rechercherTousGuichetsBancaires()
    {
        return guichetbancairedao.findTousGuichetsBancaires();

    }

    /**
     * Modificateur de l'attribut guichetbancairedao.
     * 
     * @param guichetbancairedao le nouveau guichetbancairedao
     */
    @Override
    public void setGuichetbancairedao(GuichetBancaireDao guichetbancairedao)
    {
        this.guichetbancairedao = guichetbancairedao;
    }

    /**
     * Modificateur de l'attribut transaction template.
     * 
     * @param transactionTemplate le nouveau transaction template
     */
    @Override
    public void setTransactionTemplate(TransactionTemplate transactionTemplate)
    {
        this.transactionTemplate = transactionTemplate;
    }

    /**
     * Supprime un guichet bancaire.
     * 
     * @param guichetBancaire données bancaires
     */
    @Override
    public void supprimerGuichetBancaire(GuichetBancaire guichetBancaire)
    {
        guichetbancairedao.deleteObject(guichetBancaire);
    }

    /**
     * Teste s'il existe un guichet bancaire non périmé associé à un couple code guichet code établissement.
     * 
     * @param codeGuichet --
     * @param codeEtablissement --
     * @return true si le guichet bancaire existe, false s'il n'existe pas
     */
    @Override
    public boolean testerExistenceGuichetBancaireNonPerimeParCodeGuichetEtCodeEtablissement(String codeGuichet,
        String codeEtablissement)
    {
        return guichetbancairedao.testeExistenceGuichetBancaireNonPerimeParCodeGuichetEtCodeEtablissement(codeGuichet,
            codeEtablissement);
    }

    /**
     * Teste s'il existe un guichet bancaire périmé ou non associé à un couple code guichet code établissement.
     * 
     * @param codeGuichet --
     * @param codeEtablissement --
     * @return true si le guichet bancaire existe, false s'il n'existe pas
     */
    @Override
    public boolean testerExistenceGuichetBancairePerimeOuNonParCodeGuichetEtCodeEtablissement(String codeGuichet,
        String codeEtablissement)
    {
        return guichetbancairedao.testeExistenceGuichetBancairePerimeOuNonParCodeGuichetEtCodeEtablissement(
            codeGuichet, codeEtablissement);
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param codeGuichet
     * @param codeEtablissement
     * @return list
     * @see fr.gouv.finances.lombok.bancaire.service.GuichetBancaireService#verifierExistenceGuichetBancaire(java.lang.String,
     *      java.lang.String)
     */
    @Override
    public List<GuichetBancaire> verifierExistenceGuichetBancaire(String codeGuichet, String codeEtablissement)
    {
        List<GuichetBancaire> listeGuichetBancaire =
            rechercherGuichetsBancairesParCodeGuichetEtCodeEtablissement(codeGuichet, codeEtablissement);
        if (listeGuichetBancaire.isEmpty())
        {
            throw new RegleGestionException("Aucun guichet bancaire de trouvé avec le code établissement "
                + codeEtablissement + " et code guichet " + codeGuichet);
        }
        return listeGuichetBancaire;
    }

    /**
     * methode Creer guichet bancaire : --.
     * 
     * @param guichetBancaire données bancaires
     */
    private void creerGuichetBancaire(GuichetBancaire guichetBancaire)
    {
        guichetbancairedao.saveObject(guichetBancaire);
        log.trace("Création du nouveau guichet bancaire effectuée");
    }

    /**
     * Méthode permettant de créer ou mettre à jour un guichet bancaire.
     * 
     * @param guichetBancaireAJour guichet bancaire à traiter
     */
    private void creerOuMettreAJourUnGuichetBancaire(GuichetBancaire guichetBancaireAJour)
    {
        GuichetBancaire unGuichetBancaireAMettreAJour =
            rechercherGuichetBancaireNonPerimeParCodeGuichetEtCodeEtablissement(
                guichetBancaireAJour.getCodeGuichet(), guichetBancaireAJour.getCodeEtablissement());

        // Création du guichet qui n'existe pas en base
        if (unGuichetBancaireAMettreAJour == null)
        {
            log.trace("Guichet inexistant en base ");

            creerGuichetBancaire(guichetBancaireAJour);

            log.trace("Sauvegarde du nouveau guichet bancaire effectuée");
        }
        else
        {
            log.trace("Guichet déjà existant en base, modification de celui-ci");

            // Mise à jour des valeurs du guichet trouvé en base
            affecterNouveauxAttributsMetiers(unGuichetBancaireAMettreAJour, guichetBancaireAJour);
            unGuichetBancaireAMettreAJour.setDateDerniereMiseAJour(guichetBancaireAJour.getDateDerniereMiseAJour());
            modifierGuichetBancaire(unGuichetBancaireAMettreAJour);

            log.trace("Mise à jour guichet bancaire passee avec succes");
        }
    }

    /**
     * Méthode permettant d'affecter des nouveaux attributs metiers.
     * 
     * @param unGuichetBancaireAMettreAJour --
     * @param unGuichetBancaireAJour --
     */
    private void affecterNouveauxAttributsMetiers(GuichetBancaire unGuichetBancaireAMettreAJour,
        GuichetBancaire unGuichetBancaireAJour)
    {
        // Attribut "adresse"
        if (unGuichetBancaireAJour.getAdresseGuichet() != null)
        {
            unGuichetBancaireAMettreAJour.setAdresseGuichet(unGuichetBancaireAJour.getAdresseGuichet());
        }
        // Attribut "année de modification"
        if (unGuichetBancaireAJour.getAnneeDeModification() != null)
        {
            unGuichetBancaireAMettreAJour.setAnneeDeModification(unGuichetBancaireAJour.getAnneeDeModification());
        }
        // Attribut "code enregistrement"
        if (unGuichetBancaireAJour.getCodeEnregistrement() != null)
        {
            unGuichetBancaireAMettreAJour.setCodeEnregistrement(unGuichetBancaireAJour.getCodeEnregistrement());
        }
        // Attribut "code établissement"
        if (unGuichetBancaireAJour.getCodeEtablissement() != null)
        {
            unGuichetBancaireAMettreAJour.setCodeEtablissement(unGuichetBancaireAJour.getCodeEtablissement());
        }
        // Attribut "code guichet"
        if (unGuichetBancaireAJour.getCodeGuichet() != null)
        {
            unGuichetBancaireAMettreAJour.setCodeGuichet(unGuichetBancaireAJour.getCodeGuichet());
        }
        // Attribut "code postal"
        if (unGuichetBancaireAJour.getCodePostal() != null)
        {
            unGuichetBancaireAMettreAJour.setCodePostal(unGuichetBancaireAJour.getCodePostal());
        }
        // Attribut "code réglement des crédits"
        if (unGuichetBancaireAJour.getCodeReglementDesCredits() != null)
        {
            unGuichetBancaireAMettreAJour.setCodeReglementDesCredits(unGuichetBancaireAJour
                .getCodeReglementDesCredits());
        }
        // Attribut "dénomination abrégée"
        if (unGuichetBancaireAJour.getDenominationAbregee() != null)
        {
            unGuichetBancaireAMettreAJour.setDenominationAbregee(unGuichetBancaireAJour.getDenominationAbregee());
        }
        // Attribut "dénomination complète"
        if (unGuichetBancaireAJour.getDenominationComplete() != null)
        {
            unGuichetBancaireAMettreAJour.setDenominationComplete(unGuichetBancaireAJour.getDenominationComplete());
        }
        // Attribut "libellé abréviation de domiciliation"
        if (unGuichetBancaireAJour.getLibelleAbrevDeDomiciliation() != null)
        {
            unGuichetBancaireAMettreAJour.setLibelleAbrevDeDomiciliation(unGuichetBancaireAJour
                .getLibelleAbrevDeDomiciliation());
        }
        // Attribut "mois de modification"
        if (unGuichetBancaireAJour.getMoisDeModification() != null)
        {
            unGuichetBancaireAMettreAJour.setMoisDeModification(unGuichetBancaireAJour.getMoisDeModification());
        }
        // Attribut "nom du guichet"
        if (unGuichetBancaireAJour.getNomGuichet() != null)
        {
            unGuichetBancaireAMettreAJour.setNomGuichet(unGuichetBancaireAJour.getNomGuichet());
        }
        // Attribut "nouveau code établissement"
        if (unGuichetBancaireAJour.getNouveauCodeEtablissement() != null)
        {
            unGuichetBancaireAMettreAJour.setNouveauCodeEtablissement(unGuichetBancaireAJour
                .getNouveauCodeEtablissement());
        }
        // Attribut "nouveau code guichet"
        if (unGuichetBancaireAJour.getNouveauCodeGuichet() != null)
        {
            unGuichetBancaireAMettreAJour.setNouveauCodeGuichet(unGuichetBancaireAJour.getNouveauCodeGuichet());
        }
        // Attribut "ville"
        if (unGuichetBancaireAJour.getVilleCodePostal() != null)
        {
            unGuichetBancaireAMettreAJour.setVilleCodePostal(unGuichetBancaireAJour.getVilleCodePostal());
        }
        unGuichetBancaireAMettreAJour.setEstEnVoieDePeremption(unGuichetBancaireAJour.isEstEnVoieDePeremption());
    }
}
