/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 *
 */
package fr.gouv.finances.lombok.bancaire.batch;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.StringJoiner;
import java.util.function.Consumer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import fr.gouv.finances.lombok.bancaire.bean.GuichetBancaire;
import fr.gouv.finances.lombok.bancaire.service.GuichetBancaireBatchService;
import fr.gouv.finances.lombok.batch.ServiceBatchCommunImpl;
import fr.gouv.finances.lombok.util.exception.ApplicationExceptionTransformateur;
import fr.gouv.finances.lombok.util.exception.ExploitationException;
import fr.gouv.finances.lombok.util.exception.RegleGestionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

/**
 * Batch de chargement de fichier contenant des codes guichets et etablissements bancaires.<br/>
 * Le mode de traitement "ecrasement" recharge complétement les données à partir du fichier spécifié.<br/>
 * Tout autre mot clé provoque une mise à jour des données existantes.<br/>
 * Si certains guichets bancaires ne sont pas retrouvés dans le fichier traité,
 * ils sont marqués comme étant périmés.
 *
 * @author amleplatinec-cp
 * @author Christophe Breheret-Girardin
 */
@Service("traitementchargementguichetsetetablissementstabank")
@Profile("batch")
@Lazy(true)
public class TraitementChargementGuichetsEtEtablissementsTabankImpl extends ServiceBatchCommunImpl implements TraitementChargementGuichetsEtEtablissementsTabank {

    /**
     * Initialisation de la journalisation.
     */
    protected static final Logger LOGGER = LoggerFactory.getLogger(TraitementChargementGuichetsEtEtablissementsTabankImpl.class);

    /**
     * Constante donnant le format de ligne BDFSI.
     */
    private static final int LONGUEUR_ARTICLES_BDFSI = 178;

    /**
     * Nombre de ligne traitée.
     */
    private int numLigne;

    /**
     * Service métier de gestion des guichets bancaires.
     */
    @Autowired()
    @Qualifier("guichetbancairebatchserviceso")
    private GuichetBancaireBatchService guichetbancairebatchserviceso;

    /**
     * fichiertabank.
     */
    private String fichiertabank;

    /**
     * Fichier à traiter
     */
    private File fichier;

    /**
     * Mode de traitement ("ecrasement" recharge complétement les données).
     */
    private String modetraitement;

    /**
     * Faut-il ignorer la première ligne?
     */
    private boolean ignorerPremiereLigne;

    /**
     * Constructeur.
     */
    public TraitementChargementGuichetsEtEtablissementsTabankImpl() {
        super();
    }

    /**
     * {@inheritDoc}.
     *
     * @see fr.gouv.finances.lombok.batch.ServiceBatchCommunImpl#traiterBatch()
     */
    @Override
    public void traiterBatch() {
        // Ré-initalisation des paramètres en cas de lancement du batch plusieurs fois avec une seule injection
        numLigne = 0;
        fichier = null;
        // Recupération du nombre de lignes traitées
        numLigne = traiterFichierTabank();
        LOGGER.info("Nombre d'articles traités : {}", numLigne);
    }

    /**
     * Méthode permettant d'effectuer le traitement.
     *
     * @return le nombre de ligne traitée
     */
    @Override
    public int traiterFichierTabank() {
        LOGGER.info("Debut lecture du fichier en entree : '{}'", fichiertabank);
        StringJoiner erreurParLigne = new StringJoiner("|");
        String ligneFichierTabank = null;
        List<GuichetBancaire> lesGuichetsBancaires = new ArrayList<>();
        boolean isErreur = false;
        try {
            // Récupération de l'emplacement du fichier à traiter
            String nomurl = new URL(fichiertabank).getFile();
            fichier = new File(nomurl);
        } catch (MalformedURLException mue) {
            // Emplacement du fichier non trouvé
            String chemin = getChemin(fichier);
            LOGGER.error("Fichier '{}' non trouvé", chemin, mue);
            throw new ExploitationException("Attention le fichier TABANK n'a pas été trouvé sur ce chemin '" + chemin + "' copiez le sur ce chemin ou corrigez " + " sa localisation dans le fichier 'application.properties' et relancez", chemin, mue);
        }
        // Cette trace ne doit pas être supprimée car elle permet d'ajouter un comportement via un TU
        LOGGER.trace("Service : '{}'", guichetbancairebatchserviceso.toString());
        // Chargement du fichier à traiter
        try (BufferedReader leReader = new BufferedReader(new FileReader(fichier))) {
            // La première ligne doit-elle être ignorée ?
            if (ignorerPremiereLigne) {
                // Lecture de la première sans traitement de celle-ci, pour l'ignorer
                String premiereLigne = leReader.readLine();
                LOGGER.debug("La 1ère ligne suivante est ignorée : {}", premiereLigne);
            }
            // Lecture et décodage des lignes du fichier
            while ((ligneFichierTabank = leReader.readLine()) != null) {
                // Info d'une ligne de lecture : numLigne - isErreurFormat - nbreErreurLigne
                StringJoiner infoParLigne = new StringJoiner("-");
                numLigne++;
                infoParLigne.add(String.valueOf(numLigne));
                // Récupération du fichier bancaire correspondant à la ligne lue
                GuichetBancaire unGuichetLu = null;
                // Vérification des données de la ligne
                if (isErreurFormatLigneFichier(ligneFichierTabank)) {
                    infoParLigne.add(String.valueOf(true)).add("0");
                    isErreur = true;
                } else {
                    unGuichetLu = construireObjetGuichetBancaireAPartirDeLigneFichier(ligneFichierTabank);
                    int nbreErreur = verifierGuichetBancaire(unGuichetLu);
                    infoParLigne.add(String.valueOf(false)).add(String.valueOf(nbreErreur));
                    LOGGER.error("nombre total d'erreur sur la ligne '{}' : {}", numLigne, nbreErreur);
                    if (nbreErreur != 0) {
                        isErreur = true;
                        LOGGER.error("Ligne incorrecte:\t{}\tNumero de ligne : {}", ligneFichierTabank, numLigne);
                    } else {
                        // Ajout du guichet à la liste des guichets lus
                        lesGuichetsBancaires.add(unGuichetLu);
                    }
                }
                erreurParLigne.add(infoParLigne.toString());
            }
            // Le fichier est vide
            if (numLigne == 0) {
                LOGGER.info("Le fichier '{}' en entrée est vide", fichiertabank);
                throw new ExploitationException("Le fichier en entrée est vide", getChemin(fichier));
            }
            // Il y a des erreurs dans les articles, une exception est levée pour alerter l'exploitation
            if (isErreur) {
                String erreur = erreurParLigne.toString();
                LOGGER.debug("Erreurs sur les lignes du fichier (numLigne - erreur format - nbre erreur article |) : {}", erreur);
                throw new ExploitationException("Attention : les erreurs précédentes sur certaines lignes ne permettent pas " + "de traiter le fichier tabank. Aucune mise à jour effectuée. " + "Corrigez les erreurs et recommencez le traitement", erreur);
            }
            // Il n'y a pas d'erreur de lecture, les guichets bancaires vont être créés
            Date horodatageDuTraitement = new Date();
            LOGGER.info("La lecture du fichier est terminée et aucune erreur de forme n'a été relevée");
            LOGGER.info("Debut de la mise à jour des données");
            // Création des guichets bancaires
            guichetbancairebatchserviceso.creerLesGuichetsBancaires(lesGuichetsBancaires, horodatageDuTraitement, modetraitement);
            // Journalisation du traitement
            LOGGER.trace("Aucune erreur durant le traitement");
            LOGGER.info("Nombre d'articles traités : {}", numLigne);
            return numLigne;
        } catch (IOException exception) {
            // Gestion des erreurs
            throw ApplicationExceptionTransformateur.transformer("Problème lors du traitement du fichier TABANK : " + getChemin(fichier), exception);
        }
    }

    /**
     * Méthode permettant de dénombrer les erreurs d'une ligne du fichier.
     *
     * @param guichetBancaire guichet bancaire à analyser
     */
    private int verifierGuichetBancaire(GuichetBancaire guichetBancaire) {
        int nberreurssurarticle = 0;
        nberreurssurarticle += getNbreErreur(guichetBancaire, GuichetBancaire::verifierCodeEtablissement);
        nberreurssurarticle += getNbreErreur(guichetBancaire, GuichetBancaire::verifierCodeGuichet);
        nberreurssurarticle += getNbreErreur(guichetBancaire, GuichetBancaire::verifierLibelleAbrevDeDomiciliation);
        return nberreurssurarticle;
    }

    /**
     * Méthode permettant de compter les erreurs sur l'exécution d'une méthode du guichet bancaire.
     *
     * @param guichetBancaire guichet bancaire à analyser
     * @param consumer méthode à exécuter sur le guichet bancaire
     * @return le nombre d'erreur sur le guichet bancaire
     */
    private int getNbreErreur(GuichetBancaire guichetBancaire, Consumer<GuichetBancaire> consumer) {
        try {
            consumer.accept(guichetBancaire);
            return 0;
        } catch (RegleGestionException exception) {
            LOGGER.error(exception.getMessage(), exception);
            return 1;
        }
    }

    /**
     * Méthode de construction d'un objet guichet bancaire à partir de ligne fichier.
     *
     * @param ligneFichierTabank ligne lue dans le fichier
     * @return le guichet bancaire correspondant à la ligne lue
     */
    @Override
    public GuichetBancaire construireObjetGuichetBancaireAPartirDeLigneFichier(String ligneFichierTabank) {
        // Alimentation  des valeurs du guichet
        GuichetBancaire unguichet = new GuichetBancaire();
        unguichet.setCodeEtablissement(ligneFichierTabank.substring(0, 5));
        unguichet.setCodeGuichet(ligneFichierTabank.substring(5, 10));
        unguichet.setNomGuichet(ligneFichierTabank.substring(10, 30));
        unguichet.setAdresseGuichet(ligneFichierTabank.substring(30, 59));
        unguichet.setCodePostal(ligneFichierTabank.substring(59, 64));
        unguichet.setVilleCodePostal(ligneFichierTabank.substring(64, 85));
        unguichet.setDenominationComplete(ligneFichierTabank.substring(85, 125));
        unguichet.setDenominationAbregee(ligneFichierTabank.substring(125, 135));
        unguichet.setLibelleAbrevDeDomiciliation(ligneFichierTabank.substring(135, 159));
        unguichet.setCodeReglementDesCredits(ligneFichierTabank.substring(159, 163));
        unguichet.setMoisDeModification(ligneFichierTabank.substring(163, 165));
        unguichet.setAnneeDeModification(ligneFichierTabank.substring(165, 167));
        unguichet.setCodeEnregistrement(ligneFichierTabank.substring(167, 168));
        unguichet.setNouveauCodeEtablissement(ligneFichierTabank.substring(168, 173));
        unguichet.setNouveauCodeGuichet(ligneFichierTabank.substring(173, 178));
        return unguichet;
    }

    /**
     * Modificateur de l'attribut fichiertabank.
     *
     * @param fichiertabank le nouveau fichiertabank
     */
    @Override
    @Value("${traitementchargementguichetsetetablissementstabank.fichiertabank}")
    public void setFichiertabank(String fichiertabank) {
        this.fichiertabank = fichiertabank;
    }

    /**
     * Modificateur de l'attribut guichetbancairebatchserviceso.
     *
     * @param guichetbancairebatchserviceso le nouveau guichetbancairebatchserviceso
     */
    @Override
    public void setGuichetbancairebatchserviceso(GuichetBancaireBatchService guichetbancairebatchserviceso) {
        this.guichetbancairebatchserviceso = guichetbancairebatchserviceso;
    }

    /**
     * Modificateur de l'attribut ignorer première ligne.
     *
     * @param ignorerPremiereLigne le nouveau ignorer premiere ligne
     */
    @Override
    @Value("${traitementchargementguichetsetetablissementstabank.ignorerpremiereligne}")
    public void setIgnorerPremiereLigne(boolean ignorerPremiereLigne) {
        this.ignorerPremiereLigne = ignorerPremiereLigne;
    }

    /**
     * Modificateur de l'attribut modetraitement.
     *
     * @param modetraitement le nouveau modetraitement
     */
    @Override
    @Value("${traitementchargementguichetsetetablissementstabank.modetraitement}")
    public void setModetraitement(String modetraitement) {
        this.modetraitement = modetraitement;
    }

    /**
     * Méthode permettant de récupérer le chemin d'un fichier
     *
     * @param fichier fichier sur lequel il faut déterminer le chemin
     * @return le chemin du fichier
     */
    private String getChemin(File fichier) {
        String nomFichier = "chemin non renseigné";
        if (fichier != null) {
            nomFichier = fichier.getAbsolutePath();
        }
        return nomFichier;
    }

    /**
     * Méthode de vérification du format ligne fichier.
     *
     * @param ligneFichierTabank ligne lue
     * @return true s'il y a une erreur, false sinon
     */
    private boolean isErreurFormatLigneFichier(String ligneFichierTabank) {
        LOGGER.debug("Ligne lue : {}", ligneFichierTabank);
        if (ligneFichierTabank.length() != LONGUEUR_ARTICLES_BDFSI) {
            LOGGER.info("Erreur de format de ligne du fichier");
            return true;
        }
        return false;
    }

    /**
     * Accesseur de numLigne
     *
     * @return numLigne
     */
    @Override
    public int getNumLigne() {
        return numLigne;
    }

    @Value("${traitementchargementguichetsetetablissementstabank.versionbatch}")
    @Override()
    public void setVersionBatch(String versionBatch) {
        super.setVersionBatch(versionBatch);
    }

    @Value("${traitementchargementguichetsetetablissementstabank.nombatch}")
    @Override()
    public void setNomBatch(String nomBatch) {
        super.setNomBatch(nomBatch);
    }
}
