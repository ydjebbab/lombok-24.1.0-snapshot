/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 *
 */
package fr.gouv.finances.lombok.bancaire.batch;

import fr.gouv.finances.lombok.bancaire.bean.GuichetBancaire;
import fr.gouv.finances.lombok.bancaire.service.GuichetBancaireBatchService;
import fr.gouv.finances.lombok.batch.ServiceBatchCommun;

/**
 * Classe principale du batch de chargement du fichier des codes guichets et etablissements bancaires
 * 
 * Le mode de traitement "ecrasement" recharge complétement les données à partir du fichier spécifié
 * Tout autre mot clé provoque une mise à jour des données existantes. Si certains guichets bancaires
 * ne sont pas retrouvés dans le fichier traité, ils sont marqués comme estPerime
 * 
 * @author amleplatinec-cp
 *
 */
public interface TraitementChargementGuichetsEtEtablissementsTabank extends ServiceBatchCommun
{

    /**
     * Méthode de construction d'un objet guichet bancaire a partir de ligne fichier.
     * 
     * @param ligneFichierTabank --
     * @return guichet bancaire
     */
    GuichetBancaire construireObjetGuichetBancaireAPartirDeLigneFichier(String ligneFichierTabank);

    /**
     * Modificateur de l attribut fichiertabank.
     * 
     * @param fichiertabank le nouveau fichiertabank
     */
    void setFichiertabank(String fichiertabank);

    /**
     * Modificateur de l attribut guichetbancairebatchserviceso.
     * 
     * @param guichetbancairebatchserviceso le nouveau guichetbancairebatchserviceso
     */
    void setGuichetbancairebatchserviceso(
        GuichetBancaireBatchService guichetbancairebatchserviceso);

    /**
     * Modificateur de l attribut ignorer premiere ligne.
     * 
     * @param ignorerPremiereLigne le nouveau ignorer premiere ligne
     */
    void setIgnorerPremiereLigne(boolean ignorerPremiereLigne);

    /**
     * Modificateur de l attribut modetraitement.
     * 
     * @param modetraitement le nouveau modetraitement
     */
    void setModetraitement(String modetraitement);

    /**
     * 
     * 
     * @see fr.gouv.finances.lombok.batch.ServiceBatchCommunImpl#traiterBatch()
     */
    void traiterBatch();

    /**
     * methode Traiter fichier tabank : --.
     * 
     * @return int
     */
    int traiterFichierTabank();

    /**
     * Accesseur de numLigne
     *
     * @return numLigne
     */
    int getNumLigne();
}
