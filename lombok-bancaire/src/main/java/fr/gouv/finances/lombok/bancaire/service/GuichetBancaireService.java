/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 *
 */
package fr.gouv.finances.lombok.bancaire.service;

import java.util.List;

import fr.gouv.finances.lombok.bancaire.bean.GuichetBancaire;
import fr.gouv.finances.lombok.bancaire.dao.GuichetBancaireDao;

/**
 * Interface des services métiers de gestion des guichets bancaires.
 * 
 * @author amleplatinec-cp
 * @author Christophe Breheret-Girardin
 */
public interface GuichetBancaireService
{

    /**
     * Met à jour un guichet bancaire.
     * 
     * @param guichetBancaire --
     */
    void modifierGuichetBancaire(final GuichetBancaire guichetBancaire);

    /**
     * Recherche le GuichetBancaire non périmé correspondant à un couple codeguichet, codeetablissement.
     * 
     * @param codeGuichet --
     * @param codeEtablissement --
     * @return un {@link GuichetBancaire} ou null si aucun guichet bancaire ne correspond
     */
    GuichetBancaire rechercherGuichetBancaireNonPerimeParCodeGuichetEtCodeEtablissement(String codeGuichet,
        String codeEtablissement);

    /**
     * Recherche les GuichetBancaires correspondant par code commune et code etablissement.
     * 
     * @param commune --
     * @param codeEtablissement --
     * @return liste de {@link GuichetBancaire}
     */
    List<GuichetBancaire> rechercherGuichetsBancaireParCommuneEtCodeEtablissement(String commune, String codeEtablissement);

    /**
     * Recherche les GuichetBancaires correspondant par dénomination et code etablissement.
     * 
     * @param denominationComplete --
     * @param codeEtablissement --
     * @return liste de {@link GuichetBancaire}
     */
    public List<GuichetBancaire> rechercherGuichetsBancaireParDenominationCompleteBanqueEtCodeEtablissement(String denominationComplete,
        String codeEtablissement);

    /**
     * Retourne la liste des guichets bancaires non périmés.
     * 
     * @return liste de {@link GuichetBancaire}
     */
    List<GuichetBancaire> rechercherGuichetsBancairesNonPerimes();

    /**
     * Recherche les GuichetBancaires par code etablissement.
     * 
     * @param codeEtablissement --
     * @return liste de {@link GuichetBancaire}
     */
    List<GuichetBancaire> rechercherGuichetsBancairesParCodeEtablissement(String codeEtablissement);

    /**
     * Recherche les GuichetBancaires correspondant à un couple codeguichet, codeetablissement.
     * 
     * @param codeGuichet --
     * @param codeEtablissement --
     * @return liste de {@link GuichetBancaire}
     */
    List<GuichetBancaire> rechercherGuichetsBancairesParCodeGuichetEtCodeEtablissement(String codeGuichet,
        String codeEtablissement);

    /**
     * Recherche les GuichetBancaires par code postal.
     * 
     * @param codePostal --
     * @return liste de {@link GuichetBancaire}
     */
    List<GuichetBancaire> rechercherGuichetsBancairesParCodePostal(String codePostal);

    /**
     * Recherche les GuichetBancaires par commune.
     * 
     * @param commune --
     * @return liste de {@link GuichetBancaire}
     */
    List<GuichetBancaire> rechercherGuichetsBancairesParCommune(String commune);

    /**
     * Recherche les GuichetBancaires par commune.
     * 
     * @param denominationComplete --
     * @return liste de {@link GuichetBancaire}
     */
    List<GuichetBancaire> rechercherGuichetsBancairesParDenominationCompleteBanque(String denominationComplete);

    /**
     * Retourne la liste des guichets bancaires périmés ou non.
     * 
     * @return liste de {@link GuichetBancaire}
     */
    List<GuichetBancaire> rechercherTousGuichetsBancaires();

    /**
     * Supprimer un guichet bancaire.
     * 
     * @param guichetBancaire --
     */
    void supprimerGuichetBancaire(GuichetBancaire guichetBancaire);

    /**
     * Teste s'il existe un guichet bancaire non périmé associé à un couple code guichet code établissement.
     * 
     * @param codeGuichet --
     * @param codeEtablissement --
     * @return true si le guichet bancaire existe, false s'il n'existe pas
     */
    boolean testerExistenceGuichetBancaireNonPerimeParCodeGuichetEtCodeEtablissement(String codeGuichet,
        String codeEtablissement);

    /**
     * Teste s'il existe un guichet bancaire périmé ou non associé à un couple code guichet code établissement.
     * 
     * @param codeGuichet --
     * @param codeEtablissement --
     * @return true si le guichet bancaire existe, false s'il n'existe pas
     */
    boolean testerExistenceGuichetBancairePerimeOuNonParCodeGuichetEtCodeEtablissement(String codeGuichet,
        String codeEtablissement);

    /**
     * methode Verifier existence guichet bancaire : --.
     * 
     * @param codeGuichet --
     * @param codeEtablissement --
     * @return list
     */
    List<GuichetBancaire> verifierExistenceGuichetBancaire(String codeGuichet, String codeEtablissement);

    /**
     * Modificateur de l'attribut guichetbancairedao.
     * 
     * @param guichetbancairedao guichetbancairedao à initialiser
     */
    void setGuichetbancairedao(GuichetBancaireDao guichetbancairedao);

}
