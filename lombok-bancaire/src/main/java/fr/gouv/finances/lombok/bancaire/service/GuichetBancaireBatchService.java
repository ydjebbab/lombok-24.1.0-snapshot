/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 *
 */
package fr.gouv.finances.lombok.bancaire.service;

import java.util.Date;
import java.util.List;

import org.springframework.transaction.support.TransactionTemplate;

import fr.gouv.finances.lombok.bancaire.bean.GuichetBancaire;
import fr.gouv.finances.lombok.bancaire.dao.GuichetBancaireDao;

/**
 * Interface de service métier pour les objets metiers bancaire interface
 * specifique aux batchs de chargement.
 * 
 * @author amleplatinec-cp
 * @author Christophe Breheret-Girardin
 */
public interface GuichetBancaireBatchService
{
    /**
     * Création des guichets bancaires et stockage en base de données à partir d'une liste de guichets bancaires.
     * 
     * @param lesGuichetsBancaires --
     * @param horodatageDuTraitement --
     * @param modetraitement --
     */
    void creerLesGuichetsBancaires(List<GuichetBancaire> lesGuichetsBancaires, Date horodatageDuTraitement, String modetraitement);
    
    /**
     * Modificateur de l'attribut transactionTemplate.
     * 
     * @param transactionTemplate transactionTemplate à initialiser
     */
    void setTransactionTemplate(TransactionTemplate transactionTemplate);
    
    /**
     * Modificateur de l'attribut guichetbancairedao.
     * 
     * @param guichetbancairedao guichetbancairedao à initialiser
     */
    void setGuichetbancairedao(GuichetBancaireDao guichetbancairedao);
}
