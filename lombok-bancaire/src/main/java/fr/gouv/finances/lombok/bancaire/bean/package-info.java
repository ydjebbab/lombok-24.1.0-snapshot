/*
 * Copyright (c) 2017 DGFiP - Tous droits réservés
 * 
*/
// Projet : lombok-bancaire
/**
 * Documentation du paquet fr.gouv.finances.lombok.bancaire.bean
 * @author chouard
 * @version 1.0
 */
package fr.gouv.finances.lombok.bancaire.bean;