package fr.gouv.finances.lombok.autoconfig;

import org.springframework.boot.context.properties.ConfigurationProperties;

import fr.gouv.finances.lombok.autoconfig.LombokComposantProperties;

/**
 * @author celinio fernandes Date: Feb 20, 2020
 */
@ConfigurationProperties(prefix = "lombok.composant.bancaire")
public class BancaireProperties extends LombokComposantProperties
{

    @Override
    public String toString()
    {
        return "BancaireProperties [inclus=" + inclus + "]";
    }

}
