/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 *
 */
package fr.gouv.finances.lombok.bancaire.bean;

import java.sql.Timestamp;
import java.util.Objects;
import java.util.StringJoiner;

import org.springframework.util.StringUtils;

import fr.gouv.finances.lombok.util.base.BaseBean;
import fr.gouv.finances.lombok.util.exception.RegleGestionException;

/**
 * Description des guichets bancaires à la norme Banque de France.
 * 
 * @author chouard-cp
 * @author Christophe Breheret-Girardin
 */
public class GuichetBancaire extends BaseBean
{
    /** Initialisation  de l'UID. */
    private static final long serialVersionUID = -8943664471340761056L;

    /** Identifiant. */
    private Long id;

    /** Version. */
    private Integer version;

    /** Code BDF de l'etablissement bancaire. */
    private String codeEtablissement;

    /** Code Guichet de l'etablissement bancaire. */
    private String codeGuichet;

    /** Nom guichet. */
    private String nomGuichet;

    /** Adresse guichet. */
    private String adresseGuichet;

    /** Code postal. */
    private String codePostal;

    /** Ville code postal. */
    private String villeCodePostal;

    /** Dénomination complète. */
    private String denominationComplete;

    /** Déomination abregée. */
    private String denominationAbregee;

    /** Libellé abréviation de domiciliation. */
    private String libelleAbrevDeDomiciliation;

    /** Code réglement des credits. */
    private String codeReglementDesCredits;

    /** Mois de modification. */
    private String moisDeModification;

    /** Année de modification. */
    private String anneeDeModification;

    /** Code enregistrement. */
    private String codeEnregistrement;

    /** Nouveau code etablissement. */
    private String nouveauCodeEtablissement;

    /** Nouveau code guichet. */
    private String nouveauCodeGuichet;

    /**
     * Cet attribut est vrai si un nouveauCodeEtablissement et un nouveauCodeGuichet sont trouvés,
     * ce qui signifie que le guichet et l'etablissement actuel vont bientot disparaitre.
     */
    private boolean estEnVoieDePeremption;

    /**
     * Cet attribut est utilise uniquement en mode sans ecrasement.<br/>
     * Il traduit le fait qu'un guichet n'est plus présent dans la liste transmise par la Banque de France.
     */
    private boolean estPerime;

    /** Date de dernière mise a jour. */
    private Timestamp dateDerniereMiseAJour;

    /**
     * Constructeur.
     */
    public GuichetBancaire()
    {
        super();
    }

    /**
     * Constructeur.
     *
     * @param codeEtablissement codeEtablissement
     * @param codeGuichet code guichet
     * @param nomGuichet nom guichet
     * @param adresseGuichet adresse guichet
     * @param codePostal code postal
     * @param villeCodePostal ville code postal
     * @param denominationComplete dénomination complète
     * @param denominationAbregee dénomination abregée
     * @param libelleAbrevDeDomiciliation libellé abréviation de domiciliation
     * @param codeReglementDesCredits code réglement des crédits
     * @param moisDeModification mois de modification
     * @param anneeDeModification année de modification
     * @param codeEnregistrement code enregistrement
     * @param nouveauCodeEtablissement nouveau code établissement
     * @param nouveauCodeGuichet nouveau code guichet
     * @param estEnVoieDePeremption est en voie de péremption
     * @param estPerime est périmé
     * @param dateDerniereMiseAJour date dernière mise à jour
     */
    public GuichetBancaire(String codeEtablissement, String codeGuichet, String nomGuichet, String adresseGuichet,
        String codePostal, String villeCodePostal, String denominationComplete, String denominationAbregee,
        String libelleAbrevDeDomiciliation, String codeReglementDesCredits, String moisDeModification,
        String anneeDeModification, String codeEnregistrement, String nouveauCodeEtablissement,
        String nouveauCodeGuichet, boolean estEnVoieDePeremption, boolean estPerime, Timestamp dateDerniereMiseAJour)
    {
        super();
        this.codeEtablissement = codeEtablissement;
        this.codeGuichet = codeGuichet;
        this.nomGuichet = nomGuichet;
        this.adresseGuichet = adresseGuichet;
        this.codePostal = codePostal;
        this.villeCodePostal = villeCodePostal;
        this.denominationComplete = denominationComplete;
        this.denominationAbregee = denominationAbregee;
        this.libelleAbrevDeDomiciliation = libelleAbrevDeDomiciliation;
        this.codeReglementDesCredits = codeReglementDesCredits;
        this.moisDeModification = moisDeModification;
        this.anneeDeModification = anneeDeModification;
        this.codeEnregistrement = codeEnregistrement;
        this.nouveauCodeEtablissement = nouveauCodeEtablissement;
        this.nouveauCodeGuichet = nouveauCodeGuichet;
        this.estEnVoieDePeremption = estEnVoieDePeremption;
        this.estPerime = estPerime;
        this.dateDerniereMiseAJour = dateDerniereMiseAJour;
    }

   
    
    /**
     * Méthode permettant de calculer si le guichet bancaire est en voie de péremption.
     */
    public void calculerAttributEstEnVoieDePeremption()
    {
        if (nouveauCodeEtablissement != null && nouveauCodeGuichet != null
            && !StringUtils.isEmpty(nouveauCodeEtablissement.trim())
            && !StringUtils.isEmpty(nouveauCodeGuichet))
        {
            this.setEstEnVoieDePeremption(true);
        }
    }

    /**
     * Le code etablissement doit être présent.
     */
    public void verifierCodeEtablissement()
    {
        verifierNonVide(this.getCodeEtablissement(), "Code etablissement absent");
    }

    /**
     * Le code guichet doit être présent.
     */
    public void verifierCodeGuichet()
    {
        verifierNonVide(this.getCodeGuichet(), "Code guichet absent");
    }

    /**
     * Le libellé abréviatif de domiciliation doit être présent.
     */
    public void verifierLibelleAbrevDeDomiciliation()
    {
        verifierNonVide(this.getLibelleAbrevDeDomiciliation(), "Libellé abréviatif de domiciliation absent");
    }

    /**
     * Accesseur de l'attribut adresse guichet.
     * 
     * @return adresse guichet
     */
    public String getAdresseGuichet()
    {
        return this.adresseGuichet;
    }

    /**
     * Accesseur de l'attribut annee de modification.
     * 
     * @return annee de modification
     */
    public String getAnneeDeModification()
    {
        return this.anneeDeModification;
    }

    /**
     * Accesseur de l'attribut code enregistrement.
     * 
     * @return code enregistrement
     */
    public String getCodeEnregistrement()
    {
        return this.codeEnregistrement;
    }

    /**
     * Accesseur de l'attribut code etablissement.
     * 
     * @return code etablissement
     */
    public String getCodeEtablissement()
    {
        return this.codeEtablissement;
    }

    /**
     * Accesseur de l'attribut code guichet.
     * 
     * @return code guichet
     */
    public String getCodeGuichet()
    {
        return this.codeGuichet;
    }

    /**
     * Accesseur de l'attribut code postal.
     * 
     * @return code postal
     */
    public String getCodePostal()
    {
        return this.codePostal;
    }

    /**
     * Accesseur de l'attribut code reglement des credits.
     * 
     * @return code reglement des credits
     */
    public String getCodeReglementDesCredits()
    {
        return this.codeReglementDesCredits;
    }

    /**
     * Accesseur de l'attribut date derniere mise a jour.
     * 
     * @return date derniere mise a jour
     */
    public Timestamp getDateDerniereMiseAJour()
    {
        return this.dateDerniereMiseAJour;
    }

    /**
     * Accesseur de l'attribut denomination abregee.
     * 
     * @return denomination abregee
     */
    public String getDenominationAbregee()
    {
        return this.denominationAbregee;
    }

    /**
     * Accesseur de l'attribut denomination complete.
     * 
     * @return denomination complete
     */
    public String getDenominationComplete()
    {
        return this.denominationComplete;
    }

    // Property accessors
    /**
     * Accesseur de l'attribut id.
     * 
     * @return id
     */
    public Long getId()
    {
        return this.id;
    }

    /**
     * Accesseur de l'attribut libelle abrev de domiciliation.
     * 
     * @return libelle abrev de domiciliation
     */
    public String getLibelleAbrevDeDomiciliation()
    {
        return this.libelleAbrevDeDomiciliation;
    }

    /**
     * Accesseur de l'attribut mois de modification.
     * 
     * @return mois de modification
     */
    public String getMoisDeModification()
    {
        return this.moisDeModification;
    }

    /**
     * Accesseur de l'attribut nom guichet.
     * 
     * @return nom guichet
     */
    public String getNomGuichet()
    {
        return this.nomGuichet;
    }

    /**
     * Accesseur de l'attribut nouveau code etablissement.
     * 
     * @return nouveau code etablissement
     */
    public String getNouveauCodeEtablissement()
    {
        return this.nouveauCodeEtablissement;
    }

    /**
     * Accesseur de l'attribut nouveau code guichet.
     * 
     * @return nouveau code guichet
     */
    public String getNouveauCodeGuichet()
    {
        return this.nouveauCodeGuichet;
    }

    /**
     * Accesseur de l'attribut version.
     * 
     * @return version
     */
    public Integer getVersion()
    {
        return this.version;
    }

    /**
     * Accesseur de l'attribut ville code postal.
     * 
     * @return ville code postal
     */
    public String getVilleCodePostal()
    {
        return this.villeCodePostal;
    }

    /**
     * Verifie si est en voie de peremption.
     * 
     * @return true, si c'est est en voie de peremption
     */
    public boolean isEstEnVoieDePeremption()
    {
        return this.estEnVoieDePeremption;
    }

    /**
     * Verifie si est perime.
     * 
     * @return true, si c'est est perime
     */
    public boolean isEstPerime()
    {
        return this.estPerime;
    }

    /**
     * Modificateur de l'attribut adresse guichet.
     * 
     * @param adresseGuichet le nouveau adresse guichet
     */
    public void setAdresseGuichet(String adresseGuichet)
    {
        this.adresseGuichet = adresseGuichet;
    }

    /**
     * Modificateur de l'attribut annee de modification.
     * 
     * @param anneeDeModification le nouveau annee de modification
     */
    public void setAnneeDeModification(String anneeDeModification)
    {
        this.anneeDeModification = anneeDeModification;
    }

    /**
     * Modificateur de l'attribut code enregistrement.
     * 
     * @param codeEnregistrement le nouveau code enregistrement
     */
    public void setCodeEnregistrement(String codeEnregistrement)
    {
        this.codeEnregistrement = codeEnregistrement;
    }

    /**
     * Modificateur de l'attribut code etablissement.
     * 
     * @param codeEtablissement le nouveau code etablissement
     */
    public void setCodeEtablissement(String codeEtablissement)
    {
        this.codeEtablissement = codeEtablissement;
    }

    /**
     * Modificateur de l'attribut code guichet.
     * 
     * @param codeGuichet le nouveau code guichet
     */
    public void setCodeGuichet(String codeGuichet)
    {
        this.codeGuichet = codeGuichet;
    }

    /**
     * Modificateur de l'attribut code postal.
     * 
     * @param codePostal le nouveau code postal
     */
    public void setCodePostal(String codePostal)
    {
        this.codePostal = codePostal;
    }

    /**
     * Modificateur de l'attribut code reglement des credits.
     * 
     * @param codeReglementDesCredits le nouveau code reglement des credits
     */
    public void setCodeReglementDesCredits(String codeReglementDesCredits)
    {
        this.codeReglementDesCredits = codeReglementDesCredits;
    }

    /**
     * Modificateur de l'attribut date derniere mise a jour.
     * 
     * @param dateDerniereMiseAJour le nouveau date derniere mise a jour
     */
    public void setDateDerniereMiseAJour(Timestamp dateDerniereMiseAJour)
    {
        this.dateDerniereMiseAJour = dateDerniereMiseAJour;
    }

    /**
     * Modificateur de l'attribut denomination abregee.
     * 
     * @param denominationAbregee le nouveau denomination abregee
     */
    public void setDenominationAbregee(String denominationAbregee)
    {
        this.denominationAbregee = denominationAbregee;
    }

    /**
     * Modificateur de l'attribut denomination complete.
     * 
     * @param denominationComplete le nouveau denomination complete
     */
    public void setDenominationComplete(String denominationComplete)
    {
        this.denominationComplete = denominationComplete;
    }

    /**
     * Modificateur de l'attribut est en voie de peremption.
     * 
     * @param estEnVoieDePeremption le nouveau est en voie de peremption
     */
    public void setEstEnVoieDePeremption(boolean estEnVoieDePeremption)
    {
        this.estEnVoieDePeremption = estEnVoieDePeremption;
    }

    /**
     * Modificateur de l'attribut est perime.
     * 
     * @param estPerime le nouveau est perime
     */
    public void setEstPerime(boolean estPerime)
    {
        this.estPerime = estPerime;
    }

    /**
     * Modificateur de l'attribut id.
     * 
     * @param id le nouveau id
     */
    public void setId(Long id)
    {
        this.id = id;
    }

    /**
     * Modificateur de l'attribut libelle abrev de domiciliation.
     * 
     * @param libelleAbrevDeDomiciliation le nouveau libelle abrev de domiciliation
     */
    public void setLibelleAbrevDeDomiciliation(String libelleAbrevDeDomiciliation)
    {
        this.libelleAbrevDeDomiciliation = libelleAbrevDeDomiciliation;
    }

    /**
     * Modificateur de l'attribut mois de modification.
     * 
     * @param moisDeModification le nouveau mois de modification
     */
    public void setMoisDeModification(String moisDeModification)
    {
        this.moisDeModification = moisDeModification;
    }

    /**
     * Modificateur de l'attribut nom guichet.
     * 
     * @param nomGuichet le nouveau nom guichet
     */
    public void setNomGuichet(String nomGuichet)
    {
        this.nomGuichet = nomGuichet;
    }

    /**
     * Modificateur de l'attribut nouveau code etablissement.
     * 
     * @param nouveauCodeEtablissement le nouveau nouveau code etablissement
     */
    public void setNouveauCodeEtablissement(String nouveauCodeEtablissement)
    {
        this.nouveauCodeEtablissement = nouveauCodeEtablissement;
    }

    /**
     * Modificateur de l'attribut nouveau code guichet.
     * 
     * @param nouveauCodeGuichet le nouveau nouveau code guichet
     */
    public void setNouveauCodeGuichet(String nouveauCodeGuichet)
    {
        this.nouveauCodeGuichet = nouveauCodeGuichet;
    }

    /**
     * Modificateur de l'attribut version.
     * 
     * @param version le nouveau version
     */
    public void setVersion(Integer version)
    {
        this.version = version;
    }

    /**
     * Modificateur de l'attribut ville code postal.
     * 
     * @param villeCodePostal le nouveau ville code postal
     */
    public void setVilleCodePostal(String villeCodePostal)
    {
        this.villeCodePostal = villeCodePostal;
    }

    /**
     * Méthode permettant de vérifier qu'une chaine de caractère n'est pas vide,
     * si c'est le cas lève une RegleGestionException.
     *
     * @param chaine chaine de caractères à tester
     * @param message message à apporter à l'exception si la chaine est vide
     */
    private void verifierNonVide(String chaine, String message)
    {
        if (chaine == null || chaine.trim().isEmpty())
        {
            throw new RegleGestionException(message);

        }
    }

    /** 
     * {@inheritDoc}
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        return new StringJoiner("|").add(codeGuichet).add(codeEtablissement).toString();
    }
    
    /** 
     * {@inheritDoc}
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode()
    {
        return Objects.hash(codeEtablissement, codeGuichet, estPerime);
    }

    /** 
     * {@inheritDoc}
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object object)
    {
        if (object == null)
        {
            return false;
        }

        if (this.getClass() != object.getClass())
        {
            return false;
        }

        GuichetBancaire guichetBancaire = (GuichetBancaire) object;
        return Objects.equals(codeEtablissement, guichetBancaire.codeEtablissement)
            && Objects.equals(codeGuichet, guichetBancaire.codeGuichet)
            && Objects.equals(estPerime, guichetBancaire.estPerime);
    }
}