/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 */
package fr.gouv.finances.lombok.bancaire.dao;

import java.util.Date;
import java.util.List;

import fr.gouv.finances.lombok.bancaire.bean.GuichetBancaire;
import fr.gouv.finances.lombok.util.base.CoreBaseDao;

/**
 * Interface du DAO permettant de gérer les données des guichets bancaires.<br/>
 * Utiliser de préference la classe de service métier.
 * 
 * @see fr.gouv.finances.lombok.bancaire.service.GuichetBancaireService
 * @author amleplatinec-cp
 * @author Christophe Breheret-Girardin
 */
public interface GuichetBancaireDao extends CoreBaseDao
{
    /**
     * Supprimer tous les guichets bancaires.
     */
    public void deleteTousLesGuichetsBancaires();

    /**
     * Rechercher un GuichetBancaire non périmé correspondant par code guichet et code établissement.
     * 
     * @param codeGuichet --
     * @param codeEtablissement --
     * @return un {@link GuichetBancaire} ou null si aucun guichet bancaire ne correpoond
     */
    public GuichetBancaire findGuichetBancaireNonPerimeParCodeGuichetEtCodeEtablissement(String codeGuichet,
        String codeEtablissement);

    /**
     * Rechercher les GuichetBancaires correspondant par code commune et code etablissement.
     * 
     * @param commune --
     * @param codeEtablissement --
     * @return liste de {@link GuichetBancaire}
     */
    public List<GuichetBancaire> findGuichetsBancaireParCommuneEtCodeEtablissement(String commune, String codeEtablissement);

    /**
     * Rechercher les GuichetBancaires correspondant par dénomination et code etablissement.
     * 
     * @param denominationComplete --
     * @param codeEtablissement --
     * @return liste de {@link GuichetBancaire}
     */
    public List<GuichetBancaire> findGuichetsBancaireParDenominationCompleteBanqueEtCodeEtablissement(String denominationComplete,
        String codeEtablissement);

    /**
     * Retourner la liste des guichets bancaires non périmés.
     * 
     * @return liste de {@link GuichetBancaire}
     */
    public List<GuichetBancaire> findGuichetsBancairesNonPerimes();

    /**
     * Recherche les GuichetBancaires par code etablissement.
     * 
     * @param codeEtablissement --
     * @return liste de {@link GuichetBancaire}
     */
    public List<GuichetBancaire> findGuichetsBancairesParCodeEtablissement(String codeEtablissement);

    /**
     * Rechercher les GuichetBancaires correspondant à un couple codeguichet, codeetablissement.
     * 
     * @param codeGuichet --
     * @param codeEtablissement --
     * @return liste de {@link GuichetBancaire}
     */
    public List<GuichetBancaire> findGuichetsBancairesParCodeGuichetEtCodeEtablissement(String codeGuichet, String codeEtablissement);

    /**
     * Rechercher les GuichetBancaires par code postal.
     * 
     * @param codePostal --
     * @return liste de {@link GuichetBancaire}
     */
    public List<GuichetBancaire> findGuichetsBancairesParCodePostal(String codePostal);

    /**
     * Recherche les GuichetBancaires par commune.
     * 
     * @param commune --
     * @return liste de {@link GuichetBancaire}
     */
    public List<GuichetBancaire> findGuichetsBancairesParCommune(String commune);

    /**
     * Rechercher les GuichetBancaires par commune.
     * 
     * @param denominationComplete --
     * @return liste de {@link GuichetBancaire}
     */
    public List<GuichetBancaire> findGuichetsBancairesParDenominationCompleteBanque(String denominationComplete);

    /**
     * Retourner la liste des guichets bancaires périmés ou non.
     * 
     * @return liste de {@link GuichetBancaire}
     */
    public List<GuichetBancaire> findTousGuichetsBancaires();

    /**
     * Marquer périmés les guichets bancaires à une date donnée. Les guichets bancaires qui ne sont plus dans le fichier
     * chargé par le batch
     * 
     * @param date --
     */
    public void marqueLesGuichetsBancairesPerimesADate(Date date);

    /**
     * Tester s'il existe un guichet bancaire non périmé associé à un couple code guichet code établissement.
     * 
     * @param codeGuichet --
     * @param codeEtablissement --
     * @return true si le guichet bancaire existe, false s'il n'existe pas
     */
    public boolean testeExistenceGuichetBancaireNonPerimeParCodeGuichetEtCodeEtablissement(String codeGuichet,
        String codeEtablissement);

    /**
     * Tester s'il existe un guichet bancaire périmé ou non associé à un couple code guichet code établissement.
     * 
     * @param codeGuichet --
     * @param codeEtablissement --
     * @return true si le guichet bancaire existe, false s'il n'existe pas
     */
    public boolean testeExistenceGuichetBancairePerimeOuNonParCodeGuichetEtCodeEtablissement(String codeGuichet,
        String codeEtablissement);

}
