/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 */
package fr.gouv.finances.lombok.bancaire.dao;

/**
 * Propriétés liées à la donnée guichet bancaire
 * @see fr.gouv.finances.lombok.bancaire.bean.GuichetBancaire
 *
 * @author Christophe Breheret-Girardin
 */
public enum ProprieteGuichetBancaire
{
    /** Propriété code de l'établissement associé au guichet */
    CODE_ETABLISSEMENT("codeEtablissement"),

    /** Propriété code du guichet */
    CODE_GUICHET("codeGuichet"),

    /** Propriété sur la péremption du guichet */
    EST_PERIME("estPerime"),

    /** Propriété commune du guichet */
    COMMUNE("villeCodePostal"),

    /** Propriété sur la dénomination complète du guichet */
    DENOMINATION_COMPLETE("denominationComplete"),

    /** Propriété sur le code postal associé au guichet */
    CODE_POSTAL("codePostal");
    
    /** Nom de la propriété */
    private String nom;

    /**
     * Constructeur.
     *
     * @param libelleNom libellé du nom de la propriété
     */
    private ProprieteGuichetBancaire(String libelleNom)
    {
        nom = libelleNom;
    }

    /**
     * Accesseur de nom
     *
     * @return nom de la propriété
     */
    public String getNom()
    {
        return nom;
    }
    
}
