/*
 * Copyright (c) 2016 DGFiP - Tous droits réservés
 * 
 */
package fr.gouv.finances.lombok.bancaire.service.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.gouv.finances.lombok.bancaire.bean.GuichetBancaire;
import fr.gouv.finances.lombok.bancaire.dao.GuichetBancaireDao;
import fr.gouv.finances.lombok.bancaire.service.GuichetBancaireBatchService;
import fr.gouv.finances.lombok.bancaire.service.GuichetBancaireService;
import fr.gouv.finances.lombok.test.controle.ControleDonnees;
import fr.gouv.finances.lombok.util.exception.RegleGestionException;

/**
 * Tests unitaires automatisés des services métier.
 *
 * @author chouard
 * @author Christophe Breheret-Girardin
 */
@ContextConfiguration({"classpath:/conf/applicationContext-bancaire-service-test.xml"
    , "classpath*:/conf/applicationContext-core-transaction-test.xml"
    , "classpath*:/conf/applicationContext-commun-transaction.xml"})
@ActiveProfiles(profiles = {"bancaire", "hibernate"})
@RunWith(SpringJUnit4ClassRunner.class)
public class GuichetBancaireServiceImplTest extends GuichetBancaireServiceTest
{
    /** Initialisation de la journalisation. */
    protected static final Logger LOGGER = LoggerFactory.getLogger(GuichetBancaireServiceImplTest.class);

    /** Service métier de gestion du guichet bancaire à tester */
    @Autowired
    private GuichetBancaireService guichetBancaireService;

    /** Service métier de gestion du guichet bancaire à tester */
    @Autowired
    private GuichetBancaireBatchService guichetBancaireBatchService;

    /** Bouchon du DAO de gestion des données du guichet bancaire */
    @Mock
    private GuichetBancaireDao guichetbancairedao;

    /** Valeur inconnue */
    private static final String VALEUR_INCONNUE = "kamoulox";

    /**
     * Constructeur.
     */
    public GuichetBancaireServiceImplTest()
    {
        super();
    }

    /**
     * Initialisation des tests.
     */
    @Before
    public void setupMock()
    {
        MockitoAnnotations.initMocks(this);
        guichetBancaireService.setGuichetbancairedao(guichetbancairedao);
    }

    /**
     * Test de l'injection du service.
     */
    @Test
    public final void testGuichetBancaireService()
    {
        assertNotNull(guichetBancaireService);
    }

    /**
     * Test de la méthode
     * {@link fr.gouv.finances.lombok.bancaire.service.impl.GuichetBancaireServiceImpl#rechercherTousGuichetsBancaires()}.
     */
    @Test
    public final void testRechercherTousGuichetsBancaires()
    {
        // Vérification de l'absence des données avant le test
        assertTrue("Liste de guichet bancaire non vide"
            , guichetBancaireService.rechercherTousGuichetsBancaires().isEmpty());

        // Paramétrage du comportement du bouchon
        List<GuichetBancaire> listeAttendue = getGuichetsBancaires();
        when(guichetbancairedao.findTousGuichetsBancaires())
            .thenReturn(listeAttendue);

        // Exécution du service et vérification par rapport au comportement défini
        ControleDonnees.verifierElements(guichetBancaireService
            .rechercherTousGuichetsBancaires(), listeAttendue);
    }

    /**
     * Test de la méthode
     * {@link fr.gouv.finances.lombok.bancaire.service.impl.GuichetBancaireServiceImpl#modifierGuichetBancaire(fr.gouv.finances.lombok.bancaire.bean.GuichetBancaire)}.
     */
    @Test
    public final void testModifierGuichetBancaire()
    {
        guichetBancaireService.modifierGuichetBancaire(getGuichetBancaire(1));
    }

    /**
     * Test de la méthode
     * {@link fr.gouv.finances.lombok.bancaire.service.impl.GuichetBancaireServiceImpl#rechercherGuichetBancaireNonPerimeParCodeGuichetEtCodeEtablissement(java.lang.String, java.lang.String)}.
     */
    @Test
    public final void testRechercherGuichetBancaireNonPerimeParCodeGuichetEtCodeEtablissement()
    {
        // Vérification de l'absence des données avant le test
        assertNull("Guichet bancaire non vide"
            , guichetBancaireService
                .rechercherGuichetBancaireNonPerimeParCodeGuichetEtCodeEtablissement(
                    "1", CODE_ETABLISSEMENT));

        // Paramétrage du comportement du bouchon
        GuichetBancaire guichetBancaire = getGuichetBancaire(1);
        when(guichetbancairedao.findGuichetBancaireNonPerimeParCodeGuichetEtCodeEtablissement(
            "1", CODE_ETABLISSEMENT)).thenReturn(getGuichetBancaire(1));

        // Exécution du service
        GuichetBancaire guichetBancaireResultat = guichetBancaireService
                .rechercherGuichetBancaireNonPerimeParCodeGuichetEtCodeEtablissement(
                    "1", CODE_ETABLISSEMENT);

        // Vérification du service par rapport au comportement défini
        assertNull("Liste de guichet bancaire non vide"
            , guichetBancaireService.rechercherGuichetBancaireNonPerimeParCodeGuichetEtCodeEtablissement(
                "2", CODE_ETABLISSEMENT));
        assertNotNull("Guichet bancaire vide", guichetBancaireResultat);
        assertEquals("Ce n'est pas le guichet bancaire attendu"
            , guichetBancaire, guichetBancaireResultat);
    }

    /**
     * Test de la méthode
     * {@link fr.gouv.finances.lombok.bancaire.service.impl.GuichetBancaireServiceImpl#rechercherGuichetsBancaireParCommuneEtCodeEtablissement(java.lang.String, java.lang.String)}.
     */
    @Test
    public final void testRechercherGuichetsBancaireParCommuneEtCodeEtablissement()
    {
        // Vérification de l'absence des données avant le test
        assertTrue("Liste de guichet bancaire non vide"
            , guichetBancaireService.rechercherGuichetsBancaireParCommuneEtCodeEtablissement(
                VILLE, CODE_ETABLISSEMENT).isEmpty());

        // Paramétrage du comportement du bouchon
        List<GuichetBancaire> listeAttendue = getGuichetsBancaires();
        when(guichetbancairedao.findGuichetsBancaireParCommuneEtCodeEtablissement(
            VILLE, CODE_ETABLISSEMENT)).thenReturn(listeAttendue);

        // Exécutions du service
        ControleDonnees.verifierElements(guichetBancaireService
            .rechercherGuichetsBancaireParCommuneEtCodeEtablissement(VILLE, CODE_ETABLISSEMENT), listeAttendue);

        // Vérification du service par rapport au comportement défini
        assertTrue("Liste de guichet bancaire non vide"
            , guichetBancaireService.rechercherGuichetsBancaireParCommuneEtCodeEtablissement(
                VILLE, VALEUR_INCONNUE).isEmpty());
    }

    /**
     * Test de la méthode
     * {@link fr.gouv.finances.lombok.bancaire.service.impl.GuichetBancaireServiceImpl#rechercherGuichetsBancaireParDenominationCompleteBanqueEtCodeEtablissement(java.lang.String, java.lang.String)}.
     */
    @Test
    public final void testRechercherGuichetsBancaireParDenominationCompleteBanqueEtCodeEtablissement()
    {
        // Vérification de l'absence des données avant le test
        assertTrue("Liste de guichet bancaire non vide"
            , guichetBancaireService.rechercherGuichetsBancaireParDenominationCompleteBanqueEtCodeEtablissement(
                VILLE, CODE_ETABLISSEMENT).isEmpty());

        // Paramétrage du comportement du bouchon
        List<GuichetBancaire> listeAttendue = getGuichetsBancaires();
        when(guichetbancairedao.findGuichetsBancaireParDenominationCompleteBanqueEtCodeEtablissement(
            VILLE, CODE_ETABLISSEMENT)).thenReturn(listeAttendue);

        // Exécution du service
        ControleDonnees.verifierElements(
            guichetBancaireService
                .rechercherGuichetsBancaireParDenominationCompleteBanqueEtCodeEtablissement(
                    VILLE, CODE_ETABLISSEMENT), listeAttendue);

        // Vérification du service par rapport au comportement défini
        assertTrue("Liste de guichet bancaire non vide"
            , guichetBancaireService.rechercherGuichetsBancaireParDenominationCompleteBanqueEtCodeEtablissement(
                VILLE, VALEUR_INCONNUE).isEmpty());
        
    }

    /**
     * Test de la méthode
     * {@link fr.gouv.finances.lombok.bancaire.service.impl.GuichetBancaireServiceImpl#rechercherGuichetsBancairesNonPerimes()}.
     */
    @Test
    public final void testRechercherGuichetsBancairesNonPerimes()
    {
        // Vérification de l'absence des données avant le test
        assertTrue("Liste de guichet bancaire non vide"
            , guichetBancaireService.rechercherGuichetsBancairesNonPerimes().isEmpty());

        // Paramétrage du comportement du bouchon
        List<GuichetBancaire> listeAttendue = getGuichetsBancaires();
        when(guichetbancairedao.findGuichetsBancairesNonPerimes()).thenReturn(listeAttendue);

        // Exécution du service et vérification par rapport au comportement défini
        ControleDonnees.verifierElements(guichetBancaireService
            .rechercherGuichetsBancairesNonPerimes(), listeAttendue);
    }

    /**
     * Test de la méthode
     * {@link fr.gouv.finances.lombok.bancaire.service.impl.GuichetBancaireServiceImpl#rechercherGuichetsBancairesParCodeEtablissement(java.lang.String)}.
     */
    @Test
    public final void testRechercherGuichetsBancairesParCodeEtablissement()
    {
        // Vérification de l'absence des données avant le test
        assertTrue("Liste de guichet bancaire non vide"
            , guichetBancaireService.rechercherGuichetsBancairesParCodeEtablissement(CODE_ETABLISSEMENT)
                .isEmpty());

        // Paramétrage du comportement du bouchon
        List<GuichetBancaire> listeAttendue = getGuichetsBancaires();
        when(guichetbancairedao.findGuichetsBancairesParCodeEtablissement(CODE_ETABLISSEMENT))
            .thenReturn(listeAttendue);

        // Exécution du service
        ControleDonnees.verifierElements(guichetBancaireService
            .rechercherGuichetsBancairesParCodeEtablissement(CODE_ETABLISSEMENT), listeAttendue);

        // Vérification du service par rapport au comportement défini
        assertTrue("Liste de guichet bancaire non vide"
            , guichetBancaireService.rechercherGuichetsBancairesParCodeEtablissement(
                VALEUR_INCONNUE).isEmpty());
    }

    /**
     * Test de la méthode
     * {@link fr.gouv.finances.lombok.bancaire.service.impl.GuichetBancaireServiceImpl#rechercherGuichetsBancairesParCodeGuichetEtCodeEtablissement(java.lang.String, java.lang.String)}.
     */
    @Test
    public final void testRechercherGuichetsBancairesParCodeGuichetEtCodeEtablissement()
    {
        // Vérification de l'absence des données avant le test
        assertTrue("Liste de guichet bancaire non vide"
            , guichetBancaireService.rechercherGuichetsBancairesParCodeGuichetEtCodeEtablissement(
                "1", CODE_ETABLISSEMENT).isEmpty());

        // Paramétrage du comportement du bouchon
        List<GuichetBancaire> listeAttendue = getGuichetsBancaires();
        when(guichetbancairedao.findGuichetsBancairesParCodeGuichetEtCodeEtablissement(
            "1", CODE_ETABLISSEMENT)).thenReturn(listeAttendue);

        // Exécution du service
        ControleDonnees.verifierElements(guichetBancaireService
            .rechercherGuichetsBancairesParCodeGuichetEtCodeEtablissement("1", CODE_ETABLISSEMENT)
            , listeAttendue);

        // Vérification du service par rapport au comportement défini
        assertTrue("Liste de guichet bancaire non vide"
            , guichetBancaireService.rechercherGuichetsBancairesParCodeGuichetEtCodeEtablissement(
                "2", CODE_ETABLISSEMENT).isEmpty());
    }

    /**
     * Test de la méthode
     * {@link fr.gouv.finances.lombok.bancaire.service.impl.GuichetBancaireServiceImpl#rechercherGuichetsBancairesParCodePostal(java.lang.String)}.
     */
    @Test
    public final void testRechercherGuichetsBancairesParCodePostal()
    {
        // Vérification de l'absence des données avant le test
        assertTrue("Liste de guichet bancaire non vide"
            , guichetBancaireService.rechercherGuichetsBancairesParCodePostal(VALEUR_INCONNUE).isEmpty());

        // Paramétrage du comportement du bouchon
        List<GuichetBancaire> listeAttendue = getGuichetsBancaires();
        when(guichetbancairedao.findGuichetsBancairesParCodePostal(VALEUR_INCONNUE)).thenReturn(listeAttendue);

        // Exécution du service
        ControleDonnees.verifierElements(guichetBancaireService
            .rechercherGuichetsBancairesParCodePostal(VALEUR_INCONNUE), listeAttendue);

        // Vérification du service par rapport au comportement défini
        assertTrue("Liste de guichet bancaire non vide"
            , guichetBancaireService.rechercherGuichetsBancairesParCodePostal(CODE_ETABLISSEMENT).isEmpty());
    }

    /**
     * Test de la méthode
     * {@link fr.gouv.finances.lombok.bancaire.service.impl.GuichetBancaireServiceImpl#rechercherGuichetsBancairesParCommune(java.lang.String)}.
     */
    @Test
    public final void testRechercherGuichetsBancairesParCommune()
    {
        // Vérification de l'absence des données avant le test
        assertTrue("Liste de guichet bancaire non vide"
            , guichetBancaireService.rechercherGuichetsBancairesParCommune(
                VILLE).isEmpty());

        // Paramétrage du comportement du bouchon
        List<GuichetBancaire> listeAttendue = getGuichetsBancaires();
        when(guichetbancairedao.findGuichetsBancairesParCommune(VILLE)).thenReturn(listeAttendue);

        // Exécution du service
        ControleDonnees.verifierElements(guichetBancaireService
            .rechercherGuichetsBancairesParCommune(VILLE), listeAttendue);

        // Vérification du service par rapport au comportement défini
        assertTrue("Liste de guichet bancaire non vide"
            , guichetBancaireService.rechercherGuichetsBancairesParCommune(VALEUR_INCONNUE).isEmpty());
    }

    /**
     * Test de la méthode
     * {@link fr.gouv.finances.lombok.bancaire.service.impl.GuichetBancaireServiceImpl#rechercherGuichetsBancairesParDenominationCompleteBanque(java.lang.String)}.
     */
    @Test
    public final void testRechercherGuichetsBancairesParDenominationCompleteBanque()
    {
        // Vérification de l'absence des données avant le test
        assertTrue("Liste de guichet bancaire non vide"
            , guichetBancaireService.rechercherGuichetsBancairesParDenominationCompleteBanque(
                DENOMINATION_COMPLETE).isEmpty());

        // Paramétrage du comportement du bouchon
        List<GuichetBancaire> listeAttendue = getGuichetsBancaires();
        when(guichetbancairedao.findGuichetsBancairesParDenominationCompleteBanque(DENOMINATION_COMPLETE))
            .thenReturn(listeAttendue);

        // Exécution du service
        ControleDonnees.verifierElements(guichetBancaireService
            .rechercherGuichetsBancairesParDenominationCompleteBanque(DENOMINATION_COMPLETE), listeAttendue);

        // Vérification du service par rapport au comportement défini
        assertTrue("Liste de guichet bancaire non vide", guichetBancaireService
            .rechercherGuichetsBancairesParDenominationCompleteBanque(VALEUR_INCONNUE).isEmpty());
        
    }

    /**
     * Test de la méthode
     * {@link fr.gouv.finances.lombok.bancaire.service.impl.GuichetBancaireServiceImpl#supprimerGuichetBancaire(fr.gouv.finances.lombok.bancaire.bean.GuichetBancaire)}.
     */
    @Test
    public final void testSupprimerGuichetBancaire()
    {
        // Récupération d'un guichet bancaire
        GuichetBancaire guichetBancaire = getGuichetBancaire(1);

        // Suppression du guichet bancaire
        guichetBancaireService.supprimerGuichetBancaire(guichetBancaire);

        // Vérification de l'appel à la méthode de suppression du guichet bancaire
        Mockito.verify(guichetbancairedao, Mockito.times(1)).deleteObject(guichetBancaire);
    }

    /**
         * Test de la méthode
         * {@link fr.gouv.finances.lombok.bancaire.service.impl.GuichetBancaireServiceImpl#testerExistenceGuichetBancaireNonPerimeParCodeGuichetEtCodeEtablissement(java.lang.String, java.lang.String)}.
         */
        @Test
        public final void testTesterExistenceGuichetBancaireNonPerimeParCodeGuichetEtCodeEtablissement()
        {
            // Paramétrage du comportement du bouchon
            when(guichetbancairedao
                .testeExistenceGuichetBancaireNonPerimeParCodeGuichetEtCodeEtablissement(
                    "1", CODE_ETABLISSEMENT)).thenReturn(true);
            when(guichetbancairedao
                .testeExistenceGuichetBancaireNonPerimeParCodeGuichetEtCodeEtablissement(
                    "1", VALEUR_INCONNUE)).thenReturn(false);
    
            // Exécutions du service et vérifications par rapport au comportement défini
            assertTrue("Guichet bancaire non existant", guichetBancaireService
                .testerExistenceGuichetBancaireNonPerimeParCodeGuichetEtCodeEtablissement(
                    "1", CODE_ETABLISSEMENT));
            assertFalse("Guichet bancaire existant", guichetBancaireService
                .testerExistenceGuichetBancaireNonPerimeParCodeGuichetEtCodeEtablissement(
                    "2", CODE_ETABLISSEMENT));
        }

    /**
         * Test de la méthode
         * {@link fr.gouv.finances.lombok.bancaire.service.impl.GuichetBancaireServiceImpl#testerExistenceGuichetBancairePerimeOuNonParCodeGuichetEtCodeEtablissement(java.lang.String, java.lang.String)}.
         */
        @Test
        public final void testTesterExistenceGuichetBancairePerimeOuNonParCodeGuichetEtCodeEtablissement()
        {
            // Paramétrage du comportement du bouchon
            when(guichetbancairedao
                .testeExistenceGuichetBancairePerimeOuNonParCodeGuichetEtCodeEtablissement("1", CODE_ETABLISSEMENT))
                .thenReturn(true);
            when(guichetbancairedao
                .testeExistenceGuichetBancairePerimeOuNonParCodeGuichetEtCodeEtablissement("2", CODE_ETABLISSEMENT))
                .thenReturn(false);
    
            // Exécutions du service et vérifications par rapport au comportement défini
            assertTrue("Guichet bancaire non existant", guichetBancaireService
                .testerExistenceGuichetBancairePerimeOuNonParCodeGuichetEtCodeEtablissement("1", CODE_ETABLISSEMENT));
            assertFalse("Guichet bancaire existant", guichetBancaireService
                .testerExistenceGuichetBancairePerimeOuNonParCodeGuichetEtCodeEtablissement("2", CODE_ETABLISSEMENT));
        }

    /**
     * Test de la méthode
     * {@link fr.gouv.finances.lombok.bancaire.service.impl.GuichetBancaireServiceImpl#verifierExistenceGuichetBancaire(java.lang.String, java.lang.String)}.
     */
    @Test
    public final void testVerifierExistenceGuichetBancaire()
    {
        // Vérification de l'absence des données avant le test
        verifierExceptionExistenceGuichetBancaireParCodeGuichetEtCodeEtablissement("1", CODE_ETABLISSEMENT);

        // Paramétrage du comportement du bouchon
        List<GuichetBancaire> listeAttendue = getGuichetsBancaires();
        when(guichetbancairedao.findGuichetsBancairesParCodeGuichetEtCodeEtablissement("1", CODE_ETABLISSEMENT))
            .thenReturn(listeAttendue);

        // Exécution du service
        List<GuichetBancaire> listeResultat
            = guichetBancaireService.verifierExistenceGuichetBancaire("1", CODE_ETABLISSEMENT);

        // Vérification du service par rapport au comportement défini
        assertFalse("Liste de guichet bancaire non vide", listeResultat.isEmpty());
        verifierExceptionExistenceGuichetBancaireParCodeGuichetEtCodeEtablissement("2", CODE_ETABLISSEMENT);
    }

    /**
     * Méthode permettant de vérifier qu'une RegleGestionException est levée sur la recherche
     * d'un guichet bancaire inexistant
     *
     * @param codeGuichet code du guichet recherché
     * @param codeEtablissement code de l'établissement du guichet
     */
    private void verifierExceptionExistenceGuichetBancaireParCodeGuichetEtCodeEtablissement(
        String codeGuichet, String codeEtablissement)
    {
        try
        {
            guichetBancaireService.verifierExistenceGuichetBancaire(
                codeGuichet, codeEtablissement).isEmpty();
            Assert.fail("Une RegleGestionException aurait du être levée");
        }
        catch (RegleGestionException rge)
        {
            LOGGER.debug("Une RegleGestionException a bien été levée");
        }
    }

}
