/*
 * Copyright (c) 2016 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.bancaire.batch;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.net.MalformedURLException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.ReflectionUtils;

import fr.gouv.finances.lombok.bancaire.bean.GuichetBancaire;
import fr.gouv.finances.lombok.bancaire.service.impl.GuichetBancaireServiceImpl;
import fr.gouv.finances.lombok.test.controle.VerificationExecution;
import fr.gouv.finances.lombok.util.exception.ExploitationException;

/**
 * Tests unitaires du traitement de chargement des fichiers bancaires.
 *
 * @author chouard
 * @author Christophe Breheret-Girardin
 */
@ContextConfiguration({"classpath:/conf/traitementchargementguichetsetetablissementstabankContext-service.xml"
    , "classpath:/conf/applicationContext-bancaire-service-test.xml"
    , "classpath*:/conf/applicationContext-core-transaction-test.xml"
    , "classpath*:/conf/applicationContext-commun-transaction.xml"})
//@ActiveProfiles(profiles = {"bancaire", "hibernate"})
@SpringBootTest(properties = {"lombok.composant.bancaire=true", "lombok.orm.jpa=false"})
@RunWith(SpringJUnit4ClassRunner.class)
public class TraitementChargementGuichetsEtEtablissementsTabankImplTest
{
    /** Injection du batch à tester. */
    @Autowired
    private TraitementChargementGuichetsEtEtablissementsTabank batch;

    /** Initialisation d'un bouchon du service métier. */
    @Mock
    private GuichetBancaireServiceImpl guichetBancaireService;

    /** Inspecteur de l'argument passé à une méthode du service métier. */
    private ArgumentCaptor<List<GuichetBancaire>> captureGuichet;

    /** Constante du préfix du nom du fichier temporaire à créer pour chaque test */
    private static final String PREFIX_FICHIER = "test_batch_guichet_";

    /** Constante de l'extension du fichier temporaire à créer pour chaque test */
    private static final String EXTENSION_FICHIER = ".txt";

    /** Attribut du batch permettant de stocker le fichier à traiter */
    private static final String ATTRIBUT_FICHIER_SERVICE = "fichier";
    
    /**
     * Initialisation des tests.
     */
    @SuppressWarnings("unchecked")
    @Before
    public void setupMock()
    {
        MockitoAnnotations.initMocks(this);
        batch.setGuichetbancairebatchserviceso(guichetBancaireService);

        // Initialisation d'un inspecteur de l'argument passé à une méthode du service,
        // qui permettra de savoir si celle-ci est appelée le bon nombre de fois,
        // et si les informations lui sont passées au travers de cet argument ont bien été initialisées
        captureGuichet = ArgumentCaptor.forClass((Class) List.class);
    }
    
    /**
     * Test de la méthode suivante, dans le cas de non initialisation de fichier
     * {@link fr.gouv.finances.lombok.bancaire.batch.TraitementChargementGuichetsEtEtablissementsTabankImpl#traiterBatch()}.
     */
    @Test
    public final void testTraiterBatchSansFichier()
    {
        // Exécution du traitement sans fichier fourni
        batch.setFichiertabank(null);
        batch.setIgnorerPremiereLigne(false);
        VerificationExecution.verifierException(ExploitationException.class, "chemin non renseigné"
            , () -> batch.traiterBatch());
    }

    /**
     * Test de la méthode suivante, dans le cas d'absence de fichier
     * {@link fr.gouv.finances.lombok.bancaire.batch.TraitementChargementGuichetsEtEtablissementsTabankImpl#traiterBatch()}.
     */
    @Test
    public final void testTraiterBatchSansFichierTrouve()
    {
        // Exécution du traitement sans fichier trouvé
        batch.setFichiertabank("toto.txt");
        batch.setIgnorerPremiereLigne(false);
        VerificationExecution.verifierException(ExploitationException.class
            , "chemin non renseigné", () -> batch.traiterBatch());
    }

    /**
     * Test de la méthode suivante, dans le cas d'un fichier vide
     * {@link fr.gouv.finances.lombok.bancaire.batch.TraitementChargementGuichetsEtEtablissementsTabankImpl#traiterBatch()}.
     *
     * @throws IOException erreur de création du fichier de test
     */
    @Test
    public final void testTraiterBatchFichierVide() throws IOException
    {
        // Préparation d'un fichier vide
        File tabTest = createTabankFile(PREFIX_FICHIER, EXTENSION_FICHIER, new byte[]{});
        batch.setFichiertabank(tabTest.toURI().toString());
        batch.setIgnorerPremiereLigne(false);

        // Exécution du traitement
        VerificationExecution.verifierException(ExploitationException.class, tabTest.getAbsolutePath()
            , () -> batch.traiterBatch());
    }

    /**
     * Test de la méthode suivante, avec un fichier erroné
     * {@link fr.gouv.finances.lombok.bancaire.batch.TraitementChargementGuichetsEtEtablissementsTabankImpl#traiterBatch()}.
     *
     * @throws IOException erreur de création du fichier de test
     */
    @Test
    public final void testTraiterBatchAvecContenuFichierErroné() throws IOException
    {
        // Préparation d'un fichier avec un contenu erroné
        File tabTest = createTabankFile(PREFIX_FICHIER, EXTENSION_FICHIER, "toto".getBytes());
        batch.setFichiertabank(tabTest.toURI().toString());
        batch.setIgnorerPremiereLigne(false);

        // Exécution du traitement (ligne 1 : format ko)
        VerificationExecution.verifierException(ExploitationException.class, "1-true-0"
            , () -> batch.traiterBatch());
    }

    /**
     * Test de la méthode suivante, dans le cas d'une erreur de lecture de fichier
     * {@link fr.gouv.finances.lombok.bancaire.batch.TraitementChargementGuichetsEtEtablissementsTabankImpl#traiterBatch()}.
     *
     * @throws IOException erreur de création du fichier de test
     */
    @Test
    public final void testTraiterBatchErreurFichier() throws IOException
    {
        // Initialisation d'un comportement à exécuter lors de l'appel à la méthode
        // "guichetBancaireService.toString()"
        Mockito.doAnswer(new Answer<Object>()
        {
            @Override
            public Object answer(InvocationOnMock invocationOnMock) throws Throwable {
                // Remplacement du fichier valide par un fichier erroné,
                // afin de provoquer une IOException dans la suite du traitement
                Field field = ReflectionUtils.findField(batch.getClass(), ATTRIBUT_FICHIER_SERVICE);
                ReflectionUtils.makeAccessible(field);
                ReflectionUtils.setField(field, batch, new File(""));
                return null;
            }
        }).when(guichetBancaireService).toString();

        // Préparation d'un fichier valide (au départ du traitement)
        String contenu =
            new StringBuilder(178).append("1000800001AG SIEGE SOCIAL     52 AV DES CHAMPS PIERREUX    ")
                .append("92736NANTERRE CEDEX       GE CAPITAL EQUIPEMENT FINANCE           ")
                    .append("GECAPITEQUGECAPITEQU NANTERRE     006403901          ").toString();
        File tabTest = createTabankFile(PREFIX_FICHIER, EXTENSION_FICHIER, contenu.getBytes());
        batch.setFichiertabank(tabTest.toURI().toString());
        batch.setIgnorerPremiereLigne(false);

        // Exécution du traitement
        VerificationExecution.verifierException(ExploitationException.class, () -> batch.traiterBatch());
    }
    
    /**
     * Test de la méthode suivante, dans le cas d'un code établissement absent
     * {@link fr.gouv.finances.lombok.bancaire.batch.TraitementChargementGuichetsEtEtablissementsTabankImpl#traiterBatch()}.
     *
     * @throws IOException erreur de création du fichier de test
     */
    @Test
    public final void testTraiterBatchAvecCodeEtablissementAbsent() throws IOException
    {
        // Préparation d'un fichier sans code établissement
        String contenu =
            new StringBuilder(178).append("     00001AG SIEGE SOCIAL     52 AV DES CHAMPS PIERREUX    ")
                .append("92736NANTERRE CEDEX       GE CAPITAL EQUIPEMENT FINANCE           ")
                    .append("GECAPITEQUGECAPITEQU NANTERRE     006403901          ").toString();
        File tabTest = createTabankFile(PREFIX_FICHIER, EXTENSION_FICHIER, contenu.getBytes());
        batch.setFichiertabank(tabTest.toURI().toString());
        batch.setIgnorerPremiereLigne(false);

        // Exécution du traitement (ligne 1 : format ok mais 1 erreur)
        VerificationExecution.verifierException(ExploitationException.class, "1-false-1"
            , () -> batch.traiterBatch());
    }

    /**
     * Test de la méthode suivante, dans le cas d'un code guichet absent
     * {@link fr.gouv.finances.lombok.bancaire.batch.TraitementChargementGuichetsEtEtablissementsTabankImpl#traiterBatch()}.
     *
     * @throws IOException erreur de création du fichier de test
     */
    @Test
    public final void testTraiterBatchAvecCodeGuichetAbsent() throws IOException
    {
        // Préparation d'un fichier sans code guichet
        String contenu =
            new StringBuilder(178).append("10008     AG SIEGE SOCIAL     52 AV DES CHAMPS PIERREUX    ")
                .append("92736NANTERRE CEDEX       GE CAPITAL EQUIPEMENT FINANCE           ")
                    .append("GECAPITEQUGECAPITEQU NANTERRE     006403901          ").toString();
        File tabTest = createTabankFile(PREFIX_FICHIER, EXTENSION_FICHIER, contenu.getBytes());
        batch.setFichiertabank(tabTest.toURI().toString());
        batch.setIgnorerPremiereLigne(false);

        // Exécution du traitement (ligne 1 : format ok mais 1 erreur)
        VerificationExecution.verifierException(ExploitationException.class, "1-false-1"
            , () -> batch.traiterBatch());
    }

    /**
     * Test de la méthode suivante, dans le cas d'un libellé de domiciliation absent
     * {@link fr.gouv.finances.lombok.bancaire.batch.TraitementChargementGuichetsEtEtablissementsTabankImpl#traiterBatch()}.
     *
     * @throws IOException erreur de création du fichier de test
     */
    @Test
    public final void testTraiterBatchAvecLibelleAbrevDeDomiciliationAbsent() throws IOException
    {
        // Préparation d'un fichier sans libellé de domiciliation
        String contenu =
            new StringBuilder(178).append("1000800001AG SIEGE SOCIAL     52 AV DES CHAMPS PIERREUX    ")
                .append("92736NANTERRE CEDEX       GE CAPITAL EQUIPEMENT FINANCE           ")
                    .append("GECAPITE                          006403901          ").toString();
        File tabTest = createTabankFile(PREFIX_FICHIER, EXTENSION_FICHIER, contenu.getBytes());
        batch.setFichiertabank(tabTest.toURI().toString());
        batch.setIgnorerPremiereLigne(false);

        // Exécution du traitement (ligne 1 : format ok mais 1 erreur)
        VerificationExecution.verifierException(ExploitationException.class, "1-false-1"
            , () -> batch.traiterBatch());
    }

    /**
     * Test de la méthode suivante, dans le cas de 3 erreurs
     * {@link fr.gouv.finances.lombok.bancaire.batch.TraitementChargementGuichetsEtEtablissementsTabankImpl#traiterBatch()}.
     *
     * @throws IOException erreur de création du fichier de test
     */
    @Test
    public final void testTraiterBatchAvecToutesInfosAbsentes() throws IOException
    {
        // Préparation d'un fichier avec 3 erreurs
        String contenu =
            new StringBuilder(178).append("          AG SIEGE SOCIAL     52 AV DES CHAMPS PIERREUX    ")
                .append("92736NANTERRE CEDEX       GE CAPITAL EQUIPEMENT FINANCE           ")
                    .append("GECAPITE                          006403901          ").toString();
        File tabTest = createTabankFile(PREFIX_FICHIER, EXTENSION_FICHIER, contenu.getBytes());
        batch.setFichiertabank(tabTest.toURI().toString());
        batch.setIgnorerPremiereLigne(false);

        // Exécution du traitement et vérification (ligne 1 : format ok mais 3 erreurs)
        VerificationExecution.verifierException(ExploitationException.class, "1-false-3"
            , () -> batch.traiterBatch());
    }

    /**
     * Test de la méthode suivante, dans le cas de 2 lignes en erreur
     * {@link fr.gouv.finances.lombok.bancaire.batch.TraitementChargementGuichetsEtEtablissementsTabankImpl#traiterBatch()}.
     *
     * @throws IOException erreur de création du fichier de test
     */
    @Test
    public final void testTraiterBatch2LignesErreur() throws IOException
    {
        // Préparation d'un fichier avec erreurs sur 2 lignes
        String contenu = new StringBuilder(356)
            .append("          AG SIEGE SOCIAL     52 AV DES CHAMPS PIERREUX    ")
            .append("92736NANTERRE CEDEX       GE CAPITAL EQUIPEMENT FINANCE           ")
            .append("GECAPITEQUGECAPITEQU NANTERRE     006403901          ").append('\n')
            .append("10011     CAISSE CENTRALE     6 8 R ST ROMAIN              ")
            .append("75006PARIS                CAISSE NATIONALE D EPARGNE              ")
            .append("PTT.CNE   PTT CNE PARIS           0000017411001200021").toString();

        File tabTest = createTabankFile(PREFIX_FICHIER, EXTENSION_FICHIER, contenu.getBytes());
        batch.setFichiertabank(tabTest.toURI().toString());
        batch.setIgnorerPremiereLigne(false);

        // Exécution du traitement et vérification :
        // ligne 1 : format ok mais 2 erreurs
        // ligne 2 : format ok mais 1 erreur
        VerificationExecution.verifierException(ExploitationException.class, "1-false-2|2-false-1"
            , () -> batch.traiterBatch());
    }

    /**
     * Test de la méthode suivante, dans le cas d'une ligne en erreur et d'une ligne pas au bon format
     * {@link fr.gouv.finances.lombok.bancaire.batch.TraitementChargementGuichetsEtEtablissementsTabankImpl#traiterBatch()}.
     *
     * @throws IOException erreur de création du fichier de test
     */
    @Test
    public final void testTraiterBatch2LignesErreurEtFormat() throws IOException
    {
        // Préparation d'un fichier avec erreurs sur 2 lignes
        String contenu = new StringBuilder(356)
            .append("          AG SIEGE SOCIAL     52 AV DES CHAMPS PIERREUX    ")
            .append("92736NANTERRE CEDEX       GE CAPITAL EQUIPEMENT FINANCE           ")
            .append("GECAPITEQUGECAPITEQU NANTERRE     006403901          ").append('\n')
            .append("10011     CAISSE CENTRALE     6 8 R ST ROMAIN              ")
            .append("75006PARIS                CAISSE NATIONALE D EPARGNE              ")
            .append("PTT.CNE   PTT CNE PARIS           0000017411001200021sdf").toString();

        File tabTest = createTabankFile(PREFIX_FICHIER, EXTENSION_FICHIER, contenu.getBytes());
        batch.setFichiertabank(tabTest.toURI().toString());
        batch.setIgnorerPremiereLigne(false);

        // Exécution du traitement et vérification :
        // ligne 1 : format ok mais 2 erreurs
        // ligne 2 : format ko
        VerificationExecution.verifierException(ExploitationException.class, "1-false-2|2-true-0"
            , () -> batch.traiterBatch());
    }

    /**
     * Test de la méthode suivante, dans le cas de 2 lignes dont 1 en erreur
     * {@link fr.gouv.finances.lombok.bancaire.batch.TraitementChargementGuichetsEtEtablissementsTabankImpl#traiterBatch()}.
     *
     * @throws IOException erreur de création du fichier de test
     */
    @Test
    public final void testTraiterBatch2LignesDont1Erreur() throws IOException
    {
        // Préparation d'un fichier avec erreurs sur 2 lignes
        String contenu = new StringBuilder(356)
            .append("1000800001AG SIEGE SOCIAL     52 AV DES CHAMPS PIERREUX    ")
            .append("92736NANTERRE CEDEX       GE CAPITAL EQUIPEMENT FINANCE           ")
            .append("GECAPITEQUGECAPITEQU NANTERRE     006403901          ").append('\n')
            .append("10011     CAISSE CENTRALE     6 8 R ST ROMAIN              ")
            .append("75006PARIS                CAISSE NATIONALE D EPARGNE              ")
            .append("PTT.CNE   PTT CNE PARIS           0000017411001200021").toString();

        File tabTest = createTabankFile(PREFIX_FICHIER, EXTENSION_FICHIER, contenu.getBytes());
        batch.setFichiertabank(tabTest.toURI().toString());
        batch.setIgnorerPremiereLigne(false);

        // Exécution du traitement et vérification :
        // ligne 1 : ok
        // ligne 2 : format ok mais 1 erreur
        VerificationExecution.verifierException(ExploitationException.class, "1-false-0|2-false-1"
            , () -> batch.traiterBatch());
    }

    /**
     * Test de la méthode suivante, dans le cas d'un fichier avec une ligne
     * {@link fr.gouv.finances.lombok.bancaire.batch.TraitementChargementGuichetsEtEtablissementsTabankImpl#traiterBatch()}.
     *
     * @throws IOException erreur de création du fichier de test
     */
    @Test
    public final void testTraiterBatchAvecFichier1Ligne() throws IOException
    {
        // Préparation d'un fichier avec 1 ligne
        String contenu =
            new StringBuilder(178).append("1000800001AG SIEGE SOCIAL     52 AV DES CHAMPS PIERREUX    ")
                .append("92736NANTERRE CEDEX       GE CAPITAL EQUIPEMENT FINANCE           ")
                .append("GECAPITEQUGECAPITEQU NANTERRE     006403901          ").toString();
        File tabTest = createTabankFile(PREFIX_FICHIER, EXTENSION_FICHIER, contenu.getBytes());
        batch.setFichiertabank(tabTest.toURI().toString());
        batch.setIgnorerPremiereLigne(false);

        // Exécution du traitement
        batch.traiterBatch();

        // Vérification des guichets traités par le service métier
        GuichetBancaire guichetBancaire = getGuichet("00001");
        verifications(1, Collections.singletonMap(guichetBancaire.getCodeGuichet(), guichetBancaire));
    }

    /**
     * Test de la méthode suivante, dans le cas d'un fichier avec une ligne à prendre en compte sur 2
     * {@link fr.gouv.finances.lombok.bancaire.batch.TraitementChargementGuichetsEtEtablissementsTabankImpl#traiterBatch()}.
     *
     * @throws IOException erreur de création du fichier de test
     */
    @Test
    public final void testTraiterBatchAvecFichier1LigneSur2() throws IOException
    {
        // Paramétrage permettant d'ignorer la 1ère ligne
        batch.setIgnorerPremiereLigne(true);

        // Exécution du traitement
        executer2lignes();
        
        // Vérification des guichets traités par le service métier
        GuichetBancaire guichetBancaire = getGuichet("00020");
        verifications(1, Collections.singletonMap(guichetBancaire.getCodeGuichet(), guichetBancaire));
    }
    
    /**
     * Test de la méthode suivante, dans le cas d'un fichier avec deux lignes
     * {@link fr.gouv.finances.lombok.bancaire.batch.TraitementChargementGuichetsEtEtablissementsTabankImpl#traiterBatch()}.
     *
     * @throws IOException erreur de création du fichier de test
     */
    @Test
    public final void testTraiterBatchAvecFichier2Lignes() throws IOException
    {
        // Paramétrage permettant de ne pas ignorer la 1ère ligne
        batch.setIgnorerPremiereLigne(false);

        // Exécution du traitement
        executer2lignes();
        
        // Vérification des guichets traités par le service métier
        GuichetBancaire guichetBancaire1 = getGuichet("00001");
        GuichetBancaire guichetBancaire2 = getGuichet("00020");
        Map<String, GuichetBancaire> guichetsBancaires = new HashMap<>();
        guichetsBancaires.put(guichetBancaire1.getCodeGuichet(), guichetBancaire1);
        guichetsBancaires.put(guichetBancaire2.getCodeGuichet(), guichetBancaire2);
        verifications(2, guichetsBancaires);
    }
    
    /**
     * Méthode permettant de générer un fichier de deux lignes et d'exécuter le traitement
     * {@link fr.gouv.finances.lombok.bancaire.batch.TraitementChargementGuichetsEtEtablissementsTabankImpl#traiterBatch()}.
     *
     * @throws IOException erreur de création du fichier de test
     */
    private void executer2lignes() throws IOException
    {
        // Préparation d'un fichier avec 2 lignes
        String contenu = new StringBuilder(356)
            .append("1000800001AG SIEGE SOCIAL     52 AV DES CHAMPS PIERREUX    ")
            .append("92736NANTERRE CEDEX       GE CAPITAL EQUIPEMENT FINANCE           ")
            .append("GECAPITEQUGECAPITEQU NANTERRE     006403901          ").append('\n')
            .append("1001100020CAISSE CENTRALE     6 8 R ST ROMAIN              ")
            .append("75006PARIS                CAISSE NATIONALE D EPARGNE              ")
            .append("PTT.CNE   PTT CNE PARIS           0000017411001200021").toString();
        File tabTest = createTabankFile(PREFIX_FICHIER, EXTENSION_FICHIER, contenu.getBytes());
        batch.setFichiertabank(tabTest.toURI().toString());

        // Exécution du traitement
        batch.traiterBatch();
    }

    /**
     * Test de la méthode suivante, dans le cas d'un fichier représentatif
     * {@link fr.gouv.finances.lombok.bancaire.batch.TraitementChargementGuichetsEtEtablissementsTabankImpl#traiterBatch()}.
     *
     * @throws MalformedURLException erreur de récupération du fichier de test
     */
    @Test
    public final void testTraiterBatchAvecFichierRepresentatif() throws MalformedURLException
    {
        // Chargement d'un fichier représentatif
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("bdfsi_partiel.txt").getFile());
        String path = file.toURI().toURL().toString();
        batch.setFichiertabank(path);
        batch.setIgnorerPremiereLigne(false);

        // Exécution du traitement
        batch.traiterBatch();

        // Vérification du nombre de guichets traités par le service métier
        assertEquals("Pas le bon nombre de lignes traités", 75, batch.getNumLigne());
    }

    /**
     * Vérification s'il y a eu un appel au service métier avec le bon nombre de guichets
     * {@link fr.gouv.finances.lombok.bancaire.batch.TraitementChargementGuichetsEtEtablissementsTabankImpl#traiterBatch()}.
     *
     * @param nbreGuichets nombre de guichets traités
     */
    private void verifications(int nbreGuichets, Map<String, GuichetBancaire> guichetsBancaires)
    {
        verifications(1, nbreGuichets, guichetsBancaires);
    }

    /**
     * Vérification s'il y a eu un appel au service métier avec le bon nombre de guichets.
     *
     * @param nbreAppel nombre d'appel au service métier
     * @param nbreGuichets nombre de guichets traités
     */
    private void verifications(int nbreAppel, int nbreGuichets
        , Map<String, GuichetBancaire> guichetsBancairesAttendus)
    {
        // Vérification du nombre d'appel à la méthode "creerLesGuichetsBancaires"
        Mockito.verify(guichetBancaireService, Mockito.times(nbreAppel)).creerLesGuichetsBancaires(
            captureGuichet.capture(), Mockito.any(), Matchers.eq(""));

        // Les nombres de ligne correspondant à des guichets ont-ils tous été traités ?
        assertEquals("Pas le bon nombre de lignes traités", nbreGuichets, batch.getNumLigne());

        // Vérification des valeurs des guichets bancaires
        verifierDonnees(guichetsBancairesAttendus
            , captureGuichet.getAllValues().stream().flatMap(g -> g.stream()).collect(Collectors.toList()));
    }

    /**
     * Méthode permettant de vérifier les données des guichets bancaires.
     *
     * @param guichetsBancairesAttendus Map des guichets bancaires attendus
     * @param guichetsBancaires liste des guichets bancaires à vérifier
     */
    private void verifierDonnees(Map<String, GuichetBancaire> guichetsBancairesAttendus
        , List<GuichetBancaire> guichetsBancaires)
    {
        guichetsBancaires.stream().forEach(g ->
        {
            assertTrue("Le guichet suivant n'est pas attendu : " + g.getCodeGuichet()
                , guichetsBancairesAttendus.containsKey(g.getCodeGuichet()));

            GuichetBancaire guichetBancaire = guichetsBancairesAttendus.get(g.getCodeGuichet());
            assertEquals("Erreur de code établissement"
                , guichetBancaire.getCodeEtablissement(), g.getCodeEtablissement());
            assertEquals("Erreur de nom guichet", guichetBancaire.getNomGuichet(), g.getNomGuichet());
            assertEquals("Erreur d'adresse", guichetBancaire.getAdresseGuichet(), g.getAdresseGuichet());
            assertEquals("Erreur de code postal", guichetBancaire.getCodePostal(), g.getCodePostal());
            assertEquals("Erreur de ville", guichetBancaire.getVilleCodePostal(), g.getVilleCodePostal());
            assertEquals("Erreur de dénomination complète"
                , guichetBancaire.getDenominationComplete(), g.getDenominationComplete());
            assertEquals("Erreur de dénomination abrégée"
                , guichetBancaire.getDenominationAbregee(), g.getDenominationAbregee());
            assertEquals("Erreur libellé de domiciliation"
                , guichetBancaire.getLibelleAbrevDeDomiciliation(), g.getLibelleAbrevDeDomiciliation());
            assertEquals("Erreur de crédit"
                , guichetBancaire.getCodeReglementDesCredits(), g.getCodeReglementDesCredits());
            assertEquals("Erreur de mois", guichetBancaire.getMoisDeModification(), g.getMoisDeModification());
            assertEquals("Erreur d'année", guichetBancaire.getAnneeDeModification(), g.getAnneeDeModification());
            assertEquals("Erreur de code enregistrement"
                , guichetBancaire.getCodeEnregistrement(), g.getCodeEnregistrement());
            assertEquals("Erreur de nouveau code établissement", guichetBancaire.getNouveauCodeEtablissement(), g.getNouveauCodeEtablissement());
            assertEquals("Erreur de nouveau code guichet"
                , guichetBancaire.getNouveauCodeGuichet(), g.getNouveauCodeGuichet());
        });
    }

    /**
     * Méthode permettant de créer un fichier TABANK temporaire pour les tests
     *
     * @param prefix nom du fichier
     * @param suffix extension du fichier
     * @param contenu contenu du fichier
     * @return fichier temporaire créé
     *
     * @throws IOException erreur de création du fichier de test
     */
    private File createTabankFile(String prefix, String suffix, byte[] contenu) throws IOException
    {
        File tempFile = File.createTempFile(prefix, suffix);
        tempFile.deleteOnExit();
        try (FileOutputStream fileTest = new FileOutputStream(tempFile))
        {
            fileTest.write(contenu);
        }
        return tempFile;
    }

    /**
     * Méthode permettant de générer un guichet bancaire correspondant aux jeux d'essai
     *
     * @param code code du guichet bancaire à générer
     * @return le guichet bancaire généré
     */
    private GuichetBancaire getGuichet(String code)
    {
        GuichetBancaire guichetBancaire = new GuichetBancaire();
        guichetBancaire.setCodeGuichet(code);

        switch (code)
        {
            case "00001" :
                guichetBancaire.setCodeEtablissement("10008");
                guichetBancaire.setNomGuichet("AG SIEGE SOCIAL     ");
                guichetBancaire.setAdresseGuichet("52 AV DES CHAMPS PIERREUX    ");
                guichetBancaire.setCodePostal("92736");
                guichetBancaire.setVilleCodePostal("NANTERRE CEDEX       ");
                guichetBancaire.setDenominationComplete("GE CAPITAL EQUIPEMENT FINANCE           ");
                guichetBancaire.setDenominationAbregee("GECAPITEQU");
                guichetBancaire.setLibelleAbrevDeDomiciliation("GECAPITEQU NANTERRE     ");
                guichetBancaire.setCodeReglementDesCredits("0064");
                guichetBancaire.setMoisDeModification("03");
                guichetBancaire.setAnneeDeModification("90");
                guichetBancaire.setCodeEnregistrement("1");
                guichetBancaire.setNouveauCodeEtablissement("     ");
                guichetBancaire.setNouveauCodeGuichet("     ");
                break;
            case "00020" :
                guichetBancaire.setCodeEtablissement("10011");
                guichetBancaire.setNomGuichet("CAISSE CENTRALE     ");
                guichetBancaire.setAdresseGuichet("6 8 R ST ROMAIN              ");
                guichetBancaire.setCodePostal("75006");
                guichetBancaire.setVilleCodePostal("PARIS                ");
                guichetBancaire.setDenominationComplete("CAISSE NATIONALE D EPARGNE              ");
                guichetBancaire.setDenominationAbregee("PTT.CNE   ");
                guichetBancaire.setLibelleAbrevDeDomiciliation("PTT CNE PARIS           ");
                guichetBancaire.setCodeReglementDesCredits("0000");
                guichetBancaire.setMoisDeModification("01");
                guichetBancaire.setAnneeDeModification("74");
                guichetBancaire.setCodeEnregistrement("1");
                guichetBancaire.setNouveauCodeEtablissement("10012");
                guichetBancaire.setNouveauCodeGuichet("00021");
                break;
        }
        
        return guichetBancaire;
    }
}