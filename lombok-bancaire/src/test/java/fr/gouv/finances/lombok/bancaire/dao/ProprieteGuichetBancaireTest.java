package fr.gouv.finances.lombok.bancaire.dao;

import fr.gouv.finances.lombok.test.pojo.AbstractCorePojoTest;

/**
 * Tests unitaires de l'énumération ProprieteGuichetBancaire.
 *
 * @author Christophe Breheret-Girardin
 */
public class ProprieteGuichetBancaireTest extends AbstractCorePojoTest<ProprieteGuichetBancaire>
{
    /**
     * Constructeur.
     */
    public ProprieteGuichetBancaireTest()
    {
        super();
    }
}