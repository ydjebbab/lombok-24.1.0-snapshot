/*
 * Copyright (c) 2016 DGFiP - Tous droits réservés
 * 
 */
package fr.gouv.finances.lombok.bancaire.bean;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import java.sql.Timestamp;
import java.util.Calendar;

import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;

import org.junit.Assert;
import org.junit.Test;

import fr.gouv.finances.lombok.test.controle.VerificationExecution;
import fr.gouv.finances.lombok.test.pojo.AbstractCorePojoTest;
import fr.gouv.finances.lombok.util.exception.RegleGestionException;

/**
 * Tests unitaires de l'entité GuichetBancaire.
 *
 * @author chouard
 * @author Christophe Breheret-Girardin
 */
public class GuichetBancaireTest extends AbstractCorePojoTest<GuichetBancaire>
{
    /**
     * Constructeur.
     */
    public GuichetBancaireTest()
    {
        super();
    }

    /**
     * Test de 
     * {@link fr.gouv.finances.lombok.bancaire.bean.GuichetBancaire#calculerAttributEstEnVoieDePeremption()}.
     */
    @Test
    public final void testCalculerAttributEstEnVoieDePeremption()
    {
        // Initialisation d'un guichet bancaire
        GuichetBancaire guichetBancaire = new GuichetBancaire();
        
        // La voie ne doit pas être en péremption
        Assert.assertEquals("La voie ne doit pas être en péremption"
            , false, guichetBancaire.isEstEnVoieDePeremption());

        // Après un calcul de péremption
        calculEtVerificationVoieDePeremption(false, guichetBancaire);

        // Vérification avec des codes d'établissement non initialisés
        guichetBancaire.setNouveauCodeEtablissement(null);
        calculEtVerificationVoieDePeremption(false, guichetBancaire);
        guichetBancaire.setNouveauCodeEtablissement("");
        calculEtVerificationVoieDePeremption(false, guichetBancaire);
        guichetBancaire.setNouveauCodeEtablissement(" ");
        calculEtVerificationVoieDePeremption(false, guichetBancaire);

        // Vérification avec des codes de guichet non initialisés
        guichetBancaire.setNouveauCodeGuichet(null);
        calculEtVerificationVoieDePeremption(false, guichetBancaire);
        guichetBancaire.setNouveauCodeGuichet("");
        calculEtVerificationVoieDePeremption(false, guichetBancaire);
        guichetBancaire.setNouveauCodeGuichet(" ");
        calculEtVerificationVoieDePeremption(false, guichetBancaire);
        
        // Vérification avec des valeurs non initialisées / vides / non vides
        guichetBancaire.setNouveauCodeEtablissement(null);
        guichetBancaire.setNouveauCodeGuichet("nouveau code guichet");
        calculEtVerificationVoieDePeremption(false, guichetBancaire);
        guichetBancaire.setNouveauCodeEtablissement("");
        guichetBancaire.setNouveauCodeGuichet("nouveau code guichet");
        calculEtVerificationVoieDePeremption(false, guichetBancaire);
        guichetBancaire.setNouveauCodeEtablissement("nouveau code guichet");
        guichetBancaire.setNouveauCodeGuichet(null);
        calculEtVerificationVoieDePeremption(false, guichetBancaire);
        guichetBancaire.setNouveauCodeEtablissement("nouveau code guichet");
        guichetBancaire.setNouveauCodeGuichet("");
        calculEtVerificationVoieDePeremption(false, guichetBancaire);
        
        // Vérification avec des valeurs non vides
        guichetBancaire.setNouveauCodeEtablissement("nouveau code etablissement");
        guichetBancaire.setNouveauCodeGuichet("nouveau code guichet");
        calculEtVerificationVoieDePeremption(true, guichetBancaire);
    }

    /**
     * Méthode permettant le calcul et la vérification de la voie de péremption.
     *
     * @param voieDePeremptionAttendue la voie est-elle en péremption ?
     * @param guichetBancaire guichet bancaire
     */
    private void calculEtVerificationVoieDePeremption(boolean voieDePeremptionAttendue
        , GuichetBancaire guichetBancaire)
    {
        // Calcul de péremption
        guichetBancaire.calculerAttributEstEnVoieDePeremption();

        // Vérification
        if (voieDePeremptionAttendue)
        {
            Assert.assertEquals("La voie doit être être en péremption"
                , true, guichetBancaire.isEstEnVoieDePeremption());
        }
        else
        {
            Assert.assertEquals("La voie ne doit pas être en péremption"
                , false, guichetBancaire.isEstEnVoieDePeremption());
        }
    }

    /**
     * Test de 
     * {@link fr.gouv.finances.lombok.bancaire.bean.GuichetBancaire#verifierCodeEtablissement()}.
     */
    @Test
    public final void testVerifierCodeEtablissement()
    {
        // Initialisation d'un guichet bancaire
        GuichetBancaire guichetBancaire = new GuichetBancaire();

        // Exécution des tests et vérifications
        guichetBancaire.setCodeEtablissement(null);
        testExceptionCodeEtablissement(guichetBancaire);

        guichetBancaire.setCodeEtablissement("");
        testExceptionCodeEtablissement(guichetBancaire);

        guichetBancaire.setCodeEtablissement(" ");
        testExceptionCodeEtablissement(guichetBancaire);

        guichetBancaire.setCodeEtablissement("test");
        guichetBancaire.verifierCodeEtablissement();
    }
    
    /**
     * Test de 
     * {@link fr.gouv.finances.lombok.bancaire.bean.GuichetBancaire#verifierCodeGuichet()}.
     */
    @Test
    public final void testVerifierCodeGuichet()
    {
        // Initialisation d'un guichet bancaire
        GuichetBancaire guichetBancaire = new GuichetBancaire();

        // Exécution des tests et vérifications
        guichetBancaire.setCodeGuichet(null);
        testExceptionCodeGuichet(guichetBancaire);

        guichetBancaire.setCodeGuichet("");
        testExceptionCodeGuichet(guichetBancaire);

        guichetBancaire.setCodeGuichet(" ");
        testExceptionCodeGuichet(guichetBancaire);

        guichetBancaire.setCodeGuichet("test");
        guichetBancaire.verifierCodeGuichet();
    }

    /**
     * Test de 
     * {@link fr.gouv.finances.lombok.bancaire.bean.GuichetBancaire#verifierLibelleAbrevDeDomiciliation()}.
     */
    @Test
    public final void testVerifierverifierLibelleAbrevDeDomiciliation()
    {
        // Initialisation d'un guichet bancaire
        GuichetBancaire guichetBancaire = new GuichetBancaire();
        assertFalse(guichetBancaire.isEstPerime());

        // Exécution des tests et vérifications
        guichetBancaire.setLibelleAbrevDeDomiciliation(null);
        testExceptionLibelleAbrevDeDomiciliation(guichetBancaire);

        guichetBancaire.setLibelleAbrevDeDomiciliation("");
        testExceptionLibelleAbrevDeDomiciliation(guichetBancaire);

        guichetBancaire.setLibelleAbrevDeDomiciliation(" ");
        testExceptionLibelleAbrevDeDomiciliation(guichetBancaire);

        guichetBancaire.setLibelleAbrevDeDomiciliation("test");
        guichetBancaire.verifierLibelleAbrevDeDomiciliation();
    }
    
    /**
     * Méthode permettant de vérifier qu'une RegleGestionException est levée
     * sur l'exécution de la méthode verifierCodeEtablissement
     *
     * @param guichetBancaire guichet bancaire à tester
     */
    private final void testExceptionCodeEtablissement(GuichetBancaire guichetBancaire)
    {
        try
        {
            guichetBancaire.verifierCodeEtablissement();
            Assert.fail("Une exception RegleGestionException aurait du être levée");
        }
        catch (RegleGestionException rge)
        {
            LOGGER.debug("Une exception RegleGestionException a bien été levée");
        }
    }
    
    /**
     * Méthode permettant de vérifier qu'une RegleGestionException est levée
     * sur l'exécution de la méthode verifierCodeGuichet
     *
     * @param guichetBancaire guichet bancaire à tester
     */
    private final void testExceptionCodeGuichet(GuichetBancaire guichetBancaire)
    {
        VerificationExecution.verifierException(RegleGestionException.class
            , () -> guichetBancaire.verifierCodeGuichet());
    }

    /**
     * Méthode permettant de vérifier qu'une RegleGestionException est levé
     * sur l'exécution de la méthode verifierLibelleAbrevDeDomiciliation
     *
     * @param guichetBancaire guichet bancaire à tester
     */
    private final void testExceptionLibelleAbrevDeDomiciliation(GuichetBancaire guichetBancaire)
    {
        VerificationExecution.verifierException(RegleGestionException.class
            , () -> guichetBancaire.verifierLibelleAbrevDeDomiciliation());
    }

    /**
     * Méthode permettant de tester l'égalité du POJO.
     */
    @Test
    public final void testEqualsHashCode()
    {
        EqualsVerifier.forClass(getParameterizedType())
            .suppress(Warning.NONFINAL_FIELDS, Warning.STRICT_INHERITANCE)
            .withPrefabValues(Timestamp.class, new Timestamp(Calendar.getInstance().getTimeInMillis()), new Timestamp(2015))
            .withOnlyTheseFields("estPerime", "codeGuichet", "codeEtablissement")
            .usingGetClass()
            .verify();
    }

    @Test
    public final void testToString()
    {
        // Initialisation d'un guichet bancaire
        GuichetBancaire guichetBancaire = new GuichetBancaire();
        guichetBancaire.setCodeGuichet("1");
        guichetBancaire.setCodeEtablissement("2");
        guichetBancaire.setCodePostal("3");

        // Vérification
        assertEquals("1|2", guichetBancaire.toString());
    }
}