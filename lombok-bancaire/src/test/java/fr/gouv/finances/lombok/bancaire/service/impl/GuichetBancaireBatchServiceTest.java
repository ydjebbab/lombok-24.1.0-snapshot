/*
 * Copyright (c) 2016 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.bancaire.service.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.CleanupFailureDataAccessException;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.dao.DataRetrievalFailureException;
import org.springframework.dao.OptimisticLockingFailureException;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.gouv.finances.lombok.bancaire.bean.GuichetBancaire;
import fr.gouv.finances.lombok.bancaire.dao.GuichetBancaireDao;
import fr.gouv.finances.lombok.bancaire.service.GuichetBancaireBatchService;
import fr.gouv.finances.lombok.test.controle.VerificationExecution;
import fr.gouv.finances.lombok.test.pojo.ReflectionUtils;
import fr.gouv.finances.lombok.util.exception.ExploitationException;
import fr.gouv.finances.lombok.util.exception.IngerableException;
import fr.gouv.finances.lombok.util.exception.VerrouillageOptimisteModificationException;
import fr.gouv.finances.lombok.util.exception.VerrouillageOptimisteSuppressionException;

/**
 * Tests unitaires automatisés des services métier du batch d'alimentation des guichets bancaires.
 *
 * @author chouard
 * @author Christophe Breheret-Girardin
 */
@ContextConfiguration({"classpath:/conf/applicationContext-bancaire-service-test.xml"
    , "classpath*:/conf/applicationContext-core-transaction-test.xml"
    , "classpath*:/conf/applicationContext-commun-transaction.xml"})
@ActiveProfiles(profiles = {"bancaire", "hibernate"})
@RunWith(SpringJUnit4ClassRunner.class)
public class GuichetBancaireBatchServiceTest extends GuichetBancaireServiceTest
{
    /** Initialisation de la journalisation. */
    protected static final Logger LOGGER = LoggerFactory.getLogger(GuichetBancaireBatchServiceTest.class);

    /** Service métier de gestion du guichet bancaire à tester. */
    @Autowired
    private GuichetBancaireBatchService guichetBancaireService;

    /** Bouchon du DAO de gestion des données du guichet bancaire. */
    @Mock
    private GuichetBancaireDao guichetbancairedao;

    /** Valeur permettant de savoir qu'une demande d'écrasement est effectuée. */
    private String valeurEcrasement = null;

    /**
     * Constructeur.
     */
    public GuichetBancaireBatchServiceTest()
    {
        super();
    }

    /**
     * Initialisation des tests.
     */
    @Before
    public void setupMock()
    {
        // Initialisation d'un bouchon du DAO
        MockitoAnnotations.initMocks(this);
        guichetBancaireService.setGuichetbancairedao(guichetbancairedao);

        // Réinitialisation afin que l'inspecteur n'additionne pas les appels des différents tests
        Mockito.reset(guichetbancairedao);

        // Récupération de la valeur d'écrasement à partir de la constante utilisée dans le service
        valeurEcrasement = ReflectionUtils.getValeur(guichetBancaireService, "VALEUR_ECRASEMENT", String.class);
    }

    /**
     * Test de l'injection du service.
     */
    @Test
    public final void testGuichetBancaireService()
    {
        assertNotNull(guichetBancaireService);
    }

    /**
     * Test de la méthode
     * {@link fr.gouv.finances.lombok.bancaire.service.GuichetBancaireBatchService#creerLesGuichetsBancaires()}.
     */
    @Test
    public final void testRechercherTousGuichetsBancairesSansParametres()
    {
        testRechercherTousGuichetsBancaires(5, null, null);
    }

    /**
     * Test de la méthode
     * {@link fr.gouv.finances.lombok.bancaire.service.GuichetBancaireBatchService#creerLesGuichetsBancaires()}.
     */
    @Test
    public final void testRechercherTousGuichetsBancairesSansModeTraitement()
    {
        testRechercherTousGuichetsBancaires(5, new Date(), null);
    }

    /**
     * Test de la méthode
     * {@link fr.gouv.finances.lombok.bancaire.service.GuichetBancaireBatchService#creerLesGuichetsBancaires()}.
     */
    @Test
    public final void testRechercherTousGuichetsBancairesSansEcrasement()
    {
        testRechercherTousGuichetsBancaires(5, new Date(), "xxx");
    }

    /**
     * Test de la méthode
     * {@link fr.gouv.finances.lombok.bancaire.service.GuichetBancaireBatchService#creerLesGuichetsBancaires()}.
     */
    @Test
    public final void testRechercherTousGuichetsBancairesSansHorodatage()
    {
        testRechercherTousGuichetsBancaires(5, null, "xxx");
    }

    /**
     * Test de la méthode
     * {@link fr.gouv.finances.lombok.bancaire.service.GuichetBancaireBatchService#creerLesGuichetsBancaires()}.
     */
    @Test
    public final void testRechercherTousGuichetsBancairesSansHorodatageAvecEcrasement()
    {
        testRechercherTousGuichetsBancaires(5, null, valeurEcrasement);
    }

    /**
     * Test de la méthode
     * {@link fr.gouv.finances.lombok.bancaire.service.GuichetBancaireBatchService#creerLesGuichetsBancaires()}.
     */
    @Test
    public final void testRechercherTousGuichetsBancairesAvecEcrasement()
    {
        testRechercherTousGuichetsBancaires(5, new Date(), valeurEcrasement);
    }

    /**
     * Test de la méthode
     * {@link fr.gouv.finances.lombok.bancaire.service.GuichetBancaireBatchService#creerLesGuichetsBancaires()}.
     */
    @Test
    public final void testRechercherTousGuichetsBancairesEcrasementAvecFlush()
    {
        testRechercherTousGuichetsBancaires(21, new Date(), valeurEcrasement);
    }

    /**
     * Test de la méthode
     * {@link fr.gouv.finances.lombok.bancaire.service.GuichetBancaireBatchService#creerLesGuichetsBancaires()}.
     */
    @Test
    public final void testRechercherTousGuichetsBancairesSansEcrasementAvecFlush()
    {
        testRechercherTousGuichetsBancaires(21, new Date(), "");
    }

    /**
     * Test de la méthode
     * {@link fr.gouv.finances.lombok.bancaire.service.GuichetBancaireBatchService#creerLesGuichetsBancaires()}.
     */
    @Test
    public final void testRechercherTousGuichetsBancairesIngerableException()
    {
        testExceptionRechercherTousGuichetsBancaires(21, new Date(), valeurEcrasement
          , new IngerableException("Boum"), IngerableException.class);
    }

    /**
     * Test de la méthode
     * {@link fr.gouv.finances.lombok.bancaire.service.GuichetBancaireBatchService#creerLesGuichetsBancaires()}.
     */
    @Test
    public final void testRechercherTousGuichetsBancairesVerrouillageOptimisteModificationException()
    {
        testExceptionRechercherTousGuichetsBancaires(21, new Date(), valeurEcrasement
          , new OptimisticLockingFailureException("Boum"), VerrouillageOptimisteModificationException.class);
    }

    /**
     * Test de la méthode
     * {@link fr.gouv.finances.lombok.bancaire.service.GuichetBancaireBatchService#creerLesGuichetsBancaires()}.
     */
    @Test
    public final void testRechercherTousGuichetsBancairesVerrouillageOptimisteSuppressionException()
    {
        testExceptionRechercherTousGuichetsBancaires(21, new Date(), valeurEcrasement
          , new DataRetrievalFailureException("Boum"), VerrouillageOptimisteSuppressionException.class);
    }

    /**
     * Test de la méthode
     * {@link fr.gouv.finances.lombok.bancaire.service.GuichetBancaireBatchService#creerLesGuichetsBancaires()}.
     */
    @Test
    public final void testRechercherTousGuichetsBancairesDataAccessResourceFailureException()
    {
        testExceptionRechercherTousGuichetsBancaires(21, new Date(), valeurEcrasement
          , new DataAccessResourceFailureException("Boum"), ExploitationException.class);
    }

    /**
     * Test de la méthode
     * {@link fr.gouv.finances.lombok.bancaire.service.GuichetBancaireBatchService#creerLesGuichetsBancaires()}.
     */
    @Test
    public final void testRechercherTousGuichetsBancairesCleanupFailureDataAccessException()
    {
        testExceptionRechercherTousGuichetsBancaires(21, new Date(), valeurEcrasement
          , new CleanupFailureDataAccessException("Boum", new IngerableException("Boum")), ExploitationException.class);
    }

    /**
     * Méthode permettant de tester la création de guichets bancaires en mode batch.<br/><br/>
     *
     * Jeux d'essai :<br/><br/>
     *
     * Guichet   A (à traiter)     B (en BDD)<br/>
     *  code       valeur          valeur<br/>
     *     1 -> xxxxxxxx + 1    xxxxxxxx + 101<br/>
     *     2 -> xxxxxxxx + 2    xxxxxxxx + 102<br/>
     *     3 -> xxxxxxxx + 3    xxxxxxxx + 103<br/>
     *     4 -> xxxxxxxx + 4     n'existe pas<br/>
     *     5 -> null            xxxxxxxx + 105<br/><br/>
     *
     * Résultats attendus :<br/><br/>
     *
     * Si écrasement :<br/>
     *   - suppression guichet B et création guichet A<br/>
     * Sinon<br/>
     *   - si guichet B trouvé :<br/>
     *     + si valeurs non nulles -> remplacées par guichet A<br/>
     *     + sinon -> valeur du guichet B<br/>
     *   - si guichet B non trouvé :<br/>
     *     + création guichet A<br/><br/>
     *  
     * Remarque : dans les jeux d'essai ci-dessus, pour passer du guichet A au guichet B, un indice de variation,
     * ayant la valeur 100, est utilisé.<br/>
     * Ceci correspond à la variable "indiceVariation" utilisée dans les méthodes de la classe.
     *
     * @param nbreGuichets nombre de guichets à tester
     * @param horodatage horodatage à prendre en compte
     * @param modeTraitement mode de traitement à prendre en compte (écrasement ou non)
     */
    private void testRechercherTousGuichetsBancaires(int nbreGuichets, Date horodatage
        , String modeTraitement)
    {
        // Génération d'une liste de guichets bancaires
        List<GuichetBancaire> guichetsBancaires = getGuichetsBancaires(nbreGuichets);

        if (!isEcrasement(modeTraitement))
        {
            // Initialisation des comportements
            // (chaque guichet bancaire existe déjà en base, avec une variation de valeur de 100)
            guichetsBancaires.stream().filter(g -> !g.getCodeGuichet().equals("4"))
                .forEach(g ->  when(guichetbancairedao
                    .findGuichetBancaireNonPerimeParCodeGuichetEtCodeEtablissement(
                        g.getCodeGuichet(), g.getCodeEtablissement()))
                        .thenReturn(getGuichetBancaire(Integer.parseInt(g.getCodeGuichet()), 100)));
        }

        // Initialisation d'un inspecteur de l'argument passé à la méthode saveObject du DAO,
        // qui permettra de savoir si celle-ci est appelée le bon nombre de fois,
        // et si les informations lui sont passées au travers de cet argument sont correctes
        ArgumentCaptor<GuichetBancaire> captureGuichet = ArgumentCaptor.forClass(GuichetBancaire.class);

        // Exécutions du service et vérifications
        guichetBancaireService.creerLesGuichetsBancaires(guichetsBancaires, horodatage, modeTraitement);
        verifications(nbreGuichets, captureGuichet, horodatage, isEcrasement(modeTraitement));
    }

    /**
     * Méthode permettant de tester les exceptions.
     *
     * @param nbreGuichets nombre de guichets à tester
     * @param horodatage horodatage à prendre en compte
     * @param modeTraitement mode de traitement à prendre en compte
     * @param exception erreur à lever
     * @param typeException type d'exception remonté par le service
     */
    private void testExceptionRechercherTousGuichetsBancaires(int nbreGuichets
        , Date horodatage, String modeTraitement
        , Exception exception, Class<? extends Exception> typeException)
    {
        // Initialisation du comportement levant une exception
        Mockito.doThrow(exception).when(guichetbancairedao).saveObject(Mockito.any());

        // Exécutions du service
        VerificationExecution.verifierException(typeException, () -> guichetBancaireService
            .creerLesGuichetsBancaires(getGuichetsBancaires(nbreGuichets), horodatage, modeTraitement));
    }

    /**
     * Méthode permettant d'effectuer les vérifications du traitement.
     *
     * @param nbreGuichets nombre de guichets impactés
     * @param captureGuichet inspecteur de l'argument GuichetBancaire
     * @param horodatage horodatage utilisé
     * @param isEcrasement est-ce qu'un écrasement a été demandé ?
     */
    private void verifications(int nbreGuichets,  ArgumentCaptor<GuichetBancaire> captureGuichet
        , Date horodatage, boolean isEcrasement)
    {
        // Récupération du nombre d'appel aux différentes méthodes du DAO
        int nbreAppelSuppression = 0;
        int nbreAppelNonPerime = 0;
        int nbreAppelMarquePerimes = 0;
        if (isEcrasement)
        {
            nbreAppelSuppression = 1;
        }
        else
        {
            nbreAppelNonPerime = nbreGuichets;
            nbreAppelMarquePerimes = 1;
        }
        int nbreFlush = nbreGuichets / ReflectionUtils
            .getValeur(guichetBancaireService, "NBRE_GUICHET_FLUSH", Integer.TYPE);

        // Vérification du nombre d'appel à la méthode "deleteTousLesGuichetsBancaires"
        Mockito.verify(
            guichetbancairedao, Mockito.times(nbreAppelSuppression)).deleteTousLesGuichetsBancaires();

        // Vérification du nombre d'appel à la méthode "findGuichetBancaireNonPerimeParCodeGuichetEtCodeEtablissement"
        Mockito.verify(
            guichetbancairedao, Mockito.times(nbreAppelNonPerime))
                .findGuichetBancaireNonPerimeParCodeGuichetEtCodeEtablissement(Mockito.anyString(), Mockito.anyString());

        // Vérification du nombre d'appel à la méthode "marqueLesGuichetsBancairesPerimesADate"
        Mockito.verify(guichetbancairedao
            , Mockito.times(nbreAppelMarquePerimes)).marqueLesGuichetsBancairesPerimesADate(horodatage);

        // Vérification du nombre d'appel aux méthodes "flush" et "clearPersistenceContext"
        Mockito.verify(guichetbancairedao, Mockito.times(nbreFlush)).flush();
        Mockito.verify(guichetbancairedao, Mockito.times(nbreFlush)).clearPersistenceContext();

        // Vérification du nombre d'appel à la méthode "saveObject"
        Mockito.verify(guichetbancairedao, Mockito.times(nbreGuichets)).saveObject(captureGuichet.capture());
        // Pour cette dernière, vérification du contenu passé en paramètre
        verifierContenuGuichetBancaire(captureGuichet.getAllValues(), horodatage, isEcrasement, nbreGuichets);
    }

    /**
     * Méthode permettant de vérifier les données d'une liste de guichets bancaires.
     *
     * @param guichetsBancaires liste des guichets bancaires à vérifier
     * @param horodatage horodatage utilisé
     * @param isEcrasement un écrasement a-t-il été demandé ?
     * @param nbreGuichets nombre de guichets traités
     */
    private void verifierContenuGuichetBancaire(List<GuichetBancaire> guichetsBancaires, Date horodatage
        , boolean isEcrasement, int nbreGuichets)
    {
        // Vérification du nombre de guichets traités
        assertEquals(nbreGuichets, guichetsBancaires.size());

        // Vérification des données de tous les guichets bancaires
        guichetsBancaires.stream().parallel().forEach(g ->
        {
            // Vérification des données des guichets bancaires sauvegardés en base
            controlerDonneesSaveObject(g, horodatage, isEcrasement);
            // Vérification si les guichets bancaires sont en voie de péremption ou non
            isEstEnVoieDePeremption(g);
        });
    }

    /**
     * Méthode permettant de vérifier les données d'un guichet bancaire.
     *
     * @param guichetBancaire guichet bancaire à vérifier
     * @param horodatage horodatage utilisé
     * @param isEcrasement un écrasement a-t-il été demandé ?
     * @param nbreGuichets nombre de guichets traités
     */
    private void controlerDonneesSaveObject(GuichetBancaire guichetBancaire
        , Date horodatage, boolean isEcrasement)
    {
        verifierDateDerniereMiseAJour(guichetBancaire, horodatage);
        controlerDonneesSaveObject(guichetBancaire, isEcrasement);
    }

    /**
     * Méthode permettant de vérifier les valeurs des propriétés d'un guichet bancaire.
     *
     * @param guichetBancaire guichet bancaire à vérifier
     * @param isEcrasement un écrasement a-t-il été demandé ?
     */
    private void controlerDonneesSaveObject(GuichetBancaire guichetBancaire, boolean isEcrasement)
    {
        String codeGuichet = guichetBancaire.getCodeGuichet();
        switch (codeGuichet)
        {
            case "5" :
                if (!isEcrasement)
                {
                    // Comme toutes les valeurs du guichet 5 à traiter sont nulles, les valeurs finales
                    // sont celles en BDD (avec donc une variation de 100)
                    verifierValeurs(guichetBancaire, 100);
                }
                else
                {
                    // Si écrasement, les valeurs du guichets 5 à traiter sont nulles
                    verifierNull(guichetBancaire);
                }
                break;
            case "1" :
                // Le guichet 1 n'a pas de nouveaux codes guichet et établissement
                assertNull("Nouv code guichet du guichet " + codeGuichet, guichetBancaire.getNouveauCodeGuichet());
                assertNull("Nouv code établissement du guichet " + codeGuichet
                    , guichetBancaire.getNouveauCodeEtablissement());

                // Vérification des valeurs (variation de 0 car correspondant au guichet à traiter)
                verifierValeurs(guichetBancaire, 0);
                break;
            default :
                // Les autres guichets ont des nouveaux codes guichet et établissement
                assertEquals("Nouv code guichet du guichet " + codeGuichet
                    , SUFFIXE_NOUVEAU_GUICHET + codeGuichet, guichetBancaire.getNouveauCodeGuichet());
                assertEquals("Nouv code établissement du guichet " + codeGuichet
                    , SUFFIXE_NOUVEL_ETABLISSEMENT + codeGuichet, guichetBancaire.getNouveauCodeEtablissement());

                // Vérification des valeurs (variation de 0 car correspondant au guichet à traiter)
                verifierValeurs(guichetBancaire, 0);
        }
    }

    /**
     * Méthode permettant de vérifier les valeurs d'un guichet bancaire.
     *
     * @param guichetBancaire guichetBancaire guichet bancaire à vérifier
     * @param indiceVariation indice de variation apporté aux valeurs
     */
    private void verifierValeurs(GuichetBancaire guichetBancaire, int indiceVariation)
    {
        String codeGuichet = guichetBancaire.getCodeGuichet();
        int valeurVariation = Integer.parseInt(codeGuichet) + indiceVariation;

        assertTrue("Code guichet " + codeGuichet, codeGuichet.length() <= 2);
        assertEquals("Code établissement du guichet " + codeGuichet
            , SUFFIXE_CODE_ETABLISSEMENT + valeurVariation, guichetBancaire.getCodeEtablissement());
        assertEquals("Adresse du guichet " + codeGuichet
            , SUFFIXE_ADRESSE + valeurVariation, guichetBancaire.getAdresseGuichet());
        assertEquals("Code postal du guichet " + codeGuichet
            , SUFFIXE_CODE_POSTAL + valeurVariation, guichetBancaire.getCodePostal());
        assertEquals("Année du guichet " + codeGuichet
            , SUFFIXE_ANNEE + valeurVariation, guichetBancaire.getAnneeDeModification());
        assertEquals("Mois du guichet" + codeGuichet
            , SUFFIXE_MOIS + valeurVariation, guichetBancaire.getMoisDeModification());
        assertEquals("Code enregistrement du guichet" + codeGuichet
            , SUFFIXE_CODE_ENREGISTREMENT + valeurVariation, guichetBancaire.getCodeEnregistrement());
        assertEquals("Dénomination abrégée du guichet " + codeGuichet
            , SUFFIXE_DENOMINATION_ABREGEE + valeurVariation, guichetBancaire.getDenominationAbregee());
        assertEquals("Dénomination complète du guichet " + codeGuichet
            , SUFFIXE_DENOMINATION_COMPLETE + valeurVariation, guichetBancaire.getDenominationComplete());
        assertEquals("Code règlement des crédits du guichet " + codeGuichet
            , SUFFIXE_CREDIT + valeurVariation, guichetBancaire.getCodeReglementDesCredits());
        assertEquals("Adresse abréviation du guichet " + codeGuichet
            , SUFFIXE_ADRESSE_ABREV + valeurVariation, guichetBancaire.getLibelleAbrevDeDomiciliation());
        assertEquals("Nom du guichet " + codeGuichet, SUFFIXE_NOM + valeurVariation
            , guichetBancaire.getNomGuichet());
        assertEquals("Ville du guichet " + codeGuichet, SUFFIXE_VILLE + valeurVariation
            , guichetBancaire.getVilleCodePostal());
    }

    /**
     * Méthode permettant de vérifier que les valeurs d'un guichet bancaire ne sont pas renseignés.
     *
     * @param guichetBancaire guichetBancaire guichet bancaire à vérifier
     */
    private void verifierNull(GuichetBancaire guichetBancaire)
    {
        String codeGuichet = guichetBancaire.getCodeGuichet();

        assertNull("Code établissement du guichet " + codeGuichet, guichetBancaire.getCodeEtablissement());
        assertNull("Adresse du guichet " + codeGuichet, guichetBancaire.getAdresseGuichet());
        assertNull("Code postal du guichet " + codeGuichet, guichetBancaire.getCodePostal());
        assertNull("Année du guichet " + codeGuichet, guichetBancaire.getAnneeDeModification());
        assertNull("Mois du guichet" + codeGuichet, guichetBancaire.getMoisDeModification());
        assertNull("Code enregistrement du guichet" + codeGuichet, guichetBancaire.getCodeEnregistrement());
        assertNull("Dénomination abrégée du guichet " + codeGuichet, guichetBancaire.getDenominationAbregee());
        assertNull("Dénomination complète du guichet " + codeGuichet
            , guichetBancaire.getDenominationComplete());
        assertNull("Code règlement des crédits du guichet " + codeGuichet
            , guichetBancaire.getCodeReglementDesCredits());
        assertNull("Adresse abréviation du guichet " + codeGuichet
            , guichetBancaire.getLibelleAbrevDeDomiciliation());
        assertNull("Nom du guichet " + codeGuichet, guichetBancaire.getNomGuichet());
        assertNull("Ville du guichet " + codeGuichet, guichetBancaire.getVilleCodePostal());
    }

    /**
     * Méthode permettant de vérifier la date de dernière mise à jour d'un guichet.
     *
     * @param guichetBancaire guichet bancaire à vérifier
     * @param horodatage horodatage servant à mettre à jour la date
     */
    private void verifierDateDerniereMiseAJour(GuichetBancaire guichetBancaire, Date horodatage)
    {
        if (horodatage != null)
        {
            assertTrue(horodatage.getTime() == guichetBancaire.getDateDerniereMiseAJour().getTime());
        }
        else
        {
            assertNotNull(guichetBancaire.getDateDerniereMiseAJour());
        }
    }

    /**
     * Méthode permettant de vérifier l'attribut permettant de savoir si le guichet est en voie de péremption.
     *
     * @param guichetBancaire guichet bancaire à vérifier
     */
    private void isEstEnVoieDePeremption(GuichetBancaire guichetBancaire)
    {
        switch (guichetBancaire.getCodeGuichet())
        {
            case "1" :
            case "5" :
                assertFalse("Le guichet " + guichetBancaire.getCodeGuichet()
                    + " ne devrait pas être en voie de péremption"
                    , guichetBancaire.isEstEnVoieDePeremption());
                break;
            default :
                assertTrue("Le guichet '" + guichetBancaire.getCodeGuichet()
                    + "' devrait être en voie de péremption", guichetBancaire.isEstEnVoieDePeremption());
        }
    }

    /**
     * Méthode permettant de savoir un écrasement à été demandé ou non
     * (permettant de centraliser l'utilisation de la chaine de caractères, puisqu'une énumération n'a pas
     * été utilisée).
     *
     * @param modeTraitement mode de traitement demandé
     */
    private boolean isEcrasement(String modeTraitement)
    {
        return valeurEcrasement.equals(modeTraitement);
    }
}