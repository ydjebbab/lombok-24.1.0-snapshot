/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : TraitementChargementGuichetsEtEtablissementsTabankTest.java
 *
 */
package fr.gouv.finances.lombok.test.bancaire;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import fr.gouv.finances.lombok.bancaire.batch.TraitementChargementGuichetsEtEtablissementsTabankImpl;
import org.junit.Assert;

/**
 * Class TraitementChargementGuichetsEtEtablissementsTabankTest.
 * 
 * @version $Revision: 1.2 $ Date: 15 déc. 2009
 */
// @RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(inheritLocations = true, locations = "classpath:fr/gouv/finances/lombok/test/bancaire/traitementchargementguichetsetetablissementstabankContext-service.xml")
@TransactionConfiguration(defaultRollback = false)
@Transactional
public class TraitementChargementGuichetsEtEtablissementsTabankTest
{
    @Autowired
    ApplicationContext batchContext;

    /** rollback. */
    protected final boolean rollback = false;

    /**
     * methode Test traiter batch : --.
     */
    // @Test désactivation du test d'intégration qui ne fonctionne pas pour l'instant
    public void testTraiterBatch()
    {
        // this.endTransaction();
        int statuscode = 0;
        TraitementChargementGuichetsEtEtablissementsTabankImpl sbc =
            (TraitementChargementGuichetsEtEtablissementsTabankImpl) batchContext
                .getBean("traitementchargementguichetsetetablissementstabankimpl");
        statuscode = sbc.demarrer();
        Assert.assertEquals(statuscode, 0);
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @return string
     * @see fr.gouv.finances.lombok.test.BaseServiceTestCase#ajouterConfigLocationSpecifique()
     */

    /*
     * protected String ajouterConfigLocationSpecifique() { return
     * "classpath:fr/gouv/finances/lombok/test/bancaire/traitementchargementguichetsetetablissementstabankContext-service.xml";
     * }
     */
}