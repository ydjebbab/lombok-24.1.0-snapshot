/*
 * Copyright (c) 2016 DGFiP - Tous droits réservés
 * 
 */
package fr.gouv.finances.lombok.bancaire.service.impl;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import fr.gouv.finances.lombok.bancaire.bean.GuichetBancaire;

/**
 * Socle des tests unitaires automatisés des services métier du guichet bancaire.
 *
 * @author Christophe Breheret-Girardin
 */
abstract class GuichetBancaireServiceTest
{
    /** Code établissement des guichets bancaires */
    protected static final String CODE_ETABLISSEMENT = "code établissement";
    protected static final String VILLE = "Paris";
    /** Ville des guichets bancaires */
    protected static final String SUFFIXE_VILLE = "Paris ";
    protected static final String SUFFIXE_CODE_ETABLISSEMENT = "Code établissement";
    /** Code postal des guichets bancaires */
    protected static final String SUFFIXE_CODE_POSTAL = "7500";
    protected static final String SUFFIXE_ANNEE = "200";
    protected static final String SUFFIXE_MOIS = "Mois ";
    protected static final String SUFFIXE_ADRESSE = "Adresse ";
    protected static final String SUFFIXE_CODE_ENREGISTREMENT = "code_enregistrement";
    protected static final String SUFFIXE_DENOMINATION_ABREGEE = "Dénom abreg ";
    protected static final String SUFFIXE_DENOMINATION_COMPLETE = "dénomination complète";
    protected static final String DENOMINATION_COMPLETE = SUFFIXE_DENOMINATION_COMPLETE;
    protected static final String SUFFIXE_CREDIT = "Crédit ";
    protected static final String SUFFIXE_ADRESSE_ABREV = "Adresse abrev";
    protected static final String SUFFIXE_NOM = "Nom guichet ";
    protected static final String SUFFIXE_NOUVEAU_GUICHET = "Nouv guichet ";
    protected static final String SUFFIXE_NOUVEL_ETABLISSEMENT = "Nouv établissement ";

    /**
     * Constructeur.
     */
    public GuichetBancaireServiceTest()
    {
        super();
    }

    /**
     * Méthode permettant de générer une liste de guichets bancaires.
     *
     * @return la liste de guichets bancaires générée
     */
    protected static List<GuichetBancaire> getGuichetsBancaires()
    {
        return getGuichetsBancaires(5);
    }

    /**
     * Méthode permettant de générer une liste de guichets bancaires.
     *
     * @param nbreGuichets nombre de guichets bancaires à générer
     * @return la liste de guichets bancaires générée
     */
    protected static List<GuichetBancaire> getGuichetsBancaires(int nbreGuichets)
    {
        // Génération d'une liste de guichets bancaires de code 1 à 5
        return IntStream.rangeClosed(1, nbreGuichets)
            .mapToObj(GuichetBancaireServiceTest::getGuichetBancaire).collect(Collectors.toList());
    }

    /**
     * Méthode permettant de générer un guichet bancaire
     * @param indice indice permettant d'obtenir des guichets bancaires différents.
     *
     * @return le guichet bancaire généré
     */
    protected static GuichetBancaire getGuichetBancaire(int indice)
    {
        return getGuichetBancaire(indice, 0);
    }

    /**
     * Méthode permettant de générer un guichet bancaire.
     * @param indice indice permettant d'obtenir des guichets bancaires différents
     * @param indiceVariation permet de faire varier les valeurs des données : indice + (100 * indiceVariation)
     *
     * @return le guichet bancaire généré
     */
    protected static GuichetBancaire getGuichetBancaire(int indice, int indiceVariation)
    {
        GuichetBancaire bancaire = new GuichetBancaire();
        bancaire.setCodeGuichet(String.valueOf(indice));

        // Calcul de la valeur de la variation à apporter
        int valeurVariation = indice + indiceVariation;

        // Le guichet 5 n'a aucune valeur sur une variation de 0
        if (indice != 5 || indiceVariation != 0)
        {
            if (indice == 3)
            {
                // Le guichet 3 est le seul a subir une variation sur son code
                bancaire.setCodeGuichet(String.valueOf(valeurVariation));
            }
            bancaire.setCodeEtablissement(SUFFIXE_CODE_ETABLISSEMENT + valeurVariation);
            bancaire.setCodePostal(SUFFIXE_CODE_POSTAL + valeurVariation);
            bancaire.setDenominationComplete(SUFFIXE_DENOMINATION_COMPLETE + valeurVariation);
            bancaire.setAnneeDeModification(SUFFIXE_ANNEE + valeurVariation);
            bancaire.setCodeEnregistrement(SUFFIXE_CODE_ENREGISTREMENT + valeurVariation);
            bancaire.setAdresseGuichet(SUFFIXE_ADRESSE + valeurVariation);
            bancaire.setCodeReglementDesCredits(SUFFIXE_CREDIT + valeurVariation);
            bancaire.setDenominationAbregee(SUFFIXE_DENOMINATION_ABREGEE + valeurVariation);
            bancaire.setEstEnVoieDePeremption(false);
            bancaire.setLibelleAbrevDeDomiciliation(SUFFIXE_ADRESSE_ABREV + valeurVariation);
            bancaire.setMoisDeModification(SUFFIXE_MOIS + valeurVariation);
            bancaire.setNomGuichet(SUFFIXE_NOM + valeurVariation);
            bancaire.setVilleCodePostal(SUFFIXE_VILLE + valeurVariation);
            bancaire.setEstPerime(false);

            // Le guichet 1 n'a pas de nouveaux codes guichet et établissement
            // (et guichet 5 sur une variation de 0)
            if (indice != 1)
            {
                bancaire.setNouveauCodeGuichet(SUFFIXE_NOUVEAU_GUICHET + valeurVariation);
                bancaire.setNouveauCodeEtablissement(SUFFIXE_NOUVEL_ETABLISSEMENT + valeurVariation);
            }
        }

        return bancaire;
    }
}