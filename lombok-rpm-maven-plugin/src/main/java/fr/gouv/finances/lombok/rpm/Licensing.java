/*
 * Copyright (c) 2014 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.rpm;

/**
 * Donnees supplementaires du RPM
 * 
 * @author Eric Demorsy
 * Date: 27 juin 2014
 */
public class Licensing
{

    private String license = "Ministère de l'Économie et des Finances";

    private String vendor = "Ministère de l'Économie et des Finances";

    private String url = "http://www.economie.gouv.fr";

    private String group = "Misc";

    private String packager = "Ministère de l'Économie et des Finances";

    private String description = "";

    /**
     * Constructeur sans parametres
     *
     */
    public Licensing()
    {
        super();
    }

    /**
     * Methode d'acces a la license
     *
     * @return la license
     */
    public String getLicense()
    {
        return license;
    }

    /**
     * Methode d'affectation de la license
     *
     * @param license la license
     */
    public void setLicense(String license)
    {
        this.license = license;
    }

    /**
     * Methode d'acces a l'editeur
     *
     * @return l'editeur
     */
    public String getVendor()
    {
        return vendor;
    }

    /**
     * Methode d'affectation de l'editeur
     *
     * @param vendor l'editeur
     */
    public void setVendor(String vendor)
    {
        this.vendor = vendor;
    }

    /**
     * Methode d'acces a l'URL du projet
     *
     * @return l'URL du projet
     */
    public String getUrl()
    {
        return url;
    }

    /**
     * Methode d'affectation de l'URL du projet
     *
     * @param url l'URL du projet
     */
    public void setUrl(String url)
    {
        this.url = url;
    }

    /**
     * Methode d'acces au groupe d'applications
     *
     * @return le groupe d'applications du RPM
     */
    public String getGroup()
    {
        return group;
    }

    /**
     * Methode d'affectation du groupe applicatif
     *
     * @param group le groupe d'applications du RPM
     */
    public void setGroup(String group)
    {
        this.group = group;
    }

    /**
     * Methode d'acces a l'empaqueteur
     *
     * @return l'empaqueteur
     */
    public String getPackager()
    {
        return packager;
    }

    /**
     * Methode d'affectation de l'empaqueteur
     *
     * @param packager l'empaqueteur
     */
    public void setPackager(String packager)
    {
        this.packager = packager;
    }

    /**
     * Methode d'acces a la description
     *
     * @return la description du contenu du paquet
     */
    public String getDescription()
    {
        return description;
    }

    /**
     * Methode d'affectation de la description
     *
     * @param description la description du contenu du paquet
     */
    public void setDescription(String description)
    {
        this.description = description;
    }

}
