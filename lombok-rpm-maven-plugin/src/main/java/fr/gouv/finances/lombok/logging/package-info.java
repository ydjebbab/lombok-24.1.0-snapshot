/*
 * Copyright (c) 2014 DGFiP - Tous droits réservés
 * 
 * Classes-outil de journalisation
 * 
 * @author Eric Demorsy
 */
package fr.gouv.finances.lombok.logging;