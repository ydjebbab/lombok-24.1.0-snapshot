/*
 * Copyright (c) 2014 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.rpm;

import java.io.File;
import java.io.IOException;
import java.net.UnknownHostException;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.logging.Level;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.BasicConfigurator;
import org.apache.maven.artifact.Artifact;
import org.apache.maven.artifact.factory.ArtifactFactory;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.project.MavenProject;
import org.apache.maven.project.MavenProjectHelper;
import org.apache.maven.shared.model.fileset.util.FileSetManager;
import org.redline_rpm.Builder;
import org.redline_rpm.header.Architecture;
import org.redline_rpm.header.Flags;
import org.redline_rpm.header.Os;
import org.redline_rpm.header.RpmType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.gouv.finances.lombok.logging.MavenLoggerLog4jBridge;

/**
 * AbstractRPMBuilderMojo.java Classe generique pour les mojos destines a la creation de RPMs
 * 
 * @author edemorsy Date: 11 août 2014
 */
public abstract class AbstractRPMBuilderMojo extends AbstractMojo
{

    /** Initialisation de la journalisation. */
    protected static final Logger LOGGER = LoggerFactory.getLogger(AbstractRPMBuilderMojo.class);

    private static final Pattern DEPENDENCY_REGEXP = Pattern.compile("([^<>=]+)([<>=]+)\\ *([0-9\\.\\-(SNAPSHOT)]+)");

    private static final String[] DEFAULT_EXCLUDES = { "**/.svn/*" };
    
    /**
     * Nom du paquet
     * 
     * @parameter le nom du paquet
     */
    private String name;

    /**
     * Numero de version
     * 
     * @parameter expression = "${rpm.version}"
     */
    private String version;

    /**
     * Numero de release du paquet
     * 
     * @parameter expression = "${rpm.release}"
     */
    private String release;

    /**
     * Resume du paquet
     * 
     * @parameter default-value=""
     */
    private String summary;

    /**
     * Droits par defaut des fichiers Maven 2 ne sait pas gerer les valeur octales, on passe donc par une String
     * 
     * @parameter default-value="0640"
     */
    private String defaultfilemode;

    /**
     * Droits par defaut des repertoires Maven 2 ne sait pas gerer les valeur octales, on passe donc par une String
     * 
     * @parameter default-value="0750"
     */
    private String defaultdirmode;

    /**
     * Utilisateur possedant les fichiers installes par defaut
     * 
     * @parameter
     */
    private String defaultuser;

    /**
     * Groupe possedant les fichiers installes par defaut
     * 
     * @parameter
     */
    private String defaultgroup;

    /**
     * Informations supplementaires sur le RPM
     * 
     * @parameter
     */
    private final Licensing license = new Licensing();

    /**
     * Dossier de destination du rpm
     * 
     * @parameter expression = "${project.build.directory}/rpm"
     */
    private String outputdir;

    /**
     * Dossiers a creer vides
     * 
     * @parameter
     */
    private List<Emptyfolder> emptyfolders;

    /**
     * Arborescences a copier
     * 
     * @parameter
     */
    private List<Mapping> mappings;

    /**
     * Liens symboliques a creer
     * 
     * @parameter
     */
    private List<Link> links;

    /**
     * Dependances
     * 
     * @parameter
     */
    private List<String> requires;

    /**
     * Reference au projet Maven
     * 
     * @parameter default-value="${project}"
     */
    private MavenProject mavenProject;

    /**
     * Reference a l'artifact factory
     * 
     * @component
     */
    private ArtifactFactory artifactFactory;

    /**
     * Reference au project helper
     * 
     * @component
     */
    private MavenProjectHelper mavenProjectHelper;

    /**
     * Constructeur de la classe AbstractRPMBuilderMojo.java
     *
     */
    public AbstractRPMBuilderMojo()
    {
        super();
        
    }

    /**
     * Renvoie la valeur passee dans la configuration du Mojo en tant que "name"
     * 
     * @return le nom passe en parametre "name"
     */
    protected String getName()
    {
        return name;
    }

    /**
     * Instancie un builder de RPM avec les infos de base
     * 
     * @param fullname le nom complet du rpm a creer
     * @return le builder de RPM
     */
    private Builder initialiserBuilder(String fullname) throws MojoExecutionException
    {
        if (fullname == null) throw new MojoExecutionException("Le nom du RPM ne peut pas être vide");
        if (version == null) throw new MojoExecutionException("Le numéro de version du RPM ne peut pas être vide");
        if (release == null) throw new MojoExecutionException("Le numéro de release du RPM ne peut pas être vide");
        
        Builder rpmbuilder = new Builder();
        rpmbuilder.setPackage(fullname, version, release);
        rpmbuilder.setSummary(summary);
        rpmbuilder.setLicense(license.getLicense());
        rpmbuilder.setVendor(license.getVendor());
        rpmbuilder.setUrl(license.getUrl());
        rpmbuilder.setGroup(license.getGroup());
        rpmbuilder.setPackager(license.getPackager());
        rpmbuilder.setDescription(license.getDescription());
        rpmbuilder.setPlatform(Architecture.NOARCH, Os.LINUX);
        rpmbuilder.setType(RpmType.BINARY);
        rpmbuilder.setSourceRpm(fullname + "-" + version + "-" + release + ".src.rpm");
        try
        {
            rpmbuilder.setBuildHost(java.net.InetAddress.getLocalHost().getHostName());
        }
        catch (UnknownHostException exception)
        {
            LOGGER.warn("Exception  détectée", exception);
            rpmbuilder.setBuildHost("inconnu");
        }
        return rpmbuilder;
    }

    /**
     * Gestion des dépendances du RPM
     * 
     * @param rpmbuilder le rpmbuilder de la construction en cours
     * @throws MojoFailureException en cas de syntaxe incorrecte
     */
    public void addRequires(Builder rpmbuilder) throws MojoFailureException
    {

        String reqname = null;
        String reqconditionstr = null;
        String reqversion = null;
        int reqcondition = 0;
        Matcher reqMatcher = null;

        for (String require : requires)
        {
            
            // Analayse par expression reguliere de la configuration passee au mojo
            // Pour en deduire les drapeaux de dependance a utiliser
            
            reqMatcher = DEPENDENCY_REGEXP.matcher(require.trim());
            if (reqMatcher.matches())
            {
                reqname = reqMatcher.group(1).trim();
                reqconditionstr = reqMatcher.group(2);
                reqversion = reqMatcher.group(3).trim();
                reqcondition = 0;
                if (reqconditionstr.indexOf('>') > -1)
                {
                    reqcondition |= Flags.GREATER;
                }
                else if (reqconditionstr.indexOf('<') > -1)
                {
                    reqcondition |= Flags.LESS;
                }
                if (reqconditionstr.indexOf('=') > -1)
                {
                    reqcondition |= Flags.EQUAL;
                }
                rpmbuilder.addDependency(reqname, reqcondition, reqversion);
            }
            else
            {
                throw new MojoFailureException("Dependance invalide : " + require);
            }
        }
    }

    /**
     * Gestion des dossiers vides
     * 
     * @param rpmbuilder le rpmbuilder de la construction en cours
     * @param octalDefaultDirMode le mode octal des droits par defaut sur les dossiers
     * @throws NoSuchAlgorithmException en cas de directive incorrecte
     * @throws IOException en cas de probleme d'acces au fichier
     */
    public void addEmptyFolders(Builder rpmbuilder, int octalDefaultDirMode) throws NoSuchAlgorithmException, IOException
    {
        int dirmode;
        String user;
        String group;

        for (Emptyfolder dir : emptyfolders)
        {
            dirmode = dir.getDirmode();
            if (dirmode == 0)
            {
                dirmode = octalDefaultDirMode;
            }
            user = dir.getUser();
            if (user == null)
            {
                user = defaultuser;

            }
            group = dir.getGroup();
            if (group == null)
            {
                group = defaultgroup;
            }
            rpmbuilder.addDirectory(dir.getDestination(), dirmode, dir.getDirective(), user, group, true);
        }
    }

    /**
     * Gestion des liens symboliques
     * 
     * @param rpmbuilder le rpmbuilder de la construction en cours
     * @param octalDefaultFileMode le mode octal des droits par defaut sur les fichiers
     * @throws NoSuchAlgorithmException en cas de directive incorrecte
     * @throws IOException en cas de probleme d'acces au fichier
     */
    public void addLinks(Builder rpmbuilder, int octalDefaultFileMode) throws NoSuchAlgorithmException,
        IOException
    {
        int filemode;
        for (Link link : links)
        {
            filemode = link.getFilemode();
            if (filemode == 0)
            {
                filemode = octalDefaultFileMode;
            }
            rpmbuilder.addLink(link.getDestination(), link.getTarget(), filemode);
        }
    }

    /**
     * Gestion des mappings de dossiers
     * 
     * @param rpmbuilder le rpmbuilder de la construction en cours
     * @param sourceBaseParam le repertoire de base des elements source du mapping
     * @param octalDefaultDirMode le mode octal des droits par defaut sur les dossiers
     * @param octalDefaultFileMode le mode octal des droits par defaut sur les fichiers
     * @throws NoSuchAlgorithmException en cas de directive incorrecte
     * @throws IOException en cas de probleme d'acces au fichier
     */
    public void addMappings(Builder rpmbuilder, String sourceBaseParam, int octalDefaultDirMode, int octalDefaultFileMode)
        throws NoSuchAlgorithmException, IOException
    {
        int dirmode;
        int filemode;
        String user;
        String group;
        File inputFile;
        FileSetManager fileSetManager = new FileSetManager(getLog());
        String sourceBase;
        String sourceBackup;

        if (sourceBaseParam == null)
        {
            sourceBase = "";
        }
        else
        {
            sourceBase = sourceBaseParam + File.separator;
        }

        for (Mapping tree : mappings)
        {
            sourceBackup = tree.getSource().getDirectory();

            tree.getSource().setDirectory(sourceBase + sourceBackup);

            dirmode = tree.getDirmode();
            if (dirmode == 0)
            {
                dirmode = octalDefaultDirMode;
            }
            filemode = tree.getFilemode();
            if (filemode == 0)
            {
                filemode = octalDefaultFileMode;
            }
            user = tree.getUser();
            if (user == null)
            {
                user = defaultuser;
            }
            group = tree.getGroup();
            if (group == null)
            {
                group = defaultgroup;
            }
            String destdir = tree.getDestination() + "/";

            for (String exclude : DEFAULT_EXCLUDES)
            {
                tree.getSource().addExclude(exclude);
            }
            
            if (tree.isIncludedirs())
            {
                for (String includedFilename : fileSetManager.getIncludedDirectories(tree.getSource()))
                {
                    rpmbuilder.addDirectory((destdir + corrigerChemin(includedFilename)).trim().replaceAll("/$", ""), dirmode, tree.getDirective(), user, group, true);
                }
            }

            for (String includedFilename : fileSetManager.getIncludedFiles(tree.getSource()))
            {
                inputFile = new File(tree.getSource().getDirectory(), includedFilename);
                rpmbuilder.addFile(destdir + corrigerChemin(includedFilename), inputFile, filemode, dirmode, tree.getDirective(), user, group);
            }

            tree.getSource().setDirectory(sourceBackup);
        }
    }

    /**
     * construction d'un RPM
     * 
     * @param fullname Nom complet du rpm
     * @param classifier Le classifier pour identification du rpm
     * @param sourceBase Le repertoire de base pour les mappings de fichier, ou null
     * @throws MojoExecutionException en cas d'erreur d'execution
     * @throws MojoFailureException en cas de parametrage incorrect
     * @see org.apache.maven.plugin.AbstractMojo#execute()
     */
    protected void buildRPM(String fullname, String classifier, String sourceBase) throws MojoExecutionException, MojoFailureException
    {
        org.apache.maven.plugin.logging.Log mavenLogger = getLog();
        BasicConfigurator.configure(new MavenLoggerLog4jBridge(mavenLogger, Level.WARNING.intValue()));

        Builder rpmbuilder = initialiserBuilder(fullname);
        int octalDefaultDirMode = Integer.decode(defaultdirmode);
        int octalDefaultFileMode = Integer.decode(defaultfilemode);

        try
        {
            
            // Inscription des dossiers vides a creer
            
            if (emptyfolders != null)
            {
                addEmptyFolders(rpmbuilder, octalDefaultDirMode);
            }

            // Inscription des mappings de fichiers
            
            if (mappings != null)
            {
                addMappings(rpmbuilder, sourceBase, octalDefaultDirMode, octalDefaultFileMode);
            }

            // Inscription des liens symcoliques a creer
            
            if (links != null)
            {
                addLinks(rpmbuilder, octalDefaultFileMode);
            }

            // Inscription des dependances du RPM
            
            if (requires != null)
            {
                addRequires(rpmbuilder);
            }

            // Creation du fichier
            
            File outputFile = new File(outputdir);
            outputFile.mkdirs();

            String rpmFilename = rpmbuilder.build(outputFile);
            File result = new File(outputFile, rpmFilename);

            // Rattachement de l'artefact Maven
            // Si le classifier est "main", il s'agit de l'artefact principal
            // Sinon, on le rattache en tant qu'artefact secondaire
            
            if ("main".equals(classifier))
            {
                Artifact oldArtifact = mavenProject.getArtifact();
                Artifact newArtifact =
                    artifactFactory.createArtifact(oldArtifact.getGroupId(), oldArtifact.getArtifactId(), oldArtifact.getVersion(),
                        oldArtifact.getScope(), "rpm");
                newArtifact.setFile(result);
                mavenProject.setArtifact(newArtifact);
            }
            else
            {
                getLog().info("Rattachement de l'artefact " + classifier + ": " + rpmFilename);
                mavenProjectHelper.attachArtifact(mavenProject, "rpm", classifier, result);
            }

        }
        catch (NoSuchAlgorithmException e)
        {
            throw new MojoExecutionException("Echec de la construction du RPM " + fullname, e);
        }
        catch (IOException e)
        {
            throw new MojoExecutionException("Echec de la construction du RPM " + fullname, e);
        }
    }

    /**
     * 
     * Gere le fait que le FileSet de Maven sous Windows renvoie des noms de fichier
     * avec anti-slash
     *
     * @param entree la chaîne brute
     * @return le chemin corrigé
     */
    private String corrigerChemin(String entree)
    {
        if (File.separatorChar != '/')
        {
            return entree.replace(File.separator, "/");
        }
        else
        {
            return entree;
        }
    }
}
