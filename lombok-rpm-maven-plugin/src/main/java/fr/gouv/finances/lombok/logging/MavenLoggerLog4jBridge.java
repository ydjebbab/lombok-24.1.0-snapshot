/*
 * Copyright (c) 2014 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.logging;

import org.apache.log4j.AppenderSkeleton;
import org.apache.log4j.Level;
import org.apache.log4j.spi.LoggingEvent;
import org.apache.maven.plugin.logging.Log;

/**
 * Pont entre Log4j et Maven pour rediriger des logs de Log4J vers Maven
 * 
 * @author Eric Demorsy
 * Date: 27 juin 2014
 */
public class MavenLoggerLog4jBridge extends AppenderSkeleton
{
    /**
     * logger interne
     */
    private final Log logger;

    /**
     * Niveau minimal de log
     */
    private int minLevel = Integer.MIN_VALUE;

    /**
     * Constructeur de la classe MavenLoggerLog4jBridge.java
     *
     * @param logger le logger log4j source
     * @param minlevel le niveau minimal de log a prendre en compte
     */
    public MavenLoggerLog4jBridge(Log logger, int minlevel)
    {
        super();
        this.logger = logger;
        this.minLevel = minlevel;
    }

    /**
     * Constructeur de la classe MavenLoggerLog4jBridge.java
     *
     * @param logger le logger log4j source
     */
    public MavenLoggerLog4jBridge(Log logger)
    {
        super();
        this.logger = logger;
    }

    /** 
     * Ecriture a l'aide du logger
     * 
     * @param event l'evenement log4j a transmettre au logger Maven 
     * @see org.apache.log4j.AppenderSkeleton#append(org.apache.log4j.spi.LoggingEvent)
     */
    protected void append(LoggingEvent event)
    {
        int level = event.getLevel().toInt();
        if (level < minLevel)
        {
            return;
        }
        String msg = event.getMessage().toString();
        if (level == Level.DEBUG_INT || level == Level.TRACE_INT)
        {
            this.logger.debug(msg);
        }
        else if (level == Level.INFO_INT)
        {
            this.logger.info(msg);
        }
        else if (level == Level.WARN_INT)
        {
            this.logger.warn(msg);
        }
        else if (level == Level.ERROR_INT || level == Level.FATAL_INT)
        {
            this.logger.error(msg);
        }
    }

    /** 
     * Fermeture du Logger
     * 
     * @see org.apache.log4j.AppenderSkeleton#close()
     */
    @Override
    public void close()
    {
        // Aucune action necessaire
    }

    /** 
     * (methode de remplacement)
     * {@inheritDoc}
     * @see org.apache.log4j.AppenderSkeleton#requiresLayout()
     */
    @Override
    public boolean requiresLayout()
    {
        return false;
    }
}