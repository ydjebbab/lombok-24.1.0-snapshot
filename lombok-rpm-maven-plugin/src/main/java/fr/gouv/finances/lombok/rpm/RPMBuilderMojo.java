/*
 * Copyright (c) 2014 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.rpm;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;

/**
 * Mojo de construction de paquets RPM. Permet de construire des RPM sous Windows
 * 
 * @goal rpmbuild
 */
public class RPMBuilderMojo extends AbstractRPMBuilderMojo
{

    /**
     * Classifier de l'artefact genere. Par defaut, il devient l'artefact principal
     * 
     * @parameter default-value="main"
     */
    private String classifier;


    /**
     * Constructeur de la classe RPMBuilderMojo.java
     *
     */
    public RPMBuilderMojo()
    {
        super();
        
    }


    /** 
     * Execution du plugin
     * 
     * @throws MojoExecutionException en cas d'erreur d'execution
     * @throws MojoFailureException en cas de parametrage incorrect
     * @see org.apache.maven.plugin.AbstractMojo#execute()
     */
    @Override
    public void execute() throws MojoExecutionException, MojoFailureException
    {
        buildRPM(getName(), classifier, null);        
    }

}
