/*
 * Copyright (c) 2014 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.rpm;

import java.io.File;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;

/**
 * Mojo de construction de paquets RPM en lots. Permet de construire des RPM sous Windows
 * 
 * @goal rpmmultibuild
 */
public class RPMMultiBuilderMojo extends AbstractRPMBuilderMojo
{

    /**
     * Dossier de base des fichiers source
     * 
     * @parameter expression = "${project.build.directory}/rpm-manuels"
     */
    private String source;

    /**
     * Constructeur de la classe RPMMultiBuilderMojo.java
     *
     */
    public RPMMultiBuilderMojo()
    {
        super();
        
    }

    /**
     * Execution du plugin
     * 
     * @throws MojoExecutionException en cas d'erreur d'execution
     * @throws MojoFailureException en cas de parametrage incorrect
     * @see org.apache.maven.plugin.AbstractMojo#execute()
     */
    @Override
    public void execute() throws MojoExecutionException, MojoFailureException
    {
        File sourceDir = new File(source);
        if (!sourceDir.exists()) return;
        if (!sourceDir.isDirectory())
        {
            throw new MojoFailureException(source + "n'est pas un dosssier");
        }
        File[] dossiersSource = sourceDir.listFiles();
        for (File dossierSource : dossiersSource)
        {
            buildRPM(getName() + "-" + dossierSource.getName(), dossierSource.getName(), dossierSource.getAbsolutePath());
        }

    }

}
