/*
 * Copyright (c) 2014 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.rpm;


/**
 * Constituant de RPM sous la forme d'un repertoire vide
 * 
 * @author Eric Demorsy
 * Date: 27 juin 2014
 */
public class Emptyfolder extends RPMMapping
{

    /**
     * Constructeur de la classe Emptyfolder.java
     *
     */
    public Emptyfolder()
    {
        super();        
    }
}
