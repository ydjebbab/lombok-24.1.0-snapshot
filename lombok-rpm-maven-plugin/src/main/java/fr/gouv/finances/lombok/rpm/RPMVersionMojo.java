/*
 * Copyright (c) 2014 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.rpm;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.project.MavenProject;

/**
 * Mojo qui fixe le numero de version et release pour les RPMs d'un build
 * 
 * @goal rpmversion
 */
public class RPMVersionMojo extends AbstractMojo
{
    
    /**
     * Numero de version du projet
     * 
     * @parameter expression = "${project.version}"
     */
    private String rawVersion;

    /**
     * Projet Maven
     * 
     * @parameter expression = "${project}"
     */
    private MavenProject project;

    /**
     * Nom de la propriete a laquelle est affectee la version
     * 
     * @parameter default-value = "rpm.version"
     */
    private String versionproperty;

    /**
     * Nom de la propriete a laquelle est affectee la release des nouveaux paquets
     * 
     * @parameter default-value = "rpm.release"
     */
    private String releaseproperty;

    /**
     * Constructeur.
     */
    public RPMVersionMojo()
    {
        super();
    }
    
    /**
     * Aggregation version et release
     * 
     * @author Eric Demorsy
     * Date: 27 juin 2014
     */
    static final class CompositeVersion
    {
        /**
         * version - numero de version
         */
        String version;

        /**
         * release - String, numero de release
         */
        String release;
        
        /**
         * Constructeur de la classe RPMVersionMojo.java
         *
         */
        public CompositeVersion()
        {
            super();
            
        }

        /**
         * Methode d'acces au numero de version
         *
         * @return le numero de version
         */
        public String getVersion()
        {
            return version;
        }

        /**
         * Methode d'acces au numero de release
         *
         * @return le numero de release
         */
        public String getRelease()
        {
            return release;
        }

        /** 
         * (methode de remplacement)
         * {@inheritDoc}
         * @see java.lang.Object#toString()
         */
        @Override
        public String toString()
        {
            return version + '-' + release;
        }
    }

    /**
     * Cree un numero, accompagne d'un timestamp pour les snapshots
     *
     * @return le numero de version avec eventuel modificateur de release
     */
    private CompositeVersion calculateVersion() 
    {

        final CompositeVersion response = new CompositeVersion();

        int modifierIndex = rawVersion.indexOf('-');

        if (modifierIndex == -1)
        {
            response.version = rawVersion;
            response.release = "1";
        }
        else
        {
            response.version = rawVersion.substring(0, modifierIndex);
            String modifier = rawVersion.substring(modifierIndex + 1, rawVersion.length());
            if (modifier.endsWith("SNAPSHOT"))
            {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMddHHmmss", Locale.getDefault());
                simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
                response.release = modifier + simpleDateFormat.format(new Date());
            }
            else 
            {
                response.release = modifier;                
            }

        }
        
        return response;
    }

    /** 
     * (methode de remplacement)
     * {@inheritDoc}
     * @throws MojoExecutionException en cas d'erreur d'execution
     * @throws MojoFailureException en cas de parametrage incorrect
     * @see org.apache.maven.plugin.AbstractMojo#execute()
     */
    @Override
    public void execute() throws MojoExecutionException, MojoFailureException
    {
        CompositeVersion compVersion = calculateVersion();
        project.getProperties().put(versionproperty, compVersion.version);
        project.getProperties().put(releaseproperty, compVersion.release);
    }

}
