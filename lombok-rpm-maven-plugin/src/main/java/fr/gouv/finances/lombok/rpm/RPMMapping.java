/*
 * Copyright (c) 2014 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.rpm;

import org.redline_rpm.payload.Directive;

/**
 * Classe de base des constituants d'un RPM
 * 
 * @author Eric Demorsy Date: 27 juin 2014
 */
public class RPMMapping
{
    private String destination;

    private String user;

    private String group;

    private String dirmode;
    
    private boolean configuration = false;

    /**
     * Constructeur de la classe RPMMapping.java -protected pour eviter l'instanciation
     */
    protected RPMMapping()
    {
    }

    /**
     * Methode d'acces du repertoire cible d'installation
     * 
     * @return le repertoire cible d'installation
     */
    public String getDestination()
    {
        return destination;
    }

    /**
     * Methode d'affectation du repertoire cible d'installation
     * 
     * @param destination le repertoire cible d'installation
     */
    public void setDestination(String destination)
    {
        this.destination = destination;
    }

    /**
     * Methode d'acces de l'utilisateur unix proprietaire
     * 
     * @return l'utilisateur unix proprietaire
     */
    public String getUser()
    {
        return user;
    }

    /**
     * Methode d'affectation de l'utilisateur unix proprietaire
     * 
     * @param user l'utilisateur unix proprietaire
     */
    public void setUser(String user)
    {
        this.user = user;
    }

    /**
     * Methode d'acces du groupe unix proprietaire
     * 
     * @return le groupe unix proprietaire
     */
    public String getGroup()
    {
        return group;
    }

    /**
     * Methode d'affectation du groupe unix proprietaire
     * 
     * @param group le groupe unix proprietaire
     */
    public void setGroup(String group)
    {
        this.group = group;
    }

    /**
     * Methode d'acces du mode unix des dossiers
     * 
     * @return le mode unix des dossiers
     */
    public int getDirmode()
    {
        if (dirmode == null)
        {
            return 0;
        }
        else
        {
            return Integer.decode(dirmode);
        }
    }

    /**
     * Methode d'affectation du mode unix des dossiers
     * 
     * @param dirmode le mode unix des dossiers
     */
    public void setDirmode(String dirmode)
    {
        this.dirmode = dirmode;
    }

    /**
     * Determine si cet element est identifie comme un element de configuration et ne sera donc pas ecrase a chaque
     * installation
     * 
     * @return true si c'est un element de configuration
     */
    public boolean isConfiguration()
    {
        return configuration;
    }

    /**
     * Affecte ce constituant comme etant de configuration ou pas, donc a ecraser ou pas
     * 
     * @param configuration true si c'est un element de configuration
     */
    public void setConfiguration(boolean configuration)
    {
        this.configuration = configuration;
    }

    /**
     * Determine les directive supplementaires eventuelles
     * 
     * @return les directive supplementaires eventuelles
     */
    public Directive getDirective()
    {
        if (configuration)
        {
            return Directive.NOREPLACE;
        }
        else
        {
            return Directive.NONE;
        }
    }
}
