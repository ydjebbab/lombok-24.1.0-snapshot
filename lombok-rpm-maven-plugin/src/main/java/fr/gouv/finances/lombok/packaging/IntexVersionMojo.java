/*
 * Copyright (c) 2014 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.packaging;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.project.MavenProject;

/**
 * Mojo qui fixe le numero de version du livrable d'Intex
 * 
 * @goal intexversion
 */
public class IntexVersionMojo extends AbstractMojo
{

    /**
     * Numero de version du projet
     * 
     * @parameter expression = "${project.version}"
     */
    private String rawVersion;

    /**
     * Projet Maven
     * 
     * @parameter expression = "${project}"
     */
    private MavenProject project;

    /**
     * Nom de la propriete a laquelle est affectee la version
     * 
     * @parameter default-value = "intex.version"
     */
    private String versionproperty;

    /**
     * Nom de la propriete a laquelle est affectee la date
     * 
     * @parameter default-value = "intex.date"
     */
    private String dateproperty;

    /**
     * Constructeur de la classe IntexVersionMojo.java
     *
     */
    public IntexVersionMojo()
    {
        super();
        
    }

    private final SimpleDateFormat dateFormat = new SimpleDateFormat("yyMMdd", Locale.getDefault());

    private String calculateDate()
    {
        return dateFormat.format(new Date());

    }

    /**
     * Methode de reformatage du numero de version
     *
     * @param rawVersion
     * @return
     * @throws MojoFailureException
     */
    private String calculateVersion(String rawVersion) throws MojoFailureException
    {

        String baseVersion;
        // String release;

        int modifierIndex = rawVersion.indexOf('-');

        if (modifierIndex == -1)
        {
            baseVersion = rawVersion;
            // release = null;
        }
        else
        {
            baseVersion = rawVersion.substring(0, modifierIndex);
            // release = rawVersion.substring( modifierIndex + 1, rawVersion.length() );
        }

        String[] versionItems = baseVersion.split("\\.");
        int length = versionItems.length;

        String result;
        if (length == 2)
        {
            result = String.format("%d.%02d", Integer.parseInt(versionItems[0]), Integer.parseInt(versionItems[1]));
        }
        else if (length == 3)
        {
            result =
                String.format("%d.%02d.%02d", Integer.parseInt(versionItems[0]), Integer.parseInt(versionItems[1]),
                    Integer.parseInt(versionItems[2]));
        }
        else
        {
            throw new MojoFailureException("Numéro de version incorrect, doit être de la forme M.m[.p]");
        }

        return result;
    }

    /** 
     * (methode de remplacement)
     * {@inheritDoc}
     * @see org.apache.maven.plugin.AbstractMojo#execute()
     */
    @Override
    public void execute() throws MojoExecutionException, MojoFailureException
    {
        project.getProperties().put(versionproperty, calculateVersion(rawVersion));
        project.getProperties().put(dateproperty, calculateDate());
    }
}
