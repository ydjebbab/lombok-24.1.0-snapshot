/*
 * Copyright (c) 2014 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.rpm;


/**
 * Constituant de RPM sous la forme d'un lien symbolique
 * 
 * @author Eric Demorsy
 * Date: 11 aout 2014
 */
public class Link extends RPMMapping
{

    private String target;
    
    private String filemode;
    

    /**
     * Constructeur de la classe Link.java
     *
     */
    public Link()
    {
        super();        
    }

    /**
     * Methode d'acces a la cible du lien symbolique
     * 
     * @return la cible du lien symbolique
     */
    public String getTarget()
    {
        return target;
    }

    /**
     * Methode d'affectation de la cible du lien symbolique
     * 
     * @param target la cible du lien symbolique
     */
    public void setTarget(String target)
    {
        this.target = target;
    }

    /**
     * Methode d'acces du mode unix des fichiers
     *
     * @return le mode unix des fichiers
     */
    public int getFilemode()
    {
        if (filemode == null)
        {
            return 0;
        }
        else
        {
            return Integer.decode(filemode);
        }
    }

    /**
     * Methode d'affectation du mode unix des fichiers
     *
     * @param filemode le mode unix des fichiers
     */
    public void setFilemode(String filemode)
    {
        this.filemode = filemode;
    }
}
