/*
 * Copyright (c) 2014 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.rpm;

import org.apache.maven.shared.model.fileset.FileSet;


/**
 * Definition d'un mapping de dossier complet
 * 
 * @author Eric Demorsy
 * Date: 27 juin 2014
 */
public class Mapping extends RPMMapping
{
    private FileSet source; 

    private String filemode;
    
    private boolean includedirs = true;
    
    /**
     * Constructeur de la classe Mapping.java
     *
     */
    public Mapping()
    {
        super();
    }

    /**
     * Methode d'acces du repertoire d'origine
     *
     * @return le repertoire d'origine
     */
    public FileSet getSource()
    {
        return source;
    }

    /**
     * Methode d'affectation du repertoire d'origine
     *
     * @param source le repertoire d'origine
     */
    public void setSource(FileSet source)
    {
        this.source = source;
    }

    /**
     * Methode d'acces du mode unix des fichiers
     *
     * @return le mode unix des fichiers
     */
    public int getFilemode()
    {
        if (filemode == null)
        {
            return 0;
        }
        else
        {
            return Integer.decode(filemode);
        }
    }

    /**
     * Methode d'affectation du mode unix des fichiers
     *
     * @param filemode le mode unix des fichiers
     */
    public void setFilemode(String filemode)
    {
        this.filemode = filemode;
    }

    /**
     * Methode d'acces du drapeau d'inclusion explicite des dossiers
     *
     * @return true si les dossiers doivent etre inclus dans le rpm
     */
    public boolean isIncludedirs()
    {
        return includedirs;
    }

    /**
     * Methode d'affectation du drapeau d'inclusion explicite des dossiers
     *
     * @param includedirs true si les dossiers doivent etre inclus dans le rpm
     */
    public void setIncludedirs(boolean includedirs)
    {
        this.includedirs = includedirs;
    }

    
}
