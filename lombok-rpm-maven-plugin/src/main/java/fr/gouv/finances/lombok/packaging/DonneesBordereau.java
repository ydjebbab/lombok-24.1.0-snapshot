/*
 * Copyright (c) 2014 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.packaging;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Aggregation des donnees inserees dans le bordereau.
 * 
 * @author Eric Demorsy Date: 27 juin 2014
 */
public class DonneesBordereau implements Serializable
{

    
    /**
     * Fichier du livrable decrit dans le bordereau.
     * 
     * @author Eric Demorsy Date: 27 juin 2014
     */
    public class FichierBordereau implements Serializable
    {
        
        /** serialVersionUID. */
        private static final long serialVersionUID = 1L;

        /** nom du fichier. */
        private String nom;

        /** numero de version. */
        private String version;

        /**
         * Constructeur de la classe DonneesBordereau.java
         * 
         * @param nom le nom du bordereau
         */
        public FichierBordereau(String nom)
        {
            super();
            this.nom = nom;
        }

        /**
         * Methode d'acces au nom du fichier.
         * 
         * @return le nom du fichier
         */
        public String getNom()
        {
            return nom;
        }

        /**
         * Modificateur du nom du fichier.
         * 
         * @param nom le nouveau nom du fichier
         */
        public void setNom(String nom)
        {
            this.nom = nom;
        }

        /**
         * Methode d'acces au numero de version.
         * 
         * @return le numero de version
         */
        public String getVersion()
        {
            return version;
        }

        /**
         * Modificateur du numero de version.
         * 
         * @param version le nouveau numero de version
         */
        public void setVersion(String version)
        {
            this.version = version;
        }

    }

    
    /** serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** date intex. */
    private String dateIntex;

    /** crc. */
    private String crc;

    /** code application. */
    private String codeApplication;

    /** version. */
    private String version;

    /** fichiers. */
    private final List<FichierBordereau> fichiers;

    /**
     * Constructeur de la classe DonneesBordereau.java
     * 
     * @param codeApplication le code application d'intex
     * @param dateIntex la date au format intex
     * @param version le numero de version au format intex
     */
    public DonneesBordereau(String codeApplication, String dateIntex, String version)
    {
        super();
        this.dateIntex = dateIntex;
        this.codeApplication = codeApplication;
        this.version = version;
        this.fichiers = new ArrayList<>();
    }

    /**
     * Accesseur de l attribut nom.
     * 
     * @return nom le nom du bordereau
     */
    public String getNom()
    {
        return "BL_" + codeApplication + "_" + version + "_" + dateIntex;
    }

    /**
     * Accesseur de l attribut archive.
     * 
     * @return archive le nom du fichier archive
     */
    public String getArchive()
    {
        return codeApplication + "_" + version + "_" + dateIntex + ".tar.gz";
    }

    /**
     * Accesseur de l attribut date.
     * 
     * @return date la date du bordereau au format AA/MM/JJ
     */
    public String getDate()
    {
        String year = dateIntex.substring(0, 2);
        String month = dateIntex.substring(2, 4);
        String day = dateIntex.substring(4, 6);
        
        return day + "/" + month + "/" + year;
    }

    /**
     * Modificateur de la date au format intex
     * 
     * @param date la nouvelle date au format intex
     */
    public void setDateIntex(String date)
    {
        this.dateIntex = date;
    }

    /**
     * Accesseur de l attribut crc.
     * 
     * @return le code crc de l'archive
     */
    public String getCrc()
    {
        return crc;
    }

    /**
     * Modificateur de l attribut crc.
     * 
     * @param crc le nouveau crc de l'archive
     */
    public void setCrc(String crc)
    {
        this.crc = crc;
    }

    /**
     * Accesseur de l attribut code application.
     * 
     * @return le code intex de l'application
     */
    public String getCodeApplication()
    {
        return codeApplication;
    }

    /**
     * Modificateur de l attribut code application.
     * 
     * @param codeApplication le nouveau code intex de l'application
     */
    public void setCodeApplication(String codeApplication)
    {
        this.codeApplication = codeApplication;
    }

    /**
     * Accesseur de l attribut version.
     * 
     * @return version le numero de version au format intex
     */
    public String getVersion()
    {
        return version;
    }

    /**
     * Modificateur de l attribut version.
     * 
     * @param version le nouveau numero de version au format intex
     */
    public void setVersion(String version)
    {
        this.version = version;
    }

    /**
     * Ajoute un fichier au contenu de l'archive du livrable.
     * 
     * @param nomfichier le nom du fichier a ajouter
     * @return l'objet-valeur correspondant au fichier
     */
    public FichierBordereau ajouterFichier(String nomfichier)
    {
        FichierBordereau fichier = new FichierBordereau(nomfichier);
        fichiers.add(fichier);
        return fichier;
    }

    /**
     * Accesseur de l attribut fichiers.
     * 
     * @return la liste des fichiers inclus dans l'archive
     */
    public List<FichierBordereau> getFichiers()
    {
        return fichiers;
    }

}
