/*
 * Copyright (c) 2014 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.packaging;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.shared.model.fileset.FileSet;
import org.apache.maven.shared.model.fileset.util.FileSetManager;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import fr.gouv.finances.lombok.packaging.DonneesBordereau.FichierBordereau;

/**
 * Mojo qui cree le fichier xml utilise pour generer le bordereau
 * 
 * @goal bordereau
 */
public class BordereauXMLMojo extends AbstractMojo
{

    // Expression reguliere permettant d'extraire le numero de version d'un RPM
    private static final Pattern VERSIONED_RPM_PATTERN = Pattern.compile("([0-9]{1,}\\.[0-9]{1,}(\\.[0-9]{1,})?(-[0-9]{1,})?).rpm");

    /**
     * Numero de version du projet a la norme INTEX
     * 
     * @required
     * @parameter expression = "${intex.version}"
     */
    private String version;

    /**
     * Code INTEX de l'application
     * 
     * @required
     * @parameter expression = "${code.appli.intex}"
     */
    private String codeapplication;

    /**
     * Nom et chemin du fichier genere par le mojo
     * 
     * @parameter default-value = "${project.build.directory}/bordereau.xml"
     */
    private String outputfile;

    /**
     * Liste des fichiers inclus dans le paquet
     * 
     * @required
     * @parameter
     */
    private List<FileSet> items;

    /**
     * Date au format INTEX
     * 
     * @parameter expression = "${intex.date}"
     */
    private String dateintex;

    /**
     * Fichier de checksum genere precedemment
     * 
     * @parameter expression = "${project.build.directory}/checksums.csv"
     */
    private String checksums;

    /**
     * Constructeur de la classe BordereauXMLMojo.java
     *
     */
    public BordereauXMLMojo()
    {
        super();
        
    }

    /**
     * Lit le fichier de CRC
     * Le fichier de CRC genere dans un goal precedent donne le checksum
     * du livrable, a integrer dans le bordereau
     *
     * @param nomArchive
     * @return
     * @throws IOException
     */
    private String lireCRC(String nomArchive) throws IOException
    {
        try (BufferedReader reader = new BufferedReader(new FileReader(checksums)))
        {
            String line = reader.readLine();
            String[] values = null;
            
            while (line != null)
            {
                if (!line.startsWith("#"))
                {
                    values = line.split(",");
                    if ((values.length == 2) && (values[0].equalsIgnoreCase(nomArchive)))
                    {
                        return values[1];
                    }
                }
                line = reader.readLine();
            }
            return null;
        }
    }

    /**
     * Prepare les donnees a remplir dans le bordereau
     *
     * @return
     * @throws IOException
     */
    private DonneesBordereau initDonnees() throws IOException
    {
        DonneesBordereau bordereau = new DonneesBordereau(codeapplication, dateintex, version);
        String crc = lireCRC(bordereau.getArchive());
        bordereau.setCrc(crc);
        FileSetManager fileSetManager = new FileSetManager(getLog());
        Matcher matcher = null;
        FichierBordereau nouveauFichier = null;

        // La liste des fichiers a livrer est creer a partir de la liste
        // d'items passee en configuration du mojo
        
        for (FileSet fileSet : items)
        {
            for (String filename : fileSetManager.getIncludedFiles(fileSet))
            {
                nouveauFichier = bordereau.ajouterFichier(filename);
                matcher = VERSIONED_RPM_PATTERN.matcher(filename);
                
                // si le fichier est un rpm, on extrait le numero de version
                // pour l'inscrire dans les informations du bordereau
               
                if (matcher.find())
                {
                    nouveauFichier.setVersion(matcher.group(1));
                }
            }
        }

        return bordereau;
    }

    /**
     * Cree le fichier XML de donnees et le transforme via XSLT
     *
     * @param bordereau
     * @param destination
     * @throws ParserConfigurationException
     * @throws TransformerException
     */
    private void ecrireBordereau(DonneesBordereau bordereau, String destination) throws ParserConfigurationException, TransformerException
    {
        DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
        docFactory.setExpandEntityReferences(false);
        DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

        // Element racine
        
        Document doc = docBuilder.newDocument();
        Element rootElement = doc.createElement("bordereau");
        doc.appendChild(rootElement);

        // Nom du bordereau
        
        Element newElement = doc.createElement("nom");
        newElement.setTextContent(bordereau.getNom());
        rootElement.appendChild(newElement);

        // Nom du fichier archive
        
        newElement = doc.createElement("archive");
        newElement.setTextContent(bordereau.getArchive());
        rootElement.appendChild(newElement);

        // Date de creation
        
        newElement = doc.createElement("date");
        newElement.setTextContent(bordereau.getDate());
        rootElement.appendChild(newElement);

        // CRC (Cheksum MD5)
        
        newElement = doc.createElement("crc");
        newElement.setTextContent(bordereau.getCrc());
        rootElement.appendChild(newElement);

        // Code Intex de l'application
        
        newElement = doc.createElement("codeappli");
        newElement.setTextContent(bordereau.getCodeApplication());
        rootElement.appendChild(newElement);

        // Numero de version, format Intex
        
        newElement = doc.createElement("version");
        newElement.setTextContent(bordereau.getVersion());
        rootElement.appendChild(newElement);

        // Liste des fichiers inclus dans la livraison
        
        Element fichiersElement = doc.createElement("fichiers");
        rootElement.appendChild(fichiersElement);
        Element fichierElement;
        for (FichierBordereau fichier : bordereau.getFichiers())
        {
            fichierElement = doc.createElement("fichier");
            newElement = doc.createElement("nom");
            newElement.setTextContent(fichier.getNom());
            fichierElement.appendChild(newElement);
            if (fichier.getVersion() != null)
            {
                newElement = doc.createElement("version");
                newElement.setTextContent(fichier.getVersion());
                fichierElement.appendChild(newElement);
            }
            fichiersElement.appendChild(fichierElement);
        }

        // Transformation XSLT du fichier XML produit
        // Le resultat est un fichier xml a zipper avec d'autres elements
        // pour obtenir un fichier opendocument
        
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        DOMSource source = new DOMSource(doc);
        StreamResult result = new StreamResult(new File(destination));

        transformer.transform(source, result);
    }

    /** 
     * (methode de remplacement)
     * {@inheritDoc}
     * @see org.apache.maven.plugin.AbstractMojo#execute()
     */
    @Override
    public void execute() throws MojoExecutionException, MojoFailureException
    {
        try
        {
            DonneesBordereau bordereau = initDonnees();
            ecrireBordereau(bordereau, outputfile);
        }
        catch (ParserConfigurationException e)
        {
            throw new MojoExecutionException("Erreur lors de l'écriture des données du bordereau", e);
        }
        catch (TransformerException e)
        {
            throw new MojoExecutionException("Erreur lors de l'écriture des données du bordereau", e);
        }
        catch (IOException e)
        {
            throw new MojoExecutionException("Erreur lors de l'écriture des données du bordereau", e);
        }
    }

}
