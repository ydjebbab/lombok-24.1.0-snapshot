/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 *
 */
package fr.gouv.finances.lombok.bancaire.dao.impl;

import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import fr.gouv.finances.lombok.bancaire.bean.GuichetBancaire;
import fr.gouv.finances.lombok.bancaire.dao.GuichetBancaireDao;
import fr.gouv.finances.lombok.bancaire.dao.ProprieteGuichetBancaire;
import fr.gouv.finances.lombok.util.base.BaseDaoImpl;
import fr.gouv.finances.lombok.util.exception.ProgrammationException;

/**
 * Implémentation Hibernate du DAO permettant de gérer les données des guichets bancaires.
 *
 * @author amleplatinec
 * @author Christophe Breheret-Girardin
 */
public class GuichetBancaireDaoImpl extends BaseDaoImpl implements GuichetBancaireDao
{
    protected static final Logger LOGGER = LoggerFactory.getLogger(GuichetBancaireDaoImpl.class);

    public GuichetBancaireDaoImpl()
    {
        super();
    }

    /**
     * {@inheritDoc}
     *
     * @see fr.gouv.finances.lombok.bancaire.dao.GuichetBancaireDao#deleteTousLesGuichetsBancaires().
     */
    @Override
    public void deleteTousLesGuichetsBancaires()
    {
        LOGGER.trace("Suppression de tous les guichets bancaires");

        // Suppression en masse
        getHibernateTemplate().bulkUpdate("delete GuichetBancaire");
    }

    /**
     * {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.bancaire.dao.GuichetBancaireDao#findGuichetBancaireNonPerimeParCodeGuichetEtCodeEtablissement(
     *      java.lang.String, java.lang.String).
     */
    @Override
    @SuppressWarnings("unchecked")
    public GuichetBancaire findGuichetBancaireNonPerimeParCodeGuichetEtCodeEtablissement(String codeGuichet,
        String codeEtablissement)
    {
        LOGGER.debug("Recherche du guichet bancaire, non périmé, de code '{}', d'établissement '{}'", codeGuichet, codeEtablissement);

        // Paramétrage de la recherche sur les guichets bancaires
        DetachedCriteria criteria = DetachedCriteria.forClass(GuichetBancaire.class);

        // Ajout des critères de recherche
        criteria.add(Restrictions.eq(ProprieteGuichetBancaire.CODE_ETABLISSEMENT.getNom(), codeEtablissement));
        criteria.add(Restrictions.eq(ProprieteGuichetBancaire.CODE_GUICHET.getNom(), codeGuichet));
        criteria.add(Restrictions.eq(ProprieteGuichetBancaire.EST_PERIME.getNom(), Boolean.FALSE));

        // Exécution de la recherche
        Iterator<GuichetBancaire> result = ((List<GuichetBancaire>) getHibernateTemplate().findByCriteria(criteria)).iterator();

        // Récupération du 1er résultat
        if (result.hasNext())
        {
            return result.next();
        }

        return null;
    }

    /**
     * {@inheritDoc}
     *
     * @see fr.gouv.finances.lombok.bancaire.dao.GuichetBancaireDao#findGuichetsBancaireParCommuneEtCodeEtablissement(
     *      java.lang.String, java.lang.String).
     */
    @Override
    @SuppressWarnings("unchecked")
    public List<GuichetBancaire> findGuichetsBancaireParCommuneEtCodeEtablissement(String commune, String codeEtablissement)
    {
        LOGGER.debug("Recherche des guichets bancaires, liés à la commune '{}' et l'établissement '{}'", commune, codeEtablissement);

        // Paramétrage de la recherche sur les guichets bancaires
        DetachedCriteria criteria = DetachedCriteria.forClass(GuichetBancaire.class);

        // Ajout des critères de recherche
        criteria.add(Restrictions.eq(ProprieteGuichetBancaire.COMMUNE.getNom(), commune));
        criteria.add(Restrictions.eq(ProprieteGuichetBancaire.CODE_ETABLISSEMENT.getNom(), codeEtablissement));

        // Exécution de la recherche
        return (List<GuichetBancaire>) getHibernateTemplate().findByCriteria(criteria);
    }

    /**
     * {@inheritDoc}
     *
     * @see fr.gouv.finances.lombok.bancaire.dao.GuichetBancaireDao#findGuichetsBancaireParDenominationCompleteBanqueEtCodeEtablissement(
     *      java.lang.String, java.lang.String).
     */
    @Override
    @SuppressWarnings("unchecked")
    public List<GuichetBancaire> findGuichetsBancaireParDenominationCompleteBanqueEtCodeEtablissement(String denominationComplete,
        String codeEtablissement)
    {
        LOGGER.debug("Recherche des guichets bancaires par dénomination complète '{}', liés à l'établissement '{}'", denominationComplete,
            codeEtablissement);

        // Paramétrage de la recherche sur les guichets bancaires
        DetachedCriteria criteria = DetachedCriteria.forClass(GuichetBancaire.class);

        // Ajout des critères de recherche
        criteria.add(Restrictions.eq(ProprieteGuichetBancaire.DENOMINATION_COMPLETE.getNom(), denominationComplete));
        criteria.add(Restrictions.eq(ProprieteGuichetBancaire.CODE_ETABLISSEMENT.getNom(), codeEtablissement));

        // Exécution de la recherche
        return (List<GuichetBancaire>) getHibernateTemplate().findByCriteria(criteria);
    }

    /**
     * {@inheritDoc}
     *
     * @see fr.gouv.finances.lombok.bancaire.dao.GuichetBancaireDao#findGuichetsBancairesNonPerimes().
     */
    @Override
    @SuppressWarnings("unchecked")
    public List<GuichetBancaire> findGuichetsBancairesNonPerimes()
    {
        LOGGER.trace("Recherche des guichets bancaires non périmés");

        // Paramétrage de la recherche sur les guichets bancaires
        DetachedCriteria criteria = DetachedCriteria.forClass(GuichetBancaire.class);

        // Ajout des critères de recherche
        criteria.add(Restrictions.eq(ProprieteGuichetBancaire.EST_PERIME.getNom(), false));

        // Exécution de la recherche
        return (List<GuichetBancaire>) getHibernateTemplate().findByCriteria(criteria);
    }

    /**
     * {@inheritDoc}
     *
     * @see fr.gouv.finances.lombok.bancaire.dao.GuichetBancaireDao#findGuichetsBancairesParCodeEtablissement(java.lang.String).
     */
    @Override
    @SuppressWarnings("unchecked")
    public List<GuichetBancaire> findGuichetsBancairesParCodeEtablissement(String codeEtablissement)
    {
        LOGGER.debug("Recherche des guichets bancaires liés à l'établissement '{}'", codeEtablissement);

        // Cas du paramètre absent
        if (StringUtils.isEmpty(codeEtablissement))
        {
            throw new ProgrammationException("Au moins un des paramètres de recherche d'entité n'a pas été fourni");
        }

        // Paramétrage de la recherche sur les guichets bancaires
        DetachedCriteria criteria = DetachedCriteria.forClass(GuichetBancaire.class);

        // Ajout des critères de recherche
        criteria.add(Restrictions.eq(ProprieteGuichetBancaire.CODE_ETABLISSEMENT.getNom(), codeEtablissement));

        // Exécution de la recherche
        return (List<GuichetBancaire>) getHibernateTemplate().findByCriteria(criteria);
    }

    /**
     * {@inheritDoc}
     *
     * @see fr.gouv.finances.lombok.bancaire.dao.GuichetBancaireDao#findGuichetsBancairesParCodeGuichetEtCodeEtablissement(
     *      java.lang.String, java.lang.String).
     */
    @Override
    @SuppressWarnings("unchecked")
    public List<GuichetBancaire> findGuichetsBancairesParCodeGuichetEtCodeEtablissement(
        String codeGuichet, String codeEtablissement)
    {
        LOGGER.debug("Recherche des guichets bancaires de code '{}', liés à l'établissement '{}'", codeGuichet, codeEtablissement);

        // Paramétrage de la recherche sur les guichets bancaires
        DetachedCriteria criteria = DetachedCriteria.forClass(GuichetBancaire.class);

        // Ajout des critères de recherche
        criteria.add(Restrictions.eq(ProprieteGuichetBancaire.CODE_GUICHET.getNom(), codeGuichet));
        criteria.add(Restrictions.eq(ProprieteGuichetBancaire.CODE_ETABLISSEMENT.getNom(), codeEtablissement));

        // Exécution de la recherche
        return (List<GuichetBancaire>) getHibernateTemplate().findByCriteria(criteria);
    }

    /**
     * {@inheritDoc}
     *
     * @see fr.gouv.finances.lombok.bancaire.dao.GuichetBancaireDao#findGuichetsBancairesParCodePostal(java.lang.String).
     */
    @Override
    @SuppressWarnings("unchecked")
    public List<GuichetBancaire> findGuichetsBancairesParCodePostal(String codePostal)
    {
        LOGGER.debug("Recherche des guichets bancaires de code postal '{}'", codePostal);

        // Cas du paramètre absent
        if (StringUtils.isEmpty(codePostal))
        {
            throw new ProgrammationException("Au moins un des paramètres de recherche d'entité n'a pas été fourni");
        }

        // Paramétrage de la recherche sur les guichets bancaires
        DetachedCriteria criteria = DetachedCriteria.forClass(GuichetBancaire.class);

        // Ajout des critères de recherche
        criteria.add(Restrictions.eq(ProprieteGuichetBancaire.CODE_POSTAL.getNom(), codePostal));

        // Exécution de la recherche
        return (List<GuichetBancaire>) getHibernateTemplate().findByCriteria(criteria);
    }

    /**
     * {@inheritDoc}
     *
     * @see fr.gouv.finances.lombok.bancaire.dao.GuichetBancaireDao#findGuichetsBancairesParCommune(java.lang.String).
     */
    @Override
    @SuppressWarnings("unchecked")
    public List<GuichetBancaire> findGuichetsBancairesParCommune(String commune)
    {
        LOGGER.debug("Recherche des guichets bancaires liés à la commune '{}'", commune);

        // Cas du paramètre absent
        if (StringUtils.isEmpty(commune))
        {
            throw new ProgrammationException("Au moins un des paramètres de recherche d'entité n'a pas été fourni");
        }

        // Paramétrage de la recherche sur les guichets bancaires
        DetachedCriteria criteria = DetachedCriteria.forClass(GuichetBancaire.class);

        // Ajout des critères de recherche
        criteria.add(Restrictions.eq(ProprieteGuichetBancaire.COMMUNE.getNom(), commune));

        // Exécution de la recherche
        return (List<GuichetBancaire>) getHibernateTemplate().findByCriteria(criteria);
    }

    /**
     * {@inheritDoc}
     *
     * @see fr.gouv.finances.lombok.bancaire.dao.GuichetBancaireDao#findGuichetsBancairesParDenominationCompleteBanque(java.lang.String).
     */
    @Override
    @SuppressWarnings("unchecked")
    public List<GuichetBancaire> findGuichetsBancairesParDenominationCompleteBanque(String denominationComplete)
    {
        LOGGER.debug("Recherche des guichets bancaires de dénomination complète '{}'", denominationComplete);

        // Cas du paramètre absent
        if (StringUtils.isEmpty(denominationComplete))
        {
            throw new ProgrammationException("Au moins un des paramètres de recherche d'entité n'a pas été fourni");
        }

        // Paramétrage de la recherche sur les guichets bancaires
        DetachedCriteria criteria = DetachedCriteria.forClass(GuichetBancaire.class);

        // Ajout des critères de recherche
        criteria.add(Restrictions.eq(ProprieteGuichetBancaire.DENOMINATION_COMPLETE.getNom(), denominationComplete));

        // Exécution de la recherche
        return (List<GuichetBancaire>) getHibernateTemplate().findByCriteria(criteria);
    }

    /**
     * {@inheritDoc}
     *
     * @see fr.gouv.finances.lombok.bancaire.dao.GuichetBancaireDao#findTousGuichetsBancaires().
     */
    @Override
    public List<GuichetBancaire> findTousGuichetsBancaires()
    {
        LOGGER.trace("Recherche de tous les guichets bancaires");

        // Exécution de la recherche
        return loadAllObjects(GuichetBancaire.class);
    }

    /**
     * {@inheritDoc}
     *
     * @see fr.gouv.finances.lombok.bancaire.dao.GuichetBancaireDao#marqueLesGuichetsBancairesPerimesADate(java.util.Date).
     */
    @Override
    public void marqueLesGuichetsBancairesPerimesADate(Date date)
    {
        LOGGER.debug("Marquage des guichets bancaires périmés pour la date '{}'", date);

        // Initialisation de la requête HQL de mise à jour ensembliste
        String hqlUpdate =
            "update versioned GuichetBancaire c set c.estPerime = ? where not c.dateDerniereMiseAJour = ?";

        // Initialisation des paramétres de la requête
        Object[] values = {Boolean.TRUE, date};

        // Exécution de la mise à jour ensembliste
        getHibernateTemplate().bulkUpdate(hqlUpdate, values);
    }

    /**
     * {@inheritDoc}
     *
     * @see fr.gouv.finances.lombok.bancaire.dao.GuichetBancaireDao#testeExistenceGuichetBancaireNonPerimeParCodeGuichetEtCodeEtablissement(
     *      java.lang.String, java.lang.String).
     */
    @Override
    public boolean testeExistenceGuichetBancaireNonPerimeParCodeGuichetEtCodeEtablissement(String codeGuichet,
        String codeEtablissement)
    {
        LOGGER.debug("Recherche l'existence de guichet bancaire non périmé de code '{}', lié à l'établissement '{}'", codeGuichet,
            codeEtablissement);

        // Exécution de la recherche
        long resultat = getHibernateTemplate().execute(
            // Paramétrage de la recherche sur les guichets bancaires
            session -> (Long) session.createCriteria(GuichetBancaire.class)
                // Ajout des critères de recherche
                .add(Restrictions.eq(ProprieteGuichetBancaire.CODE_ETABLISSEMENT.getNom(), codeEtablissement))
                .add(Restrictions.eq(ProprieteGuichetBancaire.CODE_GUICHET.getNom(), codeGuichet))
                .add(Restrictions.eq(ProprieteGuichetBancaire.EST_PERIME.getNom(), Boolean.FALSE))
                // Le résultat est le nombre de guichet satisfaisant les critères
                .setProjection(Projections.rowCount())
                // La récherche est un résultat unique
                .uniqueResult());

        // Existe-il au moins un résultat ?
        return resultat > 0;
    }

    /**
     * {@inheritDoc}
     *
     * @see fr.gouv.finances.lombok.bancaire.dao.GuichetBancaireDao#testeExistenceGuichetBancairePerimeOuNonParCodeGuichetEtCodeEtablissement(
     *      java.lang.String, java.lang.String).
     */
    @Override
    public boolean testeExistenceGuichetBancairePerimeOuNonParCodeGuichetEtCodeEtablissement(String codeGuichet,
        String codeEtablissement)
    {
        LOGGER.debug("Recherche l'existence de guichet bancaire de code '{}', lié à l'établissement '{}'", codeGuichet, codeEtablissement);

        // Exécution de la recherche
        long resultat = getHibernateTemplate().execute(
            // Paramétrage de la recherche sur les guichets bancaires
            session -> (Long) session.createCriteria(GuichetBancaire.class)
                // Ajout des critères de recherche
                .add(Restrictions.eq(ProprieteGuichetBancaire.CODE_ETABLISSEMENT.getNom(), codeEtablissement))
                .add(Restrictions.eq(ProprieteGuichetBancaire.CODE_GUICHET.getNom(), codeGuichet))
                // Le résultat est le nombre de guichet satisfaisant les critères
                .setProjection(Projections.rowCount())
                // La récherche est un résultat unique
                .uniqueResult());

        // Existe-il au moins un résultat ?
        return resultat > 0;
    }

}