/*
 * Copyright (c) 2016 DGFiP - Tous droits réservés
 * 
 */
package fr.gouv.finances.lombok.bancaire.dao.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.gouv.finances.lombok.bancaire.bean.GuichetBancaire;
import fr.gouv.finances.lombok.bancaire.dao.GuichetBancaireDao;
import fr.gouv.finances.lombok.test.controle.ControleDonnees;
import fr.gouv.finances.lombok.test.controle.VerificationExecution;
import fr.gouv.finances.lombok.util.base.PurgeTablesDao;
import fr.gouv.finances.lombok.util.date.TemporaliteUtils;
import fr.gouv.finances.lombok.util.exception.ProgrammationException;

/**
 * Tests unitaires automatisés du DAO gérant l'intéraction avec le guichet bancaire
 *
 * @author chouard
 * @author Christophe Breheret-Girardin
 * @author CF
 */
@ContextConfiguration({"classpath:/conf/applicationContext-bancaire-hibernate-test.xml",
        "classpath*:/conf/applicationContext-core-persistance-test.xml",
        "classpath*:/conf/applicationContext-core-orm-test.xml",
        "classpath*:/conf/applicationContext-bancaire-dao.xml"})
@ActiveProfiles(profiles = {"bancaire", "hibernate"})
@RunWith(SpringJUnit4ClassRunner.class)
public class GuichetBancaireDaoImplTest
{

    public GuichetBancaireDaoImplTest()
    {
        super();
    }

    protected static final Logger LOGGER = LoggerFactory.getLogger(GuichetBancaireDaoImplTest.class);

    /** Déclaration du DAO gérant les données du guichet bancaire. */
    @Autowired
    private GuichetBancaireDao guichetbancairedao;

    /** Déclaration du DAO de gestion des purges de la base. */
    @Autowired
    private PurgeTablesDao purgetablesdao;

    /** Map contenant guichets bancaires présents en base */
    private Map<Integer, GuichetBancaire> mapGuichetBancaire = new HashMap<>();

    /** Constante d'un 1er code guichet. */
    private static final String CODE_GUICHET_1 = "1";

    /** Constante d'un 2ème code guichet. */
    private static final String CODE_GUICHET_2 = "2";

    /** Constante d'un 3ème code guichet. */
    private static final String CODE_GUICHET_3 = "3";

    /** Constante d'un 1er code établissement. */
    private static final String CODE_ETABLISSEMENT_1 = "code_etablissement_1";

    /** Constante d'un 2ème code établissement. */
    private static final String CODE_ETABLISSEMENT_2 = "code_etablissement_2";

    /** Constante d'un 3ème code commune. */
    private static final String CODE_COMMUNE_1 = "ville_code_postal_1";

    /** Constante d'un 1er code commune. */
    private static final String CODE_COMMUNE_2 = "ville_code_postal_2";

    /** Constante d'un 1er code postal. */
    private static final String CODE_POSTAL_1 = "75001";

    /** Constante d'un 1er code postal. */
    private static final String CODE_POSTAL_2 = "75002";

    /** Constante d'une 1ere dénomination complète. */
    private static final String DENOMINATION_COMPLETE_1 = "denomination_complete_1";

    /** Constante d'un 2ème dénomination complète. */
    private static final String DENOMINATION_COMPLETE_2 = "denomination_complete_2";

    /**
     * Initialisation des données en base avant les tests.
     */
    @Before
    public void creerDonnees()
    {
        // Purge des données de la base
        purgetablesdao.purgerTables("ZGUICHETBANC_ZGUI");

        // Vérification de l'absence de données
        ControleDonnees.verifierElements(guichetbancairedao.loadAllObjects(GuichetBancaire.class));

        // Génération des traces de purge d'édition, insertion en base de données et sauvegarde dans une Map
        mapGuichetBancaire = IntStream.rangeClosed(1, 3).mapToObj(this::getGuichetBancaire)
            .peek(guichetbancairedao::saveObject)
            .collect(Collectors.toMap(g -> Integer.parseInt(g.getCodeGuichet()), g -> g));

        // Vérification avant exécution du test
        ControleDonnees.verifierElements(guichetbancairedao.loadAllObjects(GuichetBancaire.class), mapGuichetBancaire.get(1),
            mapGuichetBancaire.get(2), mapGuichetBancaire.get(3));
    }

    /**
     * Méthode permettant de tester l'injection du DAO.
     */
    @Test
    public final void testBancaireDao()
    {
        assertNotNull("DAO non injecté dans le test", guichetbancairedao);
    }

    /**
     * Test de la méthode
     * {@link fr.gouv.finances.lombok.bancaire.dao.impl.bancairedaoImpl#deleteTousLesGuichetsBancaires()}.
     */
    @Test
    public final void testDeleteTousLesGuichetsBancaires()
    {
        // Vérification des données suite à l'exécution de la méthode à tester
        guichetbancairedao.deleteTousLesGuichetsBancaires();
        ControleDonnees.verifierElements(guichetbancairedao.loadAllObjects(GuichetBancaire.class));
    }

    /**
     * Test de la méthode
     * {@link fr.gouv.finances.lombok.bancaire.dao.impl.bancairedaoImpl#findGuichetBancaireNonPerimeParCodeGuichetEtCodeEtablissement(java.lang.String, java.lang.String)}.
     */
    @Test
    public final void testFindGuichetBancaireNonPerimeParCodeGuichetEtCodeEtablissement()
    {
        // Exécution de la méthode à tester sans aucun paramètre fourni
        assertNull(guichetbancairedao.findGuichetBancaireNonPerimeParCodeGuichetEtCodeEtablissement(null, null));
        assertNull(guichetbancairedao.findGuichetBancaireNonPerimeParCodeGuichetEtCodeEtablissement("", ""));

        // Exécution de la méthode à tester sans résultat
        assertNull(guichetbancairedao
            .findGuichetBancaireNonPerimeParCodeGuichetEtCodeEtablissement(CODE_GUICHET_1, CODE_ETABLISSEMENT_2));
        assertNull(guichetbancairedao
            .findGuichetBancaireNonPerimeParCodeGuichetEtCodeEtablissement(CODE_GUICHET_2, CODE_ETABLISSEMENT_2));
        assertNull(guichetbancairedao
            .findGuichetBancaireNonPerimeParCodeGuichetEtCodeEtablissement(CODE_GUICHET_3, CODE_ETABLISSEMENT_1));

        // Exécution de la méthode à tester sur toutes les données
        assertEquals(guichetbancairedao.findGuichetBancaireNonPerimeParCodeGuichetEtCodeEtablissement(
            CODE_GUICHET_1, CODE_ETABLISSEMENT_1), mapGuichetBancaire.get(1));
    }

    /**
     * Test de la méthode
     * {@link fr.gouv.finances.lombok.bancaire.dao.impl.bancairedaoImpl#findGuichetsBancaireParCommuneEtCodeEtablissement(java.lang.String, java.lang.String)}.
     */
    @Test
    public final void testFindGuichetsBancaireParCommuneEtCodeEtablissement()
    {
        // Exécution de la méthode à tester sans aucun paramètre fourni
        ControleDonnees.verifierElements(
            guichetbancairedao.findGuichetsBancaireParCommuneEtCodeEtablissement(null, null));
        ControleDonnees.verifierElements(
            guichetbancairedao.findGuichetsBancaireParCommuneEtCodeEtablissement("", ""));

        // Exécution de la méthode à tester sans résultat
        ControleDonnees.verifierElements(guichetbancairedao.findGuichetsBancaireParCommuneEtCodeEtablissement(
            CODE_COMMUNE_1, CODE_ETABLISSEMENT_2));

        // Exécution de la méthode à tester avec résultat
        ControleDonnees.verifierElements(guichetbancairedao
            .findGuichetsBancaireParCommuneEtCodeEtablissement(CODE_COMMUNE_1, CODE_ETABLISSEMENT_1), mapGuichetBancaire.get(1),
            mapGuichetBancaire.get(3));
    }

    /**
     * Test de la méthode
     * {@link fr.gouv.finances.lombok.bancaire.dao.impl.bancairedaoImpl#findGuichetsBancaireParDenominationCompleteBanqueEtCodeEtablissement(java.lang.String, java.lang.String)}.
     */
    @Test
    public final void testFindGuichetsBancaireParDenominationCompleteBanqueEtCodeEtablissement()
    {
        // Exécution de la méthode à tester sans aucun paramètre fourni
        ControleDonnees.verifierElements(guichetbancairedao
            .findGuichetsBancaireParDenominationCompleteBanqueEtCodeEtablissement(null, null));
        ControleDonnees.verifierElements(guichetbancairedao
            .findGuichetsBancaireParDenominationCompleteBanqueEtCodeEtablissement("", ""));

        // Exécution de la méthode à tester sans résultat
        ControleDonnees.verifierElements(guichetbancairedao.findGuichetsBancaireParDenominationCompleteBanqueEtCodeEtablissement(
            DENOMINATION_COMPLETE_1, CODE_ETABLISSEMENT_2));

        // Exécution de la méthode à tester avec résultat
        ControleDonnees.verifierElements(guichetbancairedao
            .findGuichetsBancaireParDenominationCompleteBanqueEtCodeEtablissement(
                DENOMINATION_COMPLETE_1, CODE_ETABLISSEMENT_1),
            mapGuichetBancaire.get(1), mapGuichetBancaire.get(3));
    }

    /**
     * Test de la méthode
     * {@link fr.gouv.finances.lombok.bancaire.dao.impl.bancairedaoImpl#findGuichetsBancairesNonPerimes()}.
     */
    @Test
    public final void testFindGuichetsBancairesNonPerimes()
    {
        // Vérifications des données suite à l'exécution de la méthode à tester
        ControleDonnees.verifierElements(
            guichetbancairedao.findGuichetsBancairesNonPerimes(), mapGuichetBancaire.get(1));
    }

    /**
     * Test de la méthode
     * {@link fr.gouv.finances.lombok.bancaire.dao.impl.bancairedaoImpl#findGuichetsBancairesParCodeEtablissement(java.lang.String)}.
     */
    @Test
    public final void testFindGuichetsBancairesParCodeEtablissement()
    {
        // Exécution de la méthode à tester sans aucun paramètre fourni
        VerificationExecution.verifierException(ProgrammationException.class,
            () -> guichetbancairedao.findGuichetsBancairesParCodeEtablissement(null));
        VerificationExecution.verifierException(ProgrammationException.class,
            () -> guichetbancairedao.findGuichetsBancairesParCodeEtablissement(""));

        // Exécution de la méthode à tester sans résultat
        ControleDonnees.verifierElements(
            guichetbancairedao.findGuichetsBancairesParCodeEtablissement(CODE_GUICHET_1));

        // Exécution de la méthode à tester avec résultat
        ControleDonnees.verifierElements(
            guichetbancairedao.findGuichetsBancairesParCodeEtablissement(CODE_ETABLISSEMENT_1), mapGuichetBancaire.get(1),
            mapGuichetBancaire.get(3));
    }

    /**
     * Test de la méthode
     * {@link fr.gouv.finances.lombok.bancaire.dao.impl.bancairedaoImpl#findGuichetsBancairesParCodeGuichetEtCodeEtablissement(java.lang.String, java.lang.String)}.
     */
    @Test
    public final void testFindGuichetsBancairesParCodeGuichetEtCodeEtablissement()
    {
        // Exécution de la méthode à tester sans aucun paramètre fourni
        ControleDonnees.verifierElements(
            guichetbancairedao.findGuichetsBancairesParCodeGuichetEtCodeEtablissement(null, null));
        ControleDonnees.verifierElements(
            guichetbancairedao.findGuichetsBancairesParCodeGuichetEtCodeEtablissement("", ""));

        // Exécution de la méthode à tester sans résultat
        ControleDonnees.verifierElements(
            guichetbancairedao.findGuichetsBancairesParCodeGuichetEtCodeEtablissement(
                CODE_GUICHET_1, CODE_ETABLISSEMENT_2));

        // Exécution de la méthode à tester avec résultat
        ControleDonnees.verifierElements(
            guichetbancairedao.findGuichetsBancairesParCodeGuichetEtCodeEtablissement(
                CODE_GUICHET_1, CODE_ETABLISSEMENT_1),
            mapGuichetBancaire.get(1));
    }

    /**
     * Test de la méthode
     * {@link fr.gouv.finances.lombok.bancaire.dao.impl.bancairedaoImpl#findGuichetsBancairesParCodePostal(java.lang.String)}.
     */
    @Test
    public final void testFindGuichetsBancairesParCodePostal()
    {
        // Exécution de la méthode à tester sans aucun paramètre fourni
        VerificationExecution.verifierException(ProgrammationException.class,
            () -> guichetbancairedao.findGuichetsBancairesParCodePostal(null));
        VerificationExecution.verifierException(ProgrammationException.class,
            () -> guichetbancairedao.findGuichetsBancairesParCodePostal(""));

        // Exécution de la méthode à tester sans résultat
        ControleDonnees.verifierElements(guichetbancairedao.findGuichetsBancairesParCodePostal(CODE_GUICHET_1));

        // Exécution de la méthode à tester avec résultat
        ControleDonnees.verifierElements(guichetbancairedao.findGuichetsBancairesParCodePostal(CODE_POSTAL_1), mapGuichetBancaire.get(1),
            mapGuichetBancaire.get(3));
    }

    /**
     * Test de la méthode
     * {@link fr.gouv.finances.lombok.bancaire.dao.impl.bancairedaoImpl#findGuichetsBancairesParCommune(java.lang.String)}.
     */
    @Test
    public final void testFindGuichetsBancairesParCommune()
    {
        // Exécution de la méthode à tester sans aucun paramètre fourni
        VerificationExecution.verifierException(ProgrammationException.class,
            () -> guichetbancairedao.findGuichetsBancairesParCommune(null));
        VerificationExecution.verifierException(ProgrammationException.class, () -> guichetbancairedao.findGuichetsBancairesParCommune(""));

        // Exécution de la méthode à tester sans résultat
        ControleDonnees.verifierElements(guichetbancairedao.findGuichetsBancairesParCommune(CODE_GUICHET_1));

        // Exécution de la méthode à tester avec résultat
        ControleDonnees.verifierElements(guichetbancairedao.findGuichetsBancairesParCommune(CODE_COMMUNE_1), mapGuichetBancaire.get(1),
            mapGuichetBancaire.get(3));
    }

    /**
     * Test de la méthode
     * {@link fr.gouv.finances.lombok.bancaire.dao.impl.bancairedaoImpl#findGuichetsBancairesParDenominationCompleteBanque(java.lang.String)}.
     */
    @Test
    public final void testFindGuichetsBancairesParDenominationCompleteBanque()
    {
        // Exécution de la méthode à tester sans aucun paramètre fourni
        VerificationExecution.verifierException(ProgrammationException.class,
            () -> guichetbancairedao.findGuichetsBancairesParDenominationCompleteBanque(null));
        VerificationExecution.verifierException(ProgrammationException.class,
            () -> guichetbancairedao.findGuichetsBancairesParDenominationCompleteBanque(""));

        // Exécution de la méthode à tester sans résultat
        ControleDonnees.verifierElements(
            guichetbancairedao.findGuichetsBancairesParDenominationCompleteBanque(CODE_GUICHET_1));

        // Exécution de la méthode à tester avec résultat
        ControleDonnees.verifierElements(guichetbancairedao.findGuichetsBancairesParDenominationCompleteBanque(
            DENOMINATION_COMPLETE_1), mapGuichetBancaire.get(1), mapGuichetBancaire.get(3));
    }

    /**
     * Test de la méthode {@link fr.gouv.finances.lombok.bancaire.dao.impl.bancairedaoImpl#findTousGuichetsBancaires()}.
     */
    @Test
    public final void testFindTousGuichetsBancaires()
    {
        // Vérification des données suite à l'exécution de la méthode à tester
        ControleDonnees.verifierElements(guichetbancairedao.findTousGuichetsBancaires(), mapGuichetBancaire.get(1),
            mapGuichetBancaire.get(2), mapGuichetBancaire.get(3));
    }

    /**
     * Test de la méthode
     * {@link fr.gouv.finances.lombok.bancaire.dao.impl.bancairedaoImpl#marqueLesGuichetsBancairesPerimesADate(java.util.Date)}.
     */
    @Test
    public final void testMarqueLesGuichetsBancairesPerimesADate()
    {
        // Vérification des données avant le test, le guichet 1 est non périmé
        ControleDonnees.verifierElements(guichetbancairedao.findGuichetsBancairesNonPerimes(), mapGuichetBancaire.get(1));

        // Exécution de la méthode marquant le guichet 1 comme périmé
        guichetbancairedao.marqueLesGuichetsBancairesPerimesADate(
            TemporaliteUtils.ajouterJour(Calendar.getInstance().getTime(), 10));

        // Vérification des modifications
        ControleDonnees.verifierElements(guichetbancairedao.findGuichetsBancairesNonPerimes());
    }

    /**
     * Test de la méthode
     * {@link fr.gouv.finances.lombok.bancaire.dao.impl.bancairedaoImpl#testerExistenceGuichetBancaireNonPerimeParCodeGuichetEtCodeEtablissement(java.lang.String, java.lang.String)}.
     */
    @Test
    public final void testExistenceGuichetBancaireNonPerimeParCodeGuichetEtCodeEtablissement()
    {
        // Exécution de la méthode à tester sans aucun paramètre fourni
        assertFalse("Un guichet Bancaire a été trouvé",
            guichetbancairedao.testeExistenceGuichetBancaireNonPerimeParCodeGuichetEtCodeEtablissement(
                "", ""));
        assertFalse("Un guichet Bancaire a été trouvé",
            guichetbancairedao.testeExistenceGuichetBancaireNonPerimeParCodeGuichetEtCodeEtablissement(
                null, null));

        // Exécution de la méthode à tester sans résultat
        assertFalse("Un guichet Bancaire a été trouvé",
            guichetbancairedao.testeExistenceGuichetBancaireNonPerimeParCodeGuichetEtCodeEtablissement(
                CODE_GUICHET_1, CODE_ETABLISSEMENT_2));
        assertFalse("Un guichet Bancaire a été trouvé",
            guichetbancairedao.testeExistenceGuichetBancaireNonPerimeParCodeGuichetEtCodeEtablissement(
                CODE_GUICHET_2, CODE_ETABLISSEMENT_2));
        assertFalse("Un guichet Bancaire a été trouvé",
            guichetbancairedao.testeExistenceGuichetBancaireNonPerimeParCodeGuichetEtCodeEtablissement(
                CODE_GUICHET_3, CODE_ETABLISSEMENT_1));

        // Exécution de la méthode à tester avec résultat
        assertTrue("Le guichet Bancaire n'a pas été trouvé",
            guichetbancairedao.testeExistenceGuichetBancaireNonPerimeParCodeGuichetEtCodeEtablissement(
                CODE_GUICHET_1, CODE_ETABLISSEMENT_1));
    }

    /**
     * Test de la méthode
     * {@link fr.gouv.finances.lombok.bancaire.dao.impl.bancairedaoImpl#testerExistenceGuichetBancairePerimeOuNonParCodeGuichetEtCodeEtablissement(java.lang.String, java.lang.String)}.
     */
    @Test
    public final void testTesteExistenceGuichetBancairePerimeOuNonParCodeGuichetEtCodeEtablissement()
    {
        // Exécution de la méthode à tester sans aucun paramètre fourni
        assertFalse("Au moins un guichet Bancaire a été trouvé",
            guichetbancairedao.testeExistenceGuichetBancairePerimeOuNonParCodeGuichetEtCodeEtablissement(
                "", ""));
        assertFalse("Au moins un guichet Bancaire a été trouvé",
            guichetbancairedao.testeExistenceGuichetBancairePerimeOuNonParCodeGuichetEtCodeEtablissement(
                null, null));

        // Exécution de la méthode à tester sans résultat
        assertFalse("Au moins un guichet Bancaire a été trouvé",
            guichetbancairedao.testeExistenceGuichetBancairePerimeOuNonParCodeGuichetEtCodeEtablissement(
                CODE_GUICHET_1, CODE_ETABLISSEMENT_2));

        // Exécution de la méthode à tester avec résultat
        assertTrue("Aucun guichet Bancaire n'a pas été trouvé",
            guichetbancairedao.testeExistenceGuichetBancairePerimeOuNonParCodeGuichetEtCodeEtablissement(
                CODE_GUICHET_1, CODE_ETABLISSEMENT_1));
        assertTrue("Aucun guichet Bancaire n'a pas été trouvé",
            guichetbancairedao.testeExistenceGuichetBancairePerimeOuNonParCodeGuichetEtCodeEtablissement(
                CODE_GUICHET_3, CODE_ETABLISSEMENT_1));
    }

    /**
     * Méthode permettant de générer un guichet bancaire
     *
     * @param indice indice du guichet
     * @return le guichet bancaire généré
     */
    private GuichetBancaire getGuichetBancaire(int indice)
    {
        GuichetBancaire bancaire = new GuichetBancaire();
        bancaire.setCodeGuichet(String.valueOf(indice));

        switch (indice)
        {
            case 1:
                bancaire.setCodeEtablissement(CODE_ETABLISSEMENT_1);
                bancaire.setVilleCodePostal(CODE_COMMUNE_1);
                bancaire.setCodePostal(CODE_POSTAL_1);
                bancaire.setDenominationComplete(DENOMINATION_COMPLETE_1);
                bancaire.setEstEnVoieDePeremption(true);
                bancaire.setEstPerime(false);
                break;
            case 2:
                bancaire.setCodeEtablissement(CODE_ETABLISSEMENT_2);
                bancaire.setVilleCodePostal(CODE_COMMUNE_2);
                bancaire.setCodePostal(CODE_POSTAL_2);
                bancaire.setDenominationComplete(DENOMINATION_COMPLETE_2);
                bancaire.setEstEnVoieDePeremption(false);
                bancaire.setEstPerime(true);
                break;
            case 3:
                bancaire.setCodeEtablissement(CODE_ETABLISSEMENT_1);
                bancaire.setVilleCodePostal(CODE_COMMUNE_1);
                bancaire.setCodePostal(CODE_POSTAL_1);
                bancaire.setDenominationComplete(DENOMINATION_COMPLETE_1);
                bancaire.setEstEnVoieDePeremption(false);
                bancaire.setEstPerime(true);
                break;
        }

        bancaire.setAnneeDeModification("2017");
        bancaire.setCodeEnregistrement("code_enregistrement_" + indice);
        bancaire.setAdresseGuichet("adresse_guichet_" + indice);
        bancaire.setCodeReglementDesCredits("code_reglement_credits_" + indice);
        bancaire.setDateDerniereMiseAJour(new Timestamp(Calendar.getInstance().getTimeInMillis()));
        bancaire.setDenominationAbregee("denomination_abregee_" + indice);
        bancaire.setLibelleAbrevDeDomiciliation("libelle_abrev_de_domiciliation_" + indice);
        bancaire.setMoisDeModification("12");
        bancaire.setNomGuichet("nom_guichet_" + indice);
        bancaire.setNouveauCodeEtablissement("NVET");
        bancaire.setNouveauCodeGuichet("TOT");

        return bancaire;
    }

}
