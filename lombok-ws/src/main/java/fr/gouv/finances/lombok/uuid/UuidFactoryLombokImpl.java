/*
 * Copyright (c) 2016 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.uuid;

import java.util.UUID;

import fr.gouv.impots.appli.commun.frameworks.gestionuuid.transverse.UuidFactoryInterface;

/**
 * Class UuidFactoryLombokImpl.
 */
public class UuidFactoryLombokImpl implements UuidFactoryInterface
{

    public UuidFactoryLombokImpl()
    {
        super();        
    }

    /** 
     * (methode de remplacement)
     * {@inheritDoc}
     * @see fr.gouv.impots.appli.commun.frameworks.gestionuuid.transverse.UuidFactoryInterface#genererUUID()
     */
    @Override
    public byte[] genererUUID()
    {
        return UUID.randomUUID().toString().getBytes();
    }

}
