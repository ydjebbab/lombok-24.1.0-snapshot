/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : SlpaUtil.java
 *
 */
package fr.gouv.finances.lombok.util.ws;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.net.URL;
import java.util.Map;

import fr.gouv.finances.dgi.spa.slpa.LocatorService;
import fr.gouv.finances.dgi.spa.slpa.LocatorServiceDefaultImpl;
import fr.gouv.finances.dgi.spa.slpa.exception.SPAApplicationException;
import fr.gouv.finances.dgi.spa.slpa.exception.SPASystemException;
import fr.gouv.finances.lombok.util.exception.ProgrammationException;
import fr.gouv.impots.appli.commun.frameworks.gestionappelsservices.transverse.ServiceLocatorInterface;

/**
 * Class SlpaUtil.
 * 
 * @author ageffroy2-cp Cette classe permet de connaître la localisation d'un service web et du wsdl correspondant par
 *         l'intermédiaire du SLPA qui est le cache local de l'annuaire de services web Vespa.
 * @version $Revision: 1.4 $ Date: 15 déc. 2009
 */
public class SlpaUtilStandalone implements ServiceLocatorInterface
{

    private static String PARAMETRE_CONTRAT = "contrat";

    public SlpaUtilStandalone()
    {
        super();
    }

    /**
     * methode Rechercher localisation service : --.
     * 
     * @param contrat --
     * @param service --
     * @return uRL
     */
    @Override
    public URL getAccessPoint(String service, String contrat)
    {
        try
        {
            LocatorService locatorService = getLocatorService();

            Map<String, String> req = new java.util.HashMap<>();
            req.put(PARAMETRE_CONTRAT, contrat);

            // Récupération de l'url d'accès au service
            return locatorService.getAccessPoint(service, req);
        }
        catch (SPAApplicationException | SPASystemException e)
        {
            throw new ProgrammationException(e);
        }
    }

    /**
     * Accesseur de l attribut locator service.
     * 
     * @return locator service
     */
    private LocatorService getLocatorService()
    {
        LocatorServiceDefaultImpl locatorService;
        try
        {
            String slpaDir = "src/main/resources/slpa";
            locatorService = new LocatorServiceDefaultImpl();
            locatorService.setConfiguration(new FileInputStream(new File(slpaDir + "/slpa-config.xml")));
        }
        catch (FileNotFoundException | SPAApplicationException | SPASystemException e)
        {
            throw new ProgrammationException(e);
        }
        return locatorService;
    }

}
