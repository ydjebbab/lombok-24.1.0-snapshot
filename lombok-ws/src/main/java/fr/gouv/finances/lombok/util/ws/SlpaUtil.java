/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : SlpaUtil.java
 *
 */
package fr.gouv.finances.lombok.util.ws;

import java.net.URL;
import java.util.Map;

import javax.naming.NamingException;

import org.springframework.jndi.JndiTemplate;

import fr.gouv.finances.dgi.spa.slpa.LocatorService;
import fr.gouv.finances.dgi.spa.slpa.exception.SPAApplicationException;
import fr.gouv.finances.dgi.spa.slpa.exception.SPASystemException;
import fr.gouv.finances.lombok.util.exception.ProgrammationException;

/**
 * Class SlpaUtil.
 * 
 * @author ageffroy2-cp Cette classe permet de connaître la localisation d'un service web et du wsdl correspondant par
 *         l'intermédiaire du SLPA qui est le cache local de l'annuaire de services web Vespa.
 * @version $Revision: 1.4 $ Date: 15 déc. 2009
 */
public class SlpaUtil
{

    /** PARAMETR e_ contrat. */
    private static String PARAMETRE_CONTRAT = "contrat";

    /** BEA n_ locator. */
    private static String BEAN_LOCATOR = "java:comp/env/bean/Locator";

    /**
     * methode Rechercher localisation service : --.
     * 
     * @param contrat --
     * @param service --
     * @return uRL
     */
    public static URL rechercherLocalisationService(String contrat, String service)
    {
        try
        {
            LocatorService locatorService = getLocatorService();

            Map<String, String> req = new java.util.HashMap<>();
            req.put(PARAMETRE_CONTRAT, contrat);

            // Récupération de l'url d'accès au service
            return locatorService.getAccessPoint(service, req);
        }
        catch (SPAApplicationException | SPASystemException e)
        {
            throw new ProgrammationException(e);
        }
    }

    /**
     * methode Rechercher localisation wsdl : --.
     * 
     * @param contrat --
     * @param service --
     * @return uRL
     */
    public static URL rechercherLocalisationWSDL(String contrat, String service)
    {
        try
        {
            LocatorService locatorService = getLocatorService();

            Map<String, String> req = new java.util.HashMap<>();
            req.put(PARAMETRE_CONTRAT, contrat);

            // Récupération de l'url d'accès au wsdl
            return locatorService.getWsdlUrl(service, req);
        }
        catch (SPAApplicationException | SPASystemException e)
        {
            throw new ProgrammationException(e);
        }
    }

    /**
     * Accesseur de l attribut locator service.
     * 
     * @return locator service
     */
    private static LocatorService getLocatorService()
    {
        LocatorService locatorService;
        try
        {
            JndiTemplate template = new JndiTemplate();
            locatorService = (LocatorService) template.lookup(BEAN_LOCATOR);
        }
        catch (NamingException e)
        {
            throw new ProgrammationException(e);
        }
        return locatorService;
    }

}
