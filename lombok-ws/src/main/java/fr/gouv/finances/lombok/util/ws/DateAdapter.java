/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : DateAdapter.java
 *
 */
package fr.gouv.finances.lombok.util.ws;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;

import fr.gouv.finances.lombok.util.exception.ProgrammationException;

/**
 * Class DateAdapter --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.2 $ Date: 11 déc. 2009
 */
public class DateAdapter extends XmlAdapter<String, Date>
{

    public DateAdapter()
    {
        super();        
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param value
     * @return string
     * @see javax.xml.bind.annotation.adapters.XmlAdapter#marshal(java.lang.Object)
     */
    @Override
    public String marshal(Date value)
    {
        String result;
        if (value == null)
        {
            result = null;
        }
        else
        {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.FRENCH);
            dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
            result = dateFormat.format(value);
        }
        return result;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param value
     * @return date
     * @see javax.xml.bind.annotation.adapters.XmlAdapter#unmarshal(java.lang.Object)
     */
    @Override
    public Date unmarshal(String value)
    {
        Date resultat = null;
        try
        {
            resultat = DatatypeFactory.newInstance().newXMLGregorianCalendar(value).toGregorianCalendar().getTime();
        }
        catch (DatatypeConfigurationException e)
        {
            throw new ProgrammationException(e);
        }
        return resultat;
    }
}
