/*
 * Copyright (c) 2016 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.test.uuid;

import static org.junit.Assert.*;

import org.junit.Test;

import fr.gouv.finances.lombok.uuid.UuidFactoryLombokImpl;

public class TestUuidFactoryLombokImpl
{

    /**
     * methode testgenererUUIDnotNull : test instanciation et resultat non null
     */
    @Test
    public void testgenererUUIDnotNull()
    {
        UuidFactoryLombokImpl fact = new UuidFactoryLombokImpl();
        byte[] res = fact.genererUUID();
        assertNotNull(res);
    }

}
