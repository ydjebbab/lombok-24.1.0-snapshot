/*
 * Copyright (c) 2020 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.bancaire.jpa.dao.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import fr.gouv.finances.lombok.bancaire.bean.GuichetBancaire;
import fr.gouv.finances.lombok.bancaire.dao.GuichetBancaireDao;
import fr.gouv.finances.lombok.bancaire.dao.ProprieteGuichetBancaire;
import fr.gouv.finances.lombok.jpa.dao.BaseDaoJpaImpl;
import fr.gouv.finances.lombok.util.persistance.ModeCritereRecherche;

/**
 * Implémentation JPA du DAO permettant de gérer les données des guichets bancaires.
 *
 * @author Christophe Breheret-Girardin
 */
@ConditionalOnProperty(name = "lombok.orm.jpa", havingValue="true")
@Repository("guichetbancairedaoimpl")
@Transactional(transactionManager = "transactionManager")
public class GuichetBancaireDaoImpl extends BaseDaoJpaImpl implements GuichetBancaireDao
{
    protected static final Logger LOGGER = LoggerFactory.getLogger(GuichetBancaireDaoImpl.class);

    public GuichetBancaireDaoImpl()
    {
        super();
    }

    /**
     * {@inheritDoc}
     *
     * @see fr.gouv.finances.lombok.bancaire.dao.GuichetBancaireDao#deleteTousLesGuichetsBancaires().
     */
    @Override
    public void deleteTousLesGuichetsBancaires()
    {
        // Journalisation de la suppression
        LOGGER.trace("Suppression de tous les guichets bancaires");

        // Suppression des guichets bancaires
        entityManager.createQuery("delete from " + GuichetBancaire.class.getSimpleName()).executeUpdate();
    }

    /**
     * {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.bancaire.dao.GuichetBancaireDao#findGuichetBancaireNonPerimeParCodeGuichetEtCodeEtablissement(
     *      java.lang.String, java.lang.String).
     */
    @Override
    public GuichetBancaire findGuichetBancaireNonPerimeParCodeGuichetEtCodeEtablissement(
        String codeGuichet, String codeEtablissement)
    {
        // Journalisation de la recherche
        LOGGER.debug("Recherche du guichet bancaire, non périmé, de code '{}', d'établissement '{}'", codeGuichet, codeEtablissement);

        // Paramétrage de la recherche
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<GuichetBancaire> query = criteriaBuilder.createQuery(GuichetBancaire.class);
        Root<GuichetBancaire> root = query.from(GuichetBancaire.class);

        // Paramétrage des critères
        query.where(criteriaBuilder.equal(
            root.<String> get(ProprieteGuichetBancaire.CODE_ETABLISSEMENT.getNom()), codeEtablissement),
            criteriaBuilder.equal(root.<String> get(ProprieteGuichetBancaire.CODE_GUICHET.getNom()), codeGuichet),
            criteriaBuilder.equal(root.<Boolean> get(ProprieteGuichetBancaire.EST_PERIME.getNom()), false));

        // Exécution de la recherche
        return find(query);
    }

    /**
     * {@inheritDoc}
     *
     * @see fr.gouv.finances.lombok.bancaire.dao.GuichetBancaireDao#findGuichetsBancaireParCommuneEtCodeEtablissement(
     *      java.lang.String, java.lang.String).
     */
    @Override
    public List<GuichetBancaire> findGuichetsBancaireParCommuneEtCodeEtablissement(
        String commune, String codeEtablissement)
    {
        LOGGER.debug("Recherche des guichets bancaires, liés à la commune '{}' et l'établissement '{}'", commune, codeEtablissement);

        // Préparation des critères
        Map<String, String> criteres = new HashMap<>();
        criteres.put(ProprieteGuichetBancaire.COMMUNE.getNom(), commune);
        criteres.put(ProprieteGuichetBancaire.CODE_ETABLISSEMENT.getNom(), codeEtablissement);

        // Recherche des guichets bancaires
        return findAllByCriterias(GuichetBancaire.class, criteres, ModeCritereRecherche.EQUAL);
    }

    /**
     * {@inheritDoc}
     *
     * @see fr.gouv.finances.lombok.bancaire.dao.GuichetBancaireDao#findGuichetsBancaireParDenominationCompleteBanqueEtCodeEtablissement(
     *      java.lang.String, java.lang.String).
     */
    @Override
    public List<GuichetBancaire> findGuichetsBancaireParDenominationCompleteBanqueEtCodeEtablissement(String denominationComplete,
        String codeEtablissement)
    {
        LOGGER.debug("Recherche des guichets bancaires par dénomination complète '{}', liés à l'établissement '{}'", denominationComplete,
            codeEtablissement);

        // Préparation des critères
        Map<String, String> criteres = new HashMap<>();
        criteres.put(ProprieteGuichetBancaire.DENOMINATION_COMPLETE.getNom(), denominationComplete);
        criteres.put(ProprieteGuichetBancaire.CODE_ETABLISSEMENT.getNom(), codeEtablissement);

        // Recherche des guichets bancaires
        return findAllByCriterias(GuichetBancaire.class, criteres, ModeCritereRecherche.EQUAL);
    }

    /**
     * {@inheritDoc}
     *
     * @see fr.gouv.finances.lombok.bancaire.dao.GuichetBancaireDao#findGuichetsBancairesNonPerimes().
     */
    @Override
    public List<GuichetBancaire> findGuichetsBancairesNonPerimes()
    {
        LOGGER.trace("Recherche des guichets bancaires non périmés");

        // Paramétrage de la recherche
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<GuichetBancaire> query = criteriaBuilder.createQuery(GuichetBancaire.class);
        Root<GuichetBancaire> root = query.from(GuichetBancaire.class);

        // Paramétrage des critères
        query.where(criteriaBuilder.equal(root.<Boolean> get(ProprieteGuichetBancaire.EST_PERIME.getNom()), false));

        // Exécution de la recherche
        return findAll(query);
    }

    /**
     * {@inheritDoc}
     *
     * @see fr.gouv.finances.lombok.bancaire.dao.GuichetBancaireDao#findGuichetsBancairesParCodeEtablissement(java.lang.String).
     */
    @Override
    public List<GuichetBancaire> findGuichetsBancairesParCodeEtablissement(String codeEtablissement)
    {
        LOGGER.debug("Recherche des guichets bancaires liés à l'établissement '{}'", codeEtablissement);

        // Recherche des guichets bancaires
        return findAllByUniqueCriteria(GuichetBancaire.class, ProprieteGuichetBancaire.CODE_ETABLISSEMENT.getNom(), codeEtablissement);
    }

    /**
     * {@inheritDoc}
     *
     * @see fr.gouv.finances.lombok.bancaire.dao.GuichetBancaireDao#findGuichetsBancairesParCodeGuichetEtCodeEtablissement(
     *      java.lang.String, java.lang.String).
     */
    @Override
    public List<GuichetBancaire> findGuichetsBancairesParCodeGuichetEtCodeEtablissement(
        String codeGuichet, String codeEtablissement)
    {
        LOGGER.debug("Recherche des guichets bancaires de code '{}', liés à l'établissement '{}'", codeGuichet, codeEtablissement);

        // Préparation des critères
        Map<String, String> criteres = new HashMap<>();
        criteres.put(ProprieteGuichetBancaire.CODE_GUICHET.getNom(), codeGuichet);
        criteres.put(ProprieteGuichetBancaire.CODE_ETABLISSEMENT.getNom(), codeEtablissement);

        // Recherche des guichets bancaires
        return findAllByCriterias(GuichetBancaire.class, criteres, ModeCritereRecherche.EQUAL);
    }

    /**
     * {@inheritDoc}
     *
     * @see fr.gouv.finances.lombok.bancaire.dao.GuichetBancaireDao#findGuichetsBancairesParCodePostal(java.lang.String).
     */
    @Override
    public List<GuichetBancaire> findGuichetsBancairesParCodePostal(String codePostal)
    {
        LOGGER.debug("Recherche des guichets bancaires de code postal '{}'", codePostal);

        // Recherche des guichets bancaires
        return findAllByUniqueCriteria(GuichetBancaire.class, ProprieteGuichetBancaire.CODE_POSTAL.getNom(), codePostal);
    }

    /**
     * {@inheritDoc}
     *
     * @see fr.gouv.finances.lombok.bancaire.dao.GuichetBancaireDao#findGuichetsBancairesParCommune(java.lang.String).
     */
    @Override
    public List<GuichetBancaire> findGuichetsBancairesParCommune(String commune)
    {
        LOGGER.debug("Recherche des guichets bancaires liés à la commune '{}'", commune);

        // Recherche des guichets bancaires
        return findAllByUniqueCriteria(GuichetBancaire.class, ProprieteGuichetBancaire.COMMUNE.getNom(), commune);
    }

    /**
     * {@inheritDoc}
     *
     * @see fr.gouv.finances.lombok.bancaire.dao.GuichetBancaireDao#findGuichetsBancairesParDenominationCompleteBanque(java.lang.String).
     */
    @Override
    public List<GuichetBancaire> findGuichetsBancairesParDenominationCompleteBanque(
        String denominationComplete)
    {
        LOGGER.debug("Recherche des guichets bancaires de dénomination complète '{}'", denominationComplete);

        // Recherche des guichets bancaires
        return findAllByUniqueCriteria(GuichetBancaire.class, ProprieteGuichetBancaire.DENOMINATION_COMPLETE.getNom(),
            denominationComplete);
    }

    /**
     * {@inheritDoc}
     *
     * @see fr.gouv.finances.lombok.bancaire.dao.GuichetBancaireDao#findTousGuichetsBancaires().
     */
    @Override
    public List<GuichetBancaire> findTousGuichetsBancaires()
    {
        LOGGER.trace("Recherche de tous les guichets bancaires");

        // Recherche des guichets bancaires
        return loadAllObjects(GuichetBancaire.class);
    }

    /**
     * {@inheritDoc}
     *
     * @see fr.gouv.finances.lombok.bancaire.dao.GuichetBancaireDao#marqueLesGuichetsBancairesPerimesADate(java.util.Date).
     */
    @Override
    public void marqueLesGuichetsBancairesPerimesADate(Date date)
    {
        LOGGER.debug("Marquage des guichets bancaires périmés pour la date '{}'", date);

        // Mise à jour ensembliste
        entityManager
            .createQuery("update GuichetBancaire c set c.estPerime = :perime where not c.dateDerniereMiseAJour = :date")
            .setParameter("perime", Boolean.TRUE).setParameter("date", date).executeUpdate();
    }

    /**
     * {@inheritDoc}
     *
     * @see fr.gouv.finances.lombok.bancaire.dao.GuichetBancaireDao#testeExistenceGuichetBancaireNonPerimeParCodeGuichetEtCodeEtablissement(
     *      java.lang.String, java.lang.String).
     */
    @Override
    public boolean testeExistenceGuichetBancaireNonPerimeParCodeGuichetEtCodeEtablissement(
        String codeGuichet, String codeEtablissement)
    {
        LOGGER.debug("Recherche l'existence de guichet bancaire non périmé de code '{}', lié à l'établissement '{}'", codeGuichet,
            codeEtablissement);

        // Paramétrage de la recherche
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Long> query = criteriaBuilder.createQuery(Long.class);
        Root<GuichetBancaire> root = query.from(GuichetBancaire.class);

        // Paramétrage des critères
        query.where(criteriaBuilder.equal(root.<String> get(ProprieteGuichetBancaire.CODE_ETABLISSEMENT.getNom()), codeEtablissement),
            criteriaBuilder.equal(root.<String> get(ProprieteGuichetBancaire.CODE_GUICHET.getNom()), codeGuichet),
            criteriaBuilder.equal(root.<Boolean> get(ProprieteGuichetBancaire.EST_PERIME.getNom()), false));

        // La recherche doit ramener le nombre d'élément satisfaisant les critères
        criteriaBuilder.count(root);
        query.select(criteriaBuilder.count(root));

        // Existe-il au moins un résultat ?
        return find(query) > 0;
    }

    /**
     * {@inheritDoc}
     *
     * @see fr.gouv.finances.lombok.bancaire.dao.GuichetBancaireDao#testeExistenceGuichetBancairePerimeOuNonParCodeGuichetEtCodeEtablissement(
     *      java.lang.String, java.lang.String).
     */
    @Override
    public boolean testeExistenceGuichetBancairePerimeOuNonParCodeGuichetEtCodeEtablissement(
        String codeGuichet, String codeEtablissement)
    {
        LOGGER.debug("Recherche l'existence de guichet bancaire de code '{}', lié à l'établissement '{}'", codeGuichet, codeEtablissement);

        // Paramétrage de la recherche
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Long> query = criteriaBuilder.createQuery(Long.class);
        Root<GuichetBancaire> root = query.from(GuichetBancaire.class);

        // Paramétrage des critères
        query.where(criteriaBuilder.equal(root.<String> get(ProprieteGuichetBancaire.CODE_ETABLISSEMENT.getNom()), codeEtablissement),
            criteriaBuilder.equal(root.<String> get(ProprieteGuichetBancaire.CODE_GUICHET.getNom()), codeGuichet));

        // La recherche doit ramener le nombre d'élément satisfaisant les critères
        criteriaBuilder.count(root);
        query.select(criteriaBuilder.count(root));

        // Existe-il au moins un résultat ?
        return find(query) > 0;
    }

}
