/*
 * Copyright (c) 2017 DGFiP - Tous droits réservés
 * 
 */
/**
 * Paquet lié aux implémentations JPA des DAO de gestion des données du guichet bancaire
 *
 * @author Christophe Breheret-Girardin
 */
package fr.gouv.finances.lombok.bancaire.jpa.dao.impl;