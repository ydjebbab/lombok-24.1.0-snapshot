package fr.gouv.finances.cp.srm.dao.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.gouv.finances.cp.srm.dao.CodiqueDao;
import fr.gouv.finances.lombok.srm.bean.Codique;
import fr.gouv.finances.lombok.test.controle.ControleDonnees;
import fr.gouv.finances.lombok.test.controle.VerificationExecution;
import fr.gouv.finances.lombok.util.base.PurgeTablesDao;

/**
 * Tests unitaires automatisés du DAO de gestion des données du codique.
 *
 * @author Christophe Breheret-Girardin
 * @author CF
 */
@ContextConfiguration({"classpath:/conf/applicationContext-srm-hibernate-test.xml",
    "classpath*:/conf/applicationContext-core-persistance-test.xml",
    "classpath*:/conf/applicationContext-core-orm-test.xml",
    "classpath*:/conf/applicationContext-srm-dao.xml"})
@ActiveProfiles(profiles = {"sireme", "hibernate"})
@RunWith(SpringJUnit4ClassRunner.class)
public class CodiqueDaoTest 
{
    public CodiqueDaoTest()
    {
        super();
    }   

    protected static final Logger LOGGER = LoggerFactory.getLogger(CodiqueDaoTest.class);

    /** Déclaration du DAO de gestion des données du codique. */
    @Autowired
    private CodiqueDao codiquedao;

    /** Déclaration du DAO de gestion des purges de la base. */
    @Autowired
    private PurgeTablesDao purgetablesdao;

    /** Map contenant les codiques présents en base */
    private Map<Integer, Codique> mapCodique = new HashMap<>();

    /**
     * Initialisation des données en base avant chaque test.
     */
    @Before
    public void creerDonnees()
    {
        // Purge des données de la base
        purgetablesdao.purgerTables("ZTJ_MESS_PROF_TJ", "ZPROFIL_PROF", "ZTJ_MESS_CODI_TJ"
            , "ZCODIQUE_CODI", "ZMESSAGETYPE_METY", "ZMESSAGE_MESS");

        // Vérification de l'absence de données
        ControleDonnees.verifierElements(codiquedao.loadAllObjects(Codique.class));
        
        // Génération des codiques, insertion en base de données et sauvegarde dans une Map
        mapCodique = IntStream.rangeClosed(1, 2).mapToObj(SrmUtils::getCodique).peek(codiquedao::saveObject)
            .collect(Collectors.toMap(c -> Integer.parseInt(c.getLibelleCodique()), c -> c));

        // Vérification des données à partir de l'image de la base
        ControleDonnees.verifierElements(codiquedao.loadAllObjects(Codique.class)
            , mapCodique.get(1), mapCodique.get(2));

        // Suppression du cache pour forcer les requêtes des tests
        codiquedao.clearPersistenceContext();
    }

    /**
     * Méthode permettant de tester l'injection du DAO.
     */
    @Test
    public final void testInjectionDao()
    {
        assertNotNull("DAO non injecté dans le test", codiquedao);
    }

    /**
     * Test de la méthode
     * {@link fr.gouv.finances.cp.srm.dao.CodiqueDao#findCodique(java.lang.String)}.
     */
    @Test
    public void testFindCodique()
    {
        // Exécution de la méthode à tester sur un paramètre non renseigne ou vide
        assertNull((codiquedao.findCodique(null)));
        assertNull(codiquedao.findCodique(""));

        // Exécution de la méthode à tester sur des données inexistantes
        assertNull(codiquedao.findCodique("kamoulox"));

        // Exécution de la méthode à tester sur toutes les données existantes
        mapCodique.entrySet().stream().forEach(entry ->
            assertEquals(entry.getValue(), codiquedao.findCodique(entry.getValue().getLibelleCodique())));
    }

    /**
     * Test de la méthode
     * {@link fr.gouv.finances.cp.srm.dao.CodiqueDao#saveCodique(fr.gouv.finances.lombok.srm.bean.Codique)}.
     */
    @Test
    public void testSaveCodique()
    {
        // Exécution de la méthode à tester
        Codique codique = SrmUtils.getCodique(3);
        codiquedao.saveCodique(codique);

        // Vérifications
        ControleDonnees.verifierElements(codiquedao.loadAllObjects(Codique.class)
            , mapCodique.get(1), mapCodique.get(2), codique);
    }

    /**
     * Test des contraintes.
     */
    public void testContraintes()
    {
        // Exécution de la méthode à tester
        VerificationExecution.verifierException(DataIntegrityViolationException.class
            , () -> codiquedao.saveObject(SrmUtils.getCodique(1)));
    }

}
