package fr.gouv.finances.cp.srm.dao.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.gouv.finances.cp.srm.dao.MessageTypeDao;
import fr.gouv.finances.lombok.srm.bean.MessageType;
import fr.gouv.finances.lombok.srm.bean.Profil;
import fr.gouv.finances.lombok.test.controle.ControleDonnees;
import fr.gouv.finances.lombok.test.controle.VerificationExecution;
import fr.gouv.finances.lombok.util.base.PurgeTablesDao;
import fr.gouv.finances.lombok.util.exception.IngerableException;

/**
 * Tests unitaires automatisés du DAO de gestion des données du type de message.
 *
 * @author Christophe Breheret-Girardin
 */
@ContextConfiguration({"classpath:/conf/applicationContext-srm-hibernate-test.xml",
        "classpath*:/conf/applicationContext-core-persistance-test.xml",
        "classpath*:/conf/applicationContext-core-orm-test.xml",
        "classpath*:/conf/applicationContext-srm-dao.xml"})
@ActiveProfiles(profiles = {"sireme", "hibernate"})
@RunWith(SpringJUnit4ClassRunner.class)
public class MessageTypeDaoTest
{
    public MessageTypeDaoTest()
    {
        super();
    }

    protected static final Logger LOGGER = LoggerFactory.getLogger(MessageTypeDaoTest.class);

    /** Déclaration du DAO de gestion des données du type de message. */
    @Autowired
    private MessageTypeDao messagetypedao;

    /** Déclaration du DAO de gestion des purges de la base. */
    @Autowired
    private PurgeTablesDao purgetablesdao;

    /** Map contenant les types de message présents en base. */
    private Map<Integer, MessageType> mapMessageType = new HashMap<>();

    /** Valeur inexistante en base. */
    private static final String VALEUR_ALPHA_INEXISTANTE = "kamoulox";

    /**
     * Initialisation des données en base avant chaque test.
     */
    @Before
    public void creerDonnees()
    {
        // Purge des données de la base
        purgetablesdao.purgerTables("ZTJ_MESS_PROF_TJ", "ZPROFIL_PROF", "ZTJ_MESS_CODI_TJ", "ZCODIQUE_CODI", "ZMESSAGETYPE_METY",
            "ZMESSAGE_MESS");

        // Vérification de l'absence de données
        ControleDonnees.verifierElements(messagetypedao.loadAllObjects(Profil.class));

        // Génération des types de message, insertion en base de données et sauvegarde dans une Map
        mapMessageType = IntStream.rangeClosed(1, 2).mapToObj(this::getMessageType)
            .peek(messagetypedao::saveObject)
            .collect(Collectors.toMap(m -> Integer.parseInt(m.getLibelleMessageType()), m -> m));

        // Vérification des données à partir de l'image de la base
        ControleDonnees.verifierElements(messagetypedao.loadAllObjects(MessageType.class), mapMessageType.get(1), mapMessageType.get(2));

        // Suppression du cache pour forcer les requêtes au sein des tests
        messagetypedao.clearPersistenceContext();
    }

    /**
     * Méthode permettant de tester l'injection du DAO.
     */
    @Test
    public final void testInjectionDao()
    {
        assertNotNull("DAO non injecté dans le test", messagetypedao);
    }

    /**
     * Test de la méthode
     * {@link fr.gouv.finances.cp.srm.dao.MessageTypeDao#saveMessageType(fr.gouv.finances.lombok.srm.bean.MessageType)}.
     */
    @Test
    public void testSaveTypeMessage()
    {
        // Exécution de la méthode à tester
        MessageType messageType = getMessageType(3);
        messagetypedao.saveMessageType(messageType);

        // Vérifications
        ControleDonnees.verifierElements(messagetypedao.loadAllObjects(MessageType.class), mapMessageType.get(1), mapMessageType.get(2),
            messageType);
    }

    /**
     * Test de la méthode
     * {@link fr.gouv.finances.cp.srm.dao.MessageTypeDao#deleteMessageType(fr.gouv.finances.lombok.srm.bean.MessageType)}.
     */
    @Test
    public void testDeleteMessageType()
    {
        // Exécution de la méthode à tester
        messagetypedao.deleteMessageType(mapMessageType.get(1));

        // Vérifications
        ControleDonnees.verifierElements(messagetypedao.loadAllObjects(MessageType.class), mapMessageType.get(2));
    }

    /**
     * Test de la méthode {@link fr.gouv.finances.cp.srm.dao.MessageTypeDao#deleteTousMessagesType()}.
     */
    @Test
    public void testDeleteTousMessagesType()
    {
        // Exécution de la méthode à tester
        messagetypedao.deleteTousMessagesType();

        // Vérifications
        ControleDonnees.verifierElements(messagetypedao.loadAllObjects(MessageType.class));
    }

    /**
     * Test de la méthode {@link fr.gouv.finances.cp.srm.dao.MessageTypeDao#findListeMessagesTypes()}.
     */
    @Test
    public void testFindListeMessagesTypes()
    {
        // Exécution de la méthode à tester et vérifications
        ControleDonnees.verifierElements(messagetypedao.findListeMessagesTypes(), mapMessageType.get(1), mapMessageType.get(2));
    }

    /**
     * Test de la méthode
     * {@link fr.gouv.finances.cp.srm.dao.MessageTypeDao#findMessageTypeParLibelle(java.lang.String)}.
     */
    @Test
    public void testFindMessageTypeParLibelle()
    {
        // Exécution de la méthode à tester sur un paramètre non renseigne ou vide
        assertNull(messagetypedao.findMessageTypeParLibelle(null));
        assertNull(messagetypedao.findMessageTypeParLibelle(""));

        // Exécution de la méthode à tester sur des données inexistantes
        assertNull(messagetypedao.findMessageTypeParLibelle(VALEUR_ALPHA_INEXISTANTE));

        // Exécution de la méthode à tester sur toutes les données existantes
        mapMessageType.entrySet().stream().forEach(
            entry -> assertEquals(messagetypedao.findMessageTypeParLibelle(entry.getValue().getLibelleMessageType()), entry.getValue()));
    }

    /**
     * Test de la méthode
     * {@link fr.gouv.finances.cp.srm.dao.MessageTypeDao#findLibelleAssocieAMessageTypeId(java.lang.String)}.
     */
    @Test
    public void testFindLibelleAssocieAMessageTypeId()
    {
        // Exécution de la méthode à tester sur un paramètre non renseigne ou vide
        assertNull("Un type de message a été trouvé", messagetypedao.findLibelleAssocieAMessageTypeId(null));
        assertNull("Un type de message a été trouvé", messagetypedao.findLibelleAssocieAMessageTypeId(""));

        // Exécution de la méthode à tester sur des données inexistantes
        VerificationExecution.verifierException(IngerableException.class,
            () -> messagetypedao.findLibelleAssocieAMessageTypeId(VALEUR_ALPHA_INEXISTANTE));
        assertNull(messagetypedao.findLibelleAssocieAMessageTypeId("100"));

        // Exécution de la méthode à tester sur toutes les données existantes
        mapMessageType.entrySet().stream()
            .forEach(entry -> assertEquals(entry.getValue().getLibelleMessageType(), messagetypedao.findLibelleAssocieAMessageTypeId(
                String.valueOf(entry.getValue().getId()))));
    }

    /**
     * Test de la méthode
     * {@link fr.gouv.finances.cp.srm.dao.MessageTypeDao#findMessageTypeParPartieParLibelle(java.lang.String)}.
     */
    @Test
    public void testFindMessageTypeParPartieParLibelle()
    {
        final String suffixeLibelle = "Libellé ";

        // Initialisation et vérification avant exécution du test
        MessageType messageType3 = new MessageType();
        messageType3.setLibelleMessageType(suffixeLibelle + 3);
        messagetypedao.saveObject(messageType3);
        MessageType messageType4 = new MessageType();
        messageType4.setLibelleMessageType(suffixeLibelle + 4);
        messagetypedao.saveObject(messageType4);
        ControleDonnees.verifierElements(messagetypedao.loadAllObjects(MessageType.class), mapMessageType.get(1), mapMessageType.get(2),
            messageType3, messageType4);

        // Exécution de la méthode à tester sur un paramètre non renseigne ou vide
        ControleDonnees.verifierElements(messagetypedao.findMessageTypeParPartieParLibelle(null));
        ControleDonnees.verifierElements(messagetypedao.findMessageTypeParPartieParLibelle(""));

        // Exécution de la méthode à tester sur des données inexistantes
        ControleDonnees.verifierElements(messagetypedao.findMessageTypeParPartieParLibelle(VALEUR_ALPHA_INEXISTANTE));

        // Exécution de la méthode à tester sur les données existantes
        ControleDonnees.verifierElements(messagetypedao.findMessageTypeParPartieParLibelle(suffixeLibelle), messageType3, messageType4);
    }

    /**
     * Test des contraintes.
     */
    public void testContraintes()
    {
        // Exécution de la méthode à tester
        VerificationExecution.verifierException(DataIntegrityViolationException.class, () -> messagetypedao.saveObject(getMessageType(1)));
    }

    /**
     * Méthode permettant de générer un type de message.
     *
     * @return le type de message généré
     */
    private MessageType getMessageType(int indice)
    {
        MessageType messageType = new MessageType();
        messageType.setLibelleMessageType(String.valueOf(indice));
        return messageType;
    }

}
