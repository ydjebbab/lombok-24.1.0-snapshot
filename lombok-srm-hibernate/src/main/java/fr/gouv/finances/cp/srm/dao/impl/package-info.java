/*
 * Copyright (c) 2017 DGFiP - Tous droits réservés
 * 
*/
/**
 * Paquet contenant les implémentations Hibernate des DAO liés à la gestion des messages sireme
 * @author Christophe Breheret-Girardin
 */
package fr.gouv.finances.cp.srm.dao.impl;