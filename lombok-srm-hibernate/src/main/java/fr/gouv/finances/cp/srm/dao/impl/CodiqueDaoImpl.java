/*
 * Copyright (c) 2017 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.cp.srm.dao.impl;

import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.gouv.finances.cp.srm.dao.CodiqueDao;
import fr.gouv.finances.cp.srm.dao.SrmDaoHelper;
import fr.gouv.finances.lombok.srm.bean.Codique;
import fr.gouv.finances.lombok.util.base.BaseDaoImpl;

/**
 * Implémentation Hibernate de la gestion des données des codiques.
 *
 * @author amleplatinec
 * @author Christophe Breheret-Girardin
 */
public class CodiqueDaoImpl extends BaseDaoImpl implements CodiqueDao
{

    protected static final Logger LOGGER = LoggerFactory.getLogger(CodiqueDaoImpl.class);

    public CodiqueDaoImpl()
    {
        super();
    }

    /**
     * {@inheritDoc}
     * 
     * @see fr.gouv.finances.cp.srm.dao.CodiqueDao#findCodique(java.lang.String)
     */
    @Override
    public Codique findCodique(String libelle)
    {
        // Journalisation de la recherche
        LOGGER.debug("Recherche du codique correspondant au libellé '{}'", libelle);

        // Si aucun paramètre, la requête n'est pas exécutée
        if (SrmDaoHelper.parametreNonFourni(libelle))
        {
            return null;
        }

        // Récupération du codique correspondant au critère
        return getHibernateTemplate().execute
            (
                // Le libellé étant unique, un seul codique doit correspondre au maximum
                session -> (Codique) session.createCriteria(Codique.class)
                            .add(Restrictions.eq("libelleCodique", libelle))
                            .uniqueResult()
            );
    }

    /**
     * {@inheritDoc}
     * 
     * @see fr.gouv.finances.cp.srm.dao.CodiqueDao#saveCodique(fr.gouv.finances.lombok.srm.bean.Codique)
     */
    @Override
    public void saveCodique(Codique codique)
    {
        // Journalisation de la mise à jour
        LOGGER.debug("Sauvegarde du codique '{}'", codique.getLibelleCodique());

        // Exécution de la mise à jour
        getHibernateTemplate().save(codique);
    }

}
