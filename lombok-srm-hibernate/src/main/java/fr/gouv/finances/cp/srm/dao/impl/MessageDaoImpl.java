/*
 * Copyright (c) 2017 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.cp.srm.dao.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.gouv.finances.cp.srm.dao.MessageDao;
import fr.gouv.finances.cp.srm.dao.ProprieteMessage;
import fr.gouv.finances.cp.srm.techbean.CriteresRechercheMessages;
import fr.gouv.finances.lombok.srm.bean.Message;
import fr.gouv.finances.lombok.util.base.BaseDaoImpl;

/**
 * Implémentation Hibernate de la gestion des données des messages sireme.
 * 
 * @author amleplatinec
 * @author Christophe Breheret-Girardin
 */
public class MessageDaoImpl extends BaseDaoImpl implements MessageDao
{
    private static final Logger LOGGER = LoggerFactory.getLogger(MessageDaoImpl.class);

    public MessageDaoImpl()
    {
        super();
    }

    /**
     * {@inheritDoc}
     * @see fr.gouv.finances.cp.srm.dao.MessageDao#deleteMessage(fr.gouv.finances.lombok.srm.bean.Message)
     */
    @Override
    public void deleteMessage(final Message message)
    {
        // Journalisation de la suppression
        LOGGER.debug("Suppression du message sireme '{}'", message.getLibelleMessage());

        // Exécution de la mise à jour
        getHibernateTemplate().delete(message);
        
        LOGGER.trace("fin de supression du message avec libelle du message à supprimer");
    }

    /**
     * 
     * (methode de remplacement)
     * {@inheritDoc}
     * @see fr.gouv.finances.cp.srm.dao.MessageDao#findMessageExisteDeja(fr.gouv.finances.lombok.srm.bean.Message)
     */
    @SuppressWarnings("unchecked")
    @Override
    public Message findMessageExisteDeja(Message message)
    {
        // Si aucun paramètre, la requête n'est pas exécutée
        if (message == null || StringUtils.isEmpty(message.getLibelleMessage()))
        {
            LOGGER.info("Le message fourni n'est pas complet pour être un critère de recherche");
            return null;
        }

        // Journalisation de la recherche
        LOGGER.debug("Recherche si un message sireme existe déjà avec le libellé '{}'", message.getLibelleMessage());

        // Initialisation des paramétres de recherche
        DetachedCriteria detachedCriteria = DetachedCriteria.forClass(Message.class);
        detachedCriteria.add(Restrictions.eq(ProprieteMessage.PROPRIETE_LIBELLE.getNom(), message.getLibelleMessage()));

        // Exécution de la recherche
        Iterator<Message>  result = (Iterator<Message>) (getHibernateTemplate().findByCriteria(detachedCriteria)).iterator();

        // Analyse des résultats
        Message unMessage = null;
        if (result.hasNext())
        {
            unMessage = result.next();
        }

        return unMessage;
    }

    /**
     * 
     * (methode de remplacement)
     * {@inheritDoc}
     * @see fr.gouv.finances.cp.srm.dao.MessageDao#findMessageExisteDejaEnModif(fr.gouv.finances.lombok.srm.bean.Message)
     */
    @SuppressWarnings("unchecked")
    @Override
    public Message findMessageExisteDejaEnModif(Message message)
    {
        // Si aucun paramètre de recherche, la requête n'est pas exécutée
        if (message == null || StringUtils.isEmpty(message.getLibelleMessage())
            || message.getId() == null)
        {
            LOGGER.info("Le message fourni n'est pas complet pour être un critère de recherche");
            return null;
        }

        // Journalisation de la recherche
        LOGGER.debug("Recherche si un message sireme existe déjà en modification avec le libellé '{}'"
            , message.getLibelleMessage());

        // Initialisation des paramétres de recherche
        DetachedCriteria detachedCriteria = DetachedCriteria.forClass(Message.class);
        detachedCriteria.add(Restrictions.not(Restrictions.idEq(message.getId())));
        detachedCriteria.add(Restrictions.eq(ProprieteMessage.PROPRIETE_LIBELLE.getNom(), message.getLibelleMessage()));

        // Exécution de la recherche
        Iterator<Message> result
            = ((List<Message>) getHibernateTemplate().findByCriteria(detachedCriteria)).iterator();

        // Analyse des résultats
        Message unMessage = null;
        if (result.hasNext())
        {
            unMessage = result.next();
        }

        return unMessage;
    }

    /**
     * 
     * (methode de remplacement)
     * {@inheritDoc}
     * @see fr.gouv.finances.cp.srm.dao.MessageDao#findMessages(fr.gouv.finances.cp.srm.techbean.CriteresRechercheMessages)
     */
    public List<Message> findMessages(final CriteresRechercheMessages crit)
    {
        // Journalisation de la recherche
        LOGGER.debug("Recherche des messages sireme correspondant aux critères '{}'", crit);

        // Si aucun paramètre, la requête n'est pas exécutée
        if (crit == null)
        {
            LOGGER.info("Aucun critère, récupère de tous les messages");
            return findTousLesMessages();
        }

        // Récupération des messages correspondant aux critères
        return getHibernateTemplate().execute
            (
                session ->
                {
                    // Récupération des critères de recherche
                    Criteria criteria = getCriteres(crit, session);

                    // Récupération des informations associées
                    ajoutAssociations(criteria);

                    // Initialisation de l'ordre à appliquer aux résultats
                    criteria.addOrder(Order.asc(ProprieteMessage.PROPRIETE_DATE_DEBUT.getNom()));

                    // Récupération des messages correspondant
                    @SuppressWarnings("unchecked")
                    List<Message> messages = (List<Message>) criteria.setResultTransformer(
                        Criteria.DISTINCT_ROOT_ENTITY).list();

                    return messages;
                }
            );
    }

    /**
     * Méthode permettant de récupérer les critères à appliquer à la recherche de message
     *
     * @param crit critères donnés
     * @param session persistance Hibernate
     * @return un objet Criteria contenant les critères à appliquer à la recherche
     */
    private Criteria getCriteres(final CriteresRechercheMessages crit, Session session)
    {
        // Journalisation de la récupération des critères
        LOGGER.trace("Récupération des critères de recherche");

        Criteria criteria = session.createCriteria(Message.class);

        // Cas de recherche sur les libellés de message
        if (!StringUtils.isEmpty(crit.getLibelleMessage()))
        {
            criteria.add(Restrictions.eq(ProprieteMessage.PROPRIETE_LIBELLE.getNom(), crit.getLibelleMessage()));
        }

        // Cas de recherche sur les types de message
        if (!StringUtils.isEmpty(crit.getTypeMessages()))
        {
            criteria.add(Restrictions.eq("typeMessage", crit.getTypeMessages()));
        }

        // Cas de recherche sur les dates
        if (crit.getDateDebutMessage() != null)
        {
            criteria.add(Restrictions.ge(ProprieteMessage.PROPRIETE_DATE_DEBUT.getNom(), crit.getDateDebutMessage()));
        }

        if (crit.getDateFinMessage() != null)
        {
            criteria.add(Restrictions.le(ProprieteMessage.PROPRIETE_DATE_FIN.getNom(), crit.getDateFinMessage()));
        }

        // Cas de recherche sur le créateur
        if (!StringUtils.isEmpty(crit.getUidCreateurMessage()))
        {
            criteria.add(Restrictions.eq("UIDCreateurMessage",
                crit.getUidCreateurMessage().trim()).ignoreCase());
        }

        // Cas de recherche des messages actifs
        if (crit.isRechOnlyMessagesActifsNow())
        {
            criteria.add(Restrictions.le(ProprieteMessage.PROPRIETE_DATE_DEBUT.getNom(), crit.getDateHeureCourante()));
            criteria.add(Restrictions.ge(ProprieteMessage.PROPRIETE_DATE_FIN.getNom(), crit.getDateHeureCourante()));
        }

        // Cas de recherche des messages liés aux profils à désactiver à une date donnée
        if (crit.isRechOnlyMessProfilsADesactNow())
        {
            criteria.add(Restrictions.isNotNull(ProprieteMessage.PROPRIETE_DATE_DEBUT_INACTIVITE.getNom()));
            criteria.add(Restrictions.isNotNull(ProprieteMessage.PROPRIETE_DATE_FIN_INACTIVITE.getNom()));
            criteria.add(Restrictions.le(
                ProprieteMessage.PROPRIETE_DATE_DEBUT_INACTIVITE.getNom(), crit.getDateHeureCourante()));
            criteria.add(Restrictions.ge(
                ProprieteMessage.PROPRIETE_DATE_FIN_INACTIVITE.getNom(), crit.getDateHeureCourante()));
        }

        // Cas de recherche des messages liés aux profils à désactiver à la date du jour
        if (crit.isRechOnlyMessAvecProfilsADesact())
        {
            criteria.add(Restrictions.isNotEmpty(ProprieteMessage.PROPRIETE_PROFILS_DESACTIVES.getNom()));
        }

        return criteria;
    }
    
    /**
     * 
     * (methode de remplacement)
     * {@inheritDoc}
     * @see fr.gouv.finances.cp.srm.dao.MessageDao#findMessagesAyantLibelleCodiqueRattacheAAutreMessage(java.lang.String, fr.gouv.finances.lombok.srm.bean.Message)
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<Message> findMessagesAyantLibelleCodiqueRattacheAAutreMessage(
        String libelleCodique, Message message)
    {
        // Journalisation de la recherche
        LOGGER.debug("Recherche des messages sireme ayant le codique '{}' rattaché à un autre message"
            , libelleCodique);

        // Si aucun paramètre, la requête n'est pas exécutée
        if (message == null || message.getId() == null || StringUtils.isEmpty(libelleCodique))
        {
            LOGGER.info("Le message et le libellé fournis ne sont pas complet pour être des critères"
                + " de recherche");
            return new ArrayList<>();
        }
        
        // Initialisation de la recherche
        DetachedCriteria criteria = DetachedCriteria.forClass(Message.class);

        // Initialisation des critères de recherche
        criteria.add(Restrictions.not(Restrictions.idEq(message.getId())));
        criteria.createCriteria(ProprieteMessage.PROPRIETE_CODIQUES_DESTINATAIRES.getNom()).add(
                Restrictions.eq("libelleCodique", libelleCodique));

        // Exécution de la recherche
        return (List<Message>) (getHibernateTemplate().findByCriteria(criteria));
    }

    /**
     * {@inheritDoc}
     * 
     * @see fr.gouv.finances.cp.srm.dao.MessageDao#findMessagesPerimes()
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<Message> findMessagesPerimes()
    {
        // Journalisation de la recherche
        LOGGER.trace("Recherche des messages sireme périmés");

        // Récupération des messages correspondant aux critères
        return getHibernateTemplate().execute
            (
                session -> 
                {
                    // Récupération de la date du jour
                    Date dayDate = new Date();

                    Criteria criteria = session.createCriteria(Message.class);
                    // si message simple d'information, on cherche messages dont date de fin message < date du jour
                    // si message qui desactive un profil, on cherche messages dont date de fin de desactivation
                    // du profil < date du jour
                    criteria.add(Restrictions
                        .disjunction()
                        .add(Restrictions.conjunction()
                            .add(Restrictions.isNull(ProprieteMessage.PROPRIETE_DATE_FIN_INACTIVITE.getNom()))
                            .add(Restrictions.lt(ProprieteMessage.PROPRIETE_DATE_FIN.getNom(), dayDate)))
                        .add(Restrictions.conjunction()
                            .add(Restrictions.isNotNull(ProprieteMessage.PROPRIETE_DATE_FIN_INACTIVITE.getNom()))
                            .add(Restrictions.lt(
                                ProprieteMessage.PROPRIETE_DATE_FIN_INACTIVITE.getNom(), dayDate))));

                    // Récupération des informations associées
                    ajoutAssociations(criteria);

                    // Récupération des messages
                    return criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
                }
            );
    }

    /**
     * {@inheritDoc}
     * 
     * @see fr.gouv.finances.cp.srm.dao.MessageDao#findTousLesMessages()
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<Message> findTousLesMessages()
    {
        // Journalisation de la recherche
        LOGGER.trace("Récupération de tous les messages sireme (+ profils + codiques)");

        // Récupération des messages correspondant aux critères
        return getHibernateTemplate().execute
            (
                session -> 
                {
                    // Paramétrage de la recherche
                    Criteria criteria = session.createCriteria(Message.class);
                    // Récupération des informations associées au message sireme
                    ajoutAssociations(criteria);
                    // Récupération des messages
                    return criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
                }
            );
    }

    /**
     * Méthode permettant de récupérer toutes les associations non chargées du message
     *
     * @param criteria critères de recherche
     */
    private void ajoutAssociations(Criteria criteria)
    {
        // Journalisation de l'ajout des associations à récupérer
        LOGGER.trace("Ajout des associations à récupérer lors de la recherche");

        // Récupération des informations associées
        criteria.createAlias(ProprieteMessage.PROPRIETE_PROFILS_DESACTIVES.getNom()
            , ProprieteMessage.PROPRIETE_PROFILS_DESACTIVES.getNom(), JoinType.LEFT_OUTER_JOIN);
        criteria.createAlias(ProprieteMessage.PROPRIETE_CODIQUES_DESTINATAIRES.getNom()
            , ProprieteMessage.PROPRIETE_CODIQUES_DESTINATAIRES.getNom(), JoinType.LEFT_OUTER_JOIN);
        criteria.createAlias(ProprieteMessage.PROPRIETE_PROFILS_DESTINATAIRES.getNom()
            , ProprieteMessage.PROPRIETE_PROFILS_DESTINATAIRES.getNom(), JoinType.LEFT_OUTER_JOIN);
    }
    
    /**
     * {@inheritDoc}
     *
     * @see fr.gouv.finances.cp.srm.dao.MessageDao#saveMessage(fr.gouv.finances.lombok.srm.bean.Message)
     */
    @Override
    public void saveMessage(final Message message)
    {
        // Journalisation de la mise à jour
        LOGGER.debug("Création du message sireme '{}'", message.getLibelleMessage());

        // Exécution de la mise à jour
        getHibernateTemplate().saveOrUpdate(message);
    }
}
