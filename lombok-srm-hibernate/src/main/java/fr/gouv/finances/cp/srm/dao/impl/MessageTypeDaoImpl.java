/*
 * Copyright (c) 2017 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.cp.srm.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.orm.ObjectRetrievalFailureException;

import fr.gouv.finances.cp.srm.dao.MessageTypeDao;
import fr.gouv.finances.cp.srm.dao.SrmDaoHelper;
import fr.gouv.finances.lombok.srm.bean.MessageType;
import fr.gouv.finances.lombok.util.base.BaseDaoImpl;

/**
 * Implémentation Hibernate de la gestion des données des types de message sireme.
 *
 * @author amleplatinec
 * @author Christophe Breheret-Girardin
 */
public class MessageTypeDaoImpl extends BaseDaoImpl implements MessageTypeDao
{
    protected static final Logger LOGGER = LoggerFactory.getLogger(MessageTypeDaoImpl.class);

    public MessageTypeDaoImpl()
    {
        super();
    }

    /**
     * {@inheritDoc}
     *
     * @see fr.gouv.finances.cp.srm.dao.MessageTypeDao#deleteMessageType(fr.gouv.finances.lombok.srm.bean.MessageType)
     */
    @Override
    public void deleteMessageType(final MessageType messagetype)
    {
        // Journalisation de la suppression
        LOGGER.debug("Suppression du type de message sirem '{}'", messagetype.getLibelleMessageType());

        // Exécution de la suppression
        getHibernateTemplate().delete(messagetype);
    }

    /**
     * {@inheritDoc}
     *
     * @see fr.gouv.finances.cp.srm.dao.MessageTypeDao#deleteTousMessagesType()
     */
    @Override
    public void deleteTousMessagesType()
    {
        // Journalisation de la suppression
        LOGGER.trace("Suppression de tous les types de message sireme");

        // Suppression des types de message en masse
        getHibernateTemplate().bulkUpdate("delete from " + MessageType.class.getSimpleName());
    }

    /**
     * {@inheritDoc}
     *
     * @see fr.gouv.finances.cp.srm.dao.MessageTypeDao#findLibelleAssocieAMessageTypeId(java.lang.String)
     */
    @Override
    public String findLibelleAssocieAMessageTypeId(String messageTypeId)
    {
        // Journalisation de la recherche
        LOGGER.debug("Recherche d'un type de message correspondant à l'identifiant '{}'", messageTypeId);

        // Récupération de l'identifiant sous forme numérique
        Long identifiant = SrmDaoHelper.getIdentifiant(messageTypeId);

        // Si le paramètre n'est pas renseigné, la requête n'est pas exécutée
        if (identifiant == null)
        {
            return null;
        }
        
        // Exécution de la recherche
        try
        {
            // Récupération du libellé si un type de message correspond au critère
            MessageType messageType = getObject(MessageType.class, identifiant);
            return messageType.getLibelleMessageType();
        }
        catch (ObjectRetrievalFailureException orfe)
        {
            // Type de message non trouvé
            LOGGER.info("Type de message sireme d'identifiant '{}' non trouvé : {}", messageTypeId, orfe);
            return null;
        }
    }

    /**
     * {@inheritDoc}
     *
     * @see fr.gouv.finances.cp.srm.dao.MessageTypeDao#findListeMessagesTypes()
     */
    @Override
    public List<MessageType> findListeMessagesTypes()
    {
        // Journalisation de la recherche
        LOGGER.trace("Recherche de tous les types de message sireme");

        // Exécution de la rercherche et retour de la liste correspondante
        return loadAllObjects(MessageType.class);
    }

    /**
     * {@inheritDoc}
     *
     * @see fr.gouv.finances.cp.srm.dao.MessageTypeDao#findMessageTypeParLibelle(java.lang.String)
     */
    @Override
    public MessageType findMessageTypeParLibelle(String libelle)
    {
        // Journalisation de la recherche
        LOGGER.debug("Recherche d'un type de message correspondant au libellé '{}'", libelle);

        // Si le paramètre n'est pas renseigné, la requête n'est pas exécutée
        if (SrmDaoHelper.parametreNonFourni(libelle))
        {
            return null;
        }

        // Récupération du type de message correspondant au critère
        return getHibernateTemplate().execute
            (
                session -> (MessageType) session.createCriteria(MessageType.class)
                            // Ajout du critère sur le libellé
                            .add(Restrictions.eq("libelleMessageType", libelle))
                            // // Le libellé étant unique, un seul type de message doit correspondre au maximum
                            .uniqueResult()
            );
    }

    /**
     * {@inheritDoc}
     *
     * @see fr.gouv.finances.cp.srm.dao.MessageTypeDao#findMessageTypeParPartieParLibelle(java.lang.String)
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<MessageType> findMessageTypeParPartieParLibelle(final String libelle)
    {
        // Journalisation de la recherche
        LOGGER.debug("Recherche d'un type de message dont le libellé contient '{}'", libelle);

        // Si aucun paramètre, la requête n'est pas exécutée
        if (SrmDaoHelper.parametreNonFourni(libelle))
        {
            return new ArrayList<>();
        }

        // Récupération de la liste des types de message correspondant au critère
        return getHibernateTemplate().execute
            (
                session -> (List<MessageType>) session.createCriteria(MessageType.class)
                            // Ajout du critère sur le libellé (La partie recherchée peut être située n'importe où)
                            .add(Restrictions.like("libelleMessageType", libelle, MatchMode.ANYWHERE))
                            .list()
            );
    }

    /**
     * {@inheritDoc}
     *
     * @see fr.gouv.finances.cp.srm.dao.MessageTypeDao#saveMessageType(fr.gouv.finances.lombok.srm.bean.MessageType)
     */
    @Override
    public void saveMessageType(final MessageType messageType)
    {
        // Journalisation de la mise à jour
        LOGGER.debug("Sauvegarde du type de message '{}'", messageType.getLibelleMessageType());

        // Exécution de la mise à jour
        getHibernateTemplate().saveOrUpdate(messageType);
    }
}
