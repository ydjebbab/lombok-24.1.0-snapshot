/*
 * Copyright (c) 2017 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.cp.srm.dao.impl;

import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.gouv.finances.cp.srm.dao.ProfilDao;
import fr.gouv.finances.cp.srm.dao.SrmDaoHelper;
import fr.gouv.finances.lombok.srm.bean.Profil;
import fr.gouv.finances.lombok.util.base.BaseDaoImpl;

/**
 * Implémentation Hibernate de la gestion des données du profil.
 *
 * @author amleplatinec
 * @author Christophe Breheret-Girardin
 */
public class ProfilDaoImpl extends BaseDaoImpl implements ProfilDao
{
    protected static final Logger LOGGER = LoggerFactory.getLogger(ProfilDaoImpl.class);

    public ProfilDaoImpl()
    {
        super();
    }

    /**
     * {@inheritDoc}
     * 
     * @see fr.gouv.finances.cp.srm.dao.ProfilDao#findProfilParCode(java.lang.String)
     */
    @Override
    public Profil findProfilParCode(String code)
    {
        // Journalisation de la recherche
        LOGGER.debug("Recherche du profil correspondant au code '{}'", code);

        // Si aucun paramètre, la requête n'est pas exécutée
        if (SrmDaoHelper.parametreNonFourni(code))
        {
            return null;
        }

        // Récupération de la liste des profil correspondant au critère
        @SuppressWarnings("unchecked")
        List<Profil> profils = getHibernateTemplate().execute
        (
            // Récupération d'une liste car le code profil peut être le même sur plusieurs applications
            session -> (List<Profil>) session.createCriteria(Profil.class)
                        .add(Restrictions.eq("codeProfil", code))
                        .list()
        );

        // Si aucun profil
        Profil profil = null;
        if (!profils.isEmpty())
        {
            // S'il y a au moins un profil, récupération du 1er
            profil = profils.get(0);
        }
        return profil;
    }

    /**
     * {@inheritDoc}
     * 
     * @see fr.gouv.finances.cp.srm.dao.ProfilDao#saveProfil(fr.gouv.finances.lombok.srm.bean.Profil)
     */
    @Override
    public void saveProfil(Profil profil)
    {
        // Journalisation de la mise à jour
        LOGGER.debug("Enregistrement du profil '{}'", profil.getCodeProfil());

        // Exécution de la mise à jour
        getHibernateTemplate().save(profil);
    }

    /**
     * {@inheritDoc}
     * 
     * @see fr.gouv.finances.cp.srm.dao.ProfilDao#findTousLesProfils()
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<Profil> findTousLesProfils()
    {
        // Journalisation de la recherche
        LOGGER.trace("Recherche de tous les profils");

        // Exécution de la recherche
        DetachedCriteria detachedCriteria = DetachedCriteria.forClass(Profil.class);
        return (List<Profil>) getHibernateTemplate().findByCriteria(detachedCriteria);
    }

}
