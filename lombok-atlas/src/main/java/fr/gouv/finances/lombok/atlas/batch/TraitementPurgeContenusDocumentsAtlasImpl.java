/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : TraitementPurgeContenusDocumentsAtlasImpl.java
 *
 */
package fr.gouv.finances.lombok.atlas.batch;

import fr.gouv.finances.lombok.atlas.service.DocumentAtlasService;
import fr.gouv.finances.lombok.batch.ServiceBatchCommunImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

/**
 * Class TraitementPurgeContenusDocumentsAtlasImpl.
 *
 * @author chouard-cp
 * @version $Revision: 1.2 $ Date: 15 déc. 2009
 */
@Service("traitementpurgecontenusdocumentsatlas")
@Profile("batch")
@Lazy(true)
public class TraitementPurgeContenusDocumentsAtlasImpl extends ServiceBatchCommunImpl {

    /**
     * documentatlasserviceso.
     */
    @Autowired()
    @Qualifier("documentatlasserviceso")
    private DocumentAtlasService documentatlasserviceso;

    /**
     * taille de bloc.
     */
    private Integer tailleDeBloc;

    /**
     * nombre de documents purgés
     */
    private int nbDocumentsPurges;

    /**
     * Constructeur de la classe TraitementPurgeContenusDocumentsAtlasImpl.java
     */
    public TraitementPurgeContenusDocumentsAtlasImpl() {
        super();        
    }

    /**
     * Accesseur de l attribut documentatlasserviceso.
     *
     * @return documentatlasserviceso
     */
    public DocumentAtlasService getDocumentatlasserviceso() {
        return documentatlasserviceso;
    }

    /**
     * Accesseur de l attribut taille de bloc.
     *
     * @return taille de bloc
     */
    public Integer getTailleDeBloc() {
        return tailleDeBloc;
    }

    /**
     * Accesseur de l attribut nbDocumentsPurges.
     *
     * @return nbDocumentsPurges
     */
    public int getNbDocumentsPurges() {
        return nbDocumentsPurges;
    }

    /**
     * Modificateur de l attribut documentatlasserviceso.
     *
     * @param documentatlasserviceso le nouveau documentatlasserviceso
     */
    public void setDocumentatlasserviceso(DocumentAtlasService documentatlasserviceso) {
        this.documentatlasserviceso = documentatlasserviceso;
    }

    /**
     * Modificateur de l attribut taille de bloc.
     *
     * @param tailleDeBloc le nouveau taille de bloc
     */
    @Value("${traitementpurgecontenusdocumentsatlas.tailledebloc}")
    public void setTailleDeBloc(Integer tailleDeBloc) {
        this.tailleDeBloc = tailleDeBloc;
    }

    /**
     * Modificateur de l attribut nbDocumentsPurges.
     *
     * @param nbDocumentsPurges le nouveau nbDocumentsPurges
     */
    public void setNbDocumentsPurges(int nbDocumentsPurges) {
        this.nbDocumentsPurges = nbDocumentsPurges;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     *
     * @see fr.gouv.finances.lombok.batch.ServiceBatchCommunImpl#traiterBatch()
     */
    @Override
    public void traiterBatch()
    {
        nbDocumentsPurges = documentatlasserviceso.purgerContenuDocumentArchive(tailleDeBloc);
    }

    @Value("${traitementpurgecontenusdocumentsatlas.versionbatch}")
    @Override()
    public void setVersionBatch(String versionBatch) {
        super.setVersionBatch(versionBatch);
    }

    @Value("${traitementpurgecontenusdocumentsatlas.nombatch}")
    @Override()
    public void setNomBatch(String nomBatch) {
        super.setNomBatch(nomBatch);
    }
}
