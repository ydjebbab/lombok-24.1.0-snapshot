/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : DocumentAtlasServiceImpl.java
 *
 */
package fr.gouv.finances.lombok.atlas.service.impl;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.UrlResource;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;
import org.springframework.util.StringUtils;

import fr.gouv.finances.lombok.atlas.bean.DocumentAtlas;
import fr.gouv.finances.lombok.atlas.bean.EtatDocumentAtlas;
import fr.gouv.finances.lombok.atlas.bean.TypeDocumentAtlas;
import fr.gouv.finances.lombok.atlas.dao.DocumentAtlasDao;
import fr.gouv.finances.lombok.atlas.service.DocumentAtlasService;
import fr.gouv.finances.lombok.mel.service.MelService;
import fr.gouv.finances.lombok.mel.techbean.Mel;
import fr.gouv.finances.lombok.mel.techbean.PieceJointe;
import fr.gouv.finances.lombok.securite.springsecurity.UtilisateurSecurityLombokContainer;
import fr.gouv.finances.lombok.securite.techbean.PersonneAnnuaire;
import fr.gouv.finances.lombok.upload.bean.FichierJoint;
import fr.gouv.finances.lombok.upload.service.FichierJointService;
import fr.gouv.finances.lombok.util.base.BaseServiceImpl;
import fr.gouv.finances.lombok.util.exception.ApplicationExceptionTransformateur;
import fr.gouv.finances.lombok.util.exception.ProgrammationException;
import fr.gouv.finances.lombok.util.exception.RegleGestionException;
import fr.gouv.finances.lombok.util.persistance.ScrollIterator;

/**
 * Class DocumentAtlasServiceImpl.
 * 
 * @author chouard-cp
 * @version $Revision: 1.7 $ Date: 15 déc. 2009
 */
public class DocumentAtlasServiceImpl extends BaseServiceImpl implements DocumentAtlasService
{
    private static final Logger log = LoggerFactory.getLogger(DocumentAtlasServiceImpl.class);

    /** PARA m_ cod e_ appli. */
    private static String paramCODEAPPLI = "CODEAPPL";

    /** PARA m_ env. */
    private static String paramENV = "ENV";

    /** PARA m_ i d_ user. */
    private static String paramIDUSER = "IDUSER";

    /** PARA m_ cle. */
    private static String paramCLE = "CLE";

    /** PARA m_ i d_ atlas. */
    private static String paramIDATLAS = "IDENTIFIANT";

    /** PARA m_ presentation. */
    private static String paramPRESENTATION = "PRESENTATION";

    /** VALEU r_ presentation. */
    private static String valeurPRESENTATION = "E";

    /** SE p_ fichie r_ extension. */
    private static String sepFICHIEREXTENSION = ".";

    /** Constant : DEFAULT_SIZE_BUFFER. */
    private static final Integer DEFAULT_SIZE_BUFFER = 4096;

    /** Constant : CONTENT_TYPE_HTML. */
    private static final String CONTENT_TYPE_HTML = "text/html";

    /** documentatlasdao. */
    private DocumentAtlasDao documentatlasdao;

    /** url. */
    private String url;

    /** port. */
    private String port;

    /** module. */
    private String module;

    /** page. */
    private String page;

    /** env. */
    private String env;

    /** code appli. */
    private String codeAppli;

    /** cle. */
    private String cle;

    /** fichierjointserviceso. */
    private FichierJointService fichierjointserviceso;

    /** melserviceso. */
    private MelService melserviceso;

    /** transaction template. */
    private TransactionTemplate transactionTemplate;

    /**
     * Constructeur de la classe DocumentAtlasServiceImpl.java
     *
     */
    public DocumentAtlasServiceImpl()
    {
        super();
        
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param nombreOccurences
     * @param dateDEnvoiAtlas
     * @param dateAccuseReception
     * @see fr.gouv.finances.lombok.atlas.service.DocumentAtlasService#accuserReceptionEnvoiDocumentAtlas(java.lang.Integer,
     *      java.sql.Timestamp, java.sql.Timestamp)
     */
    @Override
    public int accuserReceptionEnvoiDocumentAtlas(final Integer nombreOccurences, final Timestamp dateDEnvoiAtlas,
        final Timestamp dateAccuseReception)
    {
        final List<Integer> nbDocuments = new ArrayList<>();
        try
        {
            transactionTemplate.execute(new TransactionCallbackWithoutResult()
            {
                @Override
                public void doInTransactionWithoutResult(TransactionStatus status)
                {
                    int iii = 0;
                    ScrollIterator documentsAAccuser =
                        documentatlasdao.findDocumentsEnvoyesAAtlasParIterateur(nombreOccurences, dateDEnvoiAtlas);
                    while (documentsAAccuser.hasNext())
                    {
                        DocumentAtlas document = (DocumentAtlas) documentsAAccuser.nextObjetMetier();
                        document.setDateAccuseReception(dateAccuseReception);
                        document.setEtat(EtatDocumentAtlas.ARCHIVE);
                        documentatlasdao.saveObject(document);

                        /*
                         * Le flush fait par lot sur une série de requetes préparées identiques est un facteur important
                         * de gain de performances pour la moteur de SGBD
                         */
                        iii++;
                        if (iii % nombreOccurences == 0)
                        {
                            documentatlasdao.flush();
                            documentatlasdao.clearPersistenceContext();
                            log.info("flush : " + iii);
                        }

                    }
                    log.info("nombre de documents Atlas traités : " + iii);

                    nbDocuments.add(0, iii);
                }
            });
            return nbDocuments.get(0);
        }
        catch (RuntimeException e)
        {
            throw ApplicationExceptionTransformateur.transformer(
                "Erreur lors du traitement de l'accusé-réception d'envoi de documents Atlas", e);
        }
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param nombreOccurences
     * @param dateDEnvoiAtlas
     * @see fr.gouv.finances.lombok.atlas.service.DocumentAtlasService#annulerEnvoiDocumentsAAtlas(java.lang.Integer,
     *      java.sql.Timestamp)
     */
    @Override
    public void annulerEnvoiDocumentsAAtlas(final Integer nombreOccurences, final Timestamp dateDEnvoiAtlas)
    {
        try
        {
            transactionTemplate.execute(new TransactionCallbackWithoutResult()
            {
                @Override
                public void doInTransactionWithoutResult(TransactionStatus status)
                {
                    int iii = 0;
                    ScrollIterator documentsAAnnuler =
                        documentatlasdao.findDocumentsEnvoyesAAtlasParIterateur(nombreOccurences, dateDEnvoiAtlas);
                    while (documentsAAnnuler.hasNext())
                    {
                        DocumentAtlas document = (DocumentAtlas) documentsAAnnuler.nextObjetMetier();
                        document.setDateDEnvoiAtlas(null);
                        document.setEtat(EtatDocumentAtlas.AARCHIVER);
                        documentatlasdao.saveObject(document);

                        /*
                         * Le flush fait par lot sur une série de requetes préparées identiques est un facteur important
                         * de gain de performances pour la moteur de SGBD
                         */
                        iii++;
                        if (iii % nombreOccurences == 0)
                        {
                            documentatlasdao.flush();
                            documentatlasdao.clearPersistenceContext();
                            log.info("flush : " + iii);
                        }

                    }
                    log.info("nombre de documents Atlas annulés : " + iii);
                }
            });
        }
        catch (RuntimeException e)
        {
            throw ApplicationExceptionTransformateur.transformer("Erreur lors du traitement de l'annulation d'envoi de documents Atlas", e);
        }
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.atlas.service.DocumentAtlasService#creerDocumentEtContenu
     * (fr.gouv.finances.lombok.atlas.bean.DocumentAtlas)
     */
    @Override
    public void creerDocumentEtContenu(final DocumentAtlas document)
    {
        creerDocumentEtContenu(document, true);
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.atlas.service.DocumentAtlasService#creerDocumentEtContenu
     * (fr.gouv.finances.lombok.atlas.bean.DocumentAtlas,boolean)
     */
    @Override
    public void creerDocumentEtContenu(final DocumentAtlas document, final boolean affectionNomEnvoiAutomatique)
    {
        transactionTemplate.execute(new TransactionCallbackWithoutResult()
        {
            @Override
            public void doInTransactionWithoutResult(TransactionStatus status)
            {
                document.setEtat(EtatDocumentAtlas.CREE);
                String nomFichierOriginal = document.getFichier().getNomFichierOriginal();
                String extension = StringUtils.getFilenameExtension(nomFichierOriginal);
                if (extension == null)
                {
                    throw new RegleGestionException(
                        "Le nom du fichier transféré doit impérativement contenir une extension.");
                }
                else if (!extension.matches("[a-zA-Z0-9]*"))
                {
                    throw new RegleGestionException(
                        "L'extension du fichier transféré ne doit pas contenir de caractères spéciaux.");
                }
                if (affectionNomEnvoiAutomatique)
                {
                    String nomEnvoi = document.getIdentifiantAtlas() + sepFICHIEREXTENSION + extension;
                    document.setNomEnvoi(nomEnvoi);
                }
                fichierjointserviceso.sauvegarderUnFichierJoint(document.getFichier());
                documentatlasdao.saveObject(document);
            }
        });
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param document
     * @param typeDocument
     * @see fr.gouv.finances.lombok.atlas.service.DocumentAtlasService#creerDocumentEtContenu
     * (fr.gouv.finances.lombok.atlas.bean.DocumentAtlas,fr.gouv.finances.lombok.atlas.bean.TypeDocumentAtlas)
     */
    @Override
    public void creerDocumentEtContenu(final DocumentAtlas document, final TypeDocumentAtlas typeDocument)
    {
        document.setTypeDocument(typeDocument);
        creerDocumentEtContenu(document);
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param typeDocument
     * @see fr.gouv.finances.lombok.atlas.service.DocumentAtlasService#creerTypeDocument
     * (fr.gouv.finances.lombok.atlas.bean.TypeDocumentAtlas)
     */
    @Override
    public void creerTypeDocument(final TypeDocumentAtlas typeDocument)
    {
        if (!typeDocument.getCode().matches("[a-zA-Z0-9]*"))
        {
            throw new RegleGestionException(
                "Le code du type de document ne doit pas contenir de caractères spéciaux.");
        }
        documentatlasdao.saveObject(typeDocument);
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @return string
     * @see fr.gouv.finances.lombok.atlas.service.DocumentAtlasService#genererUnUuidDAtlas()
     */
    @Override
    public String genererUnUuidDAtlas()
    {
        String uuidstring = UUID.randomUUID().toString();
        if (log.isDebugEnabled())
        {
            log.debug("nouveau uuid généré par composant atlas:" + uuidstring);
        }
        return uuidstring;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param document
     * @return string
     * @see fr.gouv.finances.lombok.atlas.service.DocumentAtlasService#genererURLDUnDocument
     * (fr.gouv.finances.lombok.atlas.bean.DocumentAtlas)
     */
    @Override
    public String genererURLDUnDocument(DocumentAtlas document)
    {
        return genererURLDUnDocument(document.getIdentifiantAtlas());
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.atlas.service.DocumentAtlasService#genererURLDUnDocument(java.lang.String)
     */
    @Override
    public String genererURLDUnDocument(String identifiantAtlas)
    {

        StringBuilder sbURL = new StringBuilder();
        PersonneAnnuaire personne = UtilisateurSecurityLombokContainer.getPersonneAnnuaire();
        sbURL.append(url).append(':').append(port).append('/').append(module).append('/').append(page).append('?')
            .append(paramENV).append('=').append(env).append('&').append(paramCODEAPPLI).append('=').append(codeAppli)
            .append('&').append(paramIDUSER).append('=').append(personne.getUid()).append('&').append(paramCLE).append(
                '=').append(cle).append('&').append(paramIDATLAS).append('=').append(identifiantAtlas)
            .append('&').append(paramPRESENTATION).append('=').append(valeurPRESENTATION);
        log.debug(sbURL.toString());
        return sbURL.toString();
    }

    /**
     * Gets the cle.
     *
     * @return the cle
     */
    public String getCle()
    {
        return cle;
    }

    /**
     * Gets the code appli.
     *
     * @return the code appli
     */
    public String getCodeAppli()
    {
        return codeAppli;
    }

    /**
     * Gets the documentatlasdao.
     *
     * @return the documentatlasdao
     */
    public DocumentAtlasDao getDocumentatlasdao()
    {
        return documentatlasdao;
    }

    /**
     * Gets the env.
     *
     * @return the env
     */
    public String getEnv()
    {
        return env;
    }

    /**
     * Gets the fichierjointserviceso.
     *
     * @return the fichierjointserviceso
     */
    public FichierJointService getFichierjointserviceso()
    {
        return fichierjointserviceso;
    }

    /**
     * Gets the module.
     *
     * @return the module
     */
    public String getModule()
    {
        return module;
    }

    /**
     * Gets the page.
     *
     * @return the page
     */
    public String getPage()
    {
        return page;
    }

    /**
     * Gets the port.
     *
     * @return the port
     */
    public String getPort()
    {
        return port;
    }

    /**
     * Gets the transaction template.
     *
     * @return the transaction template
     */
    public TransactionTemplate getTransactionTemplate()
    {
        return transactionTemplate;
    }

    /**
     * Gets the url.
     *
     * @return the url
     */
    public String getUrl()
    {
        return url;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param document
     * @param dateDEnvoiAtlas
     * @see fr.gouv.finances.lombok.atlas.service.DocumentAtlasService#marquerDocumentEnvoyeAAtlas
     * (fr.gouv.finances.lombok.atlas.bean.DocumentAtlas,
     *      java.sql.Timestamp)
     */
    @Override
    public void marquerDocumentEnvoyeAAtlas(final DocumentAtlas document, final Timestamp dateDEnvoiAtlas)
    {
        transactionTemplate.execute(new TransactionCallbackWithoutResult()
        {
            @Override
            public void doInTransactionWithoutResult(TransactionStatus status)
            {
                document.setEtat(EtatDocumentAtlas.ENVOYEPOURARCHIVAGE);
                document.setDateDEnvoiAtlas(dateDEnvoiAtlas);
                documentatlasdao.saveObject(document);
            }
        });
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param document
     * @param etat
     * @see fr.gouv.finances.lombok.atlas.service.DocumentAtlasService#mettreAJourEtatDocument
     * (fr.gouv.finances.lombok.atlas.bean.DocumentAtlas,fr.gouv.finances.lombok.atlas.bean.EtatDocumentAtlas)
     */
    @Override
    public void mettreAJourEtatDocument(final DocumentAtlas document, final EtatDocumentAtlas etat)
    {
        transactionTemplate.execute(new TransactionCallbackWithoutResult()
        {
            @Override
            public void doInTransactionWithoutResult(TransactionStatus status)
            {
                document.setEtat(etat);
                documentatlasdao.saveObject(document);
            }
        });
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @see fr.gouv.finances.lombok.atlas.service.DocumentAtlasService#nettoyerSession()
     */
    @Override
    public void nettoyerSession()
    {
        documentatlasdao.flush();
        documentatlasdao.clearPersistenceContext();
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param nombreOccurences
     * @see fr.gouv.finances.lombok.atlas.service.DocumentAtlasService#purgerContenuDocumentArchive(java.lang.Integer)
     */
    @Override
    public int purgerContenuDocumentArchive(final Integer nombreOccurences)
    {
        final List<Integer> nbDocuments = new ArrayList<>();
        try
        {
            transactionTemplate.execute(new TransactionCallbackWithoutResult()
            {
                @Override
                public void doInTransactionWithoutResult(TransactionStatus status)
                {
                    int iii = 0;
                    ScrollIterator documentsAPurger =
                        documentatlasdao.findDocumentsAtlasEtContenusAPurgerParIterateur(nombreOccurences);
                    Calendar calPurgeContenu = Calendar.getInstance();
                    Timestamp datePurgeContenu = new Timestamp(calPurgeContenu.getTimeInMillis());

                    while (documentsAPurger.hasNext())
                    {
                        DocumentAtlas document = (DocumentAtlas) documentsAPurger.nextObjetMetier();
                        FichierJoint fichier = document.getFichier();
                        if (fichier != null)
                        {
                            document.setFichier(null);
                            document.setDatePurgeContenu(datePurgeContenu);

                            documentatlasdao.saveObject(document);
                            fichierjointserviceso.supprimerUnFichierJoint(fichier);
                            iii++;
                        }

                        /*
                         * Le flush fait par lot sur une série de requetes préparées identiques est un facteur important
                         * de gain de performances pour la moteur de SGBD
                         */
                        if (iii % nombreOccurences == 0)
                        {
                            documentatlasdao.flush();
                            documentatlasdao.clearPersistenceContext();
                            log.info("flush : " + iii);
                        }
                    }
                    log.info("nombre de documents Atlas purgés : " + iii);

                    nbDocuments.add(0, iii);
                }
            });
            return nbDocuments.get(0);
        }
        catch (RuntimeException e)
        {
            throw ApplicationExceptionTransformateur.transformer(
                "Erreur lors du traitement de la purge du contenu de documents Atlas", e);
        }
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param nombreOccurences
     * @see fr.gouv.finances.lombok.atlas.service.DocumentAtlasService#purgerDocumentPerime(java.lang.Integer)
     */
    @Override
    public void purgerDocumentPerime(final Integer nombreOccurences)
    {
        try
        {
            transactionTemplate.execute(new TransactionCallbackWithoutResult()
            {
                @Override
                public void doInTransactionWithoutResult(TransactionStatus status)
                {
                    int i = 0;
                    ScrollIterator documentsPerimes =
                        documentatlasdao.findDocumentsAtlasPerimesParIterateur(nombreOccurences);
                    while (documentsPerimes.hasNext())
                    {
                        DocumentAtlas document = (DocumentAtlas) documentsPerimes.nextObjetMetier();
                        documentatlasdao.deleteObject(document);
                        i++;

                        /*
                         * Le flush fait par lot sur une série de requetes préparées identiques est un facteur important
                         * de gain de performances pour la moteur de SGBD
                         */
                        if (i % nombreOccurences == 0)
                        {
                            documentatlasdao.flush();
                            documentatlasdao.clearPersistenceContext();
                            log.info("flush : " + i);
                        }
                    }
                    log.info("nombre de documents Atlas purgés : " + i);
                }
            });

        }
        catch (RuntimeException e)
        {
            throw ApplicationExceptionTransformateur.transformer(
                "Erreur lors du traitement de la purge de documents Atlas périmés", e);
        }
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param identifiantAtlas
     * @return document atlas
     * @see fr.gouv.finances.lombok.atlas.service.DocumentAtlasService#rechercherDocumentAtlasParIdentifiantAtlas(java.lang.String)
     */
    @Override
    public DocumentAtlas rechercherDocumentAtlasParIdentifiantAtlas(String identifiantAtlas)
    {
        log.debug(">>> Debut methode rechercherDocumentAtlasParIdentifiantAtlas");
        return documentatlasdao.findDocumentAtlasParIdentifiantAtlas(identifiantAtlas);
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param nombreOccurences
     * @return dgcp scroll iterator
     * @see fr.gouv.finances.lombok.atlas.service.DocumentAtlasService#rechercherDocumentsAtlasEtContenusAArchiverParIterateur
     * (java.lang.Integer)
     */
    @Override
    public ScrollIterator rechercherDocumentsAtlasEtContenusAArchiverParIterateur(Integer nombreOccurences)
    {
        log.debug(">>> Debut methode rechercherDocumentsAtlasEtContenusAArchiverParIterateur");
        return documentatlasdao.findDocumentsAtlasEtContenusAArchiverParIterateur(nombreOccurences);
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param colIdentifiantAtlas
     * @return list
     * @see fr.gouv.finances.lombok.atlas.service.DocumentAtlasService#rechercherListeDocumentAtlasParIdentifiantAtlas(java.util.Collection)
     */
    @Override
    public List<DocumentAtlas> rechercherListeDocumentAtlasParIdentifiantAtlas(Collection<String> colIdentifiantAtlas)
    {
        log.debug(">>> Debut methode rechercherListeDocumentAtlasParIdentifiantAtlas");
        return documentatlasdao.findListeDocumentAtlasParIdentifiantAtlas(colIdentifiantAtlas);
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param typeDocument
     * @return list
     * @see fr.gouv.finances.lombok.atlas.service.DocumentAtlasService#rechercherListeDocumentAtlasParType
     * (fr.gouv.finances.lombok.atlas.bean.TypeDocumentAtlas)
     */
    @Override
    public List<DocumentAtlas> rechercherListeDocumentAtlasParType(TypeDocumentAtlas typeDocument)
    {
        log.debug(">>> Debut methode rechercherListeDocumentAtlasParType");
        return documentatlasdao.findListeDocumentAtlasParType(typeDocument);
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param typeDocument
     * @param dateDebut
     * @param dateFin
     * @return list
     * @see fr.gouv.finances.lombok.atlas.service.DocumentAtlasService#rechercherListeDocumentAtlasParTypeEtDates
     * (fr.gouv.finances.lombok.atlas.bean.TypeDocumentAtlas,java.util.Date, java.util.Date)
     */
    @Override
    public List<DocumentAtlas> rechercherListeDocumentAtlasParTypeEtDates(TypeDocumentAtlas typeDocument, Date dateDebut, Date dateFin)
    {
        return documentatlasdao.findListeDocumentAtlasParTypeEtDates(typeDocument, dateDebut, dateFin);
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @return list
     * @see fr.gouv.finances.lombok.atlas.service.DocumentAtlasService#rechercherTousLesTypesDocument()
     */
    @Override
    public List<TypeDocumentAtlas> rechercherTousLesTypesDocument()
    {
        return documentatlasdao.findTousLesTypesDocument();
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param code
     * @return type document atlas
     * @see fr.gouv.finances.lombok.atlas.service.DocumentAtlasService#rechercherTypeDocumentParCode(java.lang.String)
     */
    @Override
    public TypeDocumentAtlas rechercherTypeDocumentParCode(String code)
    {
        return documentatlasdao.findTypeDocumentParCode(code);
    }

    /**
     * Sets the cle.
     *
     * @param cle the new cle
     */
    public void setCle(String cle)
    {
        this.cle = cle;
    }

    /**
     * Sets the code appli.
     *
     * @param codeAppli the new code appli
     */
    public void setCodeAppli(String codeAppli)
    {
        this.codeAppli = codeAppli;
    }

    /**
     * Sets the documentatlasdao.
     *
     * @param documentatlasdao the new documentatlasdao
     */
    public void setDocumentatlasdao(DocumentAtlasDao documentatlasdao)
    {
        this.documentatlasdao = documentatlasdao;
    }

    /**
     * Sets the env.
     *
     * @param env the new env
     */
    public void setEnv(String env)
    {
        this.env = env;
    }

    /**
     * Sets the fichierjointserviceso.
     *
     * @param fichierjointserviceso the new fichierjointserviceso
     */
    public void setFichierjointserviceso(FichierJointService fichierjointserviceso)
    {
        this.fichierjointserviceso = fichierjointserviceso;
    }

    /**
     * Sets the module.
     *
     * @param module the new module
     */
    public void setModule(String module)
    {
        this.module = module;
    }

    /**
     * Sets the page.
     *
     * @param page the new page
     */
    public void setPage(String page)
    {
        this.page = page;
    }

    /**
     * Sets the port.
     *
     * @param port the new port
     */
    public void setPort(String port)
    {
        this.port = port;
    }

    /**
     * Sets the transaction template.
     *
     * @param transactionTemplate the new transaction template
     */
    public void setTransactionTemplate(TransactionTemplate transactionTemplate)
    {
        this.transactionTemplate = transactionTemplate;
    }

    /**
     * Sets the url.
     *
     * @param url the new url
     */
    public void setUrl(String url)
    {
        this.url = url;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param document
     * @see fr.gouv.finances.lombok.atlas.service.DocumentAtlasService#supprimerDocumentEtContenu
     * (fr.gouv.finances.lombok.atlas.bean.DocumentAtlas)
     */
    @Override
    public void supprimerDocumentEtContenu(DocumentAtlas document)
    {
        documentatlasdao.deleteObject(document);
    }

    /**
     * Gets the melserviceso.
     *
     * @return the melserviceso
     */
    public MelService getMelserviceso()
    {
        return melserviceso;
    }

    /**
     * Sets the melserviceso.
     *
     * @param melserviceso the new melserviceso
     */
    public void setMelserviceso(MelService melserviceso)
    {
        this.melserviceso = melserviceso;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.atlas.service.DocumentAtlasService#envoyerDocumentAtlasParMel
     * (fr.gouv.finances.lombok.atlas.bean.DocumentAtlas,fr.gouv.finances.lombok.mel.techbean.Mel)
     */
    @Override
    public void envoyerDocumentAtlasParMel(DocumentAtlas document, Mel mel)
    {
        try
        {
            PieceJointe pieceJointe = null;
            if (document.isArchive())
            {
                String url = genererURLDUnDocument(document);
                UrlResource urlResource = new UrlResource(url);
                pieceJointe = new PieceJointe(document.getNomEnvoi(), urlResource);
            }
            else
            {
                FichierJoint fichier = fichierjointserviceso.rechercherUnFichierJointEtSonContenuParId(document.getFichier().getId());
                byte[] data = fichier.getLeContenuDuFichier().getData();
                ByteArrayResource byteResource = new ByteArrayResource(data);
                pieceJointe = new PieceJointe(document.getNomEnvoi(), byteResource);
            }
            mel.ajouterPieceJointe(pieceJointe);
            melserviceso.envoyerMel(mel);
        }
        catch (MalformedURLException e)
        {
            throw ApplicationExceptionTransformateur.transformer("Erreur lors de l'envoi du document Atlas par mel", e);
        }
    }

    /** 
     * (methode de remplacement)
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.atlas.service.DocumentAtlasService#recupererDocumentImageSurAtlas(java.lang.String)
     */
    @Override
    public FichierJoint recupererDocumentImageSurAtlas(String url)
    {
        FichierJoint fichier = null;
        try
        {
            HttpURLConnection httpUrl = (HttpURLConnection) new URL(url).openConnection();
            if (httpUrl != null && (httpUrl.getContentType() == null || !httpUrl.getContentType().startsWith(CONTENT_TYPE_HTML)))
            {

                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                byte[] buf = new byte[DEFAULT_SIZE_BUFFER];
                int len = 0;

                while ((len = httpUrl.getInputStream().read(buf)) != -1)
                {
                    outputStream.write(buf, 0, len);
                }

                outputStream.flush();
                fichier = new FichierJoint(outputStream.toByteArray());
                outputStream.close();
            }
            else
            {
                if (log.isDebugEnabled() && httpUrl != null)
                {
                    log.debug("Document indisponible : Content-type : " + httpUrl.getContentType() + " Encoding : "
                        + httpUrl.getContentEncoding());
                    log.debug("Contenu : " + httpUrl.getContent());
                }
                throw new ProgrammationException("Document indisponible : " + url);
            }
        }
        catch (MalformedURLException exception)
        {
            log.error("URL mal formée");
            throw ApplicationExceptionTransformateur.transformer("URL mal formée", exception);
        }
        catch (IOException exception)
        {
            log.error("Erreur d'entrée/sortie");
            throw ApplicationExceptionTransformateur.transformer("Erreur d'entrée/sortie", exception);
        }
        return fichier;
    }

}
