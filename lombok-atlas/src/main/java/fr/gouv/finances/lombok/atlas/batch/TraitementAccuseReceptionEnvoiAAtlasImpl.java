/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : TraitementAccuseReceptionEnvoiAAtlasImpl.java
 *
 */
package fr.gouv.finances.lombok.atlas.batch;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.ResourceUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import fr.gouv.finances.lombok.atlas.service.DocumentAtlasService;
import fr.gouv.finances.lombok.batch.ServiceBatchCommunImpl;
import fr.gouv.finances.lombok.util.exception.ApplicationExceptionTransformateur;
import fr.gouv.finances.lombok.util.exception.ExploitationException;
import fr.gouv.finances.lombok.util.exception.ProgrammationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

/**
 * Class TraitementAccuseReceptionEnvoiAAtlasImpl.
 *
 * @author chouard-cp
 * @version $Revision: 1.4 $ Date: 15 déc. 2009
 */
@Service("traitementaccusereceptionenvoiaatlas")
@Profile("batch")
@Lazy(true)
public class TraitementAccuseReceptionEnvoiAAtlasImpl extends ServiceBatchCommunImpl
{

    /**
     * Initialisation de la journalisation.
     */
    protected static final Logger LOGGER = LoggerFactory.getLogger(TraitementAccuseReceptionEnvoiAAtlasImpl.class);

    /**
     * BALIS e_ env.
     */
    private static final String BALISE_ENV = "nomenv";

    /**
     * BALIS e_ etat.
     */
    private static final String BALISE_ETAT = "etat";

    /**
     * ETA t_ ok.
     */
    private static final String ETAT_OK = "00";

    /**
     * documentatlasserviceso.
     */
    @Autowired()
    @Qualifier("documentatlasserviceso")
    private DocumentAtlasService documentatlasserviceso;

    /**
     * fichier accuse reception.
     */
    private String fichierAccuseReception;

    /**
     * dossier accuse reception.
     */
    private String dossierAccuseReception;

    /**
     * suffixe fichier accuse reception.
     */
    private String suffixeFichierAccuseReception;

    /**
     * taille de bloc.
     */
    private Integer tailleDeBloc;

    /**
     * indique si le fichier accuse reception doit etre supprime ou non.
     */
    private boolean suppressionFichierAccuseReception;

    /**
     * informations concernant les accusés-réception
     */
    private Map<String, Integer> infosAccuseReception;

    /**
     * Constructeur de la classe TraitementAccuseReceptionEnvoiAAtlasImpl.java
     */
    public TraitementAccuseReceptionEnvoiAAtlasImpl()
    {
        super();
    }

    /**
     * Accesseur de l attribut infosAccuseReception.
     *
     * @return infosAccuseReception
     */
    public Map<String, Integer> getInfosAccuseReception()
    {
        return infosAccuseReception;
    }

    /**
     * Accesseur de l attribut suppressionFichierAccuseReception.
     *
     * @return suppressionFichierAccuseReception
     */
    public boolean isSuppressionFichierAccuseReception()
    {
        return suppressionFichierAccuseReception;
    }

    /**
     * Accesseur de l attribut documentatlasserviceso.
     *
     * @return documentatlasserviceso
     */
    public DocumentAtlasService getDocumentatlasserviceso()
    {
        return documentatlasserviceso;
    }

    /**
     * Accesseur de l attribut dossier accuse reception.
     *
     * @return dossier accuse reception
     */
    public String getDossierAccuseReception()
    {
        return dossierAccuseReception;
    }

    /**
     * Accesseur de l attribut fichier accuse reception.
     *
     * @return fichier accuse reception
     */
    public String getFichierAccuseReception()
    {
        return fichierAccuseReception;
    }

    /**
     * Accesseur de l attribut suffixe fichier accuse reception.
     *
     * @return suffixe fichier accuse reception
     */
    public String getSuffixeFichierAccuseReception()
    {
        return suffixeFichierAccuseReception;
    }

    /**
     * Accesseur de l attribut taille de bloc.
     *
     * @return taille de bloc
     */
    public Integer getTailleDeBloc()
    {
        return tailleDeBloc;
    }

    /**
     * Modificateur de l attribut infosAccuseReception.
     *
     * @param infosAccuseReception le nouveau infosAccuseReception
     */
    public void setInfosAccuseReception(Map<String, Integer> infosAccuseReception)
    {
        this.infosAccuseReception = infosAccuseReception;
    }

    /**
     * Modificateur de l attribut suppressionFichierAccuseReception.
     *
     * @param suppressionFichierAccuseReception le nouveau suppressionFichierAccuseReception
     */
    @Value("${traitementaccusereceptionenvoiaatlas.suppressionfichieraccusereception}")
    public void setSuppressionFichierAccuseReception(boolean suppressionFichierAccuseReception)
    {
        this.suppressionFichierAccuseReception = suppressionFichierAccuseReception;
    }

    /**
     * Modificateur de l attribut documentatlasserviceso.
     *
     * @param documentatlasserviceso le nouveau documentatlasserviceso
     */
    public void setDocumentatlasserviceso(DocumentAtlasService documentatlasserviceso)
    {
        this.documentatlasserviceso = documentatlasserviceso;
    }

    /**
     * Modificateur de l attribut dossier accuse reception.
     *
     * @param dossierAccuseReception le nouveau dossier accuse reception
     */
    @Value("${traitementaccusereceptionenvoiaatlas.dossieraccusereception}")
    public void setDossierAccuseReception(String dossierAccuseReception)
    {
        this.dossierAccuseReception = dossierAccuseReception;
    }

    /**
     * Modificateur de l attribut fichier accuse reception.
     *
     * @param fichierAccuseReception le nouveau fichier accuse reception
     */
    public void setFichierAccuseReception(String fichierAccuseReception)
    {
        this.fichierAccuseReception = fichierAccuseReception;
    }

    /**
     * Modificateur de l attribut suffixe fichier accuse reception.
     *
     * @param suffixeFichierAccuseReception le nouveau suffixe fichier accuse reception
     */
    @Value("${traitementaccusereceptionenvoiaatlas.suffixefichieraccusereception}")
    public void setSuffixeFichierAccuseReception(String suffixeFichierAccuseReception)
    {
        this.suffixeFichierAccuseReception = suffixeFichierAccuseReception;
    }

    /**
     * Modificateur de l attribut taille de bloc.
     *
     * @param tailleDeBloc le nouveau taille de bloc
     */
    @Value("${traitementaccusereceptionenvoiaatlas.tailledebloc}")
    public void setTailleDeBloc(Integer tailleDeBloc)
    {
        this.tailleDeBloc = tailleDeBloc;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     *
     * @see fr.gouv.finances.lombok.batch.ServiceBatchCommunImpl#traiterBatch()
     */
    @Override
    public void traiterBatch()
    {
        Calendar calAccuseReception = Calendar.getInstance();
        Timestamp dateAccuseReception = new Timestamp(calAccuseReception.getTimeInMillis());
        infosAccuseReception = new HashMap<>();
        // traitement des fichiers d'accusé reception présents dans le répertoire dossierAccuseReception
        // et suffixés par suffixeFichierAccuseReception
        // si le répertoire n'est pas au bon format ou si il n'existe pas une exception sera levée.
        File repertoire = null;
        try {
            repertoire = ResourceUtils.getFile(dossierAccuseReception);
        } catch (FileNotFoundException e) {
            throw ApplicationExceptionTransformateur.transformer("La variable dossierAccuseReception (" + dossierAccuseReception + ") injectée ne correspond pas à un dossier existant", e);
        }
        FilenameFilter filtre = (File dir, String name) -> name.endsWith(suffixeFichierAccuseReception);
        if (repertoire == null || !repertoire.isDirectory()) {
            throw new ExploitationException("La variable dossierAccuseReception (" + dossierAccuseReception + ") injectée ne correspond pas à un dossier existant");
        }
        File[] fichiersAR = repertoire.listFiles(filtre);
        if (log.isDebugEnabled()) {
            log.debug("Nombre de fichiers d'accusé réception à traiter:" + fichiersAR == null || fichiersAR.length == 0 ? "0" : String.valueOf(fichiersAR.length));
        }
        for (int i = 0; i < fichiersAR.length; i++) {
            File fichierAR = fichiersAR[i];
            if (log.isDebugEnabled()) {
                log.debug("Traitement du fichier:" + fichierAR);
            }
            traiterUnFichierAr(dateAccuseReception, fichierAR);
            if (suppressionFichierAccuseReception) {
                boolean isSupprimer = fichierAR.delete();
                if (!isSupprimer) {
                    log.info("Le fichier '" + fichierAR.getAbsolutePath() + "' n'a pas été supprimé");
                }
            }
        }
    }

    /**
     * methode Extraire contenu noeud par nom : --.
     *
     * @param document --
     * @param nom --
     * @return string
     */
    private String extraireContenuNoeudParNom(Document document, String nom)
    {
        String contenu = null;
        NodeList listeNoeuds = document.getElementsByTagName(nom);
        if (listeNoeuds != null && listeNoeuds.getLength() > 0)
        {
            Node noeud = listeNoeuds.item(0);
            contenu = noeud.getTextContent();
        }
        return contenu;
    }

    /**
     * methode Traiter erreur fichier ar : --.
     *
     * @param fichierAR --
     * @param message --
     * @param e --
     */
    private void traiterErreurFichierAr(File fichierAR, String message, Exception e)
    {
        log.error("Erreur lors du traitement du fichier " + fichierAR.getName());
        if (suppressionFichierAccuseReception)
        {
            boolean suppressionOk = fichierAR.delete();
            if (!suppressionOk)
            {
                LOGGER.info("Le fichier '{}' n'a pas été supprimé", fichierAR.getAbsolutePath());
            }
        }
        throw new ProgrammationException(message, e);
    }

    /**
     * methode Traiter un fichier ar : --.
     *
     * @param dateAccuseReception --
     * @param fichierAR --
     */
    private void traiterUnFichierAr(Timestamp dateAccuseReception, File fichierAR)
    {
        try
        {
            InputSource is = new InputSource(fichierAR.getAbsolutePath());
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            factory.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
            factory.setExpandEntityReferences(false);
            DocumentBuilder parser = factory.newDocumentBuilder();
            Document document = parser.parse(is);
            String baliseEnv = extraireContenuNoeudParNom(document, BALISE_ENV);
            Integer nbDocuments;
            if (baliseEnv == null)
            {
                traiterErreurFichierAr(fichierAR, "La balise " + BALISE_ENV + " est absente", null);
            }
            String dateArchive = null;
            String[] strings = baliseEnv.split("_TPSID");
            if (strings != null && strings.length == 2)
            {
                dateArchive = strings[1];
            }
            else
            {
                traiterErreurFichierAr(fichierAR, "Date d'envoi Atlas absente (format: _TPSIDyyyyMMddHHmmssSSS) ", null);
            }
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSS", Locale.FRANCE);
            Timestamp dateEnvoiAtlas = new Timestamp(sdf.parse(dateArchive).getTime());
            String baliseEtat = extraireContenuNoeudParNom(document, BALISE_ETAT);
            if (baliseEtat == null)
            {
                traiterErreurFichierAr(fichierAR, "La balise " + BALISE_ETAT + " est absente", null);
            }
            else if (baliseEtat.equals(ETAT_OK))
            {
                nbDocuments = documentatlasserviceso.accuserReceptionEnvoiDocumentAtlas(tailleDeBloc, dateEnvoiAtlas, dateAccuseReception);
                infosAccuseReception.put(fichierAR.getName(), nbDocuments);
            }
            else
            {
                traiterErreurFichierAr(fichierAR, "Le transfert Atlas s'est terminé en erreur (" + BALISE_ETAT + "!=" + ETAT_OK + ")",
                    null);
            }
        }
        catch (ParserConfigurationException e)
        {
            throw ApplicationExceptionTransformateur.transformer("Erreur de configuration du parseur XML", e);
        }
        catch (SAXException e)
        {
            throw ApplicationExceptionTransformateur.transformer("Erreur SAX", e);
        }
        catch (IOException e)
        {
            throw ApplicationExceptionTransformateur.transformer("Erreur d'E/S", e);
        }
        catch (ParseException e)
        {
            traiterErreurFichierAr(fichierAR, "Format de date invalide", e);
        }
    }

    @Value("${traitementaccusereceptionenvoiaatlas.versionbatch}")
    @Override()
    public void setVersionBatch(String versionBatch)
    {
        super.setVersionBatch(versionBatch);
    }

    @Value("${traitementaccusereceptionenvoiaatlas.nombatch}")
    @Override()
    public void setNomBatch(String nomBatch)
    {
        super.setNomBatch(nomBatch);
    }
}
