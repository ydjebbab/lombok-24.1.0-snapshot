package fr.gouv.finances.lombok.autoconfig;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

/**
 * Classe de configuration qui permet de conditionner les ressources à importer
 * @author celfer
 * Date: 7 févr. 2020
 */
@Configuration
@ConditionalOnProperty(name = "lombok.composant.atlas.inclus", havingValue="true")
@ImportResource({"classpath*:conf/applicationContext-atlas-dao.xml", "classpath:conf/applicationContext-atlas-service.xml"})
@ComponentScan(basePackages = {"fr.gouv.finances.lombok.atlas.jpa.dao.impl"})
@EnableConfigurationProperties(AtlasProperties.class)
public class AtlasApplicationConfig
{
    
    private static final Logger log = LoggerFactory.getLogger(AtlasApplicationConfig.class);

    @Autowired
    private AtlasProperties atlasProperties;

    @PostConstruct
    private void afficherProprietes()
    {
        if (log.isInfoEnabled())
        {
            log.info("Valeurs des propriétés du composant atlas : {}  ", atlasProperties);
        }
    }

}
