/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : TelechargementView.java
 *
 */
package fr.gouv.finances.lombok.atlas.mvc;

import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.servlet.view.AbstractView;

import fr.gouv.finances.lombok.util.exception.ProgrammationException;

/**
 * Vue utilisable depuis un enchainement dans un flux pour télécharger un fichier mis dans le contexte.
 * 
 * @author chouard-cp
 * @version $Revision: 1.2 $ Date: 15 déc. 2009
 */
public class AtlasView extends AbstractView
{

    /** Clé utilisée pour spécifier le nom du fichier à télécharger. */
    public static final String NOM_FICHIER = "TelechargementView_nomFichier";

    /**
     * Flag qui indique si l'utilisateur doit se voir proposer l'enregistrement ou l'ouverture du fichier par le
     * navigateur. Vrai par défaut
     */
    public static final String DEMANDE_ENREGISTREMENT_FICHIER = "TelechargementView_Demander_Enregistrement_Fichier";

    public static final String URL_FICHIER = "TelechargementView_urlFichier";

    /**
     * Nom de fichier par défaut utilisé quand la propriété nomFichierOriginal de l'objet FichierJoint est nulle ou
     * composée de caractères blancs.
     */
    public static final String NOM_FICHIER_PAR_DEFAULT = "telechargement";

    /** Taille du buffer utilisée pour lire le fichier temporaire et écrire sur la réponse de la servlet. */
    private static final int BUFFER_SIZE = 1024;

    /** Constant : CONTENT_TYPE_HTML. */
    private static final String CONTENT_TYPE_HTML = "text/html";

    /** Constant : log. */
    private static final Log log = LogFactory.getLog(AtlasView.class);

    /**
     * Constructeur de la classe AtlasView.java
     */
    public AtlasView()
    {
        super();

    }

    /**
     * Production de la vue.
     * 
     * @param model --
     * @param request --
     * @param response --
     */
    @Override
    protected void renderMergedOutputModel(Map model, HttpServletRequest request, HttpServletResponse response) throws IOException
    {
        HttpURLConnection httpUrl = this.getUrlFromModel(model);

        if (this.isDemanderEnregistrementFichier(model))
        {
            response.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
            response.setHeader("Pragma", "public");
            response.setHeader("Content-Disposition", "attachment; filename=\""
                + this.getNomFichier(model) + "\"");
        }

        if (httpUrl != null && httpUrl.getResponseCode() == HttpURLConnection.HTTP_OK)
        {
            response.setContentType(httpUrl.getContentType());

            response.setCharacterEncoding(httpUrl.getContentEncoding());

            if (log.isDebugEnabled())
            {
                log.debug("Content-type : " + httpUrl.getContentType() + " Encoding : " + httpUrl.getContentEncoding());

            }
            OutputStream outputStream = response.getOutputStream();
            byte[] buf = new byte[BUFFER_SIZE];
            int len = 0;

            while ((len = httpUrl.getInputStream().read(buf)) != -1)
            {
                outputStream.write(buf, 0, len);
            }

            outputStream.flush();
        }
        else
        {
            if (httpUrl != null)
            {
                if (log.isDebugEnabled())
                {
                    log.debug("Document indisponible : Content-type : " + httpUrl.getContentType() + " Encoding : "
                        + httpUrl.getContentEncoding());
                    log.debug("Contenu : " + httpUrl.getContent());
                }
                response.sendError(httpUrl.getResponseCode());
            }
        }
    }

    /**
     * Lecture du fichier à télécharger stocké dans le model de la vue.
     * 
     * @param model --
     * @return FichierJoint
     */
    private HttpURLConnection getUrlFromModel(Map model)
    {
        HttpURLConnection result = null;

        if (model.containsKey(AtlasView.URL_FICHIER))
        {
            Object object = model.get(AtlasView.URL_FICHIER);
            if (object instanceof String)
            {
                String strURL = (String) object;
                HttpURLConnection httpUrl = null;
                try
                {
                    if (log.isDebugEnabled())
                    {
                        log.debug("URL d'appel à Atlas " + strURL);
                    }
                    URL urlAltas = new URL(strURL);
                    httpUrl = (HttpURLConnection) urlAltas.openConnection();
                }
                catch (MalformedURLException e)
                {
                    log.error("Problème d'URL malformée", e);
                }
                catch (IOException e)
                {
                    log.error("Fichier introuvable", e);
                }
                result = httpUrl;
            }
            else
            {
                throw new ProgrammationException("L'URL STOCKEE DANS LE MODEL SOUS LA CLE " + URL_FICHIER
                    + " DOIT ETRE DE TYPE STRING");
            }
        }
        else
        {
            throw new ProgrammationException("AUCUNE URL N'EST STOCKEE DANS LE MODEL SOUS LA CLE "
                + URL_FICHIER);
        }

        return result;
    }

    /**
     * Lecture du nom de fichier à présenter à l'utilisateur.
     * 
     * @param model --
     * @param fichierJoint --
     * @return the nom fichier
     */
    private String getNomFichier(Map model)
    {
        String result = null;
        if (model.containsKey(AtlasView.NOM_FICHIER))
        {
            result = model.get(AtlasView.NOM_FICHIER).toString();
        }
        else
        {
            result = NOM_FICHIER_PAR_DEFAULT;
        }
        return result;
    }

    /**
     * Teste si l'enregistrement du fichier doit être proposé à l'utilisateur.
     * 
     * @param model --
     * @return true, if checks if is demander enregistrement fichier
     */
    private boolean isDemanderEnregistrementFichier(Map model)
    {
        boolean result = true;
        if (model.containsKey(DEMANDE_ENREGISTREMENT_FICHIER))
        {
            Object demandeEnregistrement = model.get(DEMANDE_ENREGISTREMENT_FICHIER);
            if (demandeEnregistrement instanceof Boolean && ((Boolean) demandeEnregistrement).booleanValue() == false)
            {
                result = false;
            }
        }

        return result;
    }

}
