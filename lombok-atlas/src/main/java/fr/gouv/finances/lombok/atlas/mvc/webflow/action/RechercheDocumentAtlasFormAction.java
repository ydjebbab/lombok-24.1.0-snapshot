/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
*
 *
 * fichier : RechercheDocumentAtlasFormAction.java
 *
 */
package fr.gouv.finances.lombok.atlas.mvc.webflow.action;

import java.beans.PropertyEditor;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import org.springframework.webflow.action.FormAction;
import org.springframework.webflow.execution.Event;
import org.springframework.webflow.execution.RequestContext;

import fr.gouv.finances.lombok.atlas.bean.DocumentAtlas;
import fr.gouv.finances.lombok.atlas.mvc.AtlasView;
import fr.gouv.finances.lombok.atlas.mvc.form.RechercheDocumentAtlasForm;
import fr.gouv.finances.lombok.atlas.service.DocumentAtlasService;
import fr.gouv.finances.lombok.mel.techbean.Mel;
import fr.gouv.finances.lombok.mvc.TelechargementView;
import fr.gouv.finances.lombok.securite.springsecurity.UtilisateurSecurityLombokContainer;
import fr.gouv.finances.lombok.securite.techbean.PersonneAnnuaire;
import fr.gouv.finances.lombok.upload.bean.FichierJoint;
import fr.gouv.finances.lombok.upload.service.FichierJointService;
import fr.gouv.finances.lombok.util.propertyeditor.SelectEditor;

/**
 * Class RechercheDocumentAtlasFormAction.
 * 
 * @author chouard-cp
 * @version $Revision: 1.5 $ Date: 14 déc. 2009
 */
public class RechercheDocumentAtlasFormAction extends FormAction
{

    /** Constant : LISTE_DOCUMENT. */
    public static final String LISTE_DOCUMENT = "listedocument";

    /** Constant : URL_DOCUMENT. */
    public static final String URL_DOCUMENT = "urldocument";

    /** Constant : FICHIER_IMAGE. */
    public static final String FICHIER_IMAGE = "fichierimg";
    
    /** documentatlasserviceso. */
    private DocumentAtlasService documentatlasserviceso;

    /** fichierjointserviceso. */
    private FichierJointService fichierjointserviceso;

    /** URL post proc */
    private String urlPostProc;
    
    public RechercheDocumentAtlasFormAction()
    {
        super();
    }
    
    /**
     * methode Binder fichier dans la liste : DGFiP.
     * 
     * @param rc param
     * @return event
     */
    @SuppressWarnings("unchecked")
    public Event binderFichierDansLaListe(RequestContext rc)
    {
        RechercheDocumentAtlasForm recherchedocumentatlasform =
            (RechercheDocumentAtlasForm) rc.getFlowScope().get(getFormObjectName());
        List<DocumentAtlas> listDocuments = (List<DocumentAtlas>) rc.getFlowScope().get(LISTE_DOCUMENT);
        PropertyEditor propertyEditor = new SelectEditor(listDocuments, "identifiantAtlas");

        propertyEditor.setAsText(rc.getRequestParameters().get("identifiantAtlas"));
        DocumentAtlas unDocumentSelectionne = (DocumentAtlas) propertyEditor.getValue();

        if (unDocumentSelectionne != null)
        {
            recherchedocumentatlasform.setDocumentCourant(unDocumentSelectionne);
        }
        return success();
    }

    /**
     * methode Executer consulter : DGFiP.
     * 
     * @param context param
     * @return event
     */
    public Event executerConsulter(RequestContext context)
    {
        RechercheDocumentAtlasForm recherchedocumentatlasform =
            (RechercheDocumentAtlasForm) context.getFlowScope().get(getFormObjectName());
        String urlDocument =
            documentatlasserviceso.genererURLDUnDocument(recherchedocumentatlasform.getDocumentCourant());
        context.getFlowScope().put(URL_DOCUMENT, urlDocument);

        return success();
    }

    /**
     * methode Executer rechercher : DGFiP.
     * 
     * @param context param
     * @return event
     */
    public Event executerRechercher(RequestContext context)
    {
        RechercheDocumentAtlasForm recherchedocumentatlasform =
            (RechercheDocumentAtlasForm) context.getFlowScope().get(getFormObjectName());
        Collection<String> setIdentifiantAtlas = new HashSet<String>();
        setIdentifiantAtlas.add(recherchedocumentatlasform.getIdentifiantAtlas());
        List<DocumentAtlas> document =
            documentatlasserviceso.rechercherListeDocumentAtlasParIdentifiantAtlas(setIdentifiantAtlas);
        context.getFlowScope().put(LISTE_DOCUMENT, document);

        return success();
    }

    /**
     * Accesseur de l attribut documentatlasserviceso.
     * 
     * @return documentatlasserviceso
     */
    public DocumentAtlasService getDocumentatlasserviceso()
    {
        return documentatlasserviceso;
    }

    /**
     * Accesseur de l attribut fichierjointserviceso.
     * 
     * @return fichierjointserviceso
     */
    public FichierJointService getFichierjointserviceso()
    {
        return fichierjointserviceso;
    }

    /**
     * Modificateur de l attribut documentatlasserviceso.
     * 
     * @param documentatlasserviceso le nouveau documentatlasserviceso
     */
    public void setDocumentatlasserviceso(DocumentAtlasService documentatlasserviceso)
    {
        this.documentatlasserviceso = documentatlasserviceso;
    }

    /**
     * Modificateur de l attribut fichierjointserviceso.
     * 
     * @param fichierjointserviceso le nouveau fichierjointserviceso
     */
    public void setFichierjointserviceso(FichierJointService fichierjointserviceso)
    {
        this.fichierjointserviceso = fichierjointserviceso;
    }

    /**
     * methode Telecharger un document : DGFiP.
     * 
     * @param context param
     * @return event
     */
    public Event telechargerUnDocument(RequestContext context)
    {

        RechercheDocumentAtlasForm recherchedocumentatlasform =
            (RechercheDocumentAtlasForm) context.getFlowScope().get(getFormObjectName());

        DocumentAtlas documentAtlas = recherchedocumentatlasform.getDocumentCourant();
        FichierJoint fichier =
            this.fichierjointserviceso.rechercherUnFichierJointEtSonContenuParId(documentAtlas.getFichier().getId());
        context.getFlowScope().put(TelechargementView.FICHIER_A_TELECHARGER, fichier);
        context.getFlowScope().put(TelechargementView.NOM_FICHIER, documentAtlas.getNomEnvoi());
        context.getFlowScope().put(TelechargementView.DEMANDE_ENREGISTREMENT_FICHIER, true);

        return success();
    }

    /**
     * methode Telecharger un document de type image : DGFiP.
     * 
     * @param context param
     * @return event
     */
    public Event telechargerUnDocumentImg(RequestContext context)
    {

        RechercheDocumentAtlasForm recherchedocumentatlasform =
            (RechercheDocumentAtlasForm) context.getFlowScope().get(getFormObjectName());

        DocumentAtlas documentAtlas = recherchedocumentatlasform.getDocumentCourant();
        FichierJoint fichier =
            this.fichierjointserviceso.rechercherUnFichierJointEtSonContenuParId(documentAtlas.getFichier().getId());
        context.getFlowScope().put(FICHIER_IMAGE, fichier);

        return success();
    }
    
    /**
     * methode Telecharger un document en rupture de flux : DGFiP.
     * 
     * @param context param
     * @return event
     */
    public Event telechargerUnDocumentEnRuptureDeFlux(RequestContext context)
    {

        RechercheDocumentAtlasForm recherchedocumentatlasform =
            (RechercheDocumentAtlasForm) context.getFlowScope().get(getFormObjectName());

        DocumentAtlas documentAtlas = recherchedocumentatlasform.getDocumentCourant();
        String url = documentatlasserviceso.genererURLDUnDocument(documentAtlas);
        context.getFlowScope().put(AtlasView.URL_FICHIER, url);
        context.getFlowScope().put(AtlasView.NOM_FICHIER, documentAtlas.getNomEnvoi());
        context.getFlowScope().put(AtlasView.DEMANDE_ENREGISTREMENT_FICHIER, true);

        return success();
    }

    /**
     * methode Telecharger un document en rupture de flux : DGFiP.
     * 
     * @param context param
     * @return event
     */
    public Event telechargerUnDocumentImgEnRuptureDeFlux(RequestContext context)
    {

//        RechercheDocumentAtlasForm recherchedocumentatlasform =
//            (RechercheDocumentAtlasForm) context.getFlowScope().get(getFormObjectName());

//        DocumentAtlas documentAtlas = recherchedocumentatlasform.getDocumentCourant();
//        String url = documentatlasserviceso.genererURLDUnDocument(documentAtlas);
        
        FichierJoint fichier = documentatlasserviceso.recupererDocumentImageSurAtlas(urlPostProc);
        context.getFlowScope().put(FICHIER_IMAGE, fichier);

        return success();
    }
    
    /**
     * methode Envoyer un document par mel : DGFiP.
     * 
     * @param context param
     * @return event
     */
    public Event envoyerUnDocumentParMel(RequestContext context)
    {

        RechercheDocumentAtlasForm recherchedocumentatlasform =
            (RechercheDocumentAtlasForm) context.getFlowScope().get(getFormObjectName());

        DocumentAtlas documentAtlas = recherchedocumentatlasform.getDocumentCourant();
        PersonneAnnuaire personneAnnuaire = UtilisateurSecurityLombokContainer.getPersonneAnnuaire();
        Mel mel = new Mel();
        mel.ajouterDestinataireA(personneAnnuaire.getMail());
        mel.setDe(personneAnnuaire.getMail());
        mel.setObjet("Atlas");
        mel.setMesssage("Test envoi de document Atlas par mel.");
        documentatlasserviceso.envoyerDocumentAtlasParMel(documentAtlas, mel);
        return success();
    }

    /**
     * Mutateur de urlPostProc
     *
     * @param urlPostProc urlPostProc
     */
    public void setUrlPostProc(String urlPostProc)
    {
        this.urlPostProc = urlPostProc;
    }
    
}
