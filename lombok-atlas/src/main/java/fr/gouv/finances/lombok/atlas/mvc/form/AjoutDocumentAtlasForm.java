/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
*
 *
 * fichier : AjoutDocumentAtlasForm.java
 *
 */
package fr.gouv.finances.lombok.atlas.mvc.form;

import java.io.Serializable;

import fr.gouv.finances.lombok.atlas.bean.DocumentAtlas;
import fr.gouv.finances.lombok.upload.bean.FichierJoint;

/**
 * Class AjoutDocumentAtlasForm DGFiP.
 * 
 * @author chouard-cp
 * @version $Revision: 1.4 $ Date: 11 déc. 2009
 */
public class AjoutDocumentAtlasForm implements Serializable
{

    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** document atlas. */
    private DocumentAtlas documentAtlas;

    /** fichier joint. */
    private FichierJoint fichierJoint;

    /**
     * Instanciation de ajout document atlas form.
     */
    public AjoutDocumentAtlasForm()
    {
        super();
    }

    /**
     * Accesseur de l attribut document atlas.
     * 
     * @return document atlas
     */
    public DocumentAtlas getDocumentAtlas()
    {
        return documentAtlas;
    }

    /**
     * Accesseur de l attribut fichier joint.
     * 
     * @return fichier joint
     */
    public FichierJoint getFichierJoint()
    {
        return fichierJoint;
    }

    /**
     * Modificateur de l attribut document atlas.
     * 
     * @param documentAtlas le nouveau document atlas
     */
    public void setDocumentAtlas(DocumentAtlas documentAtlas)
    {
        this.documentAtlas = documentAtlas;
    }

    /**
     * Modificateur de l attribut fichier joint.
     * 
     * @param fichierJoint le nouveau fichier joint
     */
    public void setFichierJoint(FichierJoint fichierJoint)
    {
        this.fichierJoint = fichierJoint;
    }

}