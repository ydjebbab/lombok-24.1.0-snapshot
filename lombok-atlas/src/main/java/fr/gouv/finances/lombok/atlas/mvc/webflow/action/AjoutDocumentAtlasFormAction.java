/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 *
 */
package fr.gouv.finances.lombok.atlas.mvc.webflow.action;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.jfree.util.Log;
import org.springframework.beans.PropertyEditorRegistry;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.validation.Errors;
import org.springframework.webflow.action.FormAction;
import org.springframework.webflow.action.FormObjectAccessor;
import org.springframework.webflow.execution.Event;
import org.springframework.webflow.execution.RequestContext;
import org.springframework.webflow.execution.ScopeType;

import fr.gouv.finances.lombok.atlas.bean.DocumentAtlas;
import fr.gouv.finances.lombok.atlas.mvc.form.AjoutDocumentAtlasForm;
import fr.gouv.finances.lombok.atlas.service.DocumentAtlasService;
import fr.gouv.finances.lombok.upload.bean.FichierJoint;
import fr.gouv.finances.lombok.upload.service.FichierJointService;
import fr.gouv.finances.lombok.util.exception.RegleGestionException;
import fr.gouv.finances.lombok.util.propertyeditor.FichierJointPropertyEditor;

/**
 * Class AjoutDocumentAtlasFormAction DGFiP.
 * 
 * @author chouard-cp
 */
public class AjoutDocumentAtlasFormAction extends FormAction
{
    /** UPLOA d_ ma x_ size. */
    private static String UPLOAD_MAX_SIZE = "uploadmaxsize";

    /** documentatlasserviceso. */
    private DocumentAtlasService documentatlasserviceso;

    /** fichierjointserviceso. */
    private FichierJointService fichierjointserviceso;

    /**
     * Constructeur.
     */
    public AjoutDocumentAtlasFormAction()
    {
        super();
    }

    /**
     * Constructeur.
     * 
     * @param arg0 param
     */
    public AjoutDocumentAtlasFormAction(Class arg0)
    {
        super(arg0);
    }

    /**
     * methode Ajouter un document atlas : DGFiP.
     * 
     * @param context param
     * @return event
     */
    public Event ajouterUnDocumentAtlas(RequestContext context)
    {
        AjoutDocumentAtlasForm ajoutDocumentAtlasForm =
            (AjoutDocumentAtlasForm) context.getConversationScope().get(getFormObjectName());
        try
        {
            documentatlasserviceso.creerDocumentEtContenu(ajoutDocumentAtlasForm.getDocumentAtlas());
        }
        catch (RegleGestionException rge)
        {
            Log.debug("Exception détectée", rge);
            Errors errors = new FormObjectAccessor(context).getFormErrors(getFormObjectName(), ScopeType.REQUEST);
            errors.reject(null, rge.getMessage());
            return result("incorrectdata");
        }
        return success();
    }

    /**
     * methode Generer identifiant atlas : DGFiP.
     * 
     * @param context param
     * @return event
     */
    public Event genererIdentifiantAtlas(RequestContext context)
    {
        AjoutDocumentAtlasForm ajoutDocumentAtlasForm =
            (AjoutDocumentAtlasForm) context.getConversationScope().get(getFormObjectName());
        String identifiantAtlas = documentatlasserviceso.genererUnUuidDAtlas();
        ajoutDocumentAtlasForm.getDocumentAtlas().setIdentifiantAtlas(identifiantAtlas);
        return success();
    }

    
    /**
     * Accesseur de l attribut file upload max size.
     *
     * @param context DOCUMENTEZ_MOI
     * @return file upload max size
     */
    public Event getFileUploadMaxSize(RequestContext context) {
        context.getFlowScope().put(UPLOAD_MAX_SIZE, fichierjointserviceso.getUploadmaxsize());
        return success();
    }
    
    /**
     * Gets the documentatlasserviceso.
     *
     * @return the documentatlasserviceso
     */
    public DocumentAtlasService getDocumentatlasserviceso()
    {
        return documentatlasserviceso;
    }

    /**
     * Gets the fichierjointserviceso.
     *
     * @return the fichierjointserviceso
     */
    public FichierJointService getFichierjointserviceso()
    {
        return fichierjointserviceso;
    }

    /**
     * methode Initialiser formulaire : DGFiP.
     * 
     * @param context param
     * @return event
     */
    public Event initialiserFormulaire(RequestContext context)
    {
        AjoutDocumentAtlasForm ajoutDocumentAtlasForm =
            (AjoutDocumentAtlasForm) context.getConversationScope().get(getFormObjectName());
        DocumentAtlas document = new DocumentAtlas();
        document.setFichier(new FichierJoint());
        ajoutDocumentAtlasForm.setDocumentAtlas(document);
        return success();
    }

    /**
     * methode Initialisertransfert : DGFiP.
     * 
     * @param context param
     * @return event
     */
    public Event initialisertransfert(RequestContext context)
    {
        AjoutDocumentAtlasForm ajoutDocumentAtlasForm =
            (AjoutDocumentAtlasForm) context.getConversationScope().get(getFormObjectName());
        ajoutDocumentAtlasForm.setFichierJoint(new FichierJoint());
        context.getFlowScope().put(UPLOAD_MAX_SIZE, fichierjointserviceso.getUploadmaxsize());
        return success();
    }

    /**
     * Sets the documentatlasserviceso.
     *
     * @param documentatlasserviceso the new documentatlasserviceso
     */
    public void setDocumentatlasserviceso(DocumentAtlasService documentatlasserviceso)
    {
        this.documentatlasserviceso = documentatlasserviceso;
    }

    /**
     * Sets the fichierjointserviceso.
     *
     * @param fichierjointserviceso the new fichierjointserviceso
     */
    public void setFichierjointserviceso(FichierJointService fichierjointserviceso)
    {
        this.fichierjointserviceso = fichierjointserviceso;
    }

    /**
     * methode Validertransfert : DGFiP.
     * 
     * @param context param
     * @return event
     */
    public Event validertransfert(RequestContext context)
    {
        AjoutDocumentAtlasForm ajoutDocumentAtlasForm =
            (AjoutDocumentAtlasForm) context.getConversationScope().get(getFormObjectName());
        FichierJoint unFichierJoint = ajoutDocumentAtlasForm.getFichierJoint();

        ajoutDocumentAtlasForm.getDocumentAtlas().setFichier(unFichierJoint);

        return success();
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param reqc
     * @param per
     * @see org.springframework.webflow.action.FormAction#registerPropertyEditors(org.springframework.webflow.execution.RequestContext,
     *      org.springframework.beans.PropertyEditorRegistry)
     */
    @Override
    protected void registerPropertyEditors(RequestContext reqc, PropertyEditorRegistry per)
    {
        super.registerPropertyEditors(reqc, per);
        per.registerCustomEditor(FichierJoint.class, "fichierJoint", new FichierJointPropertyEditor());

        SimpleDateFormat dateformat1 = new SimpleDateFormat("dd/MM/yyyy H:m:s", Locale.FRANCE);
        per.registerCustomEditor(Date.class, "fichierJoint.dateHeureSoumission",
            new CustomDateEditor(dateformat1, true));
        SimpleDateFormat dateformat2 = new SimpleDateFormat("dd/MM/yyyy", Locale.FRANCE);
        per.registerCustomEditor(Date.class, "documentAtlas.dateDePeremption", new CustomDateEditor(dateformat2, true));
    }

}
