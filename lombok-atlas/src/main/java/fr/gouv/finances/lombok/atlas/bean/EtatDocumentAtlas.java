/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 */
package fr.gouv.finances.lombok.atlas.bean;

/**
 * Enumération des états d'un document Atlas.
 * 
 * @author amleplatinec
 * @author Christophe Breheret-Girardin
 */
public enum EtatDocumentAtlas
{

    /** Cree. */
    CREE(0),

    /** A archiver. */
    AARCHIVER(1),

    /** Envoye pour archivage. */
    ENVOYEPOURARCHIVAGE(2),

    /** Archive. */
    ARCHIVE(3);

    /** Valeur de l'état. */
    private int value;

    /**
     * Constructeur.
     * 
     * @param value valeur de l'état
     */
    private EtatDocumentAtlas(int value)
    {
        this.value = value;
    }

    /**
     * {@inheritDoc}.
     *
     * @see java.lang.Enum#toString()
     */
    @Override
    public String toString()
    {
        String string = "";
        switch (this)
        {
            case AARCHIVER:
                string = "À Archiver";
                break;
            case ENVOYEPOURARCHIVAGE:
                string = "Envoyé poour archivage";
                break;
            case ARCHIVE:
                string = "Archivé";
                break;
            case CREE:
            default:
                string = "Créé";
                break;
        }
        return string;
    }

}
