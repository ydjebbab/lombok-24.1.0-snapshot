/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
*
 *
 * fichier : AjoutDocumentAtlasValidator.java
 *
 */
package fr.gouv.finances.lombok.atlas.mvc.validator;

import org.apache.commons.validator.GenericValidator;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import fr.gouv.finances.lombok.atlas.bean.DocumentAtlas;
import fr.gouv.finances.lombok.atlas.mvc.form.AjoutDocumentAtlasForm;

/**
 * Class AjoutDocumentAtlasValidator DGFiP.
 * 
 * @author chouard-cp
 * @version $Revision: 1.5 $ Date: 11 déc. 2009
 */
public class AjoutDocumentAtlasValidator implements Validator
{

    /**
     * Instanciation de ajout document atlas validator.
     */
    public AjoutDocumentAtlasValidator()
    {
        super();
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param clazz
     * @return true, si c'est vrai
     * @see org.springframework.validation.Validator#supports(java.lang.Class)
     */
    @Override
    public boolean supports(Class clazz)
    {
        return clazz.equals(AjoutDocumentAtlasForm.class);
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param arg0
     * @param arg1
     * @see org.springframework.validation.Validator#validate(java.lang.Object, org.springframework.validation.Errors)
     */
    @Override
    public void validate(Object arg0, Errors arg1)
    {
        // Methode vide
    }

    /**
     * methode Validate surface document atlas : DGFiP.
     * 
     * @param command param
     * @param errors param
     */
    public void validateSurfaceDocumentAtlas(AjoutDocumentAtlasForm command, Errors errors)
    {

        DocumentAtlas documentAtlas = command.getDocumentAtlas();

        if (documentAtlas.getFichier() == null || documentAtlas.getFichier().getTailleFichier() == 0)
        {
            errors.reject("documentatlas.vide", "Aucun fichier à ajouter ou fichier vide");
        }

        if (GenericValidator.isBlankOrNull(documentAtlas.getNom()))
        {
            errors.rejectValue("documentAtlas.nom", "rejected.documentAtlas.nom", "Le champ \"nom\" est obligatoire");
        }

    }

}
