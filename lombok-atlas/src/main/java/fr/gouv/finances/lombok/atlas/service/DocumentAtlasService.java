/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : DocumentAtlasService.java
 *
 */
package fr.gouv.finances.lombok.atlas.service;

import java.sql.Timestamp;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import fr.gouv.finances.lombok.atlas.bean.DocumentAtlas;
import fr.gouv.finances.lombok.atlas.bean.EtatDocumentAtlas;
import fr.gouv.finances.lombok.atlas.bean.TypeDocumentAtlas;
import fr.gouv.finances.lombok.mel.techbean.Mel;
import fr.gouv.finances.lombok.upload.bean.FichierJoint;
import fr.gouv.finances.lombok.util.persistance.ScrollIterator;

/**
 * Interface DocumentAtlasService.
 * 
 * @author chouard-cp
 * @author celfer
 * @version $Revision: 1.6 $ Date: 15 déc. 2009
 */
public interface DocumentAtlasService
{

    /**
     * methode Accuser reception envoi document atlas 
     *
     * @param nombreOccurences --
     * @param dateDEnvoiAtlas --
     * @param dateAccuseReception --
     * @return int
     */
    int accuserReceptionEnvoiDocumentAtlas(Integer nombreOccurences, Timestamp dateDEnvoiAtlas,
        Timestamp dateAccuseReception);

    /**
     * methode Annuler envoi documents a atlas
     * 
     * @param nombreOccurences --
     * @param dateDEnvoiAtlas --
     */
    void annulerEnvoiDocumentsAAtlas(Integer nombreOccurences, Timestamp dateDEnvoiAtlas);

    /**
     * methode Creer document et contenu 
     * 
     * @param document --
     */
    void creerDocumentEtContenu(DocumentAtlas document);

    /**
     * methode Creer document et contenu 
     * 
     * @param document --
     * @param affectionNomEnvoiAutomatique 
     */
    void creerDocumentEtContenu(DocumentAtlas document, boolean affectionNomEnvoiAutomatique);

    /**
     * méthode permettant de créer un document atlas d'un type donné.
     * 
     * @param document le document à créer
     * @param typeDocument le type de document
     */
    void creerDocumentEtContenu(DocumentAtlas document, TypeDocumentAtlas typeDocument);

    /**
     * méthode permettant de créer un type de document Atlas.
     * 
     * @param typeDocument le type de document à créer
     */
    void creerTypeDocument(TypeDocumentAtlas typeDocument);

    /**
     * methode Generer un uuid d atlas 
     * 
     * @return string
     */
    String genererUnUuidDAtlas();

    /**
     * methode Generer urld un document 
     * 
     * @param identifiantAtlas --
     * @return string
     */
    String genererURLDUnDocument(String identifiantAtlas);

    /**
     * methode Generer urld un document 
     * 
     * @param document --
     * @return string
     */
    String genererURLDUnDocument(DocumentAtlas document);

    /**
     * methode Marquer document envoye a atlas 
     * 
     * @param document --
     * @param dateDEnvoiAtlas --
     */
    void marquerDocumentEnvoyeAAtlas(DocumentAtlas document, Timestamp dateDEnvoiAtlas);

    /**
     * methode Mettre a jour etat document 
     * 
     * @param document --
     * @param etat --
     */
    void mettreAJourEtatDocument(DocumentAtlas document, EtatDocumentAtlas etat);

    /**
     * methode permettant de vider le cache de la session hibernate de DocumentAtlasBatchDao.
     */
    void nettoyerSession();

   
    /**
     * methode Purger contenu document archive
     *
     * @param nombreOccurences
     * @return int
     */
    int purgerContenuDocumentArchive(Integer nombreOccurences);

    /**
     * methode Purger document perime 
     * 
     * @param nombreOccurences --
     */
    void purgerDocumentPerime(Integer nombreOccurences);

    /**
     * methode Rechercher document atlas par identifiant atlas 
     * 
     * @param identifiantAtlas --
     * @return document atlas
     */
    DocumentAtlas rechercherDocumentAtlasParIdentifiantAtlas(String identifiantAtlas);

    /**
     * methode Rechercher documents atlas et contenus a archiver par iterateur 
     * 
     * @param nombreOccurences --
     * @return dgcp scroll iterator
     */
    ScrollIterator rechercherDocumentsAtlasEtContenusAArchiverParIterateur(Integer nombreOccurences);

    /**
     * methode Rechercher liste document atlas par identifiant atlas 
     * 
     * @param colIdentifiantAtlas --
     * @return list< document atlas>
     */
    List<DocumentAtlas> rechercherListeDocumentAtlasParIdentifiantAtlas(Collection<String> colIdentifiantAtlas);

    /**
     * Méthode permettant de rechercher les documents Atlas d'un type donné.
     * 
     * @param typeDocument le type de document recherché
     * @return la liste des documents atlas du type donné
     */
    List<DocumentAtlas> rechercherListeDocumentAtlasParType(TypeDocumentAtlas typeDocument);

    /**
     * Méthode permettant de rechercher les documents Atlas d'un type donné et dont la date de péremption se situe dans
     * un intervalle de dates donné.
     * 
     * @param typeDocument le type de document recherché
     * @param dateDebut la date de début de période pour la recherche sur la date de péremption
     * @param dateFin la fin de début de période pour la recherche sur la date de péremption
     * @return la liste des documents atlas du type donné et dont la date de péremption se situe dans l'intervalle donné
     */
    List<DocumentAtlas> rechercherListeDocumentAtlasParTypeEtDates(TypeDocumentAtlas typeDocument, Date dateDebut, Date dateFin);

    /**
     * Méthode retournant tous les types de documents atlas existants.
     * 
     * @return la liste des types de documents atlas
     */
    List<TypeDocumentAtlas> rechercherTousLesTypesDocument();

    /**
     * Méthode permettant de rechercher un type de document par son code.
     * 
     * @param code le code du type de document recherché
     * @return le type de document recherché
     */
    TypeDocumentAtlas rechercherTypeDocumentParCode(String code);

    /**
     * methode Supprimer document et contenu 
     * 
     * @param document --
     */
    void supprimerDocumentEtContenu(DocumentAtlas document);

    /**
     * Méthode permetttant d'envoyer un document par mel.
     * 
     * @param document le document à envoyer
     * @param mel les informations relatives au mel
     */
    void envoyerDocumentAtlasParMel(DocumentAtlas document, Mel mel);

    /**
     * methode Recuperer document image sur atlas.
     *
     * @param url DOCUMENTEZ_MOI
     * @return fichier joint
     */
    FichierJoint recupererDocumentImageSurAtlas(String url);
    
}