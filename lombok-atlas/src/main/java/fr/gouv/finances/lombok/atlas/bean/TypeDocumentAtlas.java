/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 */
package fr.gouv.finances.lombok.atlas.bean;

import java.util.Objects;

import fr.gouv.finances.lombok.util.base.BaseBean;

/**
 * Donnée représentant un type de document Atlas.
 *
 * @author ageffroy2-cp
 * @author Christophe Breheret-Girardin
 */
public class TypeDocumentAtlas extends BaseBean
{

    /** Initialisation de l'UID. */
    private static final long serialVersionUID = -2973467140351682477L;

    /** Identifiant technique. */
    private Long id;

    /** Version. */
    private int version;

    /** Code du type de document. */
    private String code;

    /** Libellé du type de document. */
    private String libelle;

    /**
     * Constructeur.
     */
    public TypeDocumentAtlas()
    {
        super();
    }

    /**
     * Accesseur de l'attribut code.
     * 
     * @return code
     */
    public String getCode()
    {
        return code;
    }

    /**
     * Accesseur de l'attribut id.
     * 
     * @return id
     */
    public Long getId()
    {
        return id;
    }

    /**
     * Accesseur de l'attribut libelle.
     * 
     * @return libelle
     */
    public String getLibelle()
    {
        return libelle;
    }

    /**
     * Accesseur de l'attribut version.
     * 
     * @return version
     */
    public int getVersion()
    {
        return version;
    }

    /**
     * Modificateur de l'attribut code.
     * 
     * @param code the new code
     */
    public void setCode(String code)
    {
        this.code = code;
    }

    /**
     * Modificateur de l'attribut id.
     * 
     * @param id le nouveau id
     */
    public void setId(Long id)
    {
        this.id = id;
    }

    /**
     * Modificateur de l'attribut libelle.
     * 
     * @param libelle the new libelle
     */
    public void setLibelle(String libelle)
    {
        this.libelle = libelle;
    }

    /**
     * Modificateur de l'attribut version.
     * 
     * @param version le nouveau version
     */
    public void setVersion(int version)
    {
        this.version = version;
    }

    /**
     * {@inheritDoc}
     * 
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode()
    {
        return Objects.hash(code);
    }

    /**
     * {@inheritDoc}
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object object)
    {
        if (object == null)
        {
            return false;
        }

        if (this.getClass() != object.getClass())
        {
            return false;
        }

        TypeDocumentAtlas typeDocumentAtlas = (TypeDocumentAtlas) object;
        return Objects.equals(code, typeDocumentAtlas.code);
    }
}
