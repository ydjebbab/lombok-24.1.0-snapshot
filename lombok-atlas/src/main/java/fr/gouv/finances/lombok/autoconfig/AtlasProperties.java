package fr.gouv.finances.lombok.autoconfig;

import org.hibernate.validator.constraints.URL;
import org.springframework.boot.context.properties.ConfigurationProperties;

import fr.gouv.finances.lombok.autoconfig.LombokComposantProperties;

/**
 * @author celinio fernandes Date: Feb 19, 2020
 */
@ConfigurationProperties(prefix = "lombok.composant.atlas")
public class AtlasProperties extends LombokComposantProperties
{
    @URL
    private String url;

    private int port;

    private String module;

    private String page;

    private String env;

    private int codeappli;

    private String cle;

    public String getUrl()
    {
        return url;
    }

    public void setUrl(String url)
    {
        this.url = url;
    }

    public int getPort()
    {
        return port;
    }

    public void setPort(int port)
    {
        this.port = port;
    }

    public String getModule()
    {
        return module;
    }

    public void setModule(String module)
    {
        this.module = module;
    }

    public String getPage()
    {
        return page;
    }

    public void setPage(String page)
    {
        this.page = page;
    }

    public String getEnv()
    {
        return env;
    }

    public void setEnv(String env)
    {
        this.env = env;
    }

    public int getCodeappli()
    {
        return codeappli;
    }

    public void setCodeappli(int codeappli)
    {
        this.codeappli = codeappli;
    }

    public String getCle()
    {
        return cle;
    }

    public void setCle(String cle)
    {
        this.cle = cle;
    }

    @Override
    public String toString()
    {
        return "AtlasProperties [inclus=" + inclus + ", url=" + url + ", port=" + port + ", module=" + module + ", page=" + page + ", env="
            + env + ", codeappli=" + codeappli + ", cle=" + cle + "]";
    }

}
