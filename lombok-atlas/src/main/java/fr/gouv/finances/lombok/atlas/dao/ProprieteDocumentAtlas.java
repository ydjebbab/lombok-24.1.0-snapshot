/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 */
package fr.gouv.finances.lombok.atlas.dao;

/**
 * Propriété liée à la donnée Message
 *      
 * @author Christophe Breheret-Girardin
 */
public enum ProprieteDocumentAtlas
{
    /** Propriété type de document */
    PROPRIETE_TYPE_DOCUMENT("typeDocument"),

    /** Propriété identifiant du document */
    IDENTIFIANT("identifiantAtlas"),

    /** Propriété date de péremption */
    PROPRIETE_DATE_PEREMPTION("dateDePeremption"),

    /** Propriété code du type de document */
    PROPRIETE_CODE_TYPE_DOCUMENT("code"),

    /** Propriété état du document */
    PROPRIETE_ETAT("etat"),

    /** Propriété fichier relié au document */
    PROPRIETE_FICHIER("fichier"),
    
    /** Propriété contenu du fichier */
    PROPRIETE_CONTENU_FICHIER("leContenuDuFichier"),

    /** Propriété date d'envoi du document */
    PROPRIETE_DATE_ENVOI("dateDEnvoiAtlas");

    /** Nom de la propriété */
    private String nom;

    /**
     * Constructeur.
     *
     * @param libelleNom libellé du nom de la propriété
     */
    private ProprieteDocumentAtlas(String libelleNom)
    {
        nom = libelleNom;
    }

    /**
     * Accesseur de nom
     *
     * @return nom de la propriété
     */
    public String getNom()
    {
        return nom;
    }
    
}
