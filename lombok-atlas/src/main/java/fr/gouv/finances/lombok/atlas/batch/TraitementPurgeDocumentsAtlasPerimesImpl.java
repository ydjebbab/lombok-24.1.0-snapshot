/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : TraitementPurgeDocumentsAtlasPerimesImpl.java
 *
 */
package fr.gouv.finances.lombok.atlas.batch;

import fr.gouv.finances.lombok.atlas.service.DocumentAtlasService;
import fr.gouv.finances.lombok.batch.ServiceBatchCommunImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

/**
 * Class TraitementPurgeDocumentsAtlasPerimesImpl.
 *
 * @author chouard-cp
 * @version $Revision: 1.2 $ Date: 15 déc. 2009
 */
@Service("traitementpurgedocumentsatlasperimes")
@Profile("batch")
@Lazy(true)
public class TraitementPurgeDocumentsAtlasPerimesImpl extends ServiceBatchCommunImpl {

    /**
     * documentatlasserviceso.
     */
    @Autowired()
    @Qualifier("documentatlasserviceso")
    private DocumentAtlasService documentatlasserviceso;

    /**
     * taille de bloc.
     */
    private Integer tailleDeBloc;

    /**
     * Constructeur de la classe TraitementPurgeDocumentsAtlasPerimesImpl.java
     */
    public TraitementPurgeDocumentsAtlasPerimesImpl() {
        super();       
    }

    /**
     * Accesseur de l attribut documentatlasserviceso.
     *
     * @return documentatlasserviceso
     */
    public DocumentAtlasService getDocumentatlasserviceso() {
        return documentatlasserviceso;
    }

    /**
     * Accesseur de l attribut taille de bloc.
     *
     * @return taille de bloc
     */
    public Integer getTailleDeBloc() {
        return tailleDeBloc;
    }

    /**
     * Modificateur de l attribut documentatlasserviceso.
     *
     * @param documentatlasserviceso le nouveau documentatlasserviceso
     */
    public void setDocumentatlasserviceso(DocumentAtlasService documentatlasserviceso) {
        this.documentatlasserviceso = documentatlasserviceso;
    }

    /**
     * Modificateur de l attribut taille de bloc.
     *
     * @param tailleDeBloc le nouveau taille de bloc
     */
    @Value("${traitementpurgedocumentsatlasperimes.tailledebloc}")
    public void setTailleDeBloc(Integer tailleDeBloc) {
        this.tailleDeBloc = tailleDeBloc;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     *
     * @see fr.gouv.finances.lombok.batch.ServiceBatchCommunImpl#traiterBatch()
     */
    @Override
    public void traiterBatch()
    {
        documentatlasserviceso.purgerDocumentPerime(tailleDeBloc);
    }

    @Value("${traitementpurgedocumentsatlasperimes.versionbatch}")
    @Override()
    public void setVersionBatch(String versionBatch) {
        super.setVersionBatch(versionBatch);
    }

    @Value("${traitementpurgedocumentsatlasperimes.nombatch}")
    @Override()
    public void setNomBatch(String nomBatch) {
        super.setNomBatch(nomBatch);
    }
}
