/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 */
package fr.gouv.finances.lombok.atlas.bean;

import java.sql.Timestamp;
import java.util.Date;
import java.util.Objects;

import fr.gouv.finances.lombok.upload.bean.FichierJoint;
import fr.gouv.finances.lombok.util.base.BaseBean;

/**
 * Donnée représentant un document Atlas.
 *
 * @author chouard-cp
 * @author Christophe Breheret-Girardin
 */
public class DocumentAtlas extends BaseBean
{

    /** Initialisation de l'UID. */
    private static final long serialVersionUID = 1L;

    /** Identifiant technique. */
    private Long id;

    /** Identifiant Atlas. */
    private String identifiantAtlas;

    /** Nom du document. */
    private String nom;

    /** Nom envoi. */
    private String nomEnvoi;

    /** Version. */
    private int version;

    /** Etat du document. */
    private EtatDocumentAtlas etat;

    /** Date de péremption du document. */
    private Date dateDePeremption;

    /** Date d'envoi du document. */
    private Timestamp dateDEnvoiAtlas;

    /** Date accusé de réception du document. */
    private Timestamp dateAccuseReception;

    /** Date de purge du contenu du document. */
    private Timestamp datePurgeContenu;

    /** Fichier lié au document. */
    private FichierJoint fichier;

    /** Type du document. */
    private TypeDocumentAtlas typeDocument;

    /**
     * Constructeur.
     */
    public DocumentAtlas()
    {
        super();
    }

    /**
     * Accesseur de l'attribut date accuse reception.
     * 
     * @return date accuse reception
     */
    public Timestamp getDateAccuseReception()
    {
        return dateAccuseReception;
    }

    /**
     * Accesseur de l'attribut date d envoi atlas.
     * 
     * @return date d envoi atlas
     */
    public Timestamp getDateDEnvoiAtlas()
    {
        return dateDEnvoiAtlas;
    }

    /**
     * Accesseur de l'attribut date de peremption.
     * 
     * @return date de peremption
     */
    public Date getDateDePeremption()
    {
        return dateDePeremption;
    }

    /**
     * Accesseur de l'attribut date purge contenu.
     * 
     * @return date purge contenu
     */
    public Timestamp getDatePurgeContenu()
    {
        return datePurgeContenu;
    }

    /**
     * Accesseur de l'attribut etat.
     * 
     * @return etat
     */
    public EtatDocumentAtlas getEtat()
    {
        return etat;
    }

    /**
     * Accesseur de l'attribut fichier.
     * 
     * @return fichier
     */
    public FichierJoint getFichier()
    {
        return fichier;
    }

    /**
     * Accesseur de l'attribut id.
     *
     * @return id
     */
    public Long getId()
    {
        return id;
    }

    /**
     * Accesseur de l'attribut identifiant atlas.
     * 
     * @return identifiant atlas
     */
    public String getIdentifiantAtlas()
    {
        return identifiantAtlas;
    }

    /**
     * Accesseur de l'attribut nom.
     * 
     * @return nom
     */
    public String getNom()
    {
        return nom;
    }

    /**
     * Accesseur de l'attribut nom envoi.
     * 
     * @return nom envoi
     */
    public String getNomEnvoi()
    {
        return nomEnvoi;
    }

    /**
     * Accesseur de l'attribut type de document.
     * 
     * @return type de document
     */
    public TypeDocumentAtlas getTypeDocument()
    {
        return typeDocument;
    }

    /**
     * Accesseur de l'attribut version.
     * 
     * @return version
     */
    public int getVersion()
    {
        return version;
    }

    /**
     * Verifie si c'est une archive.
     * 
     * @return true si c'est une archive, false sinon
     */
    public boolean isArchive()
    {
        return EtatDocumentAtlas.ARCHIVE.equals(etat);
    }

    /**
     * Modificateur de l'attribut date accuse reception.
     * 
     * @param dateAccuseReception le nouveau date accuse reception
     */
    public void setDateAccuseReception(Timestamp dateAccuseReception)
    {
        this.dateAccuseReception = dateAccuseReception;
    }

    /**
     * Modificateur de l'attribut date d envoi atlas.
     * 
     * @param dateDEnvoiAtlas le nouveau date d envoi atlas
     */
    public void setDateDEnvoiAtlas(Timestamp dateDEnvoiAtlas)
    {
        this.dateDEnvoiAtlas = dateDEnvoiAtlas;
    }

    /**
     * Modificateur de l'attribut date de peremption.
     * 
     * @param dateDePeremption le nouveau date de peremption
     */
    public void setDateDePeremption(Date dateDePeremption)
    {
        this.dateDePeremption = dateDePeremption;
    }

    /**
     * Modificateur de l'attribut date purge contenu.
     * 
     * @param datePurgeContenu le nouveau date purge contenu
     */
    public void setDatePurgeContenu(Timestamp datePurgeContenu)
    {
        this.datePurgeContenu = datePurgeContenu;
    }

    /**
     * Modificateur de l'attribut etat.
     * 
     * @param etat le nouveau etat
     */
    public void setEtat(EtatDocumentAtlas etat)
    {
        this.etat = etat;
    }

    /**
     * Modificateur de l'attribut fichier.
     * 
     * @param fichier le nouveau fichier
     */
    public void setFichier(FichierJoint fichier)
    {
        this.fichier = fichier;
    }

    /**
     * Modificateur de l'attribut id.
     * 
     * @param id le nouveau id
     */
    public void setId(Long id)
    {
        this.id = id;
    }

    /**
     * Modificateur de l'attribut identifiant atlas.
     * 
     * @param identifiantAtlas le nouveau identifiant atlas
     */
    public void setIdentifiantAtlas(String identifiantAtlas)
    {
        this.identifiantAtlas = identifiantAtlas;
    }

    /**
     * Modificateur de l'attribut nom.
     * 
     * @param nom le nouveau nom
     */
    public void setNom(String nom)
    {
        this.nom = nom;
    }

    /**
     * Modificateur de l'attribut nom envoi.
     * 
     * @param nomEnvoi le nouveau nom envoi
     */
    public void setNomEnvoi(String nomEnvoi)
    {
        this.nomEnvoi = nomEnvoi;
    }

    /**
     * Modificateur de l'attribut type de document.
     * 
     * @param typeDocument the new type du document
     */
    public void setTypeDocument(TypeDocumentAtlas typeDocument)
    {
        this.typeDocument = typeDocument;
    }

    /**
     * Modificateur de l'attribut version.
     * 
     * @param version le nouveau version
     */
    public void setVersion(int version)
    {
        this.version = version;
    }

    /**
     * {@inheritDoc}
     * 
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode()
    {
        return Objects.hash(identifiantAtlas);
    }

    /**
     * {@inheritDoc}
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object object)
    {
        if (object == null)
        {
            return false;
        }

        if (this.getClass() != object.getClass())
        {
            return false;
        }

        DocumentAtlas documentAtlas = (DocumentAtlas) object;
        return Objects.equals(identifiantAtlas, documentAtlas.identifiantAtlas);
    }

}
