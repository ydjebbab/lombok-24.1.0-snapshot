/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
*
 *
 * fichier : RechercheDocumentAtlasForm.java
 *
 */
package fr.gouv.finances.lombok.atlas.mvc.form;

import java.io.Serializable;

import fr.gouv.finances.lombok.atlas.bean.DocumentAtlas;

/**
 * Class RechercheDocumentAtlasForm.
 * 
 * @author chouard-cp
 * @version $Revision: 1.4 $ Date: 14 déc. 2009
 */
public class RechercheDocumentAtlasForm implements Serializable
{

    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** identifiant atlas. */
    private String identifiantAtlas;

    /** document courant. */
    private DocumentAtlas documentCourant;

    /**
     * Instanciation de recherche document atlas form.
     */
    public RechercheDocumentAtlasForm()
    {
        // RAS
    }

    /**
     * Accesseur de l attribut document courant.
     * 
     * @return document courant
     */
    public DocumentAtlas getDocumentCourant()
    {
        return documentCourant;
    }

    /**
     * Accesseur de l attribut identifiant atlas.
     * 
     * @return identifiant atlas
     */
    public String getIdentifiantAtlas()
    {
        return identifiantAtlas;
    }

    /**
     * Modificateur de l attribut document courant.
     * 
     * @param documentCourant le nouveau document courant
     */
    public void setDocumentCourant(DocumentAtlas documentCourant)
    {
        this.documentCourant = documentCourant;
    }

    /**
     * Modificateur de l attribut identifiant atlas.
     * 
     * @param identifiantAtlas le nouveau identifiant atlas
     */
    public void setIdentifiantAtlas(String identifiantAtlas)
    {
        this.identifiantAtlas = identifiantAtlas;
    }

}
