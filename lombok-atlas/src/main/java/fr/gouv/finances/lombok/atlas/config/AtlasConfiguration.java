package fr.gouv.finances.lombok.atlas.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import fr.gouv.finances.lombok.atlas.mvc.AtlasView;

/**
 * <pre>
 * Cette classe remplace le fichier APPLICATIONCONTEXT-VIEWS.XML initialement 
 * présent dans le module LOMBOK-WEBFRAGMENT
 * </pre>
 * 
 * @author celfer Date: 1 oct. 2019
 */
@Configuration
public class AtlasConfiguration
{

    @Profile("atlas")
    @Bean(name = "/atlas")
    public AtlasView atlas()
    {
        return new AtlasView();
    }
}
