/*
 * Copyright (c) 2017 DGFiP - Tous droits réservés
 * 
*/
// Projet : lombok-atlas
/**
 * Documentation du paquet fr.gouv.finances.lombok.atlas.mvc
 * @author chouard
 * @version 1.0
 */
package fr.gouv.finances.lombok.atlas.mvc;