/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
*
 *
 * fichier : RechercheDocumentAtlasValidator.java
 *
 */
package fr.gouv.finances.lombok.atlas.mvc.validator;

import org.apache.commons.validator.GenericValidator;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import fr.gouv.finances.lombok.atlas.mvc.form.RechercheDocumentAtlasForm;

/**
 * Class RechercheDocumentAtlasValidator.
 * 
 * @author chouard-cp
 * @version $Revision: 1.5 $ Date: 14 déc. 2009
 */
public class RechercheDocumentAtlasValidator implements Validator
{

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param clazz
     * @return true, si c'est vrai
     * @see org.springframework.validation.Validator#supports(java.lang.Class)
     */
    @Override
    public boolean supports(Class clazz)
    {
        return clazz.equals(RechercheDocumentAtlasForm.class);
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param command
     * @param errors
     * @see org.springframework.validation.Validator#validate(java.lang.Object, org.springframework.validation.Errors)
     */
    @Override
    public void validate(Object command, Errors errors)
    {
        // Methode vide
    }

    /**
     * methode Validate surface identifiant atlas : DGFiP.
     * 
     * @param command param
     * @param errors param
     */
    public void validateSurfaceIdentifiantAtlas(RechercheDocumentAtlasForm command, Errors errors)
    {
        if (GenericValidator.isBlankOrNull(command.getIdentifiantAtlas()))
        {
            errors.rejectValue("identifiantAtlas", "rejected.identifiantAtlas",
                "La saisie de l'identifiant est obligatoire.");
        }
    }

}
