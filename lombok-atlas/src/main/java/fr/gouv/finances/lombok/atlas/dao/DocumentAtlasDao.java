/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 */
package fr.gouv.finances.lombok.atlas.dao;

import java.sql.Timestamp;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import fr.gouv.finances.lombok.atlas.bean.DocumentAtlas;
import fr.gouv.finances.lombok.atlas.bean.TypeDocumentAtlas;
import fr.gouv.finances.lombok.util.base.CoreBaseDao;
import fr.gouv.finances.lombok.util.persistance.ScrollIterator;

/**
 * Interface de gestion des données des documents Atlas.
 * 
 * @author amleplatinec
 * @author Christophe Breheret-Girardin
 * @author Celfer
 */
public interface DocumentAtlasDao extends CoreBaseDao
{

    /**
     * Méthode de recherche d'un document par identifiant Atlas.
     *
     * @param identifiantAtlas identifiant Atlas du document
     * @return le document correspondant
     */
    DocumentAtlas findDocumentAtlasParIdentifiantAtlas(String identifiantAtlas);

    /**
     * Méthode de recherche des documents correspondant aux identifiants Atlas fournis.
     *
     * @param colIdentifiantAtlas identifiants Atlas des documents
     * @return les documents correspondant
     */
    List<DocumentAtlas> findListeDocumentAtlasParIdentifiantAtlas(Collection<String> colIdentifiantAtlas);

    /**
     * Méthode de recherche des documents correspondant à un type de document.
     *
     * @param typeDocument type de document à rechercher
     * @return les documents correspondant
     */
    List<DocumentAtlas> findListeDocumentAtlasParType(TypeDocumentAtlas typeDocument);

    /**
     * Méthode permettant de rechercher en base les documents Atlas d'un type donné et dont la date de péremption se
     * situe dans un intervalle de dates donné.
     * 
     * @param typeDocument le type de document recherché
     * @param dateDebut la date de début de période pour la recherche sur la date de péremption
     * @param dateFin la fin de début de période pour la recherche sur la date de péremption
     * @return la liste des documents atlas du type donné et dont la date de péremption se situe dans l'intervalle donné
     */
    List<DocumentAtlas> findListeDocumentAtlasParTypeEtDates(TypeDocumentAtlas typeDocument
        , Date dateDebut, Date dateFin);

    /**
     * Méthode retournant tous les types de documents Atlas.
     *
     * @return la liste des types de documents Atlas
     */
    List<TypeDocumentAtlas> findTousLesTypesDocument();

    /**
     * Méthode permettant de rechercher un type de document par son code.
     *
     * @param code le code du type de document recherché
     * @return le type de document recherché
     */
    TypeDocumentAtlas findTypeDocumentParCode(String code);

    /**
     * Méthode permettant d'exécuter une recherche de document à archiver, en mode curseur.
     *
     * @param nombreOccurences taille d'extraction des données (important pour les résultats volumineux)
     * @return un objet permettant de parcourir les résultats en mode curseur
     */
    ScrollIterator findDocumentsAtlasEtContenusAArchiverParIterateur(Integer nombreOccurences);

    /**
     * Méthode permettant d'exécuter une recherche de document à purger, en mode curseur.
     *
     * @param nombreOccurences taille d'extraction des données (important pour les résultats volumineux)
     * @return un objet permettant de parcourir les résultats en mode curseur
     */
    ScrollIterator findDocumentsAtlasEtContenusAPurgerParIterateur(Integer nombreOccurences);

    /**
     * Méthode permettant d'exécuter une recherche de document périmé, en mode curseur.
     *
     * @param nombreOccurences taille d'extraction des données (important pour les résultats volumineux)
     * @return un objet permettant de parcourir les résultats en mode curseur
     */
    ScrollIterator findDocumentsAtlasPerimesParIterateur(Integer nombreOccurences);

    /**
     * Méthode permettant d'exécuter une recherche de document envoyé, en mode curseur.
     *
     * @param nombreOccurences taille d'extraction des données (important pour les résultats volumineux)
     * @param dateDEnvoiAtlas date d'envoi des documents (égalité stricte donc attention à la partie horaire)
     * @return un objet permettant de parcourir les résultats en mode curseur
     */
    ScrollIterator findDocumentsEnvoyesAAtlasParIterateur(Integer nombreOccurences, Timestamp dateDEnvoiAtlas);

}
