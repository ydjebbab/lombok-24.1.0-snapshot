/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : TraitementEnvoiDocumentsAAtlasImpl.java
 *
 */
package fr.gouv.finances.lombok.atlas.batch;

import java.io.IOException;
import java.net.URL;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;
import fr.gouv.finances.lombok.atlas.bean.DocumentAtlas;
import fr.gouv.finances.lombok.atlas.service.DocumentAtlasService;
import fr.gouv.finances.lombok.batch.ServiceBatchCommunImpl;
import fr.gouv.finances.lombok.upload.bean.ContenuFichier;
import fr.gouv.finances.lombok.upload.bean.FichierJoint;
import fr.gouv.finances.lombok.util.exception.ApplicationExceptionTransformateur;
import fr.gouv.finances.lombok.util.persistance.ScrollIterator;
import fr.gouv.finances.lombok.util.tar.ArchiveTar;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

/**
 * Class TraitementEnvoiDocumentsAAtlasImpl.
 *
 * @author chouard-cp
 * @version $Revision: 1.5 $ Date: 15 déc. 2009
 */
@Service("traitementenvoidocumentsaatlas")
@Profile("batch")
@Lazy(true)
public class TraitementEnvoiDocumentsAAtlasImpl extends ServiceBatchCommunImpl {

    /**
     * Constant : BALISE_DOCUMENT.
     */
    private static final String BALISE_DOCUMENT = "document";

    /**
     * Constant : ATTRIBUT_NOM_FICHIER.
     */
    private static final String ATTRIBUT_NOM_FICHIER = "nom";

    /**
     * Constant : BALISE_IDENTIFIANT.
     */
    private static final String BALISE_IDENTIFIANT = "identifiant";

    /**
     * Constant : BALISE_DATE_PEREMPTION.
     */
    private static final String BALISE_DATE_PEREMPTION = "Datesuppr";

    /**
     * Constant : FICHIER_INDEX.
     */
    private static final String FICHIER_INDEX = "index.xml";

    /**
     * documentatlasserviceso.
     */
    @Autowired()
    @Qualifier("documentatlasserviceso")
    private DocumentAtlasService documentatlasserviceso;

    /**
     * nom appli.
     */
    private String nomAppli;

    /**
     * fichier archive.
     */
    private String fichierArchive;

    /**
     * taille de bloc.
     */
    private Integer tailleDeBloc;

    /**
     * taille d'enveloppe maximale.
     */
    private long tailleEnveloppeMax;

    /**
     * nombre de documents par enveloppe maximal.
     */
    private Integer nbDocumentsParEnveloppeMax;

    /**
     * envoi ou non de la date de suppression à Atlas.
     */
    private boolean envoiDateSuppression;

    /**
     * transaction template.
     */
    @Autowired()
    @Qualifier("transactionTemplate")
    private TransactionTemplate transactionTemplate;

    /**
     * informations concernant les enveloppes produites
     */
    private Map<String, Integer> infosEnveloppes;

    /**
     * Constructeur de la classe TraitementEnvoiDocumentsAAtlasImpl.java
     */
    public TraitementEnvoiDocumentsAAtlasImpl() {
        super();        
    }

    /**
     * Accesseur de l attribut infosEnveloppes.
     *
     * @return infosEnveloppes
     */
    public Map<String, Integer> getInfosEnveloppes() {
        return infosEnveloppes;
    }

    /**
     * Accesseur de l attribut documentatlasserviceso.
     *
     * @return documentatlasserviceso
     */
    public DocumentAtlasService getDocumentatlasserviceso() {
        return documentatlasserviceso;
    }

    /**
     * Accesseur de l attribut fichier archive.
     *
     * @return fichier archive
     */
    public String getFichierArchive() {
        return fichierArchive;
    }

    /**
     * Accesseur de l attribut nombre de documents par enveloppe maximal.
     *
     * @return nombre de documents par enveloppe maximal
     */
    public Integer getNbDocumentsParEnveloppeMax() {
        return nbDocumentsParEnveloppeMax;
    }

    /**
     * Accesseur de l attribut nom appli.
     *
     * @return nom appli
     */
    public String getNomAppli() {
        return nomAppli;
    }

    /**
     * Accesseur de l attribut taille de bloc.
     *
     * @return taille de bloc
     */
    public Integer getTailleDeBloc() {
        return tailleDeBloc;
    }

    /**
     * Accesseur de l attribut taille d'enveloppe maximale.
     *
     * @return taille d'enveloppe maximale
     */
    public long getTailleEnveloppeMax() {
        return tailleEnveloppeMax;
    }

    /**
     * Accesseur de l attribut envoi date de suppression.
     *
     * @return envoi date de suppression
     */
    public boolean isEnvoiDateSuppression() {
        return envoiDateSuppression;
    }

    /**
     * Modificateur de l attribut infosEnveloppes.
     *
     * @param infosEnveloppes le nouveau infosEnveloppes
     */
    public void setInfosEnveloppes(Map<String, Integer> infosEnveloppes) {
        this.infosEnveloppes = infosEnveloppes;
    }

    /**
     * Modificateur de l attribut documentatlasserviceso.
     *
     * @param documentatlasserviceso le nouveau documentatlasserviceso
     */
    public void setDocumentatlasserviceso(DocumentAtlasService documentatlasserviceso) {
        this.documentatlasserviceso = documentatlasserviceso;
    }

    /**
     * Modificateur de l attribut fichier archive.
     *
     * @param fichierArchive le nouveau fichier archive
     */
    @Value("${traitementenvoidocumentsaatlas.fichierarchiveatlas}")
    public void setFichierArchive(String fichierArchive) {
        this.fichierArchive = fichierArchive;
    }

    /**
     * Modificateur de l attribut nombre de documents par enveloppe maximal.
     *
     * @param nbDocumentsParEnveloppeMax the new nombre de documents par enveloppe maximal
     */
    @Value("${traitementenvoidocumentsaatlas.nbdocumentsparenveloppemax}")
    public void setNbDocumentsParEnveloppeMax(Integer nbDocumentsParEnveloppeMax) {
        this.nbDocumentsParEnveloppeMax = nbDocumentsParEnveloppeMax;
    }

    /**
     * Modificateur de l attribut nom appli.
     *
     * @param nomAppli le nouveau nom appli
     */
    @Value("${traitementenvoidocumentsaatlas.nomappli}")
    public void setNomAppli(String nomAppli) {
        this.nomAppli = nomAppli;
    }

    /**
     * Modificateur de l attribut taille de bloc.
     *
     * @param tailleDeBloc le nouveau taille de bloc
     */
    @Value("${traitementenvoidocumentsaatlas.tailledebloc}")
    public void setTailleDeBloc(Integer tailleDeBloc) {
        this.tailleDeBloc = tailleDeBloc;
    }

    /**
     * Modificateur de l attribut taille d'enveloppe maximale.
     *
     * @param tailleEnveloppeMax la nouvelle taille d'enveloppe maximale
     */
    @Value("${traitementenvoidocumentsaatlas.tailleenveloppemax}")
    public void setTailleEnveloppeMax(long tailleEnveloppeMax) {
        this.tailleEnveloppeMax = tailleEnveloppeMax;
    }

    /**
     * Modificateur de l attribut envoi date de suppression.
     *
     * @param envoiDateSuppression le nouvel envoi date de suppression
     */
    @Value("${traitementenvoidocumentsaatlas.envoidatesuppression}")
    public void setEnvoiDateSuppression(boolean envoiDateSuppression) {
        this.envoiDateSuppression = envoiDateSuppression;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     *
     * @see fr.gouv.finances.lombok.batch.ServiceBatchCommunImpl#traiterBatch()
     */
    @Override
    public void traiterBatch()
    {
        infosEnveloppes = new HashMap<>();
        transactionTemplate.execute(new TransactionCallbackWithoutResult() {

            public void doInTransactionWithoutResult(TransactionStatus status) {
                Timestamp dateDEnvoiAtlas = null;
                int i = 0;
                try {
                    ScrollIterator documentsAEnvoyer = documentatlasserviceso.rechercherDocumentsAtlasEtContenusAArchiverParIterateur(tailleDeBloc);
                    while (documentsAEnvoyer.hasNext()) {
                        Calendar calEnvoiAtlas = Calendar.getInstance();
                        dateDEnvoiAtlas = new Timestamp(calEnvoiAtlas.getTimeInMillis());
                        /*
                         * Variables permettant de vérifier si la taille maximale de l'enveloppe ou le nombre maximal de
                         * documents contenus dans l'enveloppe sont atteints
                         */
                        long tailleEnveloppe = 0;
                        Integer nbDocuments = 0;
                        // Création du fichier d'index xml
                        Document doc = DocumentHelper.createDocument();
                        doc.setXMLEncoding("UTF-8");
                        Element rootXMLDocuments = doc.addElement(nomAppli);
                        // Création de l'archive TAR pour envoi à Atlas
                        String nomFichierArchive = new URL(fichierArchive).getFile();
                        SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmssSSS", Locale.FRANCE);
                        nomFichierArchive = nomFichierArchive.replace("{DATEENVOI}", format.format(dateDEnvoiAtlas));
                        ArchiveTar archiveTar = new ArchiveTar(nomFichierArchive);
                        while (tailleEnveloppe < tailleEnveloppeMax && nbDocuments < nbDocumentsParEnveloppeMax && documentsAEnvoyer.hasNext()) {
                            // Lecture du document à traiter
                            DocumentAtlas document = (DocumentAtlas) documentsAEnvoyer.nextObjetMetier();
                            documentatlasserviceso.nettoyerSession();
                            if (document.getFichier() != null && document.getFichier().getLeContenuDuFichier() != null) {
                                FichierJoint fichier = document.getFichier();
                                ContenuFichier contenu = fichier.getLeContenuDuFichier();
                                // Ecriture des informations du fichier dans le fichier d'index XML
                                Element documentXML = rootXMLDocuments.addElement(BALISE_DOCUMENT).addAttribute(ATTRIBUT_NOM_FICHIER, document.getNomEnvoi());
                                documentXML.addElement(BALISE_IDENTIFIANT).addText(document.getIdentifiantAtlas().toString());
                                if (document.getDateDePeremption() != null && envoiDateSuppression) {
                                    SimpleDateFormat formatDate = new SimpleDateFormat("yyyyMMdd", Locale.FRANCE);
                                    String dateDePeremption = formatDate.format(document.getDateDePeremption());
                                    documentXML.addElement(BALISE_DATE_PEREMPTION).addText(dateDePeremption);
                                }
                                // Ecriture du contenu du fichier dans l'archive TAR
                                archiveTar.creerEntree(document.getNomEnvoi(), contenu.getData());
                                // Mise à jour de la taille et du nombre de documents de l'enveloppe
                                nbDocuments++;
                                tailleEnveloppe = tailleEnveloppe + fichier.getTailleFichier();
                                documentatlasserviceso.marquerDocumentEnvoyeAAtlas(document, dateDEnvoiAtlas);
                                /*
                                 * Le flush fait par lot sur une série de requetes préparées identiques est un facteur
                                 * important de gain de performances pour la moteur de SGBD
                                 */
                                i++;
                                if (i % tailleDeBloc == 0) {
                                    documentatlasserviceso.nettoyerSession();
                                    log.info("flush : " + i);
                                }
                            }
                        }
                        // Ecriture du fichier d'index XML dans l'archive TAR
                        archiveTar.creerEntree(FICHIER_INDEX, doc.asXML().getBytes("UTF-8"));
                        // Clôture l'archive TAR
                        archiveTar.fermerArchive();
                        // Ajout du nom et du nombre de documents de l'enveloppe produite
                        infosEnveloppes.put(nomFichierArchive, nbDocuments);
                    }
                    log.info("nombre de documents traités : " + i);
                } catch (IOException e) {
                    log.error("Erreur d'entrée / sortie sur " + fichierArchive, e);
                    documentatlasserviceso.annulerEnvoiDocumentsAAtlas(tailleDeBloc, dateDEnvoiAtlas);
                    throw ApplicationExceptionTransformateur.transformer("Erreur d'entrée / sortie sur " + fichierArchive, e);
                }
            }
        });
    }

    /**
     * Modificateur de l attribut transaction template.
     *
     * @param transactionTemplate le nouveau transaction template
     */
    public void setTransactionTemplate(TransactionTemplate transactionTemplate) {
        this.transactionTemplate = transactionTemplate;
    }

    /**
     * Accesseur de l attribut transaction template.
     *
     * @return transaction template
     */
    public TransactionTemplate getTransactionTemplate() {
        return transactionTemplate;
    }

    @Value("${traitementenvoidocumentsaatlas.versionbatch}")
    @Override()
    public void setVersionBatch(String versionBatch) {
        super.setVersionBatch(versionBatch);
    }

    @Value("${traitementenvoidocumentsaatlas.nombatch}")
    @Override()
    public void setNomBatch(String nomBatch) {
        super.setNomBatch(nomBatch);
    }
}
