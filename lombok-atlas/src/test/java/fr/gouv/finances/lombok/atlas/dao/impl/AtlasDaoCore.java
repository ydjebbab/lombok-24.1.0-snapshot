package fr.gouv.finances.lombok.atlas.dao.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.MimeTypeUtils;

import fr.gouv.finances.lombok.atlas.bean.DocumentAtlas;
import fr.gouv.finances.lombok.atlas.bean.EtatDocumentAtlas;
import fr.gouv.finances.lombok.atlas.bean.TypeDocumentAtlas;
import fr.gouv.finances.lombok.atlas.dao.DocumentAtlasDao;
import fr.gouv.finances.lombok.test.controle.ControleDonnees;
import fr.gouv.finances.lombok.upload.bean.FichierJoint;
import fr.gouv.finances.lombok.util.base.PurgeTablesDao;
import fr.gouv.finances.lombok.util.date.TemporaliteUtils;

/**
 * Socle de test de gestion des données des documents Atlas
 *
 * @author Christophe Breheret-Girardin
 */
public class AtlasDaoCore
{
    /** Déclaration du DAO de gestion des données des documents Atlas. */
    @Autowired
    protected DocumentAtlasDao documentatlasdao;

    /** Déclaration du DAO de gestion des purges de la base. */
    @Autowired
    protected PurgeTablesDao purgetablesdao;

    /** Map contenant les types de document Atlas présents en base */
    protected Map<Integer, TypeDocumentAtlas> mapTypeDocumentAtlas = new HashMap<>();

    /** Map contenant les documents Atlas présents en base */
    protected Map<Integer, DocumentAtlas> mapDocumentAtlas = new HashMap<>();

    /** Map contenant les fichiers joints présents en base */
    protected Map<Integer, FichierJoint> mapFichierJoint = new HashMap<>();

    /** Valeur inconnue */
    protected static final String VALEUR_INCONNUE = "kamoulox";

    /** Constante donnant le préfix d'un contenu de fichier */
    public static final String PREFIX_CONTENU_FICHIER = "Fichier ";

    /**
     * Méthode permettant de générer un type de document Atlas.
     *
     * @param indice indice du type de document
     * @return le type de document Atlas généré
     */
    protected TypeDocumentAtlas getTypeDocument(int indice)
    {
        TypeDocumentAtlas type = new TypeDocumentAtlas();
        type.setCode(String.valueOf(indice));
        type.setLibelle("libellé " + indice);

        return type;
    }

    /**
     * Méthode permettant de générer un document Atlas.
     *
     * @param indice indice du document
     * @param typeDocument type de document
     * @return le document Atlas généré
     */
    protected DocumentAtlas getDocument(int indice)
    {
        DocumentAtlas document = new DocumentAtlas();
        document.setIdentifiantAtlas(String.valueOf(indice));
        document.setNom("Nom " + indice);

        switch (indice)
        {
            case 1:
                document.setEtat(EtatDocumentAtlas.AARCHIVER);
                document.setTypeDocument(mapTypeDocumentAtlas.get(1));
                document.setDateDePeremption(TemporaliteUtils.getTimestamp(2018, 1, 8));
                document.setDateDEnvoiAtlas(TemporaliteUtils.getTimestamp(2018, 1, 8));
                document.setFichier(mapFichierJoint.get(1));
                break;
            case 2:
                document.setEtat(EtatDocumentAtlas.AARCHIVER);
                document.setTypeDocument(mapTypeDocumentAtlas.get(1));
                document.setDateDePeremption(TemporaliteUtils.getTimestamp(2010, 1, 8));
                document.setDateDEnvoiAtlas(TemporaliteUtils.getTimestamp(2010, 1, 8));
                break;
            case 3 :
                document.setEtat(EtatDocumentAtlas.AARCHIVER);
                document.setTypeDocument(mapTypeDocumentAtlas.get(2));
                document.setDateDePeremption(TemporaliteUtils.getDemain());
                document.setDateDEnvoiAtlas(TemporaliteUtils.getTimestamp(2018, 1, 9));
                break;
            case 4 :
                document.setEtat(EtatDocumentAtlas.ENVOYEPOURARCHIVAGE);
                document.setTypeDocument(mapTypeDocumentAtlas.get(1));
                document.setFichier(mapFichierJoint.get(1));
                document.setDateDEnvoiAtlas(TemporaliteUtils.getTimestamp(2018, 1, 9));
                break;
            case 5 :
                document.setEtat(EtatDocumentAtlas.ARCHIVE);
                document.setTypeDocument(mapTypeDocumentAtlas.get(1));
                document.setFichier(mapFichierJoint.get(2));
                document.setDateDEnvoiAtlas(TemporaliteUtils.getTimestamp(2018, 1, 9));
                break;
            case 6 :
                document.setEtat(EtatDocumentAtlas.ARCHIVE);
                document.setTypeDocument(mapTypeDocumentAtlas.get(1));
                document.setFichier(mapFichierJoint.get(1));
                document.setDateDEnvoiAtlas(TemporaliteUtils.getTimestamp(2018, 1, 9));
                break;
            default :
                document.setEtat(EtatDocumentAtlas.ENVOYEPOURARCHIVAGE);
                document.setTypeDocument(mapTypeDocumentAtlas.get(1));
        }

        return document;
    }

    /**
     * Méthode permettant de générer un document Atlas.
     *
     * @param indice indice du message
     * @param typeDocument type de document
     * @param etat état du document
     * @return le document Atlas généré
     */
    protected DocumentAtlas getDocument(int indice, EtatDocumentAtlas etat)
    {
        DocumentAtlas document = getDocument(indice);
        document.setEtat(etat);

        return document;
    }

    /**
     * Méthode permettant de générer un fichier joint.
     *
     * @param indice indice du fichier joint
     * @return le fichier joint généré
     */
    protected FichierJoint getFichierJoint(int indice)
    {
        FichierJoint fichier = new FichierJoint((PREFIX_CONTENU_FICHIER + indice).getBytes());
        fichier.setNomFichierOriginal(String.valueOf(indice));
        fichier.setTypeMimeFichier(MimeTypeUtils.TEXT_PLAIN.getType());
        fichier.setDateHeureSoumission(TemporaliteUtils.getTimestamp(2018, 5, 4));
        fichier.setTailleFichier(indice);
        return fichier;
    }

    /**
     * Méthode permettant de contrôler des documents par rapport à ceux attendus.
     *
     * @param documents documents à contrôler
     * @param isTypeDocumentCharge les types de document doivent-ils être chargés ?
     * @param isFichierCharge les fichiers joints aux documents doivent-ils être chargés ?
     * @param documentsAttendus les documents attendus
     */
    protected void verifierDocuments(List<DocumentAtlas> documents
        , boolean isTypeDocumentCharge, boolean isFichierCharge, DocumentAtlas... documentsAttendus)
    {
        // Vérification des données
        ControleDonnees.verifierElements(documents, documentsAttendus);

        // Vérification des associations
        documents.stream().forEach(document ->
        {
            verifierTypeDocument(document, isTypeDocumentCharge);
            verifierFichierJoint(document, isFichierCharge);
        });
    }

    /**
     * Méthode permettant de contrôler un document par rapport à celui attendu.
     *
     * @param document document à contrôler
     * @param isTypeDocumentCharge le type de document doit-il être chargé ?
     * @param isFichierCharge le fichier joint au document doit-il être chargé ?
     * @param documentAttendu le document attendu
     */
    protected void verifierDocument(DocumentAtlas document
        , boolean isTypeDocumentCharge, boolean isFichierCharge, DocumentAtlas documentAttendu)
    {
        // Vérification des données
        assertEquals("Le document '" + document.getIdentifiantAtlas() + "' n'est pas celui attendu"
            , document, documentAttendu);

        // Vérification des associations
        verifierTypeDocument(document, isTypeDocumentCharge);
        verifierFichierJoint(document, isFichierCharge);
    }

    /**
     * Méthode permettant de contrôler un type de document.
     *
     * @param document document dont il faut contrôler le type
     * @param isTypeDocumentCharge le type de document doit-il être chargé ?
     */
    protected void verifierTypeDocument(DocumentAtlas document, boolean isTypeDocumentCharge)
    {
        // Le type de document a-t-il bien été chargé (devrait toujours l'être)
        if (!isTypeDocumentCharge)
        {
            assertFalse("Le type du document '" + document.getIdentifiantAtlas() + "' a été chargé"
                , documentatlasdao.isCharged(document.getTypeDocument()));
        }
        else
        {
            assertTrue("Le type du document '" + document.getIdentifiantAtlas() + "' n'a pas été chargé"
                , documentatlasdao.isCharged(document.getTypeDocument()));

            TypeDocumentAtlas typeDocumentAttendu;
            switch (Integer.parseInt(document.getIdentifiantAtlas()))
            {
                case 1 :
                    typeDocumentAttendu = mapTypeDocumentAtlas.get(1);
                    break;
                case 2 :
                    typeDocumentAttendu = mapTypeDocumentAtlas.get(1);
                    break;
                case 3 :
                    typeDocumentAttendu = mapTypeDocumentAtlas.get(2);
                    break;
                default :
                    typeDocumentAttendu = mapTypeDocumentAtlas.get(1);
            }
            assertEquals("Le type du document '" + document.getIdentifiantAtlas() + "' n'est pas celui attendu"
                , document.getTypeDocument(), typeDocumentAttendu);
        }
    }

    /**
     * Méthode permettant de contrôler un fichier joint au document.
     *
     * @param document document dont il faut contrôler le fichier joint
     * @param isFichierCharge le fichier joint au document doit-il être chargé ?
     */
    protected void verifierFichierJoint(DocumentAtlas document, boolean isFichierCharge)
    {
        switch (Integer.parseInt(document.getIdentifiantAtlas()))
        {
            case 1 :
                verifierFichierJoint(document, isFichierCharge, mapFichierJoint.get(1));
                break;
            case 4 :
                verifierFichierJoint(document, isFichierCharge, mapFichierJoint.get(1));
                break;
            case 5 :
                verifierFichierJoint(document, isFichierCharge, mapFichierJoint.get(2));
                break;
            case 6 :
                verifierFichierJoint(document, isFichierCharge, mapFichierJoint.get(1));
                break;
            default :
                assertNull("Aucun fichier joint du document ne devrait être présent", document.getFichier());
        }
    }

    /**
     * Méthode permettant de contrôler la valeur d'un fichier joint au document.
     *
     * @param document document dont il faut vérifier la valeur du fichier joint
     * @param isFichierCharge le fichier joint au document doit-il être chargé ?
     * @param fichierJointAttendu fichier joint attendu
     */
    private void verifierFichierJoint(DocumentAtlas document, boolean isFichierCharge
        , FichierJoint fichierJointAttendu)
    {
        if (!isFichierCharge)
        {
            assertFalse("Le fichier joint du document '" + document.getIdentifiantAtlas()
                + "' ne devrait pas être chargé", documentatlasdao.isCharged(document.getFichier()));
        }
        else
        {
            assertTrue("Le fichier joint du document '" + document.getIdentifiantAtlas()
                + "' devrait être chargé", documentatlasdao.isCharged(document.getFichier()));
            assertEquals("Le fichier joint du document '" + document.getIdentifiantAtlas()
                + "' n'est pas celui attendu", document.getFichier(), fichierJointAttendu);
        }
    }
}