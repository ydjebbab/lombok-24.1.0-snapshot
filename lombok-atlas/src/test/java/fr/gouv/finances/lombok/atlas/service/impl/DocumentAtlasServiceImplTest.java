/*
 * Copyright (c) 2016 DGFiP - Tous droits réservés
 * 
 */
package fr.gouv.finances.lombok.atlas.service.impl;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Matchers;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionException;
import org.springframework.transaction.support.AbstractPlatformTransactionManager;
import org.springframework.transaction.support.DefaultTransactionStatus;
import org.springframework.transaction.support.TransactionTemplate;

import fr.gouv.finances.lombok.atlas.bean.DocumentAtlas;
import fr.gouv.finances.lombok.atlas.bean.EtatDocumentAtlas;
import fr.gouv.finances.lombok.atlas.bean.TypeDocumentAtlas;
import fr.gouv.finances.lombok.atlas.dao.DocumentAtlasDao;
import fr.gouv.finances.lombok.mel.service.MelService;
import fr.gouv.finances.lombok.mel.service.impl.MelServiceImpl;
import fr.gouv.finances.lombok.mel.techbean.Mel;
import fr.gouv.finances.lombok.upload.bean.ContenuFichier;
import fr.gouv.finances.lombok.upload.bean.FichierJoint;
import fr.gouv.finances.lombok.upload.service.FichierJointService;
import fr.gouv.finances.lombok.upload.service.impl.FichierJointServiceImpl;
import fr.gouv.finances.lombok.util.exception.ProgrammationException;
import fr.gouv.finances.lombok.util.exception.RegleGestionException;
import fr.gouv.finances.lombok.util.persistance.ScrollIterator;

/**
 * DocumentAtlasServiceImplTest.java Classe de test unitaire du service atlas
 * 
 * @author christian.houard Date: 25 oct. 2016
 */
public class DocumentAtlasServiceImplTest
{

    /** doc at. */
    private DocumentAtlasServiceImpl docAt;

    /** doc at dao. */
    private DocumentAtlasDao docAtDao;

    /** fichierjointserviceso. */
    private FichierJointService fichierjointserviceso;
    
    /** melservice. */
    private MelService melservice;

    /**
     * methode Sets the up : .
     *
     * @throws Exception the exception
     */
    @Before
    public void setUp() throws Exception
    {
        docAt = new DocumentAtlasServiceImpl();
        docAtDao = mock(DocumentAtlasDao.class);
        docAt.setDocumentatlasdao(docAtDao);
        fichierjointserviceso = mock(FichierJointServiceImpl.class);
        
        melservice = mock(MelServiceImpl.class);
        docAt.setMelserviceso(melservice);
        
        docAt.setFichierjointserviceso(fichierjointserviceso);
        docAt.setTransactionTemplate(new TransactionTemplate(new AbstractPlatformTransactionManager()
        {

            /**
             * serialVersionUID - long,
             */
            private static final long serialVersionUID = 1L;

            @Override
            protected Object doGetTransaction() throws TransactionException
            {
                return new Object();
            }

            @Override
            protected void doCommit(DefaultTransactionStatus status) throws TransactionException
            {
                // Raccord de méthode auto-généré
            }

            @Override
            protected void doRollback(DefaultTransactionStatus status) throws TransactionException
            {
                // Raccord de méthode auto-généré
            }

            @Override
            protected void doBegin(Object transaction, TransactionDefinition definition) throws TransactionException
            {
                // Raccord de méthode auto-généré
            }
        }));

    }

    /**
     * Test method for
     * {@link fr.gouv.finances.lombok.atlas.service.impl.DocumentAtlasServiceImpl#DocumentAtlasServiceImpl()}.
     *
     * @throws Exception the exception
     */
    @Test
    public final void testDocumentAtlasServiceImpl() throws Exception
    {
        assertNotNull(docAt);

    }

    /**
     * Test method for
     * {@link fr.gouv.finances.lombok.atlas.service.impl.DocumentAtlasServiceImpl#accuserReceptionEnvoiDocumentAtlas(java.lang.Integer, java.sql.Timestamp, java.sql.Timestamp)}
     * .
     *
     * @throws Exception the exception
     */
    @Test
    public final void testAccuserReceptionEnvoiDocumentAtlas() throws Exception
    {
        ScrollIterator stubScroll = mock(ScrollIterator.class);
        when(stubScroll.hasNext()).thenReturn(true).thenReturn(false);
        when(stubScroll.nextObjetMetier()).thenReturn(mock(DocumentAtlas.class));

        when(docAtDao.findDocumentsEnvoyesAAtlasParIterateur(anyInt(), any(Timestamp.class)))
            .thenReturn(stubScroll);
        docAt.accuserReceptionEnvoiDocumentAtlas(100, mock(Timestamp.class), mock(Timestamp.class));

    }

    /**
     * Test method for
     * {@link fr.gouv.finances.lombok.atlas.service.impl.DocumentAtlasServiceImpl#annulerEnvoiDocumentsAAtlas(java.lang.Integer, java.sql.Timestamp)}
     * .
     *
     * @throws Exception the exception
     */
    @Test
    public final void testAnnulerEnvoiDocumentsAAtlas() throws Exception
    {
        ScrollIterator stubScroll = mock(ScrollIterator.class);
        when(stubScroll.hasNext()).thenReturn(true).thenReturn(false);
        when(stubScroll.nextObjetMetier()).thenReturn(mock(DocumentAtlas.class));

        when(docAtDao.findDocumentsEnvoyesAAtlasParIterateur(anyInt(), any(Timestamp.class)))
            .thenReturn(stubScroll);
        docAt.annulerEnvoiDocumentsAAtlas(100, mock(Timestamp.class));

    }

    /** thrown. */
    @Rule
    public ExpectedException thrown = ExpectedException.none();

    /**
     * methode Test creer document et contenu document atlas avec exception : DOCUMENTEZ_MOI.
     *
     * @throws Exception the exception
     */
    @Test
    public final void testCreerDocumentEtContenuDocumentAtlasAvecException() throws Exception
    {
        thrown.expect(RegleGestionException.class);
        thrown.expectMessage("Le nom du fichier transféré doit impérativement");

        DocumentAtlas documentCree = new DocumentAtlas();
        documentCree.setIdentifiantAtlas("idtest");
        documentCree.setNom("test.txt");
        documentCree.setFichier(mock(FichierJoint.class));
        docAt.creerDocumentEtContenu(documentCree);

    }

    /**
     * methode Test creer document et contenu document atlas sans exception : DOCUMENTEZ_MOI.
     *
     * @throws Exception the exception
     */
    @Test
    public final void testCreerDocumentEtContenuDocumentAtlasSansException() throws Exception
    {

        DocumentAtlas documentCree = new DocumentAtlas();
        documentCree.setIdentifiantAtlas("idtest");
        documentCree.setNom("test.txt");
        FichierJoint ficmock = mock(FichierJoint.class);
        documentCree.setFichier(ficmock);
        when(documentCree.getFichier().getNomFichierOriginal()).thenReturn("test.txt");

        docAt.creerDocumentEtContenu(documentCree);

    }

    /**
     * Test method for
     * {@link fr.gouv.finances.lombok.atlas.service.impl.DocumentAtlasServiceImpl#creerDocumentEtContenu(fr.gouv.finances.lombok.atlas.bean.DocumentAtlas, boolean)}
     * .
     *
     * @throws Exception the exception
     */
    @Test
    public final void testCreerDocumentEtContenuDocumentAtlasBoolean() throws Exception
    {

        DocumentAtlas documentCree = new DocumentAtlas();
        documentCree.setIdentifiantAtlas("idtest");
        documentCree.setNom("test.txt");
        FichierJoint ficmock = mock(FichierJoint.class);
        documentCree.setFichier(ficmock);
        when(documentCree.getFichier().getNomFichierOriginal()).thenReturn("test.txt");
        docAt.creerDocumentEtContenu(documentCree, true);

    }

    /**
     * Test method for
     * {@link fr.gouv.finances.lombok.atlas.service.impl.DocumentAtlasServiceImpl#creerDocumentEtContenu(fr.gouv.finances.lombok.atlas.bean.DocumentAtlas, fr.gouv.finances.lombok.atlas.bean.TypeDocumentAtlas)}
     * .
     *
     * @throws Exception the exception
     */
    @Test
    public final void testCreerDocumentEtContenuDocumentAtlasTypeDocumentAtlas() throws Exception
    {

        TypeDocumentAtlas typeDoc = new TypeDocumentAtlas();
        typeDoc.setCode("abc");
        docAt.creerTypeDocument(typeDoc);

        typeDoc.setCode("a.Z.");
        thrown.expect(RegleGestionException.class);
        thrown.expectMessage("Le code du type de document");
        docAt.creerTypeDocument(typeDoc);

    }

    /**
     * Test method for {@link fr.gouv.finances.lombok.atlas.service.impl.DocumentAtlasServiceImpl#genererUnUuidDAtlas()}
     * .
     *
     * @throws Exception the exception
     */
    @Test
    public final void testGenererUnUuidDAtlas() throws Exception
    {
        assertTrue((docAt.genererUnUuidDAtlas().length() > 1));

    }

    /**
     * Test method for
     * {@link fr.gouv.finances.lombok.atlas.service.impl.DocumentAtlasServiceImpl#genererURLDUnDocument(fr.gouv.finances.lombok.atlas.bean.DocumentAtlas)}
     * .
     *
     * @throws Exception the exception
     */
    @Test
    public final void testGenererURLDUnDocumentDocumentAtlas() throws Exception
    {
        // TODO il faut "mocker" la méthode UtilisateurSecurityLombokContainer.getPersonneAnnuaire() avec powermockito

    }

    /**
     * Test method for
     * {@link fr.gouv.finances.lombok.atlas.service.impl.DocumentAtlasServiceImpl#genererURLDUnDocument(java.lang.String)}
     * .
     *
     * @throws Exception the exception
     */
    @Test
    public final void testGenererURLDUnDocumentString() throws Exception
    {
        // TODO il faut "mocker" la méthode UtilisateurSecurityLombokContainer.getPersonneAnnuaire() avec powermockito

    }

    /**
     * Test method for
     * {@link fr.gouv.finances.lombok.atlas.service.impl.DocumentAtlasServiceImpl#marquerDocumentEnvoyeAAtlas(fr.gouv.finances.lombok.atlas.bean.DocumentAtlas, java.sql.Timestamp)}
     * .
     *
     * @throws Exception the exception
     */
    @Test
    public final void testMarquerDocumentEnvoyeAAtlas() throws Exception
    {
        docAt.marquerDocumentEnvoyeAAtlas(mock(DocumentAtlas.class), mock(Timestamp.class));

    }

    /**
     * Test method for
     * {@link fr.gouv.finances.lombok.atlas.service.impl.DocumentAtlasServiceImpl#mettreAJourEtatDocument(fr.gouv.finances.lombok.atlas.bean.DocumentAtlas, fr.gouv.finances.lombok.atlas.bean.EtatDocumentAtlas)}
     * .
     *
     * @throws Exception the exception
     */
    @Test
    public final void testMettreAJourEtatDocument() throws Exception
    {

        docAt.mettreAJourEtatDocument(mock(DocumentAtlas.class), EtatDocumentAtlas.AARCHIVER);

    }

    /**
     * Test method for {@link fr.gouv.finances.lombok.atlas.service.impl.DocumentAtlasServiceImpl#nettoyerSession()}.
     *
     * @throws Exception the exception
     */
    @Test
    public final void testNettoyerSession() throws Exception
    {
        docAt.nettoyerSession();

    }

    /**
     * Test method for
     * {@link fr.gouv.finances.lombok.atlas.service.impl.DocumentAtlasServiceImpl#purgerContenuDocumentArchive(java.lang.Integer)}
     * .
     *
     * @throws Exception the exception
     */
    @Test
    public final void testPurgerContenuDocumentArchive() throws Exception
    {
        ScrollIterator stubScroll = mock(ScrollIterator.class);
        when(stubScroll.hasNext()).thenReturn(true).thenReturn(false);
        when(stubScroll.nextObjetMetier()).thenReturn(mock(DocumentAtlas.class));

        when(docAtDao.findDocumentsAtlasEtContenusAPurgerParIterateur(anyInt()))
            .thenReturn(stubScroll);
        docAt.purgerContenuDocumentArchive(100);

    }

    /**
     * Test method for
     * {@link fr.gouv.finances.lombok.atlas.service.impl.DocumentAtlasServiceImpl#purgerDocumentPerime(java.lang.Integer)}
     * .
     *
     * @throws Exception the exception
     */
    @Test
    public final void testPurgerDocumentPerime() throws Exception
    {
        ScrollIterator stubScroll = mock(ScrollIterator.class);
        when(stubScroll.hasNext()).thenReturn(true).thenReturn(false);
        when(stubScroll.nextObjetMetier()).thenReturn(mock(DocumentAtlas.class));

        when(docAtDao.findDocumentsAtlasPerimesParIterateur(anyInt()))
            .thenReturn(stubScroll);
        docAt.purgerDocumentPerime(100);

    }

    /**
     * Test method for
     * {@link fr.gouv.finances.lombok.atlas.service.impl.DocumentAtlasServiceImpl#rechercherDocumentAtlasParIdentifiantAtlas(java.lang.String)}
     * .
     *
     * @throws Exception the exception
     */
    @Test
    public final void testRechercherDocumentAtlasParIdentifiantAtlas() throws Exception
    {

        assertNull(docAt.rechercherDocumentAtlasParIdentifiantAtlas("toto"));
        when(docAtDao.findDocumentAtlasParIdentifiantAtlas("tata")).thenReturn(mock(DocumentAtlas.class));
        assertNotNull(docAt.rechercherDocumentAtlasParIdentifiantAtlas("tata"));
    }

    /**
     * Test method for
     * {@link fr.gouv.finances.lombok.atlas.service.impl.DocumentAtlasServiceImpl#rechercherDocumentsAtlasEtContenusAArchiverParIterateur(java.lang.Integer)}
     * .
     *
     * @throws Exception the exception
     */
    @Test
    public final void testRechercherDocumentsAtlasEtContenusAArchiverParIterateur() throws Exception
    {
        assertNull(docAt.rechercherDocumentsAtlasEtContenusAArchiverParIterateur(100));
        when(docAtDao.findDocumentsAtlasEtContenusAArchiverParIterateur(200)).thenReturn(mock(ScrollIterator.class));
        assertNotNull(docAt.rechercherDocumentsAtlasEtContenusAArchiverParIterateur(200));

    }

    /**
     * Test method for
     * {@link fr.gouv.finances.lombok.atlas.service.impl.DocumentAtlasServiceImpl#rechercherListeDocumentAtlasParIdentifiantAtlas(java.util.Collection)}
     * .
     *
     * @throws Exception the exception
     */
    @Test
    public final void testRechercherListeDocumentAtlasParIdentifiantAtlas() throws Exception
    {
        //new Collection<String>();
        List<DocumentAtlas> liste=docAt.rechercherListeDocumentAtlasParIdentifiantAtlas(Matchers.anyCollectionOf(String.class));
        assertNotNull(liste);
        assertTrue(liste.isEmpty());
        @SuppressWarnings("unchecked")
        List<DocumentAtlas> listemocked = mock(List.class);
        when(docAtDao.findListeDocumentAtlasParIdentifiantAtlas(Matchers.anyCollectionOf(String.class))).thenReturn(listemocked);
        liste=docAt.rechercherListeDocumentAtlasParIdentifiantAtlas(Matchers.anyCollectionOf(String.class));
        assertTrue(!liste.isEmpty());
    }

    /**
     * Test method for
     * {@link fr.gouv.finances.lombok.atlas.service.impl.DocumentAtlasServiceImpl#rechercherListeDocumentAtlasParType(fr.gouv.finances.lombok.atlas.bean.TypeDocumentAtlas)}
     * .
     *
     * @throws Exception the exception
     */
    @Test
    public final void testRechercherListeDocumentAtlasParType() throws Exception
    {
        TypeDocumentAtlas type = new TypeDocumentAtlas();
        docAt.rechercherListeDocumentAtlasParType(type);

    }

    /**
     * Test method for
     * {@link fr.gouv.finances.lombok.atlas.service.impl.DocumentAtlasServiceImpl#rechercherListeDocumentAtlasParTypeEtDates(fr.gouv.finances.lombok.atlas.bean.TypeDocumentAtlas, java.util.Date, java.util.Date)}
     * .
     *
     * @throws Exception the exception
     */
    @Test
    public final void testRechercherListeDocumentAtlasParTypeEtDates() throws Exception
    {
        docAt.rechercherListeDocumentAtlasParTypeEtDates(mock(TypeDocumentAtlas.class), mock(Date.class), mock(Date.class));
       
    }

    /**
     * Test method for
     * {@link fr.gouv.finances.lombok.atlas.service.impl.DocumentAtlasServiceImpl#rechercherTousLesTypesDocument()}.
     *
     * @throws Exception the exception
     */
    @Test
    public final void testRechercherTousLesTypesDocument() throws Exception
    {
        docAt.rechercherTousLesTypesDocument();

    }

    /**
     * Test method for
     * {@link fr.gouv.finances.lombok.atlas.service.impl.DocumentAtlasServiceImpl#rechercherTypeDocumentParCode(java.lang.String)}
     * .
     *
     * @throws Exception the exception
     */
    @Test
    public final void testRechercherTypeDocumentParCode() throws Exception
    {
        docAt.rechercherTypeDocumentParCode("df");

    }

    /**
     * Test method for
     * {@link fr.gouv.finances.lombok.atlas.service.impl.DocumentAtlasServiceImpl#supprimerDocumentEtContenu(fr.gouv.finances.lombok.atlas.bean.DocumentAtlas)}
     * .
     *
     * @throws Exception the exception
     */
    @Test
    public final void testSupprimerDocumentEtContenu() throws Exception
    {
        docAt.supprimerDocumentEtContenu(mock(DocumentAtlas.class));

    }

    /**
     * Test method for
     * {@link fr.gouv.finances.lombok.atlas.service.impl.DocumentAtlasServiceImpl#envoyerDocumentAtlasParMel(fr.gouv.finances.lombok.atlas.bean.DocumentAtlas, fr.gouv.finances.lombok.mel.techbean.Mel)}
     * .
     *
     * @throws Exception the exception
     */
    @Test
    public final void testEnvoyerDocumentAtlasParMel() throws Exception
    {
        DocumentAtlas falsedoc = new DocumentAtlas();
        falsedoc.setEtat(EtatDocumentAtlas.AARCHIVER);
        falsedoc.setFichier(mock(FichierJoint.class));
        FichierJoint ficj = new FichierJoint();
        ContenuFichier cont = new ContenuFichier();
        cont.setData(new byte[100]);
        ficj.setLeContenuDuFichier(cont);
        ficj.setId(3l);
        when(fichierjointserviceso.rechercherUnFichierJointEtSonContenuParId((Long)any())).thenReturn(ficj);
        docAt.envoyerDocumentAtlasParMel(falsedoc, mock(Mel.class));

    }

    /**
     * Test method for
     * {@link fr.gouv.finances.lombok.atlas.service.impl.DocumentAtlasServiceImpl#recupererDocumentImageSurAtlas(java.lang.String)}
     * .
     *
     * @throws Exception the exception
     */
    @Test
    public final void testRecupererDocumentImageSurAtlas() throws Exception
    {
        // faudra faire le test avec powermock pour mocker le code dans recupererDocumentImagesSurAtlas qui fait une httpurlconnection
        String url="http://toto";
        thrown.expect(ProgrammationException.class);
        thrown.expectMessage("Erreur d'entrée/sortie");
        docAt.recupererDocumentImageSurAtlas(url);
        

    }

}
