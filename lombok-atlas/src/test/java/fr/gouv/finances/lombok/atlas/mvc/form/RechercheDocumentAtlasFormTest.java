/*
 * Copyright (c) 2016 DGFiP - Tous droits réservés
 * 
 */
package fr.gouv.finances.lombok.atlas.mvc.form;

import fr.gouv.finances.lombok.test.pojo.AbstractCorePojoTest;

/**
 * Tests unitaires du POJO RechercheDocumentAtlasForm.
 *
 * @param  RechercheDocumentAtlasForm formulaire lié à la recherche de document Atlas
 *
 * @author Christophe Breheret-Girardin
 */
public class RechercheDocumentAtlasFormTest extends AbstractCorePojoTest< RechercheDocumentAtlasForm>
{   
    /**
     * Constructeur.
     */
    public RechercheDocumentAtlasFormTest()
    {
        super();
    }

}
