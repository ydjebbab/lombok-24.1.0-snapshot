/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : DocumentAtlasServiceTest.java
 *
 */
package fr.gouv.finances.lombok.test.atlas;

import java.io.File;

import org.junit.Assert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Transactional;

import fr.gouv.finances.lombok.atlas.bean.DocumentAtlas;
import fr.gouv.finances.lombok.atlas.service.DocumentAtlasService;
import fr.gouv.finances.lombok.test.FichierUtil;
import fr.gouv.finances.lombok.upload.bean.FichierJoint;
import fr.gouv.finances.lombok.util.base.LombokServiceTest;


/**
 * Class DocumentAtlasServiceTest --.
 * 
 * 
 * @version $Revision: 1.4 $ Date: 11 déc. 2009
 */
//@RunWith(SpringRunner.class)
@LombokServiceTest
@Rollback(true)
@Transactional
public class DocumentAtlasServiceTest
{

    /** documentatlasserviceso. */
    @Autowired
    protected DocumentAtlasService documentatlasserviceso;


    /**
     * methode Test rechercher document par identifiant atlas : --.
     */
  //  @Test
    public void testRechercherDocumentParIdentifiantAtlas()
    {
        DocumentAtlas documentCree = new DocumentAtlas();
        documentCree.setIdentifiantAtlas("idtest");
        documentCree.setNom("test.txt");
        FichierJoint fichierCree =
            new FichierJoint(new File(FichierUtil.recupererClassPath() + "/fr/gouv/finances/lombok/test/atlas/test.txt"));
        fichierCree.setTypeMimeFichier("text/plain");
        fichierCree.setNomFichierOriginal("test.txt");
        documentCree.setFichier(fichierCree);
        documentatlasserviceso.creerDocumentEtContenu(documentCree);
        DocumentAtlas documentCharge = documentatlasserviceso.rechercherDocumentAtlasParIdentifiantAtlas("idtest");
        Assert.assertEquals(documentCree, documentCharge);
        Assert.assertEquals(fichierCree, documentCharge.getFichier());
    }
}
