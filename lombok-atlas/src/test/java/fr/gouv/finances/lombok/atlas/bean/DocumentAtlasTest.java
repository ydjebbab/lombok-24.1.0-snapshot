package fr.gouv.finances.lombok.atlas.bean;

import java.sql.Timestamp;
import java.util.Calendar;

import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;

import org.junit.Test;

import fr.gouv.finances.lombok.atlas.dao.ProprieteDocumentAtlas;
import fr.gouv.finances.lombok.test.pojo.AbstractCorePojoTest;

/**
 * Tests unitaires de l'entité DocumentAtlas.
 *
 * @author Christophe Breheret-Girardin
 */
public class DocumentAtlasTest extends AbstractCorePojoTest<DocumentAtlas>
{

    /**
     * Constructeur.
     */
    public DocumentAtlasTest()
    {
        super();
    }

    /**
     * Méthode permettant de tester l'égalité du POJO.
     */
    @Test
    public final void testEqualsHashCode() throws Exception
    {
        EqualsVerifier.forClass(getParameterizedType())
            .suppress(Warning.NONFINAL_FIELDS, Warning.STRICT_INHERITANCE)
            .withPrefabValues(Timestamp.class
                , new Timestamp(Calendar.getInstance().getTimeInMillis()), new Timestamp(2017))
            .withOnlyTheseFields(ProprieteDocumentAtlas.IDENTIFIANT.getNom())
            .usingGetClass()
            .verify();
    }
}
