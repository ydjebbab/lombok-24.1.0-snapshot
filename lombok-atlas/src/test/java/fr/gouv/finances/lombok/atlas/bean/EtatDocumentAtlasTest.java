package fr.gouv.finances.lombok.atlas.bean;

import fr.gouv.finances.lombok.test.pojo.AbstractCorePojoTest;

/**
 * Tests unitaires de l'entité EtatDocumentAtlas.
 *
 * @author Christophe Breheret-Girardin
 */
public class EtatDocumentAtlasTest extends AbstractCorePojoTest<EtatDocumentAtlas>
{

    /**
     * Constructeur.
     */
    public EtatDocumentAtlasTest()
    {
        super();
    }

}
