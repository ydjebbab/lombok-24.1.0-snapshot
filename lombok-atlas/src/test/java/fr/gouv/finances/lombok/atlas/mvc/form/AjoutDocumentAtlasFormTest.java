/*
 * Copyright (c) 2016 DGFiP - Tous droits réservés
 * 
 */
package fr.gouv.finances.lombok.atlas.mvc.form;

import fr.gouv.finances.lombok.test.pojo.AbstractCorePojoTest;

/**
 * Tests unitaires du POJO AjoutDocumentAtlasForm.
 *
 * @param  AjoutDocumentAtlasForm formulaire lié à l'ajout d'un document Atlas
 *
 * @author Christophe Breheret-Girardin
 */
public class AjoutDocumentAtlasFormTest extends AbstractCorePojoTest< AjoutDocumentAtlasForm>
{   
    /**
     * Constructeur.
     */
    public AjoutDocumentAtlasFormTest()
    {
        super();
    }

}
