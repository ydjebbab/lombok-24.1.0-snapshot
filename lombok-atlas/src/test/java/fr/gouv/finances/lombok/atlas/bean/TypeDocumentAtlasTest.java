package fr.gouv.finances.lombok.atlas.bean;

import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;

import org.junit.Test;

import fr.gouv.finances.lombok.test.pojo.AbstractCorePojoTest;

/**
 * Tests unitaires de l'entité TypeDocumentAtlas.
 *
 * @author Christophe Breheret-Girardin
 */
public class TypeDocumentAtlasTest extends AbstractCorePojoTest<TypeDocumentAtlas>
{

    /**
     * Constructeur.
     */
    public TypeDocumentAtlasTest()
    {
        super();
    }

    /**
     * Méthode permettant de tester l'égalité du POJO.
     */
    @Test
    public final void testEqualsHashCode() throws Exception
    {
        EqualsVerifier.forClass(getParameterizedType())
            .suppress(Warning.NONFINAL_FIELDS, Warning.STRICT_INHERITANCE)
            .withOnlyTheseFields("code")
            .usingGetClass()
            .verify();
    }
}
