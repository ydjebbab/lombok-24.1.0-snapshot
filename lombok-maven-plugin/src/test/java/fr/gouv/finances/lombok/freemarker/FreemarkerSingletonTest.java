/*
 * Copyright (c) 2016 DGFiP - Tous droits réservés
 * 
 */
package fr.gouv.finances.lombok.freemarker;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * FreemarkerSingletonTest.java DOCUMENTEZ_MOI
 * 
 * @author chouard Date: 17 oct. 2016
 */
public class FreemarkerSingletonTest
{

    /**
     * Test method for {@link fr.gouv.finances.lombok.freemarker.FreemarkerSingleton#getInstance()}.
     */
    @Test
    public final void testGetInstance() throws Exception
    {
        assertNotNull(FreemarkerSingleton.getInstance());

    }

}
