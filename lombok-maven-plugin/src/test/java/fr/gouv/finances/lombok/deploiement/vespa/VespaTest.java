/*
 * Copyright (c) 2016 DGFiP - Tous droits réservés
 * 
 */
package fr.gouv.finances.lombok.deploiement.vespa;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

/**
 * VespaTest.java DOCUMENTEZ_MOI
 * 
 * @author chouard Date: 17 oct. 2016
 */
public class VespaTest
{

    /**
     * Test method for {@link fr.gouv.finances.lombok.deploiement.vespa.Vespa#Vespa()}.
     */
    @Test
    public final void testVespa() throws Exception
    {
        assertNotNull(new Vespa());

    }

    /**
     * Test method for {@link fr.gouv.finances.lombok.deploiement.vespa.Vespa#Vespa(java.lang.String)}.
     */
    @Test
    public final void testVespaString() throws Exception
    {
        assertNotNull(new Vespa("toto,tata,titi"));

    }

    /**
     * Test method for {@link fr.gouv.finances.lombok.deploiement.vespa.Vespa#getUddi()}.
     */
    @Test
    public final void testGetUddi() throws Exception
    {
        Uddi uddi = new Vespa().getUddi();
        assertNotNull(uddi);
        assertNotSame(uddi.getHote(), "");
        assertNotSame(uddi.getPort(), "");
    }

    /**
     * Test method for
     * {@link fr.gouv.finances.lombok.deploiement.vespa.Vespa#setUddi(fr.gouv.finances.lombok.deploiement.vespa.Uddi)}.
     */
    @Test
    public final void testSetUddi() throws Exception
    {
        Vespa aVesp = new Vespa();
        Uddi aUddi = new Uddi();
        aVesp.setUddi(aUddi);
        assertEquals(aUddi, aVesp.getUddi());

    }

    /**
     * Test method for {@link fr.gouv.finances.lombok.deploiement.vespa.Vespa#getContrats()}.
     */
    @Test
    public final void testGetContrats() throws Exception
    {
        List<Contrat> liste = new Vespa("toto|totoserv|totobil|totocal,tata,titi").getContrats();
        assertNotNull(liste);
        assertEquals("toto", liste.iterator().next().getName());
    }

    /**
     * Test method for {@link fr.gouv.finances.lombok.deploiement.vespa.Vespa#initVespaListContrat(java.lang.String)}.
     */
    @Test
    public final void testInitVespaListContrat() throws Exception
    {
        List<Contrat> liste = new Vespa("toto|totoserv|totobil|totocal,tata,titi").getContrats();
        assertNotNull(liste);

    }

}
