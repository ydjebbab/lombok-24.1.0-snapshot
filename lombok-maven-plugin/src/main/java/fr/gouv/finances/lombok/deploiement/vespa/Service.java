/*
 * Copyright (c) 2015 DGFiP - Tous droits réservés
 * 
 */
package fr.gouv.finances.lombok.deploiement.vespa;

/**
 * Classe représentant un service dans le fichier de cache du SLPA.
 */
public class Service
{
    /** name. */
    private String name;

    /** wsdl. */
    private String wsdl;

    /** accesspoint. */
    private String accesspoint;

    /**
     * Instanciation de {@link Service} avec un nom, une URL wsdl et une URL accesspoint.
     *
     * @param name le nom du service
     * @param wsdl l'URL du wsdl
     * @param accesspoint l'URL de l'accesspoint
     */
    public Service(String name, String wsdl, String accesspoint)
    {
        this.name = name;
        this.wsdl = wsdl;
        this.accesspoint = accesspoint;
    }

    /**
     * Instanciation de {@link Service} sans paramètres.
     */
    public Service()
    {

    }

    /**
     * Accesseur de name
     *
     * @return name
     */
    public String getName()
    {
        return name;
    }

    /**
     * Mutateur de name
     *
     * @param name name
     */
    public void setName(String name)
    {
        this.name = name;
    }

    /**
     * Accesseur de wsdl
     *
     * @return wsdl
     */
    public String getWsdl()
    {
        return wsdl;
    }

    /**
     * Mutateur de wsdl
     *
     * @param wsdl wsdl
     */
    public void setWsdl(String wsdl)
    {
        this.wsdl = wsdl;
    }

    /**
     * Accesseur de accesspoint
     *
     * @return accesspoint
     */
    public String getAccesspoint()
    {
        return accesspoint;
    }

    /**
     * Mutateur de accesspoint
     *
     * @param accesspoint accesspoint
     */
    public void setAccesspoint(String accesspoint)
    {
        this.accesspoint = accesspoint;
    }
}
