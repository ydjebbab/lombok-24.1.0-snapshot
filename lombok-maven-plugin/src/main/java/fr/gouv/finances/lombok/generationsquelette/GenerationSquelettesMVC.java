/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.generationsquelette;

/*
 * Copyright 2001-2005 The Apache Software Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Locale;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;

/**
 * Generation de squelettes de classes MVC.
 * 
 * @goal generationsquelettesmvc
 */
public class GenerationSquelettesMVC extends GenerationAbstractMojo

{

    /**
     * nom du premier ecran du cas d'utilisation.
     * 
     * @parameter expression="${neMaj}" @required
     */
    private String neMaj;

    /** zfne - String,. */
    private String zfne = "/";

    /** zfneBarre - String,. */
    private String zfneBarre = "/";

    /**
     * useCaseNameMvc. nom du cas d'utilisation
     * 
     * @parameter expression="${useCaseNameMvc}"
     * @required
     */
    private String useCaseNameMvc;

    /**
     * classNameMvc. nom de la classe concept du service principal associé au form action
     * 
     * @parameter expression="${classNameMvc}"
     * @required
     */
    private String classNameMvc;

    /**
     * numZoneFonctionnelleMvc. num du service principal associé au form action
     * 
     * @parameter expression="${numZoneFonctionnelleMvc}" default-value=""
     */
    private String numZoneFonctionnelleMvc;

    /**
     * NUMZFCCA.
     * 
     * @parameter expression="${numzfcca}" default-value=""
     */
    private String numzfcca;

    /** nomClassHgr - String,. */
    private String nomClassHgr;

    /** useCaseNameHgr - String,. */
    private String useCaseNameHgr;

    /** nomClassForGetSet - String, Pour generation getters et setters. */
    private String nomClassForGetSet;

    /**
     * Constructeur de la classe GenerationSquelettesMVC.java
     */
    public GenerationSquelettesMVC()
    {
        super();

    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see org.apache.maven.plugin.AbstractMojo#execute()
     */
    public void execute() throws MojoExecutionException, MojoFailureException
    {
        prefixepackage = prefixepackagefinances + "." + appli;
        prefixepackagecca = prefixepackage;
        racinejavasourceDir = project.getBuild().getSourceDirectory() + "/" + prefixepackagefinancesrep + "/" + appli;
        // nomClassHgr = classNameMvc.toLowerCase(Locale.FRANCE);
        nomClassForGetSet = classNameMvc;
        nomClassForGetSet = Character.toUpperCase(nomClassForGetSet.charAt(0)) + nomClassForGetSet.substring(1).toLowerCase(Locale.FRANCE);
        nomClassHgr = classNameMvc;
        nomClassHgr = Character.toUpperCase(nomClassHgr.charAt(0)) + nomClassHgr.substring(1);
        useCaseNameHgr = useCaseNameMvc;
        useCaseNameHgr = Character.toUpperCase(useCaseNameHgr.charAt(0)) + useCaseNameHgr.substring(1);
        // Character.toUpperCase(className.charAt(0))+

        if (isNomDeLaClasseOk(nomClassHgr))
        {
            if ((!"".equals(numZoneFonctionnelleMvc)) && (numZoneFonctionnelleMvc != null))
            {
                prefixepackage = prefixepackage + ".zf" + numZoneFonctionnelleMvc;

                racinejavasourceDir += "/zf" + numZoneFonctionnelleMvc;
                zfne = "zf" + numZoneFonctionnelleMvc + "/";
                zfneBarre += "zf" + numZoneFonctionnelleMvc;

            }

            if ((numzfcca != null) && (!"".equals(numzfcca)))
            {
                prefixepackagecca += ".zf" + numzfcca;
            }

            try
            {
                if (!new File(racinejavasourceDir + "/mvc/form/").exists())
                {
                    new File(racinejavasourceDir + "/mvc/form/").mkdirs();
                }
                filterFile("/templates/fragment/CUtemplateform.java.txt", racinejavasourceDir + "/mvc/form/"
                    + useCaseNameHgr + "Form.java");
                if (!new File(racinejavasourceDir + "/mvc/validator/").exists())
                {
                    new File(racinejavasourceDir + "/mvc/validator/").mkdirs();
                }
                filterFile("/templates/fragment/CUtemplatevalidator.java.txt", racinejavasourceDir
                    + "/mvc/validator/" + useCaseNameHgr + "Validator.java");
                if (!new File(racinejavasourceDir + "/mvc/webflow/action/").exists())
                {
                    new File(racinejavasourceDir + "/mvc/webflow/action/").mkdirs();
                }
                filterFile("/templates/fragment/CUtemplateformaction.java.txt", racinejavasourceDir
                    + "/mvc/webflow/action/" + useCaseNameHgr + "FormAction.java");

                if (!new File(webinfDir + "/flows/").exists())
                {
                    new File(webinfDir + "/flows/").mkdirs();

                }
                if (!new File(webinfDir + "/flows/" + zfneBarre.toLowerCase(Locale.FRANCE) + "/").exists())
                {
                    new File(webinfDir + "/flows/" + zfneBarre.toLowerCase(Locale.FRANCE)).mkdirs();
                }
                filterFile("/templates/fragment/CUtemplate-flow.xml.txt", webinfDir + "/flows/"
                    + zfne.toLowerCase(Locale.FRANCE) + useCaseNameMvc.toLowerCase(Locale.FRANCE) + "-flow.xml");

                filterFile("/templates/fragment/CUtemplate-flow-beans.xml.txt", webinfDir + "/flows/"
                    + zfne.toLowerCase(Locale.FRANCE) + useCaseNameMvc.toLowerCase(Locale.FRANCE) + "-flow-beans.xml");

                if (!new File(webinfDir + "/pages/").exists())
                {
                    new File(webinfDir + "/pages/").mkdirs();

                }
                if (!new File(webinfDir + "/pages/" + zfneBarre.toLowerCase(Locale.FRANCE) + "/").exists())
                {
                    new File(webinfDir + "/pages/" + zfneBarre.toLowerCase(Locale.FRANCE)).mkdirs();
                }
                filterFile("/templates/fragment/NEtemplatevue.jsp.txt", webinfDir + "/pages/"
                    + zfne.toLowerCase(Locale.FRANCE) + neMaj.toLowerCase(Locale.FRANCE) + ".jsp");

            }
            catch (FileNotFoundException exc)
            {
                throw new MojoExecutionException(exc.getMessage(), exc);
            }
            catch (IOException exc)
            {
                throw new MojoExecutionException(exc.getMessage(), exc);
            }

        }
        else
        {
            throw new MojoFailureException(
                "Le nom d'une classe doit comporter au moins 2 caracteres et doit commencer par une majuscule");
        }

    }

    /**
     * Méthode de parsing des template et génération des squelettes des dao, service et fichiers de configs spring.
     * 
     * @param fileNameSource Nom du template
     * @param fileNameTarget chemin relatif + Nom du squelette généré
     * @throws FileNotFoundException the file not found exception
     * @throws IOException Signal qu'une execption de type I/O s'est produite.
     */

    public void filterFile(String fileNameSource, String fileNameTarget) throws FileNotFoundException, IOException
    {
        filterFile(fileNameSource, fileNameTarget, "UTF-8");
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param fileNameSource
     * @param fileNameTarget
     * @param encoding
     * @throws FileNotFoundException the file not found exception
     * @throws IOException Signal qu'une execption de type I/O s'est produite.
     * @see fr.gouv.finances.lombok.generationsquelette.GenerationAbstractMojo#filterFile(java.lang.String,
     *      java.lang.String)
     */
    public void filterFile(String fileNameSource, String fileNameTarget, String encoding) throws FileNotFoundException, IOException
    {
        super.filterFile(fileNameSource, fileNameTarget, encoding);
        String currentLine;
        while ((currentLine = bReader.readLine()) != null)
        {

            currentLine = currentLine.replaceAll("@PREFIXEPACKAGE@", prefixepackage);
            currentLine = currentLine.replaceAll("@CU@", useCaseNameHgr);
            currentLine = currentLine.replaceAll("@CUMINUS@", useCaseNameMvc.toLowerCase(Locale.FRANCE));
            currentLine = currentLine.replaceAll("@CCAMINUS@", classNameMvc.toLowerCase(Locale.FRANCE));
            currentLine = currentLine.replaceAll("@CCA@", nomClassHgr);
            currentLine = currentLine.replaceAll("@CCAMINUSFIN@", classNameMvc.toLowerCase(Locale.FRANCE));
            currentLine = currentLine.replaceAll("@NE@", neMaj.toLowerCase(Locale.FRANCE));
            currentLine = currentLine.replaceAll("@ZFNE@", zfne.toLowerCase(Locale.FRANCE));
            currentLine = currentLine.replaceAll("@PREFIXEPACKAGECCA@", prefixepackagecca);
            currentLine = currentLine.replaceAll("@CCAGETSET@", nomClassForGetSet);
            bWriter.append(currentLine);
            bWriter.newLine();
        }
        bWriter.close();
        bReader.close();

    }

}
