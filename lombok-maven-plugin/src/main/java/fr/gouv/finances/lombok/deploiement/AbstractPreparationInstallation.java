/*
 * Copyright (c) 2016 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.deploiement;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.Inet4Address;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.io.output.FileWriterWithEncoding;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;

import fr.gouv.finances.lombok.deploiement.vespa.Vespa;
import fr.gouv.finances.lombok.freemarker.FreemarkerSingleton;
import fr.gouv.finances.lombok.maven.utils.LinkedProperties;
import fr.gouv.finances.lombok.maven.utils.Utils;
import freemarker.template.Configuration;
import freemarker.template.TemplateException;

/**
 * Class AbstractPreparationInstallation
 */
public abstract class AbstractPreparationInstallation extends GenerationConfiguration
{

    /** Constant : DOSSIER_PROPERTIES. */
    static protected final String DOSSIER_PROPERTIES = "/conf";

    /** Constant : DOSSIER_TOMCAT. */
    static protected final String DOSSIER_TOMCAT = "/tomcat7";

    /** Constant : CLE_DATASOURCE_DEFAUT. */
    static protected final String CLE_DATASOURCE_DEFAUT = "db";

    /**
     * Dossier de destination.
     *
     * @parameter expression = "${project.build.directory}/rpm-manuels"
     */
    protected String destination;

    /**
     * Dossier contenant les modeles.
     * 
     * @parameter expression = "@basedir@/integration/template"
     */
    protected String modeles;

    /**
     * Dossier contenant les fichiers de configuration.
     * 
     * @parameter expression = "@basedir@/integration/cibles-nopuppet"
     */
    protected String dossiercibles;

    /**
     * Constructeur de la classe AbstractPreparationInstallation.java
     */
    public AbstractPreparationInstallation()
    {
        super();

    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.deploiement.GenerationConfiguration#getCibles()
     */
    protected String getCibles()
    {
        return dossiercibles;
    }

    // parametres:
    // noms des configurations a preparer
    // dossier de destination de base

    /**
     * Ecrire proprietes.
     *
     * @param nomFichier the nom fichier
     * @param modificateurs
     * @param dossierCible
     * @throws IOException the IO exception
     */
    protected void ecrireProprietes(String nomFichier, Map<String, Properties> modificateurs, File dossierCible) throws IOException
    {
        File fichier = new File(nomFichier);
        String nomFichierCourt = fichier.getName();
        Properties modificateursFichier = modificateurs.get(nomFichierCourt);

        if (fichier.exists())
        {
            LinkedProperties proprietes = null;
            if ("localisations.properties".equals(nomFichierCourt))
            {
                proprietes = creerLocalisations();
            }
            else
            {
                proprietes = lireFichierSource(nomFichier);
            }
            if (modificateursFichier != null)
            {
                proprietes.putAll(modificateursFichier);
            }
            Writer writer =
                new BufferedWriter(new OutputStreamWriter(new FileOutputStream(new File(dossierCible, nomFichierCourt)), "UTF-8"));
            proprietes.store(writer, null);
            writer.close();
        }
    }

    /**
     * Met en place les elements a installer pour Tomcat.
     *
     * @param dossierTomcat dossier des destination des fichiers
     * @param modificateurs contenu du fichier de surcharge
     * @throws IOException en cas d'erreur de lecture/ecriture
     * @throws MojoFailureException en cas d'erreur de parametrage
     */
    protected void creerArborescenceTomcat(String dossierTomcat, Map<String, Properties> modificateurs) throws IOException,
        MojoFailureException
    {
        String source = modeles + File.separator + "tomcat" + File.separator;
        String dossierInstance = dossierTomcat + File.separator + instanceweb + File.separator;
        String dossierConf = dossierInstance + "conf" + File.separator;
        String dossierInit = dossierInstance + "init" + File.separator;
        String destname = null;

        // Copies directes

        Utils.cp(source + "catalina.policy", dossierConf + "catalina.policy");
        Utils.cp(source + "catalina.properties", dossierConf + "catalina.properties");
        Utils.cp(source + "tomcat-users.xml", dossierConf + "tomcat-users.xml");

        // Choix selon le mode de tomcat

        Object tempObject;
        String tomcatmode = MODE_TOMCAT_DEFAUT;
        Properties genprops = modificateurs.get(CLE_PARAMETRES_GENERAUX);
        if (genprops == null)
            throw new MojoFailureException("Fichier de surcharge incomplet (paramétrage Tomcat)");

        tempObject = genprops.get("tomcatmode");
        if (tempObject != null)
            tomcatmode = tempObject.toString();

        String tomcatHost = Inet4Address.getLocalHost().getHostAddress();
        tempObject = genprops.get("tomcathost");
        if (tempObject != null)
            tomcatHost = tempObject.toString();

        int tomcatPort = 8080;
        tempObject = genprops.get("tomcatport");
        if (tempObject != null)
            tomcatPort = Integer.parseInt(tempObject.toString());

        String tomcatMemory = MEMORY_TOMCAT_DEFAUT;
        tempObject = genprops.get("tomcatmemory");
        if (tempObject != null)
            tomcatMemory = tempObject.toString();

        String tomcatPermgen = PERMGEN_TOMCAT_DEFAUT;
        tempObject = genprops.get("tomcatpermgen");
        if (tempObject != null)
            tomcatPermgen = tempObject.toString();

        String tomcatOpts = OPTS_TOMCAT_DEFAUT;
        tempObject = genprops.get("tomcatopts");
        if (tempObject != null)
            tomcatOpts = tempObject.toString();

        genprops = modificateurs.get(CLE_PARAMETRES_SNMP);
        if (genprops == null)
            throw new MojoFailureException("Fichier de surcharge incomplet: configuration SNMP manquante");

        String snmpCommunities = genprops.get("communities").toString();
        String snmpManagers = genprops.get("managers").toString();
        String snmpIp = genprops.get("ip").toString();

        Utils.cp(source + "web.xml." + tomcatmode, dossierConf + "web.xml");

        destname = dossierConf + "context.xml";
        Utils.cp(source + "context.xml", destname);
        Utils.replaceInFile(destname, "#reloadContext#", Boolean.toString("developpement".equals(tomcatmode)));

        destname = dossierConf + "logging.properties";
        Utils.cp(source + "logging.properties", destname);
        Utils.replaceInFile(destname, "#base#", base);
        Utils.replaceInFile(destname, "#instance#", instanceweb);

        destname = dossierConf + "server.xml";
        Utils.cp(source + "server.xml", destname);
        Utils.replaceInFile(destname, "#shutdownPort#", Integer.toString(tomcatPort - 75));
        Utils.replaceInFile(destname, "#tomcatHost#", tomcatHost);
        Utils.replaceInFile(destname, "#tomcatPort#", Integer.toString(tomcatPort));
        Utils.replaceInFile(destname, "#reloadContext#", Boolean.toString("developpement".equals(tomcatmode)));

        destname = dossierConf + "snmp.acl";
        Utils.cp(source + "snmp.acl", destname);
        Utils.replaceInFile(destname, "#snmpCommunities#", snmpCommunities);
        Utils.replaceInFile(destname, "#snmpManagers#", snmpManagers);

        destname = dossierConf + "tomcat7.conf";
        Utils.cp(source + "tomcat7.conf", destname);
        Utils.replaceInFile(destname, "#base#", base);
        Utils.replaceInFile(destname, "#instance#", instanceweb);
        Utils.replaceInFile(destname, "#tomcatmemory#", tomcatMemory);
        Utils.replaceInFile(destname, "#tomcatpermgen#", tomcatPermgen);
        Utils.replaceInFile(destname, "#tomcatopts#", tomcatOpts);
        Utils.replaceInFile(destname, "#snmpIp#", snmpIp);
        Utils.replaceInFile(destname, "#snmpPort#", Integer.toString(tomcatPort - 19));
        Utils.replaceInFile(destname, "#user#", user);
        Utils.replaceInFile(destname, "#tomcatPort#", Integer.toString(tomcatPort));

        destname = dossierInit + instanceweb;
        Utils.cp(source + "init", destname);
        Utils.replaceInFile(destname, "#base#", base);
        Utils.replaceInFile(destname, "#instance#", instanceweb);
    }

    /**
     * Cree un context.xml pour une webapp specifique
     *
     * @param destination nom du fichier de destination
     * @param datasources parametres des datasources
     * @throws IOException en cas d'erreur de lecture/ecriture
     * @throws MojoFailureException en cas d'erreur de parametrage
     */
    protected void creerContexteApplication(String destination, Map<String, Properties> datasources) throws IOException,
        MojoFailureException
    {
        File destFile = new File(destination);
        destFile.getParentFile().mkdirs();
        try (Writer out = new BufferedWriter(new OutputStreamWriter(
            new FileOutputStream(destFile), Charset.forName("UTF-8"))))
        {
            out.append("<?xml version='1.0' encoding='utf-8'?>\n");
            out.append("<Context>\n");
    
            for (Map.Entry<String, Properties> datasource : datasources.entrySet())
            {
                out.append("    <Resource\n");
                out.append("        name=\"jdbc/" + datasource.getKey() + "\"\n");
                out.append("        auth=\"Container\"\n");
                out.append("        type=\"javax.sql.DataSource\"\n");
                out.append("        driverClassName=\"" + datasource.getValue().getProperty("driver") + "\"\n");
                out.append("        username=\"" + datasource.getValue().getProperty("user") + "\"\n");
                out.append("        password=\"" + datasource.getValue().getProperty("password") + "\"\n");
                out.append("        url=\"" + datasource.getValue().getProperty("url") + "\"\n");
                out.append("    />\n");
            }

            out.append("</Context>\n");
            out.flush();
        }
    }

    /**
     * Met en place les elements a installer pour Vespa.
     *
     * @param dossierTomcat dossier des destination des fichiers
     * @param modificateurs contenu du fichier de surcharge
     * @throws IOException en cas d'erreur de lecture/ecriture
     * @throws MojoExecutionException the mojo execution exception
     */
    protected void creerArborescenceVespa(String dossierTomcat, Map<String, Properties> modificateurs) throws IOException,
        MojoExecutionException
    {
        String dossierVespa = dossierTomcat + File.separator + instanceweb + File.separator + "vespa" + File.separator;
        Vespa vespa = new Vespa();

        Properties vespaprops = modificateurs.get(CLE_PARAMETRES_VESPA);
        if (vespaprops != null)
        {
            vespa.initVespaListContrat(vespaprops.getProperty(CLE_SERVICES_VESPA));
            vespa.getUddi().setActiver(Boolean.parseBoolean(vespaprops.getProperty(CLE_UDDI_ACTIVER_VESPA)));
            vespa.getUddi().setHote(vespaprops.getProperty(CLE_UDDI_HOTE_VESPA));
            vespa.getUddi().setPort(vespaprops.getProperty(CLE_UDDI_PORT_VESPA));
        }

        Map<String, Object> data = new HashMap<String, Object>();
        data.put("vespa", vespa);

        Configuration freemarkerConfiguration = FreemarkerSingleton.getInstance();
        try
        {
            File dossierDestination = new File(dossierVespa);
            dossierDestination.mkdirs();

            try (Writer writerConfig = new BufferedWriter(new FileWriterWithEncoding(
                new File(dossierVespa, "slpa-config.xml"), StandardCharsets.UTF_8)))
            {
                freemarkerConfiguration.getTemplate("vespa7/slpa-config.xml.ftl")
                    .process(data, writerConfig);
            }
            try (Writer writerCache = new BufferedWriter(new FileWriterWithEncoding(
                new File(dossierVespa, "slpa-cache.data"), StandardCharsets.UTF_8)))
            {
                freemarkerConfiguration.getTemplate("vespa7/slpa-cache.data.ftl")
                    .process(data, writerCache);
            }
        }
        catch (TemplateException templateException)
        {
            throw new MojoExecutionException(
                "Erreur lors de la création des fichiers de configuration Vespa.", templateException);
        }
    }
}
