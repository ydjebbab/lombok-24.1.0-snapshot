/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.majapartirdupb;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.project.MavenProject;

/**
 * Class MajAbstractMojo .
 */
@Deprecated
public abstract class MajAbstractMojo extends AbstractMojo
{

    /**
     * javasource dir.
     * 
     * @parameter default-value= "appliweb/JavaSource"
     */
    protected String javasourceDir;

    /**
     * Nom du project eclipse du projet blanc.
     * 
     * @parameter default-value= "lombok.projetblanc"
     */
    protected String pbappli;

    /**
     * chemin du project eclipse du projet blanc.
     * 
     * @parameter default-value= "C:/AtelierSODA/WORKSPACE/lombok.projetblanc" expression="${lombok.maj.pbbasedir}"
     */
    protected String pbbasedir;

    /**
     * Projet à mettre à jour.
     * 
     * @parameter default-value="${project}
     */
    protected MavenProject project;

    /** pathdmo. */
    protected String pathdmo;

    /**
     * Constructeur de la classe MajAbstractMojo.java
     */
    public MajAbstractMojo()
    {
        super();

    }

    /**
     * Méthode permettant d'intialiser le BufferedReader de la source et le BufferedWriter de la cible.
     * 
     * @param fileNameSource
     * @param fileNameTarget
     * @throws FileNotFoundException the file not found exception
     * @throws IOException Signal qu'une execption de type I/O s'est produite.
     */
    public void copyFile(String fileNameSource, String fileNameTarget) throws FileNotFoundException, IOException
    {

        if (!new File(fileNameTarget.substring(0, fileNameTarget.lastIndexOf(File.separator))).exists())
        {
            new File(fileNameTarget.substring(0, fileNameTarget.lastIndexOf(File.separator))).mkdirs();
        }

        try (FileInputStream fis = new FileInputStream(fileNameSource)
            ; FileOutputStream fos = new FileOutputStream(fileNameTarget))
        {
            FileChannel channelIn = fis.getChannel();
            FileChannel channelOut = fos.getChannel();
            channelIn.transferTo(0, channelIn.size(), channelOut);
        }
        catch (FileNotFoundException fnfe)
        {
            getLog().error("Erreur : fichier inexistant", fnfe);
        }
        catch (IOException ioe)
        {
            getLog().error("Erreur lors de la fermeture du flux d'E/S", ioe);
        }
    }

}
