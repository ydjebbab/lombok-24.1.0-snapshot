/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.majapartirdupb;

import java.io.File;
import java.io.IOException;
import java.util.Locale;

import org.apache.maven.plugin.MojoExecutionException;
import org.codehaus.plexus.components.interactivity.InputHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.gouv.finances.lombok.diffpb.CopyDMOProjetBlancWEBMojo;

/**
 * Dépréciée par E. Demorsy, remplacée par {@link #LombokMajMojo}
 */
@Deprecated
public class MajAPartirPBMojo extends MajAbstractMojo
{
    /** Initialisation de la journalisation. */
    protected static final Logger LOGGER = LoggerFactory.getLogger(MajAPartirPBMojo.class);

    /** Input handler, requis pour le monde interactif du plugin. @component */
    private InputHandler inputHandler;

    /**
     * Réponse oui/non à la question
     * "Avez vous bien récupéré sur cvs la version du projet blanc a partir de laquelle vous voulez faire la mise à jour par une commande replace with/another branch or version sur le projet lombok.projetblanc (oui)"
     * 
     * @parameter expression="${expression}"
     */
    private String reponse;

    /**
     * Constructeur de la classe MajAPartirPBMojo.java
     */
    public MajAPartirPBMojo()
    {
        super();
    }

    /**
     * methode Copy directory withtout cvs : .
     * 
     * @param elementPb
     * @param exclusion
     * @throws IOException Signal qu'une execption de type I/O s'est produite.
     */
    public void copyDirectoryWithtoutCvs(File elementPb, String exclusion) throws IOException
    {
        if (elementPb.isDirectory())
        {
            File[] files = elementPb.listFiles();
            for (int i = 0; i < files.length; i++)
            {

                String pathInAppli = files[i].getPath().replace(pbappli, project.getBasedir().getName());
                // pathInAppli = pathInAppli.replace(webmodulePb, webmodule);
                if (files[i].isDirectory())
                {
                    if (!"CVS".equalsIgnoreCase(files[i].getName()) && !".svn".equalsIgnoreCase(files[i].getName()))
                    {
                        File fileToCreateInDemo = new File(pathInAppli);
                        if (!fileToCreateInDemo.exists())
                        {
                            fileToCreateInDemo.mkdir();
                        }

                        copyDirectoryWithtoutCvs(files[i], exclusion);

                    }
                }
                else

                {
                    getLog().info("Element dans appli mis à jour : " + pathInAppli);
                    if ("".equals(exclusion) || !pathInAppli.contains(exclusion))
                    {
                        super.copyFile(files[i].getPath(), pathInAppli);
                    }
                }
            }
        }
        else
        {
            getLog().debug("Name base dir " + project.getBasedir().getName());
            String pathInAppli = elementPb.getPath();
            getLog().debug("pathInAppli " + pathInAppli);
            pathInAppli = elementPb.getAbsolutePath().replace(pbappli, project.getBasedir().getName());
            getLog().debug("pathInAppli " + pathInAppli);
            // pathInAppli = pathInAppli.replace(webmodulePb, webmodule);
            getLog().debug("pathInAppli " + pathInAppli);

            if ("".equals(exclusion) || !elementPb.getPath().contains(exclusion))
            {
                super.copyFile(elementPb.getPath(), pathInAppli);

            }
        }
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see org.apache.maven.plugin.AbstractMojo#execute()
     */
    public void execute() throws MojoExecutionException
    {

        if (reponse == null)
        {

            getLog()
                .info(
                    "Avez vous bien récupéré sur cvs la version du projet blanc a partir de laquelle vous voulez faire la mise à jour par une commande replace with/another branch or version sur le projet lombok.projetblanc (oui)");

            try
            {
                String userExpression = inputHandler.readLine();
                if (userExpression != null && ("oui").equals(userExpression.toLowerCase(Locale.FRENCH)))
                {
                    maj();
                }

            }
            catch (IOException e)
            {
                throw new MojoExecutionException("Unable to read from standard input.", e);
            }
        }
    }

    /**
     * methode Maj : .
     * 
     * @throws IOException Signal qu'une execption de type I/O s'est produite.
     */
    public void maj() throws IOException
    {
        String[] directoriesInwebModule = new String[] {
                "/resources/conf/commun|applicationContext-commun-edition.xml",
                "/webapp/WEB-INF/web.xml",
                "/webapp/composants/dhtmlpdfpopup",
                "/webapp/composants/dhtmlpopup",
                "/webapp/composants/extremecomponents",
                "/webapp/composants/jscalendar",
                "/webapp/composants/jscookmenu",
                "/webapp/composants/tagsapp",
                "/webapp/composants/tree",
                "/webapp/WEB-INF/pages/errors",
                "/webapp/WEB-INF/tags/app",
                "/webapp/WEB-INF/tags/html",
                "/webapp/WEB-INF/pages/identification.jsp",
                "/webapp/WEB-INF/pages/sessionexpiree.jsp",
                "/webapp/WEB-INF/pages/messagebloquantdetecteavecportail.jsp"};
        String[] directoriesInBaseDir = new String[] {"/utilitairesdev/template", "/composantsoptionnels"};
        String[] directoriesInJavaSource = new String[] {"/jasperreports.properties", "/commons-logging.properties",};

        String[] elementsToDeleteInSourceDir = new String[] {"/extremetableResourceBundle_fr_FR.properties",
                "/extremetableResourceBundle.properties",
                "/extremetableResourceBundle_fr.properties"};
        for (int i = 0; i < elementsToDeleteInSourceDir.length; i++)
        {
            // Suppression des fichiers qui ne sont plus copiés depuis le projet blanc
            File fichier = new File(project.getBuild().getSourceDirectory() + elementsToDeleteInSourceDir[i]);
            boolean suppressionOk = fichier.delete();
            if (!suppressionOk)
            {
                LOGGER.info("Le fichier '{}' n'a pas été supprimé", fichier.getAbsolutePath());
            }
        }

        for (int i = 0; i < directoriesInwebModule.length; i++)
        {
            String exclusion = "";
            getLog().debug("directoriesInwebModule[i].split(| )[0] " + directoriesInwebModule[i].split("[|]")[0]);
            if (directoriesInwebModule[i].split("[|]").length == 2)
            {
                exclusion = directoriesInwebModule[i].split("[|]")[1];
                copyDirectoryWithtoutCvs(new File(pbbasedir + "/src/main/"
                    + directoriesInwebModule[i].split("[|]")[0]), exclusion);
            }
            else
            {

                copyDirectoryWithtoutCvs(new File(pbbasedir + "/src/main" + directoriesInwebModule[i]), exclusion);
            }

        }

        for (int i = 0; i < directoriesInBaseDir.length; i++)
        {
            String exclusion = "";
            if (directoriesInBaseDir[i].split("[|]").length == 2)
            {
                exclusion = directoriesInBaseDir[i].split("|")[1];
                copyDirectoryWithtoutCvs(new File(pbbasedir + "/" + directoriesInBaseDir[i].split("[|]")[0]), exclusion);
            }
            else
            {
                copyDirectoryWithtoutCvs(new File(pbbasedir + "/" + directoriesInBaseDir[i]), exclusion);
            }

        }

        for (int i = 0; i < directoriesInJavaSource.length; i++)
        {
            String exclusion = "";
            if (directoriesInJavaSource[i].split("[|]").length == 2)
            {
                exclusion = directoriesInJavaSource[i].split("[|]")[1];
                copyDirectoryWithtoutCvs(new File(pbbasedir + "/src/main/java"
                    + directoriesInJavaSource[i].split("[|]")[0]), exclusion);
            }
            else
            {
                copyDirectoryWithtoutCvs(new File(pbbasedir + "/src/main/java"
                    + directoriesInJavaSource[i]), exclusion);
            }

        }

    }

}
