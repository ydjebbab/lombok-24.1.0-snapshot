/*
 * Copyright (c) 2016 DGFiP - Tous droits réservés
 * 
 */
package fr.gouv.finances.lombok.freemarker;

import java.nio.charset.StandardCharsets;

import freemarker.template.Configuration;

/**
 * Classe contenant l'instance de la configuration du template engine Freemarker
 */
public final class FreemarkerSingleton
{
    /** L'instance de configuration de Freemarker */
    private static Configuration configuration;

    /**
     * Constructeur de la classe FreemarkerSingleton.java Ajouté suite à un cas majeur sur SonarQube Utility classes,
     * which are a collection of static members, are not meant to be instantiated. Even abstract utility classes, which
     * can be extended, should not have public constructors. Java adds an implicit public constructor to every class
     * which does not define at least one explicitly. Hence, at least one non-public constructor should be defined.
     */
    private FreemarkerSingleton()
    {
    }

    /**
     * Retourne l'instance de configuration de Freemarker.
     *
     * @return l'instance de configuration de Freemarker
     */
    public static Configuration getInstance()
    {
        if (configuration == null)
        {
            configuration = new Configuration(Configuration.VERSION_2_3_23);
            configuration.setClassForTemplateLoading(FreemarkerSingleton.class, "/templates/");
            configuration.setDefaultEncoding(StandardCharsets.UTF_8.name());
        }
        return configuration;
    }

}
