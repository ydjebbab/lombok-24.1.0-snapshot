/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.maven.sql;

/**
 * Class Batch .
 */
public class Batch
{

    /** nom. */
    private String nom;

    /** version. */
    private String version;

    /**
     * Constructeur de la classe Batch.java
     */
    public Batch()
    {
        super();

    }

    /**
     * Accesseur de l attribut nom.
     * 
     * @return nom
     */
    public String getNom()
    {
        return nom;
    }

    /**
     * Accesseur de l attribut version.
     * 
     * @return version
     */
    public String getVersion()
    {
        return version;
    }

    /**
     * Modificateur de l attribut nom.
     * 
     * @param nom le nouveau nom
     */
    public void setNom(String nom)
    {
        this.nom = nom;
    }

    /**
     * Modificateur de l attribut version.
     * 
     * @param version le nouveau version
     */
    public void setVersion(String version)
    {
        this.version = version;
    }

}
