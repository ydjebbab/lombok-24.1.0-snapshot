/*
 * Copyright (c) 2016 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.deploiement;

/**
 * Class Livrable
 */
public class Livrable
{

    /** Constant : VERSION_COURTE_TRUE. */
    public static final String VERSION_COURTE_TRUE = "true";

    /** Constant : VERSION_COURTE_FALSE. */
    public static final String VERSION_COURTE_FALSE = "false";

    /** Constant : VERSION_COURTE_IFNEEDED. */
    public static final String VERSION_COURTE_IFNEEDED = "ifneeded";

    /** group id. */
    private String groupId;

    /** artifact id. */
    private String artifactId;

    /** version. */
    private String version;

    /** classifier. */
    private String classifier;

    /** format. */
    private String format;

    /** nomfinal. */
    private String nomfinal;

    /** obligatoire. */
    private boolean obligatoire;

    /** versioncourte. */
    private String versioncourte;

    /**
     * Constructeur de la classe Livrable.java
     */
    public Livrable()
    {
        super();

    }

    /**
     * Accesseur de l attribut group id.
     *
     * @return group id
     */
    public String getGroupId()
    {
        return groupId;
    }

    /**
     * Modificateur de l attribut group id.
     *
     * @param groupId le nouveau group id
     */
    public void setGroupId(String groupId)
    {
        this.groupId = groupId;
    }

    /**
     * Accesseur de l attribut artifact id.
     *
     * @return artifact id
     */
    public String getArtifactId()
    {
        return artifactId;
    }

    /**
     * Modificateur de l attribut artifact id.
     *
     * @param artifactId le nouveau artifact id
     */
    public void setArtifactId(String artifactId)
    {
        this.artifactId = artifactId;
    }

    /**
     * Accesseur de l attribut version.
     *
     * @return version
     */
    public String getVersion()
    {
        return version;
    }

    /**
     * Modificateur de l attribut version.
     *
     * @param version le nouveau version
     */
    public void setVersion(String version)
    {
        this.version = version;
    }

    /**
     * Accesseur de l attribut classifier.
     *
     * @return classifier
     */
    public String getClassifier()
    {
        return classifier;
    }

    /**
     * Accesseur de l attribut classifier affichage.
     *
     * @return classifier affichage
     */
    public String getClassifierAffichage()
    {
        if (classifier == null)
            return "appli";
        return classifier;
    }

    /**
     * Modificateur de l attribut classifier.
     *
     * @param classifier le nouveau classifier
     */
    public void setClassifier(String classifier)
    {
        this.classifier = classifier;
    }

    /**
     * Accesseur de l attribut format.
     *
     * @return format
     */
    public String getFormat()
    {
        return format;
    }

    /**
     * Modificateur de l attribut format.
     *
     * @param format le nouveau format
     */
    public void setFormat(String format)
    {
        this.format = format;
    }

    /**
     * Accesseur de l attribut nomfinal.
     *
     * @return nomfinal
     */
    public String getNomfinal()
    {
        return nomfinal;
    }

    /**
     * Modificateur de l attribut nomfinal.
     *
     * @param nomFinal le nouveau nomfinal
     */
    public void setNomfinal(String nomFinal)
    {
        this.nomfinal = nomFinal;
    }

    /**
     * Verifie si obligatoire.
     *
     * @return true, si c'est obligatoire
     */
    public boolean isObligatoire()
    {
        return obligatoire;
    }

    /**
     * Modificateur de l attribut obligatoire.
     *
     * @param obligatoire le nouveau obligatoire
     */
    public void setObligatoire(boolean obligatoire)
    {
        this.obligatoire = obligatoire;
    }

    /**
     * methode Checks if is versioncourte : DOCUMENTEZ_MOI.
     *
     * @return string
     */
    public String isVersioncourte()
    {
        return versioncourte;
    }

    /**
     * Modificateur de l attribut versioncourte.
     *
     * @param versioncourte le nouveau versioncourte
     */
    public void setVersioncourte(String versioncourte)
    {
        this.versioncourte = versioncourte;
    }

    /**
     * methode Permet version longue : DOCUMENTEZ_MOI.
     *
     * @return true, si c'est vrai
     */
    public boolean permetVersionLongue()
    {
        return (VERSION_COURTE_FALSE.equals(versioncourte) || VERSION_COURTE_IFNEEDED.equals(versioncourte));
    }

    /**
     * methode Permet version courte : DOCUMENTEZ_MOI.
     *
     * @return true, si c'est vrai
     */
    public boolean permetVersionCourte()
    {
        return (VERSION_COURTE_TRUE.equals(versioncourte) || VERSION_COURTE_IFNEEDED.equals(versioncourte));
    }
}
