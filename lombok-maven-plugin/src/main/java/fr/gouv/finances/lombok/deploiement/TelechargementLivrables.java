/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.deploiement;

/*
 * Copyright 2001-2005 The Apache Software Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Arrays;
import java.util.List;

import org.apache.maven.artifact.Artifact;
import org.apache.maven.artifact.factory.ArtifactFactory;
import org.apache.maven.artifact.repository.ArtifactRepository;
import org.apache.maven.artifact.resolver.ArtifactNotFoundException;
import org.apache.maven.artifact.resolver.ArtifactResolutionException;
import org.apache.maven.artifact.resolver.ArtifactResolver;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.project.MavenProject;

/**
 * The Class GenerationExempleLdif.
 * 
 * @goal telechargementlivrables
 */
public class TelechargementLivrables extends AbstractMojo

{

    /**
     * The Maven Project Object
     * 
     * @parameter expression="${project}"
     * @readonly
     */
    protected MavenProject project;

    /**
     * Nom du depot distant a utiliser
     * 
     * @parameter default-value="nexus"
     */
    // private String repoName;

    /**
     * Base de l'URL de telechargement
     * 
     * @parameter expression = "${project.distributionManagement.repository.url}"
     */
    // private String baseURL;

    /**
     * groupId
     * 
     * @parameter expression = "${project.groupId}"
     */
    private String groupId;

    /**
     * artifactId
     * 
     * @parameter expression = "${project.artifactId}"
     */
    private String artifactId;

    /**
     * Numero de version des fichiers a telecharger
     * 
     * @parameter expression = "${project.version}"
     */
    private String versionCible;

    /**
     * Nom de base des rpm a generer
     * 
     * @parameter expression = "${deploiement.module}"
     */
    private String module;

    /**
     * Repertoire cible des telechargements
     * 
     * @parameter expression = "${project.build.directory}/paquets"
     */
    private String destination;

    /**
     * Livrables a telecharger
     * 
     * @parameter
     */
    private List<Livrable> livrables;

    /**
     * List of Remote Repositories used by the resolver
     * 
     * @parameter expression="${project.remoteArtifactRepositories}"
     * @readonly
     * @required
     */
    protected List<?> remoteRepositories;

    /**
     * Dossier contenant les fichiers de surcharge nopuppet
     * 
     * @parameter expression = "@basedir@/cibles-nopuppet"
     */
    protected String dossiercibles;

    /**
     * Dossier contenant les fichiers de surcharge nopuppet ITM
     * 
     * @parameter expression = "@basedir@/cibles-itm"
     */
    protected String dossierciblesitm;

    /**
     * Location of the local repository.
     * 
     * @parameter expression="${localRepository}"
     * @readonly
     * @required
     */
    protected ArtifactRepository localRepository;

    /**
     * Used to look up Artifacts in the remote repository.
     * 
     * @parameter expression= "${component.org.apache.maven.artifact.factory.ArtifactFactory}"
     * @required
     * @readonly
     */
    protected ArtifactFactory factory;

    /**
     * Used to look up Artifacts in the remote repository.
     * 
     * @parameter expression= "${component.org.apache.maven.artifact.resolver.ArtifactResolver}"
     * @required
     * @readonly
     */
    protected ArtifactResolver artifactResolver;

    /*
     * private String buildVersionURL() { return baseURL + "/content/" + groupId.replace(".", "/") + "/" + artifactId +
     * "/" + versionCible + "/"; }
     */

    /**
     * Constructeur de la classe TelechargementLivrables.java
     */
    public TelechargementLivrables()
    {
        super();

    }

    private void telechargerLivrablesNopuppet() throws MalformedURLException, IOException, MojoExecutionException
    {
        File dossierCibles = new File(dossiercibles);
        if (!dossierCibles.exists())
        {
            getLog().warn("Impossible de localiser le dossier d'environnements cible:" + dossiercibles);
        }
        else
        {
            List<File> listeCibles = Arrays.asList(dossierCibles.listFiles(new FilenameFilter()
            {

                @Override
                public boolean accept(File dir, String name)
                {
                    return (name.endsWith(".properties") && (name.contains("intex") || (name.contains("production"))));
                }
            }));

            for (File cible : listeCibles)
            {
                String cibleCourte = cible.getName().substring(0, cible.getName().indexOf('.'));
                Livrable livrable = new Livrable();
                livrable.setFormat("rpm");
                livrable.setObligatoire(true);
                livrable.setVersioncourte(Livrable.VERSION_COURTE_IFNEEDED);
                livrable.setClassifier(cibleCourte);
                livrable.setNomfinal(module + "-" + cibleCourte + "-" + versionCible + ".rpm");
                this.telecharger(livrable);
            }
        }

    }

    private void telechargerLivrablesNopuppetItm() throws MalformedURLException, IOException, MojoExecutionException
    {
        File dossierCibles = new File(dossierciblesitm);
        if (!dossierCibles.exists())
        {
            getLog().warn("Impossible de localiser le dossier d'environnements cible:" + dossierciblesitm);
        }
        else
        {
            List<File> listeCibles = Arrays.asList(dossierCibles.listFiles(new FilenameFilter()
            {

                @Override
                public boolean accept(File dir, String name)
                {
                    return (name.endsWith(".properties") && (name.contains("intex") || (name.contains("production"))));
                }
            }));

            // S'il y a au moins 1 RPM d'itm no puppet alors on télécharge le RPM itmmas-canada
            if (!listeCibles.isEmpty())
            {
                Livrable livrable = new Livrable();
                livrable.setGroupId("fr.gouv.finances.canada");
                livrable.setArtifactId("itmmas");
                livrable.setVersion("2.00.02");
                livrable.setFormat("rpm");
                livrable.setObligatoire(true);
                livrable.setVersioncourte(Livrable.VERSION_COURTE_IFNEEDED);
                livrable.setNomfinal("itmmas-canada-2.00.02.noarch.rpm");
                this.telecharger(livrable);
            }

            for (File cible : listeCibles)
            {
                String cibleCourte = cible.getName().substring(0, cible.getName().indexOf('.'));
                Livrable livrable = new Livrable();
                livrable.setFormat("rpm");
                livrable.setObligatoire(true);
                livrable.setVersioncourte(Livrable.VERSION_COURTE_IFNEEDED);
                livrable.setClassifier("itm-" + cibleCourte);
                livrable.setNomfinal(module + "-itm-" + cibleCourte + "-" + versionCible + ".rpm");
                this.telecharger(livrable);
            }
        }

    }

    private void telechargerArtefact(Livrable livrable, boolean versionCourte)
        throws MalformedURLException, IOException, MojoExecutionException, ArtifactNotFoundException
    {
        BufferedInputStream input = null;
        File destinationFile;
        byte[] data = new byte[1024];

        String versionEffective = versionCible;
        String versionFinale = versionCible;
        if (versionCourte)
        {
            versionEffective = versionCible.replaceAll("-[0-9]*$", "");
            versionFinale = versionCible.replaceAll("-[0-9]*$", "-1");
        }
        if (livrable.getGroupId() == null)
        {
            livrable.setGroupId(groupId);
        }
        if (livrable.getArtifactId() == null)
        {
            livrable.setArtifactId(artifactId);
        }
        if (livrable.getVersion() == null)
        {
            livrable.setVersion(versionEffective);
        }
        Artifact artifact = factory.createArtifactWithClassifier(
            livrable.getGroupId(), livrable.getArtifactId(), livrable.getVersion(), livrable.getFormat(), livrable.getClassifier());
        try
        {
            artifactResolver.resolve(artifact, this.remoteRepositories,
                this.localRepository);
        }
        catch (ArtifactResolutionException e)
        {
            throw new MojoExecutionException("impossible de trouver le livrable " + livrable.getNomfinal(), e);
        }

        try
        {
            if (artifact.getDownloadUrl() != null)
            {
                URLConnection connection = new URL(artifact.getDownloadUrl()).openConnection();
                input = new BufferedInputStream(connection.getInputStream());
            }
            else if (artifact.getFile() != null)
            {
                try (FileInputStream fis = new FileInputStream(artifact.getFile()))
                {
                    input = new BufferedInputStream(fis);
                }
            }
            else
                throw new MojoExecutionException("impossible de trouver le livrable " + livrable.getNomfinal());
    
            destinationFile = new File(destination, livrable.getNomfinal().replace("VERSION", versionFinale));
            destinationFile.getParentFile().mkdirs();
    
            getLog().info("Téléchargement du livrable " + livrable.getClassifierAffichage());
    
            try (BufferedOutputStream output
                = new BufferedOutputStream(new FileOutputStream(destinationFile)))
            {
                int length = input.read(data, 0, 1024);
                while (length >= 0)
                {
                    output.write(data, 0, length);
                    length = input.read(data, 0, 1024);
                }
                output.flush();
            }
        }
        finally
        {
            if (input != null)
            {
                input.close();
            }
        }
    }

    private void telecharger(Livrable livrable) throws MalformedURLException, IOException, MojoExecutionException
    {
        ArtifactNotFoundException exception = null;
        if (livrable.permetVersionLongue())
        {
            try
            {
                telechargerArtefact(livrable, false);
                return;
            }
            catch (ArtifactNotFoundException e)
            {
                exception = e;
            }
        }
        if (livrable.permetVersionCourte())
        {
            try
            {
                telechargerArtefact(livrable, true);
                return;
            }
            catch (ArtifactNotFoundException e)
            {
                exception = e;
            }
        }
        if (livrable.isObligatoire())
        {
            throw new MojoExecutionException("impossible de trouver le livrable " + livrable.getNomfinal(), exception);
        }
        else
        {
            getLog().info("Livrable " + livrable.getClassifierAffichage() + " introuvable");
            return;
        }

    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @throws MojoExecutionException the mojo execution exception
     * @throws MojoFailureException the mojo failure exception
     * @see org.apache.maven.plugin.AbstractMojo#execute()
     */
    public void execute() throws MojoExecutionException, MojoFailureException
    {
        try
        { /*
           * ArtifactRepository repository = null;
           * @SuppressWarnings("rawtypes") List repositories = project.getRemoteArtifactRepositories(); for (Object
           * repoObject : repositories) { repository = (ArtifactRepository) repoObject; if
           * (repoName.equalsIgnoreCase(repository.getId())) break; } if ((repoName == null) ||
           * (!repoName.equalsIgnoreCase(repository.getId()))) { throw new
           * MojoExecutionException("Impossible d'identifier le dépôt " + repoName); }
           */
            for (Livrable livrable : livrables)
            {
                this.telecharger(livrable);
            }
            telechargerLivrablesNopuppet();
            telechargerLivrablesNopuppetItm();
        }
        catch (IOException ioe)
        {
            throw new MojoExecutionException(ioe.getMessage(), ioe);
        }
    }

}
