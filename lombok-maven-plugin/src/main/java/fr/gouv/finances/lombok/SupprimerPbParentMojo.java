/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok;

import java.util.List;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.apache.maven.model.Dependency;
import org.apache.maven.model.Exclusion;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.project.MavenProject;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Ajoute les dépendances de lombok-pb-parent dans le pom Supprime le parent lombok-pb-parent Déprécié par E. Demorsy
 * (code non finalisé)
 * 
 * @goal supprimerPbParent
 */
@Deprecated
public class SupprimerPbParentMojo extends AbstractLombokMojo
{

    /** Constant : PROJECT_PARENT_XPATH. */
    protected static final String PROJECT_PARENT_XPATH = "/project/parent";

    /** Constant : PROJECT_XPATH. */
    protected static final String PROJECT_XPATH = "/project";

    /** Constant : EXCLUSION. */
    protected static final String EXCLUSION = "exclusion";

    /** Constant : EXCLUSIONS. */
    protected static final String EXCLUSIONS = "exclusions";

    /** Constant : DEPENDENCY. */
    protected static final String DEPENDENCY = "dependency";

    /** Constant : EMPTY. */
    protected static final String EMPTY = "";

    /** Constant : SCOPE. */
    protected static final String SCOPE = "scope";

    /** Constant : CLASSIFIER. */
    protected static final String CLASSIFIER = "classifier";

    /** Constant : TYPE. */
    protected static final String TYPE = "type";

    /** Constant : VERSION. */
    protected static final String VERSION = "version";

    /** Constant : ARTIFACT_ID. */
    protected static final String ARTIFACT_ID = "artifactId";

    /** Constant : GROUP_ID. */
    protected static final String GROUP_ID = "groupId";

    /** Constant : DEFAULT_DEPENDENCY_TYPE. */
    protected static final String DEFAULT_DEPENDENCY_TYPE = "jar";

    /** Constant : DEFAULT_DEPENDENCY_SCOPE. */
    protected static final String DEFAULT_DEPENDENCY_SCOPE = "compile";

    /** Constant : XPATH. */
    protected static final XPath XPATH = XPathFactory.newInstance().newXPath();

    /**
     * Constructeur de la classe SupprimerPbParentMojo.java
     */
    public SupprimerPbParentMojo()
    {
        super();

    }

    /**
     * methode Adds the dependency element : .
     * 
     * @param groupId
     * @param artifactId
     * @param node
     */
    protected void addDependencyElement(String groupId, String artifactId, Node node)
    {

    }

    /**
     * methode Adds the simple child : .
     * 
     * @param key
     * @param value
     * @param node
     */
    protected void addSimpleChild(String key, String value, Node node)
    {
        Document doc = node.getOwnerDocument();
        Node child = doc.createElement(key);
        if (value != null)
        {
            child.appendChild(doc.createTextNode(value));
        }
        node.appendChild(child);
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.AbstractLombokMojo#processDocument(org.w3c.dom.Document)
     */
    @Override
    protected void processDocument(Document document)
        throws MojoExecutionException, MojoFailureException, XPathExpressionException
    {
        MavenProject parent = project.getParent();
        if (parent != null)
        {
            addPbParentDependencies(document);
            deletePbParent(document);
        }
    }

    /**
     * methode Adds the artifact id : .
     * 
     * @param artifactId
     * @param dependencyElement
     */
    private void addArtifactId(String artifactId, Node dependencyElement)
    {
        addSimpleChild(ARTIFACT_ID, artifactId, dependencyElement);
    }

    /**
     * methode Adds the classifier : .
     * 
     * @param classifier
     * @param dependencyElement
     */
    private void addClassifier(String classifier, Node dependencyElement)
    {
        if (classifier != null)
        {
            addSimpleChild(CLASSIFIER, classifier, dependencyElement);
        }
    }

    /**
     * methode Adds the dependency element : .
     * 
     * @param d
     * @param dependenciesElement
     */
    private void addDependencyElement(Dependency d, Node dependenciesElement)
    {
        Node dependencyElement = dependenciesElement.getOwnerDocument().createElement(DEPENDENCY);
        addGroupId(d.getGroupId(), dependencyElement);
        addArtifactId(d.getArtifactId(), dependencyElement);
        addVersion(d.getVersion(), dependencyElement);
        addType(d.getType(), dependencyElement);
        addClassifier(d.getClassifier(), dependencyElement);
        addScope(d.getScope(), dependencyElement);
        addExclusions(d.getExclusions(), dependencyElement);
        dependenciesElement.appendChild(dependencyElement);

    }

    /**
     * methode Adds the exclusions : .
     * 
     * @param exclusions
     * @param dependencyElement
     */
    private void addExclusions(List<Exclusion> exclusions, Node dependencyElement)
    {
        if (exclusions != null && !exclusions.isEmpty())
        {
            Document doc = dependencyElement.getOwnerDocument();
            Node exclusionsElement = doc.createElement(EXCLUSIONS);

            for (Exclusion exclusion : exclusions)
            {
                Node exclusionElement = doc.createElement(EXCLUSION);
                addSimpleChild(GROUP_ID, exclusion.getGroupId(), exclusionElement);
                addSimpleChild(ARTIFACT_ID, exclusion.getArtifactId(), exclusionElement);
                exclusionsElement.appendChild(exclusionElement);

            }
            dependencyElement.appendChild(exclusionsElement);
        }
    }

    /**
     * methode Adds the group id : .
     * 
     * @param groupId
     * @param dependencyElement
     */
    private void addGroupId(String groupId, Node dependencyElement)
    {
        addSimpleChild(GROUP_ID, groupId, dependencyElement);
    }

    /**
     * methode Adds the pb parent dependencies : .
     * 
     * @param document
     * @throws XPathExpressionException the x path expression exception
     */
    private void addPbParentDependencies(Document document) throws XPathExpressionException
    {
        getLog().info("Ajout des dépendances du parent ...");
        MavenProject parent = project.getParent();
        @SuppressWarnings("unchecked")
        List<Dependency> parentDependencies = parent.getDependencies();
        if (!parentDependencies.isEmpty())
        {
            updateDependenciesElement(parentDependencies, document);
            getLog().info("Dépendances du parent ajoutées.");
        }

    }

    /**
     * methode Adds the scope : .
     * 
     * @param scope
     * @param dependencyElement
     */
    private void addScope(String scope, Node dependencyElement)
    {
        if (scope != null && !DEFAULT_DEPENDENCY_SCOPE.equals(scope) && !EMPTY.equals(scope))
        {
            addSimpleChild(SCOPE, scope, dependencyElement);
        }
    }

    /**
     * methode Adds the type : .
     * 
     * @param type
     * @param dependencyElement
     */
    private void addType(String type, Node dependencyElement)
    {
        if (!DEFAULT_DEPENDENCY_TYPE.equals(type))
        {
            addSimpleChild(TYPE, type, dependencyElement);
        }
    }

    /**
     * methode Adds the version : .
     * 
     * @param version
     * @param dependencyElement
     */
    private void addVersion(String version, Node dependencyElement)
    {
        addSimpleChild(VERSION, version, dependencyElement);
    }

    /**
     * methode Contains dependency : .
     * 
     * @param dependency
     * @param projectDependencies
     * @return true, si c'est vrai
     */
    private boolean containsDependency(Dependency dependency, List<Dependency> projectDependencies)
    {
        boolean result = false;
        String dependencyGroupId = dependency.getGroupId();
        String dependencyArtifactId = dependency.getArtifactId();
        String dependencyType = dependency.getType();
        String dependencyClassifier = dependency.getClassifier() != null ? dependency.getClassifier() : EMPTY;
        for (Dependency d : projectDependencies)
        {
            String g = d.getGroupId();
            String a = d.getArtifactId();
            String t = d.getType();
            String c = d.getClassifier() != null ? d.getClassifier() : EMPTY;

            if (dependencyGroupId.equals(g) && dependencyArtifactId.equals(a) && dependencyType.equals(t) && dependencyClassifier.equals(c))
            {
                result = true;
                break;
            }
        }
        return result;
    }

    /**
     * methode Delete pb parent : .
     * 
     * @param document
     * @throws XPathExpressionException the x path expression exception
     */
    private void deletePbParent(Document document) throws XPathExpressionException
    {
        Node projectElement = (Node) XPATH.evaluate(PROJECT_XPATH, document, XPathConstants.NODE);
        Node parentElement = (Node) XPATH.evaluate(PROJECT_PARENT_XPATH, document, XPathConstants.NODE);
        projectElement.removeChild(parentElement);
    }

    /**
     * methode Merge dependency elements : .
     * 
     * @param projectDependenciesElement
     * @param projectDependencies
     * @param parentDependencies
     * @param pom
     */
    private void mergeDependencyElements(Node projectDependenciesElement, List<Dependency> projectDependencies,
        List<Dependency> parentDependencies, Document pom)
    {
        for (Dependency dependency : parentDependencies)
        {
            if (!containsDependency(dependency, projectDependencies))
            {
                addDependencyElement(dependency, projectDependenciesElement);
            }
        }
    }

    /**
     * methode Update dependencies element : .
     * 
     * @param parentDependencies
     * @param document
     * @throws XPathExpressionException the x path expression exception
     */
    private void updateDependenciesElement(List<Dependency> parentDependencies, Document document) throws XPathExpressionException
    {
        NodeList nodeList = (NodeList) XPATH.evaluate("/project/dependencies", document, XPathConstants.NODESET);
        if (nodeList.getLength() == 0)
        {
            Node project = document.getDocumentElement();
            Node dependenciesElement = document.createElement("dependencies");
            project.appendChild(dependenciesElement);
        }

        Node projectDependenciesElement =
            ((NodeList) XPATH.evaluate("/project/dependencies", document, XPathConstants.NODESET)).item(0);
        @SuppressWarnings("unchecked")
        List<Dependency> projectDependencies = project.getDependencies();
        projectDependencies.removeAll(parentDependencies);
        mergeDependencyElements(projectDependenciesElement, projectDependencies, parentDependencies, document);
    }
}
