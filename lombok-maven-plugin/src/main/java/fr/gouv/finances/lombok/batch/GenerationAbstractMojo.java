package fr.gouv.finances.lombok.batch;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.project.MavenProject;

public abstract class GenerationAbstractMojo extends AbstractMojo
{

    protected InputStreamReader fileReader;

    protected BufferedReader bReader;

    protected OutputStreamWriter fstream;

    protected BufferedWriter bWriter;
    
    /**
     * 
     * 
     * @parameter expression="${project}
     * 
     */
    protected MavenProject project;

    public boolean isNomDeLaClasseOk(String nomClasse)
    {

        int longueurchaine = nomClasse.length();
        char premiercar = nomClasse.charAt(0);
        if (longueurchaine < 2)
        {
            return false;
        }
        if (!Character.isUpperCase(premiercar))
        {
            return false;
        }

        return true;

    }

    public void filterFile(String fileNameSource, String fileNameTarget) throws FileNotFoundException, IOException
    {

        getLog().debug("Source" + fileNameSource);
        getLog().debug("Target" + fileNameTarget);
        //fileReader = new InputStreamReader(new FileInputStream(new File(fileNameSource)), "UTF-8");
        fileReader = new InputStreamReader(new FileInputStream(new File(fileNameSource)));
        bReader = new BufferedReader(fileReader);
        
        //fstream = new OutputStreamWriter(new FileOutputStream(new File(fileNameTarget)), "UTF-8");
        fstream = new OutputStreamWriter(new FileOutputStream(new File(fileNameTarget)));
        bWriter = new BufferedWriter(fstream);

    }

}
