/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.generationsquelette;

/*
 * Copyright 2001-2005 The Apache Software Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Locale;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;

/**
 * Class GenerationSquelettesBeans .
 * 
 * @goal generationsquelettesbeans
 */
public class GenerationSquelettesBeans extends GenerationAbstractMojo

{

    /**
     * numZoneFonctionnelleBeans - String, numéro de la zf.
     * 
     * @parameter expression="${numZoneFonctionnelleBeans}"
     */
    private String numZoneFonctionnelleBeans;

    /**
     * classNameBeans nom du bean métier.
     * 
     * @parameter expression="${classNameBeans}" @required
     */

    private String classNameBeans;

    /**
     * ca4car nom court de la classe d'analyse sur 4 caracteres en minuscules.
     * 
     * @parameter expression="${ca4car}"
     * @required
     */

    private String ca4car;

    /**
     * nom court de la classe d'analyse sur 4 caracteres en minuscules.
     * 
     * @parameter default-value="" expression="${cacheminhbm}"
     */

    private String cacheminhbm;

    /** nomClassHgr - String. */
    private String nomClassHgr;

    /**
     * Constructeur de la classe GenerationSquelettesBeans.java
     */
    public GenerationSquelettesBeans()
    {
        super();

    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see org.apache.maven.plugin.AbstractMojo#execute()
     */
    @Override
    public void execute() throws MojoExecutionException, MojoFailureException
    {
        prefixepackage = prefixepackagefinances + "." + appli;
        racinejavasourceDir = project.getBuild().getSourceDirectory() + "/" + prefixepackagefinancesrep + "/" + appli;
        // notation hongroise tout n'est pas en minuscule aptrés la premiere majuscule chaque mot en majuscule
        // nomClassHgr = classNameBeans.toLowerCase(Locale.FRANCE);
        nomClassHgr = classNameBeans;
        nomClassHgr = Character.toUpperCase(nomClassHgr.charAt(0)) + nomClassHgr.substring(1);
        cacheminhbm = prefixepackagefinancesrep + "/" + appli + "/bean";
        // Character.toUpperCase(className.charAt(0))+

        if (isNomDeLaClasseOk(classNameBeans))
        {
            if ((!"".equals(numZoneFonctionnelleBeans)) && (numZoneFonctionnelleBeans != null))
            {
                prefixepackage = prefixepackage + ".zf" + numZoneFonctionnelleBeans;

                racinejavasourceDir += "/zf" + numZoneFonctionnelleBeans;

                cacheminhbm = prefixepackagefinancesrep + "/" + appli + "/zf" + numZoneFonctionnelleBeans + "/bean";

            }

            try
            {

                if (!new File(racinejavasourceDir + "/bean/").exists())
                {
                    new File(racinejavasourceDir + "/bean/").mkdirs();
                }
                filterFile("/templates/fragment/CAtemplatebean.java.txt", racinejavasourceDir + "/bean/"
                    + nomClassHgr + ".java");
                filterFile("/templates/fragment/CAtemplatebean.hbm.xml.txt", racinejavasourceDir + "/bean/"
                    + classNameBeans + ".hbm.xml");
                if (!new File(webinfConf + "/conf/application/").exists())
                {
                    new File(webinfConf + "/conf/application/").mkdirs();
                }
                filterFile("/templates/fragment/CAFRAGMENTapplicationContext-dao.xml.txt", webinfConf
                    + "/conf/application/applicationContext-dao-fragmentacopier-" + classNameBeans.toLowerCase(Locale.FRANCE) + "-ca.xml");

            }
            catch (FileNotFoundException exc)
            {
                throw new MojoExecutionException(exc.getMessage(), exc);
            }
            catch (IOException exc)
            {
                throw new MojoExecutionException(exc.getMessage(), exc);
            }

        }
        else
        {
            throw new MojoFailureException(
                "Le nom d'une classe doit comporter au moins 2 caracteres et doit commencer par une majuscule");
        }

    }

    /**
     * methode Filter file : .
     * 
     * @param fileNameSource
     * @param fileNameTarget
     * @throws FileNotFoundException the file not found exception
     * @throws IOException Signal qu'une execption de type I/O s'est produite.
     */
    public void filterFile(String fileNameSource, String fileNameTarget) throws FileNotFoundException, IOException
    {
        filterFile(fileNameSource, fileNameTarget, "UTF-8");
    }

    /**
     * Méthode de parsing des template et génération des squelettes des dao, service et fichiers de configs spring.
     * 
     * @param fileNameSource Nom du template
     * @param fileNameTarget chemin relatif + Nom du squelette généré
     * @param encoding
     * @throws FileNotFoundException the file not found exception
     * @throws IOException Signal qu'une execption de type I/O s'est produite.
     */

    @Override
    public void filterFile(String fileNameSource, String fileNameTarget, String encoding) throws FileNotFoundException, IOException
    {
        super.filterFile(fileNameSource, fileNameTarget, encoding);
        String currentLine;
        while ((currentLine = bReader.readLine()) != null)
        {

            currentLine = currentLine.replaceAll("@PREFIXEPACKAGE@", prefixepackage);
            currentLine = currentLine.replaceAll("@CACHEMINHBM@", cacheminhbm);
            currentLine = currentLine.replaceAll("@CA@", classNameBeans);
            currentLine = currentLine.replaceAll("@CA4CAR@", ca4car.toLowerCase(Locale.FRANCE));
            currentLine = currentLine.replaceAll("@CAMAJUS@", classNameBeans.toUpperCase(Locale.FRANCE));
            bWriter.append(currentLine);
            bWriter.newLine();
        }
        bWriter.close();
        bReader.close();

    }

}
