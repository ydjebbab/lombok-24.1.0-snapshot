/*
 * Copyright (c) 2015 DGFiP - Tous droits réservés
 * 
 */
package fr.gouv.finances.lombok.deploiement.vespa;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Classe de paramétrage pour Vespa
 */
public class Vespa
{
    /** Définition des réglages de l'annuaire UDDI. */
    private Uddi uddi = new Uddi();

    /** Liste des contrats */
    private final List<Contrat> contrats = new ArrayList<Contrat>();

    /**
     * Instanciation de {@link Vespa} sans paramètres.
     */
    public Vespa()
    {
        // constructeur vide
    }

    /**
     * Constructeur de {@link Vespa} avec une liste de services sous forme d'une chaine dans le fichier de surcharge.<br>
     * Chaîne au format
     * "contrat|service|wsdl|accesspoint,contrat2|service2|wsdl2|accesspoint2,...,contratN|serviceN|wsdlN|accesspointN"
     *
     * @param serviceSurchargeString la chaine des services Vespa du fichier de surcharge
     */
    public Vespa(String serviceSurchargeString)
    {
        initVespaListContrat(serviceSurchargeString);
    }

    /**
     * Accesseur de uddi
     *
     * @return uddi
     */
    public Uddi getUddi()
    {
        return uddi;
    }

    /**
     * Mutateur de uddi
     *
     * @param uddi uddi
     */
    public void setUddi(Uddi uddi)
    {
        this.uddi = uddi;
    }

    /**
     * Accesseur de contrats
     *
     * @return contrats
     */
    public List<Contrat> getContrats()
    {
        return contrats;
    }

    /**
     * Initialise la liste des contrats de Vespa à partir d'une chaîne de surcharge.<br>
     * Chaîne au format
     * "contrat|service|wsdl|accesspoint,contrat2|service2|wsdl2|accesspoint2,...,contratN|serviceN|wsdlN|accesspointN"
     *
     * @param vespaServicesString la chaîne se trouvant dans le fichier de surcharge
     * @return la liste des contracts de Vespa
     */
    public final void initVespaListContrat(String vespaServicesString)
    {
        Map<String, Contrat> mapContrats = new HashMap<String, Contrat>();

        String[] services = vespaServicesString.split(",");
        for (String service : services)
        {
            String[] serviceElements = service.split("\\|");
            if (serviceElements.length != 4)
            {
                continue;
            }
            String nomContrat = serviceElements[0];
            String nomService = serviceElements[1];
            String wsdl = serviceElements[2];
            String accesspoint = serviceElements[3];

            Contrat contrat = mapContrats.get(nomContrat);
            if (contrat == null)
            {
                contrat = new Contrat(nomContrat);
                mapContrats.put(nomContrat, contrat);
            }
            contrat.getServices().add(new Service(nomService, wsdl, accesspoint));
        }

        contrats.clear();
        for (Map.Entry<String, Contrat> entry : mapContrats.entrySet())
        {
            contrats.add(entry.getValue());
        }
    }
}
