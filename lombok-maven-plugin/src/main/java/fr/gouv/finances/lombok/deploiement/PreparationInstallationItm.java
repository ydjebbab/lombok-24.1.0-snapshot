package fr.gouv.finances.lombok.deploiement;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;

/**
 * Class qui prepare les dossiers et fichiers necessaires a la constitution d'un RPM d'installation d'ITM
 * 
 * @goal preparationinstallationitm
 */
public class PreparationInstallationItm extends AbstractPreparationInstallation
{

    private static final String NOM_DATASOURCE = "itmmas";

    /**
     * Constructeur de la classe PreparationInstallationItm.java
     */
    public PreparationInstallationItm()
    {
        super();

    }

    /**
     * Methode d'execution du Mojo
     *
     * @throws MojoExecutionException
     * @throws MojoFailureException
     */
    @Override
    public void execute() throws MojoExecutionException, MojoFailureException
    {

        try
        {
            // Lecture des fichiers de surcharge

            Map<String, Map<String, Properties>> modificateursFichier = lireModificateursCibles();

            // On supprime arbitrairement le fichier "exemple"
            // qui n'est la que pour montrer un exemple de fichier de surcharge
            modificateursFichier.remove("exemple");

            Map<String, Properties> modificateursCible;
            Map<String, Properties> datasources = new HashMap<String, Properties>();

            // Pour chaque configuration a preparer...

            for (String cible : modificateursFichier.keySet())
            {

                // obtenir les elements de surcharge correspondants

                modificateursCible = modificateursFichier.get(cible);

                // on verifie qu'il ne manque rien:
                Properties props = modificateursCible.get(CLE_DATASOURCE_DEFAUT);
                if (props == null)
                {
                    throw new MojoFailureException("Il manque les paramètres de base de données dans le fichier de surcharge " + cible);
                }

                if (props.getProperty("user") == null)
                {
                    throw new MojoFailureException("L'élément db#user manque dans le fichier de surcharge " + cible);
                }
                if (props.getProperty("password") == null)
                {
                    throw new MojoFailureException("L'élément db#password manque dans le fichier de surcharge " + cible);
                }
                if (props.getProperty("url") == null)
                {
                    throw new MojoFailureException("L'élément db#url manque dans le fichier de surcharge " + cible);
                }
                if (props.getProperty("driver") == null)
                {
                    throw new MojoFailureException("L'élément db#driver manque dans le fichier de surcharge " + cible);
                }

                datasources.put(NOM_DATASOURCE, props);

                // ecrire le context.xml d'ITM

                creerContexteApplication(destination + "/" + "itm-" + cible + "/context.xml", datasources);

                // ecrire l'arborescence de tomcat

                creerArborescenceTomcat(destination + "/" + "itm-" + cible + DOSSIER_TOMCAT, modificateursCible);

            }

        }
        catch (IOException ioe)
        {
            throw new MojoExecutionException(ioe.getMessage(), ioe);
        }
    }

}
