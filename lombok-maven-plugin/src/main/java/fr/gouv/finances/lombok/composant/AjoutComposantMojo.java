/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.composant;

/*
 * Copyright 2001-2005 The Apache Software Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.io.File;
import java.io.IOException;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;

import fr.gouv.finances.lombok.maven.utils.Utils;

/**
 * Mojo d'ajout de composant optionnel.
 * 
 * @goal ajoutduncomposantoptionnel
 */
public class AjoutComposantMojo extends AbstractMojo
{
    /** Dossier des composants. */
    private static final String[] dossiersComposants = {"src", "integration", "data"};

    /**
     * @parameter expression="${nom.composant}"
     * @required
     */
    private String nomComposant;

    /**
     * @parameter expression="${package.appli}"
     * @required
     */
    private String appli = "";

    /**
     * @parameter expression="${nom.sgbd}"
     * @required
     */
    private String nomSgbd;

    /** path. */
    private String path;

    /**
     * Constructeur.
     */
    public AjoutComposantMojo()
    {
        super();
    }
    
    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see org.apache.maven.plugin.AbstractMojo#execute()
     */
    public void execute() throws MojoExecutionException, MojoFailureException
    {
        path = nomSgbd + "/composantsoptionnels/" + nomComposant;
        extraireJar();
        if (!new File("build/ajoutComposantMojo/" + path).isDirectory())
            throw new MojoFailureException("Composant inconnu:" + nomComposant);
        transformSqls();
        copyDirs();
      
    }

    /**
     * methode Copy dirs : .
     * 
     * @throws MojoExecutionException the mojo execution exception
     */
    private void copyDirs() throws MojoExecutionException
    {
        File fileDossier = null;
        String cheminDossier = null;
        for (String dossier : dossiersComposants)
        {
            cheminDossier = "build/ajoutComposantMojo/" + path + "/" + dossier;
            fileDossier = new File(cheminDossier);
            if (fileDossier.isDirectory())
            {
                cp(cheminDossier, dossier);
            }
        }
    }

    /**
     * methode Cp : .
     * 
     * @param src
     * @param dst
     * @throws MojoExecutionException the mojo execution exception
     */
    private void cp(String src, String dst) throws MojoExecutionException
    {
        try
        {
            Utils.cp(src, dst);
        }
        catch (IOException e)
        {
            throw new MojoExecutionException(e.getMessage(), e);
        }
    }

    /**
     * methode Extraire jar : .
     * 
     * @throws MojoExecutionException the mojo execution exception
     */
    private void extraireJar() throws MojoExecutionException
    {
        try
        {
            Utils.extractPathFromJar("build/ajoutComposantMojo/" + path);
        }
        catch (IOException e)
        {
            throw new MojoExecutionException(e.getMessage(), e);
        }
    }

    /**
     * methode Transform sqls : .
     * 
     * @throws MojoExecutionException the mojo execution exception
     */
    private void transformSqls() throws MojoExecutionException
    {
        try
        {
            Utils.replaceInDir("build/ajoutComposantMojo/" + path, "@appli@", appli);
        }
        catch (IOException e)
        {
            throw new MojoExecutionException(e.getMessage(), e);
        }
    }

}
