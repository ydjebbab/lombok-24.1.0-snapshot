/*
 * Copyright (c) 2017 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.maven.maj;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Result;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.configuration2.FileBasedConfiguration;
import org.apache.commons.configuration2.PropertiesConfiguration;
import org.apache.commons.configuration2.PropertiesConfigurationLayout;
import org.apache.commons.configuration2.builder.FileBasedConfigurationBuilder;
import org.apache.commons.configuration2.builder.fluent.Parameters;
import org.apache.commons.configuration2.ex.ConfigurationException;
import org.apache.commons.io.FileUtils;
import org.apache.maven.artifact.Artifact;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;
import org.w3c.dom.Comment;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * Class LombokMaj232en233.
 */
@Mojo(name = "maj232en233")
public class LombokMaj232en233 extends AbstractMojo
{

    /** version lombok cible. */
    @Parameter(property = "versionLombokCible", defaultValue = "23.3.5")
    private String versionLombokCible;

    /** utilise composant structure. */
    @Parameter(property = "utiliseComposantStructure", defaultValue = "false")
    private boolean utiliseComposantStructure;

    /** basedir. */
    @Parameter(defaultValue = "${project.basedir}", readonly = true)
    private File basedir;

    /** project. */
    @Parameter(defaultValue = "${project}", readonly = true)
    private MavenProject project;

    /**
     * Constructeur de la classe LombokMaj232en233.java
     */
    public LombokMaj232en233()
    {
        super(); // Raccord de constructeur auto-généré

    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see org.apache.maven.plugin.AbstractMojo#execute()
     */
    @Override
    public void execute() throws MojoExecutionException, MojoFailureException
    {

        getLog().info("Début procédure de mise à jour du projet suivant en Lombok " + versionLombokCible + ":");
        getLog().info("-> ArtifactId:" + project.getArtifactId());
        getLog().info("-> GroupId:" + project.getGroupId());
        getLog().info("-> Version:" + project.getVersion());

        verificationConditionsMiseAJour();

        getLog().info("Debut mise à jour projet vers la version " + versionLombokCible);

        ajoutCommentaireCommunProperties();

        configJasperClustering();

        String[] depToDelete = {"lombok-all", "jcl104-over-slf4j", "javax.servlet-api"};

        Set<PomDependance> dependenciesToAdd = createDependenciesToAdd();

        miseAJourPomProjet(depToDelete, dependenciesToAdd);

        miseAJourWebXmlProjet();

        suppressionRepertoires();

        getLog().info("* début suppression des fichiers qui sont désormais inclus dans le framework lombok");
        suppressionFichiers();
        getLog().info("* fin suppression des fichiers qui sont désormais inclus dans le framework lombok");

        remplacementFichier(new File("src/main/resources/conf/app-config.xml"));

        // modifs pour les batchs et le profile batchdao, le fichier
        // suivant a été mergé dans lombok-core applicationContext-commun-dao.xml
        suppressionFichier(new File("src/main/batchapp/applicationContext-commun-batchdao.xml"));
        
        // suppression du fichier commons-logging.properties
        suppressionFichier(new File("src/main/java/commons-logging.properties"));

        miseAJourBatchFactory();

        getLog().info("* début ajout de fichiers manquants");

        ajoutInputStream(getClass().getResourceAsStream("/maj233/data/sql/delete_schema_dmo_hsqldb.sql"), new
            File("data/sql/delete_schema_dmo_hsqldb.sql"));
        getLog().info("* fin ajout de fichiers manquants");

        getLog().info("Fin de la procédure de mise à jour du projet vers la version " + versionLombokCible);

    }

    /**
     * methode Config jasper clustering : .
     *
     * @throws MojoExecutionException the mojo execution exception
     */
    private void configJasperClustering() throws MojoExecutionException
    {
        getLog().info("* début paramétrage de jasper en mode clustering par défaut");
        Parameters params = new Parameters();
        FileBasedConfigurationBuilder<FileBasedConfiguration> builderProperties =
            new FileBasedConfigurationBuilder<FileBasedConfiguration>(PropertiesConfiguration.class).
                configure(params.properties().setFileName("src/main/webapp/WEB-INF/properties/composantjasper.properties"));
        try
        {
            PropertiesConfiguration config = (PropertiesConfiguration) builderProperties.getConfiguration();
            getLog().info("-> ajout ou modification de la valeur de la clé org.quartz.scheduler.instanceId à AUTO");
            config.setProperty("org.quartz.scheduler.instanceId", "AUTO");
            getLog().info("-> ajout ou modification de la valeur de la clé org.quartz.jobStore.isClustered à true");
            config.setProperty("org.quartz.jobStore.isClustered", "true");
            builderProperties.save();
            getLog().info("-> sauvegarde du fichier composantjasper.properties modifié");

        }
        catch (ConfigurationException confexc)
        {
            throw new MojoExecutionException(
                "Erreur, le fichier Lombok src/main/webapp/WEB-INF/properties/composantjasper.properties n'existe pas. Arrêt de la mise à jour"
                , confexc);
        }
        getLog().info("* fin paramétrage de jasper en mode clustering par défaut");
    }

    /**
     * methode Ajout commentaire commun properties : .
     *
     * @throws MojoExecutionException the mojo execution exception
     */
    private void ajoutCommentaireCommunProperties() throws MojoExecutionException
    {
        getLog().info("* début mise à jour commentaire (client de mail) dans fichier commun.properties");
        Parameters params = new Parameters();
        getLog().info("-> chargement des propriétés du fichier src/main/webapp/WEB-INF/properties/commun.properties");
        FileBasedConfigurationBuilder<FileBasedConfiguration> builderProperties =
            new FileBasedConfigurationBuilder<FileBasedConfiguration>(PropertiesConfiguration.class).
                configure(params.properties().setFileName("src/main/webapp/WEB-INF/properties/commun.properties"));

        try
        {
            PropertiesConfiguration config = (PropertiesConfiguration) builderProperties.getConfiguration();
            PropertiesConfigurationLayout layout = config.getLayout();

            StringBuilder commentBuilder = new StringBuilder();
            commentBuilder
                .append(
                "##################################################################################################################################\n");
            commentBuilder.append(
                "## Configuration du client de mail Lombok\n");
            commentBuilder.append("##\n");
            commentBuilder.append("## La configuration s'effectue avec le jeu de clés suivantes :\n");
            commentBuilder
                .append("## mail.host,mail.smtp.port,mail.auth.username,mail.auth.password,mail.smtp.timeout,mail.smtp.connectiontimeout,mail.debug\n");
            commentBuilder.append("##\n");
            commentBuilder
                .append("## La clé mail.host est nécessaire et suffisante (Le client de mail communiquera alors avec cet host sur le port 25, sans chiffrement et sans authentification)\n");
            commentBuilder.append("## \n");
            commentBuilder.append("## Pour passer le client en mode authentifié, il faut ajouter et valoriser les deux clés suivantes :\n");
            commentBuilder.append("## mail.auth.username et mail.auth.password\n");
            commentBuilder.append("##\n");
            commentBuilder
                .append("## Pour passer le client en négociation StartTLS et donc en communication chiffrée, il faut ajouter et valoriser la clé suivante (habituellement port 587) :\n");
            commentBuilder.append("## mail.smtp.port\n");
            commentBuilder.append("##\n");
            commentBuilder
                .append("## En négociation StartTLS, le client fera confiance au serveur ou relai de mail distant. Il n'est donc pas nécessaire d'installer\n");
            commentBuilder.append("## le certificat du relai ou serveur de mail dans le trousseau de clés de la jvm.\n");
            commentBuilder.append("## \n");
            commentBuilder
                .append("## Pour activer les logs des échanges avec le relai ou serveur de mail, ajouter la clé suivante et la valoriser à true :\n");
            commentBuilder.append("## mail.debug=true\n");
            commentBuilder.append("##\n");
            commentBuilder
                .append("## Pour changer le timeout de la socket de la connection qui est défini par défaut dans la configuration à 10000, ajouter et valoriser\n");
            commentBuilder.append("## la propriété suivante (exemple à 5000) : \n");
            commentBuilder.append("## mail.smtp.connectiontimeout=5000\n");
            commentBuilder.append("##\n");
            commentBuilder.append("## Pour changer le timeout lors des opérations de lecture écriture à une socket (millisecondes),\n");
            commentBuilder.append("## qui est par défaut à 30000, ajouter et valoriser la propriété suivante (exemple à 22222) : \n");
            commentBuilder.append("## mail.smtp.timeout=22222\n");
            commentBuilder.append("##\n");
            commentBuilder.append("## Ci-après deux exemples de configuration :\n");
            commentBuilder.append("##\n");
            commentBuilder
                .append("## Exemple 1 de réglage du client mail sur un serveur bouchonné sans remise (voir ici: http://smtpza-test-bouchon-sans-remise.infra.dgfip/)\n");
            commentBuilder
                .append("## réglage sur le port 587 donc chiffrement StartTLS. StartTLS nécessite une authentification, activée dans cet exemple.\n");
            commentBuilder.append("## mail.host=smtpza-test-bouchon-sans-remise.infra.dgfip\n");
            commentBuilder.append("## mail.smtp.port=587\n");
            commentBuilder.append("## mail.auth.username=test@dgfip.finances.gouv.fr\n");
            commentBuilder.append("## mail.auth.password=testmdp\n");
            commentBuilder.append("##\n");
            commentBuilder
                .append("## Exemple 2 de réglage vers relai sans authentification et sans chiffrement avec activation du mode debug et changement de timeout\n");
            commentBuilder.append("## mail.host=mailposl.infra.dgfip\n");
            commentBuilder.append("## mail.smtp.timeout=30000\n");
            commentBuilder.append("## mail.debug=true\n");
            commentBuilder
                .append("##################################################################################################################################\n");
            layout.setComment("mail.host", commentBuilder.toString());
            getLog().info("-> ajout du commentaire sur le client mail");
            builderProperties.save();
            getLog().info("-> sauvegarde du fichier commun.properties modifié");

        }
        catch (ConfigurationException fnfExc)
        {
            throw new MojoExecutionException(
                "Erreur, le fichier Lombok src/main/webapp/WEB-INF/properties/commun.properties n'existe pas. Arrêt de la mise à jour"
                , fnfExc);

        }

        getLog().info("* fin mise à jour commentaire (client de mail) dans fichier commun.properties");
    }
    
    /**
     * methode Ajout input stream : .
     *
     * @param inStream 
     * @param toFile 
     * @throws MojoExecutionException the mojo execution exception
     */
    private void ajoutInputStream(InputStream inStream,File toFile ) throws MojoExecutionException
    {
        if (!toFile.exists())
        {
            try
            {

                FileUtils.copyInputStreamToFile(inStream, toFile);
                getLog().info("-> Le fichier " + toFile.getAbsolutePath() + " a été ajouté");
            }
            catch (IOException exc)
            {
                getLog().error("! Problème lors de l'ajout du fichier " + toFile.getAbsolutePath());
                getLog().error(" Le répertoire de destination du fichier n'existe peut-être pas.");
                getLog().error(exc);
                // throw new
                // MojoExecutionException("-> Le fichier "+toFile.getAbsolutePath()+" n'a pas pu être ajouté. Arrêt.");
            }
        }
        else
        {
            getLog().info("-> Le fichier " + toFile.getAbsolutePath() + " existe déjà. Pas d'ajout.");
        }
        
    }

    /**
     * methode Mise a jour batch factory :.
     *
     * @throws MojoFailureException the mojo failure exception
     */
    private void miseAJourBatchFactory() throws MojoFailureException
    {
        getLog().info("* Début mise à jour du fichier batchFactory.xml");
        File webXml = new File("src/main/batchapp/batchFactory.xml");
        if (webXml.exists() && !webXml.isDirectory())
        {
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            DocumentBuilder dBuilder;

            try
            {
                DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
                dbFactory.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
                dBuilder = dbFactory.newDocumentBuilder();
                Document doc = dBuilder.parse(webXml);

                getLog().info(
                        "-> début suppression de la référence à applicationContext-commun-batchdao.xml dans le bean avec id=appcontext dans le fichier batchFactory.xml)");

                NodeList nodeList = doc.getElementsByTagName("value");
                for (int i = 0; i < nodeList.getLength(); i++)
                {
                    Node value = nodeList.item(i);
                    if (value != null && value.getTextContent().contains("applicationContext-commun-batchdao.xml"))
                    {
                        if ("list".equals(value.getParentNode().getNodeName()))
                        {
                            getLog().info(
                                "--> balise value contenant applicationContext-commun-batchdao.xml trouvée");
                            value.getParentNode().removeChild(value);
                            getLog().info(
                                "--> balise value contenant applicationContext-commun-batchdao.xml supprimée");
                        }
                    }

                }

                getLog()
                    .info(
                        "-> fin suppression de la référence à applicationContext-commun-batchdao.xml dans le bean avec id=appcontext dans le dans le fichier batchFactory.xml)");

                TransformerFactory trFac = TransformerFactory.newInstance();
                Transformer transformer = trFac.newTransformer();
                transformer.setOutputProperty(OutputKeys.METHOD, "xml");
                transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
                transformer.setOutputProperty(OutputKeys.INDENT, "yes");
                transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");

                DOMSource source = new DOMSource(doc);
                Result result = new StreamResult(outputStream);
                transformer.transform(source, result);

                // remplacement du fichier original pom.xml par celui modifié
                getLog().info("-> remplacement du fichier projet web.xml par le nouveau web.xml.");
                outputStream.writeTo(new FileOutputStream(webXml));

            }
            catch (SAXException | IOException | ParserConfigurationException | TransformerException exc)
            {
                getLog().error("error", exc);
                throw new MojoFailureException(exc, "erreur mise à jour", exc.getMessage());
            }

        }
        else
        {
            getLog().info("Le fichier web.xml n'existe pas dans le répertoire WEB-INF.");
        }
        getLog().info("* Fin mise à jour du fichier web.xml");
    }

    /**
     * methode Suppression fichiers : .
     *
     * @throws MojoFailureException the mojo failure exception
     * @throws MojoExecutionException the mojo execution exception
     */
    private void suppressionFichiers() throws MojoFailureException, MojoExecutionException
    {
        // suppression des pages d'erreur qui ont été
        // remontées dans le fragment web de lombok // on ne supprime pas tout le répertoire pour éviter de supprimer
        // des pages d'erreur non lombok
        suppressionFichier(new File("src/main/webapp/WEB-INF/pages/errors/erreuraccesrefuse.jsp"));
        suppressionFichier(new File("src/main/webapp/WEB-INF/pages/errors/erreurargumentextraction.jsp"));
        suppressionFichier(new File("src/main/webapp/WEB-INF/pages/errors/erreurcsrf.jsp"));
        suppressionFichier(new File("src/main/webapp/WEB-INF/pages/errors/erreurexploitation.jsp"));
        suppressionFichier(new File("src/main/webapp/WEB-INF/pages/errors/erreurflowexpired.jsp"));
        suppressionFichier(new File("src/main/webapp/WEB-INF/pages/errors/erreurflowid.jsp"));
        suppressionFichier(new File("src/main/webapp/WEB-INF/pages/errors/erreurmaxupload.jsp"));
        suppressionFichier(new File("src/main/webapp/WEB-INF/pages/errors/erreurnavigation.jsp"));
        suppressionFichier(new File("src/main/webapp/WEB-INF/pages/errors/erreurprogrammation.jsp"));
        suppressionFichier(new File("src/main/webapp/WEB-INF/pages/errors/erreursgeneriques.jsp"));
        suppressionFichier(new File("src/main/webapp/WEB-INF/pages/errors/erreurstd.jsp"));
        suppressionFichier(new File("src/main/webapp/WEB-INF/pages/errors/erreurwebservice.jsp"));
        suppressionFichier(new File("src/main/webapp/WEB-INF/pages/errors/rapporterreur.jspf"));
        suppressionFichier(new File("src/main/webapp/WEB-INF/pages/errors/projetblanc-V19R02.txt"));
        suppressionFichier(new File("src/main/webapp/WEB-INF/pages/identification.jsp"));
        suppressionFichier(new File("src/main/webapp/WEB-INF/pages/listmessages.jsp"));
        suppressionFichier(new File("src/main/webapp/WEB-INF/pages/messagebloquantdetecteavecportail.jsp"));
        suppressionFichier(new File("src/main/webapp/WEB-INF/pages/sessionexpiree.jsp"));
        suppressionRepertoire(new File("src/main/webapp/WEB-INF/tags/app"));
        suppressionRepertoire(new File("src/main/webapp/WEB-INF/tags/html"));
        suppressionFichier(new File("projetblanc-V19R02.txt"));

        // suppression des fichiers de configuration spring qui sont maintenant dans les modules lombok:
        suppressionFichier(new File("src/main/resources/conf/application/applicationContext-adresse-dao.xml"));
        suppressionFichier(new File("src/main/resources/conf/application/applicationContext-adresse-service.xml"));
        suppressionFichier(new File("src/main/resources/conf/application/applicationContext-atlas-dao.xml"));
        suppressionFichier(new File("src/main/resources/conf/application/applicationContext-atlas-service.xml"));
        suppressionFichier(new File("src/main/resources/conf/application/applicationContext-bancaire-dao.xml"));
        suppressionFichier(new File("src/main/resources/conf/application/applicationContext-bancaire-service.xml"));
        suppressionFichier(new File("src/main/resources/conf/application/applicationContext-clamav-service.xml"));
        suppressionFichier(new File("src/main/resources/conf/application/applicationContext-itm-service.xml"));
        suppressionFichier(new File("src/main/resources/conf/application/applicationContext-itm-ws-client.xml"));
        suppressionFichier(new File("src/main/resources/conf/application/applicationContext-journal-dao.xml"));
        suppressionFichier(new File("src/main/resources/conf/application/applicationContext-journal-service.xml"));
        suppressionFichier(new File("src/main/resources/conf/application/applicationContext-journal-ws-client.xml"));
        suppressionFichier(new File("src/main/resources/conf/application/applicationContext-journal-ws.xml"));
        suppressionFichier(new File("src/main/resources/conf/application/applicationContext-ogre-bean-session.xml"));
        suppressionFichier(new File("src/main/resources/conf/application/applicationContext-ogre-dao-jdbc.xml"));
        suppressionFichier(new File("src/main/resources/conf/application/applicationContext-ogre-dao.xml"));
        suppressionFichier(new File("src/main/resources/conf/application/applicationContext-ogre-datasources.xml"));
        suppressionFichier(new File("src/main/resources/conf/application/applicationContext-ogre-itm-service.xml"));
        suppressionFichier(new File("src/main/resources/conf/application/applicationContext-ogre-service.xml"));
        suppressionFichier(new File("src/main/resources/conf/application/applicationContext-ogre-variables.xml"));
        // pas de suppression du composant structure (qui n'est pas un module du framework lombok) si utilisé. paramètre
        // du goal à false par défaut.
        if (!utiliseComposantStructure)
        {
            suppressionFichier(new File("src/main/resources/conf/application/applicationContext-structure-dao.xml"));
            suppressionFichier(new File("src/main/resources/conf/application/applicationContext-structure-service.xml"));
        }
        suppressionFichier(new File("src/main/resources/conf/application/applicationContext-upload-dao.xml"));
        suppressionFichier(new File("src/main/resources/conf/application/applicationContext-upload-service.xml"));
        suppressionFichier(new File("src/main/resources/conf/application/applicationContext-ws.xml"));

    }

    /**
     * methode Suppression repertoires : .
     *
     * @throws MojoFailureException the mojo failure exception
     * @throws MojoExecutionException the mojo execution exception
     */
    private void suppressionRepertoires() throws MojoFailureException, MojoExecutionException
    {
        suppressionRepertoire(new File("src/main/resources/conf/commun"));
        suppressionRepertoire(new File("src/main/webapp/composants"));
        suppressionRepertoire(new File("src/main/webapp/WEB-INF/flows/composantinfoutil"));
        suppressionRepertoire(new File("src/main/webapp/WEB-INF/pages/composantinfoutil"));
    }

    /**
     * methode Mise a jour web xml projet : .
     *
     * @throws MojoFailureException the mojo failure exception
     */
    private void miseAJourWebXmlProjet() throws MojoFailureException
    {
        getLog().info("* Début mise à jour du fichier web.xml");
        File webXml = new File("src/main/webapp/WEB-INF/web.xml");
        if (webXml.exists() && !webXml.isDirectory())
        {
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            DocumentBuilder dBuilder;

            try
            {
                DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
                dbFactory.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
                dBuilder = dbFactory.newDocumentBuilder();

                Document doc = dBuilder.parse(webXml);

                // opérations à effectuer sur le web.xml
                updateEnteteWebXml(doc);

                getLog().info("-> début suppression des balises Lombok remontées dans le web fragment lombok (lombok-webfragment)");

                suppressionBalisesWebFragment(doc, new String[] {"absolute-ordering", "locale-encoding-mapping-list", "session-config",
                        "jsp-config", "welcome-file-list"});

                removeContextParamWithParamName(doc, "javax.servlet.jsp.jstl.fmt.fallbackLocale");
                removeContextParamWithParamName(doc, "log4jExposeWebAppRoot");
                removeContextParamWithParamName(doc, "log4jConfigLocation");
                removeContextParamWithParamName(doc, "contextConfigLocation");
                removeContextParamWithParamName(doc, "extremecomponentsMessagesLocation");
                removeContextParamWithParamName(doc, "extremecomponentsPreferencesLocation");
                removeContextParamWithParamName(doc, "springJspExpressionSupport");
                removeContextParamWithParamName(doc, "javax.faces.DATETIMECONVERTER_DEFAULT_TIMEZONE_IS_SYSTEM_TIMEZONE");
                removeContextParamWithParamName(doc, "defaultHtmlEscape");

                removeListener(doc, "org.springframework.web.util.Log4jConfigListener");
                removeListener(doc, "org.springframework.web.context.ContextLoaderListener");
                removeListener(doc, "fr.gouv.finances.lombok.mvc.ApplicationStartupListener");
                removeListener(doc, "org.springframework.web.util.Log4jConfigListener");
                removeListener(doc, "org.springframework.web.context.ContextLoaderListener");

                removeServletAndMappings(doc, "dispatcher", new String[] {"*.ex", "/rest/*", "/async/*"});
                removeServletAndMappings(doc, "ImageServlet", new String[] {"/image"});
                removeServletAndMappings(doc, "FichierJointServlet", new String[] {"/fichierjoint/*"});
                removeServletAndMappings(doc, "ServletWS", new String[] {"/services/*"});
                removeServletAndMappings(doc, "FichierJointServlet", new String[] {"/fichierjoint/*"});
                removeServletAndMappings(doc, "Resource Servlet", new String[] {"/resources/*"});

                removeFilterAndMappings(doc, "CharacterEncodingFilter", new String[] {"/*"});
                removeFilterAndMappings(doc, "SpringSecurity-Authentication", new String[] {"/j_spring_security_check",
                        "/j_spring_security_logout", "/j_appelportail", "*.ex", "*.pdf"});
                removeFilterAndMappings(doc, "eXtremeExport", new String[] {"/*"});

                removeErrorPage(doc, "/WEB-INF/pages/errors/erreurstd.jsp", new String[] {"404", "403", "503",
                        "java.lang.IllegalStateException", "java.lang.Throwable"});

                removeDisplayName(doc);

                addAbsoluteOrdering(doc);
                
                addContextParamForJavamelody(doc);

                StringBuilder info =
                    new StringBuilder(" Pour les contrôleurs annotés, voir le petit exemple de contrôleur: DemoControleurAnnote.java \n");
                info.append("Les mappings définis sont : *.ex, /rest/*, /async/* \n");
                info.append('\n');
                info.append("Pour ajouter un mapping, il faut recopier les mappings de la servlet dispatcher du fichier web-fragment.xml et ajouter\n");
                info.append("vos mappings (Rq: completer la conf de sécurité en fonction");

                addComment(doc, info.toString());

                // ajout d'un commentaire devant les profiles

                StringBuilder profilesComment = new StringBuilder("ApplicationContext:\n");

                profilesComment
                    .append("Le fragment web lombok active uniquement le profile postgre (pour des sources de donnees postgresql).\n");
                profilesComment
                    .append("<context-param>\n<param-name>spring.profiles.active</param-name>\n<param-value>postgre</param-value>\n</context-param>\n");

                profilesComment
                    .append("Si vous utilisez une base de données oracle ou une base de données embarquée hsqldb, vous devez alors\n");
                profilesComment
                    .append("activer le profil correspondant: oracle ou embedded en redifinissant le parametre spring.profiles.active\n");

                profilesComment
                    .append("Si vous utilisez des composants lombok (edition, atals,ogre, structure, itm, journal; journal-ws, jdbc, etc.),\n");
                profilesComment.append("vous devez activer ces composants via leur profile respectif:\n");
                profilesComment
                    .append("edition,atlas,journal,journal-ws,structure,itm,bancaire,sireme,clamav,integration,springbatch,jdbc\n");
                profilesComment.append("dans le fragment web lombok, le profile postgre\n");

                NodeList contextParamList = doc.getDocumentElement().getElementsByTagName("context-param");
                if (contextParamList != null && contextParamList.getLength() > 0)
                {
                    for (int i = 0; i < contextParamList.getLength(); i++)
                    {
                        if (contextParamList.item(i).getTextContent().contains("spring.profiles.active"))
                        {
                            getLog().info("-> ajout d'un commentaire avant spring.profiles.active");
                            doc.getDocumentElement().insertBefore(doc.createComment(profilesComment.toString()), contextParamList.item(0));
                        }

                    }

                }

                getLog().info("-> fin suppression des balises Lombok remontées dans le web fragment lombok (lombok-webfragment)");

                TransformerFactory trFac = TransformerFactory.newInstance();
                Transformer transformer = trFac.newTransformer();
                transformer.setOutputProperty(OutputKeys.METHOD, "xml");
                transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
                transformer.setOutputProperty(OutputKeys.INDENT, "yes");
                transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");

                DOMSource source = new DOMSource(doc);
                Result result = new StreamResult(outputStream);
                transformer.transform(source, result);

                // remplacement du fichier original pom.xml par celui modifié
                getLog().info("-> remplacement du fichier projet web.xml par le nouveau web.xml.");
                outputStream.writeTo(new FileOutputStream(webXml));

            }
            catch (SAXException | IOException | ParserConfigurationException | TransformerException exc)
            {
                getLog().error("error", exc);
                throw new MojoFailureException(exc, "erreur mise à jour", exc.getMessage());
            }

        }
        else
        {
            getLog().info("Le fichier web.xml n'existe pas dans le répertoire WEB-INF.");
        }
        getLog().info("* Fin mise à jour du fichier web.xml");
    }

    /**
     * methode Adds the context param for javamelody : DOCUMENTEZ_MOI.
     *
     * @param doc DOCUMENTEZ_MOI
     */
    private void addContextParamForJavamelody(Document doc)
    {
            getLog().info("-> début ajout des paramètres (context-param) pour javamelody");
            
            Node lastChild = doc.getDocumentElement();
            
            Comment commentJavaMelody = doc.createComment(" Les 3 paramètres suivants sont liés à javamelody (https://github.com/javamelody/javamelody/wiki) attention, si vous en ajoutez d'autres, bien les préfixer par javamelody");
            
            lastChild.appendChild(commentJavaMelody);
            
            Element contextParam1 = doc.createElement("context-param");
            Element name1 = doc.createElement("param-name");
            name1.setTextContent("javamelody.quartz-default-listener-disabled");
            Element value1 = doc.createElement("param-value");
            value1.setTextContent("true");
            
            contextParam1.appendChild(name1);
            contextParam1.appendChild(value1);
            
            lastChild.appendChild(contextParam1);
            
            Element contextParam2 = doc.createElement("context-param");
            Element name2 = doc.createElement("param-name");
            name2.setTextContent("javamelody.csrf-protection-enabled");
            Element value2 = doc.createElement("param-value");
            value2.setTextContent("true");
            
            contextParam2.appendChild(name2);
            contextParam2.appendChild(value2);
            
            lastChild.appendChild(contextParam2);
            
            
            Element contextParam3 = doc.createElement("context-param");
            Element name3 = doc.createElement("param-name");
            name3.setTextContent("javamelody.update-check-disabled");
            Element value3 = doc.createElement("param-value");
            value3.setTextContent("true");
            
            contextParam3.appendChild(name3);
            contextParam3.appendChild(value3);
            
            lastChild.appendChild(contextParam3);
            
            getLog().info("-> fin ajout des paramètres (context-param) pour javamelody");
    }

    /**
     * methode Adds the comment : .
     *
     * @param doc DOCUMENTEZ_MOI
     * @param comment DOCUMENTEZ_MOI
     */
    private void addComment(Document doc, String comment)
    {
        getLog().info("-> ajout d'un commentaire à la fin du web.xml");
        Element webApp = doc.getDocumentElement();
        Comment commentElmnt = doc.createComment(comment);
        webApp.appendChild(commentElmnt);

    }

    /**
     * methode Adds the absolute ordering : .
     *
     * @param doc DOCUMENTEZ_MOI
     */
    private void addAbsoluteOrdering(Document doc)
    {
        getLog().info("-> début ajout des web fragments lombok et javamelody");
        Element webApp = doc.getDocumentElement();
        Node firstChild = webApp.getFirstChild();
        Element absolute = doc.createElement("absolute-ordering");
        Element name = doc.createElement("name");
        name.setTextContent("lombok_webfragment");
        absolute.appendChild(name);
        
        Element javameldoy=doc.createElement("name");
        javameldoy.setTextContent("javamelody");
        absolute.appendChild(javameldoy);

        String comment =
            " Active uniquement le fragment web lombok (servlet 3.0) voir lombok/lombok-webfragment/src/main/resources/META-INF/web-fragment.xml Les paramétrages (ex: <context-param>), les filtres (<filter>) et autres éléments (session-timeout) présents dans le fragment peuvent être redéfinis dans ce web.xml. Ces redéfinitions auront priorité. ";

        webApp.insertBefore(doc.createComment(comment), firstChild);
        
        comment="Pour visualiser dans le log de tomcat le web.xml agrégé, mettre dans l'element context du fichier context.xml, l'attribut logEffectiveWebXml=\"true\"";
        
        webApp.insertBefore(doc.createComment(comment), firstChild);
        
        webApp.insertBefore(absolute, firstChild);
        getLog().info("-> fin ajout des web fragments lombok et javamelody");

    }

    /**
     * methode Removes the display name : .
     *
     * @param doc DOCUMENTEZ_MOI
     */
    private void removeDisplayName(Document doc)
    {
        NodeList displays = doc.getDocumentElement().getElementsByTagName("display-name");
        if (displays != null && displays.getLength() == 1)
        {
            doc.getDocumentElement().removeChild(displays.item(0));
            getLog().info("-> balise display-name supprimée.");

        }
        else
        {
            getLog().info("-> pas de balise display-name ou nombre de balises > 1 ");
        }

    }

    /**
     * methode Removes the filter and mappings : .
     *
     * @param doc DOCUMENTEZ_MOI
     * @param filterName DOCUMENTEZ_MOI
     * @param mappings DOCUMENTEZ_MOI
     */
    private void removeFilterAndMappings(Document doc, String filterName, String[] mappings)
    {
        getLog().info("-> Suppression du filter avec name :" + filterName);
        Element webApp = doc.getDocumentElement();
        NodeList nodeList = webApp.getChildNodes();

        for (int i = 0; i < nodeList.getLength(); i++)
        {
            Node current = nodeList.item(i);

            if (current.getNodeType() == Node.ELEMENT_NODE)
            {
                if ("filter".equals(current.getNodeName()))
                {
                    for (Node childNode = current.getFirstChild(); childNode != null;)
                    {
                        Node curr = childNode.getNextSibling();
                        if (childNode.getNodeType() == Node.ELEMENT_NODE && "filter-name".equals(childNode.getNodeName())
                            && childNode.getTextContent().contains(filterName))
                        {

                            webApp.removeChild(current);
                            getLog().info("--> filter avec name : " + filterName + " supprimé.");
                        }
                        childNode = curr;
                    }

                }
                if ("filter-mapping".equals(current.getNodeName()))
                {
                    for (Node childNode = current.getFirstChild(); childNode != null;)
                    {

                        Node curr = childNode.getNextSibling();
                        if (childNode.getNodeType() == Node.ELEMENT_NODE && "url-pattern".equals(childNode.getNodeName()))
                        {
                            for (int j = 0; j < mappings.length; j++)
                            {
                                if (childNode.getTextContent().contains(mappings[j]))
                                {
                                    webApp.removeChild(current);
                                    getLog().info(
                                        "--> mapping pour filter :" + filterName + " avec url-pattern " + mappings[j] + " supprimé.");

                                }
                            }

                        }
                        childNode = curr;
                    }
                }
            }

        }

    }

    /**
     * methode Removes the error page : .
     *
     * @param doc DOCUMENTEZ_MOI
     * @param pathErrorPage DOCUMENTEZ_MOI
     * @param codeORType DOCUMENTEZ_MOI
     */
    private void removeErrorPage(Document doc, String pathErrorPage, String[] codeORType)
    {

        StringBuilder allCodeOrType = new StringBuilder(codeORType.length);
        for (String each : codeORType)
        {
            allCodeOrType.append(',').append(each);
        }
        allCodeOrType.deleteCharAt(0);

        getLog().info(
            "-> Début Suppression de la (ou des) page(s) d'erreur avec location: " + pathErrorPage
                + "  ayant comme code erreur ou type d'exception :"
                + allCodeOrType);

        Element webApp = doc.getDocumentElement(); // getElementsByTagName("error-page");
        for (Node childNode = webApp.getFirstChild(); childNode != null;)
        {
            Node current = childNode.getNextSibling();
            if (childNode.getNodeType() == Node.ELEMENT_NODE && "error-page".equals(childNode.getNodeName()))
            {
                NodeList errorsElmts = childNode.getChildNodes();
                boolean pathOk = false;
                boolean codeOk = false;
                boolean exceptionOK = false;
                for (int i = 0; i < errorsElmts.getLength(); i++)
                {
                    Node currErr = errorsElmts.item(i);
                    if (currErr.getNodeType() == Node.ELEMENT_NODE
                        && "location".equals(currErr.getNodeName())
                        && currErr.getTextContent().contains(pathErrorPage))
                    {

                        pathOk = true;

                    }
                    if (currErr.getNodeType() == Node.ELEMENT_NODE
                        && "error-code".equals(currErr.getNodeName()))
                    {
                        for (int j = 0; j < codeORType.length; j++)
                        {
                            if (currErr.getTextContent().contains(codeORType[j]))
                            {
                                getLog().info("--> page erreur avec code:" + codeORType[j]);
                                codeOk = true;
                                break;
                            }
                        }

                    }
                    if (currErr.getNodeType() == Node.ELEMENT_NODE
                        && "exception-type".equals(currErr.getNodeName()))
                    {
                        for (int j = 0; j < codeORType.length; j++)
                        {
                            if (currErr.getTextContent().contains(codeORType[j]))
                            {
                                getLog().info("--> page erreur avec exception:" + codeORType[j]);
                                exceptionOK = true;
                                break;
                            }
                        }

                    }

                }
                if (pathOk && (codeOk || exceptionOK))
                {
                    webApp.removeChild(childNode);
                    getLog().info("--> page erreur avec location " + pathErrorPage + " et code ou erreur ci-dessus supprimée");
                }

            }
            childNode = current;

        }

    }

    /**
     * methode Removes the servlet and mappings : .
     *
     * @param doc DOCUMENTEZ_MOI
     * @param servletName DOCUMENTEZ_MOI
     * @param mappings DOCUMENTEZ_MOI
     */
    private void removeServletAndMappings(Document doc, String servletName, String[] mappings)
    {

        getLog().info("-> Suppression de la servlet avec name :" + servletName);
        Element webApp = doc.getDocumentElement();
        NodeList nodeList = webApp.getChildNodes();

        for (int i = 0; i < nodeList.getLength(); i++)
        {
            Node current = nodeList.item(i);

            if (current.getNodeType() == Node.ELEMENT_NODE)
            {
                if ("servlet".equals(current.getNodeName()))
                {
                    for (Node childNode = current.getFirstChild(); childNode != null;)
                    {
                        Node curr = childNode.getNextSibling();
                        if (childNode.getNodeType() == Node.ELEMENT_NODE && "servlet-name".equals(childNode.getNodeName())
                            && childNode.getTextContent().contains(servletName))
                        {

                            webApp.removeChild(current);
                            getLog().info("--> servlet avec name : " + servletName + " supprimée.");
                        }
                        childNode = curr;
                    }

                }
                if ("servlet-mapping".equals(current.getNodeName()))
                {
                    for (Node childNode = current.getFirstChild(); childNode != null;)
                    {

                        Node curr = childNode.getNextSibling();
                        if (childNode.getNodeType() == Node.ELEMENT_NODE && "url-pattern".equals(childNode.getNodeName()))
                        {
                            for (int j = 0; j < mappings.length; j++)
                            {
                                if (childNode.getTextContent().contains(mappings[j]))
                                {
                                    webApp.removeChild(current);
                                    getLog().info(
                                        "--> mapping pour servlet :" + servletName + " avec url-pattern " + mappings[j] + " supprimé.");

                                }
                            }

                        }
                        childNode = curr;
                    }
                }
            }

        }

    }

    /**
     * methode Removes the listener : .
     *
     * @param doc DOCUMENTEZ_MOI
     * @param fullClassName DOCUMENTEZ_MOI
     */
    private void removeListener(Document doc, String fullClassName)
    {
        getLog().info("-> suppression de la balise listener avec listener-class :" + fullClassName);
        NodeList listeners = doc.getElementsByTagName("listener-class");
        for (int i = 0; i < listeners.getLength(); i++)
        {
            if (fullClassName != null && listeners.item(i).getTextContent().contains(fullClassName))
            {
                getLog().info("--> balise trouvée, listener avec listener-class :" + fullClassName);
                doc.getDocumentElement().removeChild(listeners.item(i).getParentNode());
                getLog().info("--> balise supprimée, listener avec listener-class :" + fullClassName);
            }

        }
    }

    /**
     * methode Removes the context param with param name : .
     *
     * @param doc DOCUMENTEZ_MOI
     * @param paramName DOCUMENTEZ_MOI
     */
    private void removeContextParamWithParamName(Document doc, String paramName)
    {
        getLog().info("--> suppression de la balise context-param avec param-name:" + paramName);
        // pas très optimisé mais on verra après (boucle à chaque appel sur les param)
        NodeList contextparams = doc.getElementsByTagName("param-name");
        for (int i = 0; i < contextparams.getLength(); i++)
        {
            if (contextparams.item(i).getTextContent().equals(paramName)
                && contextparams.item(i).getParentNode().getNodeName().contains("context-param"))
            {
                getLog().info("---> balise trouvée dans le web.xml :" + paramName);
                doc.getDocumentElement().removeChild(contextparams.item(i).getParentNode());
                getLog().info("---> balise context-param avec param-name:" + paramName + " supprimée");

            }
        }
    }

    /**
     * methode Suppression balises web fragment : .
     *
     * @param doc DOCUMENTEZ_MOI
     * @param balisesToDelete DOCUMENTEZ_MOI
     */
    private void suppressionBalisesWebFragment(Document doc, String[] balisesToDelete)
    {

        NodeList balises = null;

        for (int i = 0; i < balisesToDelete.length; i++)
        {
            balises = doc.getElementsByTagName(balisesToDelete[i]);
            if (balises != null && balises.getLength() == 1)
            {
                doc.getDocumentElement().removeChild(balises.item(0));
                getLog().info("--> balise " + balisesToDelete[i] + " supprimée");
            }
        }

    }

    /**
     * methode Update entete web xml : .
     *
     * @param doc DOCUMENTEZ_MOI
     */
    private void updateEnteteWebXml(Document doc)
    {
        // on remet au propre les entêtes
        doc.getDocumentElement().setAttribute("xmlns", "http://java.sun.com/xml/ns/javaee");
        doc.getDocumentElement().setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
        doc.getDocumentElement().setAttribute("xsi:schemaLocation",
            "http://java.sun.com/xml/ns/javaee http://java.sun.com/xml/ns/javaee/web-app_3_0.xsd");
        doc.getDocumentElement().setAttribute("version", "3.0");

        // on supprimer les attributs id et metadata-complete
        doc.getDocumentElement().removeAttribute("id");
        doc.getDocumentElement().removeAttribute("metadata-complete");

    }

    /**
     * methode Creates the dependencies to add : .
     *
     * @return sets the
     */
    private Set<PomDependance> createDependenciesToAdd()
    {
        Set<PomDependance> dependenciesToAdd = new HashSet<PomDependance>();

        dependenciesToAdd.add(new PomDependance("lombok-adresse", "fr.gouv.finances.lombok"));
        dependenciesToAdd.add(new PomDependance("lombok-apptags", "fr.gouv.finances.lombok"));
        dependenciesToAdd.add(new PomDependance("lombok-atlas", "fr.gouv.finances.lombok"));
        dependenciesToAdd.add(new PomDependance("lombok-bancaire", "fr.gouv.finances.lombok"));
        dependenciesToAdd.add(new PomDependance("lombok-batch", "fr.gouv.finances.lombok"));
        dependenciesToAdd.add(new PomDependance("lombok-core", "fr.gouv.finances.lombok"));
        dependenciesToAdd.add(new PomDependance("lombok-editions", "fr.gouv.finances.lombok"));
        dependenciesToAdd.add(new PomDependance("lombok-itm", "fr.gouv.finances.lombok"));
        dependenciesToAdd.add(new PomDependance("lombok-journal", "fr.gouv.finances.lombok"));
        dependenciesToAdd.add(new PomDependance("lombok-itm", "fr.gouv.finances.lombok"));
        dependenciesToAdd.add(new PomDependance("lombok-ogre-core", "fr.gouv.finances.lombok"));
        dependenciesToAdd.add(new PomDependance("lombok-ogre-webfragment", "fr.gouv.finances.lombok"));
        dependenciesToAdd.add(new PomDependance("lombok-structure", "fr.gouv.finances.lombok"));
        dependenciesToAdd.add(new PomDependance("lombok-util-createtbspg", "fr.gouv.finances.lombok"));
        dependenciesToAdd.add(new PomDependance("lombok-ws", "fr.gouv.finances.lombok"));
        dependenciesToAdd.add(new PomDependance("lombok-srm", "fr.gouv.finances.lombok"));
        dependenciesToAdd.add(new PomDependance("lombok-webfragment", "fr.gouv.finances.lombok", "runtime"));
        dependenciesToAdd.add(new PomDependance("jquery", "org.webjars", "runtime", "1.8.2"));
        dependenciesToAdd.add(new PomDependance("datatables", "org.webjars", "runtime", "1.9.4"));
        dependenciesToAdd.add(new PomDependance("datatables-bootstrap", "org.webjars", "runtime", "2-20120201"));
        dependenciesToAdd.add(new PomDependance("lodash", "org.webjars", "runtime", "2.4.1"));
        dependenciesToAdd.add(new PomDependance("bootstrap", "org.webjars", "runtime", "2.3.2"));
        dependenciesToAdd.add(new PomDependance("underscorejs", "org.webjars", "runtime", "1.5.2"));
        dependenciesToAdd.add(new PomDependance("momentjs", "org.webjars", "runtime", "2.5.0"));
        dependenciesToAdd.add(new PomDependance("numeral-js", "org.webjars", "runtime", "1.4.5"));

        dependenciesToAdd.add(new PomDependance("javax.servlet-api", "javax.servlet"));
        dependenciesToAdd.add(new PomDependance("slf4j-api", "org.slf4j"));
        dependenciesToAdd.add(new PomDependance("slf4j-log4j12", "org.slf4j"));
        dependenciesToAdd.add(new PomDependance("jcl-over-slf4j", "org.slf4j"));

        dependenciesToAdd.add(new PomDependance("spring-ldap-core", "org.springframework.ldap", null, "2.0.2.RELEASE"));
        dependenciesToAdd.add(new PomDependance("mockito-all", "org.mockito"));

        return dependenciesToAdd;
    }

    /**
     * methode Suppression fichier : Cette méthode supprime le fichier passé en paramètre.
     *
     * @param fichierASupprimer le fichier à supprimer (File)
     * @return true, si c'est vrai
     * @throws MojoFailureException the mojo failure exception lorsque le fichier est ouvert par exemple
     */
    private boolean suppressionFichier(File fichierASupprimer) throws MojoFailureException
    {
        getLog().info("Tentative de suppression du fichier :" + fichierASupprimer);

        boolean result;
        try
        {
            result = Files.deleteIfExists(fichierASupprimer.toPath());

            if (result)
            {
                getLog().info("-> Le fichier " + fichierASupprimer.toPath().toString() + " a été supprimé avec succès.");

            }
            else
            {
                getLog().info("-> Le fichier " + fichierASupprimer.toPath().toString() + " n'a pas été supprimé car il n'existe pas.");

            }
        }
        catch (IOException exc)
        {
            throw new MojoFailureException(exc,
                "! La suppression du fichier " + fichierASupprimer.toPath().toString()
                    + " a échoué (fichier ouvert ?). Arrêt de la mise à jour", exc.getMessage());

        }
        return result;

    }

    /**
     * methode Remplacement fichier : supprime et remplace ce fichier par un fichier du même nom dans le réperoire
     * /src/main/resources/maj233.
     *
     * @param fichierARemplacer le fichier à remplacer
     * @throws MojoExecutionException the mojo execution exception
     * @throws MojoFailureException the mojo failure exception
     */
    private void remplacementFichier(File fichierARemplacer) throws MojoExecutionException, MojoFailureException
    {
        getLog()
            .info(
                "* Remplacement du fichier " + fichierARemplacer.getPath().toString() + " par celui de Lombok en version "
                    + versionLombokCible);

        boolean result = suppressionFichier(fichierARemplacer);
        if (result)
        {

            String fichierCorrespondant = "/maj233/" + fichierARemplacer.getName();
            InputStream newAppConfigFile = getClass().getResourceAsStream(fichierCorrespondant);
            try
            {
                FileUtils.copyInputStreamToFile(newAppConfigFile, fichierARemplacer);
            }
            catch (IOException exc)
            {
                String msg = "Un problème est survenu lors de la copie du fichier remplaçant " + fichierARemplacer.getName()
                    + " vers le repertoire "
                    + fichierARemplacer.getPath().toString();
                getLog().error(msg);
                throw new MojoFailureException(exc, msg, exc.getMessage());
            }

        }
        else
        {
            throw new MojoExecutionException(
                "Le fichier Lombok src/main/resources/conf/app-config.xml n'existe pas. Arrêt de la mise à jour");
        }

    }

    /**
     * methode Suppression repertoire : supprime le répertoire passé en paramètre.
     *
     * @param repertoire repertoire à supprimer
     * @throws MojoFailureException the mojo failure exception
     * @throws MojoExecutionException the mojo execution exception
     */
    private void suppressionRepertoire(File repertoire) throws MojoFailureException, MojoExecutionException
    {
        getLog().info("Tentative de suppression du répertoire :" + repertoire.getPath().toString());
        try
        {
            FileUtils.deleteDirectory(repertoire);

            getLog().info("-> Le répertoire " + repertoire.getPath().toString() + " a été supprimé");

        }
        catch (IOException exc)
        {
            getLog().error(exc.getMessage());
            throw new MojoFailureException(exc, "! Un problème est survenu lors de la suppression du répertoire "
                + repertoire.getPath().toString(), exc.getMessage());
        }
    }

    /**
     * methode Mise a jour version lombok : mise à jour dans le fichier pom.xml du projet de la version de Lombok.
     *
     * @param depToDelete DOCUMENTEZ_MOI
     * @param dependenciesToAdd DOCUMENTEZ_MOI
     * @throws MojoExecutionException the mojo execution exception
     * @throws MojoFailureException the mojo failure exception
     */
    private void miseAJourPomProjet(String[] depToDelete, Set<PomDependance> dependenciesToAdd) throws MojoExecutionException,
        MojoFailureException
    {

        getLog().info("* début mise à jour du pom du projet");

        String fileName = "pom.xml";
        File xmlFile = new File(fileName);

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        DocumentBuilder dBuilder;

        try
        {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            dbFactory.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
            dBuilder = dbFactory.newDocumentBuilder();

            Document doc = dBuilder.parse(xmlFile);

            // opérations à effectuer sur le pom
            updateParentLombokVersion(doc);
            updateDependencies(doc, depToDelete, dependenciesToAdd);
            deleteOutputEtTestDirectory(doc);

            TransformerFactory trFac = TransformerFactory.newInstance();
            Transformer transformer = trFac.newTransformer();
            transformer.setOutputProperty(OutputKeys.METHOD, "xml");
            transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");

            DOMSource source = new DOMSource(doc);
            Result result = new StreamResult(outputStream);
            transformer.transform(source, result);

            // remplacement du fichier original pom.xml par celui modifié
            outputStream.writeTo(new FileOutputStream(xmlFile));

        }
        catch (SAXException | IOException | ParserConfigurationException | TransformerException exc)
        {
            getLog().error("error", exc);
            throw new MojoFailureException(exc, "erreur mise à jour", exc.getMessage());
        }

        getLog().info("* fin mise à jour du pom du projet");

    }

    /**
     * methode Delete output et test directory : .
     *
     * @param doc DOCUMENTEZ_MOI
     */
    private void deleteOutputEtTestDirectory(Document doc)
    {
        getLog().info("-> début suppression de outputDirectory et de testOutputDirectory dans la balise build");

        NodeList builds = doc.getElementsByTagName("build");

        Node current = null;

        Node buildNode = null;

        for (int i = 0; i < builds.getLength(); i++)
        {
            current = builds.item(i);
            if (current.getParentNode().getNodeName() == "project")
            {
                buildNode = current;
                break;
            }

        }

        if (buildNode != null)
        {

            for (Node childNode = buildNode.getFirstChild(); childNode != null;)
            {
                Node nextChild = childNode.getNextSibling();

                if (childNode.getNodeName() == "testOutputDirectory")
                {
                    buildNode.removeChild(childNode);
                    getLog().info("--> suppression de la balise <build><testOuputDirectory>");
                }
                if (childNode.getNodeName() == "outputDirectory")
                {
                    buildNode.removeChild(childNode);
                    getLog().info("--> suppression de la balise <build><outputDirectory>");
                }

                childNode = nextChild;

            }
        }
        else
        {
            getLog().info("!! Pas de balise <build> dans ce projet !!");
        }

        getLog().info("-> fin suppression de outputDirectory et de testOutputDirectory dans la balise build");

    }

    /**
     * methode Update dependencies : mise à jour des dépendances liées à lombok.
     *
     * @param doc document pom.xml
     * @param toDelete DOCUMENTEZ_MOI
     * @param dependenciesToAdd DOCUMENTEZ_MOI
     */
    private void updateDependencies(Document doc, String[] toDelete, Set<PomDependance> dependenciesToAdd)
    {
        NodeList dependencies = doc.getElementsByTagName("dependencies");
        Node current = null;
        Node dependencyElmt = null;

        for (int i = 0; i < dependencies.getLength(); i++)
        {

            current = dependencies.item(i);
            // au cas ou, on vérifie que nous sommes bien sur l'élément
            // dependencies juste en dessous de l'élément project.
            if (current.getParentNode().getNodeName() == "project")
            {
                getLog().info("-> début suppression des dépendances");
                NodeList dependencyElements = current.getChildNodes();
                for (int j = 0; j < dependencyElements.getLength(); j++)
                {
                    dependencyElmt = dependencyElements.item(j);

                    updateDependency(dependencyElmt, toDelete);

                }
                getLog().info("-> fin suppression des dépendances");
                getLog().info("-> début ajout des dépendances");
                // ajout des dépendances
                for (Iterator<PomDependance> iterator = dependenciesToAdd.iterator(); iterator.hasNext();)
                {

                    PomDependance pomDep = iterator.next();

                    Element dependency = PomDependance.from(pomDep, doc);

                    current.appendChild(dependency);
                    getLog().info("--> ajout de la dépendance :" + pomDep.getArtifactId());

                }
                getLog().info("-> fin ajout des dépendances");
            }

        }

    }

    /**
     * methode Update dependency : .
     *
     * @param dependencyElmt DOCUMENTEZ_MOI
     * @param depToDelete DOCUMENTEZ_MOI
     */
    private void updateDependency(Node dependencyElmt, String[] depToDelete)
    {
        NodeList dependencyElmts = dependencyElmt.getChildNodes();
        Node currentBalise = null;

        for (int i = 0; i < dependencyElmts.getLength(); i++)
        {
            currentBalise = dependencyElmts.item(i);
            if ("artifactId".equals(currentBalise.getNodeName()))
            {
                for (int j = 0; j < depToDelete.length; j++)
                {

                    if (depToDelete[j].equals(currentBalise.getTextContent()))
                    {
                        getLog().info("--> suppression de la dépendance vers " + depToDelete[j]);
                        dependencyElmt.getParentNode().removeChild(dependencyElmt);
                    }

                }

            }

        }
    }

    /**
     * methode Update parent lombok version : remplacement de la balise version de l'élément parent dans le pom du
     * projet.
     *
     * @param doc document pom.xml
     * @throws MojoExecutionException the mojo execution exception
     * @throws MojoFailureException the mojo failure exception
     */
    private void updateParentLombokVersion(Document doc) throws MojoExecutionException, MojoFailureException
    {
        getLog().info("Tentative de mise à jour de la balise version de l'élément parent du pom projet en version:" + versionLombokCible);

        NodeList parents = doc.getElementsByTagName("parent");
        // normalement une seule balise parent
        if (parents.getLength() == 1)
        {

            for (Node childNode = parents.item(0).getFirstChild(); childNode != null;)
            {
                Node nextChild = childNode.getNextSibling();

                if (nextChild != null && nextChild.getNodeName() == "version")
                {
                    getLog().info("-> mise à jour effective de la version lombok de " + nextChild.getTextContent() + " à la version " +
                        versionLombokCible);
                    nextChild.setTextContent(versionLombokCible);
                    childNode = null;
                }
                childNode = nextChild;
            }

        }
        else
        {
            throw new MojoFailureException("Ce projet ne déclare pas de parent Lombok. Arrêt");
        }

    }

    /**
     * methode Verification conditions mise a jour : vérification des prérequis pour la mise à jour.
     *
     * @throws MojoExecutionException the mojo execution exception
     */
    private void verificationConditionsMiseAJour() throws MojoExecutionException
    {
        getLog().info(
            "Analyse des préconditions pour la mise à jour (le parent de ce projet doit être Lombok en version 23.2.x dans son pom.");
        Artifact parentArti = project.getParentArtifact();

        if (parentArti == null)
        {
            getLog().error("Le projet ne possède pas un parent (pas de balise <parent>");
            throw new MojoExecutionException("Le projet ne possède pas un parent (pas de balise <parent>");
        }
        if (!parentArti.getGroupId().startsWith("fr.gouv.finances"))
        {
            getLog()
                .error(
                    "Le projet sur lequel est exécuté cette mise à jour Lombok n'est pas un projet dont le group id est préfixé par fr.gouv.finances");
            throw new MojoExecutionException(
                "Le projet sur lequel est exécuté cette mise à jour Lombok n'est pas un projet dont le group id est préfixé par fr.gouv.finances");
        }

        if (!parentArti.getBaseVersion().startsWith("23.2"))
        {
            getLog().error(
                "Le projet Lombok en parent doit être en version 23.2.x. Ce projet a pour parent Lombok en version:"
                    + parentArti.getBaseVersion());

            throw new MojoExecutionException(
                "Le projet Lombok en parent doit être en version 23.2.x. Ce projet a pour parent Lombok en version:"
                    + parentArti.getBaseVersion());
        }
        getLog().info("-> les prérequis sont remplis.");

    }
}
