/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.deploiement;

/*
 * Copyright 2001-2005 The Apache Software Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.Properties;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;

/**
 * Class qui prepare les dossiers et fichiers necessaires a la constitution d'un RPM d'installation manuelle
 * 
 * @goal preparationinstallationmanuelle
 */
public class PreparationInstallationManuelle extends AbstractPreparationInstallation
{

    /**
     * Constructeur de la classe PreparationInstallationManuelle.java
     */
    public PreparationInstallationManuelle()
    {
        super();

    }

    /**
     * Methode d'execution du Mojo
     * 
     * @throws MojoExecutionException
     * @throws MojoFailureException
     */
    @Override
    public void execute() throws MojoExecutionException, MojoFailureException
    {

        try
        {
            // Lecture des fichiers de surcharge

            Map<String, Map<String, Properties>> modificateursFichier = lireModificateursCibles();

            // On supprime arbitrairement le fichier "exemple"
            // qui n'est la que pour montrer un exemple de fichier de surcharge
            modificateursFichier.remove("exemple");

            Map<String, Properties> modificateursCible;
            File dossierSortie = new File(destination);
            File dossierCible;
            dossierSortie.mkdirs();

            // Pour chaque configuration a preparer...

            for (String cible : modificateursFichier.keySet())
            {

                // obtenir les elements de surcharge correspondants

                modificateursCible = modificateursFichier.get(cible);

                // ecrire les fichiers .propertie apres surcharge

                dossierCible = new File(dossierSortie, cible + DOSSIER_PROPERTIES);
                dossierCible.mkdirs();
                if (sources != null)
                {
                    for (String source : sources)
                    {
                        ecrireProprietes(source, modificateursCible, dossierCible);
                    }
                }

                // ecrire l'arborescence de tomcat et vespa

                String dossierTomcat = destination + "/" + cible + DOSSIER_TOMCAT;
                creerArborescenceTomcat(dossierTomcat, modificateursCible);
                creerArborescenceVespa(dossierTomcat, modificateursCible);

                // ecrire le script d'init Tomcat

            }

        }
        catch (IOException ioe)
        {
            throw new MojoExecutionException(ioe.getMessage(), ioe);
        }
    }

}
