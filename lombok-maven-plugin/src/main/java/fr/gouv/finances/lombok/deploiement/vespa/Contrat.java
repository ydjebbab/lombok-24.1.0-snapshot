/*
 * Copyright (c) 2015 DGFiP - Tous droits réservés
 * 
 */
package fr.gouv.finances.lombok.deploiement.vespa;

import java.util.ArrayList;
import java.util.List;

/**
 * Classe représentant un contrat dans le fichier de cache du SLPA.
 */
public class Contrat
{
    /** name. */
    private String name;

    /** services. */
    private List<Service> services;

    /**
     * Instanciation de {@link Contrat} avec son nom et aucun service.
     *
     * @param name le nom du contrat
     */
    public Contrat(String name)
    {
        this.name = name;
        this.services = new ArrayList<Service>();
    }

    /**
     * Instanciation de {@link Contrat} sans paramètres.
     */
    public Contrat()
    {

    }

    /**
     * Accesseur de name
     *
     * @return name
     */
    public String getName()
    {
        return name;
    }

    /**
     * Mutateur de name
     *
     * @param name name
     */
    public void setName(String name)
    {
        this.name = name;
    }

    /**
     * Accesseur de services
     *
     * @return services
     */
    public List<Service> getServices()
    {
        return services;
    }

    /**
     * Mutateur de services
     *
     * @param services services
     */
    public void setServices(List<Service> services)
    {
        this.services = services;
    }
}
