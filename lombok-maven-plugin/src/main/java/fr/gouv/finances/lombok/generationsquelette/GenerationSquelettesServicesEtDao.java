/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.generationsquelette;

/*
 * Copyright 2001-2005 The Apache Software Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Locale;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;

/**
 * Generation de squelettes de services/dao.
 * 
 * @goal generationsquelettesservicesetdao
 */
public class GenerationSquelettesServicesEtDao extends GenerationAbstractMojo

{

    /**
     * classNameServicesEtDao. classe concept
     * 
     * @parameter expression="${classNameServicesEtDao}"
     * @required
     */
    private String classNameServicesEtDao;

    /**
     * numZoneFonctionnelleServicesEtDao. numéro de la zone fonctionnelle
     * 
     * @parameter expression="${numZoneFonctionnelleServicesEtDao}"
     */
    private String numZoneFonctionnelleServicesEtDao;

    /** nomClassHgr - String,. */
    private String nomClassHgr;

    /** nomClassForGetSet - String, Pour generation getters et setters. */
    private String nomClassForGetSet;

    /**
     * Constructeur de la classe GenerationSquelettesServicesEtDao.java
     */
    public GenerationSquelettesServicesEtDao()
    {
        super();

    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @throws MojoExecutionException the mojo execution exception
     * @throws MojoFailureException the mojo failure exception
     * @see org.apache.maven.plugin.AbstractMojo#execute()
     */
    public void execute() throws MojoExecutionException, MojoFailureException
    {
        prefixepackage = prefixepackagefinances + "." + appli;
        racinejavasourceDir = project.getBuild().getSourceDirectory() + "/" + prefixepackagefinancesrep + "/" + appli;
        // notationhongroise . on met premier lettre en majuscule aprés on laisse comme ecrit par l'utilisateur
        // nomClassHgr = classNameServicesEtDao.toLowerCase(Locale.FRANCE);
        nomClassForGetSet = classNameServicesEtDao;
        nomClassForGetSet = Character.toUpperCase(nomClassForGetSet.charAt(0)) + nomClassForGetSet.substring(1).toLowerCase(Locale.FRANCE);
        nomClassHgr = classNameServicesEtDao;
        nomClassHgr = Character.toUpperCase(nomClassHgr.charAt(0)) + nomClassHgr.substring(1);

        if (isNomDeLaClasseOk(classNameServicesEtDao))
        {
            if ((!"".equals(numZoneFonctionnelleServicesEtDao)) && (numZoneFonctionnelleServicesEtDao != null))
            {
                prefixepackage = prefixepackage + ".zf" + numZoneFonctionnelleServicesEtDao;

                racinejavasourceDir += "/zf" + numZoneFonctionnelleServicesEtDao;
            }

            try
            {
                if (!new File(racinejavasourceDir + "/dao/").exists())
                {
                    new File(racinejavasourceDir + "/dao/").mkdirs();
                }

                filterFile("/templates/fragment/CCAtemplatedao.java.txt", racinejavasourceDir + "/dao/"
                    + nomClassHgr + "Dao.java");

                if (!new File(racinejavasourceDir + "/dao/impl/").exists())
                {
                    new File(racinejavasourceDir + "/dao/impl/").mkdirs();
                }
                filterFile("/templates/fragment/CCAtemplatedaoimpl.java.txt", racinejavasourceDir + "/dao/impl/"
                    + nomClassHgr + "DaoImpl.java");

                if (!new File(racinejavasourceDir + "/service/").exists())
                {
                    new File(racinejavasourceDir + "/service/").mkdirs();
                }
                filterFile("/templates/fragment/CCAtemplateservice.java.txt", racinejavasourceDir + "/service/"
                    + nomClassHgr + "Service.java");

                if (!new File(racinejavasourceDir + "/service/impl").exists())
                {
                    new File(racinejavasourceDir + "/service/impl").mkdirs();
                }
                filterFile("/templates/fragment/CCAtemplateserviceimpl.java.txt", racinejavasourceDir
                    + "/service/impl/" + nomClassHgr + "ServiceImpl.java");

                if (!new File(webinfConf + "/conf/application/").exists())
                {
                    new File(webinfConf + "/conf/application/").mkdirs();
                }
                filterFile("/templates/fragment/CCAFRAGMENTapplicationContext-dao.xml.txt", webinfConf
                    + "/conf/application/applicationContext-dao-fragmentacopier-" + classNameServicesEtDao.toLowerCase(Locale.FRANCE)
                    + "-cca.xml");

                filterFile("/templates/fragment/CCAFRAGMENTapplicationContext-service.xml.txt", webinfConf
                    + "/conf/application/applicationContext-service-fragmentacopier-" + nomClassHgr + "-cca.xml");

            }
            catch (FileNotFoundException exc)
            {
                throw new MojoExecutionException(exc.getMessage(), exc);
            }
            catch (IOException exc)
            {
                throw new MojoExecutionException(exc.getMessage(), exc);
            }

        }
        else
        {
            throw new MojoFailureException(
                "Le nom d'une classe doit comporter au moins 2 caracteres et doit commencer par une majuscule");
        }

    }

    /**
     * methode Filter file : .
     * 
     * @param fileNameSource
     * @param fileNameTarget
     * @throws FileNotFoundException the file not found exception
     * @throws IOException Signal qu'une execption de type I/O s'est produite.
     */
    public void filterFile(String fileNameSource, String fileNameTarget) throws FileNotFoundException, IOException
    {
        filterFile(fileNameSource, fileNameTarget, "UTF-8");
    }

    /**
     * Méthode de parsing des template et génération des squelettes des dao, service et fichiers de configs spring.
     * 
     * @param fileNameSource Nom du template
     * @param fileNameTarget chemin relatif + Nom du squelette généré
     * @param encoding
     * @throws FileNotFoundException the file not found exception
     * @throws IOException Signal qu'une execption de type I/O s'est produite.
     */

    public void filterFile(String fileNameSource, String fileNameTarget, String encoding) throws FileNotFoundException, IOException
    {
        super.filterFile(fileNameSource, fileNameTarget, encoding);
        String currentLine;
        while ((currentLine = bReader.readLine()) != null)
        {
            currentLine = currentLine.replaceAll("@PREFIXEPACKAGE@", prefixepackage);
            currentLine = currentLine.replaceAll("@CCAMINUS@", classNameServicesEtDao.toLowerCase(Locale.FRANCE));
            currentLine = currentLine.replaceAll("@CCA@", nomClassHgr);
            currentLine = currentLine.replaceAll("@CCAGETSET@", nomClassForGetSet);
            currentLine = currentLine.replaceAll("@CCAMINUSFIN@", nomClassHgr.toLowerCase(Locale.FRANCE));
            bWriter.append(currentLine);
            bWriter.newLine();
        }
        bWriter.close();
        bReader.close();

    }

}
