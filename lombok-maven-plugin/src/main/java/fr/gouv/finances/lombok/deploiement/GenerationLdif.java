/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.deploiement;

/*
 * Copyright 2001-2005 The Apache Software Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;

import edu.emory.mathcs.backport.java.util.Arrays;
import fr.gouv.finances.lombok.maven.utils.LinkedProperties;

/**
 * Classe de génération d'un LDIF des fichiers de configuration pour puppet.
 * 
 * @goal generationldif
 */
public class GenerationLdif extends GenerationConfiguration

{

    /** The Constant dnFichier. */
    private static final String dnFichier =
        "dn: file=conf/#instanceweb#/#fichiercourt#,service=#instanceweb#,configuration=#conf#,module=#module#,application=#app#,ou=deploiement,ou=dgfip,ou=mefi,o=gouv,c=fr\n";

    /**
     * The template entete.
     * 
     * @parameter expression = "@basedir@/integration/template/entete.ldif.tpt"
     */
    private String templateEntete;

    /**
     * The output script.
     * 
     * @parameter expression = "${project.build.directory}/${package.appli}/div/puppet/puppetAppliICE.ldif"
     */
    private String outputScript;

    /**
     * Numero de version.
     * 
     * @parameter expression = "${rpm.version}"
     */
    private String rpmversion;

    /**
     * Numero de release.
     * 
     * @parameter expression = "${rpm.release}"
     */
    private String rpmrelease;

    /**
     * Build destine ou non a amender la configuration applicative
     * 
     * @parameter default-value = "false"
     */
    private boolean amendconfig;

    /**
     * Build utilisant la release de reference du pacquet d'exploitation
     * 
     * @parameter default-value = "false"
     */
    private boolean useoldexploit;

    /** template pattern. */
    private final Pattern templatePattern = Pattern.compile("^([^\\|]+)\\|(.*)");

    /**
     * Dossier contenant les fichiers de configuration.
     * 
     * @parameter expression = "@basedir@/integration/cibles"
     */
    protected String cibles;

    /**
     * Constructeur de la classe GenerationLdif.java
     */
    public GenerationLdif()
    {
        super();

    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @throws MojoExecutionException the mojo execution exception
     * @throws MojoFailureException the mojo failure exception
     * @see org.apache.maven.plugin.AbstractMojo#execute()
     */
    @SuppressWarnings("unchecked")
    public void execute() throws MojoExecutionException, MojoFailureException
    {
        try
        {
            // Génération du LDIF pour Puppet
            Map<String, String> templateParts = extractTemplateParts();
            Map<String, Map<String, Properties>> modificateursCibles = lireModificateursCibles();

            File fichierSortie = new File(outputScript);
            fichierSortie.getParentFile().mkdirs();

            try (Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fichierSortie), "UTF-8")))
            {
                ajouterEntete(writer, templateParts);
                Properties generalSettings = new Properties();
                Properties vespaSettings = null;
                List<String> modulesSupp = new ArrayList<>();
                if (extras != null)
                {
                    modulesSupp = Arrays.asList(extras.split(","));
                }
                for (Map.Entry<String, Map<String, Properties>> cible : modificateursCibles.entrySet())
                {
                    generalSettings = cible.getValue().remove(CLE_PARAMETRES_GENERAUX);
                    vespaSettings = cible.getValue().remove(CLE_PARAMETRES_VESPA);
    
                    ajouterConfiguration(writer, templateParts, cible.getKey(), generalSettings);
                    ecrireConfigurationVespa(vespaSettings, writer);
                    writer.append('\n');
    
                    for (String source : sources)
                    {
                        ecrireProprietes(source, cible.getValue(), cible.getKey(), writer);
                    }
                }
                if (modulesSupp.contains("itm") || modulesSupp.contains("ITM"))
                {
                    ajouterConfigurationSupp(writer, templateParts, "itm", generalSettings);
                }
                ajouterPiedDePage(writer);
            }
        }
        catch (IOException ioe)
        {
            throw new MojoExecutionException(ioe.getMessage(), ioe);
        }
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.deploiement.GenerationConfiguration#getCibles()
     */
    protected String getCibles()
    {
        return cibles;
    }

    /**
     * Ajouter configuration supplementaire, comme celle dediee a ITM.
     * 
     * @param writer the writer
     * @param templateParts
     * @param nomConfiguration
     * @throws IOException the IO exception
     * @throws MojoFailureException 
     */
    private void ajouterConfigurationSupp(Writer writer, Map<String, String> templateParts, String nomConfiguration,
        Properties generalSettings)
        throws IOException, MojoFailureException
    {
        Properties defaults = new Properties();
        defaults.setProperty("appli", appli);
        defaults.setProperty("module", module);
        defaults.setProperty("base", base);
        defaults.setProperty("instanceweb", instanceweb);
        defaults.setProperty("webapp", webapp);
        defaults.setProperty("conf", nomConfiguration);

        String result = templateParts.get(nomConfiguration);
        
        if (result == null)
        {
            throw new MojoFailureException("Le module de configuration \"" + nomConfiguration + "\" n'existe pas dans votre modèle LDIF.");
        }

        Set<String> clesProperties = new HashSet<String>();
        clesProperties.addAll(defaults.stringPropertyNames());
        clesProperties.addAll(generalSettings.stringPropertyNames());

        for (String cle : clesProperties)
        {
            result = StringUtils.replace(result, "#" + cle + "#", generalSettings.getProperty(cle, defaults.getProperty(cle)));
        }
        writer.write(result);
        
    }
    
    /**
     * Ajouter configuration.
     * 
     * @param writer the writer
     * @param templateParts
     * @param nomConfiguration
     * @param generalSettings
     * @throws IOException the IO exception
     */
    private void ajouterConfiguration(Writer writer, Map<String, String> templateParts, String nomConfiguration, Properties generalSettings)
        throws IOException
    {
        String release_appli = rpmrelease;
        if (amendconfig)
        {
            release_appli = "1";
        }

        String release_exploit = rpmrelease;
        if (useoldexploit)
        {
            release_exploit = "1";
        }

        Properties defaults = new Properties();
        defaults.setProperty("appli", appli);
        defaults.setProperty("module", module);
        defaults.setProperty("base", base);
        defaults.setProperty("instanceweb", instanceweb);
        defaults.setProperty("webapp", webapp);
        defaults.setProperty("conf", nomConfiguration);
        defaults.setProperty("tomcatport", "$" + webapp + "_tomcat_port");
        defaults.setProperty("tomcatmode", MODE_TOMCAT_DEFAUT);
        defaults.setProperty("runlevel", RUNLEVEL_TOMCAT_DEFAUT);
        defaults.setProperty("tomcatmemory", MEMORY_TOMCAT_DEFAUT);
        defaults.setProperty("tomcatpermgen", PERMGEN_TOMCAT_DEFAUT);
        defaults.setProperty("tomcatopts", OPTS_TOMCAT_DEFAUT);
        defaults.setProperty("version_appli", rpmversion + "-" + release_appli);
        defaults.setProperty("version_exploit", rpmversion + "-" + release_exploit);

        String result = templateParts.get("conf");
        String result_tomcat = templateParts.get("conf_tomcat");

        Set<String> clesProperties = new HashSet<String>();
        clesProperties.addAll(defaults.stringPropertyNames());
        clesProperties.addAll(generalSettings.stringPropertyNames());

        for (String cle : clesProperties)
        {
            result = StringUtils.replace(result, "#" + cle + "#", generalSettings.getProperty(cle, defaults.getProperty(cle)));
            result_tomcat =
                StringUtils.replace(result_tomcat, "#" + cle + "#", generalSettings.getProperty(cle, defaults.getProperty(cle)));
        }

        writer.write(result);
        writer.write(result_tomcat);

    }

    /**
     * Ajouter entete.
     * 
     * @param writer the writer
     * @param templateParts
     * @throws IOException the IO exception
     */
    private void ajouterEntete(Writer writer, Map<String, String> templateParts) throws IOException
    {
        String release_appli = rpmrelease;
        if (amendconfig)
        {
            release_appli = "1";
        }

        String release_exploit = rpmrelease;
        if (useoldexploit)
        {
            release_exploit = "1";
        }

        writer.write(templateParts.get("app").replaceAll("#appli#", appli).replaceAll("#base#", base));
        writer.write(templateParts.get("module").replaceAll("#appli#", appli).replaceAll("#base#", base)
            .replaceAll("#module#", module)
            .replaceAll("#version_appli#", rpmversion + "-" + release_appli)
            .replaceAll("#version_exploit#", rpmversion + "-" + release_exploit));
    }

    /**
     * Ajouter pied de page.
     * 
     * @param writer the writer
     * @throws IOException the IO exception
     */
    private void ajouterPiedDePage(Writer writer) throws IOException
    {
    }

    /**
     * Genere la partie Vespa du LDIF
     * 
     * @param nomFichier the nom fichier
     * @param modificateurs
     * @param nomConfiguration
     * @param writer the writer
     * @throws IOException the IO exception
     */
    private void ecrireConfigurationVespa(Properties confVespa, Writer writer)
        throws IOException
    {
        if ((confVespa == null) || !("false".equalsIgnoreCase(confVespa.getProperty(CLE_BOUCHON_VESPA))))
        {
            writer.append("tomcatVespaEnabled: false\n");
            return;
        }

        writer.append("tomcatVespaEnabled: true\n");

        String servicesString = confVespa.getProperty(CLE_SERVICES_VESPA);
        if ((servicesString != null) && !("".equals(servicesString)))
        {
            String[] services = servicesString.split(",");
            for (String service : services)
            {
                writer.append("tomcatVespaServices: " + service.replaceAll("\\|", ",") + "\n");
            }
        }

    }

    /**
     * Ecrire proprietes.
     * 
     * @param nomFichier the nom fichier
     * @param modificateurs
     * @param nomConfiguration
     * @param writer the writer
     * @throws IOException the IO exception
     */
    private void ecrireProprietes(String nomFichier, Map<String, Properties> modificateurs, String nomConfiguration, Writer writer)
        throws IOException
    {
        File fichier = new File(nomFichier);
        String nomFichierCourt = fichier.getName();
        Properties modificateursFichier = modificateurs.get(nomFichierCourt);

        if (fichier.exists())
        {
            LinkedProperties proprietes = null;
            if ("localisations.properties".equals(nomFichierCourt))
            {
                proprietes = creerLocalisations();
            }
            else
            {
                proprietes = lireFichierSource(nomFichier);
            }
            if (modificateursFichier != null)
            {
                proprietes.putAll(modificateursFichier);
            }
            String adn =
                dnFichier.replaceAll("#app#", appli).replaceAll("#module#", module).replaceAll("#fichiercourt#", nomFichierCourt)
                    .replaceAll("#conf#", nomConfiguration).replaceAll("#instanceweb#", instanceweb);
            writer.append(adn);
            writer.append("objectClass: top\n");
            writer.append("objectClass: puppetMarker\n");
            writer.append("objectClass: systemFile\n");
            writer.append("objectClass: propertiesFile\n");
            writer.append("file: conf/" + instanceweb + "/" + nomFichierCourt + "\n");
            writer.append("fileGroup: " + appli + "\n");
            writer.append("fileModes: 0640\n");
            writer.append("fileOwner: " + appli + "\n");
            for (Map.Entry<Object, Object> entry : proprietes.entrySet())
            {
                if ("\n".equals(entry.getValue()))
                {
                    writer.append("parameter: " + entry.getKey() + ",\\n\n");
                }
                else
                {
                    writer.append("parameter: " + entry.getKey() + "," + entry.getValue() + "\n");
                }
            }
            writer.append("\n");
        }
    }

    /**
     * Extrait les différentes sections du template et les place dans une Map selon leur clé.
     * 
     * @return une Map contenant les différentes sections du template
     * @throws IOException Signal qu'une exception de type I/O s'est produite.
     */
    private Map<String, String> extractTemplateParts() throws IOException
    {
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(new File(templateEntete)))))
        {
            Map<String, StringBuilder> builders = new HashMap<String, StringBuilder>();
            String templateLine = reader.readLine();
            Matcher lineMatcher = null;
            StringBuilder currentLineBuilder = null;
            String currentSection = null;
            String currentTemplateCode = null;
    
            while (templateLine != null)
            {
                lineMatcher = templatePattern.matcher(templateLine);
                if (lineMatcher.matches())
                {
                    currentSection = lineMatcher.group(1);
                    currentTemplateCode = lineMatcher.group(2);
                    currentLineBuilder = builders.get(currentSection);
                    if (currentLineBuilder == null)
                    {
                        currentLineBuilder = new StringBuilder(256);
                        builders.put(currentSection, currentLineBuilder);
                    }
                    currentLineBuilder.append(currentTemplateCode);
                    currentLineBuilder.append("\n");
                }
                templateLine = reader.readLine();
            }
    
            // transformation de la map de stringbuilders en map de strings
    
            Map<String, String> result = new HashMap<String, String>();
            for (Map.Entry<String, StringBuilder> entry : builders.entrySet())
            {
                result.put(entry.getKey(), entry.getValue().toString());
            }
    
            return result;
        }
    }
}
