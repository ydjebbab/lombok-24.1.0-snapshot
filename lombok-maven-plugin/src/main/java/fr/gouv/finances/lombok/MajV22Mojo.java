/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok;

import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Ajoute les dépendances de lombok-pb-parent dans le pom Supprime le parent lombok-pb-parent Déprécié par E. Demorsy
 * (code non finalisé)
 * 
 * @goal majV22
 */
@Deprecated
public class MajV22Mojo extends SupprimerPbParentMojo
{

    /** Constant : LOMBOK_STRUCTURE_XPATH. */
    protected static final String LOMBOK_STRUCTURE_XPATH =
        "dependency[groupId[.='fr.gouv.finances.lombok'] and artifactId[.='lombok-structure']]";

    /** Constant : PROJECT_DEPENDENCIES. */
    protected static final String PROJECT_DEPENDENCIES = "/project/dependencies";

    /**
     * Constructeur de la classe MajV22Mojo.java
     */
    public MajV22Mojo()
    {
        super();

    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.SupprimerPbParentMojo#processDocument(org.w3c.dom.Document)
     */
    @Override
    protected void processDocument(Document document)
        throws MojoExecutionException, MojoFailureException, XPathExpressionException
    {
        super.processDocument(document);
        replaceLombokDependencies(document);
    }

    /**
     * methode Adds the dependency : .
     * 
     * @param groupId
     * @param artifactId
     * @param version
     * @param node
     */
    private void addDependency(String groupId, String artifactId, String version, Node node)
    {
        Node dependency = node.getOwnerDocument().createElement(DEPENDENCY);
        addSimpleChild(GROUP_ID, groupId, dependency);
        addSimpleChild(ARTIFACT_ID, artifactId, dependency);
        addSimpleChild(VERSION, version, dependency);
        node.appendChild(dependency);
    }

    /**
     * methode Adds the lombok maven dependencies : .
     * 
     * @param projectElement
     * @throws XPathExpressionException the x path expression exception
     */
    private void addLombokMavenDependencies(Node projectElement) throws XPathExpressionException
    {
        Node propertiesElement = (Node) XPATH.evaluate("properties", projectElement, XPathConstants.NODE);
        if (propertiesElement == null)
        {
            Document document = projectElement.getOwnerDocument();
            propertiesElement = document.createElement("properties");
            projectElement.appendChild(propertiesElement);
        }
        addSimpleChild("lombok.version", "0.1.5-SNAPSHOT", propertiesElement);
        Node dependenciesElement = (Node) XPATH.evaluate("dependencies", projectElement, XPathConstants.NODE);
        addDependency("fr.gouv.finances.lombok", "lombok-core", "${lombok.version}", dependenciesElement);
        addDependency("fr.gouv.finances.lombok", "lombok-journal", "${lombok.version}", dependenciesElement);
    }

    /**
     * methode Exclude from dependency : .
     * 
     * @param groupId
     * @param artifactId
     * @param node
     * @throws XPathExpressionException the x path expression exception
     */
    private void excludeFromDependency(final String groupId, final String artifactId, Node node) throws XPathExpressionException
    {
        Node exclusionsElement = (Node) XPATH.evaluate(EXCLUSIONS, node, XPathConstants.NODE);
        Document doc = node.getOwnerDocument();
        if (exclusionsElement == null)
        {
            exclusionsElement = doc.createElement(EXCLUSIONS);
            node.appendChild(exclusionsElement);
        }
        Element exclusionElement = doc.createElement(EXCLUSION);
        addSimpleChild(GROUP_ID, groupId, exclusionElement);
        addSimpleChild(ARTIFACT_ID, artifactId, exclusionElement);
        exclusionsElement.appendChild(exclusionElement);
    }

    /**
     * methode Exclude lombok from structure : .
     * 
     * @param dependenciesElement
     * @throws XPathExpressionException the x path expression exception
     */
    private void excludeLombokFromStructure(Node dependenciesElement) throws XPathExpressionException
    {
        Node lombokStructureElement = (Node) XPATH.evaluate(LOMBOK_STRUCTURE_XPATH, dependenciesElement, XPathConstants.NODE);
        excludeFromDependency("fr.gouv.finances.lombok", "lombok-framework", lombokStructureElement);
        excludeFromDependency("fr.gouv.finances.lombok", "lombok-ectable", lombokStructureElement);
    }

    /**
     * methode Removes the lombok ectable dependencies : .
     * 
     * @param dependenciesElement
     * @throws XPathExpressionException the x path expression exception
     */
    private void removeLombokEctableDependencies(Node dependenciesElement) throws XPathExpressionException
    {
        NodeList lombokEcTableElement =
            (NodeList) XPATH.evaluate("dependency[groupId[.='fr.gouv.finances.lombok'] and artifactId[.='lombok-ectable']]",
                dependenciesElement, XPathConstants.NODESET);
        for (int i = 0; i < lombokEcTableElement.getLength(); i++)
        {
            dependenciesElement.removeChild(lombokEcTableElement.item(i));
        }
    }

    /**
     * methode Removes the lombok framework dependencies : .
     * 
     * @param dependenciesElement
     * @throws XPathExpressionException the x path expression exception
     */
    private void removeLombokFrameworkDependencies(Node dependenciesElement) throws XPathExpressionException
    {
        NodeList lombokFrameworkElement =
            (NodeList) XPATH.evaluate("dependency[groupId[.='fr.gouv.finances.lombok'] and artifactId[.='lombok-framework']]",
                dependenciesElement, XPathConstants.NODESET);
        for (int i = 0; i < lombokFrameworkElement.getLength(); i++)
        {
            dependenciesElement.removeChild(lombokFrameworkElement.item(i));
        }
    }

    /**
     * methode Replace lombok dependencies : .
     * 
     * @param document
     * @throws XPathExpressionException the x path expression exception
     */
    private void replaceLombokDependencies(Document document) throws XPathExpressionException
    {
        Node projectElement = (Node) XPATH.evaluate(PROJECT_XPATH, document, XPathConstants.NODE);
        Node dependenciesElement = (Node) XPATH.evaluate(PROJECT_DEPENDENCIES, document, XPathConstants.NODE);
        removeLombokFrameworkDependencies(dependenciesElement);
        removeLombokEctableDependencies(dependenciesElement);
        excludeLombokFromStructure(dependenciesElement);
        addLombokMavenDependencies(projectElement);

    }

}
