/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.generationsquelette;

/*
 * Copyright 2001-2005 The Apache Software Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Locale;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;

/**
 * Class GenerationSquelettesEditions .
 * 
 * @goal generationsqueletteseditions
 */
public class GenerationSquelettesEditions extends GenerationAbstractMojo

{

    /**
     * classNameEditions. nom du service (classe concept )associé à l'édition
     * 
     * @parameter expression="${classNameEditions}"
     * @required
     */
    private String classNameEditions;

    /**
     * numZoneFonctionnelleEditions. numéro de la zone fonctionnelle de l'édistion
     * 
     * @parameter expression="${numZoneFonctionnelleEditions}" default-value=""
     */
    private String numZoneFonctionnelleEditions;

    /**
     * numzfccaEditions.
     * 
     * @parameter expression="${numzfccaEditions}" default-value=""
     */
    private String numzfccaEditions;

    /**
     * useCaseNameEditions - nom du cas d'utilisation d'éditions, nom de l'édition.
     * 
     * @parameter expression="${useCaseNameEditions}"
     * @required
     */
    private String useCaseNameEditions;

    /** nomClassHgr - String,. */
    private String nomClassHgr;

    /** zfpoint - String,. */
    private String zfpoint = "/";

    /** zfbarres - String,. */
    private String zfbarres = "/";

    /** zftiret - String,. */
    private String zftiret = "";

    /** nomClassForGetSet - String, Pour generation getters et setters. */
    private String nomClassForGetSet;

    /**
     * Constructeur de la classe GenerationSquelettesEditions.java
     */
    public GenerationSquelettesEditions()
    {
        super();

    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see org.apache.maven.plugin.AbstractMojo#execute()
     */
    public void execute() throws MojoExecutionException, MojoFailureException
    {
        prefixepackage = prefixepackagefinances + "." + appli;
        prefixepackagecca = prefixepackage;
        racinejavasourceDir = project.getBuild().getSourceDirectory() + "/" + prefixepackagefinancesrep + "/" + appli;
        nomClassForGetSet = classNameEditions;
        nomClassForGetSet = Character.toUpperCase(classNameEditions.charAt(0)) + classNameEditions.substring(1).toLowerCase(Locale.FRANCE);
        // nomClassHgr = classNameEditions.toLowerCase(Locale.FRANCE);
        nomClassHgr = classNameEditions;
        nomClassHgr = Character.toUpperCase(nomClassHgr.charAt(0)) + nomClassHgr.substring(1);
        if (isNomDeLaClasseOk(nomClassHgr))
        {
            if ((!"".equals(numZoneFonctionnelleEditions)) && (numZoneFonctionnelleEditions != null))
            {
                prefixepackage = prefixepackage + ".zf" + numZoneFonctionnelleEditions;

                racinejavasourceDir += "/zf" + numZoneFonctionnelleEditions;

                zfpoint += "zf" + numZoneFonctionnelleEditions + ".";
                zfbarres = "/zf" + numZoneFonctionnelleEditions + "/";
                zftiret = "-zf" + numZoneFonctionnelleEditions;
            }

            if ((numzfccaEditions != null) && (!"".equals(numzfccaEditions)))
            {
                prefixepackagecca += ".zf" + numzfccaEditions;
            }

            try
            {
                if (!new File(racinejavasourceDir + "/edition/").exists())
                {
                    new File(racinejavasourceDir + "/edition/").mkdirs();
                }
                filterFile("/templates/fragment/CUEtemplatetraitementeditionsimpl.java.txt", racinejavasourceDir
                    + "/edition/TraitementEditions" + useCaseNameEditions + "Impl.java");

                filterFile("/templates/fragment/CUEtemplatetraitementeditionsstub.java.txt", racinejavasourceDir
                    + "/edition/TraitementEditions" + useCaseNameEditions + "Stub.java");

                if (!new File(webinfDir + "/properties/").exists())
                {
                    new File(webinfDir + "/properties/").mkdirs();
                }
                filterFile("/templates/fragment/CUEFRAGMENTjasper.properties.txt", webinfDir
                    + "/properties/jasper-fragmentacopier-" + useCaseNameEditions.toLowerCase(Locale.FRANCE) + "-cue.properties",
                    "ISO-8859-1");

                if (!new File(webinfConf + "/conf/application/").exists())
                {
                    new File(webinfConf + "/conf/application/").mkdirs();
                }
                filterFile("/templates/fragment/CUEFRAGMENTapplicationContext-edition.xml.txt", webinfConf
                    + "/conf/application/applicationContext-edition" + zftiret + "-fragmentacopier-"
                    + useCaseNameEditions.toLowerCase(Locale.FRANCE) + "-cue.xml");

            }
            catch (FileNotFoundException exc)
            {
                throw new MojoExecutionException(exc.getMessage(), exc);
            }
            catch (IOException exc)
            {
                throw new MojoExecutionException(exc.getMessage(), exc);
            }

        }
        else
        {
            throw new MojoFailureException(
                "Le nom d'une classe doit comporter au moins 2 caracteres et doit commencer par une majuscule");
        }

    }

    /**
     * methode Filter file : .
     * 
     * @param fileNameSource
     * @param fileNameTarget
     * @throws FileNotFoundException the file not found exception
     * @throws IOException Signal qu'une execption de type I/O s'est produite.
     */
    public void filterFile(String fileNameSource, String fileNameTarget) throws FileNotFoundException, IOException
    {
        filterFile(fileNameSource, fileNameTarget, "UTF-8");
    }

    /**
     * Méthode de parsing des template et génération des squelettes des dao, service et fichiers de configs spring.
     * 
     * @param fileNameSource Nom du template
     * @param fileNameTarget chemin relatif + Nom du squelette généré
     * @param encoding
     * @throws FileNotFoundException the file not found exception
     * @throws IOException Signal qu'une execption de type I/O s'est produite.
     */

    public void filterFile(String fileNameSource, String fileNameTarget, String encoding) throws FileNotFoundException, IOException
    {

        super.filterFile(fileNameSource, fileNameTarget, encoding);
        String currentLine;
        while ((currentLine = bReader.readLine()) != null)
        {
            currentLine = currentLine.replaceAll("@PREFIXEPACKAGE@", prefixepackage);
            currentLine = currentLine.replaceAll("@CUE@", useCaseNameEditions);
            currentLine = currentLine.replaceAll("@CCAMINUS@", classNameEditions.toLowerCase(Locale.FRANCE));
            currentLine = currentLine.replaceAll("@CCA@", nomClassHgr);
            currentLine = currentLine.replace("@CUEMINUS@", useCaseNameEditions.toLowerCase(Locale.FRANCE));
            currentLine = currentLine.replaceAll("@CCAMINUSFIN@", nomClassHgr);
            currentLine = currentLine.replaceAll("@ZFPOINT@", zfpoint);
            currentLine = currentLine.replaceAll("@ZFBARRES@", zfbarres);
            currentLine = currentLine.replaceAll("@ZFTIRET@", zftiret);
            currentLine = currentLine.replaceAll("@PREFIXEPACKAGECCA@", prefixepackagecca);
            currentLine = currentLine.replaceAll("@CCAGETSET@", nomClassForGetSet);
            bWriter.append(currentLine);
            bWriter.newLine();
        }
        bWriter.close();
        bReader.close();

    }

}
