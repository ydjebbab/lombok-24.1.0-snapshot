/*
 * Copyright (c) 2016 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.maven.utils;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Collection;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.filefilter.IOFileFilter;
import org.apache.commons.io.filefilter.TrueFileFilter;
import org.apache.commons.lang.StringUtils;
import org.apache.maven.plugin.MojoExecutionException;

/**
 * Class Utils
 */
public class Utils
{

    /**
     * methode Cp :
     *
     * @param src
     * @param dst
     * @throws IOException Signal qu'une execption de type I/O s'est produite.
     */
    public static void cp(final String src, final String dst) throws IOException
    {
        cp(src, TrueFileFilter.INSTANCE, dst);
    }

    /**
     * methode Cp :
     *
     * @param src
     * @param filter
     * @param dst
     * @throws IOException Signal qu'une execption de type I/O s'est produite.
     */
    public static void cp(final String src, FileFilter filter, final String dst) throws IOException
    {
        File srcFile = new File(src);
        File dstFile = new File(dst);
        if (srcFile.isDirectory())
        {
            FileUtils.copyDirectory(srcFile, dstFile, filter);
        }
        else
        {
            FileUtils.copyFile(srcFile, dstFile);
        }
    }

    /**
     * methode Extract from jar :
     *
     * @throws IOException Signal qu'une execption de type I/O s'est produite.
     * @throws MojoExecutionException the mojo execution exception
     */
    public static void extractFromJar() throws IOException, MojoExecutionException
    {
        extractPathFromJar("");
    }

    /**
     * methode Extract path from jar :
     *
     * @param path
     * @throws IOException Signal qu'une execption de type I/O s'est produite.
     * @throws MojoExecutionException the mojo execution exception
     */
    public static void extractPathFromJar(String path) throws IOException, MojoExecutionException
    {
        File file;
        try
        {
            file = new File(Utils.class.getProtectionDomain().getCodeSource().getLocation().toURI());
        }
        catch (URISyntaxException e1)
        {
            throw new MojoExecutionException("Jar invalide", e1);
        }
        String jarpath = file.getAbsolutePath();

        Process ps = null;
        ps = new ProcessBuilder("jar", "-xf", jarpath, path).start();
        String stderr = IOUtils.toString(ps.getErrorStream());
        String stdout = IOUtils.toString(ps.getInputStream()); // Ne pas supprimer pour attendre la fin de l'execution
        if (!StringUtils.isEmpty(stderr))
        {
            throw new IOException(stderr);
        }
    }

    /**
     * methode Replace in file :
     *
     * @param path
     * @param pattern
     * @param replaceString
     * @throws IOException Signal qu'une execption de type I/O s'est produite.
     */
    public static void replaceInFile(final String path, final String pattern, final String replaceString) throws IOException
    {
        replaceInFile(new File(path), pattern, replaceString);
    }

    /**
     * methode Replace in file :
     *
     * @param file
     * @param pattern
     * @param replaceString
     * @throws IOException Signal qu'une execption de type I/O s'est produite.
     */
    public static void replaceInFile(final File file, final String pattern, final String replaceString) throws IOException
    {
        String encoding = "UTF-8";
        if (file.getName().endsWith(".properties"))
            encoding = "ISO-8859-1";
        String content = FileUtils.readFileToString(file, encoding);
        content = content.replaceAll(pattern, replaceString);
        FileUtils.write(file, content, encoding);
    }

    /**
     * methode Replace in dir :
     *
     * @param path
     * @param pattern
     * @param replaceString
     * @throws IOException Signal qu'une execption de type I/O s'est produite.
     */
    public static void replaceInDir(final String path, final String pattern, final String replaceString) throws IOException
    {
        replaceInDir(path, TrueFileFilter.INSTANCE, pattern, replaceString);
    }

    /**
     * methode Replace in dir :
     *
     * @param path
     * @param filter
     * @param pattern
     * @param replaceString
     * @throws IOException Signal qu'une execption de type I/O s'est produite.
     */
    public static void replaceInDir(String path, IOFileFilter filter, String pattern, String replaceString) throws IOException
    {
        Collection<File> files = FileUtils.listFiles(new File(path), filter, TrueFileFilter.INSTANCE);
        for (File file : files)
        {
            replaceInFile(file, pattern, replaceString);
        }
    }
}
