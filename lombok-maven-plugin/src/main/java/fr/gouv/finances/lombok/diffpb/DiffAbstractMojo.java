/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.diffpb;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.project.MavenProject;

/**
 * Class DiffAbstractMojo .
 */
@Deprecated
public abstract class DiffAbstractMojo extends AbstractMojo
{

    /**
     * Nom du project eclipse du projet blanc.
     * 
     * @parameter expression="${pb.appli}" @required
     */
    protected String pbappli;

    /**
     * Projet démo.
     * 
     * @parameter expression="${project}"
     */
    protected MavenProject project;

    /**
     * Localisation du dossier webinf.
     * 
     * @parameter expression="${webinf.dir}" @required
     */
    protected String webinfDir;

    /** pathdmo. */
    protected String pathdmo;

    /** chemin du projet blanc. */
    protected String pathpb;

    /** file reader. */
    protected InputStreamReader fileReader;

    /** b reader. */
    protected BufferedReader bReader;

    /** fstream. */
    protected OutputStreamWriter fstream;

    /** b writer. */
    protected BufferedWriter bWriter;

    /**
     * Constructeur de la classe DiffAbstractMojo.java
     */
    public DiffAbstractMojo()
    {
        super();

    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see org.apache.maven.plugin.AbstractMojo#execute()
     */
    public void execute() throws MojoExecutionException, MojoFailureException
    {
        pathdmo = project.getBasedir().getAbsolutePath();
    }

    /**
     * Méthode permettant d'intialiser le BufferedReader de la source et le BufferedWriter de la cible.
     * 
     * @param fileNameSource
     * @param fileNameTarget
     * @throws FileNotFoundException the file not found exception
     * @throws IOException Signal qu'une execption de type I/O s'est produite.
     */
    public void initRW(String fileNameSource, String fileNameTarget) throws FileNotFoundException, IOException
    {

        fileReader = new InputStreamReader(new FileInputStream(fileNameSource), "UTF-8");
        bReader = new BufferedReader(fileReader);

        fstream = new OutputStreamWriter(new FileOutputStream(fileNameTarget), "UTF-8");
        bWriter = new BufferedWriter(fstream);

    }

    // Obsolète depuis la V23 ?
    /**
     * Méthode permettant d'extraire le chemin du projet blanc via le chemin de l'appli démo.
     * 
     * @param value le nouveau pb path
     */
    public void setPbPath(String value)
    {

        if (value != null)
        {
            pathpb = value;
        }
        else
        {
            pathpb = pbappli;
        }
        // Obsolète en V23
        // pathpb = pathdmo.substring(0, pathdmo.indexOf(project.getBasedir().getName()));
        // pathpb += pbappli;
        // if (pathdmo.indexOf(webmodule) != -1)
        // {
        //
        // int ind1 = pathdmo.indexOf(project.getBasedir().getName()) + project.getBasedir().getName().length();
        // int ind2 = pathdmo.lastIndexOf(webmodule);
        // pathpb += pathdmo.substring(ind1, ind2);
        // pathpb += pbwebmodule;
        // int ind3 = pathdmo.indexOf(webmodule) + webmodule.length();
        // int ind4 = pathdmo.length();
        // pathpb += pathdmo.substring(ind3, ind4);
        //
        // }
        // else
        // {
        // int ind5 = pathdmo.indexOf(project.getBasedir().getName()) + project.getBasedir().getName().length();
        // int ind6 = (pathdmo.length());
        // pathpb += pathdmo.substring(ind5, ind6);
        // }
    }

}
