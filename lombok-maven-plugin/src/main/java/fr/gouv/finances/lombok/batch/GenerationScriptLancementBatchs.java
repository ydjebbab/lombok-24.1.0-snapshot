package fr.gouv.finances.lombok.batch;

/*
 * Copyright 2001-2005 The Apache Software Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;

/**
 * @goal generationscriptlancementbatch
 */
public class GenerationScriptLancementBatchs extends GenerationAbstractMojo

{

    /**
     * @parameter
     */
    private String batchList;

    /**
     * @parameter expression = ${basedir}/integration/template/batch.build.tpt
     */
    private String cheminTemplate;

    /**
     * @parameter expression = "${project.build.directory}"
     */
    private File outputDirectory;

    // /**
    // * @parameter
    // * @required
    // */
    // private String appli;
    //
    /**
     * @parameter default-value ="fr.gouv.finances.lombok.batch"
     */
    private String packageName;

    //
    //
    //
    //
    //
    // /**
    // * @parameter
    // * @required
    // */
    // private String libDir;
    //
    //
    // /**
    // * @parameter
    // * @required
    // */
    // private String classDir;

    public void execute() throws MojoExecutionException, MojoFailureException
    {

        if (batchList != null && batchList != "")
        {
            File outputDivBatch = new File(outputDirectory.getPath() + "/div/batch");
            // si le repertoire build/div/batch n'existe pas on le crée

            if (!outputDivBatch.exists())
            {
                try
                {
                    // SecurityManager sm = new SecurityManager();
                    // sm.checkWrite(outputDirectory.getPath() + "/div/batch");
                    boolean test = outputDivBatch.mkdirs();
                    getLog().info("test " + test);
                }
                catch (SecurityException e)
                {
                    e.printStackTrace();
                }
            }
            for (String nomBatch : batchList.split(","))
            {
                try
                {
                    filterFile(cheminTemplate, outputDirectory.getPath() + "/div/batch/" + nomBatch + "_lancer.sh",
                        nomBatch);
                }
                catch (FileNotFoundException e)
                {
                    throw new MojoExecutionException(e.getMessage());
                }
                catch (IOException e)
                {
                    throw new MojoExecutionException(e.getMessage());
                }
            }
        }
    }

    /**
     * Méthode de parsing des template et génération des lanceurs de batchs
     * 
     * @param fileNameSource Nom du template
     * @param fileNameTarget chemin relatif + Nom du squelette généré
     * @throws FileNotFoundException
     * @throws IOException
     */

    public void filterFile(String fileNameSource, String fileNameTarget, String nomBatch) throws FileNotFoundException,
        IOException
    {

        super.filterFile(fileNameSource, fileNameTarget);
        String currentLine;
        while ((currentLine = bReader.readLine()) != null)
        {
            currentLine = currentLine.replaceAll("@nombatch@", nomBatch);
            currentLine = currentLine.replaceAll("@beanid@", nomBatch);
            currentLine = currentLine.replaceAll("@batch.package.name@", packageName);
            bWriter.append(currentLine);
            bWriter.newLine();

        }
        bWriter.close();
        bReader.close();
        getLog().info("Le script " + nomBatch + "_lancer.sh a été créé dans le répertoire " + fileNameTarget);

    }
}
