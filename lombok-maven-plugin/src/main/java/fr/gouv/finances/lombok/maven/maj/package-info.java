/*
 * Copyright (c) 2017 DGFiP - Tous droits réservés
 * 
*/
// Projet : lombok-maven-plugin
/**
 * Documentation du paquet fr.gouv.finances.lombok.maven.maj
 * @author chouard
 * @version 1.0
 */
//@javax.xml.bind.annotation.XmlSchema(namespace="http://lombok.dgfip",elementFormDefault=javax.xml.bind.annotation.XmlNsForm.QUALIFIED)
package fr.gouv.finances.lombok.maven.maj;