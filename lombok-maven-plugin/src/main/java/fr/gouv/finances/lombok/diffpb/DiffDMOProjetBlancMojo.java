/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.diffpb;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Goal qui permet de voir le diff entre l'application démo et le projet blanc.
 * 
 * @goal diffdmoprojetblanc
 */
public class DiffDMOProjetBlancMojo extends DiffAbstractMojo
{
    /** Initialisation de la journalisation. */
    protected static final Logger LOGGER = LoggerFactory.getLogger(DiffDMOProjetBlancMojo.class);

    /**
     * Constructeur de la classe DiffDMOProjetBlancMojo.java
     */
    public DiffDMOProjetBlancMojo()
    {

    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.diffpb.DiffAbstractMojo#execute()
     */
    public void execute() throws MojoExecutionException, MojoFailureException
    {
        super.execute();
        try
        {

            getFiles(new File(project.getBasedir() + "/src/main/webapp/WEB-INF/tags"), true);
            getLog().debug("Scan " + project.getBasedir() + "/src/main/webapp/WEB-INF/tags");

            getFiles(new File(project.getBasedir() + "/src/main/resources/conf/commun"), false);
            getLog().debug("Scan " + project.getBasedir() + "/src/main/resources/conf/commun");

            getFiles(new File(project.getBasedir() + "/src/main/webapp/WEB-INF/web.xml"), false);
            getLog().debug("Scan " + project.getBasedir() + "/src/main/webapp/WEB-INF/web.xml");

            getFiles(new File(project.getBasedir() + "/src/main/webapp/composants/dhtmlpdfpopup"), true);
            getLog().debug("Scan " + project.getBasedir() + "/src/main/webapp/composants/dhtmlpdfpopup");

            getFiles(new File(project.getBasedir() + "/src/main/webapp/composants/dhtmlpopup"), true);
            getLog().debug("Scan " + project.getBasedir() + "/src/main/webapp/composants/dhtmlpopup");

            getFiles(new File(project.getBasedir() + "/src/main/webapp/composants/extremecomponents"), true);
            getLog().debug("Scan " + project.getBasedir() + "/src/main/webapp/composants/extremecomponents");

            getFiles(new File(project.getBasedir() + "/src/main/webapp/composants/jscalendar"), true);
            getLog().debug("Scan " + project.getBasedir() + "/src/main/webapp/composants/jscalendar");

            getFiles(new File(project.getBasedir() + "/src/main/webapp/composants/jscookmenu"), true);
            getLog().debug("Scan " + project.getBasedir() + "/src/main/webapp/composants/jscookmenu");

            getFiles(new File(project.getBasedir() + "/src/main/webapp/composants/tagsapp"), true);
            getLog().debug("Scan " + project.getBasedir() + "/src/main/webapp/composants/tagsapp");

            getFiles(new File(project.getBasedir() + "/src/main/webapp/composants/tree"), true);
            getLog().debug("Scan " + project.getBasedir() + "/src/main/webapp/composants/tree");

            getFiles(new File(project.getBasedir() + "/src/main/webapp/WEB-INF/flows/composantinfoutil"), false);
            getLog().debug("Scan " + project.getBasedir() + "/src/main/webapp/WEB-INF/flows/composantinfoutil");

            getFiles(new File(project.getBasedir() + "/src/main/webapp/WEB-INF/pages/composantinfoutil"), false);
            getLog().debug("Scan " + project.getBasedir() + "/src/main/webapp/WEB-INF/pages/composantinfoutil");

            getFiles(new File(project.getBasedir() + "/src/main/webapp/WEB-INF/pages"), false);
            getLog().debug("Scan " + project.getBasedir() + "/src/main/webapp/WEB-INF/pages");

            getFiles(new File(project.getBasedir() + "/src/main/webapp/WEB-INF/pages/errors"), false);
            getLog().debug("Scan " + project.getBasedir() + "/src/main/webapp/WEB-INF/pages/errors");

            getFiles(new File(project.getBuild().getSourceDirectory()), new String[] {"extremecomponents.properties",
                    "jasperreports.properties",
                    "apptags.properties"}, false);

        }
        catch (FileNotFoundException fnfe)
        {
            LOGGER.debug("Exception détectée", fnfe);
        }
        catch (IOException ioe)
        {
            LOGGER.debug("Exception détectée", ioe);
        }
    }

    /**
     * Méthode permettant d'éxécuter la commande windows fc entre le chemin du fichier se trouvant dans le projet démo
     * et le meme fichier se trouvant dans le projet blanc.
     * 
     * @param file1
     * @throws IOException Signal qu'une execption de type I/O s'est produite.
     */
    public void executeDiff(String file1) throws IOException
    {
        setPbPath(file1);
        Runtime runtime = Runtime.getRuntime();
        final Process process = runtime.exec(new String[] {"fc", file1, pathpb});
        BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
        String line = "";
        BufferedReader errorReader = new BufferedReader(new InputStreamReader(process.getErrorStream()));
        String errorLine = "";
        try
        {
            while ((line = reader.readLine()) != null)
            {
                if (!"".equals(line))
                {
                    getLog().info(line);
                }
            }
            while ((errorLine = errorReader.readLine()) != null)
            {
                if (!"".equals(errorLine))
                {
                    getLog().error(errorLine);
                }
            }
        }
        finally
        {
            reader.close();
        }

    }

    /**
     * Méthode qui permet de parcourir un répertoire et d'éxécuter la méthode de diff.
     * 
     * @param dir
     * @param recursive
     * @return files
     * @throws FileNotFoundException the file not found exception
     * @throws IOException Signal qu'une execption de type I/O s'est produite.
     */
    public void getFiles(File dir, boolean recursive) throws FileNotFoundException, IOException
    {

        if (dir.isDirectory())
        {
            // Pas de traitement sur les dossiers CVS et SVN
            if (!"CVS".equalsIgnoreCase(dir.getName()) && !".svn".equalsIgnoreCase(dir.getName()))
            {
                File[] files = dir.listFiles();
                for (int i = 0; i < files.length; i++)
                {
                    if (files[i].isDirectory())
                    {
                        if (recursive)
                        {
                            getFiles(files[i], true);
                        }
                    }
                    else
                    {
                        executeDiff(files[i].getAbsolutePath());
                    }
                }
            }
        }
        else
        {
            executeDiff(dir.getAbsolutePath());
        }
    }

    /**
     * Méthode qui permet de parcourir avec un filtre conditionnel un répertoire et d'éxécuter la méthode de diff.
     * 
     * @param dir
     * @param expression
     * @param recursive
     * @return files
     * @throws FileNotFoundException the file not found exception
     * @throws IOException Signal qu'une execption de type I/O s'est produite.
     */
    public void getFiles(File dir, String[] expression, boolean recursive) throws FileNotFoundException, IOException
    {

        if (dir.isDirectory())
        {
            // Pas de traitement sur les dossiers CVS et SVN
            if (!"CVS".equalsIgnoreCase(dir.getName()) && !".svn".equalsIgnoreCase(dir.getName()))
            {
                File[] files = dir.listFiles();
                for (int i = 0; i < files.length; i++)
                {
                    if (files[i].isDirectory())
                    {
                        if (recursive)
                        {
                            getFiles(files[i], expression, recursive);
                        }
                    }
                    else
                    {
                        for (int j = 0; j < expression.length; j++)
                        {

                            if (files[i].getName().contains(expression[j]))
                            {
                                executeDiff(files[i].getAbsolutePath());
                            }

                        }

                    }
                }
            }
        }
        else
        {

            for (int j = 0; j < expression.length; j++)
            {

                if (dir.getName().contains(expression[j]))
                {
                    executeDiff(dir.getAbsolutePath());

                }

            }

        }
    }
}
