/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.oraclepg;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.filefilter.IOFileFilter;
import org.apache.commons.io.filefilter.SuffixFileFilter;
import org.apache.commons.lang.StringUtils;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;

import fr.gouv.finances.lombok.maven.utils.Utils;

/**
 * Class SwitchOraclePgMojo .
 * 
 * @goal switchoraclepg
 */
public class SwitchOraclePgMojo extends AbstractMojo

{

    /**
     * Nom du SGBD.
     * 
     * @parameter expression="${nom.sgbd}"
     * @required
     */
    private String nomSgbd;

    /**
     * repertoire cible (defaut = projet lui-meme).
     * 
     * @parameter expression="${destination}" default-value="."
     * @required
     */
    private final String destination = "";

    /**
     * Constructeur de la classe SwitchOraclePgMojo.java
     */
    public SwitchOraclePgMojo()
    {
        super();

    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see org.apache.maven.plugin.AbstractMojo#execute()
     */
    public void execute() throws MojoExecutionException, MojoFailureException
    {
        verifNomSgbd();
        extraireJar();
        copierDossiersWebModule();
        // remplacerDossiers();
        copierDossiersRacine();
        ecraserProprietes();
    }

    /**
     * remplace les proprietes liees au type de SGBD
     * 
     * @throws MojoExecutionException the mojo execution exception
     */
    private void ecraserProprietes() throws MojoExecutionException
    {
        final String dossierProperties = destination + "/properties-pom";
        final IOFileFilter propsFilter = new SuffixFileFilter(".properties");
        try
        {
            if ("oracle".equals(nomSgbd))
            {
                Utils.replaceInDir(dossierProperties, propsFilter, "\n?appli.db.driver=[^\\n]+",
                    "\nappli.db.driver=oracle.jdbc.driver.OracleDriver");
                Utils.replaceInDir(dossierProperties, propsFilter, "\n?appli.db.url=[^\\n]+",
                    "\nappli.db.url=jdbc:oracle:thin:@SERVEUR:1521:INSTANCE");
            }
            else if ("pg".equals(nomSgbd))
            {
                Utils.replaceInDir(dossierProperties, propsFilter, "\n?appli.db.driver=[^\\n]+", "\nappli.db.driver=org.postgresql.Driver");
                Utils.replaceInDir(dossierProperties, propsFilter, "\n?appli.db.url=[^\\n]+",
                    "\nappli.db.url=jdbc:postgresql://SERVEUR:5432/BASE");
            }
            else if ("embedded".equals(nomSgbd))
            {
                // mon commentaire
            }

            Utils.replaceInDir(dossierProperties, propsFilter, "nom.sgbd=[^\\n]+", "nom.sgbd=" + nomSgbd);
        }
        catch (IOException e)
        {
            throw new MojoExecutionException("Erreur pendant la surcharge des fichiers properties", e);
        }

    }

    /**
     * methode Copier dossiers racine : .
     * 
     * @throws MojoExecutionException the mojo execution exception
     */
    private void copierDossiersRacine() throws MojoExecutionException
    {
        // Dossier à copier tels quels à la racine du projet
        String[] dirs = {"utilitairesdev"};
        String sourceDir;
        for (String dir : dirs)
        {
            sourceDir = "build/switchoraclepg/" + nomSgbd + "/" + dir;
            if (new File(sourceDir).exists())
            {
                cp(sourceDir, destination + "/" + dir);
            }
        }
    }

    /**
     * methode Copier dossiers web module : .
     * 
     * @throws MojoExecutionException the mojo execution exception
     */
    private void copierDossiersWebModule() throws MojoExecutionException
    {
        String[] dirs = {"batchapp", "webapp"};
        for (String dir : dirs)
        {
            cp("build/switchoraclepg/" + nomSgbd + "/" + dir, destination + "/src/main/" + dir);
        }
    }

    /**
     * methode Cp : .
     * 
     * @param src
     * @param dst
     * @throws MojoExecutionException the mojo execution exception
     */
    private void cp(final String src, final String dst) throws MojoExecutionException
    {

        getLog().info("Copie de " + src + " vers " + dst);
        File srcDir = new File(src);
        File dstDir = new File(dst);
        try
        {
            FileUtils.copyDirectory(srcDir, dstDir);
        }
        catch (IOException e)
        {
            throw new MojoExecutionException(e.getMessage(), e);
        }
    }

    /**
     * methode Delete directory : .
     * 
     * @param target
     */
    private void deleteDirectory(final String target)
    {
        getLog().info("Suppression du dossier " + target);
        File targetFile = new File(target);
        FileUtils.deleteQuietly(targetFile);
    }

    /**
     * methode Extraire jar : .
     * 
     * @throws MojoExecutionException the mojo execution exception
     */
    private void extraireJar() throws MojoExecutionException
    {

        File file;
        try
        {
            file = new File(Utils.class.getProtectionDomain().getCodeSource().getLocation().toURI());
        }
        catch (URISyntaxException e1)
        {
            throw new MojoExecutionException("Jar invalide", e1);
        }
        String jarpath = file.getAbsolutePath();
        getLog().info("Extraction du dossier build à partir de " + jarpath);
        Process ps = null;
        String stderr = null;
        String stdout = null;
        try
        {
            ps = new ProcessBuilder("jar", "-xf", jarpath, "build").start();
            stderr = IOUtils.toString(ps.getErrorStream());
            stdout = IOUtils.toString(ps.getInputStream());
        }
        catch (IOException e)
        {
            throw new MojoExecutionException(e.getMessage(), e);
        }
        if (!StringUtils.isEmpty(stderr))
        {
            getLog().error(stderr);
            throw new MojoExecutionException(stderr);
        }
        else
        {
            getLog().info(stdout);
        }
    }

    /**
     * methode Remplacer dossiers : . OBSOLETE: le dossier est amene directement depuis le jar du plugin
     * 
     * @throws MojoExecutionException the mojo execution exception
     */
    @Deprecated
    private void remplacerDossiers() throws MojoExecutionException
    {
        // Dossier dont il faut supprimer la source originale si elle existe
        String[] dirs = {"composantsoptionnels"};
        for (String dir : dirs)
        {
            deleteDirectory(destination + "/" + dir);
            cp("build/switchoraclepg/" + nomSgbd + "/" + dir, destination + "/" + dir);
        }
    }

    /**
     * methode Verif nom sgbd : .
     * 
     * @throws MojoExecutionException the mojo execution exception
     */
    private void verifNomSgbd() throws MojoExecutionException
    {
        if (!("oracle".equals(nomSgbd) || "pg".equals(nomSgbd) || "embedded".equals(nomSgbd)))
        {
            throw new MojoExecutionException("SGBD inconnu : options possibles \"oracle\", \"pg\" ou \"embedded\"");
        }
        else
        {
            getLog().info("Switch vers " + nomSgbd);
        }
    }

}
