/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.maven.utils;

import java.util.Collections;
import java.util.Enumeration;
import java.util.LinkedHashSet;
import java.util.Properties;

/**
 * The Class LinkedProperties.
 */
public class LinkedProperties extends Properties
{

    /** serialVersionUID - long. */
    private static final long serialVersionUID = 1L;

    /** The keys. */
    private final LinkedHashSet<Object> keys = new LinkedHashSet<Object>();

    /**
     * Constructeur de la classe LinkedProperties.java
     */
    public LinkedProperties()
    {
        super();

    }

    /**
     * Constructeur de la classe LinkedProperties.java
     *
     * @param defaults
     */
    public LinkedProperties(Properties defaults)
    {
        super(defaults); // DOCUMENTEZ_MOI Raccord de constructeur auto-généré

    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @return enumeration
     * @see java.util.Hashtable#keys()
     */
    public Enumeration<Object> keys()
    {
        return Collections.<Object> enumeration(keys);
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param key
     * @param value
     * @return object
     * @see java.util.Hashtable#put(java.lang.Object, java.lang.Object)
     */
    public Object put(Object key, Object value)
    {
        keys.add(key);
        return super.put(key, value);
    }

}
