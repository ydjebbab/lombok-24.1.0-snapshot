/*
 * Copyright (c) 2017 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.maven.maj;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * Class PomDependance .
 */
//@XmlRootElement(name = "dependency")
public class PomDependance
{

    /** artifact id. */
    private String artifactId;

    /** group id. */
    private String groupId;

    /** scope. */
    private String scope;

    /** version. */
    private String version;

    /**
     * Constructeur de la classe PomDependance.java
     */
    public PomDependance()
    {
        super(); // Raccord de constructeur auto-généré

    }

    /**
     * Constructeur de la classe PomDependance.java
     *
     * @param artifactId
     * @param groupId
     */
    public PomDependance(String artifactId, String groupId)
    {
        super();
        this.artifactId = artifactId;
        this.groupId = groupId;
    }

    /**
     * Instanciation de pom dependance.
     *
     * @param artifactId
     * @param groupId
     * @param scope
     */
    public PomDependance(String artifactId, String groupId, String scope)
    {
        this(artifactId, groupId);
        this.scope = scope;

    }

    /**
     * Instanciation de pom dependance.
     *
     * @param artifactId
     * @param groupId
     * @param scope
     * @param version
     */
    public PomDependance(String artifactId, String groupId, String scope, String version)
    {
        this(artifactId, groupId, scope);
        this.version = version;

    }

    /**
     * methode From : transforme en element xml correspondant (pas d'utilisation de jaxb, trop galère avec les
     * caractères à échapper.
     *
     * @param pomDep
     * @param doc
     * @return element
     */
    public static Element from(PomDependance pomDep, Document doc)
    {

        Element dependency = doc.createElement("dependency");
        Element groupId = doc.createElement("groupId");
        groupId.setTextContent(pomDep.getGroupId());

        Element artifactId = doc.createElement("artifactId");
        artifactId.setTextContent(pomDep.getArtifactId());

        dependency.appendChild(groupId);
        dependency.appendChild(artifactId);

        if (pomDep.getScope() != null && !pomDep.getScope().isEmpty())
        {
            Element scope = doc.createElement("scope");
            scope.setTextContent(pomDep.getScope());
            dependency.appendChild(scope);
        }

        if (pomDep.getVersion() != null && !pomDep.getVersion().isEmpty())
        {
            Element version = doc.createElement("version");
            version.setTextContent(pomDep.getVersion());
            dependency.appendChild(version);
        }
        return dependency;
    }

    /**
     * Accesseur de l attribut artifact id.
     *
     * @return artifact id
     */
    public String getArtifactId()
    {
        return artifactId;
    }

    /**
     * Modificateur de l attribut artifact id.
     *
     * @param artifactId le nouveau artifact id
     */
    public void setArtifactId(String artifactId)
    {
        this.artifactId = artifactId;
    }

    /**
     * Accesseur de l attribut group id.
     *
     * @return group id
     */
    public String getGroupId()
    {
        return groupId;
    }

    /**
     * Modificateur de l attribut group id.
     *
     * @param groupId le nouveau group id
     */
    public void setGroupId(String groupId)
    {
        this.groupId = groupId;
    }

    /**
     * Accesseur de l attribut scope.
     *
     * @return scope
     */
    public String getScope()
    {
        return scope;
    }

    /**
     * Modificateur de l attribut scope.
     *
     * @param scope le nouveau scope
     */
    public void setScope(String scope)
    {
        this.scope = scope;
    }

    /**
     * Accesseur de l attribut version.
     *
     * @return version
     */
    public String getVersion()
    {
        return version;
    }

    /**
     * Modificateur de l attribut version.
     *
     * @param version le nouveau version
     */
    public void setVersion(String version)
    {
        this.version = version;
    }

}
