/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.generationsquelette;

/*
 * Copyright 2001-2005 The Apache Software Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Locale;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;

/**
 * Class GenerationSquelettesBatchs .
 * 
 * @goal generationsquelettesbatchs
 */
public class GenerationSquelettesBatchs extends GenerationAbstractMojo

{

    /**
     * classNameBatchs. nom du service (classe concept) associé au batch
     * 
     * @parameter expression="${classNameBatchs}"
     * @required
     */
    private String classNameBatchs;

    /**
     * numZoneFonctionnelleBatchs. num de zf du batch
     * 
     * @parameter expression="${numZoneFonctionnelleBatchs}"
     */
    private String numZoneFonctionnelleBatchs;

    /**
     * useCaseName - nom de l'édition,.
     * 
     * @parameter expression="${useCaseNameBatchs}" @required
     */
    private String useCaseNameBatchs;

    /**
     * numzfccaBatchs. num de la zf du service associé au batch
     * 
     * @parameter expression="${numzfccaBatchs}" default-value=""
     */
    private String numzfccaBatchs;

    /**
     * nomClassHgr - String,.
     * 
     * @parameter expression="${nomClassHgr}"
     */
    private String nomClassHgr;

    /** zfBarres - String,. */
    private String zfBarres = "/";

    /** nomClassForGetSet - String, Pour generation getters et setters. */
    private String nomClassForGetSet;

    /**
     * Constructeur de la classe GenerationSquelettesBatchs.java
     */
    public GenerationSquelettesBatchs()
    {
        super();

    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @throws MojoExecutionException the mojo execution exception
     * @throws MojoFailureException the mojo failure exception
     * @see org.apache.maven.plugin.AbstractMojo#execute()
     */
    public void execute() throws MojoExecutionException, MojoFailureException
    {
        prefixepackage = prefixepackagefinances + "." + appli;
        getLog().info("appli :" + appli);
        getLog().info("prefixepackage :" + prefixepackage);
        prefixepackagecca = prefixepackage;
        racinejavasourceDir = project.getBuild().getSourceDirectory() + "/" + prefixepackagefinancesrep + "/" + appli;
        nomClassForGetSet = classNameBatchs;
        nomClassForGetSet = Character.toUpperCase(classNameBatchs.charAt(0)) + classNameBatchs.substring(1).toLowerCase(Locale.FRANCE);
        nomClassHgr = classNameBatchs.toLowerCase(Locale.FRANCE);
        nomClassHgr = Character.toUpperCase(nomClassHgr.charAt(0)) + nomClassHgr.substring(1);

        if (isNomDeLaClasseOk(nomClassHgr))
        {
            if ((!"".equals(numZoneFonctionnelleBatchs)) && (numZoneFonctionnelleBatchs != null))
            {
                prefixepackage = prefixepackage + ".zf" + numZoneFonctionnelleBatchs;

                racinejavasourceDir += "/zf" + numZoneFonctionnelleBatchs;

                zfBarres += "zf" + numZoneFonctionnelleBatchs + "/";
            }

            if ((numzfccaBatchs != null) && (!"".equals(numzfccaBatchs)))
            {
                prefixepackagecca += ".zf" + numzfccaBatchs;
            }

            try
            {
                if (!new File(racinejavasourceDir + "/batch").exists())
                {
                    new File(racinejavasourceDir + "/batch").mkdirs();
                }
                filterFile("/templates/fragment/CUBtemplatetraitementimpl.java.txt", racinejavasourceDir
                    + "/batch/Traitement" + useCaseNameBatchs + "Impl.java");
                if (!new File(batchDir + "/properties/").exists())
                {
                    new File(batchDir + "/properties/").mkdirs();
                }
                filterFile("/templates/fragment/CUBFRAGMENTbatch.properties.txt", batchDir
                    + "/properties/batch-fragmentacopier-" + useCaseNameBatchs.toLowerCase(Locale.FRANCE) + "-cub.properties", "ISO-8859-1");
            }
            catch (FileNotFoundException exc)
            {
                throw new MojoExecutionException(exc.getMessage(), exc);
            }
            catch (IOException exc)
            {
                throw new MojoExecutionException(exc.getMessage(), exc);
            }

        }
        else
        {
            throw new MojoFailureException(
                "Le nom d'une classe doit comporter au moins 2 caracteres et doit commencer par une majuscule");
        }

    }

    /**
     * methode Filter file : .
     * 
     * @param fileNameSource
     * @param fileNameTarget
     * @throws FileNotFoundException the file not found exception
     * @throws IOException Signal qu'une execption de type I/O s'est produite.
     */
    public void filterFile(String fileNameSource, String fileNameTarget) throws FileNotFoundException, IOException
    {
        filterFile(fileNameSource, fileNameTarget, "UTF-8");
    }

    /**
     * Méthode de parsing des template et génération des squelettes des dao, service et fichiers de configs spring.
     * 
     * @param fileNameSource Nom du template
     * @param fileNameTarget chemin relatif + Nom du squelette généré
     * @param encoding
     * @throws FileNotFoundException the file not found exception
     * @throws IOException Signal qu'une execption de type I/O s'est produite.
     */

    public void filterFile(String fileNameSource, String fileNameTarget, String encoding) throws FileNotFoundException, IOException
    {
        super.filterFile(fileNameSource, fileNameTarget, encoding);
        String currentLine;
        while ((currentLine = bReader.readLine()) != null)
        {
            currentLine = currentLine.replaceAll("@PREFIXEPACKAGE@", prefixepackage);
            currentLine = currentLine.replaceAll("@CUBMINUS@", useCaseNameBatchs.toLowerCase(Locale.FRANCE));
            currentLine = currentLine.replaceAll("@CCAMINUS@", classNameBatchs.toLowerCase(Locale.FRANCE));
            currentLine = currentLine.replaceAll("@CCA@", classNameBatchs);
            currentLine = currentLine.replaceAll("@CUB@", useCaseNameBatchs);
            currentLine = currentLine.replaceAll("@ZFBARRES@", zfBarres);
            currentLine = currentLine.replaceAll("@CCAMINUSFIN@", nomClassHgr);
            currentLine = currentLine.replaceAll("@PREFIXEPACKAGECCA@", prefixepackagecca);
            currentLine = currentLine.replaceAll("@CCAGETSET@", nomClassForGetSet);
            bWriter.append(currentLine);
            bWriter.newLine();
        }
        bWriter.close();
        bReader.close();

    }

}
