/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 *
 */
package fr.gouv.finances.lombok.batch;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.UnknownHostException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.PathMatcher;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.github.javaparser.JavaParser;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.ImportDeclaration;
import com.github.javaparser.ast.Modifier;
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.expr.BooleanLiteralExpr;
import com.github.javaparser.ast.expr.MethodCallExpr;
import com.github.javaparser.ast.expr.Name;
import com.github.javaparser.ast.expr.NameExpr;
import com.github.javaparser.ast.expr.SingleMemberAnnotationExpr;
import com.github.javaparser.ast.expr.StringLiteralExpr;
import com.github.javaparser.ast.stmt.BlockStmt;
import com.github.javaparser.ast.type.VoidType;
import com.github.javaparser.ast.visitor.VoidVisitorAdapter;

/**
 * Mojo permettant de transformer une classe java batch utilisant une conf xml en conf annotation
 * 
 * @goal formatbatchannotation
 * @author Pierre Philibert
 */
public class FormatBatchAnnotation extends AbstractMojo
{
    /** Initialisation de la journalisation. */
    protected static final Logger LOGGER = LoggerFactory.getLogger(FormatBatchAnnotation.class);

    private enum PropertyType
    {
        REF("ref"), VALUE("value");

        private String name;

        private PropertyType(String name)
        {
            this.name = name;
        }
    }

    /**
     * Chemin de base de recherche des fichier *Context-service.xml
     * 
     * @parameter expression="${source.repertoire}"
     * @required
     */
    private String repertoireSource;

    /**
     * Pattern des noms des fichiers xml de conf batch
     * 
     * @parameter expression="${source.pattern}"
     * @required
     */
    private String patternSource;

    /**
     * Chemin de base de recherche des fichier *.java
     */
    private static final String REP_SOURCE_PREFIXE = "src/main/java/";

    /**
     * Nom de la classe dont doit hériter une classe pour être traitée
     */
    private static final String CLASSE_MERE_BATCH = "ServiceBatchCommunImpl";

    /**
     * Constructeur
     */
    public FormatBatchAnnotation()
    {
        super();
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see org.apache.maven.plugin.AbstractMojo#execute()
     */
    @Override
    public void execute() throws MojoExecutionException, MojoFailureException
    {
        try
        {
            // on récupère la liste des fichiers de conf xml des batchs
            List<Path> lXMLPath = recupererFichiers();

            if (!lXMLPath.isEmpty())
            {
                LOGGER.info(lXMLPath.size() + " fichier(s) trouvé(s) dans le répertoire " + repertoireSource);
                for (Path path : lXMLPath)
                {
                    LOGGER.info("=> Traitement du fichier XML " + path);
                    processXML(path);
                    LOGGER.info("=> Fin de traitement du fichier XML " + path);
                }
            }
            else
            {
                LOGGER.info("Aucun fichier respectant le motif " + patternSource + " trouvé dans le répertoire " + repertoireSource);
            }
        }
        catch (IOException | XPathExpressionException | ParserConfigurationException | SAXException e)
        {
            throw new MojoExecutionException(e.getMessage(), e);
        }
    }

    /**
     * Renvoie une liste de fichiers contenus dans repertoireSource et dont le nom satisfait le pattern patternSource
     * (paramètres du MOJO)
     *
     * @return
     * @throws IOException
     */
    private List<Path> recupererFichiers() throws IOException
    {
        final PathMatcher filter = FileSystems.getDefault().getPathMatcher("glob:" + patternSource);

        List<Path> lXMLPaths = new ArrayList<>();
        Files.walk(Paths.get(repertoireSource))
            .filter(Files::isRegularFile)
            .filter(filter::matches)
            .forEach(lXMLPaths::add);

        return lXMLPaths;
    }

    /**
     * <pre>
     * Traitement d'un fichier de configuration XML : 
     * - recherche des éléments <bean id="..." class="..."> 
     * - lancement du traitement du fichier source JAVA corresondant à chaque élément bean
     * </pre>
     *
     * @param path : fichier de configuration XML
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws IOException
     * @throws XPathExpressionException
     */
    private void processXML(Path path) throws ParserConfigurationException, SAXException, IOException, XPathExpressionException
    {
        // on charge et on parse le fichier de conf XML
        InputSource source = new InputSource(new FileInputStream(path.toString()));
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        // Préconisations remontées par Sonar sur les attaques XML External Entity (XXE)
        documentBuilderFactory.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
        documentBuilderFactory.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
        DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
        Document document = null;
        NodeList nodes = null;
        try
        {
            document = documentBuilder.parse(source);
        }
        catch (UnknownHostException e)
        {
            LOGGER.error("-->  Erreur : " + e.getMessage() + ".", e);
        }

        // requête xpath pour trouver les definitions de beans
        XPath xpath = XPathFactory.newInstance().newXPath();
        if (document != null)
        {
            nodes = (NodeList) xpath.evaluate(
                "/beans/bean[@id and @class]",
                document,
                XPathConstants.NODESET);
        }

        // pour chaque élément <bean>
        if (nodes != null && nodes.getLength() > 0)
        {
            for (int i = 0; i < nodes.getLength(); i++)
            {
                // on vérifie d'abord que le source java identifié par l'attribut class de
                // l'élément <bean> est accessible
                Element element = (Element) nodes.item(i);
                String identifiant = element.getAttributeNode("id").getValue();
                String classe = element.getAttributeNode("class").getValue();
                LOGGER.info("--> Bean " + identifiant + " de type " + classe + " trouvé.");
                String cheminJava = REP_SOURCE_PREFIXE + classe.replace(".", "/") + ".java";
                Path pathJava = Paths.get(cheminJava);
                if (pathJava.toFile().exists())
                {
                    // le source java existe, on traite donc l'élément <bean> :
                    // on construit deux map, une pour les property avec une value,
                    // un autre map pour les property avec une ref
                    LOGGER.info("--> Fichier source " + pathJava + " existant.");
                    NodeList ndsProperty = element.getElementsByTagName("property");
                    Map<String, String> mPropertiesValue = getPropertyMap(ndsProperty, PropertyType.VALUE);
                    Map<String, String> mPropertiesRef = getPropertyMap(ndsProperty, PropertyType.REF);
                    processJava(pathJava, mPropertiesValue, mPropertiesRef);
                }
                else
                {
                    LOGGER.info("--> Fichier source " + pathJava + " inexistant.");
                }
            }
        }
        else
        {
            LOGGER.info("--> Aucune définition de bean trouvée dans le fichier " + path + ".");
        }
    }

    /**
     * <pre>
     * Produire une map nom -> valeur à partir d'une liste d'éléments <property>.
     * On retient les élément <property> de "type" : 
     * - value (<property name="..." value="..."> ou <property name="..."><value>...</value></property>) 
     * - ref (<property name="..." ref="..."> ou <property name="..."><ref bean="..."/></property>
     * </pre>
     *
     * @param nodeList : liste d'éléments <property>
     * @param propertyType : type de property souhaité : value ou ref
     * @return map de type nom de la property -> valeur (value ou nom du bean référencé)
     */
    private Map<String, String> getPropertyMap(NodeList nodeList, PropertyType propertyType)
    {
        Map<String, String> map = new HashMap<>();

        // on cherche les property qui porte ref ou value en attribut de l'élément <property>
        map.putAll(
            IntStream.range(0, nodeList.getLength())
                .mapToObj(nodeList::item)
                .filter(Element.class::isInstance)
                .map(Element.class::cast)
                .filter(element -> element.hasAttribute("name"))
                .filter(element -> element.hasAttribute(propertyType.name))
                .collect(Collectors.toMap(element -> element.getAttribute("name"),
                    element -> element.getAttribute(propertyType.name))));

        // on cherche les property qui porte ref ou value en sous élément de l'élément <property>
        // pour les property "ref", il faut prendre l'attribut "bean"
        // pour les property "value", on prend le texte de l'élément
        map.putAll(
            IntStream.range(0, nodeList.getLength())
                .mapToObj(nodeList::item)
                .filter(Element.class::isInstance)
                .map(Element.class::cast)
                .filter(element -> element.hasAttribute("name"))
                // possède un et un seul sous élément "ref" / "value"
                .filter(element -> element.getElementsByTagName(propertyType.name) != null
                    && element.getElementsByTagName(propertyType.name).getLength() == 1)
                // si c'est un élément "ref", doit avoir un attribut "bean"
                .filter(element -> propertyType == PropertyType.VALUE
                    || ((Element) element.getElementsByTagName(propertyType.name).item(0)).hasAttribute("bean"))
                .collect(Collectors.toMap(element -> element.getAttribute("name"),
                    element -> propertyType == PropertyType.REF
                        ? ((Element) element.getElementsByTagName(propertyType.name).item(0)).getAttribute("bean")
                        : element.getElementsByTagName(propertyType.name).item(0).getTextContent())));

        return map;
    }

    /**
     * <pre>
     * Traiter un fichier source JAVA : 
     * - ajoute les imports nécessaires aux annotations 
     * - ajoute des annotations sur les champs correspondant aux property de type "ref" 
     * - annote ou crée des setters annotés pour les property de type "value" 
     * - renomme le source JAVA avec un suffixe .ori 
     * - écrit le source JAVA annoté
     * </pre>
     *
     * @param path : fichier source JAVA
     * @param mPropertiesValue : map<nom de la property, value> extraite de la conf XML
     * @param mPropertiesRef : map<nom de la property, ref du bean> extraite de la conf XML
     */
    private void processJava(Path path, Map<String, String> mPropertiesValue, Map<String, String> mPropertiesRef)
    {
        LOGGER.info("--> Traitement du fichier source JAVA " + path);
        // modification du fichier
        try
        {
            CompilationUnit cu = JavaParser.parse(path.toFile());

            if (needsProcessing(cu, path.getFileName().toString()))
            {
                // ajout des imports
                addImports(cu, !mPropertiesValue.isEmpty(), !mPropertiesRef.isEmpty());

                // ajout des annotations sur la classe
                // et methodes et créations / annotations de setters si nécessaires
                new MethodVisitor(mPropertiesValue, mPropertiesRef).visit(cu, null);

                // renommage du fichier d'origine
                // LOGGER.info("--> Renommage du fichier : " + path
                // + " en " + path + ".ori");
                // Files.move(path.toAbsolutePath(), path.resolveSibling(path.toAbsolutePath() + ".ori"));
                // ecriture du nouveau fichier
                LOGGER.info("--> Ecriture du fichier modifié.");
                Files.write(path.toAbsolutePath(), cu.toString().getBytes());
                LOGGER.info("--> Fin de traitement du fichier : " + path.getFileName());
            }
            else
            {
                LOGGER.info("--> Le fichier est déjà annoté ou n'hérite pas de " + CLASSE_MERE_BATCH + ", pas de modification.");
            }
        }
        catch (IOException e)
        {
            LOGGER.error(e.getMessage(), e);
        }
    }

    /**
     * Vérifier s'il faut traiter le source JAVA (présence ou non d'une annotation Service sur la classe principale)
     * 
     * @param compilationUnit : compilation unit du source JAVA
     * @param fileName nom du fichier source
     * @return vrai si le source JAVA nécessite un traitement
     */
    private boolean needsProcessing(CompilationUnit compilationUnit, String fileName)
    {
        String className = fileName.endsWith(".java") ? fileName.substring(0, fileName.length() - ".java".length()) : fileName;
        Optional<ClassOrInterfaceDeclaration> classe = compilationUnit.getClassByName(className);
        return classe.isPresent()
            && !classe.get().getAnnotationByName("Service").isPresent()
            && classe.get().getExtendedTypes().contains(JavaParser.parseClassOrInterfaceType(CLASSE_MERE_BATCH));
    }

    /**
     * Définition d'une classe MethodVisitor pour JavaParser
     */
    private static class MethodVisitor extends VoidVisitorAdapter<Object>
    {
        // champs récupérés de la classe principale
        Map<String, String> mPropertiesValue;

        Map<String, String> mPropertiesRef;

        public MethodVisitor(Map<String, String> mPropertiesValue, Map<String, String> mPropertiesRef)
        {
            super();
            this.mPropertiesRef = mPropertiesRef;
            this.mPropertiesValue = mPropertiesValue;
        }

        /**
         * (methode de remplacement) {@inheritDoc}
         * 
         * @see com.github.javaparser.ast.visitor.VoidVisitorAdapter#visit(com.github.javaparser.ast.body.ClassOrInterfaceDeclaration,
         *      java.lang.Object)
         */
        @Override
        public void visit(ClassOrInterfaceDeclaration n, Object arg)
        {
            // nom du service (@Service) : nom de la classe en minuscules, ôté du suffixe impl
            String serviceName = n.getName().asString().toLowerCase();
            final String prefixeLog = "--> Classe " + n.getNameAsString();
            if (serviceName.endsWith("impl"))
            {
                serviceName = serviceName.substring(0, serviceName.length() - "impl".length());
            }

            LOGGER.debug(prefixeLog + " : ajout de l'annotation Service avec l'argument : " + serviceName);
            // ajout annotation Service sur la classe
            n.addAnnotation(new SingleMemberAnnotationExpr(
                new Name("Service"),
                new StringLiteralExpr(serviceName)));

            LOGGER.debug(prefixeLog + " : ajout de l'annotation Profile avec l'argument : batch");
            // ajout annotation Profile sur la classe
            n.addAnnotation(new SingleMemberAnnotationExpr(
                new Name("Profile"),
                new StringLiteralExpr("batch")));

            LOGGER.debug(prefixeLog + " : ajout de l'annotation Lazy avec l'argument : true");
            // ajout annotation Lazy sur la classe
            n.addAnnotation(new SingleMemberAnnotationExpr(
                new Name("Lazy"),
                new BooleanLiteralExpr(true)));

            // ajout annotations AutoWired et Qualifier Lazy sur les champs referencant un bean par ref dans la conf XML
            n.getFields().stream()
                .filter(field -> field.getVariables().size() == 1
                    && mPropertiesRef.keySet().contains(field.getVariables().get(0).getName().asString()))
                .forEach(field -> {
                    String nomChamp = field.getVariables().get(0).getName().asString();
                    String prefixeLog1 = "--> Champ " + nomChamp;
                    LOGGER.debug(prefixeLog1 + " : ajout de l'annotation Autowired");
                    field.addAnnotation("Autowired");
                    LOGGER.debug(prefixeLog1 + " : ajout de l'annotation Qualifier avec l'argument "
                        + mPropertiesRef.get(nomChamp));
                    field.addAnnotation(new SingleMemberAnnotationExpr(
                        new Name("Qualifier"),
                        new StringLiteralExpr(mPropertiesRef.get(nomChamp))));
                });

            // ajout des setter annotés sur les champs ayant une value dans la conf XML
            for (Map.Entry<String, String> entry : mPropertiesValue.entrySet())
            {
                String methodName = "set" + entry.getKey().substring(0, 1).toUpperCase() + entry.getKey().substring(1);
                // si le setter existe, on ajoute l'annotation Value
                List<MethodDeclaration> lMethod = n.getMethodsByName(methodName);
                if (!lMethod.isEmpty())
                {
                    LOGGER.debug(
                        "--> Champ " + entry.getKey() + " : annotation Value de l'accesseur existant avec la valeur " + entry.getValue());
                    lMethod.get(0).addAnnotation(new SingleMemberAnnotationExpr(
                        new Name("Value"),
                        new StringLiteralExpr(entry.getValue())));
                }
                else
                {
                    LOGGER.debug("--> Champ " + entry.getKey() + " : creation d'un accesseur avec la valeur " + entry.getValue());
                    MethodDeclaration method = n.addMethod(methodName);
                    populateSetterMethod(method, entry.getKey(), entry.getValue());
                }
            }
        }

        /**
         * Définir une méthode de type setter à partir de sa déclaration
         *
         * @param method : déclaration de la méthode
         * @param name : nom du paramètre
         * @param value : valeur passée dans l'annotation @Value
         * @return
         */
        private MethodDeclaration populateSetterMethod(MethodDeclaration method, String name, String value)
        {
            // declaration
            method.setModifier(Modifier.PUBLIC, true);
            method.setType(new VoidType());
            method.addParameter("String", name);

            // corps
            BlockStmt block = new BlockStmt();
            method.setBody(block);
            NameExpr statement = new NameExpr("super");
            MethodCallExpr call = new MethodCallExpr(statement, method.getName());
            call.addArgument(name);
            block.addStatement(call);

            // annotations Value et Override
            method.addAnnotation(new SingleMemberAnnotationExpr(
                new Name("Value"),
                new StringLiteralExpr(value)));

            method.addAnnotation("Override");

            return method;
        }
    }

    /**
     * Ajouter les imports nécessaires pour les annotations
     *
     * @param compilationUnit : compilation unit du source JAVA
     * @param hasValues : nécessite l'import pour l'annotation @Value
     * @param hasRefs : nécessite les imports pour les annotations @Autowired et @Qualifier
     */
    private void addImports(CompilationUnit compilationUnit, boolean hasValues, boolean hasRefs)
    {
        if (hasRefs)
        {
            addImport(compilationUnit, "org.springframework.beans.factory.annotation.Autowired");
            addImport(compilationUnit, "org.springframework.beans.factory.annotation.Qualifier");
        }
        if (hasValues)
        {
            addImport(compilationUnit, "org.springframework.beans.factory.annotation.Value");
        }
        addImport(compilationUnit, "org.springframework.context.annotation.Lazy");
        addImport(compilationUnit, "org.springframework.context.annotation.Profile");
        addImport(compilationUnit, "org.springframework.stereotype.Service");
    }

    /**
     * Ajouter l'import s'il n'est pas déjà présent
     *
     * @param compilationUnit : compilation unit du source JAVA
     * @param importDeclaration : import à ajouter
     */
    private void addImport(CompilationUnit compilationUnit, String sImport)
    {
        ImportDeclaration importDeclaration = new ImportDeclaration(sImport, false, false);
        if (!compilationUnit.getImports().contains(importDeclaration))
        {
            compilationUnit.addImport(importDeclaration);
        }
    }
}
