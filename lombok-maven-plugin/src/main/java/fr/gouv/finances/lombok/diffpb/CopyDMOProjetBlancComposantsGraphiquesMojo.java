/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.diffpb;

/*
 * Copyright 2001-2005 The Apache Software Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class CopyDMOProjetBlancComposantsGraphiquesMojo .
 * 
 * @goal copydmoprojetblanccomposantsgraphiques
 */
public class CopyDMOProjetBlancComposantsGraphiquesMojo extends AbstractCopyDMOProjetBlanc
{
    /** Initialisation de la journalisation. */
    protected static final Logger LOGGER = LoggerFactory.getLogger(CopyDMOProjetBlancComposantsGraphiquesMojo.class);

    /** directories. */
    private final String[] directories = new String[] {"dhtmlpdfpopup", "dhtmlpopup", "extremecomponents", "jscalendar",
            "jscookmenu", "tagsapp", "tree"};

    /**
     * Constructeur de la classe CopyDMOProjetBlancComposantsGraphiquesMojo.java
     */
    public CopyDMOProjetBlancComposantsGraphiquesMojo()
    {
        // constructeur vide

    }


    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.diffpb.DiffAbstractMojo#execute()
     */
    public void execute() throws MojoExecutionException, MojoFailureException
    {
        super.execute();
        super.setPbPath(project.getBasedir() + "/src/main/webapp/composants");
        File dirToDeleteInPb = new File(pathpb);
        boolean suppressionOk = dirToDeleteInPb.delete();
        if (!suppressionOk)
        {
            LOGGER.info("Le fichier '{}' n'a pas été supprimé", dirToDeleteInPb.getAbsolutePath());
        }
        try
        {

            for (int i = 0; i < directories.length; i++)
            {
                copyDirectoryWithtoutCvs(new File(project.getBasedir() + "/src/main/webapp/composants/"
                    + directories[i]));
            }

        }
        catch (FileNotFoundException exc)
        {
            throw new MojoExecutionException(exc.getMessage(), exc);
        }
        catch (IOException exc)
        {
            throw new MojoExecutionException(exc.getMessage(), exc);
        }

    }
}
