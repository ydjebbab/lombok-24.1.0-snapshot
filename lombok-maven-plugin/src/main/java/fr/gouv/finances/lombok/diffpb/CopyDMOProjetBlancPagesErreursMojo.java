/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.diffpb;

/*
 * Copyright 2001-2005 The Apache Software Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class CopyDMOProjetBlancPagesErreursMojo .
 * 
 * @goal copydmoprojetblancpageserreurs
 */
public class CopyDMOProjetBlancPagesErreursMojo extends AbstractCopyDMOProjetBlanc
{
    /** Initialisation de la journalisation. */
    protected static final Logger LOGGER = LoggerFactory.getLogger(CopyDMOProjetBlancPagesErreursMojo.class);

    /**
     * Constructeur de la classe CopyDMOProjetBlancPagesErreursMojo.java
     */
    public CopyDMOProjetBlancPagesErreursMojo()
    {
        super();
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.diffpb.DiffAbstractMojo#execute()
     */
    public void execute() throws MojoExecutionException, MojoFailureException
    {
        super.execute();
        try
        {

            super.setPbPath(project.getBasedir() + "/" + webinfDir + "/pages/errors");
            File dirToDeleteInPb = new File(pathpb);
            boolean suppressionOk = dirToDeleteInPb.delete();
            if (!suppressionOk)
            {
                LOGGER.info("Le fichier '{}' n'a pas été supprimé", dirToDeleteInPb.getAbsolutePath());
            }
            copyDirectoryWithtoutCvs(new File(project.getBasedir() + "/" + webinfDir + "/pages/errors"));

        }
        catch (FileNotFoundException exc)
        {
            throw new MojoExecutionException(exc.getMessage(), exc);
        }
        catch (IOException exc)
        {
            throw new MojoExecutionException(exc.getMessage(), exc);
        }

    }
}
