/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.deploiement;

/*
 * Copyright 2001-2005 The Apache Software Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;

import fr.gouv.finances.lombok.maven.utils.LinkedProperties;

/**
 * The Class GenerationExempleLdif.
 * 
 * @goal generationproperties
 */
public class GenerationProperties extends GenerationConfiguration

{

    /**
     * The output script.
     * 
     * @parameter expression = "${project.build.directory}/${package.appli}/div/properties"
     */
    private String outputProperties;

    /**
     * Dossier contenant les fichiers de configuration.
     * 
     * @parameter expression = "@basedir@"
     */
    protected String cibles;

    /** Environnement dont on souhaite generer le RPM. @parameter expression = "${target.env}" */
    protected String env;

    /**
     * Constructeur de la classe GenerationProperties.java
     */
    public GenerationProperties()
    {
        super();

    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @throws MojoExecutionException the mojo execution exception
     * @throws MojoFailureException the mojo failure exception
     * @see org.apache.maven.plugin.AbstractMojo#execute()
     */
    public void execute() throws MojoExecutionException, MojoFailureException
    {
        try
        {
            // generation du LDIF pour Puppet

            Map<String, Map<String, Properties>> modificateursFichier = lireModificateursCibles();
            Map<String, Properties> modificateursCible;
            File dossierSortie = new File(outputProperties);
            File dossierCible;
            dossierSortie.mkdirs();
            modificateursCible = modificateursFichier.get(env + ".properties");

            if (modificateursCible == null)
            {
                getLog().warn("Impossible de trouver des paramètres spécifiques pour la configuration: " + env);
                modificateursCible = new HashMap<String, Properties>();
            }

            dossierCible = new File(dossierSortie, env);
            dossierCible.mkdir();
            for (String source : sources)
            {
                ecrireProprietes(source, modificateursCible, dossierCible);
            }

        }
        catch (IOException ioe)
        {
            throw new MojoExecutionException(ioe.getMessage(), ioe);
        }
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.deploiement.GenerationConfiguration#getCibles()
     */
    protected String getCibles()
    {
        return cibles;
    }

    /**
     * Ecrire proprietes.
     * 
     * @param nomFichier the nom fichier
     * @param modificateurs
     * @param dossierCible
     * @throws IOException the IO exception
     */
    private void ecrireProprietes(String nomFichier, Map<String, Properties> modificateurs, File dossierCible) throws IOException
    {
        File fichier = new File(nomFichier);
        String nomFichierCourt = fichier.getName();
        Properties modificateursFichier = modificateurs.get(nomFichierCourt);

        if (fichier.exists())
        {
            LinkedProperties proprietes = null;
            if ("localisations.properties".equals(nomFichierCourt))
            {
                proprietes = creerLocalisations();
            }
            else
            {
                proprietes = lireFichierSource(nomFichier);
            }
            if (modificateursFichier != null)
            {
                proprietes.putAll(modificateursFichier);
            }
            Writer writer =
                new BufferedWriter(new OutputStreamWriter(new FileOutputStream(new File(dossierCible, nomFichierCourt)), "UTF-8"));
            proprietes.store(writer, null);
            writer.close();
        }
    }
}
