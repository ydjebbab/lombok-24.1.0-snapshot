package fr.gouv.finances.lombok.maven.release;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.io.IOUtils;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;

/**
 * Mojo de mise à jour.
 * 
 * @goal informrelease
 */
public class LombokReleaseMojo extends AbstractMojo
{   
    /**
     * @parameter default-value="${project}"
     */
    private org.apache.maven.project.MavenProject mavenProject;

    private static final boolean DEBUG = false;    
    private static final String URL_LOGIN = "http://venezia.appli.dgfip/account/login.php";
    private static final String URL_NEW_POST = "http://venezia.appli.dgfip/forum/forum.php?forum_id=3003&group_id=207";
    private static final String OUTPUT_FILENAME_LOGIN = "output_auth.html";
    private static final String OUTPUT_FILENAME_NEW_POST = "output_post.html";
    private static final String USERNAME = "lomboklombok";
    private static final String PASSWORD = "l0mb0k";

    private void writeResponse(HttpURLConnection hConnection, String outputFilename) throws IOException
    {
        getLog().info("-> Code réponse Connexion : " + hConnection.getResponseCode() + ".");
        if (HttpURLConnection.HTTP_OK != hConnection.getResponseCode())
        {
            return;
        }

        if (DEBUG)
        {
            getLog().info("-> Ecriture de la réponse dans le fichier : " + outputFilename + ".");
            try (InputStream is = hConnection.getInputStream();
                OutputStream os = new FileOutputStream(outputFilename)) 
            {           
                int data;
                while ((data = is.read()) != -1)
                {
                    os.write(data);
                }
            }
        }
        else
        {
            // on "vide" l'inputstream, sinon HttpURLConnection:disconnect() ferme
            // vraiment la connexion...
            hConnection.getInputStream().skip(hConnection.getInputStream().available());
        }
    }

    private void doConnect(String sUrl, Map<String, String> inputs, String outputFileName) 
        throws MojoExecutionException
    {
        HttpURLConnection hConnection = null;

        // construction de la chaîne des champs à poster 
        StringBuilder sInputs = new StringBuilder("");
        for (Entry<String, String> entry: inputs.entrySet())                
        {
            sInputs.append(entry.getKey()).append('=').append(entry.getValue()).append('&');
        }
        if (sInputs.length() > 1)
        {
            sInputs.delete(sInputs.length() - 1, sInputs.length());
        }

        try
        {
            URL url = new URL(sUrl);

            hConnection = (HttpURLConnection) url.openConnection();

            HttpURLConnection.setFollowRedirects(true);

            hConnection.setDoOutput(true);
            hConnection.setRequestMethod("POST"); 

            try (Writer writer = new BufferedWriter(new OutputStreamWriter(hConnection.getOutputStream(), "UTF-8")))
            {
                writer.write(sInputs.toString());
                writer.flush();
            }

            hConnection.connect();

            writeResponse(hConnection, outputFileName);
        }
        catch (Exception e)
        {
            getLog().error(e.getMessage());
            throw new MojoExecutionException(e.getMessage(), e);
        }
        finally 
        {
            if (hConnection != null)
            {
                hConnection.disconnect();
            }
        }
    }

    @Override
    public void execute() throws MojoExecutionException, MojoFailureException
    {
        // permet de ne jouer le goal qu'une fois lorsqu'on fait une release 
        // sinon, joué autant de fois qu'il y'a de modules
        if (mavenProject.isExecutionRoot())
        {
            // on récupère la version du projet
            String versionLombok = mavenProject.getVersion();
            if (versionLombok.endsWith("-SNAPSHOT"))
            {
                versionLombok = versionLombok.substring(0, versionLombok.indexOf("-SNAPSHOT"));
            }
    
            getLog().info("Début procédure de publicité de la sortie de Lombok " + versionLombok + ".");
    
            // test d'existence du fichier change log
            String changeLogPath = "changelog/release_notes_" + versionLombok + ".txt";
            String releaseNotes = getReleaseNotes(changeLogPath);
            if (releaseNotes.equals(""))
            {
                getLog().info("Fin de procédure : fichier changelog (" + changeLogPath + ") inexistant.");
                return;
            }
    
            // active la gestion des cookies
            CookieHandler.setDefault(new CookieManager(null, CookiePolicy.ACCEPT_ALL));
    
            // on se connecte à venezia pour créer la session
            getLog().info("Connexion à venezia avec l'utilisateur " + USERNAME + "(url : " + URL_LOGIN + ").");
            Map<String, String> inputs = new HashMap<>();
            inputs.put("form_loginname", USERNAME);
            inputs.put("form_pw", PASSWORD);
            inputs.put("return_to", "");
            inputs.put("login", "Identification");
            doConnect(URL_LOGIN, inputs, OUTPUT_FILENAME_LOGIN);
    
            // on se connecte à venezia pour créer le fil
            getLog().info("Connexion à venezia (url : " + URL_LOGIN + ").");
            inputs.clear();
            inputs.put("post_message", "y");
            inputs.put("thread_id", "0");
            inputs.put("msg_id", "0");
            inputs.put("is_follow_to", "0");
            inputs.put("subject", "La version " + versionLombok + " de Lombok est disponible");
    
            inputs.put("body", "La version " + versionLombok + " de Lombok vient avec les corrections / évolutions suivantes :\n" 
                + releaseNotes);
            doConnect(URL_NEW_POST, inputs, OUTPUT_FILENAME_NEW_POST);
        }
    }

    private String getReleaseNotes(String changeLogPath) throws MojoExecutionException 
    {
        String res = "";
        InputStream is = getClass().getClassLoader().getResourceAsStream(changeLogPath);
        StringWriter writer = new StringWriter();
        getLog().info("Récupération du texte du changelog : " + changeLogPath);
        
        try
        {
            IOUtils.copy(is, writer, "UTF-8");
            res = URLEncoder.encode(writer.toString(), "UTF-8");
        }
        catch (IOException e)
        {
            getLog().error(e.getMessage());
            throw new MojoExecutionException(e.getMessage(), e);
        }

        return res;
    }
}
