/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.deploiement;

/*
 * Copyright 2001-2005 The Apache Software Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;

import fr.gouv.finances.lombok.maven.utils.LinkedProperties;

/**
 * The Class GenerationConfiguration.
 */
public abstract class GenerationConfiguration extends AbstractMojo

{

    /** Constant : CLE_PARAMETRES_GENERAUX. */
    protected static final String CLE_PARAMETRES_GENERAUX = "puppet";

    /** Constant : CLE_PARAMETRES_SNMP. */
    protected static final String CLE_PARAMETRES_SNMP = "snmp";

    /** Constant : CLE_PARAMETRES_VESPA */
    protected static final String CLE_PARAMETRES_VESPA = "vespa";

    /** Constant : CLE_BOUCHON_VESPA */
    protected static final String CLE_BOUCHON_VESPA = "bouchon";

    /** Constant : CLE_SERVICES_VESPA */
    protected static final String CLE_SERVICES_VESPA = "services";

    /** Constant : CLE_UDDI_ACTIVER_VESPA */
    protected static final String CLE_UDDI_ACTIVER_VESPA = "uddi.activer";

    /** Constant : CLE_UDDI_HOTE_VESPA */
    protected static final String CLE_UDDI_HOTE_VESPA = "uddi.hote";

    /** Constant : CLE_UDDI_PORT_VESPA */
    protected static final String CLE_UDDI_PORT_VESPA = "uddi.port";

    /** Constant : MODE_TOMCAT_DEFAUT. */
    protected static final String MODE_TOMCAT_DEFAUT = "developpement";

    /** Constant : RUNLEVEL_TOMCAT_DEFAUT. */
    protected static final String RUNLEVEL_TOMCAT_DEFAUT = "present";

    /** Constant : MEMORY_TOMCAT_DEFAUT. */
    protected static final String MEMORY_TOMCAT_DEFAUT = "1024M";

    /** Constant : PERMGEN_TOMCAT_DEFAUT. */
    protected static final String PERMGEN_TOMCAT_DEFAUT = "256M";

    /** Constant : OPTS_TOMCAT_DEFAUT. */
    protected static final String OPTS_TOMCAT_DEFAUT = "";

    /**
     * The sources.
     * 
     * @parameter
     */
    protected String[] sources;

    /**
     * Utilisateur applicatif.
     * 
     * @parameter expression = "${deploiement.utilisateur}"
     */
    protected String user;

    /**
     * Groupe applicatif.
     * 
     * @parameter expression = "${deploiement.groupe}"
     */
    protected String group;

    /**
     * The appli.
     * 
     * @parameter expression = "${deploiement.appli}"
     */
    protected String appli;

    /**
     * The module.
     * 
     * @parameter expression = "${deploiement.module}"
     */
    protected String module;

    /**
     * Repertoire racine du deploiement
     * 
     * @parameter expression = "${deploiement.base}"
     */
    protected String base;

    /**
     * Nom de l'instance Tomcat
     * 
     * @parameter expression = "${deploiement.instanceweb}"
     */
    protected String instanceweb;

    /**
     * Nom associe au contexte web
     * 
     * @parameter expression = "${deploiement.webapp}"
     */
    protected String webapp;

    /**
     * Modules supplementaires a activer
     * 
     * @parameter
     */
    protected String extras;

    /**
     * Constructeur de la classe GenerationConfiguration.java
     */
    public GenerationConfiguration()
    {
        super();

    }

    /**
     * Creer localisations.
     * 
     * @return the linked properties
     */
    protected LinkedProperties creerLocalisations()
    {
        LinkedProperties props = new LinkedProperties();
        props.put("appli.batch.fichiersexternes.root", "file:" + base + "/data/local");
        props.put("appli.batch.root", "file:" + base + "/bin/batch");
        props.put("appli.commun.root", "file:" + base + "/conf/batch");
        props.put("appli.data.root", "file:" + base + "/scripts/db");
        props.put("appli.web.root", "file:" + base + "/bin/webmodules/" + webapp);
        return props;
    }

    /**
     * Accesseur de l attribut cibles.
     * 
     * @return cibles
     */
    protected abstract String getCibles();

    /**
     * Lire fichier decrivant un environnement cible.
     * 
     * @param fichier le fichier cible
     * @return the properties
     * @throws IOException the IO exception
     * @throws MojoExecutionException the mojo execution exception
     */
    protected Map<String, Properties> lireFichierCible(File fichier) throws IOException, MojoExecutionException
    {
        Map<String, Properties> result = new HashMap<String, Properties>();

        Reader reader = new BufferedReader(new InputStreamReader(new FileInputStream(fichier), "ISO-8859-1"));
        Properties props = new Properties();
        props.load(reader);
        reader.close();

        String nomFichier;
        String cleComposite;
        String cle;
        int hashIndex;
        Properties targetProps;

        for (Map.Entry<Object, Object> entry : props.entrySet())
        {
            cleComposite = entry.getKey().toString();
            hashIndex = cleComposite.indexOf('#');
            if (hashIndex < 0)
            {
                throw new MojoExecutionException("Cle composite invalide (" + cleComposite + ") dans le fichier "
                    + fichier.getAbsolutePath());
            }
            nomFichier = cleComposite.substring(0, hashIndex);
            cle = cleComposite.substring(hashIndex + 1);

            targetProps = result.get(nomFichier);
            if (targetProps == null)
            {
                targetProps = new Properties();
                result.put(nomFichier, targetProps);
            }
            targetProps.setProperty(cle, entry.getValue().toString());
        }

        return result;
    }

    /**
     * Lire fichier source.
     * 
     * @param nomFichier the nom fichier
     * @return the linked properties
     * @throws IOException the IO exception
     */
    protected LinkedProperties lireFichierSource(String nomFichier) throws IOException
    {
        Reader reader = new BufferedReader(new InputStreamReader(new FileInputStream(nomFichier), "ISO-8859-1"));
        LinkedProperties props = new LinkedProperties();
        props.load(reader);
        reader.close();
        return props;
    }

    /**
     * Obtenir les modifs de proprietes des environnements cible (puppet + environnement decrits manuellement).
     * 
     * @return la liste des environnements cible
     * @throws IOException the IO exception
     * @throws MojoExecutionException the mojo execution exception
     */
    protected Map<String, Map<String, Properties>> lireModificateursCibles() throws IOException, MojoExecutionException
    {
        Map<String, Map<String, Properties>> result = new HashMap<String, Map<String, Properties>>();

        File dossierCibles = new File(getCibles());
        if (!dossierCibles.exists())
        {
            getLog().warn("Impossible de localiser le dossier d'environnements cible:" + getCibles());
        }
        else
        {
            List<File> listeCibles = Arrays.asList(dossierCibles.listFiles(new FilenameFilter()
            {

                @Override
                public boolean accept(File dir, String name)
                {
                    return name.endsWith(".properties");
                }
            }));

            for (File cible : listeCibles)
            {
                result.put(cible.getName().substring(0, cible.getName().indexOf('.')), lireFichierCible(cible));
            }
        }

        return result;
    }

}
