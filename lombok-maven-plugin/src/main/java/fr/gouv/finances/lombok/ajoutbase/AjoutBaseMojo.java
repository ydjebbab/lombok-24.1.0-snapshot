/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.ajoutbase;

import java.io.File;
import java.io.IOException;
import java.util.Collection;

import org.apache.commons.io.FileUtils;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;

import fr.gouv.finances.lombok.maven.utils.Utils;

/**
 * Mojo permettant d'ajouter une autre base de données à un projet.
 * <br/>La doclet 'ajoutbase' permet de définir le nom de la tâche Maven permettant d'exécuter le Mojo.
 * 
 * @goal ajoutbase
 *
 * @author Christophe Breheret-Girardin
 */
public class AjoutBaseMojo extends AbstractMojo
{
    /**
     * Nom du SGBD, provenant d'un paramètre à fournir lors de l'exécution de la tâche Maven.
     * @parameter expression="${nom.sgbd}"
     * @required
     */
    private String nomSgbd;

    /**
     * Nom de la base de données à ajouter, provenant d'un paramètre à fournir lors de l'exécution de la tâche Maven.
     * @parameter expression="${nom.base}"
     * @required
     */
    private String nomBase;    

    /** Valeur du paramètre pour un SGBD Oracle. */
    private static final String SGBD_ORACLE = "oracle";
    /** Valeur du paramètre pour un SGBD PostgreSQL. */
    private static final String SGBD_POSTGRESQL = "pg";

    /** Répertoire où se trouvent les modèles de configuration */
    private static final String REPERTOIRE_TEMPLATE = "build/ajoutbase/";
    /** Libellé du nom de la base à modifier au sein du modèle de configuration */
    private static final String LIBELLE_TEMPLATE_NON_BASE = "nombase";

    /** Répertoire au sein du JAR contenant des fichiers de configuration */
    private static final String REPERTOIRE_TEMPLATE_CONF = "/conf";
    /** Répertoire au sein du JAR contenant les fichiers de propriété */
    private static final String REPERTOIRE_TEMPLATE_PROPERTIES = "/properties";
    /** Répertoire au sein du JAR contenant les fichiers "properties-pom" */
    private static final String REPERTOIRE_TEMPLATE_PROPERTIES_POM = "/properties-pom";
    
    /** Répertoire du projet des fichiers de configuration */
    private static final String REPERTOIRE_PROJET_CONF = "src/main/resources/conf";
    /** Répertoire du projet des fichiers de propriété */
    private static final String REPERTOIRE_PROJET_PROPERTIES = "src/main/webapp/WEB-INF/properties";
    /** Répertoire du projet des fichiers "properties-pom" */
    private static final String REPERTOIRE_PROJET_PROPERTIES_POM = "properties-pom";

    /**
     * Constructeur.
     */
    public AjoutBaseMojo()
    {
        super();
    }
    
    /**
     * Méthode permettant d'effectuer le traitement du Mojo.<br>
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see org.apache.maven.plugin.AbstractMojo#execute()
     */
    public void execute() throws MojoExecutionException, MojoFailureException
    {
        // Vérification des paramètres permettant d'exécuter la tâche Maven
        checkNomSgbd();
        // Extraction du Jar permettant de récupérer les modèles de configuration
        extraireJar();
        // Modification des modèles avec le nom de la base de données
        remplacerNomBase();
        // Copier des fichiers de configuration à l'emplacement dédié
        copierFichiers();
    }

    /**
     * Vérification des paramètres permettant d'exécuter la tâche Maven.
     *
     * @throws MojoExecutionException Erreur rencontrée lors de l'exécution du Mojo
     */
    private void checkNomSgbd() throws MojoExecutionException
    {
        // Journalisation des paramètres
        if (getLog().isDebugEnabled())
        {
            getLog().debug("nomSgbd : " + nomSgbd);
            getLog().debug("nomBase : " + nomBase);
        }

        // Analyse de la valeur du paramètre 'nom.sgbd'
        switch (nomSgbd)
        {
            case SGBD_ORACLE :
                getLog().debug("SGBD 'Oracle' detecté");
                break;
            case SGBD_POSTGRESQL :
                getLog().debug("SGBD 'PostgreSQL' detecté");
                break;
            default :
                throw new MojoExecutionException("SGBD '" + nomSgbd + "' inconnu. Options possibles : '"
                    + SGBD_ORACLE + "' ou '" + SGBD_POSTGRESQL + "'");
        }
    }

    /**
     * Méthode permettant de copier les fichiers de configuration de prise en compte d'une autre BDD.
     * 
     * @throws MojoExecutionException Erreur rencontrée lors de l'exécution du Mojo
     */
    private void copierFichiers() throws MojoExecutionException
    {
        copier(REPERTOIRE_TEMPLATE + nomSgbd + REPERTOIRE_TEMPLATE_CONF, REPERTOIRE_PROJET_CONF);
        copier(REPERTOIRE_TEMPLATE + nomSgbd + REPERTOIRE_TEMPLATE_PROPERTIES, REPERTOIRE_PROJET_PROPERTIES);        
        copier(REPERTOIRE_TEMPLATE + nomSgbd + REPERTOIRE_TEMPLATE_PROPERTIES_POM, REPERTOIRE_PROJET_PROPERTIES_POM);
    }

    /**
     * Méthode permettant de copier un fichier de configuration.
     * 
     * @param src fichier source
     * @param dst répertoire destination
     * @throws MojoExecutionException Erreur rencontrée lors de l'exécution du Mojo
     */
    private void copier(String src, String dst) throws MojoExecutionException
    {
        try
        {
            Utils.cp(src, dst);
        }
        catch (IOException ioe)
        {
            throw new MojoExecutionException(ioe.getMessage(), ioe);
        }
    }

    /**
     * Méthode permettant l'extraction du Jar, afin de récupérer les modèles de configuration.
     * @throws MojoExecutionException Erreur rencontrée lors de l'exécution du Mojo
     */
    private void extraireJar() throws MojoExecutionException
    {
        try
        {
            Utils.extractPathFromJar(REPERTOIRE_TEMPLATE + nomSgbd);
        }
        catch (IOException ioe)
        {
            throw new MojoExecutionException(ioe.getMessage(), ioe);
        }
    }

    /**
     * Méthode permettant la modification des modèles de configuration avec le nom de la base de données.
     * @throws MojoExecutionException Erreur rencontrée lors de l'exécution du Mojo
     */
    private void remplacerNomBase() throws MojoExecutionException
    {
        Collection<File> files = FileUtils.listFiles(new File(REPERTOIRE_TEMPLATE + nomSgbd), null, true);
        for (File file : files)
        {
            replaceInFile(file, LIBELLE_TEMPLATE_NON_BASE, nomBase);
            String path = file.getPath();
            if (path.contains(LIBELLE_TEMPLATE_NON_BASE))
            {
                try
                {
                    FileUtils.moveFile(file, new File(path.replace(LIBELLE_TEMPLATE_NON_BASE, nomBase)));
                }
                catch (IOException ioe)
                {
                    throw new MojoExecutionException(ioe.getMessage(), ioe);
                }
            }
        }

    }

    /**
     * Méthode permettant la modification d'un modèle de configuration avec le nom de la base de données.
     * @param file Template de configuration à modifier
     * @param pattern Chaîne de caractères à remplacer
     * @param replaceString Chaîne de caractères de remplacement
     * @throws MojoExecutionException Erreur rencontrée lors de l'exécution du Mojo
     */
    private void replaceInFile(File file, String pattern, String replaceString) throws MojoExecutionException
    {
        try
        {
            Utils.replaceInFile(file, pattern, replaceString);
        }
        catch (IOException ioe)
        {
            throw new MojoExecutionException(ioe.getMessage(), ioe);
        }
    }
}
