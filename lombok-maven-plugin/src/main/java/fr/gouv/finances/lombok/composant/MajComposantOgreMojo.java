/*
 * Copyright (c) 2017 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.composant;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.util.StringTokenizer;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.TrueFileFilter;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.project.MavenProject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * goal de mise à jour du composant Ogre à partir du projet ad-hoc
 * 
 * @goal majcomposantogre
 */
public class MajComposantOgreMojo extends AbstractMojo
{

    /** Initialisation de la journalisation. */
    protected static final Logger LOGGER = LoggerFactory.getLogger(MajComposantOgreMojo.class);

    /**
     * The project currently being build.
     * 
     * @parameter expression="${project}"
     * @required
     * @readonly
     */
    private MavenProject project;

    /**
     * Chemin complet du projet Ogre dans le workspace
     * 
     * @parameter expression="${chemin.projet}" default-value=""
     * @required
     */
    private String cheminProjet;

    /**
     * Constructeur de la classe MajComposantOgreMojo.java
     */
    public MajComposantOgreMojo()
    {
        super(); // Raccord de constructeur auto-généré

    }

    @Override
    public void execute() throws MojoExecutionException, MojoFailureException
    {
        File racineComposant = project.getBasedir();
        File racineProjet = new File(cheminProjet);
        majSourcesJava(racineComposant, racineProjet);
        majConfApplicative(racineComposant, racineProjet);
    }

    private void majSourcesJava(File racineComposant, File racineProjet) throws MojoExecutionException
    {
        String repJavaSource = racineProjet + "/ogreweb/JavaSource/fr";

        // Préparation migration V23 d'Ogre, compatibilité nouvelle arborescence
        File repertoireJavaSource = new File(repJavaSource);
        if (!repertoireJavaSource.exists())
        {
            repJavaSource = racineProjet + "/src/main/java/fr";
        }

        String repJavaCible = racineComposant + "/src/main/java/fr";
        try
        {
            majFichier(repJavaSource, repJavaCible);
        }
        catch (IOException e)
        {
            throw new MojoExecutionException("Erreur d'entrée sortie", e);
        }
    }

    private void majConfApplicative(File racineComposant, File racineProjet) throws MojoExecutionException
    {
        String repConfSource = racineProjet + "/ogreweb/WebContent/WEB-INF/conf/application";

        // Préparation migration V23 d'Ogre, compatibilité nouvelle arborescence
        File repertoireConfSource = new File(repConfSource);
        if (!repertoireConfSource.exists())
        {
            repConfSource = racineProjet + "/src/main/resources/conf/application";
        }

        String repConfCible = racineComposant + "/src/main/resources/conf";
        try
        {
            FileFilter filtreConf = new FileFilter()
            {

                @Override
                public boolean accept(File name)
                {
                    return (!("applicationContext-security.xml").equals(name.getName())
                    );
                }
            };
            majFichier(repConfSource, repConfCible, filtreConf);
            File repertoireConfCible = new File(repConfCible);
            for (File fichierConf : repertoireConfCible.listFiles())
            {
                if (!fichierConf.getName().contains("ogre"))
                {
                    StringTokenizer token = new StringTokenizer(fichierConf.getName(), "-");
                    String prefixe = token.nextToken();
                    String ogre = "ogre";
                    String nomFinal = prefixe + "-" + ogre;
                    while (token.hasMoreTokens())
                    {
                        nomFinal = nomFinal + '-' + token.nextToken();
                    }
                    boolean renommageOk = fichierConf.renameTo(new File(repConfCible, nomFinal));
                    if (!renommageOk)
                    {
                        LOGGER.info("Le fichier '{}' n'a pas été renommé en '{}'", repConfCible, nomFinal);
                    }
                }
            }

        }
        catch (IOException e)
        {
            throw new MojoExecutionException("Erreur d'entrée sortie", e);
        }
    }

    /**
     * methode Maj fichier : .
     * 
     * @param baseSource
     * @param baseDestination
     * @param chemin
     * @param filtre
     * @param ecraser
     * @throws IOException Signal qu'une execption de type I/O s'est produite.
     */
    private void majFichier(String baseSource, String baseDestination, FileFilter filtre)
        throws IOException
    {

        File fichierSource = new File(baseSource);
        File fichierCible = new File(baseDestination);

        getLog().info("copie de " + fichierSource.getAbsolutePath() + " vers " + fichierCible.getAbsolutePath());
        if (fichierSource.isDirectory())
        {
            FileUtils.deleteDirectory(fichierCible);
            FileUtils.copyDirectory(fichierSource, fichierCible, filtre, false);
        }
        else
        {
            if (fichierSource.exists())
            {
                FileUtils.copyFile(fichierSource, fichierCible, false);

            }
            else
            {
                FileUtils.deleteQuietly(fichierCible);
            }

        }
    }

    /**
     * methode Maj fichier : .
     * 
     * @param baseSource
     * @param baseDestination
     * @param chemin
     * @param ecraser
     * @throws IOException Signal qu'une execption de type I/O s'est produite.
     */
    private void majFichier(String baseSource, String baseDestination) throws IOException
    {
        majFichier(baseSource, baseDestination, TrueFileFilter.TRUE);
    }

}
