/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPathExpressionException;

import org.apache.commons.io.FileUtils;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.project.MavenProject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

/**
 * Class AbstractLombokMojo .
 */
public abstract class AbstractLombokMojo extends AbstractMojo
{
    /** Initialisation de la journalisation. */
    protected static final Logger LOGGER = LoggerFactory.getLogger(AbstractLombokMojo.class);

    /**
     * project.
     * 
     * @parameter expression="${project}" @required @readonly
     */
    protected MavenProject project;

    /**
     * Constructeur de la classe AbstractLombokMojo.java
     */
    public AbstractLombokMojo()
    {
        super();
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see org.apache.maven.plugin.AbstractMojo#execute()
     */
    public void execute() throws MojoExecutionException, MojoFailureException
    {
        try
        {
            File file = project.getFile();
            processFile(file);
        }
        catch (ParserConfigurationException | SAXException | IOException | TransformerException | XPathExpressionException exc)
        {
            throw new MojoExecutionException(exc.getMessage(), exc);
        }
    }

    /**
     * methode Process document : .
     * 
     * @param document
     * @throws MojoExecutionException the mojo execution exception
     * @throws MojoFailureException the mojo failure exception
     * @throws XPathExpressionException the x path expression exception
     */
    protected abstract void processDocument(Document document)
        throws MojoExecutionException, MojoFailureException, XPathExpressionException;

    /**
     * methode Backup file : .
     * 
     * @param file
     * @throws IOException Signal qu'une execption de type I/O s'est produite.
     */
    private void backupFile(File file) throws IOException
    {
        File backupFile = new File(file.getParentFile(), file.getName() + ".lombokBackup");
        if (!backupFile.exists())
        {
            LOGGER.info("Archivage de '{}' vers '{}'", file, backupFile);
            FileUtils.copyFile(file, backupFile);
            boolean readonlyOk = file.setReadOnly();
            if (!readonlyOk)
            {
                LOGGER.info("Le fichier '{}' n'a pas été activé en lecture seule", file.getAbsolutePath());
            }
            LOGGER.trace("Archivé");
        }
        else
        {
            LOGGER.info("Fichier '{}' existant", backupFile);
        }
    }

    /**
     * Traitement du fichier.
     * 
     * @param file
     * @throws MojoExecutionException the mojo execution exception
     * @throws MojoFailureException the mojo failure exception
     * @throws ParserConfigurationException the parser configuration exception
     * @throws SAXException the SAX exception
     * @throws IOException Signal qu'une execption de type I/O s'est produite.
     * @throws TransformerException the transformer exception
     * @throws XPathExpressionException the x path expression exception
     */
    private void processFile(File file) throws MojoExecutionException, MojoFailureException, ParserConfigurationException, SAXException,
        IOException, TransformerException, XPathExpressionException
    {
        Document document = readDocumentFromFile(file);
        processDocument(document);
        backupFile(file);
        writeDocumentToFile(document, file);

    }

    /**
     * methode Read document from file : .
     * 
     * @param file
     * @return document
     * @throws ParserConfigurationException the parser configuration exception
     * @throws SAXException the SAX exception
     * @throws IOException Signal qu'une execption de type I/O s'est produite.
     */
    private Document readDocumentFromFile(File file) throws ParserConfigurationException, SAXException, IOException
    {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        dbf.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
        dbf.setExpandEntityReferences(false);
        DocumentBuilder db = dbf.newDocumentBuilder();
        return db.parse(file);
    }

    /**
     * methode Write document to file : .
     * 
     * @param document
     * @param file
     * @throws IOException Signal qu'une execption de type I/O s'est produite.
     * @throws TransformerException the transformer exception
     */
    private void writeDocumentToFile(Document document, File file) throws IOException, TransformerException
    {
        TransformerFactory tFactory = TransformerFactory.newInstance();
        Transformer transformer = tFactory.newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
        DOMSource source = new DOMSource(document);
        StreamResult result = new StreamResult(new FileOutputStream(file));
        transformer.transform(source, result);

    }

}