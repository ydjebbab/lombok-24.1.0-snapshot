/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.generationsquelette;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.project.MavenProject;

/**
 * Class GenerationAbstractMojo .
 */
public abstract class GenerationAbstractMojo extends AbstractMojo
{

    /**
     * prefixepackagefinances - String,.
     * 
     * @parameter expression="${prefixepackagefinances}" default-value="fr.gouv.finances.dgfip"
     */
    protected String prefixepackagefinances;

    /**
     * appli - String,.
     * 
     * @parameter expression="${appli}" default-value="${project.artifactId}" required
     */
    protected String appli;

    /**
     * racinejavasourceDir.
     * 
     * @parameter default-value="${project.build.sourcedirectory}"
     */
    protected String racinejavasourceDir;

    /**
     * modulewebDir.
     * 
     * @parameter default-value="src/main/webapp/WEB-INF"
     */
    protected String webinfDir;

    /**
     * webinfConf.
     * 
     * @parameter default-value="src/main/resources"
     */
    protected String webinfConf;

    /**
     * batchDir.
     * 
     * @parameter default-value="src/main/batchapp"
     */

    protected String batchDir;

    /**
     * prefixepackagefinancesrep.
     * 
     * @parameter expression="${prefixepackagefinancesrep}" default-value="fr/gouv/finances/dgfip"
     * @required
     */

    protected String prefixepackagefinancesrep;

    /** prefixepackage - String,. */
    protected String prefixepackage;

    /** prefixepackagecca - String,. */
    protected String prefixepackagecca;

    /** file reader. */
    protected InputStreamReader fileReader;

    /** b reader. */
    protected BufferedReader bReader;

    /** fstream. */
    protected OutputStreamWriter fstream;

    /** b writer. */
    protected BufferedWriter bWriter;

    /**
     * project.
     * 
     * @parameter expression="${project}"
     */
    protected MavenProject project;

    /**
     * Constructeur de la classe GenerationAbstractMojo.java
     */
    public GenerationAbstractMojo()
    {
        super();

    }

    /**
     * methode Filter file : .
     * 
     * @param fileNameSource
     * @param fileNameTarget
     * @param encoding
     * @throws FileNotFoundException the file not found exception
     * @throws IOException Signal qu'une execption de type I/O s'est produite.
     */
    public void filterFile(String fileNameSource, String fileNameTarget, String encoding) throws FileNotFoundException, IOException
    {
        fileReader = new InputStreamReader(this.getClass().getResourceAsStream(fileNameSource));
        bReader = new BufferedReader(fileReader);
        fstream = new OutputStreamWriter(new FileOutputStream(new File(fileNameTarget)), encoding);
        bWriter = new BufferedWriter(fstream);

    }

    // public void filterFile(String fileNameSource, String fileNameTarget) throws FileNotFoundException, IOException
    // {
    //
    // fileReader = new InputStreamReader(new FileInputStream(new File(fileNameSource)), "UTF-8");
    // bReader = new BufferedReader(fileReader);
    // fstream = new OutputStreamWriter(new FileOutputStream(new File(fileNameTarget)), "UTF-8");
    // bWriter = new BufferedWriter(fstream);
    //
    // }

    /**
     * Verifie si nom de la classe ok.
     * 
     * @param nomClasse
     * @return true, si c'est nom de la classe ok
     */
    public boolean isNomDeLaClasseOk(String nomClasse)
    {

        int longueurchaine = nomClasse.length();
        char premiercar = nomClasse.charAt(0);
        if (longueurchaine < 2)
        {
            return false;
        }
        if (!Character.isUpperCase(premiercar))
        {
            return false;
        }

        return true;

    }

}
