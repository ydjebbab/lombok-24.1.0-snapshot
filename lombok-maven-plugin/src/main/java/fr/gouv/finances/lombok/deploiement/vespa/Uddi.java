package fr.gouv.finances.lombok.deploiement.vespa;

/**
 * Réglage de l'annuaire UDDI.
 */
public class Uddi
{
    /** Si "true", le SLPA utilisera l'annuaire UDDI. */
    private boolean activer = false;

    /** Hote distant de l'annuaire UDDI. */
    private String hote = "localhost";

    /** Port d'écoute de l'annuaire UDDI. */
    private String port = "8280";

    /**
     * Instanciation de Uddi sans paramètres.
     */
    public Uddi()
    {

    }

    /**
     * Accesseur de activer
     *
     * @return activer
     */
    public boolean isActiver()
    {
        return activer;
    }

    /**
     * Mutateur de activer
     *
     * @param activer activer
     */
    public void setActiver(boolean activer)
    {
        this.activer = activer;
    }

    /**
     * Accesseur de hote
     *
     * @return hote
     */
    public String getHote()
    {
        return hote;
    }

    /**
     * Mutateur de hote
     *
     * @param hote hote
     */
    public void setHote(String hote)
    {
        this.hote = hote;
    }

    /**
     * Accesseur de port
     *
     * @return port
     */
    public String getPort()
    {
        return port;
    }

    /**
     * Mutateur de port
     *
     * @param port port
     */
    public void setPort(String port)
    {
        this.port = port;
    }
}
