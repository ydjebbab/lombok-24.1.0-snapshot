/*
 * Copyright (c) 2016 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.diffpb;

import java.io.File;
import java.io.IOException;

/**
 * Class AbstractCopyDMOProjetBlanc DOCUMENTEZ_MOI.
 */
public abstract class AbstractCopyDMOProjetBlanc extends DiffAbstractMojo
{
    /**
     * Instanciation de abstract copy dmo projet blanc.
     */
    public AbstractCopyDMOProjetBlanc()
    {
        super();
    }

    /**
     * methode Copy directory withtout cvs : .
     *
     * @param dir méthode dupliquée extraite depuis les sous classes
     * @throws IOException Signal qu'une execption de type I/O s'est produite.
     */
    public void copyDirectoryWithtoutCvs(File dir) throws IOException
    {
        File[] files = dir.listFiles();
    
        for (int i = 0; i < files.length; i++)
        {
            setPbPath(files[i].getPath());
            if (files[i].isDirectory())
            {
                if (!"CVS".equalsIgnoreCase(files[i].getName()) && !".svn".equalsIgnoreCase(files[i].getName()))
                {
                    File fileToCreateInPb = new File(pathpb);
                    fileToCreateInPb.mkdirs();
                    copyDirectoryWithtoutCvs(files[i]);
                }
            }
            else
    
            {
                this.initRW(files[i].getPath(), pathpb);
                String currentLine;
                while ((currentLine = bReader.readLine()) != null)
                {
                    bWriter.append(currentLine);
                    bWriter.newLine();
    
                }
                bWriter.close();
                bReader.close();
                getLog().info("Copie de " + files[i].getPath() + "  dans " + pathpb + " effectuée avec succès");
            }
        }
        // return this;
    }

}