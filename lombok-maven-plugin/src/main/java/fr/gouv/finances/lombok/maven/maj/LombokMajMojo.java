/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.maven.maj;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;
import java.util.TreeSet;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.TrueFileFilter;
import org.apache.commons.io.output.FileWriterWithEncoding;
import org.apache.commons.lang.StringUtils;
import org.apache.maven.artifact.resolver.AbstractArtifactResolutionException;
import org.apache.maven.artifact.versioning.InvalidVersionSpecificationException;
import org.apache.maven.execution.MavenSession;
import org.apache.maven.model.Plugin;
import org.apache.maven.model.PluginExecution;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.InvalidPluginException;
import org.apache.maven.plugin.MojoExecution;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugin.PluginManager;
import org.apache.maven.plugin.PluginManagerException;
import org.apache.maven.plugin.descriptor.MojoDescriptor;
import org.apache.maven.plugin.descriptor.PluginDescriptor;
import org.apache.maven.plugin.version.PluginVersionNotFoundException;
import org.apache.maven.plugin.version.PluginVersionResolutionException;
import org.apache.maven.project.MavenProject;
import org.codehaus.plexus.component.repository.exception.ComponentLookupException;
import org.codehaus.plexus.util.xml.Xpp3Dom;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.gouv.finances.lombok.maven.utils.Utils;

/**
 * Mojo de mise à jour.
 * 
 * @goal maj
 */
public class LombokMajMojo extends AbstractMojo
{
    /** Initialisation de la journalisation. */
    protected static final Logger LOGGER = LoggerFactory.getLogger(LombokMajMojo.class);

    // private static String SHELL = "cmd";
    // private static String SHELL_OPTS = "/c";
    /** Constant : BASE_MAJ. */
    private static final String BASE_MAJ = "build/archetype";

    /** Constant : DEFAULT_PACKAGE_ROOT. */
    private static final String DEFAULT_PACKAGE_ROOT = "fr.gouv.finances.";

    /** Encodage à utiliser dans la gestion des fichiers */
    private static final String ENCODAGE = "ISO-8859-1";
    
    /** fichiers maj communs. */
    private static String[] fichiersMajCommuns = {
            "src/assembly",
            "integration/template",
            "integration/intex.bat",
            "integration/intex.sh",
            "integration/livraison.bat",
            "integration/livraison.sh",
            "integration/pom.xml",
            "integration/cibles-nopuppet/pom.xml",
            "src/main/batchapp",
            "src/main/resources/conf/commun",
            "src/main/resources/conf/application",
            "src/main/webapp/WEB-INF/web.xml",
            "src/main/webapp/composants/dhtmlpdfpopup",
            "src/main/webapp/composants/dhtmlpopup",
            "src/main/webapp/composants/extremecomponents",
            "src/main/webapp/composants/jscalendar",
            "src/main/webapp/composants/jscookmenu",
            "src/main/webapp/composants/tagsapp",
            "src/main/webapp/composants/tree",
            "src/main/webapp/WEB-INF/pages/errors",
            "src/main/webapp/WEB-INF/tags/app",
            "src/main/webapp/WEB-INF/tags/html",
            "src/main/webapp/WEB-INF/pages/identification.jsp",
            "src/main/webapp/WEB-INF/pages/sessionexpiree.jsp",
            "src/main/webapp/WEB-INF/pages/messagebloquantdetecteavecportail.jsp",
            "utilitairesdev/template",
            "src/main/java/jasperreports.properties",
            "src/main/java/commons-logging.properties",
            "src/main/java/extremetableResourceBundle_fr_FR.properties",
            "src/main/java/extremetableResourceBundle.properties",
            "src/main/java/extremetableResourceBundle_fr.properties"
    };

    /** nouveaux fichiers maj */
    private static String[] nouveauxFichiers = {
            "integration/cibles",
            "integration/cibles-nopuppet",
            "integration/dex",
            "integration/docs",
            "integration/exploitation",
            "integration/ordonnancement",
            "integration/tests",
            "src/main/webapp/WEB-INF/properties/jasper.properties",
            "src/main/webapp/WEB-INF/properties/composantjasper.properties"
    };

    /** fichiers maj properties. */
    private static String[] fichiersMajProperties = {
            // "properties-pom/parameters-commun.properties",
            // "properties-pom/parameters-eclipse.properties",
            "properties-pom/parameters-ice.properties",
        // "properties-pom/parameters-integration.properties",
    };

    /**
     * The project currently being build.
     * 
     * @parameter expression="${project}"
     * @required
     * @readonly
     */
    private MavenProject project;

    /**
     * The current Maven session.
     * 
     * @parameter expression="${session}"
     * @required
     * @readonly
     */
    private MavenSession mavenSession;

    /**
     * The Maven PluginManager component.
     * 
     * @component
     * @required
     */
    private PluginManager pluginManager;

    /** web module. @parameter expression="${lombok.maj.migrationMaven2}" default-value="true" */
    // private boolean migrationMaven2;

    // /** svn status.
    // *
    // * @parameter expression="${lombok.maj.svnStatus}" default-value="true"
    // */
    // private boolean svnStatus;

    /**
     * switch oracle pg.
     * 
     * @parameter expression="${lombok.maj.switchOraclePg}" default-value="false"
     */
    private boolean switchOraclePg;

    /**
     * Nom du SGBD.
     * 
     * @parameter expression="${nom.sgbd}" default-value="pg"
     */
    private String nomSgbd;

    /**
     * mise niveau elements communs.
     * 
     * @parameter expression="${lombok.maj.miseNiveauElementsCommuns}" default-value="true"
     */
    private boolean miseNiveauElementsCommuns;

    // /**
    // * ajout properties.
    // *
    // * @parameter expression="${lombok.maj.ajoutProperties}" default-value="true"
    // */
    // private boolean ajoutProperties;

    /**
     * composants.
     * 
     * @parameter expression="${lombok.maj.composants}" default-value=""
     */
    private String composants;

    /**
     * base package.
     * 
     * @parameter expression="${basePackage}" default-value=""
     */
    private String basePackage;

    /**
     * destination.
     * 
     * @parameter expression="${destination}" default-value="."
     */
    private String destination;

    /**
     * Le POM du projet doit il etre ecrase par le nouveau
     * 
     * @parameter expression="${ecraserPom}" default-value="false"
     */
    private boolean ecraserPom;

    /**
     * Constructeur.
     */
    public LombokMajMojo()
    {
        super();
    }
    
    /**
     * methode Plugin : .
     * 
     * @param groupId
     * @param artifactId
     * @param version
     * @return plugin
     */
    public static Plugin plugin(String groupId, String artifactId, String version)
    {
        Plugin plugin = new Plugin();
        plugin.setArtifactId(artifactId);
        plugin.setGroupId(groupId);
        plugin.setVersion(version);
        return plugin;
    }

    /**
     * methode Maj fichier : .
     * 
     * @param baseSource
     * @param baseDestination
     * @param chemin
     * @param ecraser
     * @throws IOException Signal qu'une execption de type I/O s'est produite.
     */
    private void majFichier(String baseSource, String baseDestination, String chemin, boolean ecraser) throws IOException
    {
        majFichier(baseSource, baseDestination, chemin, TrueFileFilter.TRUE, ecraser);
    }

    /**
     * Méthode permettant d'ajouter les nouvelles propriétés et de signaler les obsolètes.
     * 
     * @param baseSource
     * @param baseDestination
     * @param chemin
     * @throws IOException Signal qu'une execption de type I/O s'est produite.
     */
    private void patcherFichierProperties(String baseSource, String baseDestination, String chemin, String version) throws IOException
    {
        Properties newProps = new Properties();
        newProps.load(new FileInputStream(baseSource + chemin));
        Properties currentProps = new Properties();
        currentProps.load(new FileInputStream(baseDestination + chemin));
        Properties oldProps = new Properties();

        for (Entry<Object, Object> prop : currentProps.entrySet())
        {
            if (newProps.containsKey(prop.getKey()))
            {
                newProps.remove(prop.getKey());
            }
            else
            {
                oldProps.put(prop.getKey(), prop.getValue());
            }
            newProps.remove(prop.getKey());
        }

        StringBuilder result = new StringBuilder();
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(
            new FileInputStream(baseDestination + chemin), ENCODAGE)))
        {
            String oldPropLine = reader.readLine();
            int equalsIndex;
            String propName;
    
            while (oldPropLine != null)
            {
                if (("".equals(oldPropLine.trim())) || (oldPropLine.startsWith("#")))
                {
                    result.append(oldPropLine).append('\n');
                }
                else
                {
                    equalsIndex = oldPropLine.indexOf('=');
                    if (equalsIndex == -1)
                    {
                        result.append(oldPropLine).append('\n');
                    }
                    else
                    {
                        propName = oldPropLine.substring(0, equalsIndex);
                        if (!oldProps.containsKey(propName))
                        {
                            result.append(oldPropLine).append('\n');
                        }
                    }
                }
                oldPropLine = reader.readLine();
            }
        }

        try (FileWriterWithEncoding writer
            = new FileWriterWithEncoding(baseDestination + chemin, ENCODAGE))
        {
            writer.write(result.toString());
            if (oldProps.size() > 0)
                oldProps.store(writer, "\nPropriétés hors-Lombok ou obsolètes pour la version " + version);
            if (newProps.size() > 0)
                newProps.store(writer, "\nPropriétés ajoutées au passage à la version " + version);
        }
    }

    /**
     * methode Maj fichier : .
     * 
     * @param baseSource
     * @param baseDestination
     * @param chemin
     * @param filtre
     * @param ecraser
     * @throws IOException Signal qu'une execption de type I/O s'est produite.
     */
    private void majFichier(String baseSource, String baseDestination, String chemin, FileFilter filtre, boolean ecraser)
        throws IOException
    {

        File fichierSource = new File(baseSource, chemin);
        File fichierCible = new File(baseDestination, chemin);

        if (!filtre.accept(fichierSource))
        {
            return;
        }

        if (fichierCible.exists() && (!ecraser))
            return;

        if (fichierSource.isDirectory())
        {
            FileUtils.copyDirectory(fichierSource, fichierCible, filtre, false);
        }
        else
        {
            if (fichierSource.exists())
            {
                FileUtils.copyFile(fichierSource, fichierCible, false);

            }
            else
            {
                FileUtils.deleteQuietly(fichierCible);
            }

        }
    }

    /**
     * methode Remplir configuration mojo : .
     * 
     * @param racine
     * @param config
     * @return xpp3 dom
     */
    static private Xpp3Dom remplirConfigurationMojo(Xpp3Dom racine, Map<String, String> config)
    {
        Xpp3Dom node = null;
        for (Map.Entry<String, String> entry : config.entrySet())
        {
            node = new Xpp3Dom(entry.getKey());
            node.setValue(entry.getValue());
            racine.addChild(node);
        }
        return racine;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see org.apache.maven.plugin.AbstractMojo#execute()
     */
    public void execute() throws MojoExecutionException, MojoFailureException
    {
        // lecture des properties avec le properties-maven-plugin
        lireProperties();

        // runSvnStatus();
        creationProjetDepuisArchetype();
        // extractProjetBlanc();

        // migrationMaven2();
        miseNiveauElementsCommuns();

        copieNouveauxFichiers();
        ajoutProperties();
        filtragePom();

        switchOraclePg();

        majComposants();
        nettoyage();
        // ajoutProperties();
    }

    /**
     * Lecture des fichiers de properties
     * 
     * @throws MojoExecutionException
     */
    private void lireProperties() throws MojoExecutionException
    {
        Map<String, String> config = new HashMap<String, String>();
        config.put("project", "${project}");

        executerMojo(
            plugin("org.codehaus.mojo", "properties-maven-plugin", "1.0-alpha-2"),
            "read-project-properties", "default", config);
    }

    /**
     * methode Construire configuration : .
     * 
     * @param plugin
     * @param executionId
     * @param simpleConfiguration
     * @return xpp3 dom
     * @throws MojoExecutionException the mojo execution exception
     */
    private Xpp3Dom construireConfiguration(Plugin plugin, String executionId, Map<String, String> simpleConfiguration)
        throws MojoExecutionException
    {

        Xpp3Dom resultat = null;
        if (executionId != null)
        {
            PluginExecution targetExecution = null;
            Plugin currentPlugin;
            for (Object currentPluginObject : project.getBuildPlugins())
            {
                currentPlugin = (Plugin) currentPluginObject;
                if (plugin.getGroupId().equals(currentPlugin.getGroupId())
                    && plugin.getArtifactId().equals(currentPlugin.getArtifactId()))
                {
                    targetExecution = (PluginExecution) (currentPlugin.getExecutionsAsMap().get(executionId));
                    break;
                }

            }

            if (targetExecution == null)
            {
                throw new MojoExecutionException("Execution " + executionId + " introuvable");
            }

            resultat = (Xpp3Dom) targetExecution.getConfiguration();
        }
        else
        {
            resultat = new Xpp3Dom("configuration");
        }
        return remplirConfigurationMojo(resultat, simpleConfiguration);
    }

    /**
     * methode Creation projet depuis archetype : .
     * 
     * @throws MojoExecutionException the mojo execution exception
     */
    private void creationProjetDepuisArchetype() throws MojoExecutionException
    {
        PluginDescriptor pluginDescriptor = (PluginDescriptor) getPluginContext().get("pluginDescriptor");
        getLog().info("Creation d'un projet depuis l'archetype...");
        String actualPackage = basePackage;
        if (StringUtils.isEmpty(actualPackage))
        {
            actualPackage = DEFAULT_PACKAGE_ROOT + project.getArtifactId();
        }

        Map<String, String> config = new HashMap<String, String>();

        config.put("basedir", "@basedir@/" + BASE_MAJ);
        config.put("archetypeCatalog", "remote,local");
        config.put("archetypeGroupId", pluginDescriptor.getGroupId());
        config.put("archetypeArtifactId", "lombok-archetype");
        config.put("archetypeVersion", pluginDescriptor.getVersion());

        Properties execProps = mavenSession.getExecutionProperties();

        execProps.setProperty("groupId", project.getGroupId());
        execProps.setProperty("artifactId", project.getArtifactId());
        execProps.setProperty("version", project.getVersion());
        execProps.setProperty("package", actualPackage);

        config.put("session", "${session}");

        config.put("localRepository", "${localRepository}");
        config.put("remoteArtifactRepositories", "${project.remoteArtifactRepositories}");

        config.put("interactiveMode", "false");
        executerMojo(
            plugin("org.apache.maven.plugins", "maven-archetype-plugin", null),
            "generate", null, config);

        getLog().info("Projet créé avec succès.");
    }

    /**
     * methode Executer mojo : .
     * 
     * @param plugin
     * @param goal
     * @param exec
     * @throws MojoExecutionException the mojo execution exception
     */
    private void executerMojo(Plugin plugin, String goal, MojoExecution exec)
        throws MojoExecutionException
    {
        Object actualManager;
        Method executionMethod;
        Object[] executionParams;

        try
        {
            try
            {
                actualManager = mavenSession.lookup("org.apache.maven.plugin.BuildPluginManager");
                executionMethod = actualManager.getClass().getMethod("executeMojo", MavenSession.class, MojoExecution.class);
                executionParams = new Object[] {mavenSession, exec};
            }
            catch (ComponentLookupException exc)
            {
                LOGGER.debug("Exception détectée", exc);
                actualManager = pluginManager;
                executionMethod =
                    actualManager.getClass().getMethod("executeMojo", MavenProject.class, MojoExecution.class, MavenSession.class);
                executionParams = new Object[] {project, exec, mavenSession};
            }

            executionMethod.invoke(actualManager, executionParams);
        }
        catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException exc)
        {
            throw new MojoExecutionException("Erreur sur le Mojo", exc);
        }
    }

    /**
     * methode Executer mojo : .
     * 
     * @param plugin
     * @param goal
     * @param executionId
     * @param simpleConfiguration
     * @throws MojoExecutionException the mojo execution exception
     */
    private void executerMojo(Plugin plugin, String goal, String executionId, Map<String, String> simpleConfiguration)
        throws MojoExecutionException
    {
        MojoDescriptor mojoDescriptor = getDescriptor(plugin, goal);
        MojoExecution exec = new MojoExecution(mojoDescriptor, construireConfiguration(plugin, executionId, simpleConfiguration));
        executerMojo(plugin, goal, exec);
    }

    /**
     * Accesseur de l attribut descriptor.
     * 
     * @param plugin
     * @param goal
     * @return descriptor
     * @throws MojoExecutionException the mojo execution exception
     */
    private MojoDescriptor getDescriptor(Plugin plugin, String goal) throws MojoExecutionException
    {
        MojoDescriptor mojoDescriptor;
        try
        {
            mojoDescriptor = pluginManager.loadPluginDescriptor(plugin, project,
                mavenSession).getMojo(goal);
        }
        catch (AbstractArtifactResolutionException exc)
        {
            throw new MojoExecutionException("Unable to find mojo", exc);
        }
        catch (PluginVersionResolutionException exc)
        {
            throw new MojoExecutionException("Unable to find mojo", exc);
        }
        catch (InvalidVersionSpecificationException exc)
        {
            throw new MojoExecutionException("Unable to find mojo", exc);
        }
        catch (InvalidPluginException exc)
        {
            throw new MojoExecutionException("Unable to find mojo", exc);
        }
        catch (PluginManagerException exc)
        {
            throw new MojoExecutionException("Unable to find mojo", exc);
        }
        catch (PluginVersionNotFoundException exc)
        {
            throw new MojoExecutionException("Unable to find mojo", exc);
        }
        if (mojoDescriptor == null)
        {
            throw new MojoExecutionException("Unknown mojo goal: " + goal);
        }
        return mojoDescriptor;
    }

    /**
     * methode Maj composants : .
     * 
     * @throws MojoExecutionException the mojo execution exception
     */
    private void majComposants() throws MojoExecutionException
    {
        if (composants != null)
        {
            PluginDescriptor pluginDescriptor = (PluginDescriptor) this.getPluginContext().get("pluginDescriptor");
            Set<String> setComposants = new TreeSet<String>();
            setComposants.addAll(Arrays.asList(composants.split(",")));

            Map<String, String> config = new HashMap<String, String>();
            Properties props = project.getProperties();
            config.put("appli", props.getProperty("package.appli"));
            config.put("nomSgbd", props.getProperty("nom.sgbd"));

            for (String composant : setComposants)
            {
                config.put("nomComposant", composant);
                executerMojo(
                    plugin("fr.gouv.finances.lombok", "lombok-maven-plugin", pluginDescriptor.getVersion()),
                    "ajoutduncomposantoptionnel", null, config);

            }
        }

    }

    /**
     * Ajoute les nouvelles proprietes de construction
     * 
     * @throws MojoExecutionException the mojo execution exception
     */
    private void ajoutProperties() throws MojoExecutionException
    {
        getLog().info("Patch des fichiers de properties ...");

        PluginDescriptor pluginDescriptor = (PluginDescriptor) this.getPluginContext().get("pluginDescriptor");
        String version = pluginDescriptor.getVersion();
        String baseSource = BASE_MAJ + "/" + project.getArtifactId() + "/";
        String baseDestination = "./";

        try
        {
            for (String fichier : fichiersMajProperties)
            {
                patcherFichierProperties(baseSource, baseDestination, fichier, version);
            }
        }
        catch (IOException exc)
        {
            throw new MojoExecutionException(exc.getMessage(), exc);
        }
    }

    /**
     * Copie les nouveaux fichiers sans ecraser l'existant
     * 
     * @throws MojoExecutionException the mojo execution exception
     */
    private void copieNouveauxFichiers() throws MojoExecutionException
    {
        getLog().info("Copie des nouveaux fichiers ...");
        String baseSource = BASE_MAJ + "/" + project.getArtifactId() + "/";
        String baseDestination = "./";

        try
        {
            for (String fichier : nouveauxFichiers)
            {
                majFichier(baseSource, baseDestination, fichier, false);
            }
        }
        catch (IOException exc)
        {
            throw new MojoExecutionException(exc.getMessage(), exc);
        }
    }

    /**
     * methode Mise niveau elements communs : .
     * 
     * @throws MojoExecutionException the mojo execution exception
     */
    private void miseNiveauElementsCommuns() throws MojoExecutionException
    {
        if (miseNiveauElementsCommuns)
        {
            getLog().info("Mise \u00E0 jour des \u00E9l\u00E9ments communs ...");
            String baseSource = BASE_MAJ + "/" + project.getArtifactId() + "/";
            String baseDestination = "./";
            FileFilter filtreCommun = new FileFilter()
            {

                @Override
                public boolean accept(File name)
                {
                    return (!("applicationContext-dao.xml").equals(name.getName())
                        && !("applicationContext-service.xml").equals(name.getName())
                        && !("applicationContext-security.xml").equals(name.getName())
                        && !("applicationContext-ws.xml").equals(name.getName())
                        && !("cubatch1").equals(name.getName())
                        && !("batchFactory.xml").equals(name.getName())
                        && !("batch.properties").equals(name.getName())
                    );
                }
            };

            try
            {
                for (String fichier : fichiersMajCommuns)
                {
                    majFichier(baseSource, baseDestination, fichier, filtreCommun, true);
                }
            }
            catch (IOException exc)
            {
                throw new MojoExecutionException(exc.getMessage(), exc);
            }
        }
    }

    /**
     * methode filtragePom : Incremente le numero de version
     * 
     * @throws MojoExecutionException the mojo execution exception
     */
    private void filtragePom() throws MojoExecutionException
    {
        getLog().info("Mise \u00E0 jour du pom.xml ...");
        String baseDestination = "./";
        File fichierCible = new File(baseDestination, "pom.xml");

        try
        {
            if (ecraserPom)
            {
                String baseSource = BASE_MAJ + "/" + project.getArtifactId() + "/";
                File fichierSource = new File(baseSource, "pom.xml");
                FileUtils.copyFile(fichierSource, fichierCible, false);
            }
            else
            {
                PluginDescriptor pluginDescriptor = (PluginDescriptor) this.getPluginContext().get("pluginDescriptor");
                String newVersion = pluginDescriptor.getVersion();
                String oldVersion = project.getParent().getVersion();
                Utils.replaceInFile(fichierCible, "<version>" + oldVersion + "</version>", "<version>" + newVersion + "</version>");
                Utils.replaceInFile(fichierCible, "<packaging>war</packaging>", "<packaging>\\$\\{packaging.type\\}</packaging>");

            }
        }
        catch (IOException exc)
        {
            throw new MojoExecutionException(exc.getMessage(), exc);
        }
    }

    /**
     * methode Nettoyage : .
     * 
     * @throws MojoExecutionException the mojo execution exception
     */
    private void nettoyage() throws MojoExecutionException
    {

        getLog().info("Nettoyage de l'arborescence ...");

        Map<String, String> config = new HashMap<String, String>();

        config.put("reportDirectory", project.getReporting().getOutputDirectory());
        config.put("directory", project.getBuild().getDirectory());
        config.put("outputDirectory", project.getBuild().getOutputDirectory());
        config.put("testOutputDirectory", project.getBuild().getTestOutputDirectory());

        executerMojo(
            plugin("org.apache.maven.plugins", "maven-clean-plugin", null),
            "clean", null, config);

        getLog().info("Nettoyage termin\u00E9.");
    }

    /**
     * Méthode permettant de basculer d'Oracle en PostgreSQL
     *
     * @throws MojoExecutionException the mojo execution exception
     */
    private void switchOraclePg() throws MojoExecutionException
    {
        if (switchOraclePg)
        {
            Map<String, String> config = new HashMap<String, String>();
            /*
             * config.put("project", "${project}"); executerMojo( plugin("org.codehaus.mojo", "properties-maven-plugin",
             * "1.0-alpha-2"), "read-project-properties", "default", config);
             */
            PluginDescriptor pluginDescriptor = (PluginDescriptor) this.getPluginContext().get("pluginDescriptor");
            //Properties props = project.getProperties();
            // config.clear();
            // config.put("nomSgbd", props.getProperty("nom.sgbd"));
            config.put("nomSgbd", nomSgbd);
            // config.put("destination", BASE_MAJ);
            config.put("destination", destination);

            executerMojo(
                plugin(pluginDescriptor.getGroupId(), pluginDescriptor.getArtifactId(), pluginDescriptor.getVersion()),
                "switchoraclepg", null, config);

        }
    }
}
