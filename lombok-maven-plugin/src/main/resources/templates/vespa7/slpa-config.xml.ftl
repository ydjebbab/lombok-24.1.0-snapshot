<?xml version="1.0" encoding="UTF-8"?>

<!-- ______________________________________________ -->
<!--                                                -->
<!--           FICHIER GENERE PAR LOMBOK            -->
<!-- ______________________________________________ -->

<!-- fichier de configuraiton utilisé par défaut du service SLPA.
    Attention à ne pas le renommer. -->
<modules>
    <module name="common">
        <class>fr.gouv.finances.dgi.spa.slpa.common.CommonConfiguratorDefaultImpl</class>
        <params>
            <!-- param name="org.xml.sax.driver"
                type="system.property"
                value="org.apache.xerces.parsers.SAXParser"/ -->
        </params>
    </module>

    <module name="cache">
        <class>fr.gouv.finances.dgi.spa.slpa.cache.CacheServiceDefaultImpl</class>
        <params>
            <param name="directory"
                type="java.lang.String"
                value="slpa-data" />
            <param name="filename"
                value="slpa-cache.data" />
        </params>
    </module>

    <module name="cacheRefresher">
        <class>fr.gouv.finances.dgi.spa.slpa.refresher.CacheRefresherDefaultImpl</class>
        <params>
            <param name="interval" value="24" />
            <param name="maxTimeBeforeRefresh" value="30" />
        </params>
    </module>

    <module name="registry">
        <class>fr.gouv.finances.dgi.spa.slpa.registry.RegistryServiceDefaultImpl</class>
        <params>
            <param name="useUDDI" value="${vespa.uddi.activer?c}" />
            <param name="publishURL" value="http://${vespa.uddi.hote}:${vespa.uddi.port}/juddi/publish" />
            <param name="inquiryURL" value="http://${vespa.uddi.hote}:${vespa.uddi.port}/juddi/inquiry" />
            <param name="organization" value="Copernic" />
            <param name="user" value="jboss" />
            <param name="psswd" value="" />
        </params>
    </module>
</modules>