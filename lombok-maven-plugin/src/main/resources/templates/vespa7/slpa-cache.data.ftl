<?xml version="1.0" encoding="UTF-8"?>

<!-- ______________________________________________ -->
<!--                                                -->
<!--           FICHIER GENERE PAR LOMBOK            -->
<!-- ______________________________________________ -->

<services
        xmlns="http://dgi.spa/"
        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xsi:schemaLocation="http://dgi.spa/slpa-cache.xsd">

<#list vespa.contrats as contrat>
        <contrat name="${contrat.name}">
<#list contrat.services as service>
            <service name="${service.name}" wsdl="${service.wsdl}" accessPoint="${service.accesspoint}" />
</#list>
        </contrat>
</#list>
</services>
