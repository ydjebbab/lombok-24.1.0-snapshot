#element ci-dessous a ajouter en fin de fichier jasper.properties et à compléter le cas échéant
#[@CUE@]
@CUEMINUS@.subreportdir=@appli.web.root@/WEB-INF/editions/rapports@ZFBARRES@
@CUEMINUS@.imagessourcedir=@appli.web.root@/WEB-INF/editions/images/
@CUEMINUS@.jasper=@appli.web.root@/WEB-INF/editions/rapports@ZFBARRES@@CUEMINUS@.jasper

#nom presente à l'utilisateur par exemple dans la banette
@CUEMINUS@.nomedition=Le nom de l'édition
@CUEMINUS@.nombrejoursconservation=0

#Titre Subject et KeyWords mis dans les proprietes des document pdf
@CUEMINUS@.title=Le titre de l'édition
@CUEMINUS@.subject=Le detail du contenu de l'édition
@CUEMINUS@.keywords=motcle1 motcle2 motcle3