package @PREFIXEPACKAGE@.batch;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import @PREFIXEPACKAGECCA@.service.@CCA@Service;
import fr.gouv.finances.lombok.batch.ServiceBatchCommunImpl;

/** TODO : documenter (javadoc) */
@Service("traitement@CUBMINUS@")
@Profile("batch")
@Lazy(true)
public class Traitement@CUB@Impl extends
        ServiceBatchCommunImpl {
        
    @Autowired
    @Qualifier("@CCAMINUS@serviceso")       
    private @CCA@Service @CCAMINUS@serviceso;
    
    public @CCA@Service get@CCAGETSET@serviceso() {
        return @CCAMINUS@serviceso;
    }
    public void set@CCAGETSET@serviceso(
            @CCA@Service @CCAMINUS@serviceso) {
        this.@CCAMINUS@serviceso = @CCAMINUS@serviceso;
    }

    @Override
    public void traiterBatch() {
        //@CCAMINUS@serviceso.methodeAnalyse1();
    }
}