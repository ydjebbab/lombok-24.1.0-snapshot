package @PREFIXEPACKAGE@.service.impl;
import @PREFIXEPACKAGE@.dao.@CCA@Dao;
import @PREFIXEPACKAGE@.service.@CCA@Service;
import fr.gouv.finances.lombok.util.base.BaseServiceImpl;


/** TODO : Documenter (javadoc) */
public class @CCA@ServiceImpl extends BaseServiceImpl implements @CCA@Service{
	private @CCA@Dao @CCAMINUS@dao;

	public @CCA@Dao get@CCAGETSET@dao() {
		return @CCAMINUS@dao;
	}

	public void set@CCAGETSET@dao(@CCA@Dao @CCAMINUS@dao) {
		this.@CCAMINUS@dao = @CCAMINUS@dao;
	}

	public void methodeAnalyse1() {
		@CCAMINUS@dao.findEnsembleObjets1ParCritere1();
	}
}
		

