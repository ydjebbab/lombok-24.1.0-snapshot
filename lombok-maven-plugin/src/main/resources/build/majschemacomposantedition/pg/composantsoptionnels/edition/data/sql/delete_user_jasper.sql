---
--- SUPPRESSION DE L'UTILISATEUR DU COMPOSANT EDITION
--- VERSION : V13R05
--- 
--- Modifications 
--- wpetit        03/05/2007    creation      
---

--DROP USER J@appli@ CASCADE;

revoke all on tablespace J@appli@_DATA from J@appli@;
revoke all on tablespace J@appli@_INDX from J@appli@;
revoke all on tablespace J@appli@_BLOB from J@appli@;

DROP USER J@appli@;
