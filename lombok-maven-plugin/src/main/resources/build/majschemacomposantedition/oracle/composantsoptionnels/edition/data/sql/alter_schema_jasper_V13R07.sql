---
--- MODIFICATION DU SCHEMA DU COMPOSANT EDITION
--- VERSION : V13R07
--- 
--- APPLICATION A UN SCHEMA INFERIEUR A V13R05
--- Modifications 
--- wpetit        04/06/2007    ajout de la colonne APPLICATIONORIGINE
---                             dans la table TRCPURGEEDITION_TRPE
---                             ajout de la colonne TAILLEFICHIER dans
---                             la table JOBHISTORY_JOHI
---
ALTER TABLE TRCPURGEEDITION_TRPE 
    ADD ("APPLICATIONORIGINE" VARCHAR2(255)) 
;
ALTER TABLE JOBHISTORY_JOHI
    ADD ("TAILLEFICHIER" NUMBER(19,0)) 
;
