---
--- SUPPRESSION DE L'UTILISATEUR DU COMPOSANT EDITION
--- VERSION : V13R05
--- 
--- Modifications 
--- wpetit        03/05/2007    creation      
---
DROP USER J@appli@ CASCADE;
