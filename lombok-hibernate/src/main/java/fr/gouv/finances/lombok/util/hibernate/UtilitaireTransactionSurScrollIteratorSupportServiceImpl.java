/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : UtilitaireTransactionSurScrollIteratorSupportService.java
 *
 */
package fr.gouv.finances.lombok.util.hibernate;

import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.transaction.TransactionStatus;

/**
 * <pre>
 * Classe de support pour la classe utilitaire UtilitaireTransactionSurScrollIteratorSupportService 
 * qui permet de purger les caches HIBERNATE lors de l'utilisation d'un scrolliterator pour des procedures de mise à jour.
 * Cette classe tient les références aux deux transactions managers nécessaires.
 * Il suffit d'injecter la référence de cette classe au service qui en a besoin.
 * </pre>
 * 
 * @author lcontinsouzas-cp
 * @version $Revision: 1.2 $ Date: 15 déc. 2009
 */

public class UtilitaireTransactionSurScrollIteratorSupportServiceImpl
    implements UtilitaireTransactionSurScrollIteratorSupportService
{

    HibernateTransactionManager transactionmanagersanscache;

    HibernateTransactionManager transactionmanagerstandard;

    public UtilitaireTransactionSurScrollIteratorSupportServiceImpl()
    {
        super();
    }

    /**
     * Permet de fermet la transaction en fin de boucle sur le scrolliterator que le traitement soit en erreur ou non
     * (lecture seule pas de rollback) A inserer au sein d'une clause finally pour garantir la fin de transaction.
     * 
     * @param status fourni lors du demarrage de la transaction
     */
    @Override
    public void commiterTransactionEnglobanteDeLectureScrollIterator(TransactionStatus status)
    {
        UtilitaireTransactionSurScrollIterator.commitTransaction(transactionmanagersanscache, status);
    }

    /**
     * Permet de fermer la transaction en fin mise à jour d'une lot dans le cas ou le traitement s'est fait sans erreur.
     * 
     * @param status fourni lors du demarrage de la transaction
     */
    @Override
    public void commiterTransactionEnglobeeDeMiseAJourDUnLot(TransactionStatus status)
    {
        UtilitaireTransactionSurScrollIterator.commitTransaction(transactionmanagerstandard, status);
    }

    /**
     * Permet de demarrer une transaction englobante sur un scroll iterator La transaction est demarree sur un
     * transaction manager specifique sans cache pour permettre l'apurement dans hibernate des caches de type
     * ActionQueue qui ne sont pas purgés par des commandes clear() et flush() En fin de parcours du scrolliterator la
     * transaction doit etre fermee par un appel à commiterTransactionEnglobanteDeLectureScrollIterator. Utiliser une
     * clause finally. Comme il s'agit d'une transaction en lecture, il n'y pas de rollback on peut commiter même en cas
     * d'exception
     * 
     * @return le transaction status retourné doit etre fourni pour le commit
     */
    @Override
    public TransactionStatus demarrerTransactionEnglobanteDeLectureScrollIterator()
    {
        return UtilitaireTransactionSurScrollIterator
            .demarrerTransactionEnglobanteDeLectureScrollIterator(transactionmanagersanscache);
    }

    /**
     * Permet de demarrer une transaction englobee pour faire des mises à jour à l'interieur d'une boucle de scroll
     * iterator En fin de traitement de mise à jour la transaction doit etre fermee par un appel à
     * commiterTransactionEnglobeeEnglobeeDeMiseAJourDUnLot. Si une exception est captee ou une erreur de traitement
     * detectee, il faut annuler le traitement du lot par un appel à
     * rollBackSurTransactionEnglobeeEnglobeeDeMiseAJourDUnLot
     * 
     * @return le transaction status retourné doit etre fourni pour le commit ou le rollback
     */
    @Override
    public TransactionStatus demarrerTransactionEnglobeeDeMiseAJourDUnLot()
    {
        return UtilitaireTransactionSurScrollIterator
            .demarrerTransactionEnglobeeDeMiseAJourDUnLot(transactionmanagerstandard);
    }

    /**
     * Accesseur de l attribut transactionmanagersanscache.
     * 
     * @return transactionmanagersanscache
     */
    @Override
    public HibernateTransactionManager getTransactionmanagersanscache()
    {
        return transactionmanagersanscache;
    }

    /**
     * Accesseur de l attribut transactionmanagerstandard.
     * 
     * @return transactionmanagerstandard
     */
    @Override
    public HibernateTransactionManager getTransactionmanagerstandard()
    {
        return transactionmanagerstandard;
    }

    /**
     * Permet de fermer la transaction en fin mise à jour d'une lot dans le cas ou le traitement s'est mal déroulé pour
     * annuler tout le lot de mise à jour.
     * 
     * @param status fourni lors du demarrage de la transaction
     */
    @Override
    public void rollBackSurTransactionEnglobeeDeMiseAJourDUnLot(TransactionStatus status)
    {
        UtilitaireTransactionSurScrollIterator.rollbackTransaction(transactionmanagerstandard, status);
    }

    /**
     * Modificateur de l attribut transactionmanagersanscache.
     * 
     * @param scrolliteratortransactionmanager le nouveau transactionmanagersanscache
     */

    public void setTransactionmanagersanscache(HibernateTransactionManager scrolliteratortransactionmanager)
    {
        this.transactionmanagersanscache = scrolliteratortransactionmanager;
    }

    /**
     * Modificateur de l attribut transactionmanagerstandard.
     * 
     * @param standardtransactionmanager le nouveau transactionmanagerstandard
     */

    public void setTransactionmanagerstandard(HibernateTransactionManager standardtransactionmanager)
    {
        this.transactionmanagerstandard = standardtransactionmanager;
    }

}
