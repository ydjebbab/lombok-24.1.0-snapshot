/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 *
 */
package fr.gouv.finances.lombok.util.base;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import org.hibernate.FlushMode;
import org.hibernate.Hibernate;
import org.springframework.orm.ObjectRetrievalFailureException;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.orm.hibernate5.support.HibernateDaoSupport;

import fr.gouv.finances.lombok.util.persistance.ModeCritereRecherche;

/**
 * Implémentation du socle des DAO Hibernate.
 *
 * @author amleplatinec
 * @author Christophe Breheret-Girardin
 */
public class BaseDaoImpl extends HibernateDaoSupport implements BaseDao
{

    /** Constante du message concernant les méthodes non implémentée */
    private static final String MESSAGE_METHODE_NON_IMPLEMENTEE = "Non implementée en Hibernate natif";

    public BaseDaoImpl()
    {
        super(); 
    }

    /** 
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.util.base.BaseDao#getHibernateTemplate(boolean).
     */
    @Override
    public HibernateTemplate getHibernateTemplate(boolean isCreerNouvelInstance)
    {
        if (isCreerNouvelInstance)
        {
            return createHibernateTemplate(getSessionFactory());
        }
        return getHibernateTemplate();
    }
    
    /** 
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.util.base.CoreBaseDao#clearSession().
     */
    @Override
    public void clearSession()
    {
        getHibernateTemplate().clear();
    }
    
    /** 
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.util.base.CoreBaseDao#clearPersistenceContext().
     */
    @Override
    public void clearPersistenceContext()
    {
        clearSession();
    }

    /** 
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.util.base.CoreBaseDao#deleteObject(java.lang.Class, java.io.Serializable).
     */
    @Override
    public <T> void deleteObject(Class<T> clazz, Serializable id)
    {
        getHibernateTemplate().delete(getObject(clazz, id));
    }

    /** 
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.util.base.CoreBaseDao#deleteObject(java.lang.Object).
     */
    @Override
    public <T> void deleteObject(T object)
    {
        // Le flush force l'exécution de la requête et permet de
        // récupérer une éventuelle erreur de lock optimiste avant la fin de la transaction
        HibernateTemplate hibernateTemplate = getHibernateTemplate(true);
        hibernateTemplate.getSessionFactory().getCurrentSession().setFlushMode(FlushMode.AUTO);
        hibernateTemplate.delete(object);
    }

    /** 
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.util.base.CoreBaseDao#evictObject(java.lang.Object).
     */
    @Override
    public <T> void evictObject(T object)
    {
        getHibernateTemplate().evict(object);
    }

    /** 
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.util.base.CoreBaseDao#flush().
     */
    @Override
    public void flush()
    {
        getHibernateTemplate().flush();
    }

    /** 
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.util.base.CoreBaseDao#getObject(java.lang.Class, java.io.Serializable).
     */
    @Override
    public <T> T getObject(Class<T> clazz, Serializable id)
    {
        T object = getHibernateTemplate(true).get(clazz, id);

        if (object == null)
        {
            throw new ObjectRetrievalFailureException(clazz, id);
        }

        return object;
    }

    /** 
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.util.base.CoreBaseDao#loadAllObjects(java.lang.Class).
     */
    @Override
    public <T> List<T> loadAllObjects(Class<T> clazz)
    {
        return getHibernateTemplate(true).loadAll(clazz);
    }

    /** 
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.util.base.CoreBaseDao#saveObject(java.lang.Object).
     */
    @Override
    public <T> void saveObject(final T o)
    {
        getHibernateTemplate().saveOrUpdate(o);
    }
    
    /** 
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.util.base.CoreBaseDao#modifyObject(java.lang.Object).
     */
    @Override
    public <T> T modifyObject(T entite)
    {
        return getHibernateTemplate().execute(session ->
        {
            return getHibernateTemplate().merge(entite);
        });
    }

    /** 
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.util.base.CoreBaseDao#refresh(java.lang.Object).
     */
    @Override
    public <T> void refresh(T entite)
    {
        getHibernateTemplate().refresh(entite);
    }


    @Override
    public <T> T findbyUniqueCriteria(Class<T> classe, String critere, String valeurCritere)
    {
        throw new UnsupportedOperationException(MESSAGE_METHODE_NON_IMPLEMENTEE);
    }

    @Override
    public <T> T findbyUniqueCriteria(Class<T> classe, String critere, String valeurCritere, ModeCritereRecherche modeCritereRecherche)
    {
        throw new UnsupportedOperationException(MESSAGE_METHODE_NON_IMPLEMENTEE);
    }
    
    @Override
    public <T> List<T> findAllByUniqueCriteria(Class<T> classe, String critere, String valeurCritere)
    {
        throw new UnsupportedOperationException(MESSAGE_METHODE_NON_IMPLEMENTEE);
    }

    @Override
    public <T> T findByCriterias(Class<T> classe, Map<String, String> criteres, ModeCritereRecherche modeCritereRecherche)
    {
        throw new UnsupportedOperationException(MESSAGE_METHODE_NON_IMPLEMENTEE);
    }

    @Override
    public <T> List<T> findAllByCriterias(Class<T> classe, Map<String, String> criteres, ModeCritereRecherche modeCritereRecherche)
    {
        throw new UnsupportedOperationException(MESSAGE_METHODE_NON_IMPLEMENTEE);
    }

    /** 
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.util.base.CoreBaseDao#countTotalNumberOfLines(java.lang.Class, java.util.Map, fr.gouv.finances.lombok.util.persistance.ModeCritereRecherche)
     */
    @Override
    public <T> long countTotalNumberOfLines(Class<T> classe, Map<String, String> criteres, ModeCritereRecherche modeCritereRecherche)
    {
        throw new UnsupportedOperationException(MESSAGE_METHODE_NON_IMPLEMENTEE);
    }

    /** 
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.util.base.CoreBaseDao#isCharged(java.lang.Object)
     */
    @Override
    public boolean isCharged(Object entite)
    {
        return Hibernate.isInitialized(entite);
    }
}