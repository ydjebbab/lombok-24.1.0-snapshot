/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 */
package fr.gouv.finances.lombok.upload.dao.impl;

import java.util.Collection;

import org.hibernate.FetchMode;
import org.hibernate.criterion.Restrictions;

import fr.gouv.finances.lombok.upload.bean.FichierJoint;
import fr.gouv.finances.lombok.upload.dao.FichierJointDao;
import fr.gouv.finances.lombok.util.base.BaseDaoImpl;
import fr.gouv.finances.lombok.util.exception.ProgrammationException;

/**
 * Implémentation Hibernate de gestion des données des fichiers joints.
 * 
 * @author amleplatinec
 * @author Christophe Breheret-Girardin
 */
public class FichierJointDaoImpl extends BaseDaoImpl implements FichierJointDao
{
    public FichierJointDaoImpl()
    {
        super();
    }

    /** 
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.upload.dao.FichierJointDao#deleteCollectionFichiersJoints(java.util.Collection).
     */
    @Override
    public void deleteCollectionFichiersJoints(Collection<FichierJoint> fichiersasupprimer)
    {
        // Suppression de tous les fichiers joints
        getHibernateTemplate().deleteAll(fichiersasupprimer);
    }

    /** 
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.upload.dao.FichierJointDao#findFichierJointEtSonContenuParId(java.lang.Long).
     */
    @Override
    public FichierJoint findFichierJointEtSonContenuParId(Long identifiant)
    {
        // Aucun paramètre fourni
        if (identifiant == null)
        {
            throw new ProgrammationException("Aucun identifiant de fichier joint fourni");
        }

        // Exécution de la requête
        return (FichierJoint) getHibernateTemplate().execute(session ->
            session.createCriteria(FichierJoint.class)
                .add(Restrictions.idEq(identifiant))
                .setFetchMode("leContenuDuFichier", FetchMode.JOIN)
                .uniqueResult());
    }
}