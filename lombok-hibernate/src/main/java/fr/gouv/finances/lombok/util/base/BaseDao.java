/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 *
 */
package fr.gouv.finances.lombok.util.base;

import org.hibernate.SessionFactory;
import org.springframework.orm.hibernate5.HibernateTemplate;

/**
 * Interface du socle des DAO Hibernate.
 *
 * @author amleplatinec
 * @author Christophe Breheret-Girardin
 */
public interface BaseDao extends CoreBaseDao
{
    /**
     * Accesseur du gestionnaire de Session Hibernate.
     * 
     * @return le gestionnaire de Session Hibernate
     */
    abstract SessionFactory getSessionFactory();

    /**
     * Méthode permettant de récupérer une instance de l'utilitaire Spring permettant de faciliter
     * l'utilisation d'Hibernate.
     *
     * @param isCreerNouvelInstance true s'il faut fournir une nouvelle instance, false pour fournir
     * l'instance courante
     * @return une instance de l'utilitaire Spring permettant de faciliter l'utilisation d'Hibernate
     */
    HibernateTemplate getHibernateTemplate(boolean isCreerNouvelInstance);
}
