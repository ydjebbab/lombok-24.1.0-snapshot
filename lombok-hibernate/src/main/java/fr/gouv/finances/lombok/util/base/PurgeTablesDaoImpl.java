/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 */
package fr.gouv.finances.lombok.util.base;

import java.util.Arrays;

import fr.gouv.finances.lombok.util.base.PurgeTablesDao;

/**
 * Implémentation Hibernate de la purge de tables de la base de données.
 *
 * @author Christophe Breheret-Girardin
 */
public class PurgeTablesDaoImpl extends BaseDaoImpl implements PurgeTablesDao
{
    /**
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.util.base.PurgeTablesDao#purgerTables(java.lang.String[])
     */
    @Override
    public void purgerTables(String... nomsTable)
    {
        // Exécution d'un ordre pour tronquer chaque table
        // (instruction immédiate par rapport à une suppression, et qui ne nécessite pas d'instruction "commit"
        Arrays.asList(nomsTable).stream().forEach(nomTable -> getHibernateTemplate().execute(
                session -> session.createSQLQuery("truncate table " + nomTable).executeUpdate()));
    }

}