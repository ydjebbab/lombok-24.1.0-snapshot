/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : GenericEnumUserType.java
 *
 */
package fr.gouv.finances.lombok.util.hibernate;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SessionImplementor;
import org.hibernate.type.SingleColumnType;
import org.hibernate.type.TypeResolver;
import org.hibernate.usertype.EnhancedUserType;
import org.hibernate.usertype.ParameterizedType;
import org.hibernate.usertype.UserType;
import org.springframework.util.ClassUtils;

/**
 * Classe permettant de gérer la persistance des enumérations par Hibernate
 * 
 * @author wpetit-cp
 * @version $Revision: 1.3 $ Date 8 déc. 2009
 */
public class GenericEnumUserType implements UserType, ParameterizedType, EnhancedUserType
{

    /** Constant : DEFAULT_IDENTIFIER_METHOD_NAME. */
    private static final String DEFAULT_IDENTIFIER_METHOD_NAME = "name";

    /** Constant : DEFAULT_VALUE_OF_METHOD_NAME. */
    private static final String DEFAULT_VALUE_OF_METHOD_NAME = "valueOf";

    /** enum class. */
    private Class<? extends Enum> enumClass;

    /** identifier type. */
    private Class<?> identifierType;

    /** identifier method. */
    private Method identifierMethod;

    /** value of method. */
    private Method valueOfMethod;

    /** type. */
    private SingleColumnType type;

    /** sql types. */
    private int[] sqlTypes;

    /**
     * Constructeur de la classe GenericEnumUserType.java
     *
     */
    public GenericEnumUserType()
    {
        super(); 
        
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param cached
     * @param owner
     * @return object
     * @throws HibernateException the hibernate exception
     * @see org.hibernate.usertype.UserType#assemble(java.io.Serializable, java.lang.Object)
     */
    @Override
    public Object assemble(Serializable cached, Object owner) throws HibernateException
    {
        return cached;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param value
     * @return object
     * @throws HibernateException the hibernate exception
     * @see org.hibernate.usertype.UserType#deepCopy(java.lang.Object)
     */
    @Override
    public Object deepCopy(Object value) throws HibernateException
    {
        return value;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param value
     * @return serializable
     * @throws HibernateException the hibernate exception
     * @see org.hibernate.usertype.UserType#disassemble(java.lang.Object)
     */
    @Override
    public Serializable disassemble(Object value) throws HibernateException
    {
        return (Serializable) value;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param object1
     * @param object2
     * @return true, si c'est vrai
     * @throws HibernateException the hibernate exception
     * @see org.hibernate.usertype.UserType#equals(java.lang.Object, java.lang.Object)
     */
    @Override
    public boolean equals(Object object1, Object object2) throws HibernateException
    {
        return object1 == object2;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param xmlValue
     * @return object
     * @see org.hibernate.usertype.EnhancedUserType#fromXMLString(java.lang.String)
     */
    @Override
    public Object fromXMLString(String xmlValue)
    {
        Object result = null;
        if (xmlValue != null)
        {
            try
            {
                result = valueOfMethod.invoke(enumClass, new Object[] {xmlValue});
            }
            catch (IllegalArgumentException arge)
            {
                throw new HibernateException(arge);
            }
            catch (IllegalAccessException acce)
            {
                throw new HibernateException(acce);
            }
            catch (InvocationTargetException inve)
            {
                throw new HibernateException(inve);
            }
        }
        return result;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param object
     * @return int
     * @throws HibernateException the hibernate exception
     * @see org.hibernate.usertype.UserType#hashCode(java.lang.Object)
     */
    @Override
    public int hashCode(Object object) throws HibernateException
    {
        return object.hashCode();
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @return true, si c'est mutable
     * @see org.hibernate.usertype.UserType#isMutable()
     */
    @Override
    public boolean isMutable()
    {
        return false;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param resultset
     * @param names
     * @param owner
     * @return object
     * @throws HibernateException the hibernate exception
     * @throws SQLException the SQL exception
     */
    @Override
    public Object nullSafeGet(ResultSet resultset, String[] names, SessionImplementor session, Object owner) throws HibernateException,
        SQLException
    {
        Object result = null;

        Object identifier = type.get(resultset, names[0], session);

        if (!(resultset.wasNull() || identifier == null))
        {

            try
            {
                result = valueOfMethod.invoke(enumClass, new Object[] {identifier});
            }
            catch (IllegalArgumentException illegale)
            {
                leveException(illegale);
            }
            catch (IllegalAccessException illegale)
            {
                leveException(illegale);
            }
            catch (InvocationTargetException invoce)
            {
                leveException(invoce);
            }
        }
        return result;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param prepst
     * @param value
     * @param index
     * @throws HibernateException the hibernate exception
     * @throws SQLException the SQL exception
     */
    @Override
    public void nullSafeSet(PreparedStatement prepst, Object value, int index, SessionImplementor session) throws HibernateException, SQLException
    {
        try
        {
            if (value == null)
            {
                prepst.setNull(index, type.sqlType());
            }
            else
            {
                Object identifier = identifierMethod.invoke(value, new Object[0]);
                type.set(prepst, identifier, index, session);
            }
        }
        catch (IllegalArgumentException invoce)
        {
            leveException(invoce);
        }
        catch (IllegalAccessException invoce)
        {
            leveException(invoce);
        }
        catch (InvocationTargetException invoce)
        {
            leveException(invoce);
        }
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param value
     * @return string
     * @see org.hibernate.usertype.EnhancedUserType#objectToSQLString(java.lang.Object)
     */
    @Override
    public String objectToSQLString(Object value)
    {
        String result = "";
        if (value != null)
        {
            try
            {
                Object identifier = identifierMethod.invoke(value, new Object[0]);
                if (identifier != null)
                {
                    result = identifier.toString();
                }
            }
            catch (IllegalArgumentException arge)
            {
                throw new HibernateException(arge);
            }
            catch (IllegalAccessException acce)
            {
                throw new HibernateException(acce);
            }
            catch (InvocationTargetException inve)
            {
                throw new HibernateException(inve);
            }
        }

        return result;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param original
     * @param target
     * @param owner
     * @return object
     * @throws HibernateException the hibernate exception
     * @see org.hibernate.usertype.UserType#replace(java.lang.Object, java.lang.Object, java.lang.Object)
     */
    @Override
    public Object replace(Object original, Object target, Object owner) throws HibernateException
    {
        return original;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @return class
     * @see org.hibernate.usertype.UserType#returnedClass()
     */
    @Override
    public Class returnedClass()
    {
        return enumClass;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param parameters le nouveau parameter values
     * @see org.hibernate.usertype.ParameterizedType#setParameterValues(java.util.Properties)
     */
    @Override
    public void setParameterValues(Properties parameters)
    {
        String enumClassName = parameters.getProperty("enumClass");
        try
        {
            enumClass = ClassUtils.forName(enumClassName, null).asSubclass(Enum.class);
        }
        catch (ClassNotFoundException cfne)
        {
            throw new HibernateException("Enum class not found", cfne);
        }

        String identifierMethodName = parameters.getProperty("identifierMethod", DEFAULT_IDENTIFIER_METHOD_NAME);

        try
        {
            identifierMethod = enumClass.getMethod(identifierMethodName, new Class[0]);
            identifierType = identifierMethod.getReturnType();
        }
        catch (SecurityException secue)
        {
            throw new HibernateException("Failed to obtain identifier method", secue);
        }
        catch (NoSuchMethodException nosuche)
        {
            throw new HibernateException("Failed to obtain identifier method", nosuche);
        }

        type = (SingleColumnType) new TypeResolver().basic(identifierType.getName());
//         type = (NullableType) TypeFactory.basic(identifierType.getName());

        if (type == null)
        {
            throw new HibernateException("Unsupported identifier type " + identifierType.getName());
        }

        sqlTypes = new int[] {type.sqlType()};

        String valueOfMethodName = parameters.getProperty("valueOfMethod", DEFAULT_VALUE_OF_METHOD_NAME);

        try
        {
            valueOfMethod = enumClass.getMethod(valueOfMethodName, new Class[] {identifierType});
        }
        catch (SecurityException secue)
        {
            throw new HibernateException("Failed to obtain identifier method", secue);
        }
        catch (NoSuchMethodException nosuche)
        {
            throw new HibernateException("Failed to obtain identifier method", nosuche);
        }

    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @return int[]
     * @see org.hibernate.usertype.UserType#sqlTypes()
     */
    @Override
    public int[] sqlTypes()
    {
        return sqlTypes;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param value
     * @return string
     * @see org.hibernate.usertype.EnhancedUserType#toXMLString(java.lang.Object)
     */
    @Override
    public String toXMLString(Object value)
    {
        return objectToSQLString(value);
    }

    /**
     * Lève une exception lors de l'exécution de la méthode <code>valueOfMethod</code>.
     * 
     * @param illegale exception à lever
     */
    private void leveException(Exception illegale)
    {
        throw new HibernateException("Exception while invoking valueOf method '" + valueOfMethod.getName() + "' of "
            + "enumeration class '" + enumClass + "'", illegale);
    }

}