/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 */
package fr.gouv.finances.lombok.monnaie.dao.impl;

import fr.gouv.finances.lombok.monnaie.bean.TauxDeChancellerie;
import fr.gouv.finances.lombok.monnaie.dao.TauxDeChancellerieDao;
import fr.gouv.finances.lombok.util.base.BaseDaoImpl;

/**
 * Implémentation Hibernate du DAO permettant de gérer les données du taux de chancellerie.
 *
 * @author amleplatinec-cp
 * @author Christophe Breheret-Girardin
 */
public class TauxDeChancellerieDaoImpl extends BaseDaoImpl implements TauxDeChancellerieDao
{

    public TauxDeChancellerieDaoImpl()
    {
        super(); 
    }

    /** 
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.monnaie.dao.TauxDeChancellerieDao#deleteTauxChancellerie(
     * fr.gouv.finances.lombok.monnaie.bean.TauxDeChancellerie).
     */
    @Override
    public void deleteTauxChancellerie(TauxDeChancellerie unTauxDeChancellerie)
    {
        this.deleteObject(unTauxDeChancellerie);
    }

    /** 
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.monnaie.dao.TauxDeChancellerieDao#modifyTauxChancellerie(
     * fr.gouv.finances.lombok.monnaie.bean.TauxDeChancellerie).
     */
    @Override
    public void modifyTauxChancellerie(TauxDeChancellerie unTauxDeChancellerie)
    {
        this.saveObject(unTauxDeChancellerie);
    }

    /** 
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.monnaie.dao.TauxDeChancellerieDao#saveTauxChancellerie(
     * fr.gouv.finances.lombok.monnaie.bean.TauxDeChancellerie).
     */
    @Override
    public void saveTauxChancellerie(TauxDeChancellerie unTauxDeChancellerie)
    {
        this.saveObject(unTauxDeChancellerie);
    }
}