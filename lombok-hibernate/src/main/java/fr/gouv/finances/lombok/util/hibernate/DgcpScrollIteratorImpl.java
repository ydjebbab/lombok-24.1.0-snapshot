/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 */
package fr.gouv.finances.lombok.util.hibernate;

import org.hibernate.ScrollableResults;

import fr.gouv.finances.lombok.util.persistance.AbstractScrollIteratorCore;

/**
 * Implémentation des recherches en mode curseur.
 * 
 * @author chouard-cp
 * @author Christophe Breheret-Girardin
 */
public class DgcpScrollIteratorImpl extends AbstractScrollIteratorCore
{
    /** L'objet Hibernate ScrollableResults à travers lequel on itère. */
    protected ScrollableResults scrollableResults;

    /**
     * Constructeur.
     *
     * @param scrollableResults utilitaire de recherche en mode curseur
     */
    public DgcpScrollIteratorImpl(ScrollableResults scrollableResults)
    {
        super();
        this.scrollableResults = scrollableResults;
    }

    /** 
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.util.persistance.ScrollIterator#close()
     */
    @Override
    public void close()
    {
        if (!isClosed)
        {
            if (scrollableResults != null)
            {
                scrollableResults.close();
            }

            scrollableResults = null;
        }

        isClosed = true;
    }

    /** 
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.util.persistance.ScrollIterator#hasNext()
     */
    @Override
    public boolean hasNext()
    {
        if (nextRow == null && !isClosed)
        {
            if (scrollableResults.next())
            {
                nextRow = scrollableResults.get();
            }
            else
            {
                close();
            }
        }

        return (nextRow != null);
    }
}