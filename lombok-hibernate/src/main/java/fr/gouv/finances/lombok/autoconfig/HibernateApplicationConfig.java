package fr.gouv.finances.lombok.autoconfig;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.context.annotation.Profile;

/**
 * Classe de configuration qui permet de conditionner les ressources à importer
 * 
 * @author celfer Date: 18 févr. 2020
 */
@Configuration
public class HibernateApplicationConfig
{

    private static final Logger log = LoggerFactory.getLogger(HibernateApplicationConfig.class);

    @Configuration
    @Profile("embedded")
    @ImportResource({"classpath:conf/applicationContext-datasources-hibernate-embedded-commun.xml"})
    public static class HibernateEmbeddedApplicationConfig
    {
        private static final Logger log = LoggerFactory.getLogger(HibernateEmbeddedApplicationConfig.class);

        @PostConstruct
        public void init()
        {
            log.debug(">>> Après initialisation de HibernateEmbeddedApplicationConfig");
        }
    }

    @Configuration
    @Profile("postgre")
    @ImportResource({"classpath:conf/applicationContext-datasources-hibernate-postgresql-commun.xml"})
    public static class HibernatePostgresqlApplicationConfig
    {
        private static final Logger log = LoggerFactory.getLogger(HibernatePostgresqlApplicationConfig.class);

        @PostConstruct
        public void init()
        {
            log.debug(">>> Après initialisation de HibernatePostgresqlApplicationConfig");
        }
    }

    @Configuration
    @ConditionalOnExpression("'${lombok.orm.jpa}' == 'false' && '${lombok.composant.batchdao.inclus}' == 'false'")
    @ImportResource({"classpath:conf/applicationContext-commun-dao-hibernate.xml"})
    public static class HibernateRequisApplicationConfig
    {
        private static final Logger log = LoggerFactory.getLogger(HibernateRequisApplicationConfig.class);

        @PostConstruct
        public void init()
        {
            log.debug(">>> Après initialisation de HibernateRequisApplicationConfig");
        }
    }

    @Configuration
    @ConditionalOnExpression("'${lombok.orm.jpa}' == 'false' && '${lombok.composant.batchoptimisation.inclus}' == 'true'")
    @ImportResource({"classpath:conf/applicationContext-commun-batchoptimisation.xml",
            "classpath:conf/applicationContext-datasources-hibernate-embedded-batchoptimisation.xml",
            "classpath:conf/applicationContext-datasources-hibernate-postgresql-batchoptimisation.xml"})
    public static class HibernateEtBatchOptimisationApplicationConfig
    {
        private static final Logger log = LoggerFactory.getLogger(HibernateEtBatchOptimisationApplicationConfig.class);

        @PostConstruct
        public void init()
        {
            log.debug(">>> Après initialisation de HibernateEtBatchOptimisationApplicationConfig");
        }

    }

    /**
     * applicationContext-commun-batchoptimisation.xml était importé avec les profils batchoptimisation ou edition
     * @author celinio fernandes
     * Date: Feb 20, 2020
     */
    @Configuration
    @ConditionalOnExpression("'${lombok.orm.jpa}' == 'false' && '${lombok.composant.edition.inclus}' == 'true'")
    @ImportResource({"classpath:conf/applicationContext-commun-batchoptimisation.xml",
            "classpath:conf/applicationContext-datasources-hibernate-embedded-edition.xml",
            "classpath:conf/applicationContext-datasources-hibernate-postgresql-edition.xml"})
    public static class HibernateEtEditionApplicationConfig
    {
        private static final Logger log = LoggerFactory.getLogger(HibernateEtEditionApplicationConfig.class);

        @PostConstruct
        public void init()
        {
            log.debug(">>> Après initialisation de HibernateEtEditionApplicationConfig");
        }

    }

    /**
     * applicationContext-upload-dao.xml était importé avec les profils upload ou atlas
     * @author celinio fernandes
     * Date: Feb 20, 2020
     */
    @Configuration
    @ConditionalOnExpression("'${lombok.orm.jpa}' == 'false' and ('${lombok.composant.upload.inclus}' == 'true' or '${lombok.composant.atlas.inclus}' == 'true')")
    @ImportResource({"classpath:conf/applicationContext-upload-dao.xml"})
    public static class HibernateUploadOuAtlasApplicationConfig
    {
        private static final Logger log = LoggerFactory.getLogger(HibernateUploadOuAtlasApplicationConfig.class);

        @PostConstruct
        public void init()
        {
            log.debug(">>> Après initialisation de HibernateUploadOuAtlasApplicationConfig");
        }

    }

    @Configuration
    @ConditionalOnExpression("'${lombok.orm.jpa}' == 'false' && '${lombok.composant.batchdao.inclus}' == 'true'")
    @ImportResource({"classpath:conf/applicationContext-commun-dao-hibernate-batchdao.xml"})
    public static class HibernateEtBatchDaoApplicationConfig
    {
        private static final Logger log = LoggerFactory.getLogger(HibernateEtBatchDaoApplicationConfig.class);

        @PostConstruct
        public void init()
        {
            log.debug(">>> Après initialisation de HibernateEtBatchDaoApplicationConfig");
        }

    }

}
