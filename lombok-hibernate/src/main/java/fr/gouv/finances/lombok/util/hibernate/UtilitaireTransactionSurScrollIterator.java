/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : UtilitaireTransactionSurScrollIterator.java
 *
 */
package fr.gouv.finances.lombok.util.hibernate;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

/**
 * <pre>
 * Classe utilitaire portant le code d'ouverture et de fermeture 
 * - d'une transaction en lecture seule sans cache servant principalement de transaction englobante pour les mise à jour avec un
 * DgcpScrollIterator étant lié à un curseur (un DgcpScrollIterator ne fonctionne que qu'à l'intérieur d'une
 * transaction) 
 * - d'une transaction en ecriture avec cache servant principalement de transaction secondaires pour les
 * mises à jour des lots lus avec le scroll iterator Ce système à pour but de purger le cache hibernate après chaque
 * mise à jour par lot. En effet Hibernate purge un certain nombre de caches lorsqu'on lance des commandes flush() et
 * clear() mais maintient un cache d'historique des commandes de mise à jour et des objets impactés (cache appele
 * ActionQueue).
 * A noter que les syntaxes de déclenchement de transactions par callback ne peuvent permettre d'obtenir
 * les mêmes fonctionnalités.
 * La classe utilitaire peut être utilisée en statique directement ou via
 * UtilitaireTransactionSurScrollIteratorSupportService qui se chargera de tenir les références sur les transactions
 * managers.
 * 
 * @author lcontinsouzas-cp
 * @version $Revision: 1.2 $ Date: 15 déc. 2009
 */
public class UtilitaireTransactionSurScrollIterator
{

    protected static final Log log = LogFactory.getLog(UtilitaireTransactionSurScrollIterator.class);

    /**
     * methode Commit transaction : --.
     * 
     * @param transactionmanager --
     * @param status --
     */
    public static void commitTransaction(HibernateTransactionManager transactionmanager, TransactionStatus status)
    {
        transactionmanager.commit(status);
        log.debug("Commit d'une transaction effectué");
    }

    /**
     * methode Demarrer transaction : --.
     * 
     * @param untransactionmanager --
     * @param name --
     * @param propagationBehavior --
     * @param readonly --
     * @return transaction status
     */
    public static TransactionStatus demarrerTransaction(HibernateTransactionManager untransactionmanager, String name,
        int propagationBehavior, boolean readonly)
    {
        DefaultTransactionDefinition def = new DefaultTransactionDefinition();
        def.setName(name);
        def.setPropagationBehavior(propagationBehavior);
        def.setReadOnly(readonly);
        TransactionStatus status = untransactionmanager.getTransaction(def);
        log.debug("Demarrage d'une transaction");
        return status;
    }

    /**
     * methode Demarrer transaction englobante de lecture scroll iterator : --.
     * 
     * @param scrolliteratortransactionmanager --
     * @return transaction status
     */
    public static TransactionStatus demarrerTransactionEnglobanteDeLectureScrollIterator(
        HibernateTransactionManager scrolliteratortransactionmanager)
    {
        TransactionStatus status =
            demarrerTransaction(scrolliteratortransactionmanager, "transaction englobante lecture seule",
                TransactionDefinition.PROPAGATION_NESTED, true);
        log.debug("Demarrage d'une transaction englobante effectue");
        return status;
    }

    /**
     * methode Demarrer transaction englobee de mise a jour d un lot : --.
     * 
     * @param standardtransactionmanager --
     * @return transaction status
     */
    public static TransactionStatus demarrerTransactionEnglobeeDeMiseAJourDUnLot(
        HibernateTransactionManager standardtransactionmanager)
    {
        TransactionStatus status =
            demarrerTransaction(standardtransactionmanager, "transaction englobee mise à jour",
                TransactionDefinition.PROPAGATION_REQUIRES_NEW, false);
        log.debug("Demarrage d'une transaction englobee effectue");
        return status;
    }

    /**
     * methode Rollback transaction : --.
     * 
     * @param scrolliteratortransactionmanager --
     * @param status --
     */
    public static void rollbackTransaction(HibernateTransactionManager scrolliteratortransactionmanager,
        TransactionStatus status)
    {
        scrolliteratortransactionmanager.rollback(status);
        log.debug("Rollback d'une transaction effectué");
    }

}
