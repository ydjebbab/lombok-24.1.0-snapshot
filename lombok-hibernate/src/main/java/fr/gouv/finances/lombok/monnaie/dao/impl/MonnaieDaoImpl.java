/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 */
package fr.gouv.finances.lombok.monnaie.dao.impl;

import java.util.Iterator;
import java.util.List;

import org.hibernate.FetchMode;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.DistinctRootEntityResultTransformer;
import org.springframework.orm.hibernate5.HibernateTemplate;

import fr.gouv.finances.lombok.monnaie.bean.Monnaie;
import fr.gouv.finances.lombok.monnaie.dao.MonnaieDao;
import fr.gouv.finances.lombok.util.base.BaseDaoImpl;

/**
 * Implémentation Hibernate du DAO permettant de gérer les données monnaie.
 *
 * @author amleplatinec-cp
 * @author Christophe Breheret-Girardin
 */
public class MonnaieDaoImpl extends BaseDaoImpl implements MonnaieDao
{
    public MonnaieDaoImpl()
    {
        super();
    }

    /** 
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.monnaie.dao.MonnaieDao#deleteMonnaie(
     * fr.gouv.finances.lombok.monnaie.bean.Monnaie)
     */
    @Override
    public void deleteMonnaie(Monnaie uneMonnaie)
    {
        this.deleteObject(uneMonnaie);
    }

    /** 
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.monnaie.dao.MonnaieDao#findAllMonnaies()
     */
    @Override
    @SuppressWarnings("unchecked")
    public List<Monnaie> findAllMonnaies()
    {
        DetachedCriteria detachedCriteria = DetachedCriteria.forClass(Monnaie.class);
        detachedCriteria.setFetchMode("listTauxChancel", FetchMode.JOIN);
        detachedCriteria.setResultTransformer(DistinctRootEntityResultTransformer.INSTANCE);
        return (List<Monnaie>) getHibernateTemplate().findByCriteria(detachedCriteria);
    }

    /** 
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.monnaie.dao.MonnaieDao#findAllMonnaiesTriSurLibelle()
     */
    @Override
    @SuppressWarnings("unchecked")
    public List<Monnaie> findAllMonnaiesTriSurLibelle()
    {
        DetachedCriteria detachedCriteria = DetachedCriteria.forClass(Monnaie.class);
        detachedCriteria.setFetchMode("listTauxChancel", FetchMode.JOIN);
        detachedCriteria.addOrder(Order.asc("libelleMonnaie"));
        detachedCriteria.setResultTransformer(DistinctRootEntityResultTransformer.INSTANCE);
        return (List<Monnaie>) this.getHibernateTemplate().findByCriteria(detachedCriteria);
    }

    /** 
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.monnaie.dao.MonnaieDao#findMonnaieParCode(java.lang.String)
     */
    @Override
    public Monnaie findMonnaieParCode(String code)
    {
        DetachedCriteria detachedCriteria = DetachedCriteria.forClass(Monnaie.class);
        detachedCriteria.add(Restrictions.eq("codeBDF", code));
        @SuppressWarnings("unchecked")
        Iterator<Monnaie> result = ((List<Monnaie>) getHibernateTemplate().findByCriteria(detachedCriteria)).iterator();
        if (result.hasNext())
        {
            return result.next();
        }
        else
        {
            return null;
        }
    }

    /** 
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.monnaie.dao.MonnaieDao#modifyMonnaie(fr.gouv.finances.lombok.monnaie.bean.Monnaie)
     */
    @Override
    public void modifyMonnaie(Monnaie uneMonnaie)
    {
        this.saveObject(uneMonnaie);
    }

    /** 
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.monnaie.dao.MonnaieDao#saveMonnaie(fr.gouv.finances.lombok.monnaie.bean.Monnaie)
     */
    @Override
    public boolean saveMonnaie(Monnaie uneMonnaie)
    {
        return saveMonnaieEtTaux(uneMonnaie);
    }

    /** 
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.monnaie.dao.MonnaieDao#saveMonnaieEtTaux(fr.gouv.finances.lombok.monnaie.bean.Monnaie)
     */
    @Override
    public boolean saveMonnaieEtTaux(Monnaie uneMonnaie)
    {
        if (this.findMonnaieParCode(uneMonnaie.getCodeBDF()) == null)
        {
            HibernateTemplate hibernateTemplate = getHibernateTemplate();
            hibernateTemplate.saveOrUpdate(uneMonnaie);
            return true;
        }
        else
        {
            return false;
        }
    }
}