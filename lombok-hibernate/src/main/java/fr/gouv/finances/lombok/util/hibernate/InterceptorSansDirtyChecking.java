/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : InterceptorSansDirtyChecking.java
 *
 */
package fr.gouv.finances.lombok.util.hibernate;

import java.io.Serializable;

import org.hibernate.EmptyInterceptor;
import org.hibernate.type.Type;

/**
 * Class InterceptorSansDirtyChecking 
 * 
 * @author amleplatinec
 * @version $Revision: 1.2 $ Date: 11 déc. 2009
 */
public class InterceptorSansDirtyChecking extends EmptyInterceptor
{

    private static final long serialVersionUID = 1L;

    public InterceptorSansDirtyChecking()
    {
        super();         
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param entity
     * @param id
     * @param currentState
     * @param previousState
     * @param propertyNames
     * @param types
     * @return int[]
     * @see org.hibernate.EmptyInterceptor#findDirty(java.lang.Object, java.io.Serializable, java.lang.Object[],
     *      java.lang.Object[], java.lang.String[], org.hibernate.type.Type[])
     */
    @Override
    public int[] findDirty(Object entity, Serializable id, Object[] currentState, Object[] previousState,
        String[] propertyNames, Type[] types)
    {
        return new int[0];
    }

}
