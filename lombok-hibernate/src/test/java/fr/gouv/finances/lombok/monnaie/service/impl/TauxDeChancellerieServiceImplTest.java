/*
 * Copyright (c) 2016 DGFiP - Tous droits réservés
 * 
 */
package fr.gouv.finances.lombok.monnaie.service.impl;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;

import org.junit.Before;
import org.junit.Test;

import fr.gouv.finances.lombok.monnaie.bean.Monnaie;
import fr.gouv.finances.lombok.monnaie.bean.TauxDeChancellerie;
import fr.gouv.finances.lombok.monnaie.dao.impl.TauxDeChancellerieDaoImpl;

/**
 * TauxDeChancellerieServiceImplTest.java
 * 
 * @author christian.houard Date: 24 oct. 2016
 */
public class TauxDeChancellerieServiceImplTest
{

    /** tx service. */
    private TauxDeChancellerieServiceImpl txService;

    /**
     * methode Setup mock.
     */
    @Before
    public void setupMock()
    {
        txService = new TauxDeChancellerieServiceImpl();
        txService.setTauxdechancelleriedao(mock(TauxDeChancellerieDaoImpl.class));

    }

    /**
     * Test method for
     * {@link fr.gouv.finances.lombok.monnaie.service.impl.TauxDeChancellerieServiceImpl#TauxDeChancellerieServiceImpl()}
     * .
     *
     * @throws Exception the exception
     */
    @Test
    public final void testTauxDeChancellerieServiceImpl() throws Exception
    {
        assertNotNull(txService);

    }

    /**
     * Test method for
     * {@link fr.gouv.finances.lombok.monnaie.service.impl.TauxDeChancellerieServiceImpl#ajouterTauxChancellerieAMonnaie(fr.gouv.finances.lombok.monnaie.bean.TauxDeChancellerie, fr.gouv.finances.lombok.monnaie.bean.Monnaie)}
     * .
     *
     * @throws Exception the exception
     */
    @Test
    public final void testAjouterTauxChancellerieAMonnaie() throws Exception
    {

        txService.ajouterTauxChancellerieAMonnaie(mock(TauxDeChancellerie.class), mock(Monnaie.class));

    }

    /**
     * Test method for
     * {@link fr.gouv.finances.lombok.monnaie.service.impl.TauxDeChancellerieServiceImpl#creerTauxChancellerie(fr.gouv.finances.lombok.monnaie.bean.TauxDeChancellerie)}
     * .
     *
     * @throws Exception the exception
     */
    @Test
    public final void testCreerTauxChancellerie() throws Exception
    {
        txService.creerTauxChancellerie(mock(TauxDeChancellerie.class));

    }

    /**
     * Test method for
     * {@link fr.gouv.finances.lombok.monnaie.service.impl.TauxDeChancellerieServiceImpl#modifierTauxChancellerie(fr.gouv.finances.lombok.monnaie.bean.TauxDeChancellerie)}
     * .
     *
     * @throws Exception the exception
     */
    @Test
    public final void testModifierTauxChancellerie() throws Exception
    {
        txService.modifierTauxChancellerie(mock(TauxDeChancellerie.class));

    }

    /**
     * Test method for
     * {@link fr.gouv.finances.lombok.monnaie.service.impl.TauxDeChancellerieServiceImpl#supprimerTauxChancellerie(fr.gouv.finances.lombok.monnaie.bean.TauxDeChancellerie)}
     * .
     *
     * @throws Exception the exception
     */
    @Test
    public final void testSupprimerTauxChancellerie() throws Exception
    {
       txService.supprimerTauxChancellerie(mock(TauxDeChancellerie.class));

    }

}
