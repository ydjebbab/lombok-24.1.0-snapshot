package fr.gouv.finances.lombok.monnaie.dao.impl;

import org.junit.runner.RunWith;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.gouv.finances.lombok.monnaie.dao.MonnaieDaoCoreTest;

/**
 * Tests unitaires automatisés du DAO Hibernate gérant les données de monnaie.
 *
 * @author Christophe Breheret-Girardin
 */
@ContextConfiguration({"classpath:/conf/applicationContext-dao-test.xml",
    "classpath*:/conf/applicationContext-core-persistance-test.xml",
    "classpath:/conf/applicationContext-core-orm-test.xml",
    "classpath*:/conf/applicationContext-monnaie-dao-test.xml"})
@ActiveProfiles(profiles = {"hibernate"})
@RunWith(SpringJUnit4ClassRunner.class)
public class MonnaieDaoTest extends MonnaieDaoCoreTest
{
    /**
     * Constructeur.
     */
    public MonnaieDaoTest()
    {
        super();
    }
}