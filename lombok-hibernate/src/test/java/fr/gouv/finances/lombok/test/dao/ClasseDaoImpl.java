package fr.gouv.finances.lombok.test.dao;

import java.util.List;

import org.hibernate.LockMode;
import org.hibernate.ScrollMode;
import org.springframework.orm.hibernate5.HibernateTemplate;

import fr.gouv.finances.lombok.util.base.BaseDaoImpl;
import fr.gouv.finances.lombok.util.hibernate.DgcpScrollIteratorImpl;
import fr.gouv.finances.lombok.util.persistance.ScrollIterator;

/**
 * Implémentation Hibernate API d'un DAO sur des classes d'élèves.
 *
 * @author Christophe Breheret-Girardin
 */
class ClasseDaoImpl extends BaseDaoImpl implements ClasseDao
{
    
    /** 
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.test.dao.ClasseDao#setMaxResultat(int)
     */
    @Override
    public void setMaxResultat(int maxResultat)
    {
        getHibernateTemplate(false).setMaxResults(maxResultat);
    }

    /** 
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.test.dao.ClasseDao#getScrollIterator()
     */
    @Override
    public ScrollIterator getScrollIterator(int nombreOccurences)
    {
        return getHibernateTemplate(true).execute(session ->
            new DgcpScrollIteratorImpl(session.createCriteria(Classe.class)
                            .setReadOnly(true)
                            .setLockMode(LockMode.NONE)
                            .setFetchSize(nombreOccurences)
                            // Activation du mode curseur
                            .scroll(ScrollMode.FORWARD_ONLY)));
    }

    /** 
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.test.dao.ClasseDao#getMaxClassesEtanche(int)
     */
    @Override
    public List<Classe> getMaxClassesEtanche(int maxResultat)
    {
        HibernateTemplate hibernateTemplate = getHibernateTemplate(true);
        hibernateTemplate.setMaxResults(maxResultat);
        return hibernateTemplate.loadAll(Classe.class);
    }

    /** 
     * {@inheritDoc}<br/>
     * <u>Remarque</u> : cette méthode est fonctionnelle avec l'implémentation Hibernate API.
     * @see fr.gouv.finances.lombok.test.dao.ClasseDao#getMaxClassesNonEtanche(int)
     */
    @Override
    public List<Classe> getMaxClassesNonEtanche(int maxResultat)
    {
        HibernateTemplate hibernateTemplate = getHibernateTemplate(false);
        hibernateTemplate.setMaxResults(maxResultat);
        return hibernateTemplate.loadAll(Classe.class);
    }

    /** 
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.test.dao.ClasseDao#getMaxClassesViaContextePersistance(int)
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<Classe> getMaxClassesViaContextePersistance(int maxResultat)
    {
        return getHibernateTemplate().execute(session ->
        {
            return (List<Classe>) session.createCriteria(Classe.class)
                .setMaxResults(maxResultat)
                .list();
        });
    }

    /** 
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.test.dao.ClasseDao#getClasses()
     */
    @Override
    public List<Classe> getClasses()
    {
        HibernateTemplate hibernateTemplate = getHibernateTemplate(false);
        return hibernateTemplate.loadAll(Classe.class);
    }
}