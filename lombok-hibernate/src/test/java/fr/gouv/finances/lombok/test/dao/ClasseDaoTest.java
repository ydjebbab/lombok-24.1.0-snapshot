package fr.gouv.finances.lombok.test.dao;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import fr.gouv.finances.lombok.test.controle.VerificationExecution;
import fr.gouv.finances.lombok.util.hibernate.DgcpScrollIteratorImpl;
import fr.gouv.finances.lombok.util.persistance.ModeCritereRecherche;
import fr.gouv.finances.lombok.util.persistance.ScrollIterator;

/**
 * Tests unitaires automatisés permettant de tester le socle Hibernate des DAO.
 *
 * @author Christophe Breheret-Girardin
 */
@ContextConfiguration({"classpath*:/conf/applicationContext-core-persistance-test.xml",
    "classpath:/conf/applicationContext-hibernate-test.xml"
    ,"classpath:/conf/applicationContext-dao-test.xml"
    ,"classpath*:/conf/applicationContext-commun-transaction.xml"})
@ActiveProfiles(profiles = {"test-socle_lombok", "hibernate"})
@RunWith(SpringJUnit4ClassRunner.class)
public class ClasseDaoTest extends ClasseDaoCoreTest
{
    /**
     * Constructeur.
     */
    public ClasseDaoTest()
    {
        super();
    }

    /**
     * Initialisation des données en base avant chaque test.
     */
    @Before
    public void initialisation()
    {
        classedao.setMaxResultat(0);
    }

    /**
     * Test de fr.gouv.finances.lombok.util.base.BaseDaoImpl#getEntite(
     * java.lang.Class, java.lang.String, java.lang.String).
     */
    @Test
    public void maxClassesNonEtanche()
    {
        // Utilisation de transactions manuelles clôturées à chaque requête, afin de montrer que le paramétrage
        // est tout de même conservé

        // Récupération de tous les éléments
        verifierClassesSansEleve(recupererTout(), mapClasse.get(1), mapClasse.get(2), mapClasse.get(3)
            , mapClasse.get(4), mapClasse.get(5));

        // Récupération des 2 premiers éléments et vérifications
        List<Classe> classes = getTransactionTemplate().execute(status -> classedao.getMaxClassesNonEtanche(2));
        verifierClassesSansEleve(classes, mapClasse.get(1), mapClasse.get(2));

        // Récupération de tous les éléments (= 2 éléments, la configuration étant conservée)
        verifierClassesSansEleve(recupererTout(), mapClasse.get(1), mapClasse.get(2));
    }

    /**
     * Test de fr.gouv.finances.lombok.util.base.BaseDaoImpl#getEntite(
     * java.lang.Class, java.lang.String, java.lang.String).
     */
    @Test
    public void maxClassesEtanche()
    {
        // Récupération de tous les éléments
        verifierClassesSansEleve(recupererTout(), mapClasse.get(1), mapClasse.get(2), mapClasse.get(3)
            , mapClasse.get(4), mapClasse.get(5));

        // Récupération des deux premiers éléments et vérifications
        List<Classe> classes = getTransactionTemplate().execute(status -> classedao.getMaxClassesEtanche(2));
        verifierClassesSansEleve(classes, mapClasse.get(1), mapClasse.get(2));

        // Récupération de tous les éléments
        verifierClassesSansEleve(recupererTout(), mapClasse.get(1), mapClasse.get(2), mapClasse.get(3)
            , mapClasse.get(4), mapClasse.get(5));
    }

    /**
     * Test de fr.gouv.finances.lombok.util.base.BaseDaoImpl#getEntite(
     * java.lang.Class, java.lang.String, java.lang.String).
     */
    @Test
    public void getEntiteUnCritere()
    {
        VerificationExecution.verifierException(UnsupportedOperationException.class
            , () -> classedao.findbyUniqueCriteria(Classe.class, "", ""));
    }

    /**
     * Test de fr.gouv.finances.lombok.util.base.BaseDaoImpl#getEntites(
     * java.lang.Class, java.lang.String, java.lang.String).
     */
    @Test
    public void getEntitesUnCritere()
    {
        VerificationExecution.verifierException(UnsupportedOperationException.class
            , () -> classedao.findAllByUniqueCriteria(Classe.class, "", ""));
    }

    /**
     * Test de fr.gouv.finances.lombok.util.base.BaseDaoImpl#getEntite(
     * java.lang.Class, java.util.Map, fr.gouv.finances.lombok.util.persistance.ModeCritereRecherche).
     */
    @Test
    public void getEntiteCriteres()
    {
        VerificationExecution.verifierException(UnsupportedOperationException.class
            , () -> classedao.findByCriterias(Classe.class, new HashMap<>(), ModeCritereRecherche.EQUAL));
    }

    /**
     * Test de fr.gouv.finances.lombok.util.base.BaseDaoImpl#getEntites(
     * java.lang.Class, java.util.Map, fr.gouv.finances.lombok.util.persistance.ModeCritereRecherche).
     */
    @Test
    public void getEntitesCriteres()
    {
        VerificationExecution.verifierException(UnsupportedOperationException.class
            , () -> classedao.findAllByCriterias(Classe.class, new HashMap<>(), ModeCritereRecherche.EQUAL));
    }

    /**
     * Test de fr.gouv.finances.lombok.util.base.BaseDaoImpl#getEntites(java.lang.Class, java.util.Map).
     */
    @Test
    public void getNombreEntite()
    {
        VerificationExecution.verifierException(UnsupportedOperationException.class
            , () -> classedao.countTotalNumberOfLines(Classe.class, new HashMap<>(), ModeCritereRecherche.EQUAL));
    }

    /**
     * Méthode permettant de vérifier la méthode
     * fr.gouv.finances.lombok.util.persistance.ScrollIterator#close().
     */
    @Test
    @Transactional
    public final void scrollIteratorClose()
    {
        // Test de la non-erreur sur une fermeture d'itérateur sans résultat
        ScrollIterator scrollIterator = new DgcpScrollIteratorImpl(null);
        assertFalse(scrollIterator.isClosed());
        scrollIterator.close();
        assertTrue(scrollIterator.isClosed());
    }
}