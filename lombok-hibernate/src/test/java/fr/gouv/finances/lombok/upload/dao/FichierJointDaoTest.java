package fr.gouv.finances.lombok.upload.dao;

import org.junit.runner.RunWith;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Tests unitaires automatisés du DAO Hibernate gérant les données des fichiers joints.
 *
 * @author Christophe Breheret-Girardin
 */
@ContextConfiguration({"classpath:/conf/applicationContext-dao-test.xml",
    "classpath*:/conf/applicationContext-core-persistance-test.xml",
    "classpath:/conf/applicationContext-core-orm-test.xml",
    "classpath*:/conf/applicationContext-upload-dao.xml"})
@ActiveProfiles(profiles = {"upload", "hibernate"})
@RunWith(SpringJUnit4ClassRunner.class)
public class FichierJointDaoTest extends FichierJointDaoCoreTest
{
    /**
     * Constructeur.
     */
    public FichierJointDaoTest()
    {
        super();
    }
}