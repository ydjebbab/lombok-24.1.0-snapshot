/*
 * Copyright (c) 2016 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.monnaie.dao.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Calendar;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.ObjectRetrievalFailureException;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import fr.gouv.finances.lombok.monnaie.bean.TauxDeChancellerie;

/**
 * Class TauxDeChancellerieDaoImplTest .
 */
@ContextConfiguration(locations = "classpath:/conf/application-context-test.xml")
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class TauxDeChancellerieDaoImplTest
{

    /** tx dao. */
    @Autowired
    private TauxDeChancellerieDaoImpl txDAO;

    /**
     * methode Test taux de chancellerie dao impl : .
     *
     * @throws Exception the exception
     */
    @Test
    public final void testTauxDeChancellerieDaoImpl() throws Exception
    {
        assertNotNull(txDAO);
    }

    /** thrown. */
    @Rule
    public ExpectedException thrown = ExpectedException.none();

    /**
     * methode Test delete taux chancellerie : .
     *
     * @throws Exception the exception
     */
    @Test
    public final void testDeleteTauxChancellerie() throws Exception
    {
        TauxDeChancellerie tx1 = new TauxDeChancellerie();
        tx1.setDateTauxChancel(Calendar.getInstance().getTime());
        tx1.setValeurTauxChancel(Double.valueOf(25.00));
        txDAO.saveObject(tx1);

        assertNotNull(txDAO.getObject(TauxDeChancellerie.class, tx1.getId()));

        txDAO.deleteTauxChancellerie(tx1);

        thrown.expect(ObjectRetrievalFailureException.class);
        thrown.expectMessage("Object of class [fr.gouv.finances.lombok.monnaie.bean.TauxDeChancellerie");
        txDAO.getObject(TauxDeChancellerie.class, tx1.getId());

    }

    /**
     * methode Test modify taux chancellerie : .
     *
     * @throws Exception the exception
     */
    @Test
    public final void testModifyTauxChancellerie() throws Exception
    {
        // certainement à revoir (pour cohérence avec hibernate et les transactions)
        TauxDeChancellerie tx1 = new TauxDeChancellerie();
        tx1.setDateTauxChancel(Calendar.getInstance().getTime());
        tx1.setValeurTauxChancel(Double.valueOf(25.00));
        txDAO.saveObject(tx1);

        tx1.setValeurTauxChancel(Double.valueOf(50.00));

        txDAO.modifyTauxChancellerie(tx1);

        TauxDeChancellerie txTest = (TauxDeChancellerie) txDAO.getObject(TauxDeChancellerie.class, tx1.getId());

        assertEquals(txTest.getValeurTauxChancel(), tx1.getValeurTauxChancel());

    }

    /**
     * methode Test save taux chancellerie : .
     *
     * @throws Exception the exception
     */
    @Test
    @Rollback(true)
    public final void testSaveTauxChancellerie() throws Exception
    {
        TauxDeChancellerie tx1 = new TauxDeChancellerie();
        tx1.setDateTauxChancel(Calendar.getInstance().getTime());
        tx1.setValeurTauxChancel(Double.valueOf(444.00));
        txDAO.saveTauxChancellerie(tx1);

    }

}
