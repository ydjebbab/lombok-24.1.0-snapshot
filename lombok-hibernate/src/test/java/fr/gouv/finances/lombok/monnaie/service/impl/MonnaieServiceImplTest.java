/*
 * Copyright (c) 2016 DGFiP - Tous droits réservés
 * 
 */
package fr.gouv.finances.lombok.monnaie.service.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Calendar;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import fr.gouv.finances.lombok.monnaie.bean.Monnaie;
import fr.gouv.finances.lombok.monnaie.bean.TauxDeChancellerie;
import fr.gouv.finances.lombok.monnaie.dao.MonnaieDao;
import fr.gouv.finances.lombok.monnaie.dao.impl.MonnaieDaoImpl;
import fr.gouv.finances.lombok.util.exception.RegleGestionException;

/**
 * MonnaieServiceImplTest.java Tests unitaires de la classe de service
 * 
 * @author christian.houard Date: 24 oct. 2016
 */
public class MonnaieServiceImplTest
{
    /** monnaiedao. */
    private MonnaieDao monnaieDao;

    /** monnaie service. */
    private MonnaieServiceImpl monnaieService;

    /**
     * methode Setup mock
     */
    @Before
    public void setupMock()
    {
        monnaieService = new MonnaieServiceImpl();
        monnaieDao = mock(MonnaieDaoImpl.class);
        monnaieService.setMonnaiedao(monnaieDao);
        monnaieService.setTauxdechancellerieserviceso(mock(TauxDeChancellerieServiceImpl.class));
    }

    /**
     * Test method for {@link fr.gouv.finances.lombok.monnaie.service.impl.MonnaieServiceImpl#MonnaieServiceImpl()}.
     *
     * @throws Exception the exception
     */
    @Test
    public final void testMonnaieServiceImpl() throws Exception
    {
        assertNotNull(monnaieService);

    }

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    /**
     * Test method for
     * {@link fr.gouv.finances.lombok.monnaie.service.impl.MonnaieServiceImpl#creerMonnaie(fr.gouv.finances.lombok.monnaie.bean.Monnaie)}
     * .
     *
     * @throws Exception the exception
     */
    @Test
    public final void testCreerMonnaie() throws Exception
    {
        Monnaie monn = new Monnaie();
        monn.setCodeBDF("toto");

        // premier appel va retourner true pour tester la sauvegarde,
        // deuxième appel va retourner false pour tester la levée d'exception
        when(monnaieDao.saveMonnaie(monn)).thenReturn(true).thenReturn(false);
        monnaieService.creerMonnaie(monn);

        thrown.expect(RegleGestionException.class);

        monnaieService.creerMonnaie(monn);

    }

    /**
     * Test method for
     * {@link fr.gouv.finances.lombok.monnaie.service.impl.MonnaieServiceImpl#creerMonnaieEtTaux(fr.gouv.finances.lombok.monnaie.bean.Monnaie)}
     * .
     *
     * @throws Exception the exception
     */
    @Test
    public final void testCreerMonnaieEtTaux() throws Exception
    {
        Monnaie monn = new Monnaie();
        monn.setCodeBDF("toto");
        monn.setTauxChancellerie(mock(TauxDeChancellerie.class));

        // premier appel va retourner true pour tester la sauvegarde,
        // deuxième appel va retourner false pour tester la levée d'exception
        when(monnaieDao.saveMonnaie(monn)).thenReturn(true).thenReturn(false);
        monnaieService.creerMonnaie(monn);

        thrown.expect(RegleGestionException.class);

        monnaieService.creerMonnaieEtTaux(monn);


    }

    /**
     * Test method for {@link fr.gouv.finances.lombok.monnaie.service.impl.MonnaieServiceImpl#getMonnaiedao()}.
     *
     * @throws Exception the exception
     */
    @Test
    public final void testGetMonnaiedao() throws Exception
    {
        // test accesseur qui ne sert pas à gd chose
        assertNotNull(monnaieService.getMonnaiedao());

    }

    /**
     * Test method for
     * {@link fr.gouv.finances.lombok.monnaie.service.impl.MonnaieServiceImpl#getTauxdechancellerieserviceso()}.
     *
     * @throws Exception the exception
     */
    @Test
    public final void testGetTauxdechancellerieserviceso() throws Exception
    {
        assertNotNull(monnaieService.getTauxdechancellerieserviceso());

    }

    /**
     * Test method for
     * {@link fr.gouv.finances.lombok.monnaie.service.impl.MonnaieServiceImpl#modifierMonnaie(fr.gouv.finances.lombok.monnaie.bean.Monnaie)}
     * .
     *
     * @throws Exception the exception
     */
    @Test
    public final void testModifierMonnaie() throws Exception
    {
        
        monnaieService.modifierMonnaie(mock(Monnaie.class));

    }

    /**
     * Test method for
     * {@link fr.gouv.finances.lombok.monnaie.service.impl.MonnaieServiceImpl#rechercherDernierTauxChancellerie(fr.gouv.finances.lombok.monnaie.bean.Monnaie)}
     * .
     *
     * @throws Exception the exception
     */
    @Test
    public final void testRechercherDernierTauxChancellerie() throws Exception
    {
        Monnaie monn = new Monnaie();
        TauxDeChancellerie txc = new TauxDeChancellerie();
        txc.setDateTauxChancel(Calendar.getInstance().getTime());
        
        Set<TauxDeChancellerie> setTx = new HashSet<TauxDeChancellerie>();
        setTx.add(txc);
        
        monn.setListTauxChancel(setTx);
        
        // on devrait retrouver en dernier tx le tx inséré.
        // à compléter par un meilleur test
        assertEquals(monnaieService.rechercherDernierTauxChancellerie(monn),txc);

    }

    /**
     * Test method for
     * {@link fr.gouv.finances.lombok.monnaie.service.impl.MonnaieServiceImpl#rechercherMonnaieParCode(java.lang.String)}
     * .
     *
     * @throws Exception the exception
     */
    @Test
    public final void testRechercherMonnaieParCode() throws Exception
    {
        when(monnaieDao.findMonnaieParCode("afn".toUpperCase(Locale.getDefault()))).thenReturn(mock(Monnaie.class));
        assertNotNull(monnaieService.rechercherMonnaieParCode("afn"));

    }

    /**
     * Test method for
     * {@link fr.gouv.finances.lombok.monnaie.service.impl.MonnaieServiceImpl#rechercherToutesMonnaies()}.
     *
     * @throws Exception the exception
     */
    @Test
    public final void testRechercherToutesMonnaies() throws Exception
    {
        assertTrue(monnaieService.rechercherToutesMonnaies().isEmpty());

    }

 
    /**
     * Test method for
     * {@link fr.gouv.finances.lombok.monnaie.service.impl.MonnaieServiceImpl#supprimerMonnaieEtTaux(fr.gouv.finances.lombok.monnaie.bean.Monnaie)}
     * .
     *
     * @throws Exception the exception
     */
    @Test
    public final void testSupprimerMonnaieEtTaux() throws Exception
    {
        monnaieService.supprimerMonnaieEtTaux(mock(Monnaie.class));

    }

}
