/*
 * Copyright (c) 2016 DGFiP - Tous droits réservés
 * 
 */
package fr.gouv.finances.lombok.journal.mvc.form;

import fr.gouv.finances.lombok.test.pojo.AbstractCorePojoTest;

/**
 * Tests unitaires du POJO RechercheOperationsJournalForm.
 *
 * @param  RechercheOperationsJournalWSForm formulaire lié à la recherche d'opération dans le journal
 * via service Web
 *
 * @author Christophe Breheret-Girardin
 */
public class RechercheOperationsJournalWSFormTest extends AbstractCorePojoTest< RechercheOperationsJournalWSForm>
{   
    /**
     * Constructeur.
     */
    public RechercheOperationsJournalWSFormTest()
    {
        super();
    }

}
