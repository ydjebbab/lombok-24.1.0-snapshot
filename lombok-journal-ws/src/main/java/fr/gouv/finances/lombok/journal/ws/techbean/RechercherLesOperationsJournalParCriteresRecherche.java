/*
 * Copyright (c) 2011 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.journal.ws.techbean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import fr.gouv.finances.lombok.journal.bean.CriteresRecherchesJournal;

/**
 * <p>
 * Java class for rechercherLesOperationsJournalParCriteresRecherche complex type.
 * <p>
 * The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name=&quot;rechercherLesOperationsJournalParCriteresRecherche&quot;&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base=&quot;{http://www.w3.org/2001/XMLSchema}anyType&quot;&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref=&quot;{http://ws.journal.lombok.finances.gouv.fr/bean}crit&quot; minOccurs=&quot;0&quot;/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "rechercherLesOperationsJournalParCriteresRecherche", propOrder = {"crit"})
public class RechercherLesOperationsJournalParCriteresRecherche
{

    /** crit. */
    @XmlElement(namespace = "http://ws.journal.lombok.finances.gouv.fr/bean")
    protected CriteresRecherchesJournal crit;

    /**
     * Constructeur de la classe RechercherLesOperationsJournalParCriteresRecherche.java
     *
     */
    public RechercherLesOperationsJournalParCriteresRecherche()
    {
        super();
        
    }

    /**
     * Gets the value of the crit property.
     * 
     * @return possible object is {@link CriteresRecherchesJournal }
     */
    public CriteresRecherchesJournal getCrit()
    {
        return crit;
    }

    /**
     * Sets the value of the crit property.
     * 
     * @param value allowed object is {@link CriteresRecherchesJournal }
     */
    public void setCrit(CriteresRecherchesJournal value)
    {
        this.crit = value;
    }

}
