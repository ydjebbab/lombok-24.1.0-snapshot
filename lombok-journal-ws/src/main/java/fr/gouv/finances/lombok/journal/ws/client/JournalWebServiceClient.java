/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : JournalWebServiceClient.java
 *
 */
package fr.gouv.finances.lombok.journal.ws.client;

import java.net.MalformedURLException;
import java.net.URL;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceFeature;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.gouv.finances.lombok.journal.ws.JournalWebService;
import fr.gouv.finances.lombok.util.constantes.ConstantesGlobales;

/**
 * Class JournalWebServiceClient.
 * 
 * @author chouard-cp
 * @version $Revision: 1.3 $ Date: 15 déc. 2009
 */
@WebServiceClient(name = "JournalWebService", targetNamespace = "http://ws.journal.lombok.finances.gouv.fr")
public class JournalWebServiceClient extends Service
{
    private static final Logger LOGGER =
        LoggerFactory.getLogger(fr.gouv.finances.lombok.journal.ws.client.JournalWebServiceClient.class.getName());
   
    private static final URL JOURNALWEBSERVICE_WSDL_LOCATION;

    static
    {
        URL url = null;
        String webApp = ConstantesGlobales.WEB_ROOT_PATH;
        try
        {
            URL baseUrl;
            baseUrl = fr.gouv.finances.lombok.journal.ws.client.JournalWebServiceClient.class.getResource(".");
            url = new URL(baseUrl, webApp + "/WEB-INF/wsdl/JournalWebService.wsdl");
        }
        catch (MalformedURLException e)
        {
            LOGGER.warn("Failed to create URL for the wsdl Location: '" + webApp
                + "/WEB-INF/wsdl/JournalWebService.wsdl', retrying as a local file");
            LOGGER.warn(e.getMessage());
        }
        JOURNALWEBSERVICE_WSDL_LOCATION = url;
    }

    public JournalWebServiceClient()
    {
        super(JOURNALWEBSERVICE_WSDL_LOCATION, new QName("http://ws.journal.lombok.finances.gouv.fr",
            "JournalWebService"));
    }

    public JournalWebServiceClient(URL wsdlLocation, QName serviceName)
    {
        super(wsdlLocation, serviceName);
    }

    /**
     * Gets the journal web service port.
     * 
     * @return returns JournalWebService
     */
    @WebEndpoint(name = "JournalWebServicePort")
    public JournalWebService getJournalWebServicePort()
    {
        return super.getPort(new QName("http://ws.journal.lombok.finances.gouv.fr", "JournalWebServicePort"),
            JournalWebService.class);
    }

    /**
     * Gets the journal web service port.
     * 
     * @param features A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy. Supported features
     *        not in the <code>features</code> parameter will have their default values.
     * @return returns JournalWebService
     */
    @WebEndpoint(name = "JournalWebServicePort")
    public JournalWebService getJournalWebServicePort(WebServiceFeature... features)
    {
        return super.getPort(new QName("http://ws.journal.lombok.finances.gouv.fr", "JournalWebServicePort"),
            JournalWebService.class, features);
    }

}
