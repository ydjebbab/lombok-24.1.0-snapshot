/*
 * Copyright (c) 2011 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.journal.ws.techbean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;

/**
 * <p>
 * Java class for rechercheUneOperationJournalParIdUtilEtDateHeure complex type.
 * <p>
 * The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name=&quot;rechercheUneOperationJournalParIdUtilEtDateHeure&quot;&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base=&quot;{http://www.w3.org/2001/XMLSchema}anyType&quot;&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref=&quot;{http://ws.journal.lombok.finances.gouv.fr/bean}idUtilOuBatch&quot; minOccurs=&quot;0&quot;/&gt;
 *         &lt;element ref=&quot;{http://ws.journal.lombok.finances.gouv.fr/bean}dateHeureOperation&quot; minOccurs=&quot;0&quot;/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "rechercheUneOperationJournalParIdUtilEtDateHeure", propOrder = {"idUtilOuBatch", "dateHeureOperation"})
public class RechercheUneOperationJournalParIdUtilEtDateHeure
{

    /** id util ou batch. */
    @XmlElement(namespace = "http://ws.journal.lombok.finances.gouv.fr/bean")
    protected String idUtilOuBatch;

    /** date heure operation. */
    @XmlElement(namespace = "http://ws.journal.lombok.finances.gouv.fr/bean")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dateHeureOperation;

    /**
     * Constructeur de la classe RechercheUneOperationJournalParIdUtilEtDateHeure.java
     *
     */
    public RechercheUneOperationJournalParIdUtilEtDateHeure()
    {
        super(); 
        
    }

    /**
     * Gets the value of the dateHeureOperation property.
     * 
     * @return possible object is {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getDateHeureOperation()
    {
        return dateHeureOperation;
    }

    /**
     * Gets the value of the idUtilOuBatch property.
     * 
     * @return possible object is {@link String }
     */
    public String getIdUtilOuBatch()
    {
        return idUtilOuBatch;
    }

    /**
     * Sets the value of the dateHeureOperation property.
     * 
     * @param value allowed object is {@link XMLGregorianCalendar }
     */
    public void setDateHeureOperation(XMLGregorianCalendar value)
    {
        this.dateHeureOperation = value;
    }

    /**
     * Sets the value of the idUtilOuBatch property.
     * 
     * @param value allowed object is {@link String }
     */
    public void setIdUtilOuBatch(String value)
    {
        this.idUtilOuBatch = value;
    }

}
