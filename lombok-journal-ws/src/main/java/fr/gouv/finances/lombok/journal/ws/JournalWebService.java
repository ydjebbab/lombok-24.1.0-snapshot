/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : JournalWebService.java
 *
 */
package fr.gouv.finances.lombok.journal.ws;

import java.util.Date;
import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

import fr.gouv.finances.lombok.journal.bean.CriteresRecherchesJournal;
import fr.gouv.finances.lombok.journal.bean.IdentifiantsMetierObjOperation;
import fr.gouv.finances.lombok.journal.bean.OperationJournal;

/**
 * Interface JournalWebService.
 * 
 * @author chouard-cp
 * @version $Revision: 1.3 $ Date: 15 déc. 2009
 */
@WebService(name = "JournalWebService", targetNamespace = "http://ws.journal.lombok.finances.gouv.fr")
@SOAPBinding(style = SOAPBinding.Style.DOCUMENT, use = SOAPBinding.Use.LITERAL, parameterStyle = SOAPBinding.ParameterStyle.WRAPPED)
@XmlSeeAlso({fr.gouv.finances.lombok.journal.bean.ObjectFactory.class,
        fr.gouv.finances.lombok.journal.ws.techbean.ObjectFactory.class})
public interface JournalWebService
{

    /**
     * Rechercher les operations journal par criteres recherche.
     * 
     * @param crit --
     * @return returns fr.gouv.finances.lombok.journal.ws.OperationsJournal
     */
    @WebMethod
    @WebResult(name = "operationsJournal", targetNamespace = "http://ws.journal.lombok.finances.gouv.fr/bean")
    @RequestWrapper(localName = "rechercherLesOperationsJournalParCriteresRecherche", targetNamespace = "http://ws.journal.lombok.finances.gouv.fr", className = "fr.gouv.finances.lombok.journal.ws.techbean.RechercherLesOperationsJournalParCriteresRecherche")
    @ResponseWrapper(localName = "rechercherLesOperationsJournalParCriteresRechercheResponse", targetNamespace = "http://ws.journal.lombok.finances.gouv.fr", className = "fr.gouv.finances.lombok.journal.ws.techbean.RechercherLesOperationsJournalParCriteresRechercheResponse")
    public List<OperationJournal> rechercherLesOperationsJournalParCriteresRecherche(
        @WebParam(name = "crit", targetNamespace = "http://ws.journal.lombok.finances.gouv.fr/bean") CriteresRecherchesJournal crit);

    /**
     * methode Rechercher operations journal par identifiant metier obj operation : --.
     * 
     * @param unIdentifiantMetierObjOperation --
     * @return list< operation journal>
     */
    @WebMethod
    @WebResult(name = "operationsJournal", targetNamespace = "http://ws.journal.lombok.finances.gouv.fr/bean")
    @RequestWrapper(localName = "rechercherOperationsJournalParIdentifiantMetierObjOperation", targetNamespace = "http://ws.journal.lombok.finances.gouv.fr", className = "fr.gouv.finances.lombok.journal.ws.techbean.RechercherOperationsJournalParIdentifiantMetierObjOperation")
    @ResponseWrapper(localName = "rechercherOperationsJournalParIdentifiantMetierObjOperationResponse", targetNamespace = "http://ws.journal.lombok.finances.gouv.fr", className = "fr.gouv.finances.lombok.journal.ws.techbean.RechercherOperationsJournalParIdentifiantMetierObjOperationResponse")
    public List<OperationJournal> rechercherOperationsJournalParIdentifiantMetierObjOperation(
        @WebParam(name = "unIdentifiantMetierObjOperation", targetNamespace = "http://ws.journal.lombok.finances.gouv.fr/bean") IdentifiantsMetierObjOperation unIdentifiantMetierObjOperation);

    /**
     * methode Recherche une operation journal par id util et date heure : --.
     * 
     * @param idUtilOuBatch --
     * @param dateHeureOperation --
     * @return operation journal
     */
    @WebMethod
    @WebResult(name = "operationJournal", targetNamespace = "http://ws.journal.lombok.finances.gouv.fr/bean")
    @RequestWrapper(localName = "rechercheUneOperationJournalParIdUtilEtDateHeure", targetNamespace = "http://ws.journal.lombok.finances.gouv.fr", className = "fr.gouv.finances.lombok.journal.ws.techbean.RechercheUneOperationJournalParIdUtilEtDateHeure")
    @ResponseWrapper(localName = "rechercheUneOperationJournalParIdUtilEtDateHeureResponse", targetNamespace = "http://ws.journal.lombok.finances.gouv.fr", className = "fr.gouv.finances.lombok.journal.ws.techbean.RechercheUneOperationJournalParIdUtilEtDateHeureResponse")
    public OperationJournal rechercheUneOperationJournalParIdUtilEtDateHeure(
        @WebParam(name = "idUtilOuBatch", targetNamespace = "http://ws.journal.lombok.finances.gouv.fr/bean") String idUtilOuBatch,
        @WebParam(name = "dateHeureOperation", targetNamespace = "http://ws.journal.lombok.finances.gouv.fr/bean") Date dateHeureOperation);

}
