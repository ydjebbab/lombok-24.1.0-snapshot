/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : FabriqueClientWSJournalServiceImpl.java
 *
 */
package fr.gouv.finances.lombok.journal.ws.service.impl;

import java.net.URL;

import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;

import fr.gouv.finances.lombok.journal.ws.JournalWebService;
import fr.gouv.finances.lombok.journal.ws.client.JournalWebServiceClient;
import fr.gouv.finances.lombok.journal.ws.service.FabriqueClientWSJournalService;
import fr.gouv.finances.lombok.util.ws.SlpaUtil;

/**
 * Class FabriqueClientWSJournalServiceImpl --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.3 $ Date: 11 déc. 2009
 */
public class FabriqueClientWSJournalServiceImpl implements FabriqueClientWSJournalService
{
    static String SERVICE = "JournalWebService";

    static String NAMESPACE = "http://ws.journal.lombok.finances.gouv.fr";

    private String contrat;

    public FabriqueClientWSJournalServiceImpl()
    {
        super();         
    }

    /**
     * methode Creer journal web service client : --.
     * 
     * @return journal web service
     */
    @Override
    public JournalWebService creerJournalWebServiceClient()
    {
        // Récupération de l' url d'accès au service
        URL urlAccessPoint = SlpaUtil.rechercherLocalisationService(contrat, SERVICE);
        // Récupération de l' url d'accès au wsdl
        URL urlWsdl = SlpaUtil.rechercherLocalisationWSDL(contrat, SERVICE);

        QName qName = new QName(NAMESPACE, SERVICE);

        // Récupération d'une instance cliente
        JournalWebService client = new JournalWebServiceClient(urlWsdl, qName).getJournalWebServicePort();

        // Paramétrage de l'url d'accès au service: pour l'exécution des opérations
        ((BindingProvider) client).getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY,
            urlAccessPoint.toString());

        return client;
    }

    /**
     * Accesseur de l'attribut contrat.
     * 
     * @return contrat
     */
    public String getContrat()
    {
        return contrat;
    }

    /**
     * Modificateur de l'attribut contrat.
     * 
     * @param contrat le nouveau contrat
     */
    public void setContrat(String contrat)
    {
        this.contrat = contrat;
    }
}
