/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : JournalWebServiceImpl.java
 *
 */
package fr.gouv.finances.lombok.journal.ws.impl;

import java.util.Date;
import java.util.List;

import javax.jws.WebService;

import fr.gouv.finances.lombok.journal.bean.CriteresRecherchesJournal;
import fr.gouv.finances.lombok.journal.bean.IdentifiantsMetierObjOperation;
import fr.gouv.finances.lombok.journal.bean.OperationJournal;
import fr.gouv.finances.lombok.journal.service.JournalService;
import fr.gouv.finances.lombok.journal.ws.JournalWebService;

/**
 * Class JournalWebServiceImpl --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.3 $ Date: 11 déc. 2009
 */
@WebService(endpointInterface = "fr.gouv.finances.lombok.journal.ws.JournalWebService", targetNamespace = "http://ws.journal.lombok.finances.gouv.fr", serviceName = "JournalWebService", portName = "JournalWebServicePort", wsdlLocation = "/WEB-INF/wsdl/JournalWebService.wsdl")
public class JournalWebServiceImpl implements JournalWebService
{

    /** journalserviceso. */
    JournalService journalserviceso;

    public JournalWebServiceImpl()
    {
        super();         
    }

    /**
     * Accesseur de l attribut journalserviceso.
     * 
     * @return journalserviceso
     */
    public JournalService getJournalserviceso()
    {
        return journalserviceso;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param crit
     * @return list
     * @see fr.gouv.finances.lombok.journal.ws.JournalWebService#rechercherLesOperationsJournalParCriteresRecherche(fr.gouv.finances.lombok.journal.bean.CriteresRecherchesJournal)
     */
    @Override
    public List<OperationJournal> rechercherLesOperationsJournalParCriteresRecherche(CriteresRecherchesJournal crit)
    {
        return journalserviceso.rechercherLesOperationsJournalParCriteresRecherche(crit);
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param unIdentifiantMetierObjOperation
     * @return list
     * @see fr.gouv.finances.lombok.journal.ws.JournalWebService#rechercherOperationsJournalParIdentifiantMetierObjOperation(fr.gouv.finances.lombok.journal.bean.IdentifiantsMetierObjOperation)
     */
    @Override
    public List<OperationJournal> rechercherOperationsJournalParIdentifiantMetierObjOperation(
        IdentifiantsMetierObjOperation unIdentifiantMetierObjOperation)
    {
        return journalserviceso
            .rechercherOperationsJournalParIdentifiantMetierObjOperation(unIdentifiantMetierObjOperation);
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param idUtilOuBatch
     * @param dateHeureOperation
     * @return operation journal
     * @see fr.gouv.finances.lombok.journal.ws.JournalWebService#rechercheUneOperationJournalParIdUtilEtDateHeure(java.lang.String,
     *      java.util.Date)
     */
    @Override
    public OperationJournal rechercheUneOperationJournalParIdUtilEtDateHeure(String idUtilOuBatch,
        Date dateHeureOperation)
    {
        return journalserviceso.rechercheUneOperationJournalParIdUtilEtDateHeure(idUtilOuBatch, dateHeureOperation);
    }

    /**
     * Modificateur de l attribut journalserviceso.
     * 
     * @param journalserviceso le nouveau journalserviceso
     */
    public void setJournalserviceso(JournalService journalserviceso)
    {
        this.journalserviceso = journalserviceso;
    }

}
