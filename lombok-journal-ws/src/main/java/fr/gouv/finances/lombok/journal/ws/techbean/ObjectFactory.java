/*
 * Copyright (c) 2011 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.journal.ws.techbean;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;

/**
 * This object contains factory methods for each Java content interface and Java element interface generated in the
 * fr.gouv.finances.lombok.journal.ws package.
 * <p>
 * An ObjectFactory allows you to programatically construct new instances of the Java representation for XML content.
 * The Java representation of XML content can consist of schema derived interfaces and classes representing the binding
 * of schema type definitions, element declarations and model groups. Factory methods for each of these are provided in
 * this class.
 */
@XmlRegistry
public class ObjectFactory
{

    /** Constant : _RechercherOperationsJournalParIdentifiantMetierObjOperation_QNAME. */
    private static final QName _RechercherOperationsJournalParIdentifiantMetierObjOperation_QNAME =
        new QName("http://ws.journal.lombok.finances.gouv.fr",
            "rechercherOperationsJournalParIdentifiantMetierObjOperation");

    /** Constant : _RechercherOperationsJournalParIdentifiantMetierObjOperationResponse_QNAME. */
    private static final QName _RechercherOperationsJournalParIdentifiantMetierObjOperationResponse_QNAME =
        new QName("http://ws.journal.lombok.finances.gouv.fr",
            "rechercherOperationsJournalParIdentifiantMetierObjOperationResponse");

    /** Constant : _RechercheUneOperationJournalParIdUtilEtDateHeure_QNAME. */
    private static final QName _RechercheUneOperationJournalParIdUtilEtDateHeure_QNAME =
        new QName("http://ws.journal.lombok.finances.gouv.fr", "rechercheUneOperationJournalParIdUtilEtDateHeure");

    /** Constant : _RechercheUneOperationJournalParIdUtilEtDateHeureResponse_QNAME. */
    private static final QName _RechercheUneOperationJournalParIdUtilEtDateHeureResponse_QNAME =
        new QName("http://ws.journal.lombok.finances.gouv.fr",
            "rechercheUneOperationJournalParIdUtilEtDateHeureResponse");

    /** Constant : _RechercherLesOperationsJournalParCriteresRechercheResponse_QNAME. */
    private static final QName _RechercherLesOperationsJournalParCriteresRechercheResponse_QNAME =
        new QName("http://ws.journal.lombok.finances.gouv.fr",
            "rechercherLesOperationsJournalParCriteresRechercheResponse");

    /** Constant : _RechercherLesOperationsJournalParCriteresRecherche_QNAME. */
    private static final QName _RechercherLesOperationsJournalParCriteresRecherche_QNAME =
        new QName("http://ws.journal.lombok.finances.gouv.fr", "rechercherLesOperationsJournalParCriteresRecherche");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package:
     * fr.gouv.finances.lombok.journal.ws
     */
    public ObjectFactory()
    {
    }

    /**
     * Create an instance of {@link RechercherLesOperationsJournalParCriteresRecherche }.
     * 
     * @return the rechercher les operations journal par criteres recherche
     */
    public RechercherLesOperationsJournalParCriteresRecherche createRechercherLesOperationsJournalParCriteresRecherche()
    {
        return new RechercherLesOperationsJournalParCriteresRecherche();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RechercherLesOperationsJournalParCriteresRecherche }.
     * 
     * @param value
     * @return the JAXB element< rechercher les operations journal par criteres recherche> {@code >}
     */
    @XmlElementDecl(namespace = "http://techbean.ws.journal.lombok.finances.gouv.fr",
        name = "rechercherLesOperationsJournalParCriteresRecherche")
    public JAXBElement<RechercherLesOperationsJournalParCriteresRecherche> createRechercherLesOperationsJournalParCriteresRecherche(
        RechercherLesOperationsJournalParCriteresRecherche value)
    {
        return new JAXBElement<RechercherLesOperationsJournalParCriteresRecherche>(
            _RechercherLesOperationsJournalParCriteresRecherche_QNAME,
            RechercherLesOperationsJournalParCriteresRecherche.class, null, value);
    }

    /**
     * Create an instance of {@link RechercherLesOperationsJournalParCriteresRechercheResponse }.
     * 
     * @return the rechercher les operations journal par criteres recherche response
     */
    public RechercherLesOperationsJournalParCriteresRechercheResponse createRechercherLesOperationsJournalParCriteresRechercheResponse()
    {
        return new RechercherLesOperationsJournalParCriteresRechercheResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}.
     * 
     * @param value
     * @return the JAXB element< rechercher les operations journal par criteres recherche response>
     *         {@link RechercherLesOperationsJournalParCriteresRechercheResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://techbean.ws.journal.lombok.finances.gouv.fr",
        name = "rechercherLesOperationsJournalParCriteresRechercheResponse")
    public JAXBElement<RechercherLesOperationsJournalParCriteresRechercheResponse>
        createRechercherLesOperationsJournalParCriteresRechercheResponse(
            RechercherLesOperationsJournalParCriteresRechercheResponse value)
    {
        return new JAXBElement<RechercherLesOperationsJournalParCriteresRechercheResponse>(
            _RechercherLesOperationsJournalParCriteresRechercheResponse_QNAME,
            RechercherLesOperationsJournalParCriteresRechercheResponse.class, null, value);
    }

    /**
     * Create an instance of {@link RechercherOperationsJournalParIdentifiantMetierObjOperation }.
     * 
     * @return the rechercher operations journal par identifiant metier obj operation
     */
    public RechercherOperationsJournalParIdentifiantMetierObjOperation
        createRechercherOperationsJournalParIdentifiantMetierObjOperation()
    {
        return new RechercherOperationsJournalParIdentifiantMetierObjOperation();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}.
     * 
     * @param value
     * @return the JAXB element< rechercher operations journal par identifiant metier obj operation>
     *         {@link RechercherOperationsJournalParIdentifiantMetierObjOperation }{@code >}
     */
    @XmlElementDecl(namespace = "http://techbean.ws.journal.lombok.finances.gouv.fr",
        name = "rechercherOperationsJournalParIdentifiantMetierObjOperation")
    public JAXBElement<RechercherOperationsJournalParIdentifiantMetierObjOperation>
        createRechercherOperationsJournalParIdentifiantMetierObjOperation(
            RechercherOperationsJournalParIdentifiantMetierObjOperation value)
    {
        return new JAXBElement<RechercherOperationsJournalParIdentifiantMetierObjOperation>(
            _RechercherOperationsJournalParIdentifiantMetierObjOperation_QNAME,
            RechercherOperationsJournalParIdentifiantMetierObjOperation.class, null, value);
    }

    /**
     * Create an instance of {@link RechercherOperationsJournalParIdentifiantMetierObjOperationResponse }.
     * 
     * @return the rechercher operations journal par identifiant metier obj operation response
     */
    public RechercherOperationsJournalParIdentifiantMetierObjOperationResponse
        createRechercherOperationsJournalParIdentifiantMetierObjOperationResponse()
    {
        return new RechercherOperationsJournalParIdentifiantMetierObjOperationResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}.
     * 
     * @param value
     * @return the JAXB element< rechercher operations journal par identifiant metier obj operation response>
     *         {@link RechercherOperationsJournalParIdentifiantMetierObjOperationResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://techbean.ws.journal.lombok.finances.gouv.fr",
        name = "rechercherOperationsJournalParIdentifiantMetierObjOperationResponse")
    public JAXBElement<RechercherOperationsJournalParIdentifiantMetierObjOperationResponse>
        createRechercherOperationsJournalParIdentifiantMetierObjOperationResponse(
            RechercherOperationsJournalParIdentifiantMetierObjOperationResponse value)
    {
        return new JAXBElement<RechercherOperationsJournalParIdentifiantMetierObjOperationResponse>(
            _RechercherOperationsJournalParIdentifiantMetierObjOperationResponse_QNAME,
            RechercherOperationsJournalParIdentifiantMetierObjOperationResponse.class, null, value);
    }

    /**
     * Create an instance of {@link RechercheUneOperationJournalParIdUtilEtDateHeure }.
     * 
     * @return the recherche une operation journal par id util et date heure
     */
    public RechercheUneOperationJournalParIdUtilEtDateHeure
        createRechercheUneOperationJournalParIdUtilEtDateHeure()
    {
        return new RechercheUneOperationJournalParIdUtilEtDateHeure();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RechercheUneOperationJournalParIdUtilEtDateHeure }.
     * 
     * @param value
     * @return the JAXB element< recherche une operation journal par id util et date heure> {@code >}
     */
    @XmlElementDecl(namespace = "http://techbean.ws.journal.lombok.finances.gouv.fr",
        name = "rechercheUneOperationJournalParIdUtilEtDateHeure")
    public JAXBElement<RechercheUneOperationJournalParIdUtilEtDateHeure>
        createRechercheUneOperationJournalParIdUtilEtDateHeure(
            RechercheUneOperationJournalParIdUtilEtDateHeure value)
    {
        return new JAXBElement<RechercheUneOperationJournalParIdUtilEtDateHeure>(
            _RechercheUneOperationJournalParIdUtilEtDateHeure_QNAME,
            RechercheUneOperationJournalParIdUtilEtDateHeure.class, null, value);
    }

    /**
     * Create an instance of {@link RechercheUneOperationJournalParIdUtilEtDateHeureResponse }.
     * 
     * @return the recherche une operation journal par id util et date heure response
     */
    public RechercheUneOperationJournalParIdUtilEtDateHeureResponse
        createRechercheUneOperationJournalParIdUtilEtDateHeureResponse()
    {
        return new RechercheUneOperationJournalParIdUtilEtDateHeureResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}.
     * 
     * @param value
     * @return the JAXB element< recherche une operation journal par id util et date heure response>
     *         {@link RechercheUneOperationJournalParIdUtilEtDateHeureResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://techbean.ws.journal.lombok.finances.gouv.fr",
        name = "rechercheUneOperationJournalParIdUtilEtDateHeureResponse")
    public JAXBElement<RechercheUneOperationJournalParIdUtilEtDateHeureResponse>
        createRechercheUneOperationJournalParIdUtilEtDateHeureResponse(
            RechercheUneOperationJournalParIdUtilEtDateHeureResponse value)
    {
        return new JAXBElement<RechercheUneOperationJournalParIdUtilEtDateHeureResponse>(
            _RechercheUneOperationJournalParIdUtilEtDateHeureResponse_QNAME,
            RechercheUneOperationJournalParIdUtilEtDateHeureResponse.class, null, value);
    }

}
