/*
 * Copyright (c) 2011 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.journal.ws.techbean;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import fr.gouv.finances.lombok.journal.bean.OperationJournal;

/**
 * <p>
 * Java class for rechercherLesOperationsJournalParCriteresRechercheResponse complex type.
 * <p>
 * The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name=&quot;rechercherLesOperationsJournalParCriteresRechercheResponse&quot;&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base=&quot;{http://www.w3.org/2001/XMLSchema}anyType&quot;&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref=&quot;{http://ws.journal.lombok.finances.gouv.fr/bean}operationsJournal&quot; maxOccurs=&quot;unbounded&quot; minOccurs=&quot;0&quot;/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "rechercherLesOperationsJournalParCriteresRechercheResponse", propOrder = {"operationsJournal"})
public class RechercherLesOperationsJournalParCriteresRechercheResponse
{

    /** operations journal. */
    @XmlElement(namespace = "http://ws.journal.lombok.finances.gouv.fr/bean")
    protected List<OperationJournal> operationsJournal;

    /**
     * Constructeur de la classe RechercherLesOperationsJournalParCriteresRechercheResponse.java
     *
     */
    public RechercherLesOperationsJournalParCriteresRechercheResponse()
    {
        super();  
        
    }

    /**
     * Gets the value of the operationsJournal property.
     * <p>
     * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to
     * the returned list will be present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for
     * the operationsJournal property.
     * <p>
     * For example, to add a new item, do as follows:
     * 
     * <pre>
     * getOperationsJournal().add(newItem);
     * </pre>
     * <p>
     * Objects of the following type(s) are allowed in the list {@link OperationJournal }
     * 
     * @return operations journal
     */
    public List<OperationJournal> getOperationsJournal()
    {
        if (operationsJournal == null)
        {
            operationsJournal = new ArrayList<OperationJournal>();
        }
        return this.operationsJournal;
    }

}
