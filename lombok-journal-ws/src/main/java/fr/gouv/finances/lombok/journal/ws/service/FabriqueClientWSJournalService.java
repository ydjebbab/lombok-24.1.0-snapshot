/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : FabriqueClientWSJournalService.java
 *
 */
package fr.gouv.finances.lombok.journal.ws.service;

import fr.gouv.finances.lombok.journal.ws.JournalWebService;

/**
 * Class FabriqueClientWSJournalService --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.3 $ Date: 11 déc. 2009
 */
public interface FabriqueClientWSJournalService
{

    /**
     * methode Creer journal web service client : --.
     * 
     * @return journal web service
     */
    public JournalWebService creerJournalWebServiceClient();

}