package fr.gouv.finances.lombok.autoconfig;

import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

@Configuration
@ConditionalOnExpression("'${lombok.composant.journalws.inclus}' == 'true' or '${lombok.composant.ws.inclus}' == 'true'")
@ImportResource({"classpath*:conf/applicationContext-ws.xml"})
public class JournalWSApplicationConfig
{


    
}
