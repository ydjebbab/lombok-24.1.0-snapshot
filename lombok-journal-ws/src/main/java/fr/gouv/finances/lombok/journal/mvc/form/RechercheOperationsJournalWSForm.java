/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
*
 *
 * fichier : RechercheOperationsJournalWSForm.java
 *
 */
package fr.gouv.finances.lombok.journal.mvc.form;

import java.io.Serializable;
import java.util.Date;

/**
 * Class RechercheOperationsJournalWSForm DGFiP.
 * 
 * @author chouard-cp
 * @version $Revision: 1.4 $ Date: 11 déc. 2009
 */
public class RechercheOperationsJournalWSForm implements Serializable
{

    /** à compléter d'autres champs de recherche éventuels. */
    private static final long serialVersionUID = 1L;

    /** date de recherche. */
    private Date dateDeRecherche;

    /** valeur identifiant metier obj operations. */
    private String valeurIdentifiantMetierObjOperations;

    /**
     * Constructeur de la classe RechercheOperationsJournalWSForm.java
     *
     */
    public RechercheOperationsJournalWSForm()
    {
        super(); 
        
    }

    /**
     * Accesseur de l attribut date de recherche.
     * 
     * @return date de recherche
     */
    public Date getDateDeRecherche()
    {
        return dateDeRecherche;
    }

    /**
     * Accesseur de l attribut valeur identifiant metier obj operations.
     * 
     * @return valeur identifiant metier obj operations
     */
    public String getValeurIdentifiantMetierObjOperations()
    {
        return valeurIdentifiantMetierObjOperations;
    }

    /**
     * Modificateur de l attribut date de recherche.
     * 
     * @param dateDeRecherche le nouveau date de recherche
     */
    public void setDateDeRecherche(Date dateDeRecherche)
    {
        this.dateDeRecherche = dateDeRecherche;
    }

    /**
     * Modificateur de l attribut valeur identifiant metier obj operations.
     * 
     * @param valeurIdentifiantMetierObjOperations le nouveau valeur identifiant metier obj operations
     */
    public void setValeurIdentifiantMetierObjOperations(String valeurIdentifiantMetierObjOperations)
    {
        this.valeurIdentifiantMetierObjOperations = valeurIdentifiantMetierObjOperations;
    }

}
