/*
 * Copyright (c) 2011 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.journal.ws.techbean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import fr.gouv.finances.lombok.journal.bean.OperationJournal;

/**
 * <p>
 * Java class for rechercheUneOperationJournalParIdUtilEtDateHeureResponse complex type.
 * <p>
 * The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name=&quot;rechercheUneOperationJournalParIdUtilEtDateHeureResponse&quot;&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base=&quot;{http://www.w3.org/2001/XMLSchema}anyType&quot;&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref=&quot;{http://ws.journal.lombok.finances.gouv.fr/bean}operationJournal&quot; minOccurs=&quot;0&quot;/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "rechercheUneOperationJournalParIdUtilEtDateHeureResponse", propOrder = {"operationJournal"})
public class RechercheUneOperationJournalParIdUtilEtDateHeureResponse
{

    /** operation journal. */
    @XmlElement(namespace = "http://ws.journal.lombok.finances.gouv.fr/bean")
    protected OperationJournal operationJournal;

    /**
     * Constructeur de la classe RechercheUneOperationJournalParIdUtilEtDateHeureResponse.java
     *
     */
    public RechercheUneOperationJournalParIdUtilEtDateHeureResponse()
    {
        super();
        
    }

    /**
     * Gets the value of the operationJournal property.
     * 
     * @return possible object is {@link OperationJournal }
     */
    public OperationJournal getOperationJournal()
    {
        return operationJournal;
    }

    /**
     * Sets the value of the operationJournal property.
     * 
     * @param value allowed object is {@link OperationJournal }
     */
    public void setOperationJournal(OperationJournal value)
    {
        this.operationJournal = value;
    }

}
