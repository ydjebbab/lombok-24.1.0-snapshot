/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
*
 *
 * fichier : RechercheOperationsJournalWSFormAction.java
 *
 */
package fr.gouv.finances.lombok.journal.mvc.webflow.action;

import java.beans.PropertyEditor;
import java.util.Date;
import java.util.List;

import org.springframework.beans.PropertyEditorRegistry;
import org.springframework.webflow.action.FormAction;
import org.springframework.webflow.execution.Event;
import org.springframework.webflow.execution.RequestContext;

import fr.gouv.finances.lombok.apptags.util.CheckboxSelectUtil;
import fr.gouv.finances.lombok.journal.bean.CriteresRecherchesJournal;
import fr.gouv.finances.lombok.journal.bean.IdentifiantsMetierObjOperation;
import fr.gouv.finances.lombok.journal.bean.OperationJournal;
import fr.gouv.finances.lombok.journal.mvc.form.RechercheOperationsJournalWSForm;
import fr.gouv.finances.lombok.journal.ws.JournalWebService;
import fr.gouv.finances.lombok.journal.ws.service.FabriqueClientWSJournalService;
import fr.gouv.finances.lombok.util.propertyeditor.CpCustomDateEditor;
import fr.gouv.finances.lombok.util.propertyeditor.SelectEditor;

/**
 * Class RechercheOperationsJournalWSFormAction DGFiP.
 * 
 * @author chouard-cp
 * @version $Revision: 1.6 $ Date: 11 déc. 2009
 */
public class RechercheOperationsJournalWSFormAction extends FormAction
{
    /** Constant : COLLECTION_OPERATIONSJOURNAL. */
    public static final String COLLECTION_OPERATIONSJOURNAL = "listeoperationsjournal";

    /**
     * fabriqueclientwsjournalserviceso - FabriqueClientWSJournalService, service chargé de la création d'un client de
     * web service journal.
     */
    private FabriqueClientWSJournalService fabriqueclientwsjournalserviceso;


    public RechercheOperationsJournalWSFormAction()
    {
        super(); 
        
    }

    /**
     * Constructeur de la classe RechercheOperationsJournalWSFormAction.java
     *
     * @param formObjectClass
     */
    public RechercheOperationsJournalWSFormAction(Class<?> formObjectClass)
    {
        super(formObjectClass);  
        
    }

    /**
     * methode Executer rechercher operations journal : DGFiP.
     * 
     * @param rc param
     * @return event
     * @throws Exception the exception
     */
    public Event executerRechercherOperationsJournal(RequestContext rc)
    {

        // récupérationde la date
        RechercheOperationsJournalWSForm dateform =
            (RechercheOperationsJournalWSForm) rc.getFlowScope().get(getFormObjectName());
        JournalWebService client = fabriqueclientwsjournalserviceso.creerJournalWebServiceClient();
        Date date = dateform.getDateDeRecherche();
        IdentifiantsMetierObjOperation unIdentifiantsMetierObjOperation = null;
        String valeurIdentifiantsMetierObjOperation = dateform.getValeurIdentifiantMetierObjOperations();

        if (valeurIdentifiantsMetierObjOperation != null
            && valeurIdentifiantsMetierObjOperation.compareToIgnoreCase("") != 0)
        {
            unIdentifiantsMetierObjOperation = new IdentifiantsMetierObjOperation();
            unIdentifiantsMetierObjOperation.setValeurIdentifiantMetier1(valeurIdentifiantsMetierObjOperation);
        }
        // à compléter des autres critéres de recherche éventuellment rajoutés
        // + les inscrire dans les CriteresRecherchesJournal

        List<OperationJournal> listeDesOperationsJournal = null;
        CriteresRecherchesJournal crit = new CriteresRecherchesJournal();

        if (date != null)
        {
            crit.setDateJJMMAAAA(date);
            listeDesOperationsJournal = client.rechercherLesOperationsJournalParCriteresRecherche(crit);
        }
        else if (unIdentifiantsMetierObjOperation != null)
        {
            listeDesOperationsJournal =
                client.rechercherOperationsJournalParIdentifiantMetierObjOperation(unIdentifiantsMetierObjOperation);
        }

        rc.getFlowScope().put(COLLECTION_OPERATIONSJOURNAL, listeDesOperationsJournal);

        return success();
    }

    /**
     * Accesseur de l attribut fabriqueclientwsjournalserviceso
     * 
     * @return fabriqueclientwsjournalserviceso
     */
    public FabriqueClientWSJournalService getFabriqueclientwsjournalserviceso()
    {
        return fabriqueclientwsjournalserviceso;
    }

    /**
     * methode Parametrer checkboxes : DGFiP.
     * 
     * @param request param
     * @return event
     * @throws Exception the exception
     */
    public Event parametrerCheckboxes(RequestContext request)
    {
        // Sauvegarde de l'objet de paramétrage des checkboxes dans le FlowScope
        CheckboxSelectUtil.parametrerCheckboxes(request, "opjour").utiliserRowid("id").utiliserListeElements(
            COLLECTION_OPERATIONSJOURNAL);
        return success();
    }

    /**
     * methode Selectionner par un lien : DGFiP.
     * 
     * @param rc param
     * @return event
     * @throws Exception the exception
     */
    public Event selectionnerParUnLien(RequestContext rc)
    {
        Event result;

        List lesopjour = (List) rc.getFlowScope().get(COLLECTION_OPERATIONSJOURNAL);
        PropertyEditor propertyEditor = new SelectEditor(lesopjour, "id");

        propertyEditor.setAsText(rc.getRequestParameters().get("id"));
        OperationJournal uneOpJourSelectionne = (OperationJournal) propertyEditor.getValue();
        if (uneOpJourSelectionne != null)
        {
            rc.getFlowScope().put("lesParamOperation", uneOpJourSelectionne.getLesParamOperation());

            result = success();
        }
        else
        {
            result = error();
        }

        return result;
    }

    /**
     * Modificateur de l attribut fabriqueclientwsjournalserviceso
     * 
     * @param fabriqueclientwsjournalserviceso le nouveau fabriqueclientwsjournalserviceso
     */
    public void setFabriqueclientwsjournalserviceso(FabriqueClientWSJournalService fabriqueclientwsjournalserviceso)
    {
        this.fabriqueclientwsjournalserviceso = fabriqueclientwsjournalserviceso;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param reqc
     * @param per
     * @see org.springframework.webflow.action.FormAction#registerPropertyEditors(org.springframework.webflow.execution.RequestContext,
     *      org.springframework.beans.PropertyEditorRegistry)
     */
    @Override
    protected void registerPropertyEditors(RequestContext reqc, PropertyEditorRegistry per)
    {
        super.registerPropertyEditors(reqc, per);
        per.registerCustomEditor(Date.class, new CpCustomDateEditor());
    }

}
