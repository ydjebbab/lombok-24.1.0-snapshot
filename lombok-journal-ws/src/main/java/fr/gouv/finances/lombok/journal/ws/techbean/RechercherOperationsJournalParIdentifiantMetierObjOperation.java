/*
 * Copyright (c) 2011 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.journal.ws.techbean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import fr.gouv.finances.lombok.journal.bean.IdentifiantsMetierObjOperation;

/**
 * <p>
 * Java class for rechercherOperationsJournalParIdentifiantMetierObjOperation complex type.
 * <p>
 * The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name=&quot;rechercherOperationsJournalParIdentifiantMetierObjOperation&quot;&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base=&quot;{http://www.w3.org/2001/XMLSchema}anyType&quot;&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref=&quot;{http://ws.journal.lombok.finances.gouv.fr/bean}unIdentifiantMetierObjOperation&quot; minOccurs=&quot;0&quot;/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "rechercherOperationsJournalParIdentifiantMetierObjOperation", propOrder = {"unIdentifiantMetierObjOperation"})
public class RechercherOperationsJournalParIdentifiantMetierObjOperation
{

    /** un identifiant metier obj operation. */
    @XmlElement(namespace = "http://ws.journal.lombok.finances.gouv.fr/bean")
    protected IdentifiantsMetierObjOperation unIdentifiantMetierObjOperation;

    /**
     * Constructeur de la classe RechercherOperationsJournalParIdentifiantMetierObjOperation.java
     *
     */
    public RechercherOperationsJournalParIdentifiantMetierObjOperation()
    {
        super(); 
        
    }

    /**
     * Gets the value of the unIdentifiantMetierObjOperation property.
     * 
     * @return possible object is {@link IdentifiantsMetierObjOperation }
     */
    public IdentifiantsMetierObjOperation getUnIdentifiantMetierObjOperation()
    {
        return unIdentifiantMetierObjOperation;
    }

    /**
     * Sets the value of the unIdentifiantMetierObjOperation property.
     * 
     * @param value allowed object is {@link IdentifiantsMetierObjOperation }
     */
    public void setUnIdentifiantMetierObjOperation(IdentifiantsMetierObjOperation value)
    {
        this.unIdentifiantMetierObjOperation = value;
    }

}
