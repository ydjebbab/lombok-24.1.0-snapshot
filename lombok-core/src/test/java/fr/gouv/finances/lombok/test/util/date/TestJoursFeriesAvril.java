package fr.gouv.finances.lombok.test.util.date;

import java.text.ParseException;
import java.util.Date;

import org.junit.Assert;

import org.junit.Test;

import fr.gouv.finances.lombok.util.date.JoursFeries;
import fr.gouv.finances.lombok.util.exception.ProgrammationException;
import fr.gouv.finances.lombok.util.format.FormaterDate;

public class TestJoursFeriesAvril
{
    private Date parserDate(String source) 
    {
        Date date = null;

        try
        {
            date = FormaterDate.getFormatDate(FormaterDate.F_DATE_CT).parse(source);
        }
        catch (ParseException exp)
        {
            throw new ProgrammationException("Date incorrecte", exp);
        }
        return date;
    }

    

    @Test
    public void testDuPremierAu31Avril()
    {
        Date premierJour = parserDate("01/04/2015");
        Date dernierJour = parserDate("30/04/2015");
        int result = JoursFeries.calculeNombreJoursOuvresEntreDeuxDates(premierJour,
            dernierJour);
        Assert.assertEquals(21, result);
    }
    
    
    @Test
    public void testDimancheLEtLundiPaque()
    {
        Date premierJour = parserDate("05/04/2015");
        Date dernierJour = parserDate("06/04/2015");
        int result = JoursFeries.calculeNombreJoursOuvresEntreDeuxDates(premierJour,
            dernierJour);
        Assert.assertEquals(0, result);
    }
    
    @Test
    public void testLundiPaqueAuSamediSuivant()
    {
        Date premierJour = parserDate("06/04/2015");
        Date dernierJour = parserDate("11/04/2015");
        int result = JoursFeries.calculeNombreJoursOuvresEntreDeuxDates(premierJour,
            dernierJour);
        Assert.assertEquals(4, result);
    }

    @Test
    public void testDuPremierAu31Avril2016()
    {
        Date premierJour = parserDate("01/04/2016");
        Date dernierJour = parserDate("30/04/2016");
        int result = JoursFeries.calculeNombreJoursOuvresEntreDeuxDates(premierJour,
            dernierJour);
        Assert.assertEquals(21, result);
    }
    
}
