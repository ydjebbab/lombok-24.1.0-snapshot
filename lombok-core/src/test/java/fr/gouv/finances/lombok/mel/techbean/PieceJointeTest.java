/*
 * Copyright (c) 2016 DGFiP - Tous droits réservés
 * 
 */
package fr.gouv.finances.lombok.mel.techbean;

import java.io.InputStream;

import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.InputStreamSource;

import fr.gouv.finances.lombok.test.pojo.AbstractCorePojoTest;

/**
 * Tests unitaires du POJO PieceJointe.
 * 
 * @author Christophe Breheret-Girardin
 */
public class PieceJointeTest extends AbstractCorePojoTest<PieceJointe>
{   
    /**
     * Constructeur permettant d'initialiser des valeurs préfabriquées utilisées automatiquement dans les tests.
     */
    public PieceJointeTest()
    {
        // Ajout d'une valeur par défaut pour les paramètres de type InputStreamSource
        InputStream inputStream
            = this.getClass().getResourceAsStream(this.getClass().getSimpleName() + ".class");
        addPrefabValues(InputStreamSource.class, new InputStreamResource(inputStream));
    }
}
