/*
 * Copyright (c) 2015 DGFiP - Tous droits réservés
 */

package fr.gouv.finances.lombok.test.util.date;

import java.text.ParseException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import org.junit.Assert;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.joda.time.DateTime;
import org.junit.Test;

import fr.gouv.finances.lombok.util.date.JoursFeries;
import fr.gouv.finances.lombok.util.format.FormaterDate;

/**
 * Class TestJoursFeries.
 */
public class TestJoursFeries
{
    private final Log log = LogFactory.getLog(getClass());

    private static final HashMap<String, String> JOURS_FERIES_2015 = new HashMap<String, String>()
    {
        /**
         * serialVersionUID - long
         */
        private static final long serialVersionUID = 1L;

        {
            put("JourDeLAN", "01/01/2015");
            put("Armistice3945", "08/05/2015");
            put("Toussaint", "01/11/2015");
            put("Assomption", "15/08/2015");
            put("FeteDuTravail", "01/05/2015");
            put("jourFeteNationale", "14/07/2015");
            put("Noel", "25/12/2015");
            put("Armistice1418", "11/11/2015");
            put("LundiDePaques", "06/04/2015");
            put("Ascension", "14/05/2015");
            put("LundiPentecote", "25/05/2015");
        }

    };

    private static final HashMap<String, String> JOURS_FERIES_PERIODE = new HashMap<String, String>()
    {
        /**
         * serialVersionUID - long
         */
        private static final long serialVersionUID = 1L;

        {
            put("JourDeLAN", "01/01/2015");
            put("Armistice3945", "08/05/2015");
            put("Toussaint", "01/11/2015");
            put("Assomption", "15/08/2015");
            put("FeteDuTravail", "01/05/2015");
            put("jourFeteNationale", "14/07/2015");
            put("Noel", "25/12/2015");
            put("Armistice1418", "11/11/2015");
            put("LundiDePaques", "06/04/2015");
            put("Ascension", "14/05/2015");
            put("LundiPentecote", "25/05/2015");
            put("JourDeLAN2016", "01/01/2016");
        }

    };

    /**
     * methode Parser date.
     * 
     * @param source chaine représentant une date
     * @return date
     */
    private Date parserDate(String source)
    {
        Date date = null;

        try
        {
            date = FormaterDate.getFormatDate(FormaterDate.F_DATE_CT).parse(source);
        }
        catch (ParseException exp)
        {
            log.warn("Date incorrecte", exp);
        }
        return date;
    }

    /**
     * methode Test bornes inversees .
     */
    @Test
    public void testBornesInversees()
    {
        Date dimanche1 = parserDate("31/05/2015");
        Date lundi = parserDate("01/05/2015");

        try
        {
            JoursFeries.calculeNombreJoursOuvresEntreDeuxDates(dimanche1,
                lundi);

            Assert.assertTrue("Une exception aurait du être levée", false);
        }
        catch (IllegalArgumentException ille)
        {
            Assert.assertTrue(true);
        }

    }

    /**
     * methode Test borne nulles.
     */
    @Test
    public void testBorneNulles()
    {
        Date bornesInf = parserDate("31/05/2015");
        Date borneSup = parserDate("01/05/2015");

        try
        {
            JoursFeries.calculeNombreJoursOuvresEntreDeuxDates(bornesInf,
                null);

            Assert.assertTrue("Une exception aurait du être levée", false);
        }
        catch (IllegalArgumentException ille)
        {
            Assert.assertTrue(true);
        }

        try
        {
            JoursFeries.calculeNombreJoursOuvresEntreDeuxDates(null,
                borneSup);

            Assert.assertTrue("Une exception aurait du être levée", false);
        }
        catch (IllegalArgumentException ille)
        {
            Assert.assertTrue(true);
        }

        try
        {
            JoursFeries.calculeNombreJoursOuvresEntreDeuxDates(null,
                null);

            Assert.assertTrue("Une exception aurait du être levée", false);
        }
        catch (IllegalArgumentException ille)
        {
            Assert.assertTrue(true);
        }

    }

    @Test
    public void testListeJoursFeries2015()
    {
        List<DateTime> joursFeries2015 = JoursFeries.creationListeDesJoursFeriesPourUneAnnee(2015);

        int nbJoursFeries2015 = JOURS_FERIES_2015.size();

        Assert.assertEquals(nbJoursFeries2015, joursFeries2015.size());

        Set<String> libelJoursFeries = JOURS_FERIES_2015.keySet();

        for (String unJourFerie : libelJoursFeries)
        {
            Date jour = parserDate(JOURS_FERIES_2015.get(unJourFerie));
            boolean flag = joursFeries2015.contains(new DateTime(jour));

            if (flag == false)
            {
                log.info(unJourFerie + " non trouvé : " + JOURS_FERIES_2015.get(unJourFerie));

            }

            Assert.assertTrue(flag);
        }

    }

    @Test
    public void testListeJoursFeriesPourUnePeriode2015()
    {
        Date dateDebut = parserDate("01/01/2015");
        Date dateFin = parserDate("02/01/2016");

        List<DateTime> joursFeriesPeriode = JoursFeries.creationListeDesJoursFeriesPourUnePeriode(dateDebut, dateFin);

        int nbJoursFeriesPeriode = JOURS_FERIES_PERIODE.size();

        Assert.assertEquals(nbJoursFeriesPeriode, joursFeriesPeriode.size());

        Set<String> libelJoursFeries = JOURS_FERIES_PERIODE.keySet();

        for (String unJourFerie : libelJoursFeries)
        {
            Date jour = parserDate(JOURS_FERIES_PERIODE.get(unJourFerie));
            boolean flag = joursFeriesPeriode.contains(new DateTime(jour));

            if (flag == false)
            {
                log.info(unJourFerie + " non trouvé : " + JOURS_FERIES_PERIODE.get(unJourFerie));

            }

            Assert.assertTrue(flag);
        }

    }

}
