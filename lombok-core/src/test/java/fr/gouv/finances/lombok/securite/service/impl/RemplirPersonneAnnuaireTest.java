/*
 * Copyright (c) 2014 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.securite.service.impl;

import java.beans.PropertyDescriptor;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.BasicAttribute;
import javax.naming.directory.BasicAttributes;

import org.junit.Assert;

import org.apache.commons.lang.StringUtils;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;

import fr.gouv.finances.lombok.securite.techbean.HabilitationAnnuaire;
import fr.gouv.finances.lombok.securite.techbean.PersonneAnnuaire;
import fr.gouv.finances.lombok.test.securite.LectureAnnuaireServicesTest.TypeAnnuaire;

/**
 * Test unitaire automatisé permettant de vérifier la bonne alimentation d'une personne, suite
 * à la réception des attributs de chaque annuaire : DGFIP, DGCP, DGI, USAGER.
 *
 * @author Christophe Breheret-Girardin
 */
public class RemplirPersonneAnnuaireTest
{   
    /** Initialisation de la journalisation. */
    protected static final Logger LOG = LoggerFactory.getLogger(RemplirPersonneAnnuaireTest.class);
   
    /** Libellé de test d'une 1ière application */
    public static final String APPLICATION_1 = "APPLICATION 1";
    /** Libellé de test d'une 2ème application de test */
    public static final String APPLICATION_2 = "APPLICATION 2";
    /** Format des dates à utiliser au sein du test */
    public static final DateFormat DATE_FORMAT = new SimpleDateFormat("dd/MM/yyyy", Locale.FRANCE);
    /** Date d'expiration des habilitations par défaut */
    public static final String DATE_EXPIRATION_HABILITATION = "01/01/1970";

    /** Valeur de test de l'attribut "AFFECTATION" renvoyé par les annuaires */
    public static final String AFFECTATION = "A";
    /** Valeur de test de l'attribut "AFFECTATIONANNEXE" renvoyé par les annuaires */
    public static final String AFFECTATIONANNEXE = "B";
    /** Valeur de test de l'attribut "AFFECTATIONCODIQUE" renvoyé par les annuaires */
    public static final String AFFECTATIONCODIQUE = "C";
    /** Valeur de test de l'attribut "AFFECTATIONSAGES" renvoyé par les annuaires */
    public static final String AFFECTATIONSAGES = "D";
    /** Valeur de test de l'attribut "AFFECTATIONSECONDAIRES" renvoyé par les annuaires */
    public static final List<String> AFFECTATIONSSECONDAIRES = Arrays.asList("E1", "E2");
    /** Valeur de test de l'attribut "ANNEXE" renvoyé par les annuaires */
    public static final String ANNEXE = "F";
    /** Valeur de test de l'attribut "BUSINESSCATEGORY" renvoyé par les annuaires */
    public static final String BUSINESSCATEGORY = "G";
    /** Valeur de test de l'attribut "CATEGORIE" renvoyé par les annuaires */
    public static final String CATEGORIE = "H";
    /** Valeur de test de l'attribut "CN" renvoyé par les annuaires */
    public static final String CN = "I";
    /** Valeur de test de l'attribut "CODECSRH" renvoyé par l'annuaire DGFIP */
    public static final String CODECSRH = StringUtils.rightPad("J", 10, '*');;
    /** Valeur de test de l'attribut "CODEFONCTION" renvoyé par les annuaires */
    public static final String CODEFONCTION = "K";
    /** Valeur de test de l'attribut "CODEGRADE" renvoyé par les annuaires */
    public static final String CODEGRADE = "L";
    /** Valeur de test de l'attribut "CODEGRADERH" renvoyé par les annuaires */
    public static final String CODEGRADERH = "M";
    /** Valeur de test de l'attribut "CODEGRADESIRHIUS" renvoyé par l'annuaire DGFIP */
    public static final String CODEGRADESIRHIUS = "N***";
    /** Valeur de test de l'attribut "DEPARTEMENT" renvoyé par les annuaires */
    public static final String DEPARTEMENT = "O1";
    /** Valeur de test de l'attribut "DEPARTMENT" renvoyé par les annuaires */
    public static final String DEPARTMENT = "O2";
    /** Valeur de test de l'attribut "EMPSUP" renvoyé par les annuaires */
    public static final String EMPSUP = "P";
    /** Valeur de test de l'attribut "FONCTION" renvoyé par les annuaires */
    public static final String FONCTION = "Q";
    /** Valeur de test de l'attribut "GIVENNAME" renvoyé par les annuaires */
    public static final String GIVENNAME = "R";
    /** Valeur de test de l'attribut "IDENTDGI" renvoyé par les annuaires */
    public static final String IDENTDGI = "S";
    /** Valeur de test de l'attribut "IDORGANISME" renvoyé par les annuaires */
    public static final List<String> IDORGANISME = Arrays.asList("T1", "T2");
    /** Valeur de test de l'attribut "MAIL" renvoyé par les annuaires */
    public static final String MAIL = "U";
    /** Valeur de test de l'attribut "MAILDELIVERYOPTION" renvoyé par les annuaires */
    public static final String MAILDELIVERYOPTION = "V";
    /** Valeur de test de l'attribut "MEFIPOINTMAJF" renvoyé par les annuaires */
    public static final String MEFIPOINTMAJF = "Y";
    /** Valeur de test de l'attribut "NIVEAURESPONSABILITE" renvoyé par les annuaires */
    public static final String NIVEAURESPONSABILITE = "Z";
    /** Valeur de test de l'attribut "OU" renvoyé par les annuaires */
    public static final String OU = "AA";
    /** Valeur de test de l'attribut "OUCOMPLET" renvoyé par les annuaires */
    public static final String OUCOMPLET = "AB";
    /** Valeur de test de l'attribut "OUDESCRIPTION" renvoyé par les annuaires */
    public static final String OUDESCRIPTION = "AC";
    /** Valeur de test de l'attribut "OUSIGLE" renvoyé par les annuaires */
    public static final String OUSIGLE = "AD";
    /** Valeur de test de l'attribut "PASSWORDEXPIRATIONTIME" renvoyé par les annuaires */
    public static final String PASSWORDEXPIRATIONTIME = "AE";
    /** Valeur de test de l'attribut "PERSONALTITLE" renvoyé par les annuaires */
    public static final String PERSONALTITLE = "AF";
    /** Valeur de test de l'attribut "PHYSICALDELIVERYOFFICENAME" renvoyé par les annuaires */
    public static final String PHYSICALDELIVERYOFFICENAME = "AG";
    /** Valeur de test de l'attribut "POSTALADDRESS" renvoyé par les annuaires */
    public static final String POSTALADDRESS = "AH";
    /** Valeur de test de l'attribut "POSTALCODE" renvoyé par les annuaires */
    public static final String POSTALCODE = "AI";
    /** Valeur de test de l'attribut "PROFILAPPLICATIF" renvoyé par les annuaires */
    public static final String PROFILAPPLICATIF = "AJ";
    /** Valeur de test de l'attribut "PROFILAPPLICATIFLONG" renvoyé par les annuaires */
    public static final String PROFILAPPLICATIFLONG = "AK";
    /** Valeur de test de l'attribut "QUESTIONREPONSE" renvoyé par les annuaires */
    public static final String QUESTIONREPONSE = "AL";
    /** Valeur de test de l'attribut "REGION" renvoyé par les annuaires */
    public static final String REGION = "AM";
    /** Valeur de test de l'attribut "ROOMNUMBER" renvoyé par les annuaires */
    public static final String ROOMNUMBER = "AN";
    /** Valeur de test de l'attribut "SAGES" renvoyé par les annuaires */
    public static final String SAGES = "AO";
    /** Valeur de test de l'attribut "SAMBANTPASSWORD" renvoyé par les annuaires */
    public static final String SAMBANTPASSWORD = "AP";
    /** Valeur de test de l'attribut "SN" renvoyé par les annuaires */
    public static final String SN = StringUtils.rightPad("AQ", 40, '*');
    /** Valeur de test de l'attribut "SPI" renvoyé par les annuaires */
    public static final String SPI = "AR";
    /** Valeur de test de l'attribut "TELEPHONENUMBER" renvoyé par les annuaires */
    public static final String TELEPHONENUMBER = "AS";
    /** Valeur de test de l'attribut "TITLE" renvoyé par les annuaires */
    public static final String TITLE = "AT";
    /** Valeur de test de l'attribut "TYPECERTIFICAT" renvoyé par les annuaires */
    public static final String TYPECERTIFICAT = "AU";
    /** Valeur de test de l'attribut "UID_DGCP" renvoyé par l'annuaire DGCP */
    public static final String UID_DGCP = "AV-CP";
    /** Valeur de test de l'attribut "UID_DGI" renvoyé par l'annuaire DGI */
    public static final String UID_DGI = "AV-DGI";
    /** Valeur de test de l'attribut "UID_DGFIP" renvoyé par l'annuaire DGFIP */
    public static final String UID_DGFIP = "AV-0";
    /** Valeur de test de l'attribut "UID_USAGERS" renvoyé parl'annuaire USAGERS */
    public static final String UID_USAGERS = "AV-USAGERS";
    /** Valeur de test de l'attribut "UIDFONCTIONNEL" renvoyé par les annuaires */
    public static final String UIDFONCTIONNEL = StringUtils.rightPad("AW", 64, '*');
    /** Valeur de test de l'attribut "UIDRH" renvoyé par les annuaires */
    public static final String UIDRH = "AX";
    /** Valeur de test de l'attribut "UIDSIRHIUS" par l'annuaire DGFIP */
    public static final String UIDSIRHIUS = StringUtils.rightPad("AY", 15, '*');
    /** Valeur de test de l'attribut "USERCERTIFICATE" renvoyé par les annuaires */
    public static final String USERCERTIFICATE = "AZ";
    /** Valeur de test de l'attribut "USERPASSWORD" renvoyé par les annuaires */
    public static final String USERPASSWORD = "BA";
    /** Valeur de test de l'attribut "USERPASSWORD" renvoyé par les annuaires */
    
    /** Valeur du caractère de séparation des habilitations renvoyés par les annuaires */
    public static final String SEPARATEUR_HABILITATION = ";";
    /** Libellé de test d'une 1ière habilitation */
    public static final String PROFIL_HABILITATION_1 = "P1";
    /** Libellé de test d'une 2ème habilitation */
    public static final String PROFIL_HABILITATION_2 = "P2";

    /** Valeur de test d'un 1er administrateur applicatif renvoyé par les annuaires DGCP et DGFIP */
    public static final String ADMIN_HABILITATION_1_DGCP_DGFIP = "admin-cp-1";
    /** Valeur de test d'une 1ère date renvoyée par les annuaires DGCP et DGFIP */
    public static final String DATE_HABILITATION_1_DGCP_DGFIP = "11/10/2004";
    /** Valeur de test d'un 2ème administrateur applicatif renvoyé par les annuaires DGCP et DGFIP */
    public static final String ADMIN_HABILITATION_2_DGCP_DGFIP = "admin-cp-2";
    /** Valeur de test d'une 2ème date renvoyée par les annuaires DGCP et DGFIP */
    public static final String DATE_HABILITATION_2_DGCP_DGFIP = "02/02/2002";
    /** Valeur de test d'une 1ère habilitation renvoyée par les annuaires DGCP et DGFIP */
    public static final String HABILITATION_DGCP_DGFIP_1
        = "<HABILITATIONS><HABILITATION><CREATED_BY><ADMIN_UID>"
            + ADMIN_HABILITATION_1_DGCP_DGFIP + "</ADMIN_UID><DATE>"
            + DATE_HABILITATION_1_DGCP_DGFIP + "</DATE></CREATED_BY><PROFIL><NAME>"
            + PROFIL_HABILITATION_1 + "</NAME></PROFIL></HABILITATION></HABILITATIONS>";
    /** Valeur de test d'une 2ème habilitation renvoyée par les annuaires DGCP et DGFIP */
    public static final String HABILITATION_DGCP_DGFIP_2
        = "<HABILITATIONS><HABILITATION><CREATED_BY><ADMIN_UID>"
            + ADMIN_HABILITATION_2_DGCP_DGFIP + "</ADMIN_UID><DATE>"
            + DATE_HABILITATION_2_DGCP_DGFIP + "</DATE></CREATED_BY><PROFIL><NAME>"
            + PROFIL_HABILITATION_2 + "</NAME></PROFIL></HABILITATION></HABILITATIONS>";
    /** Liste contenant les 2 habilitations de test renvoyées par les annuaires DGCP et DGFIP */
    public static final List<String> HABILITATION_DGCP_DGFIP
        = Arrays.asList(APPLICATION_1 + SEPARATEUR_HABILITATION + HABILITATION_DGCP_DGFIP_1 + SEPARATEUR_HABILITATION
            , APPLICATION_2 + SEPARATEUR_HABILITATION + HABILITATION_DGCP_DGFIP_2 + SEPARATEUR_HABILITATION);

    /** Valeur de l'administrateur applicatif renvoyé par l'annuaire DGI */
    public static final String ADMIN_HABILITATION_DGI = "Administrateur-dgi";
    /** Valeur de la date renvoyée par l'annuaire DGI */
    public static final String DATE_HABILITATION_DGI = "01/01/2009";
    /** Valeur de test d'une 1ère habilitation renvoyée par l'annuaire DGI */
    public static final String HABILITATION_DGI_1 = PROFIL_HABILITATION_1 + ";AA;BB;CC";
    /** Valeur de test d'une 2ème habilitation renvoyée par l'annuaire DGI */
    public static final String HABILITATION_DGI_2 = PROFIL_HABILITATION_2 + ";DD;EE;FF";
    /** Liste contenant les 2 habilitations de test renvoyées par l'annuaire DGI */
    public static final List<String> MEFIAPPLIDGI
        = Arrays.asList(APPLICATION_1 + SEPARATEUR_HABILITATION + HABILITATION_DGI_1 
            , APPLICATION_2 + SEPARATEUR_HABILITATION + HABILITATION_DGI_2);

    /** Valeur de la date renvoyée par l'annuaire DGFIP */
    public static final String ADMIN_HABILITATION_1_DGFIP = "Administrateur-0";
    public static final String DATE_HABILITATION_DGFIP = "01/01/2012";
    /** Valeur de test d'une 1ère habilitation renvoyée par l'annuaire DGFIP */
    public static final String PROFILAPPLICATIF_DGFIP_1 = APPLICATION_1 + SEPARATEUR_HABILITATION + PROFIL_HABILITATION_1;
    /** Valeur de test d'une 2ème habilitation renvoyée par l'annuaire DGFIP */
    public static final String PROFILAPPLICATIF_DGFIP_2 = APPLICATION_1 + SEPARATEUR_HABILITATION + PROFIL_HABILITATION_2;
    /** Liste contenant les 2 habilitations de test renvoyées par l'annuaire DGFIP */
    public static final List<String> PROFILAPPLICATIF_DGFIP
        = Arrays.asList(PROFILAPPLICATIF_DGFIP_1, PROFILAPPLICATIF_DGFIP_2);
    
    /** Libellé par défaut du profil d'habilitation des usagers */
    public static final String PROFIL_HABILITATION_USAGER = "0";
    public static final List<String> PROFILAPPLICATIF_USAGERS = Arrays.asList(APPLICATION_1, APPLICATION_1);

    /** Initialisation d'un attribut non géré */
    public static final String ATTRIBUT_NON_GERE = "S_O";
    
    /**
     * Méthode permettant de vérifier la bonne alimentation d'une personne, suite
     * à la réception des attributs de chaque annuaire : DGFIP, DGCP, DGI, USAGER.
     *
     * @throws NamingException Exception liée à l'alimentation des attributs pour simuler les annuaires
     * @throws ParseException Exception liée à la conversion de date
     */
    @Test
    public void testRemplissagePersonne() throws NamingException, ParseException
    {
        // Annuaire ex-DGCP
        testRemplissagePersonne(TypeAnnuaire.DGCP);

        // Annuaire ex-DGI
        testRemplissagePersonne(TypeAnnuaire.DGI);

        // Annuaire DGFIP
        testRemplissagePersonne(TypeAnnuaire.DGFIP);

        // Annuaire USAGERS
        testRemplissagePersonne(TypeAnnuaire.USAGERS);
    }

    /**
     * Méthode permettant de vérifier la bonne alimentation d'une personne, suite
     * à la réception des attributs de l'annuaire passé en paramétre.
     *
     * @param typeAnnuaire annuaire à simuler
     *
     * @throws NamingException Exception liée à l'alimentation des attributs de l'annuaire
     * @throws ParseException Exception liée à la conversion de date
     */
    private void testRemplissagePersonne(TypeAnnuaire typeAnnuaire) throws NamingException, ParseException
    {
        LOG.trace("Vérification de la bonne alimentation d'une personne, en simulant l'appel à l'annuaire '{}'"
            , typeAnnuaire);

        // Exécution de l'alimentation : attributs de l'annuaire -> objet Lombok PersonneAnnuaire
        LecturePersonneServiceImpl lecturePersonneService = new LecturePersonneServiceImpl();
        PersonneAnnuaire personneAnnuaire
            = lecturePersonneService.mapperAttributsRechercheAnnuaireAvecUnePersonneAnnuaire(
                getAttributesSimulationAnnuaire(typeAnnuaire));

        // Vérification des attributs de la personne, sont-ils renseignés ou non ?
        verificationAttributsRenseignesPersonne(typeAnnuaire, personneAnnuaire);
        // Vérification de la valeur des attributs de la personne
        verificationValeurAttributsPersonne(typeAnnuaire, personneAnnuaire);
        // Vérification des spécificités de la personne par rapport à l'annuaire
        verificationspecificites(typeAnnuaire, personneAnnuaire);

        // Calcul des habilitations
        LOG.trace("Calcul des habilitations");
        personneAnnuaire.calculerHabilitationSurApplication(APPLICATION_1);
        // Vérification de la valeur des habilitations
        verificationHabilitation(typeAnnuaire, personneAnnuaire);
    }

    /**
     * Vérification de l'alimentation des personnes en prenant en compte les spécificités de l'annuaire.
     *
     * @param typeAnnuaire type d'annuaire simulé
     * @param personneAnnuaire personne alimentée avec les informations de l'annuaire
     */
    private void verificationspecificites(TypeAnnuaire typeAnnuaire
        , PersonneAnnuaire personneAnnuaire)
    {
        LOG.trace("Vérification de l'alimentation des personnes en prenant en compte les spécificités de l'annuaire '{}'"
            , typeAnnuaire);

        switch (typeAnnuaire)
        {
            case DGCP :
                // Vérification du profil DGCP de la personne
                Assert.assertFalse("Ne devrait pas être un agent DGI", personneAnnuaire.estUtilisateurOrigineDGI());
                Assert.assertFalse("Ne devrait pas être un agent DGFIP", personneAnnuaire.estUtilisateurOrigineDGFIP());
                Assert.assertFalse("Ne devrait pas être un usager", personneAnnuaire.estUtilisateurOrigineUSAGERS());
                // Vérification de l'UID de la personne
                Assert.assertEquals(AttributsAnnuaire.UID.name(), UID_DGCP, personneAnnuaire.getUid());
                // Vérification des attributs spécifiques aux autres annuaires
                Assert.assertNotNull(AttributsAnnuaire.IDORGANISME.name() + "devrait être non null"
                    , personneAnnuaire.getListeBruteIdOrganisme());
                Assert.assertEquals(AttributsAnnuaire.IDORGANISME.name(), 0, personneAnnuaire.getListeBruteIdOrganisme().size());
                break;
            case DGI :
                // Vérification du profil DGI de la personne
                Assert.assertTrue("Devrait être un agent DGI", personneAnnuaire.estUtilisateurOrigineDGI());
                Assert.assertFalse("Ne devrait pas être un agent DGFIP", personneAnnuaire.estUtilisateurOrigineDGFIP());
                Assert.assertFalse("Ne devrait pas être un usager", personneAnnuaire.estUtilisateurOrigineUSAGERS());
                // Vérification de l'UID de la personne
                Assert.assertEquals(AttributsAnnuaire.UID.name(), UID_DGI, personneAnnuaire.getUid());
                // Vérification des attributs spécifiques aux autres annuaires
                Assert.assertNotNull(AttributsAnnuaire.IDORGANISME.name() + "devrait être non null"
                    , personneAnnuaire.getListeBruteIdOrganisme());
                Assert.assertEquals(AttributsAnnuaire.IDORGANISME.name(), 0, personneAnnuaire.getListeBruteIdOrganisme().size());
                break;
            case DGFIP :
                // Vérification du profil DGFIP de la personne
                Assert.assertFalse("Ne devrait pas être un agent DGI", personneAnnuaire.estUtilisateurOrigineDGI());
                Assert.assertTrue("Devrait être un agent DGFIP", personneAnnuaire.estUtilisateurOrigineDGFIP());
                Assert.assertFalse("Ne devrait pas être un usager", personneAnnuaire.estUtilisateurOrigineUSAGERS());
                // Vérification de l'UID de la personne
                Assert.assertEquals(AttributsAnnuaire.UID.name(), UID_DGFIP, personneAnnuaire.getUid());
                // Vérification des attributs spécifiques aux autres annuaires
                Assert.assertNotNull(AttributsAnnuaire.IDORGANISME.name() + "devrait être non null"
                    , personneAnnuaire.getListeBruteIdOrganisme());
                Assert.assertEquals(AttributsAnnuaire.IDORGANISME.name(), 0, personneAnnuaire.getListeBruteIdOrganisme().size());
                Assert.assertEquals(AttributsAnnuaire.CODECSRH.name(), CODECSRH, personneAnnuaire.getCodeCSRH());
                Assert.assertEquals(AttributsAnnuaire.CODEGRADESIRHIUS.name(), CODEGRADESIRHIUS, personneAnnuaire.getCodeGradeSirhius());
                Assert.assertEquals("UIDSIRHIUS", UIDSIRHIUS, personneAnnuaire.getUidSirhius());
                break;
            case USAGERS :
                // Vérification du profil usager de la personne
                Assert.assertFalse("Ne devrait pas être un agent DGI", personneAnnuaire.estUtilisateurOrigineDGI());
                Assert.assertFalse("Ne devrait pas être un agent DGFIP", personneAnnuaire.estUtilisateurOrigineDGFIP());
                Assert.assertTrue("Devrait être un usager", personneAnnuaire.estUtilisateurOrigineUSAGERS());
                // Vérification de l'UID de la personne
                Assert.assertEquals("UID", UID_USAGERS, personneAnnuaire.getUid());
                // Vérification des attributs spécifiques de l'annuaire USAGERS
                Assert.assertEquals("SPI", SPI, personneAnnuaire.getSpi());
                Assert.assertNotNull(AttributsAnnuaire.IDORGANISME.name() + "devrait être renseigné"
                    , personneAnnuaire.getListeBruteIdOrganisme());
                Assert.assertEquals(AttributsAnnuaire.IDORGANISME.name(), 2, personneAnnuaire.getListeBruteIdOrganisme().size());
                Assert.assertEquals(AttributsAnnuaire.IDORGANISME.name(), IDORGANISME
                    , personneAnnuaire.getListeBruteIdOrganisme());
                break;
        }
    }
    
    /**
     * Vérification de l'alimentation des personnes : si les propriétés sont bien renseignées,
     * sauf cas particulier.
     *
     * @param typeAnnuaire type d'annuaire simulé
     * @param personneAnnuaire personne alimentée avec les informations de l'annuaire
     * @throws ParseException Exception liée à la conversion de date
     */
    private void verificationAttributsRenseignesPersonne(TypeAnnuaire typeAnnuaire
        , PersonneAnnuaire personneAnnuaire) throws ParseException
    {
        LOG.trace("Vérification de l'alimentation des personnes : si les propriétés sont bien renseignées, sauf cas particulier");

        // Vérification que tous les champs ont bien été initialisés
        BeanWrapper personneAnnuaireBean = new BeanWrapperImpl(personneAnnuaire);
        PropertyDescriptor[] propertyDescriptors = personneAnnuaireBean.getPropertyDescriptors();
        for (final PropertyDescriptor propertyDescriptor : propertyDescriptors) {
            Object valeur = null;
            switch (propertyDescriptor.getName())
            {
                case "listeHabilitations" :
                    // La liste des habilitations n'est pas mise à jour automatiquement (il faut
                    // exécuter la méthode "calculerHabilitationSurApplication" de PersonneAnnuaire)
                    Assert.assertNull("La liste des habilitations devrait être non renseignée"
                        , personneAnnuaire.getListeHabilitations());
                    break;
                case "mefiAppliDGCP" :
                    // Cette propriété n'est plus renseignée, l'attribut de l'annuaire DGCP permet de
                    // renseigner la propriété "listeHabilitation" de la personne
                    Assert.assertNull(AttributsAnnuaire.MEFIAPPLIDGCP
                        + " devrait être non renseigné (valeur = " + personneAnnuaire.getMefiAppliDGCP() + ")"
                        , personneAnnuaire.getMefiAppliDGCP());
                    break;
                case "mefiAppliDGI" :
                    // Cette propriété n'est plus renseignée, l'attribut de l'annuaire DGI permet de
                    // renseigner la propriété "listeHabilitation" de la personne
                    Assert.assertNull(AttributsAnnuaire.MEFIAPPLIDGI
                        + " devrait être non renseigné (valeur = " + personneAnnuaire.getMefiAppliDGI() + ")"
                        , personneAnnuaire.getMefiAppliDGI());
                    break;
                case "mefiAppliHabilitDGCP" :
                    // Cette propriété n'est plus renseignée, l'attribut de l'annuaire DGCP permet de
                    // renseigner la propriété "listeHabilitation" de la personne
                    Assert.assertNull(AttributsAnnuaire.MEFIAPPLIHABILITDGCP
                        + " devrait être non renseigné (valeur = " + personneAnnuaire.getMefiAppliHabilitDGCP() + ")"
                        , personneAnnuaire.getMefiAppliHabilitDGCP());
                    break;
                case "profilApplicatif" :
                    // Cette propriété n'est plus renseignée, l'attribut de l'annuaire DGFIP permet de
                    // renseigner la propriété "listeHabilitation" de la personne
                    Assert.assertNull(AttributsAnnuaire.PROFILAPPLICATIF
                        + " devrait être non renseigné (valeur = " + personneAnnuaire.getProfilApplicatif() + ")"
                        , personneAnnuaire.getProfilApplicatif());
                    break;
                case "profilApplicatifLong" :
                    // Cette propriété n'est plus renseignée, l'attribut de l'annuaire DGFIP permet de
                    // renseigner la propriété "listeHabilitation" de la personne
                    Assert.assertNull(AttributsAnnuaire.PROFILAPPLICATIFLONG
                        + " devrait être non renseigné (valeur = " + personneAnnuaire.getProfilApplicatifLong() + ")"
                        , personneAnnuaire.getProfilApplicatifLong());
                    break;
                case "spi" :
                    // Le SPI est uniquement renseigné pour les usagers
                    switch (typeAnnuaire)
                    {
                        case USAGERS :
                            Assert.assertNotNull(AttributsAnnuaire.SPI
                                + " devrait être renseigné", personneAnnuaire.getSpi());
                            break;
                        default :
                            Assert.assertNull(AttributsAnnuaire.SPI
                                + " devrait être non renseigné (valeur = " + personneAnnuaire.getSpi() + ")"
                                , personneAnnuaire.getSpi());
                    }
                    break;
                case "codeCSRH" :
                    // Le codeCSRH est uniquement renseigné pour l'annuaire DGFIP
                    switch (typeAnnuaire)
                    {
                        case DGFIP :
                            Assert.assertNotNull(AttributsAnnuaire.CODECSRH
                                + " devrait être renseigné", personneAnnuaire.getCodeCSRH());
                            break;
                        default :
                            Assert.assertNull(AttributsAnnuaire.CODECSRH
                                + " devrait être non renseigné (valeur = " + personneAnnuaire.getCodeCSRH() + ")"
                                , personneAnnuaire.getCodeCSRH());
                    }
                    break;
                case "codeGradeSirhius" :
                    // Le codeGradeSirhius est uniquement renseigné pour l'annuaire DGFIP
                    switch (typeAnnuaire)
                    {
                        case DGFIP :
                            Assert.assertNotNull(AttributsAnnuaire.CODEGRADESIRHIUS
                                + " devrait être renseigné", personneAnnuaire.getCodeGradeSirhius());
                            break;
                        default :
                            Assert.assertNull(AttributsAnnuaire.CODEGRADESIRHIUS
                                + " devrait être non renseigné (valeur = " + personneAnnuaire.getCodeGradeSirhius() + ")"
                                , personneAnnuaire.getCodeGradeSirhius());
                    }
                    break;
                case "uidSirhius" :
                    // L'uidSirhius est uniquement renseigné pour l'annuaire DGFIP
                    switch (typeAnnuaire)
                    {
                        case DGFIP :
                            Assert.assertNotNull(AttributsAnnuaire.UIDSIRHIUS
                                + " devrait être renseigné", personneAnnuaire.getUidSirhius());
                            break;
                        default :
                            Assert.assertNull(AttributsAnnuaire.CODEGRADESIRHIUS
                                + " devrait être non renseigné (valeur = " + personneAnnuaire.getUidSirhius() + ")"
                                , personneAnnuaire.getUidSirhius());
                    }
                    break;
                default :
                    // Hormis les propriétés précédentes, toutes les autres doivent être renseignées
                    Assert.assertNotNull("'" + propertyDescriptor.getName() + "' non renseigné"
                        , personneAnnuaireBean.getPropertyValue(propertyDescriptor.getName()));
            }
        }
    }

    /**
     * Méthode permettant de vérifier les habilitations une fois qu'elles ont été calculées
     *
     * @param typeAnnuaire type d'annuaire simulé
     * @param personneAnnuaire personne alimentée avec les informations de l'annuaire
     * @throws ParseException Exception liée à la conversion de date
     */
    private void verificationHabilitation(TypeAnnuaire typeAnnuaire, PersonneAnnuaire personneAnnuaire)
        throws ParseException
    {
        LOG.trace("Vérification des habilitations");

        final String DATE_DEBUT = "01/01/2012";
        final String DATE_FIN = "01/01/2022";

        // La vérification est différente en fontion de l'annuaire
        switch (typeAnnuaire)
        {

            case DGCP :
                // Liste des habilitations non nulle
                Assert.assertNotNull("La liste des habilitations devrait être renseignée"
                    , personneAnnuaire.getListeHabilitations());
                // Liste des habilitations d'un élément
                Assert.assertEquals("La liste des habilitations devrait contenir 1 élément (taille = "
                    + personneAnnuaire.getListeHabilitations().size() + ")"
                    ,1 , personneAnnuaire.getListeHabilitations().size());
                // Vérification des valeurs de l'habilitation
                HabilitationAnnuaire habilitationDGCP = personneAnnuaire.getListeHabilitations().iterator().next();
                Assert.assertEquals("Valeur habilitation admin", ADMIN_HABILITATION_1_DGCP_DGFIP
                    , habilitationDGCP.getNomAdministrateur());
                Assert.assertEquals("Valeur habilitation date", 
                    DATE_FORMAT.parse(DATE_HABILITATION_1_DGCP_DGFIP)
                    , habilitationDGCP.getDateCreation());
                Assert.assertEquals("Valeur habilitation profil", PROFIL_HABILITATION_1
                    , habilitationDGCP.getNomProfil());
                // Vérification des attributs spécifiques DGCP
                Assert.assertNotNull(AttributsAnnuaire.MEFIAPPLIHABILITDGCP
                    + " devrait être renseigné", personneAnnuaire.getMefiAppliHabilitDGCP());
                Assert.assertEquals(AttributsAnnuaire.MEFIAPPLIHABILITDGCP.name()
                    , HABILITATION_DGCP_DGFIP_1, personneAnnuaire.getMefiAppliHabilitDGCP());
                Assert.assertNull(AttributsAnnuaire.MEFIAPPLIDGCP
                    + " devrait être non renseigné (valeur = " + personneAnnuaire.getMefiAppliDGCP() + ")"
                    , personneAnnuaire.getMefiAppliDGCP());
                // Vérification des attributs spécifiques autres annuaires
                Assert.assertNull(AttributsAnnuaire.MEFIAPPLIDGI
                    + " devrait être non renseigné (valeur = " + personneAnnuaire.getMefiAppliDGI() + ")"
                    , personneAnnuaire.getMefiAppliDGI());
                Assert.assertNull(AttributsAnnuaire.PROFILAPPLICATIF
                    + " devrait être non renseigné (valeur = " + personneAnnuaire.getProfilApplicatif() + ")"
                    , personneAnnuaire.getProfilApplicatif());
                Assert.assertNull(AttributsAnnuaire.PROFILAPPLICATIFLONG
                    + " devrait être non renseigné (valeur = " + personneAnnuaire.getProfilApplicatifLong() + ")"
                    , personneAnnuaire.getProfilApplicatifLong());
                break;
            case DGI :
                // Liste des habilitations non nulle
                Assert.assertNotNull("La liste des habilitations devrait être renseignée"
                    , personneAnnuaire.getListeHabilitations());
                // Liste des habilitations d'un élément
                Assert.assertEquals("La liste des habilitations devrait contenir 1 élément (taille = "
                    + personneAnnuaire.getListeHabilitations().size() + ")"
                    ,1 , personneAnnuaire.getListeHabilitations().size());
                // Vérification des valeurs de l'habilitation
                HabilitationAnnuaire habilitationDGI = personneAnnuaire.getListeHabilitations().iterator().next();
                Assert.assertEquals("Valeur habilitation admin", ADMIN_HABILITATION_DGI
                    , habilitationDGI.getNomAdministrateur());
                Assert.assertEquals("Valeur habilitation date", 
                    DATE_FORMAT.parse(DATE_HABILITATION_DGI)
                    , habilitationDGI.getDateCreation());
                Assert.assertEquals("Valeur habilitation profil", PROFIL_HABILITATION_1
                    , habilitationDGI.getNomProfil());
                // Vérification des attributs spécifiques DGI
                Assert.assertEquals(AttributsAnnuaire.MEFIAPPLIDGI.name()
                    , APPLICATION_1 + SEPARATEUR_HABILITATION + HABILITATION_DGI_1
                    , personneAnnuaire.getMefiAppliDGI());
                // Vérification des attributs spécifiques autres annuaires
                Assert.assertNull(AttributsAnnuaire.MEFIAPPLIHABILITDGCP
                    + " devrait être non renseigné", personneAnnuaire.getMefiAppliHabilitDGCP());
                Assert.assertNull(AttributsAnnuaire.MEFIAPPLIDGCP
                    + " devrait être non renseigné (valeur = " + personneAnnuaire.getMefiAppliDGCP() + ")"
                    , personneAnnuaire.getMefiAppliDGCP());
                Assert.assertNotNull(PROFIL_HABILITATION_1
                    + " devrait être renseigné", personneAnnuaire.getMefiAppliDGI());
                // Vérification des attributs spécifiques autres annuaires
                Assert.assertNull(AttributsAnnuaire.PROFILAPPLICATIF
                    + " devrait être non renseigné (valeur = " + personneAnnuaire.getProfilApplicatif() + ")"
                    , personneAnnuaire.getProfilApplicatif());
                Assert.assertNull(AttributsAnnuaire.PROFILAPPLICATIFLONG
                    + " devrait être non renseigné (valeur = " + personneAnnuaire.getProfilApplicatifLong() + ")"
                    , personneAnnuaire.getProfilApplicatifLong());
                break;
            case DGFIP :
                // Vérification de l'absence de la propriété "MEFIAPPLIHABILITDGCP"
                Assert.assertNull(AttributsAnnuaire.MEFIAPPLIHABILITDGCP
                    + " devrait être non renseigné", personneAnnuaire.getMefiAppliHabilitDGCP());
                // Liste des habilitations non nulle
                Assert.assertNotNull("La liste des habilitations devrait être renseignée"
                    , personneAnnuaire.getListeHabilitations());
                // Liste des habilitations d'un élément
                Assert.assertEquals("La liste des habilitations ne comporte pas le bon nombre d'élément"
                    ,2 , personneAnnuaire.getListeHabilitations().size());

                // Vérification des valeurs de l'habilitation
                Map<String, HabilitationAnnuaire> mapHabilitation = new HashMap<>();
                for (Iterator<HabilitationAnnuaire> iterator = personneAnnuaire.getListeHabilitations().iterator();
                    iterator.hasNext();)
                {
                    HabilitationAnnuaire verifHabilitation = iterator.next();
                    mapHabilitation.put(verifHabilitation.getNomAdministrateur(), verifHabilitation);
                }
                Assert.assertTrue("L'administrateur '" + ADMIN_HABILITATION_1_DGCP_DGFIP + "' n'est pas présent"
                    , mapHabilitation.containsKey(ADMIN_HABILITATION_1_DGCP_DGFIP));
                Assert.assertTrue("L'administrateur '" + ADMIN_HABILITATION_1_DGFIP + "' n'est pas présent"
                    , mapHabilitation.containsKey(ADMIN_HABILITATION_1_DGFIP));
                
                HabilitationAnnuaire habilitationDGFIP = mapHabilitation.get(ADMIN_HABILITATION_1_DGCP_DGFIP);                
                Assert.assertEquals("Valeur habilitation date"
                    , DATE_FORMAT.parse(DATE_HABILITATION_1_DGCP_DGFIP), habilitationDGFIP.getDateCreation());
                Assert.assertEquals("Valeur habilitation profil", PROFIL_HABILITATION_1
                    , habilitationDGFIP.getNomProfil());

                habilitationDGFIP = mapHabilitation.get(ADMIN_HABILITATION_1_DGFIP);                
                Assert.assertEquals("Valeur habilitation profil", PROFIL_HABILITATION_1
                    , habilitationDGFIP.getNomProfil());
                Assert.assertEquals("Valeur habilitation date"
                    , DATE_DEBUT, DATE_FORMAT.format(habilitationDGFIP.getDateCreation()));
                Assert.assertEquals("Valeur habilitation date"
                    , DATE_DEBUT, DATE_FORMAT.format(habilitationDGFIP.getDateDebut()));
                Assert.assertEquals("Valeur habilitation date"
                    , DATE_FIN, DATE_FORMAT.format(habilitationDGFIP.getDateFin()));

                // Vérification des attributs spécifiques autres annuaires
                Assert.assertNull(AttributsAnnuaire.MEFIAPPLIDGCP
                    + " devrait être non renseigné (valeur = " + personneAnnuaire.getMefiAppliDGCP() + ")"
                    , personneAnnuaire.getMefiAppliDGCP());
                Assert.assertNull(AttributsAnnuaire.MEFIAPPLIDGI
                    + " devrait être non renseigné (valeur = " + personneAnnuaire.getMefiAppliDGI() + ")"
                    , personneAnnuaire.getMefiAppliDGI());
                // Vérification des attributs spécifiques DGFIP
                Assert.assertNotNull(AttributsAnnuaire.PROFILAPPLICATIF
                    + " devrait être renseigné ", personneAnnuaire.getProfilApplicatif());
                Assert.assertEquals(PROFILAPPLICATIF_DGFIP_1, personneAnnuaire.getProfilApplicatif());
                Assert.assertNotNull(AttributsAnnuaire.PROFILAPPLICATIFLONG + " devrait être renseigné "
                    , personneAnnuaire.getProfilApplicatifLong());
                Assert.assertEquals(AttributsAnnuaire.PROFILAPPLICATIFLONG.name()
                    , HABILITATION_DGCP_DGFIP_1, personneAnnuaire.getProfilApplicatifLong());
                break;
            case USAGERS :
                // Vérification de l'absence de la propriété "MEFIAPPLIHABILITDGCP"
                Assert.assertNull(AttributsAnnuaire.MEFIAPPLIHABILITDGCP
                    + " devrait être non renseigné", personneAnnuaire.getMefiAppliHabilitDGCP());
                // Liste des habilitations non nulle
                Assert.assertNotNull("La liste des habilitations devrait être renseignée"
                    , personneAnnuaire.getListeHabilitations());
                // Liste des habilitations d'un élément
                Assert.assertEquals("La liste des habilitations ne comporte pas le bon nombre d'élément"
                    ,1 , personneAnnuaire.getListeHabilitations().size());
                // Vérification des valeurs de l'habilitation
                HabilitationAnnuaire habilitationUsager = personneAnnuaire.getListeHabilitations().iterator().next();
                Assert.assertEquals("Valeur habilitation profil", PROFIL_HABILITATION_USAGER
                    , habilitationUsager.getNomProfil());
                Assert.assertEquals("Valeur habilitation date"
                    , DATE_DEBUT, DATE_FORMAT.format(habilitationUsager.getDateCreation()));
                Assert.assertEquals("Valeur habilitation date"
                    , DATE_DEBUT, DATE_FORMAT.format(habilitationUsager.getDateDebut()));
                Assert.assertEquals("Valeur habilitation date"
                    , DATE_FIN, DATE_FORMAT.format(habilitationUsager.getDateFin()));
                Assert.assertNotNull(AttributsAnnuaire.PROFILAPPLICATIF
                    + " devrait être renseigné (valeur = " + personneAnnuaire.getProfilApplicatif() + ")"
                    , personneAnnuaire.getProfilApplicatif());
                // Vérification des attributs spécifiques DGI
                Assert.assertNull(AttributsAnnuaire.MEFIAPPLIDGI.name()
                    , personneAnnuaire.getMefiAppliDGI());
                // Vérification des attributs spécifiques autres annuaires
                Assert.assertNull(AttributsAnnuaire.MEFIAPPLIHABILITDGCP
                    + " devrait être non renseigné", personneAnnuaire.getMefiAppliHabilitDGCP());
                Assert.assertNull(AttributsAnnuaire.MEFIAPPLIDGCP
                    + " devrait être non renseigné (valeur = " + personneAnnuaire.getMefiAppliDGCP() + ")"
                    , personneAnnuaire.getMefiAppliDGCP());
                Assert.assertNull(PROFIL_HABILITATION_1
                    + " ne devrait pas être renseigné", personneAnnuaire.getMefiAppliDGI());
                // Vérification des attributs spécifiques autres annuaires
                Assert.assertNull(AttributsAnnuaire.PROFILAPPLICATIFLONG
                    + " devrait être non renseigné (valeur = " + personneAnnuaire.getProfilApplicatifLong() + ")"
                    , personneAnnuaire.getProfilApplicatifLong());
                break;
        }
        LOG.trace("Vérification des habilitations OK");
    }

    /**
     * Méthode permettant de vérifier les valeurs de chaque propriété de la personne après
     * association avec les attributs de l'annuaire
     *
     * @param typeAnnuaire type d'annuaire simulé
     * @param personneAnnuaire personne alimentée avec les informations de l'annuaire
     */
    private void verificationValeurAttributsPersonne(TypeAnnuaire typeAnnuaire, PersonneAnnuaire personneAnnuaire)
    {
        LOG.trace("Vérification des valeurs de chaque propriété de la personne");

        Assert.assertEquals(AttributsAnnuaire.AFFECTATION.name(), AFFECTATION, personneAnnuaire.getAffectation());
        Assert.assertEquals(AttributsAnnuaire.AFFECTATIONANNEXE.name(), AFFECTATIONANNEXE, personneAnnuaire.getAffectationANNEXE());
        Assert.assertEquals(AttributsAnnuaire.AFFECTATIONCODIQUE.name(), AFFECTATIONCODIQUE, personneAnnuaire.getAffectationCODIQUE());
        Assert.assertEquals(AttributsAnnuaire.AFFECTATIONSAGES.name(), AFFECTATIONSAGES, personneAnnuaire.getAffectationSAGES());
        Assert.assertEquals(AttributsAnnuaire.ANNEXE.name(), ANNEXE, personneAnnuaire.getAnnexe());
        Assert.assertEquals(AttributsAnnuaire.BUSINESSCATEGORY.name(), BUSINESSCATEGORY, personneAnnuaire.getBusinessCategory());
        Assert.assertEquals(AttributsAnnuaire.CATEGORIE.name(), CATEGORIE, personneAnnuaire.getCategorie());
        Assert.assertEquals(AttributsAnnuaire.CN.name(), CN, personneAnnuaire.getCn());
        Assert.assertEquals(AttributsAnnuaire.CODEFONCTION.name(), CODEFONCTION, personneAnnuaire.getCodeFonction());
        Assert.assertEquals(AttributsAnnuaire.CODEGRADE.name(), CODEGRADE, personneAnnuaire.getCodeGrade());
        Assert.assertEquals(AttributsAnnuaire.CODEGRADERH.name(), CODEGRADERH, personneAnnuaire.getCodeGradeRH());
        Assert.assertEquals("Valeur habilitation date", DATE_EXPIRATION_HABILITATION
            , DATE_FORMAT.format(personneAnnuaire.getDateExpirationDerniereHabilitation()));
        Assert.assertEquals(AttributsAnnuaire.DEPARTEMENT.name(), DEPARTEMENT, personneAnnuaire.getDepartement());
        Assert.assertEquals(AttributsAnnuaire.DEPARTMENT.name(), DEPARTMENT, personneAnnuaire.getDepartment());
        Assert.assertEquals(AttributsAnnuaire.EMPSUP.name(), EMPSUP, personneAnnuaire.getEmpsup());
        Assert.assertEquals(AttributsAnnuaire.FONCTION.name(), FONCTION, personneAnnuaire.getFonction());
        Assert.assertEquals(AttributsAnnuaire.GIVENNAME.name(), GIVENNAME, personneAnnuaire.getGivenName());
        Assert.assertEquals(AttributsAnnuaire.IDENTDGI.name(), IDENTDGI, personneAnnuaire.getIdentDGI());
        Assert.assertEquals(AttributsAnnuaire.MAIL.name(), MAIL, personneAnnuaire.getMail());
        Assert.assertEquals(AttributsAnnuaire.MAILDELIVERYOPTION.name(), MAILDELIVERYOPTION, personneAnnuaire.getMailDeliveryOption());
        Assert.assertEquals(AttributsAnnuaire.MEFIPOINTMAJF.name(), MEFIPOINTMAJF, personneAnnuaire.getMefiPointMAJF());
        Assert.assertEquals(AttributsAnnuaire.NIVEAURESPONSABILITE.name(), NIVEAURESPONSABILITE, personneAnnuaire.getNiveauResponsabilite());
        Assert.assertEquals(AttributsAnnuaire.OU.name(), OU, personneAnnuaire.getOu());
        Assert.assertEquals(AttributsAnnuaire.OUCOMPLET.name(), OUCOMPLET, personneAnnuaire.getOuComplet());
        Assert.assertEquals(AttributsAnnuaire.OUDESCRIPTION.name(), OUDESCRIPTION, personneAnnuaire.getOuDescription());
        Assert.assertEquals(AttributsAnnuaire.OUSIGLE.name(), OUSIGLE, personneAnnuaire.getOuSigle());
        Assert.assertEquals(AttributsAnnuaire.PASSWORDEXPIRATIONTIME.name(), PASSWORDEXPIRATIONTIME, personneAnnuaire.getPasswordExpirationTime());
        Assert.assertEquals(AttributsAnnuaire.PERSONALTITLE.name(), PERSONALTITLE, personneAnnuaire.getPersonalTitle());
        Assert.assertEquals(AttributsAnnuaire.PHYSICALDELIVERYOFFICENAME.name(), PHYSICALDELIVERYOFFICENAME, personneAnnuaire.getPhysicalDeliveryOfficeName());
        Assert.assertEquals(AttributsAnnuaire.POSTALADDRESS.name(), POSTALADDRESS, personneAnnuaire.getPostalAddress());
        Assert.assertEquals(AttributsAnnuaire.POSTALCODE.name(), POSTALCODE, personneAnnuaire.getPostalCode());
        Assert.assertEquals(AttributsAnnuaire.QUESTIONREPONSE.name(), QUESTIONREPONSE, personneAnnuaire.getQuestionreponse());
        Assert.assertEquals(AttributsAnnuaire.REGION.name(), REGION, personneAnnuaire.getRegion());
        Assert.assertEquals(AttributsAnnuaire.ROOMNUMBER.name(), ROOMNUMBER, personneAnnuaire.getRoomNumber());
        Assert.assertEquals(AttributsAnnuaire.SAGES.name(), SAGES, personneAnnuaire.getSages());
        Assert.assertEquals(AttributsAnnuaire.SAMBANTPASSWORD.name(), SAMBANTPASSWORD, personneAnnuaire.getSambaNTPassword());
        Assert.assertEquals(AttributsAnnuaire.SN.name(), SN, personneAnnuaire.getSn());
        Assert.assertEquals(AttributsAnnuaire.TELEPHONENUMBER.name(), TELEPHONENUMBER, personneAnnuaire.getTelephoneNumber());
        Assert.assertEquals(AttributsAnnuaire.TITLE.name(), TITLE, personneAnnuaire.getTitle());
        Assert.assertEquals(AttributsAnnuaire.TYPECERTIFICAT.name()
            , TYPECERTIFICAT, personneAnnuaire.getTypeCertificat());
        Assert.assertEquals(AttributsAnnuaire.UIDFONCTIONNEL.name()
            , UIDFONCTIONNEL, personneAnnuaire.getUidFonctionnel());
        Assert.assertEquals(AttributsAnnuaire.UIDRH.name(), UIDRH, personneAnnuaire.getUidRH());
        Assert.assertEquals(AttributsAnnuaire.USERCERTIFICATE.name()
            , USERCERTIFICATE, personneAnnuaire.getUserCertificate());
        Assert.assertEquals(AttributsAnnuaire.USERPASSWORD.name()
            , USERPASSWORD, personneAnnuaire.getUserPassword());
        Assert.assertEquals("ListeBruteAffectationsSecondaires"
            , AFFECTATIONSSECONDAIRES, personneAnnuaire.getListeBruteAffectationsSecondaires());
    }

    /**
     * Méthode permettant de simuler les attributs générés par un annuaire
     *
     * @param typeAnnuaire type d'annuaire simulé
     * @return les attributs générés par l'annuaire
     */
    private Attributes getAttributesSimulationAnnuaire(TypeAnnuaire typeAnnuaire)
    {
        LOG.trace("Simulation des attributs générés par l'annuaire '{}'", typeAnnuaire);

        Attributes attributes = new BasicAttributes();

        // Attributs communs aux annuaires
        attributes.put(AttributsAnnuaire.AFFECTATION.name(), AFFECTATION);
        attributes.put(AttributsAnnuaire.AFFECTATIONANNEXE.name(), AFFECTATIONANNEXE);
        attributes.put(AttributsAnnuaire.AFFECTATIONCODIQUE.name(), AFFECTATIONCODIQUE);
        attributes.put(AttributsAnnuaire.AFFECTATIONSAGES.name(), AFFECTATIONSAGES);
        attributes.put(getAttributeMultiple(AttributsAnnuaire.AFFECTATIONSSECONDAIRES.name()
            , AFFECTATIONSSECONDAIRES));
        attributes.put(AttributsAnnuaire.ANNEXE.name(), ANNEXE);
        attributes.put(AttributsAnnuaire.BUSINESSCATEGORY.name(), BUSINESSCATEGORY);
        attributes.put(AttributsAnnuaire.CATEGORIE.name(), CATEGORIE);
        attributes.put(AttributsAnnuaire.CN.name(), CN);
        attributes.put(AttributsAnnuaire.CODEFONCTION.name(), CODEFONCTION);
        attributes.put(AttributsAnnuaire.CODEGRADE.name(), CODEGRADE);
        attributes.put(AttributsAnnuaire.CODEGRADERH.name(), CODEGRADERH);
        attributes.put(AttributsAnnuaire.DEPARTEMENT.name(), DEPARTEMENT);
        attributes.put(AttributsAnnuaire.DEPARTMENT.name(), DEPARTMENT);
        attributes.put(AttributsAnnuaire.EMPSUP.name(), EMPSUP);
        attributes.put(AttributsAnnuaire.FONCTION.name(), FONCTION);
        attributes.put(AttributsAnnuaire.GIVENNAME.name(), GIVENNAME);
        attributes.put(AttributsAnnuaire.IDENTDGI.name(), IDENTDGI);
        attributes.put(AttributsAnnuaire.MAIL.name(), MAIL);
        attributes.put(AttributsAnnuaire.MAILDELIVERYOPTION.name(), MAILDELIVERYOPTION);
        attributes.put(AttributsAnnuaire.MEFIPOINTMAJF.name(), MEFIPOINTMAJF);
        attributes.put(AttributsAnnuaire.NIVEAURESPONSABILITE.name(), NIVEAURESPONSABILITE);
        attributes.put(AttributsAnnuaire.OU.name(), OU);
        attributes.put(AttributsAnnuaire.OUCOMPLET.name(), OUCOMPLET);
        attributes.put(AttributsAnnuaire.OUDESCRIPTION.name(), OUDESCRIPTION);
        attributes.put(AttributsAnnuaire.OUSIGLE.name(), OUSIGLE);
        attributes.put(AttributsAnnuaire.PASSWORDEXPIRATIONTIME.name(), PASSWORDEXPIRATIONTIME);
        attributes.put(AttributsAnnuaire.PERSONALTITLE.name(), PERSONALTITLE);
        attributes.put(AttributsAnnuaire.PHYSICALDELIVERYOFFICENAME.name(), PHYSICALDELIVERYOFFICENAME);
        attributes.put(AttributsAnnuaire.POSTALADDRESS.name(), POSTALADDRESS);
        attributes.put(AttributsAnnuaire.POSTALCODE.name(), POSTALCODE);
        attributes.put(AttributsAnnuaire.PROFILAPPLICATIF.name(), PROFILAPPLICATIF);
        attributes.put(AttributsAnnuaire.PROFILAPPLICATIFLONG.name(), PROFILAPPLICATIFLONG);
        attributes.put(AttributsAnnuaire.QUESTIONREPONSE.name(), QUESTIONREPONSE);
        attributes.put(AttributsAnnuaire.REGION.name(), REGION);
        attributes.put(AttributsAnnuaire.ROOMNUMBER.name(), ROOMNUMBER);
        attributes.put(AttributsAnnuaire.SAGES.name(), SAGES);
        attributes.put(AttributsAnnuaire.SAMBANTPASSWORD.name(), SAMBANTPASSWORD);
        attributes.put(AttributsAnnuaire.SN.name(), SN);
        attributes.put(AttributsAnnuaire.TELEPHONENUMBER.name(), TELEPHONENUMBER);
        attributes.put(AttributsAnnuaire.TITLE.name(), TITLE);
        attributes.put(AttributsAnnuaire.TYPECERTIFICAT.name(), TYPECERTIFICAT);
        attributes.put(AttributsAnnuaire.UIDFONCTIONNEL.name(), UIDFONCTIONNEL);
        attributes.put(AttributsAnnuaire.UIDRH.name(), UIDRH);
        attributes.put(AttributsAnnuaire.USERCERTIFICATE.name(), USERCERTIFICATE);
        attributes.put(AttributsAnnuaire.USERPASSWORD.name(), USERPASSWORD);
        // Ajout d'un attribut non géré
        attributes.put(ATTRIBUT_NON_GERE, ATTRIBUT_NON_GERE);
        
        // Attributs spécifiques à certains annuaires
        switch (typeAnnuaire)
        {
            case DGCP :
                attributes.put(AttributsAnnuaire.UID.name(), UID_DGCP);
                attributes.put(getAttributeMultiple(AttributsAnnuaire.MEFIAPPLIHABILITDGCP.name()
                    , HABILITATION_DGCP_DGFIP));
                break;
            case DGI :
                attributes.put(AttributsAnnuaire.UID.name(), UID_DGI);
                attributes.put(getAttributeMultiple(AttributsAnnuaire.MEFIAPPLIDGI.name()
                    , MEFIAPPLIDGI));
                break;
            case DGFIP :
                attributes.put(AttributsAnnuaire.UID.name(), UID_DGFIP);
                attributes.put(getAttributeMultiple(AttributsAnnuaire.PROFILAPPLICATIF.name()
                    , PROFILAPPLICATIF_DGFIP));
                attributes.put(getAttributeMultiple(AttributsAnnuaire.PROFILAPPLICATIFLONG.name()
                    , HABILITATION_DGCP_DGFIP));
                attributes.put(AttributsAnnuaire.CODEGRADESIRHIUS.name(), CODEGRADESIRHIUS);
                attributes.put(AttributsAnnuaire.UIDSIRHIUS.name(), UIDSIRHIUS);
                attributes.put(AttributsAnnuaire.CODECSRH.name(), CODECSRH);
                break;
            case USAGERS :
                attributes.put(AttributsAnnuaire.UID.name(), UID_USAGERS);
                attributes.put(AttributsAnnuaire.SPI.name(), SPI);
                attributes.put(getAttributeMultiple(AttributsAnnuaire.IDORGANISME.name(), IDORGANISME));
                attributes.put(getAttributeMultiple(AttributsAnnuaire.PROFILAPPLICATIF.name()
                    , PROFILAPPLICATIF_USAGERS));
                break;
        }

        LOG.debug("Arguments annuaire : {}", attributes);
        
        return attributes;
    }
    
    /**
     * Méthode permettant de fournir un attribut, d'un annuaire, multivalué
     *
     * @param nomAttribut nom de l'attribut
     * @param valeurs valeurs de l'attribut
     * @return un attribut multivalué
     */
    private Attribute getAttributeMultiple(String nomAttribut, List<String> valeurs)
    {
        Attribute attribute = new BasicAttribute(nomAttribut);
        for (String valeur : valeurs)
        {
            attribute.add(valeur);
        }
        return attribute;
    }
}
