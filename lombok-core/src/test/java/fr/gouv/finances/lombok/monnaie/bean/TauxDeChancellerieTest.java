/*
 * Copyright (c) 2016 DGFiP - Tous droits réservés
 * 
 */
package fr.gouv.finances.lombok.monnaie.bean;

import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;

import org.junit.Test;

import fr.gouv.finances.lombok.test.pojo.AbstractCorePojoTest;

/**
 * Tests unitaires de l'entité TauxDeChancellerie.
 * 
 * @author Christophe Breheret-Girardin
 */
public class TauxDeChancellerieTest extends AbstractCorePojoTest<TauxDeChancellerie>
{
    /**
     * Constructeur.
     */
    public TauxDeChancellerieTest()
    {
        super();
    }

    /**
     * Méthode permettant de tester l'égalité du POJO.
     */
    @Test
    public final void testEqualsHashCode()
    {
        EqualsVerifier.forClass(getParameterizedType())
            .suppress(Warning.NONFINAL_FIELDS, Warning.STRICT_INHERITANCE)
            .withPrefabValues(Monnaie.class, getMonnaie(1), getMonnaie(2))
            .withOnlyTheseFields("valeurTauxChancel", "dateTauxChancel")
            .usingGetClass()
            .verify();
    }

    /**
     * Méthode permettant de générer une monnaie.
     *
     * @param indice indice permettant d'ajouter du dynamisme
     * @return la monnaie générée
     */
    private Monnaie getMonnaie(int indice)
    {
        Monnaie monnaie = new Monnaie();
        monnaie.setCodeBDF(String.valueOf(indice));
        return monnaie;
    }
}
