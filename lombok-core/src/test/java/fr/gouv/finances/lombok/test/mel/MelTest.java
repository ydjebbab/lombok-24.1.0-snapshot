/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : MelTest.java
 *
 */
package fr.gouv.finances.lombok.test.mel;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.gouv.finances.lombok.mel.service.MelService;
import fr.gouv.finances.lombok.mel.service.impl.MelServiceImpl;
import fr.gouv.finances.lombok.mel.techbean.Mel;
import fr.gouv.finances.lombok.mel.techbean.PieceJointe;
import fr.gouv.finances.lombok.mel.techbean.PieceJointeInline;

/**
 * Class MelTest 
 * 
 * @version $Revision: 1.4 $ Date: 11 déc. 2009
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
        "classpath:/conf/testpropertysource.xml",
        "classpath:/conf/applicationContext-resources.xml",
        "classpath:/conf/applicationContext-commun-service.xml",})
public class MelTest
{
    private final Logger log = LoggerFactory.getLogger(getClass());

    /** unservicemel. */
    private MelService serveurMel;

    private MelService serveurMelStartTls;

    /** adressemel1. */
    private final String adressemel1 = "yussouf.goulamhoussen@dgfip.finances.gouv.fr";

    /** adressemel2. */
    private final String adressemel2 = "christian.houard@dgfip.finances.gouv.fr";

    /** adressemel3. */
    private final String adressemel3 = "bureau.si1a-lombok@dgfip.finances.gouv.fr";

    /** adressemel4. */
    private final String adressemel4 = "jean-luc.razafimandimby@dgfip.finances.gouv.fr";

    @Before
    public void intializeMelServices()
    {
        if (serveurMelStartTls == null)
        {
            // création d'un serveur de mel avec relai bouchonné, sans remise et en starttls
            // voir ici: http://smtpza-test-bouchon-sans-remise.infra.dgfip/
            // il est possible de rechercher les mails qui sont stockés pendant 30jours
            //
            serveurMelStartTls = new MelServiceImpl();
            // unservicemel.setHost("smtp.oc.dgfip");
            serveurMelStartTls.setHost("smtpza-test-bouchon-sans-remise.infra.dgfip"); // serveur bouchonné sans remise
            serveurMelStartTls.setPort(587);
            serveurMelStartTls.setUserName("test@dgfip.finances.gouv.fr");
            serveurMelStartTls.setPassword("testmdp");
            
            //Properties javamailproperties;
            //javamailproperties = new Properties();
            //javamailproperties.setProperty("mail.smtp.auth", "true");
            //javamailproperties.setProperty("mail.smtp.starttls.enable", "true");
            //javamailproperties.setProperty("mail.smtp.ssl.trust", "smtpza-test-bouchon-sans-remise.infra.dgfip");
            //javamailproperties.setProperty("mail.debug", "true");


            //serveurMelStartTls.setJavaMailProperties(javamailproperties);
        }
        if (serveurMel == null)
        {
            // création d'un serveur de mel avec relai sans authentification et bouchonné
            // on utilise pas le serveur avec remise car il a un délai de 1 seconde avant de le réutiliser
            //
            serveurMel = new MelServiceImpl();
            serveurMel.setHost("smtpza-test-bouchon-sans-remise.infra.dgfip");
            // serveurMel.setHost("smtpza-test-avec-remise.infra.dgfip");
            // serveurMel.setHost("smtp.oc.dgfip");
            // serveurMel.setUserName("test@dgfip.finances.gouv.fr");
            // serveurMel.setPassword("testmdp");
        }
    }

    /**
     * Methode Test envoyer mel avec code regexp RFC_822: --.
     * Ajouter + et = dans le mel afin de tester la validité l'adresse mel.
     */
    @Test
    public void testEnvoyerMelRegexpRFC5322()
    {    
        Mel unmel = new Mel();
        unmel.setCodeMailRegexp("RFC_5322");
        unmel.ajouterDestinataireA("anne-marie.leplatinec@dgfip.finances.gouv.fr");
        unmel.setDe("christian.houard+=@dgfip.finances.gouv.fr");
        unmel.setObjet("test composant mel simple");
        unmel.setMesssage("hello");

        serveurMel.envoyerMel(unmel);

    }
    
    /**
     * Methode Test envoyer mel avec code regexp RFC_822: --.
     * Ajouter + et = dans le fin de tester la validité l'adresse mel.
     *
     */
    @Test
    public void testEnvoyerMelRegexpRFC822()
    {    
        Mel unmel = new Mel();
        unmel.setCodeMailRegexp("RFC_822");
        unmel.ajouterDestinataireA("anne-marie.leplatinec@dgfip.finances.gouv.fr");
        unmel.setDe("christian.houard+=@dgfip.finances.gouv.fr");
        unmel.setObjet("test composant mel simple");
        unmel.setMesssage("hello");

        serveurMel.envoyerMel(unmel);

    }
    
    /**
     * Methode Test envoyer mel sans definir le code regexp: --.
     * Le code DEFAULT est pris par defaut.
     */
    @Test
    public void testEnvoyerMelRegexpVide()
    {    
        Mel unmel = new Mel();
        unmel.setCodeMailRegexp("");
        unmel.ajouterDestinataireA("anne-marie.leplatinec@dgfip.finances.gouv.fr");
        unmel.setDe("christian.houard@dgfip.finances.gouv.fr");
        unmel.setObjet("test composant mel simple");
        unmel.setMesssage("hello");

        serveurMel.envoyerMel(unmel);

    }
    
    /**
     * Methode Test envoyer mel avec le code regexp non defnie dans le ficher proprties: --.
     * Dans ce cas le code DEFLAUT est pris en compte et on génère un message exception.
     */
    @Test
    public void testEnvoyerMelRegexpErronee()
    {   
        Mel unmel = new Mel();
        unmel.setCodeMailRegexp("RFC_777");
        unmel.ajouterDestinataireA("anne-marie.leplatinec@dgfip.finances.gouv.fr");
        unmel.setDe("christian.houard@dgfip.finances.gouv.fr");
        unmel.setObjet("test composant mel simple");
        unmel.setMesssage("hello");

        
        serveurMel.envoyerMel(unmel);

    }
    
    /**
     * methode Test envoyer mel : --.
     * @TODO CBRE A réactiver une fois la restructuration de la configuration applicative réparée
     */
    @Test
    public void testEnvoyerMel()
    {

        Mel unmel = new Mel();
        unmel.ajouterDestinataireA(adressemel1);
        unmel.setDe(adressemel2);
        unmel.setObjet("test composant mel simple");
        unmel.setMesssage("hello");

        PieceJointe pj = new PieceJointe("testpj.jar", new ClassPathResource("htmlconverter.jar"));
        unmel.ajouterPieceJointe(pj);
        serveurMel.envoyerMel(unmel);

    }

    @Test
    public void testEnvoyerMelStartTLS()
    {

        Mel unmel = new Mel();
        unmel.ajouterDestinataireA(adressemel1);
        unmel.setDe(adressemel2);
        unmel.setObjet("test composant mel simple, envoi via StartTLS");
        unmel.setMesssage("hello");

        PieceJointe pj = new PieceJointe("testpj.jar", new ClassPathResource("htmlconverter.jar"));
        unmel.ajouterPieceJointe(pj);
        serveurMelStartTls.envoyerMel(unmel);

    }

    /**
     * methode Test envoyer mel avec cc : --.
     */
    @Test
    public void testEnvoyerMelAvecCC()
    {

        Mel unmel = new Mel();

        unmel.ajouterDestinataireA(adressemel1);
        unmel.ajouterDestinataireCc(adressemel3);
        unmel.setDe(adressemel2);
        unmel.setObjet("test composant mel un destinataire en copie");
        unmel.setMesssage("test composant mail");
        serveurMel.envoyerMel(unmel);

    }

    @Test
    public void testEnvoyerMelAvecCCStartTLS()
    {

        Mel unmel = new Mel();

        unmel.ajouterDestinataireA(adressemel1);
        unmel.ajouterDestinataireCc(adressemel3);
        unmel.setDe(adressemel2);
        unmel.setObjet("test composant mel un destinataire en copie");
        unmel.setMesssage("test composant mail");
        serveurMelStartTls.envoyerMel(unmel);

    }

    /**
     * methode Test envoyer mel avec c ci : --.
     */
    @Test
    public void testEnvoyerMelAvecCCi()
    {

        Mel unmel = new Mel();

        unmel.ajouterDestinataireA(adressemel1);
        unmel.ajouterDestinataireCci(adressemel3);
        unmel.setDe(adressemel2);
        unmel.setObjet("test composant mel un destinataire en copie cachée");
        unmel.setMesssage("hello");
        serveurMel.envoyerMel(unmel);

    }

    @Test
    public void testEnvoyerMelAvecCCiStartTLS()
    {

        Mel unmel = new Mel();

        unmel.ajouterDestinataireA(adressemel1);
        unmel.ajouterDestinataireCci(adressemel3);
        unmel.setDe(adressemel2);
        unmel.setObjet("test composant mel un destinataire en copie cachée");
        unmel.setMesssage("hello");
        serveurMelStartTls.envoyerMel(unmel);

    }

    /**
     * methode Test envoyer mel avec pj inline : .
     */
    @Test
    public void testEnvoyerMelAvecPJInline()
    {
        Mel unmel = new Mel();

        // Properties mailProperties = serveurMel.getJavaMailProperties();
        // mailProperties.put("mail.smtp.from", adressemel3);

        unmel.ajouterDestinataireA(adressemel2);
        unmel.setDe(adressemel1);
        unmel.setRepondreA(adressemel3);
        unmel.setObjet("test composant mel avec pj inline");
        unmel
            .setMesssage("<body><font face=\"Verdana\">"
                + "<table border=\"0\" width=\"100%\" align=\"CENTER\">"
                + "<tr>"
                + "<td height=\"30\" align=\"center\" valign=\"middle\"><font color=\"#204c70\"><b><a name=\"top\">ORGANISATION DU DEVELOPPEMENT</a></b></font></td>"
                + "</tr> <tr> <td height=\"40\"></td> </tr>"
                + "</table>"
                + "<br/><br/><p><b><a name=\"definitions\">Phases amonts et pilotage</a></b></p>"
                + "<blockquote>"
                + "<p ALIGN=\"JUSTIFY\">De manière générale, les phases amont et le pilotage de projet sont pris en charge "
                + "par des équipes du bureau 3D en liaison avec les maîtrises d'ouvrage. Pour certains projets nécessitant "
                + "une forte connaissance du domaine métier, des équipes du bureau 3C ou 3E mènent ou participent à ces phases.</p>"
                + "</blockquote>" + "<img src=\"cid:firefoxportable@dgfip.finances.gouv.fr\" />" + "</body></html>");
        unmel.setEncodage("ISO-8859-1");
        PieceJointeInline pji =
            new PieceJointeInline("firefoxportable@dgfip.finances.gouv.fr", new ClassPathResource("FirefoxPortable.jpg"));
        unmel.ajouterPieceJointeInline(pji);
        // serveurMel.setJavaMailProperties(mailProperties);
        serveurMel.envoyerMelAvecContenuHTML(unmel);

    }

    @Test
    public void testEnvoyerMelAvecPJInlineStartTLS()
    {
        Mel unmel = new Mel();

        // Properties mailProperties = serveurMel.getJavaMailProperties();
        // mailProperties.put("mail.smtp.from", adressemel3);

        unmel.ajouterDestinataireA(adressemel2);
        unmel.setDe(adressemel1);
        unmel.setRepondreA(adressemel3);
        unmel.setObjet("test composant mel avec pj inline");
        unmel
            .setMesssage("<body><font face=\"Verdana\">"
                + "<table border=\"0\" width=\"100%\" align=\"CENTER\">"
                + "<tr>"
                + "<td height=\"30\" align=\"center\" valign=\"middle\"><font color=\"#204c70\"><b><a name=\"top\">ORGANISATION DU DEVELOPPEMENT</a></b></font></td>"
                + "</tr> <tr> <td height=\"40\"></td> </tr>"
                + "</table>"
                + "<br/><br/><p><b><a name=\"definitions\">Phases amonts et pilotage</a></b></p>"
                + "<blockquote>"
                + "<p ALIGN=\"JUSTIFY\">De manière générale, les phases amont et le pilotage de projet sont pris en charge "
                + "par des équipes du bureau 3D en liaison avec les maîtrises d'ouvrage. Pour certains projets nécessitant "
                + "une forte connaissance du domaine métier, des équipes du bureau 3C ou 3E mènent ou participent à ces phases.</p>"
                + "</blockquote>" + "<img src=\"cid:firefoxportable@dgfip.finances.gouv.fr\" />" + "</body></html>");
        unmel.setEncodage("ISO-8859-1");
        PieceJointeInline pji =
            new PieceJointeInline("firefoxportable@dgfip.finances.gouv.fr", new ClassPathResource("FirefoxPortable.jpg"));
        unmel.ajouterPieceJointeInline(pji);
        // serveurMel.setJavaMailProperties(mailProperties);
        serveurMelStartTls.envoyerMelAvecContenuHTML(unmel);

    }

    /**
     * methode Test envoyer mel destinataire inconnu : --.
     */
    @Test
    public void testEnvoyerMelDestinataireInconnu()
    {

        Mel unmel = new Mel();

        unmel.ajouterDestinataireA("arie.leplatinec@dgfip.finances.gouv.fr");
        unmel.setDe(adressemel2);
        unmel.setObjet("test composant mel destinataire inconnu");
        unmel.setMesssage("hello");

        serveurMel.envoyerMel(unmel);

    }

    /**
     * methode Test envoyer mel destinataire inconnu : --.
     */
    @Test
    public void testEnvoyerMelDestinataireInconnuStartTLS()
    {

        Mel unmel = new Mel();

        unmel.ajouterDestinataireA("arie.leplatinec@dgfip.finances.gouv.fr");
        unmel.setDe(adressemel2);
        unmel.setObjet("test composant mel destinataire inconnu");
        unmel.setMesssage("hello");

        serveurMelStartTls.envoyerMel(unmel);

    }

    /**
     * methode Test envoyer mel html : --.
     */
    @Test
    public void testEnvoyerMelHTML()
    {

        Mel unmel = new Mel();

        unmel.ajouterDestinataireA(adressemel1);
        unmel.setDe(adressemel2);
        unmel.setObjet("test composant mel html");
        unmel
            .setMesssage(

            "<body><font face=\"Verdana\">"
                + "<table border=\"0\" width=\"100%\" align=\"CENTER\">"
                + "<tr>"
                + "<td height=\"30\" align=\"center\" valign=\"middle\"><font color=\"#204c70\"><b><a name=\"top\">ORGANISATION DU DEVELOPPEMENT</a></b></font></td>"
                + "</tr> <tr> <td height=\"40\"></td> </tr>"
                + "</table>"
                + "<br/><br/><p><b><a name=\"definitions\">Phases amonts et pilotage</a></b></p>"
                + "<blockquote>"
                + "<p ALIGN=\"JUSTIFY\">De manière générale, les phases amont et le pilotage de projet sont pris en charge "
                + "par des équipes du bureau 3D en liaison avec les maîtrises d'ouvrage. Pour certains projets nécessitant "
                + "une forte connaissance du domaine métier, des équipes du bureau 3C ou 3E mènent ou participent à ces phases.</p>"
                + "</blockquote>" + "</body></html>");
        unmel.setEncodage("ISO-8859-1");
        serveurMel.envoyerMelAvecContenuHTML(unmel);
    }

    @Test
    public void testEnvoyerMelHTMLStartTLS()
    {

        Mel unmel = new Mel();

        unmel.ajouterDestinataireA(adressemel1);
        unmel.setDe(adressemel2);
        unmel.setObjet("test composant mel html");
        unmel
            .setMesssage(

            "<body><font face=\"Verdana\">"
                + "<table border=\"0\" width=\"100%\" align=\"CENTER\">"
                + "<tr>"
                + "<td height=\"30\" align=\"center\" valign=\"middle\"><font color=\"#204c70\"><b><a name=\"top\">ORGANISATION DU DEVELOPPEMENT</a></b></font></td>"
                + "</tr> <tr> <td height=\"40\"></td> </tr>"
                + "</table>"
                + "<br/><br/><p><b><a name=\"definitions\">Phases amonts et pilotage</a></b></p>"
                + "<blockquote>"
                + "<p ALIGN=\"JUSTIFY\">De manière générale, les phases amont et le pilotage de projet sont pris en charge "
                + "par des équipes du bureau 3D en liaison avec les maîtrises d'ouvrage. Pour certains projets nécessitant "
                + "une forte connaissance du domaine métier, des équipes du bureau 3C ou 3E mènent ou participent à ces phases.</p>"
                + "</blockquote>" + "</body></html>");
        unmel.setEncodage("ISO-8859-1");
        serveurMelStartTls.envoyerMelAvecContenuHTML(unmel);
    }

    /**
     * methode Test envoyer mel nouveau format : --.
     */
    @Test
    public void testEnvoyerMelNouveauFormat()
    {
        Mel unmel = new Mel();

        unmel.ajouterDestinataireA(adressemel1);
        unmel.setDe(adressemel2);
        unmel.setObjet("test composant mel nouveau format simple");
        unmel.setMesssage("hello");

        PieceJointe pj = new PieceJointe("testpj.jar", new ClassPathResource("htmlconverter.jar"));
        unmel.ajouterPieceJointe(pj);
        serveurMel.envoyerMel(unmel);

    }

    @Test
    public void testEnvoyerMelNouveauFormatStartTLS()
    {
        Mel unmel = new Mel();

        unmel.ajouterDestinataireA(adressemel1);
        unmel.setDe(adressemel2);
        unmel.setObjet("test composant mel nouveau format simple");
        unmel.setMesssage("hello");

        PieceJointe pj = new PieceJointe("testpj.jar", new ClassPathResource("htmlconverter.jar"));
        unmel.ajouterPieceJointe(pj);
        serveurMelStartTls.envoyerMel(unmel);

    }

    /**
     * methode Test envoyer mel nouveau format avec cc : --.
     */
    @Test
    public void testEnvoyerMelNouveauFormatAvecCC()
    {

        Mel unmel = new Mel();

        unmel.ajouterDestinataireA(adressemel1);
        unmel.ajouterDestinataireCc(adressemel3);
        unmel.setDe(adressemel2);
        unmel.setObjet("test composant mel nouveau format avec un destinataire en copie");
        unmel.setMesssage("test composant mail");
        serveurMel.envoyerMel(unmel);

    }

    /**
     * methode Test envoyer mel nouveau format avec cc : --.
     */
    @Test
    public void testEnvoyerMelNouveauFormatAvecCCStartTLS()
    {

        Mel unmel = new Mel();

        unmel.ajouterDestinataireA(adressemel1);
        unmel.ajouterDestinataireCc(adressemel3);
        unmel.setDe(adressemel2);
        unmel.setObjet("test composant mel nouveau format avec un destinataire en copie");
        unmel.setMesssage("test composant mail");
        serveurMelStartTls.envoyerMel(unmel);

    }

    /**
     * methode Test envoyer mel nouveau format avec c ci : --.
     */
    @Test
    public void testEnvoyerMelNouveauFormatAvecCCi()
    {

        Mel unmel = new Mel();

        unmel.ajouterDestinataireA(adressemel1);
        unmel.ajouterDestinataireCci(adressemel3);
        unmel.setDe(adressemel2);
        unmel.setObjet("test composant mel nouveau format avec un destinataire en copie cachee");
        unmel.setMesssage("hello");
        serveurMel.envoyerMel(unmel);

    }

    @Test
    public void testEnvoyerMelNouveauFormatAvecCCiStartTLS()
    {

        Mel unmel = new Mel();

        unmel.ajouterDestinataireA(adressemel1);
        unmel.ajouterDestinataireCci(adressemel3);
        unmel.setDe(adressemel2);
        unmel.setObjet("test composant mel nouveau format avec un destinataire en copie cachee");
        unmel.setMesssage("hello");
        serveurMelStartTls.envoyerMel(unmel);

    }

    /**
     * methode Test envoyer mel nouveau format multi destinataires : --.
     */
    @Test
    public void testEnvoyerMelNouveauFormatMultiDestinataires()
    {
        Mel unmel = new Mel();

        unmel.ajouterDestinataireA(adressemel1);
        unmel.ajouterDestinataireA(adressemel2);
        unmel.ajouterDestinataireA(adressemel1);
        unmel.ajouterDestinataireCc(adressemel3);
        unmel.ajouterDestinataireCc(adressemel4);
        unmel.setDe(adressemel2);
        unmel.setObjet("test composant mel multi destinataires (3 en destinataires, 2 en copie)");
        unmel.setMesssage("hello");
        serveurMel.envoyerMel(unmel);

    }

    @Test
    public void testEnvoyerMelNouveauFormatMultiDestinatairesStartTLS()
    {
        Mel unmel = new Mel();

        unmel.ajouterDestinataireA(adressemel1);
        unmel.ajouterDestinataireA(adressemel2);
        unmel.ajouterDestinataireA(adressemel1);
        unmel.ajouterDestinataireCc(adressemel3);
        unmel.ajouterDestinataireCc(adressemel4);
        unmel.setDe(adressemel2);
        unmel.setObjet("test composant mel multi destinataires (3 en destinataires, 2 en copie)");
        unmel.setMesssage("hello");
        serveurMelStartTls.envoyerMel(unmel);

    }

    /**
     * methode Test envoyer mel pj trop grosse : --.
     */
    @Test
    public void testEnvoyerMelPJTropGrosse()
    {

        Mel unmel = new Mel();

        unmel.ajouterDestinataireA(adressemel1);
        unmel.setDe(adressemel2);
        unmel.setObjet("test composant mel");
        unmel.setMesssage("hello");

        PieceJointe pj = new PieceJointe("testpj", new ClassPathResource("tools.jar"));
        unmel.ajouterPieceJointe(pj);

        try
        {
            serveurMel.envoyerMel(unmel);
        }
        catch (RuntimeException exc)
        {

            log.error(exc.getMessage(), exc);
        }

    }

    @Test
    public void testEnvoyerMelPJTropGrosseStartTLS()
    {

        Mel unmel = new Mel();

        unmel.ajouterDestinataireA(adressemel1);
        unmel.setDe(adressemel2);
        unmel.setObjet("test composant mel");
        unmel.setMesssage("hello");

        PieceJointe pj = new PieceJointe("testpj", new ClassPathResource("tools.jar"));
        unmel.ajouterPieceJointe(pj);

        try
        {
            serveurMelStartTls.envoyerMel(unmel);
        }
        catch (RuntimeException exc)
        {

            log.error(exc.getMessage(), exc);
        }

    }

}
