/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : CpCustomEditorHeureTest.java
 *
 */
package fr.gouv.finances.lombok.test.util.propertyeditor;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import fr.gouv.finances.lombok.util.propertyeditor.CpCustomHeureEditor;
import junit.framework.TestCase;

/**
 * Class CpCustomEditorHeureTest --.
 * 
 * @version $Revision: 1.2 $ Date: 11 déc. 2009
 */
public class CpCustomEditorHeureTest extends TestCase
{

    /** Constant : log. */
    protected static final Log LOG = LogFactory.getLog(CpCustomEditorHeureTest.class);

    /**
     * methode Test dates valides1 char sans separateurs : --.
     */
    public void testDatesValides1CharSansSeparateurs()
    {
        SimpleDateFormat fmt = new SimpleDateFormat("hh", Locale.FRANCE);
        Date validHeure = null;
        try
        {
            validHeure = fmt.parse("8");
        }
        catch (ParseException e)
        {
            fail("Heure valide");
        }

        String[] validHeuret = {"8", " 8 ", "    "};

        CpCustomHeureEditor editor = new CpCustomHeureEditor();

        // Teste les dates valides
        for (int i = 0; i < validHeuret.length; i++)
        {
            String validHeureType = validHeuret[i];
            try
            {
                editor.setAsText(validHeureType);

                if (editor.getValue() == null)
                {
                    LOG.info("Texte heure : " + validHeureType + "; Heure trouvée = null ");
                }
                else
                {
                    LOG.info("Texte heure : " + validHeureType + "; Heure trouvée = " + fmt.format(editor.getValue()));
                    assertEquals(validHeure, editor.getValue());
                }
            }
            catch (IllegalArgumentException ex)
            {
                fail("Aucune exception ne devrait être levée, cette heure (" + validHeureType + ") est valide");
            }
        }
    }

    /**
     * methode Test dates valides4 char sans separateurs : --.
     */
    public void testDatesValides4CharSansSeparateurs()
    {
        SimpleDateFormat fmt = new SimpleDateFormat("hh:mm", Locale.FRANCE);
        Date validHeure = null;
        try
        {
            validHeure = fmt.parse("08:12");
        }
        catch (ParseException e)
        {
            fail("Heure valide");
        }

        String[] validHeuret = {"0812", " 0812 ", "    "};

        CpCustomHeureEditor editor = new CpCustomHeureEditor();

        // Teste les dates valides
        for (int i = 0; i < validHeuret.length; i++)
        {
            String validHeureType = validHeuret[i];
            try
            {
                editor.setAsText(validHeureType);

                if (editor.getValue() == null)
                {
                    LOG.info("Texte heure : " + validHeureType + "; Heure trouvée = null ");
                }
                else
                {
                    LOG.info("Texte heure : " + validHeureType + "; Heure trouvée = " + fmt.format(editor.getValue()));
                    assertEquals(validHeure, editor.getValue());
                }
            }
            catch (IllegalArgumentException ex)
            {
                fail("Aucune exception ne devrait être levée, cette heure (" + validHeureType + ") est valide");
            }
        }
    }

    /**
     * methode Test dates valides6 char sans separateurs : --.
     */
    public void testDatesValides6CharSansSeparateurs()
    {
        SimpleDateFormat fmt = new SimpleDateFormat("hh:mm:ss", Locale.FRANCE);
        Date validHeure = null;
        try
        {
            validHeure = fmt.parse("08:12:36");
        }
        catch (ParseException e)
        {
            fail("Heure valide");
        }

        String[] validHeuret = {"081236", " 081236 ", "    "};

        CpCustomHeureEditor editor = new CpCustomHeureEditor();

        // Teste les dates valides
        for (int i = 0; i < validHeuret.length; i++)
        {
            String validHeureType = validHeuret[i];
            try
            {
                editor.setAsText(validHeureType);

                if (editor.getValue() == null)
                {
                    LOG.info("Texte heure : " + validHeureType + "; Heure trouvée = null ");
                }
                else
                {
                    LOG.info("Texte heure : " + validHeureType + "; Heure trouvée = " + fmt.format(editor.getValue()));
                    assertEquals(validHeure, editor.getValue());
                }
            }
            catch (IllegalArgumentException ex)
            {
                fail("Aucune exception ne devrait être levée, cette heure (" + validHeureType + ") est valide");
            }
        }
    }

    /**
     * methode Test dates valides8 char sans separateurs : --.
     */
    public void testDatesValides8CharSansSeparateurs()
    {
        SimpleDateFormat fmt = new SimpleDateFormat("hh:mm:ss:SS", Locale.FRANCE);
        Date validHeure = null;
        try
        {
            validHeure = fmt.parse("08:12:36:52");
        }
        catch (ParseException e)
        {
            fail("Heure valide");
        }

        String[] validHeuret = {"08123652", "    "};

        CpCustomHeureEditor editor = new CpCustomHeureEditor();

        // Teste les dates valides
        for (int i = 0; i < validHeuret.length; i++)
        {
            String validHeureType = validHeuret[i];
            try
            {
                editor.setAsText(validHeureType);

                if (editor.getValue() == null)
                {
                    LOG.info("Texte heure : " + validHeureType + "; Heure trouvée = null ");
                }
                else
                {
                    LOG.info("Texte heure : " + validHeureType + "; Heure trouvée = " + fmt.format(editor.getValue()));
                    assertEquals(validHeure, editor.getValue());
                }
            }
            catch (IllegalArgumentException ex)
            {
                fail("Aucune exception ne devrait être levée, cette heure (" + validHeureType + ") est valide");
            }
        }
    }

    /**
     * methode Test dates valides deux blocs : --.
     */
    public void testDatesValidesDeuxBlocs()
    {
        SimpleDateFormat fmt = new SimpleDateFormat("hh:mm", Locale.FRANCE);
        Date validHeure = null;
        try
        {
            validHeure = fmt.parse("08:12");
        }
        catch (ParseException e)
        {
            fail("Heure valide");
        }

        String[] validHeuret =
        {"08:12", "08 12", "08.12", "8:12", "8 12", "8.12", " 08:: 12", "08  12 ", " 08..12  ", "    "};

        CpCustomHeureEditor editor = new CpCustomHeureEditor();

        // Teste les dates valides
        for (int i = 0; i < validHeuret.length; i++)
        {
            String validHeureType = validHeuret[i];
            try
            {
                editor.setAsText(validHeureType);

                if (editor.getValue() == null)
                {
                    LOG.info("Texte heure : " + validHeureType + "; Heure trouvée = null ");
                }
                else
                {
                    LOG.info("Texte heure : " + validHeureType + "; Heure trouvée = " + fmt.format(editor.getValue()));
                    assertEquals(validHeure, editor.getValue());
                }
            }
            catch (IllegalArgumentException ex)
            {
                fail("Aucune exception ne devrait être levée, cette heure (" + validHeureType + ") est valide");
            }
        }
    }

    /**
     * methode Test dates valides heures minutes secondes milli separateurs longs : --.
     */
    public void testDatesValidesHeuresMinutesSecondesMilliSeparateursLongs()
    {
        SimpleDateFormat fmt = new SimpleDateFormat("hh:mm:ss:SS", Locale.FRANCE);
        Date validHeure = null;
        try
        {
            validHeure = fmt.parse("08:12:08:10");
        }
        catch (ParseException e)
        {
            fail("Heure valide");
        }

        String[] validHeuret = {"8h12m08s10ms", "8h 12 m 08s10ms", "8h12m08s10"};

        CpCustomHeureEditor editor = new CpCustomHeureEditor();

        // Teste les dates valides
        for (int i = 0; i < validHeuret.length; i++)
        {
            String validHeureType = validHeuret[i];
            try
            {
                editor.setAsText(validHeureType);

                if (editor.getValue() == null)
                {
                    LOG.info("Texte heure : " + validHeureType + "; Heure trouvée = null ");
                }
                else
                {
                    LOG.info("Texte heure : " + validHeureType + "; Heure trouvée = " + fmt.format(editor.getValue()));
                    assertEquals(validHeure, editor.getValue());
                }
            }
            catch (IllegalArgumentException ex)
            {
                fail("Aucune exception ne devrait être levée, cette heure (" + validHeureType + ") est valide");
            }
        }
    }

    /**
     * methode Test dates valides heures minutes secondes separateurs longs : --.
     */
    public void testDatesValidesHeuresMinutesSecondesSeparateursLongs()
    {
        SimpleDateFormat fmt = new SimpleDateFormat("hh:mm:ss", Locale.FRANCE);
        Date validHeure = null;
        try
        {
            validHeure = fmt.parse("08:12:08");
        }
        catch (ParseException e)
        {
            fail("Heure valide");
        }

        String[] validHeuret = {"8h12m08s", "8h 12 m 08s", "8h12m08"};

        CpCustomHeureEditor editor = new CpCustomHeureEditor();

        // Teste les dates valides
        for (int i = 0; i < validHeuret.length; i++)
        {
            String validHeureType = validHeuret[i];
            try
            {
                editor.setAsText(validHeureType);

                if (editor.getValue() == null)
                {
                    LOG.info("Texte heure : " + validHeureType + "; Heure trouvée = null ");
                }
                else
                {
                    LOG.info("Texte heure : " + validHeureType + "; Heure trouvée = " + fmt.format(editor.getValue()));
                    assertEquals(validHeure, editor.getValue());
                }
            }
            catch (IllegalArgumentException ex)
            {
                fail("Aucune exception ne devrait être levée, cette heure (" + validHeureType + ") est valide");
            }
        }
    }

    /**
     * methode Test dates valides heures minutes separateurs longs : --.
     */
    public void testDatesValidesHeuresMinutesSeparateursLongs()
    {
        SimpleDateFormat fmt = new SimpleDateFormat("hh:mm", Locale.FRANCE);
        Date validHeure = null;
        try
        {
            validHeure = fmt.parse("08:12");
        }
        catch (ParseException e)
        {
            fail("Heure valide");
        }

        String[] validHeuret = {"8h12m", "8h 12 m", "8h12"};

        CpCustomHeureEditor editor = new CpCustomHeureEditor();

        // Teste les dates valides
        for (int i = 0; i < validHeuret.length; i++)
        {
            String validHeureType = validHeuret[i];
            try
            {
                editor.setAsText(validHeureType);

                if (editor.getValue() == null)
                {
                    LOG.info("Texte heure : " + validHeureType + "; Heure trouvée = null ");
                }
                else
                {
                    LOG.info("Texte heure : " + validHeureType + "; Heure trouvée = " + fmt.format(editor.getValue()));
                    assertEquals(validHeure, editor.getValue());
                }
            }
            catch (IllegalArgumentException ex)
            {
                fail("Aucune exception ne devrait être levée, cette heure (" + validHeureType + ") est valide");
            }
        }
    }

    /**
     * methode Test dates valides quatre blocs : --.
     */
    public void testDatesValidesQuatreBlocs()
    {
        SimpleDateFormat fmt = new SimpleDateFormat("hh:mm:ss:SS", Locale.FRANCE);
        Date validHeure = null;
        try
        {
            validHeure = fmt.parse("08:12:36:06");
        }
        catch (ParseException e)
        {
            fail("Heure valide");
        }

        String[] validHeuret =
        {"08:12:36:06", "08 12 36 06", "08.12.36.06", "8:12:36:6", "8 12 36 6", "8.12.36.6", " 08: 12: 36::06",
                "08  12  36  06", " 08..12. 36.06 ", "    "};

        CpCustomHeureEditor editor = new CpCustomHeureEditor();

        // Teste les dates valides
        for (int i = 0; i < validHeuret.length; i++)
        {
            String validHeureType = validHeuret[i];
            try
            {
                editor.setAsText(validHeureType);

                if (editor.getValue() == null)
                {
                    LOG.info("Texte heure : " + validHeureType + "; Heure trouvée = null ");
                }
                else
                {
                    LOG.info("Texte heure : " + validHeureType + "; Heure trouvée = " + fmt.format(editor.getValue()));
                    assertEquals(validHeure, editor.getValue());
                }
            }
            catch (IllegalArgumentException ex)
            {
                fail("Aucune exception ne devrait être levée, cette heure (" + validHeureType + ") est valide");
            }
        }
    }

    /**
     * methode Test dates valides trois blocs : --.
     */
    public void testDatesValidesTroisBlocs()
    {
        SimpleDateFormat fmt = new SimpleDateFormat("hh:mm:ss", Locale.FRANCE);
        Date validHeure = null;
        try
        {
            validHeure = fmt.parse("08:12:36");
        }
        catch (ParseException e)
        {
            fail("Heure valide");
        }

        String[] validHeuret =
        {"08:12:36", "08 12 36", "08.12.36", "8:12:36", "8 12 36", "8.12.36", " 08: 12:: 36", "08  12  36  ",
                " 08..12. 36 ", "    "};

        CpCustomHeureEditor editor = new CpCustomHeureEditor();

        // Teste les dates valides
        for (int i = 0; i < validHeuret.length; i++)
        {
            String validHeureType = validHeuret[i];
            try
            {
                editor.setAsText(validHeureType);

                if (editor.getValue() == null)
                {
                    LOG.info("Texte heure : " + validHeureType + "; Heure trouvée = null ");
                }
                else
                {
                    LOG.info("Texte heure : " + validHeureType + "; Heure trouvée = " + fmt.format(editor.getValue()));
                    assertEquals(validHeure, editor.getValue());
                }
            }
            catch (IllegalArgumentException ex)
            {
                fail("Aucune exception ne devrait être levée, cette heure (" + validHeureType + ") est valide");
            }
        }
    }

    /**
     * methode Test dates valides un bloc : --.
     */
    public void testDatesValidesUnBloc()
    {
        SimpleDateFormat fmt = new SimpleDateFormat("hh", Locale.FRANCE);
        Date validHeure = null;
        try
        {
            validHeure = fmt.parse("08");
        }
        catch (ParseException e)
        {
            fail("Heure valide");
        }

        String[] validHeuret = {"08", " 08 ", "8", "    "};

        CpCustomHeureEditor editor = new CpCustomHeureEditor();

        // Teste les dates valides
        for (int i = 0; i < validHeuret.length; i++)
        {
            String validHeureType = validHeuret[i];
            try
            {
                editor.setAsText(validHeureType);

                if (editor.getValue() == null)
                {
                    LOG.info("Texte heure : " + validHeureType + "; Heure trouvée = null ");
                }
                else
                {
                    LOG.info("Texte heure : " + validHeureType + "; Heure trouvée = " + fmt.format(editor.getValue()));
                    assertEquals(validHeure, editor.getValue());
                }
            }
            catch (IllegalArgumentException ex)
            {
                fail("Aucune exception ne devrait être levée, cette heure (" + validHeureType + ") est valide");
            }
        }
    }

    /*
     * public void testDatesInvalides() { String [] invalidDate_t = {
     * "01/01 2006","01a01a2006","01.01/2006","01.01 2006","01/01.2006", "01/15/2006" }; CpCustomDateEditor editor = new
     * CpCustomDateEditor(); //Teste les dates valides for (int i = 0; i < invalidDate_t.length; i++) { String
     * invalidDateType = invalidDate_t[i]; try { editor.setAsText(invalidDateType);
     * fail("Une exception devrait être levée, cette date (" + invalidDateType + ") n'est valide"); } catch
     * (IllegalArgumentException ex) { log.info("date invalide : " + invalidDateType); } } }
     */

}
