package fr.gouv.finances.lombok.upload.dao;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.MimeTypeUtils;

import fr.gouv.finances.lombok.test.controle.ControleDonnees;
import fr.gouv.finances.lombok.test.controle.VerificationExecution;
import fr.gouv.finances.lombok.upload.bean.ContenuFichier;
import fr.gouv.finances.lombok.upload.bean.FichierJoint;
import fr.gouv.finances.lombok.util.base.PurgeTablesDao;
import fr.gouv.finances.lombok.util.date.TemporaliteUtils;
import fr.gouv.finances.lombok.util.exception.ProgrammationException;

/**
 * Socle de test de gestion des données liées aux fichiers joints.
 *
 * @author Christophe Breheret-Girardin
 */
public abstract class FichierJointDaoCoreTest
{
    /** Initialisation de la journalisation. */
    protected static final Logger LOGGER = LoggerFactory.getLogger(FichierJointDaoCoreTest.class);

    /** Déclaration du DAO de gestion des données des fichiers joints. */
    @Autowired
    protected FichierJointDao fichierjointdao;

    /** Déclaration du DAO de gestion des purges de la base. */
    @Autowired
    protected PurgeTablesDao purgetablesdao;

    /** Initialisation d'une règle sur chaque test. */
    @Rule
    public TestName testUnitaire = new TestName();

    /** Map contenant les fichiers joints présents en base */
    private Map<Integer, FichierJoint> mapFichierJoint;

    /** Map contenant les contenus des fichiers joints présents en base */
    private Map<Integer, ContenuFichier> mapContenu;

    /**
     * Initialisation des données en base avant chaque test.
     */
    @Before
    public void creerDonnees()
    {
        LOGGER.debug("Début d'initialisation du test '{}'", testUnitaire.getMethodName());

        // Initialisation des bouchons
        MockitoAnnotations.initMocks(this);

        // Purge des données de la base
        purgetablesdao.purgerTables("ZFICHIERJOINT_ZFIJ");

        // Vérification de l'absence de données suite à la purge
        ControleDonnees.verifierElements(fichierjointdao.loadAllObjects(FichierJoint.class));

        mapContenu = IntStream.of(1, 3, 4, 6).parallel().mapToObj(this::getContenufichier)
            .collect(Collectors.toMap(contenu ->
                Integer.parseInt(new String(contenu.getData())), contenu -> contenu));

        // Génération des historiques de travaux d'édition, insertion en base de données et sauvegarde dans une Map
        mapFichierJoint = IntStream.rangeClosed(1, 6).parallel().mapToObj(this::getFichierJoint)
            .peek(fichierjointdao::saveObject)
            .collect(Collectors.toMap(fj -> Integer.parseInt(fj.getNomFichierOriginal()), fj -> fj));

        // Vérification des nouvelles données à partir de l'image de la base
        verifierFichiersJoints(fichierjointdao.loadAllObjects(FichierJoint.class)
            , mapFichierJoint.get(1), mapFichierJoint.get(2), mapFichierJoint.get(3)
            , mapFichierJoint.get(4), mapFichierJoint.get(5), mapFichierJoint.get(6));

        LOGGER.debug("Fin d'initialisation du test '{}'", testUnitaire.getMethodName());
    }

    /**
     * Test de la méthode
     * fr.gouv.finances.lombok.upload.dao.FichierJointDao#deleteCollectionFichiersJoints(java.util.Collection).
     */
    @Test
    public void deleteCollectionFichiersJoints()
    {
        // Exécution de la méthode à tester
        fichierjointdao.deleteCollectionFichiersJoints(
            Arrays.asList(mapFichierJoint.get(1), mapFichierJoint.get(2), mapFichierJoint.get(3)));

        // Vérification
        verifierFichiersJoints(fichierjointdao.loadAllObjects(FichierJoint.class)
            , mapFichierJoint.get(4), mapFichierJoint.get(5), mapFichierJoint.get(6));
    }

    /**
     * Test de la méthode
     * fr.gouv.finances.lombok.upload.dao.FichierJointDao#findFichierJointEtSonContenuParId(java.lang.Long).
     */
    @Test
    public void findFichierJointEtSonContenuParId()
    {
        // Paramètre absent
        VerificationExecution.verifierException(ProgrammationException.class
            , () -> fichierjointdao.findFichierJointEtSonContenuParId(null));

        // Recherche sur une donnée inexistante
        assertNull(fichierjointdao.findFichierJointEtSonContenuParId(100000L));

        // Recherche sur toutes les données de la base
        mapFichierJoint.values().stream().forEach(this::verifierFindFichierJointEtSonContenuParId);
    }

    /**
     * Méthode permettant d'exécuter et de vérifier la recherche du fichier joint et son contenu
     * à partir de son identifiant.
     *
     * @param fichierJoint fichier joint à traiter
     */
    private void verifierFindFichierJointEtSonContenuParId(FichierJoint fichierJoint)
    {
        verifierFichierJoint(fichierjointdao.findFichierJointEtSonContenuParId(fichierJoint.getId())
            , fichierJoint);
    }

    /**
     * Méthode permettant de vérifier un fichier joint et son contenu.
     *
     * @param fichierJoint fichier joint à vérifier
     * @param fichierJointAttendu fichier joint attendu
     */
    private void verifierFichierJoint(FichierJoint fichierJoint, FichierJoint fichierJoinAttendu)
    {
        // Contrôle des données du fichier joint
        assertEquals("Fichier joint " + fichierJoint.getNomFichierOriginal()
            , fichierJoint, fichierJoinAttendu);
        // Contrôle le contenu d'un fichier joint
        verifierContenu(fichierJoint);
    }

    /**
     * Méthode permettant vérifier le contenu d'un fichier joint.
     *
     * @param fichierJoint fichier joint à vérifier
     */
    private void verifierContenu(FichierJoint fichierJoint)
    {
        switch (fichierJoint.getNomFichierOriginal())
        {
            case "2" :
            case "5" :
                assertTrue("Chargement du contenu du fichier " + fichierJoint.getNomFichierOriginal()
                    , fichierjointdao.isCharged(fichierJoint.getLeContenuDuFichier()));
                assertNull("Contenu du fichier " + fichierJoint.getNomFichierOriginal()
                    , fichierJoint.getLeContenuDuFichier());
                break;
            default :
                assertTrue("Chargement du contenu du fichier " + fichierJoint.getNomFichierOriginal()
                    , fichierjointdao.isCharged(fichierJoint.getLeContenuDuFichier()));
                assertEquals("Contenu du fichier " + fichierJoint.getNomFichierOriginal()
                    , mapContenu.get(Integer.parseInt(fichierJoint.getNomFichierOriginal()))
                    , fichierJoint.getLeContenuDuFichier());
        }
    }

    /**
     * Méthode permettant vérifier des fichiers joints.
     *
     * @param fichiersJoints fichiers joints à vérifier
     * @param fichiersJointsAttendus fichiers joints attendus
     */
    private void verifierFichiersJoints(List<FichierJoint> fichiersJoints
        , FichierJoint... fichiersJointsAttendus)
    {
        // Contrôle des données du fichier joint
        ControleDonnees.verifierElements(fichiersJoints, fichiersJointsAttendus);
        // Contrôle de l'absence de chargement du contenu
        fichiersJoints.stream().forEach(this::verifierAbsenceContenu);
    }

    /**
     * Méthode permettant l'absence de chargement du contenu d'un fichier joint.
     *
     * @param fichierJoint fichier joint à vérifier
     */
    private void verifierAbsenceContenu(FichierJoint fichierJoint)
    {
        switch (fichierJoint.getNomFichierOriginal())
        {
            case "2" :
            case "5" :
                assertTrue("Chargement du contenu du fichier " + fichierJoint.getNomFichierOriginal()
                    , fichierjointdao.isCharged(fichierJoint.getLeContenuDuFichier()));
                assertNull("Contenu du fichier " + fichierJoint.getNomFichierOriginal()
                    , fichierJoint.getLeContenuDuFichier());
                break;
            default :
                assertFalse("Chargement du contenu du fichier " + fichierJoint.getNomFichierOriginal()
                    , fichierjointdao.isCharged(fichierJoint.getLeContenuDuFichier()));
        }
    }
    
    /**
     *  Méthode permettant de générer un fichier joint, dont le nom du fichier est l'indice fourni.<br/>
     * <br/>
     * Le contenu du fichier joint est fonction de l'indice fourni : <br/>
     * <code>
     *  fichier joint 1 : "1"<br/>
     *  fichier joint 2 : pas de contenu<br/>
     *  fichier joint 3 : "3"<br/>
     *  fichier joint 4 : "4"<br/>
     *  fichier joint 5 : pas de contenu<br/>
     *  fichier joint 6 : "6"<br/>
     *  etc.<br/>
     * </code>
     * <br/>
     *
     * @param indice indice du fichier joint
     * @return le fichier joint généré
     */
    protected FichierJoint getFichierJoint(int indice)
    {
        FichierJoint fichier = new FichierJoint((String.valueOf(indice)).getBytes());
        fichier.setNomFichierOriginal(String.valueOf(indice));
        fichier.setTypeMimeFichier(MimeTypeUtils.TEXT_PLAIN.getType());
        fichier.setDateHeureSoumission(TemporaliteUtils.getTimestamp(2018, 5, 16));
        fichier.setTailleFichier(indice);
        fichier.setLeContenuDuFichier(mapContenu.get(indice));

        return fichier;
    }

    /**
     * Méthode permettant de générer le contenu d'un fichier joint<br/>
     * Celui-ci est fonction de l'indice fourni : <br/>
     * <code>
     *  fichier joint 1 : "1"<br/>
     *  fichier joint 2 : pas de contenu<br/>
     *  fichier joint 3 : "3"<br/>
     *  fichier joint 4 : "4"<br/>
     *  fichier joint 5 : pas de contenu<br/>
     *  fichier joint 6 : "6"<br/>
     *  etc.<br/>
     * </code>
     * <br/>
     *
     * @param indice indice du contenu
     * @return le contenu du fichier joint généré
     */
    protected ContenuFichier getContenufichier(int indice)
    {
       if (indice != 2 && indice != 5)
       {
           ContenuFichier contenuFichier = new ContenuFichier();
           contenuFichier.setData((String.valueOf(indice)).getBytes());
           return contenuFichier;
       }
       return null;
    }
}