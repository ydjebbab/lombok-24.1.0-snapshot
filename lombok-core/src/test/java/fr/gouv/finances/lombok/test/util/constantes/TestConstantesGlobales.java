/*
 * Copyright (c) 2016 DGFiP - Tous droits réservés
 * 
 */
package fr.gouv.finances.lombok.test.util.constantes;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.net.URL;
import java.util.ResourceBundle;

import org.apache.commons.lang.StringUtils;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.gouv.finances.lombok.util.constantes.ConstantesGlobales;

/**
 * TestConstantesGlobales.java à revoir, des erreurs à cause du static dans ConstantesGlobales
 *
 * @author christian.houard Date: 12 oct. 2016
 */
public class TestConstantesGlobales
{
    private static final Logger log = LoggerFactory.getLogger(TestConstantesGlobales.class);

    /**
     * Test method for
     * {@link fr.gouv.finances.lombok.util.constantes.ConstantesGlobales#determineCheminCommunProperties()}.
     */

    @Rule
    public ExpectedException exploitThrown = ExpectedException.none();

    private static String appliconfroot;

    private static String applibatchroot;

    private static String appliwebroot;

    private static String applicommunroot;

    private static String confdirparam;

    @BeforeClass
    public static void setSystemProperties()
    {

        appliconfroot = System.getProperty("appli.conf.root");
        applibatchroot = System.getProperty("appli.batch.root");
        appliwebroot = System.getProperty("appli.web.root");
        applicommunroot = System.getProperty("appli.commun.root");
        confdirparam=System.getProperty(ConstantesGlobales.CONF_DIR_PARAM);

        System.setProperty("appli.conf.root", "");
        System.setProperty("appli.batch.root", "");
        System.setProperty("appli.web.root", "");
        System.setProperty("appli.commun.root", "");
        
       
    }

    @AfterClass
    public static void restoreSystemProperties()
    {
        if (appliconfroot == null)
        {
            System.clearProperty("appli.conf.root");
        }
        else
        {
            System.setProperty("appli.conf.root", appliconfroot);
        }
        if (applibatchroot == null)
        {
            System.clearProperty("appli.batch.root");
        }
        else
        {
            System.setProperty("appli.batch.root", applibatchroot);
        }
        if (appliwebroot == null)
        {
            System.clearProperty("appli.web.root");
        }
        else
        {
            System.setProperty("appli.web.root", appliwebroot);
        }
        if (applicommunroot == null)
        {
            System.clearProperty("appli.commun.root");
        }
        else
        {
            System.setProperty("appli.commun.root", applicommunroot);
        }
        
        if (confdirparam == null)
        {
            System.clearProperty(ConstantesGlobales.CONF_DIR_PARAM);
        }
        else
        {
            System.setProperty(ConstantesGlobales.CONF_DIR_PARAM, confdirparam);
        }

    }

    @Test
    public void testDetermineCheminCommunProperties()
    {
        System.setProperty(ConstantesGlobales.CONF_DIR_PARAM, "");
        // exploitThrown.expect(ExceptionInInitializerError.class);

        String chemin;
        chemin = ConstantesGlobales.determineCheminCommunProperties();
        log.info(chemin);
        assertFalse(StringUtils.isEmpty(chemin));
        assertTrue(chemin.endsWith("commun.properties"));
    }

    /**
     * Test method for
     * {@link fr.gouv.finances.lombok.util.constantes.ConstantesGlobales#determineRepertoireConfiguration()}.
     */

    @Test
    public void testDetermineRepertoireConfiguration()
    {
        System.setProperty(ConstantesGlobales.CONF_DIR_PARAM, "");
        String dirConf;
        dirConf = ConstantesGlobales.determineRepertoireConfiguration();
        log.info(dirConf);
        assertTrue(dirConf.endsWith("test-classes/"));

        System.setProperty(ConstantesGlobales.CONF_DIR_PARAM, "file:/monchemin");
        dirConf = ConstantesGlobales.determineRepertoireConfiguration();
        log.info(dirConf);
        assertTrue(dirConf.endsWith("monchemin"));

    }

    /**
     * Test method for
     * {@link fr.gouv.finances.lombok.util.constantes.ConstantesGlobales#determineURIexternalFormRepertoireConfiguration()}
     * .
     */

    @Test
    public void testDetermineURIexternalFormRepertoireConfiguration()
    {
        System.setProperty(ConstantesGlobales.CONF_DIR_PARAM, "file:/moncheminuri");
        String uri = ConstantesGlobales.determineURIexternalFormRepertoireConfiguration();
        log.info(uri);
        assertTrue(uri.endsWith("moncheminuri"));
    }

    /**
     * Test method for
     * {@link fr.gouv.finances.lombok.util.constantes.ConstantesGlobales#determineUrlRepertoireConfiguration()}.
     */

    @Test
    public void testDetermineUrlRepertoireConfiguration()
    {
        System.setProperty(ConstantesGlobales.CONF_DIR_PARAM, "file:/moncheminurl");
        URL urlRep = ConstantesGlobales.determineUrlRepertoireConfiguration();
        log.info(urlRep.toString());
        assertTrue(urlRep.toString().endsWith("moncheminurl"));
    }

    /**
     * Test method for {@link fr.gouv.finances.lombok.util.constantes.ConstantesGlobales#identifieLocalisation()}.
     */

    @Test
    public void testIdentifieLocalisation()
    {
        System.setProperty(ConstantesGlobales.CONF_DIR_PARAM, "");
        ResourceBundle localisationsBundle = ConstantesGlobales.identifieLocalisation();

        assertTrue(localisationsBundle.containsKey("appli.web.root"));
        assertTrue(localisationsBundle.containsKey("appli.batch.root"));
        assertTrue(localisationsBundle.containsKey("appli.data.root"));

    }

    /**
     * Test method for
     * {@link fr.gouv.finances.lombok.util.constantes.ConstantesGlobales#localiserParEnvironnementOuClasspath(java.lang.String)}
     * .
     */

    @Test
    public void testLocaliserParEnvironnementOuClasspath()
    {
        System.setProperty(ConstantesGlobales.CONF_DIR_PARAM, "");
        String appliProperties = ConstantesGlobales.localiserParEnvironnementOuClasspath("application.properties");
        log.info(appliProperties);
        assertTrue(appliProperties.endsWith("application.properties"));
    }

}
