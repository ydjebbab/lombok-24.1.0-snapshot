/*
 * Copyright (c) 2015 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.test.util.date;

import java.text.ParseException;
import java.util.Date;

import org.junit.Assert;

import org.junit.Test;

import fr.gouv.finances.lombok.util.date.JoursFeries;
import fr.gouv.finances.lombok.util.exception.ProgrammationException;
import fr.gouv.finances.lombok.util.format.FormaterDate;

/**
 * Class TestJoursFeriesMai.
 */
public class TestJoursFeriesMai
{

    /**
     * methode Parser date.
     * 
     * @param source chaine date
     * @return date
     */
    private Date parserDate(String source)
    {
        Date date = null;

        try
        {
            date = FormaterDate.getFormatDate(FormaterDate.F_DATE_CT).parse(source);
        }
        catch (ParseException exp)
        {
            throw new ProgrammationException("Date incorrecte", exp);
        }

        return date;
    }

    /**
     * methode Test du premier au 31 mai .
     */
    @Test
    public void testDuPremierAu31Mai()
    {
        Date dimanche1 = parserDate("01/05/2015");
        Date lundi = parserDate("31/05/2015");
        int result = JoursFeries.calculeNombreJoursOuvresEntreDeuxDates(dimanche1,
            lundi);
        Assert.assertEquals(17, result);
    }

    /**
     * methode Test s19 du premier mai au sept mai.
     */
    @Test
    public void testS19DuPremierMaiAuSeptMai()
    {
        Date dimanche1 = parserDate("01/05/2015");
        Date lundi = parserDate("07/05/2015");
        int result = JoursFeries.calculeNombreJoursOuvresEntreDeuxDates(dimanche1,
            lundi);
        Assert.assertEquals(4, result);
    }

    /**
     * methode Test s19 samedi premier et dernier jour .
     */
    @Test
    public void testS19SamediPremierEtDernierJour()
    {
        Date dimanche1 = parserDate("02/05/2015");
        Date lundi = parserDate("02/05/2015");
        int result = JoursFeries.calculeNombreJoursOuvresEntreDeuxDates(dimanche1,
            lundi);
        Assert.assertEquals(0, result);
    }

    /**
     * methode Test s19 samedi dimanche.
     */
    @Test
    public void testS19SamediDimanche()
    {
        Date dimanche1 = parserDate("02/05/2015");
        Date lundi = parserDate("03/05/2015");
        int result = JoursFeries.calculeNombreJoursOuvresEntreDeuxDates(dimanche1,
            lundi);
        Assert.assertEquals(0, result);
    }

    /**
     * methode Test s19 dimanche premier et dernier jour.
     */
    @Test
    public void testS19DimanchePremierEtDernierJour()
    {
        Date dimanche1 = parserDate("03/05/2015");
        Date lundi = parserDate("03/05/2015");
        int result = JoursFeries.calculeNombreJoursOuvresEntreDeuxDates(dimanche1,
            lundi);
        Assert.assertEquals(0, result);
    }

    /**
     * methode Test s19 dimanche premier jour periode1.
     */
    @Test
    public void testS19DimanchePremierJourPeriode1()
    {
        Date dimanche1 = parserDate("03/05/2015");
        Date lundi = parserDate("04/05/2015");
        int result = JoursFeries.calculeNombreJoursOuvresEntreDeuxDates(dimanche1,
            lundi);
        Assert.assertEquals(1, result);
    }

    /**
     * methode Test s19 dimanche premier jour periode2.
     */
    @Test
    public void testS19DimanchePremierJourPeriode2()
    {
        Date dimanche1 = parserDate("03/05/2015");
        Date mardi = parserDate("05/05/2015");
        int result = JoursFeries.calculeNombreJoursOuvresEntreDeuxDates(dimanche1,
            mardi);
        Assert.assertEquals(2, result);
    }

    /**
     * methode Test s19 dimanche premier jour periode3.
     */
    @Test
    public void testS19DimanchePremierJourPeriode3()
    {
        Date dimanche1 = parserDate("03/05/2015");
        Date mercredi = parserDate("06/05/2015");
        int result = JoursFeries.calculeNombreJoursOuvresEntreDeuxDates(dimanche1,
            mercredi);
        Assert.assertEquals(3, result);
    }

    /**
     * methode Test s19 dimanche premier jour periode4.
     */
    @Test
    public void testS19DimanchePremierJourPeriode4()
    {
        Date dimanche1 = parserDate("03/05/2015");
        Date jeudi = parserDate("07/05/2015");
        int result = JoursFeries.calculeNombreJoursOuvresEntreDeuxDates(dimanche1,
            jeudi);
        Assert.assertEquals(4, result);
    }

    /**
     * methode Test s19 dimanche premier jour periode5.
     */
    @Test
    public void testS19DimanchePremierJourPeriode5()
    {
        Date dimanche1 = parserDate("03/05/2015");
        Date vendredi = parserDate("08/05/2015");
        int result = JoursFeries.calculeNombreJoursOuvresEntreDeuxDates(dimanche1,
            vendredi);
        Assert.assertEquals(4, result);
    }

    /**
     * methode Test s19 dimanche premier jour samedi dernier jour.
     */
    @Test
    public void testS19DimanchePremierJourSamediDernierJour()
    {
        Date dimanche1 = parserDate("03/05/2015");
        Date samedi = parserDate("09/05/2015");
        int result = JoursFeries.calculeNombreJoursOuvresEntreDeuxDates(dimanche1,
            samedi);
        Assert.assertEquals(4, result);
    }

    /**
     * methode Test s19 dimanche premier jour dimanche dernier jour.
     */
    @Test
    public void testS19DimanchePremierJourDimancheDernierJour()
    {
        Date dimanche1 = parserDate("03/05/2015");
        Date samedi = parserDate("10/05/2015");
        int result = JoursFeries.calculeNombreJoursOuvresEntreDeuxDates(dimanche1,
            samedi);
        Assert.assertEquals(4, result);
    }

    /**
     * methode Test s21 lundi premier jour vendredi dernier jour.
     */
    @Test
    public void testS21LundiPremierJourVendrediDernierJour()
    {
        Date dimanche1 = parserDate("18/05/2015");
        Date samedi = parserDate("22/05/2015");
        int result = JoursFeries.calculeNombreJoursOuvresEntreDeuxDates(dimanche1,
            samedi);
        Assert.assertEquals(5, result);
    }

    /**
     * methode Test s21 lundi premier jour samedi dernier jour.
     */
    @Test
    public void testS21LundiPremierJourSamediDernierJour()
    {
        Date dimanche1 = parserDate("18/05/2015");
        Date samedi = parserDate("23/05/2015");
        int result = JoursFeries.calculeNombreJoursOuvresEntreDeuxDates(dimanche1,
            samedi);
        Assert.assertEquals(5, result);
    }

    /**
     * methode Test s2021 samedi dernier jour.
     */
    @Test
    public void testS2021SamediDernierJour()
    {
        Date dimanche1 = parserDate("11/05/2015");
        Date samedi = parserDate("23/05/2015");
        int result = JoursFeries.calculeNombreJoursOuvresEntreDeuxDates(dimanche1,
            samedi);
        Assert.assertEquals(9, result);
    }

}
