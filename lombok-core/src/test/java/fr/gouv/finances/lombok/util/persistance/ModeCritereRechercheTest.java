/*
 * Copyright (c) 2016 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.util.persistance;

import fr.gouv.finances.lombok.test.pojo.AbstractCorePojoTest;

/**
 * Tests unitaires de l'énumération ContenuFichier.
 * @see fr.gouv.finances.lombok.util.persistance.ModeCritereRecherche
 * 
 * @author Christophe Breheret-Girardin
 */
public class ModeCritereRechercheTest extends AbstractCorePojoTest<ModeCritereRecherche>
{
    /**
     * Constructeur.
     *
     */
    public ModeCritereRechercheTest()
    {
        super();
    }
}