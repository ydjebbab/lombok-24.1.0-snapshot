/*
 * Copyright (c) 2014 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.parsertexte.integration;

import fr.gouv.finances.lombok.test.pojo.AbstractCorePojoTest;

/**
 * Tests unitaires du POJO MemorisationLignesFichier
 *
 * @param MemorisationLignesFichier mémorisation de lignes de fichier
 *
 * @author Christophe Breheret-Girardin
 */
public class MemorisationLignesFichierTest extends AbstractCorePojoTest<MemorisationLignesFichier>
{

}
