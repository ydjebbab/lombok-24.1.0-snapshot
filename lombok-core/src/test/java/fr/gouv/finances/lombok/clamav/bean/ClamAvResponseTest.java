/*
 * Copyright (c) 2014 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.clamav.bean;

import fr.gouv.finances.lombok.test.pojo.AbstractCorePojoTest;

/**
 * Tests unitaires du POJO ClamAvResponse
 *
 * @param ClamAvResponse réponse de clamAV
 *
 * @author Christophe Breheret-Girardin
 */
public class ClamAvResponseTest extends AbstractCorePojoTest<ClamAvResponse>
{

}
