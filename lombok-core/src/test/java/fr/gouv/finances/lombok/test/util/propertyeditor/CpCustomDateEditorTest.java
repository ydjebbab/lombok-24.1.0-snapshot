/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : CpCustomDateEditorTest.java
 *
 */
package fr.gouv.finances.lombok.test.util.propertyeditor;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import fr.gouv.finances.lombok.util.propertyeditor.CpCustomDateEditor;
import junit.framework.TestCase;

/**
 * Class CpCustomDateEditorTest --.
 * 
 * @version $Revision: 1.3 $ Date: 11 déc. 2009
 */
public class CpCustomDateEditorTest extends TestCase
{

    /** log. */
    protected final Log log = LogFactory.getLog(getClass());

    /**
     * methode Test cree date : --.
     */
    public void testCreeDate()
    {
        Calendar cal = Calendar.getInstance();
        Integer millesime = Integer.valueOf("2007");
        Integer mois = Integer.valueOf("02");
        Integer jour = Integer.valueOf("15");

        // Pour l'objet calendar, les mois sont numérotés à partir de 0
        cal.set(millesime.intValue(), mois.intValue() - 1, jour.intValue());

        cal.getTime();
    }

    /**
     * methode Test dates invalides : --.
     */
    public void testDatesInvalides()
    {
        String[] invalidDatet = {"01/01 2006", "01a01a2006", "01.01/2006", "01.01 2006", "01/01.2006", "01/15/2006"};

        CpCustomDateEditor editor = new CpCustomDateEditor();

        // Teste les dates valides
        for (int i = 0; i < invalidDatet.length; i++)
        {
            String invalidDateType = invalidDatet[i];
            try
            {
                editor.setAsText(invalidDateType);
                fail("Une exception devrait être levée, cette date (" + invalidDateType + ") n'est valide");
            }
            catch (IllegalArgumentException ex)
            {
                log.info("date invalide : " + invalidDateType);
            }
        }
    }

    /**
     * methode Test dates valides : --.
     */
    public void testDatesValides()
    {
        SimpleDateFormat fmt = new SimpleDateFormat("dd/MM/yyyy");
        Date validDate = null;
        try
        {
            validDate = new Timestamp(fmt.parse("01/01/2006").getTime());
        }
        catch (ParseException e)
        {
            fail("Date valide");
        }

        String[] validDatet =
        {"01/01/2006", "01/01/06", "1/01/2006", "01/1/2006", "1/1/2006", "1/1/06", "1/1/6", "01 01 2006",
                "01 01 06", "1 01 2006", "01 1 2006", "1 1 2006", "1 1 06", "1 1 6", "01.01.2006", "01.01.06",
                "1.01.2006", "01.1.2006", "1.1.2006", "1.1.06", "1.1.6",

                "01//01/ 2006", "01/  01/  06", "1//01//2006", "01 /1 /2006", "1 / 1 /2006 ", " 1 / 1 / 06 ",
                " 1 / 1 / 6", "01  01  2006", "01  01  06", "1  01  2006", "01    1    2006", "1    1    2006",
                "1    1    06", "1  1  6", "01. 01 .2006", "01 .01 .06", "1 . 01 . 2006", "01 . 1. 2006",
                "1 .1. 2006", "1 .1 .06", "1. 1. 6",

                "01012006", " 01012006 ",

                "01/01/006", "01.01.006", "01 01 006", "1 1 006",

                "   "};

        CpCustomDateEditor editor = new CpCustomDateEditor();

        // Teste les dates valides
        for (int i = 0; i < validDatet.length; i++)
        {
            String validDateType = validDatet[i];
            try
            {
                editor.setAsText(validDateType);

                if (editor.getValue() != null)
                {
                    log.info("Texte date : " + validDateType + "; Date trouvée = " + fmt.format(editor.getValue()));
                    assertEquals(validDate, editor.getValue());
                }
                else
                {
                    log.info("Texte date : " + validDateType + "; Date trouvée = null ");
                }
            }
            catch (IllegalArgumentException ex)
            {
                fail("Aucune exception ne devrait être levée, cette date (" + validDateType + ") est valide");
            }
        }
    }

}
