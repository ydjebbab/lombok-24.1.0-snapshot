/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : CpFormatTest.java
 *
 */
package fr.gouv.finances.lombok.test.util.propertyeditor;

import java.text.ParseException;

import fr.gouv.finances.lombok.util.format.FormaterNombre;
import junit.framework.TestCase;

/**
 * Class CpFormatTest --.
 * 
 * @version $Revision: 1.2 $ Date: 11 déc. 2009
 */
public class CpFormatTest extends TestCase
{

    /**
     * methode Test format nombre : --.
     */
    public void testFormatNombre()
    {
        try
        {
            FormaterNombre.getFormatNombreZeroDecimale().parse("1a1a");
        }
        catch (ParseException e)
        {
            // Auto-generated catch block
            e.printStackTrace();
        }
    }

}
