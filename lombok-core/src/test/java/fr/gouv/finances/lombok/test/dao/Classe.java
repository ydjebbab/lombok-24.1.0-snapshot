package fr.gouv.finances.lombok.test.dao;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

/**
 * Entité représentant une classe d'élève
 *
 * @author Christophe Breheret-Girardin
 */
@Entity
class Classe implements Serializable
{
    /** Initialisation de l'UID */
    private static final long serialVersionUID = -5070361410878608212L;

    /** Identifiant de la classe */
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long identifiant;

    /** Libellé de la classe */
    @Basic
    private String libelle;

    /** Etablissement de la classe */
    @Basic
    private String etablissement;
    
    /** Elèves de la classe */
    @OneToMany(fetch=FetchType.LAZY, cascade={CascadeType.ALL})
    @JoinColumn(nullable=false)
    private Set<Eleve> eleves;

    /** 
     * {@inheritDoc}
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode()
    {
        return Objects.hash(identifiant, libelle, etablissement);
    }

    /** 
     * {@inheritDoc}
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj)
    {
        if (obj == this)
        {
            return true;
        }
        if (!(obj instanceof Classe)) {
            return false;
        }
        Classe classe = (Classe) obj;
        return Objects.equals(identifiant, classe.identifiant)
            && Objects.equals(libelle, classe.libelle)
            && Objects.equals(etablissement, classe.etablissement);
    }

    /** 
     * {@inheritDoc}
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        return libelle;
    }

    /**
     * Méthode permettant d'ajouter un élève à une classe.
     *
     * @param eleve élève à ajouter à la classe
     */
    public void addEleve(Eleve eleve) {
        if (eleve != null) {
           if (eleves == null) {
               eleves = new HashSet<>();             
           }
           eleves.add(eleve);
        }
     }
    
    /**
     * Accesseur de identifiant.
     *
     * @return identifiant
     */
    public Long getIdentifiant()
    {
        return identifiant;
    }

    /**
     * Mutateur de identifiant.
     *
     * @param identifiant identifiant
     */
    public void setIdentifiant(Long identifiant)
    {
        this.identifiant = identifiant;
    }

    /**
     * Accesseur de libelle.
     *
     * @return libelle
     */
    public String getLibelle()
    {
        return libelle;
    }

    /**
     * Mutateur de libelle.
     *
     * @param libelle libelle
     */
    public void setLibelle(String libelle)
    {
        this.libelle = libelle;
    }

    /**
     * Mutateur de eleves.
     *
     * @param eleves eleves
     */
    public void setEleves(Set<Eleve> eleves)
    {
        this.eleves = eleves;
    }

    /**
     * Accesseur de eleves.
     *
     * @return eleves
     */
    public Set<Eleve> getEleves()
    {
        return eleves;
    }

    /**
     * Accesseur de etablissement.
     *
     * @return etablissement
     */
    public String getEtablissement()
    {
        return etablissement;
    }

    /**
     * Mutateur de etablissement.
     *
     * @param etablissement etablissement
     */
    public void setEtablissement(String etablissement)
    {
        this.etablissement = etablissement;
    }

}
