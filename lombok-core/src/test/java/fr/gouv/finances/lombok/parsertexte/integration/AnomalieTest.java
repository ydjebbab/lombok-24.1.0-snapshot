/*
 * Copyright (c) 2014 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.parsertexte.integration;

import fr.gouv.finances.lombok.test.pojo.AbstractCorePojoTest;

/**
 * Tests unitaires du POJO Anomalie
 *
 * @param Anomalie anomalie
 *
 * @author Christophe Breheret-Girardin
 */
public class AnomalieTest extends AbstractCorePojoTest<Anomalie>
{

}
