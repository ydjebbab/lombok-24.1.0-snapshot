package fr.gouv.finances.lombok.test.securite;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import fr.gouv.finances.lombok.securite.service.AvantHabilitationService;
import fr.gouv.finances.lombok.securite.techbean.HabilitationRegleGestionException;
import fr.gouv.finances.lombok.securite.techbean.PersonneAnnuaire;

@Configuration
public class MockSpringSecurityTestProvider
{
    @Bean(name="avanthabilitationserviceso")
    public AvantHabilitationService myMock() {
        return new AvantHabilitationService()
        {
            
            @Override
            public void executerAvantHabilitation(PersonneAnnuaire unePersonneAnnuaire) throws HabilitationRegleGestionException
            {
               
            }
        };
    }
    
}
