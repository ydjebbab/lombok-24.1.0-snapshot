/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : EnumPropertyEditorTest.java
 *
 */
package fr.gouv.finances.lombok.test.util.propertyeditor;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import fr.gouv.finances.lombok.util.propertyeditor.EnumPropertyEditor;
import junit.framework.TestCase;

/**
 * Class EnumPropertyEditorTest --.
 * 
 * @version $Revision: 1.2 $ Date: 11 déc. 2009
 */
public class EnumPropertyEditorTest extends TestCase
{

    /**
     * Cette énumération définie la civilité d'un identité (elle remplace la classe et la table TRefCivilite).
     */
    public enum TRefCivilite
    {

        /** ARENSEIGNER. */
        ARENSEIGNER(0),

        /** MONSIEUR. */
        MONSIEUR(1),

        /** MADAME. */
        MADAME(2),

        /** MADEMOISELLE. */
        MADEMOISELLE(3);

        /**
         * Retourne le type de civilité à partir d'un entier le représentant.
         * 
         * @param value --
         * @return the t ref civilite
         */
        public static TRefCivilite fromInt(Integer value)
        {
            TRefCivilite result = null;
            if (value != null)
            {
                switch (value)
                {
                    case 0:
                        result = ARENSEIGNER;
                        break;
                    case 1:
                        result = MONSIEUR;
                        break;
                    case 2:
                        result = MADAME;
                        break;
                    case 3:
                        result = MADEMOISELLE;
                        break;
                    default:
                        result = ARENSEIGNER;
                        break;
                }
            }
            return result;
        }

        /** value. */
        private Integer value;

        /**
         * Instanciation de t ref civilite.
         * 
         * @param value --
         */
        TRefCivilite(Integer value)
        {
            this.value = value;
        }

        /**
         * methode To int : --.
         * 
         * @return integer
         */
        public Integer toInt()
        {
            return value;
        }

        /**
         * (methode de remplacement) {@inheritDoc}.
         * 
         * @return string
         * @see java.lang.Enum#toString()
         */
        public String toString()
        {
            String result;
            switch (this)
            {
                case ARENSEIGNER:
                    result = " ";
                    break;
                case MONSIEUR:
                    result = "M.";
                    break;
                case MADAME:
                    result = "Mme";
                    break;
                case MADEMOISELLE:
                    result = "Mlle";
                    break;
                default:
                    result = " ";
                    break;
            }
            return result;
        }
    }

    /** log. */
    protected final Log log = LogFactory.getLog(getClass());

    /**
     * pouvoir associer une chaîne arbitraire à une valeur nulle ici setValue est présent , dou renvoit la valeur de
     * getValue.
     */
    public void testGetAsTextValeurArbitraireTrueAvecValeursNonNulles()
    {

        EnumPropertyEditor<TRefCivilite> unEnumPropertyEditor = new EnumPropertyEditor<TRefCivilite>(TRefCivilite.class, "valeur a retourner");

        unEnumPropertyEditor.setValue("ARENSEIGNER");
        String unEnumPropertyEditorGetASTExtValeurARbitriare = unEnumPropertyEditor.getAsText();

        assertEquals(unEnumPropertyEditorGetASTExtValeurARbitriare, "ARENSEIGNER");
    }

    /**
     * pouvoir associer une chaîne arbitraire à une valeur nulle ici doit retourner valeur nulle.
     */
    public void testGetAsTextValeurArbitraireTrueAvecValeursNulles()
    {

        EnumPropertyEditor<TRefCivilite> unEnumPropertyEditor = new EnumPropertyEditor<TRefCivilite>(TRefCivilite.class, "valeur a retourner");

        unEnumPropertyEditor.setValue(null);
        String unEnumPropertyEditorGetASTExtValeurARbitriare = unEnumPropertyEditor.getAsText();

        assertEquals(unEnumPropertyEditorGetASTExtValeurARbitriare, "valeur a retourner");
    }

    /**
     * pouvoir associer une chaîne arbitraire à une valeur nulle test si la chaine dans le getASText est "" dans ce cas
     * se comporte comme getAsText().
     */
    public void testGetAsTextValeurArbitraireTrueAvecVALEURVide()
    {

        EnumPropertyEditor<TRefCivilite> unEnumPropertyEditor = new EnumPropertyEditor<TRefCivilite>(TRefCivilite.class, "");
        unEnumPropertyEditor.setValue(null);

        String unEnumPropertyEditorGetASTExtValeurARbitriare = unEnumPropertyEditor.getAsText();

        assertEquals(unEnumPropertyEditorGetASTExtValeurARbitriare, "");
    }

    /**
     * test du setASText CLASSIQUE 5AVEC SETAsTText avec boolean à false) le setASText présent dans enum , renvoit sa
     * valeur.
     */
    public void testSetAsTextBooleenAFalse()
    {
        EnumPropertyEditor<TRefCivilite> unEnumPropertyEditor = new EnumPropertyEditor<TRefCivilite>(TRefCivilite.class, false);
        unEnumPropertyEditor.setAsText("M.");
        assertEquals(unEnumPropertyEditor.getValue().toString(), "M.");

    }

    /**
     * test du setASText CLASSIQUE AVEC SETAsTText avec boolean à false) le setASText pas présent dans enum , renvoit
     * une exception.
     */
    public void testSetAsTextBooleenAFalseReturnStringVide()
    {
        EnumPropertyEditor<TRefCivilite> unEnumPropertyEditor = new EnumPropertyEditor<TRefCivilite>(TRefCivilite.class, false);
        try
        {

            unEnumPropertyEditor.setAsText("TEST");
        }
        // on aura une exception
        catch (IllegalArgumentException expected)
        {
            assertEquals(
                "Invalid text for enum of type 'fr.gouv.finances.lombok.test.util.propertyeditor.EnumPropertyEditorTest$TRefCivilite: 'TEST'."

                , expected.getMessage());

        }
    }

    /**
     * retourne une valeur nulle quand la chaîne de caractère qu'il transforme en énuméré ne correspond à une des
     * énumérations de la classe mappée. le setASText présent dans enum , renvoit le get value
     */
    public void testSetAsTextBooleenATrueREturnNoTNull()
    {
        EnumPropertyEditor<TRefCivilite> unEnumPropertyEditor = new EnumPropertyEditor<TRefCivilite>(TRefCivilite.class, true);
        unEnumPropertyEditor.setAsText("M.");
        assertEquals(unEnumPropertyEditor.getValue().toString(), "M.");

    }

    /**
     * retourne une valeur nulle quand la chaîne de caractère qu'il transforme en énuméré ne correspond à une des
     * énumérations de la classe mappée. le setASText pas présent dans enum , renvoit null
     */
    public void testSetAsTextBooleenATrueREturnNull()
    {
        EnumPropertyEditor<TRefCivilite> unEnumPropertyEditor = new EnumPropertyEditor<TRefCivilite>(TRefCivilite.class, true);
        unEnumPropertyEditor.setAsText("TEST");
        assertEquals(unEnumPropertyEditor.getValue(), null);

    }

}
