/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 */
package fr.gouv.finances.lombok.test.util.propertyeditor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.junit.Test;

import fr.gouv.finances.lombok.menu.service.MenuService;
import fr.gouv.finances.lombok.menu.service.impl.MenuServiceImpl;
import fr.gouv.finances.lombok.test.controle.VerificationExecution;
import fr.gouv.finances.lombok.util.propertyeditor.Assert;

/**
 * Tests unitaires de l'utilitaire fr.gouv.finances.lombok.test.util.propertyeditor.Assert.
 * @see fr.gouv.finances.lombok.test.util.propertyeditor.Assert
 *
 * @author Christophe Breheret-Girardin
 */
public class AssertTest
{
    /** Chaine à utiliser dans les tests. */
    private static final String CHAINE
        = "Choisissez un travail que vous aimez et vous n'aurez pas à travailler un seul jour de votre vie";

    /**
     * Test de fr.gouv.finances.lombok.test.util.propertyeditor.Assert#doesNotContain(
     * java.lang.String, java.lang.String).
     */
    @Test
    public void doesNotContain()
    {
        String sousChaine = StringUtils.substring(CHAINE, 5, 10);
        
        VerificationExecution.verifierException(IllegalArgumentException.class
            , () -> Assert.doesNotContain(null, null));
        VerificationExecution.verifierException(IllegalArgumentException.class
            , () -> Assert.doesNotContain("", ""));
        VerificationExecution.verifierException(IllegalArgumentException.class
            , () -> Assert.doesNotContain(sousChaine, ""));
        VerificationExecution.verifierException(IllegalArgumentException.class
            , () -> Assert.doesNotContain("", CHAINE));
        VerificationExecution.verifierException(IllegalArgumentException.class
            , () -> Assert.doesNotContain(sousChaine, CHAINE)); 
        Assert.doesNotContain("Kamoulox", CHAINE);
    }

    /**
     * Test de fr.gouv.finances.lombok.test.util.propertyeditor.Assert#hasLength(java.lang.String).
     */
    @Test
    public void hasLength()
    {
        VerificationExecution.verifierException(IllegalArgumentException.class
            , () -> Assert.hasLength(null));
        VerificationExecution.verifierException(IllegalArgumentException.class
            , () -> Assert.hasLength(""));
        Assert.hasLength(CHAINE); 
    }

    /**
     * Test de fr.gouv.finances.lombok.test.util.propertyeditor.Assert#hasText(java.lang.String).
     */
    @Test
    public void hasText()
    {
        VerificationExecution.verifierException(IllegalArgumentException.class
            , () -> Assert.hasText(null));
        VerificationExecution.verifierException(IllegalArgumentException.class
            , () -> Assert.hasText(""));
        VerificationExecution.verifierException(IllegalArgumentException.class
            , () -> Assert.hasText("  "));
        Assert.hasText(CHAINE); 
    }

    /**
     * Test de fr.gouv.finances.lombok.test.util.propertyeditor.Assert#isAssignable(java.lang.Class,
     * java.lang.Class).
     */
    @Test
    public void isAssignable()
    {
        VerificationExecution.verifierException(IllegalArgumentException.class
            , () -> Assert.isAssignable(null, null));
        VerificationExecution.verifierException(IllegalArgumentException.class
            , () -> Assert.isAssignable(AssertTest.class, null));
        VerificationExecution.verifierException(IllegalArgumentException.class
            , () -> Assert.isAssignable(null, AssertTest.class));
        Assert.isAssignable(AssertTest.class, AssertTest.class);
        Assert.isAssignable(MenuService.class, MenuServiceImpl.class);
    }

    /**
     * Test de fr.gouv.finances.lombok.test.util.propertyeditor.Assert#isInstanceOf(java.lang.Class,
     * java.lang.Object).
     */
    @Test
    public void isInstanceOf()
    {
        VerificationExecution.verifierException(IllegalArgumentException.class
            , () -> Assert.isInstanceOf(null, null));
        VerificationExecution.verifierException(IllegalArgumentException.class
            , () -> Assert.isInstanceOf(AssertTest.class, null));
        VerificationExecution.verifierException(IllegalArgumentException.class
            , () -> Assert.isInstanceOf(null, new AssertTest()));
        VerificationExecution.verifierException(IllegalArgumentException.class
            , () -> Assert.isInstanceOf(AssertTest.class, new MenuServiceImpl()));
        Assert.isInstanceOf(AssertTest.class, new AssertTest());
    }

    /**
     * Test de fr.gouv.finances.lombok.test.util.propertyeditor.Assert#isNull(java.lang.Object).
     */
    @Test
    public void isNull()
    {
        VerificationExecution.verifierException(IllegalArgumentException.class
            , () -> Assert.isNull(new AssertTest()));
        Assert.isNull(null);
    }

    /**
     * Test de fr.gouv.finances.lombok.test.util.propertyeditor.Assert#isTrue(boolean).
     */
    @Test
    public void isTrue()
    {
        VerificationExecution.verifierException(IllegalArgumentException.class
            , () -> Assert.isTrue(false));
        Assert.isTrue(true);
    }

    /**
     * Test de fr.gouv.finances.lombok.test.util.propertyeditor.Assert#notEmpty(java.util.Collection).
     */
    @Test
    public void notEmptyCollection()
    {
        VerificationExecution.verifierException(IllegalArgumentException.class
            , () -> Assert.notEmpty((Collection<String>) null));
        VerificationExecution.verifierException(IllegalArgumentException.class
            , () -> Assert.notEmpty(new ArrayList<String>()));
        Assert.notEmpty(Arrays.asList("1", "2"));
    }

    /**
     * Test de fr.gouv.finances.lombok.test.util.propertyeditor.Assert#notEmpty(java.util.Map).
     */
    @Test
    public void notEmptyMap()
    {
        VerificationExecution.verifierException(IllegalArgumentException.class
            , () -> Assert.notEmpty((Map<String, String>) null));
        VerificationExecution.verifierException(IllegalArgumentException.class
            , () -> Assert.notEmpty(new HashMap<String, String>()));
        Assert.notEmpty(Collections.singletonMap("clef", "valeur"));
    }

    /**
     * Test de fr.gouv.finances.lombok.test.util.propertyeditor.Assert#notEmpty(java.lang.Object[]).
     */
    @Test
    public void notEmptyArrays()
    {
        VerificationExecution.verifierException(IllegalArgumentException.class
            , () -> Assert.notEmpty((Object[]) null));
        VerificationExecution.verifierException(IllegalArgumentException.class
            , () -> Assert.notEmpty(new String[]{}));
        Assert.notEmpty(new String[]{"3", "4"});
    }

    /**
     * Test de fr.gouv.finances.lombok.test.util.propertyeditor.Assert#notNull(java.lang.Object).
     */
    @Test
    public void notNull()
    {
        VerificationExecution.verifierException(IllegalArgumentException.class
            , () -> Assert.notNull(null));
        Assert.notNull(new AssertTest());
    }

    /**
     * Test de fr.gouv.finances.lombok.test.util.propertyeditor.Assert#state(boolean).
     */
    @Test
    public void state()
    {
        VerificationExecution.verifierException(IllegalStateException.class
            , () -> Assert.state(false));
        Assert.state(true);
    }
}