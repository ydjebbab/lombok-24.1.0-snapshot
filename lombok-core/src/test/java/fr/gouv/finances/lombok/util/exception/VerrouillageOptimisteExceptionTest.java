/*
 * Copyright (c) 2014 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.util.exception;

import fr.gouv.finances.lombok.test.pojo.AbstractCorePojoTest;

/**
 * Tests unitaires de l'exception VerrouillageOptimisteException
 *
 * @author Christophe Breheret-Girardin
 */
public class VerrouillageOptimisteExceptionTest extends AbstractCorePojoTest<VerrouillageOptimisteException>
{

}
