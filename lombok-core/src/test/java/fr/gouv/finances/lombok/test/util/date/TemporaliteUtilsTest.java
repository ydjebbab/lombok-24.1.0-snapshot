/*
 * Copyright (c) 2015 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.test.util.date;

import static org.junit.Assert.assertEquals;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.StringJoiner;

import org.junit.Test;

import fr.gouv.finances.lombok.util.date.TemporaliteUtils;

/**
 * Tests unitaires automatisés permettant de tester l'utilitaire de gestion de la temporalité.
 *
 * @author Christophe Breheret-Girardin
 */
public class TemporaliteUtilsTest
{
    /**
     * Test de la méthode fr.gouv.finances.lombok.test.util.date.TemporaliteUtils#getTimestamp(int, int, int).
     */
    @Test
    public void getTimestamp()
    {
        // Initialisation du jeu d'essai
        int annee = 2018;
        int mois = 3;
        int jour = 5;

        // Exécution de la méthode à tester
        Timestamp timeStamp = TemporaliteUtils.getTimestamp(annee, mois, jour);

        // Vérifications
        LocalDateTime localDateTime = timeStamp.toLocalDateTime();
        assertEquals(annee, localDateTime.getYear());
        assertEquals(mois, localDateTime.getMonthValue());
        assertEquals(jour, localDateTime.getDayOfMonth());
    }

    /**
     * Test de la méthode fr.gouv.finances.lombok.test.util.date.TemporaliteUtils#getTimestamp().
     */
    @Test
    public void getToday()
    {
        // Exécution de la méthode à tester
        Timestamp timeStamp = TemporaliteUtils.getTimestamp();

        // Vérifications
        LocalDateTime localDateTime = LocalDateTime.now();
        assertEquals(timeStamp.toLocalDateTime().getYear(), localDateTime.getYear());
        assertEquals(timeStamp.toLocalDateTime().getMonthValue(), localDateTime.getMonthValue());
        assertEquals(timeStamp.toLocalDateTime().getDayOfMonth(), localDateTime.getDayOfMonth());
    }

    /**
     * Test de la méthode fr.gouv.finances.lombok.test.util.date.TemporaliteUtils#getDemain().
     */
    @Test
    public void getDemain()
    {
        // Exécution de la méthode à tester
        Timestamp timeStamp = TemporaliteUtils.getDemain();

        // Vérifications
        LocalDate localDate = LocalDate.now().plusDays(1);
        assertEquals(timeStamp.toLocalDateTime().getYear(), localDate.getYear());
        assertEquals(timeStamp.toLocalDateTime().getMonthValue(), localDate.getMonthValue());
        assertEquals(timeStamp.toLocalDateTime().getDayOfMonth(), localDate.getDayOfMonth());
    }

    /**
     * Test de la méthode
     * fr.gouv.finances.lombok.test.util.date.TemporaliteUtils#getDate(java.lang.String, java.lang.String).
     */
    @Test
    public void getStringDate()
    {
        // Initialisation du jeu d'essai
        String annee = "2018";
        String mois = "04";
        String jour = "10";

        // Exécution de la méthode à tester
        Date date = TemporaliteUtils.getDate(
            new StringJoiner("/").add(jour).add(mois).add(annee).toString(), "dd/MM/yyyy");

        // Vérifications
        LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        assertEquals(localDate.getYear(), Integer.parseInt(annee));
        assertEquals(localDate.getMonthValue(), Integer.parseInt(mois));
        assertEquals(localDate.getDayOfMonth(), Integer.parseInt(jour));
    }

    /**
     * Test de la méthode fr.gouv.finances.lombok.test.util.date.TemporaliteUtils#getDate(int, int, int).
     */
    @Test
    public void getDate()
    {
        // Initialisation du jeu d'essai
        int annee = 2018;
        int mois = 04;
        int jour = 10;

        // Exécution de la méthode à tester
        Date date = TemporaliteUtils.getDate(annee, mois, jour);

        // Vérifications
        LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        assertEquals(localDate.getYear(), annee);
        assertEquals(localDate.getMonthValue(), mois);
        assertEquals(localDate.getDayOfMonth(), jour);
    }

    /**
     * Test de la méthode
     * fr.gouv.finances.lombok.test.util.date.TemporaliteUtils#getDate(int, int, int, int, int, int).
     */
    @Test
    public void getDateHeure()
    {
        // Initialisation du jeu d'essai
        int annee = 2018;
        int mois = 04;
        int jour = 10;
        int heure = 15;
        int minute = 39;
        int seconde = 53;

        // Exécution de la méthode à tester
        Date date = TemporaliteUtils.getDate(annee, mois, jour, heure, minute, seconde);

        // Vérifications
        LocalDateTime localDateTime = LocalDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault());
        assertEquals(localDateTime.getYear(), annee);
        assertEquals(localDateTime.getMonthValue(), mois);
        assertEquals(localDateTime.getDayOfMonth(), jour);
        assertEquals(localDateTime.getHour(), heure);
        assertEquals(localDateTime.getMinute(), minute);
        assertEquals(localDateTime.getSecond(), seconde);
    }
}