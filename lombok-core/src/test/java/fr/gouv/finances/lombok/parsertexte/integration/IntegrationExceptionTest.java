/*
 * Copyright (c) 2014 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.parsertexte.integration;

import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.gouv.finances.lombok.test.pojo.AbstractCorePojoTest;

/**
 * Tests unitaire de l'exception liée à l'intégration de fichier.
 *
 * @author Christophe Breheret-Girardin
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class IntegrationExceptionTest extends AbstractCorePojoTest<IntegrationException>
{
    
}
