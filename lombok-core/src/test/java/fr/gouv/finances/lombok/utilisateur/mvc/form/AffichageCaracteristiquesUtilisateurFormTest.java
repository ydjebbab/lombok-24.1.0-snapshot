/*
 * Copyright (c) 2016 DGFiP - Tous droits réservés
 * 
 */
package fr.gouv.finances.lombok.utilisateur.mvc.form;

import fr.gouv.finances.lombok.test.pojo.AbstractCorePojoTest;

/**
 * Tests unitaires du POJO AffichageCaracteristiquesUtilisateurForm.
 *
 * @param  AffichageCaracteristiquesUtilisateurForm formulaire lié à l'affichage des caractéristiques de l'utilisateur
 *
 * @author Christophe Breheret-Girardin
 */
public class AffichageCaracteristiquesUtilisateurFormTest
    extends AbstractCorePojoTest< AffichageCaracteristiquesUtilisateurForm>
{   
    /**
     * Constructeur.
     */
    public AffichageCaracteristiquesUtilisateurFormTest()
    {
        super();
    }

}
