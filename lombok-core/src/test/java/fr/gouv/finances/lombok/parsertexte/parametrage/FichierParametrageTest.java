/*
 * Copyright (c) 2014 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.parsertexte.parametrage;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

import fr.gouv.finances.lombok.test.pojo.AbstractCorePojoTest;

/**
 * Tests unitaires du POJO FichierParametrage.
 *
 * @author Christophe Breheret-Girardin
 */
public class FichierParametrageTest extends AbstractCorePojoTest<FichierParametrage>
{

    /**
     * Constructeur.
     */
    public FichierParametrageTest()
    {
        super();

        // ajout une valeur préfabriquée pour l'instanciation d'une Map
        addPrefabValues(Map.class, new HashMap<String, AttributClasse>());

        // Ignorer les accesseurs qui renvoient une valeur différente de leur attribut
        ignorerAccesseurs("getNbLignesEntete");
    }

    /**
     * Méthode permettant de tester l'accesseur "getNbLignesEntete".
     */
    @Test
    public void testGetNbLignesEntete()
    {
        // Cas non renseigné
        FichierParametrage fichierParametrage = new FichierParametrage();
        fichierParametrage.setNbLignesEntete(null);
        assertEquals("getNbLignesEntete", new Integer(0), fichierParametrage.getNbLignesEntete());

        // Cas renseigné
        fichierParametrage = new FichierParametrage();
        fichierParametrage.setNbLignesEntete(1);
        assertEquals("getNbLignesEntete", new Integer(1), fichierParametrage.getNbLignesEntete());
    }
}