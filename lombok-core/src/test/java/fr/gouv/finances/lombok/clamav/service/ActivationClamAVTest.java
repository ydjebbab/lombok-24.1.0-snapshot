/*
 * Copyright (c) 2014 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.clamav.service;

import fr.gouv.finances.lombok.test.pojo.AbstractCorePojoTest;

/**
 * Tests unitaires du POJO ActivationClamAV.
 *
 * @param Valeurs possibles pour l'activation ou non des vérifications de virus
 *
 * @author Christophe Breheret-Girardin
 */
public class ActivationClamAVTest extends AbstractCorePojoTest<ActivationClamAV>
{

}
