package fr.gouv.finances.lombok.test.dao;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import fr.gouv.finances.lombok.test.dao.Eleve;

/**
 * Entité représentant un élève
 *
 * @author Christophe Breheret-Girardin
 */
@Entity
class Eleve implements Serializable
{
    /** Initialisation de l'UID */
    private static final long serialVersionUID = -5116848206241225013L;

    /** Identifiant de l'élève */
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long identifiant;

    /** Nom de l'élève */
    @Basic
    private String nom;

    /** Prénom de l'élève */
    @Basic
    private String prenom;

    @Override
    public int hashCode()
    {
        return Objects.hash(identifiant, nom, prenom);
    }

    @Override
    public boolean equals(Object obj)
    {
        if (obj == this)
        {
            return true;
        }
        if (!(obj instanceof Eleve)) {
            return false;
        }
        Eleve eleve = (Eleve) obj;
        return Objects.equals(identifiant, eleve.identifiant)
            && Objects.equals(nom, eleve.nom)
            && Objects.equals(prenom, eleve.prenom);
    }

    /** 
     * {@inheritDoc}
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        return nom;
    }
    
    /**
     * Accesseur de identifiant
     *
     * @return identifiant
     */
    public Long getIdentifiant()
    {
        return identifiant;
    }

    /**
     * Mutateur de identifiant
     *
     * @param identifiant identifiant
     */
    public void setIdentifiant(Long identifiant)
    {
        this.identifiant = identifiant;
    }

    /**
     * Accesseur de nom
     *
     * @return nom
     */
    public String getNom()
    {
        return nom;
    }

    /**
     * Mutateur de nom
     *
     * @param nom nom
     */
    public void setNom(String nom)
    {
        this.nom = nom;
    }

    /**
     * Accesseur de prenom
     *
     * @return prenom
     */
    public String getPrenom()
    {
        return prenom;
    }

    /**
     * Mutateur de prenom
     *
     * @param prenom prenom
     */
    public void setPrenom(String prenom)
    {
        this.prenom = prenom;
    }

}
