/*
 * Copyright (c) 2014 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.test.antivirus;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.gouv.finances.lombok.clamav.bean.ClamAvResponse;
import fr.gouv.finances.lombok.clamav.bean.ClamAvResponse.CategorieReponse;
import fr.gouv.finances.lombok.clamav.bean.ClamAvResponse.EtatReponse;
import fr.gouv.finances.lombok.clamav.exception.ClamAVException;
import fr.gouv.finances.lombok.clamav.service.ActivationClamAV;
import fr.gouv.finances.lombok.clamav.service.ClamAVCheckService;
import fr.gouv.finances.lombok.upload.bean.FichierJoint;
import fr.gouv.finances.lombok.util.exception.ExploitationException;

/**
 * Deux profils pour ce test : ("clamav" ou "clamav-simulation") et  "clamav-simulation-offline" :<br/>
 * - profil "clamav-simulation" pour utiliser ce test avec un simulateur interne autonome<br/>
 * - profil "clamav" pour utiliser ce test avec un véritable serveur clamAV (à installer au préalable)<br/>
 * - profil "clamav-simulation-offline" utilisé pour le test sur un serveur inaccessible.
 *
 * @author Christophe Breheret-Girardin
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
        "classpath:/conf/testpropertysource.xml",
        "classpath:/conf/applicationContext-resources.xml",
        "classpath:/conf/applicationContext-clamav-service.xml",
        "classpath:/conf/applicationContext-clamav-simulation-service.xml",
        "classpath:/conf/applicationContext-clamav-simulation-offline-service.xml"})
@ActiveProfiles(profiles = {"clamav-simulation", "clamav-simulation-offline"})
public class ClamavTest
{
    /** Initialisation de la journalisation. */
    protected static final Logger LOGGER = LoggerFactory.getLogger(ClamavTest.class);

    /** Libellé remonté par le service si aucun virus n'a été détecté. */
    private static final String LIBELLE_PAS_VIRUS = "Pas de virus trouvé";

    /** Service d'interfaçage avec un serveur ClamAV */
    @Autowired
    @Qualifier("clamServiceCheck")
    public ClamAVCheckService clamAVCheckService;

    /** Service d'interfaçage avec un serveur ClamAV non démarré */
    @Autowired
    @Qualifier("clamServiceOffLine")
    public ClamAVCheckService clamServiceOffLine;
    
    /** Répertoire contenant les fichiers de test */
    @Value("${antivirus.test.ressources}")
    private String repertoireFichier;

    @Test
    public void testClamAVOffLine() throws InterruptedException, IOException
    {
        LOGGER.info("Début du test d'un serveur clamAV non accessible");

        // Appel du serveur clamAV
        ClamAvResponse clamAvResponse = analyserFichier(clamServiceOffLine, "test-OK.txt");

        // Contrôle des informations du service lorsqu'un serveur ne répond pas
        Assert.assertNotNull("ClamAvResponse null", clamAvResponse);
        Assert.assertNull("ClamAvResponse.Response non null", clamAvResponse.getResponse());
        Assert.assertEquals("ClamAvResponse.CategorieReponse", CategorieReponse.NO_SERVICE
            , clamAvResponse.getCategorieReponse());
        Assert.assertNull("ClamAvResponse.Etat non null", clamAvResponse.getEtat());
        Assert.assertNull("ClamAvResponse.Stats non null", clamAvResponse.getStats());
        Assert.assertNull("ClamAvResponse.Version non null", clamAvResponse.getVersion());
        Assert.assertNull("ClamAvResponse.Virusname non null", clamAvResponse.getVirusName());

        LOGGER.info("Fin du test d'un serveur clamAV non accessible");
    }

    @Test
    public void testClamPingOK() throws InterruptedException, IOException
    {
        LOGGER.info("Début du test d'un ping de clamAV");

        // Appel du serveur clamAV
        ClamAvResponse clamAvResponse = clamAVCheckService.checkClamAVPing();

        // Contrôle des informations renvoyées par le service
        Assert.assertNotNull("ClamAvResponse null", clamAvResponse);
        Assert.assertEquals("ClamAvResponse.Response", "PONG\n", clamAvResponse.getResponse());
        Assert.assertEquals("ClamAvResponse.CategorieReponse", CategorieReponse.PING_OK
            , clamAvResponse.getCategorieReponse());
        Assert.assertNull("ClamAvResponse.Etat non null", clamAvResponse.getEtat());
        Assert.assertNull("ClamAvResponse.Stats non null", clamAvResponse.getStats());
        Assert.assertNull("ClamAvResponse.Version non null", clamAvResponse.getVersion());
        Assert.assertNull("ClamAvResponse.Virusname non null", clamAvResponse.getVirusName());
        
        LOGGER.info("Fin du test d'un fichier sans virus");
    }

    @Test
    public void testClamAVFichierOK() throws InterruptedException, IOException
    {
        LOGGER.info("Début du test d'un fichier sans virus");

        // Envoi du fichier au serveur clamAV
        ClamAvResponse clamAvResponse = analyserFichier(clamAVCheckService, "test-OK.txt");
        // Contrôle des informations renvoyées par le service
        controlerReponseFichierOk(clamAvResponse);
                
        LOGGER.info("Fin du test d'un fichier sans virus");
    }
    
    //@Test
    public void testClamAVFichierJointOK() throws InterruptedException, IOException
    {
        LOGGER.info("Début du test d'un fichier joint sans virus");

        // Envoi du fichier au serveur clamAV
        ClamAvResponse clamAvResponse = analyserFichierJoint(clamAVCheckService, "test-OK.txt");
        // Contrôle des informations renvoyées par le service
        controlerReponseFichierOk(clamAvResponse);

        LOGGER.info("Fin du test d'un fichier joint sans virus");
    }

    @Test
    public void testClamAVFluxVirus() throws InterruptedException, IOException
    {
        LOGGER.info("Début du test d'un flux avec virus");

        String virusEICAR = "X5O!P%@AP[4\\PZX54(P^)7CC)7}$EICAR-STANDARD-ANTIVIRUS-TEST-FILE!$H+H*";

        // Envoi du flux au serveur clamAV
        ClamAvResponse clamAvResponse = analyserFlux(clamAVCheckService, virusEICAR.getBytes());
        // Contrôle des informations renvoyées par le service
        Assert.assertNotNull("ClamAvResponse null", clamAvResponse);
        Assert.assertEquals("ClamAvResponse.Response", "1: stream: Eicar-Test-Signature FOUND\n", clamAvResponse.getResponse());
        Assert.assertEquals("ClamAvResponse.CategorieReponse", CategorieReponse.VIRUS
            , clamAvResponse.getCategorieReponse());
        Assert.assertEquals("ClamAvResponse.Etat", EtatReponse.KO, clamAvResponse.getEtat());
        Assert.assertNull("ClamAvResponse.Stats non null", clamAvResponse.getStats());
        Assert.assertNull("ClamAvResponse.Version non null", clamAvResponse.getVersion());
        Assert.assertEquals("ClamAvResponse.Virusname", "Eicar-Test-Signature", clamAvResponse.getVirusName());

        LOGGER.info("Fin du test d'un flux avec virus");
    }
    
    @Test
    public void testClamAVInactif() throws InterruptedException, IOException
    {
        LOGGER.info("Début du test d'un flux avec virus, mais avec un paramètre de non contrôle");

        // Initialisation d'un virus de test : EICAR Standard Anti-Virus Test File
        // (source : http://www.eicar.org/86-0-Intended-use.html)
        String virusEICAR = "X5O!P%@AP[4\\PZX54(P^)7CC)7}$EICAR-STANDARD-ANTIVIRUS-TEST-FILE!$H+H*";

        // Sauvegarde de l'ancienne valeur
        ActivationClamAV activite = clamAVCheckService.getActivationClamAv();
        try
        {
            // Paramètrage du service comme inactif (= aucun contrôle du flux)
            clamAVCheckService.setActivationClamAv(ActivationClamAV.INACTIF);
            
            ClamAvResponse clamAvResponse = analyserFlux(clamAVCheckService, virusEICAR.getBytes());
            // Contrôle des informations renvoyées par le service
            controlerReponseFichierOk(clamAvResponse);
        }
        finally
        {
            // Réactivation du service
            clamAVCheckService.setActivationClamAv(activite);
        }
        
        LOGGER.info("Fin du test d'un flux avec virus, mais avec un paramètre de non contrôle");
    }

    @Test
    public void testClamAVFichierVide() throws InterruptedException, IOException
    {
        LOGGER.info("Début du test d'un fichier vide");

        ClamAvResponse clamAvResponse = analyserFichier(clamAVCheckService, "test-VIDE.txt");

        // Contrôle des informations du service pour un fichier vide
        Assert.assertNotNull("ClamAvResponse null", clamAvResponse);
        Assert.assertNull("ClamAvResponse.Response non null", clamAvResponse.getResponse());
        Assert.assertEquals("ClamAvResponse.CategorieReponse", CategorieReponse.FICHIER_VIDE
            , clamAvResponse.getCategorieReponse());
        Assert.assertNull("ClamAvResponse.Etat non null", clamAvResponse.getEtat());
        Assert.assertNull("ClamAvResponse.Stats non null", clamAvResponse.getStats());
        Assert.assertNull("ClamAvResponse.Version non null", clamAvResponse.getVersion());
        Assert.assertNull("ClamAvResponse.Virusname non null", clamAvResponse.getVirusName());

        LOGGER.info("Fin du test d'un fichier vide");
    }
    
    @Test
    public void testClamAVFichierJointVide() throws InterruptedException, IOException
    {
        LOGGER.info("Début du test d'un fichier vide");

        ClamAvResponse clamAvResponse = analyserFichierJoint(clamAVCheckService, "test-VIDE.txt");

        // Contrôle des informations du service pour un fichier vide
        Assert.assertNotNull("ClamAvResponse null", clamAvResponse);
        Assert.assertNull("ClamAvResponse.Response non null", clamAvResponse.getResponse());
        Assert.assertEquals("ClamAvResponse.CategorieReponse", CategorieReponse.FICHIER_VIDE
            , clamAvResponse.getCategorieReponse());
        Assert.assertNull("ClamAvResponse.Etat non null", clamAvResponse.getEtat());
        Assert.assertNull("ClamAvResponse.Stats non null", clamAvResponse.getStats());
        Assert.assertNull("ClamAvResponse.Version non null", clamAvResponse.getVersion());
        Assert.assertNull("ClamAvResponse.Virusname non null", clamAvResponse.getVirusName());

        LOGGER.info("Fin du test d'un fichier vide");
    }

    /**
     * Contrôle des informations du service pour un fichier sans virus
     *
     * @param clamAvResponse Réponse clamAVA
     */
    private void controlerReponseFichierOk(ClamAvResponse clamAvResponse)
    {
        Assert.assertNotNull("ClamAvResponse null", clamAvResponse);
        Assert.assertEquals("ClamAvResponse.Response", "1: stream: OK\n", clamAvResponse.getResponse());
        Assert.assertEquals("ClamAvResponse.CategorieReponse", CategorieReponse.NO_VIRUS
            , clamAvResponse.getCategorieReponse());
        Assert.assertEquals("ClamAvResponse.Etat", EtatReponse.OK, clamAvResponse.getEtat());
        Assert.assertNull("ClamAvResponse.Stats non null", clamAvResponse.getStats());
        Assert.assertNull("ClamAvResponse.Version non null", clamAvResponse.getVersion());
        Assert.assertEquals("ClamAvResponse.Virusname", LIBELLE_PAS_VIRUS, clamAvResponse.getVirusName());

    }
    
    private ClamAvResponse analyserFichier(final ClamAVCheckService clamAvService
        , final String fichier) throws InterruptedException, IOException
    {
        return clamAvService.checkByStream(getFichier(fichier));
    }
    
    private ClamAvResponse analyserFichierJoint(final ClamAVCheckService clamAvService
        , final String fichier) throws InterruptedException, IOException
    {
        FichierJoint fichierJoint = new FichierJoint(FileUtils.readFileToByteArray(getFichier(fichier)));
        //fichierJoint.setNomFichierOriginal(fichier);
        return clamAvService.checkByStream(fichierJoint);
    }

    private ClamAvResponse analyserFlux(final ClamAVCheckService clamAvService
        , final byte[] contenu)
        throws IOException, InterruptedException
    {
        ClamAvResponse ret = null;

        try
        {
            ret = clamAvService.checkByStream(contenu);
        }
        catch (final ClamAVException e)
        {
            LOGGER.error(e.getMessage());
        }

        if (ret != null)
        {
            LOGGER.debug("Réponse Traitement fichier : " + ret.getCategorieReponse());
        }

        return ret;
    }
    
    private File getFichier(String fichier)
    {
        LOGGER.info("Répertoire contenant les fichiers de test clamAV : {}", repertoireFichier);
        LOGGER.info("Fichier à charger : {}", fichier);

        // Construction de l'URL correspondant au chemin du fichier
        String urlCheminFichier = "";
        String SLASH = "/";

        // Une URL doit posséder des caractères "slash" et non "antislash", modification dans le cas contraire
        repertoireFichier = StringUtils.replace(repertoireFichier, "\\", SLASH);

        // L'URL doit débuter par le caractère "slash", ajout dans le cas contraire
        if (!repertoireFichier.startsWith(SLASH))
        {
            urlCheminFichier += SLASH;
        }
        urlCheminFichier += repertoireFichier;

        // Il doit y avoir un caractère "slash" entre le répertoire et le fichier, ajout dans le cas contraire
        if (!repertoireFichier.endsWith(SLASH) || !fichier.startsWith(SLASH))
        {
            urlCheminFichier += SLASH;
        }
        urlCheminFichier += fichier;

        LOGGER.info("URL du chemin complet du fichier à charger : {}", urlCheminFichier);

        URL urlFichier = this.getClass().getResource(urlCheminFichier);
        if (urlFichier == null)
        {
            Assert.fail("Le fichier n'a pas été trouvé à l'URL : " + urlCheminFichier);
        }

        try
        {
            // Récupération du fichier
            return new File(urlFichier.toURI());
        }
        catch (URISyntaxException use)
        {
            throw new ExploitationException("Le fichier n'a pas été trouvé à l'URI : " + urlFichier, use);
        }
    }
}
