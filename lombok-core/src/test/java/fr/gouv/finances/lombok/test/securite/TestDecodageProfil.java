/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : TestDecodageProfil.java
 *
 */
package fr.gouv.finances.lombok.test.securite;

import junit.framework.TestCase;
import fr.gouv.finances.lombok.securite.service.impl.ProfilXMLParser;
import fr.gouv.finances.lombok.securite.techbean.ProfilAnnuaire;

/**
 * Class TestDecodageProfil --.
 * 
 * @version $Revision: 1.2 $ Date: 11 déc. 2009
 */
public class TestDecodageProfil extends TestCase
{

    /**
     * methode Testdeocedeprofilspas xml : --.
     */
    public final void testdeocedeprofilspasXML()
    {

        String unprofil =
            "GC;Gestionnaire Central;Affectation de la reserve nationale modification de la repartition du kmois entr les departements;0;";
        ProfilAnnuaire unProfilAnnuaire = ProfilXMLParser.profilDecodeAvecSeparateur(unprofil);
        assertEquals(unProfilAnnuaire.getLibelleProfil(), "Gestionnaire Central");
    }

    /**
     * methode Testdeocedeprofilspas xml sans commentaire : --.
     */
    public final void testdeocedeprofilspasXMLSansCommentaire()
    {

        String unprofil = "GC;Gestionnaire Central;;1;";
        ProfilAnnuaire unProfilAnnuaire = ProfilXMLParser.profilDecodeAvecSeparateur(unprofil);

        assertEquals(unProfilAnnuaire.getASupprimer(), "1");
    }

}
