/*
 * Copyright (c) 2014 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.test.securite;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.junit.Assert;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.gouv.finances.lombok.securite.service.LectureApplicationService;
import fr.gouv.finances.lombok.securite.service.LecturePersonneService;
import fr.gouv.finances.lombok.securite.service.impl.LectureApplicationServiceImpl;
import fr.gouv.finances.lombok.securite.service.impl.LecturePersonneServiceImpl;
import fr.gouv.finances.lombok.securite.service.impl.RessourceAnnuaireServiceImpl;
import fr.gouv.finances.lombok.securite.techbean.AnnuaireException;
import fr.gouv.finances.lombok.securite.techbean.ApplicationAnnuaire;
import fr.gouv.finances.lombok.securite.techbean.HabilitationAnnuaire;
import fr.gouv.finances.lombok.securite.techbean.PersonneAnnuaire;

/**
 * Tests liés aux accès aux annuaires.
 *
 * @author amleplatinec
 * @author Christophe Breheret-Girardin
 */
public class LectureAnnuaireServicesTest
{
    /*************************/
    /** Type d'annuaire LDAP */
    /*************************/
    public static enum TypeAnnuaire
    {
        DGCP, DGI, DGFIP, USAGERS;
    }

    /**************************/
    /** Configuration du test */
    /**************************/
    /** Recherche dans l'annuaire DGCP ? */
    private static final boolean TEST_ANNUAIRE_DGCP = true;
    /** Recherche dans l'annuaire DGI ? */
    private static final boolean TEST_ANNUAIRE_DGI = true;
    /** Recherche dans l'annuaire fusionné ? */
    private static final boolean TEST_ANNUAIRE_DGFIP = true;
    /** Recherche dans l'annuaire des usagers ? */
    private static final boolean TEST_ANNUAIRE_USAGERS = true;
    /** Constante permettant d'activer, ou non, les critères non indexés d'un annuaire,
     * car cela donne des temps de réponse trop long (conseil : activer en DEV, désactiver sur ICE) */
    private static final boolean ACTIVER_APPEL_ANNUAIRE_SUR_CRITERES_NON_INDEXES = false;

    /********************************/
    /** Adresses des annuaires LDAP */
    /********************************/

    /** Constante donnant l'adresse du serveur LDAP de l'annuaire ex-DGCP. */
    private static final String LDAP_DGCP = "ldap://10.69.247.30:389";

    /** Constante donnant l'adresse du serveur LDAP de l'annuaire ex-DGI. */
    private static final String LDAP_DGI = "ldap://10.154.96.33";

    /** Constante donnant l'adresse du serveur LDAP de l'annuaire fusionné (DGFIP). */
    private static final String LDAP_ANNUAIRE_FUSIONNE = "ldap://10.153.86.26:389";

    /** Constante donnant l'adresse du serveur LDAP des usagers. */
    private static final String LDAP_ANNUAIRE_USAGERS = "ldap://10.92.252.42:393";

    /** Adresse du serveur LDAP permettant de tester la bascule SIHRIUS */
    private static final String LDAP_SIHRIUS = "ldap://10.153.86.21:389";

    /****************************************************/
    /** Contexte de la recherche au seins des annuaires */
    /****************************************************/

    /** Constante donnant le contexte de la recherche d'applications dans l'annuaire ex-DGCP. */
    private static final String BASE_RECHERCHE_APPLICATION_DGCP = "ou=applications,ou=DGCP,ou=MEFI,o=gouv,c=fr";

    /** Constante donnant le contexte de la recherche de personnes dans l'annuaire ex-DGCP. */
    private static final String BASE_RECHERCHE_PERSONNE_DGCP = "ou=personnes,ou=DGCP,ou=MEFI,o=gouv,c=fr";

    /** Constante donnant le contexte de la recherche d'applications dans l'annuaire ex-DGI. */
    private static final String BASE_RECHERCHE_APPLICATION_DGI = "ou=applications,ou=dgi,ou=mefi,o=gouv,c=fr";

    /** Constante donnant le contexte de la recherche de personnse dans l'annuaire ex-DGI. */
    private static final String BASE_RECHERCHE_PERSONNE_DGI = "ou=personnes,ou=dgi,ou=mefi,o=gouv,c=fr";

    /** Constante donnant le contexte de la recherche d'applications dans l'annuaire fusionné. */
    private static final String BASE_RECHERCHE_APPLICATION_DGFIP = "ou=applications,ou=dgfip,ou=mefi,o=gouv,c=fr";

    /** Constante donnant le contexte de la recherche de personnes dans l'annuaire fusionné. */
    private static final String BASE_RECHERCHE_PERSONNE_DGFIP = "ou=personnes,ou=dgfip,ou=mefi,o=gouv,c=fr";

    /** Constante donnant le contexte de la recherche dans l'annuaire des usagers. */
    private static final String BASE_RECHERCHE_LDAP_ANNUAIRE_USAGERS = "ou=abonnes,ou=particuliers,ou=mefi,o=gouv,c=fr";

    /************************************************************/
    /** Identifiants / mots de passe de connexion aux annuaires */
    /************************************************************/

    /** Identifiant de connexion au sein de l'annuaire LDAP fusionné. */
    private static final String IDENTIFIANT_ANNUAIRE_DGFIP = "cn=lombok,ou=applications,ou=dgfip,ou=mefi,o=gouv,c=fr";

    /** Mot de passe de connexion au sein de l'annuaire LDAP fusionné. */
    private static final String MOT_DE_PASSE_ANNUAIRE_DGFIP = "lombok";

    /** Identifiant de connexion au sein de l'annuaire ex-DGI. */
    private static final String IDENTIFIANT_ANNUAIRE_DGI = "cn=proxy,ou=applications,ou=dgi,ou=mefi,o=gouv,c=fr";

    /** Mot de passe de connexion au sein de l'annuaire ex-DGI. */
    private static final String MOT_DE_PASSE_ANNUAIRE_DGI = "proxy";

    /** Identifiant de connexion au sein de l'annuaire LDAP des usagers. */
    private static final String IDENTIFIANT_ANNUAIRE_USAGERS = "cn=Manager,c=fr";

    /** Mot de passe de l'utilisateur accédant à l'annuaire LDAP des usagers. */
    private static final String MOT_DE_PASSE_ANNUAIRE_USAGERS = "secret";

    /***********/
    /** Divers */
    /***********/

    /** Constante donnant le libellé court de l'application Lombok démo. */
    private static final String CRITERE_RECHERCHE_APPLI_DGCP = "DMO";

    /** Critère de recherche d'une application dans l'annuaire DGI. */
    private static final String CRITERE_RECHERCHE_APPLI_DGI = "aai";

    /** Critère de recherche d'une application dans l'annuaire fusionné. */
    private static final String CRITERE_RECHERCHE_APPLI_DGFIP = "ERIREC";

    /** Critère de recherche d'une application dans l'annuaire des usagers. */
    private static final String CRITERE_RECHERCHE_APPLI_USAGERS = "TELEIR";

    /** Critère de recherche d'un nom de personne. */
    private static final String RECHERCHE_PREFIX_PERSONNE_NOM = "MARTIN";

    /** Critère de recherche d'un prénom de personne. */
    private static final String RECHERCHE_PREFIX_PERSONNE_PRENOM = "J";

    /** Critère de recherche d'un nom d'usager. */
    private static final String RECHERCHE_PREFIX_PERSONNE_NOM_USAGER = "THESMA";

    /** Critère de recherche d'un prénom d'usager. */
    private static final String RECHERCHE_PREFIX_PERSONNE_PRENOM_USAGER = "MAR";

    /** Critère de recherche d'un nom de personne pour tester la bascule SIRHIUS. */
    private static final String RECHERCHE_PREFIX_PERSONNE_NOM_SIRHIUS = "MARTINS";

    /** Critère de recherche d'un prénom de personne pour tester la bascule SIRHIUS. */
    private static final String RECHERCHE_PREFIX_PERSONNE_PRENOM_SIRHIUS = "J";

    /** Critère de recherche d'un prefix de nom de personne pour tester trop de résultats. */
    private static final String RECHERCHE_PREFIX_PERSONNE_NOM_TROP_RESULTAT = "MA";

    /** Critère de recherche d'un prefix de prénom de personne pour tester trop de résultats. */
    private static final String RECHERCHE_PREFIX_PERSONNE_PRENOM_TROP_RESULTAT = "MA";

    /** Critère de recherche d'un prefix de nom de personne pour tester trop de résultats. */
    private static final String RECHERCHE_PREFIX_PERSONNE_NOM_TROP_RESULTAT_FIXE = "MAR";

    /** Critère de recherche d'un prefix de nom de personne pour tester trop de résultats. */
    private static final String RECHERCHE_PREFIX_PERSONNE_PRENOM_TROP_RESULTAT_FIXE = "MAR";

    /** Critère de recherche d'une personne pour rechercher ses habilitations dans l'annuaire DGCP. */
    private static final String RECHERCHE_PERSONNE_HABILITATION_DGCP = "HOUARD";
    /** Critère de recherche d'une personne pour rechercher ses habilitations dans l'annuaire DGI. */
    private static final String RECHERCHE_PERSONNE_HABILITATION_DGI = "MAR";
    /** Critère de recherche d'une personne pour rechercher ses habilitations dans l'annuaire fusionné. */
    private static final String RECHERCHE_PERSONNE_HABILITATION_DGFIP = "MAR";
    /** Critère de recherche d'une personne pour rechercher ses habilitations dans l'annuaire des usagers. */
    private static final String RECHERCHE_PERSONNE_HABILITATION_USAGERS = "THESMA";

    /** Critère de recherche du profil de personne dans l'annuaire DGCP. */
    private static final String CRITERE_RECHERCHE_PROFIL_DGCP = "ADMINISTRATEUR";
    /** Critère de recherche du profil de personne dans l'annuaire DGI. */
    private static final String CRITERE_RECHERCHE_PROFIL_DGI = "ADMINISTRATEUR";
    /** Critère de recherche du profil de personne dans l'annuaire fusionné. */
    private static final String CRITERE_RECHERCHE_PROFIL_DGFIP = "5C";
    /** Critère de recherche du profil de personne dans l'annuaire des usagers. */
    private static final String CRITERE_RECHERCHE_PROFIL_USAGERS = "CFS";

    /** Critère de recherche d'une personne dans une application donnée. */
    private static final String RECHERCHE_PERSONNE_HABILITATION_APPLI = "MADRHAS";
    
    /** Constante donnant un préfixe d'un codique à rechercher. */
    private static final String RECHERCHE_PREFIXE_CODIQUE = "0";

    /** Constante donnant un UID à rechercher dans l'annuaire DGCP. */
    private static final String RECHERCHE_UID_DGCP = "chouard-cp";

    /** Constante donnant un UID à rechercher dans l'annuaire DGI. */
    private static final String RECHERCHE_UID_DGI = "867272-DGI";

    /** Constante donnant un UID à rechercher dans l'annuaire fusionne. */
    private static final String RECHERCHE_UID_DGFIP = "1016020263-0";

    /** Constante donnant un UID à rechercher dans l'annuaire des usagers. */
    private static final String RECHERCHE_UID_USAGERS = "1000004559A0";
    
    /** Constante donnant le libellé long de l'application Lombok démo */
    private static final String RESULTAT_RECHERCHE_APPLI_LIBELLE = "application de demo lombok";

    /** Constante donnant le préfixe d'un code sages à rechercher. */
    private static final String PREFIXE_SAGES = "B38";

    /** Initialisation de la journalisation. */
    protected static final Logger LOGGER = LoggerFactory.getLogger(LectureAnnuaireServicesTest.class);

    /**
     * Méthode permettant de tester une recherche au sein des annuaires en étant habilité.
     */
    @Test
    public void testConnexionNonHabilite()
    {
        // Annuaire ex-DGCP
        if (TEST_ANNUAIRE_DGCP)
        {
            testConnexionNonHabilite(TypeAnnuaire.DGCP);
        }

        // Annuaire ex-DGI
        if (TEST_ANNUAIRE_DGI)
        {
            testConnexionNonHabilite(TypeAnnuaire.DGI);
        }

        // Annuaire DGFIP
        if (TEST_ANNUAIRE_DGFIP)
        {
            testConnexionNonHabilite(TypeAnnuaire.DGFIP);
        }

        // Annuaire usagers
        if (TEST_ANNUAIRE_USAGERS)
        {
            testConnexionNonHabilite(TypeAnnuaire.USAGERS);
        }
    }

    /**
     * Méthode permettant de tester une recherche au sein d'un annuaire en étant habilité.
     * 
     * @param typeAnnuaire type d'annuaire à utiliser
     */
    private void testConnexionNonHabilite(TypeAnnuaire typeAnnuaire)
    {
        LOGGER.trace("Test d'une connexion à l'annuaire {} sans y être habilité", typeAnnuaire);

        // Démarrage du chrono
        long chrono = System.currentTimeMillis();

        try
        {
            // Connexion avec un mot de passe erroné pour la recherche des personnes dans l'annuaire
            LecturePersonneService lecturePersonneService = getLecturePersonneService(typeAnnuaire, "xxx");
            lecturePersonneService.rechercherPersonneAnnuaireParDebutPrenomNom(
                RECHERCHE_PREFIX_PERSONNE_PRENOM, RECHERCHE_PREFIX_PERSONNE_NOM);

            // Verification du résultat
            switch (typeAnnuaire)
            {
                case DGCP:
                    // Sur l'annuaire DGCP, pas de problème de connexion car celle-ci est anonyme (sans habilitation)
                    LOGGER.info("Pas de test sur l'annuaire DGCP car la connexion est anonyme");
                    break;
                default:
                    Assert.fail("La connexion à l 'annuaire " + typeAnnuaire + " aurait dû échouer car le mot de passe est erroné");
            }
        }
        catch (AnnuaireException ae)
        {
            switch (typeAnnuaire)
            {
                case DGCP:
                    // Sur l'annuaire DGCP, la connexion étant anonyme, une exception n'aurait pas dû remonter
                    Assert.fail("La connexion à l 'annuaire " + typeAnnuaire + " aurait dû fonctionner car la connexion est anonyme");
                    break;
                default:
                    // Journalisation du résultat
                    LOGGER.info("L'exception attendue a été levée avec message : {}", ae.getMessage());

                    // Vérification du résultat
                    Assert.assertEquals("Message d'erreur erroné", "Problème avec l'annuaire lors de l'ouverture de la connexion "
                        , ae.getMessage());
            }
        }
        finally
        {
            // Journalisation du chrono
            journaliserChrono(chrono);
        }
    }

    /**
     * Méthode permettant de tester une recherche d'application au seins des annuaires.
     */
    @Test
    public void testRechercherApplication()
    {
        // Annuaire ex-DGCP
        if (TEST_ANNUAIRE_DGCP)
        {
            testRechercherApplication(TypeAnnuaire.DGCP, CRITERE_RECHERCHE_APPLI_DGCP);
        }

        // Annuaire ex-DGI
        if (TEST_ANNUAIRE_DGI)
        {
            testRechercherApplication(TypeAnnuaire.DGI, CRITERE_RECHERCHE_APPLI_DGI);
        }

        // Annuaire DGFIP
        if (TEST_ANNUAIRE_DGFIP)
        {
            testRechercherApplication(TypeAnnuaire.DGFIP, CRITERE_RECHERCHE_APPLI_DGFIP);
        }

        // Annuaire USAGERS
        if (TEST_ANNUAIRE_USAGERS)
        {
            testRechercherApplication(TypeAnnuaire.USAGERS, CRITERE_RECHERCHE_APPLI_USAGERS);
        }
    }

    /**
     * Méthode permettant de tester une recherche d'application dans un annuaire.
     * 
     * @param typeAnnuaire type d'annuaire à utiliser
     * @param application nom de l'application à rechercher
     */
    private void testRechercherApplication(TypeAnnuaire typeAnnuaire, String application)
    {
        LOGGER.trace("Test de la recherche d'une application dans l'annuaire '{}'", typeAnnuaire);

        // Démarrage du chrono
        long chrono = System.currentTimeMillis();
        
        // Recherche de l'application dans l'annuaire
        LectureApplicationService lectureApplicationService = getLectureApplicationService(typeAnnuaire);
        ApplicationAnnuaire appli = lectureApplicationService.rechercherApplicationParLibelleCourt(application);

        // Journalisation du chrono
        journaliserChrono(chrono);

        // Vérification du résultat
        Assert.assertNotNull("Application non renseigné", appli);
        switch (typeAnnuaire)
        {
            case DGCP:
                Assert.assertNotNull("Libellé long de l'application non renseigné", appli.getLibelleLongAppli());
                Assert.assertEquals("Libellé long de l'application", RESULTAT_RECHERCHE_APPLI_LIBELLE, appli.getLibelleLongAppli());
                break;
            default:
                // Il n'y a pas de recherche par libellé court pour les autres annuaires
                LOGGER.info("Fonctionnalité non implémentée pour l'annuaire '{}', pas de vérification"
                    , typeAnnuaire);
                Assert.assertNull("Libellé long de l'application renseigné", appli.getLibelleLongAppli());
        }
    }

    /**
     * Méthode permettant de tester une recherche de personnes par nom / prénom au sein des annuaires.
     */
    @Test
    public void testRechercherPersonne()
    {
        // Annuaire ex-DGCP
        if (TEST_ANNUAIRE_DGCP)
        {
            testRechercherPersonne(TypeAnnuaire.DGCP
                , RECHERCHE_PREFIX_PERSONNE_NOM, RECHERCHE_PREFIX_PERSONNE_PRENOM);
        }

        // Annuaire ex-DGI
        if (TEST_ANNUAIRE_DGI)
        {
            testRechercherPersonne(TypeAnnuaire.DGI
                , RECHERCHE_PREFIX_PERSONNE_NOM, RECHERCHE_PREFIX_PERSONNE_PRENOM);
        }

        // Annuaire DGFIP
        if (TEST_ANNUAIRE_DGFIP)
        {
            testRechercherPersonne(TypeAnnuaire.DGFIP
                , RECHERCHE_PREFIX_PERSONNE_NOM, RECHERCHE_PREFIX_PERSONNE_PRENOM);
        }

        // Annuaire USAGERS
        if (TEST_ANNUAIRE_USAGERS)
        {
            testRechercherPersonne(TypeAnnuaire.USAGERS
                , RECHERCHE_PREFIX_PERSONNE_NOM_USAGER, RECHERCHE_PREFIX_PERSONNE_PRENOM_USAGER);
        }
    }

    /**
     * Méthode permettant de tester une recherche de personnes par nom / prénom dans un annuaire.
     * 
     * @param typeAnnuaire type d'annuaire à utiliser
     */
    private void testRechercherPersonne(TypeAnnuaire typeAnnuaire, String nom, String prenom)
    {
        LOGGER.trace("Test de la recherche d'une personne dans l'annuaire '{}'"
            + " ayant un nom commençant par '{}' et un prénom par '{}'", typeAnnuaire, nom, prenom);

        // Cas des annuaires non indexés sur le prénom / noms
        if (typeAnnuaire == TypeAnnuaire.USAGERS)
        {
            if (!isActiverAppelAnnuaireSurCriteresNonIndexes(typeAnnuaire))
            {
                return;
            }
        }

        // Démarrage du chrono
        long chrono = System.currentTimeMillis();

        // Recherche des personnes dans l'annuaire
        LecturePersonneService lecturePersonneService = getLecturePersonneService(typeAnnuaire);
        List<PersonneAnnuaire> personnes
            = lecturePersonneService.rechercherPersonneAnnuaireParDebutPrenomNom(prenom, nom);

        // Journalisation du chrono
        journaliserChrono(chrono);

        // Vérification du résultat
        journaliserPersonnesDebug(personnes);
        Assert.assertNotNull("Liste personne non renseignée", personnes);
        Assert.assertTrue("Liste personne vide", personnes.size() != 0);
        for (PersonneAnnuaire personneAnnuaire : personnes)
        {
            if (!personneAnnuaire.getSn().startsWith(nom))
            {
                journaliserPersonneInfo(personneAnnuaire);
                Assert.fail("Nom '" + personneAnnuaire.getSn() + "' reçu, avec un préfix recherché = '" + nom + "'");
            }
            if (!personneAnnuaire.getGivenName().startsWith(prenom))
            {
                journaliserPersonneInfo(personneAnnuaire);  
                Assert.fail("Prénom '" + personneAnnuaire.getGivenName() + "' reçu, avec un préfix recherché = '"
                    + prenom + "'");
            }
        }
    }

    /**
     * Méthode permettant de tester une recherche de personnes par UID au sein des annuaires.
     */
    @Test
    public void testRechercherPersonneParUID()
    {
        // Annuaire ex-DGCP
        if (TEST_ANNUAIRE_DGCP)
        {
            testRechercherPersonneParUID(TypeAnnuaire.DGCP, RECHERCHE_UID_DGCP);
        }

        // Annuaire ex-DGI
        if (TEST_ANNUAIRE_DGI)
        {
            testRechercherPersonneParUID(TypeAnnuaire.DGI, RECHERCHE_UID_DGI);
        }

        // Annuaire fusionné
        if (TEST_ANNUAIRE_DGFIP)
        {
            testRechercherPersonneParUID(TypeAnnuaire.DGFIP, RECHERCHE_UID_DGFIP);
        }

        // Annuaire USAGERS
        if (TEST_ANNUAIRE_USAGERS)
        {
            testRechercherPersonneParUID(TypeAnnuaire.USAGERS, RECHERCHE_UID_USAGERS);
        }
    }

    /**
     * Méthode permettant de tester une recherche de personnes par UID dans un annuaire.
     * 
     * @param typeAnnuaire type d'annuaire à utiliser
     * @param UID identifiant de la personne
     */
    private void testRechercherPersonneParUID(TypeAnnuaire typeAnnuaire
        , String uid)
    {
        LOGGER.trace("Test de la recherche d'une personne dans l'annuaire '{}'"
            + " avec un UID ayant la valeur '{}'", typeAnnuaire, uid);

        // Démarrage du chrono
        long chrono = System.currentTimeMillis();

        // Recherche des personnes dans l'annuaire
        LecturePersonneService lecturePersonneService = getLecturePersonneService(typeAnnuaire);
        PersonneAnnuaire personne = lecturePersonneService.rechercherPersonneAnnuaireParUID(uid);

        // Journalisation du chrono
        journaliserChrono(chrono);
        
        // Vérification du résultat
        journaliserPersonneDebug(personne);
        Assert.assertNotNull("Personne non renseignée", personne);
        if (!uid.equals(personne.getUid()))
        {
            journaliserPersonneInfo(personne);
            Assert.fail("UID reçu = '" + personne.getUid() + "', avec un UID recherché = '" + uid + "'");
        }
    }

    /**
     * Méthode permettant de tester une recherche du DN d'une personne par UID au sein des annuaires.
     */
    @Test
    public void testRecupererDnPersonne()
    {
        // Annuaire ex-DGCP
        if (TEST_ANNUAIRE_DGCP)
        {
            testRecupererDnPersonne(TypeAnnuaire.DGCP, RECHERCHE_UID_DGCP);
        }

        // Annuaire ex-DGI
        if (TEST_ANNUAIRE_DGI)
        {
            testRecupererDnPersonne(TypeAnnuaire.DGI, RECHERCHE_UID_DGI);
        }

        // Annuaire fusionné
        if (TEST_ANNUAIRE_DGFIP)
        {
            testRecupererDnPersonne(TypeAnnuaire.DGFIP, RECHERCHE_UID_DGFIP);
        }

        // Annuaire USAGERS
        if (TEST_ANNUAIRE_USAGERS)
        {
            testRecupererDnPersonne(TypeAnnuaire.USAGERS, RECHERCHE_UID_USAGERS);
        }
    }

    /**
     * Méthode permettant de tester une recherche du DN d'une personne par UID dans un annuaire.
     *
     * @param typeAnnuaire type d'annuaire à utiliser
     * @param UID identifiant de la personne
     */
    private void testRecupererDnPersonne(TypeAnnuaire typeAnnuaire, String UID)
    {
        LOGGER.trace("Test de la recherche d'un DN d'une personne dans l'annuaire '{}'"
            + " avec un UID ayant la valeur '{}'", typeAnnuaire, UID);

        // Démarrage du chrono
        long chrono = System.currentTimeMillis();

        LecturePersonneService lecturePersonneService = getLecturePersonneService(typeAnnuaire);
        String dnPersonne = lecturePersonneService.rechercherDNCompletUtilisateurPourUnUID(UID);

        // Journalisation du chrono
        journaliserChrono(chrono);

        // Vérification du résultat
        LOGGER.debug("DN : '{}'", dnPersonne);
        Assert.assertNotNull("DN non renseigné", dnPersonne);
    }

    /**
     * Méthode permettant de tester la levée d'une erreur lors d'une recherche de personnes dans les annuaires, s'il y a
     * trop de résultats.
     */
    @Test
    public void testNombrePersonnesTropEleve()
    {
        // Annuaire ex-DGCP
        if (TEST_ANNUAIRE_DGCP)
        {
            testNombrePersonnesTropEleve(TypeAnnuaire.DGCP);
        }

        // Annuaire ex-DGI
        if (TEST_ANNUAIRE_DGI)
        {
            testNombrePersonnesTropEleve(TypeAnnuaire.DGI);
        }

        // Annuaire DGFIP
        if (TEST_ANNUAIRE_DGFIP)
        {
            testNombrePersonnesTropEleve(TypeAnnuaire.DGFIP);
        }

        // Annuaire USAGERS
        /*if (TEST_ANNUAIRE_USAGERS)
        {
            testNombrePersonnesTropEleve(TypeAnnuaire.USAGERS);
        }*/
    }

    /**
     * Méthode permettant de tester la levée d'une erreur lors d'une recherche de personnes dans l'annuaire, s'il y a
     * trop de résultats.
     * 
     * @param typeAnnuaire type d'annuaire à utiliser
     */
    private void testNombrePersonnesTropEleve(TypeAnnuaire typeAnnuaire)
    {
        LOGGER.info("Test de la levée d'une erreur s'il y a trop de résultats dans l'annuaire {} (prénom : '{}' "
            + "nom : '{}')", typeAnnuaire, RECHERCHE_PREFIX_PERSONNE_PRENOM_TROP_RESULTAT, RECHERCHE_PREFIX_PERSONNE_NOM_TROP_RESULTAT);

        try
        {
            // Démarrage du chrono
            long chrono = System.currentTimeMillis();

            // Recherche des personnes dans l'annuaire
            LecturePersonneService lecturePersonneService = getLecturePersonneService(typeAnnuaire);
            List<PersonneAnnuaire> personnes = lecturePersonneService.rechercherPersonneAnnuaireParDebutPrenomNom(
                RECHERCHE_PREFIX_PERSONNE_PRENOM_TROP_RESULTAT, RECHERCHE_PREFIX_PERSONNE_NOM_TROP_RESULTAT);

            // Journalisation du chrono
            journaliserChrono(chrono);

            switch (typeAnnuaire)
            {
                case DGI:
                    // Pas d'exception remontée pour la DGI
                    LOGGER.info("Pas de test sur l'annuaire DGI car aucune exception remontée");
                    break;
                default:
                    Assert.fail("Une exception 'AnnuaireException' aurait du être remontée car"
                        + "il y a trop de personnes (personnes lues : " + personnes.size() + ")");
            }
        }
        catch (AnnuaireException ae)
        {
            // Journalisation du résultat
            LOGGER.info("Exception levée avec message : {}", ae.getMessage());

            // Vérification du résultat
            Assert.assertTrue("Le message devrait indiquer que le nombre de personnes est trop élevé"
                , ae.getMessage().startsWith(LecturePersonneServiceImpl.PB_NBE_PERSONNES_SUPERIEURE_MAX));
        }
    }

    /**
     * Méthode permettant de tester la levée d'une erreur lors d'une recherche de personnes dans les annuaires, s'il y a
     * plus de résultats que prévu.
     */
    @Test
    public void testNombrePersonnesTropEleveLimiteFixee()
    {
        // Annuaire ex-DGCP
        if (TEST_ANNUAIRE_DGCP)
        {
            testNombrePersonnesTropEleveLimiteFixee(TypeAnnuaire.DGCP);
        }

        // Annuaire ex-DGI
        if (TEST_ANNUAIRE_DGI)
        {
            testNombrePersonnesTropEleveLimiteFixee(TypeAnnuaire.DGI);
        }

        // Annuaire DGFIP
        if (TEST_ANNUAIRE_DGFIP)
        {
            testNombrePersonnesTropEleveLimiteFixee(TypeAnnuaire.DGFIP);
        }

        // Annuaire USAGERS
       /* if (TEST_ANNUAIRE_USAGERS)
        {
            testNombrePersonnesTropEleveLimiteFixee(TypeAnnuaire.USAGERS);
        }*/
    }

    /**
     * Méthode permettant de tester la levée d'une erreur lors d'une recherche de personnes dans l'annuaire, s'il y a
     * plus de résultats que prévu.
     * 
     * @param typeAnnuaire type d'annuaire à utiliser
     */
    private void testNombrePersonnesTropEleveLimiteFixee(TypeAnnuaire typeAnnuaire)
    {
        final Long TAILLE_LIMITE = 10L;
        LOGGER.trace("Test de la levée d'une erreur, dans l'annuaire {}, s'il y a plus de '{}' résultats (prénom : '{}' "
            + "nom : '{}')", typeAnnuaire, TAILLE_LIMITE, RECHERCHE_PREFIX_PERSONNE_NOM_TROP_RESULTAT_FIXE,
            RECHERCHE_PREFIX_PERSONNE_PRENOM_TROP_RESULTAT_FIXE);

        // Démarrage du chrono
        long chrono = System.currentTimeMillis();

        List<PersonneAnnuaire> personnes = null;
        try
        {
            // Recherche des personnes dans l'annuaire
            LecturePersonneService lecturePersonneService = getLecturePersonneService(typeAnnuaire);
            personnes = lecturePersonneService.rechercherPersonneAnnuaireParDebutPrenomNom(
                RECHERCHE_PREFIX_PERSONNE_PRENOM_TROP_RESULTAT_FIXE
                , RECHERCHE_PREFIX_PERSONNE_NOM_TROP_RESULTAT_FIXE
                , TAILLE_LIMITE);

            Assert.fail("Une exception 'AnnuaireException' aurait du être remontée car"
                + " il devrait y avoir plus de '" + TAILLE_LIMITE + "' personnes (personnes lues : "
                + personnes.size() + ")");
        }
        catch (AnnuaireException ae)
        {
            // Journalisation du chrono
            journaliserChrono(chrono);
            
            // Journalisation du résultat
            LOGGER.info("Exception attendue levée avec le message : {}", ae.getMessage());

            // Vérification du résultat
            Assert.assertEquals("Le message devrait indiquer que le nombre de personnes est trop élevé",
                LecturePersonneServiceImpl.PB_NBE_PERSONNES_SUPERIEURE_MAX, ae.getMessage());
        }
    }

    /**
     * Méthode permettant de tester la recherche d'une personne à partir d'une application au sein des différents
     * annuaires.
     */
    @Test
    public void testRechercherPersonneSurApplication()
    {
        // Annuaire ex-DGCP
        if (TEST_ANNUAIRE_DGCP)
        {
            testRechercherPersonneSurApplication(TypeAnnuaire.DGCP, CRITERE_RECHERCHE_APPLI_DGCP
                , RECHERCHE_PERSONNE_HABILITATION_DGCP);
        }

        // Annuaire ex-DGI
        if (TEST_ANNUAIRE_DGI)
        {
            testRechercherPersonneSurApplication(TypeAnnuaire.DGI, CRITERE_RECHERCHE_APPLI_DGI
                , RECHERCHE_PERSONNE_HABILITATION_DGI);
        }

        // Annuaire DGFIP
        if (TEST_ANNUAIRE_DGFIP)
        {
            testRechercherPersonneSurApplication(TypeAnnuaire.DGFIP, CRITERE_RECHERCHE_APPLI_DGFIP
                , RECHERCHE_PERSONNE_HABILITATION_DGFIP);
        }

        // Annuaire USAGERS
        if (TEST_ANNUAIRE_USAGERS)
        {
            testRechercherPersonneSurApplication(TypeAnnuaire.USAGERS, CRITERE_RECHERCHE_APPLI_USAGERS
                , RECHERCHE_PERSONNE_HABILITATION_USAGERS);
        }
    }

    /**
     * Méthode permettant de tester la recherche d'une personne à partir d'une application au sein d'un annuaire.
     *
     * @param typeAnnuaire type d'annuaire à utiliser
     * @param application application dont on recherche les personnes
     * @param nomPersonne nom des personnes à rechercher
     */
    private void testRechercherPersonneSurApplication(TypeAnnuaire typeAnnuaire, String application
        , String nomPersonne)
    {
        LOGGER.trace("Test de recherche des habilitatons des personnes commençant par '{}' "
            + "dans l'application '{}' de l'annuaire {}"
            , nomPersonne, application, typeAnnuaire);

        // Démarrage du chrono
        long chrono = System.currentTimeMillis();

        // Recherche des personnes dans l'annuaire
        LecturePersonneService lecturePersonneService = getLecturePersonneService(typeAnnuaire);
        List<PersonneAnnuaire> personnes = lecturePersonneService
            .rechercherPersonneAnnuaireAvecHabilitationSurApplicationAvecDebutNom(
                application, nomPersonne);

        // Journalisation du chrono
        journaliserChrono(chrono);

        // Vérification du résultat
        journaliserPersonnesDebug(personnes);
        Assert.assertNotNull("Liste de personnes non renseignée", personnes);
        switch (typeAnnuaire)
        {
            case USAGERS:
            case DGI:
                // Non implémentée sur les annuaires DGI et USAGERS
                LOGGER.info("Fonctionnalité non implémentée pour l'annuaire '{}', pas de vérification"
                    , typeAnnuaire);
                break;
            default:
                Assert.assertTrue("Liste de personnes vide", personnes.size() != 0);
        }
    }

    /**
     * Méthode permettant de tester une recherche d'habilitation dans les annuaires via le codique.
     */
    @Test
    public void testRechercherHabilitationViaApplicationCodique()
    {
        // Annuaire ex-DGCP
        if (TEST_ANNUAIRE_DGCP)
        {
            rechercherHabilitationViaApplicationCodique(TypeAnnuaire.DGCP, CRITERE_RECHERCHE_APPLI_DGCP);
        }

        // Annuaire ex-DGI
        if (TEST_ANNUAIRE_DGI)
        {
            rechercherHabilitationViaApplicationCodique(TypeAnnuaire.DGI, CRITERE_RECHERCHE_APPLI_DGI);
        }

        // Annuaire fusionné
        if (TEST_ANNUAIRE_DGFIP)
        {
            rechercherHabilitationViaApplicationCodique(TypeAnnuaire.DGFIP, CRITERE_RECHERCHE_APPLI_DGFIP);
        }

        // Annuaire USAGERS
        if (TEST_ANNUAIRE_USAGERS)
        {
            rechercherHabilitationViaApplicationCodique(TypeAnnuaire.USAGERS, CRITERE_RECHERCHE_APPLI_USAGERS);
        }
    }

    /**
     * Méthode permettant de tester une recherche d'habilitation dans un annuaire via le codique.
     * 
     * @param typeAnnuaire type d'annuaire à utiliser
     * @param application nom de l'application à rechercher
     */
    private void rechercherHabilitationViaApplicationCodique(TypeAnnuaire typeAnnuaire
        , String application)
    {
        LOGGER.trace("Test de recherche, sur l'annuaire {}, des habilitations des personnes de l'application '{}' "
            + "via un codique commençant par '{}'", typeAnnuaire, application, RECHERCHE_PREFIXE_CODIQUE);

        // Démarrage du chrono
        long chrono = System.currentTimeMillis();

        // Recherche des personnes dans l'annuaire sur une application, à partir d'un codique
        LecturePersonneService lecturePersonneService = getLecturePersonneService(typeAnnuaire);
        List<PersonneAnnuaire> personnes = lecturePersonneService
            .rechercherPersonneAnnuaireAvecHabilitationSurApplicationAvecPrefixeCodique(
                application, RECHERCHE_PREFIXE_CODIQUE);


        // Journalisation du chrono
        journaliserChrono(chrono);

        // Récupération des habilitations et vérification du résultat
        switch (typeAnnuaire)
        {
            case USAGERS:
            case DGI:
                // Non implémentée sur les annuaires DGI et USAGERS
                LOGGER.info("Fonctionnalité non implémentée pour l'annuaire '{}', pas de vérification"
                    , typeAnnuaire);
                break;
            default:
                journaliserPersonnesDebug(personnes);
                Assert.assertNotNull("Liste de personnes non renseignée", personnes);
                Assert.assertTrue("Liste personne vide", personnes.size() != 0);

                for (PersonneAnnuaire personne : personnes)
                {
                    int compteur = 0;

                    LOGGER.trace("Personne trouvee n°{} :", ++compteur);

                    // Récupération des habilitations de la personne
                    personne.calculerHabilitationSurApplication(application);
                    Set<HabilitationAnnuaire> habilitations = personne.getListeHabilitations();

                    // Vérification du résultat
                    Assert.assertNotNull("Habilitation non renseignée", habilitations);
                    Assert.assertTrue("Habilitation vide", habilitations.size() != 0);
                    LOGGER.debug("Nombre d'habilitation de la personne n°{} : {}"
                        , compteur, habilitations.size());

                    int compteur2 = 0;
                    for (Iterator<HabilitationAnnuaire> iterator = habilitations.iterator(); iterator.hasNext();)
                    {
                        HabilitationAnnuaire habilitation = (HabilitationAnnuaire) iterator.next();

                        // Vérification du résultat
                        Assert.assertNotNull("Habilitation non renseignée", habilitation);
                        Assert.assertNotNull("Profil non renseigné", habilitation.getNomProfil());
                        Assert.assertTrue("Profil vide", habilitation.getNomProfil().length() != 0);

                        LOGGER.debug("Habilitation #{} : {}", ++compteur2, habilitation.getNomProfil());
                    }
                }
        }
    }

    /**
     * Méthode permettant de tester une recherche de personne dans les annuaires,
     * affectée à une application, via son profil.
     */
    @Test
    public void testRechercherHabilitationViaApplicationCodiqueProfil()
    {
        // Annuaire ex-DGCP
        if (TEST_ANNUAIRE_DGCP)
        {
            testRechercherHabilitationViaApplicationCodiqueProfil(TypeAnnuaire.DGCP
                , CRITERE_RECHERCHE_APPLI_DGCP, CRITERE_RECHERCHE_PROFIL_DGCP);
        }

        // Annuaire ex-DGI
        if (TEST_ANNUAIRE_DGI)
        {
            testRechercherHabilitationViaApplicationCodiqueProfil(TypeAnnuaire.DGI
                , CRITERE_RECHERCHE_APPLI_DGI, CRITERE_RECHERCHE_PROFIL_DGI);
        }

        // Annuaire fusionné
        if (TEST_ANNUAIRE_DGFIP)
        {
            testRechercherHabilitationViaApplicationCodiqueProfil(TypeAnnuaire.DGFIP
                , CRITERE_RECHERCHE_APPLI_DGFIP, CRITERE_RECHERCHE_PROFIL_DGFIP);
        }
        
        // Annuaire USAGERS
        if (TEST_ANNUAIRE_USAGERS)
        {
            testRechercherHabilitationViaApplicationCodiqueProfil(TypeAnnuaire.USAGERS
                , CRITERE_RECHERCHE_APPLI_USAGERS, CRITERE_RECHERCHE_PROFIL_USAGERS);
        }
    }

    /**
     * Méthode permettant de tester une recherche de personne dans les annuaires,
     * affectée à une application, via son profil.
     * @param typeAnnuaire type d'annuaire à utiliser
     * @param application nom de l'application à rechercher
     * @param profil profil de la personne
     */
    private void testRechercherHabilitationViaApplicationCodiqueProfil(TypeAnnuaire typeAnnuaire
        , String application, String profil)
    {
        LOGGER.trace("test recherche habilitations sur '{}' dont les codiques commencent par '{}'"
            , typeAnnuaire, RECHERCHE_PREFIXE_CODIQUE);

        // Démarrage du chrono
        long chrono = System.currentTimeMillis();

        LecturePersonneService lecturePersonneService = getLecturePersonneService(typeAnnuaire);
        List<PersonneAnnuaire> personnes =
            lecturePersonneService.rechercherPersonneAnnuaireAvecHabilitationSurApplicationAvecPrefixeCodiqueAvecProfil(
                application, RECHERCHE_PREFIXE_CODIQUE, profil);

        // Journalisation du chrono
        journaliserChrono(chrono);

        // Vérification  du résultat
        journaliserPersonnesDebug(personnes);
        switch (typeAnnuaire)
        {
            case USAGERS:
            case DGI:
                // Non implémentée sur les annuaires DGI et USAGERS
                LOGGER.info("Fonctionnalité non implémentée pour l'annuaire '{}', pas de vérification"
                    , typeAnnuaire);
                break;
            default:
                Assert.assertNotNull("Liste de personnes non renseigné", personnes);
                Assert.assertTrue("Liste de personnes vide", personnes.size() != 0);
        }
    }

    /**
     * Méthode permettant de tester une recherche de personne dans les annuaires,
     * affectée à une application, via son profil et un code sages.
     */
    @Test
    public void testRechercherPersonneAnnuaireParHabilitationApplicationSagesAvecProfil()
    {
        // Annuaire ex-DGCP
        if (TEST_ANNUAIRE_DGCP)
        {
            testRechercherPersonneAnnuaireParHabilitationApplicationSagesAvecProfil(TypeAnnuaire.DGCP
                , CRITERE_RECHERCHE_APPLI_DGCP, CRITERE_RECHERCHE_PROFIL_DGCP);
        }

        // Annuaire ex-DGI
        if (TEST_ANNUAIRE_DGI)
        {
            testRechercherPersonneAnnuaireParHabilitationApplicationSagesAvecProfil(TypeAnnuaire.DGI
                , CRITERE_RECHERCHE_APPLI_DGI, CRITERE_RECHERCHE_PROFIL_DGI);
        }

        // Annuaire fusionné
        if (TEST_ANNUAIRE_DGFIP)
        {
            testRechercherPersonneAnnuaireParHabilitationApplicationSagesAvecProfil(TypeAnnuaire.DGFIP
                , CRITERE_RECHERCHE_APPLI_DGFIP, CRITERE_RECHERCHE_PROFIL_DGFIP);
        }
        
        // Annuaire USAGERS
        if (TEST_ANNUAIRE_USAGERS)
        {
            testRechercherPersonneAnnuaireParHabilitationApplicationSagesAvecProfil(TypeAnnuaire.USAGERS
                , CRITERE_RECHERCHE_APPLI_USAGERS, CRITERE_RECHERCHE_PROFIL_USAGERS);
        } 
    }
    
    /**
     * Méthode permettant de tester une recherche de personne dans un annuaire,
     * affectée à une application, via son profil et un code sages.
     * @param typeAnnuaire type d'annuaire à utiliser
     * @param application nom de l'application à rechercher
     * @param profil profil de la personne
     */
    private void testRechercherPersonneAnnuaireParHabilitationApplicationSagesAvecProfil(
        TypeAnnuaire typeAnnuaire, String application, String profil)
    {
        LOGGER.trace("test recherche habilitations sur {} dans les sages commençant par {}",
            RECHERCHE_PERSONNE_HABILITATION_APPLI, PREFIXE_SAGES);

        switch (typeAnnuaire)
        {
            case USAGERS:
            case DGI:
            case DGCP:
                // Non implémentée sur ces annuaires
                LOGGER.info("Fonctionnalité non implémentée pour l'annuaire '{}', test non executé"
                    , typeAnnuaire);
                return;
            default :
                // Cas DGFiP
        }
        
        // Démarrage du chrono
        long chrono = System.currentTimeMillis();

        LecturePersonneService lecturePersonneService = getLecturePersonneService(typeAnnuaire);
        List<PersonneAnnuaire> personnes = lecturePersonneService
                .rechercherPersonneAnnuaireAvecHabilitationSurApplicationAvecPrefixeSagesAvecProfilDansAnnuaireFusionne(
                    application, PREFIXE_SAGES, profil);

        // Journalisation du chrono
        journaliserChrono(chrono);

        // Vérification  du résultat
        journaliserPersonnesDebug(personnes);
        Assert.assertNotNull("Liste des personnes non renseignée", personnes);
        Assert.assertTrue("Liste de personnes vide", personnes.size() != 0);
    }

    /**
     * Méthode permettant de tester une recherche de personne dans les annuaires,
     * affectée à une application, via son profil.
     */
    @Test
    public void testRechercherPersonneAvecHabilitationSurApplicationAvecProfil()
    {
        // Annuaire ex-DGCP
        if (TEST_ANNUAIRE_DGCP)
        {
            testRechercherPersonneAvecHabilitationSurApplicationAvecProfil(TypeAnnuaire.DGCP
                , CRITERE_RECHERCHE_APPLI_DGCP, CRITERE_RECHERCHE_PROFIL_DGCP);
        }

        // Annuaire ex-DGI
        if (TEST_ANNUAIRE_DGI)
        {
            testRechercherPersonneAvecHabilitationSurApplicationAvecProfil(TypeAnnuaire.DGI
                , CRITERE_RECHERCHE_APPLI_DGI, CRITERE_RECHERCHE_PROFIL_DGI);
        }

        // Annuaire DGFIP
        if (TEST_ANNUAIRE_DGFIP)
        {
            testRechercherPersonneAvecHabilitationSurApplicationAvecProfil(TypeAnnuaire.DGFIP
                , CRITERE_RECHERCHE_APPLI_DGFIP, CRITERE_RECHERCHE_PROFIL_DGFIP);
        }

        // Annuaire USAGERS
        if (TEST_ANNUAIRE_USAGERS)
        {
            testRechercherPersonneAvecHabilitationSurApplicationAvecProfil(TypeAnnuaire.USAGERS
                , CRITERE_RECHERCHE_APPLI_USAGERS, CRITERE_RECHERCHE_PROFIL_USAGERS);
        }
    }

    /**
     * Méthode permettant de tester une recherche de personne dans un annuaire,
     * affectée à une application, via son profil.
     * @param typeAnnuaire type d'annuaire à utiliser
     * @param application nom de l'application à rechercher
     * @param profil profil de la personne
     */
    private void testRechercherPersonneAvecHabilitationSurApplicationAvecProfil(TypeAnnuaire typeAnnuaire
        , String application, String profil)
    {
        LOGGER.trace("Test de recherche des personnes ayant un profil '{}' sur l'application '{}' "
            + "de l'annuaire {}", profil, application, typeAnnuaire);
        
        // Cas des annuaires non indexés sur le prénom / noms
        if (!isActiverAppelAnnuaireSurCriteresNonIndexes(typeAnnuaire))
        {
            return;
        }
        
        // Démarrage du chrono
        long chrono = System.currentTimeMillis();
        
        LecturePersonneService lecturePersonneService = getLecturePersonneService(typeAnnuaire);

        List<PersonneAnnuaire> personnes = lecturePersonneService
            .rechercherPersonneAnnuaireAvecHabilitationSurApplicationAvecProfil(application, profil);

        // Journalisation du chrono
        journaliserChrono(chrono);
        
        // Vérification du résultat
        journaliserPersonnesDebug(personnes);
        Assert.assertNotNull("Liste des personnes non renseignée", personnes);
        switch (typeAnnuaire)
        {
            case USAGERS:
            case DGI:
                // Non implémentée sur les annuaires DGI et USAGERS
                LOGGER.info("Fonctionnalité non implémentée pour l'annuaire '{}', pas de vérification"
                    , typeAnnuaire);
                break;
            default:
                Assert.assertTrue("Liste de personnes vide", personnes.size() != 0);
        }
    }

    /**
     * Méthode permettant de tester les nouveaux champs ajoutés lors de bascule SIHRIUS.
     */
    //@Test
    public void testBasculeSirhius()
    {
        LOGGER.trace("Test de la bascule SIRHIUS");
        
        // Paramétrage de l'accès à l'annuaire LDAP
        RessourceAnnuaireServiceImpl resourceldapService = new RessourceAnnuaireServiceImpl();
        resourceldapService.setUrlAnnuaireLDAP(LDAP_SIHRIUS);

        LecturePersonneServiceImpl lecturePersonneService = new LecturePersonneServiceImpl();
        lecturePersonneService.setRessourceannuaireso(resourceldapService);
        lecturePersonneService.setBaseRechercheLDAP(BASE_RECHERCHE_PERSONNE_DGFIP);
        lecturePersonneService.setIdentifiantConnexionLdapApplication(IDENTIFIANT_ANNUAIRE_DGFIP);
        lecturePersonneService.setMotDePasseConnexionLdapApplication(MOT_DE_PASSE_ANNUAIRE_DGFIP);
        lecturePersonneService.setTypeAnnuaire(TypeAnnuaire.DGFIP.name());

        // Récupération d'une liste de personnes
        List<PersonneAnnuaire> listePersonnes = lecturePersonneService.rechercherPersonneAnnuaireParDebutPrenomNom(
            RECHERCHE_PREFIX_PERSONNE_PRENOM_SIRHIUS, RECHERCHE_PREFIX_PERSONNE_NOM_SIRHIUS);

        // Vérification de la présence de données
        journaliserPersonnesDebug(listePersonnes);
        Assert.assertNotNull("Liste de personnes non renseigné", listePersonnes);
        Assert.assertTrue("Liste de personnes vide", listePersonnes.size() != 0);
        for (PersonneAnnuaire personneAnnuaire : listePersonnes)
        {
            // Journalisation des données
            LOGGER.debug("UID SIRHIUS : '{}'", personneAnnuaire.getUidSirhius());
            LOGGER.debug("Code grade SIRHIUS : '{}'", personneAnnuaire.getCodeGradeSirhius());
            LOGGER.debug("Code CSRH : '{}'", personneAnnuaire.getCodeCSRH());

            // Vérification de la présence des attributs liés à la bascule SIRHIUS
            if (personneAnnuaire.getUidSirhius() != null
                && personneAnnuaire.getCodeGradeSirhius() != null
                && personneAnnuaire.getCodeCSRH() != null)
            {
                LOGGER.info("Les nouveaux attributs de '{}', liés à la bascule SIRHIUS, sont présents"
                    , personneAnnuaire.getCn());
            }
            else
            {
                Assert.fail("Les nouveaux attributs de '" + personneAnnuaire.getCn()
                    + "', liés à la bascule SIRHIUS, ne sont pas présents");
            }
        }
    }

    /**
     * Méthode permettant d'ajouter une journalisation d'un temps d'exécution.
     *
     * @param chrono temps de départ
     */
    private void journaliserChrono(long chrono)
    {
        LOGGER.debug("traitement effectué en {} ms", System.currentTimeMillis() - chrono);
    }

    /**
     * Méthode permettant d'ajouter une journalisation de personnes en mode "debug".
     *
     * @param personnes personnes à journaliser
     */
    private void journaliserPersonnesDebug(List<PersonneAnnuaire> personnes)
    {
        LOGGER.debug("Nombre de personnes : {}", personnes.size());
        int compteur = 0;
        for (PersonneAnnuaire personneAnnuaire : personnes)
        {
            LOGGER.debug("Personne trouvee n°{} :", ++compteur);
            journaliserPersonneDebug(personneAnnuaire);
        }
    }

    /**
     * Méthode permettant d'ajouter une journalisation d'une personne en mode "info".
     *
     * @param personneAnnuaire personne à journaliser
     */
    private void journaliserPersonneInfo(PersonneAnnuaire personneAnnuaire)
    {
        LOGGER.info("UID : '{}'", personneAnnuaire.getUid());
        LOGGER.info("SN : '{}'", personneAnnuaire.getSn());
        LOGGER.info("Nom : '{}'", personneAnnuaire.getGivenName());
        LOGGER.info("Affectation : '{}'", personneAnnuaire.getAffectation());
        LOGGER.info("Mail : '{}'", personneAnnuaire.getMail());
        LOGGER.info("Profil : '{}'", personneAnnuaire.getListeBruteMefiAppliXxx());
    }

    /**
     * Méthode permettant d'ajouter une journalisation d'une personne en mode "debug".
     *
     * @param personneAnnuaire personne à journaliser
     */
    private void journaliserPersonneDebug(PersonneAnnuaire personneAnnuaire)
    {
        LOGGER.debug("UID : '{}'", personneAnnuaire.getUid());
        LOGGER.debug("SN : '{}'", personneAnnuaire.getSn());
        LOGGER.debug("Nom : '{}'", personneAnnuaire.getGivenName());
        LOGGER.debug("Affectation : '{}'", personneAnnuaire.getAffectation());
        LOGGER.debug("Mail : '{}'", personneAnnuaire.getMail());
        LOGGER.debug("Profil : '{}'", personneAnnuaire.getListeBruteMefiAppliXxx());
    }

    /**
     * Méthode permettant d'initialiser la recherche au sein d'un annuaire.
     *
     * @param typeAnnuaire annuaire sur lequel porte la recherche
     */
    private LectureApplicationService getLectureApplicationService(TypeAnnuaire typeAnnuaire)
    {
        LectureApplicationServiceImpl lectureApplicationService = new LectureApplicationServiceImpl();
        switch (typeAnnuaire)
        {
            case DGCP:
                lectureApplicationService.setRessourceannuaireso(getRessourceAnnuaireServiceDGCP());
                lectureApplicationService.setBaseRechercheLDAP(BASE_RECHERCHE_APPLICATION_DGCP);
                break;
            case DGI:
                lectureApplicationService.setRessourceannuaireso(getRessourceAnnuaireServiceDGI());
                lectureApplicationService.setBaseRechercheLDAP(BASE_RECHERCHE_APPLICATION_DGI);
                lectureApplicationService.setIdentifiantConnexionLdapApplication(IDENTIFIANT_ANNUAIRE_DGI);
                lectureApplicationService.setMotDePasseConnexionLdapApplication(MOT_DE_PASSE_ANNUAIRE_DGI);
                break;
            case DGFIP:
                lectureApplicationService.setRessourceannuaireso(getRessourceAnnuaireServiceDGFIP());
                lectureApplicationService.setBaseRechercheLDAP(BASE_RECHERCHE_APPLICATION_DGFIP);
                lectureApplicationService.setIdentifiantConnexionLdapApplication(IDENTIFIANT_ANNUAIRE_DGFIP);
                lectureApplicationService.setMotDePasseConnexionLdapApplication(MOT_DE_PASSE_ANNUAIRE_DGFIP);
                break;
            case USAGERS:
                lectureApplicationService.setRessourceannuaireso(getRessourceAnnuaireServiceUsagers());
                lectureApplicationService.setBaseRechercheLDAP(BASE_RECHERCHE_LDAP_ANNUAIRE_USAGERS);
                lectureApplicationService.setIdentifiantConnexionLdapApplication(IDENTIFIANT_ANNUAIRE_USAGERS);
                lectureApplicationService.setMotDePasseConnexionLdapApplication(MOT_DE_PASSE_ANNUAIRE_USAGERS);
        }

        return lectureApplicationService;
    }

    /**
     * Méthode permettant d'initialiser la recherche de personne au sein d'un annuaire.
     *
     * @param typeAnnuaire annuaire sur lequel porte la recherche
     */
    private LecturePersonneService getLecturePersonneService(TypeAnnuaire typeAnnuaire)
    {
        LecturePersonneService lecturePersonneService = null;
        switch (typeAnnuaire)
        {
            case DGCP:
                lecturePersonneService = getLecturePersonneService(typeAnnuaire, null);
                break;
            case DGI:
                lecturePersonneService = getLecturePersonneService(typeAnnuaire
                    , MOT_DE_PASSE_ANNUAIRE_DGI);
                break;
            case DGFIP:
                lecturePersonneService = getLecturePersonneService(typeAnnuaire
                    , MOT_DE_PASSE_ANNUAIRE_DGFIP);
                break;
            case USAGERS:
                lecturePersonneService = getLecturePersonneService(typeAnnuaire
                    , MOT_DE_PASSE_ANNUAIRE_USAGERS);
        }

        return lecturePersonneService;
    }

    /**
     * Méthode permettant d'initialiser la recherche de personne au sein d'un annuaire sur lequel
     * il faut s'habiliter.
     *
     * @param typeAnnuaire annuaire sur lequel porte la recherche
     * @param motDePasse permettant d'obtenir l'autorisation d'effectuer la recherche
     */
    private LecturePersonneService getLecturePersonneService(TypeAnnuaire typeAnnuaire
        , String motDePasse)
    {
        LecturePersonneServiceImpl lecturePersonneService = new LecturePersonneServiceImpl();
        switch (typeAnnuaire)
        {
            case DGCP:
                lecturePersonneService.setRessourceannuaireso(getRessourceAnnuaireServiceDGCP());
                lecturePersonneService.setBaseRechercheLDAP(BASE_RECHERCHE_PERSONNE_DGCP);
                break;
            case DGI:
                lecturePersonneService.setRessourceannuaireso(getRessourceAnnuaireServiceDGI());
                lecturePersonneService.setBaseRechercheLDAP(BASE_RECHERCHE_PERSONNE_DGI);
                lecturePersonneService.setIdentifiantConnexionLdapApplication(IDENTIFIANT_ANNUAIRE_DGI);
                lecturePersonneService.setMotDePasseConnexionLdapApplication(motDePasse);
                break;
            case DGFIP:
                lecturePersonneService.setRessourceannuaireso(getRessourceAnnuaireServiceDGFIP());
                lecturePersonneService.setBaseRechercheLDAP(BASE_RECHERCHE_PERSONNE_DGFIP);
                lecturePersonneService.setIdentifiantConnexionLdapApplication(IDENTIFIANT_ANNUAIRE_DGFIP);
                lecturePersonneService.setMotDePasseConnexionLdapApplication(motDePasse);
                lecturePersonneService.setTypeAnnuaire(typeAnnuaire.name());
                break;
            case USAGERS:
                lecturePersonneService.setRessourceannuaireso(getRessourceAnnuaireServiceUsagers());
                lecturePersonneService.setBaseRechercheLDAP(BASE_RECHERCHE_LDAP_ANNUAIRE_USAGERS);
                lecturePersonneService.setIdentifiantConnexionLdapApplication(IDENTIFIANT_ANNUAIRE_USAGERS);
                lecturePersonneService.setMotDePasseConnexionLdapApplication(motDePasse);
                lecturePersonneService.setTypeAnnuaire("USAGERS");
                lecturePersonneService.setTypeAnnuaire(typeAnnuaire.name());
        }

        return lecturePersonneService;
    }

    /**
     * Méthode permettant d'initialiser l'accès à l'annuaire DGCP.
     */
    private RessourceAnnuaireServiceImpl getRessourceAnnuaireServiceDGCP()
    {
        RessourceAnnuaireServiceImpl resourceldapService = new RessourceAnnuaireServiceImpl();
        resourceldapService.setUrlAnnuaireLDAP(LDAP_DGCP);
        return resourceldapService;
    }

    /**
     * Méthode permettant d'initialiser l'accès à l'annuaire DGI.
     */
    private RessourceAnnuaireServiceImpl getRessourceAnnuaireServiceDGI()
    {
        RessourceAnnuaireServiceImpl resourceldapService = new RessourceAnnuaireServiceImpl();
        resourceldapService.setUrlAnnuaireLDAP(LDAP_DGI);
        return resourceldapService;
    }

    /**
     * Méthode permettant d'initialiser l'accès à l'annuaire fusionné.
     */
    private RessourceAnnuaireServiceImpl getRessourceAnnuaireServiceDGFIP()
    {
        RessourceAnnuaireServiceImpl resourceldapService = new RessourceAnnuaireServiceImpl();
        resourceldapService.setUrlAnnuaireLDAP(LDAP_ANNUAIRE_FUSIONNE);
        return resourceldapService;
    }

    /**
     * Méthode permettant d'initialiser l'accès à l'annuaire des usagers.
     */
    private RessourceAnnuaireServiceImpl getRessourceAnnuaireServiceUsagers()
    {
        RessourceAnnuaireServiceImpl resourceldapService = new RessourceAnnuaireServiceImpl();
        resourceldapService.setUrlAnnuaireLDAP(LDAP_ANNUAIRE_USAGERS);
        return resourceldapService;
    }

    /**
     * Méthode permettant d'exécuter les méthodes même si les annuaires ne sont pas indexés (la recherche sera longue).
     * @param typeAnnuaire annuaire sur lequel porte la recherche
     */
    private boolean isActiverAppelAnnuaireSurCriteresNonIndexes(TypeAnnuaire typeAnnuaire)
    {
        if (!ACTIVER_APPEL_ANNUAIRE_SUR_CRITERES_NON_INDEXES)
        {
            LOGGER.info("Traitement non exécuté car les critères ne sont pas indexés sur l'annuaire {}, "
                + "ce qui amènerait un temps trop long", typeAnnuaire);
        }
        return ACTIVER_APPEL_ANNUAIRE_SUR_CRITERES_NON_INDEXES;
    }
}
