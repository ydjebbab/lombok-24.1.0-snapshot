/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : CpBooleanEditorTest.java
 *
 */
package fr.gouv.finances.lombok.test.util.propertyeditor;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import fr.gouv.finances.lombok.util.format.BooleanFormat;
import fr.gouv.finances.lombok.util.format.FormaterBoolean;
import fr.gouv.finances.lombok.util.propertyeditor.CpBooleanEditor;
import junit.framework.TestCase;

/**
 * Class CpBooleanEditorTest --.
 * 
 * @version $Revision: 1.2 $ Date: 11 déc. 2009
 */
public class CpBooleanEditorTest extends TestCase
{

    /** log. */
    protected final Log log = LogFactory.getLog(getClass());

    /**
     * methode Test blank as false : --.
     */
    public void testBlankAsFalse()
    {

        Boolean validBoolean = Boolean.FALSE;

        String[] validBooleant = {"     ", ""};

        BooleanFormat bf = FormaterBoolean.getFormatBooleanVraiFaux();
        bf.setBlankEgalFalse(true);

        CpBooleanEditor editor = new CpBooleanEditor(CpBooleanEditor.FALSE);

        for (int i = 0; i < validBooleant.length; i++)
        {
            String validBooleanType = validBooleant[i];
            try
            {
                editor.setAsText(validBooleanType);

                if (editor.getValue() != null)
                {
                    log.info("Texte booléen : " + validBooleanType + "; booléen trouvé = "
                        + bf.format(editor.getValue()));
                    assertEquals(validBoolean, editor.getValue());
                }
                else
                {
                    log.info("Texte booléen : " + validBooleanType + "; booléen trouvé = null ");
                }
            }
            catch (IllegalArgumentException ex)
            {
                fail("Aucune exception ne devrait être levée, ce booléen (" + validBooleanType + ") est valide."
                    + ex.getMessage());
            }
        }
    }

    /**
     * methode Test blank as not allowed : --.
     */
    public void testBlankAsNotAllowed()
    {

        String[] validBooleant = {"     ", ""};

        BooleanFormat bf = FormaterBoolean.getFormatBooleanVraiFaux();
        bf.setBlankEgalFalse(false);

        CpBooleanEditor editor = new CpBooleanEditor(CpBooleanEditor.NOT_ALLOWED);

        for (int i = 0; i < validBooleant.length; i++)
        {
            String validBooleanType = validBooleant[i];
            try
            {
                editor.setAsText(validBooleanType);
                fail("Les chaînes vides doivent proviquer une exception");

            }
            catch (IllegalArgumentException ex)
            {
                assertTrue(true);
            }
        }
    }

    /**
     * methode Test blank as null : --.
     */
    public void testBlankAsNull()
    {

        Boolean validBoolean = null;

        String[] validBooleant = {"     ", ""};

        BooleanFormat bf = FormaterBoolean.getFormatBooleanVraiFaux();
        bf.setBlankEgalFalse(false);

        CpBooleanEditor editor = new CpBooleanEditor(CpBooleanEditor.NULL);

        for (int i = 0; i < validBooleant.length; i++)
        {
            String validBooleanType = validBooleant[i];
            try
            {
                editor.setAsText(validBooleanType);

                if (editor.getValue() != null)
                {
                    log.info("Texte booléen : " + validBooleanType + "; booléen trouvé = "
                        + bf.format(editor.getValue()));
                    assertEquals(validBoolean, editor.getValue());
                }
                else
                {
                    log.info("Texte booléen : " + validBooleanType + "; booléen trouvé = null ");
                }
            }
            catch (IllegalArgumentException ex)
            {
                fail("Aucune exception ne devrait être levée, la chaîne (" + validBooleanType
                    + ") produit un Boolean null" + ex.getMessage());
            }
        }
    }

    /**
     * methode Test valid format false : --.
     */
    public void testValidFormatFALSE()
    {

        Boolean validBoolean = Boolean.FALSE;

        String[] validBooleant =
        {"non", "NON", "FAUX", "faux", "n", "N", "FALSE", "false", "no", "0", "no n", " NON ", " FA UX", "   faux",
                " n", " N", " F A L S E", " false", " no", " 0 "

        };

        BooleanFormat bf = FormaterBoolean.getFormatBooleanVraiFaux();

        CpBooleanEditor editor = new CpBooleanEditor();

        for (int i = 0; i < validBooleant.length; i++)
        {
            String validBooleanType = validBooleant[i];
            try
            {
                editor.setAsText(validBooleanType);

                if (editor.getValue() != null)
                {
                    log.info("Texte booléen : " + validBooleanType + "; booléen trouvé = "
                        + bf.format(editor.getValue()));
                    assertEquals(validBoolean, editor.getValue());
                }
                else
                {
                    log.info("Texte booléen : " + validBooleanType + "; booléen trouvé = null ");
                }
            }
            catch (IllegalArgumentException ex)
            {
                fail("Aucune exception ne devrait être levée, ce booléen (" + validBooleanType + ") est valide");
            }
        }
    }

    /**
     * methode Test valid format true : --.
     */
    public void testValidFormatTRUE()
    {

        Boolean validBoolean = Boolean.TRUE;

        String[] validBooleant =
        {"oui", "OUI", "VRAI", "vrai", "o", "O", "TRUE", "true", "YES", "1", " oui ", " O UI ", " VRA I", " vr ai",
                " o ", " O ", "T  RUE", "tr ue", " YES", "1"

        };

        BooleanFormat bf = FormaterBoolean.getFormatBooleanVraiFaux();

        CpBooleanEditor editor = new CpBooleanEditor();

        for (int i = 0; i < validBooleant.length; i++)
        {
            String validBooleanType = validBooleant[i];
            try
            {
                editor.setAsText(validBooleanType);

                if (editor.getValue() != null)
                {
                    log.info("Texte booléen : " + validBooleanType + "; booléen trouvé = "
                        + bf.format(editor.getValue()));
                    assertEquals(validBoolean, editor.getValue());
                }
                else
                {
                    log.info("Texte booléen : " + validBooleanType + "; booléen trouvé = null ");
                }
            }
            catch (IllegalArgumentException ex)
            {
                fail("Aucune exception ne devrait être levée, ce booléen (" + validBooleanType + ") est valide");
            }
        }
    }

}
