/*
 * Copyright (c) 2017 DGFiP - Tous droit réservés
 */
package fr.gouv.finances.lombok.monnaie.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;



import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.ObjectRetrievalFailureException;
import org.springframework.transaction.annotation.Transactional;

import fr.gouv.finances.lombok.monnaie.bean.Monnaie;
import fr.gouv.finances.lombok.monnaie.bean.TauxDeChancellerie;
import fr.gouv.finances.lombok.test.controle.ControleDonnees;
import fr.gouv.finances.lombok.test.controle.VerificationExecution;
import fr.gouv.finances.lombok.util.base.PurgeTablesDao;

/**
 * Socle permettant de tester les DAO de gestion des données de monnaie,<br/>
 * quelle que soit l'implémentation choisie.
 *
 * @author Christophe Breheret-Girardin
 */
public abstract class MonnaieDaoCoreTest
{
    /** Initialisation de la journalisation. */
    protected static final Logger LOGGER = LoggerFactory.getLogger(MonnaieDaoCoreTest.class);

    /** Déclaration du DAO de gestion des données de monnaie. */
    @Autowired
    private MonnaieDao monnaiedao;

    /** Déclaration du DAO de gestion des purges de la base. */
    @Autowired
    private PurgeTablesDao purgetablesdao;

    /** Map contenant les paramètres de journalisation présents en base. */
    private Map<Integer, Monnaie> mapMonnaie = new HashMap<>();

    /** Valeur inexistante utilisée dans les tests */
    private static final String VALEUR_INEXISTANTE = "kamoulox";
    
    /** Valeur quelconque utilisée dans les tests */
    private static final String VALEUR_QUELCONQUE = "λ";

    /**
     * Initialisation des données en base avant chaque test.
     */
    @Before
    public void creerDonnees()
    {
        // Purge des données de la base
        purgetablesdao.purgerTables("TAUX_TAUX", "MONNAIE_MONN");

        // Vérification de l'absence de données
        ControleDonnees.verifierElements(monnaiedao.loadAllObjects(Monnaie.class));

        // Génération des monnaies, insertion en base de données et sauvegarde dans une Map
        mapMonnaie = IntStream.rangeClosed(1, 5).mapToObj(this::getMonnaie)
            .peek(monnaiedao::saveObject)
            .collect(Collectors.toMap(m -> Integer.parseInt(m.getCodeBDF()), m -> m));

        // Vérification de la présence des données avant exécution du test
        ControleDonnees.verifierElements(monnaiedao.loadAllObjects(Monnaie.class)
            , mapMonnaie.get(1), mapMonnaie.get(2), mapMonnaie.get(3), mapMonnaie.get(4), mapMonnaie.get(5));

        // Suppression du cache pour forcer les requêtes des tests
        monnaiedao.clearPersistenceContext();
    }

    /**
     * Méthode permettant de tester l'injection du DAO.
     */
    @Test
    public void injectionDao()
    {
        assertNotNull(monnaiedao);
    }

    /**
     * Méthode permettant de tester les contraintes de l'entité monnaie.
     */
    @Test
    public void contraintes()
    {
        // Une contrainte existe sur l'unicité de la propriété "codeBDF"
        Monnaie monnaie6 = getMonnaie(6);
        monnaie6.setCodeBDF(mapMonnaie.get(1).getCodeBDF());
        VerificationExecution.verifierException(Throwable.class, () -> monnaiedao.saveObject(monnaie6));

        // Une contrainte existe sur la propriété "libelleMonnaie" obligatoire
        Monnaie monnaie7 = getMonnaie(7);
        monnaie7.setLibelleMonnaie(null);
        VerificationExecution.verifierException(Throwable.class, () -> monnaiedao.saveObject(monnaie7));

        // Une contrainte existe sur la taille de la propriété "codeBDF"
        Monnaie monnaie8 = getMonnaie(8);
        monnaie8.setCodeBDF(VALEUR_INEXISTANTE);
        VerificationExecution.verifierException(Throwable.class, () -> monnaiedao.saveObject(monnaie8));

        // Une contrainte existe sur la taille de la propriété "libelleMonnaie"
        Monnaie monnaie9 = getMonnaie(9);
        monnaie9.setLibelleMonnaie(StringUtils.leftPad("9", 50, " "));
        VerificationExecution.verifierException(Throwable.class, () -> monnaiedao.saveObject(monnaie9));
    }

    /**
     * Méthode permettant de tester le non chargement par défaut des taux de chancellerie.
     */
    @Test
    public void lazy()
    {
        // Les taux de chancellerie doivent être en chargement "lazy" par défaut
        List<Monnaie> listeMonnaie = monnaiedao.loadAllObjects(Monnaie.class);
        listeMonnaie.stream().forEach(monnaie ->
            assertFalse(monnaiedao.isCharged(monnaie.getListTauxChancel())));
    }

    /**
     * Méthode permettant de tester le versionning automatique de l'entité monnaie.
     */
    @Test
    public void version()
    {
        // Création d'une nouvelle monnaie
        Monnaie monnaie6 = getMonnaie(6);
        monnaiedao.saveObject(monnaie6);

        // Vérification de l'insertion en base et du numéro de version
        Monnaie monnaie = monnaiedao.getObject(Monnaie.class, monnaie6.getId());
        assertEquals(monnaie6, monnaie);
        assertEquals(0, monnaie.getVersion());
        
        // Mise à jour du libellé
        monnaie.setLibelleMonnaie(VALEUR_QUELCONQUE);
        monnaiedao.modifyMonnaie(monnaie);
        
        // Vérification de la modification du numéro de version
        monnaie = monnaiedao.getObject(Monnaie.class, monnaie6.getId());
        assertEquals(VALEUR_QUELCONQUE, monnaie.getLibelleMonnaie());
        assertEquals(1, monnaie.getVersion());
    }

    /**
     * Méthode permettant de tester le mode cascade de l'entité monnaie.
     */
    @Test
    @Transactional
    public void cascade()
    {
        // Création de 7 taux de chancellerie à partir d'une monnaie
        Monnaie monnaie6 = getMonnaie(6);
        monnaiedao.saveObject(monnaie6);

        // Vérification de l'insertion en base
        Monnaie monnaie = monnaiedao.getObject(Monnaie.class, monnaie6.getId());
        assertEquals(monnaie6, monnaie);
        // Il doit y avoir 7 taux de chancellerie créé
        assertEquals(7, monnaie.getListTauxChancel().size());
        // Vérification des valeurs des 7 taux
        assertEquals("Ce ne sont pas les taux de chancellerie attendus"
            , new HashSet<>(Arrays.asList(60, 61, 62, 63, 64, 65, 66))
            , monnaie.getListTauxChancel().stream().map(taux -> taux.getValeurTauxChancel())
                .map(valeur -> valeur.intValue()).collect(Collectors.toSet()));

        // Récupération des identifiants des taux de chancellerie créés
        List<Long> ids =  monnaie.getListTauxChancel().stream().map(taux -> taux.getId())
            .collect(Collectors.toList());
        // Suppression des taux de chancellerie à partir d'une monnaie
        monnaiedao.deleteObject(monnaie6);
        // Vérification que la monnaie a été supprimé
        VerificationExecution.verifierException(ObjectRetrievalFailureException.class
            , () -> monnaiedao.getObject(Monnaie.class, monnaie6.getId()));
        // Vérification que chaque taux a été supprimé
        ids.stream().forEach(id ->
            VerificationExecution.verifierException(ObjectRetrievalFailureException.class
                , () -> monnaiedao.getObject(TauxDeChancellerie.class, id)));
    }

    /**
     * Test de la méthode
     * fr.gouv.finances.lombok.monnaie.dao.MonnaieDao#saveMonnaieEtTaux(fr.gouv.finances.lombok.monnaie.bean.Monnaie).
     */
    @Test
    public void saveMonnaieEtTaux()
    {
        // Exécution de la méthode à tester sur un cas non possible et vérification
        assertFalse(monnaiedao.saveMonnaie(mapMonnaie.get(1)));
        ControleDonnees.verifierElements(monnaiedao.loadAllObjects(Monnaie.class)
            , mapMonnaie.get(1), mapMonnaie.get(2), mapMonnaie.get(3), mapMonnaie.get(4), mapMonnaie.get(5));

        // Exécution de la méthode à tester et vérification
        Monnaie monnaie6 = getMonnaie(6);
        assertTrue(monnaiedao.saveMonnaie(monnaie6));
        ControleDonnees.verifierElements(monnaiedao.loadAllObjects(Monnaie.class)
            , mapMonnaie.get(1), mapMonnaie.get(2), mapMonnaie.get(3), mapMonnaie.get(4), mapMonnaie.get(5)
            , monnaie6);
    }

    /**
     * Test de la méthode
     * fr.gouv.finances.lombok.monnaie.dao.MonnaieDao#saveMonnaie(fr.gouv.finances.lombok.monnaie.bean.Monnaie).
     */
    @Test
    public void saveMonnaie()
    {
        saveMonnaieEtTaux();
    }

    

    /**
     * Test de la méthode
     * fr.gouv.finances.lombok.monnaie.dao.MonnaieDao#deleteMonnaie(fr.gouv.finances.lombok.monnaie.bean.Monnaie)
     */
    @Test
    public void deleteMonnaie()
    {
        // Exécution de la méthode à tester et vérifications
        monnaiedao.deleteMonnaie(mapMonnaie.get(2));
        ControleDonnees.verifierElements(monnaiedao.loadAllObjects(Monnaie.class)
            , mapMonnaie.get(1), mapMonnaie.get(3), mapMonnaie.get(4), mapMonnaie.get(5));
    }

    /**
     * Test de la méthode fr.gouv.finances.lombok.monnaie.dao.MonnaieDao#findAllMonnaies()
     */
    @Test
    public void findAllMonnaies()
    {
        // Exécution de la méthode à tester et vérification des monnaies
        List<Monnaie> monnaies = monnaiedao.findAllMonnaies();
        ControleDonnees.verifierElements(monnaies
            , mapMonnaie.get(1), mapMonnaie.get(2), mapMonnaie.get(3), mapMonnaie.get(4), mapMonnaie.get(5));

        // Verifications des taux
        monnaies.stream().forEach(monnaie -> verifierTaux(monnaie));
    }

    /**
     * Test de la méthode fr.gouv.finances.lombok.monnaie.dao.MonnaieDao#findAllMonnaiesTriSurLibelle().
     */
    @Test
    public void findAllMonnaiesTriSurLibelle()
    {
        // Exécution de la méthode à tester et vérification (en prenant l'ordre en compte) des monnaies
        List<Monnaie> monnaies = monnaiedao.findAllMonnaiesTriSurLibelle();
        ControleDonnees.verifierElements(monnaies, true
            , mapMonnaie.get(5), mapMonnaie.get(4), mapMonnaie.get(3), mapMonnaie.get(2), mapMonnaie.get(1));

        // Verifications des taux
        monnaies.stream().forEach(monnaie -> verifierTaux(monnaie));
    }

    /**
     * Test de la méthode fr.gouv.finances.lombok.monnaie.dao.MonnaieDao#findMonnaieParCode(java.lang.String).
     */
    @Test
    public void findMonnaieParCode()
    {
        mapMonnaie.entrySet().stream().forEach(entry ->
        {
            Monnaie monnaie = monnaiedao.findMonnaieParCode(String.valueOf(entry.getKey()));
            assertEquals(entry.getValue(), monnaie);
            assertFalse(monnaiedao.isCharged(monnaie.getListTauxChancel()));
        });
    }

    /**
     * Test de la méthode
     * 
     */
    @Test
    public void modifyMonnaie()
    {
        // Modification d'une monnaie non persistée
        Monnaie monnaie = mapMonnaie.get(2);
        assertEquals("libelle 8", monnaie.getLibelleMonnaie());

        // Modification d'une monnaie persistée
        monnaie.setLibelleMonnaie(VALEUR_QUELCONQUE);
        monnaiedao.modifyMonnaie(monnaie);
        assertEquals(VALEUR_QUELCONQUE, monnaiedao.getObject(Monnaie.class, monnaie.getId()).getLibelleMonnaie());
    }

    /**
     * Méthode permettant de vérifier une monnaie.
     *
     * @param monnaie la monnaie à vérifier
     */
    private void verifierTaux(Monnaie monnaie)
    {
        switch (monnaie.getCodeBDF())
        {
            case "4" :
                assertTrue("Aucun taux n'est attendu pour la monnaie #" + monnaie.getCodeBDF()
                    , monnaie.getListTauxChancel().isEmpty());
                break;
            case "5" :
                assertEquals("Un seul taux est attendu pour la monnaie #" + monnaie.getCodeBDF()
                    , 1, monnaie.getListTauxChancel().size());
                break;
            default :
                assertTrue("Une liste de taux est attendue pour la monnaie #" + monnaie.getCodeBDF()
                    , monnaiedao.isCharged(monnaie.getListTauxChancel()));
                assertEquals("Nombre de taux pour la monnaie #" + monnaie.getCodeBDF()
                    , Integer.parseInt(monnaie.getCodeBDF()) + 1, monnaie.getListTauxChancel().size());
        }
    }

    /**
     * Méthode permettant de générer une monnaie.
     *
     * @param indice indice permettant d'ajouter du dynamisme
     * @return la monnaie générée
     */
    private Monnaie getMonnaie(int indice)
    {
        Monnaie monnaie = new Monnaie();
        monnaie.setCodeBDF(String.valueOf(indice));
        monnaie.setLibelleMonnaie("libelle " + (10 - indice));

        Set<TauxDeChancellerie> tauxDeChancellerie = new HashSet<>();
        switch (indice)
        {
            case 4 :
                // Aucun taux pour la monnaie #4
                break;
            case 5 :
                // 1 seul taux pour la monnaire #5
                monnaie.setTauxChancellerie(getTauxDeChancellerie(1, monnaie));
                break;
             default :
                 // Création de (indice + 1) taux pour les autres monnaies
                 IntStream.rangeClosed(indice * 10, (indice * 10) + indice).forEach(indiceTaux ->
                     tauxDeChancellerie.add(getTauxDeChancellerie(indiceTaux, monnaie)));
                 monnaie.setListTauxChancel(tauxDeChancellerie);
        }

        return monnaie;
    }

    /**
     * Méthode permettant de générer un taux de chancellerie.
     *
     * @param indice indice permettant d'ajouter du dynamisme
     * @return le taux de chancellerie généré
     */
    private TauxDeChancellerie getTauxDeChancellerie(int indiceTaux, Monnaie monnaie)
    {
        TauxDeChancellerie tauxDeChancellerie = TauxDeChancellerieUtils.getTauxDeChancellerie(indiceTaux);
        tauxDeChancellerie.setLaMonnaie(monnaie);
        return tauxDeChancellerie;
    }

}