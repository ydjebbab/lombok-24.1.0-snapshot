package fr.gouv.finances.lombok.test.controle;

import java.util.Objects;

class Element
{
    private String attributChaine;
    
    private Long attributLong;

    @Override
    public int hashCode()
    {
        return Objects.hash(attributChaine, attributLong);
    }

    @Override
    public boolean equals(Object obj)
    {
        if (obj == this)
        {
            return true;
        }
        if (!(obj instanceof Element)) {
            return false;
        }
        Element element = (Element) obj;
        return Objects.equals(attributChaine, element.attributChaine)
            && Objects.equals(attributLong, element.attributLong);
    }

    /**
     * Accesseur de attributChaine.
     *
     * @return attributChaine
     */
    public String getAttributChaine()
    {
        return attributChaine;
    }

    /**
     * Mutateur de attributChaine.
     *
     * @param attributChaine attributChaine
     */
    public void setAttributChaine(String attributChaine)
    {
        this.attributChaine = attributChaine;
    }

    /**
     * Accesseur de attributLong.
     *
     * @return attributLong
     */
    public Long getAttributLong()
    {
        return attributLong;
    }

    /**
     * Mutateur de attributLong.
     *
     * @param attributLong attributLong
     */
    public void setAttributLong(Long attributLong)
    {
        this.attributLong = attributLong;
    }
    
}
