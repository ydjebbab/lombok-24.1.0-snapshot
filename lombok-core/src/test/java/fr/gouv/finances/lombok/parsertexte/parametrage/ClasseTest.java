/*
 * Copyright (c) 2014 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.parsertexte.parametrage;

import java.util.HashMap;
import java.util.Map;

import fr.gouv.finances.lombok.test.pojo.AbstractCorePojoTest;

/**
 * Tests unitaires du POJO Classe
 *
 * @author Christophe Breheret-Girardin
 */
public class ClasseTest extends AbstractCorePojoTest<Classe>
{

    /**
     * Constructeur.
     */
    public ClasseTest()
    {
        super();

        // ajout une valeur préfabriquée pour l'instanciation d'une Map
        addPrefabValues(Map.class, new HashMap<String, AttributClasse>());
    }

}
