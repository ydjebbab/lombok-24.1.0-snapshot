/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : TestBean.java
 *
 */
package fr.gouv.finances.lombok.test.util.propertyeditor;

import fr.gouv.finances.lombok.util.base.BaseBean;

/**
 * Class TestBean --.
 * 
 * @version $Revision: 1.2 $ Date: 11 déc. 2009
 */
public class TestBean extends BaseBean
{

    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** code. */
    private String code;

    /** libelle. */
    private String libelle;

    /**
     * Accesseur de l attribut code.
     * 
     * @return code
     */
    public String getCode()
    {
        return code;
    }

    /**
     * Accesseur de l attribut libelle.
     * 
     * @return libelle
     */
    public String getLibelle()
    {
        return libelle;
    }

    /**
     * Modificateur de l attribut code.
     * 
     * @param code le nouveau code
     */
    public void setCode(String code)
    {
        this.code = code;
    }

    /**
     * Modificateur de l attribut libelle.
     * 
     * @param libelle le nouveau libelle
     */
    public void setLibelle(String libelle)
    {
        this.libelle = libelle;
    }

}
