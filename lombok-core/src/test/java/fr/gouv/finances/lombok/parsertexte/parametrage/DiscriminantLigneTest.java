/*
 * Copyright (c) 2014 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.parsertexte.parametrage;

import fr.gouv.finances.lombok.test.pojo.AbstractCorePojoTest;

/**
 * Tests unitaires du POJO DiscriminantLigne
 *
 * @author Christophe Breheret-Girardin
 */
public class DiscriminantLigneTest extends AbstractCorePojoTest<DiscriminantLigne>
{

}
