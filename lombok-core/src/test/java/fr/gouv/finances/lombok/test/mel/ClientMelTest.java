/*
 * Copyright (c) 2017 DGFiP - Tous droits réservés
 * 
 */
package fr.gouv.finances.lombok.test.mel;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import fr.gouv.finances.lombok.mel.service.impl.MelServiceImpl;

/**
 * ClientMelTest.java (problème rencontré avec applicationContext-resources.xml qui fait appel à ConstantesGlobales qui
 * jette une exception ExploitationException visible uniquement dans le debugger à cause du static.. du coup il faut
 * rajouter les fichier test.properties et override.properties pour que ça foncitonne à voir plus tard ...
 * 
 * @author chouard Date: 9 mars 2017
 */
@RunWith(SpringJUnit4ClassRunner.class)
@TestPropertySource(value="testmel.properties")
@ContextConfiguration(loader=AnnotationConfigContextLoader.class)

public class ClientMelTest
{

    /** client mel. */
    @Autowired
    protected MelServiceImpl clientMel;

    /**
     * methode Test mel service impl :.
     */
    @Test
    public void testMelServiceImpl()
    {
        assertNotNull(clientMel);
    }
    
    /** 
     * ClientMelTest.java
     * comme on a choisi une contexte avec conf annotée, on rajoute une inner class de conf avec un importresource pour la partie xml et en
     * ajoutant le placeholder pour que les properties fonctionnent
     * @author chouard
     * Date: 15 oct. 2019
     */
    @Configuration
    @ImportResource(locations = {
    "classpath:/conf/applicationContext-commun-service.xml"})
    public static class TestContext{
            @Bean
            public static PropertySourcesPlaceholderConfigurer properties(){
              return new PropertySourcesPlaceholderConfigurer();
            }
    }

}

