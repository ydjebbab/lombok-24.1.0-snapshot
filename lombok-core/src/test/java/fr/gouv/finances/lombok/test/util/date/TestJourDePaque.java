/*
 * Copyright (c) 2015 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.test.util.date;

import java.text.ParseException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.junit.Assert;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.joda.time.DateTime;
import org.junit.Test;

import fr.gouv.finances.lombok.util.date.JoursFeries;
import fr.gouv.finances.lombok.util.exception.ProgrammationException;
import fr.gouv.finances.lombok.util.format.FormaterDate;

/**
 * Class TestJourDePaque.
 */
public class TestJourDePaque
{
    
    /** log. */
    private final Log log = LogFactory.getLog(getClass());
    
    /** Constant : DATES_PAQUES. */
    private static final Map<Integer, String> DATES_PAQUES =
        new HashMap<Integer, String>()
        {
            /**
             * serialVersionUID - long
             */
            private static final long serialVersionUID = 1L;

            {
                put(Integer.valueOf(1900), "15/04/1900");
                put(Integer.valueOf(1989), "26/03/1989");
                put(Integer.valueOf(1990), "15/04/1990");
                put(Integer.valueOf(1991), "31/03/1991");
                put(Integer.valueOf(1992), "19/04/1992");
                put(Integer.valueOf(1993), "11/04/1993");
                put(Integer.valueOf(2000), "23/04/2000");
                put(Integer.valueOf(2001), "15/04/2001");
                put(Integer.valueOf(2002), "31/03/2002");
                put(Integer.valueOf(2003), "20/04/2003");
                put(Integer.valueOf(2004), "11/04/2004");
                put(Integer.valueOf(2005), "27/03/2005");
                put(Integer.valueOf(2006), "16/04/2006");
                put(Integer.valueOf(2007), "08/04/2007");
                put(Integer.valueOf(2008), "23/03/2008");
                put(Integer.valueOf(2009), "12/04/2009");
                put(Integer.valueOf(2010), "04/04/2010");
                put(Integer.valueOf(2011), "24/04/2011");
                put(Integer.valueOf(2012), "08/04/2012");
                put(Integer.valueOf(2013), "31/03/2013");
                put(Integer.valueOf(2014), "20/04/2014");
                put(Integer.valueOf(2015), "05/04/2015");
                put(Integer.valueOf(2016), "27/03/2016");
                put(Integer.valueOf(2017), "16/04/2017");
                put(Integer.valueOf(2018), "01/04/2018");
                put(Integer.valueOf(2019), "21/04/2019");
                put(Integer.valueOf(2020), "12/04/2020");
                put(Integer.valueOf(2021), "04/04/2021");
                put(Integer.valueOf(2022), "17/04/2022");
                put(Integer.valueOf(2023), "09/04/2023");
                put(Integer.valueOf(2024), "31/03/2024");
                put(Integer.valueOf(2025), "20/04/2025");
                put(Integer.valueOf(2026), "05/04/2026");
                put(Integer.valueOf(2027), "28/03/2027");
                put(Integer.valueOf(2028), "16/04/2028");
                put(Integer.valueOf(2029), "01/04/2029");
                put(Integer.valueOf(2030), "21/04/2030");
            }
        };



    /**
     * methode Parser date :.
     * 
     * @param source chaine date
     * @return date
     */
    private Date parserDate(String source)
    {
        Date date = null;

        try
        {
            date = FormaterDate.getFormatDate(FormaterDate.F_DATE_CT).parse(source);
        }
        catch (ParseException exp)
        {
            throw new ProgrammationException("Date incorrecte", exp);
        }
        return date;
    }

    /**
     * methode Test determine le jour de paques.
     */
    @Test
    public void testDetermineLeJourDePaques()
    {
        Date paque2015 = parserDate("05/04/2015");

        DateTime paqueCalcule2015 = JoursFeries.determineLeJourDePaques(2015);

        Assert.assertEquals(paque2015, paqueCalcule2015.toDate());
        
        Set<Integer> annees = DATES_PAQUES.keySet();
        
        for (Integer annee : annees)
        {
            Date datePaqueAttendue = parserDate(DATES_PAQUES.get(annee));
            DateTime paqueCalcule = JoursFeries.determineLeJourDePaques(annee);
            Assert.assertEquals(datePaqueAttendue, paqueCalcule.toDate());
            log.warn("Date paque " + FormaterDate.formatDateAbrege(paqueCalcule.toDate()));
        }

    }
}
