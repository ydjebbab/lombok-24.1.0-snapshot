/*
 * Copyright (c) 2016 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.util.messages;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.junit.Test;


/**
 * Class MessagesTest (reste à faire un test pour le bundle messages.properties)
 */
public class MessagesTest
{
  

    /**
     * methode Test get string qui va tester une propriété non existante dans le fichier communmessages.properties
     * moche).
     *
     * @throws Exception the exception
     */
    @Test
    public final void testGetStringNull() throws Exception
    {
  
        assertNull(Messages.getString("cleAbsenteCommunProperties"));

    }
    
    /**
     * methode Test get string test une propriété dans le communmessages.properties
     *
     * @throws Exception the exception
     */
    @Test
    public final void testGetString() throws Exception
    {
  
        assertNotNull(Messages.getString("exception.test"));

    }
}
