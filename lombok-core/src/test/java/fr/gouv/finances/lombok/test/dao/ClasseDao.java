package fr.gouv.finances.lombok.test.dao;

import java.util.List;

import fr.gouv.finances.lombok.util.base.CoreBaseDao;
import fr.gouv.finances.lombok.util.persistance.ScrollIterator;

/**
 * Interface d'une classe d'élèves
 *
 * @author Christophe Breheret-Girardin
 */
interface ClasseDao extends CoreBaseDao
{
    /**
     * Méthode permettant d'initialiser le nombre maximum de résultat d'une recherche
     *
     * @param maxResultat maximum de résultat d'une recherche
     */
    void setMaxResultat(int maxResultat);

    /**
     * Méthode permettant de rechercher les classes, avec un paramètrage d'un nombre maximum de résultat
     * qui ne sera pas conservé pour les recherches futures.
     *
     * @param maxResultat maximum de résultat de la recherche
     */
    List<Classe> getMaxClassesEtanche(int maxResultat);

    /**
     * Méthode permettant de rechercher les classes, avec un paramètrage d'un nombre maximum de résultat
     * qui sera conservé pour les recherches futures dans certaines implémentations.
     *
     * @param maxResultat maximum de résultat de la recherche
     */
    List<Classe> getMaxClassesNonEtanche(int maxResultat);

    /**
     * Méthode permettant de rechercher les classes, avec un paramètrage d'un nombre maximum de résultat
     * via le contexte de persistance (et qui ne sera pas conservé pour les recherches futures).
     *
     * @param maxResultat maximum de résultat de la recherche
     */
    List<Classe> getMaxClassesViaContextePersistance(int maxResultat);

    /**
     * Méthode permettant de récupèrer une liste de classe d'élèves.
     */
    List<Classe> getClasses();

    /**
     * Méthode permettant de récupèrer un itérateur en mode curseur.
     * @param tailleBloc taille de bloc
     */
    ScrollIterator getScrollIterator(int tailleBloc);
}