/*
 * Copyright (c) 2014 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.clamav.bean;

import fr.gouv.finances.lombok.test.pojo.AbstractCorePojoTest;

/**
 * Tests unitaires du POJO TypeResponse.
 *
 * @param TypeResponse type de réponse du serveur clamAV
 *
 * @author Christophe Breheret-Girardin
 */
public class TypeResponseTest extends AbstractCorePojoTest<TypeResponse>
{

}
