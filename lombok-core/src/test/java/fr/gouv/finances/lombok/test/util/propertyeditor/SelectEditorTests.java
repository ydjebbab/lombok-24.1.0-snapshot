/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : SelectEditorTests.java
 *
 */
package fr.gouv.finances.lombok.test.util.propertyeditor;

import java.beans.PropertyEditor;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.Assert;

import org.junit.Test;

import fr.gouv.finances.lombok.util.exception.ProgrammationException;
import fr.gouv.finances.lombok.util.propertyeditor.SelectEditor;

/**
 * Class SelectEditorTests --.
 * 
 * @version $Revision: 1.2 $ Date: 11 déc. 2009
 */
public class SelectEditorTests
{

    /** tb1. */
    private TestBean tb1;

    /** tb2. */
    private TestBean tb2;

    /** tb3. */
    private TestBean tb3;

    /** tb4. */
    private TestBean tb4;

    /** tbstring1. */
    private String tbstring1 = "01";

    /** tbstring2. */
    private String tbstring2 = "02";

    /** tbstring3. */
    private String tbstring3 = "03";

    /** tbstring4. */
    private String tbstring4 = "04";

    /** set test bean. */
    private Set<TestBean> setTestBean = new HashSet<TestBean>();

    /** list test bean. */
    private List<TestBean> listTestBean = new ArrayList<TestBean>();

    /** map test bean. */
    private Map<String, TestBean> mapTestBean = new HashMap<String, TestBean>();

    /** set test bean first object null. */
    private Set<TestBean> setTestBeanFirstObjectNull = new HashSet<TestBean>();

    /** list test bean first object null. */
    private List<TestBean> listTestBeanFirstObjectNull = new ArrayList<TestBean>();

    /** list test bean incoherence. */
    private List<TestBean> listTestBeanIncoherence = new ArrayList<TestBean>();

    /** list test bean without property. */
    private List<String> listTestBeanWithoutProperty = new ArrayList<String>();

    /**
     * Instanciation de select editor tests.
     */
    public SelectEditorTests()
    {
        this.tb1 = new TestBean();
        this.tb2 = new TestBean();
        this.tb3 = new TestBean();
        this.tb4 = new TestBean();

        tb1.setCode("01");
        tb1.setLibelle("libelle01");
        tb2.setCode("02");
        tb2.setLibelle("libelle02");
        tb3.setCode("03");
        tb3.setLibelle("libelle03");
        tb4.setCode("04");
        tb4.setLibelle("libelle04");

        setTestBean.add(tb1);
        setTestBean.add(tb2);
        setTestBean.add(tb3);
        setTestBean.add(tb4);

        listTestBean.add(tb1);
        listTestBean.add(tb2);
        listTestBean.add(tb3);
        listTestBean.add(tb4);

        mapTestBean.put("01", tb1);
        mapTestBean.put("02", tb2);
        mapTestBean.put("03", tb3);
        mapTestBean.put("04", tb4);

        setTestBeanFirstObjectNull.add(null);
        listTestBeanFirstObjectNull.add(null);

        listTestBeanIncoherence.add(tb1);
        listTestBeanIncoherence.add(tb2);
        listTestBeanIncoherence.add(tb2);
        listTestBeanIncoherence.add(tb3);

        listTestBeanWithoutProperty.add(tbstring1);
        listTestBeanWithoutProperty.add(tbstring2);
        listTestBeanWithoutProperty.add(tbstring3);
        listTestBeanWithoutProperty.add(tbstring4);
    }

    /**
     * methode Test select in list set as text first object null : --.
     */
    @Test
    public void testSelectInListSetAsTextFirstObjectNull()
    {
        try
        {
            PropertyEditor selectEditor = new SelectEditor(listTestBeanFirstObjectNull, "code");
            selectEditor.setAsText("02");
        }
        catch (ProgrammationException e)
        {
            Assert.assertEquals(e.getMessage(), SelectEditor.ALL_NULL);
        }
    }

    /**
     * methode Test select in list set as text illegal property : --.
     */
    @Test
    public void testSelectInListSetAsTextIllegalProperty()
    {
        try
        {
            PropertyEditor selectEditor = new SelectEditor(setTestBean, "illegalproperty");
            selectEditor.setAsText("02");
        }
        catch (ProgrammationException e)
        {
            MessageFormat msgFormat = new MessageFormat(SelectEditor.INVALID_PROPERTY);
            Object[] attr = {"illegalproperty", tb1.getClass().getName()};

            Assert.assertEquals(e.getMessage(), msgFormat.format(attr));
        }
    }

    /**
     * methode Test select in list set as text list incoherence : --.
     */
    @Test
    public void testSelectInListSetAsTextListIncoherence()
    {
        try
        {
            PropertyEditor selectEditor = new SelectEditor(listTestBeanIncoherence, "code");
            selectEditor.setAsText("02");
        }
        catch (ProgrammationException e)
        {
            MessageFormat msgFormat = new MessageFormat(SelectEditor.INCOHERENCE);
            Object[] attr = {"getCode", "02"};

            Assert.assertEquals(e.getMessage(), msgFormat.format(attr));
        }
    }

    /**
     * methode Test select in list set as text not ok : --.
     */
    @Test
    public void testSelectInListSetAsTextNotOk()
    {
        PropertyEditor selectEditor = new SelectEditor(listTestBean, "code");
        selectEditor.setAsText("02");
        Assert.assertNotSame(this.tb1, selectEditor.getValue());
    }

    /**
     * methode Test select in list set as text null ok : --.
     */
    @Test
    public void testSelectInListSetAsTextNullOk()
    {
        PropertyEditor selectEditor = new SelectEditor(listTestBean, "code");
        selectEditor.setAsText("1000");
        Assert.assertEquals(null, selectEditor.getValue());
    }

    /*
     * Tests utilisation de SelectEditor avec des collections de type List
     */
    /**
     * methode Test select in list set as text ok : --.
     */
    @Test
    public void testSelectInListSetAsTextOk()
    {
        PropertyEditor selectEditor = new SelectEditor(listTestBean, "code");
        selectEditor.setAsText("02");
        Assert.assertEquals(this.tb2, selectEditor.getValue());
    }

    /**
     * methode Test select in list set as text property without getter : --.
     */
    @Test
    public void testSelectInListSetAsTextPropertyWithoutGetter()
    {
        try
        {
            PropertyEditor selectEditor = new SelectEditor(setTestBean, "codeWithoutGetter");
            selectEditor.setAsText("02");
        }
        catch (ProgrammationException e)
        {
            MessageFormat msgFormat = new MessageFormat(SelectEditor.INVALID_PROPERTY);
            Object[] attr = {"codeWithoutGetter", tb1.getClass().getName()};

            Assert.assertEquals(e.getMessage(), msgFormat.format(attr));
        }
    }

    /**
     * methode Test select in list wth prop set as text no ok : --.
     */
    @Test
    public void testSelectInListWthPropSetAsTextNoOk()
    {
        PropertyEditor selectEditor = new SelectEditor(listTestBeanWithoutProperty, null);
        selectEditor.setAsText("03");
        Assert.assertNotSame(this.tbstring2, selectEditor.getValue());
    }

    /**
     * methode Test select in list wth prop set as text ok : --.
     */
    @Test
    public void testSelectInListWthPropSetAsTextOk()
    {
        PropertyEditor selectEditor = new SelectEditor(listTestBeanWithoutProperty, null);
        selectEditor.setAsText("02");
        Assert.assertEquals(this.tbstring2, selectEditor.getValue());
    }

    /**
     * methode Test select in list wth set as text not ok : --.
     */
    @Test
    public void testSelectInListWthSetAsTextNotOk()
    {
        PropertyEditor selectEditor = new SelectEditor(listTestBeanWithoutProperty, null);
        selectEditor.setAsText("02");
        Assert.assertNotSame(this.tbstring3, selectEditor.getValue());
    }

    /**
     * methode Test select in map set as text not ok : --.
     */
    @Test
    public void testSelectInMapSetAsTextNotOk()
    {
        PropertyEditor selectEditor = new SelectEditor(mapTestBean, "code");
        selectEditor.setAsText("02");
        Assert.assertNotSame(this.tb1, selectEditor.getValue());
    }

    /**
     * methode Test select in map set as text null identifiant : --.
     */
    @Test
    public void testSelectInMapSetAsTextNullIdentifiant()
    {
        PropertyEditor selectEditor = new SelectEditor(mapTestBean, "code");
        selectEditor.setAsText(null);
        Assert.assertEquals(null, selectEditor.getValue());
    }

    /**
     * methode Test select in map set as text null ok : --.
     */
    @Test
    public void testSelectInMapSetAsTextNullOk()
    {
        PropertyEditor selectEditor = new SelectEditor(mapTestBean, "code");
        selectEditor.setAsText("1000");
        Assert.assertEquals(null, selectEditor.getValue());
    }

    /**
     * methode Test select in map set as text ok : --.
     */
    @Test
    public void testSelectInMapSetAsTextOk()
    {
        PropertyEditor selectEditor = new SelectEditor(mapTestBean, "code");
        selectEditor.setAsText("02");
        Assert.assertEquals(this.tb2, selectEditor.getValue());
    }

    /**
     * methode Test select in set get as text no ok : --.
     */
    @Test
    public void testSelectInSetGetAsTextNoOk()
    {
        PropertyEditor selectEditor = new SelectEditor(setTestBean, "code");
        selectEditor.setValue(this.tb3);
        Assert.assertNotSame("02", selectEditor.getAsText());
    }

    /**
     * methode Test select in set get as text null ok : --.
     */
    @Test
    public void testSelectInSetGetAsTextNullOk()
    {
        PropertyEditor selectEditor = new SelectEditor(setTestBean, "code");
        selectEditor.setValue(null);
        Assert.assertEquals(SelectEditor.NULL_VALUE, selectEditor.getAsText());
    }

    // Test de getAstext()
    /**
     * methode Test select in set get as text ok : --.
     */
    @Test
    public void testSelectInSetGetAsTextOk()
    {
        PropertyEditor selectEditor = new SelectEditor(setTestBean, "code");
        selectEditor.setValue(this.tb2);
        Assert.assertEquals("02", selectEditor.getAsText());
    }

    /*
     * Tests utilisation de SelectEditor avec des Map
     */

    /**
     * methode Test select in set set as text not ok : --.
     */
    @Test
    public void testSelectInSetSetAsTextNotOk()
    {
        PropertyEditor selectEditor = new SelectEditor(setTestBean, "code");
        selectEditor.setAsText("02");
        Assert.assertNotSame(this.tb1, selectEditor.getValue());
    }

    /**
     * methode Test select in set set as text null identifiant : --.
     */
    @Test
    public void testSelectInSetSetAsTextNullIdentifiant()
    {
        PropertyEditor selectEditor = new SelectEditor(setTestBean, "code");
        selectEditor.setAsText(null);
        Assert.assertEquals(null, selectEditor.getValue());
    }

    /**
     * methode Test select in set set as text null ok : --.
     */
    @Test
    public void testSelectInSetSetAsTextNullOk()
    {
        PropertyEditor selectEditor = new SelectEditor(setTestBean, "code");
        selectEditor.setAsText("1000");
        Assert.assertEquals(null, selectEditor.getValue());
    }

    /*
     * Tests utilisation de SelectEditor avec des collections de type Set
     */
    // Test de setAsText()
    /**
     * methode Test select in set set as text ok : --.
     */
    @Test
    public void testSelectInSetSetAsTextOk()
    {
        PropertyEditor selectEditor = new SelectEditor(setTestBean, "code");
        selectEditor.setAsText("02");
        Assert.assertEquals(this.tb2, selectEditor.getValue());
    }
}
