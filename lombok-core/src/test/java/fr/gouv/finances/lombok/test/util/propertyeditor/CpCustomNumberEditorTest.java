/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : CpCustomNumberEditorTest.java
 *
 */
package fr.gouv.finances.lombok.test.util.propertyeditor;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import fr.gouv.finances.lombok.util.format.FormaterNombre;
import fr.gouv.finances.lombok.util.propertyeditor.CpCustomNumberEditor;
import junit.framework.TestCase;

/**
 * Class CpCustomNumberEditorTest --.
 * 
 * @version $Revision: 1.2 $ Date: 11 déc. 2009
 */
public class CpCustomNumberEditorTest extends TestCase
{

    /** log. */
    protected final Log log = LogFactory.getLog(getClass());

    /**
     * methode Test invalide format double : --.
     */
    public void testInvalideFormatDouble()
    {

        NumberFormat nf = NumberFormat.getInstance(Locale.FRANCE);
        nf.setGroupingUsed(true);
        nf.setMinimumFractionDigits(2);
        try
        {
            nf.parse("9991000,12");
        }
        catch (ParseException e)
        {
            //  Auto-generated catch block
            e.printStackTrace();
        }

        String[] invalidNumbert =
        {"9991000.12a", "a999  1 000.12", "999 1a  000. 12", "9991000,12E2", "999  1E 000,12",
                " 999   aaa1  000 , 12 "

        };

        CpCustomNumberEditor editor =
            new CpCustomNumberEditor(Double.class, FormaterNombre.getFormatNombreDeuxDecimales());

        for (int i = 0; i < invalidNumbert.length; i++)
        {
            String validNumberType = invalidNumbert[i];
            try
            {
                editor.setAsText(validNumberType);
                fail("Le texte " + validNumberType + "n'est pas un nombre");
            }
            catch (IllegalArgumentException ex)
            {
                ;
                assertNull(editor.getValue());
            }
        }
    }

    /**
     * methode Test null value : --.
     */
    public void testNullValue()
    {
        String[] validNumbert = {"", "  "};

        CpCustomNumberEditor editor =
            new CpCustomNumberEditor(Double.class, FormaterNombre.getFormatNombreDeuxDecimales());

        for (int i = 0; i < validNumbert.length; i++)
        {
            String validNumberType = validNumbert[i];
            try
            {
                editor.setAsText(validNumberType);

                if (editor.getValue() == null)
                {
                    log.info("Texte nombre : " + validNumberType + "; Nombre trouvé = null");
                }
                else
                {
                    fail("Le property editor devrait retourner une valeur nulle");
                }
            }
            catch (IllegalArgumentException ex)
            {
                fail("Aucune exception ne devrait être levée");
            }
        }
    }

    /**
     * methode Test valid format big decimal : --.
     */
    public void testValidFormatBigDecimal()
    {

        Number validNumber = null;
        NumberFormat nf = NumberFormat.getInstance(Locale.FRANCE);
        nf.setGroupingUsed(true);
        nf.setMinimumFractionDigits(2);
        validNumber = new BigDecimal("9000.12");

        String[] validNumbert = {"9000.12", "9 000.12", " 9  000. 12", "9000,12", "9 000,12", " 9  000 , 12 "

        };

        CpCustomNumberEditor editor =
            new CpCustomNumberEditor(BigDecimal.class, FormaterNombre.getFormatNombreDeuxDecimales());

        for (int i = 0; i < validNumbert.length; i++)
        {
            String validNumberType = validNumbert[i];
            try
            {
                editor.setAsText(validNumberType);

                if (editor.getValue() != null)
                {
                    log.info("Texte nombre : " + validNumberType + "; Nombre trouvé = " + nf.format(editor.getValue()));
                    assertEquals(validNumber, editor.getValue());
                }
                else
                {
                    log.info("Texte nombre : " + validNumberType + "; Nombre trouvé = null ");
                }
            }
            catch (IllegalArgumentException ex)
            {
                fail("Aucune exception ne devrait être levée, ce nombre (" + validNumberType + ") est valide");
            }
        }
    }

    /**
     * methode Test valid format double : --.
     */
    public void testValidFormatDouble()
    {

        Number validNumber = null;
        NumberFormat nf = NumberFormat.getInstance(Locale.FRANCE);
        nf.setGroupingUsed(true);
        nf.setMinimumFractionDigits(2);
        try
        {
            validNumber = nf.parse("9991000,12");
        }
        catch (ParseException e)
        {
             //  Auto-generated catch block
            e.printStackTrace();
        }

        String[] validNumbert =
        {"9991000.12", "999  1 000.12", "999 1  000. 12", "9991000,12", "999  1 000,12", " 999   1  000 , 12 "

        };

        CpCustomNumberEditor editor =
            new CpCustomNumberEditor(Double.class, FormaterNombre.getFormatNombreDeuxDecimales());

        for (int i = 0; i < validNumbert.length; i++)
        {
            String validNumberType = validNumbert[i];
            try
            {
                editor.setAsText(validNumberType);

                if (editor.getValue() != null)
                {
                    log.info("Texte nombre : " + validNumberType + "; Nombre trouvé = " + nf.format(editor.getValue()));
                    assertEquals(validNumber, editor.getValue());
                }
                else
                {
                    log.info("Texte nombre : " + validNumberType + "; Nombre trouvé = null ");
                }
            }
            catch (IllegalArgumentException ex)
            {
                fail("Aucune exception ne devrait être levée, ce nombre (" + validNumberType + ") est valide");
            }
        }
    }

    /**
     * methode Test zero value : --.
     */
    public void testZeroValue()
    {
        Number validNumber = new Double("0");
        String[] validNumbert = {"", "  "};

        CpCustomNumberEditor editor =
            new CpCustomNumberEditor(Double.class, FormaterNombre.getFormatNombreDeuxDecimales(),
                CpCustomNumberEditor.ZERO);

        for (int i = 0; i < validNumbert.length; i++)
        {
            String validNumberType = validNumbert[i];
            try
            {
                editor.setAsText(validNumberType);

                if (editor.getValue() != null)
                {
                    log.info("Texte nombre : " + validNumberType + "; Nombre trouvé = "
                        + FormaterNombre.getFormatNombreDeuxDecimales().format(editor.getValue()));
                    assertEquals(validNumber, editor.getValue());
                }
                else
                {
                    log.info("Texte nombre : " + validNumberType + "; Nombre trouvé = null ");
                }
            }
            catch (IllegalArgumentException ex)
            {
                fail("Aucune exception ne devrait être levée, ce nombre (" + validNumberType + ") est valide");
            }
        }
    }

}
