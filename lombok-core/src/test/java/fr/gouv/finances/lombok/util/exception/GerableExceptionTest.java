/*
 * Copyright (c) 2014 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.util.exception;

import fr.gouv.finances.lombok.test.pojo.AbstractCorePojoTest;

/**
 * Tests unitaires de l'exception GerableException
 *
 * @author Christophe Breheret-Girardin
 */
public class GerableExceptionTest extends AbstractCorePojoTest<GerableException>
{

}
