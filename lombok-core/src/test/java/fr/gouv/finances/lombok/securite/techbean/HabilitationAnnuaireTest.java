/*
 * Copyright (c) 2014 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.securite.techbean;

import fr.gouv.finances.lombok.test.pojo.AbstractCorePojoTest;

/**
 * Tests unitaires du POJO HabilitationAnnuaire
 *
 * @author Christophe Breheret-Girardin
 */
public class HabilitationAnnuaireTest extends AbstractCorePojoTest<HabilitationAnnuaire>
{

}
