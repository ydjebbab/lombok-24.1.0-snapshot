/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 *
 */
package fr.gouv.finances.lombok.test.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionTemplate;

import fr.gouv.finances.lombok.test.controle.ControleDonnees;
import fr.gouv.finances.lombok.test.controle.VerificationExecution;
import fr.gouv.finances.lombok.util.base.PurgeTablesDao;
import fr.gouv.finances.lombok.util.persistance.ScrollIterator;

/**
 * Socle permettant de tester le socle DAO quelle que soit l'implémentation choisie.
 *
 * @author Christophe Breheret-Girardin
 */
public abstract class ClasseDaoCoreTest
{
    /** Déclaration du DAO de gestion des purges de la base. */
    @Autowired
    private PurgeTablesDao purgetablesdao;

    /** Déclaration du DAO de gestion des données de la classe. */
    @Autowired
    protected ClasseDao classedao;

    /** Déclaration du template de transaction. */
    @Autowired
    private TransactionTemplate transactionTemplate;
    
    /** Map des classes persistées avant chaque test. */
    protected Map<Integer, Classe> mapClasse;

    /** Map des élèves persistées avant chaque test. */
    protected Map<Integer, Eleve> mapEleve;

    /** Constante d'un nouveau libellé de classe. */
    private final String NOUVEAU_LIBELLE = "libellé 42";

    /** Constante d'un 1er nom d'établisssement. */
    protected static final String ETABLISSEMENT_1 = "établissement 1";

    /** Constante d'un 2ème nom d'établisssement. */
    protected static final String ETABLISSEMENT_2 = "établissement 2";

    /**
     * Initialisation des données en base avant chaque test.
     */
    @Before
    public void creerDonnees()
    {
        // Purge des données de la base
        purgetablesdao.purgerTables("Eleve", "Classe");

        // Vérification de l'absence de données suite à la purge
        ControleDonnees.verifierElements(classedao.loadAllObjects(Classe.class));
        ControleDonnees.verifierElements(classedao.loadAllObjects(Eleve.class));

        // Génération des éléments, insertion en base de données et sauvegarde dans une Map
        mapClasse = IntStream.rangeClosed(1, 5).mapToObj(this::getClasse)
            .peek(classedao::saveObject)
            .collect(Collectors.toMap(c -> Integer.parseInt(c.getLibelle()), c -> c));

        // Récupération de tous les élèves de la base de données et
        // transformation de la liste en Map pour avoir une image mémoire
        mapEleve = classedao.loadAllObjects(Eleve.class).stream().collect(Collectors.toMap(
            eleve -> Integer.parseInt(eleve.getNom()), eleve -> eleve));

        // Vérification de la présence des données avant exécution du test
        ControleDonnees.verifierElements(classedao.loadAllObjects(Classe.class)
            , mapClasse.get(1), mapClasse.get(2), mapClasse.get(3), mapClasse.get(4), mapClasse.get(5));
        assertEquals("Nombre d'élève", 14, mapEleve.size());

        // Suppression du contexte de persistence pour ne pas utiliser le cache et forcer les requêtes
        classedao.clearPersistenceContext();
    }

    /**
     * Test de l'injection du DAO.
     */
    @Test
    public final void injectionDao()
    {
        // Vérification
        assertNotNull("DAO non injecté dans le test", classedao);
    }

    /**
     * Test de fr.gouv.finances.lombok.util.base.CoreBaseDao#loadAllObjects().
     */
    public final void loadAllObjects()
    {
        // Chargement de toutes les classes et vérification
        ControleDonnees.verifierElements(classedao.loadAllObjects(Classe.class)
            , mapClasse.get(1), mapClasse.get(2), mapClasse.get(3), mapClasse.get(4), mapClasse.get(5));
    }

    /**
     * Test de fr.gouv.finances.lombok.util.base.CoreBaseDao#getObject(java.lang.Class, java.io.Serializable).
     */
    @Test
    public final void getObject()
    {
        // Vérification de la récupèration de chaque classe de la base de données
        IntStream.rangeClosed(1, 5).forEach(i -> assertEquals("Classe d'élève " + i
                , mapClasse.get(i), classedao.getObject(Classe.class, mapClasse.get(i).getIdentifiant())));
    }

    /**
     * Test de fr.gouv.finances.lombok.util.base.CoreBaseDao#saveObject(java.lang.Object).
     */
    @Test
    public final void saveObject()
    {
        // Sauvegarde d'une nouvelle classe
        Classe classe6 = getClasse(6);
        classedao.saveObject(classe6);
        
        // Suppression du contexte de persistence pour ne pas utiliser le cache et forcer les requêtes
        classedao.clearPersistenceContext();
        
        // Verification de l'insertion
        ControleDonnees.verifierElements(classedao.loadAllObjects(Classe.class)
            , classe6, mapClasse.get(1), mapClasse.get(2), mapClasse.get(3), mapClasse.get(4), mapClasse.get(5));
    }

    /**
     * Test de fr.gouv.finances.lombok.util.base.CoreBaseDao#deleteObject(java.lang.Class, java.io.Serializable).
     */
    @Test
    public final void deleteObjectById()
    {
        // Suppression de la classe #1 et vérification
        classedao.deleteObject(Classe.class, mapClasse.get(1).getIdentifiant());
        ControleDonnees.verifierElements(classedao.loadAllObjects(Classe.class)
            , mapClasse.get(2), mapClasse.get(3), mapClasse.get(4), mapClasse.get(5));

        // Suppression de la classe #4 et vérification
        classedao.deleteObject(Classe.class, mapClasse.get(4).getIdentifiant());
        ControleDonnees.verifierElements(classedao.loadAllObjects(Classe.class)
            , mapClasse.get(2), mapClasse.get(3), mapClasse.get(5));
    }

    /**
     * Test de fr.gouv.finances.lombok.util.base.CoreBaseDao#deleteObject(java.lang.Ojbect).
     */
    @Test
    public final void deleteObject()
    {
        // Suppression de la classe #1 et vérification
        classedao.deleteObject(mapClasse.get(1));
        ControleDonnees.verifierElements(classedao.loadAllObjects(Classe.class)
            , mapClasse.get(2), mapClasse.get(3), mapClasse.get(4), mapClasse.get(5));

        // Suppression de la classe #4 et vérification
        classedao.deleteObject(mapClasse.get(4));
        ControleDonnees.verifierElements(classedao.loadAllObjects(Classe.class)
            , mapClasse.get(2), mapClasse.get(3), mapClasse.get(5));
    }

    /**
     * Test de fr.gouv.finances.lombok.util.base.CoreBaseDao#modifyObject(java.lang.Object).
     */
    @Test
    public final void modifyObject()
    {
        // Chargement de la classe #2
        Classe classe2 = mapClasse.get(2);

        // Modification  du libellé et modification en base de données
        classe2.setLibelle("Libellé");
        classedao.modifyObject(classe2);

        // Vérification des modifications
        ControleDonnees.verifierElements(classedao.loadAllObjects(Classe.class)
            , mapClasse.get(1), classe2, mapClasse.get(3), mapClasse.get(4), mapClasse.get(5));
    }

    /**
     * Test de fr.gouv.finances.lombok.util.base.CoreBaseDao#refresh(java.lang.Object).
     */
    @Test
    @Transactional
    public final void refresh()
    {
        // Chargement de la classe #2
        Classe classe2 = classedao.getObject(Classe.class, mapClasse.get(2).getIdentifiant());
        
        // Modification  du libellé
        classe2.setLibelle(NOUVEAU_LIBELLE);
        // L'objet initial n'est plus identique à celui en base de données
        assertNotEquals(classe2.getLibelle(), mapClasse.get(2).getLibelle());

        // Raffraichissement de la classe à partir de la base de données
        classedao.refresh(classe2);
        // Les modifications ont été écrasées, l'objet est identique
        assertEquals(classe2.getLibelle(), mapClasse.get(2).getLibelle());
    }

//    @Test
    @Transactional
    public final void modificationCascade()
    {
        // Chargement de la classe #2
        Classe classe2 = classedao.getObject(Classe.class, mapClasse.get(2).getIdentifiant());
        // Chargement des élèves de la classe
        Set<Eleve> eleves = classe2.getEleves();
        Eleve eleve = eleves.iterator().next();
        // Contrôle du 1er élève
        assertEquals("prenom 20", eleve.getPrenom());

        // Modification du prénom et mise à jour en base de données de la classe (et l'élève en cascade)
        eleve.setPrenom("cbre");
        classedao.modifyObject(classe2);

        // Suppression du contexte de persistence pour ne pas utiliser le cache et forcer les requêtes
        classedao.clearPersistenceContext();

        // Vérification des modifications sur l'élève
        classe2 = classedao.getObject(Classe.class, mapClasse.get(2).getIdentifiant());
        assertEquals("cbre", classe2.getEleves().iterator().next().getPrenom());
    }

    /**
     * Méthode permettant de vérifier que la synchronisation de la base s'effectue au moment du commit.
     */
    @Test
    public final void flushOnCommit()
    {
        // Utilisation d'une transaction manuelle pour maitriser les synchronisations avec la base de données
        Classe classe3 = transactionTemplate.execute(status ->
            {
                // Chargement de la classe #3
                Classe classe = classedao.getObject(Classe.class, mapClasse.get(3).getIdentifiant());

                // Modification du libellé
                classe.setLibelle(NOUVEAU_LIBELLE);

                return classe;
            });

        // Vérification que la modification du libellé a été effectuée au moment du commit
        assertEquals(NOUVEAU_LIBELLE, classedao.getObject(Classe.class, classe3.getIdentifiant()).getLibelle());
    }

    /**
     * Méthode permettant de vérifier la méthode
     * fr.gouv.finances.lombok.util.base.CoreBaseDao#evictObject(java.lang.Object).
     * Même traitement que le test "flushOnCommit", en rajoutant une instruction "evictObject".
     */
    @Test
    public final void evictObject()
    {
        // Utilisation d'une transaction manunelle pour maitriser les synchronisations avec la base de données
        Classe classe3 = transactionTemplate.execute(status ->
            {
                // Chargement de la classe #3
                Classe classe = classedao.getObject(Classe.class, mapClasse.get(3).getIdentifiant());

                // L'objet est évincé du cache
                classedao.evictObject(classe);

                // Modification du libellé
                classe.setLibelle(NOUVEAU_LIBELLE);

                return classe;
            });
        
        // Vérification que la synchronisation de la base, au moment du commit, ne s'effectue pas
        // car l'objet a été évincé du cache
        assertEquals("3", classedao.getObject(Classe.class, classe3.getIdentifiant()).getLibelle());
    }

    /**
     * Méthode permettant de vérifier que la synchronisation de la base s'effectue au moment du flush.
     * Même traitement que le test "evictObject", en rajoutant une instruction "flush".
     * La classe est transactionnelle pour maitriser les synchronisations avec la base de données.
     */
    @Test
    @Transactional
    public final void flush()
    {
        // Chargement de la classe #3
        Classe classe = classedao.getObject(Classe.class, mapClasse.get(3).getIdentifiant());

        // L'objet est évincé du cache
        classedao.evictObject(classe);

        // Modification du libellé
        classe.setLibelle(NOUVEAU_LIBELLE);
          
        // Vérification que la modification du libellé est bien différent avec la base de données
        assertNotEquals(classe.getLibelle()
            , classedao.getObject(Classe.class, classe.getIdentifiant()).getLibelle());

        // Modification de la classe en base de données, l'objet est ré-associée au contexte de persistance
        classedao.modifyObject(classe);
        classedao.flush();

        // L'objet est de nouveau évincé du cache
        classedao.evictObject(classe);

        // Suppression du contexte de persistence pour ne pas utiliser le cache et forcer les requêtes
        classedao.clearPersistenceContext();
        
        // Vérification que le libellé a bien été modifié en base de données
        assertEquals(classe.getLibelle()
            , classedao.getObject(Classe.class, classe.getIdentifiant()).getLibelle());
    }
    
    /**
     * Méthode permettant de vérifier l'utilisation d'un itérateur en mode curseur,
     * avec une transaction sur la méthode.
     */
    @Test
    @Transactional
    public final void getScrollIterator()
    {
        // Récupération d'un itérateur en mode curseur, sur toutes les classes de la base de données
        ScrollIterator scrollIterator = classedao.getScrollIterator(100);

        // Parcours de l'itérateur pour récupérer toutes les classes
        List<Classe> classes = new ArrayList<>();
        while (scrollIterator.hasNext())
        {
            classes.add((Classe) scrollIterator.nextObjetMetier());
        }

        // Fermeture de l'itérateur
        scrollIterator.close();
        // Vérification de la fermeture
        assertTrue(scrollIterator.isClosed());

        // Vérification des éléments récupérés
        ControleDonnees.verifierElements(classes
            , mapClasse.get(1), mapClasse.get(2), mapClasse.get(3), mapClasse.get(4), mapClasse.get(5));
    }
    
    /**
     * Méthode permettant de vérifier l'utilisation d'un itérateur en mode curseur,
     * avec une transaction manuelle.
     */
    @Test
    public final void getScrollIteratorTransactionExplicite()
    {
        // Récupération d'un itérateur en mode curseur, sur toutes les classes de la base de données,
        // à partir d'une transaction explicite
        List<Classe> classes = transactionTemplate.execute(status -> 
            {
                // Récupération d'un itérateur en mode curseur, sur toutes les classes de la base de données
                ScrollIterator scrollIterator = classedao.getScrollIterator(0);

                // Parcours de l'itérateur pour récupérer toutes les classes
                List<Classe> classesScrollIterator = new ArrayList<>();
                while (scrollIterator.hasNext())
                {
                    classesScrollIterator.add((Classe) scrollIterator.nextObjetMetier());
                }

                // Fermeture de l'itérateur
                scrollIterator.close();
                // Vérification de la fermeture
                assertTrue(scrollIterator.isClosed());
                
                return classesScrollIterator;
            });

        // Vérification des éléments récupérés
        ControleDonnees.verifierElements(classes
            , mapClasse.get(1), mapClasse.get(2), mapClasse.get(3), mapClasse.get(4), mapClasse.get(5));
    }

    /**
     * Méthode permettant de vérifier la méthode
     * fr.gouv.finances.lombok.util.persistance.ScrollIterator#close().
     */
    @Test
    public final void getScrollIteratorClose()
    {
        // Récupération d'un itérateur en mode curseur, sur toutes les classes de la base de données,
        // à partir d'une transaction explicite
        List<Classe> classes = transactionTemplate.execute(status -> 
            {
                // Récupération d'un itérateur en mode curseur, sur toutes les classes de la base de données
                ScrollIterator scrollIterator = classedao.getScrollIterator(0);

                // Récupération du 1er élément
                List<Classe> classesScrollIterator = new ArrayList<>();
                classesScrollIterator.add((Classe) scrollIterator.nextObjetMetier());

                // Fermeture de l'itérateur
                scrollIterator.close();
                // Vérification de la fermeture
                assertTrue(scrollIterator.isClosed());

                return classesScrollIterator;
            });

        // Vérification des éléments récupérés
        ControleDonnees.verifierElements(classes, mapClasse.get(1));
    }

    /**
     * Méthode permettant de vérifier la méthode
     * fr.gouv.finances.lombok.util.persistance.ScrollIterator#remove().
     */
    @SuppressWarnings("deprecation")
    @Test
    public final void removeScrollIterator()
    {
        // Exécution de la méthode à vérifier
        VerificationExecution.verifierException(UnsupportedOperationException.class
            , () -> classedao.getScrollIterator(0).remove());
    }

    /**
     * Méthode permettant de vérifier la méthode
     * fr.gouv.finances.lombok.util.persistance.ScrollIterator#next().
     */
    @Test
    @Transactional
    public final void notNextScrollIterator()
    {
        // Récupération d'un itérateur en mode curseur, sur toutes les classes de la base de données
        ScrollIterator scrollIterator = classedao.getScrollIterator(0);

        // Parcours de l'itérateur pour récupérer toutes les classes
        List<Classe> classesScrollIterator = new ArrayList<>();
        while (scrollIterator.hasNext())
        {
            classesScrollIterator.add((Classe) scrollIterator.nextObjetMetier());
        }

        // Appel du prochain élément alors qu'il n'y en a plus, et vérification de l'exception attendue
        VerificationExecution.verifierException(NoSuchElementException.class
            , () -> scrollIterator.next());
    }

    /**
     * Méthode permettant de vérifier que le paramétrage de la récupération d'un nombre maximum
     * d'élément est étanche entre deux recherches.
     */
    @Test
    @Transactional
    public void getMaxEntite()
    {
        // Toutes les opérations sont dans une même transaction pour montrer que le paramétrage n'est pas
        // convervé entre chaque opération

        // Récupération de toutes les classes = 5
        verifierClassesSansEleve(recupererTout()
            , mapClasse.get(1), mapClasse.get(2), mapClasse.get(3), mapClasse.get(4), mapClasse.get(5)); 

        // Récupération des 2 premières classes (avec un paramétrage étanche)
        verifierClassesSansEleve(classedao.getMaxClassesEtanche(2), mapClasse.get(1), mapClasse.get(2));

        // Récupération de toutes les classes = 5 (paramétrage non conservé)
        verifierClassesSansEleve(recupererTout()
           , mapClasse.get(1), mapClasse.get(2), mapClasse.get(3), mapClasse.get(4), mapClasse.get(5)); 
    }

    /**
     * Méthode permettant de vérifier que le paramétrage de la récupération d'un nombre maximum
     * d'élément est étanche entre deux recherches.
     */
    @Test
    public void maxClassesContextePersistance()
    {
        // Récupération de tous les éléments
        verifierClassesSansEleve(recupererTout(), mapClasse.get(1), mapClasse.get(2), mapClasse.get(3)
            , mapClasse.get(4), mapClasse.get(5));

        // Récupération des deux premiers éléments et vérifications
        List<Classe> classes = getTransactionTemplate().execute(status ->
            classedao.getMaxClassesViaContextePersistance(2));
        verifierClassesSansEleve(classes, mapClasse.get(1), mapClasse.get(2));

        // Récupération de tous les éléments
        verifierClassesSansEleve(recupererTout(), mapClasse.get(1), mapClasse.get(2), mapClasse.get(3)
            , mapClasse.get(4), mapClasse.get(5));
    }

    /**
     * Méthode permettant de vérifier le mode "lazy" sur une classe.
     * @param classe classe à vérifier
     * @param classeAttendue classe attendue
     */
    protected void verifierClasseSansEleve(Classe classe, Classe classeAttendue)
    {
        // Vérification de la classe
        assertEquals(classe, classeAttendue);

        // Vérification de l'absence d'élève
        assertFalse(classedao.isCharged(classe.getEleves()));
    }

    /**
     * Méthode permettant de vérifier le mode "lazy" sur une liste de classe.
     * @param classes classes à vérifier
     * @param classesAttendues classes attendues
     */
    protected void verifierClassesSansEleve(List<Classe> classes, Classe... classesAttendues)
    {
        // Vérification de la classe
        ControleDonnees.verifierElements(classes, classesAttendues);

        // Vérification de l'absence d'élève
        classes.stream().forEach(classe -> assertFalse(classedao.isCharged(classe.getEleves())));
    }

    /**
     * Méthode permettant de vérifier le bon chargement des éléves d'une liste de classe.
     * @param classe classes d'élève à vérifier
     * @param classesAttendues classes d'élève attendues
     */
    protected void verifierClassesAvecEleves(List<Classe> classes, Classe... classesAttendues)
    {
        // Vérification de la classe
        ControleDonnees.verifierElements(classes, classesAttendues);

        // Vérification des élèves de la classe
        classes.stream().forEach(classe -> verifierEleves(classe));
    }

    /**
     * Méthode permettant de vérifier le bon chargement des éléves d'une seule classe.
     * @param classe classe d'élève à vérifier
     * @param classe classe d'élève attendue
     */
    protected void verifierClasseAvecEleves(Classe classe, Classe classeAttendue)
    {
        // Vérification de la classe
        assertEquals(classe, classeAttendue);

        // Vérification des élèves de la classe
        verifierEleves(classe);
    }

    /**
     * Méthode permettant de vérifier les données des élèves.
     * @param classe classe d'élève à vérifier
     */
    private void verifierEleves(Classe classe)
    {
        assertTrue(classedao.isCharged(classe.getEleves()));
        switch (Integer.parseInt(classe.getLibelle()))
        {
            case 1 :
                ControleDonnees.verifierElements(classe.getEleves(), mapEleve.get(10), mapEleve.get(11));
                break;
            case 2 :
                ControleDonnees.verifierElements(classe.getEleves(), mapEleve.get(20), mapEleve.get(21)
                    , mapEleve.get(22));
                break;
            case 3 :
                ControleDonnees.verifierElements(classe.getEleves(), mapEleve.get(30), mapEleve.get(31)
                    , mapEleve.get(32), mapEleve.get(33));
                break;
            case 4 :
                ControleDonnees.verifierElements(classe.getEleves(), mapEleve.get(40), mapEleve.get(41)
                    , mapEleve.get(42), mapEleve.get(43), mapEleve.get(44));
                break;
            default :
                ControleDonnees.verifierElements(classe.getEleves());
        }
    }
    
    /**
     * Méthode permettat de récupérer toutes les classes
     *
     * @return toutes les classes
     */
    protected List<Classe> recupererTout()
    {
        return getTransactionTemplate().execute(status -> classedao.getClasses());
    }

    /**
     * Méthode permettant de générer un élève, dont le nom est l'indice fourni.<br/>
     *
     * @param indice indice permettant de différencier les élèves
     * @return l'élève généré
     */
    public Eleve getEleve(int indice)
    {
        Eleve eleve = new Eleve();
        eleve.setNom(String.valueOf(indice));
        eleve.setPrenom("prenom " + indice);
        return eleve;
    }

    /**
     * Méthode permettant de générer une classe, dont le libellé est l'indice fourni.<br/>
     * <br/>
     * Le nombre d'élèves est fonction de l'indice de la classe : <br/>
     * <code>
     *  classe 1 : 11 élèves<br/>
     *  classe 2 : 22 élèves<br/>
     *  classe 3 : 33 élèves<br/>
     *  classe 4 : 44 élèves<br/>
     *  classe 5 : aucun élève<br/>
     *  classe 6 : 66 élèves<br/>
     *  etc.<br/>
     * </code>
     * <br/>
     * Le nom de l'établissement est fonction de l'indice de la classe : <br/>
     * <code>
     *  classe 1 à 2 : établissement 1<br/>
     *  classe 3 : aucun établissement
     *  classe 4 et plus : établissement 2
     * </code>
     * @param indiceClasse indice permettant de différencier les classes
     * @return la classe générée
     */
    private Classe getClasse(int indiceClasse)
    {
        Classe classe = new Classe();
        classe.setLibelle(String.valueOf(indiceClasse));
        classe.setEleves(new HashSet<>());

        // Création et affectation des élèves aux classes
        if (indiceClasse != 5)
        {
            IntStream.rangeClosed(indiceClasse * 10, (indiceClasse * 10) + indiceClasse).forEach(
                indiceEleve -> classe.addEleve(getEleve(indiceEleve)));
        }

        // Initialisation du nom d'établissement
        if (indiceClasse <= 2)
        {
            classe.setEtablissement(ETABLISSEMENT_1);
        }
        else if (indiceClasse > 3)
        {
            classe.setEtablissement(ETABLISSEMENT_2);
        }

        return classe;
    }

    /**
     * Accesseur de transactionTemplate.
     *
     * @return transactionTemplate
     */
    public TransactionTemplate getTransactionTemplate()
    {
        return transactionTemplate;
    }

}