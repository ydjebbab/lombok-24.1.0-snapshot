/*
 * Copyright (c) 2016 DGFiP - Tous droits réservés
 * 
 */
package fr.gouv.finances.lombok.mel.techbean;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import fr.gouv.finances.lombok.test.pojo.AbstractCorePojoTest;

/**
 * Tests unitaires du POJO Mel.
 * 
 * @author Christophe Breheret-Girardin
 */
public class MelTest extends AbstractCorePojoTest<Mel>
{   
    /**
     * Constructeur permettant d'initialiser des valeurs préfabriquées utilisées automatiquement dans les tests.
     *
     * @throws AddressException Exception levée en cas de construction d'adresse erronée
     */
    public MelTest() throws AddressException
    {
        // Ajout d'une valeur par défaut pour les paramètres de type InternetAddress
        InternetAddress internetAddress = new InternetAddress("titi.titi@titi.com");
        addPrefabValues(InternetAddress.class, internetAddress);
    }
}
