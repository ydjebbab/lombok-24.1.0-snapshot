/*
 * Copyright (c) 2014 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.parsertexte.parametrage;

import fr.gouv.finances.lombok.test.pojo.AbstractCorePojoTest;

/**
 * Tests unitaires du POJO ProprieteLigne
 *
 * @author Christophe Breheret-Girardin
 */
public class ProprieteLigneTest extends AbstractCorePojoTest<ProprieteLigne>
{

}
