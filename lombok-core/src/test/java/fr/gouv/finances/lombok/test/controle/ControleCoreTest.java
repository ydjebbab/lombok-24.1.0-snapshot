/*
 * Copyright (c) 2016 DGFiP - Tous droits réservés
 * 
 */
package fr.gouv.finances.lombok.test.controle;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

/**
 * Tests unitaire de l'utilitaire de contrôle d'éléments.
 * @see fr.gouv.finances.lombok.test.controle.ControleDonnees
 *
 * @author Christophe Breheret-Girardin
 */
public class ControleCoreTest
{

    /**
     * Test de la méthode fr.gouv.finances.lombok.test.controle.ControleDonnees#verifierElements(
     * java.util.Collection, java.util.Arrays).
     */
    @Test
    public void verifierElements()
    {
        List<Element> elements = new ArrayList<>();
        elements.add(getElement(1L));
        elements.add(getElement(2L));

        // Vérification OK
        ControleDonnees.verifierElements(elements, getElement(1L), getElement(2L));

        // Vérification KO
        VerificationExecution.verifierException(AssertionError.class
            , () -> ControleDonnees.verifierElements(elements, getElement(3L), getElement(4L)));
    }

    /**
     * Test de la méthode fr.gouv.finances.lombok.test.controle.ControleDonnees#verifierElements(
     * java.util.Collection, boolean, java.util.Arrays).
     */
    @Test
    public void verifierElementsTries()
    {
        List<Element> elements = new ArrayList<>();
        elements.add(getElement(1L));
        elements.add(getElement(2L));

        // Vérification OK
        ControleDonnees.verifierElements(elements, true, getElement(1L), getElement(2L));

        // Vérification KO
        VerificationExecution.verifierException(AssertionError.class
            , () -> ControleDonnees.verifierElements(elements, true, getElement(2L), getElement(1L)));
    }

    /**
     * Méthode permettant de générer un élément
     *
     * @param indice indice permettant de rendre dynamique un élément
     * @return un élément généré
     */
    private Element getElement(Long indice)
    {
        Element element = new Element();
        element.setAttributLong(indice);
        element.setAttributChaine("chaine " + indice);
        return element;
    }

}