/*
 * Copyright (c) 2016 DGFiP - Tous droits réservés
 * 
 */
package fr.gouv.finances.lombok.mel.techbean;

import java.io.InputStream;

import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;

import fr.gouv.finances.lombok.test.pojo.AbstractCorePojoTest;

/**
 * Tests unitaires du POJO PieceJointeInline.
 * 
 * @author Christophe Breheret-Girardin
 */
public class PieceJointeInlineTest extends AbstractCorePojoTest<PieceJointeInline>
{   
    /**
     * Constructeur permettant d'initialiser des valeurs préfabriquées utilisées automatiquement dans les tests.
     */
    public PieceJointeInlineTest()
    {
        // Ajout d'une valeur par défaut pour les paramètres de type Resource
        InputStream inputStream
            = this.getClass().getResourceAsStream(this.getClass().getSimpleName() + ".class");
        addPrefabValues(Resource.class, new InputStreamResource(inputStream));
    }
}
