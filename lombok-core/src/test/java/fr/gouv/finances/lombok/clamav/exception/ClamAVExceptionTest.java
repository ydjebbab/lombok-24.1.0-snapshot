/*
 * Copyright (c) 2014 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.clamav.exception;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.IntStream;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Tests unitaire de l'exception liée au service clamAv.
 *
 * @author Christophe Breheret-Girardin
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class ClamAVExceptionTest
{
    /** Code utilisé dans les tests */
    public static final String CODE = "code";

    /** Arguments utilisés dans les tests */
    public static final String[] ARGS = new String[] {"arg1", "arg2"};

    /** Message d'exception utilisé dans les tests */
    public static final String MSG = "message";

    /** Exception dans les tests */
    public static final IOException EXCEPTION = new IOException(MSG);

    /**
     * Test du 1er constructeur.
     */
    @Test
    public void testConstructeur1()
    {
        ClamAVException clamAvException = new ClamAVException(null);
        assertNull(clamAvException.getCode());
        testNoArgs(clamAvException);
        testNoException(clamAvException);
        
        clamAvException = new ClamAVException("");
        assertEquals("", clamAvException.getCode());
        testNoArgs(clamAvException);
        testNoException(clamAvException);

        clamAvException = new ClamAVException(CODE);
        assertEquals(CODE, clamAvException.getCode());
        testNoArgs(clamAvException);
        testNoException(clamAvException);
    }

    /**
     * Test du 2ème constructeur.
     */
    @Test
    public void testConstructeur2()
    {
        ClamAVException clamAvException = new ClamAVException(null, (String[]) null);
        assertNull(clamAvException.getCode());
        testNoArgs(clamAvException);
        testNoException(clamAvException);

        clamAvException = new ClamAVException(null, ARGS);
        assertNull(clamAvException.getCode());
        testArgs(ARGS, clamAvException);
        testNoException(clamAvException);

        clamAvException = new ClamAVException(CODE, ARGS);
        assertEquals(CODE, clamAvException.getCode());
        testArgs(ARGS, clamAvException);
        testNoException(clamAvException);
    }

    /**
     * Test du 3ème constructeur.
     */
    @Test
    public void testConstructeur3()
    {
        ClamAVException clamAvException = new ClamAVException(null, (ArrayList<String>) null);
        assertNull(clamAvException.getCode());
        testNoArgs(clamAvException);
        testNoException(clamAvException);

        clamAvException = new ClamAVException(null, Arrays.asList(ARGS));
        assertNull(clamAvException.getCode());
        testArgs(ARGS, clamAvException);
        testNoException(clamAvException);

        clamAvException = new ClamAVException(CODE, Arrays.asList(ARGS));
        assertEquals(CODE, clamAvException.getCode());
        testArgs(ARGS, clamAvException);
        testNoException(clamAvException);
    }

    /**
     * Test du 4ème constructeur.
     */
    @Test
    public void testConstructeur4()
    {
        ClamAVException clamAvException = new ClamAVException(null, (Throwable) null);
        assertNull(clamAvException.getCode());
        testNoArgs(clamAvException);
        testNoException(clamAvException);

        clamAvException = new ClamAVException(null, EXCEPTION);
        assertNull(clamAvException.getCode());
        testNoArgs(clamAvException);
        testException(EXCEPTION, clamAvException);

        clamAvException = new ClamAVException(CODE, EXCEPTION);
        assertEquals(CODE, clamAvException.getCode());
        assertTrue(clamAvException.getArgs().length == 0);
        assertTrue(clamAvException.getArgsList().isEmpty());
        testException(EXCEPTION, clamAvException);
    }

    /**
     * Test du 4ème constructeur.
     */
    @Test
    public void testConstructeur5()
    {
        ClamAVException clamAvException = new ClamAVException(null, (String[]) null, (Throwable) null);
        assertNull(clamAvException.getCode());
        testNoArgs(clamAvException);
        testNoException(clamAvException);

        clamAvException = new ClamAVException(null, (String[]) null, EXCEPTION);
        assertNull(clamAvException.getCode());
        testNoArgs(clamAvException);
        testException(EXCEPTION, clamAvException);
        
        clamAvException = new ClamAVException(null, ARGS, (Throwable) null);
        assertNull(clamAvException.getCode());
        testArgs(ARGS, clamAvException);
        testNoException(clamAvException);

        clamAvException = new ClamAVException(CODE, (String[]) null, (Throwable) null);
        assertEquals(CODE, clamAvException.getCode());
        testNoArgs(clamAvException);
        testNoException(clamAvException);
        
        clamAvException = new ClamAVException(null, ARGS, EXCEPTION);
        assertNull(clamAvException.getCode());
        testArgs(ARGS, clamAvException);
        testException(EXCEPTION, clamAvException);
        
        clamAvException = new ClamAVException(CODE, ARGS, (Throwable) null);
        assertEquals(CODE, clamAvException.getCode());
        testArgs(ARGS, clamAvException);
        testNoException(clamAvException);

        clamAvException = new ClamAVException(CODE, (String[]) null, EXCEPTION);
        assertEquals(CODE, clamAvException.getCode());
        testNoArgs(clamAvException);
        testException(EXCEPTION, clamAvException);
        
        clamAvException = new ClamAVException(CODE, ARGS, EXCEPTION);
        assertEquals(CODE, clamAvException.getCode());
        testArgs(ARGS, clamAvException);
        testException(EXCEPTION, clamAvException);
    }

    /**
     * Test du 5ème constructeur.
     */
    @Test
    public void testConstructeur6()
    {
        ClamAVException clamAvException = new ClamAVException(null, (ArrayList<String>) null, (Throwable) null);
        assertNull(clamAvException.getCode());
        testNoArgs(clamAvException);
        testNoException(clamAvException);

        clamAvException = new ClamAVException(null, (ArrayList<String>) null, EXCEPTION);
        assertNull(clamAvException.getCode());
        testNoArgs(clamAvException);
        testException(EXCEPTION, clamAvException);
        
        clamAvException = new ClamAVException(null, Arrays.asList(ARGS), (Throwable) null);
        assertNull(clamAvException.getCode());
        testArgs(ARGS, clamAvException);
        testNoException(clamAvException);

        clamAvException = new ClamAVException(CODE, (ArrayList<String>) null, (Throwable) null);
        assertEquals(CODE, clamAvException.getCode());
        testNoArgs(clamAvException);
        testNoException(clamAvException);
        
        clamAvException = new ClamAVException(null, Arrays.asList(ARGS), EXCEPTION);
        assertNull(clamAvException.getCode());
        testArgs(ARGS, clamAvException);
        testException(EXCEPTION, clamAvException);
        
        clamAvException = new ClamAVException(CODE, Arrays.asList(ARGS), (Throwable) null);
        assertEquals(CODE, clamAvException.getCode());
        testArgs(ARGS, clamAvException);
        testNoException(clamAvException);

        clamAvException = new ClamAVException(CODE, (ArrayList<String>) null, EXCEPTION);
        assertEquals(CODE, clamAvException.getCode());
        testNoArgs(clamAvException);
        testException(EXCEPTION, clamAvException);
        
        clamAvException = new ClamAVException(CODE, Arrays.asList(ARGS), EXCEPTION);
        assertEquals(CODE, clamAvException.getCode());
        testArgs(ARGS, clamAvException);
        testException(EXCEPTION, clamAvException);
    }

    /**
     * Méthode permettant de tester le cas sans argument.
     *
     * @param clamAvException exception à tester
     */
    private void testNoArgs(final ClamAVException clamAvException)
    {
        assertTrue(clamAvException.getArgs().length == 0);
        assertTrue(clamAvException.getArgsList().isEmpty());
    }

    /**
     * Méthode permettant de tester le cas avec argument.
     *
     * @param clamAvException exception à tester
     */
    private void testArgs(final String[] argsAttendus, final ClamAVException clamAvException)
    {
        IntStream.rangeClosed(0, 1).forEach(indice -> clamAvException.getArgs()[indice].equals(argsAttendus[indice]));
        assertEquals(Arrays.asList(argsAttendus), clamAvException.getArgsList());
    }

    /**
     * Méthode permettant de tester le cas avec exception responsable.
     *
     * @param clamAvException exception à tester
     */
    private void testException(final IOException exceptionAttendue, final ClamAVException clamAvException)
    {
        assertEquals(exceptionAttendue, clamAvException.getCause());
        assertNotNull(clamAvException.getMessage());
    }

    /**
     * Méthode permettant de tester le cas sans exception responsable.
     *
     * @param clamAvException exception à tester
     */
    private void testNoException(final ClamAVException clamAvException)
    {
        assertNull(clamAvException.getCause());
        assertNull(clamAvException.getMessage());
    }
}
