package fr.gouv.finances.lombok.monnaie.dao;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;

import fr.gouv.finances.lombok.monnaie.bean.TauxDeChancellerie;

public final class TauxDeChancellerieUtils
{
    /**
     * Constructeur.
     */
    private TauxDeChancellerieUtils()
    {
        super();
    }

    /**
     * Méthode permettant de générer un taux de chancellerie.
     *
     * @param indice indice permettant d'ajouter du dynamisme
     * @return le taux de chancellerie généré
     */
    public static TauxDeChancellerie getTauxDeChancellerie(int indice)
    {
        TauxDeChancellerie tauxDeChancellerie = new TauxDeChancellerie();
        tauxDeChancellerie.setValeurTauxChancel((double) indice);
        tauxDeChancellerie.setDateTauxChancel(getTimestamp(2017, 3, indice));

        return tauxDeChancellerie;
    }
    
    /**
     * Méthode permettant de générer une date sous la forme d'un TimeStamp.
     *
     * @param annee année de la date
     * @param mois mois de la date
     * @param jour jour de la date
     * @return la date générée sous forme de TimeStamp
     */
    private static Timestamp getTimestamp(int annee, int mois, int jour)
    {
        return new Timestamp(getDate(annee, mois, jour).getTime());
    }
    
    /**
     * Méthode permettant de générer une date.
     *
     * @param annee année de la date
     * @param mois mois de la date
     * @param jour jour de la date
     * @return la date générée
     */
    protected static Date getDate(int annee, int mois, int jour)
    {
        Calendar calendar = Calendar.getInstance();
        calendar.set(annee, mois, jour, 0, 0, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
    }
}
