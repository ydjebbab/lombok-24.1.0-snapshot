/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : SpringSecurityTest.java
 *
 */
package fr.gouv.finances.lombok.test.securite;

import java.util.Iterator;
import java.util.Locale;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.gouv.finances.lombok.securite.service.impl.AuthentificationEtLectureHabilitationsServiceImpl;
import fr.gouv.finances.lombok.securite.service.impl.LecturePersonneServiceImpl;
import fr.gouv.finances.lombok.securite.service.impl.RessourceAnnuaireServiceImpl;
import fr.gouv.finances.lombok.securite.springsecurity.LdapAuthentificationSecurityLombok;
import fr.gouv.finances.lombok.securite.springsecurity.UtilisateurSecurityLombok;
import fr.gouv.finances.lombok.securite.techbean.HabilitationAnnuaire;

/**
 * Class SpringSecurityTest.
 * 
 * @version $Revision: 1.4 $ Date: 15 déc. 2009
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {MockSpringSecurityTestProvider.class})
public class SpringSecurityTest
{
    protected final Log log = LogFactory.getLog(getClass());

    private static final String URL_LDAP = "ldap://10.69.247.30:389";

    @Autowired
    private ApplicationContext applicationContext;

    @Test
    public void testContext()
    {
        Assert.assertNotNull(applicationContext.getBean("avanthabilitationserviceso"));
    }

    @Test
    public void testretrieveUser()
    {
        LdapAuthentificationSecurityLombok ldapAuthentificationSecurityLombokService =
            new LdapAuthentificationSecurityLombok();
        AuthentificationEtLectureHabilitationsServiceImpl ldapService =
            new AuthentificationEtLectureHabilitationsServiceImpl();
        RessourceAnnuaireServiceImpl resourceldapService = new RessourceAnnuaireServiceImpl();
        LecturePersonneServiceImpl lecturePersonneService = new LecturePersonneServiceImpl();

        lecturePersonneService.setBaseRechercheLDAP("o=gouv,c=fr");

        resourceldapService.setUrlAnnuaireLDAP(URL_LDAP);
        ldapService.setRessourceannuaireso(resourceldapService);
        lecturePersonneService.setRessourceannuaireso(resourceldapService);
        ldapService.setLecturepersonneso(lecturePersonneService);

        ldapAuthentificationSecurityLombokService.setAuthentificationEtLectureHabilitationsService(ldapService);
        ldapAuthentificationSecurityLombokService.setCodeApplicationDansHabilitations("p3d");
        ldapAuthentificationSecurityLombokService.setModeAuthentification("appli");
        ldapAuthentificationSecurityLombokService.setApplicationContext(applicationContext);
        UsernamePasswordAuthenticationToken upat = new UsernamePasswordAuthenticationToken("", "0");

        try
        {
            UtilisateurSecurityLombok details =
                (UtilisateurSecurityLombok) ldapAuthentificationSecurityLombokService.retrieveUser("chouard-cp", upat);
            Iterator<HabilitationAnnuaire> iter = details.getPersonneAnnuaire().getListeHabilitations().iterator();
            boolean adminok = false;
            boolean partok = false;

            while (iter.hasNext())
            {
                HabilitationAnnuaire uneHabilitation = (HabilitationAnnuaire) iter.next();
                if (uneHabilitation.getNomProfil().equalsIgnoreCase("administrateur"))
                {
                    adminok = true;
                }
                if (uneHabilitation.getNomProfil().equalsIgnoreCase("particulier"))
                {
                    partok = true;
                }
                log.info(uneHabilitation.getNomProfil());
            }
            Assert.assertTrue(adminok);
            Assert.assertTrue(partok);

            log.info(details.getPersonneAnnuaire().getMail());
            Assert.assertEquals(details.getPersonneAnnuaire().getMail().toLowerCase(Locale.FRANCE),
                "christian.houard@dgfip.finances.gouv.fr");

        }
        catch (BadCredentialsException e)
        {
            log.info(e);
            Assert.fail();
        }

        ldapAuthentificationSecurityLombokService.setCodeApplicationDansHabilitations("p8d");

        try
        {
            ldapAuthentificationSecurityLombokService.retrieveUser("chouard-cp", upat);
            Assert.fail();
        }
        catch (BadCredentialsException e)
        {

            if (!e.getMessage().startsWith("Aucune habilitation"))
            {
                log.info(e.getMessage());
                Assert.fail();
            }

        }

        ldapAuthentificationSecurityLombokService.setCodeApplicationDansHabilitations("p3d");
        upat = new UsernamePasswordAuthenticationToken("", "toto");

        try
        {
            ldapAuthentificationSecurityLombokService.retrieveUser("chouard-cp", upat);
            Assert.fail();
        }
        catch (BadCredentialsException e)
        {
            if (!e.getMessage().startsWith("Mot de passe err"))
            {
                log.info(e.getMessage());
                Assert.fail();
            }
        }

        try
        {
            ldapAuthentificationSecurityLombokService.retrieveUser("cezzzezz-zz", upat);
            Assert.fail();
        }
        catch (BadCredentialsException e)
        {
            if (!e.getMessage().startsWith("Identifiant err"))
            {
                log.info(e.getMessage());
                Assert.fail();
            }
        }

    }

}
