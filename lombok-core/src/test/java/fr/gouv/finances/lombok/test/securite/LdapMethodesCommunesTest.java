/*
 * Copyright (c) 2014 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.test.securite;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.junit.Assert;
import fr.gouv.finances.lombok.securite.service.impl.LdapMethodesCommunes;

/**
 * Tests automatisés de la classe "LdapMethodesCommunes"
 *
 * @author Christophe Breheret-Girardin
 */
public class LdapMethodesCommunesTest
{
    /** Initialisation de la journalisation. */
    protected static final Logger LOGGER = LoggerFactory.getLogger(LdapMethodesCommunesTest.class);

    /**
     * Méthode permettant de tester le calcul du préfixe d'un nom dans le cas de recherche
     * portant sur un trop grand nombre de personnes.
     */
    @Test
    public void testcalculerPrefixeNom()
    {
        // Exécution et journalisation du résultat
        LdapMethodesCommunes ldapMethodesCommunes = new LdapMethodesCommunes();
        LOGGER.info("calculerPrefixe : {}", ldapMethodesCommunes.calculerPrefixe().toString());

        // Vérification du résultat
        Assert.assertEquals(1378, ldapMethodesCommunes.calculerPrefixe().size());
    }

    /**
     * Méthode permettant de tester le calcul du préfixe répartiteur
     */
    @Test
    public void testcalculerPrefixeRepartiteur()
    {
        // Exécution et journalisation du résultat
        LdapMethodesCommunes unemethodeCommune = new LdapMethodesCommunes();
        LOGGER.info("calculerPrefixeCasRepartiteur : {}", unemethodeCommune.calculerPrefixeCasRepartiteur().toString());

        // Vérification du résultat
        Assert.assertEquals(35178, unemethodeCommune.calculerPrefixeCasRepartiteur().size());
    }
}
