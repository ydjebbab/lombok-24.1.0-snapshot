/*
 * Copyright (c) 2016 DGFiP - Tous droits réservés
 * 
 */
package fr.gouv.finances.lombok.monnaie.bean;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;

import org.junit.Test;

import fr.gouv.finances.lombok.monnaie.dao.TauxDeChancellerieUtils;
import fr.gouv.finances.lombok.test.pojo.AbstractCorePojoTest;

/**
 * Tests unitaires de l'entité Monnaie.
 * 
 * @author Christophe Breheret-Girardin
 */
public class MonnaieTest extends AbstractCorePojoTest<Monnaie>
{
    /**
     * Constructeur.
     */
    public MonnaieTest()
    {
        super();
    }

    /**
     * Méthode permettant de tester l'égalité du POJO.
     */
    @Test
    public final void testEqualsHashCode()
    {
        EqualsVerifier.forClass(getParameterizedType())
            .suppress(Warning.NONFINAL_FIELDS, Warning.STRICT_INHERITANCE)
            .withPrefabValues(TauxDeChancellerie.class
                , TauxDeChancellerieUtils.getTauxDeChancellerie(1)
                , TauxDeChancellerieUtils.getTauxDeChancellerie(2))
            .withOnlyTheseFields("codeBDF", "libelleMonnaie")
            .usingGetClass()
            .verify();
    }

    /**
     * Méthode permettant de tester fr.gouv.finances.lombok.monnaie.bean.Monnaie#setTauxChancellerie(
     * fr.gouv.finances.lombok.monnaie.bean.TauxDeChancellerie).
     */
    @Test
    public void setTauxChancellerie()
    {
        Monnaie monnaie = new Monnaie();
        assertNull(monnaie.getListTauxChancel());

        monnaie.setTauxChancellerie(null);
        assertNotNull(monnaie.getListTauxChancel());
        assertEquals(0, monnaie.getListTauxChancel().size());

        monnaie.setTauxChancellerie(TauxDeChancellerieUtils.getTauxDeChancellerie(1));
        assertNotNull(monnaie.getListTauxChancel());
        assertEquals(1, monnaie.getListTauxChancel().size());
        assertEquals(TauxDeChancellerieUtils.getTauxDeChancellerie(1)
            , monnaie.getListTauxChancel().iterator().next());
    }
}