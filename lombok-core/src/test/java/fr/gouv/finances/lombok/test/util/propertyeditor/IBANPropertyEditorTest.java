/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : IBANPropertyEditorTest.java
 *
 */
package fr.gouv.finances.lombok.test.util.propertyeditor;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import fr.gouv.finances.lombok.util.propertyeditor.IBANPropertyEditor;
import junit.framework.TestCase;

/**
 * Class IBANPropertyEditorTest --.
 * 
 * @version $Revision: 1.2 $ Date: 11 déc. 2009
 */
public class IBANPropertyEditorTest extends TestCase
{

    /** log. */
    protected final Log log = LogFactory.getLog(getClass());

    /** un iban property editor. */
    IBANPropertyEditor unIBANPropertyEditor = new IBANPropertyEditor();

    /** un text sans blanc. */
    String unTextSansBlanc = "FR1420041010050500013M02606";

    /** un text avec blanc. */
    String unTextAvecBlanc = "FR14 2004 1010 0505 0001 3M02 606";

    /** un text blanc. */
    String unTextBlanc = " ";

    /** un text vide. */
    String unTextVide = "";

    /**
     * methode Test get as text true : --.
     */
    public void testGetAsTextTrue()
    {
        unIBANPropertyEditor.setValue(unTextAvecBlanc);
        String unIbanSansBlanc = unIBANPropertyEditor.getAsText();
        assertEquals(unIbanSansBlanc, unTextSansBlanc);
    }

    /**
     * methode Test get as text vide : --.
     */
    public void testGetAsTextVide()
    {
        unIBANPropertyEditor.setValue(unTextBlanc);
        String unIbanSansBlanc = unIBANPropertyEditor.getAsText();
        assertEquals(unIbanSansBlanc, unTextVide);
    }

    /**
     * methode Test set as text true : --.
     */
    public void testSetAsTextTrue()
    {
        unIBANPropertyEditor.setAsText(unTextSansBlanc);
        assertEquals(unIBANPropertyEditor.getValue(), unTextAvecBlanc);

    }

    /**
     * methode Test set as text vide : --.
     */
    public void testSetAsTextVide()
    {
        unIBANPropertyEditor.setAsText(unTextVide);
        assertEquals(unIBANPropertyEditor.getValue(), unTextVide);

    }

}
