/*
 * Copyright (c) 2017 DGFiP - Tous droits réservés
 * 
*/
/**
 * Paquet regroupant les POJO liés à la gestion de la monnaie
 * @author Christophe Breheret-Girardin
 */
package fr.gouv.finances.lombok.monnaie.bean;