/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : MenuProgrammationException.java
 *
 */
package fr.gouv.finances.lombok.menu.techbean;

import fr.gouv.finances.lombok.util.exception.ProgrammationException;

/**
 * Class MenuProgrammationException.
 * 
 * @author lcontinsouzas-cp
 * @version $Revision: 1.2 $ Date: 15 déc. 2009
 */
public class MenuProgrammationException extends ProgrammationException
{

    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /**
     * Instanciation de menu programmation exception.
     * 
     * @param message --
     */
    public MenuProgrammationException(String message)
    {
        super(message);
    }

    /**
     * Instanciation de menu programmation exception.
     * 
     * @param message --
     * @param cause --
     */
    public MenuProgrammationException(String message, Throwable cause)
    {
        super(message, cause);
    }
}
