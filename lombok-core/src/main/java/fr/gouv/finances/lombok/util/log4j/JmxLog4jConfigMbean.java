/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : JmxLog4jConfigMbean.java
 *
 */
package fr.gouv.finances.lombok.util.log4j;

/**
 * Interface JmxLog4jConfigMbean --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.2 $ Date: 11 déc. 2009
 */
public interface JmxLog4jConfigMbean
{

    /**
     * methode Affecter un logger a une classe ou un package : --.
     * 
     * @param category --
     * @param level --
     * @return string
     * @throws IllegalArgumentException the illegal argument exception
     */
    public String affecterUnLoggerAUneClasseOuUnPackage(String category, String level) throws IllegalArgumentException;

    /**
     * Accesseur de l attribut levels.
     * 
     * @return levels
     */
    public Object[] getLevels();

    /**
     * Accesseur de l attribut packages ou classes disposant d un logger.
     * 
     * @return packages ou classes disposant d un logger
     */
    public Object[] getPackagesOuClassesDisposantDUnLogger();

    /**
     * Accesseur de l attribut packages ou classes disposant d un logger defini.
     * 
     * @return packages ou classes disposant d un logger defini
     */
    public Object[] getPackagesOuClassesDisposantDUnLoggerDefini();

    /**
     * methode Lire le niveau de log pour une classe ou un packaqe : --.
     * 
     * @param category --
     * @return string
     * @throws IllegalArgumentException the illegal argument exception
     */
    public String lireLeNiveauDeLogPourUneClasseOuUnPackaqe(String category) throws IllegalArgumentException;
}
