/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : VoteurSecurityLombok.java
 *
 */
package fr.gouv.finances.lombok.securite.springsecurity;

import java.util.Collection;
import java.util.Iterator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.AccessDecisionVoter;
import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;

/**
 * Classe implementant l'interface springsecurity AccessDecisionVoter qui permet de décider si un utilisateur doit avoir
 * accès à une ressource (url) en fonction des profils requis pour accéder à cette ressource (config) et des profils
 * détenus par l'utilisateur (authentication). <br>
 * On peut aussi utiliser directement la classe RoleVoter fournie par springsecurity (code identique) néanmoins cette
 * classe peut être utile pour positionner des points d'arrets en debug.
 * 
 * @param <S> "documenté"
 * @author chouard-cp
 * @version $Revision: 1.3 $ Date: 15 déc. 2009
 */
public class VoteurSecurityLombok<S> implements AccessDecisionVoter<S>
{
    private static final Logger log = LoggerFactory.getLogger(VoteurSecurityLombok.class);

    private String rolePrefix = "ROLE_";

    public VoteurSecurityLombok()
    {
        super();
    }

    /**
     * Accesseur de l attribut role prefix.
     * 
     * @return role prefix
     */
    public String getRolePrefix()
    {
        return rolePrefix;
    }

    /**
     * Modificateur de l attribut role prefix.
     * 
     * @param rolePrefix le nouveau role prefix
     */
    public void setRolePrefix(String rolePrefix)
    {
        this.rolePrefix = rolePrefix;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param clazz "documenté"
     * @return true, si c'est vrai
     * @see org.springframework.security.vote.AccessDecisionVoter#supports(java.lang.Class)
     */
    @Override
    public boolean supports(Class clazz)
    {
        return true;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param attribute "documenté"
     * @return true, si c'est vrai
     * @see org.springframework.security.vote.AccessDecisionVoter#supports(org.springframework.security.ConfigAttribute)
     */
    @Override
    public boolean supports(ConfigAttribute attribute)
    {
        if ((attribute.getAttribute() != null) && attribute.getAttribute().startsWith(getRolePrefix()))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param authentication "documenté"
     * @param object "documenté"
     * @param config "documenté"
     * @return int
     * @see org.springframework.security.vote.AccessDecisionVoter#vote(org.springframework.security.Authentication,
     *      java.lang.Object, org.springframework.security.ConfigAttributeDefinition)
     */
    @Override
    public int vote(Authentication authentication, S object, Collection<ConfigAttribute> config)
    {
        log.debug(">>> Debut methode vote()");
        int result = ACCESS_ABSTAIN;
        Iterator<ConfigAttribute> iter = config.iterator();
        while (iter.hasNext())
        {
            ConfigAttribute attribute = (ConfigAttribute) iter.next();
            if (this.supports(attribute))
            {
                result = ACCESS_DENIED;
                Iterator<GrantedAuthority> iterAuth = (Iterator<GrantedAuthority>) authentication.getAuthorities().iterator();
                while (iterAuth.hasNext())
                {
                    if (attribute.getAttribute().equals(iterAuth.next().getAuthority()))
                    {
                        return ACCESS_GRANTED;
                    }
                }
            }
        }
        return result;
    }
}
