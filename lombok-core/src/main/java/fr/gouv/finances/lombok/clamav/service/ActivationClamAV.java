/*
 * Copyright (c) 2014 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.clamav.service;

/**
 * Valeurs possibles pour la configuration de l'activation ou non du contrôle d'antivirus.
 *
 * @author Christophe Breheret-Girardin
 */
public enum ActivationClamAV
{
    /** Actif. */
    ACTIF,
    /** Inactif. */
    INACTIF
}
