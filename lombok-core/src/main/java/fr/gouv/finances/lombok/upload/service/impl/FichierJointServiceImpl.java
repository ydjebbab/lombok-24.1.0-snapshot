/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : FichierJointServiceImpl.java
 *
 */
package fr.gouv.finances.lombok.upload.service.impl;

import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.gouv.finances.lombok.upload.bean.FichierJoint;
import fr.gouv.finances.lombok.upload.dao.FichierJointDao;
import fr.gouv.finances.lombok.upload.service.FichierJointService;
import fr.gouv.finances.lombok.util.base.BaseServiceImpl;

/**
 * Class FichierJointServiceImpl --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.2 $ Date: 11 déc. 2009
 */
public class FichierJointServiceImpl extends BaseServiceImpl implements FichierJointService
{
    private static final Logger log = LoggerFactory.getLogger(FichierJointServiceImpl.class);

    /** fichierjointdao. */
    private FichierJointDao fichierjointdao;

    /** uploadmaxsize. */
    private Long uploadmaxsize;

    public FichierJointServiceImpl()
    {
        super();
    }

    /**
     * Accesseur de l attribut fichierjointdao.
     * 
     * @return fichierjointdao
     */
    public FichierJointDao getFichierjointdao()
    {
        return fichierjointdao;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @return the uploadmaxsize
     * @see fr.gouv.finances.lombok.upload.service.FichierJointService#getUploadmaxsize()
     */
    @Override
    public Long getUploadmaxsize()
    {
        return uploadmaxsize;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param id
     * @return fichier joint
     * @see fr.gouv.finances.lombok.upload.service.FichierJointService#rechercherUnFichierJointEtSonContenuParId(java.lang.Long)
     */
    @Override
    public FichierJoint rechercherUnFichierJointEtSonContenuParId(Long id)
    {
        FichierJoint result = null;
        result = fichierjointdao.findFichierJointEtSonContenuParId(id);
        return result;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param unFichierJoint
     * @see fr.gouv.finances.lombok.upload.service.FichierJointService#sauvegarderUnFichierJoint(fr.gouv.finances.lombok.upload.bean.FichierJoint)
     */
    @Override
    public void sauvegarderUnFichierJoint(FichierJoint unFichierJoint)
    {
        log.info(">>> Debut methode sauvegarderUnFichierJoint");
        fichierjointdao.saveObject(unFichierJoint);
    }

    /**
     * Modificateur de l attribut fichierjointdao.
     * 
     * @param fichierjointdao le nouveau fichierjointdao
     */
    public void setFichierjointdao(FichierJointDao fichierjointdao)
    {
        this.fichierjointdao = fichierjointdao;
    }

    /**
     * Modificateur de l attribut uploadmaxsize.
     * 
     * @param uploadmaxsize le nouveau uploadmaxsize
     */
    public void setUploadmaxsize(Long uploadmaxsize)
    {
        this.uploadmaxsize = uploadmaxsize;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param fichiersASupprimer
     * @see fr.gouv.finances.lombok.upload.service.FichierJointService#supprimerLesFichiersJoints(java.util.Collection)
     */
    @Override
    public void supprimerLesFichiersJoints(Collection fichiersASupprimer)
    {
        fichierjointdao.deleteCollectionFichiersJoints(fichiersASupprimer);
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param unFichierJoint
     * @see fr.gouv.finances.lombok.upload.service.FichierJointService#supprimerUnFichierJoint(fr.gouv.finances.lombok.upload.bean.FichierJoint)
     */
    @Override
    public void supprimerUnFichierJoint(FichierJoint unFichierJoint)
    {
        fichierjointdao.deleteObject(unFichierJoint);
    }

}
