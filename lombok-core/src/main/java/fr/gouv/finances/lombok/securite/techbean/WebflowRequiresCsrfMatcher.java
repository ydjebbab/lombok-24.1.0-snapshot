/*
 * Copyright (c) 2020 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.securite.techbean;

import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.web.util.matcher.RequestMatcher;

/**
 * Class WebflowRequiresCsrfMatcher ajoute le post dans les matchs pour csrf.
 */
public class WebflowRequiresCsrfMatcher implements RequestMatcher
{
    private static Logger log = LoggerFactory.getLogger(WebflowRequiresCsrfMatcher.class);

    private Pattern allowedMethods = Pattern.compile("^(HEAD|TRACE|OPTIONS)$");

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see org.springframework.security.web.util.matcher.RequestMatcher#matches(javax.servlet.http.HttpServletRequest)
     */
    @Override
    public boolean matches(HttpServletRequest request)
    {
        boolean isMatchedMethod = !allowedMethods.matcher(request.getMethod()).matches();

        boolean isNewFlowRequest = true;
        if (request.getParameter("_flowExecutionKey") != null)
        {
            isNewFlowRequest = false;
        }
        else
        {
            if (log.isDebugEnabled())
            {
                log.debug("Protection csrf inactive au démarrage du flux.");
            }
        }

        return isMatchedMethod && !isNewFlowRequest;

    }

}
