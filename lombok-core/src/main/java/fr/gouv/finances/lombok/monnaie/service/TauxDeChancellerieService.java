/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : TauxDeChancellerieService.java
 *
 */
package fr.gouv.finances.lombok.monnaie.service;

import fr.gouv.finances.lombok.monnaie.bean.Monnaie;
import fr.gouv.finances.lombok.monnaie.bean.TauxDeChancellerie;

/**
 * Interface TauxDeChancellerieService --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.2 $ Date: 11 déc. 2009
 */
public interface TauxDeChancellerieService
{

    /**
     * methode Ajouter taux chancellerie a monnaie : --.
     * 
     * @param unTauxDeChancellerie --
     * @param uneMonnaie --
     */
    public void ajouterTauxChancellerieAMonnaie(TauxDeChancellerie unTauxDeChancellerie, Monnaie uneMonnaie);

    /**
     * methode Creer taux chancellerie : --.
     * 
     * @param unTauxDeChancellerie --
     */
    public void creerTauxChancellerie(TauxDeChancellerie unTauxDeChancellerie);

    /**
     * methode Modifier taux chancellerie : --.
     * 
     * @param unTauxDeChancellerie --
     */
    public void modifierTauxChancellerie(TauxDeChancellerie unTauxDeChancellerie);

    /**
     * methode Supprimer taux chancellerie : --.
     * 
     * @param unTauxDeChancellerie --
     */
    public void supprimerTauxChancellerie(TauxDeChancellerie unTauxDeChancellerie);
}
