/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : HabilitationXMLParser.java
 *
 */
package fr.gouv.finances.lombok.securite.service.impl;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashSet;
import java.util.Set;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import fr.gouv.finances.lombok.securite.techbean.FiltreHabilitationAnnuaire;
import fr.gouv.finances.lombok.securite.techbean.HabilitationAnnuaire;
import fr.gouv.finances.lombok.securite.techbean.ValeurFiltreHabilitationAnnuaire;
import fr.gouv.finances.lombok.util.exception.ApplicationExceptionTransformateur;
import fr.gouv.finances.lombok.util.exception.ProgrammationException;

/**
 * Class HabilitationXMLParser.
 * 
 * @author chouard-cp
 * @version $Revision: 1.2 $ Date: 15 déc. 2009
 */
public final class HabilitationXMLParser
{

    /** Constant : APTERA_BalHabilitation. */
    private static final String APTERA_BALHABILITATION = "HABILITATION";

    /** Constant : APTERA_BalCreated_by. */
    private static final String APTERA_BALCREATEDBY = "CREATED_BY";

    /** Constant : APTERA_BalAdminUID. */
    private static final String APTERA_BALADMINUID = "ADMIN_UID";

    /** Constant : APTERA_BalDateCreate. */
    private static final String APTERA_BALDATECREATE = "DATE";

    /** Constant : APTERA_BalProfil. */
    private static final String APTERA_BALPROFIL = "PROFIL";

    /** Constant : APTERA_BalPeriode. */
    private static final String APTERA_BALPERIODE = "PERIODE";

    /** Constant : APTERA_BalACL. */
    private static final String APTERA_BALACL = "ACL";

    /** Constant : APTERA_BalStart. */
    private static final String APTERA_BALSTART = "START";

    /** Constant : APTERA_BalEnd. */
    private static final String APTERA_BALEND = "END";

    /** Constant : APTERA_BalName. */
    private static final String APTERA_BALNAME = "NAME";

    /** Constant : APTERA_BalACLValue. */
    private static final String APTERA_BALACLVALUE = "ACLVALUE";

    /** Constant : APTERA_BalACLType. */
    private static final String APTERA_BALACLTYPE = "ACLTYPE";

    /** Constant : APTERA_TypeACLAllow. */
    private static final String APTERA_TYPEACLALLOW = "ALLOW";

    /** Constant : log. */
    private static final Log LOG = LogFactory.getLog(HabilitationXMLParser.class);

    /**
     * Instanciation de habilitation xml parser.
     */
    private HabilitationXMLParser()
    {
        // Pour éviter l'instanciation de la classe
    }

    /**
     * methode Habilitation decode avec separateur : --.
     * 
     * @param pListeProfilXML le liste profil xml
     * @return sets the
     */
    public static Set habilitationDecodeAvecSeparateur(String pListeProfilXML)
    {
        Set<HabilitationAnnuaire> desHabilitationsAnnuaire = new HashSet<>();
        HabilitationAnnuaire uneHabilitationAnnuaire = new HabilitationAnnuaire();

        // on ne traite en premier que le cas DGI ou un seul
        // mefiappliDGCP
        String[] valeurHab = StringUtils.split(pListeProfilXML);
        if ((valeurHab != null) && (valeurHab[0] != null))
        {
            uneHabilitationAnnuaire.setNomProfil(valeurHab[0]);
            desHabilitationsAnnuaire.add(uneHabilitationAnnuaire);
        }
        else
        {
            LOG.warn("valeur du profil non servie" + pListeProfilXML);
            desHabilitationsAnnuaire = null;
        }
        return desHabilitationsAnnuaire;
    }

    /**
     * methode Habilitation decode xml : --.
     * 
     * @param pListeProfilXML le liste profil xml
     * @return set< habilitation annuaire>
     */
    public static Set<HabilitationAnnuaire> habilitationDecodeXML(String pListeProfilXML, String libelleCourtAppli)
    {
        LOG.trace("Extraction du flux XML des habilitations");
        Document document = null;
        Node Noeud_H0;
        Node Noeud_H1;

        byte[] xmlByteArray = pListeProfilXML.getBytes();
        InputStream xmlStream = new ByteArrayInputStream(xmlByteArray);

        Set<HabilitationAnnuaire> desHabilitationsAnnuaire;

        try
        {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            factory.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
            factory.setExpandEntityReferences(false);
            DocumentBuilder parser = factory.newDocumentBuilder();
            document = parser.parse(new InputSource(xmlStream));

            NodeList ListeNoeuds_H0 = document.getElementsByTagName(APTERA_BALHABILITATION);
            NodeList ListeNoeuds_H1;

            HabilitationAnnuaire uneHabilitationAnnuaire;
            desHabilitationsAnnuaire = new HashSet<>();

            if (ListeNoeuds_H0 != null)
            {
                for (int h0 = 0; h0 < ListeNoeuds_H0.getLength(); h0++)
                {
                    Noeud_H0 = ListeNoeuds_H0.item(h0);
                    ListeNoeuds_H1 = Noeud_H0.getChildNodes();
                    uneHabilitationAnnuaire = new HabilitationAnnuaire();
                    uneHabilitationAnnuaire.setLibelleCourtAppli(libelleCourtAppli);
                    if (ListeNoeuds_H1 != null)
                    {
                        for (int h1 = 0; h1 < ListeNoeuds_H1.getLength(); h1++)
                        {
                            Noeud_H1 = ListeNoeuds_H1.item(h1);
                            if (Noeud_H1.getNodeName().equalsIgnoreCase(APTERA_BALCREATEDBY))
                            {

                                try
                                {
                                    habilitationDecodeXMLCreation(uneHabilitationAnnuaire, Noeud_H1);
                                }
                                catch (RuntimeException rte)
                                {
                                    LOG.error("Probleme non bloquant de decodage de la partie created_by de l'habilitation :"
                                        + pListeProfilXML, rte);
                                    uneHabilitationAnnuaire.setNomAdministrateur("Problème de décodage");
                                }
                            }
                            else if (Noeud_H1.getNodeName().equalsIgnoreCase(APTERA_BALPROFIL))
                            {
                                habilitationDecodeXMLProfil(uneHabilitationAnnuaire, Noeud_H1);
                            }
                            else if (Noeud_H1.getNodeName().equalsIgnoreCase(APTERA_BALPERIODE))
                            {
                                habilitationDecodeXMLPeriode(uneHabilitationAnnuaire, Noeud_H1);
                            }
                            else if (Noeud_H1.getNodeName().equalsIgnoreCase(APTERA_BALACL))
                            {
                                habilitationDecodeXMLDesFiltres(uneHabilitationAnnuaire, Noeud_H1);
                            }
                        }
                    }
                    desHabilitationsAnnuaire.add(uneHabilitationAnnuaire);
                }
            }
            else
            {
                desHabilitationsAnnuaire = null;
            }
        }
        catch (SAXException e)
        {
            LOG.warn("Erreur de traitement du flux : " + pListeProfilXML);
            throw ApplicationExceptionTransformateur.transformer("Erreur lors du traitement du fichier : "
                + pListeProfilXML, e);
        }
        catch (IOException e)
        {
            LOG.warn("Erreur de lecture du flux : " + pListeProfilXML);
            throw ApplicationExceptionTransformateur.transformer("Erreur lors de la lecture du fichier : "
                + pListeProfilXML, e);
        }
        catch (ParserConfigurationException e)
        {
            LOG.error("Aucun parseur DOM disponible");
            throw new ProgrammationException("Aucun parseur DOM disponible", e);
        }
        finally
        {
            try
            {
                if (xmlStream != null)
                {
                    // ferme le stream
                    xmlStream.close();
                }
            }
            catch (IOException ex)
            {
                LOG.warn("Impossible de fermer le flux", ex);
            }
        }

        return desHabilitationsAnnuaire;
    }

    /**
     * methode Habilitation decode xml creation : --.
     * 
     * @param uneHabilitationAnnuaire --
     * @param noeud_H1 --
     */
    static void habilitationDecodeXMLCreation(HabilitationAnnuaire uneHabilitationAnnuaire, Node noeud_H1)
    {

        NodeList listeSousNoeudsDate;
        Node sousNoeudDate;
        NodeList listeSousNoeudsTexte;
        Node noeudTexte;
        DateTime uneDate;
        DateTimeFormatter unFormat = DateTimeFormat.forPattern("dd/MM/yyyy");

        /*
         * decodage des donnees sur l'identifiant utilisateur et la date de creation de l'habilitation
         */
        if (noeud_H1 != null)
        {
            listeSousNoeudsDate = noeud_H1.getChildNodes();
            for (int p = 0; p < listeSousNoeudsDate.getLength(); p++)
            {
                sousNoeudDate = listeSousNoeudsDate.item(p);

                if (sousNoeudDate.getNodeName().equalsIgnoreCase(APTERA_BALADMINUID))
                {
                    listeSousNoeudsTexte = sousNoeudDate.getChildNodes();
                    for (int t = 0; t < listeSousNoeudsTexte.getLength(); t++)
                    {
                        noeudTexte = listeSousNoeudsTexte.item(t);
                        if (noeudTexte.getNodeType() == Node.TEXT_NODE)
                        {
                            uneHabilitationAnnuaire.setNomAdministrateur(noeudTexte.getNodeValue());
                            if (LOG.isDebugEnabled())
                            {
                                LOG.debug("Nom administrateur trouve : " + uneHabilitationAnnuaire.getNomAdministrateur());
                            }
                        }
                    }
                }
                if (sousNoeudDate.getNodeName().equalsIgnoreCase(APTERA_BALDATECREATE))
                {
                    listeSousNoeudsTexte = sousNoeudDate.getChildNodes();
                    for (int t = 0; t < listeSousNoeudsTexte.getLength(); t++)
                    {
                        noeudTexte = listeSousNoeudsTexte.item(t);
                        if (noeudTexte.getNodeType() == Node.TEXT_NODE)
                        {
                            uneDate = unFormat.parseDateTime(noeudTexte.getNodeValue());
                            uneHabilitationAnnuaire.setDateCreation(uneDate.toDate());
                            if (LOG.isDebugEnabled())
                            {
                                LOG.debug("date de creation : " + uneHabilitationAnnuaire.getDateCreation());
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * methode Habilitation decode xml des filtres : --.
     * 
     * @param uneHabilitationAnnuaire --
     * @param noeud_H1 --
     */
    static void habilitationDecodeXMLDesFiltres(HabilitationAnnuaire uneHabilitationAnnuaire, Node noeud_H1)
    {

        NodeList listeSousNoeudsFiltres;
        Node sousNoeudDonneesUnFiltre;
        FiltreHabilitationAnnuaire unFiltre;
        Set<FiltreHabilitationAnnuaire> desFiltres;

        if (noeud_H1 != null)
        {
            listeSousNoeudsFiltres = noeud_H1.getChildNodes();
            desFiltres = new HashSet<>();
            if ((listeSousNoeudsFiltres != null) && (listeSousNoeudsFiltres.getLength() > 0))
            {
                for (int p = 0; p < listeSousNoeudsFiltres.getLength(); p++)
                {
                    sousNoeudDonneesUnFiltre = listeSousNoeudsFiltres.item(p);
                    unFiltre = new FiltreHabilitationAnnuaire();
                    unFiltre.setNomDuFiltre(sousNoeudDonneesUnFiltre.getNodeName());
                    habilitationDecodeXMLUnFiltre(unFiltre, sousNoeudDonneesUnFiltre);
                    desFiltres.add(unFiltre);
                    if (LOG.isDebugEnabled())
                    {
                        LOG.debug("Filtre : " + unFiltre.getNomDuFiltre());
                    }
                }
            }
            uneHabilitationAnnuaire.setListeFiltres(desFiltres);
        }
    }

    /**
     * methode Habilitation decode xml periode : --.
     * 
     * @param uneHabilitationAnnuaire --
     * @param noeud_H1 --
     */
    static void habilitationDecodeXMLPeriode(HabilitationAnnuaire uneHabilitationAnnuaire, Node noeud_H1)
    {

        NodeList listeSousNoeudsPeriode;
        Node sousNoeudPeriode;
        NodeList listeSousNoeudsTexte;
        Node noeudTexte;
        DateTime uneDate;
        DateTimeFormatter unFormat = DateTimeFormat.forPattern("dd/MM/yyyy");

        /*
         * decodage des donnees les dates de debut et de fin de l'habilitation
         */
        if (noeud_H1 != null)
        {
            listeSousNoeudsPeriode = noeud_H1.getChildNodes();
            for (int p = 0; p < listeSousNoeudsPeriode.getLength(); p++)
            {
                sousNoeudPeriode = listeSousNoeudsPeriode.item(p);

                /* recherche date debut */
                if (sousNoeudPeriode.getNodeName().equalsIgnoreCase(APTERA_BALSTART))
                {
                    listeSousNoeudsTexte = sousNoeudPeriode.getChildNodes();
                    for (int t = 0; t < listeSousNoeudsTexte.getLength(); t++)
                    {
                        noeudTexte = listeSousNoeudsTexte.item(t);
                        if (noeudTexte.getNodeType() == Node.TEXT_NODE)
                        {
                            uneDate = unFormat.parseDateTime(noeudTexte.getNodeValue());
                            uneHabilitationAnnuaire.setDateDebut(uneDate.toDate());
                            if (LOG.isDebugEnabled())
                            {
                                LOG.debug("Date de debut : " + uneHabilitationAnnuaire.getDateDebut());
                            }
                        }
                    }
                }
                if (sousNoeudPeriode.getNodeName().equalsIgnoreCase(APTERA_BALEND))
                {
                    listeSousNoeudsTexte = sousNoeudPeriode.getChildNodes();
                    for (int t = 0; t < listeSousNoeudsTexte.getLength(); t++)
                    {
                        noeudTexte = listeSousNoeudsTexte.item(t);
                        if (noeudTexte.getNodeType() == Node.TEXT_NODE)
                        {
                            uneDate = unFormat.parseDateTime(noeudTexte.getNodeValue());
                            uneHabilitationAnnuaire.setDateFin(uneDate.toDate());
                            if (LOG.isDebugEnabled())
                            {
                                LOG.debug("Date de fin : " + uneHabilitationAnnuaire.getDateFin());
                            }
                        }
                    }
                }
            }
        }

    }

    /**
     * methode Habilitation decode xml profil : --.
     * 
     * @param uneHabilitationAnnuaire --
     * @param noeud_H1 --
     */
    static void habilitationDecodeXMLProfil(HabilitationAnnuaire uneHabilitationAnnuaire, Node noeud_H1)
    {

        NodeList listeSousNoeudsProfil;
        Node sousNoeudProfil;
        NodeList listeSousNoeudsTexte;
        Node noeudTexte;
        DateTimeFormat.forPattern("dd/MM/yyyy");

        /*
         * decodage des donnees les dates de debut et de fin de l'habilitation
         */
        if (noeud_H1 != null)
        {
            listeSousNoeudsProfil = noeud_H1.getChildNodes();
            for (int p = 0; p < listeSousNoeudsProfil.getLength(); p++)
            {
                sousNoeudProfil = listeSousNoeudsProfil.item(p);

                /* recherche date debut */
                if (sousNoeudProfil.getNodeName().equalsIgnoreCase(APTERA_BALNAME))
                {
                    listeSousNoeudsTexte = sousNoeudProfil.getChildNodes();
                    for (int t = 0; t < listeSousNoeudsTexte.getLength(); t++)
                    {
                        noeudTexte = listeSousNoeudsTexte.item(t);
                        if (noeudTexte.getNodeType() == Node.TEXT_NODE)
                        {
                            uneHabilitationAnnuaire.setNomProfil(noeudTexte.getNodeValue());
                            if (LOG.isDebugEnabled())
                            {
                                LOG.debug("Nom du profil : " + uneHabilitationAnnuaire.getNomProfil());
                            }
                        }
                    }
                }

                /*
                 * if (sousNoeudProfil.getNodeName().equalsIgnoreCase(APTERA_BalEnd)) { listeSousNoeudsTexte =
                 * sousNoeudProfil.getChildNodes(); for (int t = 0; t < listeSousNoeudsTexte.getLength(); t++) {
                 * noeudTexte = listeSousNoeudsTexte.item(t); if (noeudTexte.getNodeType() == Node.TEXT_NODE) { uneDate
                 * = unFormat.parseDateTime(noeudTexte.getNodeValue());
                 * uneHabilitationAnnuaire.setDateFin(uneDate.toDate()); } } }
                 */

            }
        }

    }

    /**
     * methode Habilitation decode xml un filtre : --.
     * 
     * @param unFiltre --
     * @param noeud_H1 --
     */
    static void habilitationDecodeXMLUnFiltre(FiltreHabilitationAnnuaire unFiltre, Node noeud_H1)
    {

        NodeList listeSousNoeudsValeurEtTypeFiltre;
        Node sousNoeudValeurFiltre;
        NodeList listeSousNoeudsTexte;
        Node noeudTexte;
        ValeurFiltreHabilitationAnnuaire uneValeur;
        unFiltre.setListeValeursDeFiltre(new HashSet<ValeurFiltreHabilitationAnnuaire>());

        listeSousNoeudsValeurEtTypeFiltre = noeud_H1.getChildNodes();
        for (int t = 0; t < listeSousNoeudsValeurEtTypeFiltre.getLength(); t++)
        {
            sousNoeudValeurFiltre = listeSousNoeudsValeurEtTypeFiltre.item(t);
            uneValeur = new ValeurFiltreHabilitationAnnuaire();

            /* decodage des valeurs de filtre */
            if (sousNoeudValeurFiltre.getNodeName().equalsIgnoreCase(APTERA_BALACLVALUE))
            {
                listeSousNoeudsTexte = sousNoeudValeurFiltre.getChildNodes();
                for (int q = 0; q < listeSousNoeudsTexte.getLength(); q++)
                {
                    noeudTexte = listeSousNoeudsTexte.item(q);
                    if (noeudTexte.getNodeType() == Node.TEXT_NODE)
                    {
                        uneValeur.setValeur(noeudTexte.getNodeValue());
                        unFiltre.getListeValeursDeFiltre().add(uneValeur);
                    }
                }
            }

            /* decodage du type de fitre (allow ou deny */
            else if (sousNoeudValeurFiltre.getNodeName().equalsIgnoreCase(APTERA_BALACLTYPE))
            {
                listeSousNoeudsTexte = sousNoeudValeurFiltre.getChildNodes();
                for (int q = 0; q < listeSousNoeudsTexte.getLength(); q++)
                {
                    noeudTexte = listeSousNoeudsTexte.item(q);
                    if (noeudTexte.getNodeType() == Node.TEXT_NODE)
                    {
                        if (noeudTexte.getNodeValue().equalsIgnoreCase(APTERA_TYPEACLALLOW))
                        {
                            unFiltre.setAllow(true);
                            unFiltre.setDeny(false);
                        }
                        else
                        {
                            unFiltre.setAllow(false);
                            unFiltre.setDeny(true);
                        }
                    }
                }
            }
        }
    }

    // cas ou l'on traite un utilsateur de type DGI ou ADC
    // dans ce cas , pas de mefiapplihabilitdgcp en format xml
    // mais un mefiappli*** de type nomappli;codeprofil;url

}
