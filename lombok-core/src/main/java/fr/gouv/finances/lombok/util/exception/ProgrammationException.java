/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 */
package fr.gouv.finances.lombok.util.exception;

/**
 * Exception dont la résolution relève d'un problème de développement.
 * Elle doit être renvoyée pour toute erreur imprévue.
 * 
 * @author amleplatinec
 * @author Christophe Breheret-Girardin
 */
public class ProgrammationException extends IngerableException
{

    /** Initialisation de l'UID. */
    private static final long serialVersionUID = 1L;

    /**
     * Constructeur.
     */
    public ProgrammationException()
    {
        super();

    }

    /**
     * Constructeur.
     * 
     * @param message message d'erreur
     */
    public ProgrammationException(String message)
    {
        super(message);
    }

    /**
     * Constructeur.
     * 
     * @param message message d'erreur
     * @param erreur cause de l'erreur
     */
    public ProgrammationException(String message, Throwable erreur)
    {
        super(message, erreur);
    }

    /**
     * Constructeur.
     * 
     * @param erreur cause de l'erreur
     */
    public ProgrammationException(Throwable erreur)
    {
        super(erreur);
    }
}
