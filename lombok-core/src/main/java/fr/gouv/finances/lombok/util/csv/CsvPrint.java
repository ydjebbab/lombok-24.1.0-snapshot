/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : CsvPrint.java
 *
 */
package fr.gouv.finances.lombok.util.csv;

/**
 * Interface CsvPrint Ecrit des données au format csv.
 * 
 * @author chouard-cp
 * @version $Revision: 1.2 $ Date: 15 déc. 2009
 */
public interface CsvPrint
{

    /**
     * Ferme le stream sous-jacent.
     */
    public void close();

    /**
     * Flush les données écrites sur le stream sous-jacent.
     */
    public void flush();

    /**
     * Positione le mode de flush. Si le mode autoflush est activé, une commande de flush esr exécutée sur le stream
     * sous jacent après chaque appel à une commande write
     * 
     * @param autoFlush true : active le mode autoflush
     */
    public void setAutoFlush(boolean autoFlush);

    /**
     * Change this printer so that it uses a new character for quoting.
     * 
     * @param newQuote The new character to use for quoting.
     */
    public void setCaractereProtectionChamp(char newQuote);

    /**
     * Change this printer so that it uses a new delimiter.
     * 
     * @param newDelimiter The new delimiter character to use.
     */
    public void setSeparateurDeChamp(char newDelimiter);

    /**
     * Indique si le printer csv doit toujours protéger les champs en les encadrant par des quotes ou s'il ne les
     * encadre que si c'est nécessaire.
     * 
     * @param toujoursProteger true si tous les champs doivent être protégés
     */
    public void setToujoursProtegerLeschamps(boolean toujoursProteger);

    /**
     * Ecrit un champ sur la ligne en cours. Le champ est encadré par des quotes si nécessaire Si la valeur du champ
     * passée en paramètre est nulle, une chaîne vide est écrite.
     * 
     * @param valeurDuChamp valeur du champ à écrire.
     */
    public void write(String valeurDuChamp);

    /**
     * Ecrit une ligne composée de champs séparés par un caractère de délimitation. Le séparateur de fin de ligne n'est
     * pas écrit. Les valeurs des champs sont encadrées par des quotes si nécessaires. Certains caractères sont protégés
     * si nécessaire.
     * 
     * @param valeursDesChamps valeurs des champs à écrire
     */
    public void write(String[] valeursDesChamps);

    /**
     * Ecrit le caractère de fin de ligne. Si le mode autoFlush est actif flush la ligne en cours
     */
    public void writeln();

    /**
     * Ecrit le dernier champ d'une ligne. La chaîne est encadrée de quotes si nécessaire
     * 
     * @param valeurDuChamp valeur du champ à écrire
     */
    public void writeln(String valeurDuChamp);

    /**
     * Ecrit une ligne composée de champs séparés par un caractère de délimitation. Les valeurs des champs sont
     * encadrées par des quotes si nécessaires. Certains caractères sont protégés si nécessaire
     * 
     * @param valeursDesChamps valeurs des champs à écrire
     */
    public void writeln(String[] valeursDesChamps);

    /**
     * Ecrit plusieurs lignes composées de champs séparés par un caractère de délimitation. Les valeurs des champs sont
     * encadrées par des quotes si nécessaires. Certains caractères sont protégés si nécessaire
     * 
     * @param tableauValeursDesChamps --
     */
    public void writeln(String[][] tableauValeursDesChamps);

    /**
     * Ecrit un commentaire dans le fichier cvs Les commentaires commencent toujours sur une nouvelle ligne et occupent
     * la ligne entière.
     * 
     * @param commentaire le commentaire à écrire
     */
    public void writelnComment(String commentaire);

}
