/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : MessagesAnomalie.java
 *
 */
package fr.gouv.finances.lombok.parsertexte.integration;

/**
 * Class MessagesAnomalie --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.3 $ Date: 11 déc. 2009
 */
public class MessagesAnomalie
{

    /** Constant : ERREUR_FICHIER_VIDE. */
    public static final String ERREUR_FICHIER_VIDE = "FICHIERVID Le fichier en entrée est vide";

    /** Constant : ERREUR_OUVERTURE_FICHIER. */
    public static final String ERREUR_OUVERTURE_FICHIER = "OUVERTFICH L'ouverture du fichier a généré une anomalie";

    /** Constant : ERREUR_VALEUR_NON_SERVIE. */
    public static final String ERREUR_VALEUR_NON_SERVIE = "VALNONSERV Valeur obligatoire non servie ";

    /** Constant : ERREUR_VALEUR_NON_ALPHANUMERIQUE. */
    public static final String ERREUR_VALEUR_NON_ALPHANUMERIQUE = "VALNONALNU Valeur non alphanumérique";

    /** Constant : ERREUR_VALEUR_NON_ALPHANUMERIQUE_INCLUANTESPACE. */
    public static final String ERREUR_VALEUR_NON_ALPHANUMERIQUE_INCLUANTESPACE = "VALNONALNUAVES Valeur non alphanumérique avec espace";

    /**
     * Constant : ERREUR_VALEUR_NON_ALPHABETIQUE_INCLUANTESPACE - String,
     */
    public static final String ERREUR_VALEUR_NON_ALPHABETIQUE_INCLUANTESPACE = "VALNONALPHAVES Valeur non alphabétique avec espace";

    /**
     * Constant : ERREUR_VALEUR_NON_ALPHANUMERIQUE_AVECAROBASE - String,
     */
    public static final String ERREUR_VALEUR_NON_ALPHANUMERIQUE_AVECAROBASE = "VALNONALPHAVARO Valeur non alphanumérique avec arobase";

    /**
     * Constant : ERREUR_VALEUR_NON_ALPHANUMERIQUE_AVECAROBASE_INCLUANTESPACE -String,
     */
    public static final String ERREUR_VALEUR_NON_ALPHANUMERIQUE_AVECAROBASE_INCLUANTESPACE =
        "VALNONALPHAVAROES Valeur non alphanumérique avec arobase et espace";

    /** Constant : ERREUR_VALEUR_NON_NUMERIQUE. */
    public static final String ERREUR_VALEUR_NON_NUMERIQUE = "VALNONNUME Valeur  non numérique";

    /** Constant : ERREUR_VALEUR_NON_NUMERIQUE_FLOTTANT. */
    public static final String ERREUR_VALEUR_NON_NUMERIQUE_FLOTTANT = "VALNONNUFL Valeur non numérique flottant";

    /** Constant : ERREUR_VALEUR_NON_NUMERIQUE_NEGATIF. */
    public static final String ERREUR_VALEUR_NON_NUMERIQUE_NEGATIF = "VALNONNUMENE Valeur  non numérique  (incluant nombre négatif) ";

    /**
     * Constant : ERREUR_VALEUR_NON_NUMERIQUE_FLOTTANT_NEGATIF -
     */
    public static final String ERREUR_VALEUR_NON_NUMERIQUE_FLOTTANT_NEGATIF =
        "VALNONNUFLNE Valeur non numérique (incluant nombre négatif) flottant";

    /** Constant : ERREUR_VALEUR_NON_ALPHABETIQUE. */
    public static final String ERREUR_VALEUR_NON_ALPHABETIQUE = "VALNONALPH Valeur non alphabétique";

    /** Constant : ERREUR_TAILLE_MIN_INCORRECTE. */
    public static final String ERREUR_TAILLE_MIN_INCORRECTE =
        "TAILMININC Valeur de taille inférieure à la taille minimum";

    /** Constant : ERREUR_TAILLE_MAX_INCORRECTE. */
    public static final String ERREUR_TAILLE_MAX_INCORRECTE =
        "TAILMAXINC Valeur de taille supérieure à la taille maximum";

    /** Constant : ERREUR_CONVERSION_INTEGER_IMPOSSIBLE. */
    public static final String ERREUR_CONVERSION_INTEGER_IMPOSSIBLE = "CONVINTIMP Conversion en Integer impossible";

    /** Constant : ERREUR_CONVERSION_DOUBLE_IMPOSSIBLE. */
    public static final String ERREUR_CONVERSION_DOUBLE_IMPOSSIBLE = "CONVDBLIMP Conversion en Double impossible";

    /** Constant : ERREUR_CONVERSION_BIGDECIMAL_IMPOSSIBLE. */
    public static final String ERREUR_CONVERSION_BIGDECIMAL_IMPOSSIBLE =
        "CONVBDLIMP Conversion en BigDecimal impossible";

    /** Constant : ERREUR_CONVERSION_BOOLEAN_IMPOSSIBLE - String,. */
    public static final String ERREUR_CONVERSION_BOOLEAN_IMPOSSIBLE =
        "CONVBOOIMP Conversion en Boolean impossible";

    /** Constant : ERREUR_NB_ELEMENTS_LIGNE_INCORRECT. */
    public static final String ERREUR_NB_ELEMENTS_LIGNE_INCORRECT =
        "NBELTSINCO Le nombre d'éléments de la ligne est incorrect";

    /** Constant : ERREUR_NB_ELEMENTS_LIGNE_MINI_INCORRECT. */
    public static final String ERREUR_NB_ELEMENTS_LIGNE_MINI_INCORRECT =
        "NBELTSMAXI Le nombre d'éléments de la ligne est inférieur au nombre d'éléments attendu";

    /** Constant : ERREUR_NB_ELEMENTS_LIGNE_MAXI_INCORRECT. */
    public static final String ERREUR_NB_ELEMENTS_LIGNE_MAXI_INCORRECT =
        "NBELTSMINI Le nombre d'éléments de la ligne est supérieur au nombre d'éléments attendus";

    /** Constant : ERREUR_TAILLE_LIGNE_INCORRECTE. */
    public static final String ERREUR_TAILLE_LIGNE_INCORRECTE = "TAICHAINCO La taille de la ligne est incorrecte";

    /** Constant : ERREUR_TAILLE_LIGNE_MINI_INCORRECTE. */
    public static final String ERREUR_TAILLE_LIGNE_MINI_INCORRECTE =
        "TAICHAMAXI La taille de la ligne est inférieure à la taille minimale attendue";

    /** Constant : ERREUR_TAILLE_LIGNE_MAXI_INCORRECTE. */
    public static final String ERREUR_TAILLE_LIGNE_MAXI_INCORRECTE =
        "TAICHAMINI La taille de la ligne est supérieure à la taille minimale attendue";

    /** Constant : ERREUR_LIGNE_MULTILIGNE_ABSENTE. */
    public static final String ERREUR_LIGNE_MULTILIGNE_ABSENTE =
        "LECTMULIAB Fin de fichier en cours d'élément multi-ligne";

    /** Constant : ERREUR_TYPE_LIGNE_INCONNU. */
    public static final String ERREUR_TYPE_LIGNE_INCONNU = "LIGINCONNU Ligne de type inconnu";

    /** Constant : ERREUR_NON_GEREE. */
    public static final String ERREUR_NON_GEREE = "ERRNONGERE ";

    // /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /** Constant : CODE_ERREUR_FICHIER_VIDE. */
    public static final String CODE_ERREUR_FICHIER_VIDE = "FICHIERVID";

    /** Constant : CODE_ERREUR_OUVERTURE_FICHIER. */
    public static final String CODE_ERREUR_OUVERTURE_FICHIER = "OUVERTFICH";

    /** Constant : CODE_ERREUR_VALEUR_NON_SERVIE. */
    public static final String CODE_ERREUR_VALEUR_NON_SERVIE = "VALNONSERV";

    /** Constant : CODE_ERREUR_VALEUR_NON_ALPHANUMERIQUE. */
    public static final String CODE_ERREUR_VALEUR_NON_ALPHANUMERIQUE = "VALNONALNU";

    /** Constant : CODE_ERREUR_VALEUR_NON_NUMERIQUE. */
    public static final String CODE_ERREUR_VALEUR_NON_NUMERIQUE = "VALNONNUME";

    /** Constant : CODE_VALEUR_NON_NUMERIQUE_FLOTTANT. */
    public static final String CODE_VALEUR_NON_NUMERIQUE_FLOTTANT = "VALNONNUFL";

    /** Constant : CODE_ERREUR_VALEUR_NON_ALPHABETIQUE. */
    public static final String CODE_ERREUR_VALEUR_NON_ALPHABETIQUE = "VALNONALPH";

    /** Constant : CODE_ERREUR_TAILLE_MIN_INCORRECTE. */
    public static final String CODE_ERREUR_TAILLE_MIN_INCORRECTE = "TAILMININC";

    /** Constant : CODE_ERREUR_TAILLE_MAX_INCORRECTE. */
    public static final String CODE_ERREUR_TAILLE_MAX_INCORRECTE = "TAILMAXINC";

    /** Constant : CODE_ERREUR_CONVERSION_INTEGER_IMPOSSIBLE. */
    public static final String CODE_ERREUR_CONVERSION_INTEGER_IMPOSSIBLE = "CONVINTIMP";

    /** Constant : CODE_ERREUR_CONVERSION_DOUBLE_IMPOSSIBLE. */
    public static final String CODE_ERREUR_CONVERSION_DOUBLE_IMPOSSIBLE = "CONVDBLIMP";

    /** Constant : CODE_ERREUR_CONVERSION_BIGDECIMAL_IMPOSSIBLE. */
    public static final String CODE_ERREUR_CONVERSION_BIGDECIMAL_IMPOSSIBLE = "CONVBDLIMP";

    /** Constant : CODE_ERREUR_CONVERSION_BOOLEAN_IMPOSSIBLE. */
    public static final String CODE_ERREUR_CONVERSION_BOOLEAN_IMPOSSIBLE = "CONVBOOIMP";

    /** Constant : CODE_ERREUR_NB_ELEMENTS_LIGNE_INCORRECT. */
    public static final String CODE_ERREUR_NB_ELEMENTS_LIGNE_INCORRECT = "NBELTSINCO";

    /** Constant : CODE_ERREUR_NB_ELEMENTS_LIGNE_MINI_INCORRECT. */
    public static final String CODE_ERREUR_NB_ELEMENTS_LIGNE_MINI_INCORRECT = "NBELTSMAXI";

    /** Constant : CODE_ERREUR_NB_ELEMENTS_LIGNE_MAXI_INCORRECT. */
    public static final String CODE_ERREUR_NB_ELEMENTS_LIGNE_MAXI_INCORRECT = "NBELTSMINI";

    /** Constant : CODE_ERREUR_TAILLE_LIGNE_INCORRECTE. */
    public static final String CODE_ERREUR_TAILLE_LIGNE_INCORRECTE = "TAICHAINCO";

    /** Constant : CODE_ERREUR_TAILLE_LIGNE_MINI_INCORRECTE. */
    public static final String CODE_ERREUR_TAILLE_LIGNE_MINI_INCORRECTE = "TAICHAMAXI";

    /** Constant : CODE_ERREUR_TAILLE_LIGNE_MAXI_INCORRECTE. */
    public static final String CODE_ERREUR_TAILLE_LIGNE_MAXI_INCORRECTE = "TAICHAMINI";

    /** Constant : CODE_ERREUR_LIGNE_MULTILIGNE_ABSENTE. */
    public static final String CODE_ERREUR_LIGNE_MULTILIGNE_ABSENTE = "LECTMULIAB";

    /** Constant : CODE_ERREUR_NON_GEREE. */
    public static final String CODE_ERREUR_NON_GEREE = "ERRNONGERE ";

    /** Constant : CODE_ERREUR_TYPE_LIGNE_INCONNU. */
    public static final String CODE_ERREUR_TYPE_LIGNE_INCONNU = "LIGINCONNU";

    // /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /** Constant : MESS_BATCH_FICHIER_DEJAINTEGRE. */
    public static final String MESS_BATCH_FICHIER_DEJAINTEGRE = "fichier deja integré : ";

    /** Constant : MESS_BATCH_FORMATFICHIER_INCORRECT. */
    public static final String MESS_BATCH_FORMATFICHIER_INCORRECT = "format fichier incorrect : ";

    /** Constant : MESS_BATCH_EXISTEDEJA_QUITTANCESANNULEES_ACETTEDATE. */
    public static final String MESS_BATCH_EXISTEDEJA_QUITTANCESANNULEES_ACETTEDATE =
        "Il existe deja des quittances annulees a cette date : ";

    /** Constant : MESS_BATCH_ANNEE_FICHIER. */
    public static final String MESS_BATCH_ANNEE_FICHIER = "L'année du fichier : ";

    /** Constant : MESS_BATCH_CORRESPONDPAS_ANNEEENCOURS. */
    public static final String MESS_BATCH_CORRESPONDPAS_ANNEEENCOURS = " ne correspond pas a l'année en cours : ";

    public MessagesAnomalie()
    {
        //constructeur vide
    }
}
