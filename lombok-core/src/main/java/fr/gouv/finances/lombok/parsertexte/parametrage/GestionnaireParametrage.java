/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : GestionnaireParametrage.java
 *
 */
package fr.gouv.finances.lombok.parsertexte.parametrage;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.XMLConstants;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.xml.sax.SAXException;

import fr.gouv.finances.lombok.parsertexte.integration.IntegrationException;

/**
 * Class GestionnaireParametrage --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.2 $ Date: 11 déc. 2009
 */
public class GestionnaireParametrage
{
    /** log. */
    protected final Log log = LogFactory.getLog(getClass());

    /** fabrique. */
    SAXParserFactory fabrique = null;

    /**
     * Instanciation de gestionnaire parametrage.
     */
    public GestionnaireParametrage()
    {
        log.debug("Avant instance SAX");
        this.fabrique = SAXParserFactory.newInstance();
        log.debug("Apres instance SAX");
    }

    /**
     * methode Lecture fichier classes : --.
     * 
     * @param pNomFichier le nom fichier
     * @return liste classes
     * @throws IntegrationException the integration exception
     */
    public ListeClasses lectureFichierClasses(String pNomFichier) throws IntegrationException
    {

        log.debug("fichier classe a lire : " + pNomFichier);
        URL fichier;

        try
        {
            fichier = new URL(pNomFichier);
        }
        catch (MalformedURLException e)
        {
            throw new IntegrationException("Integration : URL du fichier de classe mal formee : " + pNomFichier, e);
        }
        return this.lectureFichierClasses(fichier);
    }

    /**
     * Cette méthode va lire un fichier xml de paramétrage et retourne une instance de ListeClasses représentant les
     * infos de ce fichier.
     * 
     * @param urlFichierParamParseurTexteClasses --
     * @return the liste classes
     * @throws IntegrationException the integration exception
     */

    // LC : modif type du parametre 1 change en URL au lieu de File
    public ListeClasses lectureFichierClasses(URL urlFichierParamParseurTexteClasses) throws IntegrationException
    {
        ClasseHandler parserClasses = new ClasseHandler();
        try
        {
            log.debug("Recuperation Parser pour les classes");
            // création d'un parseur SAX
            this.fabrique.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
            SAXParser parseur = this.fabrique.newSAXParser();

            log.debug("Lecture par Parser des classes");

            // lecture du fichier XML
            parseur.parse(urlFichierParamParseurTexteClasses.openStream(), parserClasses);
        }
        catch (ParserConfigurationException pce)
        {
            throw new IntegrationException(
                    " Integration : Erreur de configuration du parseur lors de l'appel à newSAXParser() : "
                        + pce.getMessage(), pce);
        }
        catch (SAXException se)
        {
            throw new IntegrationException(
                    " Integration : Erreur lors du parsing du fichier de définition des classes : " + se.getMessage(),
                    se);
        }
        catch (IOException ioe)
        {
            throw new IntegrationException(" Integration : Erreur d entree/sortie lors de l'appel à newSAXParser() : "
                    + ioe.getMessage(), ioe);
        }
        return parserClasses.listeClassesValidees;
    }

    /**
     * methode Lecture fichier parametrage : --.
     * 
     * @param pNomFichier le nom fichier
     * @return fichier parametrage
     * @throws IntegrationException the integration exception
     */
    public FichierParametrage lectureFichierParametrage(String pNomFichier) throws IntegrationException
    {
        log.debug("fichier parametrage a lire : " + pNomFichier);

        URL fichier;
        try
        {
            fichier = new URL(pNomFichier);
        }
        catch (MalformedURLException e)
        {
            throw new IntegrationException("Integration : Erreur url malformee fichier parametrage", e);
        }
        return this.lectureFichierParametrage(fichier);
    }

    /**
     * Cette méthode va lire un fichier xml de paramétrage et retourne une instance de FichierParametrage représentant
     * les infos de ce fichier.
     * 
     * @param fichier --
     * @return the fichier parametrage
     * @throws IntegrationException the integration exception
     */
    // LC : modif type du parametre 1 change en URL au lieu de File
    public FichierParametrage lectureFichierParametrage(URL fichier) throws IntegrationException
    {
        ParametrageHandler parserParametrage = new ParametrageHandler();
        try
        {

            log.debug("Recuperation Parser pour parametrage");
            // création d'un parseur SAX
            this.fabrique.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
            SAXParser parseur = this.fabrique.newSAXParser();

            log.debug("Lecture par Parser du parametrage");
            parseur.parse(fichier.openStream(), parserParametrage);

        }
        catch (ParserConfigurationException pce)
        {
            throw new IntegrationException(
                    "Integration : Erreur de configuration du parseur lors de l'appel à newSAXParser()", pce);
        }
        catch (SAXException se)
        {
            throw new IntegrationException("Integration : Erreur lors du parsing du fichier de définition du fichier", se);
        }
        catch (IOException ioe)
        {
            throw new IntegrationException("Integration : Erreur d entree/sortie lors de l'appel à newSAXParser()", ioe);
        }

        return parserParametrage.fichierParamFini;
    }
}
