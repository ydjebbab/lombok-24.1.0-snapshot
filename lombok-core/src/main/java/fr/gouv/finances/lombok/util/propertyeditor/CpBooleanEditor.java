/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : CpBooleanEditor.java
 *
 */
package fr.gouv.finances.lombok.util.propertyeditor;

import java.beans.PropertyEditorSupport;
import java.text.ParseException;

import org.apache.commons.lang.StringUtils;

import fr.gouv.finances.lombok.util.format.BooleanFormat;
import fr.gouv.finances.lombok.util.format.FormaterBoolean;

/**
 * Property editor transformant des objets de type Boolean en chaînes de texte et réciproquement.
 * 
 * @author wpetit-cp
 * @version $Revision: 1.2 $ Date: 15 déc. 2009
 */
public class CpBooleanEditor extends PropertyEditorSupport
{

    /**
     * Valeurs à affecter à un objet Boolean si la châine à analyser est vide.
     */
    public enum ValeursSiChaineVide
    {

        /** NULL. */
        NULL,

        /** FALSE. */
        FALSE,

        /** TRUE. */
        TRUE,

        /** NO t_ allowed. */
        NOT_ALLOWED
    };

    /** Constant : NULL. */
    public static final ValeursSiChaineVide NULL = ValeursSiChaineVide.NULL;

    /** Constant : FALSE. */
    public static final ValeursSiChaineVide FALSE = ValeursSiChaineVide.FALSE;

    /** Constant : TRUE. */
    public static final ValeursSiChaineVide TRUE = ValeursSiChaineVide.TRUE;

    /** Constant : NOT_ALLOWED. */
    public static final ValeursSiChaineVide NOT_ALLOWED = ValeursSiChaineVide.NOT_ALLOWED;

    /** Constant : MSGERR_FORMAT_NULL. */
    private static final String MSGERR_FORMAT_NULL = "L'objet de type BooleanFormat ne doit pas être null";

    /** Format utiliser pour présenter un objet Boolean. */
    private final BooleanFormat booleanFormat;

    /** Comportement du property editor si la chaîne de texte à analyser est vide. */
    private final ValeursSiChaineVide emptyAs;

    /**
     * Crée un object CpBooleanEditor en utilisant comme format par défaut
     * <code>Formater.getFormatBooleanOuiNon()</code> pour formater un objet de type Boolean
     * <p>
     * Une chaîne vide est interprêtée comme une valeur <code>Boolean.FALSE</code>.
     * </p>
     * 
     * @throws IllegalArgumentException the illegal argument exception
     */
    public CpBooleanEditor() throws IllegalArgumentException
    {
        this(FormaterBoolean.getFormatBooleanOuiNon());
    }

    /**
     * Crée un object CpBooleanEditor en utilisant comme le format passé en paramètre pour analyser la chaîne de
     * caractères et pour formater l'objet.
     * <p>
     * Une chaîne vide est interprêtée comme une valeur <code>Boolean.False</code>.
     * </p>
     * 
     * @param booleanFormat --
     * @throws IllegalArgumentException the illegal argument exception
     */
    public CpBooleanEditor(BooleanFormat booleanFormat) throws IllegalArgumentException
    {
        this(booleanFormat, CpBooleanEditor.FALSE);
    }

    /**
     * Crée un object CpBooleanEditor en utilisant comme le format passé en paramètre pour analyser la chaîne de
     * caractères et pour formater un objet de type Boolean.
     * 
     * @param booleanFormat format de type BooleanFormat utilisé pour analyser et formater
     * @param emptyAs <p>
     *        Le paramètre "emptyAs" indique la façon dont une chaîne chaîne vide est interprêtée :
     *        <ul>
     *        <li>ValeursSiChaineVide.NULL = <code>null</code>.</li>
     *        <li>ValeursSiChaineVide.FALSE = <code>Boolean.FALSE</code>.</li>
     *        <li>ValeursSiChaineVide.TRUE = <code>Boolean.TRUE</code>.</li>
     *        <li>ValeursSiChaineVide.NOT_ALLOWED = <code>non autorisé</code>.</li>
     *        </ul>
     * @throws IllegalArgumentException the illegal argument exception
     */
    public CpBooleanEditor(BooleanFormat booleanFormat, ValeursSiChaineVide emptyAs) throws IllegalArgumentException
    {
        super();
        asserNotNullFormat(booleanFormat);
        this.booleanFormat = booleanFormat;
        this.emptyAs = (emptyAs != null ? emptyAs : ValeursSiChaineVide.FALSE);
    }

    /**
     * Crée un object CpBooleanEditor en utilisant comme format par défaut
     * <code>Formater.getFormatBooleanOuiNon()</code>
     * 
     * @param emptyAs <p>
     *        Le paramètre "emptyAs" indique la façon dont une chaîne chaîne vide est interprêtée :
     *        <ul>
     *        <li>ValeursSiChaineVide.NULL = <code>null</code>.</li>
     *        <li>ValeursSiChaineVide.FALSE = <code>Boolean.FALSE</code>.</li>
     *        <li>ValeursSiChaineVide.TRUE = <code>Boolean.TRUE</code>.</li>
     *        <li>ValeursSiChaineVide.NOT_ALLOWED = <code>non autorisé</code>.</li>
     *        </ul>
     * @throws IllegalArgumentException the illegal argument exception
     */
    public CpBooleanEditor(ValeursSiChaineVide emptyAs) throws IllegalArgumentException
    {
        this(FormaterBoolean.getFormatBooleanOuiNon(), emptyAs);
    }

    /**
     * Formate le booléen comme une chaîne de caractères en utilisant le format associé au property editor.
     * 
     * @return the as text
     */
    @Override
    public String getAsText()
    {
        String result;
        Object value = getValue();

        if (value == null)
        {
            result = "";
        }
        else if (this.booleanFormat == null)
        {
            result = value.toString();
        }
        else
        {
            // Utilise le format associé au property editor
            result = this.booleanFormat.format(value);
        }

        return result;
    }

    /**
     * Accesseur de l attribut boolean format.
     * 
     * @return boolean format
     */
    public BooleanFormat getBooleanFormat()
    {
        return booleanFormat;
    }

    /**
     * Accesseur de l attribut empty as.
     * 
     * @return empty as
     */
    public ValeursSiChaineVide getEmptyAs()
    {
        return emptyAs;
    }

    /**
     * Détermine le booléen correspondant à la chaîne de caractères passée en paramètre en utilisant le format associé
     * au property editor.
     * 
     * @param text --
     * @throws IllegalArgumentException the illegal argument exception
     */
    @Override
    public void setAsText(String text) throws IllegalArgumentException
    {

        switch (this.emptyAs)
        {
            case NOT_ALLOWED:
                throw new IllegalArgumentException("Imposible de convertir en Boolean une chaîne vide");
            case FALSE:
                // Traite une chaîne vide comme une valeur égale à Boolean.false
                this.booleanFormat.setBlankEgalFalse(true);
                break;
            case TRUE:
                // Traite une chaîne vide comme une valeur égale à Boolean.true
                this.booleanFormat.setBlankEgalFalse(false);
                break;
            default:
                break;
        }

        if (text == null || StringUtils.isBlank(this.epure(text)))
        {
            setValue(null);
        }
        else
        {
            try
            {
                setValue(this.booleanFormat.parseObject(this.epure(text)));
            }
            catch (ParseException pex)
            {
                throw new IllegalArgumentException(pex.getMessage(), pex);
            }
        }
    }

    /**
     * methode Asser not null format : --.
     * 
     * @param booleanFormat --
     */
    private void asserNotNullFormat(BooleanFormat booleanFormat)
    {
        if (booleanFormat == null)
        {
            throw new IllegalArgumentException(MSGERR_FORMAT_NULL);
        }
    }

    /**
     * Supprime tous les espaces du texte.
     * 
     * @param text --
     * @return the string
     */
    private String epure(String text)
    {
        String result = text;
        result = result.replaceAll("\\s+", "");
        return result;
    }
}
