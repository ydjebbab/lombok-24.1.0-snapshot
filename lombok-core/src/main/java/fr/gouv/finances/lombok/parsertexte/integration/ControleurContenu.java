/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : ControleurContenu.java
 *
 */
package fr.gouv.finances.lombok.parsertexte.integration;

import java.util.Locale;

import fr.gouv.finances.lombok.parsertexte.parametrage.AttributClasse;
import fr.gouv.finances.lombok.parsertexte.parametrage.TypeControles;

/**
 * Class ControleurContenu --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.2 $ Date: 11 déc. 2009
 */
public class ControleurContenu
{

    /** Constant : JEU_CARACTERE_ALPHABETIQUE. */
    protected static final String JEU_CARACTERE_ALPHABETIQUE = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    /**
     * JEU_CARACTERE_ALPHABETIQUE_INCLUANTESPACE - String, champs alphabetiques avec espace inclus dans la chaine
     */
    protected static final String JEU_CARACTERE_ALPHABETIQUE_INCLUANTESPACE = "ABCDEFGHIJKLMNOPQRSTUVWXYZ ";

    /** Constant : JEU_CARACTERE_ALPHANUMERIQUE. */
    protected static final String JEU_CARACTERE_ALPHANUMERIQUE = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

    /**
     * JEU_CARACTERE_ALPHANUMERIQUE_INCLUANTESPACE - String, champs alphanumeriques avec espace inclus dans la chaine
     */
    protected static final String JEU_CARACTERE_ALPHANUMERIQUE_INCLUANTESPACE = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789 ";

    /** Constant : JEU_CARACTERE_ALPHANUMERIQUE_AVECAROBASE. */
    protected static final String JEU_CARACTERE_ALPHANUMERIQUE_AVECAROBASE = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789@.-";
    
    /** Constant : JEU_CARACTERE_ALPHANUMERIQUE_AVECAROBASE_INCLUANTESPACE. */
    protected static final String JEU_CARACTERE_ALPHANUMERIQUE_AVECAROBASE_INCLUANTESPACE = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789@.- ";

    /** Constant : JEU_CARACTERE_NUMERIQUE. */
    protected static final String JEU_CARACTERE_NUMERIQUE = "0123456789";

    /** Constant : JEU_CARACTERE_NUMERIQUE_NEGATIF. */
    protected static final String JEU_CARACTERE_NUMERIQUE_NEGATIF = "0123456789-";

    /** Constant : JEU_CARACTERE_NUMERIQUE_FLOTTANT. */
    protected static final String JEU_CARACTERE_NUMERIQUE_FLOTTANT = "0123456789,. ";

    /**
     * JEU_CARACTERE_NUMERIQUE_FLOTTANT_NEGATIF - String, champ numerique flottant avec un caractére -
     */
    protected static final String JEU_CARACTERE_NUMERIQUE_FLOTTANT_NEGATIF = "0123456789,.- ";

    /**
     * Instanciation de controleur contenu.
     */
    public ControleurContenu()
    {
        super();
    }

    /**
     * Cette méthode réalise un certain nombre de contrôles de forme sur la valeur à intégrer. Si celle ci est
     * incorrecte un exception sera levée.
     * 
     * @param pAttribut le attribut
     * @param pValeurAControler le valeur a controler
     * @throws IntegrationException the integration exception
     */
    public void controleAttribut(AttributClasse pAttribut, String pValeurAControler) throws IntegrationException
    {
        if ((pAttribut.isObligatoire()) && ("".equals(pValeurAControler)))
        {
            throw this.generationException(pAttribut, pValeurAControler, MessagesAnomalie.ERREUR_VALEUR_NON_SERVIE);
        }
        else
        {
            if ((pAttribut.getTailleMin() != -1) && (pValeurAControler.length() < pAttribut.getTailleMin()))
            {
                throw this.generationException(pAttribut, pValeurAControler,
                    MessagesAnomalie.ERREUR_TAILLE_MIN_INCORRECTE);

            }

            if (!"".equals(pValeurAControler))
            {
                if ((pAttribut.getTailleMax() != -1) && (pValeurAControler.length() > pAttribut.getTailleMax()))
                {
                    throw this.generationException(pAttribut, pValeurAControler,
                        MessagesAnomalie.ERREUR_TAILLE_MAX_INCORRECTE);
                }

//                ancien code
//                if (pAttribut.getTypeControle() == TypeControles.CONTROLE_ALPHABETIQUE)
//                {
//                    if (!this.estComposeDeJeuDeCaracteres(JEU_CARACTERE_ALPHABETIQUE, pValeurAControler.toUpperCase(Locale.FRANCE)))
//                    {
//                        throw this.generationException(pAttribut, pValeurAControler,
//                            MessagesAnomalie.ERREUR_VALEUR_NON_ALPHABETIQUE);
//                    }
//                }
                
//                nouveau code
                if (pAttribut.getTypeControle() == TypeControles.CONTROLE_ALPHABETIQUE && 
                    !this.estComposeDeJeuDeCaracteres(JEU_CARACTERE_ALPHABETIQUE, pValeurAControler.toUpperCase(Locale.FRANCE)))
                {
                    throw this.generationException(pAttribut, pValeurAControler,
                        MessagesAnomalie.ERREUR_VALEUR_NON_ALPHABETIQUE);
                }
//                ancien code
//                else if (pAttribut.getTypeControle() == TypeControles.CONTROLE_ALPHANUMERIQUE)
//                {
//                    if (!this
//                        .estComposeDeJeuDeCaracteres(JEU_CARACTERE_ALPHANUMERIQUE, pValeurAControler.toUpperCase(Locale.FRANCE)))
//                    {
//                        throw this.generationException(pAttribut, pValeurAControler,
//                            MessagesAnomalie.ERREUR_VALEUR_NON_ALPHANUMERIQUE);
//                    }
//                }
                
//                nouveau code
                else if (pAttribut.getTypeControle() == TypeControles.CONTROLE_ALPHANUMERIQUE && 
                    !this.estComposeDeJeuDeCaracteres(JEU_CARACTERE_ALPHANUMERIQUE, pValeurAControler.toUpperCase(Locale.FRANCE)))
                {
                    throw this.generationException(pAttribut, pValeurAControler,
                        MessagesAnomalie.ERREUR_VALEUR_NON_ALPHANUMERIQUE);
                }
//                ancien code
//                else if (pAttribut.getTypeControle() == TypeControles.CONTROLE_NUMERIQUE)
//                {
//                    if (!this.estComposeDeJeuDeCaracteres(JEU_CARACTERE_NUMERIQUE, pValeurAControler))
//                    {
//                        throw this.generationException(pAttribut, pValeurAControler,
//                            MessagesAnomalie.ERREUR_VALEUR_NON_NUMERIQUE);
//                    }
//                }
                
//                nouveau code
                else if (pAttribut.getTypeControle() == TypeControles.CONTROLE_NUMERIQUE && 
                    !this.estComposeDeJeuDeCaracteres(JEU_CARACTERE_NUMERIQUE, pValeurAControler))
                {
                    throw this.generationException(pAttribut, pValeurAControler,
                        MessagesAnomalie.ERREUR_VALEUR_NON_NUMERIQUE);
                }
//                ancien code
//                else if (pAttribut.getTypeControle() == TypeControles.CONTROLE_NUMERIQUE_NEGATIF)
//                {
//                    if (!this.estComposeDeJeuDeCaracteres(JEU_CARACTERE_NUMERIQUE_NEGATIF, pValeurAControler))
//                    {
//                        throw this.generationException(pAttribut, pValeurAControler,
//                            MessagesAnomalie.ERREUR_VALEUR_NON_NUMERIQUE_NEGATIF);
//                    }
//                }
                
//                nouveau code
                else if (pAttribut.getTypeControle() == TypeControles.CONTROLE_NUMERIQUE_NEGATIF &&
                    !this.estComposeDeJeuDeCaracteres(JEU_CARACTERE_NUMERIQUE_NEGATIF, pValeurAControler))
                {
                    throw this.generationException(pAttribut, pValeurAControler,
                        MessagesAnomalie.ERREUR_VALEUR_NON_NUMERIQUE_NEGATIF);
                }
//                ancien code
//                else if (pAttribut.getTypeControle() == TypeControles.CONTROLE_NUMERIQUE_FLOTTANT)
//                {
//                    if (!this.estComposeDeJeuDeCaracteres(JEU_CARACTERE_NUMERIQUE_FLOTTANT, pValeurAControler))
//                    {
//                        throw this.generationException(pAttribut, pValeurAControler,
//                            MessagesAnomalie.ERREUR_VALEUR_NON_NUMERIQUE_FLOTTANT);
//                    }
//                }
                
//                nouveau code
                else if (pAttribut.getTypeControle() == TypeControles.CONTROLE_NUMERIQUE_FLOTTANT &&
                    !this.estComposeDeJeuDeCaracteres(JEU_CARACTERE_NUMERIQUE_FLOTTANT, pValeurAControler))
                {
                    throw this.generationException(pAttribut, pValeurAControler,
                        MessagesAnomalie.ERREUR_VALEUR_NON_NUMERIQUE_FLOTTANT);
                }
//              ancien code
//                else if (pAttribut.getTypeControle() == TypeControles.CONTROLE_NUMERIQUE_FLOTTANT_NEGATIF)
//                {
//                    if (!this.estComposeDeJeuDeCaracteres(JEU_CARACTERE_NUMERIQUE_FLOTTANT_NEGATIF, pValeurAControler))
//                    {
//                        throw this.generationException(pAttribut, pValeurAControler,
//                            MessagesAnomalie.ERREUR_VALEUR_NON_NUMERIQUE_FLOTTANT_NEGATIF);
//                    }
//                }
//              nouveau code
                else if (pAttribut.getTypeControle() == TypeControles.CONTROLE_NUMERIQUE_FLOTTANT_NEGATIF &&
                    !this.estComposeDeJeuDeCaracteres(JEU_CARACTERE_NUMERIQUE_FLOTTANT_NEGATIF, pValeurAControler))
                {
                    throw this.generationException(pAttribut, pValeurAControler,
                        MessagesAnomalie.ERREUR_VALEUR_NON_NUMERIQUE_FLOTTANT_NEGATIF);
                }
//              ancien code
//                else if (pAttribut.getTypeControle() == TypeControles.CONTROLE_ALPHABETIQUE_INCLUANTESPACE)
//                {
//                    if (!this.estComposeDeJeuDeCaracteres(JEU_CARACTERE_ALPHABETIQUE_INCLUANTESPACE,
//                        pValeurAControler.toUpperCase(Locale.FRANCE)))
//                    {
//                        throw this.generationException(pAttribut, pValeurAControler,
//                            MessagesAnomalie.ERREUR_VALEUR_NON_ALPHABETIQUE_INCLUANTESPACE);
//                    }
//                }
//              nouveau code
                else if (pAttribut.getTypeControle() == TypeControles.CONTROLE_ALPHABETIQUE_INCLUANTESPACE && 
                    !this.estComposeDeJeuDeCaracteres(JEU_CARACTERE_ALPHABETIQUE_INCLUANTESPACE,
                        pValeurAControler.toUpperCase(Locale.FRANCE)))
                {
                    throw this.generationException(pAttribut, pValeurAControler,
                        MessagesAnomalie.ERREUR_VALEUR_NON_ALPHABETIQUE_INCLUANTESPACE);
                }
//              ancien code
//                else if (pAttribut.getTypeControle() == TypeControles.CONTROLE_ALPHANUMERIQUE_INCLUANTESPACE)
//                {
//                    if (!this.estComposeDeJeuDeCaracteres(JEU_CARACTERE_ALPHANUMERIQUE_INCLUANTESPACE, 
//                        pValeurAControler.toUpperCase(Locale.FRANCE)))
//                    {
//                        throw this.generationException(pAttribut, pValeurAControler,
//                            MessagesAnomalie.ERREUR_VALEUR_NON_ALPHANUMERIQUE_INCLUANTESPACE);
//                    }
//                }
//              nouveau code
                else if (pAttribut.getTypeControle() == TypeControles.CONTROLE_ALPHANUMERIQUE_INCLUANTESPACE &&
                    !this.estComposeDeJeuDeCaracteres(JEU_CARACTERE_ALPHANUMERIQUE_INCLUANTESPACE, 
                        pValeurAControler.toUpperCase(Locale.FRANCE)))
                {
                    throw this.generationException(pAttribut, pValeurAControler,
                        MessagesAnomalie.ERREUR_VALEUR_NON_ALPHANUMERIQUE_INCLUANTESPACE);
                }
//                ancien code
//                else if (pAttribut.getTypeControle() == TypeControles.CONTROLE_ALPHANUMERIQUE_AVECAROBASE)
//                {
//                    if (!this.estComposeDeJeuDeCaracteres(JEU_CARACTERE_ALPHANUMERIQUE_AVECAROBASE, 
//                        pValeurAControler.toUpperCase(Locale.FRANCE)))
//                    {
//                        throw this.generationException(pAttribut, pValeurAControler,
//                            MessagesAnomalie.ERREUR_VALEUR_NON_ALPHANUMERIQUE_AVECAROBASE);
//                    }
//                }
//                nouveau code
                else if (pAttribut.getTypeControle() == TypeControles.CONTROLE_ALPHANUMERIQUE_AVECAROBASE &&
                    !this.estComposeDeJeuDeCaracteres(JEU_CARACTERE_ALPHANUMERIQUE_AVECAROBASE, 
                        pValeurAControler.toUpperCase(Locale.FRANCE)))
                {
                    throw this.generationException(pAttribut, pValeurAControler,
                        MessagesAnomalie.ERREUR_VALEUR_NON_ALPHANUMERIQUE_AVECAROBASE);
                }
//                ancien code
//                else if (pAttribut.getTypeControle() == TypeControles.CONTROLE_ALPHANUMERIQUE_AVECAROBASE_INCLUANTESPACE)
//                {
//                    if (!this.estComposeDeJeuDeCaracteres(JEU_CARACTERE_ALPHANUMERIQUE_AVECAROBASE_INCLUANTESPACE, 
//                        pValeurAControler.toUpperCase(Locale.FRANCE)))
//                    {
//                        throw this.generationException(pAttribut, pValeurAControler,
//                            MessagesAnomalie.ERREUR_VALEUR_NON_ALPHANUMERIQUE_AVECAROBASE_INCLUANTESPACE);
//                    }
//                }
//                nouveau code
                else if (pAttribut.getTypeControle() == TypeControles.CONTROLE_ALPHANUMERIQUE_AVECAROBASE_INCLUANTESPACE &&
                    !this.estComposeDeJeuDeCaracteres(JEU_CARACTERE_ALPHANUMERIQUE_AVECAROBASE_INCLUANTESPACE, 
                        pValeurAControler.toUpperCase(Locale.FRANCE)))
                {
                    throw this.generationException(pAttribut, pValeurAControler,
                        MessagesAnomalie.ERREUR_VALEUR_NON_ALPHANUMERIQUE_AVECAROBASE_INCLUANTESPACE);
                }
            }
        }
    }

    /**
     * Cette méthode prend en paramètre un ensemble de caractères et une chaine de caractères. Les caractères de la
     * chaine doivent être inclus dans le jeu de caractère, sinon une exception est levée
     * 
     * @param pJeuCaracteres le jeu caracteres
     * @param pChaine le chaine
     * @return true, if est compose de jeu de caracteres
     */
    public boolean estComposeDeJeuDeCaracteres(String pJeuCaracteres, String pChaine)
    {
        // On considère qu'une chaine vide n'est pas dans le jeu de caractères, il faudra la tester en amont.
        if (pChaine.length() == 0)
        {
            return false;
        }

        char[] chaineEnChar = pChaine.toCharArray();

        if (pJeuCaracteres.equalsIgnoreCase(JEU_CARACTERE_NUMERIQUE_FLOTTANT_NEGATIF))
        {
            if (pChaine.indexOf("-") != -1 && pChaine.indexOf("-") != 0)
            {
                return false;
            }
        }
        for (int i = 0; i < pChaine.length(); i++)
        // on verifie que - est en premier
        {
            // ensuite on passe aux controles classiques
            char caractere = chaineEnChar[i];

            if (pJeuCaracteres.indexOf(caractere) == -1)
            {
                return false;
            }
        }

        // reste à verifier que le - est bien au debut

        return true;
    }

    /**
     * Cette méthode contrôle qu'une chaine de caractères passée en paramètre est d'une taille égale à la valeur
     * spécifiée.
     * 
     * @param pChaine le chaine
     * @param pTaille le taille
     * @return true, if est de taille
     */
    public boolean estDeTaille(String pChaine, int pTaille)
    {
        boolean retour = false;
        if (pChaine != null)
        {
            if (pChaine.length() == pTaille)
            {
                retour = true;
            }
        }
        else
        {
            retour = true;
        }

        return retour;
    }

    /**
     * Cette méthode contrôle qu'une chaine de caractères passée en paramètre est d'une taille inférieure ou égale à la
     * valeur spécifiée.
     * 
     * @param pChaine le chaine
     * @param pTailleMaximale le taille maximale
     * @return true, if est de taille maximale
     */
    public boolean estDeTailleMaximale(String pChaine, int pTailleMaximale)
    {
        boolean retour = false;
        if (pChaine != null)
        {
            if (pChaine.length() <= pTailleMaximale)
            {
                retour = true;
            }
        }
        return retour;
    }

    /**
     * Cette méthode contrôle qu'une chaine de caractères passée en paramètre est d'une taille supérieure ou égale à la
     * valeur spécifiée.
     * 
     * @param pChaine le chaine
     * @param pTailleMinimale le taille minimale
     * @return true, if est de taille minimale
     */
    public boolean estDeTailleMinimale(String pChaine, int pTailleMinimale)
    {
        boolean retour = false;
        if (pChaine != null)
        {
            if (pChaine.length() >= pTailleMinimale)
            {
                retour = true;
            }
        }

        return retour;
    }

    /**
     * Cette méthode retourne l'attribut mis en forme en fonction des éléments demandés.
     * 
     * @param pAttribut le attribut
     * @param pValeurAControler le valeur a controler
     * @return the string
     */
    public String miseEnFormeAttribut(AttributClasse pAttribut, String pValeurAControler)
    {
        String strTmp = null;

        if (pAttribut.isSuppressionEspaceAutour())
        {
            strTmp = pValeurAControler.trim();
        }
        else
        {
            strTmp = pValeurAControler;
        }
        return strTmp;
    }

    /**
     * Cette méthode génère une exception qui sera levée pour prise en compte par le gestionnaire d'intégration.
     * 
     * @param pAttribut le attribut
     * @param pValeurAControler le valeur a controler
     * @param pMessage le message
     * @return the integration exception
     */
    private IntegrationException generationException(AttributClasse pAttribut, String pValeurAControler, String pMessage)
    {
        return new IntegrationException(pMessage, pAttribut.getNom(), pValeurAControler);
    }

}
