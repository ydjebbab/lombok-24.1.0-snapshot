/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : Messages.java
 *
 */
package fr.gouv.finances.lombok.util.messages;

import java.util.MissingResourceException;
import java.util.ResourceBundle;

import org.jfree.util.Log;

/**
 * <pre>
 * Classe statique permettant d'accéder aux fichiers messages.properties de spring (via le bean
 * MessageSourceSpring ou depuis les services ou objets métiers). 
 * Inconvénient : nom de fichier properties en dur (pour l'application, il doit se trouver à la racine du projet)
 * </pre>
 * 
 * @author lcontinsouzas-cp
 * @version $Revision: 1.2 $ Date: 15 déc. 2009
 */
public final class Messages
{
    // $NON-NLS-1$
    private static final String COMMUN_BUNDLE_NAME = "fr.gouv.finances.lombok.util.messages.communmessages";

    // $NON-NLS-1$
    private static final String APPLICATION_BUNDLE_NAME = "messages";

    private Messages()
    {
    }
    
    /**
     * Accesseur de l attribut string.
     * 
     * @param key --
     * @return string
     */
    public static String getString(String key)
    {
        String msg = null;
        try
        {
            ResourceBundle commun = ResourceBundle.getBundle(COMMUN_BUNDLE_NAME);
            msg = commun.getString(key);
            return msg;
        }
        catch (MissingResourceException mre)
        {
            Log.debug("Exception détectée", mre);
            try
            {
                ResourceBundle application = ResourceBundle.getBundle(APPLICATION_BUNDLE_NAME);
                msg = application.getString(key);
            }
            catch (MissingResourceException exception)
            {
                Log.debug("Exception détectée", exception);
                return null;
            }
            return msg;
        }
    }


}
