/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 *
 */
package fr.gouv.finances.lombok.parsertexte.integration;

import java.beans.PropertyEditorSupport;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.QueryException;
import org.springframework.util.ClassUtils;
import org.springframework.web.multipart.MultipartFile;

import fr.gouv.finances.lombok.parsertexte.parametrage.AttributClasse;
import fr.gouv.finances.lombok.parsertexte.parametrage.AttributMapping;
import fr.gouv.finances.lombok.parsertexte.parametrage.Classe;
import fr.gouv.finances.lombok.parsertexte.parametrage.DefinitionCustomPropertyEditor;
import fr.gouv.finances.lombok.parsertexte.parametrage.DiscriminantLigne;
import fr.gouv.finances.lombok.parsertexte.parametrage.FichierParametrage;
import fr.gouv.finances.lombok.parsertexte.parametrage.GestionnaireParametrage;
import fr.gouv.finances.lombok.parsertexte.parametrage.LigneFichier;
import fr.gouv.finances.lombok.parsertexte.parametrage.ListeClasses;
import fr.gouv.finances.lombok.parsertexte.parametrage.MappingLigneClasse;
import fr.gouv.finances.lombok.util.exception.ApplicationExceptionTransformateur;

/**
 * Implémentation du gestionnaire d'importation de fichier
 * 
 * @author amleplatinec
 * @author Christophe Breheret-Girardin
 */
public class GestionnaireImportationFichiers implements IGestionnaireImportationFichiers
{
    /** log. */
    protected final Log log = LogFactory.getLog(getClass());

    /** liste classes. */
    ListeClasses listeClasses = null;

    /** fichier parametrage. */
    FichierParametrage fichierParametrage = null;

    /** property editors. */
    Map propertyEditors = null;

    /** controleur contenu. */
    ControleurContenu controleurContenu = new ControleurContenu();

    /** ligne analysee. */
    LigneFichier ligneAnalysee = null;

    /** gestion memorisation lignes fichier. */
    MemorisationLignesFichier gestionMemorisationLignesFichier;

    /** memorisation lignes fichier. */
    boolean memorisationLignesFichier = false;

    /** encodage du fichier à importer. */
    String pEncoding = null;

    /**
     * Constructeur.
     */
    public GestionnaireImportationFichiers()
    {
        super();
    }

    /**
     * Constructeur.
     *
     * @param pEncoding le encoding
     */
    public GestionnaireImportationFichiers(String pEncoding)
    {
        super();
        this.pEncoding = pEncoding;
    }

    /**
     * {@inheritDoc}.
     * @override
     */
    public LigneFichier getLigneAnalysee()
    {
        return ligneAnalysee;
    }

    /**
     * {@inheritDoc}.
     * @override
     */
    @Override
    public boolean isMemorisationLignesFichier()
    {
        return memorisationLignesFichier;
    }

    /**
     * {@inheritDoc}.
     * @override
     */
    @Override
    public void lectureEtDecodageFichier(File pFichier, GestionnaireObjetExtrait pGestionnaire)
        throws IntegrationException
    {
        this.lectureEtDecodageFichier(pFichier, pGestionnaire, 0);
    }

    /**
     * {@inheritDoc}.
     * @override
     */
    @Override
    public void lectureEtDecodageFichier(File pFichier, GestionnaireObjetExtrait pGestionnaire, long pNumLigneReprise)
        throws IntegrationException
    {
        FileInputStream fileInput = null;
        try
        {
            fileInput = new FileInputStream(pFichier);
            this.realiserLectureEtDecodageFichier(fileInput, pGestionnaire, pNumLigneReprise);

        }
        catch (IOException exc)
        {
            throw new IntegrationException("Integration : Erreur durant l'acces au fichier.", exc);
        }
        finally
        {
            try
            {
                if (fileInput != null)
                {
                    fileInput.close();
                }
            }
            catch (IOException e)
            {
                log.info("Integration : Erreur durant la fermeture du flux correspondant au fichier traité.", e);
            }
        }
    }

    /**
     * {@inheritDoc}.
     * @override
     */
    @Override
    public void lectureEtDecodageFichier(InputStream flotEntree, GestionnaireObjetExtrait pGestionnaire)
        throws IntegrationException
    {
        this.realiserLectureEtDecodageFichier(flotEntree, pGestionnaire, 0);
    }

    /**
     * {@inheritDoc}.
     * @override
     */
    @Override
    public void lectureEtDecodageFichier(InputStream flotEntree, GestionnaireObjetExtrait pGestionnaire,
        long pNumLigneReprise) throws IntegrationException
    {
        this.realiserLectureEtDecodageFichier(flotEntree, pGestionnaire, pNumLigneReprise);
    }

    /**
     * {@inheritDoc}.
     * @override
     */
    @Override
    public void lectureEtDecodageFichier(MultipartFile pFichierMultiPart, GestionnaireObjetExtrait pGestionnaire)
        throws IntegrationException
    {
        this.lectureEtDecodageFichier(pFichierMultiPart, pGestionnaire, 0);
    }

    /**
     * {@inheritDoc}.
     * @override
     */
    @Override
    public void lectureEtDecodageFichier(MultipartFile pFichierMultiPart, GestionnaireObjetExtrait pGestionnaire,
        long pNumLigneReprise) throws IntegrationException
    {
        try
        {
            this.realiserLectureEtDecodageFichier(pFichierMultiPart.getInputStream(), pGestionnaire, pNumLigneReprise);
        }
        catch (IOException exc)
        {
            throw new IntegrationException("Integration : Erreur durant l'acces au fichier : ", exc);
        }
    }

    /**
     * {@inheritDoc}.
     * @override
     */
    @Override
    public void lectureEtDecodageFichier(String pRepertoireEtNomFichier, GestionnaireObjetExtrait pGestionnaire,
        long pNumLigneReprise) throws IntegrationException
    {
        try
        {
            File fichier = new File(pRepertoireEtNomFichier);

            FileInputStream fileInput = new FileInputStream(fichier);

            this.realiserLectureEtDecodageFichier(fileInput, pGestionnaire, pNumLigneReprise);
        }
        catch (IOException exc)
        {
            throw ApplicationExceptionTransformateur.transformer(exc);
        }
    }

    /**
     * {@inheritDoc}.
     * @override
     */
    @Override
    public void lectureEtDecodageFichier(String pRepertoire, String pNomFichier, GestionnaireObjetExtrait pGestionnaire)
        throws IntegrationException
    {
        this.lectureEtDecodageFichier(pRepertoire, pNomFichier, pGestionnaire, 0);
    }

    /**
     * {@inheritDoc}.
     * @override
     */
    @Override
    public void lectureEtDecodageFichier(String pRepertoire, String pNomFichier,
        GestionnaireObjetExtrait pGestionnaire, long pNumLigneReprise) throws IntegrationException
    {
        try
        {
            String cheminFichier = pRepertoire + pNomFichier;

            File fichier = new File(cheminFichier);

            FileInputStream fileInput = new FileInputStream(fichier);

            this.realiserLectureEtDecodageFichier(fileInput, pGestionnaire, pNumLigneReprise);
        }
        catch (IOException exc)
        {
            throw ApplicationExceptionTransformateur.transformer(exc);
        }
    }

    /**
     * {@inheritDoc}.
     * @override
     */
    @Override
    public void lectureParametrage(String urlFichierParamParseurTexteFormat, String urlFichierParamParseurTexteClasses)
        throws IntegrationException
    {
        GestionnaireParametrage gestionnaireParametrage = new GestionnaireParametrage();
        try
        {
            log.debug("Avant lecture fichier Classe");
            this.listeClasses = gestionnaireParametrage.lectureFichierClasses(urlFichierParamParseurTexteClasses);
            log.debug("Avant lecture fichier Parametrage");
            this.fichierParametrage =
                gestionnaireParametrage.lectureFichierParametrage(urlFichierParamParseurTexteFormat);
        }
        catch (IntegrationException exc)
        {
            throw new IntegrationException("Integration : Erreur durant l'acces au fichier : ", exc);
        }
    }

    /**
     * {@inheritDoc}.
     * @override
     */
    @Override
    public void lectureParametrage(URL urlFichierParamParseurTexteFormat, URL urlFichierParamParseurTexteClasses)
        throws IntegrationException
    {
        GestionnaireParametrage gestionnaireParametrage = new GestionnaireParametrage();
        try
        {
            log.debug("Avant lecture fichier Classe");
            this.listeClasses = gestionnaireParametrage.lectureFichierClasses(urlFichierParamParseurTexteClasses);
            log.debug("Avant lecture fichier Parametrage");
            this.fichierParametrage =
                gestionnaireParametrage.lectureFichierParametrage(urlFichierParamParseurTexteFormat);
        }
        catch (IntegrationException exc)
        {
            throw new IntegrationException("Integration : Erreur durant l'acces au fichier : ", exc);
        }
    }

    /**
     * {@inheritDoc}.
     * @override
     */
    @Override
    public void reinitialiserListeLignesMemorisees()
    {
        if (this.memorisationLignesFichier)
        {
            this.gestionMemorisationLignesFichier.effacerLignesMemorisees();
        }
    }

    /**
     * {@inheritDoc}.
     * @override
     */
    @Override
    public String retournerListeLignesMemorisees()
    {
        String tmp = null;
        if (this.memorisationLignesFichier)
        {
            tmp = this.gestionMemorisationLignesFichier.retournerLignesMemoriseesSousFormeString();
        }
        return tmp;
    }

    /**
     * {@inheritDoc}.
     * @override
     */
    @Override
    public void setLigneAnalysee(LigneFichier ligneAnalysee)
    {
        this.ligneAnalysee = ligneAnalysee;
    }

    /**
     * {@inheritDoc}.
     * @override
     */
    @Override
    public void setMemorisationLignesFichier(boolean memorisationLignesFichier)
    {
        this.memorisationLignesFichier = memorisationLignesFichier;

        if (memorisationLignesFichier)
        {
            if (this.gestionMemorisationLignesFichier == null)
            {
                this.gestionMemorisationLignesFichier = new MemorisationLignesFichier();
            }
        }
        else
        {
            this.gestionMemorisationLignesFichier = null;
        }
    }

    /**
     * Cette méthode prend en paramètre une ligne de fichier et va en retirer le type.
     * 
     * @param pLigneFichierSource le ligne fichier source
     * @return the ligne fichier
     */
    private LigneFichier analyseTypeLigneFichier(LigneFichierSource pLigneFichierSource)
    {
        // Est ce un fichier par séparateur ou un fichier à taille fixe?

        // Dans le cas d'un fichier avec séparateur, on doit découper la ligne avant de passer les
        // discriminants
        List listeLignes = this.fichierParametrage.getLignes();
        Iterator iterLignes = listeLignes.iterator();
        boolean bSortieBoucle = false;
        LigneFichier ligneFichierCorrespondante = null;

        do
        {
            if (iterLignes.hasNext())
            {
                LigneFichier descriptionLigne = (LigneFichier) iterLignes.next();

                if (descriptionLigne != null)
                {
                    DiscriminantLigne discriminant = descriptionLigne.getDiscriminantLigne();

                    if ((discriminant != null) && (pLigneFichierSource.estConformeADiscriminant(discriminant)))
                    {
                        ligneFichierCorrespondante = descriptionLigne;
                        bSortieBoucle = true;
                    }
                }
            }
            else
            {
                bSortieBoucle = true;
            }

        }
        while ((!bSortieBoucle));

        return ligneFichierCorrespondante;
    }

    /**
     * Controle format ligne.
     * 
     * @param pLigneFichierSource le ligne fichier source
     * @param pTypeFichier le type fichier
     * @param pTypeLigneFichier le type ligne fichier
     * @return the string
     */
    private String controleFormatLigne(LigneFichierSource pLigneFichierSource, int pTypeFichier,
        LigneFichier pTypeLigneFichier)
    {
        String messageErreur = null;

        if (pTypeFichier == FichierParametrage.TYPE_FICHIER_POSITION_FIXE)
        {
            int tailleChaine = pLigneFichierSource.getContenuLigne().length();

            if ((pTypeLigneFichier.getTaille() != -1) && (tailleChaine != pTypeLigneFichier.getTaille()))
            {
                messageErreur =
                    MessagesAnomalie.CODE_ERREUR_TAILLE_LIGNE_INCORRECTE + " La taille de la ligne ("
                        + Integer.toString(tailleChaine) + " caractères) n'est pas conforme à la taille attendue ("
                        + Integer.toString(pTypeLigneFichier.getTaille()) + " caractères) ";
            }
            else
            {
                if ((pTypeLigneFichier.getTailleMini() != -1) && (tailleChaine < pTypeLigneFichier.getTailleMini()))
                {
                    messageErreur =
                        MessagesAnomalie.CODE_ERREUR_TAILLE_LIGNE_MINI_INCORRECTE + " La taille de la ligne ("
                            + Integer.toString(tailleChaine)
                            + " caractères) est inférieure à la taille minimale attendue ("
                            + Integer.toString(pTypeLigneFichier.getTailleMini()) + " caractères) ";
                }

                if ((pTypeLigneFichier.getTailleMaxi() != -1) && (tailleChaine > pTypeLigneFichier.getTailleMaxi()))
                {
                    messageErreur =
                        MessagesAnomalie.CODE_ERREUR_TAILLE_LIGNE_MAXI_INCORRECTE + " La taille de la ligne ("
                            + Integer.toString(tailleChaine)
                            + " caractères) est supérieure à la taille maximale attendue ("
                            + Integer.toString(pTypeLigneFichier.getTailleMaxi()) + " caractères) ";
                }

            }
        }
        else
        {
            int nbElementsChaine = pLigneFichierSource.getElementsChaine().size();

            if ((pTypeLigneFichier.getNbElements() != -1) && (nbElementsChaine != pTypeLigneFichier.getNbElements()))
            {
                messageErreur =
                    MessagesAnomalie.CODE_ERREUR_NB_ELEMENTS_LIGNE_INCORRECT + " Le nb d'éléments de la ligne ("
                        + Integer.toString(nbElementsChaine) + " éléments) n'est pas conforme au nombre attendu ("
                        + Integer.toString(pTypeLigneFichier.getNbElements()) + " éléments) ";
            }
            else
            {
                if ((pTypeLigneFichier.getNbElementsMini() != -1)
                    && (nbElementsChaine < pTypeLigneFichier.getNbElementsMini()))
                {
                    messageErreur =
                        MessagesAnomalie.CODE_ERREUR_NB_ELEMENTS_LIGNE_MINI_INCORRECT
                            + " Le nombre d'éléments de la ligne (" + Integer.toString(nbElementsChaine)
                            + " éléments) est inférieur au nombre minimal attendu ("
                            + Integer.toString(pTypeLigneFichier.getNbElementsMini()) + " éléments) ";
                }

                if ((pTypeLigneFichier.getNbElementsMaxi() != -1)
                    && (nbElementsChaine > pTypeLigneFichier.getTailleMaxi()))
                {
                    messageErreur =
                        MessagesAnomalie.CODE_ERREUR_NB_ELEMENTS_LIGNE_MAXI_INCORRECT
                            + " Le nombre d'éléments de la ligne (" + Integer.toString(nbElementsChaine)
                            + " éléments) est supérieur au nombre maximal attendu ("
                            + Integer.toString(pTypeLigneFichier.getNbElementsMaxi()) + " éléments) ";
                }
            }
        }

        return messageErreur;
    }

    /**
     * Cette méthode effectue la conversion d'une valeur en String dans le l'attribut de classe souhaité.
     * 
     * @param pTypeAttribut le type attribut
     * @param pValeur le valeur
     * @param pLigne le ligne
     * @return the object
     * @throws IntegrationException the integration exception
     */
    private Object conversionStringVersTypeAttribut(String pTypeAttribut, String pValeur, LigneFichierSource pLigne)
        throws IntegrationException
    {
        Object objetRetour = null;

        /*
         * Affectation de la valeur à la classe, en fonction de son type
         */
        if ("java.lang.String".equalsIgnoreCase(pTypeAttribut))
        {
            objetRetour = pValeur;

        }
        else if ("java.lang.Integer".equalsIgnoreCase(pTypeAttribut))
        {
            try
            {
                objetRetour = Integer.valueOf(pValeur);
            }
            catch (NumberFormatException exc)
            {
                if ("".equals(pValeur))
                {
                    objetRetour = Integer.valueOf (0);
                }
                else
                {
                    log.debug("Pile d'exception lors de la conversion :", exc);
                    throw this
                        .generationException(null, pValeur, MessagesAnomalie.ERREUR_CONVERSION_INTEGER_IMPOSSIBLE);
                }
            }
        }
        else if ("java.lang.Double".equalsIgnoreCase(pTypeAttribut))
        {
            try
            {
                if ("".equals(pValeur))
                {
                    objetRetour = new Double(0.0);
                }
                else
                {
                    pValeur = OutilsString.deformatageNumerique(pValeur);
                    objetRetour = Double.valueOf(pValeur);
                }
            }
            catch (NumberFormatException exc)
            {
                log.debug("Pile d'exception lors de la conversion :", exc);
                throw this.generationException(exc,null, pValeur, MessagesAnomalie.ERREUR_CONVERSION_DOUBLE_IMPOSSIBLE);
            }
        }
        else if ("java.math.BigDecimal".equalsIgnoreCase(pTypeAttribut))
        {
            try
            {
                if ("".equals(pValeur))
                {
                    objetRetour = new BigDecimal("0");
                }
                else
                {
                    pValeur = OutilsString.deformatageNumerique(pValeur);
                    objetRetour = new BigDecimal(pValeur.toString());
                }
            }
            catch (NumberFormatException exc)
            {
                log.debug("Pile d'exception lors de la conversion :", exc);
                throw this.generationException(exc,null, pValeur, MessagesAnomalie.ERREUR_CONVERSION_BIGDECIMAL_IMPOSSIBLE);
            }
        }

        else if ("java.lang.Boolean".equalsIgnoreCase(pTypeAttribut))
        {
            objetRetour = Boolean.valueOf(pValeur);
        }

        return objetRetour;

    }

    /**
     * Cette méthode génère une exception qui sera levée pour prise en compte par le gestionnaire d'intégration.
     * 
     * @param pAttribut le attribut
     * @param pValeurAControler le valeur a controler
     * @param pMessage le message
     * @return the integration exception
     */
    private IntegrationException generationException(AttributClasse pAttribut, String pValeurAControler, String pMessage)
    {
        String nomAttribut = "";
        if (pAttribut != null)
        {
            nomAttribut = pAttribut.getNom();
        }
        return new IntegrationException(pMessage, nomAttribut, pValeurAControler);
    }
    
    /**
     * methode Generation exception : trick pour coca
     *
     * @param exc 
     * @param pAttribut le attribut
     * @param pValeurAControler le valeur a controler
     * @param pMessage le message
     * @return integration exception
     */
    private IntegrationException generationException(Exception exc,AttributClasse pAttribut, String pValeurAControler, String pMessage)
    {
        return this.generationException(pAttribut,pValeurAControler,pMessage);
        
    }

    /**
     * Cette méthode parcourt la liste des custom property editor déclarés dans le fichier de paramétrage et les
     * instancie.
     * 
     * @param pFichier le fichier
     * @param pGestionnaire le gestionnaire
     */
    private void initialisationCustomPropertyEditors(FichierParametrage pFichier, GestionnaireObjetExtrait pGestionnaire)
    {
        this.propertyEditors = new HashMap();
        List listeEditors = pFichier.getCustomPropertyEditors();
        if (listeEditors != null)
        {
            Iterator iter = listeEditors.iterator();
            while (iter.hasNext())
            {
                DefinitionCustomPropertyEditor customPropertyEditor = (DefinitionCustomPropertyEditor) iter.next();
                Object instanceCustomPropertyEditor = null;

                if (customPropertyEditor.getModeInstanciation() == DefinitionCustomPropertyEditor.INSTANCIATION_STANDARD)
                {
                    /* Si on est en mode d'instanciation standard, on instancie directement la classe */
                    instanceCustomPropertyEditor =
                        this.retourneInstanceClasse(customPropertyEditor.getCheminCustomPropertyEditor());
                }
                else
                {
                    /*
                     * Si on est en mode d'instanciation custom, on appelle la méthode d'instanciation qui devra être
                     * implémentée par le gestionnaire passé en parmaètre et qui permettra au développeur d'initialiser
                     * son property editor comme il est nécessaire
                     */
                    instanceCustomPropertyEditor =
                        pGestionnaire.instanciationCustomPropertyEditor(customPropertyEditor);
                }
                if (instanceCustomPropertyEditor != null)
                {
                    this.propertyEditors.put(customPropertyEditor.getTypeGere(), instanceCustomPropertyEditor);
                }
            }
        }
    }

    /**
     * Cette méthode utilise l'introspection pour appeler une méthode sur une classe.
     * 
     * @param ooo : Objet sur lequel on souhaite exécuter la méthode
     * @param args : tableau d'arguments (object) pour l'appel la méthode
     * @param nomMethode : Nom de la méthode à appeler
     * @return Un objet correspondant au résultat de la méthode
     * @throws IllegalAccessException the illegal access exception
     * @throws NoSuchMethodException the no such method exception
     * @throws InvocationTargetException the invocation target exception
     */
    private Object lancerMethode(Object ooo, Object[] args, String nomMethode) throws IllegalAccessException,
        NoSuchMethodException, InvocationTargetException
    {
        Class[] paramTypes = null;
        if (args != null)
        {
            paramTypes = new Class[args.length];
            for (int i = 0; i < args.length; ++i)
            {
                paramTypes[i] = args[i].getClass();
            }
        }
        Method mmm = ooo.getClass().getMethod(nomMethode, paramTypes);
        return mmm.invoke(ooo, args);
    }

    /**
     * Mappe ligne avec class.
     * 
     * @param pDescriptionLigne le description ligne
     * @param pLigne le ligne
     * @param pGestionnaire le gestionnaire
     * @return the retour traitement
     */
    private RetourTraitement mappeLigneAvecClass(LigneFichier pDescriptionLigne, LigneFichierSource pLigne,
        GestionnaireObjetExtrait pGestionnaire)
    {
        MappingLigneClasse mapping = this.fichierParametrage.retourneMappingPourLigneFichier(pDescriptionLigne);

        if (mapping != null)
        {
            Map instancesClasses = new HashMap();

            List attributs = mapping.getListeAttributs();

            // Cette variable indique si le traitement doit continuer ou s'arreter en fonction des éventuelles
            // anomalies trouvées lors du parsing du fichier
            int etatExtraction = 0;
            boolean presenceAnomalie = false;

            /*
             * TODO Revoir la gestion du type d'affectation qui pour le moment n'est pas du tout gérée. Créer un objet
             * type d'affectation portant pour chaque classe (dc avec l'id de la classe) du mapping le type
             * d'affectation, la méthode si besoin
             */

            // itération sur la liste des attributs pour réaliser le mapping au fur et à mesure
            Iterator iter = attributs.iterator();
            while ((iter.hasNext()) && (etatExtraction == GestionnaireObjetExtrait.TRAITEMENT_CONTINUER))
            {
                AttributMapping attribut = (AttributMapping) iter.next();
                Object instance;

                instance = instancesClasses.get(attribut.getIdentifiantInstance());

                Classe classeAttribut = this.listeClasses.retournerClassePourId(attribut.getIdClasse());

                // TODO optimiser la récupération de la classe & l'affectation ensuite.

                if (instance == null)
                {
                    instance = this.retourneInstanceClasse(classeAttribut.getCheminClasse());
                    // Ajout de l'instance créée dans la map et dans la liste
                    instancesClasses.put(attribut.getIdentifiantInstance(), instance);
                    // listeInstances.add(instance);
                }

                if (instance != null)
                {
                    String nomAttribut = attribut.getAttribut();
                    AttributClasse attributClasse = classeAttribut.retourneAttributPourNom(nomAttribut);

                    if (attributClasse != null)
                    {
                        String nomSetter = this.transformerNomAttributEnSetter(attribut.getAttribut());

                        // Cette chaine de caractère contient la valeur "brut" lue du fichier texte
                        String valeurAAffecter = pLigne.retournerProprieteMappeeParNom(attribut.getIdPropriete());

                        // Cet objet représente l'objet à affecter après transformation de la String dans le type
                        // cible.
                        Object objetAAffecter = null;

                        /* Si le contenu de cette zone nécessite un contrôle, on demande au contrôleur de le faire */
                        if (attributClasse.miseEnFormeAEffectuer())
                        {
                            valeurAAffecter = controleurContenu.miseEnFormeAttribut(attributClasse, valeurAAffecter);
                        }

                        /* Si le contenu de cette zone nécessite un contrôle, on demande au contrôleur de le faire */
                        if (attributClasse.controleAEffectuer())
                        {
                            try
                            {
                                controleurContenu.controleAttribut(attributClasse, valeurAAffecter);
                            }
                            catch (IntegrationException exc)
                            {
                                log.debug("Exception détectée", exc);

                                etatExtraction =
                                    pGestionnaire.anomalieExtraction(pDescriptionLigne.getId(),
                                        pLigne.getNumeroLigne(), exc.getIdAttribut(), exc.getMessage(), pLigne
                                            .getContenuCompletLigne("\n"));
                                presenceAnomalie = true;
                            }
                        }

                        if (etatExtraction == GestionnaireObjetExtrait.TRAITEMENT_CONTINUER)
                        {
                            boolean bAffectationElementConverti = true;

                            // Récupération de l'éventuel custom property editor pour ce type
                            PropertyEditorSupport customPropertyEditor =
                                (PropertyEditorSupport) this.propertyEditors.get(attributClasse.getType());
                            // Il y a un custom property editor pour ce type donc on l'utilise

                            try
                            {
                                if (customPropertyEditor != null)
                                {
                                    customPropertyEditor.setAsText(valeurAAffecter);
                                    objetAAffecter = customPropertyEditor.getValue();
                                }
                                else
                                {
                                    objetAAffecter =
                                        this.conversionStringVersTypeAttribut(attributClasse.getType(),
                                            valeurAAffecter, pLigne);
                                }
                            }

                            catch (IntegrationException exc)
                            {
                                log.debug("Pile d'exception lors de la levée de l'anomalie :", exc);
                                etatExtraction =
                                    pGestionnaire.anomalieExtraction(pDescriptionLigne.getId(),
                                        pLigne.getNumeroLigne(), attributClasse.getNom(), exc.getMessage(), pLigne
                                            .getContenuCompletLigne("\n"));
                                presenceAnomalie = true;
                                bAffectationElementConverti = false;
                            }

                            // Appel du setter uniquement si on continue l'extraction et si la conversion s'est bien
                            // passée
                            if ((objetAAffecter != null)
                                && (etatExtraction == GestionnaireObjetExtrait.TRAITEMENT_CONTINUER)
                                && (bAffectationElementConverti))
                            {
                                try
                                {
                                    this.lancerMethode(instance, new Object[] {objetAAffecter}, nomSetter);
                                }
                                catch (IllegalAccessException exc)
                                {
                                    String message =
                                        MessagesAnomalie.ERREUR_NON_GEREE + " IllegalAccessException : "
                                            + exc.getMessage();
                                    log.debug("Pile d'exception lors de la levée de l'anomalie :", exc);
                                    etatExtraction =
                                        pGestionnaire.anomalieExtraction(pDescriptionLigne.getId(), pLigne
                                            .getNumeroLigne(), attributClasse.getNom(), message, pLigne
                                            .getContenuCompletLigne("\n"));
                                }
                                catch (NoSuchMethodException exc)
                                {
                                    String message =
                                        MessagesAnomalie.ERREUR_NON_GEREE + " NoSuchMethodException : "
                                            + exc.getMessage();
                                    log.debug("Pile d'exception lors de la levée de l'anomalie :", exc);
                                    etatExtraction =
                                        pGestionnaire.anomalieExtraction(pDescriptionLigne.getId(), pLigne
                                            .getNumeroLigne(), attributClasse.getNom(), message, pLigne
                                            .getContenuCompletLigne("\n"));
                                }
                                catch (InvocationTargetException exc)
                                {
                                    String message =
                                        MessagesAnomalie.ERREUR_NON_GEREE + " InvocationTargetException : "
                                            + exc.getMessage();
                                    log.debug("Pile d'exception lors de la levée de l'anomalie :", exc);
                                    etatExtraction =
                                        pGestionnaire.anomalieExtraction(pDescriptionLigne.getId(), pLigne
                                            .getNumeroLigne(), attributClasse.getNom(), message, pLigne
                                            .getContenuCompletLigne("\n"));
                                }
                            }
                        }
                    }
                    else
                    {
                        String message =
                            MessagesAnomalie.ERREUR_NON_GEREE
                                + " Attribut de classe specifié dans le fichier de config inexistant (nom propriete): "
                                + nomAttribut;
                        etatExtraction =
                            pGestionnaire.anomalieExtraction(pDescriptionLigne.getId(), pLigne.getNumeroLigne(),
                                "Classe inconnue", message, pLigne.getContenuCompletLigne("\n"));
                    }
                }
            }

            // Retour de la liste et de l'état de continuation du traitement
            return new RetourTraitement(instancesClasses, etatExtraction, presenceAnomalie);
        }
        else
        {
            return null;
        }
    }

    /**
     * Realiser lecture et decodage fichier.
     * 
     * @param flotEntree --
     * @param pGestionnaire le gestionnaire
     * @param pNumLigneReprise le num ligne reprise
     * @throws IntegrationException the integration exception
     */
    private void realiserLectureEtDecodageFichier(InputStream flotEntree, GestionnaireObjetExtrait pGestionnaire,
        long pNumLigneReprise) throws IntegrationException
    {

        this.initialisationCustomPropertyEditors(this.fichierParametrage, pGestionnaire);
        pGestionnaire.setGestionnaireImportationFichiers(this);
        boolean presenceAnomalieDansFichier = false;

        try
        {
            BufferedReader lecteurFichierSource = null;
            ;
            int numLigne = 0;
            int nbLignesReellementTraitees = 0;

            // Encapsule l'inputStream dans un BufferedReader
            if (pEncoding != null)
            {
                lecteurFichierSource = new BufferedReader(new InputStreamReader(flotEntree, pEncoding));
            }
            else
            {
                lecteurFichierSource = new BufferedReader(new InputStreamReader(flotEntree));
            }

            String ligneFichier = null;

            boolean continuerTraitementFichier = true;

            do
            {
                ligneFichier = lecteurFichierSource.readLine();

                // Incrémentation du nb de ligne (on commene à la ligne 1) et on contrôle
                // si cette ligne est une ligne d'entête. Dans ce cas là, on l'ignore tout simplement.
                numLigne++;
                boolean estUneLigneEntete = (numLigne <= fichierParametrage.getNbLignesEntete().intValue());
                int avancementTraitement = GestionnaireObjetExtrait.TRAITEMENT_CONTINUER;

                if ((ligneFichier != null) && (!estUneLigneEntete))
                {
                    // Création de l'instance qui héberge la ligne du fichier proprement dite
                    LigneFichierSource ligneFichierSource =
                        new LigneFichierSource(ligneFichier, fichierParametrage, numLigne);

                    LigneFichier typeLigneFichier = this.analyseTypeLigneFichier(ligneFichierSource);

                    // quand on arrive a la ligne TOTAL GENERAL qui signifie la fin de fichier, les lignes qui suivent,
                    // sont consideres comme inconnus,
                    // meme si elles sont identifiees par le mapping

                    if (typeLigneFichier == null)
                    {
                        // /////////////
                        // /// le type de ligne peut être décidé en fonction de critère non descriptibles dans le
                        // fichier de conf.
                        // /// dans ce cas, on offre la possibilité à l'utilisateur de nous donner le code du type de
                        // ligne permettant
                        // /// le décodage de la ligne
                        // /////////////
                        String codeTypeLigne = pGestionnaire.extractionLigneInconnue(ligneFichierSource, numLigne);
                        typeLigneFichier = fichierParametrage.retourneTypeLigneFichierPourId(codeTypeLigne);

                    }

                    // //////////////////////////////////////////////////////////////////////////////////////
                    // Important : Ici est pris en compte la gestion des démarrages
                    // de traitement en cours de fichier (dans le cas de reprise de batch)
                    // //////////////////////////////////////////////////////////////////////////////////////

                    if ((typeLigneFichier != null) && (numLigne >= pNumLigneReprise))
                    {
                        // Gestion du nombre de ligne réellement traitées dans le fichier
                        nbLignesReellementTraitees++;

                        // Si la mémorisation des lignes du fichier est activée...on
                        // mémorise...

                        if (this.memorisationLignesFichier)
                        {
                            this.gestionMemorisationLignesFichier.memoriserLigne(ligneFichier);
                        }

                        String messageErreurControle =
                            this.controleFormatLigne(ligneFichierSource, fichierParametrage.getType().intValue(),
                                typeLigneFichier);
                        if (messageErreurControle != null)
                        {
                            // Si la ligne est malformée, on lève une erreur.
                            avancementTraitement =
                                pGestionnaire.anomalieExtraction(typeLigneFichier.getId(), numLigne, "",
                                    messageErreurControle, ligneFichier);
                            presenceAnomalieDansFichier = true;
                            // demande d'arrêt de traitement du fichier?
                            if (avancementTraitement == GestionnaireObjetExtrait.TRAITEMENT_ARRETER_FICHIER)
                            {
                                continuerTraitementFichier = false;
                            }
                        }

                        if (avancementTraitement == GestionnaireObjetExtrait.TRAITEMENT_CONTINUER)
                        {
                            // cas des
                            if (typeLigneFichier.getNombreDeLignes().intValue() > 1)
                            {
                                for (int i = 1; i < typeLigneFichier.getNombreDeLignes().intValue(); i++)
                                {
                                    numLigne++;
                                    ligneFichier = lecteurFichierSource.readLine();
                                    if (ligneFichier != null)
                                    {
                                        ligneFichierSource.ajouterLigneSupplementaire(ligneFichier);
                                    }
                                    else
                                    {
                                        avancementTraitement =
                                            pGestionnaire.anomalieExtraction(typeLigneFichier.getId(), numLigne, "",
                                                MessagesAnomalie.ERREUR_LIGNE_MULTILIGNE_ABSENTE, ligneFichierSource
                                                    .getContenuCompletLigne("\n"));
                                        presenceAnomalieDansFichier = true;
                                    }
                                }
                            }
                            if (avancementTraitement == GestionnaireObjetExtrait.TRAITEMENT_CONTINUER)
                            {
                                // Recherche des propriétés de la ligne qu'on souhaite récupérer pour affectation
                                // future
                                ligneFichierSource.initialiserProprietesAMapper(typeLigneFichier.getProprietes(),
                                    fichierParametrage);

                                // Le traitement de mapping retourne un objet indiquant si on doit continuer dans le
                                // cas d'ano et la liste d'objet extraits
                                RetourTraitement retourMapping =
                                    this.mappeLigneAvecClass(typeLigneFichier, ligneFichierSource, pGestionnaire);

                                if ((retourMapping != null)
                                    && ((retourMapping.getRetourTraitement() == GestionnaireObjetExtrait.TRAITEMENT_CONTINUER)))
                                {
                                    this.ligneAnalysee = typeLigneFichier;

                                    // Appel du gestionnaire passé en paramètre afin de lui indiquer qu'on vient
                                    // d'extraire un ou plusieurs objets.
                                    int retourTraitementExtraction =
                                        pGestionnaire.extractionObjet(retourMapping.getMapResultat(), typeLigneFichier
                                            .getId(), numLigne, retourMapping.isAnomalieDetectee(), ligneFichierSource
                                            .getContenuCompletLigne("\n"));

                                    // On arrête le traitement si c'est ce qui a été demandé lors de la détection d'ano
                                    continuerTraitementFichier =
                                        (retourMapping.getRetourTraitement() != GestionnaireObjetExtrait.TRAITEMENT_ARRETER_FICHIER)
                                            && (retourTraitementExtraction != GestionnaireObjetExtrait.TRAITEMENT_ARRETER_FICHIER);
                                }
                                else if (retourMapping != null
                                    && retourMapping.getRetourTraitement() == GestionnaireObjetExtrait.TRAITEMENT_ARRETER_FICHIER)
                                {
                                    // Demande d'arrêt du traitement suite à une anomalie rencontrée dans le fichier.
                                    continuerTraitementFichier = false;
                                }
                            }

                        }
                    }
                    else if (typeLigneFichier == null)
                    {
                        // Cas où le type de la ligne n'a pas été déterminé (ni en auto, ni en manuel).
                        // Dans ce cas un ano est levée pour l'utilisateur qui peut choisir de continuer ou d'arrêter
                        // le traitement
                        avancementTraitement =
                            pGestionnaire.anomalieExtraction(null, numLigne, "",
                                MessagesAnomalie.ERREUR_TYPE_LIGNE_INCONNU, ligneFichierSource
                                    .getContenuCompletLigne("\n"));
                        presenceAnomalieDansFichier = true;
                        continuerTraitementFichier =
                            (avancementTraitement == GestionnaireObjetExtrait.TRAITEMENT_CONTINUER);
                    }
                }
                /**
                 * else { // Si la ligne est à 1 et qu'on est ici, c'est que le fichier est vide. // On va lever une
                 * anomalie pour prévenir le développeur if (numLigne == 1) { avancementTraitement =
                 * pGestionnaire.anomalieExtraction(null, 0, "", MessagesAnomalie.ERREUR_FICHIER_VIDE, "");
                 * presenceAnomalieDansFichier = true; } }
                 **/
            }
            while ((ligneFichier != null) && (continuerTraitementFichier));

            log.debug("Le traitement du fichier s est correctement deroule : " + nbLignesReellementTraitees
                + "  lignes de fichier traitees (arrivee a la ligne " + numLigne + " du fichier).");

            pGestionnaire.finTraitementFichier(presenceAnomalieDansFichier);
            lecteurFichierSource.close();

        }
        catch (FileNotFoundException exc)
        {
            throw new IntegrationException("Integration : Erreur d'ouverture du fichier", exc);
        }
        catch (QueryException exc)
        {
            throw new IntegrationException("Integration : Erreur durant la lecture du fichier", exc);
        }
        catch (IOException exc)
        {
            throw new IntegrationException("Integration : Erreur durant la lecture du fichier", exc);
        }

    }

    /**
     * Retourne instance classe.
     * 
     * @param pCheminClasse le chemin classe
     * @return the object
     */
    private Object retourneInstanceClasse(String pCheminClasse)
    {
        Object instance = null;
        Class<?> classeJavaAttribut;

        try
        {
            classeJavaAttribut = ClassUtils.forName(pCheminClasse, null);
        }
        catch (ClassNotFoundException exc)
        {
            throw ApplicationExceptionTransformateur.transformer(exc.getMessage(),exc);
        }
        try
        {
            instance = classeJavaAttribut.newInstance();
        }
        catch (InstantiationException exc)
        {
            throw ApplicationExceptionTransformateur.transformer(exc.getMessage(),exc);
        }
        catch (IllegalAccessException exc)
        {
            throw ApplicationExceptionTransformateur.transformer(exc.getMessage(),exc);
        }

        return instance;

    }

    /**
     * methode Transformer nom attribut en setter : --.
     * 
     * @param pNomAttribut le nom attribut
     * @return string
     */
    private String transformerNomAttributEnSetter(String pNomAttribut)
    {

        String nomSetter = "set" + pNomAttribut.substring(0, 1).toUpperCase() + pNomAttribut.substring(1);
        return nomSetter;

    }

}
