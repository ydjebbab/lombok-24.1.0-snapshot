/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : JmxLog4jConfig.java
 *
 */
package fr.gouv.finances.lombok.util.log4j;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.LoggerContext;
import org.apache.logging.log4j.core.config.Configuration;
import org.apache.logging.log4j.core.config.LoggerConfig;
import org.springframework.boot.logging.LogLevel;

import fr.gouv.finances.lombok.util.base.BaseBean;

/**
 * Class JmxLog4jConfig
 * 
 * @author amleplatinec
 * @version $Revision: 1.2 $ Date: 11 déc. 2009
 */
public class JmxLog4jConfig extends BaseBean implements JmxLog4jConfigMbean
{

    private static final long serialVersionUID = 1L;

    public JmxLog4jConfig()
    {
        super();
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param category
     * @param level
     * @return string
     * @throws IllegalArgumentException the illegal argument exception
     * @see fr.gouv.finances.lombok.util.log4j.JmxLog4jConfigMbean#affecterUnLoggerAUneClasseOuUnPackage(java.lang.String,
     *      java.lang.String)
     */
    @Override    
    public String affecterUnLoggerAUneClasseOuUnPackage(String category, String level) throws IllegalArgumentException
    {
        Logger logger = LogManager.getLogger(category);
        StringBuilder msg = new StringBuilder();

        if (isLevelAutorise(level))
        {
            if (logger != null)
            {
                LoggerContext logContext = (LoggerContext) LogManager
                    .getContext(false);
                Configuration conf = logContext.getConfiguration();
                LoggerConfig loggerConfig = conf.getLoggerConfig(logger.getName());
                loggerConfig.setLevel(Level.toLevel(level));
                logContext.updateLoggers();

                msg.append("Le niveau de log de la classe ou du package ");
                msg.append(category);
                msg.append("est : ");
                msg.append(level);
            }
            else
            {
                msg.append("La classe ou le package ");
                msg.append(category);
                msg.append(" ne dispose pas de logger.");
            }
        }
        else
        {
            msg.append(level);
            msg.append(" n'est pas un niveau log4j valide.");
        }
        return msg.toString();
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @return levels
     * @see fr.gouv.finances.lombok.util.log4j.JmxLog4jConfigMbean#getLevels()
     */
    @Override
    public Object[] getLevels()
    {
        return LogLevel.values();
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @return packages ou classes disposant d un logger
     * @see fr.gouv.finances.lombok.util.log4j.JmxLog4jConfigMbean#getPackagesOuClassesDisposantDUnLogger()
     */
    @Override
    public Object[] getPackagesOuClassesDisposantDUnLogger()
    {
        LoggerContext logContext = (LoggerContext) LogManager.getContext(false);
        Map<String, LoggerConfig> loggers = logContext.getConfiguration()
            .getLoggers();

        List<String> result = new ArrayList<String>();
        for (Map.Entry<String, LoggerConfig> entry : loggers.entrySet())
        {
            LoggerConfig logConfig = entry.getValue();
            if (logConfig != null)
            {
                StringBuilder infologger = new StringBuilder();
                infologger.append(logConfig.getName());
                infologger.append(" (");
                infologger.append(logConfig.getLevel());
                infologger.append(')');

                result.add(infologger.toString());

            }
        }
        Collections.sort(result);
        return result.toArray();
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @return packages ou classes disposant d un logger defini
     * @see fr.gouv.finances.lombok.util.log4j.JmxLog4jConfigMbean#getPackagesOuClassesDisposantDUnLoggerDefini()
     */
    @Override
    public Object[] getPackagesOuClassesDisposantDUnLoggerDefini()
    {
        LoggerContext logContext = (LoggerContext) LogManager.getContext(false);
        Map<String, LoggerConfig> loggers = logContext.getConfiguration()
            .getLoggers();
        List<String> result = new ArrayList<String>();
        for (Map.Entry<String, LoggerConfig> entry : loggers.entrySet())
        {
            LoggerConfig logConfig = entry.getValue();
            if (logConfig != null && logConfig.getLevel() != null)
            {
                StringBuilder infologger = new StringBuilder();
                infologger.append(logConfig.getName());
                infologger.append(" (");
                infologger.append(logConfig.getLevel());
                infologger.append(')');

                result.add(infologger.toString());

            }
        }

        Collections.sort(result);
        return result.toArray();
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param packageOuClasse
     * @return string
     * @throws IllegalArgumentException the illegal argument exception
     * @see fr.gouv.finances.lombok.util.log4j.JmxLog4jConfigMbean#lireLeNiveauDeLogPourUneClasseOuUnPackaqe(java.lang.String)
     */
    @Override
    public String lireLeNiveauDeLogPourUneClasseOuUnPackaqe(String packageOuClasse) throws IllegalArgumentException
    {
        Logger logger = LogManager.getLogger(packageOuClasse);
        StringBuilder msg = new StringBuilder();

        if (logger == null)
        {
            msg.append("Le logger pour le package ou la classe");
            msg.append(packageOuClasse);
            msg.append("n'existe pas.");
        }
        else
        {
            msg.append(logger.getLevel().toString());
        }

        return msg.toString();
    }

    /**
     * Verifie si level autorise.
     * 
     * @param level --
     * @return true, si c'est level autorise
     */
    private boolean isLevelAutorise(String level)
    {
        boolean result = false;
        LogLevel[] log4jLogLevel = LogLevel.values();
        for (int i = 0; i < log4jLogLevel.length; i++)
        {
            LogLevel element = log4jLogLevel[i];
            if (element.name().equalsIgnoreCase(level))
            {
                result = true;
                break;
            }
        }
        return result;

    }

}
