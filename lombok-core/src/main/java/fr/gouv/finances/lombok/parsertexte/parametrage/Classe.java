/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 */
package fr.gouv.finances.lombok.parsertexte.parametrage;

import java.util.HashMap;
import java.util.Map;

/**
 * Class Classe --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.2 $ Date: 11 déc. 2009
 */
public class Classe
{

    // Identificateur de la classe, utilisé pour parler de cette classe
    // et de ses propriétés dans les autres paramétres
    /** id. */
    String id = null;

    // Chemin complet pour instancier la classe.
    /** chemin classe. */
    String cheminClasse = null;

    /** attributs. */
    Map attributs = null;

    /**
     * Instanciation de classe.
     * 
     * @param pId le id
     * @param pCheminClasse le chemin classe
     */
    public Classe(String pId, String pCheminClasse)
    {
        this.id = pId;
        this.cheminClasse = pCheminClasse;
    }

    /**
     * methode Ajouter attribut : --.
     * 
     * @param pAttribut le attribut
     */
    public void ajouterAttribut(AttributClasse pAttribut)
    {
        if (this.attributs == null)
        {
            this.attributs = new HashMap();
        }
        this.attributs.put(pAttribut.getNom(), pAttribut);
    }

    /**
     * methode Ajouter attribut : --.
     * 
     * @param pNom le nom
     * @param pType le type
     */
    public void ajouterAttribut(String pNom, String pType)
    {
        if (this.attributs == null)
        {
            this.attributs = new HashMap();
        }
        this.attributs.put(pNom, new AttributClasse(pNom, pType));
    }

    /**
     * Accesseur de l attribut attributs.
     * 
     * @return attributs
     */
    public Map getAttributs()
    {
        return attributs;
    }

    /**
     * Accesseur de l attribut chemin classe.
     * 
     * @return chemin classe
     */
    public String getCheminClasse()
    {
        return cheminClasse;
    }

    /**
     * Accesseur de l attribut id.
     * 
     * @return id
     */
    public String getId()
    {
        return id;
    }

    /**
     * methode Retourne attribut pour nom : --.
     * 
     * @param pNom le nom
     * @return attribut classe
     */
    public AttributClasse retourneAttributPourNom(String pNom)
    {
        return (AttributClasse) this.attributs.get(pNom);
    }

    /**
     * Modificateur de l attribut attributs.
     * 
     * @param attributs le nouveau attributs
     */
    public void setAttributs(Map attributs)
    {
        this.attributs = attributs;
    }

    /**
     * Modificateur de l attribut chemin classe.
     * 
     * @param cheminClasse le nouveau chemin classe
     */
    public void setCheminClasse(String cheminClasse)
    {
        this.cheminClasse = cheminClasse;
    }

    /**
     * Modificateur de l attribut id.
     * 
     * @param id le nouveau id
     */
    public void setId(String id)
    {
        this.id = id;
    }

}
