/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 *
 */
package fr.gouv.finances.lombok.util.persistance;

import java.util.Iterator;

/**
 * <p>
 * Interface permettant d'effectuer des recherches en mode curseur.<br/>
 * Cela permet d'éviter de charger en mémoire le résultat d'une requête en une seule fois.<br/>
 * </p>
 * <p>
 * Exemple:
 * </p>
 * <code>
 * ScrollIterator iterator = dao.getResultat();<br/>
 * while (iterator.hasNext())<br/>
 * {<br/>
 *   Object o = iterator.nextObjetMetier();<br/>
 *   [..]<br/>
 * }<br/>
 * iterator.close();<br/>
 *
 * </code>
 * <p>
 * La méthode close() doit être appelée explicitement, comme indiqué ci-dessus, pour relacher l'objet
 * ScrollableResults et le curseur associé de la base de données.
 * </p>
 *
 * @author chouard-cp
 * @author Christophe Breheret-Girardin
 */
public interface ScrollIterator extends Iterator<Object>
{

    /**
     * Méthode permettant de fermer d'arrêter la recherche.
     */
    void close();

    /**
     * Un autre objet est-il disponible ?
     *
     * @return true si un autre objet est disponible, false sinon.
     */

    boolean hasNext();

    /**
     * Méthode permettant de savoir si la recherche est arrêtée.
     *
     * @return true si la recherche est arrêtée, false sinon
     */

    boolean isClosed();

    /**
     * Retourne l'objet suivant.
     *
     * @return l'objet suivant
     */

    public Object next();

    /**
     * Pour les requêtes qui ne sont pas des projections, cette méthode permet de récupérer les objets métier.
     *
     * @return l'objet métier disponible
     */

    Object nextObjetMetier();

    /**
     * Cette méthode n'est pas supportée
     */
    @Deprecated
    void remove();
}
