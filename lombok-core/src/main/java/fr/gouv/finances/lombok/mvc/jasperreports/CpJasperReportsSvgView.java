/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 *
 */
package fr.gouv.finances.lombok.mvc.jasperreports;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporter;

import org.springframework.web.servlet.view.jasperreports.AbstractJasperReportsSingleFormatView;

import fr.gouv.finances.lombok.util.exception.ProgrammationException;

/**
 * Classe CpJasperReportsSvgView.
 * 
 * @author amleplatinec
 * @version $Revision: 1.2 $ Date: 11 déc. 2009
 */
public class CpJasperReportsSvgView extends AbstractJasperReportsSingleFormatView
{

    /**
     * Constructeur.
     */
    public CpJasperReportsSvgView()
    {
        setContentType("image/svg+xml;charset=utf-8");
    }

    /**
     * Surcharge de la méthode pour la rendre finale, pour que l'appel, au sein du constructeur,
     * ne soit plus une mauvaise pratique puisqu'elle ne peut plus ainsi être surchargée dans les
     * classes dérivées.
     *
     * @see org.springframework.web.servlet.view.AbstractView#setContentType(java.lang.String)
     */
    @Override
    public final void setContentType(String contentType)
    {
        super.setContentType(contentType);
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @return jR exporter
     * @see org.springframework.web.servlet.view.jasperreports.AbstractJasperReportsSingleFormatView#createExporter()
     */
    @Override
    protected JRExporter createExporter()
    {
        CpJRSvgExporter cpJRSvgExporter = null;
        try
        {
            cpJRSvgExporter = new CpJRSvgExporter();
        }
        catch (JRException e)
        {
            throw new ProgrammationException(e);
        }
        return cpJRSvgExporter;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @return true, si c'est vrai
     * @see org.springframework.web.servlet.view.jasperreports.AbstractJasperReportsSingleFormatView#useWriter()
     */
    @Override
    protected boolean useWriter()
    {
        return false;
    }
}
