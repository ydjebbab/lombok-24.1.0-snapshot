/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : Csvprinter.java
 *
 */
package fr.gouv.finances.lombok.util.csv;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.gouv.finances.lombok.util.exception.ApplicationExceptionTransformateur;
import fr.gouv.finances.lombok.util.exception.ProgrammationException;

/**
 * Class Csvprinter Ecrit des données au format csv.
 * 
 * @author chouard-cp
 * @version $Revision: 1.2 $ Date: 15 déc. 2009
 */
public class Csvprinter implements CsvPrint
{
    public static final String DELIMITEUR_ILLEGAL = "CE DELIMITEUR DE CHAMPS N'EST PAS AUTORISE";

    public static final String QUOTE_ILLEGAL = "CE DELIMITEUR DE PROTECTION N'EST PAS AUTORISE";

    public static final String DELIMITEUR_LIGNE_UNIX = "\n";

    public static final String DELIMITEUR_LIGNE_WINDOW = "\r\n";

    private static final String ENCODAGE_PAR_DEFAUT = "ISO-8859-1";

    protected Logger log = LoggerFactory.getLogger(getClass());

    /** Flag qui indique si le mode autoflush est actif. */
    protected boolean autoFlush = true;

    /** Flag qui indique si les champs doivent toujours être protégés. */
    protected boolean toujoursProtegerLesChamps = false;

    /**
     * Flag qui indique qu'aucun champ ne doit être protégé. Si ce paramètre est positionné à true, le paramètre
     * toujoursProtegerLeschamps n' aaucun effet
     */
    protected boolean ignorerLaProtectionDesChamps = false;

    /** Séparateur de champs. */
    protected char separateurDeChamp = ';';

    /** Séparateur de fin de ligne. */
    protected String delimiteurFinDeLigne = DELIMITEUR_LIGNE_WINDOW;

    /** Caractères de protection des champs. */
    protected char caractereProtectionChamp = '"';

    /** Writer où les données sont écrites. */
    protected Writer out;

    /** Flag qui indique si on commence juste une nouvelle ligne. */
    protected boolean nouvelleLigne = true;

    /** Caractère utilisée pour écrire des commentaires. */
    protected char caractereDebutCommentaire = '#';

    /** Charset utilisé pour l'encodage lors de l'écriture des données csv. */
    protected String encodage = ENCODAGE_PAR_DEFAUT;

    /**
     * Créer un Cvsprinter autoFlush false : la commande flush est n'est pas exécutée après chaque commande write
     * toujoursProtegerLesChamps false : les champs ne sont protégés par des quotes que si c'est pas nécessaire
     * caractereProtectionChamp : " delimiteurFinDeLigne : \r\n (windows) encodage : ISO-8859-1 separateurDeChamp : ;.
     * 
     * @param out stream utilisée pour écrire les données csv
     */
    public Csvprinter(OutputStream out)
    {
        super();
        this.createWriter(out, this.encodage);
    }

    /**
     * Créer un Cvsprinter.
     * 
     * @param out stream utilisée pour écrire les données csv
     * @param autoFlush true : la commande flush est exécutée après chaque commande write
     * @param toujoursProtegerLesChamps true : tous les champs sont protégés par des quotes même si ce n'est pas
     *        nécessaire
     * @param separateurDeChamp caractère séparateur de champs utilisé
     * @param delimiteurFinDeLigne caractères utilisés pour délimiter la fin d'une ligne
     * @param caractereProtectionChamp caractère utilisé pour protéger les champs
     * @param encodage charset d'encodage(UTF-8, ISO-8859-1, ISO-8859-15, US-ASCII, ...)
     */
    public Csvprinter(OutputStream out, boolean autoFlush, boolean toujoursProtegerLesChamps, char separateurDeChamp,
        String delimiteurFinDeLigne, char caractereProtectionChamp, String encodage)
    {
        super();
        this.autoFlush = autoFlush;
        this.toujoursProtegerLesChamps = toujoursProtegerLesChamps;
        this.separateurDeChamp = separateurDeChamp;
        this.delimiteurFinDeLigne = delimiteurFinDeLigne;
        this.caractereProtectionChamp = caractereProtectionChamp;
        this.encodage = encodage;
        this.createWriter(out, encodage);
    }

    /**
     * Créer un Cvsprinter autoFlush false : la commande flush est n'est pas exécutée après chaque commande write
     * toujoursProtegerLesChamps false : les champs ne sont protégés par des quotes que si c'est pas nécessaire
     * caractereProtectionChamp : " delimiteurFinDeLigne : \r\n (windows) encodage : ISO-8859-1.
     * 
     * @param out stream utilisée pour écrire les données csv
     * @param separateurDeChamp caractère séparateur de champs utilisé
     */
    public Csvprinter(OutputStream out, char separateurDeChamp)
    {
        super();
        this.separateurDeChamp = separateurDeChamp;
        this.createWriter(out, this.encodage);
    }

    /**
     * Créer un Cvsprinter autoFlush false : la commande flush est n'est pas exécutée après chaque commande write
     * toujoursProtegerLesChamps false : les champs ne sont protégés par des quotes que si c'est pas nécessaire
     * caractereProtectionChamp : " delimiteurFinDeLigne : \r\n (windows).
     * 
     * @param out stream utilisée pour écrire les données csv
     * @param separateurDeChamp caractère séparateur de champs utilisé
     * @param encodage charset d'encodage(UTF-8, ISO-8859-1, ISO-8859-15, US-ASCII, ...)
     */
    public Csvprinter(OutputStream out, char separateurDeChamp, String encodage)
    {
        super();
        this.separateurDeChamp = separateurDeChamp;
        this.encodage = encodage;
        this.createWriter(out, encodage);
    }

    /**
     * Créer un Cvsprinter autoFlush false : la commande flush est n'est pas exécutée après chaque commande write
     * toujoursProtegerLesChamps false : les champs ne sont protégés par des quotes que si c'est pas nécessaire
     * caractereProtectionChamp : ".
     * 
     * @param out stream utilisée pour écrire les données csv
     * @param separateurDeChamp caractère séparateur de champs utilisé
     * @param delimiteurFinDeLigne caractères utilisés pour délimiter la fin d'une ligne
     * @param encodage charset d'encodage(UTF-8, ISO-8859-1, ISO-8859-15, US-ASCII, ...)
     */
    public Csvprinter(OutputStream out, char separateurDeChamp, String delimiteurFinDeLigne, String encodage)
    {
        super();
        this.separateurDeChamp = separateurDeChamp;
        this.delimiteurFinDeLigne = delimiteurFinDeLigne;
        this.encodage = encodage;
        this.createWriter(out, encodage);
    }

    /**
     * Ferme le stream sous-jacent.
     */
    @Override
    public void close()
    {
        try
        {
            this.out.close();
        }
        catch (IOException iox)
        {
            this.throwIOError(iox);
        }
    }

    /**
     * Flush les données écrites sur le stream sous-jacent.
     */
    @Override
    public void flush()
    {
        try
        {
            this.out.flush();
        }
        catch (IOException iox)
        {
            this.throwIOError(iox);
        }
    }

    /**
     * Checks if is flag qui indique qu'aucun champ ne doit être protégé.
     *
     * @return the flag qui indique qu'aucun champ ne doit être protégé
     */
    public boolean isIgnorerLaProtectionDesChamps()
    {
        return ignorerLaProtectionDesChamps;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.util.csv.CsvPrint#setAutoFlush(boolean)
     */
    @Override
    public void setAutoFlush(boolean autoFlush)
    {
        this.autoFlush = autoFlush;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.util.csv.CsvPrint#setCaractereProtectionChamp(char)
     */
    @Override
    public void setCaractereProtectionChamp(char nouveauCaractereProtectionChamp)
    {
        if (nouveauCaractereProtectionChamp == '\n' || nouveauCaractereProtectionChamp == '\r'
            || nouveauCaractereProtectionChamp == this.separateurDeChamp
            || nouveauCaractereProtectionChamp == this.caractereProtectionChamp)
        {
            throw new ProgrammationException(QUOTE_ILLEGAL);
        }

        this.caractereProtectionChamp = nouveauCaractereProtectionChamp;
    }

    /**
     * Sets the flag qui indique qu'aucun champ ne doit être protégé.
     *
     * @param ignorerLaProtectionDesChamps the new flag qui indique qu'aucun champ ne doit être protégé
     */
    public void setIgnorerLaProtectionDesChamps(boolean ignorerLaProtectionDesChamps)
    {
        this.ignorerLaProtectionDesChamps = ignorerLaProtectionDesChamps;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.util.csv.CsvPrint#setSeparateurDeChamp(char)
     */
    @Override
    public void setSeparateurDeChamp(char nouveauSeparateurDeChamp)
    {
        if (nouveauSeparateurDeChamp == '\n' || nouveauSeparateurDeChamp == '\r'
            || nouveauSeparateurDeChamp == separateurDeChamp || nouveauSeparateurDeChamp == caractereProtectionChamp)
        {
            throw new ProgrammationException(DELIMITEUR_ILLEGAL);
        }

        separateurDeChamp = nouveauSeparateurDeChamp;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.util.csv.CsvPrint#setToujoursProtegerLeschamps(boolean)
     */
    @Override
    public void setToujoursProtegerLeschamps(boolean toujoursProteger)
    {
        this.toujoursProtegerLesChamps = toujoursProteger;
    }

    /**
     * Ecrit un champ sur la ligne en cours. Le champ est encadré par des quotes si nécessaire Si la valeur du champ
     * passée en paramètre est nulle, une chaîne vide est écrite.
     * 
     * @param valeurDuChamp valeur du champ à écrire.
     */
    @Override
    public void write(String valeurDuChamp)
    {
        try
        {

            String valeurDuChampAEcrire = (valeurDuChamp == null ? "" : valeurDuChamp);

            boolean quote = this.evaluateIfQuoteIsTobeUsed(valeurDuChampAEcrire);

            if (this.nouvelleLigne)
            {
                this.nouvelleLigne = false;
            }
            else
            {
                this.out.write(separateurDeChamp);
            }

            if (quote)
            {
                this.out.write(escapeAndQuote(valeurDuChampAEcrire));
            }
            else
            {
                this.out.write(valeurDuChampAEcrire);
            }

            if (this.autoFlush)
            {
                this.flush();
            }
        }
        catch (IOException iox)
        {
            this.throwIOError(iox);
        }
    }

    /**
     * Ecrit une ligne composée de champs séparés par un caractère de délimitation. Le séparateur de fin de ligne n'est
     * pas écrit. Les valeurs des champs sont encadrées par des quotes si nécessaires. Certains caractères sont protégés
     * si nécessaire
     * 
     * @param valeursDesChamps valeurs des champs à écrire
     */
    @Override
    public void write(String[] valeursDesChamps)
    {
        for (int i = 0; i < valeursDesChamps.length; i++)
        {
            write(valeursDesChamps[i]);
        }
    }

    /**
     * Ecrit le caractère de fin de ligne. Si le mode autoFlush est actif flush la ligne en cours
     */
    @Override
    public void writeln()
    {
        try
        {
            this.out.write(this.delimiteurFinDeLigne);
            if (this.autoFlush)
            {
                flush();
            }
            this.nouvelleLigne = true;
        }
        catch (IOException iox)
        {
            this.throwIOError(iox);
        }
    }

    /**
     * Ecrit le dernier champ d'une ligne. La chaîne est encadrée de quotes si nécessaire
     * 
     * @param valeurDuChamp valeur du champ à écrire
     */
    @Override
    public void writeln(String valeurDuChamp)
    {
        this.write(valeurDuChamp);
        this.writeln();
    }

    /**
     * Ecrit une ligne composée de champs séparés par un caractère de délimitation. Les valeurs des champs sont
     * encadrées par des quotes si nécessaires. Certains caractères sont protégés si nécessaire
     * 
     * @param valeursDesChamps valeurs des champs à écrire
     */
    @Override
    public void writeln(String[] valeursDesChamps)
    {
        this.write(valeursDesChamps);
        this.writeln();
    }

    /**
     * Ecrit plusieurs lignes composées de champs séparés par un caractère de délimitation. Les valeurs des champs sont
     * encadrées par des quotes si nécessaires. Certains caractères sont protégés si nécessaire
     * 
     * @param tableauValeursDesChamps --
     */
    public void writeln(String[][] tableauValeursDesChamps)
    {
        for (int i = 0; i < tableauValeursDesChamps.length; i++)
        {
            writeln(tableauValeursDesChamps[i]);
        }

        if (tableauValeursDesChamps.length == 0)
        {
            writeln();
        }
    }

    /**
     * Ecrit un commentaire dans le fichier cvs Les commentaires commencent toujours sur une nouvelle ligne et occupent
     * la ligne entière.
     * 
     * @param commentaire le commentaire à écrire
     */
    @Override
    public void writelnComment(String commentaire)
    {
        try
        {
            String commentaireAEcrire = (commentaire == null ? "" : commentaire);

            // On démarre une nouvelle ligne si la ligne en cours
            // n'est pas terminée
            if (!nouvelleLigne)
            {
                this.writeln();
            }

            this.out.write(this.caractereDebutCommentaire);
            this.out.write(' ');

            // Ecriture du commentaire passé en paramètre
            // Les caractères de fin de ligne sont recherchés dans
            // le commentaire pour placer le symbole indiquant qu'il
            // s'agit d'un commentaire en début de chaque ligne
            for (int i = 0; i < commentaireAEcrire.length(); i++)
            {
                char c = commentaireAEcrire.charAt(i);
                switch (c)
                {
                    case '\r':
                    {
                        if (i + 1 < commentaireAEcrire.length() && commentaireAEcrire.charAt(i + 1) == '\n')
                        {
                            i++;
                        }
                    }
                    // pas de break intentionnel
                    case '\n':
                    {
                        this.writeln();
                        this.out.write(this.caractereDebutCommentaire);
                        this.out.write(' ');
                    }
                        break;
                    default:
                    {
                        this.out.write(c);
                    }
                        break;
                }
            }
            this.writeln();
        }
        catch (IOException iox)
        {
            this.throwIOError(iox);
        }
    }

    /**
     * Création du writer utilisé pour écrire les données csv.
     * 
     * @param out --
     * @param charset --
     */
    private void createWriter(OutputStream out, String charset)
    {
        try
        {
            this.out = new OutputStreamWriter(out, charset);
        }
        catch (UnsupportedEncodingException exception)
        {
            log.warn("LE CHARSET " + charset + "N'EST PAS SUPPORTE", exception);
            throw ApplicationExceptionTransformateur.transformer(exception);
        }
    }

    /**
     * Encadre la valeur d'un champ par des quotes et protège les délimiteurs de champs et les quotes contenus dans le
     * champ.
     * 
     * @param valeurDuChamp la valeur du champ
     * @return la valeur du champ encadré de quotes et dont certains caractères sont protégés
     */
    private String escapeAndQuote(String valeurDuChamp)
    {
        int count = 2;
        for (int i = 0; i < valeurDuChamp.length(); i++)
        {
            char car = valeurDuChamp.charAt(i);
            switch (car)
            {
                case '\n':
                    // pas de break intentionnel
                case '\r':
                    // pas de break intentionnel
                case '\\':
                {
                    count++;
                }
                    break;
                default:
                {
                    if (car == caractereProtectionChamp)
                    {
                        count++;
                    }
                }
                    break;
            }
        }

        StringBuilder sbf = new StringBuilder(valeurDuChamp.length() + count);
        sbf.append(this.caractereProtectionChamp);

        for (int i = 0; i < valeurDuChamp.length(); i++)
        {
            char car = valeurDuChamp.charAt(i);
            switch (car)
            {
                case '\n':
                {
                    sbf.append("\\n");
                }
                    break;
                case '\r':
                {
                    sbf.append("\\r");
                }
                    break;
                case '\\':
                {
                    sbf.append("\\\\");
                }
                    break;
                default:
                {
                    if (car == caractereProtectionChamp)
                    {
                        sbf.append("\\" + caractereProtectionChamp);
                    }
                    else
                    {
                        sbf.append(car);
                    }
                }
            }
        }

        sbf.append(this.caractereProtectionChamp);
        return sbf.toString();
    }

    /**
     * methode Evaluate if quote is tobe used : --.
     * 
     * @param valeurDuChampAEcrire --
     * @return true, si c'est vrai
     */
    private boolean evaluateIfQuoteIsTobeUsed(String valeurDuChampAEcrire)
    {
        boolean quote = false;

        if (this.ignorerLaProtectionDesChamps)
        {
            quote = false;
        }
        else if (this.toujoursProtegerLesChamps || this.nouvelleLigne)
        {
            quote = true;
            // Pour une nouvelle ligne, encadre toujours par des quotes le premier champ
            // d'une ligne s'il est vide car il pourrait être le seul champ de la ligne.
            // Dans ce cas, si le champ n'était pas encadré de quotes il n'y aurait aucun champ
            // dans la ligne.
        }
        else if (valeurDuChampAEcrire.length() > 0)
        {
            char c = valeurDuChampAEcrire.charAt(0);
            if (this.nouvelleLigne && (c < '0' || (c > '9' && c < 'A') || (c > 'Z' && c < 'a') || (c > 'z')))
            {
                quote = true;
            }
            if (c == ' ' || c == '\f' || c == '\t')
            {
                quote = true;
            }

            for (int i = 0; i < valeurDuChampAEcrire.length(); i++)
            {
                c = valeurDuChampAEcrire.charAt(i);
                if (c == this.caractereProtectionChamp || c == this.separateurDeChamp || c == '\n' || c == '\r')
                {
                    quote = true;
                }
            }

            if (c == ' ' || c == '\f' || c == '\t')
            {
                quote = true;
            }
        }

        return quote;
    }

    /**
     * Trace une erreur d'entrée / sortie et lève l'exception correspondante.
     * 
     * @param exception --
     */
    private void throwIOError(Exception exception)
    {
        log.warn("ERREUR D'ENTREE / SORTIE", exception);
        throw ApplicationExceptionTransformateur.transformer(exception);
    }
}
