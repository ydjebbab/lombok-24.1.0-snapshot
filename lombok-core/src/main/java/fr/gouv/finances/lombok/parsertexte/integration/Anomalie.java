/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 */
package fr.gouv.finances.lombok.parsertexte.integration;

/**
 * Class Anomalie --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.2 $ Date: 11 déc. 2009
 */
public class Anomalie
{

    /** numero ligne. */
    int numeroLigne = -1;

    /** attribut. */
    private String attribut = null;

    /** message erreur. */
    private String messageErreur = null;

    /** id classe destination. */
    private String idClasseDestination = null;

    /**
     * Instanciation de anomalie.
     * 
     * @param attribut --
     * @param messageErreur --
     * @param idClasseDestination --
     * @param numeroLigne --
     */
    public Anomalie(String attribut, String messageErreur, String idClasseDestination, int numeroLigne)
    {
        super();
        this.attribut = attribut;
        this.messageErreur = messageErreur;
        this.idClasseDestination = idClasseDestination;
        this.numeroLigne = numeroLigne;
    }

    /**
     * Accesseur de l attribut attribut.
     * 
     * @return attribut
     */
    public String getAttribut()
    {
        return attribut;
    }

    /**
     * Accesseur de l attribut id classe destination.
     * 
     * @return id classe destination
     */
    public String getIdClasseDestination()
    {
        return idClasseDestination;
    }

    /**
     * Accesseur de l attribut message erreur.
     * 
     * @return message erreur
     */
    public String getMessageErreur()
    {
        return messageErreur;
    }

    /**
     * Accesseur de l attribut numero ligne.
     * 
     * @return numero ligne
     */
    public int getNumeroLigne()
    {
        return numeroLigne;
    }

    /**
     * Modificateur de l attribut attribut.
     * 
     * @param attribut le nouveau attribut
     */
    public void setAttribut(String attribut)
    {
        this.attribut = attribut;
    }

    /**
     * Modificateur de l attribut id classe destination.
     * 
     * @param idClasseDestination le nouveau id classe destination
     */
    public void setIdClasseDestination(String idClasseDestination)
    {
        this.idClasseDestination = idClasseDestination;
    }

    /**
     * Modificateur de l attribut message erreur.
     * 
     * @param messageErreur le nouveau message erreur
     */
    public void setMessageErreur(String messageErreur)
    {
        this.messageErreur = messageErreur;
    }

    /**
     * Modificateur de l attribut numero ligne.
     * 
     * @param numeroLigne le nouveau numero ligne
     */
    public void setNumeroLigne(int numeroLigne)
    {
        this.numeroLigne = numeroLigne;
    }

}
