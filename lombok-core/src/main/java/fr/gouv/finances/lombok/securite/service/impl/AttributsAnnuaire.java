/*
 * Copyright (c) 2014 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.securite.service.impl;

/**
 * Enumération des attributs renvoyés par l'annuaire
 *
 * @author Christophe Breheret-Girardin
 */
enum AttributsAnnuaire
{
    /****************************************/
    /** Attributs historiques de l'annuaire */
    /****************************************/

    /** Identifiant technique de l'agent (identifiant ministériel suffixé par "-0"). */
    UID,
    /** Affectation de la personne. */
    AFFECTATION,
    ANNEXE,
    BUSINESSCATEGORY,
    /** Nom de la personne à afficher. */
    CN,
    CODEFONCTION,
    /** Code SIRH du grade AGORA de l’agent. */
    CODEGRADE,
    /** Département géographique. */
    DEPARTEMENT,
    /** Département profesionnel d'affectation. */
    DEPARTMENT,
    /** Code fonction. */
    FONCTION,
    /** Prénom usuel de l'agent. */
    GIVENNAME,
    /** Identifiant DGI. */
    IDENTDGI,
    /** Adresse de messagerie électronique calculée à partir du prénom et nom d'usage. */
    MAIL,
    MAILDELIVERYOPTION,
    MEFIPOINTMAJF,
    /** Lien LDAP vers la structure d'affectation principale de l'agent. */
    OUCOMPLET,
    /** Sigle des structures. */
    OUSIGLE,
    /** Libellé long de la structure d'affectation principale de l'agent. */
    OUDESCRIPTION,
    /** Civilité de la personne. */
    PERSONALTITLE,
    /** Bureau distributeur. */
    PHYSICALDELIVERYOFFICENAME,
    /** Adresse postale. */
    POSTALADDRESS,
    /** Code postal. */
    POSTALCODE,
    REGION,
    /** Code SAGES. */
    SAGES,
    ROOMNUMBER,
    /** Nom usuel de l'agent. */
    SN,
    /** Numéro de téléphone. */
    TELEPHONENUMBER,
    TITLE,
    /** Habilitation au format court (code de l'application + "," + profil). */
    PROFILAPPLICATIF,

    /******************/
    /** Attribut DGI */
    /******************/

    /** Habilitations. */
    MEFIAPPLIDGI,
    
    /******************/
    /** Attribut DGCP */
    /******************/

    MEFIAPPLIDGCP,
    /** Habilitations. */
    MEFIAPPLIHABILITDGCP,

    /**********************************************/
    /** Nouveaux attributs de l'annuaire fusionné */
    /**********************************************/

    /** Identifiant utilisé pour l'authentification lors de l'accès aux applications. */
    UIDFONCTIONNEL,
    /** Identifiant de l'agent dans le SIRH AGORA. */
    UIDRH,
    /** Mot de passe compatible Windows. */
    SAMBANTPASSWORD,
    /** Date de validité du mot de passe. */
    PASSWORDEXPIRATIONTIME,
    /** Code SIRH du grade AGORA de l’agent. */
    CODEGRADERH,
    /** Indicateur booléen indiquant si l’agent est un employé supérieur ou non. */
    EMPSUP,
    /** Code categorie de l'agent (A, B, ...). */
    CATEGORIE,
    /** Indicateur du niveau de responsabilité de l’agent. */
    NIVEAURESPONSABILITE,
    /** Identifiant ARTEMIS de la structure d'affectation principale de l'agent. */
    OU,
    /** Codique de la structure d’affectation principale de l’agent. */
    AFFECTATIONCODIQUE,
    /** Code annexe de la structure d’affectation principale de l’agent. */
    AFFECTATIONANNEXE,
    /** Code SAGES de la structure d’affectation principale de l’agent. */
    AFFECTATIONSAGES,
    /** Affectations secondaires de l’agent. */
    AFFECTATIONSSECONDAIRES,
    /** Habilitation au format long. */
    PROFILAPPLICATIFLONG,
    /** Informations sur les certificats détenus par l’agent. */
    USERCERTIFICATE,
    /** Mot de passe standard LDAP chiffré en SSHA. */
    USERPASSWORD,
    /** Phrase secrète associée au certificat, chiffrée en base 64. */
    QUESTIONREPONSE,

    /***************************************************************************/
    /** Nouveaux attributs liés à la migration SIRHIUS en remplacement d'AGORA */
    /***************************************************************************/

    /** Identifiant de l’agent dans le SIRH. */
    UIDSIRHIUS,
    /** Code SIRH du grade SIRHIUS de l’agent. */
    CODEGRADESIRHIUS,
    /** Code du CSRH territorial. */
    CODECSRH,

    /*****************************/
    /** Cas annuaire des usagers */
    /*****************************/

    /** Numéro d'identification fiscal de l'usager. */
    SPI,
    /** Type de certificat. */
    TYPECERTIFICAT,

    /*****************************/
    /** Cas annuaire des usagers */
    /*****************************/

    /** identifiants d'organisme. */
    IDORGANISME;
}
