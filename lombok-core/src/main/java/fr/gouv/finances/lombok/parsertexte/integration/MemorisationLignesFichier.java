/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 */
package fr.gouv.finances.lombok.parsertexte.integration;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Class MemorisationLignesFichier --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.2 $ Date: 11 déc. 2009
 */
public class MemorisationLignesFichier
{

    /** liste ligne. */
    private final List listeLigne;

    /**
     * Instanciation de memorisation lignes fichier.
     */
    public MemorisationLignesFichier()
    {
        this.listeLigne = new ArrayList();
    }

    /**
     * methode Effacer lignes memorisees : --.
     */
    public void effacerLignesMemorisees()
    {
        this.listeLigne.clear();
    }

    /**
     * methode Memoriser ligne : --.
     * 
     * @param pLigne le ligne
     */
    public void memoriserLigne(String pLigne)
    {
        this.listeLigne.add(pLigne);
    }

    /**
     * methode Retourner lignes memorisees sous forme string : --.
     * 
     * @return string
     */
    public String retournerLignesMemoriseesSousFormeString()
    {
        int tailleTotaleChaine = 0;
        Iterator iter = this.listeLigne.iterator();
        // Calcul de la taille totale de la chaine de caractères a retourner car
        // il vaut mieux éviter trop de concaténations qui sont désastreuses pour les performances
        while (iter.hasNext())
        {
            String tmp = (String) iter.next();
            // Ajout de 2 pour le CR/LF
            tailleTotaleChaine += tmp.length() + 2;
        }

        StringBuilder   chaineConcat = new StringBuilder(tailleTotaleChaine);
        iter = this.listeLigne.iterator();
        while (iter.hasNext())
        {
            String tmp = (String) iter.next();
            chaineConcat.append(tmp).append('\n');
        }

        return chaineConcat.toString();
    }

}
