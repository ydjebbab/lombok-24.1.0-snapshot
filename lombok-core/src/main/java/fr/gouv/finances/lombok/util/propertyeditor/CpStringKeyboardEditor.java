/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : CpStringKeyboardEditor.java
 *
 */
package fr.gouv.finances.lombok.util.propertyeditor;

import java.beans.PropertyEditorSupport;
import java.util.HashMap;
import java.util.Map;

/**
 * Class CpStringKeyboardEditor Property editor qui supprime les caractères qui ne sont pas sur un clavier azerty.
 * Optionnellement, permet de transformer une chaine vide en une valeur nulle
 * 
 * @author chouard-cp
 * @version $Revision: 1.2 $ Date: 15 déc. 2009
 */
public class CpStringKeyboardEditor extends PropertyEditorSupport
{

    /** Constant : MSG_ERREUR. */
    private static final String MSG_ERREUR = "La chaîne doit contenir au moins un caractère alphanumérique";

    /** Constant : VALID_ALPHANUM_CHAR. */
    private static final char[] VALID_ALPHANUM_CHAR =
    {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L',
            'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
            'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'};

    /** Constant : VALID_KEYBOARD_CHAR. */
    private static final char[] VALID_KEYBOARD_CHAR =
    {'!', '"', '#', '$', '%', '&', '\'', '(', ')', '*', '+', ',', '-', '.', '/', '0', '1', '2', '3', '4', '5', '6',
            '7', '8', '9', ':', ';', '<', '=', '>', '?', '@', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J',
            'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '[', '\\', '[', '^',
            '_', '`', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r',
            's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '{', '|', '}', '~', '\u00A1', '\u00A2', '\u00A3', '\u00A4',
            '\u00A5', '\u00A6', '\u00A7', '\u00A8', '\u00A9', '\u00AA', '\u00AB', '\u00AC', '\u00AE', '\u00AF',
            '\u00B0', '\u00B1', '\u00B2', '\u00B3', '\u00B4', '\u00B5', '\u00B6', '\u00B7', '\u00B8', '\u00B9',
            '\u00BA', '\u00BB', '\u00BC', '\u00BE', '\u00BF', '\u00C0', '\u00C1', '\u00C2', '\u00C3', '\u00C4',
            '\u00C5', '\u00C6', '\u00C7', '\u00C8', '\u00C9', '\u00CA', '\u00CB', '\u00CC', '\u00CE', '\u00CF',
            '\u00D0', '\u00D1', '\u00D2', '\u00D3', '\u00D4', '\u00D5', '\u00D6', '\u00D7', '\u00D8', '\u00D9',
            '\u00DA', '\u00DB', '\u00DC', '\u00DE', '\u00DF', '\u00E0', '\u00E1', '\u00E2', '\u00E3', '\u00E4',
            '\u00E5', '\u00E6', '\u00E7', '\u00E8', '\u00E9', '\u00EA', '\u00EB', '\u00EC', '\u00EE', '\u00EF',
            '\u00F0', '\u00F1', '\u00F2', '\u00F3', '\u00F4', '\u00F5', '\u00F6', '\u00F7', '\u00F8', '\u00F9',
            '\u00FA', '\u00FB', '\u00FC', '\u00FE', '\u00FF', '\u0153', '€', ' ', ']'};

    /** Constant : VALID_PARAGRAPH_SEPARATOR_CHAR. */
    private static final char[] VALID_PARAGRAPH_SEPARATOR_CHAR = {'\r', '\n'};

    /** empty as null. */
    private final boolean emptyAsNull;

    /** trim. */
    private final boolean trim;

    /** accept paragraph separator. */
    private final boolean acceptParagraphSeparator;

    /** require one alpha numeric character. */
    private final boolean requireOneAlphaNumericCharacter;

    /** keyboard char map. */
    private final Map<String, String> keyboardCharMap = new HashMap<String, String>();

    /** separator char map. */
    private final Map<String, String> separatorCharMap = new HashMap<String, String>();

    /** alpha num char map. */
    private final Map<String, String> alphaNumCharMap = new HashMap<String, String>();

    /**
     * Creation d'une nouvelle instance de CpStringKeyboardEditor.
     * 
     * @param emptyAsNull <code>true</code> si une chaine vide doit être transformée en <code>null</code>
     * @param trim <code>true</code> pour supprimer les caractères blancs en début et en fin de chaîne
     * @param acceptParagraphSeparator <code>true</code> pour autoriser les sauts de paragraphe
     */
    public CpStringKeyboardEditor(final boolean emptyAsNull, final boolean trim, final boolean acceptParagraphSeparator)
    {
        super();
        this.emptyAsNull = emptyAsNull;
        this.trim = trim;
        this.acceptParagraphSeparator = acceptParagraphSeparator;
        this.requireOneAlphaNumericCharacter = false;

        for (int i = 0; i < VALID_KEYBOARD_CHAR.length; i++)
        {
            this.keyboardCharMap.put(Integer.toHexString(VALID_KEYBOARD_CHAR[i]), null);
        }

        for (int i = 0; i < VALID_PARAGRAPH_SEPARATOR_CHAR.length; i++)
        {
            this.separatorCharMap.put(Integer.toHexString(VALID_PARAGRAPH_SEPARATOR_CHAR[i]), null);
        }

        if (this.requireOneAlphaNumericCharacter)
        {
            for (int i = 0; i < VALID_ALPHANUM_CHAR.length; i++)
            {
                this.alphaNumCharMap.put(Integer.toHexString(VALID_ALPHANUM_CHAR[i]), null);
            }
        }

    }

    /**
     * Creation d'une nouvelle instance de CpStringKeyboardEditor.
     * 
     * @param emptyAsNull <code>true</code> si une chaine vide doit être transformée en <code>null</code>
     * @param trim <code>true</code> pour supprimer les caractères blancs en début et en fin de chaîne
     * @param acceptParagraphSeparator <code>true</code> pour autoriser les sauts de paragraphe
     * @param requireOneAlphaNumericCharacter <code>true</code> pour refuser les chaînes de texte qui ne contiennent pas
     *        au moins un caractère alphanumérique
     */
    public CpStringKeyboardEditor(final boolean emptyAsNull, final boolean trim,
        final boolean acceptParagraphSeparator, final boolean requireOneAlphaNumericCharacter)
    {
        super();
        this.emptyAsNull = emptyAsNull;
        this.trim = trim;
        this.acceptParagraphSeparator = acceptParagraphSeparator;
        this.requireOneAlphaNumericCharacter = requireOneAlphaNumericCharacter;

        for (int i = 0; i < VALID_KEYBOARD_CHAR.length; i++)
        {
            this.keyboardCharMap.put(Integer.toHexString(VALID_KEYBOARD_CHAR[i]), null);
        }

        for (int i = 0; i < VALID_PARAGRAPH_SEPARATOR_CHAR.length; i++)
        {
            this.separatorCharMap.put(Integer.toHexString(VALID_PARAGRAPH_SEPARATOR_CHAR[i]), null);
        }

        if (this.requireOneAlphaNumericCharacter)
        {
            for (int i = 0; i < VALID_ALPHANUM_CHAR.length; i++)
            {
                this.alphaNumCharMap.put(Integer.toHexString(VALID_ALPHANUM_CHAR[i]), null);
            }
        }

    }

    /**
     * Transforme un objet en chaîne de caractères.
     * 
     * @return the as text
     */
    public String getAsText()
    {
        Object value = getValue();
        return (value != null ? value.toString() : "");
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param text le nouveau as text
     * @see java.beans.PropertyEditorSupport#setAsText(java.lang.String)
     */
    public void setAsText(String text)
    {

        String epureValue = this.replaceInvalidCharWithBlank(text);

        if (this.trim)
        {
            epureValue = this.supprimeBlancsDebutFin(epureValue);
        }
        /*
         * else { epureValue = epureValue; }
         */

        if (this.emptyAsNull && "".equals(epureValue))
        {
            epureValue = null;
        }
        /*
         * else { epureValue = epureValue; }
         */

        if (this.requireOneAlphaNumericCharacter && !this.emptyAsNull && epureValue != null)
        {
            this.testSiLaChaineContientAuMoinsUnCarAlphaNum(epureValue);
        }

        setValue(epureValue);
    }

    /**
     * Teste si un caractère est alphanumérique.
     * 
     * @param caractere --
     * @return true, if checks if is alpha char
     */
    private boolean isAlphaChar(char caractere)
    {
        return (this.alphaNumCharMap.containsKey(Integer.toHexString(caractere)));
    }

    /**
     * Teste si un caractère est autorisé.
     * 
     * @param caractere --
     * @return true, if checks if is valid char
     */
    private boolean isValidChar(char caractere)
    {
        return (this.keyboardCharMap.containsKey(Integer.toHexString(caractere)) || (this.acceptParagraphSeparator && this.separatorCharMap
            .containsKey(Integer.toHexString(caractere))));
    }

    /**
     * Supprime les caractères de la chaîne qui ne sont pas dans la liste des caractères autorisés.
     * 
     * @param text --
     * @return the string
     */
    private String replaceInvalidCharWithBlank(String text)
    {
        StringBuilder cleanValue = new StringBuilder();

        for (int i = 0; i < text.length(); i++)
        {
            char codeChar = text.charAt(i);

            if (isValidChar(codeChar))
            {
                cleanValue.append(codeChar);
            }
            else
            {
                cleanValue.append(' ');
            }
        }

        return cleanValue.toString();
    }

    /**
     * Supprime les blancs en début et en fin de chaîne.
     * 
     * @param text --
     * @return the string
     */
    private String supprimeBlancsDebutFin(String text)
    {
        return (text != null ? text.trim() : text);
    }

    /**
     * Teste si une chaîne contient au moins un caractère alphanumérique.
     * 
     * @param text --
     */
    private void testSiLaChaineContientAuMoinsUnCarAlphaNum(String text)
    {
        boolean result = false;

        for (int i = 0; i < text.length(); i++)
        {
            char codeChar = text.charAt(i);
            if (this.isAlphaChar(codeChar))
            {
                result = true;
                break;
            }
        }

        if (!result)
        {
            throw new IllegalArgumentException(MSG_ERREUR);
        }
    }

}
