/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 */
package fr.gouv.finances.lombok.parsertexte.integration;

import java.io.File;
import java.io.InputStream;
import java.net.URL;

import org.springframework.web.multipart.MultipartFile;

import fr.gouv.finances.lombok.parsertexte.parametrage.LigneFichier;

/**
 * Interface du gestionnaire d'importation de fichier
 * 
 * @author Christophe Breheret-Girardin
 */
public interface IGestionnaireImportationFichiers
{

    /**
     * Accesseur de ligne analysee.
     *
     * @return l'élément ligne analysee
     */
    LigneFichier getLigneAnalysee();

    /**
     * Vérifie si memorisation lignes fichier.
     *
     * @return l'élément memorisation lignes fichier
     */
    boolean isMemorisationLignesFichier();

    /**
     * Lecture du fichier de données pour lequel on a spécifié auparavant les fichiers de paramétrage.
     * 
     * @param pFichier le fichier
     * @param pGestionnaire le gestionnaire
     * @throws IntegrationException l'élément integration exception
     */
    void lectureEtDecodageFichier(File pFichier, GestionnaireObjetExtrait pGestionnaire)
        throws IntegrationException;

    /**
     * Lecture du fichier de données pour lequel on a spécifié auparavant les fichiers de paramétrage.
     * 
     * @param pFichier le fichier
     * @param pGestionnaire le gestionnaire
     * @param pNumLigneReprise le num ligne reprise
     * @throws IntegrationException l'élément integration exception
     */
    void lectureEtDecodageFichier(File pFichier, GestionnaireObjetExtrait pGestionnaire, long pNumLigneReprise)
        throws IntegrationException;

    /**
     * Lecture du fichier de données pour lequel on a spécifié auparavant les fichiers de paramétrage.
     * 
     * @param flotEntree --
     * @param pGestionnaire le gestionnaire
     * @throws IntegrationException l'élément integration exception
     */
    void lectureEtDecodageFichier(InputStream flotEntree, GestionnaireObjetExtrait pGestionnaire)
        throws IntegrationException;

    /**
     * Lecture du fichier de données pour lequel on a spécifié auparavant les fichiers de paramétrage.
     * 
     * @param flotEntree --
     * @param pGestionnaire le gestionnaire
     * @param pNumLigneReprise le num ligne reprise
     * @throws IntegrationException l'élément integration exception
     */
    void lectureEtDecodageFichier(InputStream flotEntree, GestionnaireObjetExtrait pGestionnaire,
        long pNumLigneReprise) throws IntegrationException;

    /**
     * Lecture du fichier de données pour lequel on a spécifié auparavant les fichiers de paramétrage.
     * 
     * @param pFichierMultiPart le fichier multi part
     * @param pGestionnaire le gestionnaire
     * @throws IntegrationException l'élément integration exception
     */
    void lectureEtDecodageFichier(MultipartFile pFichierMultiPart, GestionnaireObjetExtrait pGestionnaire)
        throws IntegrationException;

    /**
     * Lecture du fichier de données pour lequel on a spécifié auparavant les fichiers de paramétrage.
     * 
     * @param pFichierMultiPart le fichier multi part
     * @param pGestionnaire le gestionnaire
     * @param pNumLigneReprise le num ligne reprise
     * @throws IntegrationException l'élément integration exception
     */
    void lectureEtDecodageFichier(MultipartFile pFichierMultiPart, GestionnaireObjetExtrait pGestionnaire,
        long pNumLigneReprise) throws IntegrationException;

    /**
     * Lecture du fichier de données pour lequel on a spécifié auparavant les fichiers de paramétrage.
     * 
     * @param pRepertoireEtNomFichier le repertoire et nom fichier
     * @param pGestionnaire le gestionnaire
     * @param pNumLigneReprise le num ligne reprise
     * @throws IntegrationException l'élément integration exception
     */
    void lectureEtDecodageFichier(String pRepertoireEtNomFichier, GestionnaireObjetExtrait pGestionnaire,
        long pNumLigneReprise) throws IntegrationException;

    /**
     * Lecture du fichier de données pour lequel on a spécifié auparavant les fichiers de paramétrage.
     * 
     * @param pRepertoire le repertoire
     * @param pNomFichier le nom fichier
     * @param pGestionnaire le gestionnaire
     * @throws IntegrationException l'élément integration exception
     */
    void lectureEtDecodageFichier(String pRepertoire, String pNomFichier, GestionnaireObjetExtrait pGestionnaire)
        throws IntegrationException;

    /**
     * Lecture du fichier de données pour lequel on a spécifié auparavant les fichiers de paramétrage.
     * 
     * @param pRepertoire le repertoire
     * @param pNomFichier le nom fichier
     * @param pGestionnaire le gestionnaire
     * @param pNumLigneReprise le num ligne reprise
     * @throws IntegrationException l'élément integration exception
     */
    void lectureEtDecodageFichier(String pRepertoire, String pNomFichier,
        GestionnaireObjetExtrait pGestionnaire, long pNumLigneReprise) throws IntegrationException;

    /**
     * Cette méthode réalise la lecture du fichier de paramétrage contenant la description du fichier et le mapping à
     * réaliser entre ce fichier et les classes.
     * 
     * @param urlFichierParamParseurTexteFormat --
     * @param urlFichierParamParseurTexteClasses --
     * @throws IntegrationException l'élément integration exception
     */
    void lectureParametrage(String urlFichierParamParseurTexteFormat, String urlFichierParamParseurTexteClasses)
        throws IntegrationException;

    /**
     * Cette méthode réalise la lecture du fichier de paramétrage contenant la description du fichier et le mapping à
     * réaliser entre ce fichier et les classes.
     * 
     * @param urlFichierParamParseurTexteFormat --
     * @param urlFichierParamParseurTexteClasses --
     * @throws IntegrationException l'élément integration exception
     */
    void lectureParametrage(URL urlFichierParamParseurTexteFormat, URL urlFichierParamParseurTexteClasses)
        throws IntegrationException;

    /**
     * methode Reinitialiser liste lignes memorisees : --.
     */
    void reinitialiserListeLignesMemorisees();

    /**
     * methode Retourner liste lignes memorisees : --.
     * 
     * @return string
     */
    String retournerListeLignesMemorisees();

    /**
     * Mutateur de l'élément ligne analysee.
     *
     * @param ligneAnalysee l'élément new ligne analysee
     */
    void setLigneAnalysee(LigneFichier ligneAnalysee);

    /**
     * Mutateur de l'élément memorisation lignes fichier.
     *
     * @param memorisationLignesFichier l'élément new memorisation lignes fichier
     */
    void setMemorisationLignesFichier(boolean memorisationLignesFichier);

}
