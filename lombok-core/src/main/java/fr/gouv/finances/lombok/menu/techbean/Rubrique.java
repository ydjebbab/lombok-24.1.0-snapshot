/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : Rubrique.java
 *
 */
package fr.gouv.finances.lombok.menu.techbean;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Class Rubrique.
 * 
 * @author lcontinsouzas-cp Classe portant les attributs communs entre RubriqueLien et RubriqueSousMenu
 * @version $Revision: 1.2 $ Date: 15 déc. 2009
 */
public abstract class Rubrique extends ElementDeMenu
{

    /**
     * Initialisation de l'UID.
     */
    private static final long serialVersionUID = -6348816115860975140L;

    /**
     * Constructeur de la classe Rubrique.java
     *
     */
    public Rubrique()
    {
        super(); 
    }

    /**
     * methode Donner liste des rubriques : --.
     * 
     * @param rubriquesliensseulement --
     * @param elementsaffichablesseulement --
     * @return list< element de menu>
     * @throws MenuProgrammationException the menu programmation exception
     */
    public List<ElementDeMenu> donnerListeDesRubriques(boolean rubriquesliensseulement,
        boolean elementsaffichablesseulement) throws MenuProgrammationException
    {
        List<ElementDeMenu> listeobjets = new ArrayList<>();
        try
        {
            RubriqueLien unerubriquelien = (RubriqueLien) (this);
            log.debug("Rubrique lien trouvée :" + unerubriquelien.libelle);

            if (elementsaffichablesseulement)
            {
                unerubriquelien.calculerEstaffichable();
            }
            if (!elementsaffichablesseulement || (unerubriquelien.getEstAffichable()))
            {
                log.debug("Rubrique lien ajoutee à la liste :" + unerubriquelien.libelle);
                listeobjets.add(unerubriquelien);
            }
        }
        catch (ClassCastException cce)
        {
            log.debug("Exception détectée", cce);

            List<ElementDeMenu> uneliste = new ArrayList<>();

            RubriqueSousMenu unerubriquesousmenu = (RubriqueSousMenu) (this);
            log.debug("Rubrique sous-menu trouvée :" + unerubriquesousmenu.libelle);
            Iterator itlistederubriques = unerubriquesousmenu.listeDeRubriques.iterator();
            while (itlistederubriques.hasNext())
            {
                Rubrique unesousrubrique = (Rubrique) itlistederubriques.next();
                uneliste.addAll(unesousrubrique.donnerListeDesRubriques(rubriquesliensseulement,
                    elementsaffichablesseulement));
            }
            if (elementsaffichablesseulement)
            {
                unerubriquesousmenu.calculerEstAffichable(uneliste.isEmpty());
            }
            if (!elementsaffichablesseulement || (unerubriquesousmenu.getEstAffichable()))
            {
                if (!rubriquesliensseulement)
                {
                    listeobjets.add(unerubriquesousmenu);
                    log.debug("Rubrique sous-menu ajoutee à la liste :" + unerubriquesousmenu.libelle);
                }
                listeobjets.addAll(uneliste);
                log.debug("Rubrique rattachees à  " + unerubriquesousmenu.libelle + " ajoutées à la liste");
            }
        }
        return listeobjets;
    }

}
