/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : StubBeanPostProcessor.java
 *
 */

package fr.gouv.finances.lombok.stubs;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.stereotype.Component;

/**
 * Class StubBeanPostProcessor.
 * 
 * @author chouard-cp
 * @version $Revision: 1.3 $ Date: 15 déc. 2009
 */
@Component
public class StubBeanPostProcessor implements BeanPostProcessor
{
    /** log. */
    protected final Log log = LogFactory.getLog(getClass());

    /** stubs habilitation. */
    // stub annuaire
    private Map<String, Object> stubsHabilitation;
    
    /** profil test. */
    @Value(value = "${appli.profiltest}")
    String profilTest;

    /** test. */
    @Value(value = "${appli.test}")
    private String test;

    /** DN test. */
    @Value(value = "${appli.dntest}")
    String dnTest;
    
    /** type d'annuaire de test. */
    @Value(value = "${appli.typeAnnuairetest}")
    String typeAnnuaireTest;
    
    /** mel test. */
    @Value(value = "${appli.meltest}")
    String melTest;

    /**
     * Instanciation de stub bean post processor.
     */
    @PostConstruct
    public void initStubs()
    {       
      stubsHabilitation = new HashMap<>();
      StubLdapService authentificationserviceso = new StubLdapService();
      authentificationserviceso.setProfil(profilTest);
      authentificationserviceso.setDN(dnTest);
      authentificationserviceso.setTypeAnnuaire(typeAnnuaireTest);
      authentificationserviceso.setMel(melTest);
      stubsHabilitation.put("authentificationEtLectureHabilitationsService", authentificationserviceso);
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param bean
     * @param beanName
     * @return object
     * @see org.springframework.beans.factory.config.BeanPostProcessor#postProcessAfterInitialization(java.lang.Object,
     *      java.lang.String)
     */
    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName)
    {
        log.debug("After du bean " + beanName);
        return bean;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param bean
     * @param beanName
     * @return object
     * @throws BeansException the beans exception
     * @see org.springframework.beans.factory.config.BeanPostProcessor#postProcessBeforeInitialization(java.lang.Object,
     *      java.lang.String)
     */
    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException
    {
        Object retour = bean;
        log.debug("Post traitement du bean " + beanName);
        if (stubsHabilitation.containsKey(beanName) && test.compareTo("test") == 0)
        {
            log
                .warn("Attention pas de connexion reelle à l'annuaire -  mode simulation ne convient pas en production - modifiez le fichier application.properties");
            retour = stubsHabilitation.get(beanName);
        }

        return retour;
    }

}
