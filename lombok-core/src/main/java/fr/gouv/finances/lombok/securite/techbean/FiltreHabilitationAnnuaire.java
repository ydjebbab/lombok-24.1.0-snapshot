/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 */
package fr.gouv.finances.lombok.securite.techbean;

import java.util.Set;

import fr.gouv.finances.lombok.util.base.BaseTechBean;

/**
 * Class FiltreHabilitationAnnuaire.
 * 
 * @author lcontinsouzas-cp Re transcrit les acl tels qu'ils ont été définis dans Aptera soit filtre défini par
 *         l'application, soit filtre sur la compétence géographique nommé CODE_STRUCTURE_SUP dans Aptera
 * @version $Revision: 1.2 $ Date: 15 déc. 2009
 */
public class FiltreHabilitationAnnuaire extends BaseTechBean
{
    /** Constant : NOM_FILTRE_ZONE_DE_COMPETENCE_GEOGRAPHIQUE. */
    public static final String NOM_FILTRE_ZONE_DE_COMPETENCE_GEOGRAPHIQUE = "CODE_STRUCTURE_SUP";

    /** Constant : NOM_FORMATE_FILTRE_ZONE_DE_COMPETENCE_GEOGRAPHIQUE. */
    public static final String NOM_FORMATE_FILTRE_ZONE_DE_COMPETENCE_GEOGRAPHIQUE = "Zone de compétence géographique";

    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** nom du filtre. */
    private String nomDuFiltre;

    /** allow. */
    private boolean allow;

    /** deny. */
    private boolean deny;

    /** liste valeurs de filtre. */
    private Set<ValeurFiltreHabilitationAnnuaire> listeValeursDeFiltre;

    /**
     * Constructeur de la classe FiltreHabilitationAnnuaire.java
     *
     */
    public FiltreHabilitationAnnuaire()
    {
        super(); 
        
    }

    /**
     * methode Est un filtre zone de competence geographique : --.
     * 
     * @return true, si c'est vrai
     */
    public boolean estUnFiltreZoneDeCompetenceGeographique()
    {
        if (nomDuFiltre != null && nomDuFiltre.equalsIgnoreCase(NOM_FILTRE_ZONE_DE_COMPETENCE_GEOGRAPHIQUE))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    /**
     * Accesseur de l attribut liste valeurs de filtre.
     * 
     * @return liste valeurs de filtre
     */
    public Set<ValeurFiltreHabilitationAnnuaire> getListeValeursDeFiltre()
    {
        return listeValeursDeFiltre;
    }

    /**
     * Accesseur de l attribut nb valeurs de filtre.
     * 
     * @return nb valeurs de filtre
     */
    public int getNbValeursDeFiltre()
    {
        if (listeValeursDeFiltre != null)
        {
            return listeValeursDeFiltre.size();
        }
        else
        {
            return 0;
        }
    }

    /**
     * Accesseur de l attribut nom du filtre.
     * 
     * @return nom du filtre
     */
    public String getNomDuFiltre()
    {
        return nomDuFiltre;
    }

    /**
     * Accesseur de l attribut nom du filtre formate.
     * 
     * @return nom du filtre formate
     */
    public String getNomDuFiltreFormate()
    {
        if (this.estUnFiltreZoneDeCompetenceGeographique())
        {
            return NOM_FORMATE_FILTRE_ZONE_DE_COMPETENCE_GEOGRAPHIQUE;
        }
        else
        {
            return nomDuFiltre;
        }
    }

    /**
     * Verifie si allow.
     * 
     * @return true, si c'est allow
     */
    public boolean isAllow()
    {
        return allow;
    }

    /**
     * Verifie si deny.
     * 
     * @return true, si c'est deny
     */
    public boolean isDeny()
    {
        return deny;
    }

    /**
     * Modificateur de l attribut allow.
     * 
     * @param allow le nouveau allow
     */
    public void setAllow(boolean allow)
    {
        this.allow = allow;
    }

    /**
     * Modificateur de l attribut deny.
     * 
     * @param deny le nouveau deny
     */
    public void setDeny(boolean deny)
    {
        this.deny = deny;
    }

    /**
     * Modificateur de l attribut liste valeurs de filtre.
     * 
     * @param listeValeursDeFiltre le nouveau liste valeurs de filtre
     */
    public void setListeValeursDeFiltre(Set<ValeurFiltreHabilitationAnnuaire> listeValeursDeFiltre)
    {
        this.listeValeursDeFiltre = listeValeursDeFiltre;
    }

    /**
     * Modificateur de l attribut nom du filtre.
     * 
     * @param nomDuFiltre le nouveau nom du filtre
     */
    public void setNomDuFiltre(String nomDuFiltre)
    {
        this.nomDuFiltre = nomDuFiltre;
    }

}
