/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : MenuServiceImpl.java
 *
 */
package fr.gouv.finances.lombok.menu.service.impl;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Iterator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import fr.gouv.finances.lombok.menu.service.AvantGenerationMenuService;
import fr.gouv.finances.lombok.menu.service.GenerateurMenuService;
import fr.gouv.finances.lombok.menu.service.MenuService;
import fr.gouv.finances.lombok.menu.techbean.BarreDeMenusHierarchiques;
import fr.gouv.finances.lombok.menu.techbean.MenuProgrammationException;
import fr.gouv.finances.lombok.menu.techbean.RubriqueLien;
import fr.gouv.finances.lombok.securite.service.VerificationDroitAccesService;
import fr.gouv.finances.lombok.securite.springsecurity.UtilisateurSecurityLombok;
import fr.gouv.finances.lombok.util.base.BaseServiceImpl;
import fr.gouv.finances.lombok.util.xml.ConvertisseurDocumentXMLEnGrapheDObjets;

/**
 * Class MenuServiceImpl --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.4 $ Date: 11 déc. 2009
 */
public class MenuServiceImpl extends BaseServiceImpl implements MenuService, ApplicationContextAware
{
    private static final Logger log = LoggerFactory.getLogger(MenuServiceImpl.class);

    /** menu statique. */
    private BarreDeMenusHierarchiques menuStatique = null;

    /** fichier definition menu. */
    private String fichierDefinitionMenu = "/menudefinition.xml";

    /** verificationdroitaccesso. */
    private VerificationDroitAccesService verificationdroitaccesso;

    /** generateurmenuso. */
    private GenerateurMenuService generateurmenuso;

    /** menudefinitionaffichable. */
    private String menudefinitionaffichable = "oui";

    // Contexte Spring
    /** ctx. */
    private ApplicationContext ctx;

    /**
     * Constructeur de la classe MenuServiceImpl.java
     */
    public MenuServiceImpl()
    {
        super();

    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param contextpath
     * @see fr.gouv.finances.lombok.menu.service.MenuService#calculerBarreDeMenusDynamique(java.lang.String)
     */
    @Override
    public void calculerBarreDeMenusDynamique(String contextpath)
    {
        this.calculerBarreDeMenusDynamique(contextpath, new Object());
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param contextpath
     * @param parametreIntercepteur
     * @see fr.gouv.finances.lombok.menu.service.MenuService#calculerBarreDeMenusDynamique(java.lang.String,
     *      java.lang.Object)
     */
    @Override
    public void calculerBarreDeMenusDynamique(String contextpath, Object parametreIntercepteur)
    {

        if (log.isDebugEnabled())
        {
            log.debug("Demande de barre de menu avec contextpath : " + contextpath);
        }

        /**
         * si menudefinitionaffichable=oui dans application.properties , on générera le menu sinon ( cas notamment de
         * GID ) pas de génération standard du menu
         */
        if (this.getMenudefinitionaffichable() != null && this.getMenudefinitionaffichable().compareTo("oui") == 0)
        {

            /*
             * chargement du menustatique lors du premier appel au service uniquement si le menuStatique n'a pas encore
             * été chargé
             */
            if (this.menuStatique == null)
            {
                if (log.isDebugEnabled())
                {
                    log.debug("Definition statique non chargee - chargement");
                }

                this.chargerDefinitionStatiqueDuMenu();
            }

            Authentication refrolesutilisateur = SecurityContextHolder.getContext().getAuthentication();

            /* calcul du menu dynamique en fonction des URL accessibles à l'utilisateur */
            BarreDeMenusHierarchiques uneBarreDeMenusDynamique = this.creerBarreDeMenusDynamique(refrolesutilisateur);

            /* ajout du contexte path aux url */
            uneBarreDeMenusDynamique = this.ajouterPrefixesUrls(uneBarreDeMenusDynamique, contextpath);

            // appel de l'intercepteur géré par le code applicatif
            this.appelerIntercepteurAvantGenerationMenu(uneBarreDeMenusDynamique, parametreIntercepteur);
            uneBarreDeMenusDynamique.calculerEstAffichablePourTousLesElements();

            /* génération */

            if (log.isDebugEnabled())
            {
                log.debug("debut generation du fichier de configuration du menu (pour le javascript d'affichage)");
            }
            String uneGenDeBarreDeMenuDynamique = this.genererFichierConfigBarreDeMenusDynamique(uneBarreDeMenusDynamique);

            if (log.isDebugEnabled())
            {
                log.debug("fin generation du fichier de configuration du menu (pour le javascript d'affichage)");
                log.debug("parametrage genere : " + uneGenDeBarreDeMenuDynamique);
            }

            String uneGenDeBarreDeMenuDynamiqueHtml = this.genererFichierConfigBarreDeMenusDynamiqueHtml(uneBarreDeMenusDynamique);

            if (log.isDebugEnabled())
            {
                log.debug("fin generation du fichier de configuration du menu en pure HTML");
                log.debug("parametrage genere : " + uneGenDeBarreDeMenuDynamiqueHtml);
            }

            // Stockage du menu dans le contexte securisé de l'utilisateur
            ((UtilisateurSecurityLombok) refrolesutilisateur.getPrincipal()).setGenerationmenu(uneGenDeBarreDeMenuDynamique);

            // Stockage du menu dans le contexte securisé de l'utilisateur
            ((UtilisateurSecurityLombok) refrolesutilisateur.getPrincipal()).setGenerationmenuhtml(uneGenDeBarreDeMenuDynamiqueHtml);

            if (log.isDebugEnabled())
            {
                log.debug("parametrage stocke dans le contexte securise");
            }

        }

        // cas ou n'a pas de menu statique
        else
        {
            if (log.isDebugEnabled())
            {
                log.debug("menudefinitionaffichable=non dans application.properties. pas de chargement du menu");
            }
        }

    }

    /**
     * Accesseur de l attribut fichier definition menu.
     * 
     * @return fichier definition menu
     */
    public String getFichierDefinitionMenu()
    {
        return fichierDefinitionMenu;
    }

    /**
     * Accesseur de l attribut generateurmenuso.
     * 
     * @return generateurmenuso
     */
    public GenerateurMenuService getGenerateurmenuso()
    {
        return generateurmenuso;
    }

    /**
     * Accesseur de l attribut menudefinitionaffichable.
     * 
     * @return menudefinitionaffichable
     */
    public String getMenudefinitionaffichable()
    {
        return menudefinitionaffichable;
    }

    /**
     * Accesseur de l attribut verificationdroitaccesso.
     * 
     * @return verificationdroitaccesso
     */
    public VerificationDroitAccesService getVerificationdroitaccesso()
    {
        return verificationdroitaccesso;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param applicationcontext le nouveau application context
     * @throws BeansException the beans exception
     * @see org.springframework.context.ApplicationContextAware#setApplicationContext(org.springframework.context.ApplicationContext)
     */
    @Override
    public void setApplicationContext(ApplicationContext applicationcontext) throws BeansException
    {
        this.ctx = applicationcontext;

    }

    /**
     * Modificateur de l attribut fichier definition menu.
     * 
     * @param cheminfichierdefinitionmenu le nouveau fichier definition menu
     */
    public void setFichierDefinitionMenu(String cheminfichierdefinitionmenu)
    {
        this.fichierDefinitionMenu = cheminfichierdefinitionmenu;
    }

    /**
     * Modificateur de l attribut generateurmenuso.
     * 
     * @param generateurmenuso le nouveau generateurmenuso
     */
    public void setGenerateurmenuso(GenerateurMenuService generateurmenuso)
    {
        this.generateurmenuso = generateurmenuso;
    }

    /**
     * Modificateur de l attribut menudefinitionaffichable.
     * 
     * @param menudefinitionaffichable le nouveau menudefinitionaffichable
     */
    public void setMenudefinitionaffichable(String menudefinitionaffichable)
    {
        this.menudefinitionaffichable = menudefinitionaffichable;
    }

    /**
     * Modificateur de l attribut verificationdroitaccesso.
     * 
     * @param verificationdroitaccesso le nouveau verificationdroitaccesso
     */
    public void setVerificationdroitaccesso(VerificationDroitAccesService verificationdroitaccesso)
    {
        this.verificationdroitaccesso = verificationdroitaccesso;
    }

    /**
     * methode Appeler intercepteur avant generation menu : --.
     * 
     * @param uneBarreDeMenusDynamique --
     * @param parametreIntercepteur --
     */
    protected void appelerIntercepteurAvantGenerationMenu(BarreDeMenusHierarchiques uneBarreDeMenusDynamique,
        Object parametreIntercepteur)
    {
        try
        {
            AvantGenerationMenuService avantgenerationmenuserviceso =
                (AvantGenerationMenuService) ctx.getBean("avantgenerationmenuserviceimpl");
            if (avantgenerationmenuserviceso != null)
            {
                avantgenerationmenuserviceso.executerApresFiltrageMenu(uneBarreDeMenusDynamique, parametreIntercepteur);
            }
        }
        catch (NoSuchBeanDefinitionException nsbde)
        {
            log.debug("Pas de bean avantgenerationmenuserviceso trouvé : pas d'interception", nsbde);
        }
    }

    /**
     * methode Ajouter prefixes urls : --.
     * 
     * @param uneBarreDeMenusDynamique --
     * @param contextpath --
     * @return barre de menus hierarchiques
     */
    private BarreDeMenusHierarchiques ajouterPrefixesUrls(BarreDeMenusHierarchiques uneBarreDeMenusDynamique,
        String contextpath)
    {

        if (log.isDebugEnabled())
        {
            log.debug(" début ajout des prefixe aux url ");
        }
        Iterator desrubriqueslien = uneBarreDeMenusDynamique.itererRubriquesLien();

        while (desrubriqueslien.hasNext())
        {
            RubriqueLien unerubriquelien = (RubriqueLien) desrubriqueslien.next();
            if (log.isDebugEnabled())
            {
                log.debug(" url trouvée :  " + unerubriquelien.url);
            }
            if ((unerubriquelien.url != null) && (unerubriquelien.url.startsWith("/")))
            {
                unerubriquelien.url = contextpath.concat(unerubriquelien.url);
                if (log.isDebugEnabled())
                {
                    log.debug(" nouvelle url :  " + unerubriquelien.url);
                }
            }
        }
        return uneBarreDeMenusDynamique;
    }

    // accès synchonisé pour éviter plusiers chargements simultanés
    /**
     * methode Charger definition statique du menu : --.
     */
    private synchronized void chargerDefinitionStatiqueDuMenu()
    {

        if (this.menuStatique != null)
        {
            return;
        }

        log.debug("debut chargement definition statique du menu ");

        /* recuperation du nom de package des bean menu */

        String nomclasse = BarreDeMenusHierarchiques.class.getName();
        int indexnomdeclasse = nomclasse.lastIndexOf('.');

        StringBuilder nompackage = new StringBuilder(nomclasse);
        nompackage = nompackage.delete(indexnomdeclasse, nompackage.length());

        log.debug("nom de package des classes métier du menu : " + nompackage);

        ConvertisseurDocumentXMLEnGrapheDObjets unConvertisseur =
            new ConvertisseurDocumentXMLEnGrapheDObjets(nompackage.toString());
        this.getClass().getResourceAsStream("fichierDefinitionMenu");

        log.debug("debut parsing du fichier de config du menu : " + fichierDefinitionMenu);

        try
        {
            /* la recherche d'un fichier contenu dans les packages java se fait avec getRessource */
            // String chemincomplet = this.getClass().getResource(fichierDefinitionMenu).getFile();

            // log.debug("chemin complet du fichier de config du menu : " + chemincomplet);

            InputSource sourceXml = new InputSource(this.getClass().getClassLoader().getResourceAsStream(fichierDefinitionMenu));

            unConvertisseur.convertir(sourceXml);
        }
        catch (FileNotFoundException fileNotFound)
        {
            throw new MenuProgrammationException("Fichier de configuration du menu non trouvé dans le classpath :"
                + fichierDefinitionMenu, fileNotFound);
        }
        catch (SAXException saxExc)
        {
            throw new MenuProgrammationException(
                "Fichier de configuration XML du menu non conforme ou sans correspondance"
                    + " avec les classes métiers menu : " + fichierDefinitionMenu,
                saxExc);
        }
        catch (IOException ioExc)
        {
            throw new MenuProgrammationException("Probleme lors de la lecture du fichier de configuration du menu : "
                + fichierDefinitionMenu, ioExc);
        }

        log.debug("fin normale parsing du fichier de config du menu : " + fichierDefinitionMenu);

        this.menuStatique = (BarreDeMenusHierarchiques) (unConvertisseur.getObjetResultat());

        log.debug("recuperation du graphe d'objet menu réussie : " + fichierDefinitionMenu);
    }

    /**
     * methode Creer barre de menus dynamique : --.
     * 
     * @param refrolesutilisateur --
     * @return barre de menus hierarchiques
     */
    private BarreDeMenusHierarchiques creerBarreDeMenusDynamique(Object refrolesutilisateur)
    {

        /*
         * Clonage du menuStatique qui sert de modèle pour batir le menu dynamique adapté au profil utilisateur
         */

        BarreDeMenusHierarchiques uneBarreDeMenusDynamique;
        try
        {
            //uneBarreDeMenusDynamique = (BarreDeMenusHierarchiques) (ObjectDeepCloner.deepCopy(menuStatique));
            uneBarreDeMenusDynamique = (BarreDeMenusHierarchiques) menuStatique.clone();
        }
        catch (RuntimeException e)
        {
            throw new MenuProgrammationException(
                "Impossible de cloner le graphe d'objets du menu statique pour batir un menu dynamique", e);
        }

        /*
         * Pour chaque rubrique lien on fait appel au service de securite (sprinsecurity en principe) pour vérifier que
         * l'url est accessible
         */

        Boolean estautorise;
        Iterator desrubriqueslien = uneBarreDeMenusDynamique.itererRubriquesLien();

        while (desrubriqueslien.hasNext())
        {
            RubriqueLien unerubriquelien = (RubriqueLien) desrubriqueslien.next();
            if (unerubriquelien.url != null && "O".equalsIgnoreCase(unerubriquelien.verifierUrl))
            {
                if (log.isDebugEnabled())
                {
                    log.debug("verification des droits d'accès pour : " + unerubriquelien.libelle);
                }
                estautorise =
                    Boolean.valueOf(this.verificationdroitaccesso.isURLaccessible(unerubriquelien.url,
                        refrolesutilisateur));
            }
            else
            {
                if (log.isDebugEnabled())
                {
                    log.debug("pas de verification des droits (URL autorisée) pour : " + unerubriquelien.libelle);
                }
                estautorise = Boolean.TRUE;
            }
            unerubriquelien.setUrlEstAutorisee(estautorise);
        }
        return uneBarreDeMenusDynamique;

    }

    /**
     * methode Generer fichier config barre de menus dynamique : --.
     * 
     * @param uneBarreDeMenusDynamique --
     * @return string
     */
    private String genererFichierConfigBarreDeMenusDynamique(BarreDeMenusHierarchiques uneBarreDeMenusDynamique)
    {
        return generateurmenuso.genererBarreDeMenu(uneBarreDeMenusDynamique);
    }

    /**
     * methode Generer fichier config barre de menus dynamique : --.
     * 
     * @param uneBarreDeMenusDynamique --
     * @return string
     */
    private String genererFichierConfigBarreDeMenusDynamiqueHtml(BarreDeMenusHierarchiques uneBarreDeMenusDynamique)
    {
        return generateurmenuso.genererBarreDeMenuHtml(uneBarreDeMenusDynamique);
    }

}
