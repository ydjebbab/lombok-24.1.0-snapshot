/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : LdapAuthentificationSecurityLombok.java
 *
 */
package fr.gouv.finances.lombok.securite.springsecurity;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;

import fr.gouv.finances.lombok.securite.techbean.AuthentificationRegleGestionException;
import fr.gouv.finances.lombok.securite.techbean.HabilitationAnnuaire;
import fr.gouv.finances.lombok.securite.techbean.HabilitationRegleGestionException;
import fr.gouv.finances.lombok.securite.techbean.PersonneAnnuaire;
import fr.gouv.finances.lombok.util.format.FormaterDate;

/**
 * <pre>
 * Cette classe détourne le login / mot de passe si la clé annuaire.typeannuaire vaut AUCUN.
 * Le username devient alors le DN.
 * Le password devient alors le profil.
 * </pre>
 * 
 * @author chouard-cp
 * @version $Revision: 1.4 $ Date: 15 déc. 2009
 */
public class RealAuthentificationSecurityLombok extends AbstractAuthentificationSecurityLombok
{
    private static final Logger log = LoggerFactory.getLogger(RealAuthentificationSecurityLombok.class);

    private String typeAnnuaire;

    public RealAuthentificationSecurityLombok()
    {
        super();
    }

    /**
     * Accesseur de l attribut type annuaire.
     *
     * @return type annuaire
     */
    public String getTypeAnnuaire()
    {
        return typeAnnuaire;
    }

    /**
     * Modificateur de l attribut type annuaire.
     *
     * @param typeAnnuaire le nouveau type annuaire
     */
    public void setTypeAnnuaire(String typeAnnuaire)
    {
        this.typeAnnuaire = typeAnnuaire;
    }

    /*
     * (non-Javadoc)
     * @see org.springframework.security.providers.dao.AbstractUserDetailsAuthenticationProvider#retrieveUser(java.lang.
     * String , org.springframework.security.providers.UsernamePasswordAuthenticationToken)
     */
    /**
     * Cette méthode est appelée par l'authenticate, elle ne vérifiera pas le mot de passe qui sera utilisé comme
     * profil. (methode de remplacement) {@inheritDoc}.
     * 
     * @param username "documenté"
     * @param auth "documenté"
     * @return user details
     * @throws AuthenticationException the authentication exception
     * @see org.springframework.security.providers.dao.AbstractUserDetailsAuthenticationProvider#retrieveUser(java.lang.String,
     *      org.springframework.security.providers.UsernamePasswordAuthenticationToken)
     */
    @Override
    public UserDetails retrieveUser(String username, UsernamePasswordAuthenticationToken auth)
        throws AuthenticationException
    {
        log.debug(">>> Debut methode retrieveUser()");
        String passwordprofil = (String) auth.getCredentials();

        PersonneAnnuaire unePersonneAnnuaire;
        try
        {
            if (log.isDebugEnabled())
            {
                log.debug("entree retrieveUser ");
            }
            // verifier du mode d'accés à l'appli
            verifierModeAuthentificationParLApplication();
            // verifier messages srm (pas de bloquant)
            verifierPasDeMessageBloquantSRM();
            // recherche personne avec habilitations aprés authentification
            unePersonneAnnuaire =
                this.construirePersonneReal(username, passwordprofil);

            /*
             * on enleve les profils bloqués de sireme de la personne +exception si tous le sprofils sont bloqués
             */

            boolean desProfilsOntEteFiltres = filtrerProfilsBloquesParSireme(unePersonneAnnuaire);

            if (unePersonneAnnuaire.getListeHabilitations() == null
                || unePersonneAnnuaire.getListeHabilitations().isEmpty())
            {
                jeterExceptionPasDHabilitation(desProfilsOntEteFiltres);
            }

            appelerIntercepteurAvantHabilitation(unePersonneAnnuaire);

            String urlAppli = UtilisateurSecurityLombokContainer.getUrl();
            // construction utilisateur
            return this.construitUtilisateurSecurityLombok(unePersonneAnnuaire, username, passwordprofil, urlAppli);

        }

        /*
         * Exception Coca : Il ne faut pas ajouter l'exception e en paramètre du constructeur de l'exception
         * BadCredentialsException des 3 blocs catch suivants car le texte remonté à l'utilisateur affiche une
         * information de type "Nested exception..." qui nuit à la clarté du message
         */
        catch (AuthentificationRegleGestionException | HabilitationRegleGestionException exception)
        {
            throw new BadCredentialsException(exception.getMessage(), exception);
        }
        catch (RuntimeException exception)
        {
            log.error("Erreur imprevue lors de l'authentification ", exception);
            throw new BadCredentialsException(exception.getMessage(), exception);
        }

    }

    /**
     * methode Construire personne annuaire sans ldap : .
     *
     * @param usernamedn
     * @param passwordprofil
     * @return personne annuaire
     */
    private PersonneAnnuaire construirePersonneReal(String usernamedn, String passwordprofil)
    {
        log.debug(">>> Debut methode construirePersonneReal()");
        PersonneAnnuaire unePersonneAnnuaire;
        unePersonneAnnuaire = new PersonneAnnuaire();

        // regex utilisé pour désencapsuler les infos qui seront utilisées pour créer une personneannuaire
        // exemple attendu: cn=BUSTE Tara (3134100058),ou=Notaires,o=Professions Reglementées,c=FR
        // la partie ou= est optionnelle. Si elle n'est pas présente, un profil "aucun" sera
        // créé
        String pattern = "(?:cn\\=(?<cn>.*))\\((?<real>\\d+)\\)(\\W+)?((?:ou\\=(?<ou>\\w+)))?";
        Pattern pat = Pattern.compile(pattern);

        Matcher matcher = pat.matcher(usernamedn);
        if (matcher.find())
        {
            String uid;
            String cnn;
            String ouu;
            cnn = matcher.group("cn");
            uid = matcher.group("real");
            ouu = matcher.group("ou");
            if (log.isDebugEnabled())
            {
                log.debug("cn=" + matcher.group("cn"));
                log.debug("clé real=" + matcher.group("real"));
                log.debug("profil(ou=)=" + matcher.group("ou"));
            }

            DateFormat dateFormat = FormaterDate.getFormatDate("dd/MM/yyyy");
            Date dateDebut = null;
            Date dateFin = null;

            try
            {
                dateDebut = dateFormat.parse("01/01/2017");
                dateFin = dateFormat.parse("01/01/2099");
            }
            catch (ParseException pexc)
            {
                log.debug("Problème de parsing de la date qui sera utilisée pour l'habilitation ", pexc);
            }
            unePersonneAnnuaire = new PersonneAnnuaire();

            Set<HabilitationAnnuaire> listeHabilitations = new HashSet<>();
            HabilitationAnnuaire unehabilitation = new HabilitationAnnuaire();

            if (ouu != null)
            {
                unehabilitation.setNomProfil(ouu);
            }
            else
            {
                unehabilitation.setNomProfil("aucun");

            }
            unehabilitation.setDateDebut(dateDebut);
            unehabilitation.setDateFin(dateFin);
            unehabilitation.setDateCreation(dateDebut);
            unehabilitation.setNomAdministrateur("Inconnu");
            unehabilitation.setLibelleCourtAppli("Inconnu");

            listeHabilitations.add(unehabilitation);
            unePersonneAnnuaire.setListeHabilitations(listeHabilitations);

            unePersonneAnnuaire.setUid(uid);
            unePersonneAnnuaire.setAffectation("Inconnu");
            unePersonneAnnuaire.setAnnexe("0");
            unePersonneAnnuaire.setCn(cnn);
            unePersonneAnnuaire.setCodeFonction("Inconnu");
            unePersonneAnnuaire.setCodeGrade("Inconnu");
            unePersonneAnnuaire.setDepartement("Inconnu");
            unePersonneAnnuaire.setGivenName(uid);
            unePersonneAnnuaire.setMail("Inconnu");
            unePersonneAnnuaire.setPersonalTitle("");
            unePersonneAnnuaire.setPostalAddress("Inconnu");
            unePersonneAnnuaire.setPostalCode("Inconnu");
            unePersonneAnnuaire.setSn("Inconnu");
            unePersonneAnnuaire.setTelephoneNumber("Inconnu");
            unePersonneAnnuaire.setTitle("Inconnu");

            if (log.isDebugEnabled())
            {
                log.debug(
                    "Construction utilisateur avec clé REAL passée dans l'entête (annuaire.typeannuaire=REAL), la clé est utilisée pour l'uid : "
                        + unePersonneAnnuaire.getUid());
                log.debug("Une habilitation avec profil (récupéré si l'entête contient ou=)  : " + unehabilitation.getNomProfil());
            }
        }
        else
        {
            throw new AuthentificationRegleGestionException(
                "L'information passée (login ou entête http : " + usernamedn + ") ne permet pas de créer un utilisateur");
        }
        return unePersonneAnnuaire;

    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider#authenticate(org.springframework.security.core.Authentication)
     */
    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException
    {
        log.debug(">>> Debut methode authenticate()");
        Authentication auth = null;

        if ("REAL".equalsIgnoreCase(typeAnnuaire))
        {
            if (log.isDebugEnabled())
            {
                log.debug("Mode real, pas d'annuaire (annuaire.typeannuaire=REAL), pas de mot de passe ");
            }
            auth = super.authenticate(authentication);
        }
        else
        {
            if (log.isDebugEnabled())
            {
                log.debug(
                    "Mode auncun ldap pas activé, on passe au provider suivant (voir applicationContext-commun-security.xml) à l'authentification avec ldap ");
            }

        }

        return auth;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider#additionalAuthenticationChecks(org.springframework.security.core.userdetails.UserDetails,
     *      org.springframework.security.authentication.UsernamePasswordAuthenticationToken)
     */
    @Override
    protected void additionalAuthenticationChecks(UserDetails userDetails, UsernamePasswordAuthenticationToken authentication)
        throws AuthenticationException
    {
        // rien à faire
    }

}
