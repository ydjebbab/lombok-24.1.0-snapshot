/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 */
package fr.gouv.finances.lombok.util.base;

/**
 * Interface de la purge de tables de la base de données.
 *
 * @author Christophe Breheret-Girardin
 */
public interface PurgeTablesDao
{
    /**
     * Méthode permettant de purger les tables dont les noms sont passés en paramètre.
     *
     * @param nomsTables noms des tables à purger
     */
    void purgerTables(String... nomsTables);
}
