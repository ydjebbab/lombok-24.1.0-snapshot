/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : CpCustomHeureEditor.java
 *
 * Copyright 2002-2006 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.gouv.finances.lombok.util.propertyeditor;

import java.beans.PropertyEditorSupport;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.util.StringUtils;

/**
 * PropertyEditor pour <code>java.util.Date</code>, qui utilise les formats définis par la
 * classe <code>java.text.DateFormat</code>.
 * 
 * @author chouard-cp
 * @version $Revision: 1.3 $ Date: 15 déc. 2009
 */
public class CpCustomHeureEditor extends PropertyEditorSupport
{

    /** Constant : DATE_FORMAT_PARSE. */
    private static final String DATE_FORMAT_PARSE = "HH:mm:ss:SS";

    /** Constant : DATE_FORMAT. */
    private static final String DATE_FORMAT = "HH:mm:ss";

    /** Constant : MSG_ERREUR. */
    private static final String MSG_ERREUR = "Format d'heure incorrect";

    /** Constant : SEPARATEUR_DEUXPOINTS. */
    private static final String SEPARATEUR_DEUXPOINTS = ":";

    /** Constant : SEPARATEUR_POINT. */
    private static final String SEPARATEUR_POINT = ".";

    /** Constant : SEPARATEUR_BLANC. */
    private static final String SEPARATEUR_BLANC = " ";

    /** date format. */
    private final DateFormat dateFormat;

    /** allow empty. */
    private final boolean allowEmpty;

    /** allow lenient. */
    private final boolean allowLenient;

    /**
     * Crée une nouvelle instance de CpCustomDateEditor, en utilisant par défaut le format de présentation (JJ/MM/AAAA).
     * Les dates nulles sont autorisées. Les dates hors calendier sont refusées.
     */
    public CpCustomHeureEditor()
    {
        super();
        this.dateFormat = new SimpleDateFormat(DATE_FORMAT, Locale.FRANCE);
        this.allowLenient = false;
        this.allowEmpty = true;
    }

    /**
     * Create a new CustomDateEditor instance, using the given DateFormat for parsing and rendering.
     * <p>
     * The "allowEmpty" parameter states if an empty String should be allowed for parsing, i.e. get interpreted as null
     * value. Otherwise, an IllegalArgumentException gets thrown in that case.
     * 
     * @param dateFormat DateFormat to use for parsing and rendering
     * @param allowEmpty if empty strings should be allowed
     * @param allowLenient --
     */
    /**
     * Crée une nouvelle instance de CpCustomDateEditor, en utilisant le format de présentation de la date passée en
     * paramètre
     * 
     * @param allowEmpty : indique si les dates nulles sont autorisées ou non
     * @param allowLenient : indique si les dates hors calendier sont refusées ou non.
     * @param dateFormat : format date
     */
    public CpCustomHeureEditor(DateFormat dateFormat, boolean allowEmpty, boolean allowLenient)
    {
        super();
        this.dateFormat = dateFormat;
        this.allowEmpty = allowEmpty;
        this.allowLenient = allowLenient;
    }

    /**
     * Formate la date en utilisant le format.
     * 
     * @return the as text
     */
    @Override
    public String getAsText()
    {
        Date value = (Date) getValue();
        return (value == null ? "" : this.dateFormat.format(value));
    }

    /**
     * Construit une date à partir de la chaîne passée en paramètre, après l'avoir retraitée.
     * 
     * @param text --
     * @throws IllegalArgumentException the illegal argument exception
     */
    @Override
    public void setAsText(String text) throws IllegalArgumentException
    {

        Date result = null;
        String texteEpure = this.epure(text);

        if (this.allowEmpty && !StringUtils.hasText(texteEpure))
        {
            setValue(result);
        }
        else
        {
            String normalizeString = this.constructNormalizeString(texteEpure);
            result = new Timestamp(this.parse(normalizeString).getTime());
            setValue(result);
        }
    }

    /**
     * Teste si deux séparateurs sont identiques.
     * 
     * @param sep1Normalise --
     * @param sep2Normalise --
     */
    private void areSepEquals(String sep1Normalise, String sep2Normalise)
    {
        // Si le séparateur 1 n'est pas égal au séparateur 2
        if (sep1Normalise == null || sep2Normalise == null || !sep1Normalise.equals(sep2Normalise))
        {
            throw new IllegalArgumentException(MSG_ERREUR);
        }
    }

    /**
     * Si possible retraite la chaîne de caractères pour la conformer au format hh:mm:ss:SS.
     * 
     * @param textSource --
     * @return la chaîne retraitée
     */
    private String constructNormalizeString(String textSource)
    {
        String result = null;
        String[] patterns =
        {"(\\d{1,2})([:.\\s]+)(\\d{1,2})", "(\\d{1,2})([:.\\s]+)(\\d{1,2})([:.\\s]+)(\\d{1,2})",
                "(\\d{1,2})([:.\\s]+)(\\d{1,2})([:.\\s]+)(\\d{1,2})([:.\\s]+)(\\d{1,2})", "(\\d{1,2})",};

        String[] patternsLong =
        {"(\\d{1,2})([h\\s]+)(\\d{1,2})([m\\s]*+)", "(\\d{1,2})([h\\s]+)(\\d{1,2})",
                "(\\d{1,2})([h\\s]+)(\\d{1,2})([m\\s]*+)(\\d{1,2})([s\\s]*+)",
                "(\\d{1,2})([h\\s]+)(\\d{1,2})([m\\s]*+)(\\d{1,2})",
                "(\\d{1,2})([h\\s]+)(\\d{1,2})([m\\s]*+)(\\d{1,2})([s\\s]*+)(\\d{1,2})([ms\\s]*+)",
                "(\\d{1,2})([h\\s]+)(\\d{1,2})([m\\s]*+)(\\d{1,2})([s\\s]*+)(\\d{1,2})"};

        if (textSource == null)
        {
            throw new IllegalArgumentException(MSG_ERREUR);
        }

        // séparateurs . : blanc
        int iii = 0;
        while (iii < patterns.length && result == null)
        {
            result = constructStringDependPattern(textSource, patterns[iii]);
            iii++;
        }

        // séparateurs h m s ms
        int jjj = 0;
        while (jjj < patternsLong.length && result == null)
        {
            result = constructStringDependPatternLong(textSource, patternsLong[jjj]);
            jjj++;
        }

        if (result == null)
        {
            result = constructStringWithoutSep(textSource);
        }

        if (result == null)
        {
            result = textSource;
        }
        return result;
    }

    /**
     * Construit une chaîne de texte normalisée sous la forme hh:mm:ss:SS à partir d'une chaîne dans laquelle les
     * heures, minutes, secondes et millisecondes sont séparées par les caractères ':' '.' ou 'blanc'
     * 
     * @param textSource --
     * @param pattern --
     * @return the string
     */
    private String constructStringDependPattern(String textSource, String pattern)
    {
        String result = null;

        Matcher matcher = Pattern.compile(pattern).matcher(textSource);

        if (matcher.matches())
        {
            StringBuilder newtext = new StringBuilder();
            String sep1;
            String sep2;
            String sep3;
            String bloc1;
            String bloc2;
            String bloc3;
            String bloc4;
            String sep1Normalise;
            String sep2Normalise;
            String sep3Normalise;

            // Décomposition de la chaîne en blocs
            bloc1 = (matcher.groupCount() < 1 ? "00" : matcher.group(1));
            sep1 = (matcher.groupCount() < 2 ? ":" : matcher.group(2));
            bloc2 = (matcher.groupCount() < 3 ? "00" : matcher.group(3));
            sep2 = (matcher.groupCount() < 4 ? sep1 : matcher.group(4));
            bloc3 = (matcher.groupCount() < 5 ? "00" : matcher.group(5));
            sep3 = (matcher.groupCount() < 6 ? sep1 : matcher.group(6));
            bloc4 = (matcher.groupCount() < 7 ? "00" : matcher.group(7));

            // Normalisation des séparateurs
            sep1Normalise = this.normalizeSeparator(sep1);
            sep2Normalise = this.normalizeSeparator(sep2);
            sep3Normalise = this.normalizeSeparator(sep3);

            this.areSepEquals(sep1Normalise, sep2Normalise);
            this.areSepEquals(sep2Normalise, sep3Normalise);

            newtext.append(bloc1);
            newtext.append(SEPARATEUR_DEUXPOINTS);
            newtext.append(bloc2);
            newtext.append(SEPARATEUR_DEUXPOINTS);
            newtext.append(bloc3);
            newtext.append(SEPARATEUR_DEUXPOINTS);
            newtext.append(bloc4);
            result = newtext.toString();
        }

        return result;
    }

    /**
     * methode Construct string depend pattern long : --.
     * 
     * @param textSource --
     * @param pattern --
     * @return string
     */
    private String constructStringDependPatternLong(String textSource, String pattern)
    {
        String result = null;

        Matcher matcher = Pattern.compile(pattern).matcher(textSource);

        if (matcher.matches())
        {
            StringBuilder newtext = new StringBuilder();
            // String sep1;

            String bloc1;
            String bloc2;
            String bloc3;
            String bloc4;

            // Décomposition de la chaîne en blocs
            bloc1 = (matcher.groupCount() < 1 ? "00" : matcher.group(1));
            // sep1 = (matcher.groupCount() < 2 ? ":" : matcher.group(2));
            bloc2 = (matcher.groupCount() < 3 ? "00" : matcher.group(3));
            // sep1 = (matcher.groupCount() < 4 ? sep1 : matcher.group(4));
            bloc3 = (matcher.groupCount() < 5 ? "00" : matcher.group(5));
            // sep1 = (matcher.groupCount() < 6 ? sep1 : matcher.group(6));
            bloc4 = (matcher.groupCount() < 7 ? "00" : matcher.group(7));

            newtext.append(bloc1);
            newtext.append(SEPARATEUR_DEUXPOINTS);
            newtext.append(bloc2);
            newtext.append(SEPARATEUR_DEUXPOINTS);
            newtext.append(bloc3);
            newtext.append(SEPARATEUR_DEUXPOINTS);
            newtext.append(bloc4);
            result = newtext.toString();
        }

        return result;
    }

    /**
     * Construit une chaîne de texte normalisée sous la forme hh:mm:ss:SS à partir d'une chaîne dans laquelle les
     * heures, minutes, secondes et millisecondes sont placés les uns à côté des autres dans séparateur.
     * 
     * @param textSource --
     * @return the string
     */
    private String constructStringWithoutSep(String textSource)
    {
        String result = null;
        Matcher matcher = Pattern.compile("(\\d{3,8})").matcher(textSource);

        if (matcher.matches())
        {
            StringBuilder newtext = new StringBuilder();
            String bloc = matcher.group(1);
            String bloc1;
            String bloc2;
            String bloc3;
            String bloc4;

            while (bloc.length() < 8)
            {
                bloc = new StringBuilder(bloc).append("0").toString();
            }

            bloc1 = bloc.substring(0, 2);
            bloc2 = bloc.substring(2, 4);
            bloc3 = bloc.substring(4, 6);
            bloc4 = bloc.substring(6, 8);

            newtext.append(bloc1);
            newtext.append(SEPARATEUR_DEUXPOINTS);
            newtext.append(bloc2);
            newtext.append(SEPARATEUR_DEUXPOINTS);
            newtext.append(bloc3);
            newtext.append(SEPARATEUR_DEUXPOINTS);
            newtext.append(bloc4);
            result = newtext.toString();
        }

        return result;
    }

    /**
     * methode Epure : --.
     * 
     * @param text --
     * @return string
     */
    private String epure(String text)
    {
        return text.trim();
    }

    /**
     * Normalise le caractère utilisé pour séparé les heures, minutes, secondes et années.
     * 
     * @param separatorText --
     * @return the string
     */
    private String normalizeSeparator(String separatorText)
    {
        String result = "";

        if (separatorText == null)
        {
            throw new IllegalArgumentException(MSG_ERREUR);
        }
        else
        {
            if (separatorText.length() > 0 && separatorText.indexOf(SEPARATEUR_DEUXPOINTS) != -1)
            {
                result = SEPARATEUR_DEUXPOINTS;
            }
            else if (separatorText.length() > 0 && separatorText.indexOf(SEPARATEUR_POINT) != -1)
            {
                result = SEPARATEUR_POINT;
            }
            else if (separatorText.length() > 0 && separatorText.indexOf(SEPARATEUR_BLANC) != -1)
            {
                result = SEPARATEUR_BLANC;
            }
            else
            {
                throw new IllegalArgumentException(MSG_ERREUR);
            }
        }

        return result;
    }

    /**
     * Convertit une chaîne de texte en date en utilisant le format DATE_FORMAT_PARSE.
     * 
     * @param text --
     * @return une date
     */
    private Date parse(String text)
    {
        Date result = null;

        try
        {
            DateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT_PARSE, Locale.FRANCE);
            dateFormat.setLenient(this.allowLenient);
            result = new Timestamp(dateFormat.parse(text).getTime());
        }
        catch (ParseException exception)
        {
            throw new IllegalArgumentException(MSG_ERREUR, exception);
        }
        return result;
    }

}
