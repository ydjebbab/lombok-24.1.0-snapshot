/*
 * Copyright (c) 2017 DGFiP - Tous droits réservés
 * 
*/
// Projet : lombok-core
/**
 * Documentation du paquet fr.gouv.finances.lombok.clamav.service
 * @author chouard
 * @version 1.0
 */
package fr.gouv.finances.lombok.clamav.service;