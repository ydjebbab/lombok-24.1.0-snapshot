/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - ageffroy2-cp
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : PieceJointeInline.java
 *
 */
package fr.gouv.finances.lombok.mel.techbean;

import org.springframework.core.io.Resource;

/**
 * Class PieceJointeInline.
 * 
 * @author ageffroy2-cp
 * @version $Revision: 1.2 $ Date: 15 déc. 2009
 */
public class PieceJointeInline
{

    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = -1672946764192661330L;

    /** contentId. */
    private String contentId;

    /** fileName. */
    private String fileName;

    /** source. */
    private Resource source;

    /**
     * Instanciation de piece jointe inline.
     * 
     * @param contentId
     * @param source --
     */
    public PieceJointeInline(String contentId, Resource source)
    {
        this(contentId, source, null);
    }

    /**
     * Instanciation de piece jointe inline.
     * 
     * @param contentId
     * @param source --
     * @param fileName --
     */
    public PieceJointeInline(String contentId, Resource source, String fileName)
    {
        this.source = source;
        this.contentId = contentId;
        this.fileName = fileName;
    }

    /**
     * Accesseur de l attribut contentId
     * 
     * @return contentId
     */
    public String getContentId()
    {
        return contentId;
    }

    /**
     * Accesseur de l attribut fileName
     * 
     * @return fileName
     */
    public String getFileName()
    {
        return fileName;
    }

    /**
     * Accesseur de l attribut source
     * 
     * @return source
     */
    public Resource getSource()
    {
        return source;
    }

}
