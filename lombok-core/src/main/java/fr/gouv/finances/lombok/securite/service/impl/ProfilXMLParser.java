/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : ProfilXMLParser.java
 *
 */
package fr.gouv.finances.lombok.securite.service.impl;

/**
 * author : amleplatinec-cp
 * decode d'une ligne détailprofil dela branche application du ldap pour une application
 * Detail profil en xml est de la forme suivante 
 * <DetailProfil>
 *<CodeProfil>C1</CodeProfil>
 *<LibelleProfil>Libelle1</LibelleProfil>
 *<CommentaireProfil>Commentaire1</CommentaireProfil>
 *<ASupprimer>0</ASupprimer>
 </DetailProfil>
 */

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import fr.gouv.finances.lombok.securite.techbean.ProfilAnnuaire;
import fr.gouv.finances.lombok.util.exception.ApplicationExceptionTransformateur;
import fr.gouv.finances.lombok.util.exception.ProgrammationException;

/**
 * Class ProfilXMLParser --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.3 $ Date: 11 déc. 2009
 */
public class ProfilXMLParser
{

    /** Constant : log. */
    private static final Log log = LogFactory.getLog(ProfilXMLParser.class);

    /** Constant : APTERA_BalCodeProfil. */
    private static final String APTERA_BalCodeProfil = "CodeProfil";

    /** Constant : APTERA_BalLibelleProfil. */
    private static final String APTERA_BalLibelleProfil = "LibelleProfil";

    /** Constant : APTERA_BalCommentaireProfil. */
    private static final String APTERA_BalCommentaireProfil = "CommentaireProfil";

    /** Constant : APTERA_BalASupprimer. */
    private static final String APTERA_BalASupprimer = "ASupprimer";

    /** Constant : APTERA_Separateur_DetailProfil. */
    private static final String APTERA_Separateur_DetailProfil = ";";

    /**
     * methode Profil decode avec separateur : --.
     * 
     * @param pProfil le profil
     * @return profil annuaire
     */
    public static ProfilAnnuaire profilDecodeAvecSeparateur(String pProfil)
    {
        ProfilAnnuaire unProfilAnnuaire = null;

        String[] attributsProfils = StringUtils.splitPreserveAllTokens(pProfil, APTERA_Separateur_DetailProfil);
        if (attributsProfils != null && attributsProfils.length > 0)
        {
            unProfilAnnuaire = new ProfilAnnuaire();
            unProfilAnnuaire.setCodeProfil(attributsProfils[0]);
            unProfilAnnuaire.setLibelleProfil(attributsProfils[1]);
            unProfilAnnuaire.setCommentaireProfil(attributsProfils[2]);
            unProfilAnnuaire.setASupprimer(attributsProfils[3]);
        }
        return unProfilAnnuaire;
    }

    /**
     * methode Profil decode xml : --.
     * 
     * @param pProfilXML le profil xml
     * @return profil annuaire
     */
    public static ProfilAnnuaire profilDecodeXML(String pProfilXML)
    {
        ProfilAnnuaire unProfilAnnuaire = null;
        Document document = null;
        InputStream xmlStream = null;
        Node Noeud_H0;

        byte[] xmlByteArray = pProfilXML.getBytes();
        xmlStream = new ByteArrayInputStream(xmlByteArray);

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

        try
        {
            DocumentBuilder parser = factory.newDocumentBuilder();
            factory.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
            document = parser.parse(new InputSource(xmlStream));

            unProfilAnnuaire = new ProfilAnnuaire();

            // récupération du tag ayant pour nom CodeProfil
            NodeList ListeNoeuds_H0 = document.getElementsByTagName(APTERA_BalCodeProfil);
            if (ListeNoeuds_H0 != null)
            {
                for (int h0 = 0; h0 < ListeNoeuds_H0.getLength(); h0++)
                {
                    Noeud_H0 = ListeNoeuds_H0.item(h0);
                    // récupération de tous les enfants pour récupérer la valeur du code profil
                    NodeList ListeNoeuds_H1 = Noeud_H0.getChildNodes();
                    for (int t = 0; t < ListeNoeuds_H1.getLength(); t++)
                    {
                        Node noeudTexte = ListeNoeuds_H1.item(t);
                        if (noeudTexte.getNodeType() == Node.TEXT_NODE)
                        {
                            unProfilAnnuaire.setCodeProfil(noeudTexte.getNodeValue());
                        }

                    }
                }
            }

            ListeNoeuds_H0 = document.getElementsByTagName(APTERA_BalLibelleProfil);
            if (ListeNoeuds_H0 != null)
            {
                for (int h0 = 0; h0 < ListeNoeuds_H0.getLength(); h0++)
                {
                    Noeud_H0 = ListeNoeuds_H0.item(h0);
                    NodeList ListeNoeuds_H1 = Noeud_H0.getChildNodes();

                    for (int t = 0; t < ListeNoeuds_H1.getLength(); t++)
                    {
                        Node noeudTexte = ListeNoeuds_H1.item(t);
                        if (noeudTexte.getNodeType() == Node.TEXT_NODE)
                        {
                            unProfilAnnuaire.setLibelleProfil(noeudTexte.getNodeValue());
                        }
                    }
                }
            }

            ListeNoeuds_H0 = document.getElementsByTagName(APTERA_BalCommentaireProfil);
            if (ListeNoeuds_H0 != null)
            {
                for (int h0 = 0; h0 < ListeNoeuds_H0.getLength(); h0++)
                {
                    Noeud_H0 = ListeNoeuds_H0.item(h0);
                    NodeList ListeNoeuds_H1 = Noeud_H0.getChildNodes();

                    for (int t = 0; t < ListeNoeuds_H1.getLength(); t++)
                    {
                        Node noeudTexte = ListeNoeuds_H1.item(t);
                        if (noeudTexte.getNodeType() == Node.TEXT_NODE)
                        {
                            unProfilAnnuaire.setCommentaireProfil(noeudTexte.getNodeValue());
                        }
                    }
                }
            }

            ListeNoeuds_H0 = document.getElementsByTagName(APTERA_BalASupprimer);
            if (ListeNoeuds_H0 != null)
            {
                for (int h0 = 0; h0 < ListeNoeuds_H0.getLength(); h0++)
                {
                    Noeud_H0 = ListeNoeuds_H0.item(h0);
                    NodeList ListeNoeuds_H1 = Noeud_H0.getChildNodes();

                    for (int t = 0; t < ListeNoeuds_H1.getLength(); t++)
                    {
                        Node noeudTexte = ListeNoeuds_H1.item(t);
                        if (noeudTexte.getNodeType() == Node.TEXT_NODE)
                        {
                            unProfilAnnuaire.setASupprimer(noeudTexte.getNodeValue());
                        }
                    }
                }
            }

        }
        catch (SAXException e)
        {
            log.warn("Erreur de traitement du document : " + pProfilXML);
            throw ApplicationExceptionTransformateur.transformer(
                "Erreur lors du traitement du fichier : " + pProfilXML, e);
        }
        catch (IOException e)
        {
            log.warn("Erreur de lecture du document : " + pProfilXML);
            throw ApplicationExceptionTransformateur.transformer(
                "Erreur lors de la lecture du fichier : " + pProfilXML, e);
        }
        catch (ParserConfigurationException e)
        {
            log.error("Aucun parseur DOM disponible");
            throw new ProgrammationException("Aucun parseur DOM disponible", e);
        }
        finally
        {
            try
            {
                if (xmlStream != null)
                {
                    // ferme le stream
                    xmlStream.close();
                }
            }
            catch (IOException ex)
            {
                log.warn("Impossible de fermer le stream", ex);
            }
        }
        return unProfilAnnuaire;

    }

}
