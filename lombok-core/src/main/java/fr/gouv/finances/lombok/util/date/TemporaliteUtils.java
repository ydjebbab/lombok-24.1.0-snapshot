/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 */
package fr.gouv.finances.lombok.util.date;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Date;

/**
 * Utilitaire de gestion de la temporalité
 *
 * @author Christophe Breheret-Girardin
 */
public final class TemporaliteUtils
{
    /**
     * Constructeur.
     */
    private TemporaliteUtils()
    {
        super();
    }

    /**
     * Méthode permettant de générer une date.
     *
     * @param annee année de la date
     * @param mois mois de la date (de 1 à 12)
     * @param jour jour de la date (de 1 à 31)
     * @return la date générée
     */
    public static Date getDate(int annee, int mois, int jour)
    {
        LocalDate localDate = LocalDate.of(annee, mois, jour);
        return Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
    }

    /**
     * Méthode permettant de générer une date.
     *
     * @param annee année de la date
     * @param mois mois de la date (de 1 à 12)
     * @param jour jour de la date (de 1 à 31)
     * @param heure heure
     * @param minute minute
     * @param seconde seconde
     * @return la date générée
     */
    public static Date getDate(int annee, int mois, int jour, int heure, int minute, int seconde)
    {
        LocalDateTime localDateTime = LocalDateTime.of(annee, mois, jour, heure, minute, seconde);
        return Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
    }

    /**
     * Méthode permettant de générer une date au format java.util.Date.
     *
     * @param date sous la forme de chaine de caractère
     * @param pattern format de la date
     * @return la date sous la forme d'un objet java.util.Date
     */
    public static Date getDate(String date, String pattern)
    {
        return Date.from(LocalDate.parse(date, DateTimeFormatter.ofPattern(pattern))
            .atStartOfDay(ZoneId.systemDefault()).toInstant());
    }

    /**
     * Méthode permettant de générer une date sous la forme d'un TimeStamp.
     *
     * @param annee année de la date
     * @param mois mois de la date (de 1 à 12)
     * @param jour jour de la date (de 1 à 31)
     * @return la date générée sous forme de TimeStamp
     */
    public static Timestamp getTimestamp(int annee, int mois, int jour)
    {
        LocalDate localDate = LocalDate.of(annee, mois, jour);
        return Timestamp.valueOf(localDate.atStartOfDay());
    }

    /**
     * Méthode permettant de générer une date sous la forme d'un TimeStamp.
     *
     * @param annee année de la date
     * @param mois mois de la date (de 1 à 12)
     * @param jour jour de la date (de 1 à 31)
     * @param heure heure de l'horaire de la date
     * @param minute minute de l'horaire de la date
     * @param seconde seconde de l'horaire de la date
     * @return la date + heure générée sous forme de TimeStamp
     */
    public static Timestamp getTimestamp(int annee, int mois, int jour, int heure, int minute, int seconde)
    {
        LocalDateTime localDateTime = LocalDate.of(annee, mois, jour).atTime(heure, minute, seconde);
        return Timestamp.valueOf(localDateTime);
    }

    /**
     * Méthode permettant de générer une date sous la forme d'un TimeStamp.
     *
     * @param annee année de la date
     * @param mois mois de la date (de 1 à 12)
     * @param jour jour de la date (de 1 à 31)
     * @return la date générée sous forme de TimeStamp
     */
    public static Timestamp getTimestamp()
    {
        return Timestamp.valueOf(LocalDateTime.now());
    }

    /**
     * Méthode permettant de générer la date de demain.
     *
     * @return la date de demain
     */
    public static Timestamp getDemain()
    {
        return Timestamp.valueOf(LocalDate.now().plusDays(1).atStartOfDay());
    }

    /**
     * Méthode permettant d'ajouter un jour à une date.
     * @param date date initiale
     * @param jour jour à ajouter à la date initiale
     *
     * @return la date initiale + le nombre de jour fourni
     */
    public static Date ajouterJour(Date date, long jour)
    {
        LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        return Date.from(localDate.plusDays(jour).atStartOfDay(ZoneId.systemDefault()).toInstant());
    }

    /**
     * Méthode permettant de convertir une java.util.Date en java.time.LocalDateTime.
     *
     * @param date date à convertir
     * @return date convertie
     */
    public static LocalDateTime convert(Date date)
    {
        return date.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
    }

    /**
     * Méthode permettant de convertir une java.time.LocalDateTime en java.util.Date.
     *
     * @param localDateTime date à convertir
     * @return date convertie
     */
    public static Date convert(LocalDateTime localDateTime)
    {
        return Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
    }

    /**
     * Méthode permettant de supprimer la partie horaire d'une java.time.LocalDateTime.
     *
     * @param localDateTime temporalité à traiter
     * @return temporalité traitée
     */
    public static LocalDateTime supprimerHoraire(LocalDateTime localDateTime)
    {
        return localDateTime.truncatedTo(ChronoUnit.DAYS);
    }
}