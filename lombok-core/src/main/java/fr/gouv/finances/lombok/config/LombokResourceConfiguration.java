package fr.gouv.finances.lombok.config;

import javax.inject.Named;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import fr.gouv.finances.lombok.mel.service.MelService;
import fr.gouv.finances.lombok.stubs.StubMelService;
import fr.gouv.finances.lombok.stubs.StubMelServiceRedirect;

@Configuration
@ComponentScan(basePackages = {"fr.gouv.finances.lombok.mel.service.impl"})
public class LombokResourceConfiguration
{
    protected final Log log = LogFactory.getLog(getClass());

    @Value(value = "${appli.notenvoimel}")
    private String notenvoimel;

    @Autowired
    @Named("clientmelparent")
    private MelService clientMelParent;

    @Bean(name = "serveurmel")
    @ConditionalOnExpression("!'${appli.notenvoimel}'.startsWith('redirect:') && !'${appli.notenvoimel}'.equals('notenvoimel')")
    public MelService serveurmel()
    {
        return clientMelParent;
    }

    @Bean(name = "serveurmel")
    @ConditionalOnProperty(prefix = "appli", name = "notenvoimel", havingValue = "notenvoimel")
    public MelService stubserveurmel()
    {
        log.warn(
            "Attention pas de connexion reelle au serveur de mail -  mode simulation ne convient pas en production - modifiez le fichier application.properties");
        return new StubMelService();
    }

    @Bean(name = "serveurmel")
    @ConditionalOnExpression("'${appli.notenvoimel}'.startsWith('redirect:')")
    public MelService stubserveurmelredirect()
    {
        StubMelServiceRedirect melService = new StubMelServiceRedirect();
        melService.setNotenvoimel(notenvoimel);
        melService.setServeurmelstubredirect(clientMelParent);
        log.warn(
                "Attention mails reroutés vers un ou des destinataires spécifiques ne convient pas en production - modifiez le fichier application.properties");
        return melService;
    }
}
