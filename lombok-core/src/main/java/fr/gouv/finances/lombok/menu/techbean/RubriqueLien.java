/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : RubriqueLien.java
 *
 */
package fr.gouv.finances.lombok.menu.techbean;

/**
 * Class RubriqueLien Classe représentant une extremite de menu hierarchique portant un lien de type url.
 * 
 * @author lcontinsouzas-cp
 * @version $Revision: 1.2 $ Date: 15 déc. 2009
 */
public class RubriqueLien extends Rubrique
{

    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** url. */
    public String url = null;

    /** target. */
    public String target = null;

    /** verifier url. */
    public String verifierUrl = "O";

    /** url est autorisee. */
    private Boolean urlEstAutorisee = null;

    public RubriqueLien()
    {
        super();
    }

    public void calculerEstaffichable()
    {
        super.calculerEstAffichable(!urlEstAutorisee.booleanValue());

    }

    /**
     * Accesseur de l attribut url est autorisee.
     * 
     * @return url est autorisee
     */
    public Boolean getUrlEstAutorisee()
    {
        return urlEstAutorisee;
    }

    /**
     * Modificateur de l attribut url est autorisee.
     * 
     * @param urlestautorisee le nouveau url est autorisee
     */
    public void setUrlEstAutorisee(Boolean urlestautorisee)
    {
        this.urlEstAutorisee = urlestautorisee;
    }
}
