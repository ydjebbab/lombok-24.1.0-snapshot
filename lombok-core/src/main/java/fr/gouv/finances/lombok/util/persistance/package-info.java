/*
 * Copyright (c) 2017 DGFiP - Tous droits réservés
 * 
*/
/**
 * Paquet regroupant les utilitaires de persistance.
 * @author Christophe Breheret-Girardin
 */
package fr.gouv.finances.lombok.util.persistance;