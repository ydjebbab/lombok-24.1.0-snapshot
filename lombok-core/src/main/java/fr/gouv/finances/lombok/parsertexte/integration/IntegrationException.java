/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 */
package fr.gouv.finances.lombok.parsertexte.integration;

/**
 * Class IntegrationException --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.3 $ Date: 11 déc. 2009
 */
public class IntegrationException extends Exception
{

    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = 100114L;

    /** num ligne. */
    private int numLigne = -1;

    /** message. */
    private String message = null;

    /** id ligne. */
    private String idLigne = null;

    /** id attribut. */
    private String idAttribut = null;

    /** valeur. */
    private String valeur = null;

    /**
     * Instanciation de integration exception.
     */
    public IntegrationException()
    {
        super();
    }

    /**
     * Instanciation de integration exception.
     * 
     * @param arg0 --
     */
    public IntegrationException(String arg0)
    {
        super(arg0);
    }

    /**
     * Instanciation de integration exception.
     * 
     * @param message --
     * @param idAttribut --
     * @param valeur --
     */
    public IntegrationException(String message, String idAttribut, String valeur)
    {
        super();
        this.message = message;
        this.idAttribut = idAttribut;
        this.valeur = valeur;
    }

    /**
     * Instanciation de integration exception.
     * 
     * @param message --
     * @param idLigne --
     * @param idAttribut --
     * @param numLigne --
     * @param valeur --
     */
    public IntegrationException(String message, String idLigne, String idAttribut, int numLigne, String valeur)
    {
        super();
        this.message = message;
        this.idLigne = idLigne;
        this.idAttribut = idAttribut;
        this.numLigne = numLigne;
        this.valeur = valeur;
    }

    /**
     * Instanciation de integration exception.
     * 
     * @param arg0 --
     * @param arg1 --
     */
    public IntegrationException(String arg0, Throwable arg1)
    {
        super(arg0, arg1);
    }

    /**
     * Instanciation de integration exception.
     * 
     * @param arg0 --
     */
    public IntegrationException(Throwable arg0)
    {
        super(arg0);
    }

    /**
     * Accesseur de l attribut id attribut.
     * 
     * @return id attribut
     */
    public String getIdAttribut()
    {
        return idAttribut;
    }

    /**
     * Accesseur de l attribut id ligne.
     * 
     * @return id ligne
     */
    public String getIdLigne()
    {
        return idLigne;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @return the message
     * @see java.lang.Throwable#getMessage()
     */
    public String getMessage()
    {
        return message;
    }

    /**
     * Accesseur de l attribut num ligne.
     * 
     * @return num ligne
     */
    public int getNumLigne()
    {
        return numLigne;
    }

    /**
     * Accesseur de l attribut valeur.
     * 
     * @return valeur
     */
    public String getValeur()
    {
        return valeur;
    }

    /**
     * Modificateur de l attribut id attribut.
     * 
     * @param idAttribut le nouveau id attribut
     */
    public void setIdAttribut(String idAttribut)
    {
        this.idAttribut = idAttribut;
    }

    /**
     * Modificateur de l attribut id ligne.
     * 
     * @param idLigne le nouveau id ligne
     */
    public void setIdLigne(String idLigne)
    {
        this.idLigne = idLigne;
    }

    /**
     * Modificateur de l attribut message.
     * 
     * @param message le nouveau message
     */
    public void setMessage(String message)
    {
        this.message = message;
    }

    /**
     * Modificateur de l attribut num ligne.
     * 
     * @param numLigne le nouveau num ligne
     */
    public void setNumLigne(int numLigne)
    {
        this.numLigne = numLigne;
    }

    /**
     * Modificateur de l attribut valeur.
     * 
     * @param valeur le nouveau valeur
     */
    public void setValeur(String valeur)
    {
        this.valeur = valeur;
    }

}
