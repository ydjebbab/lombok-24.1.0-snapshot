/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - ageffroy2-cp
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : SessionExpirationFilter.java
 *
 */
package fr.gouv.finances.lombok.securite.springsecurity;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class SessionExpirationFilter .
 * 
 * @author ageffroy2-cp
 * @version $Revision: 1.1 $ Date: 8 juil. 2010
 */
public class SessionExpirationFilter implements Filter
{
    private static final Logger log = LoggerFactory.getLogger(SessionExpirationFilter.class);

    /** url expiration. */
    private String urlExpiration;

    /** afficher expiration. */
    private boolean afficherExpiration;

    /**
     * Constructeur de la classe SessionExpirationFilter.java
     */
    public SessionExpirationFilter()
    {
        super();
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @see javax.servlet.Filter#destroy()
     */
    @Override
    public void destroy()
    {
        // RAS
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param req "documenté"
     * @param res "documenté"
     * @param chain "documenté"
     * @throws IOException Signal qu'une execption de type I/O s'est produite.
     * @throws ServletException the servlet exception
     * @see javax.servlet.Filter#doFilter(javax.servlet.ServletRequest, javax.servlet.ServletResponse,
     *      javax.servlet.FilterChain)
     */
    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException,
        ServletException
    {
        log.debug(">>> Debut methode doFilter()");
        if (req instanceof HttpServletRequest)
        {
            HttpServletRequest hReq = (HttpServletRequest) req;
            HttpServletResponse hRes = (HttpServletResponse) res;
            HttpSession session = hReq.getSession(false);
            if (afficherExpiration && session == null && !hReq.isRequestedSessionIdValid())
            {
                log.debug("Expiration de session, redirection vers : " + urlExpiration);
                String targetUrl = hReq.getContextPath() + urlExpiration;
                hRes.sendRedirect(hRes.encodeRedirectURL(targetUrl));
            }
            else
            {
                chain.doFilter(req, res);
            }
        }
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param arg0 "documenté"
     * @throws ServletException the servlet exception
     * @see javax.servlet.Filter#init(javax.servlet.FilterConfig)
     */
    @Override
    public void init(FilterConfig arg0) throws ServletException
    {
        // RAS
    }

    /**
     * Modificateur de l attribut afficher expiration.
     * 
     * @param afficherExpiration le nouveau afficher expiration
     */
    public void setAfficherExpiration(boolean afficherExpiration)
    {
        this.afficherExpiration = afficherExpiration;
    }

    /**
     * Modificateur de l attribut url expiration.
     * 
     * @param urlExpiration le nouveau url expiration
     */
    public void setUrlExpiration(String urlExpiration)
    {
        this.urlExpiration = urlExpiration;
    }

}
