/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : LectureApplicationServiceImpl.java
 *
 */
package fr.gouv.finances.lombok.securite.service.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;

import fr.gouv.finances.lombok.securite.service.LectureApplicationService;
import fr.gouv.finances.lombok.securite.techbean.AnnuaireException;
import fr.gouv.finances.lombok.securite.techbean.ApplicationAnnuaire;
import fr.gouv.finances.lombok.securite.techbean.ProfilAnnuaire;

/**
 * Class LectureApplicationServiceImpl
 * 
 * @author amleplatinec
 * @version $Revision: 1.3 $ Date: 11 déc. 2009
 */
public class LectureApplicationServiceImpl extends AbstractLectureInformationsAnnuaireServiceImpl implements
    LectureApplicationService
{

    /** Constant : PB_CONNEXION_ANNUAiRE_DN. */
    private static final String PB_CONNEXION_ANNUAiRE_DN =
        "Problème de connexion à l'annuaire lors de la recherche d'un DN : ";

    /*
     * Branche sur laquelle on se positionne pour effectuer la recherche du DN d'une application
     */
    /** base recherche ldap. */
    private String baseRechercheLDAP = "ou=applications,ou=DGCP,ou=MEFI,o=gouv,c=fr";

    public LectureApplicationServiceImpl()
    {
        super();

    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @return the base recherche ldap
     * @see fr.gouv.finances.lombok.securite.service.LectureApplicationService#getBaseRechercheLDAP()
     */
    @Override
    public String getBaseRechercheLDAP()
    {
        return baseRechercheLDAP;
    }

    /**
     * recherche d'une application et de ses profils décodés dans le ldap à partir du libellé court.
     * 
     * @param libelleCourt --
     * @return the application annuaire
     */
    public ApplicationAnnuaire rechercherApplicationParLibelleCourt(String libelleCourt)
    {

        ApplicationAnnuaire applicationAnnuaire = new ApplicationAnnuaire();
        DirContext unApplicationDirContext = null;
        List<Object> listDetailProfil = new ArrayList<Object>();
        Set<ProfilAnnuaire> listProfil = new HashSet<ProfilAnnuaire>();
        try
        {
            if (log.isDebugEnabled())
            {
                log.debug(" entree rechercherApplicationParLibelleCourt ");
            }

            /* ouverture de la connexion applicative vers l'annuaire */
            unApplicationDirContext = this.ouvreLDAPConnexionApplication();

            String filtreRecherche = "libelleCourtAppli=" + libelleCourt;
            SearchControls controlesRecherche = new SearchControls();
            controlesRecherche.setSearchScope(SearchControls.SUBTREE_SCOPE);
            // précise les attributs que l on veut ramener dans
            // notre objet
            // amlp 19/3/07 rajout de typeGestionStructure
            String[] listeAttributs = {"libelleCourtAppli", "libelleLongAppli", "detailProfil", "typeGestionStructure"};
            controlesRecherche.setReturningAttributes(listeAttributs);
            if (log.isDebugEnabled())
            {
                log.debug("Recherche application  " + libelleCourt);
            }

            try
            {
                NamingEnumeration<?> resultats =
                    unApplicationDirContext.search(baseRechercheLDAP, filtreRecherche, controlesRecherche);

                /*
                 * exploration des résultats : a priori un seul car dans uid il y a "unique" s'il y en a plusieurs (???)
                 * : c'est le premier qui sera pris
                 */
                if (resultats.hasMore())
                {
                    /* extraction du 1er resultat */
                    SearchResult searchResult = (SearchResult) resultats.next();
                    // normallement un seul résultat doit
                    // être trouvé

                    Attributes attributes = searchResult.getAttributes();
                    Attribute attr = attributes.get("libelleCourtAppli");
                    String libelleCourtAppli = (String) attr.get();
                    applicationAnnuaire.setLibelleCourtAppli(libelleCourtAppli);
                    attr = attributes.get("libelleLongAppli");
                    applicationAnnuaire.setLibelleLongAppli((String) attr.get());
                    attr = attributes.get("typeGestionStructure");
                    applicationAnnuaire.setTypeGestionStructure((String) attr.get());
                    attr = attributes.get("detailProfil");

                    int i;
                    for (i = 0; i < attr.size(); i++)
                    {
                        Object key = attr.get(i);
                        // key correspond à une ligne de
                        // détail profil dans l'annuaire
                        listDetailProfil.add(key);
                        // on decode profil
                        ProfilAnnuaire profil = this.extraireProfilDecode(key.toString());
                        if (profil != null)
                        {
                            listProfil.add(profil);
                        }

                    }

                    applicationAnnuaire.setDetailProfil(listDetailProfil);
                    applicationAnnuaire.setDetailProfilDecode(listProfil);
                }
                else
                {
                    if (log.isDebugEnabled())
                    {
                        log.debug("DN application non trouve ");
                    }
                }
            }
            catch (NamingException e)
            {
                throw new AnnuaireException(PB_CONNEXION_ANNUAiRE_DN, e);
            }
            return applicationAnnuaire;

        }
        finally
        {
            this.fermerLDAPConnexionApplication(unApplicationDirContext);
        }

    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param baseRechercheLDAP the new base recherche ldap
     * @see fr.gouv.finances.lombok.securite.service.LectureApplicationService#setBaseRechercheLDAP(java.lang.String)
     */
    public void setBaseRechercheLDAP(String baseRechercheLDAP)
    {
        this.baseRechercheLDAP = baseRechercheLDAP;
    }

    /**
     * methode Extraire profil decode : --.
     * 
     * @param detailProfil --
     * @return profil annuaire
     */
    protected ProfilAnnuaire extraireProfilDecode(String detailProfil)
    {
        // modif amlp 10/09/07 GAP a encore ses profils avec un
        // séparateur
        // on rajoute une méthode pour decoder des profils pas XML

        String str1 = detailProfil.trim();
        String str2 = str1.replaceAll(",", "");
        ProfilAnnuaire profilAnnuaire;

        if (log.isDebugEnabled())
        {
            log.debug(" entree extraireProfilDecode ");
        }
        try
        {

            if (this.testerSiFormatXMLouPas(str2))
            {

                profilAnnuaire = ProfilXMLParser.profilDecodeXML(str2);
            }

            else

            {
                profilAnnuaire = ProfilXMLParser.profilDecodeAvecSeparateur(str2);
            }
        }
        catch (RuntimeException e)
        {
            throw new AnnuaireException(e.getMessage(), e);
        }
        if (log.isDebugEnabled())
        {
            log.debug(" sortie extraireProfilDecode ");
        }
        return profilAnnuaire;
    }

    /**
     * methode Tester si format xm lou pas : --.
     * 
     * @param pProfil le profil
     * @return true, si c'est vrai
     */
    private boolean testerSiFormatXMLouPas(String pProfil)
    {
        boolean retour = false;
        if (pProfil != null && pProfil.startsWith("<DetailProfil>"))
        {
            retour = true;
        }
        else
        {
            retour = false;
        }
        return retour;
    }

}
