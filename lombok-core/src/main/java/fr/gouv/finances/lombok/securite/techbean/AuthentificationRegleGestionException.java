/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 */
package fr.gouv.finances.lombok.securite.techbean;

import org.springframework.security.core.AuthenticationException;

/* modification amlp : on change RegleGestion en AuthenticationException pour pouvoir diriger
 ces exceptions sur une pages d'erreur type lombok (sinon si mauvaise ip par exemple
 on a une page blanche erreur serveur */
/**
 * Class AuthentificationRegleGestionException.
 * 
 * @author lcontinsouzas-cp
 * @version $Revision: 1.3 $ Date: 15 déc. 2009
 */
public class AuthentificationRegleGestionException extends AuthenticationException
{

    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /**
     * Instanciation de authentification regle gestion exception.
     * 
     * @param message --
     */
    public AuthentificationRegleGestionException(String message)
    {
        super(message);
    }

    /**
     * Instanciation de authentification regle gestion exception.
     * 
     * @param message --
     * @param cause --
     */
    public AuthentificationRegleGestionException(String message, Throwable cause)
    {
        super(message, cause);
    }

}
