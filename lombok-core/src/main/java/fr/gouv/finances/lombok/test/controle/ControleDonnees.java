/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 */
package fr.gouv.finances.lombok.test.controle;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Socle de contrôle de données, utilisé principalement au sein des TU.
 *
 * @author Christophe Breheret-Girardin
 */
public final class ControleDonnees
{
    private ControleDonnees()
    {
        super();
    }

    /**
     * Méthode permettant de contrôler des éléments par rapport à ceux attendus.
     *
     * @param T élément manipulé
     * @param elements les éléments à contrôler
     * @param elementsAttendus les éléments attendus
     */
    public static <T> void verifierElements(Collection<T> elements
        , @SuppressWarnings("unchecked") T... elementsAttendus)
    {
        verifierElements(elements, false, elementsAttendus);
    }

    /**
     * Méthode permettant de contrôler des éléments par rapport à ceux attendus.
     *
     * @param T élément manipulé
     * @param elements les éléments à contrôler
     * @param elementsAttendus les éléments attendus
     */
    @SuppressWarnings("unchecked")
    public static <T> void verifierElements(Collection<T> elements, Collection<T> elementsAttendus)
    {
        verifierElements(elements, (T[]) elementsAttendus.stream().toArray());
    }

    /**
     * Méthode permettant de contrôler des éléments par rapport à ceux attendus, avec éventuellement
     * la prise en compte de l'ordre.
     *
     * @param T élément manipulé
     * @param elements les éléments à contrôler
     * @param tri les éléments doivent-il être ordonnés comme ceux attendus ?
     * @param elementsAttendus les éléments attendus
     */
    public static <T> void verifierElements(Collection<T> elements, boolean tri
        , @SuppressWarnings("unchecked") T... elementsAttendus)
    {
        assertNotNull("La liste n'a pas été initialisée", elements);
        assertEquals("Ce n'est pas le bon nombre d'élément", elementsAttendus.length, elements.size());

        if (tri)
        {
            // Vérification en prenant en compte l'ordre
            int indice = -1;
            for (T element : elements)
            {
                assertEquals("Ce n'est pas l'élément attendu (l'ordre est pris en compte)"
                    , elementsAttendus[++indice], element); 
            }
        }
        else
        {
            // Vérification sans prendre en compte l'ordre
            // Création d'un Set à partir des éléments attendus
            Set<T> eltsAttendus = new HashSet<>(Arrays.asList(elementsAttendus));

            // Les éléments sont-ils ceux attendus ?
            assertEquals("Ce ne sont pas les éléments attendus (sans prendre en compte l'ordre)"
                , eltsAttendus, elements.stream().map(element -> element).collect(Collectors.toSet()));     
        }
    }
}