/*
/*Copyright (c) 2014 DGFiP - Tous droits réservés/* * Copyright 2004 original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package fr.gouv.finances.lombok.util.comparators;

import java.lang.reflect.InvocationTargetException;
import java.util.Comparator;

import org.apache.commons.beanutils.BeanComparator;
import org.apache.commons.beanutils.NestedNullException;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Override BeanComparator to workaround the NestedNullException thrown by PropertyUtils.getProperty(...) when a parent
 * object in the hierarchy is null.
 */
public class NullSafeBeanComparator extends BeanComparator
{

    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** log. */
    private Log log = LogFactory.getLog(NullSafeBeanComparator.class);

    /** nulls are high. */
    protected boolean nullsAreHigh = true;

    /** property. */
    protected String property;

    /** comparator. */
    protected Comparator<Object> comparator;


    /**
     * Instanciation de null safe bean comparator.
     * 
     * @param property
     * @param c
     */
    public NullSafeBeanComparator(String property, Comparator<Object> c)
    {
        this.comparator = c;
        this.property = property;
    }

    /**
     * Instanciation de null safe bean comparator.
     * 
     * @param property
     * @param c
     * @param nullAreHigh
     */
    public NullSafeBeanComparator(String property, Comparator<Object> c, boolean nullAreHigh)
    {
        this.comparator = c;
        this.property = property;
        this.nullsAreHigh = nullAreHigh;

    }
    
    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see org.apache.commons.beanutils.BeanComparator#getComparator()
     */
    public Comparator<Object> getComparator()
    {
        return comparator;
    }

    /**
     * Modificateur de l attribut comparator.
     * 
     * @param comparator le nouveau comparator
     */
    public void setComparator(Comparator<Object> comparator)
    {
        this.comparator = comparator;
    }

    /**
     * Verifie si nulls are high.
     * 
     * @return true, si c'est nulls are high
     */
    public boolean isNullsAreHigh()
    {
        return nullsAreHigh;
    }

    /**
     * Modificateur de l attribut nulls are high.
     * 
     * @param nullsAreHigh le nouveau nulls are high
     */
    public void setNullsAreHigh(boolean nullsAreHigh)
    {
        this.nullsAreHigh = nullsAreHigh;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see org.apache.commons.beanutils.BeanComparator#getProperty()
     */
    public String getProperty()
    {
        return property;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see org.apache.commons.beanutils.BeanComparator#setProperty(java.lang.String)
     */
    public void setProperty(String property)
    {
        this.property = property;
    }

    /**
     * Compare des beans. Ignore l'exception NestedNullException levée lorsque l'objet parent est null
     * 
     * @param object1
     * @param object2
     * @return int
     */
    public int compare(Object object1, Object object2)
    {

        int result = 0;

        if (property == null)
        {
            // Utilisation la méthode compare de l'objet si aucune propriété n'est spécifiée
            result = this.comparator.compare(object1, object2);
        }
        else
        {

            Object val1 = null;
            Object val2 = null;

            try
            {
                val1 = getVal(object1, val1);

                val2 = getVal(object2, val2);

                if (val1 == null && val2 == null)
                {
                    result = 0;
                }

                else if (val1 == null)
                {
                    result = this.nullsAreHigh ? 1 : -1;
                }

                else if (val2 == null)
                {
                    result = this.nullsAreHigh ? -1 : 1;
                }
                else
                {
                    result = this.comparator.compare(val1, val2);
                }
            }
            catch (Exception exception)
            {
                log.warn("Erreur", exception);
            }
        }
        return result;
    }

    /**
     * DOCUMENTEZ_MOI
     *
     * @param object1
     * @param val1
     * @return
     * @throws IllegalAccessException
     * @throws InvocationTargetException
     * @throws NoSuchMethodException
     */
    private Object getVal(Object object1, Object val1) throws IllegalAccessException, InvocationTargetException, NoSuchMethodException
    {
        try
        {
            val1 = PropertyUtils.getProperty(object1, property);

        }
        catch (NestedNullException ignored)
        {
            // Exception ignorée
        }
        return val1;
    }

}