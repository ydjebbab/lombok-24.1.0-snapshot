package fr.gouv.finances.lombok.autoconfig;

import org.springframework.boot.context.properties.ConfigurationProperties;

import fr.gouv.finances.lombok.autoconfig.LombokComposantProperties;

/**
 * @author celinio fernandes Date: Feb 19, 2020
 */
@ConfigurationProperties(prefix = "lombok.composant.clamav")
public class ClamavProperties extends LombokComposantProperties
{

    private StatutActivite actif;

    private String serverAddress;

    private int serverPort;

    private int timeout;

    private int connectTimeout;

    public StatutActivite getActif()
    {
        return actif;
    }

    public void setActif(StatutActivite actif)
    {
        this.actif = actif;
    }

    public String getServerAddress()
    {
        return serverAddress;
    }

    public void setServerAddress(String serverAddress)
    {
        this.serverAddress = serverAddress;
    }

    public int getServerPort()
    {
        return serverPort;
    }

    public void setServerPort(int serverPort)
    {
        this.serverPort = serverPort;
    }

    public int getTimeout()
    {
        return timeout;
    }

    public void setTimeout(int timeout)
    {
        this.timeout = timeout;
    }

    public int getConnectTimeout()
    {
        return connectTimeout;
    }

    public void setConnectTimeout(int connectTimeout)
    {
        this.connectTimeout = connectTimeout;
    }

    @Override
    public String toString()
    {
        return "ClamavProperties [inclus=" + inclus + ", actif=" + actif + ", serverAddress=" + serverAddress + ", serverPort=" + serverPort
            + ", timeout=" + timeout + ", connectTimeout=" + connectTimeout + "]";
    }
}
