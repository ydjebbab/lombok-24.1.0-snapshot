/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 */
package fr.gouv.finances.lombok.parsertexte.parametrage;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Class LigneFichier --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.2 $ Date: 11 déc. 2009
 */
public class LigneFichier
{

    /** id. */
    String id = null;

    /** separateur. */
    String separateur = null;

    /** nombre de lignes. */
    Integer nombreDeLignes = null;

    /** ligne a ignorer. */
    Boolean ligneAIgnorer = new Boolean(false); // Indique si après avoir "découvert" cette ligne elle doit être ignorée

    // ou pas

    /** nb elements. */
    int nbElements = -1;

    /** nb elements mini. */
    int nbElementsMini = -1;

    /** nb elements maxi. */
    int nbElementsMaxi = -1;

    /** taille. */
    int taille = -1;

    /** taille mini. */
    int tailleMini = -1;

    /** taille maxi. */
    int tailleMaxi = -1;

    /* On gère une ou plusieurs classes destination pour une ligne de fichier. */
    /** classes destination. */
    Map classesDestination = null;

    /* propriétés permettant de savoir comment reconnaitre un type de ligne d'un autre type */
    /** discriminant ligne. */
    DiscriminantLigne discriminantLigne = null;

    /* Liste des propriétés de la ligne de fichier */
    /** proprietes. */
    List proprietes = null;

    /**
     * Instanciation de ligne fichier.
     */
    public LigneFichier()
    {
        super();
    }

    /**
     * Ajouter propriete.
     * 
     * @param propriete --
     */
    public void ajouterPropriete(ProprieteLigne propriete)
    {
        if (this.proprietes == null)
        {
            this.proprietes = new ArrayList();
        }
        if (propriete != null)
        {
            this.proprietes.add(propriete);
        }
    }

    /**
     * Accesseur de l attribut classes destination.
     * 
     * @return classes destination
     */
    public Map getClassesDestination()
    {
        return classesDestination;
    }

    /**
     * Accesseur de l attribut discriminant ligne.
     * 
     * @return discriminant ligne
     */
    public DiscriminantLigne getDiscriminantLigne()
    {
        return discriminantLigne;
    }

    /**
     * Accesseur de l attribut id.
     * 
     * @return id
     */
    public String getId()
    {
        return id;
    }

    /**
     * Accesseur de l attribut ligne a ignorer.
     * 
     * @return ligne a ignorer
     */
    public Boolean getLigneAIgnorer()
    {
        return ligneAIgnorer;
    }

    /**
     * Accesseur de l attribut nb elements.
     * 
     * @return nb elements
     */
    public int getNbElements()
    {
        return nbElements;
    }

    /**
     * Accesseur de l attribut nb elements maxi.
     * 
     * @return nb elements maxi
     */
    public int getNbElementsMaxi()
    {
        return nbElementsMaxi;
    }

    /**
     * Accesseur de l attribut nb elements mini.
     * 
     * @return nb elements mini
     */
    public int getNbElementsMini()
    {
        return nbElementsMini;
    }

    /**
     * Accesseur de l attribut nombre de lignes.
     * 
     * @return nombre de lignes
     */
    public Integer getNombreDeLignes()
    {
        return nombreDeLignes;
    }

    /**
     * Accesseur de l attribut proprietes.
     * 
     * @return proprietes
     */
    public List getProprietes()
    {
        return proprietes;
    }

    /**
     * Accesseur de l attribut separateur.
     * 
     * @return separateur
     */
    public String getSeparateur()
    {
        return separateur;
    }

    /**
     * Accesseur de l attribut taille.
     * 
     * @return taille
     */
    public int getTaille()
    {
        return taille;
    }

    /**
     * Accesseur de l attribut taille maxi.
     * 
     * @return taille maxi
     */
    public int getTailleMaxi()
    {
        return tailleMaxi;
    }

    /**
     * Accesseur de l attribut taille mini.
     * 
     * @return taille mini
     */
    public int getTailleMini()
    {
        return tailleMini;
    }

    /**
     * Modificateur de l attribut classes destination.
     * 
     * @param classesDestination le nouveau classes destination
     */
    public void setClassesDestination(Map classesDestination)
    {
        this.classesDestination = classesDestination;
    }

    /**
     * Modificateur de l attribut discriminant ligne.
     * 
     * @param discriminantLigne le nouveau discriminant ligne
     */
    public void setDiscriminantLigne(DiscriminantLigne discriminantLigne)
    {
        this.discriminantLigne = discriminantLigne;
    }

    /**
     * Modificateur de l attribut id.
     * 
     * @param id le nouveau id
     */
    public void setId(String id)
    {
        this.id = id;
    }

    /**
     * Modificateur de l attribut ligne a ignorer.
     * 
     * @param ligneAIgnorer le nouveau ligne a ignorer
     */
    public void setLigneAIgnorer(Boolean ligneAIgnorer)
    {
        this.ligneAIgnorer = ligneAIgnorer;
    }

    /**
     * Modificateur de l attribut nb elements.
     * 
     * @param nbElements le nouveau nb elements
     */
    public void setNbElements(int nbElements)
    {
        this.nbElements = nbElements;
    }

    /**
     * Modificateur de l attribut nb elements maxi.
     * 
     * @param nbElementsMaxi le nouveau nb elements maxi
     */
    public void setNbElementsMaxi(int nbElementsMaxi)
    {
        this.nbElementsMaxi = nbElementsMaxi;
    }

    /**
     * Modificateur de l attribut nb elements mini.
     * 
     * @param nbElementsMini le nouveau nb elements mini
     */
    public void setNbElementsMini(int nbElementsMini)
    {
        this.nbElementsMini = nbElementsMini;
    }

    /**
     * Modificateur de l attribut nombre de lignes.
     * 
     * @param nombreDeLignes le nouveau nombre de lignes
     */
    public void setNombreDeLignes(Integer nombreDeLignes)
    {
        this.nombreDeLignes = nombreDeLignes;
    }

    /**
     * Modificateur de l attribut proprietes.
     * 
     * @param proprietes le nouveau proprietes
     */
    public void setProprietes(List proprietes)
    {
        this.proprietes = proprietes;
    }

    /**
     * Modificateur de l attribut separateur.
     * 
     * @param separateur le nouveau separateur
     */
    public void setSeparateur(String separateur)
    {
        this.separateur = separateur;
    }

    /**
     * Modificateur de l attribut taille.
     * 
     * @param taille le nouveau taille
     */
    public void setTaille(int taille)
    {
        this.taille = taille;
    }

    /**
     * Modificateur de l attribut taille maxi.
     * 
     * @param tailleMaxi le nouveau taille maxi
     */
    public void setTailleMaxi(int tailleMaxi)
    {
        this.tailleMaxi = tailleMaxi;
    }

    /**
     * Modificateur de l attribut taille mini.
     * 
     * @param tailleMini le nouveau taille mini
     */
    public void setTailleMini(int tailleMini)
    {
        this.tailleMini = tailleMini;
    }
}
