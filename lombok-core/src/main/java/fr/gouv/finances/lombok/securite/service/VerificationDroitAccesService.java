/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : VerificationDroitAccesService.java
 *
 */
package fr.gouv.finances.lombok.securite.service;

/**
 * Interface VerificationDroitAccesService Interface donnat accès à un service vérifiant qu'une url est accessible.
 * 
 * @author lcontinsouzas-cp
 * @version $Revision: 1.2 $ Date: 15 déc. 2009
 */
public interface VerificationDroitAccesService
{

    /**
     * Méthode appelée pour vérifier que l'url est accessible à l'utilisateur.
     * 
     * @param url url à tester
     * @param object objet portant les droits (roles de l'utilisateur)
     * @return true si l'url est atteignable
     */
    public boolean isURLaccessible(String url, Object object);

}