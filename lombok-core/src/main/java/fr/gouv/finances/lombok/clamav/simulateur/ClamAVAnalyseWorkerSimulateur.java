/*
 * Copyright (c) 2014 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.clamav.simulateur;

import java.io.IOException;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.util.concurrent.ExecutorService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Classe permettant de traiter les requêtes du client.
 */
public class ClamAVAnalyseWorkerSimulateur implements Runnable
{

    /** Initialisation de la journalisation. */
    protected static final Logger LOGGER = LoggerFactory.getLogger(ClamAVAnalyseWorkerSimulateur.class);

    /** Socket (dialogue entre le simulateur et le client). */
    private final SocketChannel channel;

    /** Evénement lié à  la socket. */
    private final SelectionKey selectionKey;

    /** Pool de connexion. */
    private final ExecutorService pool;

    /** Virus d'exemple (Cf : http://www.eicar.org/86-0-Intended-use.html) */
    private static final String VIRUS_EICAR = "X5O!P%@AP[4\\PZX54(P^)7CC)7}$EICAR-STANDARD-ANTIVIRUS-TEST-FILE!$H+";

    /**
     * Différents états que peut prendre le traitement courant.
     */
    private enum State
    {
        /** Lecture. */
        READING,
        /** Traitement de la commande. */
        PROCESS_CMD,
        /** Traitement du flux. */
        PROCESS_FLUX,
        /** Ecriture. */
        WRITING,
        /** Fin. */
        FINISH
    };

    /** Etat courant du traitement. */
    private State state;

    /** Requête envoyée par le client (commande + contenu du fichier à  traiter). */
    private final ClamAVBufferInDataSimulateur readBuffer;

    /** Réponse à  envoyer au client. */
    private String response;

    /**
     * Constructeur.
     * 
     * @param channel channel
     * @param selector selector
     * @param apool apool
     * @throws IOException IOException
     */
    public ClamAVAnalyseWorkerSimulateur(
        final SocketChannel channel, final Selector selector,
        final ExecutorService apool) throws IOException
    {
        super();

        // Récupération de paramètres de la requête
        this.channel = channel;
        this.pool = apool;

        // Configuration du canal en mode non bloquant
        channel.configureBlocking(false);

        // Initialisation du simulateur du buffer de la réponse
        this.readBuffer = new ClamAVBufferInDataSimulateur();

        // Le traitement doit commencer par une lecture de la requête client
        this.state = State.READING;

        // Traitement du canal
        this.selectionKey = channel.register(selector, 0);
        this.selectionKey.attach(this);
        this.selectionKey.interestOps(SelectionKey.OP_READ);
        selector.wakeup();
    }

    /**
     * Lecture des données provenant de la socket.
     * 
     * @param channel channel
     * @param readBuffer readBuffer
     * @return le nombre d'octet lus
     * @throws IOException IOException
     */
    private int readData(
        final SocketChannel channel, final ClamAVBufferInDataSimulateur readBuffer)
        throws IOException
    {
        // Lecture des données par paquet
        final ByteBuffer buffer = ByteBuffer.allocate(8192);
        int numRead = channel.read(buffer);
        if (numRead != -1)
        {
            readBuffer.add(buffer.array(), 0, numRead);
        }
        return numRead;
    }

    /**
     * Lecture des données de la socket.
     *
     * @return nombre d'octet lus
     * @throws IOException IOException
     */
    private int readStep() throws IOException
    {
        // Lecture des données
        final int ret = this.readData(this.channel, this.readBuffer);
        // Commande reçue
        if (this.readBuffer.isCmdLue())
        {
            LOGGER.trace("Commande reçue");

        }
        else if (this.readBuffer.isStreamLu())
        {
            // Flux reçu
            LOGGER.trace("Flux à analyser reçu");
        }
        // Retour du nombre d'octets lus
        return ret;
    }

    /**
     * Analyse des données reçues.
     * 
     * @return vrai si toutes les données à lire ont été lues.
     */
    private boolean postReadStep() throws IOException
    {
        boolean ret;
        ret = this.readBuffer.processData();
        // Commande reçue
        if (this.readBuffer.isCmdLue())
        {
            LOGGER.debug("Commande reçue");

        }
        else if (this.readBuffer.isStreamLu())
        {
            // Flux reçu
            LOGGER.debug("Flux à analyser reçu");
        }
        // Retour du nombre d'octets lus
        return ret;
    }

    /**
     * Traitement du contenu du fichier et sélection de la réponse à  envoyer au client.
     */
    private void processStep()
    {
        this.response =  "1: stream: OK\n";
        // Récuperation du contenu du fichier
        final String data = this.readBuffer.getStreamAsString();
        
        LOGGER.debug("Données reçu par le client : {}", data);

        // Virus de test reçu
        if (data != null && data.contains(VIRUS_EICAR))
        {
            this.response = "1: stream: Eicar-Test-Signature FOUND\n";
        }
        else
        {
            // Pas de virus de test
            this.response = "1: stream: OK\n";
        }
    }

    /**
     * Envoi de la réponse au client.
     * 
     * @return vrai si la socket doit être fermée
     * @throws IOException IOException
     */
    private boolean writeStep() throws IOException
    {

        LOGGER.debug("Envoi de la réponse au client");
        // Envoi de la réponse
        if (this.response != null)
        {
            this.channel.write(ByteBuffer.wrap(this.response.getBytes()));
            LOGGER.trace("Réponse envoyée");
        }
        else
        {
            // Pas de réponse
            LOGGER.trace("Pas de réponse");
        }

        return true;
    }

    /**
     * {@inheritDoc}
     */
    public void run()
    {
        synchronized (this)
        {
            try
            {
                // Analyse de l'état du traitement
                switch (this.state)
                {
                    // Lecture des données de la socket
                    case READING:
                        LOGGER.trace("Simulateur - lecture des données de la socket");

                        // Si des données sont présentes
                        if (this.readStep() != -1)
                        {
                            this.state = State.PROCESS_CMD;
                            this.pool.execute(this);
                        }
                        break;

                    // Traitement du flux envoyé par le client
                    case PROCESS_FLUX:
                        LOGGER.trace("Simulateur - traitement du flux envoyé par le client");

                        this.processStep();
                        this.state = State.WRITING;
                        this.selectionKey.interestOps(SelectionKey.OP_WRITE);
                        this.selectionKey.selector().wakeup();

                        break;

                    // Envoi de la réponse au client
                    case WRITING:
                        LOGGER.trace("Simulateur - envoi de la réponse au client");

                        this.writeStep();
                        this.closeAndRemoveChannel();
                        this.state = State.FINISH;
                        break;

                    // Traitement de la commande provenant du client
                    case PROCESS_CMD:
                        LOGGER.trace("Simulateur - traitement de la commande provenant du client");

                        if (this.postReadStep())
                        {
                            // Si la commande est PING, réponse PONG au client
                            if (this.readBuffer.getCommand().equals(ClamAVBufferInDataSimulateur.Command.PING))
                            {
                                LOGGER.trace("Simulateur : reçu PING, réponse PONG");
                                this.response = "PONG\n";
                                this.state = State.WRITING;
                                this.pool.execute(this);
                                break;
                            }
                            this.state = State.PROCESS_FLUX;
                            this.pool.execute(this);
                        }
                        else
                        {
                            // Lecture de la commande
                            this.state = State.READING;
                            this.selectionKey.interestOps(SelectionKey.OP_READ);
                            LOGGER.trace("process_cmd -> reading");
                            this.selectionKey.selector().wakeup();

                        }
                        break;

                    // Fin
                    case FINISH:
                        LOGGER.trace("Simulateur - fin analyse");

                        if (this.selectionKey.isValid())
                        {
                            LOGGER.warn("Simulateur - Fin alors que la sk est valide");
                        }
                        break;

                    // Cas par défaut (ne devrait pas se produire)
                    default:
                        LOGGER.error("Simulateur - default condition state, bug ??");
                        break;
                }
            }
            catch (final IOException e)
            {
                LOGGER.info("Simulateur - Arrêt ", e);

                // Fermeture de la socket
                this.closeAndRemoveChannel();
            }
        }
    }

    /**
     * Ferme de façon sûre la socket sans lever d'erreur. Peut être appelée même si la scoket a déjà  été fermé par le
     * simulateur ou le client.
     */
    private void closeAndRemoveChannel()
    {
        // Récupération de la socket à partir du canal de réception du flux
        final Socket socket = this.channel.socket();
        LOGGER.debug("Connexion fermée par : {}", socket.getRemoteSocketAddress());
        try
        {
            // Fermeture de la socket
            this.channel.close();
        }
        catch (final IOException ioe)
        {
            LOGGER.info("Simulateur - Erreur lors de la fermeture, la socket était peut-être déjà fermée : {}"
                , ioe);
        }
        this.selectionKey.cancel();
    }
}
