/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : AvantHabilitationService.java
 *
 */
package fr.gouv.finances.lombok.securite.service;

import fr.gouv.finances.lombok.securite.techbean.HabilitationRegleGestionException;
import fr.gouv.finances.lombok.securite.techbean.PersonneAnnuaire;

/**
 * Interface AvantHabilitationService.
 * 
 * @author lcontinsouzas-cp Interface permettant d'ajouter un controle avant l'habilitation d'une personne Cette
 *         interface doit être implémentée dans une classe qui doit ensuite etre instanciée en tant que service dans la
 *         config spring sous le nom de bean avanthabilitationserviceso
 * @version $Revision: 1.3 $ Date: 15 déc. 2009
 */
public interface AvantHabilitationService
{

    /**
     * Méthode exécutée avant habilitation Au stade de l'appel, - les vérifications standard ont été faites (mot de
     * passe ou provenance portail, présence d'une habilitation sur l'application) - les profils bloques dans Sireme ont
     * été retirés La méthode permet soit de modifier les caractéristiques de la personne ou de l'habilitation soit
     * d'ajouter des controles supplémentaires en jetant une exception HabilitationRegleGestionException.
     * 
     * @param unePersonneAnnuaire --
     * @throws HabilitationRegleGestionException the habilitation regle gestion exception
     */

    public void executerAvantHabilitation(PersonneAnnuaire unePersonneAnnuaire)
        throws HabilitationRegleGestionException;

}
