/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : ParameterControlAdvice.java
 *
 */
package fr.gouv.finances.lombok.util.spring;

import java.lang.reflect.Method;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.aop.MethodBeforeAdvice;
import org.springframework.aop.framework.Advised;
import org.springframework.aop.support.AopUtils;

import fr.gouv.finances.lombok.util.base.BaseServiceImpl;
import fr.gouv.finances.lombok.util.exception.ProgrammationException;

/* permet de controler que les paramétres d'entrées d'une méthode ne sont pas à null
 * avant de l'appeler rééllement (exemple AOP)
 */

/**
 * Class ParameterControlAdvice --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.2 $ Date: 11 déc. 2009
 */
public class ParameterControlAdvice implements MethodBeforeAdvice
{

    protected final Log log = LogFactory.getLog(getClass());

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param method
     * @param args
     * @param target
     * @throws Throwable the throwable
     * @see org.springframework.aop.MethodBeforeAdvice#before(java.lang.reflect.Method, java.lang.Object[],
     *      java.lang.Object)
     */
    @Override
    public void before(Method method, Object[] args, Object target) throws Throwable
    {
        List<String> listeMethodesAIgnorer = null;

        if (target instanceof BaseServiceImpl)
        {
            listeMethodesAIgnorer = ((BaseServiceImpl) target).getMethodesAIgnorer();
        }
        else if (BaseServiceImpl.class.isAssignableFrom(AopUtils.getTargetClass(target)))
        {
            listeMethodesAIgnorer = ((BaseServiceImpl) ((Advised)target).getTargetSource().getTarget()).getMethodesAIgnorer();
        }

        if ((listeMethodesAIgnorer == null || !listeMethodesAIgnorer.contains(method.getName())) && args != null)
        {
            for (int i = 0; i < args.length; i++)
            {
                if (args[i] == null)
                { 
                    ProgrammationException pexc =
                        new ProgrammationException(
                            " Erreur de programmation : appel d'un service métier avec au moins un paramètre null. Classe : "
                                + method.getClass().getName() + "; Méthode : " + method.getName() + "; Argument : " + i
                                + " = null");

                    log.warn(pexc.getMessage());
                    throw pexc;
                }
            }
        }

    }

}
