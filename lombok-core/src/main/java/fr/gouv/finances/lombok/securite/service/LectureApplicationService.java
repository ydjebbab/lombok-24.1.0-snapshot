/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : LectureApplicationService.java
 *
 */
package fr.gouv.finances.lombok.securite.service;

import fr.gouv.finances.lombok.securite.techbean.ApplicationAnnuaire;

/**
 * Interface LectureApplicationService --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.2 $ Date: 11 déc. 2009
 */
public interface LectureApplicationService extends AbstractLectureInformationsAnnuaireService
{

    /**
     * Accesseur de l attribut base recherche ldap.
     * 
     * @return base recherche ldap
     */
    public String getBaseRechercheLDAP();

    /**
     * Modificateur de l attribut base recherche ldap.
     * 
     * @param baseRechercheLDAP le nouveau base recherche ldap
     */
    public void setBaseRechercheLDAP(String baseRechercheLDAP);

    /**
     * Rechercher application par libelle court.
     * 
     * @param libelleCourt --
     * @return un objet ApplicationAnnuaire valorise avec les entrees corespondantes de l'annuaire ou jette une
     *         AnnuaireException
     */
    ApplicationAnnuaire rechercherApplicationParLibelleCourt(String libelleCourt);
}
