/*
 * Copyright (c) 2011 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.util.date;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.joda.time.DateTimeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;

import fr.gouv.finances.lombok.util.cle.ApplicationPropertiesUtil;

/**
 * Class ForcageDate
 */
public class ForcageDate implements BeanFactoryPostProcessor
{
    private static final Logger logger = LoggerFactory.getLogger(ForcageDate.class);

    public ForcageDate()
    {
        super();
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see org.springframework.beans.factory.config.BeanFactoryPostProcessor#postProcessBeanFactory(org.springframework.beans.factory.config.ConfigurableListableBeanFactory)
     */
    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException
    {
        if (Boolean.valueOf(ApplicationPropertiesUtil.getProperty("date.estforcee")))
        {
            try
            {
                String chaineForcee = ApplicationPropertiesUtil.getProperty("date.dateforcee");
                SimpleDateFormat sdf = new SimpleDateFormat(("dd/MM/yyyy HH:mm:ss"), Locale.FRANCE);
                Date dateForcee = sdf.parse(chaineForcee);
                long offset = dateForcee.getTime() - System.currentTimeMillis();
                DateTimeUtils.setCurrentMillisOffset(offset);
            }
            catch (ParseException exc)
            {
                // En cas d'erreur, la date système reste inchangée
                logger.error(
                    "Une erreur s'est produite lors du forçage de la date (date.dateforcee={}). La date n'a pas été changée. Message d'erreur: {}",
                    ApplicationPropertiesUtil.getProperty("date.estforcee"), exc.getMessage());
            }
        }
    }

}
