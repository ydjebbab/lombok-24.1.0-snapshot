/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : ClasseHandler.java
 *
 */
package fr.gouv.finances.lombok.parsertexte.parametrage;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 * Class ClasseHandler --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.2 $ Date: 11 déc. 2009
 */
public class ClasseHandler extends DefaultHandler
{

    /** liste classes validees. */
    ListeClasses listeClassesValidees = null;

    /** liste classes. */
    ListeClasses listeClasses = null;

    /** classe en cours. */
    Classe classeEnCours = null;

    /**
     * Instanciation de classe handler.
     */
    public ClasseHandler()
    {
        super();
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param uri
     * @param localName
     * @param qName
     * @throws SAXException the SAX exception
     * @see org.xml.sax.helpers.DefaultHandler#endElement(java.lang.String, java.lang.String, java.lang.String)
     */
    public void endElement(String uri, String localName, String qName) throws SAXException
    {
        if ("classes".equalsIgnoreCase(qName))
        {
            this.listeClassesValidees = this.listeClasses;
        }
        else if ("classe".equalsIgnoreCase(qName))
        {
            if (this.classeEnCours != null)
            {
                this.listeClasses.ajouterClasseMappee(this.classeEnCours);
                this.classeEnCours = null;
            }
            else
            {
                throw new SAXException("Erreur : Fermeture de la balise <classe> sans balise ouvrante.");
            }
        }
        else if ("attribut".equalsIgnoreCase(qName))
        {
            // mon commentaire
        }
    }

    /*
     * (non-Javadoc)
     * @see org.xml.sax.ContentHandler#startElement(java.lang.String, java.lang.String, java.lang.String,
     * org.xml.sax.Attributes)
     */
    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param uri
     * @param localName
     * @param qName
     * @param attributes
     * @throws SAXException the SAX exception
     * @see org.xml.sax.helpers.DefaultHandler#startElement(java.lang.String, java.lang.String, java.lang.String,
     *      org.xml.sax.Attributes)
     */
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException
    {
        if ("classes".equalsIgnoreCase(qName))
        {
            if (this.listeClasses == null)
            {
                this.listeClasses = new ListeClasses();
            }
            else
            {
                throw new SAXException(
                    "Ouverture d une balise <classes> alors qu'une balise <classes> est déjà ouverte. La balise <classes> doit englober les informations du fichier.");
            }
        }
        else if ("classe".equalsIgnoreCase(qName))
        {

            if (this.classeEnCours == null)
            {
                String id = attributes.getValue("id");
                String chemin = attributes.getValue("chemin");

                if (((id == null) || ("".equals(id))) || ((chemin == null) || ("".equals(chemin))))
                {
                    throw new SAXException("Les propriétés id et chemin de la balise <classe> sont obligatoires.");
                }
                this.classeEnCours = new Classe(id, chemin);
            }
            else
            {
                throw new SAXException("Ouverture d une balise <classe> alors qu'une balise <classe> est déjà ouverte.");
            }
        }
        else if ("attribut".equalsIgnoreCase(qName))
        {

            if (this.classeEnCours != null)
            {
                String nom = attributes.getValue("nom");
                String type = attributes.getValue("type");

                if (((nom == null) || ("".equals(nom))) || ((type == null) || ("".equals(type))))
                {
                    throw new SAXException("Les propriétés nom et type de la balise <attribut> sont obligatoires.");
                }

                AttributClasse attribut = new AttributClasse(nom, type);

                String obligatoire = attributes.getValue("obligatoire");
                String suppressionEspace = attributes.getValue("suppressionespace");
                String tailleMin = attributes.getValue("taillemin");
                String tailleMax = attributes.getValue("taillemax");
                String typeControle = attributes.getValue("typecontrole");

                if ((obligatoire != null) && (!"".equals(obligatoire)))
                {
                    boolean blnObligatoire = this.conversionOuiNon(obligatoire, "obligatoire", "attribut");
                    attribut.setObligatoire(blnObligatoire);
                }

                if ((suppressionEspace != null) && (!"".equals(suppressionEspace)))
                {
                    boolean blnSuppressionEspace =
                        this.conversionOuiNon(suppressionEspace, "suppressionespace", "attribut");
                    attribut.setSuppressionEspaceAutour(blnSuppressionEspace);
                }

                if ((tailleMax != null) && (!"".equals(tailleMax)))
                {
                    int intTailleMax = conversionEnInt(tailleMax, "taillemax", "attribut");
                    attribut.setTailleMax(intTailleMax);
                }

                if ((tailleMin != null) && (!"".equals(tailleMin)))
                {
                    int intTailleMin = conversionEnInt(tailleMin, "taillemin", "attribut");
                    attribut.setTailleMin(intTailleMin);
                }

                if ((typeControle != null) && (!"".equals(typeControle)))
                {
                    int intTypeControle = -1;
                    if ("alphabetique".equalsIgnoreCase(typeControle))
                    {
                        intTypeControle = TypeControles.CONTROLE_ALPHABETIQUE;
                    }
                    else if ("numerique".equalsIgnoreCase(typeControle))
                    {
                        intTypeControle = TypeControles.CONTROLE_NUMERIQUE;
                    }
                    else if ("numeriquenegatif".equalsIgnoreCase(typeControle))
                    {
                        intTypeControle = TypeControles.CONTROLE_NUMERIQUE_NEGATIF;
                    }
                    else if ("numeriqueflottant".equalsIgnoreCase(typeControle))
                    {
                        intTypeControle = TypeControles.CONTROLE_NUMERIQUE_FLOTTANT;
                    }
                    else if ("alphanumerique".equalsIgnoreCase(typeControle))
                    {
                        intTypeControle = TypeControles.CONTROLE_ALPHANUMERIQUE;
                    }
                    else if ("numeriqueflottantnegatif".equalsIgnoreCase(typeControle))
                    {
                        intTypeControle = TypeControles.CONTROLE_NUMERIQUE_FLOTTANT_NEGATIF;
                    }
                    else if ("alphanumeriqueincluantespace".equalsIgnoreCase(typeControle))
                    {
                        intTypeControle = TypeControles.CONTROLE_ALPHANUMERIQUE_INCLUANTESPACE;
                    }
                    else if ("alphabetiqueincluantespace".equalsIgnoreCase(typeControle))
                    {
                        intTypeControle = TypeControles.CONTROLE_ALPHABETIQUE_INCLUANTESPACE;
                    }
                    else if ("alphanumeriqueavecarobase".equalsIgnoreCase(typeControle))
                    {
                        intTypeControle = TypeControles.CONTROLE_ALPHANUMERIQUE_AVECAROBASE;
                    }
                    else
                    {
                        throw new SAXException(
                            "La propriété typecontrole de la balise <attribut> doit contenir une valeur parmi 'alphabetique', 'alphanumerique' et 'numerique'.");
                    }
                    attribut.setTypeControle(intTypeControle);
                }

                this.classeEnCours.ajouterAttribut(attribut);
            }
            else
            {
                throw new SAXException(
                    "Ouverture d'une balise <attribut> alors qu'aucune balise <classe> n'est ouverte.");
            }

        }
    }

    /**
     * Conversion en int.
     * 
     * @param pValeur le valeur
     * @param pNomPropriete le nom propriete
     * @param pNomBalise le nom balise
     * @return the int
     * @throws SAXException the SAX exception
     */
    private int conversionEnInt(String pValeur, String pNomPropriete, String pNomBalise) throws SAXException
    {
        int tmp = -1;
        try
        {
            tmp = Integer.parseInt(pValeur);
        }
        catch (NumberFormatException e)
        {
            throw new SAXException("Erreur durant la conversion en int de la propriété '" + pNomPropriete
                + "' de la balise <" + pNomBalise + ">", e);
        }
        return tmp;
    }

    /**
     * methode Conversion oui non : --.
     * 
     * @param pValeur le valeur
     * @param pNomPropriete le nom propriete
     * @param pNomBalise le nom balise
     * @return true, si c'est vrai
     * @throws SAXException the SAX exception
     */
    private boolean conversionOuiNon(String pValeur, String pNomPropriete, String pNomBalise) throws SAXException
    {
        boolean tmp = false;
        if ("oui".equalsIgnoreCase(pValeur))
        {
            tmp = true;
        }
        else if (!"non".equalsIgnoreCase(pValeur))
        {
            throw new SAXException("Erreur : la propriété '" + pNomPropriete + "' de la balise <" + pNomBalise
                + "> doit contenir les valeurs 'oui' ou 'non'");
        }
        return tmp;
    }

}
