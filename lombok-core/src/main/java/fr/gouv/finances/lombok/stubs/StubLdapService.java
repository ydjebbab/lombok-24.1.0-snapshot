/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : StubLdapService.java
 *
 */
package fr.gouv.finances.lombok.stubs;

/**
 * 
 * @author amleplatinec-cp
 * classe de stub pour simuler un utilisateur sans acces au ldap 
 * déclancher quand appli.test="test" dans application.properties
 */
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;

import fr.gouv.finances.lombok.securite.service.AuthentificationEtLectureHabilitationsService;
import fr.gouv.finances.lombok.securite.techbean.HabilitationAnnuaire;
import fr.gouv.finances.lombok.securite.techbean.PersonneAnnuaire;

/**
 * Class StubLdapService --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.2 $ Date: 11 déc. 2009
 */
public class StubLdapService implements AuthentificationEtLectureHabilitationsService
{

    /** code application dans habilitations. */
    private String codeApplicationDansHabilitations;

    /** profil. */
    private String profil;

    /** DN. */
    private String DN;

    /**
     * typeAnnuaire - String,
     */
    private String typeAnnuaire;

    /**
     * mel - String,
     */
    private String mel;

    /**
     * Constructeur de la classe StubLdapService.java
     *
     */
    public StubLdapService()
    {
        super();
        
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param UID
     * @param motDePasse
     * @return string
     * @see fr.gouv.finances.lombok.securite.service.AuthentificationEtLectureHabilitationsService#authentifier(java.lang.String,
     *      java.lang.String)
     */
    @Override
    public String authentifier(String UID, String motDePasse)
    {
        return null;
    };

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param UID
     * @param motDePasse
     * @param application
     * @return personne annuaire
     * @see fr.gouv.finances.lombok.securite.service.AuthentificationEtLectureHabilitationsService#authentifierEtRechercherHabilitation(java.lang.String,
     *      java.lang.String, java.lang.String) 01/10/12 : on met à jour cas annuaire fusionné
     */
    @Override
    public PersonneAnnuaire authentifierEtRechercherHabilitation(String UID, String motDePasse, String application)
    {
        PersonneAnnuaire unePersonneTest = contruirePersonneAnnuaire();
        // construction de ses habilitations

        HabilitationAnnuaire uneHabilitationTest = new HabilitationAnnuaire();
        uneHabilitationTest.setDateCreation(new Date());
        uneHabilitationTest.setDateDebut(new Date());
        DateTime uneDateFin = new DateTime();
        uneDateFin.plus(50000);
        uneHabilitationTest.setDateFin(uneDateFin.toDate());
        // fait reference à appli.codeappli dans application.properties
        uneHabilitationTest.setLibelleCourtAppli(codeApplicationDansHabilitations);
        uneHabilitationTest.setNomAdministrateur("AdministrateurTest");
        // fait reference à appli.profiltest dans application.properties
        uneHabilitationTest.setNomProfil(profil);
        Set uneListeHab = new HashSet();
        uneListeHab.add(uneHabilitationTest);
        unePersonneTest.setListeHabilitations(uneListeHab);
        return unePersonneTest;

    };

    /**
     * Accesseur de l attribut code application dans habilitations.
     * 
     * @return code application dans habilitations
     */
    public String getCodeApplicationDansHabilitations()
    {
        return codeApplicationDansHabilitations;
    };

    /**
     * getter de DN
     * 
     * @return
     */
    public String getDN()
    {
        return DN;
    }

    /**
     * getter mel
     * 
     * @return
     */
    public String getMel()
    {
        return mel;
    }

    /**
     * Accesseur de l attribut profil.
     * 
     * @return profil
     */
    public String getProfil()
    {
        return profil;
    }

    /**
     * getter typeAnnuaire
     * 
     * @return
     */
    public String getTypeAnnuaire()
    {
        return typeAnnuaire;
    }

    /**
     * Accesseur de l attribut url annuaire ldap.
     * 
     * @return url annuaire ldap
     */
    public String getUrlAnnuaireLDAP()
    {
        return null;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param DN
     * @param application
     * @return personne annuaire
     * @see fr.gouv.finances.lombok.securite.service.AuthentificationEtLectureHabilitationsService#rechercherPersonneAnnuaireAvecHabilitationParDN(java.lang.String,
     *      java.lang.String)
     */
    @Override
    public PersonneAnnuaire rechercherPersonneAnnuaireAvecHabilitationParDN(String DN, String application)
    {
        // construction d'une personne annuaire et de ses habilitations
        PersonneAnnuaire unePersonneTest = new PersonneAnnuaire();
        unePersonneTest = this.contruirePersonneAnnuaire();
        HabilitationAnnuaire uneHabilitationTest = new HabilitationAnnuaire();
        uneHabilitationTest.setDateCreation(new Date());
        uneHabilitationTest.setDateDebut(new Date());
        DateTime uneDateFin = new DateTime();
        uneDateFin.plus(50000);
        uneHabilitationTest.setDateFin(uneDateFin.toDate());
        // fait reference à appli.codeappli dans application.properties
        uneHabilitationTest.setLibelleCourtAppli(codeApplicationDansHabilitations);
        uneHabilitationTest.setNomAdministrateur("AdministrateurTest");
        // fait reference à appli.profiltest dans application.properties
        uneHabilitationTest.setNomProfil(profil);
        Set uneListeHab = new HashSet();
        uneListeHab.add(uneHabilitationTest);
        unePersonneTest.setListeHabilitations(uneListeHab);
        return unePersonneTest;
    }

    /**
     * methode Rechercher personne annuaire avec habilitation par uid : --.
     * 
     * @param UID --
     * @param application --
     * @return personne annuaire
     */
    public PersonneAnnuaire rechercherPersonneAnnuaireAvecHabilitationParUID(String UID, String application)
    {
        return null;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.securite.service.AuthentificationEtLectureHabilitationsService#rechercherPersonneAnnuaireParticuliersAvecHabilitationParDN(java.lang.String)
     */
    @Override
    public PersonneAnnuaire rechercherPersonneAnnuaireParticuliersAvecHabilitationParDN(String dn)
    {
        PersonneAnnuaire unePersonneTest = new PersonneAnnuaire();
        unePersonneTest = this.contruirePersonneAnnuaire();
        HabilitationAnnuaire uneHabilitationTest = new HabilitationAnnuaire();
        uneHabilitationTest.setDateCreation(new Date());
        uneHabilitationTest.setDateDebut(new Date());
        DateTime uneDateFin = new DateTime();
        uneDateFin.plus(50000);
        uneHabilitationTest.setDateFin(uneDateFin.toDate());
        // pas de lombok.aptera.codeappli cas usagers
        uneHabilitationTest.setLibelleCourtAppli("codeapplication");
        uneHabilitationTest.setNomAdministrateur("AdministrateurTest");
        // fait reference à appli.profiltest dans application.properties
        uneHabilitationTest.setNomProfil(profil);
        Set uneListeHab = new HashSet();
        uneListeHab.add(uneHabilitationTest);
        unePersonneTest.setListeHabilitations(uneListeHab);
        return unePersonneTest;
    }

    /**
     * methode Rechercher personne annuaire par uid : --.
     * 
     * @param UID --
     * @return personne annuaire
     */
    public PersonneAnnuaire rechercherPersonneAnnuaireParUID(String UID)
    {
        return null;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.securite.service.AuthentificationEtLectureHabilitationsService#rechercherPersonneAnnuaireSansHabilitationParDN(java.lang.String)
     */
    @Override
    public PersonneAnnuaire rechercherPersonneAnnuaireSansHabilitationParDN(String dn)
    {

        return contruirePersonneAnnuaire();
    }

    /**
     * Modificateur de l attribut code application dans habilitations.
     * 
     * @param codeApplicationDansHabilitations le nouveau code application dans habilitations
     */
    public void setCodeApplicationDansHabilitations(String codeApplicationDansHabilitations)
    {
        this.codeApplicationDansHabilitations = codeApplicationDansHabilitations;
    }

    /**
     * setter de DN
     * 
     * @param dN
     */
    public void setDN(String dN)
    {
        DN = dN;
    }

    /**
     * setter mel
     * 
     * @param mel
     */
    public void setMel(String mel)
    {
        this.mel = mel;
    }

    /**
     * Modificateur de l attribut profil.
     * 
     * @param profil le nouveau profil
     */
    public void setProfil(String profil)
    {
        this.profil = profil;
    }

    /**
     * setter typeAnnuaire
     * 
     * @param typeAnnuaire
     */
    public void setTypeAnnuaire(String typeAnnuaire)
    {
        this.typeAnnuaire = typeAnnuaire;
    }

    /**
     * Modificateur de l attribut url annuaire ldap.
     * 
     * @param urlAnnuaireLDAP le nouveau url annuaire ldap
     */
    public void setUrlAnnuaireLDAP(String urlAnnuaireLDAP)
    {
        // RAS
    }

    /**
     * construction d'un objet personneAnnuaire fictif
     * 
     * @return PersonneAnnuaire
     */
    private PersonneAnnuaire contruirePersonneAnnuaire()
    {
        PersonneAnnuaire unePersonneTest = new PersonneAnnuaire();
        if (this.getTypeAnnuaire().compareToIgnoreCase("DGCP") == 0 ||
            this.getTypeAnnuaire().compareToIgnoreCase("DGI") == 0)
        {
            if (this.getTypeAnnuaire().compareToIgnoreCase("DGCP") == 0)
            {
                unePersonneTest.setUid(StringUtils.removeEnd(this.getDN(), ",ou=personnes,ou=dgcp,ou=mefi,o=gouv,c=fr"));
            }
            else
            {
                unePersonneTest.setUid(StringUtils.removeEnd(this.getDN(), ",ou=personnes,ou=dgi,ou=mefi,o=gouv,c=fr"));
            }
            unePersonneTest.setCn("usertest-cp");
            unePersonneTest.setFonction("administrateur de test");
            unePersonneTest.setAffectation("affectationtest");
            unePersonneTest.setSn("SnTest");
            unePersonneTest.setGivenName("GivenNameTest");
            unePersonneTest.setMail(mel);
        }

        else if (this.getTypeAnnuaire().compareToIgnoreCase("DGFIP") == 0)
        {
            unePersonneTest.setUid(StringUtils.removeEnd(this.getDN(), ",ou=personnes,ou=dgfip,ou=mefi,o=gouv,c=fr"));
            unePersonneTest.setCn("cntest");
            unePersonneTest.setAffectationCODIQUE("affectationtest");
            unePersonneTest.setAffectationSAGES("affectationtest");
            unePersonneTest.setSn("SnTest");
            unePersonneTest.setGivenName("GivenNameTest");
            unePersonneTest.setMail(mel);
        }

        else if (this.getTypeAnnuaire().compareToIgnoreCase("USAGERS") == 0)
        {
            unePersonneTest.setUid(StringUtils.removeEnd(this.getDN(), ",ou=abonnes,ou=particuliers,ou=mefi,o=gouv,c=fr"));
            unePersonneTest.setSn("SnTest");
            unePersonneTest.setGivenName("GivenNameTest");
            unePersonneTest.setCn("cntest");
            unePersonneTest.setMail(mel);
        }

        else
        {
            unePersonneTest.setUid("UidTest");
            unePersonneTest.setSn("SnTest");
            unePersonneTest.setGivenName("GivenNameTest");
            unePersonneTest.setCn("cntest");
            unePersonneTest.setMail(mel);
        }

        return unePersonneTest;
    }

}
