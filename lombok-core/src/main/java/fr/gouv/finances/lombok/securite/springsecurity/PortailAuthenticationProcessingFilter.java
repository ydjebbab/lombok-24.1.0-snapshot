/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : PortailAuthenticationProcessingFilter.java
 *
 */
package fr.gouv.finances.lombok.securite.springsecurity;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import fr.gouv.finances.lombok.securite.techbean.AuthentificationRegleGestionException;
import fr.gouv.finances.lombok.util.exception.ExploitationException;

/**
 * <pre>
 * On surcharge AuthenticationProcessingFilter afin de récupérer la requete.
 * On vérifie que le portail est bien l'appelant. On décode la requete du portail afin de ne retourner que le DN à
 * "retrieveUser" dans PortailAuthentificationSecurityLombok.
 * </pre>
 * 
 * @author chouard-cp
 * @version $Revision: 1.7 $ Date: 15 déc. 2009
 */
public class PortailAuthenticationProcessingFilter extends UsernamePasswordAuthenticationFilter
{
    private static final Logger log = LoggerFactory.getLogger(PortailAuthenticationProcessingFilter.class);
    
    // attribut ci dessous ne sert que pour simuler une connexion dpaep et tester que le code est ok
    // private String suffixedndpaep="o=gouv,c=fr";
    /** SEPARATEUR r_ d n_ profi l_ usernam e_ dpaep. */
    public static final String SEPARATEUR_DN_PROFIL_USERNAME_DPAEP = "##!DPAEP!##";

    /** SEPARATEU SEPARATEUR_DN_PROFIL_USERNAME_USAGERS. */
    public static final String SEPARATEUR_DN_PROFIL_USERNAME_USAGERS = "##!USAGERS!##";

    /** SEPARATEU SEPARATEUR_DN_PROFIL_USERNAME_DGFIP. */
    public static final String SEPARATEUR_DN_PROFIL_USERNAME_DGFIP = "##!DGFIP!##";

    /** SEPARATEU SEPARATEUR_DN_PROFIL_USERNAME_AUCUN. */
    public static final String SEPARATEUR_DN_PROFIL_USERNAME_AUCUN = "##!AUCUN!##";

    /** SEPARATEU SEPARATEUR_DN_PROFIL_USERNAME_REAL. */
    public static final String SEPARATEUR_DN_PROFIL_USERNAME_REAL = "##!REAL!##";

    /** SEPARATEU r_ niveau x_ dan s_ l e_ dn. */
    public static final String SEPARATEUR_NIVEAUX_DANS_LE_DN = ",";

    /** Constant : SEPARATEUR_ADRESSEIP. */
    public static final String SEPARATEUR_ADRESSEIP = ",";

    /** DEBU t_ parametr e_ authorizatio n_ basic. */
    private static final String DEBUT_PARAMETRE_AUTHORIZATION_BASIC = "Basic ";

    /** NO m_ parametr e_ authorization. */
    private static final String NOM_PARAMETRE_AUTHORIZATION = "Authorization";

    /** nom parametre web remote user. */
    private static final String NOM_PARAMETRE_WEBPASS_REMOTE_USER = "webpass-remote-user";

    /** SEPARATEUR_DN_PROFIL - String,. */
    public static final String SEPARATEUR_DN_PROFIL = ":";

    /** J_APPEL_PORTAIL - String,. */
    public static final String J_APPEL_PORTAIL = "j_appelportail";

    /** PORTAIL_INTERNET - String,. */
    public static final String PORTAIL_INTERNET = "http://portail.dgfip.finances.gouv.fr";

    /** result. */
    boolean result = false;

    /** mot de passe portail. */
    @Deprecated
    private String motDePassePortail;

    /** mode authentification. */
    private String modeAuthentification;

    /** separateurdnmotdepasse. */
    private String separateurdnmotdepasse = ";";

    /** suffixedndpaep. */
    private String suffixedndpaep = "ou=centrale,ou=mefi,o=gouv,c=fr";

    /** suffixeusagers. */
    private String suffixeusagers = "ou=particuliers,ou=mefi,o=gouv,c=fr";

    /** adresses ip. */
    private String adressesIP;

    /** typeannuaire. */
    private String typeAnnuaire;

    public PortailAuthenticationProcessingFilter()
    {
        super();
    }

    /**
     * Gets the typeannuaire.
     *
     * @return the typeannuaire
     */
    public String getTypeAnnuaire()
    {
        return typeAnnuaire;
    }

    /**
     * Sets the typeannuaire.
     *
     * @param typeAnnuaire the new typeannuaire
     */
    public void setTypeAnnuaire(String typeAnnuaire)
    {
        this.typeAnnuaire = typeAnnuaire;
    }

    /**
     * Gets the adresses ip.
     *
     * @return the adresses ip
     */
    public String getAdressesIP()
    {
        return adressesIP;
    }

    /**
     * Gets the mode authentification.
     *
     * @return the mode authentification
     */
    public String getModeAuthentification()
    {
        return modeAuthentification;
    }

    /**
     * Gets the mot de passe portail.
     *
     * @return the mot de passe portail
     */
    public String getMotDePassePortail()
    {
        return motDePassePortail;
    }

    /**
     * Gets the separateurdnmotdepasse.
     *
     * @return the separateurdnmotdepasse
     */
    public String getSeparateurdnmotdepasse()
    {
        return separateurdnmotdepasse;
    }

    /**
     * Gets the suffixedndpaep.
     *
     * @return the suffixedndpaep
     */
    public String getSuffixedndpaep()
    {
        return suffixedndpaep;
    }

    /**
     * Gets the suffixeusagers.
     *
     * @return the suffixeusagers
     */
    public String getSuffixeusagers()
    {
        return suffixeusagers;
    }

    /**
     * Sets the adresses ip.
     *
     * @param adressesIP the new adresses ip
     */
    public void setAdressesIP(String adressesIP)
    {
        this.adressesIP = adressesIP;
    }

    /**
     * Sets the mode authentification.
     *
     * @param modeAuthentification the new mode authentification
     */
    public void setModeAuthentification(String modeAuthentification)
    {
        this.modeAuthentification = modeAuthentification;
    }

    /**
     * Sets the mot de passe portail.
     *
     * @param motDePassePortail the new mot de passe portail
     */
    public void setMotDePassePortail(String motDePassePortail)
    {
        this.motDePassePortail = motDePassePortail;
    }

    /**
     * Sets the separateurdnmotdepasse.
     *
     * @param separateurdnmotdepasse the new separateurdnmotdepasse
     */
    public void setSeparateurdnmotdepasse(String separateurdnmotdepasse)
    {
        this.separateurdnmotdepasse = separateurdnmotdepasse;
    }

    /**
     * Sets the suffixedndpaep.
     *
     * @param suffixedndpaep the new suffixedndpaep
     */
    public void setSuffixedndpaep(String suffixedndpaep)
    {
        this.suffixedndpaep = suffixedndpaep;
    }

    /**
     * Sets the suffixeusagers.
     *
     * @param suffixeusagers the new suffixeusagers
     */
    public void setSuffixeusagers(String suffixeusagers)
    {
        this.suffixeusagers = suffixeusagers;
    }

    /**
     * cas du portail internet : on récupére https://portail.dgfip.finances.gouv.fr/gap par exemple , l'url doit rester
     * en https...
     * 
     * @param urlRecuperee "documenté"
     * @return string
     */
    public String traiterPortailInternet(String urlRecuperee)
    {
        String laUrlPortail = "";
        if (StringUtils.contains(urlRecuperee, PORTAIL_INTERNET))
        {
            laUrlPortail = StringUtils.replace(urlRecuperee, "http", "https");
        }
        else
        {
            laUrlPortail = urlRecuperee;

        }
        return laUrlPortail;
    }

    /**
     * verifie si dn est d' origine dpaep.
     * 
     * @param dn --
     * @return true, if est dn d origine dpaep
     */
    protected boolean estDnDOrigineDpaep(String dn)
    {
        boolean estDnDOrigineDpaep = false;
        if (dn.trim().endsWith(this.getSuffixedndpaep()))
        {
            estDnDOrigineDpaep = true;
            if (log.isDebugEnabled())
            {
                log.debug("DN identifie comme étant d'origine DPAEP : " + dn);
            }
        }
        return estDnDOrigineDpaep;
    }

    /**
     * methode Lire et decoder parametre webpass-remote-user dans header http : .
     *
     * @param httpr
     * @return string
     */
    protected String lireEtDecoderParametreWebRemoteUserDansHeader(HttpServletRequest httpr)
    {
        log.debug(">>> Debut methode lireEtDecoderParametreWebRemoteUserDansHeader()");
        String headerWRU = httpr.getHeader(NOM_PARAMETRE_WEBPASS_REMOTE_USER);

        if ((headerWRU != null) && (headerWRU.length() != 0))
        {

            if (log.isDebugEnabled())
            {
                log.debug("Entete portail parametre " + NOM_PARAMETRE_WEBPASS_REMOTE_USER + " trouve : " + headerWRU);
            }

        }
        else
        {
            log.error("Le paramètre " + NOM_PARAMETRE_WEBPASS_REMOTE_USER
                + " n'est pas transmis dans le header par le portail d'authentification");
            throw new ExploitationException(
                "Le paramètre " + NOM_PARAMETRE_WEBPASS_REMOTE_USER
                    + " n'a pas été transmis dans le header par le portail d'authentification");

        }

        return headerWRU;
    }

    /**
     * recupere le champsauthorization et le decode en base 64.
     * 
     * @param httpr --
     * @return the string
     */
    protected String lireEtDecoderParametreAuthorizationDansHeader(HttpServletRequest httpr)
    {
        log.debug(">>> Debut methode lireEtDecoderParametreAuthorizationDansHeader()");
        String authorization = httpr.getHeader(NOM_PARAMETRE_AUTHORIZATION);
        String decodageDebutAuthorization = null;
        String debutAuthorization = null;

        if ((authorization != null) && (authorization.length() != 0))
        {

            if (log.isDebugEnabled())
            {
                log.debug("Entete portail parametre authorization trouve (avant decodage) : " + authorization);
            }

            // suppression du mot "basic " en debut d'entete avant decodage
            debutAuthorization = authorization.substring(DEBUT_PARAMETRE_AUTHORIZATION_BASIC.length());
            decodageDebutAuthorization = new String(Base64.decodeBase64(debutAuthorization.getBytes()));

            if (log.isDebugEnabled())
            {
                log.debug("Entete portail parametre authorization trouve (apres decodage): "
                    + decodageDebutAuthorization);
            }
        }
        else
        {
            log.error("Le paramètre Authorization n'est pas transmis dans le header par le portail d'authentification");
            throw new ExploitationException(
                "Le paramètre Authorization n'a pas été transmis dans le header par le portail d'authentification");
        }

        return decodageDebutAuthorization;
    }

    /*
     * Enables subclasses to override the composition of the username, such as by including additional values and a
     * separator. (non-Javadoc)
     * @seeorg.springframework.security.ui.webapp.AuthenticationProcessingFilter#obtainUsername(javax.servlet.http.
     * HttpServletRequest)
     */
    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param httpr "documenté"
     * @return string
     * @see org.springframework.security.ui.webapp.AuthenticationProcessingFilter#obtainUsername(javax.servlet.http.HttpServletRequest)
     */
    @Override
    protected String obtainUsername(HttpServletRequest httpr)
    {
        String retourUserNameSecurityLombok = null;

        /*
         * Dans le cas clé REAL, qui correspond à un type annuaire REAL dans le fichier application.properties,
         * l'information à desencapsuler se situe dans le header http web-remote-user
         */
        if ("REAL".contentEquals(typeAnnuaire))
        {
            retourUserNameSecurityLombok = obtainUsernameFromWebRemoteUser(httpr);
        }
        else
        {
            retourUserNameSecurityLombok = obtainUsernameFromAuthorization(httpr);
        }
        return retourUserNameSecurityLombok;
    }

    /**
     * methode Obtain username from web remote user : .
     *
     * @param httpr
     * @return string
     */
    private String obtainUsernameFromWebRemoteUser(HttpServletRequest httpr)
    {
        String wru = lireEtDecoderParametreWebRemoteUserDansHeader(httpr);

        return wru.concat(SEPARATEUR_DN_PROFIL_USERNAME_REAL);
    }

    /**
     * methode Obtain username from authorization : .
     *
     * @param httpr
     * @return string
     */
    private String obtainUsernameFromAuthorization(HttpServletRequest httpr)
    {
        log.debug(">>> Debut methode obtainUsernameFromAuthorization()");
        /**
         * on recupere la requete transmise par le portail et on récupere le dn uniquement requete =
         * DN+separateur+profil si modeauthentification dans application.properties<>portail 04/12 : on doit désormais
         * récupérer l'url portail pour la stocker (plus de stockage en dur)
         */

        String authorizationDecode = null;
        String[] tableauDnProfil = null;
        String dn = null;
        String profil = null;
        String profilTransmis = null;
        String retourUserNameSecurityLombok = null;

        authorizationDecode = lireEtDecoderParametreAuthorizationDansHeader(httpr);

        // les url ne sont plus stockées , notammment pour les éditions dans application.properties
        // elles seront mises dans utilisateursecuritylombok
        String laUrlPortail = lireUrlPortail(httpr);

        if (log.isDebugEnabled())
        {
            log.debug("Separateur utilise entre dn et profil : " + separateurdnmotdepasse);
        }
        tableauDnProfil = StringUtils.split(authorizationDecode, separateurdnmotdepasse);

        // Lecture du dn qui se trouve dans tous les cas en première position avant le séparateur

        if ((tableauDnProfil != null) && (tableauDnProfil.length > 0) && (tableauDnProfil[0] != null))
        {
            dn = tableauDnProfil[0];

            if ((tableauDnProfil.length > 1) && (tableauDnProfil[1] != null))
            {
                profil = tableauDnProfil[1];
            }
            else
            {
                log.info("aucun profil de transmis");
            }
        }
        else
        {
            log.error("le format d'entete transmis par le portail est incorrect :" + authorizationDecode);
            log.error("attendu : dn (au moins) + " + separateurdnmotdepasse + " + profil");
            throw new AuthentificationRegleGestionException(
                "Accés intrusif à l'application , le format d'entete transmis par le portail est incorrect");
        }

        // controle de l'adresse IP
        // si adresse ip servie dans aplli.properties , l'ip de l'utilisateur doit en faire partie
        List<String> listeAdresseIPAutorisee = this.recupererListeADresseIPAyantAccesAApplication();
        String adresseIPUtilisateur = httpr.getRemoteAddr();

        this.verifierAdresseIPUtilisateurEstIdentiqueAAdresseIPAutorisee(adresseIPUtilisateur, listeAdresseIPAutorisee);

        // Cas général : traitement de l'entète dn + separateur +profil portail (avant cas ex dgcp c etait motdepasse)
        // verification du mot de passe
        // retour du dn

        // Cas dpaep : traitement de l'entète dn + : + profil
        // retour du dn + separateur + profil
        // Le profil est nécessaire pour créer la personne dans la phae habilitation (pas de relecture
        // annuaire pour chercher le profil dans le cas dpaep)

        if (estDnDOrigineDpaep(dn))

        {

            if (log.isDebugEnabled())
            {
                log.debug("Le dn correspond à un utilisateur dpaep");
            }

            profil = null;
            try
            {
                profil = tableauDnProfil[1];
            }
            catch (ArrayIndexOutOfBoundsException exc)
            {
                log.error("Pas de profil pour utilisateur dpaep.", exc);
            }
            retourUserNameSecurityLombok =
                dn + SEPARATEUR_DN_PROFIL_USERNAME_DPAEP + profil + SEPARATEUR_DN_PROFIL_USERNAME_DPAEP + laUrlPortail;
        }
        // cas annuaire particuliers
        // on est en mode https , non pris en compte par tomcat dans lombok
        // on met donc l url en https
        else if (estDnDOrigineUsagers(dn))
        {
            if (log.isDebugEnabled())
            {
                log.debug("Le dn correspond à un utilisateur  usagers");
            }
            laUrlPortail = StringUtils.replace(laUrlPortail, "http", "https");
            {
                if (log.isDebugEnabled())
                {
                    log.debug("urlportail pour usagers" + laUrlPortail);
                }
            }

            retourUserNameSecurityLombok = dn + SEPARATEUR_DN_PROFIL_USERNAME_USAGERS + profil;

        }
        // cas sans annuaire (annuaire.typeannuaire=AUCUN)
        // on vérifie qu'il y a bien un dn et un profil
        else if (estDnEtProfilSansAppelAnnuaire(dn))
        {

            profil = null;
            try
            {
                profil = tableauDnProfil[1];
            }
            catch (ArrayIndexOutOfBoundsException exc)
            {
                log.error("Dans le champs authorization du header (" + authorizationDecode
                    + "), il n'y a pas de profil pour l'utilisateur :" + dn, exc);
            }

            if (log.isDebugEnabled())
            {
                StringBuffer buff = new StringBuffer(300)
                    .append("Le type annuaire étant AUCUN (voir configuration dans le fichier application.properties ")
                    .append(",le dn correspond alors à un utilisateur qui ne nécessite pas d'appel à un ldap");
                log.debug("buff : " + buff);
            }

            retourUserNameSecurityLombok = dn + SEPARATEUR_DN_PROFIL_USERNAME_AUCUN + profil;

        }

        // cas ex dgcp , ex dgi ou fusionné
        else

        {
            if (log.isDebugEnabled())
            {
                log.debug("Le dn correspond à un utilisateur dgfip");
            }

            try
            {
                // attention : cas dgcp+dgfip sera le profil de l'utilisateur sur l'application
                profilTransmis = tableauDnProfil[1];
            }
            catch (ArrayIndexOutOfBoundsException aioobe)
            {
                log.error("separateur dn - profil errone - pas de profil trouve pour un utilisateur agent", aioobe);
            }
            // dans le cas du portail internet
            if (StringUtils.isNotEmpty(laUrlPortail))
            {
                laUrlPortail = traiterPortailInternet(laUrlPortail);
            }

            retourUserNameSecurityLombok =
                dn + SEPARATEUR_DN_PROFIL_USERNAME_DGFIP + profilTransmis + SEPARATEUR_DN_PROFIL_USERNAME_DGFIP + laUrlPortail;
        }

        return retourUserNameSecurityLombok;
    }

    /**
     * raméne la liste des adresses ip présentes dans le champs adressesIP de application.properties
     * 
     * @return the list< string>
     */
    protected List<String> recupererListeADresseIPAyantAccesAApplication()
    {
        log.debug(">>> Debut methode recupererListeADresseIPAyantAccesAApplication()");
        List<String> listeAdresseIPAutorisees = new ArrayList<>();
        String[] champs = adressesIP.split(SEPARATEUR_ADRESSEIP);
        if (champs != null && champs.length > 0 && champs[0] != null && ("").equalsIgnoreCase(champs[0]) == false)
        {
            for (int i = 0; i < champs.length; i++)
            {
                listeAdresseIPAutorisees.add(champs[i]);
                log.info("AdresseIPAutorisees : " + champs[i]);
            }
        }
        else
        {
            log.info("AdresseIPAutorisees non rempli ");

        }
        return listeAdresseIPAutorisees;
    }

    /**
     * dans application.properties, on peut mettre pour une appli , les adresses ip qui auront accés à l'application (ex
     * ip du portail ) controle que l'adresse ip transmis par l'utilisateur est ok
     * 
     * @param adresseIPTransmise --
     * @param listeAdresseIPAttendues --
     */
    protected void verifierAdresseIPUtilisateurEstIdentiqueAAdresseIPAutorisee(String adresseIPTransmise,
        List<String> listeAdresseIPAttendues)
    {
        log.debug(">>> Debut methode verifierAdresseIPUtilisateurEstIdentiqueAAdresseIPAutorisee()");
        if (log.isDebugEnabled())
        {
            log.debug("Adrese IP utilisateur  transmis : " + adresseIPTransmise);
        }
        if (log.isDebugEnabled())
        {
            log.debug("listeAdresseIPAttendues Adrese IP attendu : " + listeAdresseIPAttendues.toString());
        }
        boolean adressesipidentiques = false;

        if (listeAdresseIPAttendues != null && listeAdresseIPAttendues.size() > 0)
        {
            for (int i = 0; i < listeAdresseIPAttendues.size(); i++)
            {
                if (listeAdresseIPAttendues.get(i).equals(adresseIPTransmise))
                {
                    adressesipidentiques = true;
                    break;
                }
            }
            if (adressesipidentiques == false)
            {
                throw new AuthentificationRegleGestionException(
                    "Accés intrusif à l'application , l adresse ip de l utilisateur est incorrecte ");
            }
            else
            {
                if (log.isDebugEnabled())
                {
                    log.debug("Controle OK sur les adresses ip");
                }
            }
        }
        else
        {
            if (log.isDebugEnabled())
            {
                log.debug("listeAdresseIPAttendues n 'est pas rempli dans application.properties   . Controle OK ");
            }
        }

    }

    /**
     * verifie l'origine de la personne si usagers.
     * 
     * @param dn "documenté"
     * @return true, si c'est vrai
     */
    private boolean estDnDOrigineUsagers(String dn)
    {
        boolean estDnDOrigineUsagers = false;
        if (dn.trim().endsWith(this.getSuffixeusagers()))
        {
            estDnDOrigineUsagers = true;
            if (log.isDebugEnabled())
            {
                log.debug("DN identifie comme étant d'origine usagers : " + dn);
            }
        }
        return estDnDOrigineUsagers;
    }

    /**
     * verifie que la personne connectée ne nécessite pas d'appel à un ldap.
     * 
     * @param dn "documenté"
     * @return true, si c'est vrai
     */
    private boolean estDnEtProfilSansAppelAnnuaire(String dn)
    {
        boolean estIl = false;

        if ("AUCUN".equalsIgnoreCase(typeAnnuaire))
        {
            estIl = true;
            if (log.isDebugEnabled())
            {
                log.debug("Le type Annuaire vaut AUCUN, le DN passé dans l'entête ne nécessitera pas d'appel à un ldap : " + dn);
            }
        }

        return estIl;
    }

    /**
     * récupérer l'url transmise par le portail sans j_appelportail.
     * 
     * @param httpr "documenté"
     * @return string
     */
    private String lireUrlPortail(HttpServletRequest httpr)
    {
        log.debug(">>> Debut methode lireUrlPortail()");
        String urlFinal = "";
        String laUrlPortail = httpr.getRequestURL().toString();
        String urlPortailAUtiliser =
            StringUtils.remove(laUrlPortail, J_APPEL_PORTAIL);
        // dans le cas du prortail internet PIGP ou portail sso, url trnasmise contenant appli est transformée en host
        // on la retraite donc
        if (StringUtils.contains(urlPortailAUtiliser, ".host."))
        {
            urlFinal =
                StringUtils.replace(urlPortailAUtiliser, "host", "appli", 1);
        }
        else
        {
            urlFinal = urlPortailAUtiliser;
        }
        if (log.isDebugEnabled())
        {
            log.debug("urlPortail qui sera utilisée dans l'application : " + urlFinal);
        }
        return urlFinal;
    }

}
