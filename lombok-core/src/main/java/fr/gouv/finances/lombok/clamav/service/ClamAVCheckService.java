/*
 * Copyright (c) 2014 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.clamav.service;

import java.io.File;

import fr.gouv.finances.lombok.clamav.bean.ClamAvResponse;
import fr.gouv.finances.lombok.upload.bean.FichierJoint;

/**
 * Interface du service permet de dialoguer avec clamAV.
 *
 * @author Christophe Breheret-Girardin
 */
public interface ClamAVCheckService
{

    /**
     * Cette méthode permet de savoir si le serveur ClamAV est accessible ou non.
     * 
     * @return la réponse à la demande
     */
    ClamAvResponse checkClamAVPing();

    /**
     * Cette méthode retourne la version de ClamAV.
     * 
     * @return la réponse à la demande
     */
    ClamAvResponse checkClamAVVersion();

    /**
     * Cette méthode retourne les statistiques courantes du serveur ClamAV.
     * 
     * @return la réponse à la demande
     */
    ClamAvResponse checkClamAVStats();

    /**
     * Cette méthode permet d'analyser un fichier par envoi dans un flux TCP du contenu de ce fichier.
     * 
     * @param fileForTest - le fichier à analyser
     * @return la réponse à la demande
     */
    ClamAvResponse checkByStream(File fileForTest);
    
    /**
     * Cette méthode permet d'analyser un flux par envoi dans un flux TCP.
     * 
     * @param contenu - le flux à analyser
     * @return la réponse à la demande
     */
    ClamAvResponse checkByStream(final byte[] contenu);

    /**
     * Cette méthode permet d'analyser un fichier joint par envoi dans un flux TCP du contenu de ce fichier.
     * 
     * @param fileForTest - le fichier à analyser
     * @return la réponse à la demande
     */
    ClamAvResponse checkByStream(FichierJoint fileForTest);

    /**
     * Accesseur de activationClamAv
     *
     * @return activationClamAv
     */
    public ActivationClamAV getActivationClamAv();

    /**
     * Mutateur de activationClamAv
     *
     * @param activationClamAv activationClamAv
     */
    public void setActivationClamAv(ActivationClamAV activationClamAv);

}