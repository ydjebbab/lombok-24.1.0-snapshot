/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : EnumPropertyEditor.java
 *
 */
package fr.gouv.finances.lombok.util.propertyeditor;

import java.beans.PropertyEditorSupport;

/**
 * Class EnumPropertyEditor.
 * 
 * @param <T> property editor générique
 * @author amleplatinec
 * @version $Revision: 1.2 $ Date: 15 déc. 2009
 */
public class EnumPropertyEditor<T> extends PropertyEditorSupport
{

    /** enum type. */
    private Class<T> enumType;

    /** valeur arbitraire. */
    private final String valeurArbitraire;

    /** return null. */
    private final boolean returnNull;

    /**
     * Instanciation de enum property editor.
     */
    public EnumPropertyEditor()
    {
        this(null, null, false);
    }

    /**
     * Constructeur de la classe EnumPropertyEditor.java
     * 
     * @param enumType --
     */
    public EnumPropertyEditor(Class<T> enumType)
    {
        this(enumType, null, false);
    }

    /**
     * Constructeur de la classe EnumPropertyEditor.java
     * 
     * @param enumType --
     * @param returnNull :retourner une valeur nulle quand aucune la chaîne de caractère qu'il transforme en énuméré ne
     *        correspond à une des énumérations de la classe mappée.
     */
    public EnumPropertyEditor(Class<T> enumType, boolean returnNull)
    {
        this(enumType, null, returnNull);

    }

    /**
     * Constructeur de la classe EnumPropertyEditor.java
     * 
     * @param enumType --
     * @param valeurArbitraire : renvoit valeurArbitraire quand valeur nulle
     */
    public EnumPropertyEditor(Class<T> enumType, String valeurArbitraire)
    {
        this(enumType, valeurArbitraire, false);
    }

    /**
     * Constructeur de la classe EnumPropertyEditor.java
     * 
     * @param enumType --
     * @param valeurArbitraire : renvoit valeurArbitraire quand valeur nulle
     * @param returnNull ::retourner une valeur nulle quand aucune la chaîne de caractère qu'il transforme en énuméré
     */
    public EnumPropertyEditor(Class<T> enumType, String valeurArbitraire, boolean returnNull)
    {
        super();
        this.enumType = enumType;
        this.returnNull = returnNull;
        this.valeurArbitraire = valeurArbitraire;

    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @return as text
     * @see java.beans.PropertyEditorSupport#getAsText()
     */
    public String getAsText()
    {
        String chainePourEnumNull = (this.valeurArbitraire != null ? this.valeurArbitraire : "");

        return getValue() != null ? getValue().toString() : chainePourEnumNull;
    }

    /**
     * Accesseur de l attribut enum type.
     * 
     * @return enum type
     */
    public Class<T> getEnumType()
    {
        return enumType;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param text le nouveau as text
     * @throws IllegalArgumentException the illegal argument exception
     * @see java.beans.PropertyEditorSupport#setAsText(java.lang.String)
     */
    public void setAsText(String text) throws IllegalArgumentException
    {

        T enumRecherche = null;

        for (T objet : getEnumType().getEnumConstants())
        {
            if (objet.toString().equals(text) || ((Enum) objet).name().equals(text))
            {
                enumRecherche = objet;
            }
        }

        if (enumRecherche != null)
        {
            setValue(enumRecherche);
        }
        else if (returnNull)
        {
            setValue(null);
        }
        else
        {
            throw new IllegalArgumentException("Invalid text for enum of type '" + enumType.getName() + ": '" + text
                + "'.");
        }
    }

    /**
     * Modificateur de l attribut enum type.
     * 
     * @param enumType le nouveau enum type
     */
    public void setEnumType(Class<T> enumType)
    {
        if (!enumType.isEnum())
        {
            throw new IllegalArgumentException("Class must be an enum");
        }
        this.enumType = enumType;
    }
}