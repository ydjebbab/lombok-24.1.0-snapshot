/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : PeriodeEditor.java
 *
 */
package fr.gouv.finances.lombok.util.propertyeditor;

import java.beans.PropertyEditorSupport;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Permet d'afficher des jours et de stocker l'équivalent en millisecondes
 * 
 * @author lcontinsouzas-cp 
 * @version $Revision: 1.2 $ Date: 15 déc. 2009
 */
public class PeriodeEditor extends PropertyEditorSupport
{
    public PeriodeEditor()
    {
        super(); 
        
    }

    public PeriodeEditor(Object source)
    {
        super(source);    // DOCUMENTEZ_MOI Raccord de constructeur auto-généré
        
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @return as text
     * @see java.beans.PropertyEditorSupport#getAsText()
     */
    @Override
    public String getAsText()
    {
        String result = null;
        if (getValue() != null)
        {
            Long jours = (Long) getValue() * 1000 / (24 * 60 * 60);
//            ancien code
//            Math.round n'est pas utile parce que nbpageprint est un long, une division sur un long donnera un résultat en long
//            int nbjours = Math.round(jours);
            int nbjours = (int) jours.longValue();

            result = nbjours + " j";
        }
        else
        {
            result = "null";
        }
        return result;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param text le nouveau as text
     * @throws IllegalArgumentException the illegal argument exception
     * @see java.beans.PropertyEditorSupport#setAsText(java.lang.String)
     */
    @Override
    public void setAsText(String text) throws IllegalArgumentException
    {
        Pattern p = Pattern.compile("[-+]?[0-9]*\\.?[0-9]+");
        Matcher m = p.matcher(text);

        if (m.find())
        {
            String result = m.group();
            setValue(result);
        }

    }

}
