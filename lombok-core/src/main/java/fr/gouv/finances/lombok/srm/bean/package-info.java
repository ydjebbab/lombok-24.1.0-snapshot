/*
 * Copyright (c) 2017 DGFiP - Tous droits réservés
 * 
*/
/**
 * Paquet regroupant les POJO liés au sireme
 * @author Christophe Breheret-Girardin
 */
package fr.gouv.finances.lombok.srm.bean;