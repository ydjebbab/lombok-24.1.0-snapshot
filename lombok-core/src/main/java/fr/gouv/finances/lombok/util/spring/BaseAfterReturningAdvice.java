/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : BaseAfterReturningAdvice.java
 *
 */
package fr.gouv.finances.lombok.util.spring;

import java.lang.reflect.Method;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.aop.AfterReturningAdvice;

/**
 * Class BaseAfterReturningAdvice.
 * 
 * @author hbesson2-cp
 * @description trace la valeur retournée par une méthode
 * @version $Revision: 1.2 $ Date: 15 déc. 2009
 */
public class BaseAfterReturningAdvice implements AfterReturningAdvice
{
    protected final Log log = LogFactory.getLog(getClass());

    public BaseAfterReturningAdvice()
    {
        super();          
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param returnValue
     * @param m
     * @param arg
     * @param target
     * @see org.springframework.aop.AfterReturningAdvice#afterReturning(java.lang.Object, java.lang.reflect.Method,
     *      java.lang.Object[], java.lang.Object)
     */
    @Override
    public void afterReturning(Object returnValue, Method m, Object[] arg, Object target)
    {
        if (log.isDebugEnabled())
        {
            log.debug("Methode = " + m.getName() + " return " + returnValue);
        }
    }
}
