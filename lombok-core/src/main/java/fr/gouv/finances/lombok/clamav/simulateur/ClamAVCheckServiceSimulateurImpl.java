/*
 * Copyright (c) 2014 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.clamav.simulateur;

import java.io.IOException;
import java.net.BindException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.nio.channels.CancelledKeyException;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.gouv.finances.lombok.clamav.service.impl.ClamAVCheckServiceImpl;

/**
 * Implémentation du service d'interfaçage de clamAV, possédant un serveur interne de simulation de clamAV
 *
 * @author Christophe Breheret-Girardin
 */
public class ClamAVCheckServiceSimulateurImpl
    extends ClamAVCheckServiceImpl implements ClamAVCheckServiceSimulateur
{
    /** Initialisation de la journalisation. */
    protected static final Logger LOGGER = LoggerFactory.getLogger(ClamAVCheckServiceSimulateurImpl.class);

    /** Attente d'evenements sur la socket avec timeout. */
    private static final int TIMEOUT_SELECT = 5000;

    /** LoopSelector. */
    private SelectorRun loopSelector = null;

    /** Pool d'exécution des analyses anti-virales. */
    private final ExecutorService pool;

    /** Thread du serveur. */
    private Thread selectorThread;
    
    private ServerSocketChannel serverChannel;

    /**
     * Constructeur par défaut
     */
    public ClamAVCheckServiceSimulateurImpl()
    {
        super();
        final int threadPool = 100;
        this.pool = Executors.newFixedThreadPool(threadPool);
    }
    
    /**
     * Classe de gestion de l'événement.
     */
    class Acceptor implements Runnable
    {

        /** Evénement lié à la socket. */
        private final SelectionKey key;

        /**
         * Constructeur.
         * 
         * @param key key
         */
        public Acceptor(final SelectionKey key)
        {
            super();
            this.key = key;
        }

        /**
         * {@inheritDoc}
         */
        public void run()
        {
            try
            {
                final ServerSocketChannel serverChannel =
                    (ServerSocketChannel) this.key.channel();
                final SocketChannel channel = serverChannel.accept();

                final Socket socket = channel.socket();
                final SocketAddress remoteAddr =
                    socket.getRemoteSocketAddress();
                LOGGER.info("Simulateur - Connexion à l'adresse : {}", remoteAddr);

                // Lancement du traitement de la requête client
                new ClamAVAnalyseWorkerSimulateur(
                    channel, this.key.selector(),
                    ClamAVCheckServiceSimulateurImpl.this.pool);

            }
            catch (final IOException ioe)
            {
                LOGGER.error("Simulateur - Erreur lors de la tentative d'accès à la socket", ioe);
            }

        }
    }

    /**
     * Classe gérant le traitement d'écoute.
     */
    class SelectorRun implements Runnable
    {
        /** Doit-on arrêter le boucle d'écoute ?. */
        private boolean isStopped = false;

        /** Gestionnaire d'évenement sur la socket. */
        private Selector selector;

        /**
         * Constructeur.
         */
        public SelectorRun()
        {

            super();
            this.isStopped = false;
        }

        /**
         * Arrête les tâches de fond gérant les demandes d'analyse. bascule le booléen et signale au selector (via
         * wakeup) de sortir de l'appel bloquant (select).
         */
        public void stopLoop()
        {
            LOGGER.info("Simulateur - Arrêt demandé");

            this.isStopped = true;
            this.selector.wakeup();
            LOGGER.info("Simulateur - Signal Wakeup");
        }

        /**
         * {@inheritDoc}
         */
        public void run()
        {
            try
            {
                this.createAndListen();
                this.loopSelector();
            }
            catch (final IOException ioe)
            {
                LOGGER.error("Simulateur : erreur lors de la tentative d'accès à la socket", ioe);
            }
            LOGGER.info("Simulateur : arrêt");
        }

        /**
         * Boucle d'attente des événements.
         * 
         * @throws IOException IOException
         */
        private void loopSelector() throws IOException
        {

            while (!this.isStopped)
            {
                // Attente d'evenements sur la socket avec timeout
                // (code défensif à priori inutile - wakeup dans le stopserver)
                this.selector.select(TIMEOUT_SELECT);

                // wakeup to work on selected keys
                final Iterator<SelectionKey> keys =
                    this.selector.selectedKeys().iterator();
                while (keys.hasNext())
                {
                    try
                    {
                        final SelectionKey key = keys.next();

                        // Prévention nécessaire pour éviter que la même clé soit rejouée
                        keys.remove();
                        if (!key.isValid())
                        {
                            continue;
                        }

                        final Runnable runnable = (Runnable) (key.attachment());
                        if (runnable != null)
                        {
                            runnable.run();
                        }
                    }
                    catch (final CancelledKeyException cke)
                    {
                        LOGGER.warn("Simulateur : un événement a été annulé - {}", cke);
                    }
                }
                this.selector.selectedKeys().clear();
            }
            if (this.isStopped)
            {
                LOGGER.info("Simulateur : arrêt reçu");

            }
        }

        /**
         * Méthode permettant de créer la socket.
         * 
         * @throws IOException IOException
         */
        private void createAndListen() throws IOException
        {

            // Information de connexion
            this.selector = Selector.open();
            serverChannel = ServerSocketChannel.open();
            serverChannel.configureBlocking(false);

            // Adresse et port du simulateur
            final InetSocketAddress listenAddr = new InetSocketAddress(getClamAVServer(), getClamAVPort());
            try
            {
                serverChannel.socket().bind(listenAddr);
            }
            catch (BindException be)
            {
                LOGGER.warn("Il y a déjà un processus qui est exécuté sur le port '{}' en local,"
                    + " le simulateur ne peut démarrer", getClamAVPort());
                throw be;
            }

            // Détermination du traitement à exécution lors de la réception d'un événement sur la socket
            final SelectionKey acceptKey = serverChannel.register(this.selector, SelectionKey.OP_ACCEPT);
            acceptKey.attach(new Acceptor(acceptKey));
            LOGGER.info("Simulateur ClamAV en écoute \n");
        }
    }

    /**
     * Méthode permettant de démarrer l'écoute La boucle d'écoute est lancé dans un thread afin de redonner la main au
     * programme principal.
     * 
     * @throws IOException IOException
     * @throws InterruptedException InterruptedException
     */
    public void startServer() throws IOException, InterruptedException
    {
        if (loopSelector == null)
        {
            LOGGER.info(String
                .format(
                    "Simulateur ClamAV Adresse et port : %s %s\n", getClamAVServer(), getClamAVPort()));
    
            // Instanciation de la boule d'écoute
            this.loopSelector = new SelectorRun();
    
            // Démarrage du thread
            this.selectorThread = new Thread(
                this.loopSelector);
            this.selectorThread.setDaemon(true);
            this.selectorThread.start();
    
            LOGGER.debug("Démarré");
        }
    }

    /**
     * Arrête les tâches de fond gérant les demandes d'analyse. bascule le booléen et signale au selector (via wakeup)
     * de sortir de l'appel bloquant (select).
     */
    public synchronized void stopServer()
    {
        if (this.loopSelector != null)
        {
            this.loopSelector.stopLoop();
            this.loopSelector = null;
        }
        if (this.serverChannel != null)
        {
            try
            {
                this.serverChannel.close();
            }
            catch (IOException ioe)
            {
                LOGGER.error(ioe.getMessage(), ioe);
            }
        }
        this.pool.shutdown();
    }

}
