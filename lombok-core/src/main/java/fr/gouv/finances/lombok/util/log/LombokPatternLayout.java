/*
 * Copyright (c) 2011 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.util.log;

import org.apache.logging.log4j.core.LogEvent;

//import org.apache.logging.log4j.PatternLayout;


/**
 * Class LombokPatternLayout .
 * 
 * @deprecated
 * si le besoin est tjs là (réécrire le timestamp avec la date forcée via jodatime, il faudra refaire cette classe.
 * En attendant, nous la "supprimons" en la marquant deprecated pour le moment .
 * Cette classe était référencée dans le fichier log4j.properties.
 */
@Deprecated
public class LombokPatternLayout //extends PatternLayout
{

    /**
     * Constructeur de la classe LombokPatternLayout.java
     *
     */
    public LombokPatternLayout()
    {
        super();  
        
    }

    /**
     * Constructeur de la classe LombokPatternLayout.java
     *
     * @param pattern
     */
    public LombokPatternLayout(String pattern)
    {
        //super(pattern);    // DOCUMENTEZ_MOI Raccord de constructeur auto-généré
        
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see org.apache.log4j.PatternLayout#format(org.apache.log4j.spi.LoggingEvent)
     */
    public String format(LogEvent event)
    {
       /* LoggingEvent eventModifie;
        if (Boolean.valueOf(ApplicationPropertiesUtil.getProperty("date.estforcee")))
        {
            eventModifie =
                new LoggingEvent(event.fqnOfCategoryClass, event.getLogger(), new DateTime().getMillis(), event.getLevel(),
                    event.getMessage(), event.getThreadName(), event.getThrowableInformation(), event.getNDC(),
                    event.getLocationInformation(), event.getProperties());
        }
        else
        {
            eventModifie = event;
        }
        return super.format(eventModifie);*/
        return null;
    }

}
