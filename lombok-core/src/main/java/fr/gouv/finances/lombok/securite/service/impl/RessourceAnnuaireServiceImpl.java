/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : RessourceAnnuaireServiceImpl.java
 *
 */
package fr.gouv.finances.lombok.securite.service.impl;

import java.util.Properties;

import javax.naming.CommunicationException;
import javax.naming.ConfigurationException;
import javax.naming.Context;
import javax.naming.InsufficientResourcesException;
import javax.naming.NamingException;
import javax.naming.ReferralException;
import javax.naming.ServiceUnavailableException;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SchemaViolationException;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jndi.JndiTemplate;

import fr.gouv.finances.lombok.securite.service.RessourceAnnuaireService;
import fr.gouv.finances.lombok.securite.techbean.AnnuaireException;

/**
 * Class RessourceAnnuaireServiceImpl
 * 
 * @author amleplatinec
 * @version $Revision: 1.3 $ Date: 11 déc. 2009
 */
public class RessourceAnnuaireServiceImpl implements RessourceAnnuaireService
{
    /** Constant : log. */
    protected static final Log log = LogFactory.getLog(RessourceAnnuaireServiceImpl.class);

    /** Constant : PB_DISPONIBILITE_ANNUAiRE. */
    private static final String PB_DISPONIBILITE_ANNUAiRE = "L'annuaire ldap n'est pas disponible";

    /** Constant : PB_DISPONIBILITE_ANNUAiRE_PRINCIPAL_ET_SECOURS. */
    private static final String PB_DISPONIBILITE_ANNUAiRE_PRINCIPAL_ET_SECOURS =
        "L'annuaire ldap n'est pas disponible (principal et secours)";

    /** Constant : PB_FERMETURE. */
    private static final String PB_FERMETURE = "Erreur lors de la fermeture de la connexion LDAP";

    /** url annuaire ldap. */
    private String urlAnnuaireLDAP;

    /** url annuaire ldap secours. */
    private String urlAnnuaireLDAPSecours = null;

    /** timeout tentative de connexion annuaire ldap. */
    // Timeout
    private String timeoutTentativeDeConnexionAnnuaireLDAP = "10000";

    /** ldapctxfactory. */
    // private LdapCtxFactory ldapctxfactory = new LdapCtxFactory();
    private final JndiTemplate jnditemplate = new JndiTemplate();

    /** utiliser le pool de connexions ldap. */
    private boolean utiliserLePoolDeConnexionsLdap = false;

    public RessourceAnnuaireServiceImpl()
    {
        super();        
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param unDirContext "documenté"
     * @see fr.gouv.finances.lombok.securite.service.RessourceAnnuaireService#fermerLDAPConnexion(javax.naming.directory.DirContext)
     */
    @Override
    public void fermerLDAPConnexion(DirContext unDirContext)
    {
        try
        {
            if (unDirContext != null)
            {
                unDirContext.close();
            }
        }
        catch (RuntimeException | NamingException e)
        {
            log.warn(PB_FERMETURE, e);
            // On ignore l'exception
        }
    }

    /*
     * (non-Javadoc)
     * @see fr.gouv.finances.lombok.securite.service.impl.RessourceAnnuaireService#fournirUnSqueletteDEnvironnement()
     */
    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @return properties
     * @see fr.gouv.finances.lombok.securite.service.RessourceAnnuaireService#fournirUnSqueletteDEnvironnement()
     */
    @Override
    public Properties fournirUnSqueletteDEnvironnement()
    {
        Properties props = new Properties();

        props.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
        if (utiliserLePoolDeConnexionsLdap)
        {
            props.put("com.sun.jndi.ldap.connect.pool", "true");
        }
        props.put("com.sun.jndi.ldap.connect.timeout", this.timeoutTentativeDeConnexionAnnuaireLDAP);
        props.put(Context.PROVIDER_URL, this.urlAnnuaireLDAP);
        return props;
    }

    /**
     * Accesseur de l attribut timeout tentative de connexion annuaire ldap.
     * 
     * @return timeout tentative de connexion annuaire ldap
     */
    public String getTimeoutTentativeDeConnexionAnnuaireLDAP()
    {
        return timeoutTentativeDeConnexionAnnuaireLDAP;
    }

    /**
     * Accesseur de l attribut url annuaire ldap.
     * 
     * @return url annuaire ldap
     */
    public String getUrlAnnuaireLDAP()
    {
        return urlAnnuaireLDAP;
    }

    /**
     * Accesseur de l attribut url annuaire ldap secours.
     * 
     * @return url annuaire ldap secours
     */
    public String getUrlAnnuaireLDAPSecours()
    {
        return urlAnnuaireLDAPSecours;
    }

    /**
     * Verifie si utiliser le pool de connexions ldap.
     * 
     * @return true, si c'est utiliser le pool de connexions ldap
     */
    public boolean isUtiliserLePoolDeConnexionsLdap()
    {
        return utiliserLePoolDeConnexionsLdap;
    }

    /*
     * (non-Javadoc)
     * @see
     * fr.gouv.finances.lombok.securite.service.impl.RessourceAnnuaireService#ouvreLdapConnexion(java.util.Hashtable)
     */
    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param props "documenté"
     * @return dir context
     * @throws NamingException the naming exception
     * @see fr.gouv.finances.lombok.securite.service.RessourceAnnuaireService#ouvreLdapConnexion(java.util.Hashtable)
     */
    @Override
    public DirContext ouvreLdapConnexion(Properties props) throws NamingException
    {

        DirContext unApplicationDirContext = null;
        log.debug("Pool de connexion ldap actif ? " + utiliserLePoolDeConnexionsLdap);

        try
        {
            if (utiliserLePoolDeConnexionsLdap)
            {
                jnditemplate.setEnvironment(props);
                unApplicationDirContext = (DirContext) jnditemplate.getContext();
            }
            else
            {
                unApplicationDirContext = new InitialDirContext(props);
            }

        }
        catch (NamingException e)
        {
            // Liste des causes pouvant nécessiter de basculer sur le secours
            if (!(e instanceof CommunicationException) && !(e instanceof ConfigurationException)
                && !(e instanceof InsufficientResourcesException) && !(e instanceof ReferralException)
                && !(e instanceof SchemaViolationException) && !(e instanceof ServiceUnavailableException))
            {
                throw e;
            }

            // retenter sur l'adresse de secours si elle existe
            if (!StringUtils.isEmpty(this.urlAnnuaireLDAPSecours))
            {
                log.warn("Service ldap indisponible (" + e.getClass().getName() + ") sur l'adresse principale : "
                    + props.get(Context.PROVIDER_URL));
                props.remove(Context.PROVIDER_URL);
                unApplicationDirContext = retenterOuvreLDAPConnexionSurLDAPSecours(props, this.urlAnnuaireLDAPSecours);
            }
            else
            {
                log.error("Service ldap indisponible sur l'adresse principale " + props.get(Context.PROVIDER_URL)
                    + " - aucune adresse de secours précisée");
                throw new AnnuaireException(PB_DISPONIBILITE_ANNUAiRE, e);
            }

        }
        return unApplicationDirContext;
    }

    /**
     * Modificateur de l attribut timeout tentative de connexion annuaire ldap.
     * 
     * @param timeoutTentativeDeConnexionAnnuaireLDAP le nouveau timeout tentative de connexion annuaire ldap
     */
    public void setTimeoutTentativeDeConnexionAnnuaireLDAP(String timeoutTentativeDeConnexionAnnuaireLDAP)
    {
        this.timeoutTentativeDeConnexionAnnuaireLDAP = timeoutTentativeDeConnexionAnnuaireLDAP;
    }

    /**
     * Modificateur de l attribut url annuaire ldap.
     * 
     * @param urlAnnuaireLDAP le nouveau url annuaire ldap
     */
    public void setUrlAnnuaireLDAP(String urlAnnuaireLDAP)
    {
        this.urlAnnuaireLDAP = urlAnnuaireLDAP;
    }

    /**
     * Modificateur de l attribut url annuaire ldap secours.
     * 
     * @param urlAnnuaireLDAPSecours le nouveau url annuaire ldap secours
     */
    public void setUrlAnnuaireLDAPSecours(String urlAnnuaireLDAPSecours)
    {
        this.urlAnnuaireLDAPSecours = urlAnnuaireLDAPSecours;
    }

    /**
     * Modificateur de l attribut utiliser le pool de connexions ldap.
     * 
     * @param utiliserLePool le nouveau utiliser le pool de connexions ldap
     */
    public void setUtiliserLePoolDeConnexionsLdap(boolean utiliserLePool)
    {
        this.utiliserLePoolDeConnexionsLdap = utiliserLePool;
    }

    /**
     * methode Retenter ouvre ldap connexion sur ldap secours : --.
     * 
     * @param props "documenté"
     * @param urlAnnuaireLDAPSecours --
     * @return dir context
     */
    private DirContext retenterOuvreLDAPConnexionSurLDAPSecours(Properties props, String urlAnnuaireLDAPSecours)
    {
        DirContext unApplicationDirContext = null;
        props.put(Context.PROVIDER_URL, urlAnnuaireLDAPSecours);
        try
        {
            if (utiliserLePoolDeConnexionsLdap)
            {
                jnditemplate.setEnvironment(props);
                unApplicationDirContext = (DirContext) jnditemplate.getContext();
            }
            else
            {
                unApplicationDirContext = new InitialDirContext(props);
            }
            log.warn("Connexion réussie sur annuaire de secours : " + urlAnnuaireLDAPSecours);
        }
        catch (NamingException exception)
        {
            log.error("Impossible de se connecter au ldap sur l'adresse de secours " + urlAnnuaireLDAPSecours);
            throw new AnnuaireException(PB_DISPONIBILITE_ANNUAiRE_PRINCIPAL_ET_SECOURS, exception);
        }
        return unApplicationDirContext;

    }
}
