/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : MelService.java
 *
 */
package fr.gouv.finances.lombok.mel.service;

import java.util.Properties;

import javax.mail.Session;

import fr.gouv.finances.lombok.mel.techbean.Mel;

/**
 * Interface MelService.
 * 
 * @author lcontinsouzas-cp
 * @version $Revision: 1.2 $ Date: 15 déc. 2009
 */
public interface MelService
{

    /**
     * methode Envoyer mel : --.
     * 
     * @param mel --
     */
    public void envoyerMel(Mel mel);

    /**
     * methode Envoyer mel avec contenu html : --.
     * 
     * @param mel --
     */
    public void envoyerMelAvecContenuHTML(Mel mel);

    /**
     * Accesseur de l attribut host.
     * 
     * @return host
     */
    public String getHost();

    /**
     * Accesseur de l attribut java mail properties.
     * 
     * @return java mail properties
     */
    public Properties getJavaMailProperties();

    /**
     * Accesseur de l attribut password.
     * 
     * @return password
     */
    public String getPassword();

    /**
     * Accesseur de l attribut port.
     * 
     * @return port
     */
    public int getPort();

    /**
     * Accesseur de l attribut protocol.
     * 
     * @return protocol
     */
    public String getProtocol();

    /**
     * Accesseur de l attribut session.
     * 
     * @return session
     */
    public Session getSession();

    /**
     * Accesseur de l attribut user name.
     * 
     * @return user name
     */
    public String getUserName();

    /**
     * Modificateur de l attribut host.
     * 
     * @param host le nouveau host
     */
    public void setHost(String host);

    /**
     * Modificateur de l attribut java mail properties.
     * 
     * @param javamailproperties le nouveau java mail properties
     */
    public void setJavaMailProperties(Properties javamailproperties);

    /**
     * Modificateur de l attribut password.
     * 
     * @param password le nouveau password
     */
    public void setPassword(String password);

    /**
     * Modificateur de l attribut port.
     * 
     * @param port le nouveau port
     */
    public void setPort(int port);

    /**
     * Modificateur de l attribut protocol.
     * 
     * @param protocol le nouveau protocol
     */
    public void setProtocol(String protocol);

    /**
     * Modificateur de l attribut session.
     * 
     * @param session le nouveau session
     */
    public void setSession(Session session);

    /**
     * Modificateur de l attribut user name.
     * 
     * @param username le nouveau user name
     */
    public void setUserName(String username);

}