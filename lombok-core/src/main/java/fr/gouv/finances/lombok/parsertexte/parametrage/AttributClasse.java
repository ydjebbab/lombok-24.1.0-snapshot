/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 */
package fr.gouv.finances.lombok.parsertexte.parametrage;

/**
 * Class AttributClasse --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.2 $ Date: 11 déc. 2009
 */
public class AttributClasse
{

    /** nom. */
    String nom = null;

    /** type. */
    String type = null;

    /** taille max. */
    int tailleMax = -1;

    /** taille min. */
    int tailleMin = -1;

    /** obligatoire. */
    boolean obligatoire = false;

    /** suppression espace autour. */
    boolean suppressionEspaceAutour = false;

    /** type controle. */
    int typeControle = TypeControles.CONTROLE_AUCUN;

    /** controle a effectuer. */
    boolean controleAEffectuer = false;

    /** mise en forme a effectuer. */
    boolean miseEnFormeAEffectuer = false;

    /**
     * Instanciation de attribut classe.
     * 
     * @param pNom le nom
     * @param pType le type
     */
    public AttributClasse(String pNom, String pType)
    {
        this.nom = pNom;
        this.type = pType;

        this.majControleOuMiseEnFormeAEffectuer();
    }

    /**
     * Instanciation de attribut classe.
     * 
     * @param pNom le nom
     * @param pType le type
     * @param pObligatoire le obligatoire
     * @param pSuppressionEspace le suppression espace
     */
    public AttributClasse(String pNom, String pType, boolean pObligatoire, boolean pSuppressionEspace)
    {
        this.nom = pNom;
        this.type = pType;
        this.obligatoire = pObligatoire;
        this.suppressionEspaceAutour = pSuppressionEspace;

        this.majControleOuMiseEnFormeAEffectuer();
    }

    /**
     * Instanciation de attribut classe.
     * 
     * @param pNom le nom
     * @param pType le type
     * @param pObligatoire le obligatoire
     * @param pSuppressionEspace le suppression espace
     * @param pTypeControle le type controle
     */
    public AttributClasse(String pNom, String pType, boolean pObligatoire, boolean pSuppressionEspace, int pTypeControle)
    {
        this.nom = pNom;
        this.type = pType;
        this.obligatoire = pObligatoire;
        this.suppressionEspaceAutour = pSuppressionEspace;
        this.typeControle = pTypeControle;

        this.majControleOuMiseEnFormeAEffectuer();
    }

    /**
     * Instanciation de attribut classe.
     * 
     * @param pNom le nom
     * @param pType le type
     * @param pObligatoire le obligatoire
     * @param pSuppressionEspace le suppression espace
     * @param pTailleMax le taille max
     * @param pTypeControle le type controle
     */
    public AttributClasse(String pNom, String pType, boolean pObligatoire, boolean pSuppressionEspace, int pTailleMax,
        int pTypeControle)
    {
        this.nom = pNom;
        this.type = pType;
        this.obligatoire = pObligatoire;
        this.suppressionEspaceAutour = pSuppressionEspace;
        this.tailleMax = pTailleMax;
        this.typeControle = pTypeControle;

        this.majControleOuMiseEnFormeAEffectuer();
    }

    /**
     * Instanciation de attribut classe.
     * 
     * @param pNom le nom
     * @param pType le type
     * @param pObligatoire le obligatoire
     * @param pSuppressionEspace le suppression espace
     * @param pTailleMin le taille min
     * @param pTailleMax le taille max
     * @param pTypeControle le type controle
     */
    public AttributClasse(String pNom, String pType, boolean pObligatoire, boolean pSuppressionEspace, int pTailleMin,
        int pTailleMax, int pTypeControle)
    {
        this.nom = pNom;
        this.type = pType;
        this.obligatoire = pObligatoire;
        this.suppressionEspaceAutour = pSuppressionEspace;
        this.tailleMin = pTailleMin;
        this.tailleMax = pTailleMax;
        this.typeControle = pTypeControle;

        this.majControleOuMiseEnFormeAEffectuer();
    }

    /**
     * Cette méthode retourne un booléen indiquant si un contrôle est nécessaire à réaliser sur cette valeur.
     * 
     * @return true, if controle a effectuer
     */
    public boolean controleAEffectuer()
    {
        return this.controleAEffectuer;
    }

    /**
     * Accesseur de l attribut nom.
     * 
     * @return nom
     */
    public String getNom()
    {
        return nom;
    }

    /**
     * Accesseur de l attribut taille max.
     * 
     * @return taille max
     */
    public int getTailleMax()
    {
        return tailleMax;
    }

    /**
     * Accesseur de l attribut taille min.
     * 
     * @return taille min
     */
    public int getTailleMin()
    {
        return tailleMin;
    }

    /**
     * Accesseur de l attribut type.
     * 
     * @return type
     */
    public String getType()
    {
        return type;
    }

    /**
     * Accesseur de l attribut type controle.
     * 
     * @return type controle
     */
    public int getTypeControle()
    {
        return typeControle;
    }

    /**
     * Verifie si obligatoire.
     * 
     * @return true, si c'est obligatoire
     */
    public boolean isObligatoire()
    {
        return obligatoire;
    }

    /**
     * Verifie si suppression espace autour.
     * 
     * @return true, si c'est suppression espace autour
     */
    public boolean isSuppressionEspaceAutour()
    {
        return suppressionEspaceAutour;
    }

    /**
     * Cette méthode retourne un booléen indiquant si une mise en forme est nécessaire à réaliser sur cette valeur.
     * 
     * @return true, if mise en forme a effectuer
     */
    public boolean miseEnFormeAEffectuer()
    {
        return this.miseEnFormeAEffectuer;
    }

    /**
     * Modificateur de l attribut nom.
     * 
     * @param nom le nouveau nom
     */
    public void setNom(String nom)
    {
        this.nom = nom;
    }

    /**
     * Modificateur de l attribut obligatoire.
     * 
     * @param obligatoire le nouveau obligatoire
     */
    public void setObligatoire(boolean obligatoire)
    {
        this.obligatoire = obligatoire;
        this.majControleOuMiseEnFormeAEffectuer();
    }

    /**
     * Modificateur de l attribut suppression espace autour.
     * 
     * @param suppressionEspaceAutour le nouveau suppression espace autour
     */
    public void setSuppressionEspaceAutour(boolean suppressionEspaceAutour)
    {
        this.suppressionEspaceAutour = suppressionEspaceAutour;
        this.majControleOuMiseEnFormeAEffectuer();
    }

    /**
     * Modificateur de l attribut taille max.
     * 
     * @param tailleMax le nouveau taille max
     */
    public void setTailleMax(int tailleMax)
    {
        this.tailleMax = tailleMax;
        this.majControleOuMiseEnFormeAEffectuer();
    }

    /**
     * Modificateur de l attribut taille min.
     * 
     * @param tailleMin le nouveau taille min
     */
    public void setTailleMin(int tailleMin)
    {
        this.tailleMin = tailleMin;
        this.majControleOuMiseEnFormeAEffectuer();
    }

    /**
     * Modificateur de l attribut type.
     * 
     * @param type le nouveau type
     */
    public void setType(String type)
    {
        this.type = type;
    }

    /**
     * Modificateur de l attribut type controle.
     * 
     * @param typeControle le nouveau type controle
     */
    public void setTypeControle(int typeControle)
    {
        this.typeControle = typeControle;
        this.majControleOuMiseEnFormeAEffectuer();
    }

    /**
     * Méthode interne qui initialise le booléen représentant le fait qu'un contrôle ou une mise en forme ait à être
     * exécutée.
     */
    private void majControleOuMiseEnFormeAEffectuer()
    {
        this.controleAEffectuer =
            (this.obligatoire) || (this.tailleMax != -1) || (this.tailleMin != -1)
                || (this.typeControle != TypeControles.CONTROLE_AUCUN);
        this.miseEnFormeAEffectuer = (this.suppressionEspaceAutour);
    }

}
