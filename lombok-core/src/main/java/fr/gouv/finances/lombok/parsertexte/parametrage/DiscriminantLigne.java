/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 */
package fr.gouv.finances.lombok.parsertexte.parametrage;

import java.util.ArrayList;
import java.util.List;

/**
 * Class DiscriminantLigne --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.2 $ Date: 11 déc. 2009
 */
public class DiscriminantLigne
{

    /** criteres. */
    List criteres = null;

    /**
     * Instanciation de discriminant ligne.
     */
    public DiscriminantLigne()
    {
        super();
    }

    /**
     * Ajouter critere.
     * 
     * @param critere --
     */
    public void ajouterCritere(CritereDiscriminant critere)
    {
        if (this.criteres == null)
        {
            this.criteres = new ArrayList();
        }
        if (critere != null)
        {
            this.criteres.add(critere);
        }
    }

    /**
     * Accesseur de l attribut criteres.
     * 
     * @return criteres
     */
    public List getCriteres()
    {
        return criteres;
    }

    /**
     * Sets the criteres.
     * 
     * @param criteres --
     */
    public void setCriteres(List criteres)
    {
        this.criteres = criteres;
    }

}