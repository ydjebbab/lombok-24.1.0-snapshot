/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 */
package fr.gouv.finances.lombok.monnaie.bean;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import fr.gouv.finances.lombok.util.base.BaseBean;

/**
 * Class Monnaie reprise d'éléments communs des beans dans eaf et coriolis.
 * 
 * @author amleplatinec-cp
 * @version $Revision: 1.3 $ Date: 15 déc. 2009
 */
public class Monnaie extends BaseBean
{

    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** id. */
    private Long id;

    /** version. */
    private int version;

    /** libelle monnaie. */
    private String libelleMonnaie;

    /** code bdf. */
    private String codeBDF;

    /** list taux chancel. */
    private Set<TauxDeChancellerie> listTauxChancel;

    /**
     * Constructeur.
     *
     */
    public Monnaie()
    {
        super(); 
    }

    /** 
     * {@inheritDoc}
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode()
    {
        return Objects.hash(codeBDF, libelleMonnaie);
    }

    /** 
     * {@inheritDoc}
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj)
    {
        if (obj == this)
        {
            return true;
        }

        if (obj == null || this.getClass() != obj.getClass())
        {
            return false;
        }

        Monnaie monnaie = (Monnaie) obj;
        return Objects.equals(codeBDF, monnaie.codeBDF)
            && Objects.equals(libelleMonnaie, monnaie.libelleMonnaie);
    }
    
    /**
     * Accesseur de l attribut code bdf.
     * 
     * @return code bdf
     */
    public String getCodeBDF()
    {
        return codeBDF;
    }

    /**
     * Accesseur de l attribut id.
     * 
     * @return id
     */
    public Long getId()
    {
        return id;
    }

    /**
     * Accesseur de l attribut libelle monnaie.
     * 
     * @return libelle monnaie
     */
    public String getLibelleMonnaie()
    {
        return libelleMonnaie;
    }

    /**
     * Accesseur de l attribut list taux chancel.
     * 
     * @return list taux chancel
     */
    public Set<TauxDeChancellerie> getListTauxChancel()
    {
        return listTauxChancel;
    }

    /**
     * Accesseur de l attribut version.
     * 
     * @return version
     */
    public int getVersion()
    {
        return version;
    }

    /**
     * Modificateur de l attribut code bdf.
     * 
     * @param codeBDF le nouveau code bdf
     */
    public void setCodeBDF(String codeBDF)
    {
        this.codeBDF = codeBDF;
    }

    /**
     * Modificateur de l attribut id.
     * 
     * @param id le nouveau id
     */
    public void setId(Long id)
    {
        this.id = id;
    }

    /**
     * Modificateur de l attribut libelle monnaie.
     * 
     * @param libelleMonnaie le nouveau libelle monnaie
     */
    public void setLibelleMonnaie(String libelleMonnaie)
    {
        this.libelleMonnaie = libelleMonnaie;
    }

    /**
     * Modificateur de l attribut list taux chancel.
     * 
     * @param listTauxChancellerie le nouveau list taux chancel
     */
    public void setListTauxChancel(Set<TauxDeChancellerie> listTauxChancellerie)
    {
        this.listTauxChancel = listTauxChancellerie;
    }

    /**
     * Modificateur de l attribut taux chancellerie.
     * 
     * @param taux le nouveau taux chancellerie
     */
    public void setTauxChancellerie(TauxDeChancellerie taux)
    {
        // utiliser à la création de la monnaie lorsqu'il n'y a forcément qu'un seul taux
        if (this.getListTauxChancel() == null)
        {
            this.listTauxChancel = new HashSet<>();
        }
        
        if (taux != null)
        {
            this.listTauxChancel.add(taux);
        }
    }

    /**
     * Modificateur de l attribut version.
     * 
     * @param version le nouveau version
     */
    public void setVersion(int version)
    {
        this.version = version;
    }

    /** 
     * {@inheritDoc}
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        return libelleMonnaie;
    }
}
