/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : CpCustomNumberEditor.java
 *
 */
package fr.gouv.finances.lombok.util.propertyeditor;

import java.beans.PropertyEditorSupport;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.Locale;

import org.apache.commons.lang.StringUtils;

import fr.gouv.finances.lombok.util.format.FormaterNombre;

/**
 * Class CpCustomNumberEditor
 * 
 * @author amleplatinec
 * @version $Revision: 1.2 $ Date: 11 déc. 2009
 */
public class CpCustomNumberEditor extends PropertyEditorSupport
{

    /** Constant : NULL. */
    public static final int NULL = 1;

    /** Constant : ZERO. */
    public static final int ZERO = 2;

    /** Constant : NOT_ALLOWED. */
    public static final int NOT_ALLOWED = 3;

    /**
     * Teste si une chaîne de caractère est composée uniquement de chiffres, des caractères ' ', ',', '.' et
     * éventuellement du caractère '-' en début de chaîne
     * 
     * @param str --
     * @return true, if checks if is numeric space
     */
    public static boolean isNumericSpace(String str)
    {
        boolean result = true;

        if (str == null)
        {
            result = false;
        }
        else
        {
            int sz = str.length();

            DecimalFormatSymbols dformat = new DecimalFormatSymbols(Locale.FRANCE);
            char groupSeparator = dformat.getGroupingSeparator();

            for (int i = 0; i < sz; i++)
            {
                if ((Character.isDigit(str.charAt(i)) == false) && (StringUtils.isNotBlank(str.charAt(i) + ""))
                    && groupSeparator != str.charAt(i) && str.charAt(i) != ',' && str.charAt(i) != '.'
                    && (str.charAt(i) != '-' || i != 0))
                {
                    result = false;
                }
            }
        }
        return result;
    }

    /** number class. */
    private final Class numberClass;

    /** number format. */
    private final NumberFormat numberFormat;

    /** empty interpreted as. */
    private final int emptyInterpretedAs;

    /** refuse nombre avec trop de decimales. */
    private final boolean refuseNombreAvecTropDeDecimales;

    /**
     * Crée un object CpCustomNumberEditor en utilisant comme format par défaut
     * <code>Formater.getFormatNombredeuxDecimales()</code> pour analyser la chaîne de caractères et pour formater un
     * nombre.
     * <p>
     * Une chaîne vide est interprêtée comme une valeur <code>null</code>.
     * </p>
     * 
     * @param numberClass Sous classe de Number produite par le property editor
     * @throws IllegalArgumentException si la classe passée dans le paramètre numberClass n'est pas valide
     */
    public CpCustomNumberEditor(Class numberClass) throws IllegalArgumentException
    {
        this(numberClass, FormaterNombre.getFormatNombreDeuxDecimales(), NULL);
    }

    /**
     * Crée un object CpCustomNumberEditor en utilisant comme format par défaut
     * <code>Formater.getFormatNombredeuxDecimales()</code> pour analyser la chaîne de caractères et pour formater un
     * nombre.
     * <p>
     * Le paramètre "emptyAs" indique la façon dont une chaîne chaîne vide est interprêtée :
     * <ul>
     * <li>CpCustomNumberEditor.NULL = <code>null</code>.</li>
     * <li>CpCustomNumberEditor.ZERO = <code>zero</code>.</li>
     * <li>CpCustomNumberEditor.NOT_ALLOWED = <code>non autorisé</code>.</li>
     * </ul>
     * 
     * @param numberClass Sous classe de Number produite par le property editor
     * @param emptyAs --
     * @throws IllegalArgumentException si la classe passée dans le paramètre numberClass n'est pas valide
     */
    public CpCustomNumberEditor(Class numberClass, int emptyAs) throws IllegalArgumentException
    {
        this(numberClass, FormaterNombre.getFormatNombreDeuxDecimales(), emptyAs);
    }

    /**
     * Crée un object CpCustomNumberEditor en utilisant comme le format passé en paramètre pour analyser la chaîne de
     * caractères et pour formater un nombre.
     * <p>
     * Une chaîne vide est interprêtée comme une valeur <code>null</code>.
     * </p>
     * 
     * @param numberClass Sous classe de Number produite par le property editor
     * @param numberFormat --
     * @throws IllegalArgumentException si la classe passée dans le paramètre numberClass n'est pas valide
     */
    public CpCustomNumberEditor(Class numberClass, NumberFormat numberFormat) throws IllegalArgumentException
    {
        this(numberClass, numberFormat, NULL);
    }

    /**
     * Crée un object CpCustomNumberEditor en utilisant comme le format passé en paramètre pour analyser la chaîne de
     * caractères et pour formater un nombre. Les nombres saisis avec plus de décimales qu'indiqué dans le paramètre
     * numberFormat sont acceptés : le nombre est tronqué pour se conformer au format.
     * <p>
     * Le paramètre "emptyAs" indique la façon dont une chaîne chaîne vide est interprêtée :
     * <ul>
     * <li>CpCustomNumberEditor.NULL = <code>null</code>.</li>
     * <li>CpCustomNumberEditor.ZERO = <code>zero</code>.</li>
     * <li>CpCustomNumberEditor.NOT_ALLOWED = <code>non autorisé</code>.</li>
     * </ul>
     * 
     * @param numberClass Sous classe de Number produite par le property editor
     * @param numberFormat format de type NumberFormat utilisé pour analyser et formater
     * @param emptyAs --
     * @throws IllegalArgumentException si la classe passée dans le paramètre numberClass n'est pas valide
     */
    public CpCustomNumberEditor(Class numberClass, NumberFormat numberFormat, int emptyAs)
        throws IllegalArgumentException
    {
        this(numberClass, numberFormat, emptyAs, false);
    }

    /**
     * Crée un object CpCustomNumberEditor en utilisant comme le format passé en paramètre pour analyser la chaîne de
     * caractères et pour formater un nombre.
     * <p>
     * Le paramètre "emptyAs" indique la façon dont une chaîne chaîne vide est interprêtée :
     * <ul>
     * <li>CpCustomNumberEditor.NULL = <code>null</code>.</li>
     * <li>CpCustomNumberEditor.ZERO = <code>zero</code>.</li>
     * <li>CpCustomNumberEditor.NOT_ALLOWED = <code>non autorisé</code>.</li>
     * </ul>
     * 
     * @param numberClass Sous classe de Number produite par le property editor
     * @param numberFormat format de type NumberFormat utilisé pour analyser et formater
     * @param emptyInterpretedAs --
     * @param refuseNombreAvecTropDeDecimales true : indique que le property editor doit refuser les nombres saisis avec
     *        trop de décimales.
     */
    public CpCustomNumberEditor(Class numberClass, NumberFormat numberFormat, int emptyInterpretedAs,
        boolean refuseNombreAvecTropDeDecimales)
    {
        super();
        if (numberClass == null || !Number.class.isAssignableFrom(numberClass))
        {
            throw new IllegalArgumentException("La propriété 'numberClass' doit être une sous classe de 'Number'");
        }
        this.numberClass = numberClass;
        this.numberFormat = numberFormat;
        this.emptyInterpretedAs = emptyInterpretedAs;
        this.refuseNombreAvecTropDeDecimales = refuseNombreAvecTropDeDecimales;
    }

    /**
     * Formate le nombre comme une chaîne de caractères en utilisant le format associé au property editor.
     * 
     * @return the as text
     */
    @Override
    public String getAsText()
    {
        String result;
        Object value = getValue();

        if (value == null)
        {
            result = "";
        }
        else if (this.numberFormat == null)
        {
            result = value.toString();
        }
        else
        {
            // Utilise le format associé au property editor
            result = this.numberFormat.format(value);
        }
        return result;
    }

    /**
     * Détermine le nombre correspondant à la chaîne de caractères passée en paramètre en utilisant le format associé au
     * property editor.
     * 
     * @param text --
     * @throws IllegalArgumentException the illegal argument exception
     */
    @Override
    public void setAsText(String text) throws IllegalArgumentException
    {

        if (this.emptyInterpretedAs != NOT_ALLOWED && StringUtils.isBlank(text))
        {
            if (this.emptyInterpretedAs == ZERO)
            {
                // Traite une chaîne vide comme une valeur égale
                // à 0
                setValue(NumericUtility.parseNumber("0", this.numberClass, this.numberFormat));
            }
            else
            {
                // Traite une chaîne vide comme une valeur nulle
                setValue(null);
            }
        }
        else
        {
            String textEpure = this.epure(text);
            if (!isNumericSpace(textEpure))
            {
                throw new IllegalArgumentException("La chaîne de caractères ne peut pas être convertie en nombre.");
            }

            this.testSiNombreMaxDecimalesRespecte(textEpure);

            if (this.numberFormat == null)
            {
                // Utilise le format par défaut pour analyser le
                // texte
                setValue(NumericUtility.parseNumber(textEpure, this.numberClass));
            }
            else
            {
                // Utilise le format passé en paramètre pour
                // analyser la chaîne.
                setValue(NumericUtility.parseNumber(textEpure, this.numberClass, this.numberFormat));
            }
        }
    }

    /**
     * Supprime tous les espaces du texte Remplace les séparateurs de type . par une virgule
     * 
     * @param text --
     * @return the string
     */
    private String epure(String text)
    {
        String result = text;
        result = result.replaceAll("\\s+", "");
        result = result.replace('.', ',');
        return result;
    }

    /**
     * methode Test si nombre max decimales respecte : --.
     * 
     * @param textEpure --
     */
    private void testSiNombreMaxDecimalesRespecte(String textEpure)
    {
        if (this.refuseNombreAvecTropDeDecimales)
        {
            int fractionDigit = NumericUtility.countDecimalfractionDigit(textEpure, Locale.FRANCE);
            if (fractionDigit > this.numberFormat.getMaximumFractionDigits())
            {
                StringBuilder   mes = new StringBuilder();
                mes.append("Le nombre saisi (");
                mes.append(textEpure);
                mes.append(") doit comporter au maximum ");
                mes.append(this.numberFormat.getMaximumFractionDigits());
                mes.append(" décimales");

                throw new IllegalArgumentException(mes.toString());
            }

        }
    }

}
