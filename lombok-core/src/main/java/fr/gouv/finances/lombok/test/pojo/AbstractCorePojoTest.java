/*
 * Copyright (c) 2016 DGFiP - Tous droits réservés
 * 
 */
package fr.gouv.finances.lombok.test.pojo;

import static org.junit.Assert.assertEquals;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.gouv.finances.lombok.test.controle.Executable;
import fr.gouv.finances.lombok.test.controle.VerificationExecution;
import fr.gouv.finances.lombok.util.exception.IngerableException;

/**
 * Socle des tests des POJO (Plain Old Java Object).
 *
 * @param <T> POJO à tester
 *
 * @author Christophe Breheret-Girardin
 */
public abstract class AbstractCorePojoTest<T>
{
    /** Initialisation de la journalisation. */
    protected static final Logger LOGGER = LoggerFactory.getLogger(AbstractCorePojoTest.class);

    /** Type du bean paramétrisé. */
    private Class<T> parameterizedType = getParameterizedType();

    /** Préfix des méthodes des mutateurs. */
    private static final String PREFIX_MUTATEUR = "set";

    /** Préfix des méthodes des accesseurs. */
    private static final String PREFIX_ACCESSEUR = "get";

    /** Préfix des méthodes des accesseurs booléens. */
    private static final String PREFIX_ACCESSEUR_BOOLEEN = "is";
    
    /** Méthode "toString". */
    private static final String TO_STRING = "toString";

    /** Valeur permettant d'instancier un objet particulier. */
    private static final Map<Class<?>, Object> prefabValues = new HashMap<>();

    /** Liste des accesseurs à exclure de l'analyse car
     *  ils renvoient une valeur différente de leur attribut. */
    private static final List<String> ignorerAccesseur = new ArrayList<>(); 

    /**
     * Méthode permettant de tester tous les éléments d'un POJO
     */
    @Test
    public void testPojo()
    {
        // Test des constructeurs
        testConstructeurs();

        // Instanciation d'un POJO
        T pojo = newInstance(parameterizedType);

        // Test des mutateurs
        testMutateurs(pojo);
        // Test des accesseurs
        testAccesseurs(pojo);
        // Test de la méthode toString()
        // (le pojo contient alors les valeurs passées dans le test sur les mutateurs)
        testToString(pojo);
    }

    /**
     * Méthode permettant d'identifiant le type paramétré de la classe
     * @return type paramétré de la classe
     */
    @SuppressWarnings("unchecked")
    protected Class<T> getParameterizedType()
    {
        if (parameterizedType == null)
        {
            ParameterizedType localParameterizedType = (ParameterizedType) this.getClass().getGenericSuperclass();
            parameterizedType = (Class<T>) localParameterizedType.getActualTypeArguments()[0];

            LOGGER.debug("Type : {}", parameterizedType);
        }

        return parameterizedType;
    }

    /**
     * Méthode permettant de tester les accesseurs.
     * @param pojo POJO dont on veut tester les accesseurs
     */
    private void testAccesseurs(T pojo)
    {
        LOGGER.trace("Test des accesseurs");

        String nomAccesseur = null;
        try
        {
            // Parcours de toutes les méthodes
            for (Method methode : parameterizedType.getDeclaredMethods())
            {
                // Si c'est un accesseur, vérification de la valeur retournée
                nomAccesseur = methode.getName();
                if (isAccesseur(nomAccesseur))
                {
                    verificationAccesseur(methode, nomAccesseur, pojo);
                }
            }
        }
        catch (IllegalArgumentException iae)
        {
            LOGGER.trace("Il n'y a pas de constucteur vide", iae);
            // Exécution du 1er constructeur trouvé
            appelConstructeur(parameterizedType.getDeclaredConstructors()[0]);
        }
    }

    private boolean isAccesseur(String nomAccesseur)
    {
        return (nomAccesseur.startsWith(PREFIX_ACCESSEUR)
            || nomAccesseur.startsWith(PREFIX_ACCESSEUR_BOOLEEN))
            && !ignorerAccesseur.contains(nomAccesseur);
    }
    
    /**
     * Vérification de l'accesseur.
     *
     * @param methode accesseur
     * @param nomAccesseur nom de la méthode
     * @param pojo objet dont il faut exécuter l'accesseur
     */
    private void verificationAccesseur(Method methode, String nomAccesseur, T pojo)
    {
        // Un accesseur n'a pas de paramètre
        if (methode.getParameterCount() == 0)
        {
            try
            {
                String nomAttribut = getNomAttribut(nomAccesseur);
                // Exécution de l'accesseur et vérification de la valeur à partir de l'attribut associé
                assertEquals("Attribut " + nomAttribut
                    , ReflectionUtils.getValeur(pojo, nomAttribut, parameterizedType)
                    , methode.invoke(pojo));
            }
            catch (IngerableException exception)
            {
                // L'accesseur ne correspond pas à un attribut, cela arrive et n'est pas forcément
                // une erreur
                LOGGER.debug(exception.getMessage(), exception);
            }
            catch (InvocationTargetException | IllegalAccessException exception)
            {
                LOGGER.error("Erreur methode '{}' : {}", nomAccesseur, exception.getMessage(), exception);
                throw new IngerableException(exception);
            }
        }
    }

    /**
     * Méthode permettant de récupérer le nom d'un attribut à partir de sont accesseur.
     *
     * @param nomAccesseur nom de l'accesseur
     * @return le nom de l'attribut correspondant au nom de l'accesseur
     */
    private String getNomAttribut(String nomAccesseur)
    {
        // Appel de la méthode et comparaison avec la valeur du champs
        String attribut = nomAccesseur.replaceFirst(PREFIX_ACCESSEUR, "");
        if (attribut.equals(nomAccesseur))
        {
            attribut = nomAccesseur.replaceFirst(PREFIX_ACCESSEUR_BOOLEEN, "");
        }
        return lowerFirstLetter(attribut);
    }

    /**
     * Méthode permettant de mettre en minuscule la première lettre d'une chaine de caractères.
     *
     * @param chaine chaine à traiter
     * @return chaine modifiée
     */
    private String lowerFirstLetter(String chaine)
    {
        // Méthode la plus efficace pour remplacer la 1ère lettre en minuscule
        // (après mesures de plusieurs algorithmes)
        char[] caracteres = chaine.toCharArray();
        caracteres[0] = Character.toLowerCase(caracteres[0]);
        return new String(caracteres);
    }

    /**
     * Méthode permettant de tester les mutateurs
     */
    private void testMutateurs(T pojo)
    {
        LOGGER.trace("Test des mutateurs");

        String nomMethode = "";
        try
        {
            for (Method methode : parameterizedType.getDeclaredMethods())
            {
                nomMethode = methode.getName();
                if (nomMethode.startsWith(PREFIX_MUTATEUR))
                {
                    Object[] param = new Object[methode.getParameterTypes().length];
                    int indice = 0;
                    for (Class<?> type : methode.getParameterTypes())
                    {
                        param[indice++] = getInstance(type);
                    }
                    methode.invoke(pojo, param);
                }
            }
        }
        catch (InvocationTargetException | IllegalAccessException | IllegalArgumentException exception)
        {
            LOGGER.error("Erreur methode '{}' : {}", nomMethode, exception.getMessage(), exception);
            throw new IngerableException(exception);
        }
    }

    /**
     * Méthode permettant de tester les constructeurs
     */
    private void testConstructeurs()
    {
        LOGGER.trace("Test des constructeurs");
        
        if (!parameterizedType.isEnum())
        {
            String nomConstructeur = "";
            try
            {
                for (Constructor<?> constructeur : parameterizedType.getDeclaredConstructors())
                {
                    appelConstructeur(constructeur);
                }
            }
            catch (SecurityException exception)
            {
                LOGGER.error("Erreur constructeur '{}' : {}", nomConstructeur
                    , exception.getMessage(), exception);
                throw new IngerableException(exception);
            }
        }
    }

    /**
     * Méthode permettant de tester la méthode toString
     */
    private void testToString(T element)
    {
        try
        {
            Method methode = element.getClass().getMethod(TO_STRING);
            if (!parameterizedType.isEnum())
            {
                // Cas des Pojo
                methode.invoke(element);
            }
            else
            {
                // Cas des énumérations
                Object[] enumerations = element.getClass().getEnumConstants();
                for (Object enumeration : enumerations)
                {
                    methode.invoke(enumeration);
                }
            }
        }
        catch (NoSuchMethodException | SecurityException | IllegalAccessException
             | IllegalArgumentException | InvocationTargetException exception)
        {
            LOGGER.error("Erreur appel méthode '{}' : {}", TO_STRING, exception.getMessage(), exception);
            throw new IngerableException(exception);
        }
 
    }
    
    /**
     * Méthode permettant d'instancier un objet via un constructeur
     *
     * @param constructeur  à exécuter
     * @return l'objet instancié
     */
    private Object appelConstructeur(Constructor<?> constructeur)
    {
        Object[] param = null;
        if (constructeur.getParameterTypes().length > 0)
        {
            param = new Object[constructeur.getParameterTypes().length];
        }
        else
        {
            param = new Object[0];
        }
        
        try
        {
            int indice = 0;
            for (Class<?> type : constructeur.getParameterTypes())
            {
                param[indice++] = getInstance(type);
            }
            return constructeur.newInstance(param);
        }
        
        catch (InstantiationException | IllegalAccessException | IllegalArgumentException
            | InvocationTargetException exception)
        {
            LOGGER.error("Erreur constructeur '{}' : {}", constructeur.getName()
                , exception.getMessage(), exception);
            throw new IngerableException(exception);
        }
    }
    
    /**
     * Instanciation du POJO de la classe paramétrée
     *
     * @param classe POJO à instancier
     * @return une instance du POJO
     */
    @SuppressWarnings("unchecked")
    protected T newInstance(Class<T> classe)
    {
        return (T) getInstance(classe);
    }

    /**
     * Méthode permettant de récupérer une instance du POJO
     * 
     * @param classe type du POJO
     * @return Une instance du POJO
     */
    private Object getInstance(Class<?> classe)
    {
        try
        {
            if (prefabValues.containsKey(classe))
            {
                return prefabValues.get(classe);
            }
            if (classe.equals(Integer.TYPE) || classe.equals(Integer.class))
            {
                return 1;
            }
            else if (classe.equals(Long.TYPE) || classe.equals(Long.class))
            {
                return 1l;
            }
            else if (classe.equals(Float.TYPE) || classe.equals(Float.class))
            {
                return 1f;
            }
            else if (classe.equals(Double.TYPE) || classe.equals(Double.class))
            {
                return 1d;
            }
            else if (classe.equals(String.class))
            {
                return "a.a@a.a";
            }
            else if (classe.equals(Character.TYPE) || classe.equals(Character.class))
            {
                return 'a';
            }
            else if (classe.equals(Boolean.TYPE) || classe.equals(Boolean.class))
            {
                return Boolean.TRUE;
            }
            else if (classe.isEnum())
            {
                return classe.getEnumConstants()[0];
            }
            else if (classe.equals(Timestamp.class))
            {
                return new Timestamp(Calendar.getInstance().getTimeInMillis());
            }
            else if (classe.equals(List.class))
            {
                return new ArrayList<Object>();
            }
            else if (classe.equals(Set.class))
            {
                return new HashSet<Object>();
            }
            else if (classe.getComponentType() != null && prefabValues.containsKey(classe.getComponentType()))
            {
                return prefabValues.get(classe.getComponentType());
            }
            return classe.newInstance();
        }
        catch (InstantiationException | IllegalArgumentException iae)
        {
            LOGGER.trace("Il n'y a pas de constucteur sans paramètre", iae);
            // Instanciation du Pojo avec le 1er constructeur trouvé
            return appelConstructeur(parameterizedType.getDeclaredConstructors()[0]);
        }
        catch (IllegalAccessException exception)
        {
            LOGGER.error(exception.getMessage(), exception);
            throw new IngerableException(exception);
        }
    }

    /**
     * Méthode permettant d'ajouter des valeurs préfabriquées, pour être injectées automatiquement
     * dans les tests (mutateurs, constructeurs, etc.).
     *
     * @param type type de la valeur
     * @param objet valeur
     */
    protected void addPrefabValues(Class<?> type, Object objet)
    {
        prefabValues.put(type, objet);
    }

    /**
     * Méthode permettant d'ignorer des accesseurs qui renvoient une valeur différente de leur attribut.
     *
     * @param nomsAccesseusr noms des accesseurs à ignorer lors de l'analyse
     */
    protected void ignorerAccesseurs(String... nomsAccesseurs)
    {
        for (String nomAccesseur : nomsAccesseurs)
        {
            ignorerAccesseur.add(nomAccesseur);
        }
    }

    /**
     * Méthode permettant de tester qu'une méthode exécutée lève bien une exception. 
     *
     * @param exception exception levée par l'exécution de la méthode (comportement attendu)
     * @param executable traitement à exécuter 
     */
    protected void verifierException(Class<? extends Throwable> exceptionAttendue, Executable executable)
    {
        VerificationExecution.verifierException(exceptionAttendue, executable);
    }
}
