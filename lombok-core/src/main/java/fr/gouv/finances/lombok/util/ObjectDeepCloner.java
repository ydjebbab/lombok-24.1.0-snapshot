/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : ObjectDeepCloner.java
 *
 */
package fr.gouv.finances.lombok.util;

import java.io.Serializable;

import org.apache.commons.lang.SerializationUtils;

/**
 * Class ObjectDeepCloner Classe utilitaire permettant de réaliser le clonage d'un graphe d'objet s'appuie sur la
 * fonction de serialization des objets en java Toutes les classes des objets du graphe doivent donc être serialisables
 * (implements serializable).
 * 
 * @author chouard-cp
 * @version $Revision: 1.4 $ Date: 15 déc. 2009
 */
public final class ObjectDeepCloner
{

    /**
     * Retourne un clone de la grappe d'objet.
     * 
     * @param oldObj --
     * @return clone de la grappe d'objet
     */
    public static Object deepCopy(Object oldObj)
    {
        Object newObj;
        if (oldObj == null)
        {
            newObj = null;
        }
        else if (oldObj instanceof Serializable)
        {
            newObj = SerializationUtils.clone((Serializable) oldObj);
        }
        else
        {
            throw new IllegalArgumentException("L'objet passé en paramètre doit être sérialisable");
        }
        return newObj;
    }

    /**
     * Instanciation de object deep cloner.
     */
    private ObjectDeepCloner()
    {
        // Pour éviter que la classe soit instanciée
    }

}
