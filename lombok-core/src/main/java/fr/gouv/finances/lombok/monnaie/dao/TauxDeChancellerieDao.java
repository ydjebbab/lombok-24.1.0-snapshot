/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 */
package fr.gouv.finances.lombok.monnaie.dao;

import fr.gouv.finances.lombok.monnaie.bean.TauxDeChancellerie;
import fr.gouv.finances.lombok.util.base.CoreBaseDao;

/**
 * Interface TauxDeChancellerieDao --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.2 $ Date: 11 déc. 2009
 */
public interface TauxDeChancellerieDao extends CoreBaseDao
{

    /**
     * methode Delete taux chancellerie : --.
     * 
     * @param unTauxDeChancellerie --
     */
    public void deleteTauxChancellerie(TauxDeChancellerie unTauxDeChancellerie);

    /**
     * methode Modify taux chancellerie : --.
     * 
     * @param unTauxDeChancellerie --
     */
    public void modifyTauxChancellerie(TauxDeChancellerie unTauxDeChancellerie);

    /**
     * methode Save taux chancellerie : --.
     * 
     * @param unTauxDeChancellerie --
     */
    public void saveTauxChancellerie(TauxDeChancellerie unTauxDeChancellerie);
}
