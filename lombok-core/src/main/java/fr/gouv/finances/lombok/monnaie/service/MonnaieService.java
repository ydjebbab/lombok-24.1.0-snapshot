/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : MonnaieService.java
 *
 */
package fr.gouv.finances.lombok.monnaie.service;

import java.util.List;

import fr.gouv.finances.lombok.monnaie.bean.Monnaie;
import fr.gouv.finances.lombok.monnaie.bean.TauxDeChancellerie;

/**
 * Interface MonnaieService --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.2 $ Date: 11 déc. 2009
 */
public interface MonnaieService
{

    /**
     * methode Creer monnaie : --.
     * 
     * @param uneMonnaie --
     */
    public void creerMonnaie(Monnaie uneMonnaie);

    /**
     * coriolis *.
     * 
     * @param uneMonnaie --
     */
    public void creerMonnaieEtTaux(Monnaie uneMonnaie);

    /**
     * methode Modifier monnaie : --.
     * 
     * @param uneMonnaie --
     */
    public void modifierMonnaie(Monnaie uneMonnaie);

    /**
     * methode Rechercher dernier taux chancellerie : --.
     * 
     * @param uneMonnaie --
     * @return taux de chancellerie
     */
    public TauxDeChancellerie rechercherDernierTauxChancellerie(Monnaie uneMonnaie);

    /**
     * methode Rechercher monnaie par code : --.
     * 
     * @param code --
     * @return monnaie
     */
    public Monnaie rechercherMonnaieParCode(String code);

    /**
     * methode Rechercher toutes monnaies : --.
     * 
     * @return list
     */
    public List rechercherToutesMonnaies();

    /**
     * methode Supprimer monnaie et taux : --.
     * 
     * @param uneMonnaie --
     */
    public void supprimerMonnaieEtTaux(Monnaie uneMonnaie);

}
