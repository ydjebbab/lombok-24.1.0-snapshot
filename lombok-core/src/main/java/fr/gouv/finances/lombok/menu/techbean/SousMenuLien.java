/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : SousMenuLien.java
 *
 */
package fr.gouv.finances.lombok.menu.techbean;

import java.util.List;

/**
 * Class SousMenuLien Classe représentant un sous menu dans une barre de menu hierarchique (idem "file" ou "edit" dans
 * la barre de menu d'Eclipse) avec un lien direct vers une url (ajout suite à demande caen) Methode retenue pour
 * l'implementation : On fait comme si le sous-menu comportait une seul rubrique lien La difference est faite uniquement
 * à la generation du menu en javascript (permet de gerer les cas ou un futur menu ne supporterait pas cette
 * fonctionnalite).
 * 
 * @author lcontinsouzas-cp
 * @version $Revision: 1.2 $ Date: 15 déc. 2009
 */
public class SousMenuLien extends SousMenu
{

    private static final long serialVersionUID = -8051246913900291485L;

    public SousMenuLien()
    {
        super();  
    }
        
    // Ces deux methodes permettent d'extraire l'url et la target associes au sousmenu lien
    // et stockees dans une rubriquelien dans la liste de rubriques
    public String donnerTargetLien()
    {
        return this.lireRubriqueLienAssociee().target;
    }


    /**
     * methode Donner url lien : --.
     * 
     * @return string
     */
    public String donnerUrlLien()
    {
        return this.lireRubriqueLienAssociee().url;
    }

    /**
     * methode Lire rubrique lien associee : --.
     * 
     * @return rubrique lien
     */
    private RubriqueLien lireRubriqueLienAssociee()
    {
        List listeDeRubriquesLien = this.donnerListedesRubriques(true, true);
        RubriqueLien uneRubriqueLien;

        if (listeDeRubriquesLien == null || listeDeRubriquesLien.size() != 1)
        {
            throw new MenuProgrammationException("Un SousMenuLien doit être associe a une et une seule rubrique lien");
        }
        else
        {
            uneRubriqueLien = (RubriqueLien) listeDeRubriquesLien.get(0);
        }

        return uneRubriqueLien;
    }

}
