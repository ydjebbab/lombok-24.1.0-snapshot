/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 */
package fr.gouv.finances.lombok.securite.techbean;

import fr.gouv.finances.lombok.util.exception.RegleGestionException;

/**
 * Class HabilitationRegleGestionException.
 * 
 * @author lcontinsouzas-cp
 * @version $Revision: 1.2 $ Date: 15 déc. 2009
 */
public class HabilitationRegleGestionException extends RegleGestionException
{

    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /**
     * Instanciation de habilitation regle gestion exception.
     * 
     * @param message --
     */
    public HabilitationRegleGestionException(String message)
    {
        super(message);
    }

    /**
     * Instanciation de habilitation regle gestion exception.
     * 
     * @param message --
     * @param cause --
     */
    public HabilitationRegleGestionException(String message, Throwable cause)
    {
        super(message, cause);
    }

}
