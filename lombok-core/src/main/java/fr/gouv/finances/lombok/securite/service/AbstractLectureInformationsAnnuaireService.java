/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : AbstractLectureInformationsAnnuaireService.java
 *
 */
package fr.gouv.finances.lombok.securite.service;

/**
 * Interface AbstractLectureInformationsAnnuaireService --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.2 $ Date: 11 déc. 2009
 */
public interface AbstractLectureInformationsAnnuaireService
{

    /**
     * Accesseur de l attribut type authentification ldap.
     * 
     * @return type authentification ldap
     */
    public abstract String getTypeAuthentificationLDAP();

    /**
     * Modificateur de l attribut type authentification ldap.
     * 
     * @param typeAuthentificationLDAP le nouveau type authentification ldap
     */
    public abstract void setTypeAuthentificationLDAP(String typeAuthentificationLDAP);

}