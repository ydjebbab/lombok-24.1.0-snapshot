/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : NullorNotComparableComparator.java
 *
 */
package fr.gouv.finances.lombok.util.comparators;

import java.io.Serializable;
import java.util.Comparator;

import org.apache.commons.collections.comparators.ComparableComparator;

/**
 * Class NullorNotComparableComparator Adaptation de la classe NullComparator pour qu'elle accepte des propriétés
 * n'implémentent pas l'interface Comparable.
 * 
 * @author wpetit-cp
 * @version $Revision: 1.2 $ Date: 15 déc. 2009
 */
public class NullorNotComparableComparator implements Comparator, Serializable
{

    /** Serialization version. */
    private static final long serialVersionUID = -5820772575483504339L;

    /** The comparator to use when comparing two non-<code>null</code> objects. */
    private Comparator nonNullComparator;

    /** Specifies whether a <code>null</code> are compared as higher than non-<code>null</code> objects. */
    private boolean nullsAreHigh;

    // -----------------------------------------------------------------------
    /**
     * Construct an instance that sorts <code>null</code> higher than any non-<code>null</code> object it is compared
     * with. When comparing two non-<code>null</code> objects, the {@link ComparableComparator} is used.
     */
    public NullorNotComparableComparator()
    {
        this(ComparableComparator.getInstance(), true);
    }

    /**
     * Construct an instance that sorts <code>null</code> higher or lower than any non-<code>null</code> object it is
     * compared with. When comparing two non-<code>null</code> objects, the {@link ComparableComparator} is used.
     * 
     * @param nullsAreHigh a <code>true</code> value indicates that <code>null</code> should be compared as higher than
     *        a non-<code>null</code> object. A <code>false</code> value indicates that <code>null</code> should be
     *        compared as lower than a non-<code>null</code> object.
     */
    public NullorNotComparableComparator(boolean nullsAreHigh)
    {
        this(ComparableComparator.getInstance(), nullsAreHigh);
    }

    /**
     * Construct an instance that sorts <code>null</code> higher than any non-<code>null</code> object it is compared
     * with. When comparing two non-<code>null</code> objects, the specified {@link Comparator} is used.
     * 
     * @param nonNullComparator the comparator to use when comparing two non-<code>null</code> objects. This argument
     *        cannot be <code>null</code>
     */
    public NullorNotComparableComparator(Comparator nonNullComparator)
    {
        this(nonNullComparator, true);
    }

    /**
     * Construct an instance that sorts <code>null</code> higher or lower than any non-<code>null</code> object it is
     * compared with. When comparing two non-<code>null</code> objects, the specified {@link Comparator} is used.
     * 
     * @param nonNullComparator the comparator to use when comparing two non-<code>null</code> objects. This argument
     *        cannot be <code>null</code>
     * @param nullsAreHigh a <code>true</code> value indicates that <code>null</code> should be compared as higher than
     *        a non-<code>null</code> object. A <code>false</code> value indicates that <code>null</code> should be
     *        compared as lower than a non-<code>null</code> object.
     */
    public NullorNotComparableComparator(Comparator nonNullComparator, boolean nullsAreHigh)
    {
        this.nonNullComparator = nonNullComparator;
        this.nullsAreHigh = nullsAreHigh;

        if (nonNullComparator == null)
        {
            throw new IllegalArgumentException("null nonNullComparator");
        }
    }

    // -----------------------------------------------------------------------
    /**
     * Perform a comparison between two objects. If both objects are <code>null</code>, a <code>0</code> value is
     * returned. If one object is <code>null</code> and the other is not, the result is determined on whether the
     * Comparator was constructed to have nulls as higher or lower than other objects. If neither object is
     * <code>null</code>, an underlying comparator specified in the constructor (or the default) is used to compare the
     * non-<code>null</code> objects.
     * 
     * @param o1 the first object to compare
     * @param o2 the object to compare it to.
     * @return <code>-1</code> if <code>o1</code> is "lower" than (less than, before, etc.) <code>o2</code>;
     *         <code>1</code> if <code>o1</code> is "higher" than (greater than, after, etc.) <code>o2</code>; or
     *         <code>0</code> if <code>o1</code> and <code>o2</code> are equal.
     */
    public int compare(Object o1, Object o2)
    {
        Object o1b = o1;
        Object o2b = o2;

        if (o1 == o2)
        {
            return 0;
        }
        if (o1 == null)
        {
            return (this.nullsAreHigh ? 1 : -1);
        }
        if (o2 == null)
        {
            return (this.nullsAreHigh ? -1 : 1);
        }
        if ((o1 instanceof Comparable == false) || (o2 instanceof Comparable == false))
        {
            o1b = o1.toString();
            o2b = o2.toString();
        }
        return this.nonNullComparator.compare(o1b, o2b);
    }

    /**
     * Determines whether the specified object represents a comparator that is equal to this comparator.
     * 
     * @param obj the object to compare this comparator with.
     * @return <code>true</code> if the specified object is a NullComparator with equivalent <code>null</code>
     *         comparison behavior (i.e. <code>null</code> high or low) and with equivalent underlying non-
     *         <code>null</code> object comparators.
     */
    public boolean equals(Object obj)
    {
        if (obj == null)
        {
            return false;
        }
        if (obj == this)
        {
            return true;
        }
        if (!obj.getClass().equals(this.getClass()))
        {
            return false;
        }

        NullorNotComparableComparator other = (NullorNotComparableComparator) obj;

        return ((this.nullsAreHigh == other.nullsAreHigh) && (this.nonNullComparator.equals(other.nonNullComparator)));
    }

    // -----------------------------------------------------------------------
    /**
     * Implement a hash code for this comparator that is consistent with {@link #equals(Object)}.
     * 
     * @return a hash code for this comparator.
     */
    public int hashCode()
    {
        return (nullsAreHigh ? -1 : 1) * nonNullComparator.hashCode();
    }

}
