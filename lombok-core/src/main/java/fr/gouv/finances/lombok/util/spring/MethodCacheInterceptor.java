/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : MethodCacheInterceptor.java
 *
 */
package fr.gouv.finances.lombok.util.spring;

import java.io.Serializable;

import net.sf.ehcache.Cache;
import net.sf.ehcache.Element;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.util.Assert;

/**
 * Classe MethodCacheInterceptor.java. intercepte les résultats d'une méthode définie dans le contexte spring et la met
 * en cache
 * 
 * @author amleplatinec-cp
 * @version $Revision: 1.2 $
 */
public class MethodCacheInterceptor implements MethodInterceptor, InitializingBean
{

    private static final Log LOGGER = LogFactory.getLog(MethodCacheInterceptor.class);

    private Cache cache;

    public MethodCacheInterceptor()
    {
        super();        
    }

    /**
     * un cache est nécessaire.
     * 
     * @throws Exception the exception
     */
    @Override
    public void afterPropertiesSet() throws Exception
    {
        Assert.notNull(cache, "un cache est requis .utiliser setCache pour en instancier un.");
    }

    /**
     * (methode de remplacement) méthode principale qui va cacher le resultat de la methode s il n existe pas dejà sinon
     * le recupere.
     * 
     * @param invocation --
     * @return the object
     * @throws Throwable the throwable
     * @see org.aopalliance.intercept.MethodInterceptor#invoke(org.aopalliance.intercept.MethodInvocation)
     */
    @Override
    public Object invoke(MethodInvocation invocation) throws Throwable
    {
        String targetName = invocation.getThis().getClass().getName();
        String methodName = invocation.getMethod().getName();
        Object[] arguments = invocation.getArguments();
        Object result;

        if (LOGGER.isDebugEnabled())
        {
            LOGGER.debug("recherche du resultat de la methode dans le cache");
        }
        String cacheKey = getCacheKey(targetName, methodName, arguments);
        Element element = cache.get(cacheKey);

        if (element == null)
        {
            // appel de l'intercepteur
            if (LOGGER.isDebugEnabled())
            {
                LOGGER.debug("element non en cache .interception de l'appel de la methode");
            }
            result = invocation.proceed();

            // resultat mis en cache
            if (LOGGER.isDebugEnabled())
            {
                LOGGER.debug("resultat mis en cache");
            }

            element = new Element(cacheKey, (Serializable) result);
            cache.put(element);
        }
        else
        {
            if (LOGGER.isDebugEnabled())
            {
                LOGGER.debug("le resultat est deja en cache");
            }
        }
        return element.getValue();
    }

    /**
     * sets cache name to be used.
     * 
     * @param cache --
     */
    public void setCache(Cache cache)
    {
        this.cache = cache;
    }

    /**
     * creation de la clé methodeappellanteparchemin+nommethode+arguments.
     * 
     * @param targetName --
     * @param methodName --
     * @param arguments --
     * @return the cache key
     */
    private String getCacheKey(String targetName, String methodName, Object[] arguments)
    {
        StringBuilder   sb = new StringBuilder();
        sb.append(targetName).append('.').append(methodName);
        if ((arguments != null) && (arguments.length != 0))
        {
            for (int i = 0; i < arguments.length; i++)
            {
                sb.append('.').append(arguments[i]);
            }
        }

        return sb.toString();
    }
}