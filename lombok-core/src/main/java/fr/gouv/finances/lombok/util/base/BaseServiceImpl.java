/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : BaseServiceImpl.java
 *
 */
package fr.gouv.finances.lombok.util.base;

import java.util.List;

/**
 * @author amleplatinec
 * @version $Revision: 1.3 $ Date: 11 déc. 2009
 */
public class BaseServiceImpl
{
    public BaseServiceImpl()
    {
        super();
    }

    /**
     * Cette méthode doit être surchargée pour les services où l'on souhaite exclure une ou plusieurs méthodes du
     * contrôle de non-nullité des paramètres d'appel implémenté par l'AOP ParameterControlAdvice.
     * 
     * @return la liste des méthodes à ignorer
     */
    public List<String> getMethodesAIgnorer()
    {
        return null;
    }
}
