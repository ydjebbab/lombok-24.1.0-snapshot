/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 */
package fr.gouv.finances.lombok.util.exception;

import java.util.List;

/**
 * Class RegleGestionInformationException RegleGestionInformationException Sous classe de RegleGestionException qui
 * permet de distinguer les exceptions avec severite minimale.
 * 
 * @see RegleGestionException
 * @author chouard-cp
 * @version $Revision: 1.2 $ Date: 15 déc. 2009
 */
public class RegleGestionInformationException extends RegleGestionException
{

    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /**
     * Instanciation de regle gestion information exception.
     */
    public RegleGestionInformationException()
    {
        super();

    }

    /**
     * Instanciation de regle gestion information exception.
     * 
     * @param listeMessages --
     */
    public RegleGestionInformationException(List<String> listeMessages)
    {
        super(listeMessages);
    }

    /**
     * Instanciation de regle gestion information exception.
     * 
     * @param arg0 --
     */
    public RegleGestionInformationException(String arg0)
    {
        super(arg0);

    }

    /**
     * Instanciation de regle gestion information exception.
     * 
     * @param arg0 --
     * @param arg1 --
     */
    public RegleGestionInformationException(String arg0, Throwable arg1)
    {
        super(arg0, arg1);

    }

    /**
     * Instanciation de regle gestion information exception.
     * 
     * @param arg0 --
     */
    public RegleGestionInformationException(Throwable arg0)
    {
        super(arg0);

    }

}
