/*
 * Copyright (c) 2011 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.srm.service;

import java.util.List;

import fr.gouv.finances.lombok.srm.bean.Message;

/**
 * Classe MessageWebService.java.
 * 
 * @author amleplatinec
 * @version $Revision: 1.2 $
 */
public interface SiremeService
{

    /**
     * recherche des profils désactives sur une application.
     * 
     * @param pLibelleCourtAppli le libelle court appli
     * @return list
     */
    public List<Message> rechercherListeProfilsADesactiverALaDateHeureCourantePourLApplication();

    /**
     * recherche du message bloquant sur l'application.
     * 
     * @param pLibelleCourtAppli le libelle court appli
     * @return message
     */
    public Message rechercherMessageBloquantALaDateHeureCourantePourLApplication();

    /**
     * recherche des messages actuellement actif sur l'application.
     * 
     * @param pLibelleCourtAppli le libelle court appli
     * @return list
     */
    public List<Message> rechercherMessagesALaDateHeureCourantePourLApplication();

}