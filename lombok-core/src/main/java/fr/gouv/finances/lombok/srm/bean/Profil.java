/*
 * Copyright (c) 2011 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.srm.bean;

import java.util.Objects;

import fr.gouv.finances.lombok.util.base.BaseBean;

/**
 * Objet métier Profil.
 * 
 * @author amleplatinec
 * @author Christophe Breheret-Girardin
 */
public class Profil extends BaseBean
{
    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** libelle profil. */
    private String libelleProfil;

    /** code profil. */
    private String codeProfil;

    /** code profil concat code appli. */
    private String codeProfilConcatCodeAppli;

    // La clé métier pour le profil est la concatenation profil + code appli
    // (sinon profil consultation est présent dans plusieurs applications
    /** id. */
    private Long id;

    /** version. */
    private int version;

    /**
     * Constructeur.
     */
    public Profil()
    {
        super();
    }

    /** 
     * {@inheritDoc}
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode()
    {
        return Objects.hash(codeProfilConcatCodeAppli);
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    public boolean equals(Object obj)
    {
        if (obj == null)
        {
            return false;
        }

        if (this.getClass() != obj.getClass())
        {
            return false;
        }

        Profil profil = (Profil) obj;
        return Objects.equals(codeProfilConcatCodeAppli, profil.codeProfilConcatCodeAppli);
    }

    /**
     * Accesseur de l'attribut code profil.
     * 
     * @return code profil
     */
    public String getCodeProfil()
    {
        return codeProfil;
    }

    /**
     * Accesseur de l'attribut code profil concat code appli.
     * 
     * @return code profil concat code appli
     */
    public String getCodeProfilConcatCodeAppli()
    {
        return codeProfilConcatCodeAppli;
    }

    /**
     * Accesseur de l'attribut id.
     * 
     * @return id
     */
    public Long getId()
    {
        return id;
    }

    /**
     * Accesseur de l'attribut libelle profil.
     * 
     * @return libelle profil
     */
    public String getLibelleProfil()
    {
        return libelleProfil;
    }

    /**
     * Accesseur de l'attribut version.
     * 
     * @return version
     */
    public int getVersion()
    {
        return version;
    }

    /**
     * Modificateur de l'attribut code profil.
     * 
     * @param codeProfil le nouveau code profil
     */
    public void setCodeProfil(String codeProfil)
    {
        this.codeProfil = codeProfil;
    }

    /**
     * Modificateur de l'attribut code profil concat code appli.
     * 
     * @param codeProfilConcatCodeAppli le nouveau code profil concat code appli
     */
    public void setCodeProfilConcatCodeAppli(String codeProfilConcatCodeAppli)
    {
        this.codeProfilConcatCodeAppli = codeProfilConcatCodeAppli;
    }

    /**
     * Modificateur de l attribut id.
     * 
     * @param id le nouvel id
     */
    public void setId(Long id)
    {
        this.id = id;
    }

    /**
     * Modificateur de l attribut libelle profil.
     * 
     * @param libelleProfil le nouveau libelle profil
     */
    public void setLibelleProfil(String libelleProfil)
    {
        this.libelleProfil = libelleProfil;
    }

    /**
     * Modificateur de l attribut version.
     * 
     * @param version la nouvelle version
     */
    public void setVersion(int version)
    {
        this.version = version;
    }

}
