/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : StubMelService.java
 *
 */
package fr.gouv.finances.lombok.stubs;

import java.util.Properties;

import javax.mail.Session;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

import fr.gouv.finances.lombok.mel.service.MelService;
import fr.gouv.finances.lombok.mel.techbean.Mel;

/**
 * Class StubMelService.
 * 
 * @author chouard-cp
 * @version $Revision: 1.3 $ Date: 15 déc. 2009
 */
@Component(value="serveurmel")
@ConditionalOnProperty(prefix="appli", name="notenvoimel", havingValue="notenvoimel")
public class StubMelService implements MelService
{

    /**
     * Constructeur de la classe StubMelService.java
     *
     */
    public StubMelService()
    {
        super();  
        
    }

    /*
     * (non-Javadoc)
     * @see fr.gouv.finances.lombok.mel.service.MelService#envoyerMel(fr.gouv.finances.lombok.mel.techbean.Mel)
     */
    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param arg0
     * @see fr.gouv.finances.lombok.mel.service.MelService#envoyerMel(fr.gouv.finances.lombok.mel.techbean.Mel)
     */
    public void envoyerMel(Mel arg0)
    {
        // méthode de stub non remplie
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param mel
     * @see fr.gouv.finances.lombok.mel.service.MelService#envoyerMelAvecContenuHTML(fr.gouv.finances.lombok.mel.techbean.Mel)
     */
    public void envoyerMelAvecContenuHTML(Mel mel)
    {
        // méthode de stub non remplie
    }

    /*
     * (non-Javadoc)
     * @see fr.gouv.finances.lombok.mel.service.MelService#getHost()
     */
    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @return host
     * @see fr.gouv.finances.lombok.mel.service.MelService#getHost()
     */
    public String getHost()
    {

        return null;
    }

    /*
     * (non-Javadoc)
     * @see fr.gouv.finances.lombok.mel.service.MelService#getJavamailproperties()
     */
    /**
     * Accesseur de l attribut javamailproperties.
     * 
     * @return javamailproperties
     */
    public Properties getJavamailproperties()
    {

        return null;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @return java mail properties
     * @see fr.gouv.finances.lombok.mel.service.MelService#getJavaMailProperties()
     */
    public Properties getJavaMailProperties()
    {
        return null;
    }

    /*
     * (non-Javadoc)
     * @see fr.gouv.finances.lombok.mel.service.MelService#getPassword()
     */
    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @return password
     * @see fr.gouv.finances.lombok.mel.service.MelService#getPassword()
     */
    public String getPassword()
    {

        return null;
    }

    /*
     * (non-Javadoc)
     * @see fr.gouv.finances.lombok.mel.service.MelService#getPort()
     */
    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @return port
     * @see fr.gouv.finances.lombok.mel.service.MelService#getPort()
     */
    public int getPort()
    {

        return 0;
    }

    /*
     * (non-Javadoc)
     * @see fr.gouv.finances.lombok.mel.service.MelService#getProtocol()
     */
    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @return protocol
     * @see fr.gouv.finances.lombok.mel.service.MelService#getProtocol()
     */
    public String getProtocol()
    {

        return null;
    }

    /*
     * (non-Javadoc)
     * @see fr.gouv.finances.lombok.mel.service.MelService#getSession()
     */
    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @return session
     * @see fr.gouv.finances.lombok.mel.service.MelService#getSession()
     */
    public Session getSession()
    {

        return null;
    }

    /*
     * (non-Javadoc)
     * @see fr.gouv.finances.lombok.mel.service.MelService#getUsername()
     */
    /**
     * Accesseur de l attribut username.
     * 
     * @return username
     */
    public String getUsername()
    {

        return null;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @return user name
     * @see fr.gouv.finances.lombok.mel.service.MelService#getUserName()
     */
    public String getUserName()
    {
        return null;
    }

    /*
     * (non-Javadoc)
     * @see fr.gouv.finances.lombok.mel.service.MelService#setHost(java.lang.String)
     */
    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param arg0 le nouveau host
     * @see fr.gouv.finances.lombok.mel.service.MelService#setHost(java.lang.String)
     */
    public void setHost(String arg0)
    {
        // méthode de stub non remplie
    }

    /*
     * (non-Javadoc)
     * @see fr.gouv.finances.lombok.mel.service.MelService#setJavamailproperties(java.util.Properties)
     */
    /**
     * Modificateur de l attribut javamailproperties.
     * 
     * @param arg0 le nouveau javamailproperties
     */
    public void setJavamailproperties(Properties arg0)
    {
        // méthode de stub non remplie
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param javamailproperties le nouveau java mail properties
     * @see fr.gouv.finances.lombok.mel.service.MelService#setJavaMailProperties(java.util.Properties)
     */
    public void setJavaMailProperties(Properties javamailproperties)
    {
        // méthode de stub non remplie
    }

    /*
     * (non-Javadoc)
     * @see fr.gouv.finances.lombok.mel.service.MelService#setPassword(java.lang.String)
     */
    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param arg0 le nouveau password
     * @see fr.gouv.finances.lombok.mel.service.MelService#setPassword(java.lang.String)
     */
    public void setPassword(String arg0)
    {
        // méthode de stub non remplie
    }

    /*
     * (non-Javadoc)
     * @see fr.gouv.finances.lombok.mel.service.MelService#setPort(int)
     */
    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param arg0 le nouveau port
     * @see fr.gouv.finances.lombok.mel.service.MelService#setPort(int)
     */
    public void setPort(int arg0)
    {
        // méthode de stub non remplie
    }

    /*
     * (non-Javadoc)
     * @see fr.gouv.finances.lombok.mel.service.MelService#setProtocol(java.lang.String)
     */
    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param arg0 le nouveau protocol
     * @see fr.gouv.finances.lombok.mel.service.MelService#setProtocol(java.lang.String)
     */
    public void setProtocol(String arg0)
    {
        // méthode de stub non remplie
    }

    /*
     * (non-Javadoc)
     * @see fr.gouv.finances.lombok.mel.service.MelService#setSession(javax.mail.Session)
     */
    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param arg0 le nouveau session
     * @see fr.gouv.finances.lombok.mel.service.MelService#setSession(javax.mail.Session)
     */
    public void setSession(Session arg0)
    {
        // méthode de stub non remplie
    }

    /*
     * (non-Javadoc)
     * @see fr.gouv.finances.lombok.mel.service.MelService#setUsername(java.lang.String)
     */
    /**
     * Modificateur de l attribut username.
     * 
     * @param arg0 le nouveau username
     */
    public void setUsername(String arg0)
    {
        // méthode de stub non remplie
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param username le nouveau user name
     * @see fr.gouv.finances.lombok.mel.service.MelService#setUserName(java.lang.String)
     */
    public void setUserName(String username)
    {
        // méthode de stub non remplie
    }

}
