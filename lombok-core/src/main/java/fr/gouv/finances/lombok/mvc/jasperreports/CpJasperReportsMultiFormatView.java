/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : CpJasperReportsMultiFormatView.java
 *
 */
package fr.gouv.finances.lombok.mvc.jasperreports;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JasperPrint;

import org.springframework.context.support.MessageSourceResourceBundle;
import org.springframework.web.servlet.support.RequestContextUtils;
import org.springframework.web.servlet.view.jasperreports.AbstractJasperReportsView;
import org.springframework.web.servlet.view.jasperreports.JasperReportsMultiFormatView;

/**
 * Class CpJasperReportsMultiFormatView --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.4 $ Date: 11 déc. 2009
 */
public class CpJasperReportsMultiFormatView extends JasperReportsMultiFormatView

{
    private final Map<String, Class<? extends AbstractJasperReportsView>> formatMappings1 = new HashMap<>();

    /**
     * Instanciation de cp jasper reports multi format view.
     */
    public CpJasperReportsMultiFormatView()
    {
        super();
        this.formatMappings1.put("csv", org.springframework.web.servlet.view.jasperreports.JasperReportsCsvView.class);
        this.formatMappings1.put("xls", org.springframework.web.servlet.view.jasperreports.JasperReportsXlsView.class);
        this.formatMappings1.put("jxl", fr.gouv.finances.lombok.mvc.jasperreports.JasperReportsJxlView.class);
        this.formatMappings1.put("pdf", fr.gouv.finances.lombok.mvc.jasperreports.CpJasperReportsPdfView.class);
        this.formatMappings1.put("rtf", fr.gouv.finances.lombok.mvc.jasperreports.CpJasperReportsRtfView.class);
        this.formatMappings1.put("odt", fr.gouv.finances.lombok.mvc.jasperreports.CpJasperReportsOdtView.class);
        this.formatMappings1.put("png", fr.gouv.finances.lombok.mvc.jasperreports.CpJasperReportsPngView.class);
        this.formatMappings1.put("svg", fr.gouv.finances.lombok.mvc.jasperreports.CpJasperReportsSvgView.class);
        this.formatMappings1.put("cpcsv", org.springframework.web.servlet.view.jasperreports.JasperReportsCsvView.class);
        this.formatMappings1.put("ods", fr.gouv.finances.lombok.mvc.jasperreports.CpJasperReportsOdsView.class);
        super.setFormatMappings(formatMappings1);
    }

    /**
     * Prepares the view given the specified model, merging it with static attributes and a RequestContext attribute, if
     * necessary. Delegates to renderMergedOutputModel for the actual rendering.
     * 
     * @param model --
     * @param request --
     * @param response --
     * @throws Exception the exception
     * @see #renderMergedOutputModel
     */
    @SuppressWarnings("unchecked")
    @Override
    public void render(Map model, HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        if (logger.isDebugEnabled())
        {
            logger.debug("Rendering view with name '" + this.getBeanName() + "' with model " + model
                + " and static attributes " + this.getStaticAttributes());
        }

        // Consolidate static and dynamic model attributes.
        Map mergedModel = new HashMap(this.getStaticAttributes().size() + (model != null ? model.size() : 0));

        mergedModel.putAll(this.getStaticAttributes());
        if (model != null)
        {
            mergedModel.putAll(model);
        }

        // Expose RequestContext?
        if (this.getRequestContextAttribute() != null)
        {
            mergedModel.put(this.getRequestContextAttribute(), createRequestContext(request, response, mergedModel));
        }

        renderMergedOutputModel(mergedModel, request, response);

        if (model != null)
        {
            model.put(CpJasperReportsHtmlView.JASPER_PRINT_KEY, mergedModel.get(CpJasperReportsHtmlView.JASPER_PRINT_KEY));
        }
    }

    /*
     * Correction suite évolution spring 2.5 sur AbstractJasperReportsView surcharge de la méthode qui impose d'avoir un
     * contexte web de type WebApplicationContext ce qui empèche le déclenchement par batch des éditions jasper
     */
    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param model
     * @param request
     * @see org.springframework.web.servlet.view.jasperreports.AbstractJasperReportsView#exposeLocalizationContext(java.util.Map,
     *      javax.servlet.http.HttpServletRequest)
     */
    @Override
    protected void exposeLocalizationContext(Map model, HttpServletRequest request)
    {
        /*
         * code spring 2.5.4 initial RequestContext rc = new RequestContext(request, getServletContext());
         * model.put(JRParameter.REPORT_LOCALE, rc.getLocale()); JasperReport report = getReport(); if (report == null
         * || report.getResourceBundle() == null) { model.put(JRParameter.REPORT_RESOURCE_BUNDLE, new
         * MessageSourceResourceBundle(rc.getMessageSource(), rc.getLocale())); }
         */

        /* code de remplacement (copie du code spring 2.0.4) */
        Locale locale = RequestContextUtils.getLocale(request);
        model.put(JRParameter.REPORT_LOCALE, locale);
        if (this.getReport().getResourceBundle() == null)
        {
            ResourceBundle bundle = new MessageSourceResourceBundle(getApplicationContext(), locale);
            model.put(JRParameter.REPORT_RESOURCE_BUNDLE, bundle);
        }

        /* fin du code de remplacement */

    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @return true, si c'est context required
     * @see org.springframework.web.context.support.WebApplicationObjectSupport#isContextRequired()
     */
    @Override
    protected boolean isContextRequired()
    {
        // suite bug lancement en batch de Jasper avec Spring 2.5+
        return true;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param populatedReport
     * @param model
     * @throws Exception the exception
     * @see org.springframework.web.servlet.view.jasperreports.AbstractJasperReportsView#postProcessReport
     * (net.sf.jasperreports.engine.JasperPrint,java.util.Map)
     */
    @SuppressWarnings("unchecked")
    @Override
    protected void postProcessReport(JasperPrint populatedReport, Map model) throws Exception
    {
        model.put(CpJasperReportsHtmlView.JASPER_PRINT_KEY, populatedReport);
    }

}
