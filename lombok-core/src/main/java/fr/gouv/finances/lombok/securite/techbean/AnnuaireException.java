/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 */
package fr.gouv.finances.lombok.securite.techbean;

import fr.gouv.finances.lombok.util.exception.ProgrammationException;

/**
 * Class AnnuaireException --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.2 $ Date: 11 déc. 2009
 */
public class AnnuaireException extends ProgrammationException
{

    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /**
     * Instanciation de annuaire exception.
     */
    public AnnuaireException()
    {
        super();
    }

    /**
     * Instanciation de annuaire exception.
     * 
     * @param arg0 --
     */
    public AnnuaireException(String arg0)
    {
        super(arg0);
    }

    /**
     * Instanciation de annuaire exception.
     * 
     * @param arg0 --
     * @param arg1 --
     */
    public AnnuaireException(String arg0, Throwable arg1)
    {
        super(arg0, arg1);
    }

    /**
     * Instanciation de annuaire exception.
     * 
     * @param arg0 --
     */
    public AnnuaireException(Throwable arg0)
    {
        super(arg0);
    }

}
