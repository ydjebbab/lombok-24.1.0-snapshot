/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : FichierJointService.java
 *
 */
package fr.gouv.finances.lombok.upload.service;

import java.util.Collection;

import fr.gouv.finances.lombok.upload.bean.FichierJoint;

/**
 * Interface FichierJointService --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.2 $ Date: 11 déc. 2009
 */
public interface FichierJointService
{

    /**
     * Accesseur de l attribut uploadmaxsize.
     * 
     * @return uploadmaxsize
     */
    public Long getUploadmaxsize();

    /**
     * methode Rechercher un fichier joint et son contenu par id : --.
     * 
     * @param id --
     * @return fichier joint
     */
    public FichierJoint rechercherUnFichierJointEtSonContenuParId(Long id);

    /**
     * methode Sauvegarder un fichier joint : --.
     * 
     * @param unFichierJoint --
     */
    public void sauvegarderUnFichierJoint(FichierJoint unFichierJoint);

    /**
     * methode Supprimer les fichiers joints : --.
     * 
     * @param fichiersASupprimer --
     */
    public void supprimerLesFichiersJoints(Collection fichiersASupprimer);

    /**
     * methode Supprimer un fichier joint : --.
     * 
     * @param unFichierJoint --
     */
    public void supprimerUnFichierJoint(FichierJoint unFichierJoint);
}
