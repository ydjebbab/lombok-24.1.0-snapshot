/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : AffichageCaracteristiquesUtilisateurForm.java
 *
 */
package fr.gouv.finances.lombok.utilisateur.mvc.form;

import java.io.Serializable;

import fr.gouv.finances.lombok.securite.techbean.HabilitationAnnuaire;
import fr.gouv.finances.lombok.securite.techbean.PersonneAnnuaire;

/**
 * Class AffichageCaracteristiquesUtilisateurForm --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.2 $ Date: 11 déc. 2009
 */
public class AffichageCaracteristiquesUtilisateurForm implements Serializable
{

    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** une personne annuaire. */
    private PersonneAnnuaire unePersonneAnnuaire;

    /** une habilitation. */
    private HabilitationAnnuaire uneHabilitation;

    /**
     * Instanciation de affichage caracteristiques utilisateur form.
     */
    public AffichageCaracteristiquesUtilisateurForm()
    {
        super();
    }

    /**
     * Accesseur de l attribut une habilitation.
     * 
     * @return une habilitation
     */
    public HabilitationAnnuaire getUneHabilitation()
    {
        return uneHabilitation;
    }

    /**
     * Accesseur de l attribut une personne annuaire.
     * 
     * @return une personne annuaire
     */
    public PersonneAnnuaire getUnePersonneAnnuaire()
    {
        return unePersonneAnnuaire;
    }

    /**
     * Modificateur de l attribut une habilitation.
     * 
     * @param uneHabilitation le nouveau une habilitation
     */
    public void setUneHabilitation(HabilitationAnnuaire uneHabilitation)
    {
        this.uneHabilitation = uneHabilitation;
    }

    /**
     * Modificateur de l attribut une personne annuaire.
     * 
     * @param unePersonneAnnuaire le nouveau une personne annuaire
     */
    public void setUnePersonneAnnuaire(PersonneAnnuaire unePersonneAnnuaire)
    {
        this.unePersonneAnnuaire = unePersonneAnnuaire;
    }

}
