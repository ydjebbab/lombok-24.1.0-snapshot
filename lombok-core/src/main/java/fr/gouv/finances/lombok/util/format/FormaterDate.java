/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : FormaterDate.java
 *
 */
package fr.gouv.finances.lombok.util.format;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Class FormaterDate FormaterDate.
 * 
 * @author amleplatinec
 * @version $Revision: 1.2 $ Date: 14 déc. 2009
 */
public final class FormaterDate
{

    /**
     * Instanciation de formater date.
     */
    private FormaterDate()
    {
        // Constructeur privé vide. Toutes les méthodes de la classe sont statiques
    }

    /**
     * Enum TypeFormatDate TypeFormatDate.
     * 
     * @author amleplatinec
     * @version $Revision: 1.2 $ Date: 14 déc. 2009
     */
    public enum TypeFormatDate
    {

        /** F_ d a_ abrege. */
        F_DA_ABREGE,

        /** F_ d a_ h e_ ct. */
        F_DA_HE_CT,

        /** F_ d a_ h e_ m i_ ct. */
        F_DA_HE_MI_CT,

        /** F_ d a_ h e_ m i_ s e_ ct. */
        F_DA_HE_MI_SE_CT,

        /** F_ d a_ h e_ m i_ s e_ mi l_ ct. */
        F_DA_HE_MI_SE_MIL_CT,

        /** F_ d a_ long. */
        F_DA_LONG,

        /** F_ dat e_ ct. */
        F_DATE_CT,

        /** F_ h e_ mi. */
        F_HE_MI,

        /** F_ h e_ m i_ se. */
        F_HE_MI_SE,

        /** F_ heure. */
        F_HEURE;

        /**
         * Accesseur de l attribut pattern.
         * 
         * @param typeFormatDate typeFormatDate
         * @return pattern
         */
        public static String getPattern(TypeFormatDate typeFormatDate)
        {
            String pattern = "dd/MM/yyyy";
            switch (typeFormatDate)
            {
                case F_DA_ABREGE:
                    pattern = "E d MMM yyyy";
                    break;
                case F_DA_HE_CT:
                    pattern = "dd/MM/yyyy HH";
                    break;
                case F_DA_HE_MI_CT:
                    pattern = "dd/MM/yyyy HH:mm";
                    break;
                case F_DA_HE_MI_SE_CT:
                    pattern = "dd/MM/yyyy HH:mm:ss";
                    break;
                case F_DA_HE_MI_SE_MIL_CT:
                    pattern = "dd/MM/yyyy HH:mm:ss:S";
                    break;
                case F_DA_LONG:
                    pattern = "EEEE d MMMM yyyy";
                    break;
                case F_DATE_CT:
                    pattern = "dd/MM/yyyy";
                    break;
                case F_HE_MI:
                    pattern = "HH:mm";
                    break;
                case F_HE_MI_SE:
                    pattern = "HH:mm:SS";
                    break;
                case F_HEURE:
                    pattern = "HH";
                    break;
            }

            return pattern;
        }

    }

    /** Constant : F_DA_ABREGE. */
    public static final String F_DA_ABREGE = "E d MMM yyyy";

    /** Constant : F_DA_HE_CT. */
    public static final String F_DA_HE_CT = "dd/MM/yyyy HH";

    /** Constant : F_DA_HE_MI_CT. */
    public static final String F_DA_HE_MI_CT = "dd/MM/yyyy HH:mm";

    /** Constant : F_DA_HE_MI_SE_CT. */
    public static final String F_DA_HE_MI_SE_CT = "dd/MM/yyyy HH:mm:ss";

    /** Constant : F_DA_HE_MI_SE_MIL_CT. */
    public static final String F_DA_HE_MI_SE_MIL_CT = "dd/MM/yyyy HH:mm:ss:S";

    /** Constant : F_DA_LONG. */
    public static final String F_DA_LONG = "EEEE d MMMM yyyy";

    /** Constant : F_DATE_CT. */
    public static final String F_DATE_CT = "dd/MM/yyyy";

    /** Constant : F_HE_MI. */
    public static final String F_HE_MI = "HH:mm";

    /** Constant : F_HE_MI_SE. */
    public static final String F_HE_MI_SE = "HH:mm:SS";

    /** Constant : F_HEURE. */
    public static final String F_HEURE = "HH";

    /**
     * Formate une date sous une forme abrégée (ex: lun 2 oct. 2006)
     * 
     * @param value value
     * @return the string
     */
    public static String formatDateAbrege(final Date value)
    {
        return (value == null ? "" : getFormatDate(TypeFormatDate.getPattern(TypeFormatDate.F_DA_ABREGE)).format(value));
    }

    /**
     * Formate une date sous la forme JJ/MM/AAAA.
     * 
     * @param value value
     * @return the string
     */
    public static String formatDateCourt(final Date value)
    {
        return (value == null ? "" : getFormatDate(TypeFormatDate.getPattern(TypeFormatDate.F_DATE_CT)).format(value));
    }

    /**
     * Formate la date et l'heure sous la forme JJ/MM/AAAA HH.
     * 
     * @param value value
     * @return the string
     */
    public static String formatDateHeureCourt(final Date value)
    {
        return (value == null ? "" : getFormatDate(TypeFormatDate.getPattern(TypeFormatDate.F_DA_HE_CT)).format(value));
    }

    /**
     * Formate la date, l'heure et les minutes sous la forme JJ/MM/AAAA HH:MM.
     * 
     * @param value value
     * @return the string
     */
    public static String formatDateHeureMinuteCourt(final Date value)
    {
        return (value == null ? "" : getFormatDate(TypeFormatDate.getPattern(TypeFormatDate.F_DA_HE_MI_CT)).format(
            value));
    }

    /**
     * Formate la date, l'heure, les minutes et les secondes sous la forme JJ/MM/AAAA HH:MM:SS.
     * 
     * @param value value
     * @return the string
     */
    public static String formatDateHeureMinuteSecondeCourt(final Date value)
    {
        return (value == null ? "" : getFormatDate(TypeFormatDate.getPattern(TypeFormatDate.F_DA_HE_MI_SE_CT)).format(
            value));
    }

    /**
     * Formate la date, l'heure, les minutes,les secondes, les millisecondes sous la forme JJ/MM/AAAA HH:MM:SS:MILLI.
     * 
     * @param value value
     * @return the string
     */
    public static String formatDateHeureMinuteSecondeMillisecondeCourt(final Date value)
    {
        return (value == null ? "" : getFormatDate(TypeFormatDate.getPattern(TypeFormatDate.F_DA_HE_MI_SE_MIL_CT))
            .format(value));
    }

    /**
     * Formate une date sous une forme longue (ex: lundi 2 octobre 2006).
     * 
     * @param value value
     * @return the string
     */
    public static String formatDateLong(final Date value)
    {
        return (value == null ? "" : getFormatDate(TypeFormatDate.getPattern(TypeFormatDate.F_DA_LONG)).format(value));
    }

    /**
     * Formate une date sous la forme HH.
     * 
     * @param value value
     * @return the string
     */
    public static String formatHeure(final Date value)
    {
        return (value == null ? "" : getFormatDate(TypeFormatDate.getPattern(TypeFormatDate.F_HEURE)).format(value));
    }

    /**
     * Formate une date sous la forme HH:MM.
     * 
     * @param value value
     * @return the string
     */
    public static String formatHeureMinute(final Date value)
    {
        return (value == null ? "" : getFormatDate(TypeFormatDate.getPattern(TypeFormatDate.F_HE_MI)).format(value));
    }

    /**
     * Formate une date sous la forme HH:MM:SS.
     * 
     * @param value value
     * @return the string
     */
    public static String formatHeureMinuteSeconde(final Date value)
    {
        return (value == null ? "" : getFormatDate(TypeFormatDate.getPattern(TypeFormatDate.F_HE_MI_SE)).format(value));
    }

    /**
     * Gets the format date.
     * 
     * @param format value
     * @return the format date
     */
    public static DateFormat getFormatDate(final String format)
    {
        DateFormat unDateFormat = new SimpleDateFormat(format, Locale.FRANCE);
        unDateFormat.setLenient(false);

        return unDateFormat;
    }

}
