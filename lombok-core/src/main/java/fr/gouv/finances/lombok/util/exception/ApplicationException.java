/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : ApplicationException.java
 *
 */
package fr.gouv.finances.lombok.util.exception;

import java.util.ArrayList;
import java.util.List;

/**
 * Class ApplicationException : Exception generique lombok.
 * 
 * @author chouard-cp
 * @version $Revision: 1.3 $ Date: 15 déc. 2009
 */
public abstract class ApplicationException extends RuntimeException
{

    private static final long serialVersionUID = 1L;
    
    /**
     * Cette classe interne permet, à partir d'une liste de chaîne, de constituer une
     * chaîne unique correspondant à la concaténation de l'ensemble des chaînes de la liste. Concrètement, elle permet,
     * à partir d'une liste de messages (attribut listeMessages) de constituer une chaîne pouvant être passée en entrée
     * du constructeur de la classe mère RuntimeException.
     * 
     * @author ageffroy2-cp
     * @version $Revision: 1.3 $
     */
    private static class ListeUtil
    {

        /**
         * methode Concatener liste : --.
         * 
         * @param liste --
         * @return string
         */
        public static String concatenerListe(List<String> liste)
        {
            StringBuilder   buffer = new StringBuilder();
            for (String chaine : liste)
            {
                buffer.append(chaine);
            }
            return buffer.toString();
        }
    }


    /** liste messages. */
    private List<String> listeMessages = new ArrayList<>();

    /**
     * Instanciation de application exception.
     */
    public ApplicationException()
    {
        super();

    }

    /**
     * Instanciation de application exception.
     * 
     * @param listeMessages --
     */
    public ApplicationException(List<String> listeMessages)
    {
        super(ListeUtil.concatenerListe(listeMessages));
        this.listeMessages = listeMessages;
    }

    /**
     * Constructeur de la classe ApplicationException.java on ajoute un message arg0 qui se retrouvera en premier dans
     * la liste des messages
     * 
     * @param listeMessages
     * @param arg0
     */
    public ApplicationException(List<String> listeMessages, String arg0)
    {
        super(ListeUtil.concatenerListe(listeMessages));
        listeMessages.add(0, arg0);
        this.listeMessages = listeMessages;
    }

    /**
     * Instanciation de application exception.
     * 
     * @param arg0 --
     */
    public ApplicationException(String arg0)
    {
        super(arg0);

    }

    /**
     * Instanciation de application exception.
     * 
     * @param arg0 --
     * @param arg1 --
     */
    public ApplicationException(String arg0, Throwable arg1)
    {
        super(arg0, arg1);

    }

    /**
     * Instanciation de application exception.
     * 
     * @param arg0 --
     */
    public ApplicationException(Throwable arg0)
    {
        super(arg0);
    }

    /**
     * Accesseur de l attribut messages.
     * 
     * @return messages
     */
    public List<String> getMessages()
    {
        return listeMessages;
    }

}
