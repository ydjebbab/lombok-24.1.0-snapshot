/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : FichierUtil.java
 *
 */
package fr.gouv.finances.lombok.test;

import java.util.StringTokenizer;

/**
 * Class FichierUtil Cette classe fournit des méthodes facilitant l'utilisation de fichiers utilisés lors de la
 * réalisation de tests unitaires.
 * 
 * @version $Revision: 1.2 $ Date: 14 déc. 2009
 */
public class FichierUtil
{

    private FichierUtil()
    {
        // RAS
    }
    
    /**
     * Cette méthode permet de retourner le répertoire contenant de compilation du projet.
     * 
     * @return Le chemin de compilation du projet
     */
    public static String recupererClassPath()
    {
        String classpath = System.getProperty("java.class.path");
        String separator = System.getProperty("path.separator");
        StringTokenizer st = new StringTokenizer(classpath, separator);

        return st.nextToken();
    }

 

}
