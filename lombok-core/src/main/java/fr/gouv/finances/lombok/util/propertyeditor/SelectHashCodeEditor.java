/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : SelectHashCodeEditor.java
 *
 */
package fr.gouv.finances.lombok.util.propertyeditor;

import java.beans.PropertyEditorSupport;
import java.text.MessageFormat;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import fr.gouv.finances.lombok.util.exception.ProgrammationException;

/**
 * Class SelectHashCodeEditor 
 * 
 * @author amleplatinec
 * @version $Revision: 1.2 $ Date: 11 déc. 2009
 */
public class SelectHashCodeEditor extends PropertyEditorSupport
{

    /**
     * Enum TypeStockageDesObjets
     * 
     * @author amleplatinec
     * @version $Revision: 1.2 $ Date: 11 déc. 2009
     */
    private enum TypeStockageDesObjets
    {

        /** COLLECTION. */
        COLLECTION,

        /** MAP. */
        MAP
    }

    /** Constant : INVALID_PROPERTY. */
    public static final String INVALID_PROPERTY = "La propriété : {0} n''est pas valide dans la classe {1}";

    /** Constant : ALL_NULL. */
    public static final String ALL_NULL = "Tous les objets de la collection sont null";

    /** Constant : METHOD_NULL. */
    public static final String METHOD_NULL = "Impossible de déterminer le nom de la méthode.";

    /** Constant : INCOHERENCE. */
    public static final String INCOHERENCE =
        "La collection utilisée est incohérente, elle contient plusieurs éléments pour lesquels la valeur de {0}.toString() = {1}";

    /** Constant : COLLECTION_NULL. */
    public static final String COLLECTION_NULL = "La collection utilisée n''est pas instanciée";

    /** Constant : TEXTE_NON_NUMERIQUE. */
    public static final String TEXTE_NON_NUMERIQUE = "Le hashcode doit être un entier";

    /** Constant : MAP_NULL. */
    public static final String MAP_NULL = "La Map utilisée n'est pas instanciée";

    /** Constant : NULL_VALUE. */
    public static final String NULL_VALUE = "NULL";

    /** Constant : log. */
    private static final Log log = LogFactory.getLog(SelectHashCodeEditor.class);;

    /** type stockage items. */
    private final TypeStockageDesObjets typeStockageItems;

    /** items. */
    private final Object items;

    /** defaut item value. */
    private final String defautItemValue;

    /**
     * Créé une nouvelle instance de l'objet SelectEditor, en utilisant la liste d'items passée en paramètres.
     * Utilisable avec le tag app:select (ex : <app:select itemslist="${listeimpots}" value="codeImpot"
     * label="nomImpot"/>)
     * 
     * @param items liste des objets utilisés pour construire la liste déroulante (liste stockée dans le contexte sous
     *        le nom indiqué dans l'attribut "itemslist" (ex : listeimpots)
     */
    public SelectHashCodeEditor(List items)
    {
        super();
        this.typeStockageItems = TypeStockageDesObjets.COLLECTION;
        this.items = items;
        this.defautItemValue = SelectEditor.NULL_VALUE;
    }

    /**
     * Créé une nouvelle instance de l'objet SelectEditor, en utilisant la liste d'items passée en paramètres.
     * Utilisable avec le tag app:select (ex : <app:select itemslist="${listeimpots}" value="codeImpot" label="nomImpot"
     * defautitemvalue="-999999"/>)
     * 
     * @param items liste des objets utilisés pour construire la liste déroulante (liste stockée dans le contexte sous
     *        le nom indiqué dans l'attribut "itemslist" (ex : listeimpots)
     * @param defautItemValue String qui représente un objet null dans la liste déroulante defautitemvalue, correspond à
     *        la valeur affectée à l'attribut defautItemValue du tag select (Par défaut -999999)
     */
    public SelectHashCodeEditor(List items, String defautItemValue)
    {
        super();
        this.typeStockageItems = TypeStockageDesObjets.COLLECTION;
        this.items = items;
        this.defautItemValue = defautItemValue;
    }

    /**
     * Créé une nouvelle instance de l'objet SelectEditor, en utilisant la Map d'items passée en paramètres. Utilisable
     * avec le tag app:select (ex : <app:select itemsmap="${mapimpots}" value="codeImpot" label="nomImpot"/>)
     * 
     * @param items Map des objets utilisés pour construire la liste déroulante (Map stockée dans le contexte sous le
     *        nom indiqué dans l'attribut "itemsmap" (ex : mapImpots)
     */
    public SelectHashCodeEditor(Map items)
    {
        super();
        this.typeStockageItems = TypeStockageDesObjets.MAP;
        this.items = items;
        this.defautItemValue = SelectEditor.NULL_VALUE;
    }

    /**
     * Créé une nouvelle instance de l'objet SelectEditor, en utilisant la Map d'items passée en paramètres. Utilisable
     * avec le tag app:select (ex : <app:select itemsmap="${mapimpots}" value="codeImpot" label="nomImpot"/>)
     * 
     * @param items Map des objets utilisés pour construire la liste déroulante (Map stockée dans le contexte sous le
     *        nom indiqué dans l'attribut "itemsmap" (ex : mapImpots)
     * @param defautItemValue String qui représente un objet null dans la liste déroulante defautitemvalue, correspond à
     *        la valeur affectée à l'attribut defautItemValue du tag select (Par défaut -999999)
     */
    public SelectHashCodeEditor(Map items, String defautItemValue)
    {
        super();
        this.typeStockageItems = TypeStockageDesObjets.MAP;
        this.items = items;
        this.defautItemValue = defautItemValue;
    }

    /**
     * Créé une nouvelle instance de l'objet SelectEditor, en utilisant le Set d'items passé en paramètres. Utilisable
     * avec le tag app:select (ex : <app:select itemsset="${setimpots}" value="codeImpot" label="nomImpot"/>)
     * 
     * @param items Map des objets utilisés pour construire la liste déroulante (Map stockée dans le contexte sous le
     *        nom indiqué dans l'attribut "itemsset" (ex : setImpots)
     * @param valueProperty --
     */
    public SelectHashCodeEditor(Set items, String valueProperty)
    {
        super();
        this.typeStockageItems = TypeStockageDesObjets.COLLECTION;
        this.items = items;
        this.defautItemValue = SelectEditor.NULL_VALUE;
    }

    /**
     * Créé une nouvelle instance de l'objet SelectEditor, en utilisant le Set d'items passé en paramètres. Utilisable
     * avec le tag app:select (ex : <app:select itemsset="${setimpots}" value="codeImpot" label="nomImpot"/>)
     * 
     * @param items Set des objets utilisés pour construire la liste déroulante (Map stockée dans le contexte sous le
     *        nom indiqué dans l'attribut "itemsset" (ex : setImpots)
     * @param valueProperty --
     * @param defautItemValue String qui représente un objet null dans la liste déroulante defautitemvalue, correspond à
     *        la valeur affectée à l'attribut defautItemValue du tag select (Par défaut -999999)
     */
    public SelectHashCodeEditor(Set items, String valueProperty, String defautItemValue)
    {
        super();
        this.typeStockageItems = TypeStockageDesObjets.COLLECTION;
        this.items = items;
        this.defautItemValue = defautItemValue;
    }

    /**
     * Retourne la chaîne de caractères qui représente l'objet.
     * 
     * @return the as text
     */
    @Override
    public String getAsText()
    {
        String result;
        Object unObjet = getValue();

        // L'objet est null
        if (unObjet == null)
        {
            result = this.defautItemValue;
        }
        else
        {
            result = Integer.toString(unObjet.hashCode());
        }

        if (log.isDebugEnabled())
        {
            if (unObjet == null)
            {
                log.debug("la valeur nulle est représentée par la chaîne : " + result);
            }
            else
            {
                log.debug("L'Objet : " + unObjet.toString() + " est représenté par la chaîne : " + result);
            }
        }
        return result;
    }

    /**
     * Retourne l'objet de la collection (ou de la Map d'objet) qui est identifié par le String text.
     * 
     * @param text --
     * @throws IllegalArgumentException the illegal argument exception
     */
    @Override
    public void setAsText(final String text) throws IllegalArgumentException
    {
        Object unObjet = null;

        if (log.isDebugEnabled())
        {
            log.debug("Text = " + text);
        }

        switch (this.typeStockageItems)
        {
            case COLLECTION:
                unObjet = findObjectInCollection((Collection) this.items, text);
                break;
            case MAP:
                unObjet = findObjectInMap((Map) this.items, text);
                break;
            default:
        }

        if (log.isDebugEnabled())
        {
            if (unObjet == null)
            {
                log.debug("Object trouvé = null");
            }
            else
            {
                log.debug("Object trouvé = " + unObjet.toString());
            }
        }

        this.setValue(unObjet);
    }

    /**
     * Recherche un objet dans une collection en utilisant son hashCode.
     * 
     * @param uneCollection collection dans laquelle l'objet est recherché
     * @param text String qui identifie un objet dans la collection
     * @return l'objet sélectionné ou null si aucun objet ne correspond à la clé dans la collection
     */
    private Object findObjectInCollection(final Collection uneCollection, final String text)
    {
        Object unObjet = null;
        String methodName = null;
        long hashCodeRecherche;

        if (uneCollection == null)
        {
            throw new ProgrammationException(COLLECTION_NULL);
        }

        try
        {
            hashCodeRecherche = Long.valueOf(text).longValue();
        }

        catch (NumberFormatException nbe)
        {

            throw new ProgrammationException(TEXTE_NON_NUMERIQUE, nbe);
        }

        Set selectedElements = new HashSet();
        for (Iterator iter = uneCollection.iterator(); iter.hasNext();)
        {
            Object element = iter.next();

            if (element != null && element.hashCode() == hashCodeRecherche)
            {
                selectedElements.add(element);
            }

        }

        // Si plus d'un élément est sélectionné, on considère que la
        // collection est incohérente
        if (selectedElements.size() > 1)
        {
            MessageFormat msgFormat = new MessageFormat(INCOHERENCE);
            Object[] attr = {methodName, text};

            throw new ProgrammationException(msgFormat.format(attr));
        }

        if (selectedElements.iterator().hasNext())
        {
            unObjet = selectedElements.iterator().next();
        }
        return unObjet;
    }

    /**
     * Recherche un objet dans une Map en utilisant une clé.
     * 
     * @param uneMap map dans laquelle l'objet est recherché
     * @param text String qui identifie l'objet dans la map
     * @return l'objet sélectionné ou null si aucun objet ne correspond à la clé dans la map
     */
    private Object findObjectInMap(final Map uneMap, final String text)
    {
        Object unObjet = null;

        if (uneMap == null)
        {
            throw new ProgrammationException(MAP_NULL);
        }
        unObjet = uneMap.get(text);
        return unObjet;
    }

}
