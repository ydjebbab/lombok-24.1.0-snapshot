/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : ContenuFichier.java
 *
 */
package fr.gouv.finances.lombok.upload.bean;

import java.io.File;
import java.util.Arrays;

import fr.gouv.finances.lombok.util.base.BaseBean;

/**
 * Class ContenuFichier --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.2 $ Date: 11 déc. 2009
 */
public class ContenuFichier extends BaseBean
{

    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** id. */
    private Long id;

    /** version. */
    private int version;

    /** Contenu du fichier sous forme de tableau de bytes. */
    private byte[] data;
    
    /** Flag permettant d'activer/désactiver la suppression du fichier temporaire à la destruction de l'objet */
    private boolean supprimerFichierTemporaire;

    /**
     * Fichier temporaire dans lequel les données peuvent éventuellement être stockée. Destiné à stocker les fichiers de
     * taille importante avant leur stockage en base ou lors de la phase de restitution à l'utilisateur. Cette propriété
     * n'est pas stockée directement en base de donnée. Les données contenue dans le fichier temporaire sont placées
     * dans le champ 'data
     */
    private File fichierTemporaire;

    /**
     * Constructeur de la classe ContenuFichier.java
     *
     */
    public ContenuFichier()
    {
        super();  
        
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param object
     * @return true, si c'est vrai
     * @see java.lang.Object#equals(java.lang.Object)
     */
    public boolean equals(Object object)
    {
        if (this == object)
        {
            return true;
        }

        if (object == null || (object.getClass() != this.getClass()))
        {
            return false;
        }

        ContenuFichier myObj = (ContenuFichier) object;

        return ((Arrays.equals(data, myObj.data)));

    }

    /**
     * Accesseur de l attribut data.
     * 
     * @return data
     */
    public byte[] getData()
    {
        return data;
    }

    /**
     * Accesseur de l attribut fichier temporaire.
     * 
     * @return fichier temporaire
     */
    public File getFichierTemporaire()
    {
        return fichierTemporaire;
    }

    /**
     * Accesseur de l attribut id.
     * 
     * @return id
     */
    public Long getId()
    {
        return id;
    }

    /**
     * Accesseur de l attribut version.
     * 
     * @return version
     */
    public int getVersion()
    {
        return version;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @return int
     * @see java.lang.Object#hashCode()
     */
    public int hashCode()
    {
        int hash = hashCODEHASHINIT;
        int varCode = 0;
        if (data != null)
        {
            for (int fieldCounter = 0; fieldCounter < data.length; fieldCounter++)
            {
                varCode = data[fieldCounter];
                hash = hashCODEHASHMULT * hash + varCode;
            }
        }
        return hash;
    }

    /**
     * Modificateur de l attribut data.
     * 
     * @param data le nouveau data
     */
    public void setData(byte[] data)
    {
        this.data = data;
    }

    /**
     * Modificateur de l attribut fichier temporaire.
     * 
     * @param fichierTemporaire le nouveau fichier temporaire
     */
    public void setFichierTemporaire(File fichierTemporaire)
    {
        this.fichierTemporaire = fichierTemporaire;
    }

    /**
     * Modificateur de l attribut id.
     * 
     * @param id le nouveau id
     */
    public void setId(Long id)
    {
        this.id = id;
    }

    /**
     * Modificateur de l attribut version.
     * 
     * @param version le nouveau version
     */
    public void setVersion(int version)
    {
        this.version = version;
    }

    /**
     * Accesseur de l attribut supprimerFichierTemporaire.
     * 
     * @return supprimerFichierTemporaire
     */
    public boolean isSupprimerFichierTemporaire()
    {
        return supprimerFichierTemporaire;
    }

    /**
     * Modificateur de l attribut supprimerFichierTemporaire.
     * 
     * @param supprimerFichierTemporaire le nouveau supprimerFichierTemporaire
     */
    public void setSupprimerFichierTemporaire(boolean supprimerFichierTemporaire)
    {
        this.supprimerFichierTemporaire = supprimerFichierTemporaire;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @see java.lang.Object#finalize()
     */
    protected void finalize()
    {
        if (this.fichierTemporaire != null && supprimerFichierTemporaire)
        {
            boolean isSupprimer = this.fichierTemporaire.delete();
            if (!isSupprimer)
            {
                log.info("Le fichier '" + this.fichierTemporaire.getAbsolutePath() + "' n'a pas été supprimé");
            }
        }
    }

}
