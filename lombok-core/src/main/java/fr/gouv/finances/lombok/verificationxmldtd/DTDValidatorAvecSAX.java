/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 *
 */

package fr.gouv.finances.lombok.verificationxmldtd;

import java.io.IOException;
import java.util.ArrayList;

import javax.xml.XMLConstants;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

/**
 * Validation d'un fichier XML avec DOM et SAX en utilisant un DTD
 * 
 * @author amleplatinec
 */
public class DTDValidatorAvecSAX
{
    /**
     * Constructeur.
     *
     */
    public DTDValidatorAvecSAX()
    {
        super();
    }

    /**
     * on implémente un nouveau Entity resolver pour remplacer la dtd présente dans le fichier initial par une autre
     * valeur.
     * 
     * @author amleplatinec-cp
     */
    private class DTDResolver implements EntityResolver
    {
        /**
         * Constructeur.
         */
        public DTDResolver()
        {
            super();
        }

        /**
         * (methode de remplacement) {@inheritDoc}.
         * 
         * @param publicId
         * @param systemId
         * @return input source
         * @throws SAXException the SAX exception
         * @throws IOException Signal qu'une execption de type I/O s'est produite.
         * @see org.xml.sax.EntityResolver#resolveEntity(java.lang.String, java.lang.String)
         */
        public InputSource resolveEntity(java.lang.String publicId, java.lang.String systemId) throws SAXException,
            java.io.IOException
        {
            return null;
        }

    }

    /**
     * Surcharge validateur pour récuperer les erreurs.
     * 
     * @author amleplatinec-cp
     */
    private class Validator extends DefaultHandler
    {

        /** Memo erreur validation. */
        public boolean validationError = false;

        /** Memo Exception(s) sax. */
        public ArrayList<SAXParseException> saxParseExceptionList = new ArrayList<>();

        /**
         * (methode de remplacement) {@inheritDoc}.
         * 
         * @param exception
         * @see org.xml.sax.helpers.DefaultHandler#error(org.xml.sax.SAXParseException)
         */
        @Override
        public void error(final SAXParseException exception)
        {
            validationError = true;
            saxParseExceptionList.add(exception);
        }

        /**
         * (methode de remplacement) {@inheritDoc}.
         * 
         * @param exception
         * @see org.xml.sax.helpers.DefaultHandler#fatalError(org.xml.sax.SAXParseException)
         */
        @Override
        public void fatalError(final SAXParseException exception)
        {
            validationError = true;
            saxParseExceptionList.add(exception);
        }

        /**
         * (methode de remplacement) {@inheritDoc}.
         * 
         * @param exception
         * @see org.xml.sax.helpers.DefaultHandler#warning(org.xml.sax.SAXParseException)
         */
        @Override
        public void warning(final SAXParseException exception)
        {
            validationError = true;
            saxParseExceptionList.add(exception);
        }
    }

    /** propriétés dom *. */

    protected static final String LOAD_EXTERNAL_DTD_FEATURE_ID =
        "http://apache.org/xml/features/nonvalidating/load-external-dtd";

    /** Constant : FATAL_ERROR. */
    static final String FATAL_ERROR = "http://apache.org/xml/features/continue-after-fatal-error";

    /** résultat de validation *. */
    private boolean isValid = false;

    /** La réponse de la validation. */
    private String response = null;

    /*
     * -----------------------------------------------------------------------------------------------------------
     * Premier cas : la DTD est celle spécifiée dans le fichier xml et est située au même endroit physique que le
     * fichier xml
     */

    /** Le logger. */
    private final Log log = LogFactory.getLog(this.getClass());

    // ------------------------------------------------------------------------------------------------------------------------------------------------

    // Second cas : la DTD n'estpas forcément celle spécifiée dans le fichier xml
    // ou n' est située au même endroit physique que le fichier xml

    /**
     * Verifie si valide.
     * 
     * @return true, si c'est valide
     */
    public boolean isValid()
    {
        return isValid;
    }

    /**
     * on passe juste le nom du fichier xml (avec son chemin ) en entree méthode principale qui fait la validation.
     * 
     * @param xmlFile --
     * @return the string
     */
    public String validerXMLParRapportDTD(String xmlFile)
    {

        Validator handler = new Validator();

        try
        {
            SAXParserFactory factory = SAXParserFactory.newInstance();
            factory.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
            // Obligatoire sinon erreur Error: cvc-elt.1: Cannot find the declaration
            factory.setNamespaceAware(true);
            // of element 'Document'.
            // at line 2, column 66
            factory.setValidating(true);

            SAXParser parser = factory.newSAXParser();

            factory.setFeature(FATAL_ERROR, Boolean.TRUE);

            XMLReader reader = parser.getXMLReader();
            reader.setErrorHandler(handler);
            reader.parse(xmlFile);

            isValid = !handler.validationError;
            if (handler.validationError)
            {
                response = calculerReponse(handler.saxParseExceptionList);
                return response;
            }
        }
        catch (ParserConfigurationException pce)
        {
            log.debug("Exception détectée", pce);
            if (handler.validationError)
            {
                response = calculerReponse(handler.saxParseExceptionList);
            }
            StringBuilder   sbu;
            if (response != null && response.compareTo("") != 0)
            {
                sbu = new StringBuilder(response);
            }
            else
            {
                sbu = new StringBuilder("");
            }
            sbu.append("\n  ");
            sbu.append(" *** ");
            sbu.append(pce.getMessage());
            return sbu.toString();

        }
        catch (IOException ioe)
        {
            log.debug("Exception détectée", ioe);
            if (handler.validationError)
            {
                response = calculerReponse(handler.saxParseExceptionList);
            }
            StringBuilder   sbu;
            if (response != null && response.compareTo("") != 0)
            {
                sbu = new StringBuilder(response);
            }
            else
            {
                sbu = new StringBuilder("");
            }
            sbu.append("\n  ");
            sbu.append(" *** ");
            sbu.append(ioe.getMessage());
            return sbu.toString();

        }
        catch (SAXException saxe)
        {
            log.debug("Exception détectée", saxe);
            if (handler.validationError)
            {
                response = calculerReponse(handler.saxParseExceptionList);
            }
            StringBuilder   sbu;
            if (response != null && response.compareTo("") != 0)
            {
                sbu = new StringBuilder(response);
            }
            else
            {
                sbu = new StringBuilder("");
            }
            sbu.append("\n  ");
            sbu.append(" *** ");
            sbu.append(saxe.getMessage());
            return sbu.toString();

        }

        return response;
    }

    // -----------------------------------------------------------------------------------------------------------------------------------------------------

    // Méthodes communes pour récupérer les erreurs

    /**
     * on passe juste les noms de fichiers en entree méthode principale qui fait la validation.
     * 
     * @param xmlFile --
     * @param dtdFile --
     * @return the string
     */
    public String validerXMLParRapportDTDNouvelle(String xmlFile, String dtdFile)
    {

        Validator handler = new Validator();

        try
        {
            SAXParserFactory factory = SAXParserFactory.newInstance();
            factory.setNamespaceAware(true);
            factory.setValidating(false);
            SAXParser parser = factory.newSAXParser();

            factory.setFeature(LOAD_EXTERNAL_DTD_FEATURE_ID, Boolean.FALSE);
            factory.setFeature(FATAL_ERROR, Boolean.TRUE);

            XMLReader reader = parser.getXMLReader();
            reader.setEntityResolver(new DTDResolver());
            reader.setErrorHandler(handler);
            reader.parse(xmlFile);

            isValid = !handler.validationError;
            if (handler.validationError)
            {
                response = calculerReponse(handler.saxParseExceptionList);
                return response;
            }
        }
        catch (ParserConfigurationException | IOException | IllegalArgumentException | SAXException exception)
        {
            log.debug("Exception détectée", exception);
            if (handler.validationError)
            {
                response = calculerReponse(handler.saxParseExceptionList);
            }
            StringBuilder   sbu;
            if (response != null && response.compareTo("") != 0)
            {
                sbu = new StringBuilder(response);
            }
            else
            {
                sbu = new StringBuilder("");
            }
            sbu.append("\n  ");
            sbu.append(" *** ");
            sbu.append(exception.getMessage());
            return sbu.toString();

        }

        return response;
    }

    /**
     * mise sous une forme string de la réponse.
     * 
     * @param saxParseExceptionList --
     * @return the string
     */
    private String calculerReponse(ArrayList<SAXParseException> saxParseExceptionList)
    {
        StringBuilder   sb = new StringBuilder();
        sb.append("les erreurs suivantes ont ete detectees");
        for (int i = 0; i < saxParseExceptionList.size(); i++)
        {
            SAXParseException xcpt = (SAXParseException) saxParseExceptionList.get(i);
            sb.append("\n  ").append(xcpt.getLineNumber());
            sb.append(':').append(xcpt.getColumnNumber());
            sb.append(" *** ").append(xcpt.getMessage());
        }
        return sb.toString();
    }

}
