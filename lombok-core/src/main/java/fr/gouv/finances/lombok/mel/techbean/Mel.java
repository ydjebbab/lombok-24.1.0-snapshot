/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : Mel.java
 *
 */
package fr.gouv.finances.lombok.mel.techbean;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import javax.mail.internet.InternetAddress;

import org.springframework.beans.factory.annotation.Value;

import fr.gouv.finances.lombok.util.base.BaseTechBean;
import fr.gouv.finances.lombok.util.messages.Messages;

/**
 * Cette classe permet de constituer un objet mel traité ensuite par un service implementant la classe
 * MelService.
 * 
 * @author lcontinsouzas-cp
 * @version $Revision: 1.3 $ Date: 15 déc. 2009
 */
public class Mel extends BaseTechBean
{

    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = 1L;
    /** Le code regexp */
    @Value("${mel.code.regexp:DEFAULT}")
    private String codeMailRegexp;

    /** a. */
    private final InternetAddress a = null;

    /** de. */
    private InternetAddress de = null;

    /** repondreA. */
    private InternetAddress repondreA = null;

    /** liste a. */
    private final List<InternetAddress> listeA = new ArrayList<InternetAddress>();

    /** liste cc. */
    private final List<InternetAddress> listeCc = new ArrayList<InternetAddress>();

    /** liste cci. */
    private final List<InternetAddress> listeCci = new ArrayList<InternetAddress>();

    /** encodage. */
    private String encodage = "UTF-8";

    /** liste pieces jointes. */
    private final List<PieceJointe> listePiecesJointes = new ArrayList<PieceJointe>();

    /** liste pieces jointes inline. */
    private final List<PieceJointeInline> listePiecesJointesInline = new ArrayList<PieceJointeInline>();

    /** messsage. */
    private String messsage = null;

    /** objet. */
    private String objet = null;

    /* Traitement des destinataire A */

    /**
     * Permet d'ajouter un destinataire.
     * 
     * @param nouveauA --
     */
    public void ajouterDestinataireA(InternetAddress nouveauA)
    {
        listeA.add(nouveauA);
    }

    /**
     * Constructeur de la classe Mel.java
     *
     */
    public Mel()
    {
        super();
        
    }

    /**
     * Permet d'ajouter un destinataire.
     * 
     * @param adressemel --
     */
    public void ajouterDestinataireA(String adressemel)
    {
        ajouterDestinataireA(adressemel, "");
    }

    /**
     * Permet d'ajouter un destinataire.
     * 
     * @param adressemel --
     * @param nom du destinataire à ajouter
     */
    public void ajouterDestinataireA(String adressemel, String nom)
    {
        try
        {
            InternetAddress iadd = new InternetAddress(adressemel, nom, encodage);
            ajouterDestinataireA(iadd);
        }
        catch (UnsupportedEncodingException e)
        {
            throw new MelProgrammationException(Messages.getString("exception.encodage"), e);
        }
    }

    /**
     * Permet d'ajouter un correspondants en copie.
     * 
     * @param nouveauCc --
     */
    public void ajouterDestinataireCc(InternetAddress nouveauCc)
    {
        listeCc.add(nouveauCc);
    }

    /* Traitement des destinataires en copie Cc */

    /**
     * Permet d'ajouter un correspondant en copie.
     * 
     * @param adressemel --
     */
    public void ajouterDestinataireCc(String adressemel)
    {
        ajouterDestinataireCc(adressemel, "");
    }

    /**
     * Permet d'ajouter des correspondants en copie.
     * 
     * @param adressemel --
     * @param nom du correspondant en copie à ajouter
     */
    public void ajouterDestinataireCc(String adressemel, String nom)
    {
        try
        {
            InternetAddress iadd = new InternetAddress(adressemel, nom, encodage);
            ajouterDestinataireCc(iadd);
        }
        catch (UnsupportedEncodingException e)
        {
            throw new MelProgrammationException(Messages.getString("exception.encodage"), e);
        }
    }

    /**
     * Permet d'ajouter un correspondants en copie.
     * 
     * @param nouveauCci --
     */
    public void ajouterDestinataireCci(InternetAddress nouveauCci)
    {
        listeCci.add(nouveauCci);
    }

    /**
     * Permet d'ajouter un correspondant en copie cachée.
     * 
     * @param adressemel --
     */
    public void ajouterDestinataireCci(String adressemel)
    {
        ajouterDestinataireCci(adressemel, "");
    }

    /* Traitement des destinataires en copie cachée Cci */

    /**
     * Permet d'ajouter des correspondants en copie cachée.
     * 
     * @param adressemel --
     * @param nom du correspondant en copie cachée à ajouter
     */
    public void ajouterDestinataireCci(String adressemel, String nom)
    {
        try
        {
            InternetAddress iadd = new InternetAddress(adressemel, nom, encodage);
            ajouterDestinataireCci(iadd);
        }
        catch (UnsupportedEncodingException e)
        {
            throw new MelProgrammationException(Messages.getString("exception.encodage"), e);
        }
    }

    /**
     * Permet d'ajouter une pièce jointe à une mel.
     * 
     * @param pj --
     */
    public void ajouterPieceJointe(PieceJointe pj)
    {
        this.listePiecesJointes.add(pj);
    }

    /**
     * Permet d'ajouter une pièce jointe inline à une mel.
     * 
     * @param pji --
     */
    public void ajouterPieceJointeInline(PieceJointeInline pji)
    {
        this.listePiecesJointesInline.add(pji);
    }

    /**
     * Permet de récupérer le nom et adresse mail de l'expéditeur du message.
     * 
     * @return nom et adresse mail de l'expéditeur du message
     */
    public InternetAddress getDe()
    {
        return de;
    }

    /**
     * Permet de récupérer le nom et adresse mail de réponse du message.
     * 
     * @return nom et adresse mail de réponse du message
     */
    public InternetAddress getRepondreA()
    {
        return repondreA;
    }

    /* Traitement de l'expediteur De */

    /**
     * Accesseur de l attribut encodage.
     * 
     * @return encodage
     */
    public String getEncodage()
    {
        return encodage;
    }

    /**
     * Permet d'obtenir la liste des mail et noms des destinaires du message.
     * 
     * @return liste des destinataires
     */
    public List<InternetAddress> getListeDestinatairesA()
    {
        return listeA;
    }

    /**
     * Permet d'obtenir la liste des mail et noms des correpondants en copie du message.
     * 
     * @return liste des destinataires
     */
    public List<InternetAddress> getListeDestinatairesCc()
    {
        return listeCc;
    }

    /**
     * Permet d'obtenir la liste des mail et noms des correpondants en copie cachée du message.
     * 
     * @return liste des destinataires
     */
    public List<InternetAddress> getListeDestinatairesCci()
    {
        return listeCci;
    }

    /**
     * Permet de recuprer la liste des pièces jointes associées.
     * 
     * @return liste des pièces jointes associées au mel
     */
    public List<PieceJointe> getListePiecesJointes()
    {
        return listePiecesJointes;
    }

    /**
     * Permet de recuprer la liste des pièces jointes inline associées.
     * 
     * @return liste des pièces jointes inline associées au mel
     */
    public List<PieceJointeInline> getListePiecesJointesInline()
    {
        return listePiecesJointesInline;
    }

    /**
     * Accesseur de l attribut messsage.
     * 
     * @return messsage
     */
    public String getMesssage()
    {
        return messsage;
    }

    /**
     * Accesseur de l attribut objet.
     * 
     * @return objet
     */
    public String getObjet()
    {
        return objet;
    }

    /**
     * Permet de definir l'expediteur de message.
     * 
     * @param de --
     */
    public void setDe(InternetAddress de)
    {
        this.verifierAdresseMel(de.getAddress());
        this.de = de;
    }

    /**
     * Permet de definir l'expediteur de message.
     * 
     * @param adressemel adresse mel de reponse de l'expediteur. Le format doit etre valide mais la boite n'est pas
     *        forcement existante si pas de possibilite de reponse souhaitee.
     */
    public void setDe(String adressemel)
    {
        setDe(adressemel, "");
    }

    /**
     * Permet de definir l'expediteur de message.
     * 
     * @param adressemel adresse mel de reponse de l'expediteur. Le format doit etre valide mais la boite n'est pas
     *        forcement existante si pas de possibilite de reponse souhaitee.
     * @param nom nom de l'expediteur
     */
    public void setDe(String adressemel, String nom)
    {
        try
        {
            InternetAddress iadd = new InternetAddress(adressemel, nom, encodage);
            setDe(iadd);
        }
        catch (UnsupportedEncodingException e)
        {
            throw new MelProgrammationException(Messages.getString("exception.encodage"), e);
        }
    }

    /**
     * Permet de definir l'adresse de réponse du message.
     * 
     * @param repondreA --
     */
    public void setRepondreA(InternetAddress repondreA)
    {
        this.verifierAdresseMel(repondreA.getAddress());
        this.repondreA = repondreA;
    }

    /**
     * Permet de definir l'adresse de réponse du message.
     * 
     * @param adressemel adresse mel de reponse de l'expediteur. Le format doit etre valide mais la boite n'est pas
     *        forcement existante si pas de possibilite de reponse souhaitee.
     */
    public void setRepondreA(String adressemel)
    {
        setRepondreA(adressemel, "");
    }

    /**
     * Permet de definir l'adresse de réponse du message.
     * 
     * @param adressemel adresse mel de reponse de l'expediteur. Le format doit etre valide mais la boite n'est pas
     *        forcement existante si pas de possibilite de reponse souhaitee.
     * @param nom nom de l'expediteur
     */
    public void setRepondreA(String adressemel, String nom)
    {
        try
        {
            InternetAddress iadd = new InternetAddress(adressemel, nom, encodage);
            setRepondreA(iadd);
        }
        catch (UnsupportedEncodingException e)
        {
            throw new MelProgrammationException(Messages.getString("exception.encodage"), e);
        }
    }

    /**
     * Permet de définir l'encodage du mel la valeur par defaut est UTF-8.
     * 
     * @param encodage --
     */
    public void setEncodage(String encodage)
    {
        this.encodage = encodage;
    }

    /**
     * Permet de definir le corps du message.
     * 
     * @param messsage --
     */
    public void setMesssage(String messsage)
    {
        this.messsage = messsage;
    }

    /**
     * Permet de definir l'objet du message.
     * 
     * @param objet --
     */
    public void setObjet(String objet)
    {
        this.objet = objet;
    }

    /**
     * Permet de verifier le format d'une adresse mel.
     * 
     * @param adressemel --
     */
    public void verifierAdresseMel(String adressemel)
    {
    	AdresseMailRegexp addressMailRegexp = this.getAdresseMailRegexp();
    	
        if ((adressemel == null) || (!adressemel.matches(addressMailRegexp.getRegexp())))
        {
            throw new MelRegleGestionException(Messages.getString("exception.adressemailerronee")
                + " : " + adressemel);
        }
    }

    /**
     * 
     * Permet de choisir le type de regexp à appliquer afin de valider l'adresse mel. 
     *
     * @return
     */
    public AdresseMailRegexp getAdresseMailRegexp(){
    	if(codeMailRegexp == null || codeMailRegexp.isEmpty()){
    		return AdresseMailRegexp.DEFAULT_REGEXP;
    	}else{
    		try {
    			return AdresseMailRegexp.getRegexpByCode(codeMailRegexp);
    		}catch (IllegalArgumentException e) {
				log.warn(Messages.getString("exception.coderegexperronee") + " : " + e.getMessage());
				return AdresseMailRegexp.DEFAULT_REGEXP;
			}
    	}
    }
    
    /**
     * Permet de verifier que l'emetteur, le destinatiaire, le message et l'objet du mel sont definis.
     */
    public void verifierPresenceInformationsMinimalesPourEnvoyerLeMel()
    {
        if ((this.a == null && this.listeA.isEmpty()) || (this.getDe() == null) || (this.getMesssage() == null)
            || (this.getObjet() == null))
        {
            throw new MelRegleGestionException(Messages.getString("exception.informationsminimalesnonpresentes"));
        }
    }
    
    /**
     *
     * @param codeMailRegexp
     */
	public void setCodeMailRegexp(String codeMailRegexp) {
		this.codeMailRegexp = codeMailRegexp;
	}

	/**
	 * 
	 * @return
	 */
    public String getCodeMailRegexp()
    {
        return codeMailRegexp;
    }	

}
