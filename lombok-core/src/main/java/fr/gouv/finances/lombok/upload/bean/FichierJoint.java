/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 */
package fr.gouv.finances.lombok.upload.bean;

import java.io.File;
import java.util.Date;
import java.util.Objects;

import fr.gouv.finances.lombok.util.base.BaseBean;

/**
 * Fichier joint.
 *
 * @author amleplatinec
 * @author Christophe Breheret-Girardin
 */
public class FichierJoint extends BaseBean
{
    /** Initialisation de l'UID. */
    private static final long serialVersionUID = 1L;

    /** Identifiant. */
    private Long id;

    /** Version. */
    private int version;

    /** Nom du fichier. */
    private String nomFichierOriginal;

    /** Type MIME. */
    private String typeMimeFichier;

    /** Taille. */
    private long tailleFichier;

    /** Date de création du fichier. */
    private Date dateHeureSoumission;

    /** Données contenues dans le fichier. */
    private ContenuFichier leContenuDuFichier = new ContenuFichier();

    /**
     * Flag qui indique si le contenu du fichier est stocké directement dans l'objet ContenuFichier via un tableau de
     * bytes ou si seule la référence à un fichier stocké dans le SF est utilisée.
     */
    private boolean dataInByteArray;

    /**
     * Instanciation de fichier joint.
     */
    public FichierJoint()
    {
        super();
    }

    /**
     * Constructeur à partir d'un contenu de fichier.
     * 
     * @param data données contenues dans le fichier
     */
    public FichierJoint(byte[] data)
    {
        super();
        if (data == null)
        {
            throw new IllegalArgumentException("LE PARAMETRE DATA NE DOIT PAS ETRE NULL.");
        }
        else
        {
            ContenuFichier unContenuFichier = new ContenuFichier();
            this.dataInByteArray = true;
            unContenuFichier.setData(data);
            this.dateHeureSoumission = new Date();
            this.leContenuDuFichier = unContenuFichier;
            this.tailleFichier = unContenuFichier.getData().length;
        }
    }

    /**
     * Constructeur à partir d'un fichier temporaire.
     * 
     * @param fichierTemporaire --
     */
    public FichierJoint(File fichierTemporaire)
    {
        this(fichierTemporaire, true);
    }
    
    /**
     * Constructeur.
     *
     * @param fichierTemporaire --
     * @param supprimerFichierTemporaire --
     */
    public FichierJoint(File fichierTemporaire, boolean supprimerFichierTemporaire)
    {
        super();
        if (fichierTemporaire == null)
        {
            throw new IllegalArgumentException("LE PARAMETRE FICHIERTEMPORAIRE NE DOIT PAS ETRE NULL.");
        }
        else if (!fichierTemporaire.exists())
        {
            throw new IllegalArgumentException("LE FICHIER " + fichierTemporaire.getAbsolutePath() + " N'EXISTE PAS");
        }
        else if (!(fichierTemporaire.canRead() && fichierTemporaire.canWrite()))
        {
            throw new IllegalArgumentException("LE FICHIER " + fichierTemporaire.getAbsolutePath()
                + " N'EST PAS ACCESSIBLE EN LECTURE / ECRITURE");
        }
        else
        {
            ContenuFichier unContenuFichier = new ContenuFichier();
            this.dataInByteArray = false;
            unContenuFichier.setFichierTemporaire(fichierTemporaire);
            unContenuFichier.setSupprimerFichierTemporaire(supprimerFichierTemporaire);
            this.dateHeureSoumission = new Date();
            this.leContenuDuFichier = unContenuFichier;
            this.tailleFichier = unContenuFichier.getFichierTemporaire().length();
        }        
    }

    /** 
     * {@inheritDoc}
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode()
    {
        return Objects.hash(tailleFichier, nomFichierOriginal, typeMimeFichier, dateHeureSoumission);
    }

    /** 
     * {@inheritDoc}
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj)
    {
        if (obj == null)
        {
            return false;
        }

        if (this.getClass() != obj.getClass())
        {
            return false;
        }

        FichierJoint fichierJoint = (FichierJoint) obj;
        return tailleFichier == fichierJoint.tailleFichier
            && Objects.equals(nomFichierOriginal, fichierJoint.nomFichierOriginal)
            && Objects.equals(typeMimeFichier, fichierJoint.typeMimeFichier)
            && Objects.equals(dateHeureSoumission, fichierJoint.dateHeureSoumission);
    }

    /**
     * Retourne un objet FichierJoint contenant le tableau de bytes passé en paramètre.
     * 
     * @param data --
     * @return the instance
     */
    public static final FichierJoint getInstance(byte[] data)
    {
        FichierJoint unFichierJoint = new FichierJoint();

        ContenuFichier unContenuFichier = new ContenuFichier();
        unFichierJoint.dataInByteArray = true;
        unContenuFichier.setData(data);
        unFichierJoint.setDateHeureSoumission(new Date());
        unFichierJoint.setLeContenuDuFichier(unContenuFichier);
        unFichierJoint.setTailleFichier(unContenuFichier.getData().length);

        return unFichierJoint;
    }

    /**
     * Accesseur de l attribut date heure soumission.
     * 
     * @return date heure soumission
     */
    public Date getDateHeureSoumission()
    {
        return dateHeureSoumission;
    }

    /**
     * Accesseur de l attribut id.
     * 
     * @return id
     */
    public Long getId()
    {
        return id;
    }

    /**
     * Accesseur de l attribut le contenu du fichier.
     * 
     * @return le contenu du fichier
     */
    public ContenuFichier getLeContenuDuFichier()
    {
        return leContenuDuFichier;
    }

    /**
     * Accesseur de l attribut nom fichier original.
     * 
     * @return nom fichier original
     */
    public String getNomFichierOriginal()
    {
        return nomFichierOriginal;
    }

    /**
     * Accesseur de l attribut taille fichier.
     * 
     * @return taille fichier
     */
    public long getTailleFichier()
    {
        return tailleFichier;
    }

    /**
     * Accesseur de l attribut type mime fichier.
     * 
     * @return type mime fichier
     */
    public String getTypeMimeFichier()
    {
        return typeMimeFichier;
    }

    /**
     * Accesseur de l attribut version.
     * 
     * @return version
     */
    public int getVersion()
    {
        return version;
    }

    /**
     * Verifie si data in byte array.
     * 
     * @return true, si c'est data in byte array
     */
    public boolean isDataInByteArray()
    {
        return dataInByteArray;
    }

    /**
     * Modificateur de l attribut data in byte array.
     * 
     * @param dataInByteArray le nouveau data in byte array
     */
    public void setDataInByteArray(boolean dataInByteArray)
    {
        this.dataInByteArray = dataInByteArray;
    }

    /**
     * Modificateur de l attribut date heure soumission.
     * 
     * @param dateHeureSoumission le nouveau date heure soumission
     */
    public void setDateHeureSoumission(Date dateHeureSoumission)
    {
        this.dateHeureSoumission = dateHeureSoumission;
    }

    /**
     * Modificateur de l attribut id.
     * 
     * @param id le nouveau id
     */
    public void setId(Long id)
    {
        this.id = id;
    }

    /**
     * Modificateur de l attribut le contenu du fichier.
     * 
     * @param leContenuDuFichier le nouveau le contenu du fichier
     */
    public void setLeContenuDuFichier(ContenuFichier leContenuDuFichier)
    {
        this.leContenuDuFichier = leContenuDuFichier;
    }

    /**
     * Modificateur de l attribut nom fichier original.
     * 
     * @param nomFichierOriginal le nouveau nom fichier original
     */
    public void setNomFichierOriginal(String nomFichierOriginal)
    {
        this.nomFichierOriginal = nomFichierOriginal;
    }

    /**
     * Modificateur de l attribut taille fichier.
     * 
     * @param tailleFichier le nouveau taille fichier
     */
    public void setTailleFichier(long tailleFichier)
    {
        this.tailleFichier = tailleFichier;
    }

    /**
     * Modificateur de l attribut type mime fichier.
     * 
     * @param typeMimeFichier le nouveau type mime fichier
     */
    public void setTypeMimeFichier(String typeMimeFichier)
    {
        this.typeMimeFichier = typeMimeFichier;
    }

    /**
     * Modificateur de l attribut version.
     * 
     * @param version le nouveau version
     */
    public void setVersion(int version)
    {
        this.version = version;
    }

}
