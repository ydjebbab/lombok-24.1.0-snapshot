package fr.gouv.finances.lombok.autoconfig;

import org.springframework.boot.context.properties.ConfigurationProperties;

import fr.gouv.finances.lombok.autoconfig.LombokComposantProperties;

/**
 * @author celinio fernandes Date: Feb 19, 2020
 */
@ConfigurationProperties(prefix = "lombok.composant.upload")
public class UploadProperties extends LombokComposantProperties
{

    private int maxsize;

    public int getMaxsize()
    {
        return maxsize;
    }

    public void setMaxsize(int maxsize)
    {
        this.maxsize = maxsize;
    }

    @Override
    public String toString()
    {
        return "UploadProperties [inclus=" + inclus + ", maxsize=" + maxsize + "]";
    }
}
