/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 */
package fr.gouv.finances.lombok.util.exception;

import java.util.List;

/**
 * Class RegleGestionException --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.3 $ Date: 11 déc. 2009
 */
public class RegleGestionException extends GerableException
{

    /**
     * RegleGestionException Fait partie des exceptions potentiellement gérables dans le code Elle correspond à la non
     * satisfaction des règles de gestion applicative Pour les parties web elle permet de solliciter une correction de
     * l'utilisateur afin qu'il corrige ses données saisies pour qu'elles satisfassent les règles de gestion.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Instanciation de regle gestion exception.
     */
    public RegleGestionException()
    {
        super();

    }

    /**
     * Instanciation de regle gestion exception.
     * 
     * @param listeMessages --
     */
    public RegleGestionException(List<String> listeMessages)
    {
        super(listeMessages);
    }

    /**
     * Constructeur de la classe RegleGestionException.java on ajoute un message de libellé "arg0" qui se retrouvera en
     * premier dans la liste des messages
     * 
     * @param listeMessages
     * @param arg0
     */
    public RegleGestionException(List<String> listeMessages, String arg0)
    {
        super(listeMessages, arg0);
    }

    /**
     * Instanciation de regle gestion exception.
     * 
     * @param arg0 --
     */
    public RegleGestionException(String arg0)
    {
        super(arg0);

    }

    /**
     * Instanciation de regle gestion exception.
     * 
     * @param arg0 --
     * @param arg1 --
     */
    public RegleGestionException(String arg0, Throwable arg1)
    {
        super(arg0, arg1);

    }

    /**
     * Instanciation de regle gestion exception.
     * 
     * @param arg0 --
     */
    public RegleGestionException(Throwable arg0)
    {
        super(arg0);

    }

}
