/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 *
 */
package fr.gouv.finances.lombok.util.propertyeditor;

import java.beans.PropertyEditorSupport;
import java.util.HashMap;
import java.util.Map;

/**
 * Class AcceptStringKeyboardEditor Property editor qui n'accepte que la liste des caractères suivants. Optionnellement,
 * permet de transformer une chaine vide en une valeur nulle
 * 
 * @author chouard-cp
 * @version $Revision: 1.2 $ Date: 15 déc. 2009
 */
public class AcceptStringKeyboardEditor extends PropertyEditorSupport
{

    /** Constant : MSG_ERREUR. */
    private static final String MSG_ERREUR = "La chaîne doit contenir au moins un caractère alphanumérique";

    /** Constant : VALID_ALPHANUM_CHAR. */
    private static final char[] VALID_ALPHANUM_CHAR =
    {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L',
            'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
            'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'};

    /** Constant : VALID_KEYBOARD_CHAR. */
    private static final char[] VALID_KEYBOARD_CHAR =
    {'-', '.', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J',
            'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd',
            'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x',
            'y', 'z'};

    /** Constant : VALID_PARAGRAPH_SEPARATOR_CHAR. */
    private static final char[] VALID_PARAGRAPH_SEPARATOR_CHAR = {'\r', '\n'};

    /** empty as null. */
    private final boolean emptyAsNull;

    /** trim. */
    private final boolean trim;

    /** require one alpha numeric character. */
    private final boolean requireOneAlphaNumericCharacter;

    /** accept paragraph separator. */
    private final boolean acceptParagraphSeparator;

    /** keyboard char map. */
    private final Map<String, String> keyboardCharMap = new HashMap<String, String>();

    /** separator char map. */
    private final Map<String, String> separatorCharMap = new HashMap<String, String>();

    /** alpha num char map. */
    private final Map<String, String> alphaNumCharMap = new HashMap<String, String>();

    /**
     * Creation d'une nouvelle instance de AcceptStringKeyboardEditor.
     * 
     * @param emptyAsNull <code>true</code> si une chaine vide doit être transformée en <code>null</code>
     * @param trim <code>true</code> pour supprimer les caractères blancs en début et en fin de chaîne
     * @param acceptParagraphSeparator <code>true</code> pour autoriser les sauts de paragraphe
     */
    public AcceptStringKeyboardEditor(final boolean emptyAsNull, final boolean trim,
        final boolean acceptParagraphSeparator)
    {
        super();
        this.emptyAsNull = emptyAsNull;
        this.trim = trim;
        this.acceptParagraphSeparator = acceptParagraphSeparator;
        this.requireOneAlphaNumericCharacter = false;

        for (int i = 0; i < VALID_KEYBOARD_CHAR.length; i++)
        {
            this.keyboardCharMap.put(Integer.toHexString(VALID_KEYBOARD_CHAR[i]), null);
        }
        for (int i = 0; i < VALID_PARAGRAPH_SEPARATOR_CHAR.length; i++)
        {
            this.separatorCharMap.put(Integer.toHexString(VALID_PARAGRAPH_SEPARATOR_CHAR[i]), null);
        }
        if (this.requireOneAlphaNumericCharacter)
        {
            for (int i = 0; i < VALID_ALPHANUM_CHAR.length; i++)
            {
                this.alphaNumCharMap.put(Integer.toHexString(VALID_ALPHANUM_CHAR[i]), null);
            }
        }
    }

    /**
     * Creation d'une nouvelle instance de AcceptStringKeyboardEditor.
     * 
     * @param emptyAsNull <code>true</code> si une chaine vide doit être transformée en <code>null</code>
     * @param trim <code>true</code> pour supprimer les caractères blancs en début et en fin de chaîne
     * @param acceptParagraphSeparator <code>true</code> pour autoriser les sauts de paragraphe
     * @param requireOneAlphaNumericCharacter <code>true</code> pour refuser les chaînes de texte qui ne contiennent pas
     *        au moins un caractère alphanumérique
     */
    public AcceptStringKeyboardEditor(final boolean emptyAsNull, final boolean trim,
        final boolean acceptParagraphSeparator, final boolean requireOneAlphaNumericCharacter)
    {
        super();
        this.emptyAsNull = emptyAsNull;
        this.trim = trim;
        this.acceptParagraphSeparator = acceptParagraphSeparator;
        this.requireOneAlphaNumericCharacter = requireOneAlphaNumericCharacter;

        for (int i = 0; i < VALID_KEYBOARD_CHAR.length; i++)
        {
            this.keyboardCharMap.put(Integer.toHexString(VALID_KEYBOARD_CHAR[i]), null);
        }
        for (int i = 0; i < VALID_PARAGRAPH_SEPARATOR_CHAR.length; i++)
        {
            this.separatorCharMap.put(Integer.toHexString(VALID_PARAGRAPH_SEPARATOR_CHAR[i]), null);
        }
        if (this.requireOneAlphaNumericCharacter)
        {
            for (int i = 0; i < VALID_ALPHANUM_CHAR.length; i++)
            {
                this.alphaNumCharMap.put(Integer.toHexString(VALID_ALPHANUM_CHAR[i]), null);
            }
        }
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @return as text
     * @see java.beans.PropertyEditorSupport#getAsText()
     */
    @Override
    public String getAsText()
    {
        Object value = getValue();
        if (value != null)
        {
            return value.toString();

        }
        else
        {
            return "";
        }

    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param text le nouveau as text
     * @see java.beans.PropertyEditorSupport#setAsText(java.lang.String)
     */
    @Override
    public void setAsText(String text)
    {
        String epureValue = this.replaceInvalidCharWithBlank(text);
        if (this.trim)
        {
            epureValue = this.supprimeBlancsDebutFin(epureValue);
        }
        /*
         * else { epureValue = epureValue; }
         */
        if (this.emptyAsNull && "".equals(epureValue))
        {
            epureValue = null;
        }
        /*
         * else { epureValue = epureValue; }
         */

        if (this.requireOneAlphaNumericCharacter && !this.emptyAsNull && epureValue != null)
        {
            this.testSiLaChaineContientAuMoinsUnCarAlphaNum(epureValue);
        }

        setValue(epureValue);
    }

    /**
     * Teste si un caractère est alphanumérique.
     * 
     * @param caractere --
     * @return true, if checks if is alpha char
     */
    private boolean isAlphaChar(char caractere)
    {
        return (this.alphaNumCharMap.containsKey(Integer.toHexString(caractere)));
    }

    /**
     * Teste si un caractère est autorisé.
     * 
     * @param caractere --
     * @return true, if checks if is valid char
     */
    private boolean isValidChar(char caractere)
    {
        boolean result = false;
        result =
            (this.keyboardCharMap.containsKey(Integer.toHexString(caractere)) || (this.acceptParagraphSeparator && this.separatorCharMap
                .containsKey(Integer.toHexString(caractere))));
        return result;
    }

    /**
     * Supprime les caractères de la chaîne qui ne sont pas dans la liste des caractères autorisés.
     * 
     * @param text --
     * @return the string
     */
    private String replaceInvalidCharWithBlank(String text)
    {
        StringBuilder cleanValue = new StringBuilder();
        for (int i = 0; i < text.length(); i++)
        {
            char codeChar = text.charAt(i);
            if (isValidChar(codeChar))
            {
                cleanValue.append(codeChar);
            }
            else
            {
                cleanValue.append(' ');
            }
        }
        return cleanValue.toString();
    }

    /**
     * Supprime les blancs en début et en fin de chaîne.
     * 
     * @param text --
     * @return the string
     */
    private String supprimeBlancsDebutFin(String text)
    {
        if (text != null)
        {
            return text.trim();
        }
        else
        {
            return text;
        }

    }

    /**
     * Teste si une chaîne contient au moins un caractère alphanumérique.
     * 
     * @param text --
     */
    private void testSiLaChaineContientAuMoinsUnCarAlphaNum(String text)
    {
        boolean result = false;

        for (int i = 0; i < text.length(); i++)
        {
            char codeChar = text.charAt(i);
            if (this.isAlphaChar(codeChar))
            {
                result = true;
                break;
            }
        }

        if (!result)
        {
            throw new IllegalArgumentException(MSG_ERREUR);
        }
    }
}