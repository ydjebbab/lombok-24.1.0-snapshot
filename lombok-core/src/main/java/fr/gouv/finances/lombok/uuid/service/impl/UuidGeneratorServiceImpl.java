/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : UuidGeneratorServiceImpl.java
 *
 */
package fr.gouv.finances.lombok.uuid.service.impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.gouv.finances.lombok.util.base.BaseServiceImpl;
import fr.gouv.finances.lombok.uuid.service.UuidGeneratorService;

/**
 * Class UuidGeneratorServiceImpl --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.2 $ Date: 11 déc. 2009
 */
@Deprecated
public class UuidGeneratorServiceImpl extends BaseServiceImpl implements UuidGeneratorService
{
    
    private static final Logger log = LoggerFactory.getLogger(UuidGeneratorServiceImpl.class);

    /** Constant : DIGITS. */
    private static final char[] DIGITS =
    {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};

    /**
     * Transforms a <code>int</code> to a character array of hex octets.
     * 
     * @param input the integer
     * @return char[] the hex byte array
     */
    private static char[] asChars(int input)
    {
        return asChars(input, 8);
    }

    /**
     * Transforms an <code>int</code> to a character array of hex octets.
     * 
     * @param input the integer
     * @param length the number of octets to produce
     * @return char[]
     */
    private static char[] asChars(int input, int length)
    {
        int saisie = input; 
        int longueur = length;
        char[] out = new char[longueur--];

        for (int i = longueur; i > -1; i--)
        {
            out[i] = DIGITS[(byte) (saisie & 0x0F)];
            saisie >>= 4;
        }
        return out;
    }

    /**
     * Transforms a <code>long</code> to a character array of hex octets.
     * 
     * @param input the long
     * @param length the number of octets to produce
     * @return char[] the hex byte array
     */
    private static char[] asChars(long input, int length)
    {
        long saisie = input; 
        int longueur = length;
        char[] out = new char[longueur--];

        for (int i = longueur; i > -1; i--)
        {
            out[i] = DIGITS[(byte) (saisie & 0x0F)];
            saisie >>= 4;
        }
        return out;
    }

    /**
     * Transforms a <code>short</code> to a character array of hex octets.
     * 
     * @param input the integer
     * @return char[] the hex byte array
     */
    private static char[] asChars(short input)
    {
        return asChars(input, 4);
    }

    /**
     * Transforms a <code>short</code> to a character array of hex octets.
     * 
     * @param input the integer
     * @param length the number of octets to produce
     * @return char[] the hex byte array
     */
    private static char[] asChars(short input, int length)
    {
        short saisie = input; 
        int longueur = length;
        char[] out = new char[length--];

        for (int i = longueur; i > -1; i--)
        {
            out[i] = DIGITS[(byte) (saisie & 0x0F)];
            saisie >>= 4;
        }
        return out;
    }

    /**
     * Parses a <code>long</code> from a hex encoded number. This method will skip all characters that are not 0-9 and
     * a-f (the String is lower cased first). Returns 0 if the String does not contain any interesting characters.
     * 
     * @param input the String to extract a <code>long</code> from, may not be <code>null</code>
     * @return a <code>long</code>
     * @throws NullPointerException if the String is <code>null</code>
     */
    private static long parseLong(String input) throws NullPointerException
    {
        String inputLowerCase = input.toLowerCase(Locale.FRANCE);
        long out = 0;
        byte shifts = 0;
        char oneChar;

        for (int i = 0; i < inputLowerCase.length() && shifts < 16; i++)
        {
            oneChar = inputLowerCase.charAt(i);
            if ((oneChar > 47) && (oneChar < 58))
            {
                out <<= 4;
                ++shifts;
                out |= oneChar - 48;
            }
            else if ((oneChar > 96) && (oneChar < 103))
            {
                ++shifts;
                out <<= 4;
                out |= oneChar - 87;
            }
        }
        return out;
    }

    /**
     * Parses a <code>short</code> from a hex encoded number. This method will skip all characters that are not 0-9 and
     * a-f (the String is lower cased first). Returns 0 if the String does not contain any interesting characters.
     * 
     * @param input the String to extract a <code>short</code> from, may not be <code>null</code>
     * @return a <code>short</code>
     * @throws NullPointerException if the String is <code>null</code>
     */
    private static short parseShort(String input) throws NullPointerException
    {
        String inputLowerCase = input.toLowerCase(Locale.FRANCE);
        short out = 0;
        byte shifts = 0;
        char oneChar;

        for (int i = 0; i < inputLowerCase.length() && shifts < 4; i++)
        {
            oneChar = inputLowerCase.charAt(i);
            if ((oneChar > 47) && (oneChar < 58))
            {
                out <<= 4;
                ++shifts;
                out |= oneChar - 48;
            }
            else if ((oneChar > 96) && (oneChar < 103))
            {
                ++shifts;
                out <<= 4;
                out |= oneChar - 87;
            }
        }
        return out;
    }

    /** Clock sequence et node. */
    private long clockSeqAndNode;

    /** Le champ time de l'UUID. */
    private long time;

    /** Valeur précédente de la propriété time. */
    private long lastTime = Long.MIN_VALUE;

    /**
     * Instanciation de uuid generator service impl.
     */
    public UuidGeneratorServiceImpl()
    {
        super();
        this.time = this.newTime();
        this.clockSeqAndNode = this.constructClockSeqAndNode();
    }

    /**
     * Accesseur de l attribut clock seq and node.
     * 
     * @return clock seq and node
     */
    public long getClockSeqAndNode()
    {
        return clockSeqAndNode;
    }

    /**
     * Accesseur de l attribut time.
     * 
     * @return time
     */
    public long getTime()
    {
        return time;
    }

    /**
     * Fournit le prochain UUID.
     * 
     * @return UUID
     */
    public String nextUid()
    {
        this.time = this.newTime();
        StringBuilder   uid = new StringBuilder(36);

        uid.append(asChars((int) (time >> 32))).append('-').append(asChars((short) (time >> 16))).append('-').append(
            asChars((short) time)).append('-').append(asChars((short) (clockSeqAndNode >> 48))).append('-').append(
            asChars(clockSeqAndNode, 12));

        return uid.toString();
    }

    /**
     * Modificateur de l attribut clock seq and node.
     * 
     * @param clockSeqAndNode le nouveau clock seq and node
     */
    public void setClockSeqAndNode(long clockSeqAndNode)
    {
        this.clockSeqAndNode = clockSeqAndNode;
    }

    /**
     * Modificateur de l attribut time.
     * 
     * @param time le nouveau time
     */
    public void setTime(long time)
    {
        this.time = time;
    }

    /**
     * methode Construct clock seq and node : --.
     * 
     * @return long
     */
    private long constructClockSeqAndNode()
    {
        long clockseqnode = 0x8000000000000000L;
        String macAddress = null;

        Process process = null;
        InputStreamReader inputStreamReader = null;
        BufferedReader inputBufferedReader = null;

        try
        {
            String osname = System.getProperty("os.name");

            log.debug("Osname : " + osname);
            if (osname.startsWith("Windows"))
            {
                process = Runtime.getRuntime().exec(new String[] {"ipconfig", "/all"}, null);
            }
            // Solaris code must appear before the generic code
            else if (osname.startsWith("Solaris") || osname.startsWith("SunOS"))
            {
                String hostName = getFirstLineOfCommand(new String[] {"uname", "-n"});

                if (hostName != null)
                {
                    process = Runtime.getRuntime().exec(new String[] {"/usr/sbin/arp", hostName}, null);
                }
            }
            else if (osname.startsWith("AIX"))
            {
                if (new File("/usr/bin/entstat").exists())
                {
                    process = Runtime.getRuntime().exec(new String[] {"/usr/bin/entstat", "en0"}, null);
                }
            }
            else if (new File("/usr/sbin/lanscan").exists())
            {
                process = Runtime.getRuntime().exec(new String[] {"/usr/sbin/lanscan"}, null);
            }
            else if (new File("/sbin/ifconfig").exists())
            {
                process = Runtime.getRuntime().exec(new String[] {"/sbin/ifconfig", "-a"}, null);
            }

            if (process != null)
            {
                inputStreamReader = new InputStreamReader(process.getInputStream());
                inputBufferedReader = new BufferedReader(inputStreamReader, 128);
                String line = null;

                while ((line = inputBufferedReader.readLine()) != null)
                {
                    log.debug(line);
                    macAddress = this.parseMacAddress(line);
                    if (macAddress != null && parseShort(macAddress) != 0xff)
                    {
                        break;
                    }
                }
            }
        }
        catch (SecurityException e)
        {
            log.warn("Erreur lors de la lecture de l'adresse mac", e);
            // On ne fait rien
        }
        catch (IOException e)
        {
            log.warn("Erreur lors de la lecture de l'adresse mac", e);
            // On ne fait rien
        }
        finally
        {
            if (process != null)
            {
                if (inputBufferedReader != null)
                {
                    try
                    {
                        inputBufferedReader.close();
                    }
                    catch (IOException exception)
                    {
                        log.warn("Erreur lors de la lecture de l'adresse mac", exception);
                        // On ne fait rien
                    }
                }
                if (inputStreamReader != null)
                {
                    try
                    {
                        inputStreamReader.close();
                    }
                    catch (IOException exception)
                    {
                        log.warn("Erreur lors de la lecture de l'adresse mac", exception);
                        // On ne fait rien
                    }
                }
                try
                {
                    process.getErrorStream().close();
                }
                catch (IOException exception)
                {
                    log.warn("Erreur lors de la lecture de l'adresse mac", exception);
                    // On ne fait rien
                }
                try
                {
                    process.getOutputStream().close();
                }
                catch (IOException exception)
                {
                    log.warn("Erreur lors de la lecture de l'adresse mac", exception);
                    // On ne fait rien
                }
                process.destroy();
            }
        }

        log.info("Adresse mac du serveur utilisée pour la génération d'UUID : " + macAddress);

        if (macAddress != null)
        {
            if (macAddress.indexOf(':') != -1)
            {
                clockseqnode |= parseLong(macAddress);
            }
            else if (macAddress.startsWith("0x"))
            {
                clockseqnode |= parseLong(macAddress.substring(2));
            }
        }
        else
        {
            try
            {
                byte[] local = InetAddress.getLocalHost().getAddress();

                clockseqnode |= (local[0] << 24) & 0xFF000000L;
                clockseqnode |= (local[1] << 16) & 0xFF0000;
                clockseqnode |= (local[2] << 8) & 0xFF00;
                clockseqnode |= local[3] & 0xFF;
            }
            catch (UnknownHostException uhe)
            {
                log.debug("Exception détectée", uhe);
                clockseqnode |= (long) (Math.random() * 0x7FFFFFFF);
            }
        }

        // Skip the clock sequence generation process and use random
        // instead.

        clockseqnode |= (long) (Math.random() * 0x3FFF) << 48;

        return clockseqnode;
    }

    /**
     * Retourne la première ligne d'une commande shell.
     * 
     * @param commands la commande à exécuter
     * @return la première ligne du résultat de la commande
     * @throws IOException Signal qu'une execption de type I/O s'est produite.
     */
    private String getFirstLineOfCommand(String[] commands) throws IOException
    {
        Process process = null;
        BufferedReader reader = null;
        InputStreamReader inputStreamReader = null;

        try
        {
            process = Runtime.getRuntime().exec(commands);
            inputStreamReader = new InputStreamReader(process.getInputStream());
            reader = new BufferedReader(inputStreamReader, 128);
            return reader.readLine();
        }
        finally
        {
            if (process != null)
            {
                if (reader != null)
                {
                    try
                    {
                        reader.close();
                    }
                    catch (IOException exception)
                    {
                        log.warn("ERREUR LORS DE LA LECTURE DE LA PREMIERE LIGNE D'UNE COMMANDE", exception);
                        // On ne fait rien
                    }
                }
                if (inputStreamReader != null)
                {
                    try
                    {
                        inputStreamReader.close();
                    }
                    catch (IOException exception)
                    {
                        log.warn("ERREUR LORS DE LA LECTURE DE LA PREMIERE LIGNE D'UNE COMMANDE", exception);
                        // On ne fait rien
                    }
                }
                try
                {
                    process.getErrorStream().close();
                }
                catch (IOException exception)
                {
                    log.warn("ERREUR LORS DE LA LECTURE DE LA PREMIERE LIGNE D'UNE COMMANDE", exception);
                    // On ne fait rien
                }
                try
                {
                    process.getOutputStream().close();
                }
                catch (IOException exception)
                {
                    log.warn("ERREUR LORS DE LA LECTURE DE LA PREMIERE LIGNE D'UNE COMMANDE", exception);
                    // On ne fait rien
                }
                process.destroy();
            }
        }
    }

    /**
     * Actualise la valeur de la propriété time. Chaque valeur prise par la propriété time est unique et supérieure à la
     * valeur précédente de la propriété
     * 
     * @return une nouvelle valeur de la propriété time
     */
    private synchronized long newTime()
    {

        long newTimeValue;

        // UTC time
        long timeMillis = (System.currentTimeMillis() * 10000) + 0x01B21DD213814000L;

        if (timeMillis > lastTime)
        {
            lastTime = timeMillis;
        }
        else
        {
            timeMillis = ++lastTime;
        }

        // time low

        newTimeValue = timeMillis << 32;

        // time mid

        newTimeValue |= (timeMillis & 0xFFFF00000000L) >> 16;

        // time hi and version
        // version
        newTimeValue |= 0x1000 | ((timeMillis >> 48) & 0x0FFF);
        // 1

        return newTimeValue;
    }

    /**
     * methode Parses the mac address : --.
     * 
     * @param input --
     * @return string
     */
    private String parseMacAddress(String input)
    {
        String macAddress = null;

        if (input == null)
        {
            throw new IllegalArgumentException("La chaîne passée en paramètre est nulle");
        }
        else
        {
            int hexStart = input.indexOf("0x");

            if (hexStart != -1 && input.indexOf(' ', hexStart) != -1)
            {
                int hexEnd = input.indexOf(' ', hexStart);

                macAddress = input.substring(hexStart, hexEnd);
            }
            else
            {
                int octets = 0;
                int lastIndex;
                int old;
                int end;

                if (input.indexOf('-') > -1)
                {
                    input = input.replace('-', ':');
                }

                lastIndex = input.lastIndexOf(':');

                if (lastIndex <= input.length() - 2)
                {
                    end = Math.min(input.length(), lastIndex + 3);

                    ++octets;
                    old = lastIndex;
                    while (octets != 5 && lastIndex != -1 && lastIndex > 1)
                    {
                        lastIndex = input.lastIndexOf(':', --lastIndex);
                        if (old - lastIndex == 3 || old - lastIndex == 2)
                        {
                            ++octets;
                            old = lastIndex;
                        }
                    }

                    if (octets == 5 && lastIndex > 1)
                    {
                        macAddress = input.substring(lastIndex - 2, end).trim();
                    }
                }
            }
        }

        return macAddress;
    }

}
