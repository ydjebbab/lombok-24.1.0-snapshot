package fr.gouv.finances.lombok.autoconfig;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Classe de configuration pour les propriétés présentes dans application.properties et spécifiques à Lombok
 * 
 * @author celinio fernandes Date: Feb 25, 2020
 */
@ConfigurationProperties(prefix = "lombok")
public class LombokProperties
{
    private final OrmProperties ormProperties = new OrmProperties();

    private final ApteraProperties apteraProperties = new ApteraProperties();

    public ApteraProperties getApteraProperties()
    {
        return apteraProperties;
    }

    public OrmProperties getOrmProperties()
    {
        return ormProperties;
    }

    @ConfigurationProperties(prefix = "lombok.orm")
    public static class OrmProperties
    {

        private Boolean jpa;

        public Boolean getJpa()
        {
            return jpa;
        }

        public void setJpa(Boolean jpa)
        {
            this.jpa = jpa;
        }

    }

    @ConfigurationProperties(prefix = "lombok.aptera")
    public static class ApteraProperties
    {
        private String codeappli;

        public String getCodeappli()
        {
            return codeappli;
        }

        public void setCodeappli(String codeappli)
        {
            this.codeappli = codeappli;
        }
    }

}
