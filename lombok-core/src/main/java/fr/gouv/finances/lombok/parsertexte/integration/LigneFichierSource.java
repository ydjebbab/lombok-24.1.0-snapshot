/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : LigneFichierSource.java
 *
 */
package fr.gouv.finances.lombok.parsertexte.integration;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import fr.gouv.finances.lombok.parsertexte.parametrage.CritereDiscriminant;
import fr.gouv.finances.lombok.parsertexte.parametrage.DiscriminantLigne;
import fr.gouv.finances.lombok.parsertexte.parametrage.FichierParametrage;
import fr.gouv.finances.lombok.parsertexte.parametrage.ProprieteLigne;
import fr.gouv.finances.lombok.util.exception.ExploitationException;

/**
 * Class LigneFichierSource --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.4 $ Date: 11 déc. 2009
 */
public class LigneFichierSource
{

    /** contenu ligne. */
    private String contenuLigne = null;

    /** type separation. */
    private Integer typeSeparation = null;

    /** caractere separation. */
    private String caractereSeparation = null;

    /** elements chaine. */
    private List elementsChaine = null;

    /** contenu ligne apres ligne1. */
    private List contenuLigneApresLigne1 = null;

    /** elements chaine apres ligne1. */
    private List elementsChaineApresLigne1 = null;

    /** controleur contenu. */
    private final ControleurContenu controleurContenu = new ControleurContenu();

    /** numero ligne. */
    private int numeroLigne = -1;

    /** proprietes a mapper. */
    private Map proprietesAMapper = null;

    /** gestionGuillemets - boolean. */
    private final boolean gestionGuillemets;

    public LigneFichierSource()
    {
        super();
        gestionGuillemets=true;       
    }

    /**
     * Constructeur de la ligne, elle prend en paramètre la ligne elle même, le fichier de paramétrage afin d'en
     * extraire le caractère de séparation (si la ligne est une ligne avec séparateur) et le numéro de la ligne.
     * 
     * @param pLigne le ligne
     * @param pFichierParametrage le fichier parametrage
     * @param pNumeroLigne le numero ligne
     */
    public LigneFichierSource(String pLigne, FichierParametrage pFichierParametrage, int pNumeroLigne)
    {
        this.contenuLigne = pLigne;
        this.typeSeparation = pFichierParametrage.getType();
        this.gestionGuillemets = pFichierParametrage.getGestionGuillemets();

        // Si c'est un fichier avec séparateur, on le récupère en même temps.
        if (pFichierParametrage.getType().intValue() == FichierParametrage.TYPE_FICHIER_SEPARATEUR)
        {
            this.caractereSeparation = pFichierParametrage.getCaractereSeparation();
        }
        this.numeroLigne = pNumeroLigne;
    }

    /**
     * Cette méthode est utilisée dans le cas d'informations dans le fichier composées de plusieurs lignes de fichier.
     * 
     * @param pLigneSupplementaire le ligne supplementaire
     */
    public void ajouterLigneSupplementaire(String pLigneSupplementaire)
    {
        // Ajout de la chaine dans les éléments de la ligne
        if (this.contenuLigneApresLigne1 == null)
        {
            this.contenuLigneApresLigne1 = new ArrayList();
        }
        this.contenuLigneApresLigne1.add(pLigneSupplementaire);

        // On regarde maintenant si on doit découper cette ligne...
        if ((this.typeSeparation.intValue() == FichierParametrage.TYPE_FICHIER_SEPARATEUR))
        {
            if (this.elementsChaineApresLigne1 == null)
            {
                this.elementsChaineApresLigne1 = new ArrayList();
            }
            int numLigne = 1 + (this.elementsChaineApresLigne1.size() + 1);
            this.decoupageCsv(pLigneSupplementaire, this.caractereSeparation, this.gestionGuillemets, numLigne);
        }
    }

    /**
     * Controle du fait que la ligne soit conforme au discriminant passé en paramètre.
     * 
     * @param discriminant --
     * @return true, if est conforme a discriminant
     */
    public boolean estConformeADiscriminant(DiscriminantLigne discriminant)
    {

        if ((this.typeSeparation.intValue() == FichierParametrage.TYPE_FICHIER_SEPARATEUR)
            && (this.elementsChaine == null))
        {
            // Découpage de la chaine (pour les discriminants on ne peut travailler que la ligne 1)
            this.decoupageCsv(this.contenuLigne, this.caractereSeparation, this.gestionGuillemets, 1);
        }

        Iterator iterCriteres = discriminant.getCriteres().iterator();
        boolean bEstConforme = true;
        String strTmp = "";

        while ((iterCriteres.hasNext()) && (bEstConforme))
        {
            CritereDiscriminant critere = (CritereDiscriminant) iterCriteres.next();
            if (!critere.isToujoursVrai())
            {
                // Est ce un critère basé sur le n° de ligne ?
                if (critere.getNumeroLigne() > 0)
                {
                    bEstConforme = (numeroLigne == critere.getNumeroLigne());
                }
                else
                // non, donc on regarde la suite des critères possibles
                {
                    if (this.typeSeparation.intValue() == FichierParametrage.TYPE_FICHIER_SEPARATEUR)
                    {
                        int positionElementAControler = critere.getPosition();
                        // l'élément à contrôler se trouve t il après la fin du tableau ?
                        if (positionElementAControler > this.elementsChaine.size())
                        {
                            // oui, dc on est pas conforme
                            bEstConforme = false;
                        }
                        else
                        {
                            strTmp = (String) this.elementsChaine.get(positionElementAControler - 1);
                        }
                    }
                    else if (this.typeSeparation.intValue() == FichierParametrage.TYPE_FICHIER_POSITION_FIXE)
                    {
                        strTmp = this.extractionChaineDepuisCritere(this.contenuLigne, critere);
                    }

                    // /////////// Contrôle de la valeur //////////////
                    if (bEstConforme)
                    {
                        bEstConforme = false;
                        int operateur = critere.getOperateur();

                        if (critere.isSuppressionEspacesAutour())
                        {
                            strTmp = strTmp.trim();
                        }
                        // Gestion des critères de type "commence par" & "ne commence pas par"
                        if ((operateur >= CritereDiscriminant.BASE_CRITERE_CHAINE)
                            && (operateur <= CritereDiscriminant.BASE_FIN_CRITERE_CHAINE))
                        {
                            if (operateur == CritereDiscriminant.CRITERE_TAILLE_EGALE)
                            {
                                int taille = Integer.valueOf(critere.getValeur()).intValue();
                                bEstConforme = this.controleurContenu.estDeTaille(strTmp, taille);
                            }
                            else if (operateur == CritereDiscriminant.CRITERE_TAILLE_MINIMUM)
                            {
                                int tailleMinimale = Integer.valueOf(critere.getValeur()).intValue();
                                bEstConforme = this.controleurContenu.estDeTailleMinimale(strTmp, tailleMinimale);
                            }
                            else if (operateur == CritereDiscriminant.CRITERE_TAILLE_MAXIMUM)
                            {
                                int tailleMaximale = Integer.valueOf(critere.getValeur()).intValue();
                                critere.getValeur();
                                bEstConforme = this.controleurContenu.estDeTailleMaximale(strTmp, tailleMaximale);
                            }
                            else
                            {
                                String commencePar = critere.getValeur();
                                bEstConforme =
                                    controleCommencePar(strTmp, commencePar, operateur, critere.isIgnoreCase());
                            }
                        }

                        else if ((operateur >= CritereDiscriminant.BASE_CRITERE_TYPE)
                            && (operateur <= CritereDiscriminant.BASE_FIN_CRITERE_TYPE))
                        {
                            // Gestion des critères de type de la valeur
                            if (operateur == CritereDiscriminant.CRITERE_EST_ALPHABETIQUE)
                            {
                                bEstConforme =
                                    this.controleurContenu.estComposeDeJeuDeCaracteres(
                                        ControleurContenu.JEU_CARACTERE_ALPHABETIQUE, strTmp);
                            }
                            else if (operateur == CritereDiscriminant.CRITERE_EST_ALPHANUMERIQUE)
                            {
                                bEstConforme =
                                    this.controleurContenu.estComposeDeJeuDeCaracteres(
                                        ControleurContenu.JEU_CARACTERE_ALPHANUMERIQUE, strTmp);
                            }
                            else if (operateur == CritereDiscriminant.CRITERE_EST_NUMERIQUE)
                            {
                                bEstConforme =
                                    this.controleurContenu.estComposeDeJeuDeCaracteres(
                                        ControleurContenu.JEU_CARACTERE_NUMERIQUE, strTmp);
                            }
                        }
                        // Gestion des critères de comparaison
                        else if ((operateur >= CritereDiscriminant.BASE_CRITERE_COMPARAISON)
                            && (operateur <= CritereDiscriminant.BASE_FIN_CRITERE_COMPARAISON))
                        {
                            int comparaison;
                            if (critere.isIgnoreCase())
                            {
                                // Comparaison sans contrôle de la casse.
                                comparaison = critere.getValeur().compareToIgnoreCase(strTmp);
                            }
                            else
                            {
                                comparaison = critere.getValeur().compareTo(strTmp);
                            }
                            // Analyse des résultats de la comparaison
                            if (isconforme1(comparaison, operateur)
                                || isconforme2(comparaison, operateur))
                            {
                                bEstConforme = true;
                            }
                        }
                    }
                }
            }
        }

        return bEstConforme;
    }
    
    /**
     * Méthode permettant d'effectuer un test de conformité
     *
     * @param comparaison comparaison
     * @param operateur opérateur
     * @return true si le test est conforme, false sinon
     */
    private boolean isconforme1(int comparaison, int operateur)
    {
        return (comparaison == 0)
            && ((operateur == CritereDiscriminant.CRITERE_EGAL)
                || (comparaison == CritereDiscriminant.CRITERE_EGAL_OU_INFERIEUR)
                || (comparaison == CritereDiscriminant.CRITERE_EGAL_OU_SUPERIEUR));
    }

    /**
     * Méthode permettant d'effectuer un autre test de conformité
     *
     * @param comparaison comparaison
     * @param operateur opérateur
     * @return true si le test est conforme, false sinon
     */
    private boolean isconforme2(int comparaison, int operateur)
    {
        return (((comparaison > 0) && ((operateur == CritereDiscriminant.CRITERE_SUPERIEUR) 
            || (operateur == CritereDiscriminant.CRITERE_EGAL_OU_SUPERIEUR)))
            || ((comparaison < 0) && ((operateur == CritereDiscriminant.CRITERE_INFERIEUR) 
            || (operateur == CritereDiscriminant.CRITERE_EGAL_OU_INFERIEUR)))
            || ((comparaison != 0) && (operateur == CritereDiscriminant.CRITERE_DIFFERENT)));
    }
    
    /**
     * Accesseur de l attribut caractere separation.
     * 
     * @return caractere separation
     */
    public String getCaractereSeparation()
    {
        return caractereSeparation;
    }

    /**
     * Cette méthode retourne le contenu complet d'une ligne de fichier source, ligne étant entendu comme un élément du
     * fichier source et donc éventuellement sur plusieurs lignes.
     * 
     * @param pSeparateur le separateur
     * @return Chaine de caractères contenant le contenu éventuellement sur plusieurs lignes
     */
    public String getContenuCompletLigne(String pSeparateur)
    {
        String retour = contenuLigne;

        if (this.contenuLigneApresLigne1 != null)
        {
            Iterator iter = this.contenuLigneApresLigne1.iterator();
            while (iter.hasNext())
            {
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append(retour);
                stringBuilder.append(pSeparateur);
                stringBuilder.append((String) iter.next());
                retour = stringBuilder.toString();
            }
        }
        return retour;
    }

    /**
     * Accesseur de l attribut contenu ligne.
     * 
     * @return contenu ligne
     */
    public String getContenuLigne()
    {
        return contenuLigne;
    }

    /**
     * Accesseur de l attribut elements chaine.
     * 
     * @return elements chaine
     */
    public List getElementsChaine()
    {
        return elementsChaine;
    }

    /**
     * Accesseur de l attribut numero ligne.
     * 
     * @return numero ligne
     */
    public int getNumeroLigne()
    {
        return numeroLigne;
    }

    /**
     * Accesseur de l attribut proprietes a mapper.
     * 
     * @return proprietes a mapper
     */
    public Map getProprietesAMapper()
    {
        return proprietesAMapper;
    }

    /**
     * Accesseur de l attribut type separation.
     * 
     * @return type separation
     */
    public Integer getTypeSeparation()
    {
        return typeSeparation;
    }

    /**
     * Cette méthode extrait de la ligne les propriétés listée dans le fichier de mapping et les positionne dans une
     * liste.
     * 
     * @param listePropriete --
     * @param pFichierParametrage le fichier parametrage
     */
    public void initialiserProprietesAMapper(List listePropriete, FichierParametrage pFichierParametrage)
    {
        this.proprietesAMapper = new HashMap();
        if (listePropriete != null)
        {
            Iterator iter = listePropriete.iterator();
            while (iter.hasNext())
            {
                ProprieteLigne propriete = (ProprieteLigne) iter.next();
                // Est ce qu'on est dans le standard où on a une seule ligne?
                if (propriete.getLigne() == 1)
                {
                    String donneeFichier = null;

                    if (this.typeSeparation.intValue() == FichierParametrage.TYPE_FICHIER_SEPARATEUR)
                    {
                        if (propriete.getPosition() <= this.elementsChaine.size())
                        {
                            donneeFichier = (String) this.elementsChaine.get(propriete.getPosition() - 1);
                        }
                    }
                    else
                    {
                        donneeFichier = this.extractionChaineDepuisPropriete(this.contenuLigne, propriete);
                    }

                    if (donneeFichier != null)
                    {
                        if (propriete.getOperationARealiser() != OperationPropriete.OPE_AUCUNE)
                        {
                            donneeFichier =
                                this.realisationOperationSurPropriete(donneeFichier, propriete.getOperationARealiser());
                        }
                        this.proprietesAMapper.put(propriete.getId(), donneeFichier);
                    }
                }
                else
                // cas spécial des éléments sur plusieurs lignes
                {
                    if (this.typeSeparation.intValue() == FichierParametrage.TYPE_FICHIER_SEPARATEUR)
                    {
                        List listTmp = (ArrayList) this.elementsChaineApresLigne1.get(propriete.getLigne() - 2);
                        if (propriete.getPosition() <= listTmp.size())
                        {
                            this.proprietesAMapper.put(propriete.getId(), listTmp.get(propriete.getPosition() - 1));
                        }
                    }
                    else
                    {
                        String strTmp = (String) this.contenuLigneApresLigne1.get(propriete.getLigne() - 2);
                        this.proprietesAMapper.put(propriete.getId(), this.extractionChaineDepuisPropriete(strTmp,
                            propriete));
                    }
                }
            }
        }
    }

    /**
     * Cette méthode retourne la valeur d'une propriété présente dans la ligne grace à son nom de mapping.
     * 
     * @param pNom le nom
     * @return the string
     */
    public String retournerProprieteMappeeParNom(String pNom)
    {
        return (String) this.proprietesAMapper.get(pNom);
    }

    /**
     * Modificateur de l attribut caractere separation.
     * 
     * @param caractereSeparation le nouveau caractere separation
     */
    public void setCaractereSeparation(String caractereSeparation)
    {
        this.caractereSeparation = caractereSeparation;
    }

    /**
     * Modificateur de l attribut contenu ligne.
     * 
     * @param contenuLigne le nouveau contenu ligne
     */
    public void setContenuLigne(String contenuLigne)
    {
        this.contenuLigne = contenuLigne;
    }

    /**
     * Modificateur de l attribut numero ligne.
     * 
     * @param numeroLigne le nouveau numero ligne
     */
    public void setNumeroLigne(int numeroLigne)
    {
        this.numeroLigne = numeroLigne;
    }

    /**
     * Modificateur de l attribut type separation.
     * 
     * @param typeSeparation le nouveau type separation
     */
    public void setTypeSeparation(Integer typeSeparation)
    {
        this.typeSeparation = typeSeparation;
    }

    /**
     * Ajouter element dans la liste.
     * 
     * @param pChaine le chaine
     * @param pSupprimerDoubleGuillemet le supprimer double guillemet
     * @param pListe le liste
     */
    private void ajouterElementDansLaListe(String pChaine, boolean pSupprimerDoubleGuillemet, List pListe)
    {
        if (pSupprimerDoubleGuillemet)
        {
            pChaine = pChaine.replaceAll("\"\"", "\"");
        }
        pListe.add(pChaine);

    }

    /**
     * Controle commence par.
     * 
     * @param pValeur le valeur
     * @param pCommencePar le commence par
     * @param pOperateur le operateur
     * @param pIgnoreCase le ignore case
     * @return true, if controle commence par
     */
    private boolean controleCommencePar(String pValeur, String pCommencePar, int pOperateur, boolean pIgnoreCase)
    {
        if (pValeur.length() >= pCommencePar.length())
        {
            // On regarde donc (et en fonction de l'activation du contrôle de la casse ou non) si la chaine démarre par
            // la valeur spécifiée.
            if (pIgnoreCase)
            {
                if (pValeur.substring(0, pCommencePar.length()).equalsIgnoreCase(pCommencePar))
                {
                    return (pOperateur == CritereDiscriminant.CRITERE_COMMENCE_PAR);
                }
                else
                {
                    return (pOperateur == CritereDiscriminant.CRITERE_NE_COMMENCE_PAS_PAR);
                }

            }
            else
            {
                if (pValeur.substring(0, pCommencePar.length()).equals(pCommencePar))
                {
                    return (pOperateur == CritereDiscriminant.CRITERE_COMMENCE_PAR);
                }
                else
                {
                    return (pOperateur == CritereDiscriminant.CRITERE_NE_COMMENCE_PAS_PAR);
                }
            }
        }

        return false;
    }

    /**
     * Decoupage csv.
     * 
     * @param pChaineADecouper le chaine a decouper
     * @param pSeparateur le separateur
     * @param gestionGuillemets --
     * @param pNumeroLigne le numero ligne
     */
    private void decoupageCsv(String pChaineADecouper, String pSeparateur, boolean gestionGuillemets, int pNumeroLigne)
    {
        final int debut = 0;
        final int fin = 1;

        List listeElements;
        if (pNumeroLigne == 1)
        {
            this.elementsChaine = new ArrayList();
            listeElements = this.elementsChaine;
        }
        else
        {
            listeElements = new ArrayList();
            this.elementsChaineApresLigne1.add(listeElements);
        }

        if ((pChaineADecouper != null) && (pChaineADecouper.compareTo("") != 0))
        {

            boolean pasFini = true;

            char[] contenuChaine = pChaineADecouper.toCharArray();
            char separateur = pSeparateur.charAt(0);

            int positionEnCours = 0;
            int dernierePosition = 0;
            boolean chaineCommenceeParGuillemets = false;
            int guillemetDebutOuFin = fin;
            boolean suppressionDoubleGuillemet = false;

            while (pasFini)
            {
                boolean guillemetTrouve = false;
                boolean separateurTrouve = false;

                // On parcourt la chaine à la recherche de la première instance de
                // guillemet ou de séparateur
                for (int i = positionEnCours; i < pChaineADecouper.length(); i++)
                {
                    positionEnCours = i;
                    if (contenuChaine[i] == separateur)
                    {
                        separateurTrouve = true;
                        break;
                    }
                    else if ((gestionGuillemets) && (contenuChaine[i] == '"'))
                    {
                        guillemetTrouve = true;
                        break;
                    }
                }
                if (separateurTrouve || guillemetTrouve)
                {
                    if (separateurTrouve)
                    {
                        if (!chaineCommenceeParGuillemets)
                        {
                            ajouterElementDansLaListe(pChaineADecouper.substring(dernierePosition, positionEnCours),
                                suppressionDoubleGuillemet, listeElements);
                            dernierePosition = positionEnCours + 1; // on passe le caractère de séparation
                            positionEnCours++;
                            suppressionDoubleGuillemet = false;
                        }
                        else
                        {
                            positionEnCours++;
                        }
                    }
                    else
                    {
                        if (chaineCommenceeParGuillemets)
                        {
                            guillemetDebutOuFin = (guillemetDebutOuFin == fin ? debut : fin);

                            if ((positionEnCours + 1) < pChaineADecouper.length())
                            {
                                char caractereSuivant = contenuChaine[positionEnCours + 1];
                                if (caractereSuivant == '"')
                                {
                                    guillemetDebutOuFin = (guillemetDebutOuFin == fin ? debut : fin);
                                    suppressionDoubleGuillemet = true;
                                    positionEnCours += 2;
                                }
                                else if (caractereSuivant == separateur)
                                {
                                    if (guillemetDebutOuFin == debut)
                                    {
                                        positionEnCours++;
                                    }
                                    else
                                    {
                                        ajouterElementDansLaListe(pChaineADecouper.substring(dernierePosition + 1,
                                            positionEnCours), suppressionDoubleGuillemet, listeElements);
                                        positionEnCours += 2;
                                        dernierePosition = positionEnCours;
                                        chaineCommenceeParGuillemets = false;
                                        suppressionDoubleGuillemet = false;
                                    }
                                }
                                else
                                {
                                    positionEnCours++;
                                }
                            }
                            else
                            {
                                pasFini = false;
                                ajouterElementDansLaListe(pChaineADecouper.substring(dernierePosition + 1,
                                    positionEnCours), suppressionDoubleGuillemet, listeElements);
                                suppressionDoubleGuillemet = false;
                            }
                        }
                        else
                        {
                            // Ce morceau commence par un guillemet, donc on le note.
                            if (positionEnCours == dernierePosition)
                            {
                                chaineCommenceeParGuillemets = true;
                                guillemetDebutOuFin = debut;
                                positionEnCours++;
                            }
                            else
                            {
                                // // gestion des erreurs sinon on ne sort jamais du while
                                try
                                {
                                    pasFini = false;
                                    throw new ExploitationException("Erreur de découpage csv du fichier.");
                                }
                                catch (ExploitationException exception)
                                {
                                    throw new ExploitationException(exception.getMessage(), exception);
                                }
                            }
                        }
                    }

                }
                else
                {
                    if (chaineCommenceeParGuillemets)
                    {
                        if (guillemetDebutOuFin == fin)
                        {
                            ajouterElementDansLaListe(pChaineADecouper.substring(dernierePosition + 1,
                                positionEnCours - 1), suppressionDoubleGuillemet, listeElements);
                        }
                        else
                        { // //gestion des erreurs sinon on ne sort jamais du while
                            try
                            {
                                pasFini = false;
                                throw new ExploitationException("Erreur de découpage csv du fichier.");
                            }
                            catch (ExploitationException exception)
                            {
                                throw new ExploitationException(exception.getMessage(), exception);
                            }
                        }
                    }
                    else
                    {
                        ajouterElementDansLaListe(pChaineADecouper.substring(dernierePosition),
                            suppressionDoubleGuillemet, listeElements);
                    }
                    pasFini = false;
                }
            }

        }
    }

    /**
     * Cette méthode réalise l'extraction de la chaine de caractères entre les positions indiquées par le critère.
     * 
     * @param pContenuLigne le contenu ligne
     * @param pCritere le critere
     * @return the string
     */
    private String extractionChaineDepuisCritere(String pContenuLigne, CritereDiscriminant pCritere)
    {
        int tailleChaine = pContenuLigne.length();
        int positionDebut = pCritere.getPositionDebut() - 1;

        if (positionDebut > tailleChaine)
        {
            return "";
        }
        int positionFin = pCritere.getPositionFin();

        if (positionFin > tailleChaine)
        {
            positionFin = tailleChaine;
        }

        return pContenuLigne.substring(positionDebut, positionFin);
    }

    /**
     * Extraction chaine depuis propriete.
     * 
     * @param pContenuLigne le contenu ligne
     * @param pPropriete le propriete
     * @return the string
     */
    private String extractionChaineDepuisPropriete(String pContenuLigne, ProprieteLigne pPropriete)
    {
        int tailleChaine = pContenuLigne.length();
        int positionDebut = pPropriete.getPositionDebut() - 1;

        if (positionDebut > tailleChaine)
        {
            return "";
        }
        int positionFin = pPropriete.getPositionFin();

        if (positionFin > tailleChaine)
        {
            positionFin = tailleChaine;
        }

        return pContenuLigne.substring(positionDebut, positionFin);
    }

    /**
     * appel d'une opération sur une propriété lue dans le fichier.
     * 
     * @param pValeur le valeur
     * @param pOperation le operation
     * @return the string
     */
    private String realisationOperationSurPropriete(String pValeur, int pOperation)
    {
        switch (pOperation)
        {
            case OperationPropriete.OPE_DEFORMATAGE_NUMERIQUE_FORMAT_ANGLAIS:
                pValeur = OutilsString.deformatageNumeriqueFormatAnglais(pValeur);
                break;
            case OperationPropriete.OPE_VIDE_SI_TOUT_ZERO:
                pValeur = OutilsString.videSiToutAZero(pValeur);
                break;
            case OperationPropriete.OPE_VIDE_SI_TOUT_ZERO_OU_ESPACE:
                pValeur = OutilsString.videSiToutAZeroOuEspace(pValeur);
                break;
            case OperationPropriete.OPE_INSERTION_SEPARATEUR_DECIMAL_POSITION_1:
                pValeur = OutilsString.insertionSeparateurDecimal(pValeur, ".", 1);
                break;
            case OperationPropriete.OPE_INSERTION_SEPARATEUR_DECIMAL_POSITION_2:
                pValeur = OutilsString.insertionSeparateurDecimal(pValeur, ".", 2);
                break;
            case OperationPropriete.OPE_INSERTION_SEPARATEUR_DECIMAL_POSITION_3:
                pValeur = OutilsString.insertionSeparateurDecimal(pValeur, ".", 3);
                break;
            case OperationPropriete.OPE_MAJUSCULE:
                pValeur = pValeur.toUpperCase(Locale.FRANCE);
                break;
            case OperationPropriete.OPE_MINUSCULE:
                pValeur = pValeur.toLowerCase(Locale.FRANCE);
                break;

        }
        return pValeur;
    }

}
