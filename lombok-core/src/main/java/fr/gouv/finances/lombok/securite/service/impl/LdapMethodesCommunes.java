/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : LdapMethodesCommunes.java
 *
 */
package fr.gouv.finances.lombok.securite.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Class LdapMethodesCommunes
 * 
 * @author amleplatinec
 * @version $Revision: 1.3 $ Date: 11 déc. 2009
 */
public class LdapMethodesCommunes
{

    public LdapMethodesCommunes()
    {
        super();         
    }

    /**
     * sert pour faire de srecherches dans l'annuaire portant sur un trop grand nombre de personnes exemple: recehrche
     * de spzersonnes habilitées sur une application et sur un profil retourne une liste de caractéres alphabétiques de
     * 26*27 champs ( exemple : A',AA,AB,.....AZ) plus 26 champs avec espace : "A " "B "
     * 
     * @return the list< string>
     */
    public List<String> calculerPrefixe()
    {

        List<String> lesLettres = new ArrayList<>();
        List<String> uneListeNom = new ArrayList<>();

        String nom = null;
        String nomBlanc = null;

        lesLettres.add("A");
        lesLettres.add("B");
        lesLettres.add("C");
        lesLettres.add("D");
        lesLettres.add("E");
        lesLettres.add("F");
        lesLettres.add("G");
        lesLettres.add("H");
        lesLettres.add("I");
        lesLettres.add("J");
        lesLettres.add("K");
        lesLettres.add("L");
        lesLettres.add("M");
        lesLettres.add("N");
        lesLettres.add("O");
        lesLettres.add("P");
        lesLettres.add("Q");
        lesLettres.add("R");
        lesLettres.add("S");
        lesLettres.add("T");
        lesLettres.add("U");
        lesLettres.add("V");
        lesLettres.add("W");
        lesLettres.add("X");
        lesLettres.add("Y");
        lesLettres.add("Z");

        for (Iterator<String> iterator = lesLettres.iterator(); iterator.hasNext();)
        {
            String unelettre1 = (String) iterator.next();

            String unelettre1avecapostrophe = unelettre1.concat("'");
            uneListeNom.add(unelettre1avecapostrophe);
            for (Iterator<String> iterator1 = lesLettres.iterator(); iterator1.hasNext();)
            {
                String unelettre2 = (String) iterator1.next();
                nomBlanc = unelettre1;
                nomBlanc = nomBlanc.concat(" ");
                nomBlanc = nomBlanc.concat(unelettre2);
                uneListeNom.add(nomBlanc);
                nom = unelettre1;
                nom = nom.concat(unelettre2);
                uneListeNom.add(nom);
            }
        }

        return uneListeNom;

    }

    /**
     * recherche sur 3 caracteres si jamais size limit à100.
     * 
     * @return list
     */
    public List<String> calculerPrefixeCasRepartiteur()
    {

        List<String> lesLettres = new ArrayList<>();
        List<String> uneListeNom = new ArrayList<>();

        String nom = null;
        String nomBlanc = null;

        lesLettres.add("A");
        lesLettres.add("B");
        lesLettres.add("C");
        lesLettres.add("D");
        lesLettres.add("E");
        lesLettres.add("F");
        lesLettres.add("G");
        lesLettres.add("H");
        lesLettres.add("I");
        lesLettres.add("J");
        lesLettres.add("K");
        lesLettres.add("L");
        lesLettres.add("M");
        lesLettres.add("N");
        lesLettres.add("O");
        lesLettres.add("P");
        lesLettres.add("Q");
        lesLettres.add("R");
        lesLettres.add("S");
        lesLettres.add("T");
        lesLettres.add("U");
        lesLettres.add("V");
        lesLettres.add("W");
        lesLettres.add("X");
        lesLettres.add("Y");
        lesLettres.add("Z");

        for (Iterator<String> iterator = lesLettres.iterator(); iterator.hasNext();)
        {
            String unelettre1 = (String) iterator.next();

            String unelettre1avecapostrophe = unelettre1.concat("'");
            uneListeNom.add(unelettre1avecapostrophe);
            for (Iterator<String> iterator1 = lesLettres.iterator(); iterator1.hasNext();)
            {
                String unelettre2 = (String) iterator1.next();
                for (Iterator<String> iterator2 = lesLettres.iterator(); iterator2.hasNext();)
                {
                    String unelettre3 = (String) iterator2.next();
                    nomBlanc = unelettre1;
                    nomBlanc = nomBlanc.concat(" ");
                    nomBlanc = nomBlanc.concat(unelettre2);
                    nomBlanc = nomBlanc.concat(unelettre3);
                    uneListeNom.add(nomBlanc);
                    nom = unelettre1;
                    nom = nom.concat(unelettre2);
                    nom = nom.concat(unelettre3);
                    uneListeNom.add(nom);
                }
            }
        }

        return uneListeNom;

    }
}
