/*
 * Copyright (c) 2014 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.clamav.bean;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Classe correspondant à la réponse de clamAV.
 */
public class ClamAvResponse
{
    /** Initialisation de la journalisation. */
    protected static final Logger LOGGER = LoggerFactory.getLogger(ClamAvResponse.class);

    /**
     * Position du nom du virus dans la réponse.
     */
    private static final int POS_NOM_VIRUS = 11;

    /**
     * Catégorie des réponses de ClamAV.
     */
    public enum CategorieReponse
    {
        /** Service non présent. */
        NO_SERVICE,
        /** Le service clamAV répond */
        PING_OK,
        /** Timeout. */
        TIMEOUT,
        /** Virus. */
        VIRUS,
        /** Pas de virus. */
        NO_VIRUS,
        /** Statistiques. */
        STATS,
        /** Version. */
        VERSION,
        /** Fichier vide. */
        FICHIER_VIDE;
    }
    
    /**
     * Etat des réponses de ClamAV.
     */
    public enum EtatReponse
    {
        /** Etat OK. */
        OK,
        /** Etat KO. */
        KO;
    }

    /** Réponse retournée par ClamAV. */
    private String response;

    /** Etat de la réponse : virus ou non. */
    private EtatReponse etat;

    /** Nom du virus. */
    private String virusName;

    /** Statistique ClamAV. */
    private String stats;

    /** version ClamAV. */
    private String version;

    /** categorieReponse. */
    private CategorieReponse categorieReponse;

    /**
     * Constructeur.
     */
    public ClamAvResponse()
    {
        super();
    }
    
    /**
     * Réponse ClamAV.
     * 
     * @return la réponse
     */
    public final String getResponse()
    {
        return this.response;
    }

    /**
     * ajout de la réponse.
     * 
     * @param response la réponse
     */
    public final void setResponse(
        final String response)
    {
        this.response = response;
    }

    /**
     * Etat de la réponse.
     * 
     * @return l'état
     */
    public final EtatReponse getEtat()
    {
        return this.etat;
    }

    /**
     * Nom du virus.
     * 
     * @return le virus
     */
    public final String getVirusName()
    {
        return this.virusName;
    }

    /**
     * Récupération des stats clamAV.
     * 
     * @return les statistiques
     */
    public final String getStats()
    {
        return this.stats;
    }

    /**
     * Récupération de la version de ClamAV.
     * 
     * @return la version
     */
    public final String getVersion()
    {

        return this.version;
    }

    /**
     * A utiliser pour savoir si un virus a été détecté.
     * 
     * @return the categorieReponse
     */
    public final CategorieReponse getCategorieReponse()
    {

        return this.categorieReponse;
    }

    /**
     * Méthode permettant de créer une réponse à un "PING"
     *
     * @param resultat requête reçue
     * @return réponse adaptée à la requête
     */
    public static final ClamAvResponse createPingResponse(final String resultat)
    {
        LOGGER.debug("Traitement de la réponse liée à un ping");

        ClamAvResponse response;
        response = new ClamAvResponse();
        response.setResponse(resultat);
        response.categorieReponse = CategorieReponse.PING_OK;

        return response;
    }

    /**
     * Creer une instance à partir d'une réponse d'analyse virale. Si on a le mot OK sans le mot FOUND alors c'est qu'il
     * n'y a pas de virus, ni d'erreur
     * 
     * @param resultat resultat
     * @return ClamAvResponse
     */
    public static final ClamAvResponse createVirusResponse(
        final String resultat)
    {
        final int indiceNomVirus = 7;
        if (ClamAvResponse.LOGGER.isDebugEnabled())
        {
            ClamAvResponse.LOGGER.debug(" entree createVirusResponse");
        }
        ClamAvResponse response;
        response = new ClamAvResponse();
        response.setResponse(resultat);
        if (resultat.length() != 0)
        {
            if ((resultat.lastIndexOf("FOUND") != -1)
                || (resultat.indexOf("OK") == -1))
            {
                // le nom du virus comme après le message
                // "1: stream: "
                // soit à la 11ième position
                response.etat = EtatReponse.KO;
                response.virusName = resultat.substring(
                    ClamAvResponse.POS_NOM_VIRUS, resultat.length() - indiceNomVirus);
                response.categorieReponse = CategorieReponse.VIRUS;
            }
            else
            {
                response.etat = EtatReponse.OK;
                response.virusName = "Pas de virus trouvé";
                response.categorieReponse = CategorieReponse.NO_VIRUS;
            }
        }

        return response;
    }

    /**
     * Creer une instance à partir d'une réponse issue de la commande stats.
     * 
     * @param resultat resultat
     * @return ClamAvResponse
     */
    public static final ClamAvResponse createStatsResponse(
        final String resultat)
    {
        if (ClamAvResponse.LOGGER.isDebugEnabled())
        {
            ClamAvResponse.LOGGER.debug(" entree createStatsResponse");
        }
        ClamAvResponse response;
        response = new ClamAvResponse();
        response.setResponse(resultat);
        response.categorieReponse = CategorieReponse.STATS;
        response.stats = resultat;

        return response;
    }

    /**
     * Creer une instance à partir d'une réponse issue de la commande version.
     * 
     * @param resultat resultat
     * @return ClamAvResponse
     */
    public static final ClamAvResponse createVersionResponse(
        final String resultat)
    {
        if (ClamAvResponse.LOGGER.isDebugEnabled())
        {
            ClamAvResponse.LOGGER.debug(" entree createVersionResponse");
        }
        ClamAvResponse response;
        response = new ClamAvResponse();
        response.setResponse(resultat);
        response.version = resultat;
        response.categorieReponse = CategorieReponse.VERSION;

        return response;
    }

    /**
     * Créer une instance représentant un appel en timeout.
     * 
     * @return ClamAvResponse
     */
    public static final ClamAvResponse createTimeoutResponse()
    {
        if (ClamAvResponse.LOGGER.isDebugEnabled())
        {
            ClamAvResponse.LOGGER.debug(" entree createTimeoutResponse");
        }
        ClamAvResponse response;
        response = new ClamAvResponse();
        response.categorieReponse = CategorieReponse.TIMEOUT;
        return response;
    }

    /**
     * Créer une instance représentant un appel en échec suite à un service non opérationnel .
     * 
     * @return ClamAvResponse
     */
    public static final ClamAvResponse createNoServiceResponse()
    {
        LOGGER.debug("Création d'une réponse de catégorie '{}'", CategorieReponse.NO_SERVICE.name());
        ClamAvResponse response = new ClamAvResponse();
        response.categorieReponse = CategorieReponse.NO_SERVICE;
        return response;
    }

    /**
     * Méthode permettant de créer une réponse faisant suite à une absence de flux
     *
     * @return la réponse faisant suite à une absence de flux
     */
    public static final ClamAvResponse createNoFileResponse()
    {
        LOGGER.debug("Création d'une réponse de catégorie '{}'", CategorieReponse.FICHIER_VIDE.name());
        ClamAvResponse response = new ClamAvResponse();
        response.categorieReponse = CategorieReponse.FICHIER_VIDE;
        return response;
    }

    /**
     * Méthode d'impression.
     * 
     * @return l'état de la réponse
     */
    @Override
    public final String toString()
    {

        final StringBuilder reponse = new StringBuilder(
            "ClamAvResponse : \n");
        if (this.categorieReponse != null)
        {
            reponse.append(this.categorieReponse.toString());
        }
        if (this.etat != null)
        {
            reponse.append("\netat=" + this.etat);
        }
        if (this.response != null)
        {
            reponse.append("\nresponse=" + this.response);
        }
        if (this.stats != null)
        {
            reponse.append("\nstats=" + this.stats);
        }
        if (this.version != null)
        {
            reponse.append("\nversion=" + this.version);
        }
        if (this.virusName != null)
        {
            reponse.append("\nvirusName=" + this.virusName);
        }
        return reponse.toString();
    }

}
