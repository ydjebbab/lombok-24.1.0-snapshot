/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : BarreDeMenusHierarchiques.java
 *
 */
package fr.gouv.finances.lombok.menu.techbean;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import fr.gouv.finances.lombok.util.base.BaseTechBean;

/**
 * Class BarreDeMenusHierarchiques.
 * 
 * @author lcontinsouzas-cp Classe représentant une barre de menu hierarchique Les attributs mappés sur des données XML
 *         doivent rester public à cause de l'utilisation du mécanisme d'introspection
 * @version $Revision: 1.2 $ Date: 15 déc. 2009
 */
public class BarreDeMenusHierarchiques extends BaseTechBean
{

    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** liste de sous menus. */
    public List<ElementDeMenu> listeDeSousMenus = new ArrayList<>();

    /** defaut afficher rubriques avec url non autorisee. */
    public String defautAfficherRubriquesAvecURLNonAutorisee = "N";

    /** nom. */
    public String nom = "barremenu";

    /**
     * Constructeur de la classe BarreDeMenusHierarchiques.java
     *
     */
    public BarreDeMenusHierarchiques()
    {
        super();
        
    }
    
    public BarreDeMenusHierarchiques(List<ElementDeMenu> listeDeSousMenus, String defautAfficherRubriquesAvecURLNonAutorisee, String nom)
    {
        super();
        this.listeDeSousMenus = listeDeSousMenus;
        this.defautAfficherRubriquesAvecURLNonAutorisee = defautAfficherRubriquesAvecURLNonAutorisee;
        this.nom = nom;
    }

    /**
     * permet de mettre à jour la propriété EstAffichable de tous les éléments du menu.
     * 
     * @throws MenuProgrammationException the menu programmation exception
     */
    public void calculerEstAffichablePourTousLesElements() throws MenuProgrammationException
    {
        donnerListedesElements(false, true);
    }

    /**
     * Transforme la structure hierarchique du menu en liste d'objets de type iterateur.
     * 
     * @return un iterateur contenant tous objets rattachés à l'objet menu dans l'ordre de construction
     * @throws MenuProgrammationException the menu programmation exception
     */
    public Iterator itererElementsDeMenu() throws MenuProgrammationException
    {
        log.debug("debut appel à la liste de tous les éléments du menu");
        return donnerListedesElements(false, false).iterator();
    }

    /**
     * Transforme la structure hierarchique du menu en liste d'objet de type iterateur.
     * 
     * @return un iterateur contenant tous objets affichables (en fonction de l'accessiblité des URL et des options
     *         d'affichage par défaut retenues) rattachés à l'objet menu dans l'ordre de construction
     * @throws MenuProgrammationException the menu programmation exception
     */
    public Iterator itererElementsDeMenuAffichables() throws MenuProgrammationException
    {
        log.debug("debut appel à la liste des éléments affichables");
        return donnerListedesElements(false, true).iterator();
    }

    /**
     * Transforme la structure hierarchique du menu en liste d'objets de type iterateur.
     * 
     * @return un iterateur contenant tous les objets RubriquesLien rattachés à l'objet menu dans l'ordre de
     *         construction
     * @throws MenuProgrammationException the menu programmation exception
     */
    public Iterator itererRubriquesLien() throws MenuProgrammationException
    {
        log.debug("debut appel à la liste des rubriques lien");
        return donnerListedesElements(true, false).iterator();
    }

    /**
     * methode Donner listedes elements : --.
     * 
     * @param rubriquesliensseulement --
     * @param elementsaffichablesseulement --
     * @return list
     * @throws MenuProgrammationException the menu programmation exception
     */
    private List donnerListedesElements(boolean rubriquesliensseulement, boolean elementsaffichablesseulement)
        throws MenuProgrammationException
    {
        List<ElementDeMenu> listeobjets = new ArrayList<>();
        List<ElementDeMenu> uneliste;
        Iterator itlistedegroupederubriques = listeDeSousMenus.iterator();

        /* parcours de la liste de sous menus */
        try
        {
            while (itlistedegroupederubriques.hasNext())
            {
                SousMenu unsousmenu = (SousMenu) (itlistedegroupederubriques.next());
                log.debug("Sous menu trouvé :" + unsousmenu.libelle);
                uneliste = unsousmenu.donnerListedesRubriques(rubriquesliensseulement, elementsaffichablesseulement);
                if (!elementsaffichablesseulement || (unsousmenu.getEstAffichable()))
                {
                    if (!rubriquesliensseulement)
                    {
                        listeobjets.add(unsousmenu);
                        log.debug("Sous-menu ajoutee à la liste :" + unsousmenu.libelle);
                    }
                    listeobjets.addAll(uneliste);
                    log.debug("Rubriques rattachees au sous-menu " + unsousmenu.libelle + " ajoutées à la liste");
                }
            }
        }
        catch (ClassCastException e)
        {
            throw new MenuProgrammationException(
                "Les éléments de menu sont mal reliés entre eux vérifiez les imbrications Menu-1--n-GroupeDeRubriques-1--n-Rubriquexxx et RubriquesSousMenu-1--n-Rubriquexxx",
                e);
        }

        return listeobjets;
    }
    
    @Override
    public Object clone() {
        try {
            return (BarreDeMenusHierarchiques) super.clone();
        } catch (CloneNotSupportedException e) {
            return new BarreDeMenusHierarchiques(this.listeDeSousMenus, this.defautAfficherRubriquesAvecURLNonAutorisee, this.nom);
        }
    }

}
