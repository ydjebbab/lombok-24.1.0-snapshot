/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 *
 */
package fr.gouv.finances.lombok.mvc.jasperreports;

import net.sf.jasperreports.engine.JRExporter;
import net.sf.jasperreports.engine.export.JRTextExporter;

import org.springframework.web.servlet.view.jasperreports.AbstractJasperReportsSingleFormatView;

/**
 * Class CpJasperReportsTextView.
 * 
 * @author amleplatinec
 */
public class CpJasperReportsTextView extends AbstractJasperReportsSingleFormatView
{

    public CpJasperReportsTextView()
    {
        setContentType("application/txt");
    }

    /**
     * Surcharge de la méthode pour la rendre finale, pour que l'appel, au sein du constructeur,
     * ne soit plus une mauvaise pratique puisqu'elle ne peut plus ainsi être surchargée dans les
     * classes dérivées.
     *
     * @see org.springframework.web.servlet.view.AbstractView#setContentType(java.lang.String)
     */
    @Override
    public final void setContentType(String contentType)
    {
        super.setContentType(contentType);
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @return jR exporter
     * @see org.springframework.web.servlet.view.jasperreports.AbstractJasperReportsSingleFormatView#createExporter()
     */
    @Override
    protected JRExporter createExporter()
    {
        return new JRTextExporter();
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @return true, si c'est vrai
     * @see org.springframework.web.servlet.view.jasperreports.AbstractJasperReportsSingleFormatView#useWriter()
     */
    @Override
    protected boolean useWriter()
    {
        return false;
    }

}
