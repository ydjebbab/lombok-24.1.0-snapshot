/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 */
package fr.gouv.finances.lombok.util.exception;

/**
 * Sous-classe de VerrouillageOptimisteException qui permet de distinguer les suppressions de données.
 * 
 * @see VerrouillageOptimisteException
 * @author chouard-cp
 * @version $Revision: 1.2 $ Date: 15 déc. 2009
 */
public class VerrouillageOptimisteSuppressionException extends VerrouillageOptimisteException
{

    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** Constant : message. */
    private static final String message = "Un autre utilisateur a supprimé les données sur lesquelles vous travaillez";

    /**
     * Instanciation de verrouillage optimiste suppression exception.
     */
    public VerrouillageOptimisteSuppressionException()
    {
        this(message);
    }

    /**
     * Instanciation de verrouillage optimiste suppression exception.
     * 
     * @param arg0 --
     */
    public VerrouillageOptimisteSuppressionException(String arg0)
    {
        super(arg0);
    }

    /**
     * Instanciation de verrouillage optimiste suppression exception.
     * 
     * @param arg0 --
     * @param arg1 --
     */
    public VerrouillageOptimisteSuppressionException(String arg0, Throwable arg1)
    {
        super(arg0, arg1);
    }

    /**
     * Instanciation de verrouillage optimiste suppression exception.
     * 
     * @param arg0 --
     */
    public VerrouillageOptimisteSuppressionException(Throwable arg0)
    {
        this(message, arg0);
    }

}
