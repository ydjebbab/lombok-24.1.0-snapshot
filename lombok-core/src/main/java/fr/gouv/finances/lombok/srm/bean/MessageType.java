/*
 * Copyright (c) 2011 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.srm.bean;

import java.util.Objects;

import fr.gouv.finances.lombok.util.base.BaseBean;

/**
 * Objet métier MessageType.
 * 
 * @author amleplatinec
 * @author Christophe Breheret-Girardin
 */
public class MessageType extends BaseBean
{
    /** Initialisation d'un UID. */
    private static final long serialVersionUID = 128937436594908320L;

    /** id. */
    private Long id;

    /** version. */
    private int version;

    /** libelle message type. */
    private String libelleMessageType;

    /**
     * Constructeur.
     */
    public MessageType()
    {
        super();
    }

    /** 
     * {@inheritDoc}
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode()
    {
        return Objects.hash(libelleMessageType);
    }

    /** 
     * {@inheritDoc}
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj)
    {
        if (obj == null)
        {
            return false;
        }

        if (this.getClass() != obj.getClass())
        {
            return false;
        }

        MessageType messageType = (MessageType) obj;
        return Objects.equals(libelleMessageType, messageType.libelleMessageType);
    }

    /**
     * Accesseur de l attribut id.
     * 
     * @return id
     */
    public Long getId()
    {
        return id;
    }

    /**
     * Accesseur de l attribut libelle message type.
     * 
     * @return libelle message type
     */
    public String getLibelleMessageType()
    {
        return libelleMessageType;
    }

    /**
     * Accesseur de l attribut version.
     * 
     * @return version
     */
    public int getVersion()
    {
        return version;
    }

    /**
     * Modificateur de l attribut id.
     * 
     * @param id
     *            le nouveau id
     */
    public void setId(Long id)
    {
        this.id = id;
    }

    /**
     * Modificateur de l attribut libelle message type.
     * 
     * @param libelleMessageType
     *            le nouveau libelle message type
     */
    public void setLibelleMessageType(String libelleMessageType)
    {
        this.libelleMessageType = libelleMessageType;
    }

    /**
     * Modificateur de l attribut version.
     * 
     * @param version
     *            le nouveau version
     */
    public void setVersion(int version)
    {
        this.version = version;
    }

}
