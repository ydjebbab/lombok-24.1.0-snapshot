/*
 * Copyright (c) 2016 DGFiP - Tous droits réservés
 * 
 */
package fr.gouv.finances.lombok.test.transaction;

import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionException;
import org.springframework.transaction.TransactionStatus;

/**
 * 
 * Bouchon de gestionnaire de transaction pour faciliter les tests
 *
 * @author Christophe Brehret-Girardin
 */
public class MockTransactionManager implements PlatformTransactionManager
{

    /** {@inheritDoc} */
    @Override
    public TransactionStatus getTransaction(TransactionDefinition definition) throws TransactionException
    {
        return null;
    }

    /** {@inheritDoc} */
    @Override
    public void commit(TransactionStatus status) throws TransactionException
    {
        // Ne rien faire
    }

    /** {@inheritDoc} */
    @Override
    public void rollback(TransactionStatus status) throws TransactionException
    {
        // Ne rien faire
    }

}