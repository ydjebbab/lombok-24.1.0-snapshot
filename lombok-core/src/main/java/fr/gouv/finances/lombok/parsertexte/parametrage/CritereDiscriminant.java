/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 */
package fr.gouv.finances.lombok.parsertexte.parametrage;

/**
 * Class CritereDiscriminant --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.2 $ Date: 11 déc. 2009
 */
public class CritereDiscriminant
{

    /** Constant : BASE_CRITERE_COMPARAISON. */
    public static final int BASE_CRITERE_COMPARAISON = 0;

    /** Constant : CRITERE_EGAL. */
    public static final int CRITERE_EGAL = 0;

    /** Constant : CRITERE_SUPERIEUR. */
    public static final int CRITERE_SUPERIEUR = 1;

    /** Constant : CRITERE_INFERIEUR. */
    public static final int CRITERE_INFERIEUR = 2;

    /** Constant : CRITERE_EGAL_OU_SUPERIEUR. */
    public static final int CRITERE_EGAL_OU_SUPERIEUR = 3;

    /** Constant : CRITERE_EGAL_OU_INFERIEUR. */
    public static final int CRITERE_EGAL_OU_INFERIEUR = 4;

    /** Constant : CRITERE_DIFFERENT. */
    public static final int CRITERE_DIFFERENT = 5;

    /** Constant : BASE_FIN_CRITERE_COMPARAISON. */
    public static final int BASE_FIN_CRITERE_COMPARAISON = 9;

    /** Constant : BASE_CRITERE_CHAINE. */
    public static final int BASE_CRITERE_CHAINE = 10;

    /** Constant : CRITERE_COMMENCE_PAR. */
    public static final int CRITERE_COMMENCE_PAR = 10;

    /** Constant : CRITERE_NE_COMMENCE_PAS_PAR. */
    public static final int CRITERE_NE_COMMENCE_PAS_PAR = 11;

    /** Constant : CRITERE_TAILLE_EGALE. */
    public static final int CRITERE_TAILLE_EGALE = 12;

    /** Constant : CRITERE_TAILLE_MINIMUM. */
    public static final int CRITERE_TAILLE_MINIMUM = 13;

    /** Constant : CRITERE_TAILLE_MAXIMUM. */
    public static final int CRITERE_TAILLE_MAXIMUM = 14;

    /** Constant : BASE_FIN_CRITERE_CHAINE. */
    public static final int BASE_FIN_CRITERE_CHAINE = 19;

    // Critères basé sur le type de la valeur à contrôler ==> à partir de 20
    /** Constant : BASE_CRITERE_TYPE. */
    public static final int BASE_CRITERE_TYPE = 20;

    /** Constant : CRITERE_EST_NUMERIQUE. */
    public static final int CRITERE_EST_NUMERIQUE = 20;

    /** Constant : CRITERE_EST_ALPHABETIQUE. */
    public static final int CRITERE_EST_ALPHABETIQUE = 21;

    /** Constant : CRITERE_EST_ALPHANUMERIQUE. */
    public static final int CRITERE_EST_ALPHANUMERIQUE = 22;

    /** Constant : BASE_FIN_CRITERE_TYPE. */
    public static final int BASE_FIN_CRITERE_TYPE = 29;

    // position de ce critère dans la ligne
    /** position debut. */
    int positionDebut = -1;

    // position de fin de ce critère dans la ligne (dans le cas d'un fichier en positionnel)
    /** position fin. */
    int positionFin = -1;

    /** position. */
    int position = -1;

    /** operateur. */
    int operateur = CRITERE_EGAL;

    // Ce critère répondre Vrai si on est sur la ligne ayant ce n°. Cela permet de gérer les fichiers
    // avec bandeau ou info à des endroits prédeterminés
    /** numero ligne. */
    int numeroLigne = -1;

    // Si ce booleen est positionne ce critère est toujours vrai.
    /** toujours vrai. */
    boolean toujoursVrai = false;

    // les comparaisons de ce critères doivent elles se faire en ignorant la casse?
    /** ignore case. */
    boolean ignoreCase = false;

    // suppression des espaces avant et apres avant de faire la comparaison?
    /** suppression espaces autour. */
    boolean suppressionEspacesAutour = false;

    // Valeur permettant de savoir si le critère discriminant est ok.
    /** valeur. */
    String valeur = null;

    /**
     * Instanciation de critere discriminant.
     */
    public CritereDiscriminant()
    {

    }

    /**
     * Instanciation de critere discriminant.
     * 
     * @param pToujoursVrai le toujours vrai
     */
    public CritereDiscriminant(boolean pToujoursVrai)
    {
        this.toujoursVrai = pToujoursVrai;
    }

    /**
     * Instanciation de critere discriminant.
     * 
     * @param pNumeroLigne le numero ligne
     */
    public CritereDiscriminant(int pNumeroLigne)
    {
        this.numeroLigne = pNumeroLigne;
    }

    /**
     * Instanciation de critere discriminant.
     * 
     * @param pPositionDebut le position debut
     * @param pPositionFin le position fin
     * @param pValeur le valeur
     */
    public CritereDiscriminant(int pPositionDebut, int pPositionFin, String pValeur)
    {
        this.positionDebut = pPositionDebut;
        this.positionFin = pPositionFin;
        this.valeur = pValeur;
    }

    /**
     * Instanciation de critere discriminant.
     * 
     * @param pPositionDebut le position debut
     * @param pPositionFin le position fin
     * @param pValeur le valeur
     * @param pOperateur le operateur
     */
    public CritereDiscriminant(int pPositionDebut, int pPositionFin, String pValeur, int pOperateur)
    {
        this.positionDebut = pPositionDebut;
        this.positionFin = pPositionFin;
        this.valeur = pValeur;
        this.operateur = pOperateur;
    }

    /**
     * Instanciation de critere discriminant.
     * 
     * @param pPosition le position
     * @param pValeur le valeur
     */
    public CritereDiscriminant(int pPosition, String pValeur)
    {
        this.position = pPosition;
        this.valeur = pValeur;
    }

    /**
     * Accesseur de l attribut numero ligne.
     * 
     * @return numero ligne
     */
    public int getNumeroLigne()
    {
        return numeroLigne;
    }

    /**
     * Accesseur de l attribut operateur.
     * 
     * @return operateur
     */
    public int getOperateur()
    {
        return operateur;
    }

    /**
     * Accesseur de l attribut position.
     * 
     * @return position
     */
    public int getPosition()
    {
        return position;
    }

    /**
     * Accesseur de l attribut position debut.
     * 
     * @return position debut
     */
    public int getPositionDebut()
    {
        return positionDebut;
    }

    /**
     * Accesseur de l attribut position fin.
     * 
     * @return position fin
     */
    public int getPositionFin()
    {
        return positionFin;
    }

    /**
     * Accesseur de l attribut valeur.
     * 
     * @return valeur
     */
    public String getValeur()
    {
        return valeur;
    }

    /**
     * Verifie si ignore case.
     * 
     * @return true, si c'est ignore case
     */
    public boolean isIgnoreCase()
    {
        return ignoreCase;
    }

    /**
     * Verifie si suppression espaces autour.
     * 
     * @return true, si c'est suppression espaces autour
     */
    public boolean isSuppressionEspacesAutour()
    {
        return suppressionEspacesAutour;
    }

    /**
     * Verifie si toujours vrai.
     * 
     * @return true, si c'est toujours vrai
     */
    public boolean isToujoursVrai()
    {
        return toujoursVrai;
    }

    /**
     * Modificateur de l attribut ignore case.
     * 
     * @param ignoreCase le nouveau ignore case
     */
    public void setIgnoreCase(boolean ignoreCase)
    {
        this.ignoreCase = ignoreCase;
    }

    /**
     * Modificateur de l attribut numero ligne.
     * 
     * @param numeroLigne le nouveau numero ligne
     */
    public void setNumeroLigne(int numeroLigne)
    {
        this.numeroLigne = numeroLigne;
    }

    /**
     * Modificateur de l attribut operateur.
     * 
     * @param operateur le nouveau operateur
     */
    public void setOperateur(int operateur)
    {
        this.operateur = operateur;
    }

    /**
     * Modificateur de l attribut position.
     * 
     * @param position le nouveau position
     */
    public void setPosition(int position)
    {
        this.position = position;
    }

    /**
     * Modificateur de l attribut position debut.
     * 
     * @param positionDebut le nouveau position debut
     */
    public void setPositionDebut(int positionDebut)
    {
        this.positionDebut = positionDebut;
    }

    /**
     * Modificateur de l attribut position fin.
     * 
     * @param positionFin le nouveau position fin
     */
    public void setPositionFin(int positionFin)
    {
        this.positionFin = positionFin;
    }

    /**
     * Modificateur de l attribut suppression espaces autour.
     * 
     * @param suppressionEspacesAutour le nouveau suppression espaces autour
     */
    public void setSuppressionEspacesAutour(boolean suppressionEspacesAutour)
    {
        this.suppressionEspacesAutour = suppressionEspacesAutour;
    }

    /**
     * Modificateur de l attribut toujours vrai.
     * 
     * @param toujoursVrai le nouveau toujours vrai
     */
    public void setToujoursVrai(boolean toujoursVrai)
    {
        this.toujoursVrai = toujoursVrai;
    }

    /**
     * Modificateur de l attribut valeur.
     * 
     * @param valeur le nouveau valeur
     */
    public void setValeur(String valeur)
    {
        this.valeur = valeur;
    }

}
