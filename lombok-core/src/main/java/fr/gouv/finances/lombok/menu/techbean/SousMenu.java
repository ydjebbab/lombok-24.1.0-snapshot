/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : SousMenu.java
 *
 */
package fr.gouv.finances.lombok.menu.techbean;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Class SousMenu Classe représentant un sous menu dans une barre de menu hierarchique (idem "file" ou "edit" dans la
 * barre de menu d'Eclipse).
 * 
 * @author lcontinsouzas-cp
 * @version $Revision: 1.2 $ Date: 15 déc. 2009
 */
public class SousMenu extends ElementDeMenu
{

    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** defaut afficher si url non autorisee. */
    public String defautAfficherSiURLNonAutorisee = "N";

    /** liste de rubriques. */
    public List listeDeRubriques = new ArrayList();

    public SousMenu()
    {
        super();           
    }

    /**
     * methode Donner listedes rubriques : --.
     * 
     * @param rubriquesliensseulement --
     * @param elementsaffichablesseulement --
     * @return list< element de menu>
     * @throws MenuProgrammationException the menu programmation exception
     */
    public List<ElementDeMenu> donnerListedesRubriques(boolean rubriquesliensseulement,
        boolean elementsaffichablesseulement) throws MenuProgrammationException
    {
        List<ElementDeMenu> listeobjets = new ArrayList<ElementDeMenu>();
        Iterator itlistederubriques = this.listeDeRubriques.iterator();

        while (itlistederubriques.hasNext())
        {
            Rubrique unerubrique = (Rubrique) itlistederubriques.next();
            listeobjets.addAll(unerubrique.donnerListeDesRubriques(rubriquesliensseulement,
                elementsaffichablesseulement));
        }
        if (elementsaffichablesseulement)
        {
            this.calculerEstAffichable(listeobjets.isEmpty());
        }
        return listeobjets;
    }

}
