/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : BaseBean.java
 *
 */
package fr.gouv.finances.lombok.util.base;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlType;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Class BaseBean
 * 
 * @author amleplatinec
 * @version $Revision: 1.3 $ Date: 11 déc. 2009
 */
@XmlType(namespace = "http://base.util.lombok.finances.gouv.fr")
public abstract class BaseBean implements Serializable
{
    private static final long serialVersionUID = -605967453729553083L;

    protected final Log log = LogFactory.getLog(getClass());

    /** HASHCOD e_ has h_ init. */
    protected final int hashCODEHASHINIT = 7;

    /** HASHCOD e_ has h_ mult. */
    protected final int hashCODEHASHMULT = 31;

    public BaseBean()
    {

    }

}
