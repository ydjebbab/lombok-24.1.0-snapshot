/*
 * Copyright (c) 2014 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.util.persistance;

/**
 * Valeurs possibles pour une recherche liée à des critères.
 *
 * @author Christophe Breheret-Girardin
 */
public enum ModeCritereRecherche
{
    /** Recherche large sur le début. */
    LIKE_DEBUT,
    /** Recherche large sur le début, sans casse. */
    LIKE_DEBUT_IGNORE_CASSE,
    /** Recherche très large. */
    LIKE_ANYWHERE,
    /** Recherche très large, sans casse. */
    LIKE_ANYWHERE_IGNORE_CASSE,
    /** Recherche large sur la fin. */
    LIKE_FIN,
    /** Recherche large sur la fin, sans casse. */
    LIKE_FIN_IGNORE_CASSE,
    /** Recherche stricte. */
    EQUAL,
    /** Recherche stricte, sans casse. */
    EQUAL_IGNORE_CASSE,
    /** Recherche excluante. */
    NOT_EQUAL,
    /** Recherche de non initialisation. */
    IS_NULL;
}
