/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : GenerateurMenuJSCookServiceImpl.java
 *
 */
package fr.gouv.finances.lombok.menu.service.impl;

import java.util.Iterator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.gouv.finances.lombok.menu.service.GenerateurMenuService;
import fr.gouv.finances.lombok.menu.techbean.BarreDeMenusHierarchiques;
import fr.gouv.finances.lombok.menu.techbean.ElementDeMenu;
import fr.gouv.finances.lombok.menu.techbean.MenuProgrammationException;
import fr.gouv.finances.lombok.menu.techbean.Rubrique;
import fr.gouv.finances.lombok.menu.techbean.RubriqueLien;
import fr.gouv.finances.lombok.menu.techbean.RubriqueSousMenu;
import fr.gouv.finances.lombok.menu.techbean.SousMenu;
import fr.gouv.finances.lombok.menu.techbean.SousMenuLien;

/**
 * Class GenerateurMenuJSCookServiceImpl.
 * 
 * @author chouard-cp
 * @version $Revision: 1.2 $ Date: 15 déc. 2009
 */
public class GenerateurMenuJSCookServiceImpl implements GenerateurMenuService
{
    /** Initialisation de la journalisation. */
    protected static final Logger LOGGER = LoggerFactory.getLogger(GenerateurMenuJSCookServiceImpl.class);

    /** COMPLEMEN t_ fi n_ sousmenu. */
    private static String complementFINSOUSMENU = "_cmSplit,\n";

    /** DEBU t_ barremen u_1. */
    private static String debutBARREMENU1 = "var ";

    /** DEBU t_ barremen u_2. */
    private static String debutBARREMENU2 = " =\n[";

    /** DEBU t_ element. */
    private static String debutELEMENT = "\n[";

    /** FI n_ barremenu. */
    private static String finBARREMENU = "];";

    /** FI n_ element. */
    private static String finELEMENT = "]";

    /** SEPA r_ propriete. */
    private static String separPROPRIETE = " ,";

    /** SEPA r_ element. */
    private static String separELEMENT = ",";

    /** GUILLEMET. */
    private static String guillemet = "'";

    /** PROPRIET e_ null. */
    private static String proprieteNULL = "null";

    /** TARGE t_ null. */
    private static String targetNULL = "_self";

    /**
     * Constructeur de la classe GenerateurMenuJSCookServiceImpl.java
     */
    public GenerateurMenuJSCookServiceImpl()
    {
        super();

    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param unmenu
     * @return string
     * @see fr.gouv.finances.lombok.menu.service.GenerateurMenuService#genererBarreDeMenu(fr.gouv.finances.lombok.menu.techbean.BarreDeMenusHierarchiques)
     */
    @Override
    public String genererBarreDeMenu(BarreDeMenusHierarchiques unmenu)
    {
        StringBuilder generation = new StringBuilder(100);

        if (unmenu == null)
        {
            throw new IllegalArgumentException();
        }

        SousMenu unsousmenu;

        generation.append(debutBARREMENU1);
        generation.append(unmenu.nom);
        generation.append(debutBARREMENU2);

        Iterator<ElementDeMenu> listesousmenus = unmenu.listeDeSousMenus.iterator();
        boolean premiersousmenu = true;

        while (listesousmenus.hasNext())
        {
            try
            {
                unsousmenu = (SousMenu) listesousmenus.next();
                if (!premiersousmenu && unsousmenu.getEstAffichable())
                {
                    generation.append(separELEMENT);
                    generation.append(complementFINSOUSMENU);
                }
                if (unsousmenu.getEstAffichable())
                {
                    // Patch suite demande de Caen lien direct depuis un sous menu
                    if (unsousmenu instanceof SousMenuLien)
                    {
                        SousMenuLien unsousmenulien = (SousMenuLien) (unsousmenu);
                        generation.append(this.ecrireElement(unsousmenu, unsousmenulien.donnerUrlLien(), unsousmenulien
                            .donnerTargetLien()));

                        generation.append(finELEMENT);
                        premiersousmenu = false;
                    }
                    else
                    {
                        // Fin patch

                        generation.append(this.ecrireElement(unsousmenu));
                        generation.append(this.genererContenuSousMenu(unsousmenu));
                        generation.append(finELEMENT);

                        premiersousmenu = false;
                    }
                }
            }
            catch (ClassCastException e)
            {
                throw new MenuProgrammationException(
                    "La barre de menu contient des objets qui ne sont pas correctement imbriqués", e);
            }
        }
        generation.append(finBARREMENU);
        return generation.toString();
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param unmenu HTML
     * @return string : liste HTML (
     *         <ul>
     *         <li>)
     * @see fr.gouv.finances.lombok.menu.service.GenerateurMenuService#genererBarreDeMenu(fr.gouv.finances.lombok.menu.techbean.BarreDeMenusHierarchiques)
     */
    @Override
    public String genererBarreDeMenuHtml(BarreDeMenusHierarchiques unmenu)
    {
        String generationHtml = "";

        if (unmenu == null)
        {
            throw new IllegalArgumentException();
        }

        SousMenu unsousmenu;

        Iterator<ElementDeMenu> listesousmenus = unmenu.listeDeSousMenus.iterator();
        boolean premiersousmenu = true;

        while (listesousmenus.hasNext())
        {
            try
            {
                unsousmenu = (SousMenu) listesousmenus.next();

                if (unsousmenu.getEstAffichable())
                {
                    // Patch suite demande de Caen lien direct depuis un sous menu
                    if (unsousmenu instanceof SousMenuLien)
                    {
                        SousMenuLien unsousmenulien = (SousMenuLien) (unsousmenu);

                        generationHtml += "<li class='genererBarreDeMenu " + unsousmenu.getEstAffichable() + "'>";
                        generationHtml +=
                            this.ecrireElementHtml(unsousmenu, unsousmenulien.donnerUrlLien(), unsousmenulien.donnerTargetLien());
                        generationHtml += "</li>";

                        premiersousmenu = false;
                    }
                    else
                    {
                        generationHtml += this.ecrireElementEtSousMenuHtml(unsousmenu);
                        premiersousmenu = false;
                    }
                }
            }
            catch (ClassCastException e)
            {
                throw new MenuProgrammationException(
                    "La barre de menu contient des objets qui ne sont pas correctement imbriqués", e);
            }
        }

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("<ul>");
        stringBuilder.append(generationHtml);
        stringBuilder.append("</ul>");
        return stringBuilder.toString();
    }

    /**
     * methode Ecrire element : --.
     * 
     * @param unelement --
     * @return string
     */
    private String ecrireElement(ElementDeMenu unelement)
    {
        return this.ecrireElement(unelement, null, null);
    }

    /**
     * methode Ecrire element : --.
     * 
     * @param unelement --
     * @param url --
     * @param target --
     * @return string
     */
    private String ecrireElement(ElementDeMenu unelement, String url, String target)
    {
        String targetUse;

        if (target == null)
        {
            targetUse = targetNULL;
        }
        else
        {
            targetUse = target;
        }
        StringBuilder generationelement = new StringBuilder(30);
        generationelement.append(debutELEMENT);
        generationelement.append(this.ecrirePropriete(unelement.urlicone));
        generationelement.append(separPROPRIETE);
        generationelement.append(this.ecrirePropriete(unelement.libelle));
        generationelement.append(separPROPRIETE);
        generationelement.append(this.ecrirePropriete(url));
        generationelement.append(separPROPRIETE);
        generationelement.append(this.ecrirePropriete(targetUse));
        generationelement.append(separPROPRIETE);
        generationelement.append(this.ecrirePropriete(unelement.commentaire));

        return generationelement.toString();
    }

    /**
     * methode Ecrire element d'une liste HTML (
     * <ul>
     * <li>): --.
     * 
     * @param unelement --
     * @return string
     */
    private String ecrireElementEtRubriqueSousMenuHtml(ElementDeMenu unelement)
    {
        return this.ecrireElementEtSousMenuHtml(unelement, null, null);
    }

    /**
     * methode Ecrire element d'une liste HTML (
     * <ul>
     * <li>) : --.
     * 
     * @param unelement --
     * @return string
     */
    private String ecrireElementEtSousMenuHtml(ElementDeMenu unelement)
    {
        return this.ecrireElementEtSousMenuHtml(unelement, null, null);
    }

    /**
     * methode Ecrire element d'une liste HTML (
     * <ul>
     * <li>): --.
     * 
     * @param unelement --
     * @param url --
     * @param target --
     * @return string
     */
    private String ecrireElementEtSousMenuHtml(ElementDeMenu unelement, String url, String target)
    {

        String targetUse = (target == null) ? targetNULL : target;
        String tmpclass = (url == null) ? "nourl" : "";
        url = (url == null) ? "tabindex=\"0\"" : "href=\"" + url + "\"";

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("<li class='parent ecrireElementEtSousMenuHtml'>");
        stringBuilder.append("<a ");
        stringBuilder.append(url);
        stringBuilder.append(" target='");
        stringBuilder.append(targetUse);
        stringBuilder.append("'");
        stringBuilder.append(" class='");
        stringBuilder.append(tmpclass);
        stringBuilder.append("'>");
        stringBuilder.append(unelement.libelle);
        stringBuilder.append("</a>");
        stringBuilder.append(this.genererContenuSousMenuHtml((SousMenu) unelement));
        stringBuilder.append("</li>");
        return stringBuilder.toString();

    }

    /**
     * methode Ecrire element d'une liste HTML (
     * <ul>
     * <li>): --.
     * 
     * @param unelement --
     * @return string
     */
    private String ecrireElementHtml(ElementDeMenu unelement)
    {
        return this.ecrireElementHtml(unelement, null, null);
    }

    /**
     * methode Ecrire element d'une liste HTML (
     * <ul>
     * <li>): --.
     * 
     * @param unelement --
     * @param url --
     * @param target --
     * @return string
     */
    private String ecrireElementHtml(ElementDeMenu unelement, String url, String target)
    {

        String targetUse = (target == null) ? targetNULL : target;
        String tmpclass = (url == null) ? "nourl" : "";
        url = (url == null) ? "tabindex=\"0\"" : "href=\"" + url + "\"";

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("<a ");
        stringBuilder.append(url);
        stringBuilder.append(" target='");
        stringBuilder.append(targetUse);
        stringBuilder.append("'");
        stringBuilder.append(" class='");
        stringBuilder.append(tmpclass);
        stringBuilder.append("'>");
        stringBuilder.append(unelement.libelle);
        stringBuilder.append("</a>");
        return stringBuilder.toString();

    }

    /**
     * methode Ecrire propriete : --.
     * 
     * @param propriete --
     * @return string
     */
    private String ecrirePropriete(String propriete)
    {
        StringBuilder generationpropriete = new StringBuilder();
        if ((propriete == null) || "".equalsIgnoreCase(propriete))
        {
            generationpropriete.append(proprieteNULL);
        }
        else
        {
            propriete = protegeguillemet(propriete);
            generationpropriete.append(guillemet);
            generationpropriete.append(propriete);
            generationpropriete.append(guillemet);
        }
        return generationpropriete.toString();
    }

    /**
     * methode Generer contenu sous menu : --.
     * 
     * @param unsousmenu --
     * @return string
     */
    private String genererContenuSousMenu(SousMenu unsousmenu)
    {
        StringBuilder generation = new StringBuilder();
        Rubrique unerubrique = null;
        Iterator listerubriques = unsousmenu.listeDeRubriques.iterator();

        while (listerubriques.hasNext())
        {
            unerubrique = (Rubrique) listerubriques.next();
            if (unerubrique.getEstAffichable())
            {
                generation = generation.append(separELEMENT);
                generation = generation.append(this.genererRubriques(unerubrique));
            }
        }
        return generation.toString();
    }

    /**
     * methode Generer contenu sous menu HTML : --.
     * 
     * @param unsousmenu --
     * @return string
     */
    private String genererContenuSousMenuHtml(SousMenu unsousmenu)
    {
        String generationHtml = "";
        Rubrique unerubrique = null;
        Iterator listerubriques = unsousmenu.listeDeRubriques.iterator();

        while (listerubriques.hasNext())
        {
            unerubrique = (Rubrique) listerubriques.next();
            if (unerubrique.getEstAffichable())
            {
                generationHtml += this.genererRubriquesHtml(unerubrique);
            }
        }
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("<ul>");
        stringBuilder.append(generationHtml);
        stringBuilder.append("</ul>");
        return stringBuilder.toString();
    }

    /**
     * methode Generer rubriques : --.
     * 
     * @param unerubrique --
     * @return string
     */
    private String genererRubriques(Rubrique unerubrique)
    {
        StringBuilder generation = new StringBuilder();
        RubriqueLien unerubriquelien;
        RubriqueSousMenu unerubriquesousmenu;

        try
        {
            unerubriquelien = (RubriqueLien) unerubrique;
            if (unerubriquelien.getEstAffichable())
            {
                generation.append(this.ecrireElement(unerubriquelien, unerubriquelien.url, unerubriquelien.target));
                generation.append(finELEMENT);
            }

        }
        catch (ClassCastException cce)
        {
            LOGGER.debug("Exception détectée", cce);

            unerubriquesousmenu = (RubriqueSousMenu) unerubrique;
            if (unerubriquesousmenu.getEstAffichable())
            {
                generation.append(this.ecrireElement(unerubriquesousmenu));
                Iterator listerubriques = unerubriquesousmenu.listeDeRubriques.iterator();
                while (listerubriques.hasNext())
                {
                    generation.append(separELEMENT);
                    generation.append(this.genererRubriques((Rubrique) listerubriques.next()));
                }
                generation.append(finELEMENT);
            }
        }
        return generation.toString();
    }

    /**
     * methode Generer rubriques HTML : --.
     * 
     * @param unerubrique --
     * @return string
     */
    private String genererRubriquesHtml(Rubrique unerubrique)
    {
        StringBuilder generation = new StringBuilder();
        RubriqueLien unerubriquelien;
        RubriqueSousMenu unerubriquesousmenu;
        String generationHtml = "";

        try
        {
            unerubriquelien = (RubriqueLien) unerubrique;
            if (unerubriquelien.getEstAffichable())
            {
                generationHtml +=
                    "<li  class='try genererRubriquesHtml'>"
                        + this.ecrireElementHtml(unerubriquelien, unerubriquelien.url, unerubriquelien.target) + "</li>";
            }

        }
        catch (ClassCastException cce)
        {
            LOGGER.debug("Exception détectée", cce);

            unerubriquesousmenu = (RubriqueSousMenu) unerubrique;
            if (unerubriquesousmenu.getEstAffichable())
            {
                generationHtml += "<li class='parent catch genererRubriquesHtml'>";

                generationHtml += this.ecrireElementHtml(unerubriquesousmenu);

                Iterator listerubriques = unerubriquesousmenu.listeDeRubriques.iterator();

                generationHtml += "<ul>";

                while (listerubriques.hasNext())
                {
                    generationHtml += this.genererRubriquesHtml((Rubrique) listerubriques.next());
                }

                generationHtml += "</ul>";
                generationHtml += "</li>";
            }
        }
        return generationHtml;
    }

    /**
     * methode Protegeguillemet : --.
     * 
     * @param propriete --
     * @return string
     */
    private String protegeguillemet(String propriete)
    {
        // Suppression des anteslashs devant les guillemets si déjà protégés
        String proprietesansantislash = propriete.replaceAll("\\'", "'");
        // Ajout d'un anteslash devant tous les guillemets
        return proprietesansantislash.replaceAll("'", "\\\\'");
    }
}
