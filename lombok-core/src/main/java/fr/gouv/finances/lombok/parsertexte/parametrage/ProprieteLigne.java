/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 */
package fr.gouv.finances.lombok.parsertexte.parametrage;

import fr.gouv.finances.lombok.parsertexte.integration.OperationPropriete;

/**
 * Class ProprieteLigne --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.2 $ Date: 11 déc. 2009
 */
public class ProprieteLigne
{

    // position de début ce critère dans la ligne
    /** position debut. */
    int positionDebut = -1;

    // position de fin de ce critère dans la ligne (dans le cas d'un fichier en positionnel)
    /** position fin. */
    int positionFin = -1;

    // position de cette propriété dans la ligne si on est en fichier par séparateur
    /** position. */
    int position = -1;

    /** ligne. */
    int ligne = 1;

    /** id. */
    String id = null;

    /** operation a realiser. */
    int operationARealiser = OperationPropriete.OPE_AUCUNE;

    /**
     * Instanciation de propriete ligne.
     */
    public ProprieteLigne()
    {
    }

    /**
     * Instanciation de propriete ligne.
     * 
     * @param pId le id
     * @param pPosition le position
     */
    public ProprieteLigne(String pId, int pPosition)
    {
        this.id = pId;
        this.position = pPosition;
    }

    /**
     * Instanciation de propriete ligne.
     * 
     * @param pId le id
     * @param pPositionDebut le position debut
     * @param pPositionFin le position fin
     */
    public ProprieteLigne(String pId, int pPositionDebut, int pPositionFin)
    {
        this.id = pId;
        this.positionDebut = pPositionDebut;
        this.positionFin = pPositionFin;
    }

    /**
     * Accesseur de l attribut id.
     * 
     * @return id
     */
    public String getId()
    {
        return id;
    }

    /**
     * Accesseur de l attribut ligne.
     * 
     * @return ligne
     */
    public int getLigne()
    {
        return ligne;
    }

    /**
     * Accesseur de l attribut operation a realiser.
     * 
     * @return operation a realiser
     */
    public int getOperationARealiser()
    {
        return operationARealiser;
    }

    /**
     * Accesseur de l attribut position.
     * 
     * @return position
     */
    public int getPosition()
    {
        return position;
    }

    /**
     * Accesseur de l attribut position debut.
     * 
     * @return position debut
     */
    public int getPositionDebut()
    {
        return positionDebut;
    }

    /**
     * Accesseur de l attribut position fin.
     * 
     * @return position fin
     */
    public int getPositionFin()
    {
        return positionFin;
    }

    /**
     * Modificateur de l attribut id.
     * 
     * @param id le nouveau id
     */
    public void setId(String id)
    {
        this.id = id;
    }

    /**
     * Modificateur de l attribut ligne.
     * 
     * @param ligne le nouveau ligne
     */
    public void setLigne(int ligne)
    {
        this.ligne = ligne;
    }

    /**
     * Modificateur de l attribut operation a realiser.
     * 
     * @param operationARealiser le nouveau operation a realiser
     */
    public void setOperationARealiser(int operationARealiser)
    {
        this.operationARealiser = operationARealiser;
    }

    /**
     * Modificateur de l attribut position.
     * 
     * @param position le nouveau position
     */
    public void setPosition(int position)
    {
        this.position = position;
    }

    /**
     * Modificateur de l attribut position debut.
     * 
     * @param positionDebut le nouveau position debut
     */
    public void setPositionDebut(int positionDebut)
    {
        this.positionDebut = positionDebut;
    }

    /**
     * Modificateur de l attribut position fin.
     * 
     * @param positionFin le nouveau position fin
     */
    public void setPositionFin(int positionFin)
    {
        this.positionFin = positionFin;
    }

}
