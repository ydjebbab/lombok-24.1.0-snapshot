/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : AuthentificationEtLectureHabilitationsService.java
 *
 */
package fr.gouv.finances.lombok.securite.service;

import fr.gouv.finances.lombok.securite.techbean.PersonneAnnuaire;

/**
 * Interface AuthentificationEtLectureHabilitationsService.
 * 
 * @author lcontinsouzas-cp
 * @version $Revision: 1.2 $ Date: 15 déc. 2009
 */
public interface AuthentificationEtLectureHabilitationsService
{

    /**
     * Verifie l'uid/mot de passe d'une personne dans l'annuaire.
     * 
     * @param UID --
     * @param motDePasse --
     * @return the string
     */
    public String authentifier(String UID, String motDePasse);

    /**
     * Verifie l'uid/mot de passe d'une personne dans l'annuaire, verifie son habiliration sur l'application puis
     * redonne les caracteristiques de la personne.
     * 
     * @param UID --
     * @param motDePasse --
     * @param application --
     * @return the personne annuaire
     */
    public PersonneAnnuaire authentifierEtRechercherHabilitation(String UID, String motDePasse, String application);

    /**
     * Recherche une personne dans l'annuaire et verifie qu'elle dispose d'une habilitation pour l'application
     * concernee.
     * 
     * @param DN --
     * @param application --
     * @return the personne annuaire
     */
    public PersonneAnnuaire rechercherPersonneAnnuaireAvecHabilitationParDN(String DN, String application);

    /**
     * recherche d'un particulier avec son habilitation par le dn.
     * 
     * @param dn "documenté"
     * @return personne annuaire
     */
    public PersonneAnnuaire rechercherPersonneAnnuaireParticuliersAvecHabilitationParDN(String dn);

    /**
     * recherche juste d'une personne pour ensuite créer son habilitaion de toute piece (cas usagers ).
     * 
     * @param dn "documenté"
     * @return personne annuaire
     */
    public PersonneAnnuaire rechercherPersonneAnnuaireSansHabilitationParDN(String dn);

}