/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 */
package fr.gouv.finances.lombok.securite.techbean;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import fr.gouv.finances.lombok.util.base.BaseTechBean;

/**
 * Class HabilitationAnnuaire.
 * 
 * @author lcontinsouzas-cp Classe qui reprend les attributs d'une habilitation , selon son format défini dans Aptera
 * @version $Revision: 1.3 $ Date: 15 déc. 2009
 */
public class HabilitationAnnuaire extends BaseTechBean
{

    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** date creation. */
    private Date dateCreation;

    /** date debut. */
    private Date dateDebut;

    /** date fin. */
    private Date dateFin;

    /** libelle court appli. */
    private String libelleCourtAppli;

    /** nom administrateur. */
    private String nomAdministrateur;

    /** nom profil. */
    private String nomProfil;

    /** liste filtres. */
    private Set listeFiltres;

    /**
     * Constructeur de la classe HabilitationAnnuaire.java
     *
     */
    public HabilitationAnnuaire()
    {
        super();  
        
    }

    /**
     * renvoit true si l'habilitation n'a aucun attribut autre que profil / libelle appli de rempli amlp sept 2011.
     * 
     * @return true, si c'est vrai
     */
    public boolean estFormatCourt()
    {
        boolean estFormatCourt = true;
        if (dateCreation != null || dateDebut != null || dateFin != null || nomAdministrateur != null || listeFiltres != null)
        {
            estFormatCourt = false;
        }
        return estFormatCourt;
    }

    /**
     * Accesseur de l attribut date creation.
     * 
     * @return date creation
     */
    public Date getDateCreation()
    {
        return dateCreation;
    }

    /**
     * Accesseur de l attribut date debut.
     * 
     * @return date debut
     */
    public Date getDateDebut()
    {
        return dateDebut;
    }

    /**
     * Accesseur de l attribut date fin.
     * 
     * @return date fin
     */
    public Date getDateFin()
    {
        return dateFin;
    }

    /**
     * Accesseur de l attribut libelle court appli.
     * 
     * @return libelle court appli
     */
    public String getLibelleCourtAppli()
    {
        return libelleCourtAppli;
    }

    /**
     * Accesseur de l attribut liste filtres.
     * 
     * @return liste filtres
     */
    public Set getListeFiltres()
    {
        return listeFiltres;
    }

    /**
     * Accesseur de l attribut nom administrateur.
     * 
     * @return nom administrateur
     */
    public String getNomAdministrateur()
    {
        return nomAdministrateur;
    }

    /**
     * Accesseur de l attribut nom profil.
     * 
     * @return nom profil
     */
    public String getNomProfil()
    {
        return nomProfil;
    }

    /**
     * Retourne la liste des valeurs d'un filtre.
     * 
     * @param nomFiltre --
     * @return List
     */
    public List<String> rechercherListeValeursDUnFiltre(String nomFiltre)
    {
        String valeur = null;
        List<String> listValeurs = new ArrayList<String>();
        FiltreHabilitationAnnuaire filtre = null;

        Iterator itfiltre;
        Iterator itvaleur;

        if (this.getListeFiltres() != null)
        {
            itfiltre = this.getListeFiltres().iterator();
            while (itfiltre.hasNext())
            {
                filtre = (FiltreHabilitationAnnuaire) itfiltre.next();
                if (filtre.getListeValeursDeFiltre() != null && filtre.getNomDuFiltre().equalsIgnoreCase(nomFiltre))
                {
                    itvaleur = filtre.getListeValeursDeFiltre().iterator();
                    while (itvaleur.hasNext())
                    {
                        valeur = ((ValeurFiltreHabilitationAnnuaire) itvaleur.next()).getValeur();
                        listValeurs.add(valeur);
                    }
                }
            }
        }
        return listValeurs;
    }

    /**
     * Retourne la prmière valeur d'un filtre.
     * 
     * @param nomdefiltre --
     * @return the string
     */
    public String rechercherPremiereValeurDUnFiltre(String nomdefiltre)
    {
        String valeur = null;

        FiltreHabilitationAnnuaire filtre = null;

        Iterator itfiltre;
        Iterator itvaleur;

        // dec 07 LC : plus d'exception jetée si aucune valeur de filtre / on se contente de renvoyer null (demande
        // D.Colin)

        if (this.getListeFiltres() != null)
        {
            itfiltre = this.getListeFiltres().iterator();
            while (itfiltre.hasNext())
            {
                filtre = (FiltreHabilitationAnnuaire) itfiltre.next();
                if (filtre.getListeValeursDeFiltre() != null && filtre.getNomDuFiltre().equalsIgnoreCase(nomdefiltre))
                {
                    itvaleur = filtre.getListeValeursDeFiltre().iterator();
                    if (itvaleur.hasNext())
                    {
                        valeur = ((ValeurFiltreHabilitationAnnuaire) itvaleur.next()).getValeur();
                    }
                }
            }
        }

        return valeur;
    }

    /**
     * Modificateur de l attribut date creation.
     * 
     * @param dateCreation le nouveau date creation
     */
    public void setDateCreation(Date dateCreation)
    {
        this.dateCreation = dateCreation;
    }

    /**
     * Modificateur de l attribut date debut.
     * 
     * @param dateDebut le nouveau date debut
     */
    public void setDateDebut(Date dateDebut)
    {
        this.dateDebut = dateDebut;
    }

    /**
     * Modificateur de l attribut date fin.
     * 
     * @param dateFin le nouveau date fin
     */
    public void setDateFin(Date dateFin)
    {
        this.dateFin = dateFin;
    }

    /**
     * Modificateur de l attribut libelle court appli.
     * 
     * @param libelleCourtAppli le nouveau libelle court appli
     */
    public void setLibelleCourtAppli(String libelleCourtAppli)
    {
        this.libelleCourtAppli = libelleCourtAppli;
    }

    /**
     * Modificateur de l attribut liste filtres.
     * 
     * @param listeFiltres le nouveau liste filtres
     */
    public void setListeFiltres(Set listeFiltres)
    {
        this.listeFiltres = listeFiltres;
    }

    /**
     * Modificateur de l attribut nom administrateur.
     * 
     * @param nomAdministrateur le nouveau nom administrateur
     */
    public void setNomAdministrateur(String nomAdministrateur)
    {
        this.nomAdministrateur = nomAdministrateur;
    }

    /**
     * Modificateur de l attribut nom profil.
     * 
     * @param nomProfil le nouveau nom profil
     */
    public void setNomProfil(String nomProfil)
    {
        this.nomProfil = nomProfil;
    }

}
