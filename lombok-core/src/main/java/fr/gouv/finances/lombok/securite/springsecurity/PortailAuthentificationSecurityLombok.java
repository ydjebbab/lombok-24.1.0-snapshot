/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : PortailAuthentificationSecurityLombok.java
 *
 */
package fr.gouv.finances.lombok.securite.springsecurity;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;

import fr.gouv.finances.lombok.securite.techbean.AuthentificationRegleGestionException;
import fr.gouv.finances.lombok.securite.techbean.HabilitationAnnuaire;
import fr.gouv.finances.lombok.securite.techbean.HabilitationRegleGestionException;
import fr.gouv.finances.lombok.securite.techbean.PersonneAnnuaire;
import fr.gouv.finances.lombok.util.format.FormaterDate;

/**
 * <pre>
 * Classe pour accès via le portail.
 * Cette classe fait appel à authentificationEtLectureHabilitationsService qui va s'authentifier dans le LDAP et
 * récupère les profils utilisateurs ainsi que toutes les informations sur l'utilisateur trouvées dans l'annuaire.
 * Puis à partir de ces résultats, elle construit un objet UtilisateurSecurityLombok implémentant l'interface userdetails
 * attendue par SpringSEcurity.
 * Pour les utilisateurs LDAP, l'accès annuaire étant impossible, on construit une
 * personneAnnuaire à partir du profil transmis dans le username (voir classe LdapAuthentificationProcessingFliter).
 * </pre>
 * 
 * @author chouard-cp
 * @version $Revision: 1.4 $ Date: 15 déc. 2009 version octobre 2012 v21r02 : on n'utilise plus les url portail (sont
 *          récupérées dynamiquement à la connexion)
 */
public class PortailAuthentificationSecurityLombok extends AbstractAuthentificationSecurityLombok
{
    private static final Logger log = LoggerFactory.getLogger(PortailAuthentificationSecurityLombok.class);

    /** url portail. */
    @Deprecated
    private String urlPortail;

    /*
     * découpage de l'url urlPortail en 3 parties ( pour les récupérer pour édition) exemple
     * http:/p3d-dev1.appli.cp/p3dweb/j_appelportail urlPortailPrefixe=http://p3d-dev1.appli.cp urlPortailAppli=p3dweb
     * urlPortailSuffixe=j_appelportail
     */

    /** url portail prefixe. */
    @Deprecated
    private String urlPortailPrefixe;

    /** url portail appli. */
    @Deprecated
    private String urlPortailAppli;

    /** magellan. */
    private String ulysse;

    public PortailAuthentificationSecurityLombok()
    {
        super();
    }

    /**
     * Gets the magellan.
     *
     * @return the magellan
     */
    public String getUlysse()
    {
        return ulysse;
    }

    /*
     * * Accesseur de l attribut url portail.
     * @return url portail
     */
    /**
     * Gets the url portail.
     *
     * @return the url portail
     */
    public String getUrlPortail()
    {
        return urlPortail;
    }

    /**
     * Gets the url portail appli.
     *
     * @return the url portail appli
     */
    public String getUrlPortailAppli()
    {
        return urlPortailAppli;
    }

    /**
     * Gets the url portail prefixe.
     *
     * @return the url portail prefixe
     */
    public String getUrlPortailPrefixe()
    {

        return urlPortailPrefixe;
    }

    /*
     * méthode de AbstractUserDetailsAuthenticationProvider Allows subclasses to actually retrieve the UserDetails from
     * an implementation-specific location, with the option of throwing an AuthenticationException immediately if the
     * presented credentials are incorrect (this is especially useful if it is necessary to bind to a resource as the
     * user in order to obtain or generate a UserDetails). (non-Javadoc)
     * @see org.springframework.security.providers.dao.AbstractUserDetailsAuthenticationProvider#retrieveUser(java.lang.
     * String , org.springframework.security.providers.UsernamePasswordAuthenticationToken)
     */
    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param username "documenté"
     * @param auth "documenté"
     * @return user details
     * @throws AuthenticationException the authentication exception
     * @see org.springframework.security.providers.dao.AbstractUserDetailsAuthenticationProvider#retrieveUser(java.lang.String,
     *      org.springframework.security.providers.UsernamePasswordAuthenticationToken)
     */
    @Override
    public UserDetails retrieveUser(String username, UsernamePasswordAuthenticationToken auth)
        throws AuthenticationException
    {
        log.debug(">>> Debut methode retrieveUser()");
        PersonneAnnuaire unePersonneAnnuaire = null;
        try
        {
            String urlPortail = "";
            this.verifierAuthentificationEnModePortail();
            // Pas de vérification du mot de passe ensuite (authentification déjà faite par le portail,
            // donc on met un mot de passe quelconque
            String mdpVide = "";

            verifierPasDeMessageBloquantSRM();

            // Cas général : construction personneAnnuaire et
            // verification
            // habilitation dans l'annuaire

            // Cas username dpaep : Pas d'accès annuaire
            // possible
            // extraction du profil et construction d'un objet
            // personneAnnuaire
            if (estUserNameDOrigineDpaep(username))

            {
                urlPortail = this.urlPortailDOrigineDgfipOuDpaep(username);
                unePersonneAnnuaire = this.constructionPersonneAnnuaireDpaep(username);
            }

            // 10/02/12 on ne doit plus rechercher l'habilitation dans l'annuiare usagers
            // on constuira une habilitaion fictive avec profil 0
            // cf message olivier thomas 09/02/2012 sur patrim , expéditeur fichini
            else if (estUserNameDOrigineUsagers(username))
            {
                urlPortail = this.urlPortailDOrigineUsagers(username);
                String[] tableauDnProfil =
                    StringUtils.splitByWholeSeparator(username,
                        PortailAuthenticationProcessingFilter.SEPARATEUR_DN_PROFIL_USERNAME_USAGERS);
                String dnn = tableauDnProfil[0];
                unePersonneAnnuaire = this.constructionPersonneAnnuaireSansRechercheHabilitation(dnn);
            }
            // cas où l'utilisateur ne possède qu'un identifiant et un profil (annuaire.typeannuaire=AUCUN)
            else if (estUserNameSansAnnuaire(username))
            {
                String[] tableauDnProfil =
                    StringUtils.splitByWholeSeparator(username,
                        PortailAuthenticationProcessingFilter.SEPARATEUR_DN_PROFIL_USERNAME_AUCUN);
                String dnn = tableauDnProfil[0];
                String profil = tableauDnProfil[1];
                unePersonneAnnuaire = this.constructionPersonneSansAnnuaire(dnn, profil);
            }
            else if (estUserNameReal(username))
            {
                String[] tableauDnProfil =
                    StringUtils.splitByWholeSeparator(username,
                        PortailAuthenticationProcessingFilter.SEPARATEUR_DN_PROFIL_USERNAME_REAL);
                unePersonneAnnuaire = this.construirePersonneReal(tableauDnProfil[0]);
            }
            else
            // cas ex dgi ex dgcp fusion
            {
                urlPortail = this.urlPortailDOrigineDgfipOuDpaep(username);
                String[] tableauDnProfil =
                    StringUtils.splitByWholeSeparator(username,
                        PortailAuthenticationProcessingFilter.SEPARATEUR_DN_PROFIL_USERNAME_DGFIP);
                String dnn = tableauDnProfil[0];
                String profil = tableauDnProfil[1];
                // cas surtout ex dgcp, on ira ainsi chercher toutes les hbailittaions sur l'application donnée
                if (StringUtils.isNotBlank(codeApplicationDansHabilitations))
                {
                    unePersonneAnnuaire =
                        authentificationEtLectureHabilitationsService.rechercherPersonneAnnuaireAvecHabilitationParDN(
                            dnn, codeApplicationDansHabilitations);
                }
                else
                // cas ex dgi , on construit l'habilitation à partir des infos du portail
                {
                    unePersonneAnnuaire = this.constructionPersonneAnnuaireSansRechercheHabilitationAvecPassageProfil(dnn, profil);

                }

            }

            // gestion des profils bloqués par sireme
            boolean desProfilsOntEteFiltres = filtrerProfilsBloquesParSireme(unePersonneAnnuaire);

            if (unePersonneAnnuaire.getListeHabilitations() == null
                || unePersonneAnnuaire.getListeHabilitations().isEmpty())
            {
                jeterExceptionPasDHabilitation(desProfilsOntEteFiltres);
            }

            appelerIntercepteurAvantHabilitation(unePersonneAnnuaire);

            return this.construitUtilisateurSecurityLombok(unePersonneAnnuaire, unePersonneAnnuaire.getUid(), mdpVide, urlPortail);
        }

        catch (AuthentificationRegleGestionException | HabilitationRegleGestionException e)
        {
            throw new BadCredentialsException(e.getMessage(), e);
        }
        catch (RuntimeException e)
        {
            log.error("Erreur imprevue lors de l'authentification ", e);
            throw new BadCredentialsException(e.getMessage(), e);
        }

    }

    /**
     * Sets the magellan.
     *
     * @param ulysse the new magellan
     */
    public void setUlysse(String ulysse)
    {
        this.ulysse = ulysse;
    }

    /**
     * Sets the url portail.
     *
     * @param urlPortail the new url portail
     */
    public void setUrlPortail(String urlPortail)
    {
        log.info("Url  complete de connexion au portail pour les utilisateurs  : " + urlPortail);
        this.urlPortail = urlPortail;
    }

    /**
     * Sets the url portail appli.
     *
     * @param urlPortailAppli the new url portail appli
     */
    public void setUrlPortailAppli(String urlPortailAppli)
    {
        log.info("Url de connexion au portail pour les utilisateurs (partie application) : " + urlPortailAppli);
        this.urlPortailAppli = urlPortailAppli;
    }

    /**
     * Sets the url portail prefixe.
     *
     * @param urlPortailPrefixe the new url portail prefixe
     */
    public void setUrlPortailPrefixe(String urlPortailPrefixe)
    {
        log.info("Url de connexion au portail pour les utilisateurs (partie prefixe) : " + urlPortailPrefixe);
        this.urlPortailPrefixe = urlPortailPrefixe;
    }

    /*
     * Allows subclasses to perform any additional checks of a returned (or cached) UserDetails for a given
     * authentication request. (non-Javadoc)
     * @see org.springframework.security.providers.dao.AbstractUserDetailsAuthenticationProvider#
     * additionalAuthenticationChecks (org.springframework.security.userdetails.UserDetails,
     * org.springframework.security.providers.UsernamePasswordAuthenticationToken)
     */
    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param udd "documenté"
     * @param upat "documenté"
     * @throws AuthenticationException the authentication exception
     * @see org.springframework.security.providers.dao.AbstractUserDetailsAuthenticationProvider#additionalAuthenticationChecks(org.springframework.security.userdetails.UserDetails,
     *      org.springframework.security.providers.UsernamePasswordAuthenticationToken)
     */
    @Override
    protected void additionalAuthenticationChecks(UserDetails udd, UsernamePasswordAuthenticationToken upat)
        throws AuthenticationException
    {
        // le mot de passe a été controlé par le portail donc pas besoin de
        // surcharger cette méthode

    }

    /**
     * methode Construire personne real : .
     *
     * @param usernameraw
     * @return personne annuaire
     */
    private PersonneAnnuaire construirePersonneReal(String usernameraw)
    {
        log.debug(">>> Debut methode construirePersonneReal()");
        PersonneAnnuaire unePersonneAnnuaire;
        unePersonneAnnuaire = new PersonneAnnuaire();

        // regex utilisé pour désencapsuler les infos qui seront utilisées pour créer une personneannuaire
        // exemple attendu: cn=BUSTE Tara (3134100058),ou=Notaires,o=Professions Reglementées,c=FR
        // la partie ou= est optionnelle. Si elle n'est pas présente, un profil "aucun" sera
        // créé
        String pattern = "(?:cn\\=(?<cn>.*))\\((?<real>\\d+)\\)(\\W+)?((?:ou\\=(?<ou>\\w+)))?";
        Pattern pat = Pattern.compile(pattern);

        Matcher matcher = pat.matcher(usernameraw);
        if (matcher.find())
        {
            String uid;
            String cnn;
            String ouu;
            cnn = matcher.group("cn");
            uid = matcher.group("real");
            ouu = matcher.group("ou");
            if (log.isDebugEnabled())
            {
                log.debug("cn=" + matcher.group("cn"));
                log.debug("clé real=" + matcher.group("real"));
                log.debug("profil(ou=)=" + matcher.group("ou"));
            }

            DateFormat dateFormat = FormaterDate.getFormatDate("dd/MM/yyyy");
            Date dateDebut = null;
            Date dateFin = null;

            try
            {
                dateDebut = dateFormat.parse("01/01/2017");
                dateFin = dateFormat.parse("01/01/2099");
            }
            catch (ParseException pexc)
            {
                log.debug("Problème de parsing de la date qui sera utilisée pour l'habilitation ", pexc);
            }
            unePersonneAnnuaire = new PersonneAnnuaire();

            Set<HabilitationAnnuaire> listeHabilitations = new HashSet<>();
            HabilitationAnnuaire unehabilitation = new HabilitationAnnuaire();

            if (ouu != null)
            {
                unehabilitation.setNomProfil(ouu);
            }
            else
            {
                unehabilitation.setNomProfil("aucun");

            }
            unehabilitation.setDateDebut(dateDebut);
            unehabilitation.setDateFin(dateFin);
            unehabilitation.setDateCreation(dateDebut);
            unehabilitation.setNomAdministrateur("Inconnu");
            unehabilitation.setLibelleCourtAppli("Inconnu");

            listeHabilitations.add(unehabilitation);
            unePersonneAnnuaire.setListeHabilitations(listeHabilitations);

            unePersonneAnnuaire.setUid(uid);
            unePersonneAnnuaire.setAffectation("Inconnu");
            unePersonneAnnuaire.setAnnexe("0");
            unePersonneAnnuaire.setCn(cnn);
            unePersonneAnnuaire.setCodeFonction("Inconnu");
            unePersonneAnnuaire.setCodeGrade("Inconnu");
            unePersonneAnnuaire.setDepartement("Inconnu");
            unePersonneAnnuaire.setGivenName(uid);
            unePersonneAnnuaire.setMail("Inconnu");
            unePersonneAnnuaire.setPersonalTitle("");
            unePersonneAnnuaire.setPostalAddress("Inconnu");
            unePersonneAnnuaire.setPostalCode("Inconnu");
            unePersonneAnnuaire.setSn("Inconnu");
            unePersonneAnnuaire.setTelephoneNumber("Inconnu");
            unePersonneAnnuaire.setTitle("Inconnu");

            if (log.isDebugEnabled())
            {
                log.debug(
                    "Construction utilisateur avec clé REAL passée dans l'entête (annuaire.typeannuaire=REAL), la clé est utilisée pour l'uid : "
                        + unePersonneAnnuaire.getUid());
                log.debug("Une habilitation avec profil (récupéré si l'entête contient ou=)  : " + unehabilitation.getNomProfil());

            }
        }
        else
        {
            throw new AuthentificationRegleGestionException(
                "L'information passée (login ou entête http : " + usernameraw + ") ne permet pas de créer un utilisateur");
        }
        return unePersonneAnnuaire;
    }

    /**
     * Methode permettant de construire une personneAnnuaire pour les utilisateurs "sans annuaire". les seules
     * informations connues sont le "dn" et le "profil" les autres champs de personneannuaire sont mises à "Inconnu"
     *
     * @param dnn DOCUMENTEZ_MOI
     * @param profil DOCUMENTEZ_MOI
     * @return the personne annuaire
     */
    private PersonneAnnuaire constructionPersonneSansAnnuaire(String dnn, String profil)
    {
        log.debug(">>> Debut methode constructionPersonneSansAnnuaire()");
        PersonneAnnuaire unePersonneAnnuaire;
        unePersonneAnnuaire = new PersonneAnnuaire();

        /* on décompose quand même le dnn pour obtenir l'uid */
        String[] tableauDecomposeDn =
            StringUtils.split(dnn, PortailAuthenticationProcessingFilter.SEPARATEUR_NIVEAUX_DANS_LE_DN);
        String uid = tableauDecomposeDn[0];

        DateFormat dateFormat = FormaterDate.getFormatDate("dd/MM/yyyy");
        Date dateDebut = null;
        Date dateFin = null;

        try
        {
            dateDebut = dateFormat.parse("01/01/2016");
            dateFin = dateFormat.parse("01/01/2099");
        }
        catch (ParseException e)
        {
            log.debug("Problème de parsing de la date qui sera utilisée pour l'habilitation selon le profil " + profil, e);
        }
        unePersonneAnnuaire = new PersonneAnnuaire();

        Set<HabilitationAnnuaire> listeHabilitations = new HashSet<>();
        HabilitationAnnuaire unehabilitation = new HabilitationAnnuaire();

        unehabilitation.setNomProfil(profil);
        unehabilitation.setDateDebut(dateDebut);
        unehabilitation.setDateFin(dateFin);
        unehabilitation.setDateCreation(dateDebut);
        unehabilitation.setNomAdministrateur("Inconnu");
        unehabilitation.setLibelleCourtAppli("Inconnu");

        listeHabilitations.add(unehabilitation);
        unePersonneAnnuaire.setListeHabilitations(listeHabilitations);

        unePersonneAnnuaire.setUid(uid);
        unePersonneAnnuaire.setAffectation("Inconnu");
        unePersonneAnnuaire.setAnnexe("0");
        unePersonneAnnuaire.setCn(uid);
        unePersonneAnnuaire.setCodeFonction("Inconnu");
        unePersonneAnnuaire.setCodeGrade("Inconnu");
        unePersonneAnnuaire.setDepartement("Inconnu");
        unePersonneAnnuaire.setGivenName("Inconnu");
        unePersonneAnnuaire.setMail("Inconnu");
        unePersonneAnnuaire.setPersonalTitle("");
        unePersonneAnnuaire.setPostalAddress("Inconnu");
        unePersonneAnnuaire.setPostalCode("Inconnu");
        unePersonneAnnuaire.setSn("Inconnu");
        unePersonneAnnuaire.setTelephoneNumber("Inconnu");
        unePersonneAnnuaire.setTitle("Inconnu");

        if (log.isDebugEnabled())
        {
            log.debug("Construction utilisateur sans annuaire (annuaire.typeannuaire=AUCUN) avec uid : " + unePersonneAnnuaire.getUid());
            log.debug("Une habilitation avec profil  : " + unehabilitation.getNomProfil());

        }
        return unePersonneAnnuaire;
    }

    /**
     * Methode permettant de construire une personneAnnauire pour les utilisateurs DPAEP.
     * 
     * @param username username
     * @return the personne annuaire
     */
    protected PersonneAnnuaire constructionPersonneAnnuaireDpaep(String username)
    {
        log.debug(">>> Debut methode constructionPersonneAnnuaireDpaep()");
        PersonneAnnuaire unePersonneAnnuaire;

        // Separation dn et profil
        String[] tableauDnProfil =
            StringUtils.splitByWholeSeparator(username,
                PortailAuthenticationProcessingFilter.SEPARATEUR_DN_PROFIL_USERNAME_DPAEP);
        String dnn = tableauDnProfil[0];
        String profil = "0";

        String[] tableauDecomposeDn =
            StringUtils.split(dnn, PortailAuthenticationProcessingFilter.SEPARATEUR_NIVEAUX_DANS_LE_DN);
        String uid = tableauDecomposeDn[0];

        DateFormat dateFormat = FormaterDate.getFormatDate("dd/MM/yyyy");
        Date dateDebut = null;
        Date dateFin = null;

        try
        {
            dateDebut = dateFormat.parse("01/01/2009");
            dateFin = dateFormat.parse("01/01/2029");
        }
        catch (ParseException e)
        {
            log.debug("Problème parse date ", e);
        }

        unePersonneAnnuaire = new PersonneAnnuaire();

        Set<HabilitationAnnuaire> listeHabilitations = new HashSet<>();
        HabilitationAnnuaire unehabilitation = new HabilitationAnnuaire();

        unehabilitation.setNomProfil(profil);
        unehabilitation.setDateDebut(dateDebut);
        unehabilitation.setDateFin(dateFin);
        unehabilitation.setDateCreation(dateDebut);
        unehabilitation.setNomAdministrateur("Administrateur-Dpaep");
        unehabilitation.setLibelleCourtAppli(codeApplicationDansHabilitations);

        listeHabilitations.add(unehabilitation);
        unePersonneAnnuaire.setListeHabilitations(listeHabilitations);

        unePersonneAnnuaire.setUid(uid);
        unePersonneAnnuaire.setAffectation("Dpaep");
        unePersonneAnnuaire.setAnnexe("0");
        unePersonneAnnuaire.setCn(uid);
        unePersonneAnnuaire.setCodeFonction("Inconnu");
        unePersonneAnnuaire.setCodeGrade("Inconnu");
        unePersonneAnnuaire.setDepartement("Inconnu");
        unePersonneAnnuaire.setGivenName("Inconnu");
        unePersonneAnnuaire.setMail("Inconnu");
        unePersonneAnnuaire.setPersonalTitle("");
        unePersonneAnnuaire.setPostalAddress("Inconnu");
        unePersonneAnnuaire.setPostalCode("Inconnu");
        unePersonneAnnuaire.setSn("Inconnu");
        unePersonneAnnuaire.setTelephoneNumber("Inconnu");
        unePersonneAnnuaire.setTitle("");

        if (log.isDebugEnabled())
        {
            log.debug("Construction utilisateur dpaep avec uid : " + unePersonneAnnuaire.getUid());
            log.debug("Une habilitation avec profil  : " + unehabilitation.getNomProfil());
            log.debug("Sur application  : " + unehabilitation.getLibelleCourtAppli());
        }
        return unePersonneAnnuaire;
    }

    /**
     * Methode permettant de construire une personneAnnauire pour les utilisateurs usagers sans relire l'attribut profil
     * applicatif.
     * 
     * @param username username
     * @return the personne annuaire
     */
    protected PersonneAnnuaire constructionPersonneAnnuaireSansRechercheHabilitation(String username)
    {
        log.debug(">>> Debut methode constructionPersonneAnnuaireSansRechercheHabilitation()");
        PersonneAnnuaire unePersonneAnnuaire =
            authentificationEtLectureHabilitationsService.rechercherPersonneAnnuaireSansHabilitationParDN(username);

        if (unePersonneAnnuaire != null)
        {

            if (log.isDebugEnabled())
            {
                log.debug("Construction utilisateur avec uid : " + unePersonneAnnuaire.getUid());

            }
            HabilitationAnnuaire unehabilitation = new HabilitationAnnuaire();

            DateFormat dateFormat = FormaterDate.getFormatDate("dd/MM/yyyy");
            Date dateDebut = null;
            Date dateFin = null;

            try
            {
                dateDebut = dateFormat.parse("01/01/2012");
                dateFin = dateFormat.parse("01/01/2022");
            }
            catch (ParseException e)
            {
                log.debug("Problème parse date ", e);
            }

            // cas usagers on met un profil bidon 0 à remettre dans applicationContext-security

            Set<HabilitationAnnuaire> listeHabilitations = new HashSet<>();

            unehabilitation.setNomProfil("0");
            unehabilitation.setDateDebut(dateDebut);
            unehabilitation.setDateFin(dateFin);
            unehabilitation.setDateCreation(dateDebut);
            unehabilitation.setNomAdministrateur("Administrateur");
            unehabilitation.setLibelleCourtAppli(codeApplicationDansHabilitations);
            listeHabilitations.add(unehabilitation);

            unePersonneAnnuaire.setListeHabilitations(listeHabilitations);

        }

        else

        {
            log.debug("probléme de construction utilisateur avec dn  : " + username + "utilisateur non trouve dans l'annuaire");

        }

        return unePersonneAnnuaire;
    }

    /**
     * Methode permettant de construire une personneAnnauire pour les utilisateurs en affectant en profil dans
     * l'habilitation le profil transmis par le portail.
     * 
     * @param username username
     * @param profil "documenté"
     * @return the personne annuaire
     */
    protected PersonneAnnuaire constructionPersonneAnnuaireSansRechercheHabilitationAvecPassageProfil(String username, String profil)
    {
        log.debug(">>> Debut methode constructionPersonneAnnuaireSansRechercheHabilitationAvecPassageProfil()");
        PersonneAnnuaire unePersonneAnnuaire =
            authentificationEtLectureHabilitationsService.rechercherPersonneAnnuaireSansHabilitationParDN(username);

        if (unePersonneAnnuaire != null)
        {
            HabilitationAnnuaire unehabilitation = new HabilitationAnnuaire();

            DateFormat dateFormat = FormaterDate.getFormatDate("dd/MM/yyyy");
            Date dateDebut = null;
            Date dateFin = null;

            try
            {
                dateDebut = dateFormat.parse("01/01/2012");
                dateFin = dateFormat.parse("01/01/2022");
            }
            catch (ParseException e)
            {
                log.debug("Problème parse date ", e);
            }

            Set<HabilitationAnnuaire> listeHabilitations = new HashSet<>();

            unehabilitation.setNomProfil(profil);
            unehabilitation.setDateDebut(dateDebut);
            unehabilitation.setDateFin(dateFin);
            unehabilitation.setDateCreation(dateDebut);
            unehabilitation.setNomAdministrateur("Administrateur");
            unehabilitation.setLibelleCourtAppli("codeApplicationDansHabilitations");
            listeHabilitations.add(unehabilitation);

            unePersonneAnnuaire.setListeHabilitations(listeHabilitations);

            if (log.isDebugEnabled())
            {
                log.debug("Construction utilisateur avec uid : " + unePersonneAnnuaire.getUid());
                log.debug("Sur application  : " + unehabilitation.getLibelleCourtAppli());
            }
        }

        else

        {
            log.debug("probléme de construction utilisateur   usagers avec dn  : " + username + "utilisateur non trouve dans l'annuaire");

        }

        return unePersonneAnnuaire;
    }

    /**
     * methode Est user name real : .
     *
     * @param username DOCUMENTEZ_MOI
     * @return true, si c'est vrai
     */
    private boolean estUserNameReal(String username)
    {
        boolean estIl = false;
        estIl =
            StringUtils.contains(username,
                PortailAuthenticationProcessingFilter.SEPARATEUR_DN_PROFIL_USERNAME_REAL);
        if (log.isDebugEnabled())
        {
            log.debug("L'utilisateur est reconnu grace à sa cle Real (dans l'entête http webpass-remote-user)");

        }

        return estIl;
    }

    /**
     * recherche une personne sans besoin d'appel à un annuaire.
     *
     * @param username username
     * @return true,
     */
    private boolean estUserNameSansAnnuaire(String username)
    {
        boolean estIl = false;

        estIl =
            StringUtils.contains(username,
                PortailAuthenticationProcessingFilter.SEPARATEUR_DN_PROFIL_USERNAME_AUCUN);
        if (log.isDebugEnabled())
        {
            log.debug("Le filtre en amont a placé un séparateur qui indique que l'utilisateur ne nécessitera pas d'appel à un annuaire");

        }

        return estIl;
    }

    /**
     * recherche l'origine de la personne ,si elle appartient à la dpaep.
     * 
     * @param username username
     * @return true, if est user name d origine dpaep
     */
    protected boolean estUserNameDOrigineDpaep(String username)
    {
        boolean estIl = false;
        if (username.indexOf(PortailAuthenticationProcessingFilter.SEPARATEUR_DN_PROFIL_USERNAME_DPAEP) != -1)
        {
            if (log.isDebugEnabled())
            {
                log.debug("User Name d'origine dpaep avec profil concatene detecte : " + username);
            }
            estIl = true;
        }
        else
        {
            estIl = false;
        }
        return estIl;
    }

    /**
     * recherhe origine usagers - on construit une habilitation avec 0.
     * 
     * @param username "documenté"
     * @return true, si c'est vrai
     */
    protected boolean estUserNameDOrigineUsagers(String username)
    {
        boolean estIl = false;
        if (username.indexOf(PortailAuthenticationProcessingFilter.SEPARATEUR_DN_PROFIL_USERNAME_USAGERS) != -1)
        {
            if (log.isDebugEnabled())
            {
                log.debug("User Name d'origine   usagers avec profil concatene detecte : " + username);
            }
            estIl = true;
        }
        else
        {
            estIl = false;
        }
        return estIl;
    }

    /**
     * récupére l'url portail transmise via leportailauthenticationprocessingfilter dans le cas dgfip.
     * 
     * @param username "documenté"
     * @return string
     */
    protected String urlPortailDOrigineDgfipOuDpaep(String username)
    {
        log.debug(">>> Debut methode urlPortailDOrigineDgfipOuDpaep()");
        boolean estIl = false;
        if (username.indexOf(PortailAuthenticationProcessingFilter.SEPARATEUR_DN_PROFIL_USERNAME_DGFIP) != -1)
        {
            if (log.isDebugEnabled())
            {
                log.debug("User Name d'origine dgfip avec profil concatene detecte : " + username);
            }
            String[] tableauDnProfil =
                StringUtils.splitByWholeSeparator(username,
                    PortailAuthenticationProcessingFilter.SEPARATEUR_DN_PROFIL_USERNAME_DGFIP);
            String urlPortail = tableauDnProfil[2];
            if (log.isDebugEnabled())
            {
                log.debug("url portail récupéré : " + urlPortail);
            }
            return urlPortail;
        }

        else if (username.indexOf(PortailAuthenticationProcessingFilter.SEPARATEUR_DN_PROFIL_USERNAME_DPAEP) != -1)
        {
            if (log.isDebugEnabled())
            {
                log.debug("User Name d'origine dpaep avec profil concatene detecte : " + username);
            }
            String[] tableauDnProfil =
                StringUtils.splitByWholeSeparator(username,
                    PortailAuthenticationProcessingFilter.SEPARATEUR_DN_PROFIL_USERNAME_DPAEP);
            String urlPortail = tableauDnProfil[2];
            if (log.isDebugEnabled())
            {
                log.debug("url portail récupéré : " + urlPortail);
            }
            return urlPortail;
        }
        else
        {
            return null;
        }

    }

    /**
     * récupére l'url portail transmise via leportailauthenticationprocessingfilter dans le cas usagers.
     * 
     * @param username "documenté"
     * @return string @return+
     */
    protected String urlPortailDOrigineUsagers(String username)
    {
        log.debug(">>> Debut methode urlPortailDOrigineUsagers()");
        boolean estIl = false;
        if (username.indexOf(PortailAuthenticationProcessingFilter.SEPARATEUR_DN_PROFIL_USERNAME_USAGERS) != -1)
        {
            if (log.isDebugEnabled())
            {
                log.debug("User Name d'origine   usagers avec profil concatene detecte : " + username);
            }
            String[] tableauDnProfil =
                StringUtils.splitByWholeSeparator(username,
                    PortailAuthenticationProcessingFilter.SEPARATEUR_DN_PROFIL_USERNAME_USAGERS);
            String urlPortail = tableauDnProfil[1];
            if (log.isDebugEnabled())
            {
                log.debug("url portail récupéré : " + urlPortail);
            }
            return urlPortail;
        }
        else
        {
            return null;
        }

    }

}
