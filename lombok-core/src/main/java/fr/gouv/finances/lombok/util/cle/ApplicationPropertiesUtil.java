/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : ApplicationPropertiesUtil.java
 *
 */
package fr.gouv.finances.lombok.util.cle;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.net.URLDecoder;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.Properties;
import java.util.ResourceBundle;

import org.apache.commons.lang.StringUtils;
import org.jfree.util.Log;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.ResourceUtils;

import fr.gouv.finances.lombok.util.constantes.ConstantesGlobales;
import fr.gouv.finances.lombok.util.exception.ProgrammationException;

/**
 * <pre>
 * Classe utilitaire qui se trouvait auparavant dans fr.gouv.finances.lombok.rejetfichier.bean
 * Cette classe permet de récupérer des valeurs dans les fichiers de proprietes :
 * commun.properties ou application.properties ou test.properties ou log4j.properties (definis dans ConstantesGlobales)
 * </pre>
 * 
 * @author amleplatinec-cp
 * @version $Revision: 1.3 $ Date: 15 déc. 2009
 */
public final class ApplicationPropertiesUtil
{
    private static final Logger LOG = LoggerFactory.getLogger(ApplicationPropertiesUtil.class);

    private static ResourceBundle appliResourceBundle;

    static
    {
        String confURL = System.getProperty(ConstantesGlobales.CONF_DIR_PARAM);

        ClassLoader classLoader = null;
        if (StringUtils.isBlank(confURL))
        {
            classLoader = Thread.currentThread().getContextClassLoader();
        }
        else
        {
            URL[] loaderUrls;
            try
            {
                loaderUrls = new URL[] {new URL(confURL)};
            }
            catch (MalformedURLException e)
            {
                LOG.warn("Répertoire de configuration incorrect: " + confURL);
                LOG.debug("Cause exception : " + e.getCause());
                throw new RuntimeException("Répertoire de configuration incorrect: " + confURL, e);
            }

            classLoader = new URLClassLoader(loaderUrls, null);
        }
        try
        {
            appliResourceBundle = ResourceBundle.getBundle("application", Locale.getDefault(), classLoader);
        }
        catch (MissingResourceException e)
        {
            LOG.warn("Impossible de localiser application.properties");
            LOG.debug("Cause exception : " + e.getCause());
            throw new ProgrammationException("Impossible de localiser application.properties", e);
        }
    }

    /**
     * Methode initiale de la class renvoie le valeur de nompropriete trouvé dans application.properties
     * 
     * @param nompropriete --
     * @return the property
     * @author wpetit-cp
     */

    public static String getProperty(String nompropriete)
    {
        String propriete = null;

        try
        {
            propriete = appliResourceBundle.getString(nompropriete);
        }
        catch (RuntimeException rte)
        {
            LOG.warn("Impossible de recuperer la propriete dont la clé est " + nompropriete != null ? nompropriete
                : "null" + " de l'application");
            LOG.debug("Cause :", rte);
        }
        return propriete;
    }

    /**
     * méthode qui permet de trouver la valeur de nompropriete situé dans le fichier nomfichier.
     * 
     * @param nompropriete --
     * @param nomfichier --
     * @return the property
     */
    public static final String getProperty(String nompropriete, String nomfichier)
    {
        String propriete = null;
        if (nomfichier.equalsIgnoreCase(ConstantesGlobales.APPLIPROPERTIES_PATH))
        {
            propriete = getProperty(nompropriete);
        }
        else
        {

            FileInputStream input = null;
            File fic = null;

            try
            {

                Properties fichierAppliProperties = new Properties();
                fic = ResourceUtils.getFile(URLDecoder.decode(nomfichier, "UTF-8"));

                input = new FileInputStream(fic);
                fichierAppliProperties.load(input);
                propriete = fichierAppliProperties.getProperty(nompropriete);
            }

            catch (FileNotFoundException fnfe)
            {
                LOG.warn("FileNotFoundException: Fichier " + nomfichier + " non trouvé ", fnfe);
                if (fic != null)
                {
                    Log.debug(fic.getPath());
                }
            }
            catch (IOException ioe)
            {
                LOG.warn("IOExceptio: Fichier " + nomfichier + " non trouvé ", ioe);
                if (fic != null)
                {
                    Log.debug(fic.getPath());
                }
            }

            finally
            {
                try
                {
                    if (input != null)
                    {
                        // ferme le stream
                        input.close();
                    }
                }
                catch (IOException ex)
                {
                    LOG.warn("Impossible de fermer le fichier", ex);
                }
            }
        }
        return propriete;
    }

    /**
     * Methode qui va logguer une liste de valeurs lesNomsCles situées dans application.properties
     * 
     * @param lesNomsCles --
     */
    public static final void logguerValeursClesDansAppliProperties(List<String> lesNomsCles)
    {
        if (lesNomsCles != null && lesNomsCles.size() > 0)
        {
            for (Iterator<String> iter = lesNomsCles.iterator(); iter.hasNext();)
            {

                String unNomsCle = iter.next();
                String unevaleurPropriete =
                    ApplicationPropertiesUtil.getProperty(unNomsCle);
                if (LOG.isInfoEnabled())
                {
                    LOG.info("valeur de la cle " + unNomsCle);
                    LOG.info(" :  " + unevaleurPropriete);
                }
            }
        }
        else
        {
            LOG.warn("parametre passé à null dans logguerValeursClesDansAppliProperties");
        }

    }

    /**
     * Methode qui va logguer une liste de valeurs lesNomsCles situées dans commun.properties
     * 
     * @param lesNomsCles --
     */
    public static final void logguerValeursClesDansCommunProperties(List<String> lesNomsCles)
    {
        if (lesNomsCles != null && lesNomsCles.size() > 0)
        {
            for (Iterator<String> iter = lesNomsCles.iterator(); iter.hasNext();)
            {

                String unNomsCle = iter.next();
                String unevaleurPropriete =
                    ApplicationPropertiesUtil.getProperty(unNomsCle, ConstantesGlobales.COMMUNPROPERTIES_PATH);
                if (LOG.isInfoEnabled())
                {
                    LOG.info("valeur de la cle " + unNomsCle);
                    LOG.info(" :  " + unevaleurPropriete);
                }
            }
        }
        else
        {
            LOG.warn("parametre passé à null dans logguerValeursClesDansCommunProperties");
        }

    }

    /**
     * Methode qui va logguer une liste de valeurs lesNomsCles situées dans test.properties
     * 
     * @param lesNomsCles --
     */
    public static final void logguerValeursClesDansTestProperties(List<String> lesNomsCles)
    {
        if (lesNomsCles != null && lesNomsCles.size() > 0)
        {
            for (Iterator<String> iter = lesNomsCles.iterator(); iter.hasNext();)
            {

                String unNomsCle = iter.next();
                String unevaleurPropriete =
                    ApplicationPropertiesUtil.getProperty(unNomsCle, ConstantesGlobales.TESTPROPERTIES_PATH);
                if (LOG.isInfoEnabled())
                {
                    LOG.info("valeur de la cle " + unNomsCle);
                    LOG.info(" :  " + unevaleurPropriete);
                }
            }
        }
        else
        {
            LOG.warn("parametre passé à null dans logguerValeursClesDansTestProperties");
        }

    }

}
