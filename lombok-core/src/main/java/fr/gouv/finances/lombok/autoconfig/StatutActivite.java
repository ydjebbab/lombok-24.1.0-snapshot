package fr.gouv.finances.lombok.autoconfig;

/**
 * 
 * @author celinio fernandes
 * Date: Feb 19, 2020
 */
public enum StatutActivite
{
    ACTIF, INACTIF;
}
