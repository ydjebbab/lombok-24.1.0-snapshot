/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : RubriqueSousMenu.java
 *
 */
package fr.gouv.finances.lombok.menu.techbean;

import java.util.ArrayList;
import java.util.List;

/**
 * Class RubriqueSousMenu Classe représentant dans un sous menu un sous-menu imbriqué contenant lui même des
 * RubriquesLien ou d'autres RubriquesSousMenu.
 * 
 * @author lcontinsouzas-cp
 * @version $Revision: 1.2 $ Date: 15 déc. 2009
 */
public class RubriqueSousMenu extends Rubrique
{

    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** liste de rubriques. */
    public List listeDeRubriques = new ArrayList();
    

    public RubriqueSousMenu()
    {
        // RAS
    }
}
