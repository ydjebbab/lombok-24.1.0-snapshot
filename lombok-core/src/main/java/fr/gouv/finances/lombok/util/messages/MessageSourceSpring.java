/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : MessageSourceSpring.java
 *
 */
package fr.gouv.finances.lombok.util.messages;

/**
 * Surcharge de la classe Spring AbstractMessageSource pour permettre d'unifier
 * l'accès aux fichiers properties que se soit par les objets accédant au contexte 
 * spring (via le bean messagesource) ou par les objets métiers ou les services métiers
 * n'ayant pas a y accéder
 * @author lcontinsouzas-cp
 */

import java.text.MessageFormat;
import java.util.Locale;

import org.springframework.context.support.AbstractMessageSource;

/**
 * Class MessageSourceSpring.
 * 
 * @author lcontinsouzas-cp
 * @version $Revision: 1.2 $ Date: 15 déc. 2009
 */
public class MessageSourceSpring extends AbstractMessageSource
{

    public MessageSourceSpring()
    {
        super();
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param code
     * @param locale
     * @return message format
     * @see org.springframework.context.support.AbstractMessageSource#resolveCode(java.lang.String, java.util.Locale)
     */
    @Override
    protected MessageFormat resolveCode(String code, Locale locale)
    {
        String msg = Messages.getString(code);
        if (msg != null)
        {
            return createMessageFormat(msg, locale);
        }
        else
        {
            return null;
        }

    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param code
     * @param locale
     * @return string
     * @see org.springframework.context.support.AbstractMessageSource#resolveCodeWithoutArguments(java.lang.String,
     *      java.util.Locale)
     */
    @Override
    protected String resolveCodeWithoutArguments(String code, Locale locale)
    {
        return Messages.getString(code);
    }
}
