/*
 * Copyright (c) 2011 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.srm.bean;

import java.io.Serializable;
import java.text.ParseException;
import java.util.Date;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.xml.bind.annotation.XmlRootElement;

import fr.gouv.finances.lombok.util.base.BaseBean;
import fr.gouv.finances.lombok.util.exception.RegleGestionException;
import fr.gouv.finances.lombok.util.messages.Messages;

/**
 * Objet métier Message.
 * 
 * @author amleplatinec
 * @author Christophe Breheret-Girardin
 */
@XmlRootElement
public class Message extends BaseBean implements Serializable
{
    /** Initialisation d'un UID. */
    private static final long serialVersionUID = 7044725501152615377L;

    /** id. */
    private Long id;

    /** version. */
    private int version;

    /** date debut message. */
    private Date dateDebutMessage;

    /** date fin message. */
    private Date dateFinMessage;

    /** date debut inactivite profil. */
    private Date dateDebutInactiviteProfil;

    /** date fin inactivite profil. */
    private Date dateFinInactiviteProfil;

    /** libelle message. */
    private String libelleMessage;

    /** type message. */
    private String typeMessage;

    /** message type. */
    private MessageType messageType;

    // modif amlp 12/03/07
    // on affiche desormais suite e l'ecran de recherche des messages qui a cree
    // (ou modifie) le message
    /** UID createur message. */
    private String UIDCreateurMessage;

    // attribut cree pour permettre l'unicite pour <natural-id> du libelle
    // message concatene avec sa date de creation
    /** date creation message. */
    private Date dateCreationMessage;

    /** les profils desactives. */
    private Set<Profil> lesProfilsDesactives = new HashSet<>();

    // rajout amlp 12/03/07
    // un message peut etre e destination de profils ou de codiques particuliers
    // .
    /** les profils destinataires. */
    private Set<Profil> lesProfilsDestinataires = new HashSet<>();

    /** les codiques destinataires. */
    private Set<Codique> lesCodiquesDestinataires = new HashSet<>();

    /**
     * Instanciation de message.
     */
    public Message()
    {
        super();
    }

    /**
     * Constructeur.
     *
     * @param dateDebutMessage
     * @param dateFinMessage
     * @param libelleMessage
     * @param typeMessage
     * @param uIDCreateurMessage
     * @param dateCreationMessage
     * @param application
     */
    public Message(Date dateDebutMessage, Date dateFinMessage,
        String libelleMessage, String typeMessage,
        String uIDCreateurMessage, Date dateCreationMessage)
    {
        super();
        this.dateDebutMessage = dateDebutMessage;
        this.dateFinMessage = dateFinMessage;
        this.libelleMessage = libelleMessage;
        this.typeMessage = typeMessage;
        this.UIDCreateurMessage = uIDCreateurMessage;
        this.dateCreationMessage = dateCreationMessage;
    }

    /**
     * {@inheritDoc}
     * 
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode()
    {
        return Objects.hash(libelleMessage, dateCreationMessage);
    }

    /**
     * {@inheritDoc}
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj)
    {
        if (obj == null)
        {
            return false;
        }

        if (this.getClass() != obj.getClass())
        {
            return false;
        }

        Message message = (Message) obj;
        return Objects.equals(libelleMessage, message.libelleMessage)
            && Objects.equals(dateCreationMessage, message.dateCreationMessage);
    }

    /**
     * Gets the date creation message.
     *
     * @return the date creation message
     */
    public Date getDateCreationMessage()
    {
        return dateCreationMessage;
    }

    /**
     * Gets the date debut inactivite profil.
     *
     * @return the date debut inactivite profil
     */
    public Date getDateDebutInactiviteProfil()
    {
        return dateDebutInactiviteProfil;
    }

    /**
     * Gets the date debut message.
     *
     * @return the date debut message
     */
    public Date getDateDebutMessage()
    {
        return dateDebutMessage;
    }

    /**
     * Gets the date fin inactivite profil.
     *
     * @return the date fin inactivite profil
     */
    public Date getDateFinInactiviteProfil()
    {
        return dateFinInactiviteProfil;
    }

    /**
     * Gets the date fin message.
     *
     * @return the date fin message
     */
    public Date getDateFinMessage()
    {
        return dateFinMessage;
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Long getId()
    {
        return id;
    }

    /**
     * Gets the les codiques destinataires.
     *
     * @return the les codiques destinataires
     */
    public Set<Codique> getLesCodiquesDestinataires()
    {
        return lesCodiquesDestinataires;
    }

    /**
     * Gets the les profils desactives.
     *
     * @return the les profils desactives
     */
    public Set<Profil> getLesProfilsDesactives()
    {
        return lesProfilsDesactives;
    }

    /**
     * Gets the les profils destinataires.
     *
     * @return the les profils destinataires
     */
    public Set<Profil> getLesProfilsDestinataires()
    {
        return lesProfilsDestinataires;
    }

    /**
     * Gets the libelle message.
     *
     * @return the libelle message
     */
    public String getLibelleMessage()
    {
        return libelleMessage;
    }

    /**
     * Accesseur de l'attribut libelle type message.
     *
     * @return libelle type message
     */
    public String getLibelleTypeMessage()
    {
        if (this.getTypeMessage() == null)
        {
            return null;
        }

        String retour;
        if (this.getTypeMessage().compareTo("1") == 0)
        {
            retour = "message d information";
        }
        else
        {
            retour = "message bloquant";
        }
        return retour;
    }

    /**
     * Accesseur de message type.
     *
     * @return the message type
     */
    public MessageType getMessageType()
    {
        return messageType;
    }

    /**
     * Gets the type message.
     *
     * @return the type message
     */
    public String getTypeMessage()
    {
        return typeMessage;
    }

    /**
     * Accesseur de l attribut UID createur message.
     *
     * @return UID createur message
     */
    public String getUIDCreateurMessage()
    {
        return UIDCreateurMessage;
    }

    /**
     * Gets the version.
     *
     * @return the version
     */
    public int getVersion()
    {
        return version;
    }

    /**
     * Verifie si message bloquant.
     *
     * @return true, si c'est message bloquant
     */
    public boolean isMessageBloquant()
    {
        boolean retour = false;
        if ((this.getTypeMessage() != null)
            && (this.getTypeMessage().compareTo("2") == 0))
        {
            retour = true;
        }

        return retour;

    }

    /**
     * Sets the date creation message.
     *
     * @param dateCreationMessage the new date creation message
     */
    public void setDateCreationMessage(Date dateCreationMessage)
    {
        this.dateCreationMessage = dateCreationMessage;
    }

    /**
     * Sets the date debut inactivite profil.
     *
     * @param dateDebutInactiviteProfil the new date debut inactivite profil
     */
    public void setDateDebutInactiviteProfil(Date dateDebutInactiviteProfil)
    {
        this.dateDebutInactiviteProfil = dateDebutInactiviteProfil;
    }

    /**
     * Sets the date debut message.
     *
     * @param dateDebutMessage the new date debut message
     */
    public void setDateDebutMessage(Date dateDebutMessage)
    {
        this.dateDebutMessage = dateDebutMessage;
    }

    /**
     * Sets the date fin inactivite profil.
     *
     * @param dateFinIncativiteProfil the new date fin inactivite profil
     */
    public void setDateFinInactiviteProfil(Date dateFinIncativiteProfil)
    {
        this.dateFinInactiviteProfil = dateFinIncativiteProfil;
    }

    /**
     * Sets the date fin message.
     *
     * @param dateFinMessage the new date fin message
     */
    public void setDateFinMessage(Date dateFinMessage)
    {
        this.dateFinMessage = dateFinMessage;
    }

    /**
     * Sets the id.
     *
     * @param id the new id
     */
    public void setId(Long id)
    {
        this.id = id;
    }

    /**
     * Sets the les codiques destinataires.
     *
     * @param lesCodiquesDestinataires the new les codiques destinataires
     */
    public void setLesCodiquesDestinataires(
        Set<Codique> lesCodiquesDestinataires)
    {
        this.lesCodiquesDestinataires = lesCodiquesDestinataires;
    }

    /**
     * Sets the les profils desactives.
     *
     * @param listProfilsDesactives the new les profils desactives
     */
    public void setLesProfilsDesactives(Set<Profil> listProfilsDesactives)
    {
        this.lesProfilsDesactives = listProfilsDesactives;
    }

    /**
     * Sets the les profils destinataires.
     *
     * @param lesProfilsDestinataires the new les profils destinataires
     */
    public void setLesProfilsDestinataires(Set<Profil> lesProfilsDestinataires)
    {
        this.lesProfilsDestinataires = lesProfilsDestinataires;
    }

    /**
     * Sets the libelle message.
     *
     * @param libelleMessage the new libelle message
     */
    public void setLibelleMessage(String libelleMessage)
    {
        this.libelleMessage = libelleMessage;
    }

    /**
     * Sets the message type.
     *
     * @param messageType the new message type
     */
    public void setMessageType(MessageType messageType)
    {
        this.messageType = messageType;
    }

    /**
     * Sets the type message.
     *
     * @param typeMessage the new type message
     */
    public void setTypeMessage(String typeMessage)
    {
        this.typeMessage = typeMessage;
    }

    /**
     * Modificateur de l attribut UID createur message.
     *
     * @param createurMessage le nouveau UID createur message
     */
    public void setUIDCreateurMessage(String createurMessage)
    {
        UIDCreateurMessage = createurMessage;
    }

    /**
     * Sets the version.
     *
     * @param version the new version
     */
    public void setVersion(int version)
    {
        this.version = version;
    }

    /**
     * methodes de verification des dates.
     * 
     * @param dateDebut --
     * @param dateFin --
     * @param dateDuJour --
     * @throws ParseException the parse exception
     */
    public void verifierDatesMessage(Date dateDebut, Date dateFin,
        Date dateDuJour) throws ParseException
    {

        if (dateFin.before(dateDebut))
        {
            throw new RegleGestionException(
                Messages.getString("exception.message.verifdate1"));
        }
        if (dateFin.before(dateDuJour))
        {
            throw new RegleGestionException(
                Messages.getString("exception.message.verifdate2"));
        }

    }

    /**
     * verification des dates pour les profils desactives.
     * 
     * @param dateDebut --
     * @param dateFin --
     * @param dateDuJour --
     * @throws ParseException the parse exception
     */
    public void verifierDatesMessageProfil(Date dateDebut, Date dateFin,
        Date dateDuJour) throws ParseException
    {

        if (dateFin.before(dateDebut))
        {
            throw new RegleGestionException(
                Messages.getString("exception.message.verifdateprofils1"));
        }
        if (dateFin.before(dateDuJour))
        {
            throw new RegleGestionException(
                Messages.getString("exception.message.verifdateprofils2"));
        }

    }

}
