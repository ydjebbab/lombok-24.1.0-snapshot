/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 */
package fr.gouv.finances.lombok.util.exception;

import java.util.List;

/**
 * Sous-classe de RegleGestionException qui permet de distinguer les exceptions avec severite intermediaire.
 *
 * @see RegleGestionException
 * @author chouard-cp
 */
public class RegleGestionAvertissementException extends RegleGestionException
{

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /**
     * Instantiates a new regle gestion avertissement exception.
     */
    public RegleGestionAvertissementException()
    {
        super();

    }

    /**
     * Instanciation de regle gestion avertissement exception.
     * 
     * @param listeMessages --
     */
    public RegleGestionAvertissementException(List<String> listeMessages)
    {
        super(listeMessages);
    }

    /**
     * Constructeur de la classe RegleGestionAvertissementException.java on ajoute un message de libellé "arg0" qui se
     * retrouvera en premier dans la liste des messages
     * 
     * @param listeMessages
     * @param arg0
     */
    public RegleGestionAvertissementException(List<String> listeMessages, String arg0)
    {
        super(listeMessages, arg0);
    }

    /**
     * Instantiates a new regle gestion avertissement exception.
     * 
     * @param arg0 the arg0
     */
    public RegleGestionAvertissementException(String arg0)
    {
        super(arg0);

    }

    /**
     * Instantiates a new regle gestion avertissement exception.
     * 
     * @param arg0 the arg0
     * @param arg1 the arg1
     */
    public RegleGestionAvertissementException(String arg0, Throwable arg1)
    {
        super(arg0, arg1);

    }

    /**
     * Instantiates a new regle gestion avertissement exception.
     * 
     * @param arg0 the arg0
     */
    public RegleGestionAvertissementException(Throwable arg0)
    {
        super(arg0);

    }

}
