/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : CpJRSvgExporter.java
 *
 */
package fr.gouv.finances.lombok.mvc.jasperreports;

import java.awt.Graphics2D;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.export.JRGraphics2DExporter;
import net.sf.jasperreports.engine.export.JRGraphics2DExporterParameter;

import org.apache.batik.dom.GenericDOMImplementation;
import org.apache.batik.svggen.SVGGeneratorContext;
import org.apache.batik.svggen.SVGGraphics2D;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;

import fr.gouv.finances.lombok.util.exception.ProgrammationException;

/**
 * Class CpJRSvgExporter --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.2 $ Date: 11 déc. 2009
 */
@SuppressWarnings("deprecation")
public class CpJRSvgExporter extends JRGraphics2DExporter
{

    /** Constant : DEFAULT_ZOOM. */
    private static final float DEFAULT_ZOOM = 1f;

    /** writer. */
    protected Writer writer = null;

    /**
     * Instanciation de cp jr svg exporter.
     * 
     * @throws JRException the JR exception
     */
    public CpJRSvgExporter() throws JRException
    {
        super();
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @throws JRException the JR exception
     * @see net.sf.jasperreports.engine.export.JRGraphics2DExporter#exportReport()
     */
    /*
     * public void exportReport() throws JRException {
     *//**
     * progressMonitor = (JRExportProgressMonitor) parameters.get(JRExporterParameter.PROGRESS_MONITOR); /*
     */
    /*
     * setOffset(); try { setExportContext(); setInput(); setPageRange(); setTextRenderer(); Float zoomRatio = (Float)
     * parameters.get(JRGraphics2DExporterParameter.ZOOM_RATIO); if (zoomRatio != null) { zoom = zoomRatio.floatValue();
     * if (zoom <= 0) { throw new JRException("Invalid zoom ratio : " + zoom); } } else { zoom = DEFAULT_ZOOM; }
     * OutputStream os = (OutputStream) parameters.get(JRExporterParameter.OUTPUT_STREAM); writer = new
     * OutputStreamWriter(os, "UTF-8"); DOMImplementation domImpl = GenericDOMImplementation.getDOMImplementation();
     * Document document = domImpl.createDocument(null, "svg", null); SVGGeneratorContext ctx =
     * SVGGeneratorContext.createDefault(document); ctx.setEmbeddedFontsOn(true);
     * ctx.setComment("Généré par CpJrSvgExporter avecle générateur SVG Batik"); grx = new SVGGraphics2D(ctx, true); if
     * (grx == null) { throw new
     * JRException("No output specified for the exporter. java.awt.Graphics2D object expected."); }
     * exportReportToGraphics2D(); ((SVGGraphics2D) grx).stream(writer, true); } catch (IOException e) { throw new
     * ProgrammationException(e); } finally { resetExportContext(); } }
     */
    /**
     * (methode de remplacement) est remodelée suite à passage jasper5.5.2 {@inheritDoc}
     * 
     * @see net.sf.jasperreports.engine.export.JRGraphics2DExporter#exportReport()
     */
    @Override
    public void exportReport() throws JRException
    {
        try
        {
            ensureJasperReportsContext();
            ensureInput();

            initExport();

            // ensureOutput();
            Float zoom;
            Float zoomRatio = (Float) parameters.get(JRGraphics2DExporterParameter.ZOOM_RATIO);
            if (zoomRatio != null)
            {
                zoom = zoomRatio.floatValue();
                if (zoom <= 0)
                {
                    throw new JRException("Invalid zoom ratio : " + zoom);
                }
            }
            else
            {
                zoom = DEFAULT_ZOOM;
            }

            OutputStream os = (OutputStream) parameters.get(JRExporterParameter.OUTPUT_STREAM);
            writer = new OutputStreamWriter(os, "UTF-8");

            DOMImplementation domImpl = GenericDOMImplementation.getDOMImplementation();
            Document document = domImpl.createDocument(null, "svg", null);

            SVGGeneratorContext ctx = SVGGeneratorContext.createDefault(document);
            ctx.setEmbeddedFontsOn(true);

            ctx.setComment("Généré par CpJrSvgExporter avecle générateur SVG Batik");

            Graphics2D grx = new SVGGraphics2D(ctx, true);

            if (grx == null)
            {
                throw new JRException("No output specified for the exporter. java.awt.Graphics2D object expected.");
            }

            exportReportToGraphics2D(grx);
            ((SVGGraphics2D) grx).stream(writer, true);

        }
        catch (IOException e)
        {
            throw new ProgrammationException(e);
        }
        finally
        {
            resetExportContext();
        }
    }
}
