/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : ApplicationExceptionTransformateur.java
 *
 */
package fr.gouv.finances.lombok.util.exception;

import java.io.FileNotFoundException;

import javax.imageio.IIOException;

import org.springframework.dao.CleanupFailureDataAccessException;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.dao.DataRetrievalFailureException;
import org.springframework.dao.OptimisticLockingFailureException;

/**
 * Class ApplicationExceptionTransformateur.
 * 
 * @author lcontinsouzas-cp Classe à utiliser pour ajouter une exception de type ApplicationException à une pile
 *         d'exceptions La méthode transformer prend en entrée n'importe quel type d'exception : - transforme les
 *         exceptions Hibernate correspondant à un verrouillage optimiste en exceptions "VerouillageOptimisteException"
 *         (pour éviter l'adhérence des package de présentation qui géreront ces exception avec les packages hibernate -
 *         transforme les exceptions qui n'héritent pas de RuntimeException (et obligent à utiliser des clauses
 *         "throws") en exception DGCP héritant de RunTimeException - type certaines exceptions caractéristiques d'un
 *         problème d'exploitationen ExploitationException - toutes les autres exceptions sont classées en
 *         ProgrammationException - si aucun message n'est précisé, le message de l'exception une nouvelle exception est
 *         créée et la pile d'exception transmise en entrée est ajoutée pour conserver toute la trace d'exception
 * @version $Revision: 1.2 $ Date: 15 déc. 2009
 */
public class ApplicationExceptionTransformateur
{

    /**
     * Transformer.
     * 
     * @param e the e
     * @return the application exception
     */
    public static ApplicationException transformer(Exception e)
    {
        return transformer(null, e);
    }

    /**
     * Transformer.
     * 
     * @param msg the msg
     * @param e the e
     * @return the application exception
     */
    public static ApplicationException transformer(String msg, Exception e)
    {
        ApplicationException rte = null;

        // lock optimiste modif
        if (e instanceof OptimisticLockingFailureException)
        {
            // on prend le message par défaut de l'exception
            // pour ne pas afficher éventuellement à l'utilisateur
            // un message (msg) à vocation de debug
            rte = new VerrouillageOptimisteModificationException(e);
        }
        // lock optimiste suppression
        else if (e instanceof DataRetrievalFailureException)
        {
            rte = new VerrouillageOptimisteSuppressionException(e);
        }
        // exception avec cause probable problème d'exploitation
        else if ((e instanceof FileNotFoundException) || (e instanceof IIOException)
            || (e instanceof DataAccessResourceFailureException) || (e instanceof CleanupFailureDataAccessException))
        {
            if (msg == null)
            {
                rte = new ExploitationException(e);
            }
            else
            {
                rte = new ExploitationException(msg, e);
            }

        }
        // exceptions autres
        else if (!(e instanceof ApplicationException))
        {
            if (msg == null)
            {
                rte = new ProgrammationException(e);
            }
            else
            {
                rte = new ProgrammationException(msg, e);
            }
        }
        // c'est déjà une ApplicationException, on en crée pas une nouvelle
        else
        {
            rte = (ApplicationException) (e);
        }
        return rte;
    }

    /**
     * Instantiates a new application exception transformateur.
     */
    public ApplicationExceptionTransformateur()
    {
        super();

    }
}
