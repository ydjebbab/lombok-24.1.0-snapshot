/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : DgcpFileSystemXmlApplicationContext.java
 *
 */
package fr.gouv.finances.lombok.util.spring;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.util.StringUtils;

import fr.gouv.finances.lombok.util.cle.ApplicationPropertiesUtil;
import fr.gouv.finances.lombok.util.constantes.ConstantesGlobales;

/**
 * Classe utiliataire surdéfinissant la classe Spring
 * FileSystemXmlApplicationContext qui n'accepte pas les chemins absolus pour le fichier de contexte.
 * 
 * @author lcontinsouzas-cp
 */
public class DgcpFileSystemXmlApplicationContext extends FileSystemXmlApplicationContext
{

    /**
     * Constructeur.
     */
    public DgcpFileSystemXmlApplicationContext()
    {
        super();
        String profils = ApplicationPropertiesUtil.getProperty("batch.profils.actifs", ConstantesGlobales.BATCHPROPERTIES_PATH);
        if (!StringUtils.isEmpty(profils))
        {
            String[] tabProfils = profils.split(",");
            this.getEnvironment().setActiveProfiles(tabProfils);
        }
    }

    /**
     * Constructeur.
     * 
     * @param arg0 --
     * @throws BeansException the beans exception
     */
    public DgcpFileSystemXmlApplicationContext(String arg0) throws BeansException
    {
        super(ResolveurDeLocalisations.resoudreURLLocalisationEtTransformerEnPath(arg0));
    }

    /**
     * Constructeur.
     * 
     * @param arg0 --
     * @throws BeansException the beans exception
     */
    public DgcpFileSystemXmlApplicationContext(String[] arg0) throws BeansException
    {
        super(ResolveurDeLocalisations.resoudreURLLocalisationEtTransformerEnPath(arg0));

    }

    /**
     * Constructeur.
     * 
     * @param arg0 --
     * @param arg1 --
     * @throws BeansException the beans exception
     */
    public DgcpFileSystemXmlApplicationContext(String[] arg0, ApplicationContext arg1) throws BeansException
    {
        super(ResolveurDeLocalisations.resoudreURLLocalisationEtTransformerEnPath(arg0), arg1);
    }

    /**
     * Constructeur.
     * 
     * @param arg0 --
     * @param arg1 --
     * @throws BeansException the beans exception
     */
    public DgcpFileSystemXmlApplicationContext(String[] arg0, boolean arg1) throws BeansException
    {
        super(ResolveurDeLocalisations.resoudreURLLocalisationEtTransformerEnPath(arg0), arg1);
    }

    /**
     * Constructeur.
     * 
     * @param arg0 --
     * @param arg1 --
     * @param arg2 --
     * @throws BeansException the beans exception
     */
    public DgcpFileSystemXmlApplicationContext(String[] arg0, boolean arg1, ApplicationContext arg2)
        throws BeansException
    {
        super(ResolveurDeLocalisations.resoudreURLLocalisationEtTransformerEnPath(arg0), arg1, arg2);
    }

    /**
     * {@inheritDoc}
     * @see org.springframework.context.support.AbstractApplicationContext#getEnvironment()
     */
    @Override
    public final ConfigurableEnvironment getEnvironment()
    {
        return super.getEnvironment();
    }

    // Surdefinition de cette méthode spring qui sinon enleve le /
    // et interdit les chemins absolus
    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param path
     * @return resource by path
     * @see org.springframework.context.support.FileSystemXmlApplicationContext#getResourceByPath(java.lang.String)
     */
    @Override
    protected Resource getResourceByPath(String path)
    {
        return new FileSystemResource(path);
    }

}
