/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 */
package fr.gouv.finances.lombok.parsertexte.parametrage;

import java.util.ArrayList;
import java.util.List;

/**
 * Class MappingLigneClasse --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.2 $ Date: 11 déc. 2009
 */
public class MappingLigneClasse
{

    /** Constant : AFFECTATION_SETTER. */
    public static final int AFFECTATION_SETTER = 1;

    /** Constant : AFFECTATION_CONTRUCTEUR. */
    public static final int AFFECTATION_CONTRUCTEUR = 2;

    /** id ligne. */
    String idLigne = null;

    /** type affectation. */
    Integer typeAffectation = null;

    /** liste attributs. */
    List listeAttributs = null;

    /**
     * Instanciation de mapping ligne classe.
     * 
     * @param pIdLigne le id ligne
     * @param pTypeAffectation le type affectation
     */
    public MappingLigneClasse(String pIdLigne, Integer pTypeAffectation)
    {
        this.idLigne = pIdLigne;
        this.typeAffectation = pTypeAffectation;
    }

    /**
     * methode Ajouter attributs : --.
     * 
     * @param pAttribut le attribut
     */
    public void ajouterAttributs(AttributMapping pAttribut)
    {
        if (this.listeAttributs == null)
        {
            this.listeAttributs = new ArrayList();
        }
        this.listeAttributs.add(pAttribut);
    }

    /**
     * Accesseur de l attribut id ligne.
     * 
     * @return id ligne
     */
    public String getIdLigne()
    {
        return idLigne;
    }

    /**
     * Accesseur de l attribut liste attributs.
     * 
     * @return liste attributs
     */
    public List getListeAttributs()
    {
        return listeAttributs;
    }

    /**
     * Accesseur de l attribut type affectation.
     * 
     * @return type affectation
     */
    public Integer getTypeAffectation()
    {
        return typeAffectation;
    }

    /**
     * Modificateur de l attribut id ligne.
     * 
     * @param idLigne le nouveau id ligne
     */
    public void setIdLigne(String idLigne)
    {
        this.idLigne = idLigne;
    }

    /**
     * Modificateur de l attribut liste attributs.
     * 
     * @param listeAttributs le nouveau liste attributs
     */
    public void setListeAttributs(List listeAttributs)
    {
        this.listeAttributs = listeAttributs;
    }

    /**
     * Modificateur de l attribut type affectation.
     * 
     * @param typeAffectation le nouveau type affectation
     */
    public void setTypeAffectation(Integer typeAffectation)
    {
        this.typeAffectation = typeAffectation;
    }

}
