/*
 * Copyright (c) 2015 DGFiP - Tous droits réservés
 */

package fr.gouv.finances.lombok.util.date;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.joda.time.DateMidnight;
import org.joda.time.DateTime;
import org.joda.time.Days;

/**
 * Class JoursFeries --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.2 $ Date: 11 déc. 2009
 */
public class JoursFeries
{

    private static final Log LOG = LogFactory.getLog(JoursFeries.class);

    /**
     * Constructeur de la classe JoursFeries.java
     * Ajouté suite à un cas majeur sur SonarQube
     * 
     * Utility classes, which are a collection of static members, are not meant to be instantiated. Even abstract utility classes, which
     * can be extended, should not have public constructors. Java adds an implicit public constructor to every class
     * which does not define at least one explicitly. Hence, at least one non-public constructor should be defined.
     */
    private JoursFeries() {}
    
    private static void controleDesDatesEnParametre(Date dateDebut, Date dateFin)
    {
        if (dateDebut == null || dateFin == null)
        {
            throw new IllegalArgumentException("Les dates passées en paramètres ne doivent pas être nulles ");
        }
        
        if (dateDebut.after(dateFin))
        {
            throw new IllegalArgumentException("Pour calculer le nombre de jours ouvrés"
                + "entre deux dates, la date de début doit précéder la date de fin.");
        }
    }
    
    /**
     * Calcule le nombre de jours fériés de la période qui ne tombent pas un samedi ou un dimanche.
     * 
     * @param dateDebut Date de début de la période
     * @param dateFin Date de fin de la période
     * @return nombre de jours fériés hors samedi et dimanche
     */
    public static int calculeNombreJoursFeriesHorsSamediEtDimancheEntreDeuxDates(Date dateDebut, Date dateFin)
    {
        controleDesDatesEnParametre(dateDebut, dateFin);
        
        int nbJoursFeriesHorsSamediEtDimanches = 0;
        List<DateTime> joursFeries = creationListeDesJoursFeriesPourUnePeriode(dateDebut, dateFin);

        List<DateTime> joursFeriesHorsSamedisEtDimanches = new ArrayList<DateTime>();


        for (Iterator<DateTime> iterJoursFeries = joursFeries.iterator(); iterJoursFeries.hasNext();)
        {
            DateTime jourFerie =  iterJoursFeries.next();

            if (jourFerie != null && jourFerie.dayOfWeek().get() != 7 && jourFerie.dayOfWeek().get() != 6)
            {
                joursFeriesHorsSamedisEtDimanches.add(jourFerie);
            }
        }

        nbJoursFeriesHorsSamediEtDimanches = joursFeriesHorsSamedisEtDimanches.size();

        if (LOG.isDebugEnabled())
        {
            LOG.debug("Nombre de jours fériés hors samedis et dimanches  : " + nbJoursFeriesHorsSamediEtDimanches);
        }

        return nbJoursFeriesHorsSamediEtDimanches;
    }

    /**
     * Calcule le nombre de jours ouvrés entre deux dates Les jours de début et de fin sont inclus dans le calcul.
     * 
     * @param dateDebut Date de début de la période
     * @param dateFin Date de fin de la période
     * @return nombre de jours ouvrés
     */
    public static int calculeNombreJoursOuvresEntreDeuxDates(Date dateDebut, Date dateFin)
    {
        controleDesDatesEnParametre(dateDebut, dateFin);
        
        int nombreJoursOuvres = 0;
        int nombreJoursHorsSamedisEtDimanches =
            calculeNombreJoursSansSamedisEtDimanchesEntreDeuxDates(dateDebut, dateFin);
        int nombreJoursFeriesHorsSamedisEtDimanches =
            calculeNombreJoursFeriesHorsSamediEtDimancheEntreDeuxDates(dateDebut, dateFin);

        nombreJoursOuvres = nombreJoursHorsSamedisEtDimanches - nombreJoursFeriesHorsSamedisEtDimanches;

        return nombreJoursOuvres;
    }

    /**
     * Calcule le nombre de jours ouvrés entre deux dates en excluant uniquement les samedis et les dimanches.
     * 
     * @param dateDebut Date de début de la période
     * @param dateFin Date de fin de la période
     * @return nombre de jours ouvrés
     */
    public static int calculeNombreJoursSansSamedisEtDimanchesEntreDeuxDates(Date dateDebut, Date dateFin)
    {

        controleDesDatesEnParametre(dateDebut, dateFin);

        DateMidnight debutPeriode = new DateMidnight(dateDebut);
        DateMidnight finPeriode = new DateMidnight(dateFin);
        DateMidnight premierDimanche;
        DateMidnight dernierSamedi;

        int nombreDeJoursOuvres = 0;

        // Détermination du premier Dimanche de la période
        int nbJoursAAjouter = (7 - debutPeriode.dayOfWeek().get()) % 7;
        premierDimanche = debutPeriode.plusDays(nbJoursAAjouter);

        // Détermination du dernier samedi
        int nbJoursARetrancher = (finPeriode.dayOfWeek().get() + 1) % 7;
        dernierSamedi = finPeriode.minusDays(nbJoursARetrancher);

        // Si la période comporte un samedi et un dimanche
        if ((premierDimanche.isBefore(finPeriode) || premierDimanche.isEqual(finPeriode))
            && (dernierSamedi.isAfter(debutPeriode) || dernierSamedi.isEqual(debutPeriode)))
        {
            // calcul du nombre de jours entre le premier dimache et
            // le dernier samedi
            nombreDeJoursOuvres = ((Days.daysBetween(premierDimanche, dernierSamedi).getDays() + 1) / 7) * 5;

            // Si le dimanche n'est pas le premier jour de la
            // période
            if (!premierDimanche.isEqual(debutPeriode))
            {
                int complementPremierDimanche = ((7 - debutPeriode.dayOfWeek().get()) % 7) - 1;
                nombreDeJoursOuvres = nombreDeJoursOuvres + complementPremierDimanche;
            }

            // Si le samedi n'est pas le dernier jour de la période
            if (!dernierSamedi.isEqual(finPeriode))
            {
                // Le dimanche n'est pas ajouté
                int complementDernierSamedi = nbJoursARetrancher - 1;
                nombreDeJoursOuvres = nombreDeJoursOuvres + complementDernierSamedi;
            }
        }
        // Si la période comporte un samedi et pas de dimanche
        // le samedi est alors le dernier jour de la période
        else if ((!(premierDimanche.isBefore(finPeriode) || premierDimanche.isEqual(finPeriode)))
            && (dernierSamedi.isAfter(debutPeriode) || dernierSamedi.isEqual(debutPeriode)))
        {
            nombreDeJoursOuvres = finPeriode.dayOfWeek().get() - debutPeriode.dayOfWeek().get();
        }
        else if (premierDimanche.isEqual(debutPeriode) && premierDimanche.isEqual(finPeriode))
        {
            nombreDeJoursOuvres = 0;
        }
        // Si la période comporte un dimanche et pas de samedi
        // Le dimanche est alors le premier jour de la période
        else if ((premierDimanche.isBefore(finPeriode) || premierDimanche.isEqual(finPeriode))
            && (!(dernierSamedi.isAfter(debutPeriode) || dernierSamedi.isEqual(debutPeriode))))
        {
            nombreDeJoursOuvres = finPeriode.dayOfWeek().get() - (debutPeriode.dayOfWeek().get() % 7);
        }
        // La période ne comporte ni samedi ni dimanche
        else
        {
            nombreDeJoursOuvres = finPeriode.dayOfWeek().get() - debutPeriode.dayOfWeek().get() + 1;
        }

        return nombreDeJoursOuvres;
    }

    /**
     * Construit la liste des jours fériés d'une année donnée.
     * 
     * @param annee Année pour laquelle on cherche la liste des jours fériés
     * @return Liste des dates des jours fériés
     */
    public static List<DateTime> creationListeDesJoursFeriesPourUneAnnee(int annee)
    {
        List<DateTime> joursFeries = new ArrayList<>();

        // Jours fériés dont la date est fixe
        DateTime jourDeLAN = new DateMidnight(annee, 1, 1).toDateTime();
        DateTime jourArmistice3945 = new DateMidnight(annee, 5, 8).toDateTime();
        DateTime jourToussaint = new DateMidnight(annee, 11, 1).toDateTime();
        DateTime jourAssomption = new DateMidnight(annee, 8, 15).toDateTime();
        DateTime jourFeteDuTravail = new DateMidnight(annee, 5, 1).toDateTime();
        DateTime jourFeteNationale = new DateMidnight(annee, 7, 14).toDateTime();
        DateTime jourNoel = new DateMidnight(annee, 12, 25).toDateTime();
        DateTime jourArmistice1418 = new DateMidnight(annee, 11, 11).toDateTime();

        joursFeries.add(jourDeLAN);
        joursFeries.add(jourArmistice3945);
        joursFeries.add(jourToussaint);
        joursFeries.add(jourAssomption);
        joursFeries.add(jourFeteDuTravail);
        joursFeries.add(jourFeteNationale);
        joursFeries.add(jourNoel);
        joursFeries.add(jourArmistice1418);

        // Jours fériés dont la date dépend du jour de Paques
        DateTime jourLundiDePaques = (new DateTime(determineLeJourDePaques(annee))).plusDays(1);
        DateTime jourAscension = (new DateTime(determineLeJourDePaques(annee))).plusDays(39);
        DateTime jourLundiPentecote = (new DateTime(determineLeJourDePaques(annee))).plusDays(50);

        joursFeries.add(jourLundiDePaques);
        joursFeries.add(jourAscension);
        joursFeries.add(jourLundiPentecote);

        return joursFeries;

    }

    /**
     * Construit la liste des jours fériés pour une période définie par une date de début et une date de fin.
     * 
     * @param dateDebut Date de début de la période
     * @param dateFin Date de fin de la période
     * @return Liste des dates des jours fériés
     */
    public static List<DateTime> creationListeDesJoursFeriesPourUnePeriode(Date dateDebut, Date dateFin)
    {
        List<DateTime> joursFeries = new ArrayList<>();
        List<DateTime> joursFeriesPourUnePeriode = new ArrayList<>();

        controleDesDatesEnParametre(dateDebut, dateFin);

        DateTime debutPeriode = new DateTime(dateDebut);
        DateTime finPeriode = new DateTime(dateFin);

        Calendar calDebut = Calendar.getInstance();
        calDebut.setTime(dateDebut);
        int anneeDebutPeriode = calDebut.get(Calendar.YEAR);

        Calendar calFin = Calendar.getInstance();
        calFin.setTime(dateFin);
        int anneeFinPeriode = calFin.get(Calendar.YEAR);

        // On ajoute tous les jours fériés des années concernées
        for (int annee = anneeDebutPeriode; annee <= anneeFinPeriode; annee++)
        {
            joursFeries.addAll(creationListeDesJoursFeriesPourUneAnnee(annee));
        }

        for (Iterator<DateTime> iterJourFeries = joursFeries.iterator(); iterJourFeries.hasNext();)
        {
            DateTime jourFerie =  iterJourFeries.next();
            if (jourFerie != null && (!jourFerie.isAfter(finPeriode)) && (!jourFerie.isBefore(debutPeriode)))
            {
                joursFeriesPourUnePeriode.add(jourFerie);
            }
        }

        return joursFeriesPourUnePeriode;
    }

    /**
     * Détermine le jour de Paques pour une année donnée.
     * 
     * @param annee Année pour laquelle la date du jour de paque est cherchée
     * @return La date du jour de paque
     */
    public static DateTime determineLeJourDePaques(int annee)
    {
        DateTime jourDePaques;
        int mois;
        int nombreOrAnneeCycleMetonic = (annee % 19) + 1;
        int numeroSiecle = annee / 100 + 1;
        int nbAnneesNonBissextiles = 3 * numeroSiecle / 4 - 12;
        int correctionSynChroPaquesOrbtLune = (8 * numeroSiecle + 5) / 25 - 5;
        int dimanche = 5 * annee / 4 - nbAnneesNonBissextiles - 10;
        int epact =
            (11 * nombreOrAnneeCycleMetonic + 20 + correctionSynChroPaquesOrbtLune - nbAnneesNonBissextiles) % 30;
        if (epact == 25 && nombreOrAnneeCycleMetonic > 11 || epact == 24)
        {
            ++epact;
        }
        int pleineLune = 44 - epact;
        if (pleineLune < 21)
        {
            pleineLune = pleineLune + 30;
        }
        int jour = pleineLune + 7 - ((dimanche + pleineLune) % 7);
        if (jour > 31)
        {
            mois = Calendar.APRIL + 1;
            jour = jour - 31;
        }
        else
        {
            mois = Calendar.MARCH + 1;
        }

        jourDePaques = new DateMidnight(annee, mois, jour).toDateTime();

        if (LOG.isDebugEnabled())
        {
            LOG.debug("Jour de Pâques : " + jourDePaques.toString());
        }
        return jourDePaques;

    }

}
