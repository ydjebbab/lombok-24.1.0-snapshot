/*
 * Copyright (c) 2017 DGFiP - Tous droits réservés
 * 
 */
/**
 * Paquet lié aux transactions pour faciliter les tests
 *
 * @author Christophe Breheret-Girardin
 */
package fr.gouv.finances.lombok.test.transaction;