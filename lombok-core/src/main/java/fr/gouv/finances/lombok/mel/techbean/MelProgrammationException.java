/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : MelProgrammationException.java
 *
 */
package fr.gouv.finances.lombok.mel.techbean;

import fr.gouv.finances.lombok.util.exception.ProgrammationException;

/**
 * Class MelProgrammationException --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.2 $ Date: 11 déc. 2009
 */
public class MelProgrammationException extends ProgrammationException
{

    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /**
     * Instanciation de mel programmation exception.
     */
    public MelProgrammationException()
    {
        super();
    }

    /**
     * Instanciation de mel programmation exception.
     * 
     * @param arg0 --
     */
    public MelProgrammationException(String arg0)
    {
        super(arg0);
    }

    /**
     * Instanciation de mel programmation exception.
     * 
     * @param arg0 --
     * @param arg1 --
     */
    public MelProgrammationException(String arg0, Throwable arg1)
    {
        super(arg0, arg1);
    }

    /**
     * Instanciation de mel programmation exception.
     * 
     * @param arg0 --
     */
    public MelProgrammationException(Throwable arg0)
    {
        super(arg0);
    }

}
