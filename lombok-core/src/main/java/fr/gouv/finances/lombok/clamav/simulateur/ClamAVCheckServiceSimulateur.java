/*
 * Copyright (c) 2014 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.clamav.simulateur;

import java.io.IOException;

import fr.gouv.finances.lombok.clamav.service.ClamAVCheckService;

/**
 * 
 * Service d'interfaçage de clamAV, possédant un serveur interne de simulation de clamAV
 *
 * @author Christophe Breheret-Girardin
 */
public interface ClamAVCheckServiceSimulateur extends ClamAVCheckService
{   
    /**
     * Méthode permettant de démarrer l'écoute La boucle d'écoute est lancé dans un thread afin de redonner la main au
     * programme principal.
     * 
     * @throws IOException IOException
     * @throws InterruptedException InterruptedException
     */
    void startServer() throws IOException, InterruptedException;


    /**
     * Arrête les tâches de fond gérant les demandes d'analyse. bascule le booléen et signale au selector (via wakeup)
     * de sortir de l'appel bloquant (select).
     */
    void stopServer();

}
