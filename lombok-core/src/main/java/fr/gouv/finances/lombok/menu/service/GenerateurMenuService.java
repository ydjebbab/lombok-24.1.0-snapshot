/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : GenerateurMenuService.java
 *
 */
package fr.gouv.finances.lombok.menu.service;

import fr.gouv.finances.lombok.menu.techbean.BarreDeMenusHierarchiques;

/**
 * Interface GenerateurMenuService --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.2 $ Date: 11 déc. 2009
 */
public interface GenerateurMenuService
{

    /**
     * methode Generer barre de menu : --.
     * 
     * @param unmenu --
     * @return string
     */
    String genererBarreDeMenu(BarreDeMenusHierarchiques unmenu);

    /**
     * methode Generer barre de menu HTML : --.
     * 
     * @param unmenu --
     * @return string
     */
    String genererBarreDeMenuHtml(BarreDeMenusHierarchiques unmenu);

}
