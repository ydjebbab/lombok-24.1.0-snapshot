/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : UtilisateurSecurityLombok.java
 *
 */
package fr.gouv.finances.lombok.securite.springsecurity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

import javax.naming.directory.Attributes;
import javax.naming.directory.BasicAttributes;
import javax.naming.ldap.Control;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import fr.gouv.finances.lombok.securite.techbean.PersonneAnnuaire;

/**
 * <pre>
 * Classe implémentant l'interface SpringSEcurity UserDetails qui permet de restituer
 * les informations sur l'authentification de l'utilisateur.
 * Cette classe a été redéfinie pour pouvoir raccrocher un objet personneAnnuaire qui soit ainsi disponible en appelant le contexte sécurisé.
 * Exemple d'appel du contexte sécurisé SpringSEcurity pour pouvoir récupérer les informations utilisateur : 
 * Authenticationpa=SecureContextUtils.getSecureContext().getAuthentication(); 
 * UtilisateurSecurityLombok utilisateur = (UtilisateurSecurityLombok)pa.getPrincipal();
 * </pre>
 * 
 * @author chouard-cp
 * @version $Revision: 1.3 $ Date: 14 déc. 2009
 */
public class UtilisateurSecurityLombok implements UserDetails, Serializable
{    
    private static final Logger log = LoggerFactory.getLogger(UtilisateurSecurityLombok.class);
    
    private static final long serialVersionUID = 1L;

    /** personne annuaire. */
    public PersonneAnnuaire personneAnnuaire;

    /** authorities. */
    private Collection<GrantedAuthority> authorities = new ArrayList<>();

    /** password. */
    private String password;

    /** username. */
    private String username;

    /** account non expired. */
    private boolean accountNonExpired = true;

    /** account non locked. */
    private boolean accountNonLocked = true;

    /** credentials non expired. */
    private boolean credentialsNonExpired = true;

    /** enabled. */
    private boolean enabled = true;

    /** attributes. */
    private Attributes attributes = new BasicAttributes();

    /** dn. */
    private String dn;

    /** controls. */
    private Control[] controls = new Control[0];

    /** generationmenu. */
    private String generationmenu;

    /** generationmenuhtml. */
    private String generationmenuhtml;

    /** urlPortail - String,. */
    private String urlPortail;

    /**
     * Instanciation de utilisateur security lombok.
     * 
     * @param personneAnnuaire --
     * @param authorities --
     * @param username --
     * @param password --
     * @param urlPortail "documenté"
     */
    public UtilisateurSecurityLombok(PersonneAnnuaire personneAnnuaire, Collection<GrantedAuthority> authorities,
        String username, String password, String urlPortail)
    {
        this.authorities = authorities;
        this.password = password;
        this.username = username;
        this.personneAnnuaire = personneAnnuaire;
        this.urlPortail = urlPortail;
    }

    /**
     * Instanciation de utilisateur security lombok.
     * 
     * @param personneAnnuaire --
     * @param authorities --
     * @param username --
     * @param password --
     * @param urlPortail "documenté"
     * @param enabled --
     * @param accountNonExpired --
     * @param accountNonLocked --
     * @param credentialsNonExpired --
     */
    public UtilisateurSecurityLombok(PersonneAnnuaire personneAnnuaire, Collection<GrantedAuthority> authorities,
        String username, String password, String urlPortail, boolean enabled, boolean accountNonExpired, boolean accountNonLocked,
        boolean credentialsNonExpired)
    {
        this(personneAnnuaire, authorities, username, password, urlPortail);
        this.enabled = enabled;
        this.accountNonExpired = accountNonExpired;
        this.accountNonLocked = accountNonLocked;
        this.credentialsNonExpired = credentialsNonExpired;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @return the attributes
     * @see org.springframework.security.userdetails.ldap.LdapUserDetails#getAttributes()
     */
    public Attributes getAttributes()
    {
        return attributes;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @return the authorities
     * @see org.springframework.security.userdetails.UserDetails#getAuthorities()
     */
    @Override
    public Collection<GrantedAuthority> getAuthorities()
    {
        return authorities;
    }

    /**
     * Accesseur de l attribut controls.
     * 
     * @return controls
     */
    public Control[] getControls()
    {
        return controls;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @return the dn
     * @see org.springframework.security.userdetails.ldap.LdapUserDetails#getDn()
     */
    public String getDn()
    {
        return dn;
    }

    /**
     * Accesseur de l attribut generationmenu.
     * 
     * @return generationmenu
     */
    public String getGenerationmenu()
    {
        return generationmenu;
    }

    /**
     * Gets the generationmenuhtml.
     * 
     * @return the generationmenuhtml
     */
    public String getGenerationmenuhtml()
    {
        return generationmenuhtml;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @return the password
     * @see org.springframework.security.userdetails.UserDetails#getPassword()
     */
    @Override
    public String getPassword()
    {
        return this.password;
    }

    /**
     * Accesseur de l attribut personne annuaire.
     * 
     * @return personne annuaire
     */
    public PersonneAnnuaire getPersonneAnnuaire()
    {
        return personneAnnuaire;
    }

    /**
     * getter de urlportail.
     * 
     * @return the urlPortail - String,
     */
    public String getUrlPortail()
    {
        return urlPortail;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @return the username
     * @see org.springframework.security.userdetails.UserDetails#getUsername()
     */
    @Override
    public String getUsername()
    {
        return this.username;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @return the account non expired
     * @see org.springframework.security.userdetails.UserDetails#isAccountNonExpired()
     */
    @Override
    public boolean isAccountNonExpired()
    {
        return accountNonExpired;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @return the account non locked
     * @see org.springframework.security.userdetails.UserDetails#isAccountNonLocked()
     */
    @Override
    public boolean isAccountNonLocked()
    {
        return accountNonLocked;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @return the credentials non expired
     * @see org.springframework.security.userdetails.UserDetails#isCredentialsNonExpired()
     */
    @Override
    public boolean isCredentialsNonExpired()
    {
        return credentialsNonExpired;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @return the enabled
     * @see org.springframework.security.userdetails.UserDetails#isEnabled()
     */
    @Override
    public boolean isEnabled()
    {
        return enabled;
    }

    /**
     * Modificateur de l attribut attributes.
     * 
     * @param attributes le nouveau attributes
     */
    public void setAttributes(Attributes attributes)
    {
        this.attributes = attributes;
    }

    /**
     * Modificateur de l attribut authorities.
     * 
     * @param authorities le nouveau authorities
     */
    public void setAuthorities(Collection<GrantedAuthority> authorities)
    {
        this.authorities = authorities;
    }

    /**
     * Modificateur de l attribut controls.
     * 
     * @param controls le nouveau controls
     */
    public void setControls(Control[] controls)
    {
        this.controls = controls;
    }

    /**
     * Modificateur de l attribut dn.
     * 
     * @param dn le nouveau dn
     */
    public void setDn(String dn)
    {
        this.dn = dn;
    }

    /**
     * Modificateur de l attribut generationmenu.
     * 
     * @param generationmenu le nouveau generationmenu
     */
    public void setGenerationmenu(String generationmenu)
    {
        this.generationmenu = generationmenu;
    }

    /**
     * Sets the generationmenuhtml.
     * 
     * @param generationmenuhtml the new generationmenuhtml
     */
    public void setGenerationmenuhtml(String generationmenuhtml)
    {
        this.generationmenuhtml = generationmenuhtml;
    }

    /**
     * Modificateur de l attribut password.
     * 
     * @param password le nouveau password
     */
    public void setPassword(String password)
    {
        this.password = password;
    }

    /**
     * Modificateur de l attribut personne annuaire.
     * 
     * @param personneAnnuaire le nouveau personne annuaire
     */
    public void setPersonneAnnuaire(PersonneAnnuaire personneAnnuaire)
    {
        this.personneAnnuaire = personneAnnuaire;
    }

    /**
     * setter de urlportail.
     * 
     * @param urlPortail the new urlPortail - String,
     */
    public void setUrlPortail(String urlPortail)
    {
        this.urlPortail = urlPortail;
    }

    /**
     * Modificateur de l attribut username.
     * 
     * @param username le nouveau username
     */
    public void setUsername(String username)
    {
        this.username = username;
    }

}
