/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 *
 */
package fr.gouv.finances.lombok.parsertexte.parametrage;

/**
 * Class AttributMapping --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.2 $ Date: 11 déc. 2009
 */
public class AttributMapping
{

    /** id propriete. */
    private String idPropriete = null;

    /** id classe. */
    private String idClasse = null;

    /** attribut. */
    private String attribut = null;

    // cette propriété permet de mapper une ligne dans plusieurs instances de la même classe.
    // Elle définit dont l'identifiant de l'instance qui est impactée par le mapping
    /** identifiant instance original. */
    private String identifiantInstanceOriginal = null;

    /** identifiant instance. */
    private String identifiantInstance = null;

    // Ce paramètre est utile si on utilise un constructeur pour l'initialisation de la classe
    /** position dans constructeur. */
    private int positionDansConstructeur = -1;

    /**
     * Instanciation de attribut mapping.
     * 
     * @param pIdPropriete le id propriete
     * @param pIdClasse le id classe
     * @param pPositionDansConstructeur le position dans constructeur
     */
    public AttributMapping(String pIdPropriete, String pIdClasse, int pPositionDansConstructeur)
    {
        this.idPropriete = pIdPropriete;
        this.idClasse = pIdClasse;
        this.positionDansConstructeur = pPositionDansConstructeur;
    }

    /**
     * Instanciation de attribut mapping.
     * 
     * @param pIdPropriete le id propriete
     * @param pIdClasse le id classe
     * @param pAttribut le attribut
     * @param pIdentifiantInstance le identifiant instance
     */
    public AttributMapping(String pIdPropriete, String pIdClasse, String pAttribut, String pIdentifiantInstance)
    {
        this.idPropriete = pIdPropriete;
        this.idClasse = pIdClasse;
        this.attribut = pAttribut;

        // Identifiant original sert à mémoriser le fait qu'on a affecté un identifiant spécifique à cette instance
        // (dc que ce n'est pas un identifiant calculé).
        this.identifiantInstanceOriginal = pIdentifiantInstance;
        if (this.identifiantInstanceOriginal == null)
        {
            this.identifiantInstance = this.idClasse;
        }
        else
        {
            this.identifiantInstance = this.identifiantInstanceOriginal;
        }
    }

    /**
     * Accesseur de l attribut attribut.
     * 
     * @return attribut
     */
    public String getAttribut()
    {
        return attribut;
    }

    /**
     * Accesseur de l attribut id classe.
     * 
     * @return id classe
     */
    public String getIdClasse()
    {
        return idClasse;
    }

    /**
     * Accesseur de l attribut identifiant instance.
     * 
     * @return identifiant instance
     */
    public String getIdentifiantInstance()
    {
        return identifiantInstance;
    }

    /**
     * Accesseur de l attribut id propriete.
     * 
     * @return id propriete
     */
    public String getIdPropriete()
    {
        return idPropriete;
    }

    /**
     * Accesseur de l attribut position dans constructeur.
     * 
     * @return position dans constructeur
     */
    public int getPositionDansConstructeur()
    {
        return positionDansConstructeur;
    }

    /**
     * Modificateur de l attribut attribut.
     * 
     * @param attribut le nouveau attribut
     */
    public void setAttribut(String attribut)
    {
        this.attribut = attribut;
    }

    /**
     * Modificateur de l attribut id classe.
     * 
     * @param idClasse le nouveau id classe
     */
    public void setIdClasse(String idClasse)
    {
        this.idClasse = idClasse;

        if (this.identifiantInstanceOriginal == null)
        {
            this.identifiantInstance = this.idClasse;
        }
    }

    /**
     * Modificateur de l attribut identifiant instance.
     * 
     * @param identifiantInstance le nouveau identifiant instance
     */
    public void setIdentifiantInstance(String identifiantInstance)
    {
        this.identifiantInstanceOriginal = identifiantInstance;
        this.identifiantInstance = identifiantInstance;
    }

    /**
     * Modificateur de l attribut id propriete.
     * 
     * @param idPropriete le nouveau id propriete
     */
    public void setIdPropriete(String idPropriete)
    {
        this.idPropriete = idPropriete;
    }

    /**
     * Modificateur de l attribut position dans constructeur.
     * 
     * @param positionDansConstructeur le nouveau position dans constructeur
     */
    public void setPositionDansConstructeur(int positionDansConstructeur)
    {
        this.positionDansConstructeur = positionDansConstructeur;
    }
}
