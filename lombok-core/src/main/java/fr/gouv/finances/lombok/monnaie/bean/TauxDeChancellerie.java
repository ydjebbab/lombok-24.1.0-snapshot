/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 */
package fr.gouv.finances.lombok.monnaie.bean;

import java.util.Date;
import java.util.Objects;

import fr.gouv.finances.lombok.util.base.BaseBean;

/**
 * Reprise d'éléments communs au bean TauxDeChancellerie dans eaf et coriolis.
 * 
 * @author amleplatinec-cp
 * @author Christophe Breheret-Girardin
 */
public class TauxDeChancellerie extends BaseBean
{

    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** id. */
    private Long id;

    /** version. */
    private int version;

    /** date taux chancel. */
    private Date dateTauxChancel;

    /** valeur taux chancel. */
    private Double valeurTauxChancel;

    /** la monnaie. */
    private Monnaie laMonnaie;

    /**
     * Constructeur.
     */
    public TauxDeChancellerie()
    {
        super();
    }

    /** 
     * {@inheritDoc}
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode()
    {
        return Objects.hash(valeurTauxChancel, dateTauxChancel);
    }

    /** 
     * {@inheritDoc}
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj)
    {
        if (obj == this)
        {
            return true;
        }

        if (obj == null || this.getClass() != obj.getClass())
        {
            return false;
        }

        TauxDeChancellerie tauxDeChancellerie = (TauxDeChancellerie) obj;
        return Objects.equals(valeurTauxChancel, tauxDeChancellerie.valeurTauxChancel)
            && Objects.equals(dateTauxChancel, tauxDeChancellerie.dateTauxChancel);
    }

    /**
     * Accesseur de l'attribut date taux chancel.
     * 
     * @return date taux chancel
     */
    public Date getDateTauxChancel()
    {
        return dateTauxChancel;
    }

    /**
     * Accesseur de l'attribut id.
     * 
     * @return id
     */
    public Long getId()
    {
        return id;
    }

    /**
     * Accesseur de l'attribut la monnaie.
     * 
     * @return la monnaie
     */
    public Monnaie getLaMonnaie()
    {
        return laMonnaie;
    }

    /**
     * Accesseur de l'attribut valeur taux chancel.
     * 
     * @return valeur taux chancel
     */
    public Double getValeurTauxChancel()
    {
        return valeurTauxChancel;
    }

    /**
     * Accesseur de l'attribut version.
     * 
     * @return version
     */
    public int getVersion()
    {
        return version;
    }

    /**
     * Modificateur de l'attribut date taux chancel.
     * 
     * @param dateTauxChancel le nouveau date taux chancel
     */
    public void setDateTauxChancel(Date dateTauxChancel)
    {
        this.dateTauxChancel = dateTauxChancel;
    }

    /**
     * Modificateur de l'attribut id.
     * 
     * @param id le nouveau id
     */
    public void setId(Long id)
    {
        this.id = id;
    }

    /**
     * Modificateur de l'attribut la monnaie.
     * 
     * @param laMonnaie le nouveau la monnaie
     */
    public void setLaMonnaie(Monnaie laMonnaie)
    {
        this.laMonnaie = laMonnaie;
    }

    /**
     * Modificateur de l'attribut valeur taux chancel.
     * 
     * @param valeurTauxChancel le nouveau valeur taux chancel
     */
    public void setValeurTauxChancel(Double valeurTauxChancel)
    {
        this.valeurTauxChancel = valeurTauxChancel;
    }

    /**
     * Modificateur de l'attribut version.
     * 
     * @param version le nouveau version
     */
    public void setVersion(int version)
    {
        this.version = version;
    }
}
