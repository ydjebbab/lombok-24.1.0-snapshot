/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : GestionnaireAnomalie.java
 *
 */
package fr.gouv.finances.lombok.parsertexte.integration;

import java.util.ArrayList;
import java.util.List;

/**
 * Class GestionnaireAnomalie --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.2 $ Date: 11 déc. 2009
 */
public class GestionnaireAnomalie
{

    /** liste anomalie. */
    private final List listeAnomalie;

    /**
     * Instanciation de gestionnaire anomalie.
     */
    public GestionnaireAnomalie()
    {
        super();
        this.listeAnomalie = new ArrayList();
    }

    /**
     * methode Ajouter anomalie : --.
     * 
     * @param pAnomalie le anomalie
     */
    public void ajouterAnomalie(Anomalie pAnomalie)
    {
        this.listeAnomalie.add(pAnomalie);
    }

}
