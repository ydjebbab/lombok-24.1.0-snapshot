/*
 * Copyright (c) 2014 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.clamav.simulateur;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Simulateur du buffer de la réponse.
 */
public class ClamAVBufferInDataSimulateur
{
    /** Initialisation de la journalisation. */
    protected static final Logger LOGGER = LoggerFactory.getLogger(ClamAVBufferInDataSimulateur.class);

    /** Data. */
    private ByteArrayOutputStream data;

    /** La commande a t-elle été lue ?. */
    private boolean isCmdLue;

    /** Le flux a t-il été lu ?. */
    private boolean isStreamLu;

    /** Taille du flux. */
    private int streamLength;

    /** Position de la taille du contenu du fichier dans le flux obtenu. */
    private int dataSizePositionInStream;

    /** Le flux sous forme de chaine de caractères */
    private String streamStr;
    
    /** Taille de lecture des octets */
    private static final int NOMBRE_OCTETS_RECUS = 4;
    
    /**
     * Catégorie de commande pouvant être traité par le simulateur.
     */
    public enum Command
    {
        /** inconnu. */
        INCONNU,
        /** ping. */
        PING,
        /** analyse. */
        ANALYSE
    }

    /** Commande courante. */
    private Command command;

    /**
     * Constructeur.
     */
    public ClamAVBufferInDataSimulateur()
    {
        super();
        final int taille = 1024;
        this.data = new ByteArrayOutputStream(taille);
        this.streamLength = -1;
    }

    /**
     * Conversion d'un tableau d'octet en entier.
     * 
     * @param octets tableau d'octet
     * @param offset position de l'entier à lire
     * @return un entier
     */
    public static int byteArrayToInt(final byte[] octets, final int offset)
    {
        final int taille = 4;
        int value = 0;
        for (int i = 0; i < taille; i++)
        {
            final int shift = (4 - 1 - i) * 8;
            value += (octets[i + offset] & 0x000000FF) << shift;
        }
        return value;
    }

    /**
     * Méthode permettant de poursuivre la lecture des données.
     * 
     * @param newData nouvelle données à lire
     * @param offset départ
     * @param len longueur
     */
    public void add(final byte[] newData, final int offset, final int len)
    {
        this.data.write(newData, offset, len);
    }

    /**
     * Analyse des données reçues pour vérifier si la commande et le flux éventuel ont bien été transmis.
     * 
     * @return vrai si toutes les données à lire ont été lues.
     */
    public boolean processData() throws IOException
    {
        boolean ret;
        ret = false;
        
        LOGGER.debug("Commande lue ? : {}", this.isCmdLue);
        // Lecture de la commande
        if (!this.isCmdLue)
        {
            String sData;
            try
            {
                sData = new String(this.data.toByteArray(), 0, this.data.size());
            }
            finally
            {
                closeData();
            }
            // La commande est un PING
            if (sData.startsWith("nPING\n"))
            {
                this.command = Command.PING;
                this.isCmdLue = true;
                LOGGER.trace("PING trouvé");
                return true;
            }
            // Lecture du contenu de la commande
            else if (sData.startsWith("nIDSESSION\nnINSTREAM\n"))
            {
                this.command = Command.ANALYSE;
                LOGGER.trace("INSTREAM trouvé");
                this.isCmdLue = true;
                this.dataSizePositionInStream ="nIDSESSION\nnINSTREAM\n".length();

            }
        }

        LOGGER.debug("Commande : {}", this.command);
        if (this.isCmdLue && (this.command == Command.ANALYSE))
        {
            final int taille = 1024;
            ByteArrayOutputStream ndata = null;
            try
            {
                // Les 4 octets donnant la taille du flux n'ont pas encore été décodés
                if (this.streamLength == -1 &&
                    this.data.size() - this.dataSizePositionInStream >= NOMBRE_OCTETS_RECUS)
                {

                    final byte[] octets = this.data.toByteArray();
                    this.streamLength = ClamAVBufferInDataSimulateur.byteArrayToInt(
                        octets, this.dataSizePositionInStream);

                    ndata = new ByteArrayOutputStream(
                            this.streamLength + this.dataSizePositionInStream + taille);
                    try
                    {
                        this.data.writeTo(ndata);
                    }
                    catch (final IOException ioe)
                    {
                        this.streamLength = -1;
                        ret = false;
                        LOGGER.error(ioe.getMessage(), ioe);
                    }
                    this.data = ndata;

                    if (LOGGER.isInfoEnabled())
                    {
                        final int position1 = 1;
                        final int position2 = 2;
                        final int position3 = 3;
                        LOGGER.info(String.format(
                            "Taille du flux %d bytes %d %d %d %d ",
                            this.streamLength,
                            octets[this.dataSizePositionInStream],
                            octets[this.dataSizePositionInStream + position1],
                            octets[this.dataSizePositionInStream + position2],
                            octets[this.dataSizePositionInStream + position3]));
                    }
                }
            }
            finally
            {
                // Fermeture du flux lu
                if (ndata != null)
                {
                    try
                    {
                        ndata.close();
                    }
                    catch (IOException ioe)
                    {
                        LOGGER.error("Simulateur - Erreur lors de la fermeture du flux", ioe);
                    }
                }
            }

            if (LOGGER.isInfoEnabled())
            {
                LOGGER.info(
                    String.format("Simulateur - Avancement %.2f ", (float) this.data.size() / this.streamLength));
            }

            // Tout le flux a-t-il été reçu ?
            if ((this.streamLength != -1)
                && (this.data.size() >= this.dataSizePositionInStream + NOMBRE_OCTETS_RECUS
                    + this.streamLength + NOMBRE_OCTETS_RECUS))
            {
                LOGGER.trace("Flux lu");
                this.isStreamLu = true;
                ret = true;
            }
        }
        return ret;
    }

    /**
     * Récupération du contenu du flux envoyé.
     * 
     * @return contenu du fichier
     */
    public String getStreamAsString()
    {
        try
        {
            String ret = null;
            final byte[] bData = this.data.toByteArray();
    
            if (LOGGER.isDebugEnabled())
            {
                LOGGER.debug(String.format(
                    "Simulateur - Stream content  %d %d", this.dataSizePositionInStream, bData.length));
            }

            // On copie la chaine sans les 4 octets donnant la taille du
            // flux et sans les 4 octets (à zéro) donnant la fin du flux et sans le \r\n
            // soit -10 dans le cas d'une réponse vide, la taille est négative
            final int indice = 10;
            int tailleReponse = bData.length - this.dataSizePositionInStream - indice;
            if (tailleReponse > 0)
            {
                ret = new String(bData, this.dataSizePositionInStream + NOMBRE_OCTETS_RECUS, tailleReponse);
            }
            return ret;
        }
        finally
        {
            closeData();
        }
    }

    /**
     * Méthode de fermeture du flux
     */
    private void closeData()
    {
        if (this.data != null)
        {
            try
            {
                // Fermeture du flux
                LOGGER.trace("Simulateur - Fermeture du flux reçu");
                this.data.close();
            }
            catch (IOException ioe)
            {
                LOGGER.error("Simulateur - le flux reçu n'a pas pu être fermé", ioe);
            }
        }
    }

    /**
     * Accesseur du champ isStreamReadden.
     * 
     * @return la valeur booléenne "vrai" si le flux a été lu, "faux" sinon
     */
    public boolean isStreamLu()
    {
        return this.isStreamLu;
    }

    /**
     * Accesseur du champ isCmdLue.
     * 
     * @return la valeur booléenne "vrai" si la commande a été lue, "faux" sinon
     */
    public boolean isCmdLue()
    {
        return this.isCmdLue;
    }

    /**
     * Accesseur de command
     *
     * @return command
     */
    public Command getCommand()
    {
        return command;
    }

    /**
     * Accesseur de streamStr
     *
     * @return streamStr
     */
    public String getStreamStr()
    {
        return streamStr;
    }
}
