/*
 * Copyright (c) 2017 DGFiP - Tous droits réservés
 * 
 */
/**
 * Paquet lié aux contrôles de données des DAO, au sein des TU.
 *
 * @author Christophe Breheret-Girardin
 */
package fr.gouv.finances.lombok.test.controle;