/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : ArchiveTar.java
 *
 */
package fr.gouv.finances.lombok.util.tar;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.Adler32;
import java.util.zip.CheckedOutputStream;
import java.util.zip.GZIPOutputStream;

import org.apache.tools.tar.TarEntry;
import org.apache.tools.tar.TarOutputStream;

/**
 * Class ArchiveTar --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.3 $ Date: 11 déc. 2009
 */
public class ArchiveTar
{
    /** TAILL e_ buffe r_ pa r_ defaut. */
    private static int tailleBUFFERPARDEFAUT = 2048;

    /** file out. */
    private final FileOutputStream fileOut;

    /** check out. */
    private final CheckedOutputStream checkOut;

    /** buff out. */
    private final BufferedOutputStream buffOut;

    /** gzip out. */
    private final GZIPOutputStream gzipOut;

    /** tar out. */
    private final TarOutputStream tarOut;

    /** taille buffer. */
    private final int tailleBuffer;

    /**
     * Instanciation de archive tar.
     * 
     * @param nomArchive --
     * @throws IOException Signal qu'une execption de type I/O s'est produite.
     */
    public ArchiveTar(String nomArchive) throws IOException
    {
        fileOut = new FileOutputStream(nomArchive);
        checkOut = new CheckedOutputStream(fileOut, new Adler32());
        buffOut = new BufferedOutputStream(checkOut);
        gzipOut = new GZIPOutputStream(buffOut);
        tarOut = new TarOutputStream(gzipOut);
        tailleBuffer = tailleBUFFERPARDEFAUT;
    }

    /**
     * Instanciation de archive tar.
     * 
     * @param nomArchive --
     * @param tailleBuffer --
     * @throws IOException Signal qu'une execption de type I/O s'est produite.
     */
    public ArchiveTar(String nomArchive, int tailleBuffer) throws IOException
    {
        fileOut = new FileOutputStream(nomArchive);
        checkOut = new CheckedOutputStream(fileOut, new Adler32());
        buffOut = new BufferedOutputStream(checkOut);
        gzipOut = new GZIPOutputStream(buffOut);
        tarOut = new TarOutputStream(gzipOut);
        this.tailleBuffer = tailleBuffer;
    }

    /**
     * methode Creer entree : --.
     * 
     * @param nomEntreeBinaire --
     * @param donnees --
     * @throws IOException Signal qu'une execption de type I/O s'est produite.
     */
    public void creerEntree(String nomEntreeBinaire, byte[] donnees) throws IOException
    {
        ByteArrayInputStream fi = new ByteArrayInputStream(donnees);
        BufferedInputStream buffi = new BufferedInputStream(fi);
        TarEntry entry = new TarEntry(nomEntreeBinaire);

        entry.setSize(donnees.length);
        tarOut.putNextEntry(entry);
        int count;
        byte[] data = new byte[tailleBuffer];

        while ((count = buffi.read(data, 0, tailleBuffer)) != -1)
        {
            tarOut.write(data, 0, count);
        }
        tarOut.closeEntry();
        buffi.close();
        fi.close();
    }

    /**
     * methode Fermer archive : --.
     * 
     * @throws IOException Signal qu'une execption de type I/O s'est produite.
     */
    public void fermerArchive() throws IOException
    {
        tarOut.close();
        gzipOut.close();
        buffOut.close();
        checkOut.close();
        fileOut.close();
    }

}
