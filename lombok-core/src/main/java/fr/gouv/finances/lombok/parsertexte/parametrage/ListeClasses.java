/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : ListeClasses.java
 *
 */
package fr.gouv.finances.lombok.parsertexte.parametrage;

import java.util.HashMap;
import java.util.Map;

/**
 * Class ListeClasses --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.2 $ Date: 11 déc. 2009
 */
public class ListeClasses
{

    /** classes mappees. */
    Map classesMappees = null;

    /**
     * Instanciation de liste classes.
     */
    public ListeClasses()
    {
        super();
    }

    /**
     * methode Ajouter classe mappee : --.
     * 
     * @param pClasse le classe
     */
    public void ajouterClasseMappee(Classe pClasse)
    {
        if (classesMappees == null)
        {
            classesMappees = new HashMap();
        }

        classesMappees.put(pClasse.id, pClasse);
    }

    /**
     * Accesseur de l attribut classes mappees.
     * 
     * @return classes mappees
     */
    public Map getClassesMappees()
    {
        return this.classesMappees;
    }

    /**
     * methode Retourner classe pour id : --.
     * 
     * @param pId le id
     * @return classe
     */
    public Classe retournerClassePourId(String pId)
    {
        if (classesMappees == null)
        {
            return null;
        }
        return (Classe) classesMappees.get(pId);
    }

    /**
     * Modificateur de l attribut classes mappees.
     * 
     * @param classesMappees le nouveau classes mappees
     */
    public void setClassesMappees(Map classesMappees)
    {
        this.classesMappees = classesMappees;
    }

}
