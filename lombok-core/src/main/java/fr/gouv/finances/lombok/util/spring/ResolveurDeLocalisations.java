/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : ResolveurDeLocalisations.java
 *
 */
package fr.gouv.finances.lombok.util.spring;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ResourceBundle;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;

import fr.gouv.finances.lombok.util.constantes.ConstantesGlobales;

/**
 * Class ResolveurDeLocalisations --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.2 $ Date: 11 déc. 2009
 */
public class ResolveurDeLocalisations
{

    /** Constant : LOCALISATION_PREFIXE. */
    public static final String LOCALISATION_PREFIXE = "$" + ConstantesGlobales.LOCALISATION_BUNDLE_NAME + "{";

    /** Constant : LOCALISATION_SUFFIXE. */
    public static final String LOCALISATION_SUFFIXE = "}";

    /** Constant : LOCALISATION_BUNDLE. */
    public static final ResourceBundle LOCALISATION_BUNDLE = ConstantesGlobales.LOCALISATION_RESOURCE_BUNDLE;

    /** Constant : PREFIXE_RESOLUTION_WEB. */
    public static final String PREFIXE_RESOLUTION_WEB =
        LOCALISATION_PREFIXE + ConstantesGlobales.LOCALISATION_CLE_WEB_ROOT + LOCALISATION_SUFFIXE;

    /** Constant : PREFIXE_RESOLUTION_BATCH. */
    public static final String PREFIXE_RESOLUTION_BATCH =
        LOCALISATION_PREFIXE + ConstantesGlobales.LOCALISATION_CLE_BATCH_ROOT + LOCALISATION_SUFFIXE;

    /** Constant : log. */
    protected static final Log log = LogFactory.getLog(ResolveurDeLocalisations.class.getName());

    /**
     * methode Resoudre url localisation : --.
     * 
     * @param location --
     * @return resource
     */
    public static Resource resoudreURLLocalisation(Resource location)
    {
        Resource[] unTableau = new FileSystemResource[1];
        unTableau[0] = location;
        unTableau = resoudreURLLocalisation(unTableau);
        location = unTableau[0];
        return location;
    }

    /**
     * Classe permettant de remplacer les placeholders liées au fichier localisation avant le PropertyPlaceHolder de
     * spring.
     * 
     * @param locations --
     * @return the resource[]
     * @author lcontinsouzas-cp
     */
    public static Resource[] resoudreURLLocalisation(Resource[] locations)
    {
        for (int i = 0; i < locations.length; i++)
        {
            Resource unelocation = locations[i];

            String uncheminparse = null;
            try
            {
                uncheminparse = unelocation.getURL().getFile();
                if (uncheminparse.indexOf(LOCALISATION_PREFIXE) != -1)
                {
                    log.debug("chemin reçu (url)" + uncheminparse);
                    uncheminparse = parseStringValue(uncheminparse, null);
                    log.debug("chemin transformé (url)" + uncheminparse);
                    Resource unenouvellelocation;
                    try
                    {
                        unenouvellelocation = new UrlResource(uncheminparse);
                    }
                    catch (MalformedURLException malfe)
                    {
                        log.error("L'adresse transmise n'est pas une URL  :" + uncheminparse);
                        throw new RuntimeException(malfe.getMessage(), malfe);
                    }
                    locations[i] = unenouvellelocation;
                }
                else
                {
                    log.debug("chemin non transformé" + uncheminparse);
                }
            }
            catch (IOException ioe)
            {
                // rustine pour les fichiers qui ne sont pas obligatoires. on reverra plus tard ce Resolveur et
                // ConstantesGlobales.
                if (unelocation.getFilename().contains("override.properties") || unelocation.getFilename().contains("test.properties"))
                {

                    log.warn("Fichier optionnel " + unelocation.getFilename() + " non disponible.");

                }
                else
                {
                    log.error("Probleme dans la lecture du chemin de la ressource transmise (avant transformation) :"
                        + unelocation.getFilename());
                    throw new RuntimeException(ioe.getMessage(), ioe);
                }
            }

        }
        return locations;
    }

    /**
     * methode Resoudre url localisation et transformer en path : --.
     * 
     * @param location --
     * @return string
     */
    public static String resoudreURLLocalisationEtTransformerEnPath(String location)
    {
        String[] unTableau = new String[1];
        unTableau[0] = location;
        unTableau = ResolveurDeLocalisations.resoudreURLLocalisationEtTransformerEnPath(unTableau);
        location = unTableau[0];
        return location;
    }

    /**
     * methode Resoudre url localisation et transformer en path : --.
     * 
     * @param locations --
     * @return string[]
     */
    public static String[] resoudreURLLocalisationEtTransformerEnPath(String[] locations)
    {
        // Properties localisationsproperties = null;
        // localisationsproperties = chargerfichierproperties();

        URL uneURL = null;
        for (int i = 0; i < locations.length; i++)
        {

            String unelocation = locations[i];
            log.debug("chemin reçu (url) : " + unelocation);
            if (unelocation.indexOf(LOCALISATION_PREFIXE) != -1)
            {
                String unenouvellelocation = parseStringValue(unelocation, null);
                try
                {
                    uneURL = new URL(unenouvellelocation);
                }
                catch (MalformedURLException exc)
                {
                    log.error("L'adresse transmise n'est pas une URL  :" + unenouvellelocation);
                    throw new RuntimeException(exc.getMessage(), exc);
                }
                unenouvellelocation = uneURL.getPath();
                log.debug("chemin transforme (path)" + unenouvellelocation);
                locations[i] = unenouvellelocation;
            }
            else
            {
                log.debug("chemin non transformé" + unelocation);
            }
        }
        return locations;
    }

    /**
     * Parse the given String value recursively, to be able to resolve nested placeholders (when resolved property
     * values in turn contain placeholders again).
     * 
     * @param strVal the String value to parse
     * @param originalPlaceholder the original placeholder, used to detect circular references between placeholders.
     *        Only non-null if we're parsing a nested placeholder.
     * @return the string
     */
    protected static String parseStringValue(String strVal, String originalPlaceholder)
    {

        StringBuilder buf = new StringBuilder(strVal);
        String placeholderPrefix = LOCALISATION_PREFIXE;
        String placeholderSuffix = LOCALISATION_SUFFIXE;
        // The following code does not use JDK 1.4's StringBuffer.indexOf(String)
        // method to retain JDK 1.3 compatibility. The slight loss in performance
        // is not really relevant, as this code will typically just run on startup.

        int startIndex = strVal.indexOf(placeholderPrefix);
        while (startIndex != -1)
        {
            int endIndex = buf.toString().indexOf(placeholderSuffix, startIndex + placeholderPrefix.length());
            if (endIndex != -1)
            {
                String placeholder = buf.substring(startIndex + placeholderPrefix.length(), endIndex);
                String originalPlaceholderToUse = null;

                if (originalPlaceholder != null)
                {
                    originalPlaceholderToUse = originalPlaceholder;
                    if (placeholder.equals(originalPlaceholder))
                    {
                        log.error("Attention une clé circulaire de localisation a été trouvée");
                        throw new RuntimeException("Circular placeholder reference '" + placeholder
                            + "' in property definitions");
                    }
                }
                else
                {
                    originalPlaceholderToUse = placeholder;
                }

                String propVal = null;

                if (ConstantesGlobales.CONF_DIR_PARAM.equals(placeholder))
                {
                    propVal = ConstantesGlobales.determineURIexternalFormRepertoireConfiguration();
                    log.debug("Localisation des fichiers de configuration : " + propVal);
                }
                else
                {
                    propVal = LOCALISATION_BUNDLE.getString(placeholder);
                }

                if (propVal != null)
                {
                    // Recursive invocation, parsing placeholders contained in the
                    // previously resolved placeholder value.
                    propVal = parseStringValue(propVal, originalPlaceholderToUse);
                    buf.replace(startIndex, endIndex + placeholderSuffix.length(), propVal);
                    startIndex = buf.toString().indexOf(placeholderPrefix, startIndex + propVal.length());
                }
                else
                {
                    log.error("La clé recherchée dans le fichier properties de localisation " + placeholder
                        + " est absente ou introuvable");
                    throw new RuntimeException("Could not resolve placeholder '" + placeholder + "'");
                }
            }
            else
            {
                startIndex = -1;
            }
        }

        return buf.toString();
    }

}