/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : TypeControles.java
 *
 */
package fr.gouv.finances.lombok.parsertexte.parametrage;

/**
 * Class TypeControles --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.2 $ Date: 11 déc. 2009
 */
public class TypeControles
{

    /** Constant : CONTROLE_AUCUN. */
    public static final int CONTROLE_AUCUN = 0;

    /** Constant : CONTROLE_ALPHABETIQUE. */
    public static final int CONTROLE_ALPHABETIQUE = 1;

    /** Constant : CONTROLE_ALPHANUMERIQUE. */
    public static final int CONTROLE_ALPHANUMERIQUE = 2;

    /** Constant : CONTROLE_NUMERIQUE. */
    public static final int CONTROLE_NUMERIQUE = 3;

    /** Constant : CONTROLE_NUMERIQUE_FLOTTANT. */
    public static final int CONTROLE_NUMERIQUE_FLOTTANT = 4;

    /**
     * CONTROLE_NUMERIQUE_FLOTTANT_NEGATIF - int,
     */
    public static final int CONTROLE_NUMERIQUE_FLOTTANT_NEGATIF = 5;

    /**
     * CONTROLE_ALPHABETIQUE_INCLUANTESPACE - int,
     */
    public static final int CONTROLE_ALPHABETIQUE_INCLUANTESPACE = 6;

    /**
     * CONTROLE_ALPHANUMERIQUE_INCLUANTESPACE - int,
     */
    public static final int CONTROLE_ALPHANUMERIQUE_INCLUANTESPACE = 7;

    /**
     * CONTROLE_NUMERIQUE_NEGATIF - int,
     */
    public static final int CONTROLE_NUMERIQUE_NEGATIF = 8;

    /**
     * CONTROLE_ALPHANUMERIQUE_AVECAROBASE - int,
     */
    public static final int CONTROLE_ALPHANUMERIQUE_AVECAROBASE = 9;

    public static final int CONTROLE_ALPHANUMERIQUE_AVECAROBASE_INCLUANTESPACE = 10;

    /**
     * Constructeur de la classe TypeControles.java
     */
    public TypeControles()
    {
        // controleur vide
    }

}
