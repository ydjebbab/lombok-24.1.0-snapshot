/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 */
package fr.gouv.finances.lombok.parsertexte.parametrage;

/**
 * Class DefinitionCustomPropertyEditor --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.2 $ Date: 11 déc. 2009
 */
public class DefinitionCustomPropertyEditor
{

    /** Constant : INSTANCIATION_STANDARD. */
    public static final int INSTANCIATION_STANDARD = 1;

    /** Constant : INSTANCIATION_CUSTOM. */
    public static final int INSTANCIATION_CUSTOM = 2;

    /** chemin custom property editor. */
    private String cheminCustomPropertyEditor = null;

    /** type gere. */
    private String typeGere = null;

    /** mode instanciation. */
    private int modeInstanciation = -1;

    /** id. */
    private String id = null;

    /**
     * Instanciation de definition custom property editor.
     * 
     * @param pId le id
     * @param pTypeGere le type gere
     * @param pModeInstanciation le mode instanciation
     */
    public DefinitionCustomPropertyEditor(String pId, String pTypeGere, int pModeInstanciation)
    {
        this.typeGere = pTypeGere;
        this.modeInstanciation = pModeInstanciation;
        this.id = pId;
    }

    /**
     * Instanciation de definition custom property editor.
     * 
     * @param pId le id
     * @param pCheminPropertyEditor le chemin property editor
     * @param pTypeGere le type gere
     * @param pModeInstanciation le mode instanciation
     */
    public DefinitionCustomPropertyEditor(String pId, String pCheminPropertyEditor, String pTypeGere,
        int pModeInstanciation)
    {
        this.cheminCustomPropertyEditor = pCheminPropertyEditor;
        this.typeGere = pTypeGere;
        this.modeInstanciation = pModeInstanciation;
        this.id = pId;
    }

    /**
     * Accesseur de l attribut chemin custom property editor.
     * 
     * @return chemin custom property editor
     */
    public String getCheminCustomPropertyEditor()
    {
        return cheminCustomPropertyEditor;
    }

    /**
     * Accesseur de l attribut id.
     * 
     * @return id
     */
    public String getId()
    {
        return id;
    }

    /**
     * Accesseur de l attribut mode instanciation.
     * 
     * @return mode instanciation
     */
    public int getModeInstanciation()
    {
        return modeInstanciation;
    }

    /**
     * Accesseur de l attribut type gere.
     * 
     * @return type gere
     */
    public String getTypeGere()
    {
        return typeGere;
    }

    /**
     * Modificateur de l attribut chemin custom property editor.
     * 
     * @param cheminCustomPropertyEditor le nouveau chemin custom property editor
     */
    public void setCheminCustomPropertyEditor(String cheminCustomPropertyEditor)
    {
        this.cheminCustomPropertyEditor = cheminCustomPropertyEditor;
    }

    /**
     * Modificateur de l attribut mode instanciation.
     * 
     * @param modeInstanciation le nouveau mode instanciation
     */
    public void setModeInstanciation(int modeInstanciation)
    {
        this.modeInstanciation = modeInstanciation;
    }

    /**
     * Modificateur de l attribut nom.
     * 
     * @param id le nouveau nom
     */
    public void setNom(String id)
    {
        this.id = id;
    }

    /**
     * Modificateur de l attribut type gere.
     * 
     * @param typeGere le nouveau type gere
     */
    public void setTypeGere(String typeGere)
    {
        this.typeGere = typeGere;
    }

}
