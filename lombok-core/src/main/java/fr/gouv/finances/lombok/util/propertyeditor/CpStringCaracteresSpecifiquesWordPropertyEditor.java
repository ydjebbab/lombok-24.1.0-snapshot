/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : CpStringCaracteresSpecifiquesWordPropertyEditor.java
 *
 */
package fr.gouv.finances.lombok.util.propertyeditor;

/**
 * Class CpStringCaracteresSpecifiquesWordPropertyEditor classe héritant cpstringkeyboardeditor dans le cas d'un
 * copier/coller de texte depuis word l' apostrophe typographique (code \u2019) ’ n'est pas un caractères valide dans le
 * CpStringKeyboardEditor ( normal car n'est pas un caractére clavier) et est donc remplacé par un blanc. on le remplace
 * donc par un apostrophe droit les guillments et les fractions se copient bien depuis un doc word , en effet guillemet
 * = u00AB U00BB traités dans CpStringKeyboardEditor et fraction aussi présent / pour l'instant
 * 
 * @author amleplatinec-cp
 * @version $Revision: 1.2 $ Date: 15 déc. 2009
 */
public class CpStringCaracteresSpecifiquesWordPropertyEditor extends CpStringKeyboardEditor
{

    /**
     * Instanciation de cp string caracteres specifiques word property editor.
     * 
     * @param emptyAsNull --
     * @param trim --
     * @param acceptParagraphSeparator --
     */
    public CpStringCaracteresSpecifiquesWordPropertyEditor(boolean emptyAsNull, boolean trim,
        boolean acceptParagraphSeparator)
    {
        super(emptyAsNull, trim, acceptParagraphSeparator);
        //  Auto-generated constructor stub
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param text le nouveau as text
     * @see fr.gouv.finances.lombok.util.propertyeditor.CpStringKeyboardEditor#setAsText(java.lang.String)
     */
    public void setAsText(String text)
    {
        String epureValue = this.replaceApostrophe(text);
        super.setAsText(epureValue);
    }

    /**
     * methode Replace apostrophe : --.
     * 
     * @param text --
     * @return string
     */
    private String replaceApostrophe(String text)
    {
        StringBuilder   cleanValue = new StringBuilder();
        cleanValue.append(text.replaceAll("\u2019", "'"));
        return cleanValue.toString();
    }

}
