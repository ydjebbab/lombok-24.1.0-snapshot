package fr.gouv.finances.lombok.autoconfig;

/**
 * Classe mère des classes de propriétés: contient les propriétés communes aux composants
 * @author celinio fernandes
 * Date: Feb 25, 2020
 */
public class LombokComposantProperties
{

    protected Boolean inclus;

    public Boolean getInclus()
    {
        return inclus;
    }

    public void setInclus(Boolean inclus)
    {
        this.inclus = inclus;
    }
    

}
