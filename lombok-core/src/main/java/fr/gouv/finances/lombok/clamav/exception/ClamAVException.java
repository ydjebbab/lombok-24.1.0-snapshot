/*
 * Copyright (c) 2014 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.clamav.exception;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Exception liée au service de recherche de virus clamAV.
 *
 * @author Christophe Breheret-Girardin
 */
public class ClamAVException extends RuntimeException
{
    /** Initialisation de la journalisation. */
    protected static final Logger LOGGER = LoggerFactory.getLogger(ClamAVException.class);

    /** Initialisation de l'UID. */
    private static final long serialVersionUID = 1L;

    /** Code de l'exception. */
    private String code;

    /** Tableau des arguments. */
    private List<String> argsList;

    /**
     * Constructeur.
     * 
     * @param code code de l'exception
     */
    public ClamAVException(final String code)
    {
        super();
        this.code = code;
        this.argsList = new ArrayList<>();
    }

    /**
     * Constructeur.
     * 
     * @param code code de l'exception
     * @param args tableau des arguments
     */
    public ClamAVException(final String code, final String[] args)
    {
        super();

        this.code = code;
        if (args != null)
        {
            this.argsList = new ArrayList<>(Arrays.asList(args));
        }
        else
        {
            this.argsList = new ArrayList<>();
        }
    }

    /**
     * Constructeur.
     * 
     * @param code code de l'exception
     * @param argsList liste des arguments
     */
    public ClamAVException(final String code, final List<String> argsList)
    {
        super();
        this.code = code;
        if (argsList != null)
        {
            this.argsList = argsList;
        }
        else
        {
            this.argsList = new ArrayList<>();
        }
    }

    /**
     * Constructeur.
     * 
     * @param code code de l'exception
     * @param exc exception
     */
    public ClamAVException(final String code, final Throwable exc)
    {
        super(exc);
        this.code = code;
        this.argsList = new ArrayList<>();
    }

    /**
     * Constructeur.
     *
     * @param code code de l'exception
     * @param args tableau des arguments
     * @param exc exception
     */
    public ClamAVException(final String code, final String[] args, final Throwable exc)
    {
        super(exc);
        this.code = code;
        if (args != null)
        {
            this.argsList = new ArrayList<>(Arrays.asList(args));
        }
        else
        {
            this.argsList = new ArrayList<>();
        }
    }

    /**
     * Constructeur.
     *
     * @param code code de l'exception
     * @param argsList liste des arguments
     * @param exc exception
     */
    public ClamAVException(final String code, final List<String> argsList, final Throwable exc)
    {
        super(exc);
        this.code = code;
        if (argsList != null)
        {
            this.argsList = argsList;
        }
        else
        {
            this.argsList = new ArrayList<>();
        }
    }

    /**
     * Accesseur de code.
     * 
     * @return Retourne le code
     */
    public final String getCode()
    {
        return this.code;
    }

    /**
     * Mutatteur de code.
     * 
     * @param code valeur de code
     */
    public final void setCode(
        final String code)
    {
        this.code = code;
    }

    /**
     * Accesseur de args.
     * 
     * @return Retourne args
     */
    public final String[] getArgs()
    {
        String[] args;
        args = this.argsList.toArray(new String[this.argsList.size()]);
        return args;
    }

    /**
     * Mutatteur de args.
     * 
     * @param args valeur de args
     */
    public final void setArgs(
        final String[] args)
    {
        this.argsList = new ArrayList<>(Arrays.asList(args));
    }

    /**
     * Accesseur de argsList.
     * 
     * @return Retourne argsList
     */
    public final List<String> getArgsList()
    {
        return this.argsList;
    }

    /**
     * Mutatteur de argsList.
     * 
     * @param argsList valeur de argsList
     */
    public final void setArgsList(
        final List<String> argsList)
    {
        this.argsList = argsList;
    }
}
