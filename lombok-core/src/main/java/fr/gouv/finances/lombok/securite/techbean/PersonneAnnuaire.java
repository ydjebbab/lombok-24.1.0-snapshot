/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 */
package fr.gouv.finances.lombok.securite.techbean;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import org.apache.commons.lang.StringUtils;

import fr.gouv.finances.lombok.securite.service.impl.HabilitationXMLParser;
import fr.gouv.finances.lombok.util.base.BaseTechBean;
import fr.gouv.finances.lombok.util.exception.RegleGestionException;
import fr.gouv.finances.lombok.util.format.FormaterDate;

/**
 * Classe portant les caractéristiques d'une personne dans les annuaires DGCP, DGI ou DGFIP et reliée à un
 * ensemble d'objets représentant les habilitations de l'utilisateur.<br/>
 * 12/09/11 : ajout de caractéristiques pour le passage à l'annuaire fusionné DGFIP.<br/>
 * 29/09/17 : ajout de caractéristiques pour la migration SIRHIUS en remplacement d'AGORA.<br/>
 * 
 * @author chouard-cp
 * @author amp
 * @author Christophe Breheret-Girardin
 *
 */
public class PersonneAnnuaire extends BaseTechBean
{

    /** Identifiant unique de la classe utilisé lors de la déserialisation. */
    private static final long serialVersionUID = 2L;

    /**
     * =================================================================
     * Paramètres relatifs au stockage des habilitations dans l'annuaire.
     * =================================================================
     */

    /** Constante "ATTRIBUT_HABILITATION". */
    public static final String ATTRIBUT_HABILITATION = "MEFIAPPLIHABILITDGCP";
    
    /** Constante "ATTRIBUT_HABILITATION". */
    public static final String ATTRIBUT_HABILITATION_EXDGCP = "MEFIAPPLIDGCP";

    /** Constante "APTERA_SEPARATEUR_MEFIAPPLIHABILITDGCP". */
    public static final char APTERA_SEPARATEUR_MEFIAPPLIHABILITDGCP = ';';

    /** Constante "APTERA_SEPARATEUR_MEFIAPPLIDGI". */
    public static final String APTERA_SEPARATEUR_MEFIAPPLIDGI = ";";

    /** Constante "ATTRIBUT_HABILITATION_PROFAPPLICATIF". */
    public static final String ATTRIBUT_HABILITATION_PROFAPPLICATIF = "profilApplicatif";

    /** Constante "ATTRIBUT_HABILITATION_PROFAPPLICATIFLONG". */
    public static final String ATTRIBUT_HABILITATION_PROFAPPLICATIFLONG = "profilApplicatifLong";

    /** Constante "APTERA_SEPARATEUR_MEFIAPPLIDGFIP". */
    public static final String APTERA_SEPARATEUR_MEFIAPPLIDGFIP = ";";

    /** Message utilisé lors de l'absence d'habilitation. */
    private static final String EXCEPTION_PAS_D_HABILITATION = "Aucune habilitation trouvee pour : ";

    /** L'agent était rattaché initialement à la DGCP. */
    private static final String ORIGINE_DGCP = "DGCP";

    /** L'agent était rattaché initialement à la DGI. */
    private static final String ORIGINE_DGI = "DGI";

    /** L'agent n'était pas initialement rattaché à la DGCP ou la DGI. */
    private static final String ORIGINE_AUTRE = "AUTRE";

    /** La personne est un usager. */
    private static final String ORIGINE_USAGERS = "USAGERS";

    /** L'agent a toujours fait parti de la DGFIP. */
    private static final String ORIGINE_DGFIP = "DGFIP";

    /** Date de début d'une habilitation */
    private static final String DATE_DEBUT = "01/01/2012";
    /** Date de Fin d'une habilitation */
    private static final String DATE_FIN = "01/01/2022";
    /** Administrateur par défaut */
    private static final String ADMIN = "Administrateur-0";

    /**
     * =====================================================
     * Attributs communs annuaire dgfip / annuaire ex-dgcp
     * =====================================================
     */

    /** Identifiant technique de l'agent (identifiant ministériel suffixé par "-0"). */
    private String uid;

    /** Civilité de la personne (M., Mme). */
    private String personalTitle;

    /** Prénom usuel de l'agent. */
    private String givenName;

    /** Nom usuel de l'agent. */
    private String sn;

    /** Nom à afficher. */
    private String cn;

    /** Libellé long du grade SIRH SIRHIUS. */
    private String title;

    /** Code générique du grade de l'agent. */
    private String codeGrade;

    /** Adresse de messagerie électronique calculée à partir du prénom et nom d'usage. */
    private String mail;

    /** Lien LDAP vers la structure d'affectation principale de l'agent. */
    private String ouComplet;

    /** Libellé long de la structure d'affectation principale de l'agent. */
    private String ouDescription;

    /** Habilitation au format court (code de l'application + "," + profil). */
    private String profilApplicatif;

    /** Département géographique. */
    private String departement;

    /** Sigle des structures. */
    private String ouSigle;

    /** Bureau distributeur. */
    private String physicalDeliveryOfficeName;

    /** Adresse postale. */
    private String postalAddress;

    /** Code postal. */
    private String postalCode;

    /** Identifiant utilisé pour l'authentification lors de l'accès aux applications
     * (partie gauche de l'adresse mail). */
    private String uidFonctionnel;

    /**
     * ===============================================================
     * Attributs présents uniquement dans l'annuaire ex-dgcp ou ex-DGI
     * ===============================================================
     */

    /** Affectation. */
    @Deprecated
    private String affectation;

    /** Annexe. */
    @Deprecated
    private String annexe;

    /** Attribut "Business category". */
    @Deprecated
    private String businessCategory;

    /** Code fonction. */
    @Deprecated
    private String codeFonction;

    /** Département profesionnel d'affectation. */
    @Deprecated
    private String department;

    /** Fonction. */
    @Deprecated
    private String fonction;

    /** Identifiant DGI. */
    @Deprecated
    private String identDGI;

    /** Attribut "mail delivery option". */
    @Deprecated
    private String mailDeliveryOption;

    /** Attribut "mefi appli dgcp". */
    @Deprecated
    private String mefiAppliDGCP;

    /** Attribut "mefi appli habilit dgcp". */
    @Deprecated
    private String mefiAppliHabilitDGCP;

    /** Attribut "mefi appli dgi". */
    @Deprecated
    private String mefiAppliDGI;

    /** Attribut "mefi point majf". */
    @Deprecated
    private String mefiPointMAJF;

    /** Region. */
    @Deprecated
    private String region;

    /** Attribut "room number". */
    @Deprecated
    private String roomNumber;

    /** Code SAGES. */
    @Deprecated
    private String sages;

    /** Numéro de téléphone. */
    @Deprecated
    private String telephoneNumber;

    /**
     * ===============================================================
     * Attributs ajoutés dans l'annuaire DGFIP
     * ===============================================================
     */

    /**
     * Identifiant de l'agent dans le SIRH AGORA
     */
    @Deprecated
    private String uidRH;

    /** Mot de passe compatible Windows (sensible à la casse, chiffré en MD5). */
    private String sambaNTPassword;

    /** Date de validité du mot de passe. */
    private String passwordExpirationTime;

    /** Code SIRH du grade AGORA de l’agent. */
    @Deprecated
    private String codeGradeRH;

    /** Indicateur booléen indiquant si l’agent est un employé supérieur ou non. */
    private String empsup;

    /** Code categorie de l'agent (A, B, ...). */
    private String categorie;

    /**
     * Indicateur du niveau de responsabilité de l’agent :<br/>
     * - 1 s’il est responsable de sa structure d’affectation principale ;<br/>
     * - 2 s’il est adjoint de sa structure d’affectation principale;<br/>
     * - 0 sinon.
     */
    private String niveauResponsabilite;

    /** Identifiant ARTEMIS de la structure d'affectation principale de l'agent. */
    private String ou;

    /** Codique de la structure d’affectation principale de l’agent. */
    private String affectationCODIQUE;

    /** Code annexe de la structure d’affectation principale de l’agent. */
    private String affectationANNEXE;

    /**
     * Code SAGES de la structure d’affectation principale de l’agent
     * (sur 3, 5, 7 ou 10 caractères selon l’affectation principale de l’agent).
     */
    private String affectationSAGES;

    /**
     * Habilitation au format long :<br/>
     * - code de l'application ; <br/>
     * - caractère séparateur "," ;<br/>
     * - une ligne XML contenant les caractéristiques de l'habilitation (profils associés + filtre associés) ;<br/>
     * - caractère séparateur ";".
     */
    private String profilApplicatifLong;

    /** Informations sur les certificats détenus par l’agent. */
    private String userCertificate;

    /** Phrase secrète associée au certificat, chiffrée en base 64. */
    private String questionreponse;

    /**
     * Mot de passe standard LDAP chiffré en SSHA.
     */
    private String userPassword;

    /**
     * ===============================================
     * Attributs spéficiques de l'annuaire des usagers
     * ===============================================
     */

    /** Numéro d'identification fiscal de l'usager. */
    private String spi;

    /** Type de certificat. */
    private String typeCertificat;

    /**
     * =================================================================
     * Nouveaux attributs de l'annuaire DGFIP suite à la bascule SIRHIUS
     * =================================================================
     */
    
    /** Identifiant de l’agent dans le SIRH. */
    private String uidSirhius;
    
    /** Code SIRH du grade SIRHIUS de l’agent. */
    private String codeGradeSirhius;
    
    /** Code du CSRH territorial. */
    private String codeCSRH;

    /**
     * ==================================
     * Liste d'information de la personne
     * ==================================
     */

    /** Liste des habilitations de la personne. */
    private Set<HabilitationAnnuaire> listeHabilitations;

    /** Liste des habilitations de la personne sur une application donnée. */
    private List<String> listeBruteMefiAppliXxx = new ArrayList<String>();

    /** Liste des habilitations de la personne dans l'annuaire ex-DGI. */
    @Deprecated
    private List<String> listeBruteMefiAppliDgi = new ArrayList<String>();

    /** Liste des habilitations de la personne dans l'annuaire ex-DGCP. */
    @Deprecated
    private List<String> listeBruteMefiAppliHabilitDgcp = new ArrayList<String>();

    /** 
     * Liste brute des affectations secondaires. 
     * - identifiant ARTEMIS de la structure ;<br/>
     * - code SAGES de la structure ;<br/>
     * - numéro CODIQUE de la structure complété de l’ANNEXE (séparé par un caractère ".") ;<br/>
     * - niveau de responsabilité ;
     * - Lien LDAP vers la structure d’affectation secondaire de l’agent.
     */
    private List<String> listeBruteAffectationsSecondaires = new ArrayList<String>();

    /** Liste brute des profils applicatifs. */
    private List<String> listeBruteProfilApplicatif = new ArrayList<String>();

    /** Liste brute des profils applicatifs long. */
    private List<String> listeBruteProfilApplicatifLong = new ArrayList<String>();

    /** Liste brute des identifiants d'organisme. */
    private List<String> listeBruteIdOrganisme = new ArrayList<String>();

    /**
     * Constructeur.
     */
    public PersonneAnnuaire()
    {
        super(); 
    }
    
    /**
     * retourne l'habilitation sur une application si,utilisateur d' origine dgi, cette habilitation se trouve dans
     * mefiapplidgi sinon utilisateur cp ou externe elle se trouve dans mefiapplihabilitdgcp. evolution septembre 2011 ,
     * si cas annuiare fusioné dgfip , on va regarder dans les nouveaux attributs (profilapplicatif, profilapplicatif
     * long)
     * 
     * @param application --
     * @return true, if calculer habilitation sur application
     */
    public boolean calculerHabilitationSurApplication(String application)
    {

        boolean estHabilitee = false;
        // Cas annuaire ex-dgi
        if (this.estUtilisateurOrigineDGI() == true)
        {
            log.trace("L'utilisateur est originaire de la DGI");
            estHabilitee = calculerHabilitationSurApplicationAvecMefiAppliDgi(application);
        }
        // Cas annuaire fusionné
        else if (this.estUtilisateurOrigineDGFIP() == true)
        {
            log.trace("L'utilisateur est originaire de la DGFIP");
            estHabilitee = this.calculerHabilitationSurApplicationAvecProfilApplicatif(application);

        }
        // Cas annuaire usagers
        else if (this.estUtilisateurOrigineUSAGERS() == true)
        {
            log.trace("L'utilisateur est un usager");
            estHabilitee = this.calculerHabilitationSurApplicationCasUsagers(application);
        }
        else
        // Cas annuaire ex-dgcp ou externes
        {
            log.trace("L'utilisateur est originaire de la DGCP ou externe");
            estHabilitee = calculerHabilitationSurApplicationAvecMefiAppliHabilitDgcp(application);
        }

        return estHabilitee;
    }

    /**
     * methode Calculer habilitation sur application avec mefi appli dgi : on recherche l' habilitation sur une
     * application dgi dans l'annuaire dgi.
     * 
     * @param application --
     * @return true, si c'est vrai
     */
    public boolean calculerHabilitationSurApplicationAvecMefiAppliDgi(String application)
    {
        log.trace("Calcul des habilitations a partir de l'attribut 'MefiAppliDgi' de l'annuaire");
        if (log.isDebugEnabled())
        {
            log.debug("Habilitations demandees sur l'application : " + application);
        }
        boolean estHabilitee = false;

        /* Ce code convient pour une habilitation au format Aptera MefiAppliDgi */
        List<String> listeBruteMefiAppliDgi = this.getListeBruteMefiAppliDgi();
        if ((listeBruteMefiAppliDgi != null) && (listeBruteMefiAppliDgi.size() > 0))
        {
            log.trace("L'attribut 'MefiAppliDgi' est valorise avec les habilitations");
            String valeurMefiAppliDGI =
                this.extraireAttributMefiAppliDgiContenantHabilitation(listeBruteMefiAppliDgi, application);
            this.setMefiAppliDGI(valeurMefiAppliDGI);
            if (valeurMefiAppliDGI != null)
            {
                estHabilitee = true;
                if (log.isDebugEnabled())
                {
                    log.trace("La personne est bien habilitee sur l'application demandee");
                }
                this.construitUneHabilitationAPartirDuContenuDeMEFIDgi(valeurMefiAppliDGI);
            }
        }
        return estHabilitee;
    }

    /**
     * Méthode permettant de savoir si la personne possède des habilitations, au format court ou long,
     * sur une application, dans le cas du nouvel annuaire fusionné
     * 
     * @param application application sélectionnée
     * @return vrai la personne possède des habilitations, faux sinon
     */
    public boolean calculerHabilitationSurApplicationAvecProfilApplicatif(String application)
    {
        // calcul à partir du champ profilApplicatifLong (plus complet que profilApplicatif)
        log.trace("Calcul des habilitations a partir de l'attribut 'profilApplicatifLong' de l'annuaire");
        if (log.isDebugEnabled())
        {
            log.debug("Habilitations demandees sur l'application : " + application);
        }
        boolean estHabilitee = false;

        List<String> listeBruteProfil = this.getListeBruteProfilApplicatifLong();
        if (listeBruteProfil != null && !listeBruteProfil.isEmpty())
        {
            log.trace("L'attribut 'profilApplicatifLong' est valorise avec les habilitations");

            String valeurMefiAppliDGFIP =
                this.extraireAttributProfilApplicatifLongContenantHabilitation(listeBruteProfil, application);
            this.setProfilApplicatifLong(valeurMefiAppliDGFIP);

            if (valeurMefiAppliDGFIP != null)
            {
                estHabilitee = true;
                log.trace("La personne est bien habilitee sur l'application demandee");
                this.construitUneHabilitationAPartirDuContenuProfilApplicatifLong(valeurMefiAppliDGFIP,
                    StringUtils.splitByWholeSeparator(application, APTERA_SEPARATEUR_MEFIAPPLIDGI)[0]);
            }
        }
       
        // calcul à partir du champ profilApplicatif        
        log.trace("Calcul des habilitations a partir de l'attribut 'profilApplicatif' de l'annuaire");
        if (log.isDebugEnabled())
        {
            log.debug("Habilitations demandees sur l'application : " + application);
        }

        listeBruteProfil = this.getListeBruteProfilApplicatif();
        if (listeBruteProfil != null && !listeBruteProfil.isEmpty())
        {
            log.trace("L'attribut 'profilApplication' est valorise avec les habilitations");

            String valeurMefiAppliDGFIP =
                this.extraireAttributProfilApplicatifContenantHabilitation(listeBruteProfil, application);
            this.setProfilApplicatif(valeurMefiAppliDGFIP);
            // construction d'une habilitation ssi il n'y en a pas déjà eu à partir de profilApplicatifLong
            if (!estHabilitee && valeurMefiAppliDGFIP != null)
            {
                estHabilitee = true;
                log.trace("La personne est bien habilitee sur l'application demandee");
                this.construitUneHabilitationAPartirDuContenuProfilApplicatif(valeurMefiAppliDGFIP);
            }
        }
        return estHabilitee;
    }

    /**
     *  Méthode permettant de savoir si la personne possède des habilitations, au format long,
     *  dans le cas du nouvel annuaire fusionné
     * 
     * @param application application sélectionnée
     * @return vrai la personne possède des habilitations, faux sinon
     */
    public boolean calculerHabilitationSurApplicationAvecProfilApplicatifLong(String application)
    {
        log.trace("Calcul des habilitations a partir de l'attribut 'profilApplicatifLong' de l'annuaire");
        if (log.isDebugEnabled())
        {
            log.debug("Habilitations demandees sur l'application : " + application);
        }
        boolean estHabilitee = false;

        List<String> listeBruteProfilApplicatifLong = this.getListeBruteProfilApplicatifLong();
        if (listeBruteProfilApplicatifLong != null && !listeBruteProfilApplicatifLong.isEmpty())
        {
            log.trace("L'attribut 'profilApplicatifLong' est valorise avec les habilitations");

            String valeurMefiAppliDGFIP =
                this.extraireAttributProfilApplicatifLongContenantHabilitation(listeBruteProfilApplicatifLong, application);
            this.setProfilApplicatifLong(valeurMefiAppliDGFIP);

            if (valeurMefiAppliDGFIP != null)
            {
                estHabilitee = true;
                log.trace("La personne est bien habilitee sur l'application demandee");
                this.construitUneHabilitationAPartirDuContenuProfilApplicatifLong(valeurMefiAppliDGFIP, 
                    StringUtils.splitByWholeSeparator(application, APTERA_SEPARATEUR_MEFIAPPLIDGI)[0]);
            }
        }
        return estHabilitee;
    }

    /**
     *  Méthode permettant de savoir si l'usager possède des habilitations
     * 
     * @param application application sélectionnée
     * @return vrai l'usager possède des habilitations, faux sinon
     */
    public boolean calculerHabilitationSurApplicationCasUsagers(String application)
    {
        log.trace("Calcul des habilitations a partir de l'attribut 'profilApplicatif' de l'annuaire");
        if (log.isDebugEnabled())
        {
            log.debug("Habilitations demandees sur l'application : " + application);
        }
        boolean estHabilitee = false;

        List<String> listeBruteProfilApplicatif = this.getListeBruteProfilApplicatif();
        if ((listeBruteProfilApplicatif != null) && !listeBruteProfilApplicatif.isEmpty())
        {
            log.trace("L'attribut 'profilApplicatif' est valorise avec les habilitations");

            String valeurMefiAppliDGFIP =
                this.extraireAttributProfilApplicatifContenantHabilitation(listeBruteProfilApplicatif, application);
            this.setProfilApplicatif(valeurMefiAppliDGFIP);
            if (valeurMefiAppliDGFIP != null)
            {
                estHabilitee = true;
                log.trace("La personne est bien habilitee sur l'application demandee");
                this.construitUneHabilitationAPartirDuContenuProfilApplicatifPourUsagers(valeurMefiAppliDGFIP);
            }
        }
        return estHabilitee;
    }

    /**
     * Méthode permettant de savoir si un agent de l'ex-DGCP possède des habilitations
     *(en décodant le flux xml de l'attribut "mefiapplihabilitdgcp").
     * 
     * @param applicationapplication sélectionnée
     * @return vrai l'agent possède des habilitations, faux sinon
     */
    protected boolean calculerHabilitationSurApplicationAvecMefiAppliHabilitDgcp(String application)
    {

        boolean estHabilitee = false;

        /* Ce code convient pour une habilitation au format Aptera MefiAppliHabilitDgcp */
        List<String> listeMefiAppliHabilitDgcp = this.getListeBruteMefiAppliHabilitDgcp();
        if ((listeMefiAppliHabilitDgcp != null) && (!listeMefiAppliHabilitDgcp.isEmpty()))
        {
            if (log.isDebugEnabled())
            {
                log.debug("il existe des attributs d'habilitation MefiAppliHabilitDGCP");
            }
            String valeurMefiAppliHabilitDGCP =
                this.extraireAttributMefiAppliHabilitDGCPContenantHabilitation(listeMefiAppliHabilitDgcp, application);
            this.setMefiAppliHabilitDGCP(valeurMefiAppliHabilitDGCP);
            if (valeurMefiAppliHabilitDGCP != null)
            {
                estHabilitee = true;
                if (log.isDebugEnabled())
                {
                    log.debug("la personne est habilitee avec un attribut MefiAppliHabilitDGCP sur l'application");
                }
                this.construitListeHabilitationsAPartirDuContenuDeMEFIHabilitDGCP(mefiAppliHabilitDGCP,
                    StringUtils.splitByWholeSeparator(application, Character.toString(APTERA_SEPARATEUR_MEFIAPPLIHABILITDGCP))[0]);
            }
        }
        return estHabilitee;
    }

    /**
     * Méthode permettant de savoir si la personne est est fournie par l'annuaire DGFIP.
     *
     * @return true si vrai, faux sinon
     */
    public boolean estUtilisateurOrigineDGFIP()
    {
        boolean retourUtilisateurOrigineDGFIP = false;

        if (this.rechercherAppartenancePersonne().compareTo(ORIGINE_DGFIP) == 0)
        {
            retourUtilisateurOrigineDGFIP = true;
        }

        return retourUtilisateurOrigineDGFIP;
    }

    /**
     * Méthode permettant de retrouver le type d'annuaire qui a fourni la personne
     * 
     * @return la direction l'origine d'une personne ("DGI", "DGCP", "DGFIP", "USAGERS", "AUTRE")
     */
    public String rechercherAppartenancePersonne()
    {
        if (uid.toUpperCase(Locale.FRENCH).trim().endsWith("-DGI"))
        {
            return ORIGINE_DGI;
        }
        else if (uid.toUpperCase(Locale.FRENCH).trim().endsWith("-CP")
            || uid.toUpperCase(Locale.FRENCH).trim().endsWith("-XT"))
        {
            return ORIGINE_DGCP;
        }

        else if (uid.toUpperCase(Locale.FRENCH).trim().endsWith("-0"))
        {
            return ORIGINE_DGFIP;
        }

        else if (spi != null && StringUtils.isNotEmpty(spi))
        {
            return ORIGINE_USAGERS;
        }
        else
        {
            return ORIGINE_AUTRE;
        }
    }

    /**
     * Méthode permettant de récupérer la dernière date d'expiration des habilitations.
     * 
     * @return la date d'expiration la dernière date d'expiration des habilitations
     */
    public Date rechercherDateExpirationDerniereHabilitation()
    {
        Date datelue = new Date();
        Date datemax = new Date();

        datemax.setTime(0);
        
        if (listeHabilitations != null)
        {
            Iterator<HabilitationAnnuaire> unelisteHabilitations = listeHabilitations.iterator();
    
            while (unelisteHabilitations.hasNext())
            {
                datelue = ((HabilitationAnnuaire) (unelisteHabilitations.next())).getDateFin();
                if (datelue != null && datelue.compareTo(datemax) > 0)
                {
                    datemax = datelue;
                }
            }
        }
        return datemax;
    }

    /**
     * Méthode permettant de récupérer la première habilitation d'une personne.
     * 
     * @return the habilitation annuaire
     */
    public HabilitationAnnuaire rechercherPremiereHabilitation()
    {
        HabilitationAnnuaire habilitation = null;
        if (this.getListeHabilitations() != null)
        {
            Iterator<HabilitationAnnuaire> ithabilit = this.getListeHabilitations().iterator();
            if (ithabilit.hasNext())
            {
                habilitation = (HabilitationAnnuaire) ithabilit.next();
            }
            else
            {
                throw new RegleGestionException(EXCEPTION_PAS_D_HABILITATION + this.getUid());
            }
        }
        else
        {
            throw new RegleGestionException(EXCEPTION_PAS_D_HABILITATION + this.getUid());
        }
        return habilitation;
    }

    /**
     * Méthode permettant de récupérer la liste des profils contenus dans les habilitations.
     * 
     * @return la liste des profils contenus dans les habilitations.
     */
    public String[] rechercherProfils()
    {
        HabilitationAnnuaire uneHabilitation;
        Iterator<HabilitationAnnuaire> iter = this.listeHabilitations.iterator();
        int index = 0;
        String[] desProfils = new String[listeHabilitations.size()];

        while (iter.hasNext())
        {
            uneHabilitation = (HabilitationAnnuaire) iter.next();
            desProfils[index] = uneHabilitation.getNomProfil().toUpperCase(Locale.FRANCE);
            index++;
        }
        return desProfils;
    }

    /**
     * Méthode permettant de savoir si la personne est fournie par l'annuaire DGI.
     * 
     * @return @return true si vrai, faux sinon
     */
    public boolean estUtilisateurOrigineDGI()
    {
        boolean retourUtilisateurOrigineDGI = false;

        if (this.rechercherAppartenancePersonne().compareTo(ORIGINE_DGI) == 0)
        {
            retourUtilisateurOrigineDGI = true;
        }

        return retourUtilisateurOrigineDGI;
    }

    /**
     * Méthode permettant de savoir si la personne est fournie par l'annuaire USAGERS.
     * 
     * @return true, si c'est vrai
     */
    public boolean estUtilisateurOrigineUSAGERS()
    {
        boolean retourUtilisateurOrigineUSAGERS = false;

        if (this.rechercherAppartenancePersonne().compareTo(ORIGINE_USAGERS) == 0)
        {
            retourUtilisateurOrigineUSAGERS = true;
        }

        return retourUtilisateurOrigineUSAGERS;
    }

    /**
     * Méthode permettat d'obtenir le nombre d'habilitation.
     * 
     * @return le nombre d'habilitation
     */
    public int getNbHabilitations()
    {
        int result;
        if (listeHabilitations != null)
        {
            result = listeHabilitations.size();
        }
        else
        {
            result = 0;
        }
        return result;
    }

    /**
     * Méthode permettant d'établir la liste des habilitations d'un agent ex-DGCP
     * 
     * @param mefiAppliHabilitDGCP les habilitations de la personne au format DGCP
     */
    protected void construitListeHabilitationsAPartirDuContenuDeMEFIHabilitDGCP(String mefiAppliHabilitDGCP, String libelleCourtAppli)
    {
        Set<HabilitationAnnuaire> desHabilitations = null;
        try
        {
            desHabilitations = HabilitationXMLParser.habilitationDecodeXML(mefiAppliHabilitDGCP, libelleCourtAppli);
        }
        catch (RuntimeException e)
        {
            log.error("Impossible de construire l'habilitation à partir de l'attribut mefiAppliHabilit DGCP : "
                + mefiAppliHabilitDGCP);
            throw new HabilitationRegleGestionException(e.getMessage(), e);
        }
        this.setListeHabilitations(desHabilitations);
    }

    /**
     * Méthode permettant d'établir la liste des habilitations d'un agent ex-DGI
     * 
     * @param mefiAppliDgi les habilitations de la personne au format DGI
     */
    protected void construitUneHabilitationAPartirDuContenuDeMEFIDgi(String mefiAppliDgi)
    {
        HabilitationAnnuaire uneHabilitation = new HabilitationAnnuaire();
        Set<HabilitationAnnuaire> desHabilitations = new HashSet<>();
        String[] tableauCodeAppliProfil =
            StringUtils.splitByWholeSeparator(mefiAppliDgi, APTERA_SEPARATEUR_MEFIAPPLIDGI);
        String appli = tableauCodeAppliProfil[0];
        String profil = tableauCodeAppliProfil[1];

        uneHabilitation.setLibelleCourtAppli(appli);
        uneHabilitation.setNomProfil(profil);
        DateFormat dateFormat = FormaterDate.getFormatDate("dd/MM/yyyy");
        Date dateDebut = null;
        Date dateFin = null;

        try
        {
            dateDebut = dateFormat.parse("01/01/2009");
            dateFin = dateFormat.parse("01/01/2029");
        }
        catch (ParseException e)
        {
            log.debug("Problème parse date ", e);
        }
        uneHabilitation.setDateCreation(dateDebut);
        uneHabilitation.setDateDebut(dateDebut);
        uneHabilitation.setDateFin(dateFin);
        uneHabilitation.setNomAdministrateur("Administrateur-dgi");
        desHabilitations.add(uneHabilitation);

        this.setListeHabilitations(desHabilitations);
    }

    /**
     * Méthode permettant d'établir la liste des habilitations à partir d'un profil applicatif court
     * 
     * @param profilapplicatif profil applicatif court d'une personne
     */
    protected void construitUneHabilitationAPartirDuContenuProfilApplicatif(String profilapplicatif)
    {
        HabilitationAnnuaire uneHabilitation = new HabilitationAnnuaire();
        Set<HabilitationAnnuaire> desHabilitations = new HashSet<>();
        String[] tableauCodeAppliProfil =
            StringUtils.splitByWholeSeparator(profilapplicatif, APTERA_SEPARATEUR_MEFIAPPLIDGI);
        String appli = tableauCodeAppliProfil[0];
        String profil = tableauCodeAppliProfil[1];

        uneHabilitation.setLibelleCourtAppli(appli);
        uneHabilitation.setNomProfil(profil);
        if (log.isDebugEnabled())
        {
            log.debug("Nom du profil : " + profil);
        }

        DateFormat dateFormat = FormaterDate.getFormatDate("dd/MM/yyyy");
        Date dateDebut = null;
        Date dateFin = null;
        
        try
        {
            dateDebut = dateFormat.parse(DATE_DEBUT);
            dateFin = dateFormat.parse(DATE_FIN);
        }
        catch (ParseException e)
        {
            log.debug("Probleme de conversion de date ", e);
        }
        uneHabilitation.setDateCreation(dateDebut);
        log.debug("Date de creation forcee a : " + DATE_DEBUT);
        uneHabilitation.setDateDebut(dateDebut);
        log.debug("Date de debut forcee a : " + DATE_DEBUT);
        uneHabilitation.setDateFin(dateFin);
        log.debug("Date de fin forcee a : " + DATE_FIN);
        uneHabilitation.setNomAdministrateur(ADMIN);
        log.debug("Nom administrateur forcee a : " + ADMIN);
        desHabilitations.add(uneHabilitation);
        if (this.getListeHabilitations() != null)
        {
            this.getListeHabilitations().addAll(desHabilitations);
        }
        else
        {
            this.setListeHabilitations(desHabilitations);
        }
    }

    /**
     * Méthode permettant d'établir la liste des habilitations à partir d'un profil applicatif long
     * 
     * @param profilapplicatiflong profil applicatif long d'une personne
     */
    protected void construitUneHabilitationAPartirDuContenuProfilApplicatifLong(String profilapplicatiflong, String libelleCourtAppli)
    {
        Set<HabilitationAnnuaire> desHabilitations = null;
        try
        {
            desHabilitations = HabilitationXMLParser.habilitationDecodeXML(profilapplicatiflong, libelleCourtAppli);
        }
        catch (RuntimeException e)
        {
            log.error("Impossible de construire l'habilitation à partir de l'attribut profilapplicatiflong : "
                + profilapplicatiflong);
            throw new HabilitationRegleGestionException(e.getMessage(), e);
        }
        this.setListeHabilitations(desHabilitations);
    }

    /**
     * Méthode permettant d'établir la liste des habilitations d'un usager (par défaut 0 qui sera à mettre dans aplicationContext-security).
     * 
     * @param profilapplicatif les habilitations d'un usager
     */
    protected void construitUneHabilitationAPartirDuContenuProfilApplicatifPourUsagers(String profilapplicatif)
    {
        HabilitationAnnuaire uneHabilitation = new HabilitationAnnuaire();
        Set<HabilitationAnnuaire> desHabilitations = new HashSet<>();

        String appli = profilapplicatif;
        // valeur du profil par défaut pour un usager
        final String profil = "0";

        uneHabilitation.setLibelleCourtAppli(appli);
        uneHabilitation.setNomProfil(profil);
        DateFormat dateFormat = FormaterDate.getFormatDate("dd/MM/yyyy");
        Date dateDebut = null;
        Date dateFin = null;

        try
        {
            dateDebut = dateFormat.parse(DATE_DEBUT);
            dateFin = dateFormat.parse(DATE_FIN);
        }
        catch (ParseException e)
        {
            log.debug("Problème parse date ", e);
        }
        uneHabilitation.setDateCreation(dateDebut);
        log.debug("Date de creation forcee a : " + DATE_DEBUT);
        uneHabilitation.setDateDebut(dateDebut);
        log.debug("Date de debut forcee a : " + DATE_DEBUT);
        uneHabilitation.setDateFin(dateFin);
        log.debug("Date de fin forcee a : " + DATE_FIN);
        uneHabilitation.setNomAdministrateur(ADMIN);
        log.debug("Nom administrateur forcee a : " + ADMIN);
        desHabilitations.add(uneHabilitation);

        this.setListeHabilitations(desHabilitations);
    }

    /**
     * Méthode permettant de récupérer la donnée "MefiAppli" de l'annuaire ex-DGI contenant l'application
     * sur laquelle on recherche l'habilitation.
     * 
     * @param listeBruteMefiAppliDgi liste de données "MefiAppli"
     * @param application application sélectionnée
     * @return la donnée "MefiAppli" correspondant à la demande
     */
    protected String extraireAttributMefiAppliDgiContenantHabilitation(
        List<String> listeBruteMefiAppliDgi, String application)
    {
        String valeur = null;
        String valeurMefiAppliDgi = null;

        if (log.isDebugEnabled())
        {
            log.debug("extraction de l'attribut mefiAppliDGI contenant l'habilitation ");
        }

        Iterator<String> iter = listeBruteMefiAppliDgi.iterator();
        while (valeurMefiAppliDgi == null && iter.hasNext())
        {
            valeur = iter.next();
            if (log.isDebugEnabled())
            {
                log.debug("lecture de la valeur " + valeur);
            }
            valeur = valeur.trim();
            // on remplace startsWith par substringBefore pour gérer cas ou trigramme appli = sirius par ex et autre
            // habilitation sur siriusdev
            if (StringUtils.substringBefore(valeur.toUpperCase(Locale.FRANCE), APTERA_SEPARATEUR_MEFIAPPLIDGI).compareTo(
                application.toUpperCase(Locale.FRANCE)) == 0)
            {
                valeurMefiAppliDgi = valeur;

            }
        }

        {
            if (valeurMefiAppliDgi == null)
            {
                if (log.isDebugEnabled())
                {
                    log.debug("code application : " + application.toUpperCase(Locale.FRANCE)
                        + " non trouve dans valeur : " + valeur);
                }
            }
        }

        return valeurMefiAppliDgi;
    }

    /**
     * Méthode permettant de récupérer la donnée "MefiAppliHabilit" de l'annuaire ex-DGCP,
     * contenant l'application sur laquelle on recherche l'habilitation.
     * 
     * @param listeMefiAppliHabilitDGCP liste de données "MefiAppliHabilit"
     * @param application application sélectionnée
     * @return la donnée "MefiAppliHabilit" correspondant à la demande
     */
    protected String extraireAttributMefiAppliHabilitDGCPContenantHabilitation(
        List<String> listeMefiAppliHabilitDGCP, String application)
    {
        String valeur = null;
        String valeurMefiAppliHabilitDGCP = null;
        int positionSeparateur = -1;

        if (log.isDebugEnabled())
        {
            log.debug("extraction de l'attribut mefiAppliHabilitDGCP contenant l'habilitation ");
        }

        Iterator<String> iter = listeMefiAppliHabilitDGCP.iterator();
        while (valeurMefiAppliHabilitDGCP == null && iter.hasNext())
        {
            valeur = iter.next();
            if (log.isDebugEnabled())
            {
                log.debug("lecture de la valeur " + valeur);
            }

            valeur = valeur.trim();
            if (valeur.toUpperCase(Locale.FRANCE).startsWith(application.toUpperCase(Locale.FRANCE)))
            {
                positionSeparateur = valeur.indexOf(APTERA_SEPARATEUR_MEFIAPPLIHABILITDGCP);
                if (positionSeparateur != -1)
                {
                    /*
                     * on enleve le code appli devant l'habilitation XML
                     */
                    valeur = valeur.substring(positionSeparateur + 1);
                    positionSeparateur = valeur.indexOf(APTERA_SEPARATEUR_MEFIAPPLIHABILITDGCP);

                    /*
                     * on enleve le separateur apres l'habilitation XML
                     */
                    valeur = valeur.substring(0, positionSeparateur);
                    valeurMefiAppliHabilitDGCP = valeur;

                    if (log.isDebugEnabled())
                    {
                        log.debug("code application trouve : " + application.toUpperCase(Locale.FRANCE)
                            + " valeur habilitation extraite " + valeurMefiAppliHabilitDGCP);
                    }

                }
                else
                {
                    if (log.isDebugEnabled())
                    {
                        log.debug("separateur : " + APTERA_SEPARATEUR_MEFIAPPLIHABILITDGCP
                            + "non trouve dans la valeur " + valeur);
                    }
                }
            }
            else
            {
                if (log.isDebugEnabled())
                {
                    log.debug("code application : " + application.toUpperCase(Locale.FRANCE)
                        + " non trouve dans valeur : " + valeur);
                }
            }
        }
        return valeurMefiAppliHabilitDGCP;
    }

    /**
     * Méthode permettant de récupérer le profil applicatif contenant l'application sur laquelle on recherche l'habilitation.
     * 
     * @param listeBruteProfilApplicatif liste des profils applicatifs
     * @param application application sélectionnée
     * @return le profil applicatif correspondant à la demande
     */
    protected String extraireAttributProfilApplicatifContenantHabilitation(
        List<String> listeBruteProfilApplicatif, String application)
    {
        String valeur = null;
        String valeurProfilApplicatif = null;

        if (log.isTraceEnabled())
        {
            log.trace("La valeur des habilitations doit etre sous la forme : nom application + '"
                + APTERA_SEPARATEUR_MEFIAPPLIDGFIP + "' + nom profil");
        }

        Iterator<String> iter = listeBruteProfilApplicatif.iterator();
        while (valeurProfilApplicatif == null && iter.hasNext())
        {
            valeur = iter.next();
            if (log.isDebugEnabled())
            {
                log.debug("Valeur des habilitations a analyser : " + valeur);
            }
            valeur = valeur.trim();
            // on remplace startsWith par substringBefore pour gérer cas ou trigramme appli = sirius par ex et autre
            // habilitation sur siriusdev
            if (StringUtils.substringBefore(valeur.toUpperCase(Locale.FRANCE), APTERA_SEPARATEUR_MEFIAPPLIDGFIP).compareTo(
                application.toUpperCase(Locale.FRANCE)) == 0)
            {
                valeurProfilApplicatif = valeur;
                if (log.isDebugEnabled())
                {
                    log.debug("Code application trouve : " + application.toUpperCase(Locale.FRANCE));
                    log.debug("Flux XML des habilitations : " + valeurProfilApplicatif);
                }
            }
        }

        {
            if (valeurProfilApplicatif == null)
            {
                if (log.isDebugEnabled())
                {
                    log.debug("Code application '" + application.toUpperCase(Locale.FRANCE) + "' non trouve");
                }
            }
        }

        return valeurProfilApplicatif;
    }

    /**
     * Méthode permettant de récupérer le profil applicatif long contenant l'application sur laquelle on recherche l'habilitation.
     * 
     * @param listeBruteProfilApplicatif liste des profils applicatifs longs
     * @param application application sélectionnée
     * @return le profil applicatif long correspondant à la demande
     */
    protected String extraireAttributProfilApplicatifLongContenantHabilitation(
        List<String> listeProfilApplicatifLong, String application)
    {
        String valeur = null;
        String valeurProfilApplicatifLong = null;
        int positionSeparateur = -1;

        if (log.isTraceEnabled())
        {
            log.trace("La valeur des habilitations doit etre sous la forme : nom application + '"
                + APTERA_SEPARATEUR_MEFIAPPLIHABILITDGCP + "' + flux XML des habilitations");
        }

        Iterator<String> iter = listeProfilApplicatifLong.iterator();
        while (valeurProfilApplicatifLong == null && iter.hasNext())
        {
            valeur = iter.next();
            if (log.isDebugEnabled())
            {
                log.debug("Valeur des habilitations a analyser : " + valeur);
            }

            valeur = valeur.trim();
            // on remplace startsWith par substringBefore pour gérer cas ou trigramme appli = sirius par ex et autre
            // habilitation sur siriusdev
            if (StringUtils.substringBefore(valeur.toUpperCase(Locale.FRANCE), Character.toString(APTERA_SEPARATEUR_MEFIAPPLIHABILITDGCP)).compareTo(
                application.toUpperCase(Locale.FRANCE)) == 0) 
            {
                positionSeparateur = valeur.indexOf(APTERA_SEPARATEUR_MEFIAPPLIHABILITDGCP);
                if (positionSeparateur != -1)
                {
                    /*
                     * on enleve le code appli devant l'habilitation XML
                     */
                    valeur = valeur.substring(positionSeparateur + 1);
                    positionSeparateur = valeur.indexOf(APTERA_SEPARATEUR_MEFIAPPLIHABILITDGCP);

                    /*
                     * on enleve le separateur apres l'habilitation XML
                     */
                    valeur = valeur.substring(0, positionSeparateur);
                    valeurProfilApplicatifLong = valeur;

                    if (log.isDebugEnabled())
                    {
                        log.debug("Code application trouve : " + application.toUpperCase(Locale.FRANCE));
                        log.debug("Flux XML des habilitations : " + valeurProfilApplicatifLong);
                    }
                }
                else
                {
                    if (log.isDebugEnabled())
                    {
                        log.debug("Separateur '" + APTERA_SEPARATEUR_MEFIAPPLIHABILITDGCP + "' non trouve");
                    }
                }
            }
            else
            {
                if (log.isDebugEnabled())
                {
                    log.debug("Code application '" + application.toUpperCase(Locale.FRANCE) + "' non trouve");
                }
            }
        }
        return valeurProfilApplicatifLong;
    }

    /**
     * Accesseur de l'affectation dans l'annuaire ex-DGCP ou ex-DGI.
     * 
     * @return affectation dans l'annuaire ex-DGCP ou ex-DGI
     */
    public String getAffectation()
    {
        return affectation;
    }

    /**
     * Accesseur du code annexe de la structure d’affectation principale de l’agent.
     * 
     * @return le code annexe de la structure d’affectation principale de l’agent
     */
    public String getAffectationANNEXE()
    {
        return affectationANNEXE;
    }

    /**
     * Accesseur du codique de la structure d’affectation principale de l’agent.
     *
     * @return le codique de la structure d’affectation principale de l’agent
     */
    public String getAffectationCODIQUE()
    {
        return affectationCODIQUE;
    }

    /**
     * Accesseur du code SAGES de la structure d’affectation principale de l’agent
     * (sur 3, 5, 7 ou 10 caractères selon l’affectation principale de l’agent).
     *
     * @return le code SAGES de la structure d’affectation principale de l’agent
     */
    public String getAffectationSAGES()
    {
        return affectationSAGES;
    }

    /**
     * Accesseur de l'attribut "annexe" dans l'annuaire ex-DGCP ou ex-DGI.
     * 
     * @return attribut "annexe" dans l'annuaire ex-DGCP ou ex-DGI
     */
    public String getAnnexe()
    {
        return annexe;
    }

    /**
     * Accesseur de l'attribut "BusinessCategory" dans l'annuaire ex-DGCP ou ex-DGI.
     * 
     * @return attribut "BusinessCategory" dans l'annuaire ex-DGCP ou ex-DGI
     */
    public String getBusinessCategory()
    {
        return businessCategory;
    }

    /**
     * Accesseur du code catégorie de l'agent (A, B, ...).
     * 
     * @return le code catégorie de l'agent
     */
    public String getCategorie()
    {
        return categorie;
    }

    /**
     * Accesseur du nom de la personne à afficher.
     * 
     * @return nom de la personne à afficher
     */
    public String getCn()
    {
        return cn;
    }

    /**
     * Accesseur de l'attribut "CodeFonction" dans l'annuaire ex-DGCP ou ex-DGI.
     * 
     * @return l'attribut "CodeFonction" dans l'annuaire ex-DGCP ou ex-DGI
     */
    public String getCodeFonction()
    {
        return codeFonction;
    }

    /**
     * Accesseur du code générique du grade de l'agent.
     * 
     * @return le code générique du grade de l'agent
     */
    public String getCodeGrade()
    {
        return codeGrade;
    }

    /**
     * @deprecated
     * Accesseur du code SIRH du grade AGORA de l’agent.
     * 
     * @return le code SIRH du grade AGORA de l’agent
     */
    public String getCodeGradeRH()
    {
        return codeGradeRH;
    }

    /**
     * Accesseur de la dernière date d'expiration des habilitations.
     * 
     * @return la dernière date d'expiration des habilitations
     */
    public Date getDateExpirationDerniereHabilitation()
    {
        return rechercherDateExpirationDerniereHabilitation();
    }

    /**
     * Accesseur du département géographique de la personne.
     * 
     * @return departement
     */
    public String getDepartement()
    {
        return departement;
    }

    /**
     * Accesseur du département professionnel d'affectation de l'agent dans l'annuaire ex-DGCP ou ex-DGI.
     * 
     * @return bureau d'affectation de l'agent dans l'annuaire ex-DGCP ou ex-DGI
     */
    public String getDepartment()
    {
        return department;
    }

    /**
     * Accesseur permettant de savoir si l’agent est un employé supérieur ou non.
     * 
     * @return l'information si l’agent est un employé supérieur ou non
     */
    public String getEmpsup()
    {
        return empsup;
    }

    /**
     * Accesseur de l'attribut "fonction" dans l'annuaire ex-DGCP ou ex-DGI.
     * 
     * @return l'attribut "fonction" dans l'annuaire ex-DGCP ou ex-DGI
     */
    public String getFonction()
    {
        return fonction;
    }

    /**
     * Accesseur du prénom usuel de la personne.
     * 
     * @return le prénom usuel de la personne
     */
    public String getGivenName()
    {
        return givenName;
    }

    /**
     * Accesseur de l'identifiant dans l'annuaire ex-DGI.
     * 
     * @return l'identifiant dans l'annuaire ex-DGI
     */
    public String getIdentDGI()
    {
        return identDGI;
    }

    /**
     * Accesseurs de la liste brute des affectations secondaires de l'agent.
     * - identifiant ARTEMIS de la structure ;<br/>
     * - code SAGES de la structure ;<br/>
     * - numéro CODIQUE de la structure complété de l’ANNEXE (séparé par un caractère ".") ;<br/>
     * - niveau de responsabilité ;
     * - Lien LDAP vers la structure d’affectation secondaire de l’agent..
     * 
     * @return la liste brute des affectations secondaires de l'agent
     */
    public List<String> getListeBruteAffectationsSecondaires()
    {
        return listeBruteAffectationsSecondaires;
    }

    /**
     * Accesseurs des habilitations de la personne dans l'annuaire ex-DGI.
     * 
     * @return les habilitations de la personne dans l'annuaire ex-DGI
     */
    public List<String> getListeBruteMefiAppliDgi()
    {
        return listeBruteMefiAppliDgi;
    }

    /**
     * Accesseur des habilitations de la personne dans l'annuaire ex-DGCP.
     * 
     * @return les habilitations de la personne dans l'annuaire ex-DGCP
     */
    public List<String> getListeBruteMefiAppliHabilitDgcp()
    {
        return listeBruteMefiAppliHabilitDgcp;
    }

    /**
     * Accesseur des habilitations de la personne sur une application donnée
     * 
     * @return les habilitations de la personne sur une application donnée
     */
    public List<String> getListeBruteMefiAppliXxx()
    {
        return listeBruteMefiAppliXxx;
    }

    /**
     * Accesseur de la liste brute des profils applicatifs.
     * 
     * @return la liste brute des profils applicatifs
     */
    public List<String> getListeBruteProfilApplicatif()
    {
        return listeBruteProfilApplicatif;
    }

    /**
     * Accesseur des profils applicatifs longs
     * 
     * @return les profils applicatifs longs
     */
    public List<String> getListeBruteProfilApplicatifLong()
    {
        return listeBruteProfilApplicatifLong;
    }

    /**
     * Accesseur des habilitations de la personne.
     * 
     * @return les habilitations de la personne
     */
    public Set<HabilitationAnnuaire> getListeHabilitations()
    {
        return listeHabilitations;
    }

    /**
     * Accesseur de l'adresse de messagerie électronique.
     * 
     * @return l'adresse de messagerie électronique
     */
    public String getMail()
    {
        return mail;
    }

    /**
     * Accesseur de l'attribut "MailDeliveryOption" dans l'annuaire ex-DGCP ou ex-DGI.
     * 
     * @return l'attribut "MailDeliveryOption" dans l'annuaire ex-DGCP ou ex-DGI
     */
    public String getMailDeliveryOption()
    {
        return mailDeliveryOption;
    }

    /**
     * Accesseur des données "MefiAppli" de l'ex-DGCP.
     * 
     * @return les données "MefiAppli" de l'ex-DGCP
     */
    public String getMefiAppliDGCP()
    {
        return mefiAppliDGCP;
    }

    /**
     * Accesseur des données "MefiAppli" de l'ex-DGI.
     * 
     * @return les données "MefiAppli" de l'ex-DGI
     */
    public String getMefiAppliDGI()
    {
        return mefiAppliDGI;
    }

    /**
     * Accesseur des données "MefiAppliHabilit" de l'ex-DGCP.
     * 
     * @return les données "MefiAppliHabilit" de l'ex-DGCP
     */
    public String getMefiAppliHabilitDGCP()
    {
        return mefiAppliHabilitDGCP;
    }

    /**
     * Accesseur de l'attribut "MefiPointMAJF" dans l'annuaire ex-DGCP ou ex-DGI.
     * 
     * @return l'attribut "MefiPointMAJF" dans l'annuaire ex-DGCP ou ex-DGI
     */
    public String getMefiPointMAJF()
    {
        return mefiPointMAJF;
    }

    /**
     * Accesseur du niveau de responsabilité de l'agent :<br/>
     * - 1 s’il est responsable de sa structure d’affectation principale ;<br/>
     * - 2 s’il est adjoint de sa structure d’affectation principale;<br/>
     * - 0 sinon.
     * 
     * @return le niveau de responsabilité de l'agent
     */
    public String getNiveauResponsabilite()
    {
        return niveauResponsabilite;
    }

    /**
     * Accesseur de l'origine de l'agent si non ex-DGCP ou ex-DGI.
     * 
     * @return l'origine de l'agent si non ex-DGCP ou ex-DGI
     */
    public String getOrigineAUTRE()
    {
        return ORIGINE_AUTRE;
    }

    /**
     * Accesseur de l'origine de l'agent si ex-DGCP.
     *
     * @return l'origine de l'agent si ex-DGCP
     */
    public String getOrigineDGCP()
    {
        return ORIGINE_DGCP;
    }

    /**
     * Accesseur de l'origine de l'agent si DGFiP.
     *
     * @return l'origine de l'agent si DGFiP
     */
    public String getOrigineDGFIP()
    {
        return ORIGINE_DGFIP;
    }

    /**
     * Accesseur de l'origine de l'agent si ex-DGI.
     *
     * @return l'origine de l'agent si ex-DGI
     */
    public String getOrigineDGI()
    {
        return ORIGINE_DGI;
    }

    /**
     * Accesseur de l'identifiant ARTEMIS de la structure d'affectation principale de l'agent.
     * 
     * @return l'identifiant ARTEMIS de la structure d'affectation principale de l'agent
     */
    public String getOu()
    {
        return ou;
    }

    /**
     * Accesseur du lien LDAP vers la structure d'affectation principale de l'agent.
     * 
     * @return le lien LDAP vers la structure d'affectation principale de l'agent
     */
    public String getOuComplet()
    {
        return ouComplet;
    }

    /**
     * Accesseur du libellé long de la structure d'affectation principale de l'agent.
     * 
     * @return le libellé long de la structure d'affectation principale de l'agent
     */
    public String getOuDescription()
    {
        return ouDescription;
    }

    /**
     * Accesseur du sigle des structures.
     * 
     * @return le sigle des structures
     */
    public String getOuSigle()
    {
        return ouSigle;
    }

    /**
     * Accesseur de la date validité du mot de passe.
     * 
     * @return la date validité du mot de passe
     */
    public String getPasswordExpirationTime()
    {
        return passwordExpirationTime;
    }

    /**
     * Accesseur de la civilité de la personne.
     * 
     * @return la civilité de la personne (M. ou Mme)
     */
    public String getPersonalTitle()
    {
        return personalTitle;
    }

    /**
     * Accesseur du bureau distributeur.
     * 
     * @return le bureau distributeur
     */
    public String getPhysicalDeliveryOfficeName()
    {
        return physicalDeliveryOfficeName;
    }

    /**
     * Accesseur de l'adresse postale.
     * 
     * @return l'adresse postale
     */
    public String getPostalAddress()
    {
        return postalAddress;
    }

    /**
     * Accesseur du code postal.
     * 
     * @return le code postal
     */
    public String getPostalCode()
    {
        return postalCode;
    }

    /**
     * Accesseur des habilitations au format court (code de l'application + "," + profil).
     * 
     * @return les habilitations au format court
     */
    public String getProfilApplicatif()
    {
        return profilApplicatif;
    }

    /**
     * Accesseur des habilitations au format long :<br/>
     * - code de l'application ; <br/>
     * - caractère séparateur "," ;<br/>
     * - une ligne XML contenant les caractéristiques de l'habilitation (profils associés + filtre associés) ;<br/>
     * - caractère séparateur ";".
     * 
     * @return les habilitations au format long
     */
    public String getProfilApplicatifLong()
    {
        return profilApplicatifLong;
    }

    /**
     * Accesseur de la phrase secrète associée au certificat (chiffrée en base 64).
     * 
     * @return la phrase secrète associée au certificat
     */
    public String getQuestionreponse()
    {
        return questionreponse;
    }

    /**
     * Accesseur de l'attribut "region" dans l'annuaire ex-DGCP ou ex-DGI.
     * 
     * @return l'attribut "region" dans l'annuaire ex-DGCP ou ex-DGI
     */
    public String getRegion()
    {
        return region;
    }

    /**
     * Accesseur de l'attribut "RoomNumber" dans l'annuaire ex-DGCP ou ex-DGI.
     * 
     * @return l'attribut "RoomNumber" dans l'annuaire ex-DGCP ou ex-DGI
     */
    public String getRoomNumber()
    {
        return roomNumber;
    }

    /**
     * Accesseur du code SAGES dans l'annuaire ex-DGCP ou ex-DGI.
     * 
     * @return le code SAGES "RoomNumber" dans l'annuaire ex-DGCP ou ex-DGI
     */
    public String getSages()
    {
        return sages;
    }

    /**
     * Accesseur du mot de passe compatible Windows (sensible à la casse, chiffré en MD5).
     * 
     * @return le mot de passe compatible Windows
     */
    public String getSambaNTPassword()
    {
        return sambaNTPassword;
    }

    /**
     * Accesseur du nom usuel de l'agent.
     * 
     * @return le nom usuel de l'agent
     */
    public String getSn()
    {
        return sn;
    }

    /**
     * Accesseur du numéro d'identification fiscal de l'usager.
     *
     * @return le numéro d'identification fiscal de l'usager
     */
    public String getSpi()
    {
        return spi;
    }

    /**
     * Accesseur du numéro de téléphone dans l'annuaire ex-DGCP ou ex-DGI.
     * 
     * @return le numéro de téléphone dans l'annuaire ex-DGCP ou ex-DGI
     */
    public String getTelephoneNumber()
    {
        return telephoneNumber;
    }

    /**
     * Accesseur du libellé long du grade SIRH SIRHIUS.
     * 
     * @return le libellé long du grade SIRH SIRHIUS
     */
    public String getTitle()
    {
        return title;
    }

    /**
     * Accesseur du type de certificat.
     * 
     * @return le type de certificat
     */
    public String getTypeCertificat()
    {
        return typeCertificat;
    }

    /**
     * Accesseur de l'identifiant technique de l'agent (identifiant ministériel suffixé par "-0")
     *
     * @return l'identifiant technique de l'agent
     */
    public String getUid()
    {
        return uid;
    }

    /**
     * Accesseur de l'identifiant utilisé pour l'authentification lors de l'accès aux applications
     * (partie gauche de l'adresse mail).
     * 
     * @return l'identifiant utilisé pour l'authentification lors de l'accès aux applications
     */
    public String getUidFonctionnel()
    {
        return uidFonctionnel;
    }

    /**
     * Accesseur de l'identifiant de l'agent dans le SIRH AGORA
     * 
     * @return l'identifiant de l'agent dans le SIRH AGORA
     */
    public String getUidRH()
    {
        return uidRH;
    }

    /**
     * Accesseur des informations sur les certificats détenus par l’agent.
     * 
     * @return les informations sur les certificats détenus par l’agent
     */
    public String getUserCertificate()
    {
        return userCertificate;
    }

    /**
     * Modificateur de l'affectation provenant de l'annuaire ex-DGCP ou ex-DGI.
     * 
     * @param affectation valeur de l'affectation
     */
    public void setAffectation(String affectation)
    {
        this.affectation = affectation;
    }

    /**
     * Modificateur du code annexe de la structure d’affectation principale de l’agent.
     * 
     * @param affectationANNEXE valeur du code annexe de la structure d’affectation principale de l’agent
     */
    public void setAffectationANNEXE(String affectationANNEXE)
    {
        this.affectationANNEXE = affectationANNEXE;
    }

    /**
     * Modificateur du codique de la structure d’affectation principale de l’agent.
     * 
     * @param affectationCODIQUE valeur du codique de la structure d’affectation principale de l’agent
     */
    public void setAffectationCODIQUE(String affectationCODIQUE)
    {
        this.affectationCODIQUE = affectationCODIQUE;
    }

    /**
     * Modificateur du code SAGES de la structure d’affectation principale de l’agent.
     * 
     * @param affectationSAGES valeur du code SAGES de la structure d’affectation principale de l’agent
     * (sur 3, 5, 7 ou 10 caractères selon l’affectation principale de l’agent).
     */
    public void setAffectationSAGES(String affectationSAGES)
    {
        this.affectationSAGES = affectationSAGES;
    }

    /**
     * Modificateur de l'attribut "annexe" provenant de l'annuaire ex-DGCP ou ex-DGI.
     * 
     * @param annexe valeur de l'attribut "annexe"
     */
    public void setAnnexe(String annexe)
    {
        this.annexe = annexe;
    }

    /**
     * Modificateur de l'attribut "businessCategory" provenant de l'annuaire ex-DGCP ou ex-DGI.
     * 
     * @param businessCategory valeur de l'attribut "businessCategory"
     */
    public void setBusinessCategory(String businessCategory)
    {
        this.businessCategory = businessCategory;
    }

    /**
     * Modificateur du code catégorie de l'agent.
     * 
     * @param categorie valeur du code catégorie de l'agent (A, B, ...)
     */
    public void setCategorie(String categorie)
    {
        this.categorie = categorie;
    }

    /**
     * Modificateur du nom de la personne à afficher.
     * 
     * @param cn valeur du nom de la personne à afficher
     */
    public void setCn(String cn)
    {
        this.cn = cn;
    }

    /**
     * Modificateur de l'attribut "CodeFonction" provenant de l'annuaire ex-DGCP ou ex-DGI.
     * 
     * @param codeFonction valeur de l'attribut "CodeFonction"
     */
    public void setCodeFonction(String codeFonction)
    {
        this.codeFonction = codeFonction;
    }

    /**
     * Modificateur du code générique du grade de l'agent.
     * 
     * @param codeGrade valeur du code générique du grade de l'agent
     */
    public void setCodeGrade(String codeGrade)
    {
        this.codeGrade = codeGrade;
    }

    /**
     * @deprecated
     * Modificateur du code SIRH du grade AGORA de l’agent.
     * 
     * @param codeGradeRH valeur du code SIRH du grade AGORA de l’agent
     */
    public void setCodeGradeRH(String codeGradeRH)
    {
        this.codeGradeRH = codeGradeRH;
    }

    /**
     * Modificateur du departement géographique de la personne.
     * 
     * @param departement valeur du département géographique
     */
    public void setDepartement(String departement)
    {
        this.departement = departement;
    }

    /**
     * Modificateur du département profesionnel d'affectation de l'agent provenant de l'annuaire ex-DGCP ou ex-DGI.
     * 
     * @param department la valeur du département profesionnel d'affectation de l'agent
     */
    public void setDepartment(String department)
    {
        this.department = department;
    }

    /**
     * Modificateur de l'information si l’agent est un employé supérieur ou non
     * 
     * @param empsup valeur de l'information si l’agent est un employé supérieur ou non
     */
    public void setEmpsup(String empsup)
    {
        this.empsup = empsup;
    }

    /**
     * Modificateur de l'attribut "fonction" provenant de l'annuaire ex-DGCP ou ex-DGI.
     * 
     * @param fonction valeur de l'attribut "fonction"
     */
    public void setFonction(String fonction)
    {
        this.fonction = fonction;
    }

    /**
     * Modificateur du prénom usuel de l'agent.
     * 
     * @param givenName valeur du prénom usuel de l'agent
     */
    public void setGivenName(String givenName)
    {
        this.givenName = givenName;
    }

    /**
     * Modificateur de l'identifiant provenant de l'annuaire ex-DGI.
     * 
     * @param identDGI valeur de l'identifiant provenant de l'annuaire ex-DGI
     */
    public void setIdentDGI(String identDGI)
    {
        this.identDGI = identDGI;
    }

    /**
     * Modificateur de la liste brute des affectations secondaires de l'agent.
     * 
     * @param listeBruteAffectationsSecondaires valeur de la liste brute des affectations secondaires de l'agent
     * - identifiant ARTEMIS de la structure ;<br/>
     * - code SAGES de la structure ;<br/>
     * - numéro CODIQUE de la structure complété de l’ANNEXE (séparé par un caractère ".") ;<br/>
     * - niveau de responsabilité ;
     * - Lien LDAP vers la structure d’affectation secondaire de l’agent.
     */
    public void setListeBruteAffectationsSecondaires(List<String> listeBruteAffectationsSecondaires)
    {
        this.listeBruteAffectationsSecondaires = listeBruteAffectationsSecondaires;
    }

    /**
     * Modificateur des habilitations de la personne provenant de l'annuaire ex-DGI.
     * 
     * @param listeBruteMefiAppliDgi valeur des habilitations de la personne provenant de l'annuaire ex-DGI
     */
    public void setListeBruteMefiAppliDgi(List<String> listeBruteMefiAppliDgi)
    {
        this.listeBruteMefiAppliDgi = listeBruteMefiAppliDgi;
    }

    /**
     * Modificateur des habilitations de la personne provenant de l'annuaire ex-DGCP.
     * 
     * @param listeBruteMefiAppliHabilitDgcp valeur des habilitations de la personne provenant de l'annuaire ex-DGCP
     */
    public void setListeBruteMefiAppliHabilitDgcp(List<String> listeBruteMefiAppliHabilitDgcp)
    {
        this.listeBruteMefiAppliHabilitDgcp = listeBruteMefiAppliHabilitDgcp;
    }

    /**
     * Modificateur des habilitations de la personne sur une application donnée.
     * 
     * @param listeBruteMefiAppliXxx valeur des habilitations de la personne sur une application donnée
     */
    public void setListeBruteMefiAppliXxx(List<String> listeBruteMefiAppliXxx)
    {
        this.listeBruteMefiAppliXxx = listeBruteMefiAppliXxx;
    }

    /**
     * Modificateur de la liste brute des profils applicatifs.
     * 
     * @param listeBruteProfilApplicatif valeur de la liste brute des profils applicatifs
     */
    public void setListeBruteProfilApplicatif(List<String> listeBruteProfilApplicatif)
    {
        this.listeBruteProfilApplicatif = listeBruteProfilApplicatif;
    }

    /**
     * Modificateur des profils applicatifs longs.
     * 
     * @param listeBruteProfilApplicatifLong valeur des profils applicatifs longs
     */
    public void setListeBruteProfilApplicatifLong(List<String> listeBruteProfilApplicatifLong)
    {
        this.listeBruteProfilApplicatifLong = listeBruteProfilApplicatifLong;
    }

    /**
     * Modificateur des habilitations de la personne.
     * 
     * @param listeHabilitations valeur des habilitations de la personne
     */
    public void setListeHabilitations(Set<HabilitationAnnuaire> listeHabilitations)
    {
        this.listeHabilitations = listeHabilitations;
    }

    /**
     * Modificateur de l'adresse de messagerie électronique.
     * 
     * @param mail valeur de l'adresse de messagerie électronique
     */
    public void setMail(String mail)
    {
        this.mail = mail;
    }

    /**
     * Modificateur de l'attribut "MailDeliveryOption" provenant de l'annuaire ex-DGCP ou ex-DGI.
     * 
     * @param mailDeliveryOption valeur de l'attribut "MailDeliveryOption"
     */
    public void setMailDeliveryOption(String mailDeliveryOption)
    {
        this.mailDeliveryOption = mailDeliveryOption;
    }

    /**
     * Modificateur des données "MefiAppli" provenant de l'ex-DGCP.
     * 
     * @param mefiAppliDGCP la valeur des données "MefiAppli"
     */
    public void setMefiAppliDGCP(String mefiAppliDGCP)
    {
        this.mefiAppliDGCP = mefiAppliDGCP;
    }

    /**
     * Modificateur des données "MefiAppli" provenant de l'ex-DGI.
     * 
     * @param mefiAppliDGI la valeur des données "MefiAppli"
     */
    public void setMefiAppliDGI(String mefiAppliDGI)
    {
        this.mefiAppliDGI = mefiAppliDGI;
    }

    /**
     * Modificateur des données "MefiAppliHabilit" provenant de l'ex-DGCP.
     * 
     * @param mefiAppliHabilitDGCP valeur des données "MefiAppliHabilit"
     */
    public void setMefiAppliHabilitDGCP(String mefiAppliHabilitDGCP)
    {
        this.mefiAppliHabilitDGCP = mefiAppliHabilitDGCP;
    }

    /**
     * Modificateur de l'attribut "MefiPointMAJF" provenant de l'annuaire ex-DGCP ou ex-DGI.
     * 
     * @param mefiPointMAJF valeur de l'attribut "MefiPointMAJF"
     */
    public void setMefiPointMAJF(String mefiPointMAJF)
    {
        this.mefiPointMAJF = mefiPointMAJF;
    }

    /**
     * Modificateur du niveau de responsabilité de l'agent.
     * 
     * @param niveauResponsabilite niveau de responsabilité de l'agent :<br/>
     * - 1 s’il est responsable de sa structure d’affectation principale ;<br/>
     * - 2 s’il est adjoint de sa structure d’affectation principale;<br/>
     * - 0 sinon.
     */
    public void setNiveauResponsabilite(String niveauResponsabilite)
    {
        this.niveauResponsabilite = niveauResponsabilite;
    }

    /**
     * Modificateur de l'identifiant ARTEMIS de la structure d'affectation principale de l'agent.
     * 
     * @param ou valeur de l'identifiant ARTEMIS de la structure d'affectation principale de l'agent
     */
    public void setOu(String ou)
    {
        this.ou = ou;
    }

    /**
     * Modificateur du lien LDAP vers la structure d'affectation principale de l'agent.
     * 
     * @param ouComplet valeur du lien LDAP vers la structure d'affectation principale de l'agent
     */
    public void setOuComplet(String ouComplet)
    {
        this.ouComplet = ouComplet;
    }

    /**
     * Modificateur du libellé long de la structure d'affectation principale de l'agent.
     * 
     * @param ouDescription valeur du libellé long de la structure d'affectation principale de l'agent
     */
    public void setOuDescription(String ouDescription)
    {
        this.ouDescription = ouDescription;
    }

    /**
     * Modificateur du sigle des structures.
     * 
     * @param ouSigle valeur du sigle des structures
     */
    public void setOuSigle(String ouSigle)
    {
        this.ouSigle = ouSigle;
    }

    /**
     * Modificateur de la date validité du mot de passe.
     * 
     * @param passwordExpirationTime valeur de la date validité du mot de passe.
     */
    public void setPasswordExpirationTime(String passwordExpirationTime)
    {
        this.passwordExpirationTime = passwordExpirationTime;
    }

    /**
     * Modificateur de la civilité de la personne.
     * 
     * @param personalTitle valeur de la civilité de la personne
     */
    public void setPersonalTitle(String personalTitle)
    {
        this.personalTitle = personalTitle;
    }

    /**
     * Modificateur du bureau distributeur.
     * 
     * @param physicalDeliveryOfficeName valeur du bureau distributeur
     */
    public void setPhysicalDeliveryOfficeName(String physicalDeliveryOfficeName)
    {
        this.physicalDeliveryOfficeName = physicalDeliveryOfficeName;
    }

    /**
     * Modificateur de l'adresse postale.
     * 
     * @param postalAddress valeur de l'adresse postale
     */
    public void setPostalAddress(String postalAddress)
    {
        this.postalAddress = postalAddress;
    }

    /**
     * Modificateur du code postal.
     * 
     * @param postalCode valeur du code postal.
     */
    public void setPostalCode(String postalCode)
    {
        this.postalCode = postalCode;
    }

    /**
     * Modificateur des habilitations au format court.
     * 
     * @param profilApplicatif valeur des habilitations au format court
     * (code de l'application + "," + profil)
     */
    public void setProfilApplicatif(String profilApplicatif)
    {
        this.profilApplicatif = profilApplicatif;
    }

    /**
     * Modificateur des habilitations au format long.
     * 
     * @param profilApplicatifLong valeur des habilitations au format long :<br/>
     * - code de l'application ; <br/>
     * - caractère séparateur "," ;<br/>
     * - une ligne XML contenant les caractéristiques de l'habilitation (profils associés + filtre associés) ;<br/>
     * - caractère séparateur ";".
     */
    public void setProfilApplicatifLong(String profilApplicatifLong)
    {
        this.profilApplicatifLong = profilApplicatifLong;
    }

    /**
     * Modificateur de la phrase secrète associée au certificat.
     * 
     * @param questionreponse valeur de la phrase secrète associée au certificat (chiffrée en base 64)
     */
    public void setQuestionreponse(String questionreponse)
    {
        this.questionreponse = questionreponse;
    }

    /**
     * Modificateur de l'attribut "region" provenant de l'annuaire ex-DGCP ou ex-DGI.
     * 
     * @param region valeur de l'attribut "region" provenant de  l'annuaire ex-DGCP ou ex-DGI
     */
    public void setRegion(String region)
    {
        this.region = region;
    }

    /**
     * Modificateur de l'attribut "RoomNumber" provenant de l'annuaire ex-DGCP ou ex-DGI.
     * 
     * @param roomNumber valeur de l'attribut "RoomNumber" provenant de  l'annuaire ex-DGCP ou ex-DGI
     */
    public void setRoomNumber(String roomNumber)
    {
        this.roomNumber = roomNumber;
    }

    /**
     * Modificateur du code SAGES provenant de l'annuaire ex-DGCP ou ex-DGI.
     * 
     * @param sages valeur du code SAGES provenant de l'annuaire ex-DGCP ou ex-DGI.
     */
    public void setSages(String sages)
    {
        this.sages = sages;
    }

    /**
     * Modificateur du mot de passe compatible Windows.
     * 
     * @param sambaNTPassword valeur du mot de passe compatible Windows (sensible à la casse, chiffré en MD5).
     */
    public void setSambaNTPassword(String sambaNTPassword)
    {
        this.sambaNTPassword = sambaNTPassword;
    }

    /**
     * Modificateur du nom usuel de l'agent.
     * 
     * @param sn valeur du nom usuel de l'agent
     */
    public void setSn(String sn)
    {
        this.sn = sn;
    }

    /**
     * Modificateur du numéro d'identification fiscal de l'usager.
     * 
     * @param spi valeur du numéro d'identification fiscal de l'usager
     */
    public void setSpi(String spi)
    {
        this.spi = spi;
    }

    /**
     * Modificateur du numéro de téléphone provenant de l'annuaire ex-DGCP ou ex-DGI.
     * 
     * @param telephoneNumber valeur du numéro de téléphone provenant de l'annuaire ex-DGCP ou ex-DGI
     */
    public void setTelephoneNumber(String telephoneNumber)
    {
        this.telephoneNumber = telephoneNumber;
    }

    /**
     * Modificateur du libellé long du grade SIRH SIRHIUS.
     * 
     * @param title valeur du libellé long du grade SIRH SIRHIUS
     */
    public void setTitle(String title)
    {
        this.title = title;
    }

    /**
     * Modificateur du type de certificat.
     * 
     * @param typeCertificat valeur du type de certificat
     */
    public void setTypeCertificat(String typeCertificat)
    {
        this.typeCertificat = typeCertificat;
    }

    /**
     * Modificateur de l'identifiant technique de l'agent.
     * 
     * @param uid valeur de l'identifiant technique de l'agent (identifiant ministériel suffixé par "-0")
     */
    public void setUid(String uid)
    {
        this.uid = uid;
    }

    /**
     * Modificateur de l'identifiant utilisé pour l'authentification lors de l'accès aux applications
     * 
     * @param uidFonctionnel valeur de l'identifiant utilisé pour l'authentification lors de l'accès aux applications
     * (partie gauche de l'adresse mail)
     */
    public void setUidFonctionnel(String uidFonctionnel)
    {
        this.uidFonctionnel = uidFonctionnel;
    }

    /**
     * Modificateur de l'identifiant de l'agent dans le SIRH AGORA
     * 
     * @param uidRH valeur de l'identifiant de l'agent dans le SIRH AGORA
     */
    public void setUidRH(String uidRH)
    {
        this.uidRH = uidRH;
    }

    /**
     * Modificateur des informations sur les certificats détenus par l’agent.
     * 
     * @param userCertificate valeur des informations sur les certificats détenus par l’agent
     */
    public void setUserCertificate(String userCertificate)
    {
        this.userCertificate = userCertificate;
    }

    /**
     * Accesseur du mot de passe standard LDAP (chiffré en SSHA).
     *
     * @return le mot de passe standard LDAP
     */
    public String getUserPassword()
    {
        return userPassword;
    }

    /**
     * Modificateur du mot de passe standard LDAP.
     *
     * @param userPassword valeur du mot de passe standard LDAP (chiffré en SSHA).
     */
    public void setUserPassword(String userPassword)
    {
        this.userPassword = userPassword;
    }

    /**
     * Accesseur des identifiants d'organisme.
     * 
     * @return les identifiants d'organisme
     */
    public List<String> getListeBruteIdOrganisme()
    {
        return listeBruteIdOrganisme;
    }

    /**
     * Modificateur des identifiants d'organisme.
     * 
     * @param listeBruteIdOrganisme valeur des identifiants d'organisme
     */
    public void setListeBruteIdOrganisme(List<String> listeBruteIdOrganisme)
    {
        this.listeBruteIdOrganisme = listeBruteIdOrganisme;
    }

    /**
     * Accesseur de l'identifiant de l’agent dans le SIRH
     *
     * @return l'identifiant de l’agent dans le SIRH
     */
    public String getUidSirhius()
    {
        return uidSirhius;
    }

    /**
     * Mutateur de l'identifiant de l’agent dans le SIRH
     *
     * @param uidSirhius identifiant de l’agent dans le SIRH
     */
    public void setUidSirhius(String uidSirhius)
    {
        this.uidSirhius = uidSirhius;
    }

    /**
     * Accesseur du code SIRH du grade SIRHIUS de l’agent
     *
     * @return le code SIRH du grade SIRHIUS de l’agent
     */
    public String getCodeGradeSirhius()
    {
        return codeGradeSirhius;
    }

    /**
     * Mutateur du code SIRH du grade SIRHIUS de l’agent
     *
     * @param codeGradeSirhius code SIRH du grade SIRHIUS de l’agent
     */
    public void setCodeGradeSirhius(String codeGradeSirhius)
    {
        this.codeGradeSirhius = codeGradeSirhius;
    }

    /**
     * Accesseur du code du CSRH territorial
     *
     * @return le code du CSRH territorial
     */
    public String getCodeCSRH()
    {
        return codeCSRH;
    }

    /**
     * Mutateur du code du CSRH territorial
     *
     * @param codeCSRH code du CSRH territorial
     */
    public void setCodeCSRH(String codeCSRH)
    {
        this.codeCSRH = codeCSRH;
    }

}
