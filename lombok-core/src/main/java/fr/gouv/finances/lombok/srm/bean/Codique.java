/*
 * Copyright (c) 2011 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.srm.bean;

import java.util.Objects;

import fr.gouv.finances.lombok.util.base.BaseBean;

/**
 * Objet métier codique.
 * 
 * @author amleplatinec
 * @author Christophe Breheret-Girardin
 */
public class Codique extends BaseBean
{
    /** Initialisation d'un UID. */
    private static final long serialVersionUID = 4868030839320631575L;

    /** Identifiant. */
    private Long id;

    /** Version. */
    private int version;

    /** Libellé. */
    private String libelleCodique;

    /**
     * Constructeur.
     */
    public Codique()
    {
        super();
    }

    /**
     * Constructeur.
     * 
     * @param libelle libellé du codique
     */
    public Codique(String libelle)
    {
        super();
        this.libelleCodique = libelle;
    }

    /** 
     * {@inheritDoc}
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode()
    {
        return Objects.hash(libelleCodique);
    }

    /** 
     * {@inheritDoc}
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj)
    {
        if (obj == null)
        {
            return false;
        }

        if (this.getClass() != obj.getClass())
        {
            return false;
        }

        Codique codique = (Codique) obj;
        return Objects.equals(libelleCodique, codique.libelleCodique);
    }

    /**
     * Accesseur de l'attribut id.
     * 
     * @return id
     */
    public Long getId()
    {
        return id;
    }

    /**
     * Accesseur de l'attribut libelle codique.
     * 
     * @return libelle codique
     */
    public String getLibelleCodique()
    {
        return libelleCodique;
    }

    /**
     * Accesseur de l'attribut version.
     * 
     * @return version
     */
    public int getVersion()
    {
        return version;
    }

    /**
     * Modificateur de l'attribut id.
     * 
     * @param id valeur de id
     */
    public void setId(Long id)
    {
        this.id = id;
    }

    /**
     * Modificateur de l'attribut libelle codique.
     * 
     * @param libelleCodique valeur de libelle codique
     */
    public void setLibelleCodique(String libelleCodique)
    {
        this.libelleCodique = libelleCodique;
    }

    /**
     * Modificateur de l'attribut version.
     * 
     * @param version valeur de version
     */
    public void setVersion(int version)
    {
        this.version = version;
    }
}
