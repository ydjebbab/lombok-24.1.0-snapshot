/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : PieceJointe.java
 *
 */
package fr.gouv.finances.lombok.mel.techbean;

import org.springframework.core.io.InputStreamSource;

import fr.gouv.finances.lombok.util.base.BaseTechBean;

/**
 * Class PieceJointe.
 * 
 * @author lcontinsouzas-cp
 * @version $Revision: 1.2 $ Date: 15 déc. 2009
 */
public class PieceJointe extends BaseTechBean
{

    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** nom pj. */
    private final String nomPj;

    /** source. */
    private final InputStreamSource source;

    /**
     * Constructeur de la classe PieceJointe.java
     *
     */
    public PieceJointe()
    {
        super(); 
        this.nomPj=null;
        this.source=null;
        
    }

    /**
     * Instanciation de piece jointe.
     * 
     * @param nompj --
     * @param source --
     */
    public PieceJointe(String nompj, InputStreamSource source)
    {
        super();
        this.nomPj = nompj;
        this.source = source;
    }

    /**
     * Accesseur de l attribut nom pj.
     * 
     * @return nom pj
     */
    public String getNomPj()
    {
        return nomPj;
    }

    /**
     * Accesseur de l attribut source.
     * 
     * @return source
     */
    public InputStreamSource getSource()
    {
        return source;
    }
}
