/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : UuidGeneratorService.java
 *
 */
package fr.gouv.finances.lombok.uuid.service;

/**
 * Interface UuidGeneratorService --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.2 $ Date: 11 déc. 2009
 */
@Deprecated
public interface UuidGeneratorService
{

    /**
     * methode Next uid : --.
     * 
     * @return string
     */
    public String nextUid();

}
