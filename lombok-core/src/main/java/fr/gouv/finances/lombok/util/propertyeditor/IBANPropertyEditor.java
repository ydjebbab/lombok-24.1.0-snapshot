/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : IBANPropertyEditor.java
 *
 */
package fr.gouv.finances.lombok.util.propertyeditor;

import java.beans.PropertyEditorSupport;
import java.math.BigDecimal;

import org.apache.commons.lang.StringUtils;

;

/**
 * Class IBANPropertyEditor --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.3 $ Date: 11 déc. 2009
 */
public class IBANPropertyEditor extends PropertyEditorSupport
{

    /**
     * Constructeur de la classe IBANPropertyEditor.java
     *
     */
    public IBANPropertyEditor()
    {
        super(); 
        
    }

    /**
     * Constructeur de la classe IBANPropertyEditor.java
     *
     * @param source
     */
    public IBANPropertyEditor(Object source)
    {
        super(source);    // DOCUMENTEZ_MOI Raccord de constructeur auto-généré
        
    }

    /*
     * va supprimer tous les blancs contenus dans l'iban (non-Javadoc)
     * @see java.beans.PropertyEditorSupport#getAsText()
     */
    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @return as text
     * @see java.beans.PropertyEditorSupport#getAsText()
     */
    public String getAsText()
    {
        Object value = getValue();
        String valueString = value.toString();

        return StringUtils.deleteWhitespace(valueString);
    }

    /*
     * insere un blanc tous les 4 caractéres
     * @see java.beans.PropertyEditorSupport#setAsText(java.lang.String)
     */
    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param text le nouveau as text
     * @see java.beans.PropertyEditorSupport#setAsText(java.lang.String)
     */
    public void setAsText(String text)
    {
        String result = "";

        int i = 0;
        int j = 0;

        BigDecimal[] division = (new BigDecimal(text.length())).divideAndRemainder(new BigDecimal(4));

        // 34 est la longueur maximale d'un iban
        String[] tableauString = new String[34];

        while (i < (4 * division[0].intValue()))
        {
            result = text.substring(i, i + 4);
            result = result.concat(" ");
            if (i == 1)
            {
                j = 1;
            }
            else
            {
                j = i / 4;
            }
            tableauString[j] = result;

            i = i + 4;
        }
        tableauString[j + 1] = text.substring(4 * division[0].intValue(), text.length());
        setValue(StringUtils.join(tableauString));
    }

}
