/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 */
package fr.gouv.finances.lombok.util.exception;

/**
 * Super classe pour l'ensemble des exceptions non gérables dans l'application,
 * c'est à dire pour lesquelles l'appelant ne pourra proposer aucune solution corrective.
 * Il faudra obligatoirement une correction de l'application au niveau exploitation ou développement
 * pour résoudre cette erreur.
 * 
 * @author chouard-cp
 * @author Christophe Breheret-Girardin
 */
public class IngerableException extends ApplicationException
{

    /** Initialisation de l'UID. */
    private static final long serialVersionUID = 1L;

    /** Elément notable lié à l'erreur. */
    private String element;

    /**
     * Constructeur.
     */
    public IngerableException()
    {
        super();
    }

    /**
     * Constructeur.
     * 
     * @param message message d'erreur
     */
    public IngerableException(String message)
    {
        super(message);
    }

    /**
     * Constructeur.
     * 
     * @param message message d'erreur
     * @param paramElement élément notable lié à l'erreur
     */
    public IngerableException(String message, String paramElement)
    {
        super(message);
        this.element = paramElement;
    }
    
    /**
     * Constructeur.
     * 
     * @param message message d'erreur
     * @param erreur cause de l'erreur
     */
    public IngerableException(String message, Throwable erreur)
    {
        super(message, erreur);
    }

    /**
     * Constructeur.
     * 
     * @param message message d'erreur
     * @param paramElement élément notable lié à l'erreur
     * @param erreur cause de l'erreur
     */
    public IngerableException(String message, String paramElement, Throwable erreur)
    {
        super(message, erreur);
        this.element = paramElement;
    }

    /**
     * Constructeur.
     * 
     * @param erreur cause de l'erreur
     */
    public IngerableException(Throwable erreur)
    {
        super(erreur);
    }

    /**
     * Accesseur de element.
     *
     * @return element
     */
    public String getElement()
    {
        return element;
    }
}