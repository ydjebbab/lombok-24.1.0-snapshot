/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 *
 */
package fr.gouv.finances.lombok.mvc.jasperreports;

import net.sf.jasperreports.engine.JRExporter;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.export.JRPdfExporter;

import org.springframework.web.servlet.view.jasperreports.AbstractJasperReportsSingleFormatView;

/**
 * Classe CpJasperReportsPdfView --.
 * 
 * @author amleplatinec
 */
public class CpJasperReportsPdfView extends AbstractJasperReportsSingleFormatView
{

    /**
     * Constructeur.
     */
    public CpJasperReportsPdfView()
    {
        setContentType("application/pdf");
    }

    /**
     * Surcharge de la méthode pour la rendre finale, pour que l'appel, au sein du constructeur,
     * ne soit plus une mauvaise pratique puisqu'elle ne peut plus ainsi être surchargée dans les
     * classes dérivées.
     *
     * @see org.springframework.web.servlet.view.AbstractView#setContentType(java.lang.String)
     */
    @Override
    public final void setContentType(String contentType)
    {
        super.setContentType(contentType);
    }
    
    /**
     * WP Correction d'une incompatibilité entre la vue Spring et la version 2.05 de jasperreport : erreur de cast entre
     * un Integer et un String dans la méthode exportreport de la classe JRPdfExporter, code concerné String
     * strPdfVersion = getStringParameter( JRPdfExporterParameter.PDF_VERSION,
     * JRPdfExporterParameter.PROPERTY_PDF_VERSION ); Convert the supplied parameter value into the actual type required
     * by the corresponding {@link JRExporterParameter}.
     * <p>
     * The default implementation simply converts the String values "true" and "false" into corresponding
     * <code>Boolean</code> objects, and tries to convert String values that start with a digit into
     * <code>Integer</code> objects (simply keeping them as String if number conversion fails).
     * 
     * @param parameter the parameter key
     * @param value the parameter value
     * @return the converted parameter value
     */
    @Override
    protected Object convertParameterValue(JRExporterParameter parameter, Object value)
    {
        if (value instanceof String)
        {
            String str = (String) value;
            if ("true".equals(str))
            {
                return Boolean.TRUE;
            }
            else if ("false".equals(str))
            {
                return Boolean.FALSE;
            }

            /*
             * else if (str.length() > 0 && Character.isDigit(str.charAt(0))) { // Looks like a number... let's try. try
             * { return new Integer(str); } catch (NumberFormatException ex) { // OK, then let's keep it as a String
             * value. return str; } }
             */
        }
        return value;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @return jR exporter
     * @see org.springframework.web.servlet.view.jasperreports.AbstractJasperReportsSingleFormatView#createExporter()
     */
    @Override
    protected JRExporter createExporter()
    {
        return new JRPdfExporter();
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @return true, si c'est vrai
     * @see org.springframework.web.servlet.view.jasperreports.AbstractJasperReportsSingleFormatView#useWriter()
     */
    @Override
    protected boolean useWriter()
    {
        return false;
    }

}
