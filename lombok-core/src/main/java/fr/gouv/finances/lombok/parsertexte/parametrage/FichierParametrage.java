/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 */
package fr.gouv.finances.lombok.parsertexte.parametrage;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Class FichierParametrage --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.3 $ Date: 11 déc. 2009
 */
public class FichierParametrage
{

    /** Constant : TYPE_FICHIER_SEPARATEUR. */
    public static final int TYPE_FICHIER_SEPARATEUR = 1;

    /** Constant : TYPE_FICHIER_POSITION_FIXE. */
    public static final int TYPE_FICHIER_POSITION_FIXE = 2;

    /** Constant : TYPE_FICHIER_POSITION_GESTIONGUILLEMETS. */
    public static final int TYPE_FICHIER_POSITION_GESTIONGUILLEMETS = 3;

    /** version. */
    String version = null;

    /** type. */
    Integer type = null;

    /** caractere separation. */
    String caractereSeparation = null;

    /** nb lignes entete. */
    // indique le nombre de ligne en entête du fichier qui doivent être ignorées
    Integer nbLignesEntete = null;

    /** lignes. */
    List lignes = null;

    /** mappings ligne classe. */
    Map mappingsLigneClasse = null;

    /** classes. */
    Map classes = null;

    /** custom property editors. */
    List customPropertyEditors = null;

    /** gestion des Guillemets. */

    boolean gestionGuillemets = true;

    /**
     * Instanciation de fichier parametrage.
     */
    public FichierParametrage()
    {
        super();
    }

    /**
     * Cette méthode ajoute une definition de custom property editor.
     * 
     * @param pCustomPropertyEditor le custom property editor
     */
    public void ajouterCustomPropertyEditor(DefinitionCustomPropertyEditor pCustomPropertyEditor)
    {
        if (this.customPropertyEditors == null)
        {
            this.customPropertyEditors = new ArrayList();
        }
        this.customPropertyEditors.add(pCustomPropertyEditor);
    }

    /*
     * Cette méthode ajouter une ligne à celles déjà gérée par ce fichier
     */
    /**
     * methode Ajouter ligne : --.
     * 
     * @param pLigne le ligne
     */
    public void ajouterLigne(LigneFichier pLigne)
    {
        if (this.lignes == null)
        {
            this.lignes = new ArrayList();
        }
        this.lignes.add(pLigne);
    }

    /**
     * methode Ajouter mapping classe : --.
     * 
     * @param pMappingLigneClasse le mapping ligne classe
     */
    public void ajouterMappingClasse(MappingLigneClasse pMappingLigneClasse)
    {
        if (pMappingLigneClasse != null)
        {
            if (this.mappingsLigneClasse == null)
            {
                this.mappingsLigneClasse = new HashMap();
            }
            this.mappingsLigneClasse.put(pMappingLigneClasse.getIdLigne(), pMappingLigneClasse);
        }
    }

    /**
     * methode Controle integrite parametrage : --.
     * 
     * @return string
     */
    public String controleIntegriteParametrage()
    {
        /*
         * prévoir le contrôle d'intégrité du fichier de paramétrage.
         */
        return null;
    }

    /**
     * Accesseur de l attribut caractere separation.
     * 
     * @return caractere separation
     */
    public String getCaractereSeparation()
    {
        return caractereSeparation;
    }

    /**
     * Accesseur de l attribut classes.
     * 
     * @return classes
     */
    public Map getClasses()
    {
        return classes;
    }

    /**
     * Accesseur de l attribut custom property editors.
     * 
     * @return custom property editors
     */
    public List getCustomPropertyEditors()
    {
        return customPropertyEditors;
    }

    /**
     * get de gestionGuillemets
     * 
     * @return the gestion des Guillemets
     */
    public boolean getGestionGuillemets()
    {
        return gestionGuillemets;
    }

    /**
     * Accesseur de l attribut lignes.
     * 
     * @return lignes
     */
    public List getLignes()
    {
        return lignes;
    }

    /**
     * Accesseur de l attribut mappings ligne classe.
     * 
     * @return mappings ligne classe
     */
    public Map getMappingsLigneClasse()
    {
        return mappingsLigneClasse;
    }

    /**
     * Accesseur de l attribut nb lignes entete.
     * 
     * @return nb lignes entete
     */
    public Integer getNbLignesEntete()
    {
        if (nbLignesEntete != null)
        {
            return nbLignesEntete;
        }
        else
        {
            return Integer.valueOf (0);
        }
    }

    /**
     * Accesseur de l attribut type.
     * 
     * @return type
     */
    public Integer getType()
    {
        return type;
    }

    /**
     * Accesseur de l attribut version.
     * 
     * @return version
     */
    public String getVersion()
    {
        return version;
    }

    /**
     * Retourne mapping pour ligne fichier.
     * 
     * @param pLigne le ligne
     * @return the mapping ligne classe
     */
    public MappingLigneClasse retourneMappingPourLigneFichier(LigneFichier pLigne)
    {
        MappingLigneClasse mappingTmp = null;

        if ((this.mappingsLigneClasse != null) && (pLigne != null))
        {
            mappingTmp = (MappingLigneClasse) this.mappingsLigneClasse.get(pLigne.getId());
        }
        return mappingTmp;
    }

    /**
     * Retourne type ligne fichier pour id.
     * 
     * @param pId le id
     * @return the ligne fichier
     */
    public LigneFichier retourneTypeLigneFichierPourId(String pId)
    {
        List listeLignes = this.getLignes();
        Iterator iterLignes = listeLignes.iterator();
        boolean bSortieBoucle = false;
        LigneFichier ligneRetour = null;

        do
        {
            if (iterLignes.hasNext())
            {
                LigneFichier descriptionLigne = (LigneFichier) iterLignes.next();
                if (descriptionLigne.getId().equalsIgnoreCase(pId))
                {
                    ligneRetour = descriptionLigne;
                    bSortieBoucle = true;
                }
            }
            else
            {
                bSortieBoucle = true;
            }

        }
        while ((!bSortieBoucle));

        return ligneRetour;
    }

    /**
     * Modificateur de l attribut caractere separation.
     * 
     * @param caractereSeparation le nouveau caractere separation
     */
    public void setCaractereSeparation(String caractereSeparation)
    {
        this.caractereSeparation = caractereSeparation;
    }

    /**
     * Modificateur de l attribut classes.
     * 
     * @param classes le nouveau classes
     */
    public void setClasses(Map classes)
    {
        this.classes = classes;
    }

    /**
     * Modificateur de l attribut custom property editors.
     * 
     * @param customPropertyEditors le nouveau custom property editors
     */
    public void setCustomPropertyEditors(List customPropertyEditors)
    {
        this.customPropertyEditors = customPropertyEditors;
    }

    /**
     * set de gestionGuillemets
     * 
     * @param gestionGuillemets the new gestion des Guillemets
     */
    public void setGestionGuillemets(boolean gestionGuillemets)
    {
        this.gestionGuillemets = gestionGuillemets;
    }

    /**
     * Modificateur de l attribut lignes.
     * 
     * @param lignes le nouveau lignes
     */
    public void setLignes(List lignes)
    {
        this.lignes = lignes;
    }

    /**
     * Modificateur de l attribut mappings ligne classe.
     * 
     * @param mappingsLigneClasse le nouveau mappings ligne classe
     */
    public void setMappingsLigneClasse(Map mappingsLigneClasse)
    {
        this.mappingsLigneClasse = mappingsLigneClasse;
    }

    /**
     * Modificateur de l attribut nb lignes entete.
     * 
     * @param nbLignesEntete le nouveau nb lignes entete
     */
    public void setNbLignesEntete(Integer nbLignesEntete)
    {
        this.nbLignesEntete = nbLignesEntete;
    }

    /**
     * Modificateur de l attribut type.
     * 
     * @param type le nouveau type
     */
    public void setType(Integer type)
    {
        this.type = type;
    }

    /**
     * Modificateur de l attribut version.
     * 
     * @param version le nouveau version
     */
    public void setVersion(String version)
    {
        this.version = version;
    }

}
