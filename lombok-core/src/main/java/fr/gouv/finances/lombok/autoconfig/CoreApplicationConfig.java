package fr.gouv.finances.lombok.autoconfig;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

/**
 * Classe de configuration qui permet de conditionner les ressources à importer
 * @author celfer
 * Date: 7 févr. 2020
 */
@Configuration
@EnableConfigurationProperties({ClamavProperties.class, UploadProperties.class})
public class CoreApplicationConfig
{    
    private static final Logger log = LoggerFactory.getLogger(CoreApplicationConfig.class);

    @Autowired
    private ClamavProperties clamavProperties;
    
    @Autowired
    private UploadProperties uploadProperties;

    @PostConstruct
    private void afficherProprietes()
    {     
        if (log.isInfoEnabled())
        {
            log.info("Valeurs des propriétés du composant clamav : {}  ", clamavProperties);
            log.info("Valeurs des propriétés du composant upload : {}  ", uploadProperties);
        }
    }

    @Configuration
    @ConditionalOnExpression("'${lombok.composant.atlas.inclus}' == 'true' or '${lombok.composant.upload.inclus}' == 'true'")
    @ImportResource({"classpath:conf/applicationContext-upload-dao.xml", "classpath:conf/applicationContext-upload-service.xml"})
    @ComponentScan(basePackages = {"fr.gouv.finances.lombok.upload.jpa.dao.impl"})
    public class CoreAtlasUploadApplicationConfig
    {

    }

    @Configuration
    @ConditionalOnProperty(name = "lombok.composant.clamav.inclus", havingValue = "true")
    @ImportResource({"classpath:conf/applicationContext-clamav-service.xml"})
    public class CoreClamavApplicationConfig
    {

    }

    @Configuration
    @ConditionalOnProperty(name = "lombok.composant.clamav.simulation.inclus", havingValue = "true")
    @ImportResource({"classpath:conf/applicationContext-clamav-simulation-service.xml"})
    public class CoreClamavSimulationApplicationConfig
    {

    }

    @Configuration
    @ConditionalOnProperty(name = "lombok.composant.clamav.simulation.offline.inclus", havingValue = "true")
    @ImportResource({"classpath:conf/applicationContext-clamav-simulation-offline.xml"})
    public class CoreClamavSimulationOfflineApplicationConfig
    {

    }
    
    
    @Configuration
    @ConditionalOnProperty(name = "lombok.composant.clamav.batchdao.inclus", havingValue = "true")
    @ImportResource({"classpath:conf/applicationContext-commun-batchdao.xml"})
    public class CoreBatchDaoApplicationConfig
    {

    }
    
    @Configuration
    @ConditionalOnProperty(name = "lombok.composant.clamav.batchdao.inclus", havingValue = "false")
    @ImportResource({"classpath:conf/applicationContext-commun-dao.xml"})
    public class CoreNonBatchDaoApplicationConfig
    {

    }

}
