/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : BaseTechBean.java
 *
 */
package fr.gouv.finances.lombok.util.base;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlType;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Class BaseTechBean
 * 
 * @author amleplatinec
 * @version $Revision: 1.3 $ Date: 11 déc. 2009
 */
@XmlType(namespace = "http://base.util.lombok.finances.gouv.fr")
public abstract class BaseTechBean implements Serializable
{
    protected final Log log = LogFactory.getLog(getClass());

    public BaseTechBean()
    {

    }
}
