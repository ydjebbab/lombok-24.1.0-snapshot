/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 */
package fr.gouv.finances.lombok.securite.techbean;

import fr.gouv.finances.lombok.util.base.BaseTechBean;

/**
 * Class ValeurFiltreHabilitationAnnuaire.
 * 
 * @author lcontinsouzas-cp
 * @version $Revision: 1.2 $ Date: 15 déc. 2009
 */
public class ValeurFiltreHabilitationAnnuaire extends BaseTechBean
{

    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** valeur. */
    private String valeur;

    /**
     * Constructeur de la classe ValeurFiltreHabilitationAnnuaire.java
     *
     */
    public ValeurFiltreHabilitationAnnuaire()
    {
        super();
        
    }

    /**
     * Accesseur de l attribut valeur.
     * 
     * @return valeur
     */
    public String getValeur()
    {
        return valeur;
    }

    /**
     * Modificateur de l attribut valeur.
     * 
     * @param valeur le nouveau valeur
     */
    public void setValeur(String valeur)
    {
        this.valeur = valeur;
    }
}
