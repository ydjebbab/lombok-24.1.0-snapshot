package fr.gouv.finances.lombok.mel.service;

/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : PrefixeUrlService.java
 *
 */

import java.util.List;

/**
 * Interface PrefixeUrlService Classe fournissant les préfixes d'url d'accèsà l'application ceci peut être utile en
 * particulier pour envoyer des liens retours vers l'application dans des mels.
 * 
 * @author lcontinsouzas-cp
 * @version $Revision: 1.2 $ Date: 15 déc. 2009
 */
public interface PrefixeUrlService
{

    /**
     * Donne le préfixe de l'url pour atteindre l'application avec portail internet.
     * 
     * @return préfixe d'url
     */
    public String construitPrefixeUrlAccessALApplicationAvecPortailInternet();

    /**
     * Donne le préfixe de l'url pour atteindre l'application avec portail sso.
     * 
     * @return préfixe d'url
     */
    public String construitPrefixeUrlAccessALApplicationAvecPortailSSO();

    /**
     * Donne le préfixe de l'url pour atteindre l'application sans portail.
     * 
     * @return préfixe d'url
     */
    public String construitPrefixeUrlAccessALApplicationSansPortail();

    /**
     * Donne la liste des préfixes d'url pour atteindre l'application en fonction du mode de démarrage (appli,
     * appliportail, portail.
     * 
     * @return liste de préfixes d'url
     */
    public List<String> donnerListeDesPrefixesUrlPossiblesPourAccesALapplication();

    /**
     * Donne le mode d'authentification de l'application (appli,appliportail, portail).
     * 
     * @return the modeauthentification
     */
    public String getModeauthentification();

}