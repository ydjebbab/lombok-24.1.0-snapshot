/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : OperationPropriete.java
 *
 */
package fr.gouv.finances.lombok.parsertexte.integration;

/**
 * Class OperationPropriete --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.2 $ Date: 11 déc. 2009
 */
public class OperationPropriete
{

    /** Constant : OPE_AUCUNE. */
    public static final int OPE_AUCUNE = -1;

    /** Constant : OPE_DEFORMATAGE_NUMERIQUE_FORMAT_ANGLAIS. */
    public static final int OPE_DEFORMATAGE_NUMERIQUE_FORMAT_ANGLAIS = 1;

    /** Constant : OPE_VIDE_SI_TOUT_ZERO. */
    public static final int OPE_VIDE_SI_TOUT_ZERO = 2;

    /** Constant : OPE_VIDE_SI_TOUT_ZERO_OU_ESPACE. */
    public static final int OPE_VIDE_SI_TOUT_ZERO_OU_ESPACE = 3;

    /** Constant : OPE_INSERTION_SEPARATEUR_DECIMAL_POSITION_1. */
    public static final int OPE_INSERTION_SEPARATEUR_DECIMAL_POSITION_1 = 4;

    /** Constant : OPE_INSERTION_SEPARATEUR_DECIMAL_POSITION_2. */
    public static final int OPE_INSERTION_SEPARATEUR_DECIMAL_POSITION_2 = 5;

    /** Constant : OPE_INSERTION_SEPARATEUR_DECIMAL_POSITION_3. */
    public static final int OPE_INSERTION_SEPARATEUR_DECIMAL_POSITION_3 = 6;

    /** Constant : OPE_MAJUSCULE. */
    public static final int OPE_MAJUSCULE = 7;

    /** Constant : OPE_MINUSCULE. */
    public static final int OPE_MINUSCULE = 8;
    
   
    public OperationPropriete()
    {
        // RAS
    }

}
