/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : EhCacheManagerFactoryBean.java
 *
 */
package fr.gouv.finances.lombok.ehcache.hibernate;

import net.sf.ehcache.CacheException;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.config.Configuration;
import net.sf.ehcache.config.DiskStoreConfiguration;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.cache.ehcache.EhCacheManagerUtils;
import org.springframework.core.io.Resource;

/**
 * Classe utilitaire pour le lancement de EhCache en maitrisant le nom ehcache pour
 * eviter que plusieurs applications dans le même Tomcat ne partagent le même.
 * Se présente sous la forme d'un Factory bean spring qui fournit une instance de cacheManager EhCache, nommée
 * et paramétrée par un objet Configuration (via la methode getObject(), cf fonctionnement des factory bean Spring).
 *
 * @author wpetit-cp
 * @version $Revision: 1.2 $ Date: 15 déc. 2009
 * @see fr.gouv.finances.lombok.ehcache.hibernate.EhCacheConfigurationFactoryBean
 */
public class EhCacheManagerFactoryBean implements FactoryBean<CacheManager>, InitializingBean, DisposableBean
{

    /** logger. */
    protected final Log logger = LogFactory.getLog(getClass());

    /** cache manager. */
    private CacheManager cacheManager;

    /** fichier de configuration ehcache.xml */
    private Resource configFile;

    /** name. */
    private String name;

    /** disk store configuration parameter. */
    private DiskStoreConfiguration diskStoreConfigurationParameter;

    /** shared. */
    private boolean shared = false;
    
    /**
     * Instanciation de eh cache manager factory bean.
     */
    public EhCacheManagerFactoryBean()
    {
        super();
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @throws CacheException the cache exception
     * @see org.springframework.beans.factory.InitializingBean#afterPropertiesSet()
     */
    @Override
    public void afterPropertiesSet() throws CacheException
    {
        logger.info("Initialisation du CacheManager EHCache : " + this.name);
        Configuration configuration = EhCacheManagerUtils.parseConfiguration(this.configFile);
        configuration.addDiskStore(diskStoreConfigurationParameter);
        logger.info("Répertoire de stockage du cache : " + configuration.getDiskStoreConfiguration().getPath());
        
        if (this.shared) {
            // Old-school EhCache singleton sharing...
            // No way to find out whether we actually created a new CacheManager
            // or just received an existing singleton reference.
            // ajout temporaire, voir plus tard pour refaire les configs et supprimer le cache
            this.cacheManager = CacheManager.create(configuration);
        }
        else {
        
            this.cacheManager = new CacheManager(configuration);
        }

        this.cacheManager.setName(this.name);

        logger.info("Liste des caches définis");
        String[] cachesNames = this.cacheManager.getCacheNames();
        for (int i = 0; i < cachesNames.length; i++)
        {
            logger.info("\t\t" + cachesNames[i]);
        }
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @see org.springframework.beans.factory.DisposableBean#destroy()
     */
    @Override
    public void destroy()
    {
        logger.info("Arrêt du CacheManager EHCache " + this.name);
        this.cacheManager.shutdown();
    }

    /**
     * Gets the name.
     *
     * @return the name
     */
    public String getName()
    {
        return name;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @return object
     * @see org.springframework.beans.factory.FactoryBean#getObject()
     */
    @Override
    public CacheManager getObject()
    {
        return this.cacheManager;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @return object type
     * @see org.springframework.beans.factory.FactoryBean#getObjectType()
     */
    @Override
    public Class getObjectType()
    {
        return (this.cacheManager != null ? this.cacheManager.getClass() : CacheManager.class);
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @return true, si c'est singleton
     * @see org.springframework.beans.factory.FactoryBean#isSingleton()
     */
    @Override
    public boolean isSingleton()
    {
        return true;
    }

    /**
     * Sets the name.
     *
     * @param name the new name
     */
    public void setName(String name)
    {
        this.name = name;
    }

    /**
     * Gets the fichier de configuration ehcache.
     *
     * @return the fichier de configuration ehcache
     */
    public Resource getConfigFile()
    {
        return configFile;
    }

    /**
     * Sets the fichier de configuration ehcache.
     *
     * @param configFile the new fichier de configuration ehcache
     */
    public void setConfigFile(Resource configFile)
    {
        this.configFile = configFile;
    }
    
    /**
     * Gets the disk store configuration parameter.
     *
     * @return the disk store configuration parameter
     */
    public DiskStoreConfiguration getDiskStoreConfigurationParameter()
    {
        return diskStoreConfigurationParameter;
    }

    /**
     * Sets the disk store configuration parameter.
     *
     * @param diskStoreConfigurationParameter the new disk store configuration parameter
     */
    public void setDiskStoreConfigurationParameter(DiskStoreConfiguration diskStoreConfigurationParameter)
    {
        this.diskStoreConfigurationParameter = diskStoreConfigurationParameter;
    }

    /**
     * Accesseur de shared
     *
     * @return shared
     */
    public boolean isShared()
    {
        return shared;
    }

    /**
     * Mutateur de shared
     *
     * @param shared shared
     */
    public void setShared(boolean shared)
    {
        this.shared = shared;
    }

}
