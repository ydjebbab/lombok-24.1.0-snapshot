/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : FabriqueDeParseurSAXAvecOptionsCommunes.java
 *
 */
package fr.gouv.finances.lombok.util.xml;

import javax.xml.XMLConstants;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.xml.sax.SAXException;
import org.xml.sax.SAXNotRecognizedException;
import org.xml.sax.SAXNotSupportedException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

import fr.gouv.finances.lombok.util.exception.ProgrammationException;

/**
 * Class FabriqueDeParseurSAXAvecOptionsCommunes.
 * 
 * @author L.Continsouzas Classe utilitaire dont la methode CreateXMLReader donne une instance de parseur SAX avec les
 *         options les plus communes, notamment active la validation par XSD si celui-ci est présent Peut être
 *         sous-classée pour surcharger une option par defaut Usage : Appeler en statique la methode CreateXMLReader
 *         pour obtenir une instance de XMLReader Il faut ensuite attacher le ContentHandler à cette instance avant de
 *         delcaencher le parse La validation xsd est activée néanmoins, il faut que le xml contienne une référence au
 *         xsd et que cette référence soit atteignable en local avec la commande xsi:scemaLocation= exemple de balise
 *         d'entète pour le decodage xsd si le fichier xsd se trouve en relatif dans un repertoire local
 *         ./xsd/MonFichierXSD.xsd : n:MonXML xmlns:n="http://monappli.cp"
 *         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
 *         xsi:schemaLocation="http://monappli.cp ./xsd/MonFichierXSD.xsd">
 * @author chouard-cp
 * @version $Revision: 1.4 $ Date: 15 déc. 2009
 */
public class FabriqueDeParseurSAXAvecOptionsCommunes
{
    /** erreur detectee. */
    protected static boolean erreurDetectee;

    /** Constant : NAMESPACES_FEATURE_ID. */
    protected static final String NAMESPACES_FEATURE_ID = "http://xml.org/sax/features/namespaces";

    /** Constant : NAMESPACE_PREFIXES_FEATURE_ID. */
    protected static final String NAMESPACE_PREFIXES_FEATURE_ID = "http://xml.org/sax/features/namespace-prefixes";

    /** Constant : VALIDATION_FEATURE_ID. */
    protected static final String VALIDATION_FEATURE_ID = "http://xml.org/sax/features/validation";

    /** Constant : SCHEMA_VALIDATION_FEATURE_ID. */
    protected static final String SCHEMA_VALIDATION_FEATURE_ID = "http://apache.org/xml/features/validation/schema";

    /** Constant : SCHEMA_FULL_CHECKING_FEATURE_ID. */
    protected static final String SCHEMA_FULL_CHECKING_FEATURE_ID =
        "http://apache.org/xml/features/validation/schema-full-checking";

    /** Constant : DYNAMIC_VALIDATION_FEATURE_ID. */
    protected static final String DYNAMIC_VALIDATION_FEATURE_ID = "http://apache.org/xml/features/validation/dynamic";

    /** Constant : LOAD_EXTERNAL_DTD_FEATURE_ID. */
    protected static final String LOAD_EXTERNAL_DTD_FEATURE_ID =
        "http://apache.org/xml/features/nonvalidating/load-external-dtd";
    
    /** Constant : LOAD_EXTERNAL_DTD_FEATURE_ID. */
    protected static final String SECURE_PROCESSING =
        "http://javax.xml.XMLConstants/feature/secure-processing";

    /** Constant : DEFAULT_NAMESPACES. */
    protected static final boolean DEFAULT_NAMESPACES = true;

    /** Constant : DEFAULT_NAMESPACE_PREFIXES. */
    protected static final boolean DEFAULT_NAMESPACE_PREFIXES = true;

    /** Constant : DEFAULT_VALIDATION. */
    protected static final boolean DEFAULT_VALIDATION = true;

    /** Constant : DEFAULT_LOAD_EXTERNAL_DTD. */
    protected static final boolean DEFAULT_LOAD_EXTERNAL_DTD = true;

    /** Constant : DEFAULT_SCHEMA_VALIDATION. */
    protected static final boolean DEFAULT_SCHEMA_VALIDATION = true;

    /** Constant : DEFAULT_SCHEMA_FULL_CHECKING. */
    protected static final boolean DEFAULT_SCHEMA_FULL_CHECKING = true;

    /** Constant : DEFAULT_DYNAMIC_VALIDATION. */
    protected static final boolean DEFAULT_DYNAMIC_VALIDATION = true;

    /** namespaces. */
    static boolean namespaces = DEFAULT_NAMESPACES;

    /** namespace prefixes. */
    static boolean namespacePrefixes = DEFAULT_NAMESPACE_PREFIXES;

    /** validation. */
    static boolean validation = DEFAULT_VALIDATION;

    /** external dtd. */
    static boolean externalDTD = DEFAULT_LOAD_EXTERNAL_DTD;

    /** schema validation. */
    static boolean schemaValidation = DEFAULT_SCHEMA_VALIDATION;

    /** schema full checking. */
    static boolean schemaFullChecking = DEFAULT_SCHEMA_FULL_CHECKING;

    /** dynamic validation. */
    static boolean dynamicValidation = DEFAULT_DYNAMIC_VALIDATION;
    
    /**
     * methode Creates the xml reader : --.
     * 
     * @return xML reader
     */
    public static XMLReader createXMLReader()
    {
        XMLReader parser = null;

        try
        {
            parser = XMLReaderFactory.createXMLReader();
            parser.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
        }
        catch (RuntimeException e)
        {
            throw new ProgrammationException(e);
        }
        catch (SAXException e)
        {
            throw new ProgrammationException(e);
        }

        try
        {
            parser.setFeature(NAMESPACES_FEATURE_ID, namespaces);
        }
        catch (SAXException e)
        {
            throw new ProgrammationException(e);
        }

        try
        {
            parser.setFeature(NAMESPACE_PREFIXES_FEATURE_ID, namespacePrefixes);
        }
        catch (SAXException e)
        {
            throw new ProgrammationException(e);
        }

        try
        {
            parser.setFeature(VALIDATION_FEATURE_ID, validation);
        }
        catch (SAXException e)
        {
            throw new ProgrammationException(e);
        }

        try
        {
            parser.setFeature(LOAD_EXTERNAL_DTD_FEATURE_ID, externalDTD);
        }
        catch (SAXNotRecognizedException e)
        {
            throw new ProgrammationException(e);
        }
        catch (SAXNotSupportedException e)
        {
            throw new ProgrammationException(e);
        }

        try
        {
            parser.setFeature(SCHEMA_VALIDATION_FEATURE_ID, schemaValidation);
        }
        catch (SAXNotRecognizedException e)
        {
            throw new ProgrammationException(e);
        }
        catch (SAXNotSupportedException e)
        {
            throw new ProgrammationException(e);
        }

        try
        {
            parser.setFeature(SCHEMA_FULL_CHECKING_FEATURE_ID, schemaFullChecking);
        }
        catch (SAXNotRecognizedException e)
        {
            throw new ProgrammationException(e);
        }
        catch (SAXNotSupportedException e)
        {
            throw new ProgrammationException(e);
        }
        
        try
        {
            //protection XXE du parseur
            // voir:
            //https://www.owasp.org/index.php/XML_External_Entity_%28XXE%29_Prevention_Cheat_Sheet
            parser.setFeature("http://xml.org/sax/features/external-general-entities", false);
            parser.setFeature("http://xml.org/sax/features/external-parameter-entities", false);
            parser.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd",false);
        }
        catch (SAXNotRecognizedException e)
        {
            throw new ProgrammationException(e);
        }
        catch (SAXNotSupportedException e)
        {
            throw new ProgrammationException(e);
        }


        try
        {
            parser.setFeature(DYNAMIC_VALIDATION_FEATURE_ID, dynamicValidation);
        }
        catch (SAXNotRecognizedException e)
        {
            throw new ProgrammationException(e);
        }
        catch (SAXNotSupportedException e)
        {
            throw new ProgrammationException(e);
        }
        


        return parser;
    }

    /** log. */
    protected final Log log = LogFactory.getLog(getClass());

}
