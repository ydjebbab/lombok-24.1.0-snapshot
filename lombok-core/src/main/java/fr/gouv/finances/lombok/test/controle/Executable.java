/*
 * Copyright (c) 2011 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.test.controle;

/**
 * Interface fonctionnelle utilisée pour déterminer si l'exécution d'une méthode lève une exception ou non.
 *
 * @author Christophe Breheret-Girardin
 */
@FunctionalInterface
public interface Executable
{
    /** 
     * Exécution du traitement
     *
     * @throws exception potentiellement levée par le traitement
     */
    void execute() throws Throwable;
}