/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : MonnaieServiceImpl.java
 *
 */
package fr.gouv.finances.lombok.monnaie.service.impl;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import org.apache.commons.beanutils.BeanComparator;

import fr.gouv.finances.lombok.monnaie.bean.Monnaie;
import fr.gouv.finances.lombok.monnaie.bean.TauxDeChancellerie;
import fr.gouv.finances.lombok.monnaie.dao.MonnaieDao;
import fr.gouv.finances.lombok.monnaie.service.MonnaieService;
import fr.gouv.finances.lombok.monnaie.service.TauxDeChancellerieService;
import fr.gouv.finances.lombok.util.base.BaseServiceImpl;
import fr.gouv.finances.lombok.util.exception.RegleGestionException;
import fr.gouv.finances.lombok.util.messages.Messages;

/**
 * Class MonnaieServiceImpl reprise d'éléments communs à MonnaieSErviceIMpl dans eaf et coriolis.
 * 
 * @author chouard-cp
 * @version $Revision: 1.2 $ Date: 15 déc. 2009
 */
public class MonnaieServiceImpl extends BaseServiceImpl implements MonnaieService
{

    private MonnaieDao monnaiedao;

    private TauxDeChancellerieService tauxdechancellerieserviceso;

    public MonnaieServiceImpl()
    {
        super();
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param uneMonnaie
     * @see fr.gouv.finances.lombok.monnaie.service.MonnaieService#creerMonnaie(fr.gouv.finances.lombok.monnaie.bean.Monnaie)
     */
    @Override
    public void creerMonnaie(Monnaie uneMonnaie)
    {
        uneMonnaie.setCodeBDF(uneMonnaie.getCodeBDF().toUpperCase());
        if (!monnaiedao.saveMonnaie(uneMonnaie))
        {
            throw new RegleGestionException(Messages.getString("exception.monnaie.dejaexistante"));

        }

    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param uneMonnaie
     * @see fr.gouv.finances.lombok.monnaie.service.MonnaieService#creerMonnaieEtTaux(fr.gouv.finances.lombok.monnaie.bean.Monnaie)
     */
    @Override
    public void creerMonnaieEtTaux(Monnaie uneMonnaie)
    {
        TauxDeChancellerie monTaux = this.rechercherDernierTauxChancellerie(uneMonnaie);
        monTaux.setLaMonnaie(uneMonnaie);
        uneMonnaie.setTauxChancellerie(monTaux);
        uneMonnaie.setCodeBDF(uneMonnaie.getCodeBDF().toUpperCase());
        if (!monnaiedao.saveMonnaieEtTaux(uneMonnaie))
        {
            throw new RegleGestionException(Messages.getString("exception.monnaie.dejaexistante"));

        }

    }

    /**
     * Accesseur de l attribut monnaiedao.
     * 
     * @return monnaiedao
     */
    public MonnaieDao getMonnaiedao()
    {
        return monnaiedao;
    }

    /**
     * Accesseur de l attribut tauxdechancellerieserviceso.
     * 
     * @return tauxdechancellerieserviceso
     */
    public TauxDeChancellerieService getTauxdechancellerieserviceso()
    {
        return tauxdechancellerieserviceso;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param uneMonnaie
     * @see fr.gouv.finances.lombok.monnaie.service.MonnaieService#modifierMonnaie(fr.gouv.finances.lombok.monnaie.bean.Monnaie)
     */
    @Override
    public void modifierMonnaie(Monnaie uneMonnaie)
    {
        // lignes ci dessous modifiées /eaf
        // initialiserDateEtUtilisateur(monnaieModifiee);
        // monnaieModifiee.setLibelle(monnaieModifiee.getLibelle().toUpperCase());
        monnaiedao.modifyMonnaie(uneMonnaie);

    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param uneMonnaie
     * @return taux de chancellerie
     * @see fr.gouv.finances.lombok.monnaie.service.MonnaieService#rechercherDernierTauxChancellerie(fr.gouv.finances.lombok.monnaie.bean.Monnaie)
     */
    @SuppressWarnings("unchecked")
    @Override
    public TauxDeChancellerie rechercherDernierTauxChancellerie(Monnaie uneMonnaie)
    {
        Set listDesTaux = uneMonnaie.getListTauxChancel();
        BeanComparator monCritereDeChoix = new BeanComparator("dateTauxChancel");
        Collection collectionsMax = listDesTaux;
        return (TauxDeChancellerie) Collections.max(collectionsMax, monCritereDeChoix);
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param code
     * @return monnaie
     * @see fr.gouv.finances.lombok.monnaie.service.MonnaieService#rechercherMonnaieParCode(java.lang.String)
     */
    @Override
    public Monnaie rechercherMonnaieParCode(String code)
    {
        return monnaiedao.findMonnaieParCode(code.toUpperCase(Locale.getDefault()));
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @return list
     * @see fr.gouv.finances.lombok.monnaie.service.MonnaieService#rechercherToutesMonnaies()
     */
    @Override
    public List rechercherToutesMonnaies()
    {
        return monnaiedao.findAllMonnaies();
    }

    /**
     * Modificateur de l attribut monnaiedao.
     * 
     * @param monnaiedao le nouveau monnaiedao
     */
    public void setMonnaiedao(MonnaieDao monnaiedao)
    {
        this.monnaiedao = monnaiedao;
    }

    /**
     * Modificateur de l attribut tauxdechancellerieserviceso.
     * 
     * @param tauxdechancellerieserviceso le nouveau tauxdechancellerieserviceso
     */
    public void setTauxdechancellerieserviceso(TauxDeChancellerieService tauxdechancellerieserviceso)
    {
        this.tauxdechancellerieserviceso = tauxdechancellerieserviceso;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param uneMonnaie
     * @see fr.gouv.finances.lombok.monnaie.service.MonnaieService#supprimerMonnaieEtTaux(fr.gouv.finances.lombok.monnaie.bean.Monnaie)
     */
    @Override
    public void supprimerMonnaieEtTaux(Monnaie uneMonnaie)
    {
        monnaiedao.deleteMonnaie(uneMonnaie);

    }

}
