/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 *
 */
package fr.gouv.finances.lombok.util.base;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import fr.gouv.finances.lombok.util.persistance.ModeCritereRecherche;

/**
 * Interface du socle des DAO quelle que soit l'implémentation choisie.
 *
 * @author amleplatinec
 * @author Christophe Breheret-Girardin
 * @author Celinio Fernandes
 */
public interface CoreBaseDao
{

    /**
     * Vide le contexte de persistance.
     * 
     * @deprecated use {@link #clearPersistenceContext()} instead.
     */
    @Deprecated
    void clearSession();

    /**
     * Vide le contexte de persistance.
     */
    void clearPersistenceContext();

    /**
     * Supprime une entité identifiée par son type et son identifiant.
     *
     * @param type Type de l'entité à supprimer
     * @param identifiant Identifiant de l'entité à supprimer
     */
    abstract <T> void deleteObject(Class<T> type, Serializable identifiant);

    /**
     * Supprime une entité.
     *
     * @param entite entité à supprimer de la base de données.
     */
    abstract <T> void deleteObject(T entite);

    /**
     * Supprime l'entité du cache du contexte de persistance.
     *
     * @param entite entité à supprimer du cache du contexte de persistance
     */
    abstract <T> void evictObject(T entite);

    /**
     * Provoque l'exécution des requêtes SQL nécessaires à la réalisation des actions du gestionnaire de persistance.
     */
    abstract void flush();

    /**
     * Lecture d'une entité identifiée par son type et son identifiant.
     *
     * @param type Type de l'entité à lire
     * @param identifiant Identifiant de l'entité à lire
     * @return l'entité lue
     */
    abstract <T> T getObject(Class<T> type, Serializable identifiant);

    /**
     * Charge toutes les entités d'un type donné.
     *
     * @param type Type des entités à charger
     * @return la liste des entités chargées
     */
    abstract <T> List<T> loadAllObjects(Class<T> type);

    /**
     * Sauvegarde une entité.
     *
     * @param entite entité à sauvegarder en base de données
     */
    abstract <T> void saveObject(final T entite);

    /**
     * Modification d'une entité en base de données.
     *
     * @param entite Entité à modifier
     * @return entité modifiée
     */
    <T> T modifyObject(final T entite);

    /**
     * Rechargement d'une entité
     *
     * @param entite entité à recharcher
     */
    <T> void refresh(T entite);

    /**
     * Méthode permettant de faire une recherche sur une entité donnée, à partir d'un critère.
     *
     * @param <T> Entité à rechercher
     * @param classe Type de l'entité à rechercher
     * @param critere Critere sur lequel il faut appliquer un filtre d'égalité stricte (= propriété de l'entité)
     * @param valeurCritere Valeur du critère (= valeur de la propriété de l'entité)
     * @return l'entité correspondante
     */
    <T> T findbyUniqueCriteria(Class<T> classe, String critere, String valeurCritere);
   
    /**
     * Méthode permettant de faire une recherche sur une entité donnée, à partir d'un critère.
     *
     * @param <T> Entité à rechercher
     * @param classe Type de l'entité à rechercher
     * @param critere Critere sur lequel il faut appliquer un filtre d'égalité stricte (= propriété de l'entité)
     * @param valeurCritere Valeur du critère (= valeur de la propriété de l'entité)
     * @param modeCritereRecherche Type de recherche sur les critères (égalité stricte, égalité large, etc.)
     * @return l'entité correspondante
     */
    <T> T findbyUniqueCriteria(Class<T> classe, String critere, String valeurCritere, ModeCritereRecherche modeCritereRecherche);
    
    /**
     * Méthode permettant de faire une recherche sur des entités données, à partir d'un critère.
     *
     * @param <T> Entité à rechercher
     * @param classe Type des entités à rechercher
     * @param critere Critere sur lequel il faut appliquer un filtre d'égalité stricte (= propriété de l'entité)
     * @param valeurCritere Valeur du critère (= valeur de la propriété de l'entité)
     * @return la liste des entités correspondantes
     */
    <T> List<T> findAllByUniqueCriteria(Class<T> classe, String critere, String valeurCritere);

    /**
     * Méthode permettant de faire une recherche sur une entité donnée, à partir de critères.
     *
     * @param <T> Entité à rechercher
     * @param classe Type de l'entité à rechercher
     * @param criteres Criteres sur lesquels il faut appliquer un filtre (= propriété de l'entité)
     * @param modeCritereRecherche Type de recherche sur les critères (égalité stricte, égalité large, etc.)
     * @return l'entité correspondante
     */
    <T> T findByCriterias(Class<T> classe, Map<String, String> criteres, ModeCritereRecherche modeCritereRecherche);

    /**
     * Méthode permettant de faire une recherche sur des entités données, à partir de critères.
     *
     * @param <T> Entité à rechercher
     * @param classe Type des entités à rechercher
     * @param criteres Criteres sur lesquels il faut appliquer un filtre (= propriété de l'entité)
     * @param modeCritereRecherche Type de recherche sur les critères (égalité stricte, égalité large, etc.)
     * @return la liste des entités correspondantes
     */
    <T> List<T> findAllByCriterias(Class<T> classe, Map<String, String> criteres, ModeCritereRecherche modeCritereRecherche);

    /**
     * Méthode construisant une requête Criteria permettant de faire une recherche sur une entité donnée, en effectuant
     * des jointures sur d'autres entités, à partir de critères.
     *
     * @param <T> Entité à rechercher
     * @param classe Type de l'entité à rechercher
     * @param criteres Criteres sur lesquels il faut appliquer un filtre (= propriétés de l'entité)
     * @param modeCritereRecherche Type de recherche sur les critères (égalité stricte, égalité large, etc.)
     * @return la requête Criteria permettant de faire une recherche sur l'entité
     */
    <T> long countTotalNumberOfLines(Class<T> classe, Map<String, String> criteres, ModeCritereRecherche modeCritereRecherche);

    /**
     * Méthode permettant de savoir si l'association a été chargée ou non.
     *
     * @param entite Entité à tester
     * @return true si l'association a été chargée, false sinon
     */
    boolean isCharged(Object entite);
}