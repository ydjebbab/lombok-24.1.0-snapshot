/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : MenuService.java
 *
 */
package fr.gouv.finances.lombok.menu.service;

/**
 * Interface MenuService. <br>
 * Cree une barre de menu dynamique à partir d'une définition XML. <br>
 * Le fichier se trouve obligatoirement dans (ou à la racine) des packages java. <br>
 * Le chemin du fichier doit être précisé par setFichierDefinitionMenu. Dans le cas contraire on lit le fichier
 * /menudefinition.xml <br>
 * Le fichier XML doit être structuré selon l'exemple ci-dessous. <br>
 * Chaque url est vérifiée en fonction des droits utilisateurs sauf si l'on précise verifierUrl="N" . <br>
 * Utilisez cette option pour marquer tous les liens vers l'extérieur de l'application web. <br>
 * Par défaut seules les urls autorisées sont affichées et les conteneurs ne contenant aucune RubriqueLien avec url
 * autorise ne sont pas afficher sauf si l'attribut afficherSiRienDAutorise="O"
 * {@literal<BarreDeMenusHierarchiques nom="barremenu">}
 * {@literal<SousMenu libelle="Contrat" afficherSiRienDAutorise="O"> } {@literal<RubriqueSousMenu libelle="Adhérer"> }
 * {@literal<RubriqueLien libelle="Prelevement à l'échéance"/>}
 * {@literal</RubriqueSousMenu>} {@literal<RubriqueSousMenu libelle="Prélèvement mensuel"> }
 * {@literal<RubriqueLien libelle="immédiat" url="/p3dweb/saisieadhesionmensualisation.htm?_flowId=particulier.DemandeAdhesion"}
 * {@literalcommentaire="adhésion immediate à la mensualisation"/>}
 * {@literal<RubriqueLien libelle="après imposition" url="/p3dweb/saisieadhesionmensualisation.htm?_flowId=particulier.DemandeAdhesion"}
 * {@literalcommentaire="adhésion à la mensualisation après l'établissement du prochain avis d'imposition"/>}
 * {@literal</RubriqueSousMenu>} {@literal<RubriqueSousMenu libelle="Modifier"> <RubriqueLien libelle=
 * "Prelevement à l'échéance" url="/p3dweb/prelevementalecheance.htm"/>}
 * {@literal<RubriqueLien libelle="Prelevement mensuel" url="/p3dweb/prelevementmensuel.htm"/>}
 * {@literal</SousMenu>} {@literal<SousMenu libelle="Divers"> <RubriqueLien libelle="Magellan" url=
 * "http://magellan" verifierUrl="N">}
 * {@literal<RubriqueLien libelle="Google" url="http://www.google.com" verifierUrl="N"/>}
 * {@literal</SousMenu>} {@literal</BarreDeMenusHierarchiques> }
 * 
 * @author chouard-cp
 * @version $Revision: 1.2 $ Date: 15 déc. 2009
 */
public interface MenuService
{

    /**
     * Creation de la barre de menu.
     * 
     * @param contextpath le chemin du contexte de l'application (répertoire racine des url d'accès à l'application)
     *        correspond au parametre "context path" dans la configuration tomcat
     */
    public void calculerBarreDeMenusDynamique(String contextpath);

    /**
     * Creation de la barre de menu avec passage de paramètre pour l'intercepteur.
     * 
     * @param contextpath le chemin du contexte de l'application (répertoire racine des url d'accès à l'application)
     *        correspond au parametre "context path" dans la configuration tomcat
     * @param parametreIntercepteur un parametre passé à l'intercepteur pour réaliser un traitement spécifique sur le
     *        menu
     */
    public void calculerBarreDeMenusDynamique(String contextpath, Object parametreIntercepteur);

}