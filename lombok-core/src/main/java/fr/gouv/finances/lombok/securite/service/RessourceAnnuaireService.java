/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : RessourceAnnuaireService.java
 *
 */
package fr.gouv.finances.lombok.securite.service;

import java.util.Properties;

import javax.naming.NamingException;
import javax.naming.directory.DirContext;

/**
 * Interface RessourceAnnuaireService --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.3 $ Date: 11 déc. 2009
 */
public interface RessourceAnnuaireService
{

    /**
     * Ferme une connexion ldap.
     * 
     * @param unDirContext --
     */
    public abstract void fermerLDAPConnexion(DirContext unDirContext);

    /**
     * Fournit un squelette d'environnement à compléter du mode d'authentification et des éventuelles uid/mot de passe
     * du compte de connexion.
     * 
     * @return un squelette d'environnement
     */
    public abstract Properties fournirUnSqueletteDEnvironnement();

    /**
     * Ouvre une connexion ldap et renvoie le contexte associé.
     * 
     * @param props "documenté"
     * @return the dir context
     * @throws NamingException the naming exception
     */
    public abstract DirContext ouvreLdapConnexion(Properties props) throws NamingException;

}