/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 *
 */
package fr.gouv.finances.lombok.monnaie.dao;

import java.util.List;

import fr.gouv.finances.lombok.monnaie.bean.Monnaie;
import fr.gouv.finances.lombok.util.base.CoreBaseDao;

/**
 * Interface de gestion des données des monnaies.
 *
 * @author amleplatinec
 * @author Christophe Breheret-Girardin
 */
public interface MonnaieDao extends CoreBaseDao
{
    /**
     * Méthode de suppression d'une monnaie.
     * 
     * @param uneMonnaie monnaie à supprimer
     */
    public void deleteMonnaie(Monnaie uneMonnaie);

    /**
     * Méthode de recherche de toutes les monnaies.
     * 
     * @return toutes les monnaies
     */
    List<Monnaie> findAllMonnaies();

    /**
     * Méthode permettant de récupérer toutes les monnaies, en triant les libellés.
     *
     * @return toutes les monnaies
     */
    List<Monnaie> findAllMonnaiesTriSurLibelle();

    /**
     * Méthode permettant de récupérer une monnaie.
     * 
     * @param code code de la monnaie à récupérer
     * @return monnaie la monnaie correspondant au code fourni
     */
    public Monnaie findMonnaieParCode(String code);

    /**
     * Méthode permettant de modifier une monnaie
     * 
     * @param uneMonnaie monnaie à modifier
     */
    public void modifyMonnaie(Monnaie uneMonnaie);

    /**
     * Méthode permettant de persister une monnaie, et son taux
     * 
     * @param uneMonnaie monnaie à persister
     */
    @Deprecated
    public boolean saveMonnaie(Monnaie uneMonnaie);

    /**
     * Méthode permettant de persister une monnaie, et son taux
     * 
     * @param uneMonnaie monnaie à persister
     */
    public boolean saveMonnaieEtTaux(Monnaie uneMonnaie);

}
