/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 */
package fr.gouv.finances.lombok.util.exception;

import java.util.List;

/**
 * Exception pouvant être gérée au sein de l'application, c'est à dire pour lesquelles
 * l'appelant pourra le cas échéant proposer une solution corrective.
 *
 * @author amleplatinec
 * @author Christophe Breheret-Girardin
 */
public class GerableException extends ApplicationException
{

    /** Initialisation de l'UID. */
    private static final long serialVersionUID = 1L;

    /**
     * Constructeur.
     */
    public GerableException()
    {
        super();
    }

    /**
     * Constructeur.
     * 
     * @param listeMessages liste des messages
     */
    public GerableException(List<String> listeMessages)
    {
        super(listeMessages);
    }

    /**
     * Constructeur, on ajoute un libellé de message qui se retrouvera en
     * premier dans la liste des messages.
     * 
     * @param listeMessages liste des messages
     * @param message message d'erreur
     */
    public GerableException(List<String> listeMessages, String message)
    {
        super(listeMessages, message);
    }

    /**
     * Constructeur.
     * 
     * @param message message d'erreur
     */
    public GerableException(String message)
    {
        super(message);
    }

    /**
     * Constructeur.
     * 
     * @param message message d'erreur
     * @param erreur cause de l'erreur
     */
    public GerableException(String message, Throwable erreur)
    {
        super(message, erreur);
    }

    /**
     * Constructeur.
     * 
     * @param erreur cause de l'erreur
     */
    public GerableException(Throwable erreur)
    {
        super(erreur);
    }

}
