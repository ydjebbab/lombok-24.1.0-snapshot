/*
 * Copyright (c) 2017 DGFiP - Tous droits réservés
 * 
 */
/**
 * Paquet lié aux utilitaires de test des POJO.
 *
 * @author Christophe Breheret-Girardin
 */
package fr.gouv.finances.lombok.test.pojo;