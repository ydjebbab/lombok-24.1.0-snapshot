/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : ConvertisseurDocumentXMLEnGrapheDObjets.java
 *
 */
package fr.gouv.finances.lombok.util.xml;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Stack;

import javax.xml.XMLConstants;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.util.ClassUtils;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.helpers.XMLReaderFactory;

import fr.gouv.finances.lombok.util.exception.ApplicationExceptionTransformateur;
import fr.gouv.finances.lombok.util.exception.ProgrammationException;

/**
 * Class ConvertisseurDocumentXMLEnGrapheDObjets cette classe permet de transformer un document XML en graphe d'objets
 * java le graphe etant stocké en mémoire, son utilisation est réservée à la lecture de documents de taille raisonable.
 * Prévu principalement pour la lecture de fichiers paramètres. Fonctionnement : - le fichier XML ne doit pas comporter
 * de valeurs entre les éléments toutes les valeurs doivent être portées par les attributs - chaque nom d'élément doit
 * correspondre exactement à un nom de classe java - chaque nom d'attribut de classe doit correspondre exactement à un
 * nom d'attribut de la classe java - toutes les classes java représentant un document doivent se trouver dans le même
 * package - quand un élément XML comporte des sous élément, l'objet représentant le sous élément (objet bas de graphe)
 * est rattaché à l'objet représentant l'élément (objet haut de graphe) ceci récursivement sur plusieurs niveaux - pour
 * cela, il faut que les classes de haut de graphe comportent un et un seul attribut de type collection (List de
 * préférence pour conserver l'ordre du document)
 * 
 * @author chouard-cp
 * @version $Revision: 1.3 $ Date: 15 déc. 2009
 */
public class ConvertisseurDocumentXMLEnGrapheDObjets extends DefaultHandler
{

    /** log. */
    private final Log log = LogFactory.getLog(getClass());

    /** nom package des objets java. */
    private String nomPackageDesObjetsJava = null;

    /** classe utilisee comme type des attributs listes. */
    private String classeUtiliseeCommeTypeDesAttributsListes = "java.util.List";

    /** methode utilisee pour ajouter dans les attributs listes. */
    private String methodeUtiliseePourAjouterDansLesAttributsListes = "add";

    /** pile des objets. */
    private final Stack pileDesObjets = new Stack();

    /** objet resultat. */
    private Object objetResultat = null;

    /**
     * The Constructor.
     * 
     * @param nompackagedesobjetsjava --
     */

    public ConvertisseurDocumentXMLEnGrapheDObjets(String nompackagedesobjetsjava)
    {
        this.nomPackageDesObjetsJava = nompackagedesobjetsjava;
    }

    /**
     * The Constructor.
     * 
     * @param nompackagedesojetsjava : nom du package dans lequel on doit trouver toures les classes nécessaires à la
     *        construction des graphes d'objets ex ("fr.gouv.finances.lombok.menu.bean")
     * @param classeutiliseecommetypedesattributsliste --
     * @param methodeutiliseepourajouterdanslesattributsliste : nom de la méthode pour ajouter un objet à la collection
     *        (defaut "add")
     */

    public ConvertisseurDocumentXMLEnGrapheDObjets(String nompackagedesojetsjava,
        String classeutiliseecommetypedesattributsliste, String methodeutiliseepourajouterdanslesattributsliste)
    {
        this(nompackagedesojetsjava);
        this.classeUtiliseeCommeTypeDesAttributsListes = classeutiliseecommetypedesattributsliste;
        this.methodeUtiliseePourAjouterDansLesAttributsListes = methodeutiliseepourajouterdanslesattributsliste;
    }

    /**
     * Méthode à appeler pour convertir un document XML.
     *
     * @param inputXmlSource document XML à convertir
     * @throws SAXException the SAX exception
     * @throws IOException Signal qu'une execption de type I/O s'est produite.
     */
    public synchronized void convertir(InputSource inputXmlSource) throws SAXException, IOException
    {
        XMLReader parser = XMLReaderFactory.createXMLReader();

        /* instantiation d'un deuxième objet pour jouer le role de handler SAX */
        ConvertisseurDocumentXMLEnGrapheDObjets handler =
            new ConvertisseurDocumentXMLEnGrapheDObjets(this.nomPackageDesObjetsJava,
                this.classeUtiliseeCommeTypeDesAttributsListes, this.methodeUtiliseePourAjouterDansLesAttributsListes);
        parser.setContentHandler(handler);

        log.trace("debut du parsing SAX");
        parser.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
        parser.parse(inputXmlSource);

        /* recuperation du resultat dans le handler */
        this.objetResultat = handler.getObjetResultat();

        log.trace("fin du parsing SAX");

    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @throws SAXException the SAX exception
     * @see org.xml.sax.helpers.DefaultHandler#endDocument()
     */
    @Override
    public void endDocument() throws SAXException
    {
        if (objetResultat == null)
        {
            throw new SAXException(
                "Fin de document mais aucun objet resultat - vérifier qu'il n'y a pas plus d'objets empilés que d'objets dépilés");
        }
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param uri
     * @param localName
     * @param qName
     * @throws SAXException the SAX exception
     * @see org.xml.sax.helpers.DefaultHandler#endElement(java.lang.String, java.lang.String, java.lang.String)
     */
    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException
    {

        Class classeDeLAttributDeLObjetSommetDeGraphe;
        Object objetAttributListe = null;
        Method methodeAjout;
        Object[] objetsEnParametresDeLaMethodeAjout = new Object[1];
        Object objetARattacherAuSommetDeGraphe;

        /* On dépile le dernier objet qui devient l'objet à rattacher */

        log.debug("fin d'element détectée : " + qName);

        if (pileDesObjets.size() > 0)
        {
            objetARattacherAuSommetDeGraphe = pileDesObjets.pop();

            log.debug("dépilage ok - objet à rattacher de type :"
                + objetARattacherAuSommetDeGraphe.getClass().getName());

        }
        else
        {
            throw new SAXException("Impossible de retrouver dans la pile l'objet correspondant à la fin d'élément :"
                + qName);
        }

        /*
         * S'il existe, l'avant dernier objet est l'objet sommet de graphe on le lit puis on parcourt ses attriibuts
         * jusqu'à trouver un attribut de type Liste on appelle la méthode d'ajout sur cet attribut en lui passant en
         * paramétre l'objet à rattache
         */

        if (pileDesObjets.size() > 0)
        {
            Object objetSommetDeGraphe = pileDesObjets.peek();

            log.debug("lecture pile ok - objet au sommet de graphe de type :"
                + objetSommetDeGraphe.getClass().getName());

            Field[] listeDesAttributsDeLObjetSommetDeGraphe = objetSommetDeGraphe.getClass().getFields();
            for (int i = 0; i < listeDesAttributsDeLObjetSommetDeGraphe.length; i++)
            {
                classeDeLAttributDeLObjetSommetDeGraphe = listeDesAttributsDeLObjetSommetDeGraphe[i].getType();
                log.debug("lecture du type d'attributs de l'objet sommet de graphe : "
                    + classeDeLAttributDeLObjetSommetDeGraphe.getName());
                if (classeDeLAttributDeLObjetSommetDeGraphe.getName().equalsIgnoreCase(
                    classeUtiliseeCommeTypeDesAttributsListes))
                {
                    log.debug("tentative de rattachement de l'objet base de graphe");

                    try
                    {
                        methodeAjout =
                            classeDeLAttributDeLObjetSommetDeGraphe.getMethod(
                                methodeUtiliseePourAjouterDansLesAttributsListes, new Class[] {Object.class});
                    }

                    catch (NoSuchMethodException e)
                    {
                        log.warn("Erreur : ", e);
                        throw new ProgrammationException("La méthode "
                            + methodeUtiliseePourAjouterDansLesAttributsListes + " n'est pas définie", e);
                    }
                    catch (RuntimeException e)
                    {
                        throw new SAXException("Erreur lors de la recherche de la méthode d'ajout "
                            + methodeUtiliseePourAjouterDansLesAttributsListes + " pour l'attribut "
                            + listeDesAttributsDeLObjetSommetDeGraphe[i].getName(), e);
                    }
                    try
                    {
                        objetAttributListe = listeDesAttributsDeLObjetSommetDeGraphe[i].get(objetSommetDeGraphe);
                    }
                    catch (RuntimeException e)
                    {
                        throw new SAXException("Erreur lors de la recherche de l'instance de l'attribut "
                            + listeDesAttributsDeLObjetSommetDeGraphe[i].getName(), e);
                    }
                    catch (IllegalAccessException e)
                    {
                        log.warn("Accès illégal lors de la recherche de la liste des attributs ", e);
                        throw new ProgrammationException("Accès illégal  ", e);
                    }

                    objetsEnParametresDeLaMethodeAjout[0] = objetARattacherAuSommetDeGraphe;
                    try
                    {
                        methodeAjout.invoke(objetAttributListe, objetsEnParametresDeLaMethodeAjout);
                    }
                    catch (RuntimeException e)
                    {
                        throw new SAXException("Erreur lors de l'invocation de la méthode " + methodeAjout.getName()
                            + "Sur l'attribut " + listeDesAttributsDeLObjetSommetDeGraphe[i].getName(), e);
                    }
                    catch (IllegalAccessException e)
                    {
                        log.warn("Accès illégal lors de l'exécution de la méthode " + methodeAjout.getName(), e);
                        throw new ProgrammationException("Accès illégal  ", e);
                    }
                    catch (InvocationTargetException e)
                    {
                        log.warn("Erreur lors de l'exécution de la méthode " + methodeAjout.getName());
                        throw new ProgrammationException("Erreur d'exécution de la méthode " + methodeAjout.getName(),
                            e);
                    }
                    log.debug("succès rattachement de l'objet base de graphe");
                }
            }
        }
        else
        {
            log.debug("plus d'objets dans la pile, l'objet à rattacher est l'objet résultat");
            objetResultat = objetARattacherAuSommetDeGraphe;
        }
    }

    /**
     * Accesseur de l attribut objet resultat.
     * 
     * @return objet resultat
     */
    public Object getObjetResultat()
    {
        return objetResultat;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param uri
     * @param localName
     * @param qName
     * @param attributes
     * @throws SAXException the SAX exception
     * @see org.xml.sax.helpers.DefaultHandler#startElement(java.lang.String, java.lang.String, java.lang.String,
     *      org.xml.sax.Attributes)
     */
    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException
    {

        log.debug("debut d'element détectée : " + qName);

        /* création d'une classe de nom équivalent au nom de l'élément trouvé */

        String nomDeClasse = this.getprefixepackage().concat(qName);
        Class<?> classe = null;

        log.debug("tentative de recherche de la classe :" + nomDeClasse);
        try
        {
            classe = ClassUtils.forName(nomDeClasse, null);
        }
        catch (ClassNotFoundException e)
        {
            throw new SAXException("La classe " + nomDeClasse + "n'a pas été trouvée", e);
        }

        log.debug("recherche d'un constructeur sans paramètres :" + nomDeClasse);
        Object objet = null;
        try
        {
            Constructor cons = classe.getConstructor(new Class[] {});
            objet = cons.newInstance(new Object[] {});
        }
        catch (NoSuchMethodException e)
        {
            log.warn(e);
            throw new SAXException("Le constructeur sans arguments sur " + nomDeClasse + "n'a pas été trouvée", e);
        }
        catch (InstantiationException e)
        {
            log.warn(e);
            throw new SAXException("Erreur lors de l'instanciation de la classe : " + nomDeClasse, e);
        }
        catch (IllegalAccessException e)
        {
            log.warn(e);
            throw new SAXException("Accès illégal lors de l'instanciation de la classe : " + nomDeClasse, e);
        }
        catch (InvocationTargetException e)
        {
            log.warn(e);
            throw new SAXException("Erreur lors de l'exécution du constructeur de la classe : " + nomDeClasse, e);
        }
        catch (RuntimeException e)
        {
            log.warn(e);
            throw new SAXException("Impossible de creér une instance de  " + nomDeClasse, e);
        }

        /* mapping des attributs de noms équivalent (nécessite des classes publiques) */
        // modifier pour passer par les "setters"
        Field[] listeAttributs = classe.getFields();
        for (int i = 0; i < listeAttributs.length; i++)
        {
            for (int j = 0; j < attributes.getLength(); j++)
            {
                if (listeAttributs[i].getName().equalsIgnoreCase(attributes.getQName(j)))
                {
                    try
                    {
                        listeAttributs[i].set(objet, attributes.getValue(j));
                    }
                    catch (IllegalAccessException e)
                    {
                        log.warn(e);
                        throw new SAXException("Accès illégal lors de l'appel d'une méthode. ", e);
                    }
                    catch (RuntimeException e)
                    {
                        throw ApplicationExceptionTransformateur.transformer(e);
                    }
                }
            }
        }

        this.pileDesObjets.push(objet);
    }

    /**
     * Accesseur de l attribut prefixepackage.
     * 
     * @return prefixepackage
     */
    private String getprefixepackage()
    {
        return nomPackageDesObjetsJava.concat(".");
    }

}
