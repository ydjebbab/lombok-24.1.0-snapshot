/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : LdapAuthentificationSecurityLombok.java
 *
 */
package fr.gouv.finances.lombok.securite.springsecurity;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;

import fr.gouv.finances.lombok.securite.techbean.AuthentificationRegleGestionException;
import fr.gouv.finances.lombok.securite.techbean.HabilitationRegleGestionException;
import fr.gouv.finances.lombok.securite.techbean.PersonneAnnuaire;

/**
 * <pre>
 *  
 * Cette classe fait appel à authentificationEtLectureHabilitationsService qui va s'authentifier dans le LDAP 
 * et récupère les profils utilisateurs ainsi que toutes les informations sur l'utilisateur trouvées dans l'annuaire. 
 * Puis à partir de ces résultats, elle construit un objet UtilisateurSecurityLombok implémentant l'interface userdetails 
 * attendue par Spring security.
 * </pre>
 * 
 * @author chouard-cp
 * @version $Revision: 1.4 $ Date: 15 déc. 2009
 */
public class LdapAuthentificationSecurityLombok extends AbstractAuthentificationSecurityLombok
{
    private static final Logger log = LoggerFactory.getLogger(LdapAuthentificationSecurityLombok.class);

    public LdapAuthentificationSecurityLombok()
    {
        super();
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param username "documenté"
     * @param auth "documenté"
     * @return user details
     * @throws AuthenticationException the authentication exception
     * @see org.springframework.security.providers.dao.AbstractUserDetailsAuthenticationProvider#retrieveUser(java.lang.String,
     *      org.springframework.security.providers.UsernamePasswordAuthenticationToken)
     */
    @Override
    public UserDetails retrieveUser(String username, UsernamePasswordAuthenticationToken auth)
        throws AuthenticationException
    {
        log.debug(">>> Debut methode retrieveUser()");

        String password = (String) auth.getCredentials();

        PersonneAnnuaire unePersonneAnnuaire;
        try
        {
            if (log.isDebugEnabled())
            {
                log.debug("entree retrieveUser ");
            }
            // verifier le mode d'accès à l'appli
            verifierModeAuthentificationParLApplication();
            // verifier messages srm (pas de bloquant)
            verifierPasDeMessageBloquantSRM();
            // recherche personne avec habilitations après authentification
            unePersonneAnnuaire =
                authentificationEtLectureHabilitationsService.authentifierEtRechercherHabilitation(username, password,
                    codeApplicationDansHabilitations);

            /*
             * on enleve les profils bloqués de sireme de la personne +exception si tous les profils sont bloqués
             */

            boolean desProfilsOntEteFiltres = filtrerProfilsBloquesParSireme(unePersonneAnnuaire);

            if (unePersonneAnnuaire.getListeHabilitations() == null
                || unePersonneAnnuaire.getListeHabilitations().isEmpty())
            {
                jeterExceptionPasDHabilitation(desProfilsOntEteFiltres);
            }

            appelerIntercepteurAvantHabilitation(unePersonneAnnuaire);

            String urlAppli = UtilisateurSecurityLombokContainer.getUrl();
            // construction utilisateur
            return this.construitUtilisateurSecurityLombok(unePersonneAnnuaire, username, password, urlAppli);

        }

        /*
         * Exception Coca : Il ne faut pas ajouter l'exception e en paramètre du constructeur de l'exception
         * BadCredentialsException dans les 3 blocs catch suivants car le texte remonté à l'utilisateur affiche une
         * information de type "Nested exception..." qui nuit à la clarté du message
         */
        catch (AuthentificationRegleGestionException | HabilitationRegleGestionException exception)
        {
            throw new BadCredentialsException(exception.getMessage(), exception);
        }
        catch (RuntimeException exception)
        {
            log.error("Erreur imprevue lors de l'authentification ", exception);
            throw new BadCredentialsException(exception.getMessage(), exception);
        }

    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param udd "documenté"
     * @param upat "documenté"
     * @throws AuthenticationException the authentication exception
     * @see org.springframework.security.providers.dao.AbstractUserDetailsAuthenticationProvider#additionalAuthenticationChecks(org.springframework.security.userdetails.UserDetails,
     *      org.springframework.security.providers.UsernamePasswordAuthenticationToken)
     */
    @Override
    protected void additionalAuthenticationChecks(UserDetails udd, UsernamePasswordAuthenticationToken upat)
        throws AuthenticationException
    {
        log.debug(">>> Debut methode additionalAuthenticationChecks()");
        if (!udd.getPassword().equals(upat.getCredentials()))
        {
            throw new BadCredentialsException("Erreur sur le controle additionnel du password");
        }
    }

}
