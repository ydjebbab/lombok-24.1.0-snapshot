/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 */
package fr.gouv.finances.lombok.test.pojo;

import java.lang.reflect.Field;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.gouv.finances.lombok.util.exception.IngerableException;

/**
 * Utilitaire d'introspection.
 *
 * @author Christophe Breheret-Girardin
 */
public final class ReflectionUtils
{
    /** Initialisation de la journalisation. */
    private static final Logger LOGGER = LoggerFactory.getLogger(ReflectionUtils.class);

    /**
     * Constructeur.
     */
    private ReflectionUtils()
    {
        super();
    }

    /**
     * Méthode permettant de récupérer une valeur d'un attribut d'un objet.
     *
     * @param T type du champs à récupérer
     * @param objet objet dont on veut récupérer la valeur du champs
     * @param nomChamps nom du champs
     * @param typeChamps type du champs à récupérer
     * @return la valeur du champs
     */
    @SuppressWarnings("unchecked")
    public static <T> T getValeur(Object objet, String nomChamps, Class<T> typeChamps)
    {
        try
        {
            LOGGER.debug("nomChamps : {}", nomChamps);
            LOGGER.debug("typeChamps : {}", typeChamps);

            // Récupération du champs
            Field field = objet.getClass().getDeclaredField(nomChamps);
            // Les champs private final ne sont pas accessibles par défaut
            field.setAccessible(true);
            // Récupération de la valeur du champs
            return (T) field.get(objet);
        }
        catch (NoSuchFieldException | IllegalAccessException exception)
        {
            // Gestion des erreurs
            LOGGER.debug("champs '{}' non trouvé ou non accessible : {}", nomChamps, exception.getMessage()
                , exception);
            throw new IngerableException(exception);
        }
    }
}
