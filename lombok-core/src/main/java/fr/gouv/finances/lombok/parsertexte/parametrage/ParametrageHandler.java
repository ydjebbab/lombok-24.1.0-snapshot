/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : ParametrageHandler.java
 *
 */
package fr.gouv.finances.lombok.parsertexte.parametrage;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import fr.gouv.finances.lombok.parsertexte.integration.OperationPropriete;

/**
 * Class ParametrageHandler.
 * 
 * @author amleplatinec
 * @version $Revision: 1.4 $ Date: 14 déc. 2009
 */
public class ParametrageHandler extends DefaultHandler
{

    /** fichier param fini. */
    FichierParametrage fichierParamFini = null;

    /** fichier param. */
    FichierParametrage fichierParam = null;

    /** buffer. */
    StringBuilder   buffer = null;

    /** ligne en cours. */
    LigneFichier ligneEnCours = null;

    /** discriminant en cours. */
    DiscriminantLigne discriminantEnCours = null;

    /** critere en cours. */
    CritereDiscriminant critereEnCours = null;

    /** propriete en cours. */
    ProprieteLigne proprieteEnCours = null;

    /** mapping en cours. */
    MappingLigneClasse mappingEnCours = null;

    /** custom editor en cours. */
    DefinitionCustomPropertyEditor customEditorEnCours = null;

    /**
     * Instanciation de parametrage handler.
     */
    public ParametrageHandler()
    {
        super();
    }

    // détection de caractères
    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param cha
     * @param start
     * @param length
     * @throws SAXException the SAX exception
     * @see org.xml.sax.helpers.DefaultHandler#characters(char[], int, int)
     */
    public void characters(char[] cha, int start, int length) throws SAXException
    {
        String lecture = new String(cha, start, length);
        if (buffer != null)
        {
            buffer.append(lecture);
        }
    }

    // fin du parsing
    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @throws SAXException the SAX exception
     * @see org.xml.sax.helpers.DefaultHandler#endDocument()
     */
    public void endDocument() throws SAXException
    {

    }

    /*
     * (non-Javadoc)
     * @see org.xml.sax.ContentHandler#endElement(java.lang.String, java.lang.String, java.lang.String)
     */
    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param uri
     * @param localName
     * @param qName
     * @throws SAXException the SAX exception
     * @see org.xml.sax.helpers.DefaultHandler#endElement(java.lang.String, java.lang.String, java.lang.String)
     */
    public void endElement(String uri, String localName, String qName) throws SAXException
    {

        if ("fichier".equalsIgnoreCase(qName))
        {
            String messageErreur = this.fichierParam.controleIntegriteParametrage();
            if (messageErreur != null)
            {
                throw new SAXException("Erreur de controle du fichier : '" + messageErreur + "'");
            }
            this.fichierParamFini = this.fichierParam;

        }
        else if ("type".equalsIgnoreCase(qName))
        {
            String type = this.buffer.toString();
            int tmp = -1;

            if ("positionnel".equalsIgnoreCase(type))
            {
                tmp = FichierParametrage.TYPE_FICHIER_POSITION_FIXE;
            }
            else if ("separateur".equalsIgnoreCase(type))
            {
                tmp = FichierParametrage.TYPE_FICHIER_SEPARATEUR;
            }

            else
            {
                throw new SAXException("Balise <type> la valeur doit être 'positionnel' ou 'separateur'.");
            }
            this.fichierParam.setType(Integer.valueOf(tmp));
        }
        else if ("separateur".equalsIgnoreCase(qName))
        {
            String separateur = this.buffer.toString();
            if (separateur.length() == 1)
            {
                this.fichierParam.setCaractereSeparation(separateur);
            }
            else
            {
                throw new SAXException(
                    "Balise <separateur> le caractère de séparation doit faire un caractère obligatoirement.");
            }
        }

        else if ("gestionguillemets".equalsIgnoreCase(qName))
        {
            String gestionguillemets = this.buffer.toString();
            if (gestionguillemets.length() > 0)
            {
                this.fichierParam.setGestionGuillemets(Boolean.valueOf(gestionguillemets));
            }
            else
            { // par défaut on le met à true
                this.fichierParam.setGestionGuillemets(true);
            }
        }
        else if ("ligne".equalsIgnoreCase(qName))
        {
            this.fichierParam.ajouterLigne(this.ligneEnCours);
            this.ligneEnCours = null;
        }
        else if ("nombrelignes".equalsIgnoreCase(qName))
        {
            String nombreLigne = this.buffer.toString();
            int tmp = -1;

            try
            {
                tmp = Integer.parseInt(nombreLigne);
            }
            catch (NumberFormatException exc)
            {
                throw new SAXException("Balise <nombrelignes>:erreur lors de la conversion de la valeur en int.", exc);
            }
            this.ligneEnCours.setNombreDeLignes(Integer.valueOf(tmp));
        }

        else if ("nombrelignesenteteaignorer".equalsIgnoreCase(qName))
        {
            String nombreLignesEntete = this.buffer.toString();
            int tmp = -1;

            try
            {
                tmp = Integer.parseInt(nombreLignesEntete);
            }
            catch (NumberFormatException exc)
            {
                throw new SAXException("Balise <nombreLignesEntete>:erreur lors de la conversion de la valeur en int.", exc);
            }
            this.fichierParam.setNbLignesEntete(Integer.valueOf(tmp));
        }

        endElement(qName);
    }

    // début du parsing
    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @throws SAXException the SAX exception
     * @see org.xml.sax.helpers.DefaultHandler#startDocument()
     */
    public void startDocument() throws SAXException
    {

        // Méthode vide

    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param uri
     * @param localName
     * @param qName
     * @param attributes
     * @throws SAXException the SAX exception
     * @see org.xml.sax.helpers.DefaultHandler#startElement(java.lang.String, java.lang.String, java.lang.String,
     *      org.xml.sax.Attributes)
     */
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException
    {
        if ("fichier".equalsIgnoreCase(qName))
        {
            this.fichierParam = new FichierParametrage();
            String version = attributes.getValue("version");
            if (version != null)
            {
                this.fichierParam.setVersion(version);
            }
        }
        else if ("type".equalsIgnoreCase(qName) || "separateur".equalsIgnoreCase(qName) || "nombrelignes".equalsIgnoreCase(qName)
            || "gestionguillemets".equalsIgnoreCase(qName) || "nombrelignesenteteaignorer".equalsIgnoreCase(qName))
        {
            this.buffer = new StringBuilder();
        }
        /*
         * GESTION DE LA PARTIE DESCRIPTION DES LIGNES DU FICHIER XML
         */
        else if ("ligne".equalsIgnoreCase(qName))
        {
            String idLigne = attributes.getValue("id");
            if ((idLigne != null) && (!"".equals(idLigne)))
            {
                if (this.ligneEnCours == null)
                {
                    this.ligneEnCours = new LigneFichier();
                    this.ligneEnCours.setId(idLigne);
                }
                else
                {
                    throw new SAXException("Ouverture d une balise <ligne> alors que la précédente balise "
                        + "<ligne> ouverte n est pas fermee.");
                }
            }
            else
            {
                throw new SAXException("Balise ligne dans attribut id. Cet attribut est obligatoire");
            }
        }
        else if ("discriminant".equalsIgnoreCase(qName))
        {
            if (this.discriminantEnCours == null)
            {
                this.discriminantEnCours = new DiscriminantLigne();
            }
            else
            {
                throw new SAXException(
                    "Ouverture d une balise <discriminant> alors que la précédente balise <discriminant> "
                        + "ouverte n est pas fermee.");
            }
        }
        else if ("format".equalsIgnoreCase(qName))
        {
            if (this.ligneEnCours != null)
            {
                String avancement = "";
                try
                {

                    String nbElements = attributes.getValue("nbelements");
                    boolean blnnbElements = ((nbElements != null) && (!"".equals(nbElements)));

                    avancement = "nbElements";
                    if (blnnbElements)
                    {
                        int tmp = Integer.parseInt(nbElements);
                        this.ligneEnCours.setNbElements(tmp);
                    }

                    String nbElementsMini = attributes.getValue("nbelementsmini");
                    boolean blnnbElementsMini = ((nbElementsMini != null) && (!"".equals(nbElementsMini)));

                    avancement = "nbelementsmini";
                    if (blnnbElementsMini)
                    {
                        int tmp = Integer.parseInt(nbElementsMini);
                        this.ligneEnCours.setNbElementsMini(tmp);
                    }

                    String nbElementsMaxi = attributes.getValue("nbelementsmaxi");
                    boolean blnnbElementsMaxi = ((nbElementsMaxi != null) && (!"".equals(nbElementsMaxi)));

                    avancement = "nbElementsMaxi";
                    if (blnnbElementsMaxi)
                    {
                        int tmp = Integer.parseInt(nbElementsMaxi);
                        this.ligneEnCours.setNbElementsMaxi(tmp);
                    }

                    boolean blnNbElementsServis = blnnbElements || blnnbElementsMini || blnnbElementsMaxi;

                    String taille = attributes.getValue("taille");
                    boolean blntaille = ((taille != null) && (!"".equals(taille)));

                    avancement = "taille";
                    if (blntaille)
                    {
                        int tmp = Integer.parseInt(taille);
                        this.ligneEnCours.setTaille(tmp);
                    }

                    String tailleMini = attributes.getValue("taillemini");
                    boolean blntailleMini = ((tailleMini != null) && (!"".equals(tailleMini)));

                    avancement = "tailleMini";
                    if (blntailleMini)
                    {
                        int tmp = Integer.parseInt(tailleMini);
                        this.ligneEnCours.setTailleMini(tmp);
                    }

                    String tailleMaxi = attributes.getValue("taillemaxi");
                    boolean blntailleMaxi = ((tailleMaxi != null) && (!"".equals(tailleMaxi)));

                    avancement = "tailleMaxi";
                    if (blntailleMaxi)
                    {
                        int tmp = Integer.parseInt(tailleMaxi);
                        this.ligneEnCours.setTailleMaxi(tmp);
                    }

                    boolean blntaillesServie = blntaille || blntailleMini || blntailleMaxi;

                    // Contrôles de cohérence sur la saisie des paramètres
                    if (blnNbElementsServis && blntaillesServie)
                    {
                        throw new SAXException(
                            "balise <format> les proprietes nbelements,nbelementsmini et nbelementsmaxi  ne sont pas "
                                + "compatibles avec les proprietes taille,taillemini et taillemaxi.");
                    }
                    else if (blnnbElements && (blnnbElementsMini || blnnbElementsMaxi))
                    {
                        throw new SAXException(
                            "balise <format> la propriete nbelements n'est pas compatibles avec les proprietes "
                                + "nbelementsmini et nbelementsmaxi.");
                    }
                    else if (blntaille && (blntailleMini || blntailleMaxi))
                    {
                        throw new SAXException(
                            "balise <format> la propriete taille n'est pas compatibles avec les proprietes taillemini"
                                + " et taillemaxi.");
                    }
                    else if (blnNbElementsServis
                        && (this.fichierParam.getType().intValue() == FichierParametrage.TYPE_FICHIER_POSITION_FIXE))
                    {
                        throw new SAXException(
                            "balise <format> les proprietes de type nbElements ne sont utilisables qu'avec les fichiers "
                                + "de type séparateur.");
                    }
                    else if (blntaillesServie
                        && (this.fichierParam.getType().intValue() == FichierParametrage.TYPE_FICHIER_SEPARATEUR))
                    {
                        throw new SAXException(
                            "balise <format> les proprietes de type nbElements ne sont utilisables qu'avec les fichiers"
                                + " de type taille fixe.");
                    }
                }
                catch (NumberFormatException e)
                {
                    throw new SAXException("Erreur durant la conversion en int de la propriété '" + avancement
                        + "' de la balise", e);
                }
            }
            else
            {
                throw new SAXException("Ouverture d une balise <format> alors qu'aucune balise <ligne> n'est ouverte.");
            }
        }

        else if ("critere".equalsIgnoreCase(qName))
        {
            String operateur = attributes.getValue("operateur");

            if (this.critereEnCours == null)
            {
                this.critereEnCours = new CritereDiscriminant();
                if ((operateur != null) && (!"".equals(operateur)))
                {
                    // Décryptage de l'opérateur de comparaison sélectionné
                    if ("=".equals(operateur))
                    {
                        critereEnCours.setOperateur(CritereDiscriminant.CRITERE_EGAL);
                    }
                    else if ("!=".equals(operateur))
                    {
                        critereEnCours.setOperateur(CritereDiscriminant.CRITERE_DIFFERENT);
                    }
                    else if ("=>".equals(operateur) || ">=".equals(operateur))
                    {
                        critereEnCours.setOperateur(CritereDiscriminant.CRITERE_EGAL_OU_SUPERIEUR);
                    }
                    else if ("=<".equals(operateur) || "<=".equals(operateur))
                    {
                        critereEnCours.setOperateur(CritereDiscriminant.CRITERE_EGAL_OU_INFERIEUR);
                    }
                    else if (">".equals(operateur) || ">".equals(operateur))
                    {
                        critereEnCours.setOperateur(CritereDiscriminant.CRITERE_SUPERIEUR);
                    }
                    else if ("<".equals(operateur) || "<".equals(operateur))
                    {
                        critereEnCours.setOperateur(CritereDiscriminant.CRITERE_INFERIEUR);
                    }
                    else if ("commencepar".equalsIgnoreCase(operateur))
                    {
                        critereEnCours.setOperateur(CritereDiscriminant.CRITERE_COMMENCE_PAR);
                    }
                    else if ("necommencepaspar".equalsIgnoreCase(operateur))
                    {
                        critereEnCours.setOperateur(CritereDiscriminant.CRITERE_NE_COMMENCE_PAS_PAR);
                    }
                    else if ("estnumerique".equalsIgnoreCase(operateur))
                    {
                        critereEnCours.setOperateur(CritereDiscriminant.CRITERE_EST_NUMERIQUE);
                    }
                    else if ("estalphabetique".equalsIgnoreCase(operateur))
                    {
                        critereEnCours.setOperateur(CritereDiscriminant.CRITERE_EST_ALPHABETIQUE);
                    }
                    else if ("estalphanumerique".equalsIgnoreCase(operateur))
                    {
                        critereEnCours.setOperateur(CritereDiscriminant.CRITERE_EST_ALPHANUMERIQUE);
                    }
                    else if ("tailleegale".equalsIgnoreCase(operateur))
                    {
                        critereEnCours.setOperateur(CritereDiscriminant.CRITERE_TAILLE_EGALE);
                    }
                    else if ("tailleminimum".equalsIgnoreCase(operateur))
                    {
                        critereEnCours.setOperateur(CritereDiscriminant.CRITERE_TAILLE_MINIMUM);
                    }
                    else if ("taillemaximum".equalsIgnoreCase(operateur))
                    {
                        critereEnCours.setOperateur(CritereDiscriminant.CRITERE_TAILLE_MAXIMUM);
                    }

                }
            }
            else
            {
                throw new SAXException("Ouverture d une balise <critere> alors que la précédente balise <critere>"
                    + " ouverte n est pas fermee.");
            }
        }
        else if ("positioncritere".equalsIgnoreCase(qName))
        {
            if (this.critereEnCours != null)
            {
                String positionDebut = attributes.getValue("positiondebut");
                String positionFin = attributes.getValue("positionfin");
                String position = attributes.getValue("position");
                String numeroLigne = attributes.getValue("numeroligne");

                if ((positionDebut != null) && (positionFin == null))
                {
                    throw new SAXException(
                        "Propriété positiondebut de la balise <position> valorisée alors que la propriété"
                            + " positionfin ne l'est pas.");
                }
                else if ((positionDebut == null) && (positionFin != null))
                {
                    throw new SAXException(
                        "Propriété positionfin de la balise <position> valorisée alors que la propriété"
                            + " positiondebut ne l'est pas.");
                }
                if ((position != null) && ((positionDebut != null) || (positionFin != null)))
                {
                    throw new SAXException(
                        "Propriété position de la balise <position> valorisée alors que la propriété"
                            + " positiondebut ou positionfin l'est (ces propriétés sont exclusives).");
                }
                else if (((numeroLigne == null) || ("".equals(numeroLigne)))
                    && ((position == null) || ("".equals(position)))
                    && ((positionDebut == null) || ("".equals(positionDebut)))
                    && ((positionFin == null) || ("".equals(positionFin))))
                {
                    throw new SAXException("Balise <position> : les propriétés positiondebut et positionfin, position"
                        + " ou numeroligne doivent etre valorisées.");
                }
                else if (((numeroLigne != null) && (!"".equals(numeroLigne)))
                    && (((position != null) && (!"".equals(position)))
                        || ((positionDebut != null) && (!"".equals(positionDebut)))
                        || ((positionFin != null) && (!"".equals(positionFin)))))
                {
                    throw new SAXException(
                        "Balise <position> : lorsque la propriété numeroligne est valorisée les propriétés "
                            + "positiondebut, positionfin et position ne peuvent l etre.");
                }
                else
                {
                    String avancement = "";
                    try
                    {
                        avancement = "position";
                        if (position != null)
                        {
                            int tmp = Integer.parseInt(position);
                            this.critereEnCours.setPosition(tmp);
                        }

                        avancement = "positiondebut";
                        if (positionDebut != null)
                        {
                            int tmp = Integer.parseInt(positionDebut);
                            this.critereEnCours.setPositionDebut(tmp);
                        }

                        avancement = "positionfin";
                        if (positionFin != null)
                        {
                            int tmp = Integer.parseInt(positionFin);
                            this.critereEnCours.setPositionFin(tmp);
                        }

                        avancement = "numeroligne";
                        if (numeroLigne != null)
                        {
                            int tmp = Integer.parseInt(numeroLigne);
                            this.critereEnCours.setNumeroLigne(tmp);
                        }

                    }
                    catch (NumberFormatException e)
                    {
                        throw new SAXException("Erreur durant la conversion en int de la propriété '" + avancement
                            + "' de la balise", e);
                    }
                }

            }
            else
            {
                throw new SAXException(
                    "Ouverture d une balise <positioncritere> alors qu'aucune balise <critere> n'est ouverte.");
            }
        }
        else if ("valeur".equalsIgnoreCase(qName))
        {
            if (this.critereEnCours != null)
            {
                this.buffer = new StringBuilder();
            }
            else
            {
                throw new SAXException(
                    "Ouverture d une balise <valeur> alors qu'aucune balise <critere> n'est ouverte.");
            }
        }
        else if ("propriete".equalsIgnoreCase(qName))
        {
            String idPropriete = attributes.getValue("id");
            String idOperation = attributes.getValue("operation");

            if ((idPropriete != null) && (!"".equals(idPropriete)))
            {
                if (this.proprieteEnCours == null)
                {
                    this.proprieteEnCours = new ProprieteLigne();
                    this.proprieteEnCours.setId(idPropriete);
                }
                else
                {
                    throw new SAXException(
                        "Ouverture d une balise <propriete> alors que la précédente balise <propriete>"
                            + " ouverte n est pas fermee.");
                }
            }
            else
            {
                throw new SAXException("Balise <propriete> sans attribut id. Cet attribut est obligatoire");
            }

            // Lecture du type d'opération à réaliser suite à la lecture.
            if ((idOperation != null) && (!"".equals(idOperation)))
            {
                if ("conversionformatnumeriqueanglais".equalsIgnoreCase(idOperation))
                {
                    this.proprieteEnCours
                        .setOperationARealiser(OperationPropriete.OPE_DEFORMATAGE_NUMERIQUE_FORMAT_ANGLAIS);
                }
                else if ("videsitoutazero".equalsIgnoreCase(idOperation))
                {
                    this.proprieteEnCours.setOperationARealiser(OperationPropriete.OPE_VIDE_SI_TOUT_ZERO);
                }
                else if ("videsitoutazeroouespace".equalsIgnoreCase(idOperation))
                {
                    this.proprieteEnCours.setOperationARealiser(OperationPropriete.OPE_VIDE_SI_TOUT_ZERO_OU_ESPACE);
                }
                else if ("insertionseparateurdecimalpos1".equalsIgnoreCase(idOperation))
                {
                    this.proprieteEnCours
                        .setOperationARealiser(OperationPropriete.OPE_INSERTION_SEPARATEUR_DECIMAL_POSITION_1);
                }
                else if ("insertionseparateurdecimalpos2".equalsIgnoreCase(idOperation))
                {
                    this.proprieteEnCours
                        .setOperationARealiser(OperationPropriete.OPE_INSERTION_SEPARATEUR_DECIMAL_POSITION_2);
                }
                else if ("insertionseparateurdecimalpos3".equalsIgnoreCase(idOperation))
                {
                    this.proprieteEnCours
                        .setOperationARealiser(OperationPropriete.OPE_INSERTION_SEPARATEUR_DECIMAL_POSITION_3);
                }
                else if ("majuscule".equalsIgnoreCase(idOperation))
                {
                    this.proprieteEnCours.setOperationARealiser(OperationPropriete.OPE_MAJUSCULE);
                }
                else if ("minuscule".equalsIgnoreCase(idOperation))
                {
                    this.proprieteEnCours.setOperationARealiser(OperationPropriete.OPE_MINUSCULE);
                }
            }

        }
        else if ("positionpropriete".equalsIgnoreCase(qName))
        {
            if (this.proprieteEnCours != null)
            {
                String positionDebut = attributes.getValue("positiondebut");
                String positionFin = attributes.getValue("positionfin");
                String position = attributes.getValue("position");
                String ligne = attributes.getValue("ligne");

                if ((positionDebut != null) && (positionFin == null))
                {
                    throw new SAXException(
                        "Propriété positiondebut de la balise <positionpropriete> valorisée alors que la propriété"
                            + " positionfin ne l'est pas.");
                }
                else if ((positionDebut == null) && (positionFin != null))
                {
                    throw new SAXException(
                        "Propriété positionfin de la balise <positionpropriete> valorisée alors que la propriété"
                            + " positiondebut ne l'est pas.");
                }
                if ((position != null) && ((positionDebut != null) || (positionFin != null)))
                {
                    throw new SAXException(
                        "Propriété position de la balise <positionpropriete> valorisée alors que la propriété "
                            + "positiondebut ou positionfin l'est (ces propriétés sont exclusives).");
                }
                else
                {
                    String avancement = "";
                    try
                    {
                        avancement = "position";
                        if (position != null)
                        {
                            int tmp = Integer.parseInt(position);
                            this.proprieteEnCours.setPosition(tmp);
                        }

                        avancement = "positiondebut";
                        if (positionDebut != null)
                        {
                            int tmp = Integer.parseInt(positionDebut);
                            this.proprieteEnCours.setPositionDebut(tmp);
                        }

                        avancement = "positionfin";
                        if (positionFin != null)
                        {
                            int tmp = Integer.parseInt(positionFin);
                            this.proprieteEnCours.setPositionFin(tmp);
                        }

                        avancement = "ligne";
                        if (ligne != null)
                        {
                            int tmp = Integer.parseInt(ligne);
                            this.proprieteEnCours.setLigne(tmp);
                        }

                    }
                    catch (NumberFormatException e)
                    {
                        throw new SAXException("Erreur durant la conversion en int de la propriété '" + avancement
                            + "' de la balise", e);
                    }
                }

            }
            else
            {
                throw new SAXException(
                    "Ouverture d une balise <positionpropriete> alors qu'aucune balise <propriete> n'est ouverte.");
            }
        }
        /**
         * GESTION DE LA PARTIE MAPPING CLASSE - FICHIER
         */
        else if ("mappingligneclasse".equalsIgnoreCase(qName))
        {

            if (this.mappingEnCours == null)
            {
                String idLigne = attributes.getValue("idligne");
                if ((idLigne == null) || ("".equals(idLigne)))
                {
                    throw new SAXException(
                        "Balise <mappingligneclasse> sans attribut idligne. Cet attribut est obligatoire");
                }

                String affectation = attributes.getValue("affectation");
                Integer tmp = null;

                if ((affectation == null) || ("".equals(affectation)))
                {
                    throw new SAXException(
                        "Balise <mappingligneclasse> sans attribut affectation. Cet attribut est obligatoire");
                }
                // Contrôle du contenu de la propriété
                if ("setter".equalsIgnoreCase(affectation))
                {
                    tmp = Integer.valueOf(MappingLigneClasse.AFFECTATION_SETTER);
                }
                else if ("constructeur".equalsIgnoreCase(affectation))
                {
                    tmp = Integer.valueOf(MappingLigneClasse.AFFECTATION_CONTRUCTEUR);
                }

                // La conversion s'est elle bien passée?
                if (tmp == null)
                {
                    throw new SAXException(
                        "La valeur de l'attribut 'affectation' de la balise <mappingligneclasse> est incorrect. "
                            + "Il ne peut contenir que les valeur 'setter' ou 'constructeur'.");
                }

                if (this.mappingEnCours == null)
                {
                    this.mappingEnCours = new MappingLigneClasse(idLigne, tmp);
                    this.mappingEnCours.setIdLigne(idLigne);
                }
                else
                {
                    throw new SAXException(
                        "Ouverture d une balise <mappingligneclasse> alors que la précédente balise "
                            + "<mappingligneclasse> ouverte n est pas fermee.");
                }
            }
            else
            {
                throw new SAXException("Balise <mappingligneclasse> ouverte alors que la précédente n'est pas fermée");
            }
        }
        else if ("attributmapping".equalsIgnoreCase(qName))
        {
            if (this.mappingEnCours != null)
            {
                String propriete = attributes.getValue("propriete");
                String classe = attributes.getValue("classe");
                String attribut = attributes.getValue("attribut");
                String identifiantInstance = attributes.getValue("identifiantinstance");

                if ((propriete == null) || ("".equals(propriete)) || (classe == null) || ("".equals(classe))
                    || (attribut == null) || ("".equals(attribut)))
                {
                    throw new SAXException(
                        "Balise <mappingligneclasse> : les attributs 'propriete', 'classe', 'attribut' "
                            + "sont obligatoires");
                }
                AttributMapping attributClasse = new AttributMapping(propriete, classe, attribut, identifiantInstance);
                this.mappingEnCours.ajouterAttributs(attributClasse);
            }
            else
            {
                throw new SAXException(
                    "Balise <attributmapping> ouverte alors qu'aucune balise <mappingligneclasse> n'est ouverte.");
            }
        }

        else if ("editor".equalsIgnoreCase(qName))
        {
            if (this.customEditorEnCours == null)
            {
                String ide = attributes.getValue("id");
                String modeInstanciation = attributes.getValue("modeinstanciation");
                String type = attributes.getValue("type");

                if ((ide == null) || ("".equals(ide)) || (modeInstanciation == null) || ("".equals(modeInstanciation))
                    || (type == null) || ("".equals(type)))
                {
                    throw new SAXException(
                        "Balise <editor> : les attributs 'id', 'modeinstanciation', 'type' sont obligatoires");
                }

                int tmpModeInstanciation = -1;
                if ("standard".equalsIgnoreCase(modeInstanciation))
                {
                    tmpModeInstanciation = DefinitionCustomPropertyEditor.INSTANCIATION_STANDARD;
                }
                else if ("custom".equalsIgnoreCase(modeInstanciation))
                {
                    tmpModeInstanciation = DefinitionCustomPropertyEditor.INSTANCIATION_CUSTOM;
                }
                else
                {
                    throw new SAXException(
                        "Balise <editor> : La propriété 'modeinstanciation' ne doit posséder que les valeur"
                            + " 'standard' ou 'custom'.");
                }

                this.customEditorEnCours = new DefinitionCustomPropertyEditor(ide, type, tmpModeInstanciation);
            }
            else
            {
                throw new SAXException("Balise <editor> ouverte alors qu'une balise <editor> est deja ouverte.");
            }
        }
        else if ("classeeditor".equalsIgnoreCase(qName))
        {
            if (this.customEditorEnCours != null)
            {
                this.buffer = new StringBuilder();
            }
            else
            {
                throw new SAXException("Balise <classeeditor> ouverte alors qu'aucune balise <editor> n'est ouverte.");
            }
        }
        else if ("classeeditor".equalsIgnoreCase(qName))
        { // mon commentaire
        }
        else if ("custompropertyeditor".equalsIgnoreCase(qName))
        { // mon commentaire
        }
        else
        {
            throw new SAXException("Balise '" + qName + "' inconnue.");
        }
    }

    /**
     * Correction coca cyclomatique.
     * 
     * @param qName
     */
    private void endElement(String qName)
    {
        if ("valeur".equalsIgnoreCase(qName))
        {
            this.critereEnCours.setValeur(this.buffer.toString());
        }
        else if ("critere".equalsIgnoreCase(qName))
        {
            this.discriminantEnCours.ajouterCritere(this.critereEnCours);
            this.critereEnCours = null;
        }
        else if ("discriminant".equalsIgnoreCase(qName))
        {
            this.ligneEnCours.setDiscriminantLigne(this.discriminantEnCours);
            this.discriminantEnCours = null;
        }
        else if ("propriete".equalsIgnoreCase(qName))
        {
            this.ligneEnCours.ajouterPropriete(this.proprieteEnCours);
            this.proprieteEnCours = null;
        }
        else if ("ligne".equalsIgnoreCase(qName))
        {
            this.fichierParam.ajouterLigne(this.ligneEnCours);
            this.ligneEnCours = null;
        }
        else if ("mappingligneclasse".equalsIgnoreCase(qName))
        {
            this.fichierParam.ajouterMappingClasse(this.mappingEnCours);
            this.mappingEnCours = null;
        }
        else if ("classeeditor".equalsIgnoreCase(qName))
        {
            String classeEditor = this.buffer.toString();

            this.customEditorEnCours.setCheminCustomPropertyEditor(classeEditor);
        }
        else if ("editor".equalsIgnoreCase(qName))
        {
            this.fichierParam.ajouterCustomPropertyEditor(this.customEditorEnCours);
            this.customEditorEnCours = null;
        }
        else if ("custompropertyeditor".equalsIgnoreCase(qName))
        {
              //moncommentaire
        }
    }

}
