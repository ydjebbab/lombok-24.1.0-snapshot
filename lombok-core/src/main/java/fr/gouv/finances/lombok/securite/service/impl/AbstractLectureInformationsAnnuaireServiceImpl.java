/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : AbstractLectureInformationsAnnuaireServiceImpl.java
 *
 */
package fr.gouv.finances.lombok.securite.service.impl;

import java.util.Properties;

import javax.naming.Context;
import javax.naming.NamingException;
import javax.naming.directory.DirContext;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.gouv.finances.lombok.securite.service.AbstractLectureInformationsAnnuaireService;
import fr.gouv.finances.lombok.securite.service.RessourceAnnuaireService;
import fr.gouv.finances.lombok.securite.techbean.AnnuaireException;
import fr.gouv.finances.lombok.util.base.BaseServiceImpl;

/**
 * Class AbstractLectureInformationsAnnuaireServiceImpl --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.4 $ Date: 11 déc. 2009
 */
public abstract class AbstractLectureInformationsAnnuaireServiceImpl extends BaseServiceImpl implements
    AbstractLectureInformationsAnnuaireService
{
    protected static final Logger log = LoggerFactory.getLogger(AbstractLectureInformationsAnnuaireServiceImpl.class);

    /** Constant : PB_CONNEXION_ANNUAiRE_DN. */
    private static final String PBCONNEXIONANNUAIREDN = "Problème avec l'annuaire lors de l'ouverture de la connexion ";

    /**
     * Identifiant et mot de passe de connexion au LDAP pour l'application operations de recherches et de lecture
     * d'attribut dans l'annuaire ) * si l'identifiant est laissé a "", la connexion sera anonyme.
     */

    private String identifiantConnexionLdapApplication;

    /** mot de passe connexion ldap application. */
    private String motDePasseConnexionLdapApplication;

    /*
     * Type d'authentification de l'utilisateur ("simple" ou "strong") utilisé pour vérifier l'identifiant / mot de
     * passe sur l'annuaire
     */
    /** type authentification ldap. */
    private String typeAuthentificationLDAP = "simple";

    /** ressourceannuaireso. */
    private RessourceAnnuaireService ressourceannuaireso;

    /**
     * Constructeur de la classe AbstractLectureInformationsAnnuaireServiceImpl.java
     *
     */
    public AbstractLectureInformationsAnnuaireServiceImpl()
    {
        super(); 
        
    }

    /**
     * Accesseur de l attribut identifiant connexion ldap application.
     * 
     * @return identifiant connexion ldap application
     */
    public String getIdentifiantConnexionLdapApplication()
    {
        return identifiantConnexionLdapApplication;
    }

    /**
     * Accesseur de l attribut mot de passe connexion ldap application.
     * 
     * @return mot de passe connexion ldap application
     */
    public String getMotDePasseConnexionLdapApplication()
    {
        return motDePasseConnexionLdapApplication;
    }

    /**
     * Accesseur de l attribut ressourceannuaireso.
     * 
     * @return ressourceannuaireso
     */
    public RessourceAnnuaireService getRessourceannuaireso()
    {
        return ressourceannuaireso;
    }

    /*
     * (non-Javadoc)
     * @see
     * fr.gouv.finances.lombok.securite.service.impl.AbstractLectureInformationsAnnuaireService#getTypeAuthentificationLDAP
     * ()
     */
    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @return the type authentification ldap
     * @see fr.gouv.finances.lombok.securite.service.AbstractLectureInformationsAnnuaireService#getTypeAuthentificationLDAP()
     */
    @Override
    public String getTypeAuthentificationLDAP()
    {
        return typeAuthentificationLDAP;
    }

    /**
     * Modificateur de l attribut identifiant connexion ldap application.
     * 
     * @param identifiantConnexionLdapApplication le nouveau identifiant connexion ldap application
     */
    public void setIdentifiantConnexionLdapApplication(String identifiantConnexionLdapApplication)
    {
        this.identifiantConnexionLdapApplication = identifiantConnexionLdapApplication;
    }

    /**
     * Modificateur de l attribut mot de passe connexion ldap application.
     * 
     * @param motDePasseConnexionLdapApplication le nouveau mot de passe connexion ldap application
     */
    public void setMotDePasseConnexionLdapApplication(String motDePasseConnexionLdapApplication)
    {
        this.motDePasseConnexionLdapApplication = motDePasseConnexionLdapApplication;
    }

    /**
     * Modificateur de l attribut ressourceannuaireso.
     * 
     * @param ressourceannuaireso le nouveau ressourceannuaireso
     */
    public void setRessourceannuaireso(RessourceAnnuaireService ressourceannuaireso)
    {
        this.ressourceannuaireso = ressourceannuaireso;
    }

    /*
     * (non-Javadoc)
     * @see
     * fr.gouv.finances.lombok.securite.service.impl.AbstractLectureInformationsAnnuaireService#setTypeAuthentificationLDAP
     * (java.lang.String)
     */
    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param typeAuthentificationLDAP the new type authentification ldap
     * @see fr.gouv.finances.lombok.securite.service.AbstractLectureInformationsAnnuaireService#setTypeAuthentificationLDAP(java.lang.String)
     */
    @Override
    public void setTypeAuthentificationLDAP(String typeAuthentificationLDAP)
    {
        this.typeAuthentificationLDAP = typeAuthentificationLDAP;
    }

    /**
     * fermeture connexion avec ldap.
     * 
     * @param unApplicationDirContext --
     */
    protected void fermerLDAPConnexionApplication(DirContext unApplicationDirContext)
    {
        ressourceannuaireso.fermerLDAPConnexion(unApplicationDirContext);
    }

    /**
     * ouverture connexion avec ldap.
     * 
     * @return the dir context
     */
    protected DirContext ouvreLDAPConnexionApplication()
    {
        log.debug(">>> Debut methode ouvreLDAPConnexionApplication");
        DirContext unApplicationDirContext = null;
        Properties props;

        props = ressourceannuaireso.fournirUnSqueletteDEnvironnement();

        /** connexion anonyme */
        if (StringUtils.isEmpty(this.identifiantConnexionLdapApplication))
        {
            log.debug("Ouverture de la connexion applicative en anonyme");
            props.put(Context.SECURITY_AUTHENTICATION, "none");
        }
        /** connexion avec uid de l'application */
        else
        {
            log.debug("Ouverture de la connexion applicative avec " + identifiantConnexionLdapApplication);
            props.put(Context.SECURITY_AUTHENTICATION, typeAuthentificationLDAP);
            props.put(Context.SECURITY_PRINCIPAL, identifiantConnexionLdapApplication);
            props.put(Context.SECURITY_CREDENTIALS, motDePasseConnexionLdapApplication);
        }

        try
        {
            unApplicationDirContext = ressourceannuaireso.ouvreLdapConnexion(props);
        }
        catch (NamingException exception)
        {
            throw new AnnuaireException(PBCONNEXIONANNUAIREDN, exception);
        }

        return unApplicationDirContext;
    }

}
