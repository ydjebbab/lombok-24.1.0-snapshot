/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : LecturePersonneService.java
 *
 */
package fr.gouv.finances.lombok.securite.service;

import java.util.List;

import javax.naming.directory.DirContext;

import fr.gouv.finances.lombok.securite.techbean.PersonneAnnuaire;

/**
 * Interface LecturePersonneService --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.3 $ Date: 11 déc. 2009
 */
public interface LecturePersonneService extends AbstractLectureInformationsAnnuaireService
{

    /**
     * retourne une liste de PersonneAnnuaire répondant aux critères passés.
     * 
     * @param filtreRecherche filtre de recherche en syntaxe ldap
     * @param unApplicationDirContext un context annuaire ouvert
     * @return une liste de PersonneAnuuaire, vide si les criteres ne correspondent à aucune personne ou jette une
     *         AnnuaireException
     */
    List<PersonneAnnuaire> rechercherDesPersonnesAnnuaireAvecFiltreComplexe(String filtreRecherche,
        DirContext unApplicationDirContext);

    /**
     * retourne une liste de PersonneAnnuaire répondant aux critères passés.
     * 
     * @param filtreRecherche filtre de recherche en syntaxe ldap
     * @param unApplicationDirContext un contexte annuaire ouvert
     * @param maxReponses le nombre max de réponses que doit ramener la requète
     * @return une liste de PersonneAnuuaire, vide si les criteres ne correspondent à aucune personne ou jette une
     *         AnnuaireException
     */
    List<PersonneAnnuaire> rechercherDesPersonnesAnnuaireAvecFiltreComplexe(String filtreRecherche,
        DirContext unApplicationDirContext, Long maxReponses);

    /**
     * retourne une liste de PersonneAnnuaire répondant aux critères passés.
     * 
     * @param filtreRecherche filtre de recherche en syntaxe ldap
     * @param unApplicationDirContext un contexte annuaire ouvert
     * @param listeAttributsAnnuaireALire une liste d'attributs à lire
     * @return une liste de PersonneAnuuaire, vide si les criteres ne correspondent à aucune personne ou jette une
     *         AnnuaireException
     */
    List<PersonneAnnuaire> rechercherDesPersonnesAnnuaireAvecFiltreComplexe(String filtreRecherche,
        DirContext unApplicationDirContext, String[] listeAttributsAnnuaireALire);

    /**
     * retourne une liste de PersonneAnnuaire répondant aux critères passés.
     * 
     * @param filtreRecherche filtre de recherche en syntaxe ldap
     * @param unApplicationDirContext un contexte annuaire ouvert
     * @param listeAttributsAnnuaireALire une liste d'attributs à lire
     * @param maxReponses le nombre max de réponses que doit ramener la requète
     * @return une liste de PersonneAnuuaire, vide si les criteres ne correspondent à aucune personne ou jette une
     *         AnnuaireException
     */
    List<PersonneAnnuaire> rechercherDesPersonnesAnnuaireAvecFiltreComplexe(String filtreRecherche,
        DirContext unApplicationDirContext, String[] listeAttributsAnnuaireALire, Long maxReponses);

    /**
     * Recherche du DN complet de l'utilisateur pour pouvoir tenter une connexion ceci permet de pouvoir trouver des uid
     * même s'ils ne se trouvent pas dans la branche personne de l'annuaire (cas des prestataires et des partenaires
     * externes par exemple.
     * 
     * @param uid uid de la personne dans l'annuaire (type jmartin-cp)
     * @return une chaine de caractere donnant le dn complet de la personne ou null si l'uid n'a pas été trouvé ou
     *         AnnuaireException en cas d'erreur
     */
    abstract String rechercherDNCompletUtilisateurPourUnUID(String uid);

    /**
     * Recherche du DN complet de l'utilisateur pour pouvoir tenter une connexion ceci permet de pouvoir trouver des uid
     * même s'ils ne se trouvent pas dans la branche personne de l'annuaire (cas des prestataires et des partenaires
     * externes par exemple.
     * 
     * @param uid uid de la personne dans l'annuaire (type jmartin-cp)
     * @param unApplicationDirContext applicationContextExistant a des fins d'optimisation pour eviter de rouvrir un
     *        contexte pour cette requète
     * @return une chaine de caractere donnant le dn complet de la personne ou null si l'uid n'a pas été trouvé ou
     *         AnnuaireException en cas d'erreur
     */
    abstract String rechercherDNCompletUtilisateurPourUnUID(String uid, DirContext unApplicationDirContext);

    /**
     * dans le cas du nouvel annuaire fusionné , recherche à partir de l'uid fonctionnel prenom.nom
     * 
     * @param uidFonctionnel "documenté"
     * @return string
     */
    abstract String rechercherDNCompletUtilisateurPourUnUidFonctionnel(String uidFonctionnel);

    /**
     * retourne une liste de PersonneAnnuaire répondant aux critères passés.
     * 
     * @param application code aptera de l'application sur laquelle la personne doit être habilitée
     * @param prefixeCodique préfixe du codique des personnes à rechercher - à 3 chiffres, cela représente le numéro de
     *        département
     * @return une liste de PersonneAnuuaire, vide si les criteres ne correspondent à aucune personne ou jette une
     *         AnnuaireException
     */
    List<PersonneAnnuaire> rechercherPersonneAnnuaireAvecHabilitationSurApplicationAvecPrefixeCodique(
        String application, String prefixeCodique);

    /**
     * retourne une liste de PersonneAnnuaire répondant aux critères passés.
     * 
     * @param application code Aptera de l'application sur laquelle les personnes doivent être habilitée
     * @param prefixeCodique préfixe du codique des personnes à rechercher - à 3 chiffres, cela représente le numéro de
     *        département
     * @param profil profil Aptera que doivent avoir les personnes habilitées
     * @return une liste de PersonneAnuuaire, vide si les criteres ne correspondent à aucune personne ou jette une
     *         AnnuaireException
     */
    List<PersonneAnnuaire> rechercherPersonneAnnuaireAvecHabilitationSurApplicationAvecPrefixeCodiqueAvecProfil(
        String application, String prefixeCodique, String profil);

    /**
     * recherche dans l'annuaire fusionné une liste des personnes à partir du nom d'application , du préfixe sages et du
     * profil car le affectationsages est indexé contrairement au affectationcodique.
     * 
     * @param application "documenté"
     * @param prefixeSages "documenté"
     * @param profil "documenté"
     * @return list
     */
    List<PersonneAnnuaire> rechercherPersonneAnnuaireAvecHabilitationSurApplicationAvecPrefixeSagesAvecProfilDansAnnuaireFusionne(
        String application, String prefixeSages, String profil);

    /**
     * recherche dans l'annuaire fusionné une liste des personnes à partir du nom d'application , du préfixe sages car
     * le affectationsages est indexé contrairement au affectationcodique.
     * 
     * @param application "documenté"
     * @param prefixeSages "documenté"
     * @return list
     */
    List<PersonneAnnuaire> rechercherPersonneAnnuaireAvecHabilitationSurApplicationAvecPrefixeSagesDansAnnuaireFusionne(
        String application, String prefixeSages);

    /**
     * retourne les personnes ayant une habilitation sur une application avec un profil donné.
     * 
     * @param application --
     * @param profil --
     * @return le critere application + profil ramene parfois (meêm souvent ) plus de 500 champs , d'ou erreur ldap on
     *         prend donc le parti de balayer l'alphabet sous forme (aa, ab, ac...) afin de rechercher en plus les
     *         personnes dont le nom commence par aa , puis ab , puis ac (même logique que dans aptera) ATTENTION / :
     *         CETTE METHODE EST DONC TRES LONGUE ET DOIT DONC ETRE INSEREE DANS UN BATCH DE PREFERENCE + remplir dans
     *         le contrat annuaire que on lit le champ mefiapplihabilitdgcp
     */
    abstract List<PersonneAnnuaire> rechercherPersonneAnnuaireAvecHabilitationSurApplicationAvecProfil(
        String application, String profil);

    /**
     * retourne une liste de PersonneAnnuaire répondant aux critères passés.
     * 
     * @param prefixePrenom le prefixe du nom propre de la personne recherche si chaine vide : pas de recherche par nom
     * @param prefixeNom le prefixe du prenom de la personne recherche si chaine vide : pas de recherche par prenom
     * @return une liste de PersonneAnuuaire, vide si les criteres ne correspondent à aucune personne ou jette une
     *         AnnuaireException
     */
    abstract List<PersonneAnnuaire> rechercherPersonneAnnuaireParDebutPrenomNom(String prefixePrenom, String prefixeNom);

    /**
     * retourne une liste de PersonneAnnuaire répondant aux critères passés.
     * 
     * @param prefixePrenom le prefixe du nom propre de la personne recherche si chaine vide : pas de recherche par nom
     * @param prefixeNom le prefixe du prenom de la personne recherche si chaine vide : pas de recherche par prenom
     * @param maxReponses le nombre max de réponses que doit ramener la requète
     * @return une liste de PersonneAnuuaire, vide si les criteres ne correspondent à aucune personne ou jette une
     *         AnnuaireException
     */
    abstract List<PersonneAnnuaire> rechercherPersonneAnnuaireParDebutPrenomNom(String prefixePrenom, String prefixeNom,
        Long maxReponses);

    /**
     * retourne une personne annuaire pour un dn complet.
     * 
     * @param dn : dn(chemin + uid) complet de la personne dans l'annuaire
     * @return un objet PersonneAnnuaire valorise avec les entrees corespondantes de l'annuaire ou jette une
     *         AnnuaireException
     */
    abstract PersonneAnnuaire rechercherPersonneAnnuaireParDN(String dn);

    /**
     * retourne une PersonneAnnuaire pour un dn complet.
     * 
     * @param dn dn(chemin + uid) complet de la personne dans l'annuaire
     * @param applicationContextExistant a des fins d'optimisation pour eviter de rouvrir un contexte pour cette requète
     * @return un objet PersonneAnnuaire valorise avec les entrees corespondantes de l'annuaire ou jette une
     *         AnnuaireException si le dn est errone
     */
    abstract PersonneAnnuaire rechercherPersonneAnnuaireParDN(String dn, DirContext applicationContextExistant);

    /**
     * retourne une PersonneAnnuaire pour un uid.
     * 
     * @param uid uid de la personne dans l'annuaire (type jmartin-cp)
     * @return un objet PersonneAnnuaire valorise avec les entrees corespondantes de l'annuaire ou jette une
     *         AnnuaireException
     */
    abstract PersonneAnnuaire rechercherPersonneAnnuaireParUID(String uid);

    /**
     * retourne une PersonneAnnuaire pour un uidFonctionnel.
     * 
     * @param uidFonctionnel "documenté"
     * @return un objet PersonneAnnuaire valorise avec les entrees corespondantes de l'annuaire ou jette une
     *         AnnuaireException
     */
    abstract PersonneAnnuaire rechercherPersonneAnnuaireParUIDFonctionnel(String uidFonctionnel);
    
    /**
     * recherche des personnes habilitées sur une application avec libelle court :application et dont le nom commence
     * par debutnom. evolution vers le nouvel annuiare fusionné ,on change dans ce cas les attributs sur lesquels se
     * font la recherche (amlp sept2011)
     * 
     * @param application application
     * @param debutNom debutNom
     * @return the list< personne annuaire>
     */
    List<PersonneAnnuaire> rechercherPersonneAnnuaireAvecHabilitationSurApplicationAvecDebutNom(
        String application, String debutNom);
}