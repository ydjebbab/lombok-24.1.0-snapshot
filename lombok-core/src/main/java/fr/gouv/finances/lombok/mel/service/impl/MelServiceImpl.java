/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : MelServiceImpl.java
 *
 */
package fr.gouv.finances.lombok.mel.service.impl;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.Properties;

import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.MailSendException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import fr.gouv.finances.lombok.mel.service.MelService;
import fr.gouv.finances.lombok.mel.techbean.Mel;
import fr.gouv.finances.lombok.mel.techbean.MelExploitationException;
import fr.gouv.finances.lombok.mel.techbean.MelProgrammationException;
import fr.gouv.finances.lombok.mel.techbean.PieceJointe;
import fr.gouv.finances.lombok.mel.techbean.PieceJointeInline;
import fr.gouv.finances.lombok.util.base.BaseServiceImpl;
import fr.gouv.finances.lombok.util.messages.Messages;

/**
 * Cette classe de service interagit avec un serveur de messagerie pour permettre d'envoyer des mel.
 * 
 * @author chouard-cp
 * @version $Revision: 1.4 $ Date: 15 déc. 2009
 */
@Component(value="clientmelparent")
public class MelServiceImpl extends BaseServiceImpl implements MelService, InitializingBean
{
    private static final Logger log = LoggerFactory.getLogger(MelServiceImpl.class);

    /** serveur de mel. */
    private JavaMailSender serveurDeMel = null;

    /** host. */
    @Value("${mail.host:#{null}}" )
    private String host;

    /** port : paramétrage du port du serveur ou relai distant. Si le port n'est pas celui par défaut 
     * ( -1, constante DEFAULT_PORT) alors le client sera configuré pour se connecter avec StartTLS 
     * et donc négociera un chiffrement des échanges. */
    @Value("${mail.smtp.port:-1}")
    private int port;

    /** password : mot de passe du compte auprès du relai ou serveur de mail utilisateur pour accès authentifié. */
    @Value("${mail.auth.password:#{null}}")
    private String password;

    /** protocol. */
    private String protocol = null;

    /** user name : nom du compte utilisateur auprès du relai ou serveur de mail pour accès authentifié. 
     * Si aucune propriété trouvée, l'attribut userName vaudra null et donc le client mail ne sera pas 
     * configuré en accès authentifié. */
    @Value("${mail.auth.username:#{null}}")
    private String userName;

    /** session. */
    private Session session = null;

    /** java mail properties. */
    @Value("#{{'mail.smtp.connectiontimeout':'${mail.smtp.connectiontimeout:10000}', "
        + "'mail.smtp.timeout':'${mail.smtp.timeout:30000}', "
        + "'mail.debug':'${mail.debug:false}'}}")
    private Properties javaMailProperties = null;

    /**
     * Constructeur de la classe MelServiceImpl.java
     */
    public MelServiceImpl()
    {
        super();

    }

    /*
     * (non-Javadoc)
     * @see fr.gouv.finances.cp.commun.mel.service.MelService)
     */
    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param mel
     * @see fr.gouv.finances.lombok.mel.service.MelService#envoyerMel(fr.gouv.finances.lombok.mel.techbean.Mel)
     */
    @Override
    public void envoyerMel(Mel mel)
    {
        doEnvoyerMel(mel, false);

    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param mel
     * @see fr.gouv.finances.lombok.mel.service.MelService#envoyerMelAvecContenuHTML(fr.gouv.finances.lombok.mel.techbean.Mel)
     */
    @Override
    public void envoyerMelAvecContenuHTML(Mel mel)
    {
        doEnvoyerMel(mel, true);

    }

    /*
     * (non-Javadoc)
     * @see fr.gouv.finances.cp.commun.mel.service.impl.MelService#getHost()
     */
    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @return the host
     * @see fr.gouv.finances.lombok.mel.service.MelService#getHost()
     */
    @Override
    public String getHost()
    {
        return host;
    }

    /*
     * (non-Javadoc)
     * @see fr.gouv.finances.cp.commun.mel.service.impl.MelService#getJavamailproperties()
     */
    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @return the java mail properties
     * @see fr.gouv.finances.lombok.mel.service.MelService#getJavaMailProperties()
     */
    @Override
    public Properties getJavaMailProperties()
    {
        return javaMailProperties;
    }

    /*
     * (non-Javadoc)
     * @see fr.gouv.finances.cp.commun.mel.service.impl.MelService#getPassword()
     */
    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @return the password
     * @see fr.gouv.finances.lombok.mel.service.MelService#getPassword()
     */
    @Override
    public String getPassword()
    {
        return password;
    }

    /*
     * (non-Javadoc)
     * @see fr.gouv.finances.cp.commun.mel.service.impl.MelService#getPort()
     */
    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @return the port
     * @see fr.gouv.finances.lombok.mel.service.MelService#getPort()
     */
    @Override
    public int getPort()
    {
        return port;
    }

    /*
     * (non-Javadoc)
     * @see fr.gouv.finances.cp.commun.mel.service.impl.MelService#getProtocol()
     */
    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @return the protocol
     * @see fr.gouv.finances.lombok.mel.service.MelService#getProtocol()
     */
    @Override
    public String getProtocol()
    {
        return protocol;
    }

    /*
     * (non-Javadoc)
     * @see fr.gouv.finances.cp.commun.mel.service.impl.MelService#getSession()
     */
    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @return the session
     * @see fr.gouv.finances.lombok.mel.service.MelService#getSession()
     */
    @Override
    public Session getSession()
    {
        return session;
    }

    /*
     * (non-Javadoc)
     * @see fr.gouv.finances.cp.commun.mel.service.impl.MelService#getUsername()
     */
    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @return the user name
     * @see fr.gouv.finances.lombok.mel.service.MelService#getUserName()
     */
    @Override
    public String getUserName()
    {
        return userName;
    }

    /*
     * (non-Javadoc)
     * @see fr.gouv.finances.cp.commun.mel.service.impl.MelService#setHost(java.lang.String)
     */
    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @param host the new host
     * @see fr.gouv.finances.lombok.mel.service.MelService#setHost(java.lang.String)
     */
    @Override
    public void setHost(String host)
    {
        this.host = host;
    }

    /*
     * (non-Javadoc)
     * @see fr.gouv.finances.cp.commun.mel.service.impl.MelService#setJavamailproperties(java.util.Properties)
     */
    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @param javamailproperties the new java mail properties
     * @see fr.gouv.finances.lombok.mel.service.MelService#setJavaMailProperties(java.util.Properties)
     */
    @Override
    public void setJavaMailProperties(Properties javamailproperties)
    {
        this.javaMailProperties = javamailproperties;
    }

    /*
     * (non-Javadoc)
     * @see fr.gouv.finances.cp.commun.mel.service.impl.MelService#setPassword(java.lang.String)
     */
    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @param password the new password
     * @see fr.gouv.finances.lombok.mel.service.MelService#setPassword(java.lang.String)
     */
    @Override
    public void setPassword(String password)
    {
        this.password = password;
    }

    /*
     * (non-Javadoc)
     * @see fr.gouv.finances.cp.commun.mel.service.impl.MelService#setPort(int)
     */
    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @param port the new port
     * @see fr.gouv.finances.lombok.mel.service.MelService#setPort(int)
     */
    @Override
    public void setPort(int port)
    {
        this.port = port;
    }

    /*
     * (non-Javadoc)
     * @see fr.gouv.finances.cp.commun.mel.service.impl.MelService#setProtocol(java.lang.String)
     */
    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @param protocol the new protocol
     * @see fr.gouv.finances.lombok.mel.service.MelService#setProtocol(java.lang.String)
     */
    @Override
    public void setProtocol(String protocol)
    {
        this.protocol = protocol;
    }

    /*
     * (non-Javadoc)
     * @see fr.gouv.finances.cp.commun.mel.service.impl.MelService#setSession(javax.mail.Session)
     */
    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @param session the new session
     * @see fr.gouv.finances.lombok.mel.service.MelService#setSession(javax.mail.Session)
     */
    @Override
    public void setSession(Session session)
    {
        this.session = session;
    }

    /*
     * (non-Javadoc)
     * @see fr.gouv.finances.cp.commun.mel.service.impl.MelService#setUsername(java.lang.String)
     */
    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @param username the new user name
     * @see fr.gouv.finances.lombok.mel.service.MelService#setUserName(java.lang.String)
     */
    @Override
    public void setUserName(String username)
    {
        this.userName = username;
    }

    /**
     * methode Do envoyer mel : --.
     * 
     * @param mel --
     * @param estHTML --
     */
    private void doEnvoyerMel(Mel mel, boolean estHTML)
    {

        log.debug(">>> Debut doEnvoyerMel");
        if (!mel.getListeDestinatairesA().isEmpty())
        {
            log.debug("Debut envoyerMel vers adresse mel : " + mel.getListeDestinatairesA().get(0).getAddress());
        }

        this.initServeurDeMel();
        mel.verifierPresenceInformationsMinimalesPourEnvoyerLeMel();
        MimeMessage message = serveurDeMel.createMimeMessage();
        MimeMessageHelper messagehelper = null;

        try
        {
            messagehelper = new MimeMessageHelper(message, true, mel.getEncodage());
            // Traitement destinataire principal
            if (!mel.getListeDestinatairesA().isEmpty())
            {
                messagehelper.setTo(mel.getListeDestinatairesA().toArray(new InternetAddress[0]));
            }

            // Traitement destinataire en copie
            if (!mel.getListeDestinatairesCc().isEmpty())
            {
                messagehelper.setCc(mel.getListeDestinatairesCc().toArray(new InternetAddress[0]));
            }

            // Traitement destinataire en copie cachée
            if (!mel.getListeDestinatairesCci().isEmpty())
            {
                messagehelper.setBcc(mel.getListeDestinatairesCci().toArray(new InternetAddress[0]));
            }

            // Traitement adresse de réponse
            if (mel.getRepondreA() != null)
            {
                messagehelper.setReplyTo(mel.getRepondreA());
            }

            // Autres éléments
            messagehelper.setSubject(mel.getObjet());
            messagehelper.setFrom(mel.getDe());
            // Pour éviter les regressions suite à l'ajout de la possibilité d'envoyer du html
            // on conserve l'appel initial
            if (!estHTML)
            {
                messagehelper.setText(mel.getMesssage());
            }
            else
            {
                messagehelper.setText(mel.getMesssage(), true);
            }

        }
        catch (MessagingException e1)
        {
            throw new MelProgrammationException(Messages.getString("exception.problemeparametragemessage"), e1);
        }

        // Traitement des pièces jointes
        Iterator<PieceJointe> pjs = mel.getListePiecesJointes().iterator();
        while (pjs.hasNext())
        {
            PieceJointe unepj = ((PieceJointe) pjs.next());
            log.debug("Debut traitement d'une piece jointe : " + unepj.getNomPj());
            try
            {
                messagehelper.addAttachment(unepj.getNomPj(), unepj.getSource());
            }
            catch (MessagingException exception)
            {
                throw new MelProgrammationException(Messages.getString("exception.problemeattachementpj") + "- nompj:"
                    + unepj.getNomPj() + "- mel a:" + mel.getListeDestinatairesA().get(0).getAddress(), exception);
            }
        }

        // Traitement des pièces jointes inline
        Iterator<PieceJointeInline> pjis = mel.getListePiecesJointesInline().iterator();
        while (pjis.hasNext())
        {
            PieceJointeInline unepji = ((PieceJointeInline) pjis.next());
            log.debug("Debut traitement d'une piece jointe inline : " + unepji.getContentId());
            try
            {
                messagehelper.addInline(unepji.getContentId(), unepji.getSource());
                if (unepji.getFileName() != null)
                {
                    messagehelper.getMimeMultipart().getBodyPart("<" + unepji.getContentId() + ">").setFileName(unepji.getFileName());
                }
            }
            catch (MessagingException exception)
            {
                throw new MelProgrammationException(Messages.getString("exception.problemeattachementpjinline")
                    + "- contentid:" + unepji.getContentId() + "- mel a:"
                    + mel.getListeDestinatairesA().get(0).getAddress(), exception);
            }
        }

        try
        {
            serveurDeMel.send(message);
        }
        catch (MailSendException exception)
        {
            throw new MelProgrammationException(Messages.getString("exception.problemeenvoi"), exception);
        }
        if (!mel.getListeDestinatairesA().isEmpty())
        {
            log.debug("Succes envoyerMel vers liste d'adresses. Premiere adresse : "
                + mel.getListeDestinatairesA().get(0).getAddress());
        }
    }

    /**
     * methode Inits the serveur de mel : --.
     */
    private synchronized void initServeurDeMel()
    {
        if (serveurDeMel == null)
        {
            log.debug("Le client de mel n'est pas initialisé. Début Initialisation ...");
            if (host == null)
            {
                throw new MelExploitationException(Messages.getString("exception.adresseserveurmailnonprecise"));
            }
            JavaMailSenderImpl serv = new JavaMailSenderImpl();
            log.debug("initialisation du serveur host :" + this.host);
            serv.setHost(this.host);
            // propriétés pour activer l'authentification et/ou StartTLS
            boolean activerAuthentification = false;
            boolean activerStartTLS = false;

            if (this.userName != null)
            {
                log.debug("initialisation du compte utilisateur pour authentification auprès du serveur : " + this.userName);
                log.debug("le client mail sera paramétré en mode authentifié avec la propriété javamail suivante : mail.smtp.auth=true");
                serv.setUsername(this.userName);
                activerAuthentification = true;
            }
            if (this.password != null)
            {
                log.debug(
                    "initialisation du mot de passe du compte utilisateur pour authentification auprès du serveur : " + this.password);
                serv.setPassword(this.password);
            }
            if (this.protocol != null)
            {
                log.debug("initialisation du protocole : " + this.protocol + " à la place du protocole par défaut : " + serv.getProtocol());
                serv.setProtocol(this.protocol);
            }
            else
            {
                log.debug("protocole par défaut utilisé : " + JavaMailSenderImpl.DEFAULT_PROTOCOL);
            }
            if (this.port != JavaMailSenderImpl.DEFAULT_PORT)
            {
                log.debug("initialisation du port : " + this.port + " à la place du port par défaut : " + JavaMailSenderImpl.DEFAULT_PORT
                    + " (-1 permet de laisser javamail utiliser le port par défaut à savoir 25)");
                serv.setPort(this.port);
                log.debug("port par défaut changé : StartTLS sera activé pour la négociation d'une communication chiffrée");
                log.debug("la propriété javamail mail.smtp.starttls.enable=true et la propriété mail.smtp.ssl.trust à " + serv.getHost());
                activerStartTLS = true;
            }
            // ce code sur la session ne sert à rien. En effet soit la config de javamail est faite par
            // javamailproperties, soit par la
            // session mais pas par les deux...
            if (this.session != null)
            {
                log.debug("initialisation de la session : " + this.session);
                serv.setSession(this.session);
            }
            if (this.javaMailProperties != null)
            {
                log.debug("initialisation des javamailpropertiers : ");
                if (log.isDebugEnabled())
                {

                    Enumeration<Object> keys = this.javaMailProperties.keys();
                    while (keys.hasMoreElements())
                    {
                        String key = (String) keys.nextElement();
                        log.debug(key + ": " + this.javaMailProperties.get(key));
                    }

                }
                serv.setJavaMailProperties(this.javaMailProperties);
            }

            if (activerAuthentification)
            {
                //
                Properties mailProps = serv.getJavaMailProperties();
                if (mailProps == null)
                {
                    mailProps = new Properties();
                }
                mailProps.setProperty("mail.smtp.auth", "true");
                serv.setJavaMailProperties(mailProps);
            }
            if (activerStartTLS)
            {
                //
                Properties mailProps = serv.getJavaMailProperties();
                if (mailProps == null)
                {
                    mailProps = new Properties();
                }
                mailProps.setProperty("mail.smtp.starttls.enable", "true");
                mailProps.setProperty("mail.smtp.ssl.trust", serv.getHost());
                serv.setJavaMailProperties(mailProps);
            }
            StringWriter writer = new StringWriter();
            serv.getJavaMailProperties().list(new PrintWriter(writer));
            log.debug(writer.getBuffer().toString());

            log.debug("Fin initialisation du client de mail.");

            log.info("Client de mail créé avec la configuration suivante :");
            log.info("-> communication avec le serveur de mail à l'adresse  : " + host);
            log.info("-> mode authentifié : " + activerAuthentification);
            log.info("-> Le port spécifié (mail.smtp.port=" + port
                + ") est différent de celui par défaut => StartTls et Chiffrement activés.");
            log.info("-> mail.smtp.timeout : " + serv.getJavaMailProperties().getProperty("mail.smtp.timeout"));
            log.info("-> mail.smtp.connectiontimeout : " + serv.getJavaMailProperties().getProperty("mail.smtp.connectiontimeout"));
            log.info("-> mode debug :" + serv.getJavaMailProperties().getProperty("mail.debug"));

            serveurDeMel = serv;
        }
        else
        {
            log.debug("Le serveur de mel est déjà initialisé");
        }
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see org.springframework.beans.factory.InitializingBean#afterPropertiesSet()
     */
    @Override
    public void afterPropertiesSet() throws Exception
    {
        log.debug(">>> Debut afterPropertiesSet");
        initServeurDeMel();
    }

}
