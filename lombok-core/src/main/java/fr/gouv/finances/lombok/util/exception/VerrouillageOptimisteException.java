/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 */
package fr.gouv.finances.lombok.util.exception;

/**
 * Fait partie des exceptions potentiellement gérables dans le code. Elle correspond à
 * la detection d'une modification ou suppression des données par un tiers (autre utilisateur ou batch) pendant un
 * traitement de modification Pour les parties web elle permet d'informer l'utilisateur que les données qu'il
 * cherche à modifier ont été mises à jour entre temps.
 * 
 * @author amleplatinec
 * @version $Revision: 1.2 $ Date: 11 déc. 2009
 */
public class VerrouillageOptimisteException extends GerableException
{

    /**
     * Constant : serialVersionUID.
     */
    private static final long serialVersionUID = 1L;

    /** Constant : message. */
    private static final String message =
        "Un autre utilisateur a modifié ou supprimé les données sur lesquelles vous travaillez";

    /**
     * Instanciation de verrouillage optimiste exception.
     */
    public VerrouillageOptimisteException()
    {
        this(message);
    }

    /**
     * Instanciation de verrouillage optimiste exception.
     * 
     * @param arg0 --
     */
    public VerrouillageOptimisteException(String arg0)
    {
        super(arg0);
    }

    /**
     * Instanciation de verrouillage optimiste exception.
     * 
     * @param arg0 --
     * @param arg1 --
     */
    public VerrouillageOptimisteException(String arg0, Throwable arg1)
    {
        super(arg0, arg1);
    }

    /**
     * Instanciation de verrouillage optimiste exception.
     * 
     * @param arg0 --
     */
    public VerrouillageOptimisteException(Throwable arg0)
    {
        this(message, arg0);
    }

}
