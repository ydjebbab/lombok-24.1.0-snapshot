/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 */
package fr.gouv.finances.lombok.securite.techbean;

import java.util.List;
import java.util.Set;

import fr.gouv.finances.lombok.util.base.BaseTechBean;

/**
 * Class ApplicationAnnuaire --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.2 $ Date: 11 déc. 2009
 */
public class ApplicationAnnuaire extends BaseTechBean
{

    /**
     * author : amleplatinec-cp reprend les attributs de l'application telle qu'on l'a défini dans l'annuaire et Aptera.
     */
    private static final long serialVersionUID = 1L;

    /** url appli. */
    private String urlAppli;

    /** url logo. */
    private String urlLogo;

    /** commentaire appli. */
    private String commentaireAppli;

    /** type stockage habilit. */
    private String typeStockageHabilit;

    /** delegation profils. */
    private Boolean delegationProfils;

    /** exclusion admin util. */
    private Boolean exclusionAdminUtil;

    /** habilitation xsd. */
    private String habilitationXsd;

    /** libelle court appli. */
    private String libelleCourtAppli;

    /** libelle long appli. */
    private String libelleLongAppli;

    /** multi habilit. */
    private Boolean multiHabilit;

    /** type gestion structure. */
    private String typeGestionStructure;

    /** detail filtre. */
    private String detailFiltre;

    /** detail profil. */
    private List detailProfil;

    /** detail profil decode. */
    private Set detailProfilDecode;

    /**
     * Constructeur de la classe ApplicationAnnuaire.java
     *
     */
    public ApplicationAnnuaire()
    {
        super(); 
        
    }

    /**
     * Accesseur de l attribut commentaire appli.
     * 
     * @return commentaire appli
     */
    public String getCommentaireAppli()
    {
        return commentaireAppli;
    }

    /**
     * Accesseur de l attribut delegation profils.
     * 
     * @return delegation profils
     */
    public Boolean getDelegationProfils()
    {
        return delegationProfils;
    }

    /**
     * Accesseur de l attribut detail filtre.
     * 
     * @return detail filtre
     */
    public String getDetailFiltre()
    {
        return detailFiltre;
    }

    /**
     * Accesseur de l attribut detail profil.
     * 
     * @return detail profil
     */
    public List getDetailProfil()
    {
        return detailProfil;
    }

    /**
     * Accesseur de l attribut detail profil decode.
     * 
     * @return detail profil decode
     */
    public Set getDetailProfilDecode()
    {
        return detailProfilDecode;
    }

    /**
     * Accesseur de l attribut exclusion admin util.
     * 
     * @return exclusion admin util
     */
    public Boolean getExclusionAdminUtil()
    {
        return exclusionAdminUtil;
    }

    /**
     * Accesseur de l attribut habilitation xsd.
     * 
     * @return habilitation xsd
     */
    public String getHabilitationXsd()
    {
        return habilitationXsd;
    }

    /**
     * Accesseur de l attribut libelle court appli.
     * 
     * @return libelle court appli
     */
    public String getLibelleCourtAppli()
    {
        return libelleCourtAppli;
    }

    /**
     * Accesseur de l attribut libelle long appli.
     * 
     * @return libelle long appli
     */
    public String getLibelleLongAppli()
    {
        return libelleLongAppli;
    }

    /**
     * Accesseur de l attribut multi habilit.
     * 
     * @return multi habilit
     */
    public Boolean getMultiHabilit()
    {
        return multiHabilit;
    }

    /**
     * Accesseur de l attribut type gestion structure.
     * 
     * @return type gestion structure
     */
    public String getTypeGestionStructure()
    {
        return typeGestionStructure;
    }

    /**
     * Accesseur de l attribut type stockage habilit.
     * 
     * @return type stockage habilit
     */
    public String getTypeStockageHabilit()
    {
        return typeStockageHabilit;
    }

    /**
     * Accesseur de l attribut url appli.
     * 
     * @return url appli
     */
    public String getUrlAppli()
    {
        return urlAppli;
    }

    /**
     * Accesseur de l attribut url logo.
     * 
     * @return url logo
     */
    public String getUrlLogo()
    {
        return urlLogo;
    }

    /**
     * Modificateur de l attribut commentaire appli.
     * 
     * @param commentaireAppli le nouveau commentaire appli
     */
    public void setCommentaireAppli(String commentaireAppli)
    {
        this.commentaireAppli = commentaireAppli;
    }

    /**
     * Modificateur de l attribut delegation profils.
     * 
     * @param delegationProfils le nouveau delegation profils
     */
    public void setDelegationProfils(Boolean delegationProfils)
    {
        this.delegationProfils = delegationProfils;
    }

    /**
     * Modificateur de l attribut detail filtre.
     * 
     * @param detailFiltre le nouveau detail filtre
     */
    public void setDetailFiltre(String detailFiltre)
    {
        this.detailFiltre = detailFiltre;
    }

    /**
     * Modificateur de l attribut detail profil.
     * 
     * @param detailProfil le nouveau detail profil
     */
    public void setDetailProfil(List detailProfil)
    {
        this.detailProfil = detailProfil;
    }

    /**
     * Modificateur de l attribut detail profil decode.
     * 
     * @param detailProfilDecode le nouveau detail profil decode
     */
    public void setDetailProfilDecode(Set detailProfilDecode)
    {
        this.detailProfilDecode = detailProfilDecode;
    }

    /**
     * Modificateur de l attribut exclusion admin util.
     * 
     * @param exclusionAdminUtil le nouveau exclusion admin util
     */
    public void setExclusionAdminUtil(Boolean exclusionAdminUtil)
    {
        this.exclusionAdminUtil = exclusionAdminUtil;
    }

    /**
     * Modificateur de l attribut habilitation xsd.
     * 
     * @param habilitationXsd le nouveau habilitation xsd
     */
    public void setHabilitationXsd(String habilitationXsd)
    {
        this.habilitationXsd = habilitationXsd;
    }

    /**
     * Modificateur de l attribut libelle court appli.
     * 
     * @param libelleCourtAppli le nouveau libelle court appli
     */
    public void setLibelleCourtAppli(String libelleCourtAppli)
    {
        this.libelleCourtAppli = libelleCourtAppli;
    }

    /**
     * Modificateur de l attribut libelle long appli.
     * 
     * @param libelleLongAppli le nouveau libelle long appli
     */
    public void setLibelleLongAppli(String libelleLongAppli)
    {
        this.libelleLongAppli = libelleLongAppli;
    }

    /**
     * Modificateur de l attribut multi habilit.
     * 
     * @param multiHabilit le nouveau multi habilit
     */
    public void setMultiHabilit(Boolean multiHabilit)
    {
        this.multiHabilit = multiHabilit;
    }

    /**
     * Modificateur de l attribut type gestion structure.
     * 
     * @param typeGestionStructure le nouveau type gestion structure
     */
    public void setTypeGestionStructure(String typeGestionStructure)
    {
        this.typeGestionStructure = typeGestionStructure;
    }

    /**
     * Modificateur de l attribut type stockage habilit.
     * 
     * @param typeStockageHabilit le nouveau type stockage habilit
     */
    public void setTypeStockageHabilit(String typeStockageHabilit)
    {
        this.typeStockageHabilit = typeStockageHabilit;
    }

    /**
     * Modificateur de l attribut url appli.
     * 
     * @param urlAppli le nouveau url appli
     */
    public void setUrlAppli(String urlAppli)
    {
        this.urlAppli = urlAppli;
    }

    /**
     * Modificateur de l attribut url logo.
     * 
     * @param urlLogo le nouveau url logo
     */
    public void setUrlLogo(String urlLogo)
    {
        this.urlLogo = urlLogo;
    }

}
