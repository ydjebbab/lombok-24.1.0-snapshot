/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 */
package fr.gouv.finances.lombok.securite.techbean;

import fr.gouv.finances.lombok.util.base.BaseTechBean;

/**
 * Class ProfilAnnuaire --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.2 $ Date: 11 déc. 2009
 */
public class ProfilAnnuaire extends BaseTechBean
{

    /** amleplatinec-cp description du profil dans la branche application de l'annuaire. */
    private static final long serialVersionUID = 1L;

    /** code profil. */
    private String codeProfil;

    /** libelle profil. */
    private String libelleProfil;

    /** commentaire profil. */
    private String commentaireProfil;

    /** a supprimer. */
    private String aSupprimer;

    /**
     * Constructeur de la classe ProfilAnnuaire.java
     *
     */
    public ProfilAnnuaire()
    {
        super();
        
    }

    /**
     * Accesseur de l attribut a supprimer.
     * 
     * @return a supprimer
     */
    public String getASupprimer()
    {
        return aSupprimer;
    }

    /**
     * Accesseur de l attribut code profil.
     * 
     * @return code profil
     */
    public String getCodeProfil()
    {
        return codeProfil;
    }

    /**
     * Accesseur de l attribut commentaire profil.
     * 
     * @return commentaire profil
     */
    public String getCommentaireProfil()
    {
        return commentaireProfil;
    }

    /**
     * Accesseur de l attribut libelle profil.
     * 
     * @return libelle profil
     */
    public String getLibelleProfil()
    {
        return libelleProfil;
    }

    /**
     * Modificateur de l attribut a supprimer.
     * 
     * @param supprimer le nouveau a supprimer
     */
    public void setASupprimer(String supprimer)
    {
        aSupprimer = supprimer;
    }

    /**
     * Modificateur de l attribut code profil.
     * 
     * @param codeProfil le nouveau code profil
     */
    public void setCodeProfil(String codeProfil)
    {
        this.codeProfil = codeProfil;
    }

    /**
     * Modificateur de l attribut commentaire profil.
     * 
     * @param commentaireProfil le nouveau commentaire profil
     */
    public void setCommentaireProfil(String commentaireProfil)
    {
        this.commentaireProfil = commentaireProfil;
    }

    /**
     * Modificateur de l attribut libelle profil.
     * 
     * @param libelleProfil le nouveau libelle profil
     */
    public void setLibelleProfil(String libelleProfil)
    {
        this.libelleProfil = libelleProfil;
    }

}
