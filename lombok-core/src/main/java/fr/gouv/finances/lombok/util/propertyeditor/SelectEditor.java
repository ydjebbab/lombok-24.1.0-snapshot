/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : SelectEditor.java
 *
 */
package fr.gouv.finances.lombok.util.propertyeditor;

import java.beans.PropertyDescriptor;
import java.beans.PropertyEditorSupport;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.MessageFormat;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.beanutils.PropertyUtilsBean;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.PredicateUtils;
import org.apache.commons.collections.Transformer;
import org.apache.commons.collections.TransformerUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import fr.gouv.finances.lombok.util.exception.ProgrammationException;

/**
 * Class SelectEditor --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.2 $ Date: 11 déc. 2009
 */
public class SelectEditor extends PropertyEditorSupport
{

    /** Constant : INVALID_PROPERTY. */
    public static final String INVALID_PROPERTY = "La propriété : {0} n''est pas valide dans la classe {1}";

    /** Constant : ALL_NULL. */
    public static final String ALL_NULL = "Tous les objets de la collection sont null";

    /** Constant : METHOD_NULL. */
    public static final String METHOD_NULL = "Impossible de déterminer le nom de la méthode.";

    /** Constant : INCOHERENCE. */
    public static final String INCOHERENCE =
        "La collection utilisée est incohérente, elle contient plusieurs éléments pour lesquels la valeur de {0}.toString() = {1}";

    /** Constant : COLLECTION_NULL. */
    public static final String COLLECTION_NULL = "La collection utilisée n''est pas instanciée";

    /** Constant : MAP_NULL. */
    public static final String MAP_NULL = "La Map utilisée n'est pas instanciée";

    /** Constant : NULL_VALUE. */
    public static final String NULL_VALUE = "-999999";

    /** Constant : TYPE_LIST. */
    private static final int TYPE_LIST = 1;

    /** Constant : TYPE_SET. */
    private static final int TYPE_SET = 2;

    /** Constant : TYPE_MAP. */
    private static final int TYPE_MAP = 3;

    /** Constant : logger. */
    private static final Log logger = LogFactory.getLog(SelectEditor.class);

    /** itemstype. */
    private final int itemstype;

    /** value property. */
    private final String valueProperty;

    /** items. */
    private final Object items;

    /** defaut item value. */
    private final String defautItemValue;

    /**
     * Créé une nouvelle instance de l'objet SelectEditor, en utilisant la liste d'items passée en paramètres.
     * Utilisable avec le tag app:select (ex : <app:select itemslist="${listeimpots}" value="codeImpot"
     * label="nomImpot"/>)
     * 
     * @param items liste des objets utilisés pour construire la liste déroulante (liste stockée dans le contexte sous
     *        le nom indiqué dans l'attribut "itemslist" (ex : listeimpots)
     * @param valueProperty nom de la propriété des objets de la liste qui est utilisée dans l'attribut "value" du tag
     *        select (ex : codeImpot))
     */
    public SelectEditor(List items, String valueProperty)
    {
        super();
        this.itemstype = SelectEditor.TYPE_LIST;
        this.items = items;
        this.valueProperty = valueProperty;
        this.defautItemValue = SelectEditor.NULL_VALUE;
    }

    /**
     * Créé une nouvelle instance de l'objet SelectEditor, en utilisant la liste d'items passée en paramètres.
     * Utilisable avec le tag app:select (ex : <app:select itemslist="${listeimpots}" value="codeImpot" label="nomImpot"
     * defautitemvalue="-999999"/>)
     * 
     * @param items liste des objets utilisés pour construire la liste déroulante (liste stockée dans le contexte sous
     *        le nom indiqué dans l'attribut "itemslist" (ex : listeimpots)
     * @param valueProperty nom de la propriété des objets de la liste qui est utilisée dans l'attribut "value" du tag
     *        select (ex : codeImpot))
     * @param defautItemValue String qui représente un objet null dans la liste déroulante defautitemvalue, correspond à
     *        la valeur affectée à l'attribut defautItemValue du tag select (Par défaut -999999)
     */
    public SelectEditor(List items, String valueProperty, String defautItemValue)
    {
        super();
        this.itemstype = SelectEditor.TYPE_LIST;
        this.items = items;
        this.valueProperty = valueProperty;
        this.defautItemValue = defautItemValue;
    }

    /**
     * Créé une nouvelle instance de l'objet SelectEditor, en utilisant la Map d'items passée en paramètres. Utilisable
     * avec le tag app:select (ex : <app:select itemsmap="${mapimpots}" value="codeImpot" label="nomImpot"/>)
     * 
     * @param items Map des objets utilisés pour construire la liste déroulante (Map stockée dans le contexte sous le
     *        nom indiqué dans l'attribut "itemsmap" (ex : mapImpots)
     * @param valueProperty nom de la propriété des objets de la Map qui est utilisée dans l'attribut "value" du tag
     *        select (ex : codeImpot))
     */
    public SelectEditor(Map items, String valueProperty)
    {
        super();
        this.itemstype = SelectEditor.TYPE_MAP;
        this.items = items;
        this.valueProperty = valueProperty;
        this.defautItemValue = SelectEditor.NULL_VALUE;
    }

    /**
     * Créé une nouvelle instance de l'objet SelectEditor, en utilisant la Map d'items passée en paramètres. Utilisable
     * avec le tag app:select (ex : <app:select itemsmap="${mapimpots}" value="codeImpot" label="nomImpot"/>)
     * 
     * @param items Map des objets utilisés pour construire la liste déroulante (Map stockée dans le contexte sous le
     *        nom indiqué dans l'attribut "itemsmap" (ex : mapImpots)
     * @param valueProperty nom de la propriété des objets de la Map qui est utilisée dans l'attribut "value" du tag
     *        select (ex : codeImpot))
     * @param defautItemValue String qui représente un objet null dans la liste déroulante defautitemvalue, correspond à
     *        la valeur affectée à l'attribut defautItemValue du tag select (Par défaut -999999)
     */
    public SelectEditor(Map items, String valueProperty, String defautItemValue)
    {
        super();
        this.itemstype = SelectEditor.TYPE_MAP;
        this.items = items;
        this.valueProperty = valueProperty;
        this.defautItemValue = defautItemValue;
    }

    /**
     * Créé une nouvelle instance de l'objet SelectEditor, en utilisant le Set d'items passé en paramètres. Utilisable
     * avec le tag app:select (ex : <app:select itemsset="${setimpots}" value="codeImpot" label="nomImpot"/>)
     * 
     * @param items Map des objets utilisés pour construire la liste déroulante (Map stockée dans le contexte sous le
     *        nom indiqué dans l'attribut "itemsset" (ex : setImpots)
     * @param valueProperty nom de la propriété des objets de le set qui est utilisée dans l'attribut "value" du tag
     *        select (ex : codeImpot))
     */
    public SelectEditor(Set items, String valueProperty)
    {
        super();
        this.itemstype = SelectEditor.TYPE_SET;
        this.items = items;
        this.valueProperty = valueProperty;
        this.defautItemValue = SelectEditor.NULL_VALUE;
    }

    /**
     * Créé une nouvelle instance de l'objet SelectEditor, en utilisant le Set d'items passé en paramètres. Utilisable
     * avec le tag app:select (ex : <app:select itemsset="${setimpots}" value="codeImpot" label="nomImpot"/>)
     * 
     * @param items Map des objets utilisés pour construire la liste déroulante (Map stockée dans le contexte sous le
     *        nom indiqué dans l'attribut "itemsset" (ex : setImpots)
     * @param valueProperty nom de la propriété des objets de le set qui est utilisée dans l'attribut "value" du tag
     *        select (ex : codeImpot))
     * @param defautItemValue String qui représente un objet null dans la liste déroulante defautitemvalue, correspond à
     *        la valeur affectée à l'attribut defautItemValue du tag select (Par défaut -999999)
     */
    public SelectEditor(Set items, String valueProperty, String defautItemValue)
    {
        super();
        this.itemstype = SelectEditor.TYPE_SET;
        this.items = items;
        this.valueProperty = valueProperty;
        this.defautItemValue = defautItemValue;
    }

    /**
     * Retourne la chaîne de caractères qui représente l'objet.
     * 
     * @return the as text
     */
    @Override
    public String getAsText()
    {
        String result;
        Object unObjet = getValue();

        // L'objet est null
        if (unObjet == null)
        {
            result = this.defautItemValue;
        }
        // L'objet n'est pas null et aucune propriété n'est passée en paramètre. objet.toString() est l'identifiant
        // de l'objet dans la collection
        else if (valueProperty == null)
        {
            result = unObjet.toString();
            if (logger.isDebugEnabled())
            {
                logger.debug("result = " + result + "- valueAttributeName = null");
            }
        }
        else
        {
            Set unSet = new HashSet();
            unSet.add(unObjet);
            String methodName = null;

            // lecture de l'accesseur
            methodName = this.getGetterMethodName(unObjet, this.valueProperty);

            // Transformeur qui exécute une méthode sur un objet, puis convertit le résultat
            // en String via un appel à toString()
            Transformer unTransformer = TransformerUtils.invokerTransformer(methodName);

            CollectionUtils.transform(unSet, unTransformer);
            Object uneCle = unSet.iterator().next();

            if (uneCle == null)
            {
                result = SelectEditor.NULL_VALUE;
            }
            else
            {
                result = uneCle.toString();
            }

            if (logger.isDebugEnabled())
            {
                logger.debug("result = " + result + "- valueAttributeName = null" + "- methodName = " + methodName);
            }
        }
        return result;
    }

    /**
     * Retourne l'objet de la collection (ou de la Map d'objet) qui est identifié par le String text.
     * 
     * @param text --
     * @throws IllegalArgumentException the illegal argument exception
     */
    @Override
    public void setAsText(final String text) throws IllegalArgumentException
    {
        Object unObjet = null;
        if (text != null && !text.equals(NULL_VALUE))
        {
            switch (this.itemstype)
            {
                case SelectEditor.TYPE_LIST:
                case SelectEditor.TYPE_SET:
                    unObjet = findObjectInCollection((Collection) this.items, text, this.valueProperty);
                    break;
                case SelectEditor.TYPE_MAP:
                    unObjet = findObjectInMap((Map) this.items, text);
                    break;
                default:
            }
        }

        this.setValue(unObjet);
    }

    /**
     * Recherche un objet dans une collection en utilisant une clé.
     * 
     * @param uneCollection collection dans laquelle l'objet est recherché
     * @param text String qui identifie un objet dans la collection
     * @param valueProperty Propriété portée par les objets de la collection qui est comparée avec
     * @return l'objet sélectionné ou null si aucun objet ne correspond à la clé dans la collection
     */
    private Object findObjectInCollection(final Collection uneCollection, final String text, final String valueProperty)
    {
        Object unObjet = null;
        String methodName = null;
        Transformer unTransformer;

        if (uneCollection == null)
        {
            throw new ProgrammationException(COLLECTION_NULL);
        }

        if (valueProperty != null && StringUtils.isNotBlank(valueProperty))
        {
            methodName = this.getGetterMethodNameFromCollection(uneCollection, valueProperty);

            // Transformeur qui exécute une méthode sur un objet, puis convertit le résultat
            // en String via un appel à toString()
            unTransformer =
                TransformerUtils.chainedTransformer(TransformerUtils.invokerTransformer(methodName), TransformerUtils
                    .stringValueTransformer());
        }
        else
        {
            // Transformeur qui convertit en String via un appel à toString()
            unTransformer = TransformerUtils.stringValueTransformer();
        }

        // Extrait de la liste les éléments pour lesquels la valeur de la propriété "valueProperty"
        // convertie en String est égale au paramètre "text"
        final Collection selectedElements =
            CollectionUtils.select(uneCollection, PredicateUtils.transformedPredicate(unTransformer, PredicateUtils
                .equalPredicate(text)));

        // Si plus d'un élément est sélectionné, on considère que la collection est incohérente
        if (selectedElements.size() > 1)
        {
            MessageFormat msgFormat = new MessageFormat(INCOHERENCE);
            Object[] attr = {methodName, text};

            throw new ProgrammationException(msgFormat.format(attr));
        }

        if (selectedElements.iterator().hasNext())
        {
            unObjet = selectedElements.iterator().next();
        }
        return unObjet;
    }

    /**
     * Recherche un objet dans une Map en utilisant une clé.
     * 
     * @param uneMap map dans laquelle l'objet est recherché
     * @param text String qui identifie l'objet dans la map
     * @return l'objet sélectionné ou null si aucun objet ne correspond à la clé dans la map
     */
    private Object findObjectInMap(final Map uneMap, final String text)
    {
        Object unObjet = null;

        if (uneMap == null)
        {
            throw new ProgrammationException(MAP_NULL);
        }
        unObjet = uneMap.get(text);
        return unObjet;
    }

    /**
     * Retourne le nom de la méthode qui permet de lire la propriété de nom "property" pour un objet "element.
     * 
     * @param element objet sur lequel le getter va être appelé
     * @param property nom de la propriété qui sert pour la construction du getter
     * @return the getter method name
     */
    private String getGetterMethodName(final Object element, final String property)
    {
        Method method = null;
        PropertyUtilsBean propertyutilsbean = new PropertyUtilsBean();
        PropertyDescriptor propDescriptor;

        try
        {
            propDescriptor = propertyutilsbean.getPropertyDescriptor(element, property);
        }
        catch (NoSuchMethodException e)
        {
            logger.warn(e);
            MessageFormat msgFormat = new MessageFormat(INVALID_PROPERTY);
            Object[] attr = {property, element.getClass().getName()};

            throw new ProgrammationException(msgFormat.format(attr), e);
        }
        catch (IllegalAccessException e)
        {
            logger.warn(e);
            throw new ProgrammationException(e);
        }
        catch (InvocationTargetException e)
        {
            logger.warn(e);
            throw new ProgrammationException(e);
        }

        if (propDescriptor == null)
        {
            MessageFormat msgFormat = new MessageFormat(INVALID_PROPERTY);
            Object[] attr = {property, element.getClass().getName()};

            throw new ProgrammationException(msgFormat.format(attr));
        }

        method = propertyutilsbean.getReadMethod(propDescriptor);

        if (method == null)
        {
            MessageFormat msgFormat = new MessageFormat(INVALID_PROPERTY);
            Object[] attr = {property, element.getClass().getName()};

            throw new ProgrammationException(msgFormat.format(attr));
        }

        return method.getName();
    }

    /**
     * Accesseur de l attribut getter method name from collection.
     * 
     * @param uneCollection --
     * @param property --
     * @return getter method name from collection
     */
    private String getGetterMethodNameFromCollection(final Collection uneCollection, final String property)
    {
        Object firstInColl = null;
        String methodName = null;
        boolean allNull = true;

        if (uneCollection == null)
        {
            throw new ProgrammationException(COLLECTION_NULL);
        }

        // Utilise le premier objet non null de la liste pour déterminer le nom de la méthode à utiliser
        // La liste doit être composée d'éléments qui accèdent à la propriété par la même méthode
        Iterator coll = uneCollection.iterator();
        while (coll.hasNext())
        {
            firstInColl = coll.next();
            if (firstInColl != null)
            {
                allNull = false;
                methodName = this.getGetterMethodName(firstInColl, property);
                break;
            }
        }

        if (allNull)
        {
            throw new ProgrammationException(ALL_NULL);
        }

        if (methodName == null)
        {
            throw new ProgrammationException(METHOD_NULL);
        }
        return methodName;
    }
}
