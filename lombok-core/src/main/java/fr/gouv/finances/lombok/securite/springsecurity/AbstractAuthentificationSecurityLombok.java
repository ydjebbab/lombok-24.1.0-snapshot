/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : AbstractAuthentificationSecurityLombok.java
 *
 */
package fr.gouv.finances.lombok.securite.springsecurity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import fr.gouv.finances.lombok.securite.service.AuthentificationEtLectureHabilitationsService;
import fr.gouv.finances.lombok.securite.service.AvantHabilitationService;
import fr.gouv.finances.lombok.securite.techbean.AuthentificationRegleGestionException;
import fr.gouv.finances.lombok.securite.techbean.HabilitationAnnuaire;
import fr.gouv.finances.lombok.securite.techbean.HabilitationRegleGestionException;
import fr.gouv.finances.lombok.securite.techbean.PersonneAnnuaire;
import fr.gouv.finances.lombok.srm.bean.Message;
import fr.gouv.finances.lombok.srm.bean.Profil;
import fr.gouv.finances.lombok.srm.service.SiremeService;

/**
 * Class AbstractAuthentificationSecurityLombok.
 * 
 * @author chouard-cp
 * @version $Revision: 1.6 $ Date: 15 déc. 2009
 */
public abstract class AbstractAuthentificationSecurityLombok extends AbstractUserDetailsAuthenticationProvider
    implements ApplicationContextAware
{
    private static final Logger log = LoggerFactory.getLogger(AbstractAuthentificationSecurityLombok.class);
    
    /** Constant : MESSAGE_BLOQUANT. */
    protected static final String MESSAGE_BLOQUANT = " Acces interdit à l 'application par un administrateur :  ";

    /** Constant : APPLI. */
    private static final String APPLI = "appli";

    /** Constant : APPLIPORTAIL. */
    private static final String APPLIPORTAIL = "appliportail";

    /** Constant : PORTAIL. */
    private static final String PORTAIL = "portail";

    /** authentification et lecture habilitations service. */
    protected AuthentificationEtLectureHabilitationsService authentificationEtLectureHabilitationsService;

    /** code application dans habilitations. */
    protected String codeApplicationDansHabilitations;

    /** mode authentification. */
    protected String modeAuthentification;


    /** ctx. */
    private ApplicationContext ctx;

    /** siremeserviceso. */
    private SiremeService siremeserviceso;

    public AbstractAuthentificationSecurityLombok()
    {
        super();
    }

    /**
     * Gets the siremeserviceso.
     *
     * @return the siremeserviceso
     */
    public SiremeService getSiremeserviceso()
    {
        return siremeserviceso;
    }

    /**
     * Accesseur de l attribut authentification et lecture habilitations service.
     * 
     * @return authentification et lecture habilitations service
     */
    public AuthentificationEtLectureHabilitationsService getAuthentificationEtLectureHabilitationsService()
    {
        return authentificationEtLectureHabilitationsService;
    }

    /**
     * Accesseur de l attribut code application dans habilitations.
     * 
     * @return code application dans habilitations
     */
    public String getCodeApplicationDansHabilitations()
    {
        return codeApplicationDansHabilitations;
    }

    /**
     * Accesseur de l attribut mode authentification.
     * 
     * @return mode authentification
     */
    public String getModeAuthentification()
    {
        return modeAuthentification;
    }

    /**
     * Sets the siremeserviceso.
     *
     * @param siremeserviceso the new siremeserviceso
     */
    public void setSiremeserviceso(SiremeService siremeserviceso)
    {
        this.siremeserviceso = siremeserviceso;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param appctx le nouveau application context
     * @throws BeansException the beans exception
     * @see org.springframework.context.ApplicationContextAware#setApplicationContext(org.springframework.context.ApplicationContext)
     */
    @Override
    public void setApplicationContext(ApplicationContext appctx) throws BeansException
    {
        this.ctx = appctx;

    }

    /**
     * Modificateur de l attribut authentification et lecture habilitations service.
     * 
     * @param authentificationEtLectureHabilitationsService le nouveau authentification et lecture habilitations service
     */
    public void setAuthentificationEtLectureHabilitationsService(
        AuthentificationEtLectureHabilitationsService authentificationEtLectureHabilitationsService)
    {
        this.authentificationEtLectureHabilitationsService = authentificationEtLectureHabilitationsService;
    }

    /**
     * Modificateur de l attribut code application dans habilitations.
     * 
     * @param codeApplicationDansHabilitations le nouveau code application dans habilitations
     */
    public void setCodeApplicationDansHabilitations(String codeApplicationDansHabilitations)
    {
        this.codeApplicationDansHabilitations = codeApplicationDansHabilitations;
    }

    /**
     * Modificateur de l attribut mode authentification.
     * 
     * @param modeAuthentification le nouveau mode authentification
     */
    public void setModeAuthentification(String modeAuthentification)
    {
        this.modeAuthentification = modeAuthentification;
        log.info("mode d'authentification des utilisateurs : " + modeAuthentification);
    }

    /**
     * appel d'un intercepteur avant les habilitations.
     * 
     * @param unePersonneAnnuaire --
     */
    protected void appelerIntercepteurAvantHabilitation(PersonneAnnuaire unePersonneAnnuaire)
    {
        log.debug(">>> Debut methode appelerIntercepteurAvantHabilitation()");
        try
        {
            AvantHabilitationService avanhabilitationserviceso =
                (AvantHabilitationService) ctx.getBean("avanthabilitationservice");
            if (avanhabilitationserviceso != null)
            {
                avanhabilitationserviceso.executerAvantHabilitation(unePersonneAnnuaire);
            }
        }
        catch (NoSuchBeanDefinitionException nsbde)
        {
            log.debug("Exception détectée", nsbde);
        }
    }

    /**
     * extraction des profils Aptera pour construire des objets GrantedAuthority reconnus par springsecurity puis
     * creation d'un objet UtilisateurSecurityLombok (implémentant UserDetails).
     * 
     * @param unePersonneAnnuaire --
     * @param UID --
     * @param motDePasse --
     * @param urlPortail "documenté"
     * @return the utilisateur security lombok
     */
    protected UtilisateurSecurityLombok construitUtilisateurSecurityLombok(PersonneAnnuaire unePersonneAnnuaire,
        String UID, String motDePasse, String urlPortail)
    {
        log.debug(">>> Debut methode construitUtilisateurSecurityLombok()");
        UtilisateurSecurityLombok unUtilisateurSecurityLombok;
        String[] desProfils = unePersonneAnnuaire.rechercherProfils();
        Collection<GrantedAuthority> desDroits = new ArrayList<>();
        for (int i = 0; i < desProfils.length; i++)
        {
            desDroits.add(new SimpleGrantedAuthority(desProfils[i]));
        }
        unUtilisateurSecurityLombok = new UtilisateurSecurityLombok(unePersonneAnnuaire, desDroits, UID, motDePasse, urlPortail);
        return unUtilisateurSecurityLombok;
    }

    /**
     * on enléve de la liste des habilitations de la personne celles qui portent sur un profil qui a été invalidé par
     * SIREME.
     * 
     * @param unePersonneAnnuaire --
     * @return true, if filtrer profils bloques par sireme
     */
    protected boolean filtrerProfilsBloquesParSireme(PersonneAnnuaire unePersonneAnnuaire)
    {
        log.debug(">>> Debut methode filtrerProfilsBloquesParSireme()");
        Set lesProfilsInactifs = new HashSet();
        boolean desProfilsOntEteFiltres = false;

        if (log.isDebugEnabled())
        {
            log.debug("entree filtrerProfilsBloquesParSireme pour uid  " + unePersonneAnnuaire.getUid());
        }

        Set<HabilitationAnnuaire> desHabilitationsASupprimer = new HashSet<>();

        // phase 1 : on recherche les messages actifs à ca jour
        List lesMessages = new ArrayList();

        if (siremeserviceso != null)
        {
            lesMessages =
                siremeserviceso
                    .rechercherListeProfilsADesactiverALaDateHeureCourantePourLApplication();
        }

        if (lesMessages != null && lesMessages.size() > 0)
        {
            int i;
            for (i = 0; i < lesMessages.size(); i++)
            {
                Profil unProfil;
                Message unmessage = (Message) lesMessages.get(i);
                if (unmessage.getLesProfilsDesactives() != null
                    && unmessage.getLesProfilsDesactives().size() > 0)
                {
                    Iterator lesprofilsdesactives = unmessage.getLesProfilsDesactives().iterator();
                    while (lesprofilsdesactives.hasNext())
                    {
                        unProfil = (Profil) (lesprofilsdesactives.next());
                        if (unProfil != null)
                        {
                            lesProfilsInactifs.add(unProfil);
                        }
                    }

                }
            }

        }

        // phase 2 : on enléve les profils inactifs de la liste des profils
        // et on construit la liste des habilitations à supprimer
        if (unePersonneAnnuaire.getListeHabilitations() != null
            && unePersonneAnnuaire.getListeHabilitations().size() > 0)
        {
            Iterator leshabilitations = unePersonneAnnuaire.getListeHabilitations().iterator();
            if (log.isDebugEnabled())
            {
                log.debug("recherche des profils à supprimer dans habilitations de la personne ");
            }
            while (leshabilitations.hasNext())
            {
                HabilitationAnnuaire habilitation = (HabilitationAnnuaire) leshabilitations.next();
                // modif amlp: on obtient le code profil et non le libellé
                String codeProfilHab = habilitation.getNomProfil();

                log.debug("habilitation utilisateur trouvee - profil : " + codeProfilHab);
                boolean profilAConserver = true;

                if (lesProfilsInactifs != null && lesProfilsInactifs.size() > 0)
                {
                    Iterator lesProfilsInactifsIt = lesProfilsInactifs.iterator();
                    Profil unProfil;

                    while (lesProfilsInactifsIt.hasNext())
                    {
                        unProfil = (Profil) (lesProfilsInactifsIt.next());
                        if (unProfil != null)
                        {
                            if (log.isDebugEnabled())
                            {
                                log.debug("profil a supprimer trouvé dans SRM : " + unProfil);
                            }
                            if (unProfil.getCodeProfil().compareTo(codeProfilHab) == 0)
                            {
                                profilAConserver = false;
                                if (log.isDebugEnabled())
                                {
                                    log.debug("profil a eliminer");
                                }
                                break;
                            }
                            else if (log.isDebugEnabled())
                            {
                                log.debug("profil a conserver");
                            }
                        }
                        else if (log.isDebugEnabled())
                        {
                            log.debug("profil null");
                        }
                    }
                    if (profilAConserver == false)
                    {
                        desHabilitationsASupprimer.add(habilitation);
                    }

                }
                else if (log.isDebugEnabled())
                {
                    log.debug("aucun profil a supprimer trouve dans Sireme");
                }
            }

            // phase 3 : on met à jour les habilitations de la personne en supprimant les habilitations à supprimer
            if (desHabilitationsASupprimer.size() > 0)
            {
                int comptagedeshabilitationsinitialesdelapersonne =
                    unePersonneAnnuaire.getListeHabilitations().size();
                Iterator leshabilitationsasupp = desHabilitationsASupprimer.iterator();

                if (log.isDebugEnabled())
                {
                    log.debug("debut suppression des profils bloques");
                }

                while (leshabilitationsasupp.hasNext())
                {
                    HabilitationAnnuaire habilitation = (HabilitationAnnuaire) leshabilitationsasupp.next();
                    unePersonneAnnuaire.getListeHabilitations().remove(habilitation);
                }

                if (comptagedeshabilitationsinitialesdelapersonne > unePersonneAnnuaire.getListeHabilitations()
                    .size())
                {
                    desProfilsOntEteFiltres = true;

                    if (log.isDebugEnabled())
                    {
                        log.debug("des profils ont ete retires à l'utilisateur");
                    }
                }
                else if (log.isDebugEnabled())
                {
                    log
                        .debug("l'utilisateur n'a pas de profil corespondant à ceux bloques par Sireme. Pas de profil retire");
                }
            }

        }
        // }
        return desProfilsOntEteFiltres;

    }

    /**
     * renovoyer exception si plus aune habilitation avec profil correct.
     * 
     * @param desProfilsOntEteFiltres --
     */
    protected void jeterExceptionPasDHabilitation(boolean desProfilsOntEteFiltres)
    {
        if (desProfilsOntEteFiltres)
        {
            throw new HabilitationRegleGestionException(
                "Vos habilitations vous ont été retirées temporairement durant la réalisation de travaux préparatoires");
        }
        else
        {
            throw new HabilitationRegleGestionException("Vous n'avez aucune habilitation sur cette application");
        }
    }

    /**
     * recherche s'il y a un message bloquant sur l application.
     * 
     * @return the message
     */
    protected Message rechercherMessageBloquantSRM()
    {
        log.debug(">>> Debut methode rechercherMessageBloquantSRM()");
        Message messagebloquantBloquant = null;

        if (log.isDebugEnabled())
        {
            log.debug("entree rechercherMessageBloquantSRM ");
        }

        if (siremeserviceso != null)
        {
            messagebloquantBloquant =
                siremeserviceso
                    .rechercherMessageBloquantALaDateHeureCourantePourLApplication();
        }

        if (log.isDebugEnabled())
        {
            log.debug("sortie rechercherMessageBloquantSRM");
        }

        return messagebloquantBloquant;
    }

    /**
     * Cas où on se connecte en utilisant le portail.<br/>
     * Exception si modeAuthentification dans application.properties est le mode appli.
     */
    protected void verifierAuthentificationEnModePortail()
    {
        if (!((modeAuthentification.compareTo(PORTAIL) == 0) || (modeAuthentification.compareTo(APPLIPORTAIL) == 0)))
        {
            throw new AuthentificationRegleGestionException(
                "Mode d'authentification incorrect. Vous devez passer par la page de login de l'application.");
        }
    }

    /**
     * Cas où on se connecte en utilisant le login.<br/>
     * Exception si modeAuthentification dans application.properties est le mode portail
     */
    protected void verifierModeAuthentificationParLApplication()
    {
        if (!((modeAuthentification.compareTo(APPLI) == 0) || (modeAuthentification.compareTo(APPLIPORTAIL) == 0)))
        {
            throw new AuthentificationRegleGestionException(
                "Mode d'authentification incorrect. Vous devez passer par le portail de connexion aux applications.");
        }

    }

    /**
     * renvoyer exception si message bloquant.
     */
    protected void verifierPasDeMessageBloquantSRM()
    {
        if (this.rechercherMessageBloquantSRM() != null)
        {
            throw new AuthentificationRegleGestionException(MESSAGE_BLOQUANT
                + this.rechercherMessageBloquantSRM().getLibelleMessage());
        }

    }

}
