/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : SelectOrderEditor.java
 *
 */

package fr.gouv.finances.lombok.util.propertyeditor;

import java.beans.PropertyDescriptor;
import java.beans.PropertyEditorSupport;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.commons.beanutils.PropertyUtilsBean;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.PredicateUtils;
import org.apache.commons.collections.Transformer;
import org.apache.commons.collections.TransformerUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import fr.gouv.finances.lombok.util.exception.ProgrammationException;

/**
 * Class SelectOrderEditor --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.2 $ Date: 11 déc. 2009
 */
public class SelectOrderEditor extends PropertyEditorSupport
{

    /** Constant : INVALID_PROPERTY. */
    public static final String INVALID_PROPERTY = "La propriété : {0} n''est pas valide dans la classe {1}";

    /** Constant : ALL_NULL. */
    public static final String ALL_NULL = "Tous les objets de la collection sont null";

    /** Constant : METHOD_NULL. */
    public static final String METHOD_NULL = "Impossible de déterminer le nom de la méthode.";

    /** Constant : INCOHERENCE. */
    public static final String INCOHERENCE =
        "La collection utilisée est incohérente, elle contient plusieurs éléments pour lesquels la valeur de {0}.toString() = {1}";

    /** Constant : COLLECTION_NULL. */
    public static final String COLLECTION_NULL = "La collection utilisée n''est pas instanciée";

    /** Constant : ID_SEPARATOR. */
    private static final char ID_SEPARATOR = ';';

    /** Constant : logger. */
    private static final Log logger = LogFactory.getLog(SelectOrderEditor.class);

    /** value property. */
    private final String valueProperty;

    // private final Object items;

    /**
     * Créé une nouvelle instance de l'objet SelectOrderEditor, en utilisant la liste d'items passée en paramètres.
     * Utilisable avec le tag app:select (ex : <app:select itemslist="${listeimpots}" value="codeImpot"
     * label="nomImpot"/>)
     * 
     * @param valueProperty nom de la propriété des objets de la liste qui est utilisée dans l'attribut "value" du tag
     *        select (ex : codeImpot))
     */
    public SelectOrderEditor(String valueProperty)
    {
        super();
        // this.items = items;
        this.valueProperty = valueProperty;
    }

    /**
     * Retourne la chaîne de caractères qui représente l'objet.
     * 
     * @return the as text
     */
    public String getAsText()
    {
        StringBuilder   result = new StringBuilder();
        Object unObjet = getValue();

        // L'objet utilisé par le property editor est de type 'List'
        if (!(unObjet instanceof List))
        {
            throw new ProgrammationException("La propriété mappée par ce property editor doit être de type List");
        }

        List uneListe = (List) unObjet;

        for (Iterator iter = uneListe.iterator(); iter.hasNext();)
        {
            Object unElement = iter.next();
            if (unElement == null)
            {
                // RAS l'élément n'est pas représenté dans la liste
            }
            // L'objet n'est pas null et aucune propriété n'est passée en paramètre. objet.toString() est l'identifiant
            // de l'objet dans la collection
            else if (this.valueProperty == null || StringUtils.isBlank(this.valueProperty))
            {
                //   contrôler que le caractère utilisé n'est pas présent dans la valeur de la propriété
                result.append(unObjet.toString());
            }
            else
            {
                Set<Object> unSet = new HashSet<Object>();
                unSet.add(unElement);
                String methodName = null;

                // lecture de l'accesseur
                methodName = this.getGetterMethodName(unElement, this.valueProperty);

                // Transformeur qui exécute une méthode sur un objet, puis convertit le résultat
                // en String via un appel à toString()
                Transformer unTransformer = TransformerUtils.invokerTransformer(methodName);

                CollectionUtils.transform(unSet, unTransformer);
                Object uneCle = unSet.iterator().next();

                if (uneCle == null)
                {
                    logger.warn("Les identifiants null ne peuvent servir à identifier un élément dans une liste");
                }
                else if (uneCle instanceof String)
                {
                    result.append(uneCle);
                }
                else
                {
                    result.append(uneCle.toString());
                }
            }
            result.append(ID_SEPARATOR);
        }

        if (logger.isDebugEnabled())
        {
            logger.debug("result = " + result);
        }
        return result.toString();
    }

    /**
     * Retourne la liste qui est représentée par la chaîne de texte qui représente l'ordre de la collection le String
     * text.
     * 
     * @param text --
     * @throws IllegalArgumentException the illegal argument exception
     */
    public void setAsText(final String text) throws IllegalArgumentException
    {
        List items = (List) getValue();

        //   tester items not null

        String[] idTab = StringUtils.split(text, ID_SEPARATOR);

        List<Object> uneListeObjet = new ArrayList<Object>(idTab.length);

        for (int i = 0; i < idTab.length; i++)
        {
            Object unObjet = findObjectInCollection(items, idTab[i], this.valueProperty);
            if (unObjet != null)
            {
                uneListeObjet.add(unObjet);
            }
        }

        this.setValue(uneListeObjet);
    }

    /**
     * Recherche un objet dans une collection en utilisant une clé.
     * 
     * @param uneCollection collection dans laquelle l'objet est recherché
     * @param text String qui identifie un objet dans la collection
     * @param valueProperty Propriété portée par les objets de la collection qui est comparée avec
     * @return l'objet sélectionné ou null si aucun objet ne correspond à la clé dans la collection
     */
    private Object findObjectInCollection(final Collection uneCollection, final String text, final String valueProperty)
    {
        Object unObjet = null;
        String methodName = null;
        Transformer unTransformer;

        if (uneCollection == null)
        {
            throw new ProgrammationException(COLLECTION_NULL);
        }

        if (valueProperty != null && StringUtils.isNotBlank(valueProperty))
        {
            methodName = this.getGetterMethodNameFromCollection(uneCollection, valueProperty);

            // Transformeur qui exécute une méthode sur un objet, puis convertit le résultat
            // en String via un appel à toString()
            unTransformer =
                TransformerUtils.chainedTransformer(TransformerUtils.invokerTransformer(methodName), TransformerUtils
                    .stringValueTransformer());
        }
        else
        {
            // Transformeur qui convertit en String via un appel à toString()
            unTransformer = TransformerUtils.stringValueTransformer();
        }

        // Extrait de la liste les éléments pour lesquels la valeur de la propriété "valueProperty"
        // convertie en String est égale au paramètre "text"
        final Collection selectedElements =
            CollectionUtils.select(uneCollection, PredicateUtils.transformedPredicate(unTransformer, PredicateUtils
                .equalPredicate(text)));

        // Si plus d'un élément est sélectionné, on considère que la collection est incohérente
        if (selectedElements.size() > 1)
        {
            MessageFormat msgFormat = new MessageFormat(INCOHERENCE);
            Object[] attr = {methodName, text};

            throw new ProgrammationException(msgFormat.format(attr));
        }

        if (selectedElements.iterator().hasNext())
        {
            unObjet = selectedElements.iterator().next();
        }
        return unObjet;
    }

    /**
     * Retourne le nom de la méthode qui permet de lire la propriété de nom "property" pour un objet "element.
     * 
     * @param element objet sur lequel le getter va être appelé
     * @param property nom de la propriété qui sert pour la construction du getter
     * @return the getter method name
     */
    private String getGetterMethodName(final Object element, final String property)
    {
        Method method = null;
        PropertyUtilsBean propertyutilsbean = new PropertyUtilsBean();
        PropertyDescriptor propDescriptor;

        try
        {
            propDescriptor = propertyutilsbean.getPropertyDescriptor(element, property);
        }
        catch (NoSuchMethodException e)
        {
            logger.warn(e);
            MessageFormat msgFormat = new MessageFormat(INVALID_PROPERTY);
            Object[] attr = {property, element.getClass().getName()};

            throw new ProgrammationException(msgFormat.format(attr), e);
        }
        catch (IllegalAccessException e)
        {
            logger.warn(e);
            throw new ProgrammationException(e);
        }
        catch (InvocationTargetException e)
        {
            logger.warn(e);
            throw new ProgrammationException(e);
        }

        if (propDescriptor == null)
        {
            MessageFormat msgFormat = new MessageFormat(INVALID_PROPERTY);
            Object[] attr = {property, element.getClass().getName()};

            throw new ProgrammationException(msgFormat.format(attr));
        }

        method = propertyutilsbean.getReadMethod(propDescriptor);

        if (method == null)
        {
            MessageFormat msgFormat = new MessageFormat(INVALID_PROPERTY);
            Object[] attr = {property, element.getClass().getName()};

            throw new ProgrammationException(msgFormat.format(attr));
        }

        return method.getName();
    }

    /**
     * Accesseur de l attribut getter method name from collection.
     * 
     * @param uneCollection --
     * @param property --
     * @return getter method name from collection
     */
    private String getGetterMethodNameFromCollection(final Collection uneCollection, final String property)
    {
        Object firstInColl = null;
        String methodName = null;
        boolean allNull = true;

        if (uneCollection == null)
        {
            throw new ProgrammationException(COLLECTION_NULL);
        }

        // Utilise le premier objet non null de la liste pour déterminer le nom de la méthode à utiliser
        // La liste doit être composée d'éléments qui accèdent à la propriété par la même méthode
        Iterator coll = uneCollection.iterator();
        while (coll.hasNext())
        {
            firstInColl = coll.next();
            if (firstInColl != null)
            {
                allNull = false;
                methodName = this.getGetterMethodName(firstInColl, property);
                break;
            }
        }

        if (allNull)
        {
            throw new ProgrammationException(ALL_NULL);
        }

        if (methodName == null)
        {
            throw new ProgrammationException(METHOD_NULL);
        }
        return methodName;
    }
}
