/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : ElementDeMenu.java
 *
 */
package fr.gouv.finances.lombok.menu.techbean;

import fr.gouv.finances.lombok.util.base.BaseTechBean;

/**
 * Classe portant les attributs communs à tous les éléments d'un menu (Groupe de rubriques, Rubriques...)
 * 
 * @author lcontinsouzas-cp
 * @version $Revision: 1.2 $ Date: 15 déc. 2009
 */
public abstract class ElementDeMenu extends BaseTechBean
{

    /** libelle. */
    public String libelle = "";

    /** commentaire. */
    public String commentaire = "";

    /** urlicone. */
    public String urlicone = "";

    /** afficher si rien d autorise. */
    public String afficherSiRienDAutorise = "N";

    /** est affichable. */
    private Boolean estAffichable = null;

    /**
     * Constructeur de la classe ElementDeMenu.java
     */
    public ElementDeMenu()
    {
        super();

    }

    /**
     * methode Calculer est affichable : --.
     * 
     * @param rienDautorise --
     */
    public void calculerEstAffichable(boolean rienDautorise)
    {
        if ((rienDautorise) && (("N").equalsIgnoreCase(this.afficherSiRienDAutorise)))
        {
            estAffichable = Boolean.FALSE;
        }
        else
        {
            estAffichable = Boolean.TRUE;
        }
    }

    /**
     * Accesseur de l attribut est affichable.
     * 
     * @return est affichable
     */
    public boolean getEstAffichable() throws MenuProgrammationException
    {
        if (estAffichable == null)
        {
            throw new MenuProgrammationException(
                "Est affichble n'a pas été calculé - appelez calculerEstaffichable avant");
        }
        return estAffichable.booleanValue();
    }

}
