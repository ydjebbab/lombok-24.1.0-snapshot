/*
 * Copyright (c) 2014 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.clamav.service.impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.net.ConnectException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.SocketChannel;
import java.util.Date;
import java.util.UUID;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.gouv.finances.lombok.clamav.bean.ClamAvResponse;
import fr.gouv.finances.lombok.clamav.bean.TypeResponse;
import fr.gouv.finances.lombok.clamav.exception.ClamAVException;
import fr.gouv.finances.lombok.clamav.service.ActivationClamAV;
import fr.gouv.finances.lombok.clamav.service.ClamAVCheckService;
import fr.gouv.finances.lombok.upload.bean.FichierJoint;

/**
 * Cette classe propose des méthodes pour simplifier les tests antivirus, via ClamAV, sur des fichiers reçus (suite a un
 * upload ou non), envoyés directement comme flux TCP.
 * 
 * @author EffiTIC
 * @author Christophe Breheret-Girardin
 */
public class ClamAVCheckServiceImpl implements ClamAVCheckService
{
    /** Initialisation de la journalisation. */
    protected static final Logger LOGGER = LoggerFactory.getLogger(ClamAVCheckServiceImpl.class);

    /** Délai par défaut pour les réponses en millisecondes. */
    private static final int DEFAULT_TIMEOUT = 1000;

    /** Délai par défaut pour les connexions en millisecondes. */
    private static final int DEFAULT_TIMEOUT_CONNECT = 1000;

    /** Port par défaut. */
    private static final int DEFAULT_PORT = 3310;

    /** Chaine de caractère représentant la commande ClamAV à envoyer au serveur pour l'analyse du flux. */
    private static final String COMMANDE = "nIDSESSION\nnINSTREAM\n";

    /** Contient l'url d'accès au serveur clamAV. */
    private String clamAVServer;

    /**Contient le port d'accès au serveur clamAV. */
    private int clamAVPort = ClamAVCheckServiceImpl.DEFAULT_PORT;

    /** Délai accordé à ClamAV pour répondre en milli secondes. */
    private int timeout = ClamAVCheckServiceImpl.DEFAULT_TIMEOUT;

    /** Délai accordé à ClamAV lors des connexions en milli secondes. */
    private int connectTimeout = ClamAVCheckServiceImpl.DEFAULT_TIMEOUT_CONNECT;

    /** Le contrôle de virus doit-il être effectué ? */
    private ActivationClamAV activationClamAv;

    /**
     * Constructeur
     */
    public ClamAVCheckServiceImpl()
    {
        super();
    }

    @Override
    public ClamAvResponse checkClamAVPing()
    {
        return this.callClamAV(TypeResponse.PING, "END");
    }

    @Override
    public ClamAvResponse checkClamAVVersion()
    {
        return this.callClamAV(TypeResponse.VERSION, "CLamAV");
    }

    @Override
    public ClamAvResponse checkClamAVStats()
    {
        return this.callClamAV(TypeResponse.STATS, "END");
    }

    @Override
    public ClamAvResponse checkByStream(final File fileForTest)
    {
        LOGGER.debug("Détection des virus demandée sur le fichier '{}'", fileForTest.getName());

        final StringBuilder resultat = new StringBuilder();
        ClamAvResponse result = null;

        // Si le service est configuré comme ne devant faire aucun contrôle
        LOGGER.debug("Activation de l'exécution de clamAV : {}", getActivationClamAv().name());
        if (getActivationClamAv().equals(ActivationClamAV.INACTIF))
        {
            return ClamAvResponse.createVirusResponse("1: stream: OK\n");
        }

        // Récupération de la taille du fichier
        final long fileForTestSize = fileForTest.length();
        SocketChannel channel = null;
        try
        {
            LOGGER.debug("Une taille de '{}' octets a été détectée pour le fichier '{}'"
                , fileForTestSize, fileForTest.getAbsolutePath());

            // Pas de contrôle pour un fichier vide
            if (fileForTestSize == 0)
            {
                result = ClamAvResponse.createNoFileResponse();
            }
            else
            {
                // Récupération du flux correspondant au fichier
                final MappedByteBuffer bufFileForTestRead
                    = this.recupFichierStream(fileForTest, fileForTestSize);
                if (bufFileForTestRead == null)
                {
                    return ClamAvResponse.createVirusResponse("1: stream: OK\n");
                }
    
                // Ouverture de la socket
                channel = this.openSocket();
    
                if (channel == null)
                {
                    // Si la socket n'a pu être ouverte, création d'une réponse d'absence de service
                    result = ClamAvResponse.createNoServiceResponse();
                }
                else
                {
                    // Si la socket a été ouverte, envoi du fichier
                    this.readAndSendFile(
                        resultat, fileForTestSize, channel, bufFileForTestRead);
    
                    LOGGER.debug("Réponse reçu de clamAV : '{}'", resultat);
                }
            }
        }
        catch (final SocketTimeoutException e)
        {
            LOGGER.error("Temps de délai dépassé lors de l'appel au serveur ClamAV avec la commande '{}'"
                , ClamAVCheckServiceImpl.COMMANDE, e);
            result = ClamAvResponse.createTimeoutResponse();
        }
        catch (final UnknownHostException uhe)
        {
            LOGGER.error("Erreur lors de l'envoi de la Commande '{}' au serveur clamAV"
                 , ClamAVCheckServiceImpl.COMMANDE, uhe);
            throw new ClamAVException("ERR-TEC-CLAMAV-01", new String[] {uhe.getMessage()}, uhe);
        }
        catch (final IOException ioe)
        {
            LOGGER.error("Erreur d'entrée-sortie lors de l'envoi de la Commande '{}' au serveur clamAV"
                , ClamAVCheckServiceImpl.COMMANDE, ioe);
            throw new ClamAVException("ERR-TEC-CLAMAV-01", new String[] {ioe.getMessage()}, ioe);
        }
        finally
        {
            try
            {
                if (channel != null)
                {
                    LOGGER.trace("Fermeture de la socket");
                    channel.close();
                }
            }
            catch (final IOException ioe)
            {
                LOGGER.error(
                    "Erreur lors de la fermeture de la socket pendant l'envoi de la Commande '{}'"
                        + " au serveur clamAV", ClamAVCheckServiceImpl.COMMANDE, ioe);
            }
        }

        // Recherche de virus
        if ((result == null) && (resultat.length() > 0))
        {
            result = this.traitementReponseClamAV(resultat.toString(), TypeResponse.RECHERCHE_VIRUS);
        }

        return result;
    }

    @Override
    public ClamAvResponse checkByStream(
        final FichierJoint fileForTest)
    {
        LOGGER.trace("Détection des virus demandée sur un fichier joint");

        // Si le service est configuré comme ne devant faire aucun contrôle
        LOGGER.debug("Activation de l'exécution de clamAV : {}", getActivationClamAv().name());
        if (getActivationClamAv().equals(ActivationClamAV.INACTIF))
        {
            return ClamAvResponse.createVirusResponse("1: stream: OK\n");
        }

        // Création d'un fichier à partir du fichier joint
        if (fileForTest.getNomFichierOriginal() == null)
        {
            fileForTest.setNomFichierOriginal("fichier_temporaire_clamav_" + UUID.randomUUID().toString()
                + ".txt");
        }
        LOGGER.trace("Création d'un fichier temporaire '{}' correspondant au fichier joint"
            , fileForTest.getNomFichierOriginal());
        File fichier = new File(fileForTest.getNomFichierOriginal());
        FileOutputStream fos = null;
        try
        {
            LOGGER.trace("Ouverture d'un flux de lecture du fichier joint");
            fos = new FileOutputStream(fichier);
            fos.write(fileForTest.getLeContenuDuFichier().getData());
        }
        catch (IOException ioe)
        {
            LOGGER.error("Problème dans la création du fichier temporaire '{}'"
                , fichier.getAbsolutePath(), ioe);
            throw new ClamAVException("ERR-TEC-CLAMAV-01", new String[] {ioe.getMessage()}, ioe);
        }
        finally
        {
            if (fos != null)
            {
                try
                {
                    LOGGER.trace("Fermeture du flux de lecture du fichier joint");
                    fos.close();
                }
                catch (IOException ioe)
                {
                    LOGGER.error("Problème de fermeture du flux de lecture du fichier joint '{}'. Cause : {}"
                        , fileForTest. getNomFichierOriginal(), ioe);
                }
            }
        }

        try
        {
            return checkByStream(fichier);
        }
        finally
        {
            if (fichier.exists())
            {
                LOGGER.trace("Suppression du fichier temporaire");
                boolean suppression = fichier.delete();
                if (!suppression)
                {
                    LOGGER.info("Suppression du fichier temporaire non effective : {}", fileForTest.getNomFichierOriginal());
                }
            }
        }
    }
    
    @Override
    public ClamAvResponse checkByStream(final byte[] contenu)
    {
        final StringBuilder resultat = new StringBuilder();
        ClamAvResponse result = null;
        
        // Si le service est configuré comme ne devant faire aucun contrôle
        LOGGER.debug("Activation de l'exécution de clamAV : {}", getActivationClamAv().name());
        if (getActivationClamAv().equals(ActivationClamAV.INACTIF))
        {
            return ClamAvResponse.createVirusResponse("1: stream: OK\n");
        }

        // Récupération de la taille du fichier
        final long fileForTestSize = contenu.length;
        SocketChannel channel = null;
        try
        {
            // Récupération du fichier à stream
            final ByteBuffer bufFileForTestRead = ByteBuffer.wrap(contenu);

            // Ouverture de la socket
            channel = this.openSocket();
            if (channel == null)
            {
                result = ClamAvResponse.createNoServiceResponse();
            }
            else
            {
                this.readAndSendFile(
                    resultat, fileForTestSize, channel, bufFileForTestRead);

                LOGGER.debug("Retour clamAV : {}", resultat);
            }
        }
        catch (final SocketTimeoutException ste)
        {
            LOGGER.error("Timeout lors de l'appel ClamAV avec la commande '{}'", COMMANDE, ste);
            result = ClamAvResponse.createTimeoutResponse();
        }
        catch (final UnknownHostException uhe)
        {
            LOGGER.error("Erreur lors de l'envoi de la commande '{}'", COMMANDE, uhe);
            throw new ClamAVException("ERR-TEC-CLAMAV-01", new String[] {uhe.getMessage()}, uhe);
        }
        catch (final IOException ioe)
        {
            LOGGER.error("Erreur lors de l'envoi de la commande '{}'", COMMANDE, ioe);
            throw new ClamAVException("ERR-TEC-CLAMAV-01", new String[] {ioe.getMessage()}, ioe);
        }
        finally
        {
            try
            {
                if (channel != null)
                {
                    channel.close();
                }
            }
            catch (final IOException ioe)
            {
                LOGGER.error("Erreur lors de la fermeture de la socket pendant "
                        + "l'envoi de la Commande '{}' à ClamAV", COMMANDE, ioe);
            }
        }

        // Recherche de Virus
        if ((result == null) && (resultat.length() > 0))
        {
            result = this.traitementReponseClamAV(
                resultat.toString(), TypeResponse.RECHERCHE_VIRUS);
        }

        return result;
    }

    private void readAndSendFile(
        final StringBuilder resultat, final long fileForTestSize,
        final SocketChannel channel,
        final MappedByteBuffer mappedByteBuffer) throws IOException
    {
        readAndSendFile(resultat, fileForTestSize, channel, mappedByteBuffer, null);
    }
    
    private void readAndSendFile(
        final StringBuilder resultat, final long fileForTestSize,
        final SocketChannel channel,
        final ByteBuffer byteBuffer) throws IOException
    {
        readAndSendFile(resultat, fileForTestSize, channel, null, byteBuffer);
    }

    /**
     * Lecture du fichier et envoi sur la socket.
     * 
     * @param resultat resultat
     * @param fileForTestSize fileForTestSize
     * @param channel channel
     * @param bufFileForTestRead bufFileForTestRead
     * @throws IOException IOException
     */
    private void readAndSendFile(
        final StringBuilder resultat, final long fileForTestSize,
        final SocketChannel channel,
        final MappedByteBuffer mappedByteBuffer,
        final ByteBuffer byteBuffer) throws IOException
    {
        LOGGER.trace("Envoi du flux, correspondant au contenu du fichier, sur la socket ouverte");

        // Envoi des commandes 'nIDSESSION' et 'nINSTREAM<length><data>'
        LOGGER.trace("Les commandes 'nIDSESSION' et 'nINSTREAM<length><data>' vont être envoyées sur la socket");

        final ByteBuffer writeReadBuffer = ByteBuffer.allocate(1024);
        writeReadBuffer.put(COMMANDE.getBytes());
        writeReadBuffer.put(this.intToByteArray((int) fileForTestSize));
        writeReadBuffer.flip();
        channel.write(writeReadBuffer);
        LOGGER.trace("Préparation des commandes 'nIDSESSION' et 'nINSTREAM<length>'");

        long size = fileForTestSize;
        LOGGER.debug("Taille du flux : {}", size);
        while (size > 0)
        {
            LOGGER.trace("Ajout du flux du fichier (<data>) à la commande");
            if (mappedByteBuffer != null)
            {
                size -= channel.write(mappedByteBuffer);
            }
            else
            {
                size -= channel.write(byteBuffer);
            }
        }
        final ByteBuffer writeBuffer = ByteBuffer.allocate(4);
        writeBuffer.put(new byte[] {0, 0, 0, 0});
        writeBuffer.flip();
        channel.write(writeBuffer);
        LOGGER.trace("Ecriture de la commande dans la socket");

        // Lecture de la réponse
        final int tailleBuffer = 1024;
        ByteBuffer readBuffer;
        readBuffer = ByteBuffer.allocate(tailleBuffer);
        readBuffer.clear();

        boolean readLine = false;
        while (!readLine)
        {
            final int numReadden = channel.read(readBuffer);
            if (numReadden > 0)
            {
                readLine = readBuffer.get(numReadden - 1) == '\n';
                resultat.append(new String(
                    readBuffer.array(), 0, numReadden));
                readBuffer.clear();
            }
            else
            {
                if (numReadden == -1)
                {
                    readLine = true;
                    readBuffer.clear();
                }
            }
        }
    }

    /**
     * Méthode de traitement de la réponse à renvoyer.
     * 
     * @param resultat le résultat ClamAV
     * @param type "documenté"
     * @return le résultat
     */
    private ClamAvResponse traitementReponseClamAV(
        final String resultat, final TypeResponse type)
    {
        LOGGER.trace("Traitement du retour de clamAV, type = {}", type);
        ClamAvResponse response = null;

        switch (type)
        {
            case PING:
                response = ClamAvResponse.createPingResponse(resultat);
                break;
            case RECHERCHE_VIRUS:
                response = ClamAvResponse.createVirusResponse(resultat);
                break;
            case STATS:
                response = ClamAvResponse.createStatsResponse(resultat);
                break;
            case VERSION:
                response = ClamAvResponse.createVersionResponse(resultat);
                break;
            default:
                break;
        }

        return response;
    }

    /**
     * Ouverture de la socket.
     * 
     * @return la socket
     */
    private SocketChannel openSocket()
    {
        SocketChannel channel = null;
        boolean isErreur = false;
        try
        {
            LOGGER.debug("Ouverture de la socket sur le port '{}' du serveur '{}'"
                , this.clamAVPort, this.clamAVServer);

            channel = SocketChannel.open();
            channel.socket().setSoTimeout(getTimeout());
            channel.configureBlocking(true);
            //channel.connect(new InetSocketAddress(this.clamAVServer, this.clamAVPort));
            channel.socket().connect(new InetSocketAddress(this.clamAVServer, this.clamAVPort)
                , getConnectTimeout());

            LOGGER.trace("Ouverture de la socket OK");

            return channel;
        }
        catch (final IOException ioe)
        {
            LOGGER.error("Echec de la connexion de la socket sur le port '{}' du serveur '{}'. Cause : {}"
                , this.clamAVPort, this.clamAVServer, ioe);
            isErreur = true;

            return null;
        }
        finally
        {
            close(channel, isErreur);
        }
    }

    /**
     * Méthode permettant de fermer le canal en cas d'erreur
     *
     * @param channel canal à éventuellement fermer
     * @param isErreur une erreur a-t-elle été levée ?
     */
    private void close(SocketChannel channel, boolean isErreur)
    {
        if (isErreur && channel != null)
        {
            try
            {
                channel.close();
            }
            catch (IOException ioException)
            {
                LOGGER.error("Erreur lors de la fermeture de la socket sur le port '{}' du serveur '{}'"
                    + ". Cause : {}", this.clamAVPort, this.clamAVServer, ioException);
            }
        }
    }
    
    /**
     * Récupération du fichier à stream.
     * 
     * @param fileForTest fichier de test
     * @param fileForTestSize taille du fichier
     * @return MappedByteBuffer
     * @throws IOException problème de lecture
     */
    private MappedByteBuffer recupFichierStream(
        final File fileForTest, final long fileForTestSize)
        throws IOException
    {
        LOGGER.debug("Préparation de la récupération du flux correspondant au contenu du fichier '{}'"
           , fileForTest.getName());

        FileChannel readChannel = null;
        RandomAccessFile raf = null;
        try
        {
            LOGGER.trace("Initialisation d'un accès au fichier");
            raf = new RandomAccessFile(fileForTest, "r");

            LOGGER.trace("Initialisation d'un canal de lecture du fichier");
            readChannel = raf.getChannel();

            LOGGER.trace("Récupération du flux correspondant au flux du fichier");
            return readChannel.map(FileChannel.MapMode.READ_ONLY, 0, fileForTestSize);
        }
        finally
        {
            if (readChannel != null)
            {
                try
                {
                    LOGGER.trace("Fermeture du canal de lecture du fichier du fichier");
                    readChannel.close();
                }
                catch (final IOException ioe)
                {
                    LOGGER.error("Erreur lors de la fermeture du canal de lecture du fichier '{}'"
                        , fileForTest.getName() , ioe);
                }
            }
            if (raf != null)
            {
                try
                {
                    LOGGER.trace("Fermeture du composant d'accès au fichier");
                    raf.close();
                }
                catch (final IOException ioe)
                {
                    LOGGER.error("Erreur lors de la fermeture du composant d'accès au fichier '{}'"
                        , fileForTest.getName() , ioe);
                }
            }
        }
    }

    /**
     * Cette méthode transforme un entier en un tableau de 4 bytes.
     * 
     * @param value entier à transformer
     * @return le tableau de bytes représentant l'entier non signé
     */
    private byte[] intToByteArray(final int value)
    {
        final int decalage1 = 24;
        final int decalage2 = 16;
        final int decalage3 = 8;
        return new byte[] {(byte) (value >>> decalage1), (byte) (value >>> decalage2),
                (byte) (value >>> decalage3), (byte) value};
    }

    /**
     * Cette méthode permet l'envoie d'une commande à ClamAV et retourne le résultat sous la forme d'une chaine de
     * caractère.
     * 
     * @param command - la commande à transmettre
     * @param strEndDetection - la chaine de caractères permettant de détecter la dernière ligne du retour.
     * @return La réponse de clamAV sans traitement
     */
    private ClamAvResponse callClamAV(
        final TypeResponse command, final String strEndDetection)
    {
        LOGGER.debug("Exécution de la commande '{}'", command);
        ClamAvResponse result;

        final StringBuilder resultat = new StringBuilder();
        final Date dateDebut = new Date();
        Socket socket = null;
        BufferedReader buffer = null;
        InputStream input = null;
        OutputStream ouput = null;

        try
        {
            socket = this.connect(command);
            if (socket == null)
            {
                result = ClamAvResponse.createNoServiceResponse();
            }
            else
            {
                input = socket.getInputStream();
                ouput = socket.getOutputStream();

                // Envoi de la commande à clamAV
                ouput.write(("n" + command.toString() + "\n").getBytes());

                // Attente et traitement de la réponse
                buffer = new BufferedReader(new InputStreamReader(input));

                String retour = "";
                int indexResultat = -1;
                while ((retour != null) && (indexResultat == -1))
                {
                    retour = buffer.readLine();
                    if (retour != null)
                    {
                        indexResultat = retour.indexOf(strEndDetection);
                        resultat.append(retour);
                        resultat.append('\n');
                    }
                }
                if (LOGGER.isDebugEnabled())
                {
                    LOGGER.debug("Reçu '{}' de clamAV en {}ms "
                        , StringUtils.remove(resultat.toString(), "\n")
                        ,  new Date().getTime() - dateDebut.getTime());
                }
                result = this.traitementReponseClamAV(resultat.toString(), command);
            }
        }
        catch (final UnknownHostException e)
        {
            ClamAVCheckServiceImpl.LOGGER.error(
                "Erreur lors de l'envoi de la Commande '" + command
                    + "' à l'antivirus", e);
            throw new ClamAVException(
                "ERR-TEC-CLAMAV-01", new String[] {e.getMessage()}, e);
        }
        catch (final IOException e)
        {
            ClamAVCheckServiceImpl.LOGGER.error(
                "Erreur lors de l'envoi de la Commande '" + command
                    + "' à l'antivirus", e);
            throw new ClamAVException(
                "ERR-TEC-CLAMAV-01", new String[] {e.getMessage()}, e);
        }
        finally
        {
            try
            {
                if (input != null)
                {
                    input.close();
                }
                if (ouput != null)
                {
                    ouput.close();
                }
                this.safeClose(
                    command, socket, buffer);
            }
            catch (IOException ioe)
            {
                LOGGER.error("Erreur lors de fermeture des flux liés à la socket : ", ioe);
            }
        }

        return result;
    }

    /**
     * Fermeture sécurisée de la socket.
     *
     * @param commande commande
     * @param socket socket
     * @param buffer buffer
     */
    private void safeClose(
        final TypeResponse commande, final Socket socket,
        final BufferedReader buffer)
    {
        LOGGER.trace("Fermeture sécurisée de la socket");
        try
        {
            if (buffer != null)
            {
                buffer.close();
            }
        }
        catch (final IOException e)
        {
            LOGGER.error("Problème de fermeture du buffer lors de l'envoi de la commande'{}'"
                    + "' à clamAV", commande, e);
        }
        try
        {
            if (socket != null)
            {
                socket.close();
            }
        }
        catch (final IOException e)
        {
            LOGGER.error("Problème de fermeture de la socket lors de l'envoi de la commande'{}'"
                + "' à clamAV", commande, e);
        }
    }

    /**
     * Ouvre la socket avec délai max.
     * 
     * @param commande commande
     * @return null si le délai imparti à la connexion a été atteint
     * @throws IOException si erreur autre que timeout atteint
     */
    private Socket connect(
        final TypeResponse commande) throws IOException
    {
        LOGGER.trace("Connexion au serveur clamAV");
        LOGGER.debug("Ouverture de la socket pour la commande '{}'", commande);
        
        boolean isErreur = false;

        InetSocketAddress hostport = null;
        Socket socket = null;

        try
        {
            hostport = new InetSocketAddress(this.clamAVServer, this.clamAVPort);
            socket = new Socket();

            LOGGER.debug("Connexion timeout : '{}'", this.connectTimeout);
            LOGGER.debug("Timeout pour l'ensemble des opérations sur la socket : '{}'", this.timeout);

            // Connexion
            socket.connect(hostport, this.connectTimeout);
            // Timeout pour l'ensemble des opérations sur la socket
            socket.setSoTimeout(this.timeout);

            return socket;
        }
        catch (final ConnectException e)
        {
            LOGGER.error("Timeout système lors de la connexion ", e);
            isErreur = true;
            return null;
        }
        catch (final SocketTimeoutException e)
        {
            LOGGER.error("Timeout atteint lors de la connexion ", e);
            isErreur = true;
            return null;
        }
        finally
        {
            close(socket, isErreur);
        }
    }

    /**
     * Méthode permettant de fermer la socket en cas d'erreur
     *
     * @param socket socket à éventuellement fermer
     * @param isErreur une erreur a-t-elle été levée ?
     */
    private void close(Socket socket, boolean isErreur) throws IOException
    {
        if (isErreur && socket != null)
        {
            socket.close();
        }
    }

    /**
     * Getter pour clamAVServer.
     * 
     * @return la valeur de clamAVServer
     */
    public String getClamAVServer()
    {

        return this.clamAVServer;
    }

    /**
     * Setter pour clamAVServer.
     * 
     * @param clamAVServer le serveur
     */
    public void setClamAVServer(
        final String clamAVServer)
    {

        this.clamAVServer = clamAVServer;
    }

    /**
     * Getter pour clamAVPort.
     * 
     * @return la valeur de clamAVPort
     */
    public int getClamAVPort()
    {
        return this.clamAVPort;
    }

    /**
     * Setter pour clamAVPort.
     * 
     * @param clamAVPort la valeur du port
     */
    public void setClamAVPort(
        final int clamAVPort)
    {
        this.clamAVPort = clamAVPort;
    }

    /**
     * Getter pour timeout.
     * 
     * @return la valeur de timeout
     */
    public int getTimeout()
    {
        return this.timeout;
    }

    /**
     * Setter pour timeout.
     * 
     * @param readTimeout la valeur du timeout
     */
    public void setTimeout(final int readTimeout)
    {
        this.timeout = readTimeout;
    }

    /**
     * Getter pour connectTimeout.
     * 
     * @return la valeur de connectTimeout
     */
    public int getConnectTimeout()
    {

        return this.connectTimeout;
    }

    /**
     * Setter pour connectTimeout.
     * 
     * @param connectTimeout connectTimeout
     */
    public void setConnectTimeout(
        final int connectTimeout)
    {
        this.connectTimeout = connectTimeout;
    }

    /**
     * Accesseur de activationClamAv
     *
     * @return activationClamAv
     */
    public ActivationClamAV getActivationClamAv()
    {
        return activationClamAv;
    }

    /**
     * Mutateur de activationClamAv
     *
     * @param activationClamAv activationClamAv
     */
    public void setActivationClamAv(ActivationClamAV activationClamAv)
    {
        this.activationClamAv = activationClamAv;
    }

}
