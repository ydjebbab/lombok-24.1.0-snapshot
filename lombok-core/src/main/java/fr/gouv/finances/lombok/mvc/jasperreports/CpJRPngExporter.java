/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : CpJRPngExporter.java
 *
 */
package fr.gouv.finances.lombok.mvc.jasperreports;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.OutputStream;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.ImageOutputStream;

import com.sun.imageio.plugins.png.PNGMetadata;

import fr.gouv.finances.lombok.util.exception.ProgrammationException;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.export.JRGraphics2DExporter;
import net.sf.jasperreports.engine.export.JRGraphics2DExporterParameter;
import net.sf.jasperreports.engine.export.draw.PrintDrawVisitor;
import net.sf.jasperreports.renderers.RenderersCache;

//import net.sf.jasperreports.engine.export.legacy.BorderOffset;

/**
 * Class CpJRPngExporter --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.2 $ Date: 11 déc. 2009
 */
public class CpJRPngExporter extends JRGraphics2DExporter
{

    /** Constant : DEFAULT_ZOOM. */
    private static final float DEFAULT_ZOOM = 1f;

    /**
     * Instanciation de cp jr png exporter.
     * 
     * @throws JRException the JR exception
     */
    public CpJRPngExporter() throws JRException
    {
        super();
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @throws JRException the JR exception
     * @see net.sf.jasperreports.engine.export.JRGraphics2DExporter#exportReport()
     */
    /*
     * public void exportReport() throws JRException { progressMonitor = (JRExportProgressMonitor)
     * parameters.get(JRExporterParameter.PROGRESS_MONITOR); setOffset(false); try { setExportContext(); setInput();
     * setPageRange(); setTextRenderer(); BorderOffset.setLegacy(JRProperties.getBooleanProperty(jasperPrint,
     * BorderOffset.PROPERTY_LEGACY_BORDER_OFFSET, false)); Float zoomRatio = (Float)
     * parameters.get(JRGraphics2DExporterParameter.ZOOM_RATIO); if (zoomRatio != null) { zoom = zoomRatio.floatValue();
     * if (zoom <= 0) { throw new JRException("Invalid zoom ratio : " + zoom); } } else { zoom = DEFAULT_ZOOM; }
     * OutputStream os = (OutputStream) parameters.get(JRExporterParameter.OUTPUT_STREAM); int w = (int)
     * (jasperPrint.getPageWidth() * zoom); int h = (int) (jasperPrint.getPageHeight() * zoom); BufferedImage img = new
     * BufferedImage(w, h, BufferedImage.TYPE_INT_RGB); grx = img.createGraphics(); if (grx == null) { throw new
     * JRException("No output specified for the exporter. java.awt.Graphics2D object expected."); } setDrawers();
     * exportReportToGraphics2D(); img.flush(); float resolution = 72 * zoom; PNGMetadata png = new PNGMetadata(); int
     * resX = (int) Math.ceil(resolution / 0.0254f); png.pHYs_pixelsPerUnitXAxis = resX; png.pHYs_pixelsPerUnitYAxis =
     * resX; png.pHYs_unitSpecifier = 1; png.pHYs_present = true; // Set the compression quality and other header //
     * information ImageWriteParam iwparam = new ImageWriteParam() { public int getProgressiveMode() { return
     * MODE_COPY_FROM_METADATA; } }; IIOImage iioImage = new IIOImage(img, null, png); ImageOutputStream
     * imageoutbufstreal = ImageIO.createImageOutputStream(os); ImageWriter writerbuf =
     * ImageIO.getImageWritersByFormatName("png").next(); writerbuf.setOutput(imageoutbufstreal); writerbuf.write(png,
     * iioImage, iwparam); writerbuf.dispose(); } catch (IOException e) { throw new ProgrammationException(e); } finally
     * { resetExportContext(); } }
     */
    /**
     * (methode de remplacement) est remodelée suite passage jasper5.5.2 {@inheritDoc}
     * 
     * @see net.sf.jasperreports.engine.export.JRGraphics2DExporter#exportReport()
     */
    @Override
    public void exportReport() throws JRException
    {
        try
        {
            ensureJasperReportsContext();
            ensureInput();

            initExport();

            // ensureOutput();
            Float zoom;
            Float zoomRatio = (Float) parameters.get(JRGraphics2DExporterParameter.ZOOM_RATIO);
            if (zoomRatio != null)
            {
                zoom = zoomRatio.floatValue();
                if (zoom <= 0)
                {
                    throw new JRException("Invalid zoom ratio : " + zoom);
                }
            }
            else
            {
                zoom = DEFAULT_ZOOM;
            }

            OutputStream os = (OutputStream) parameters.get(JRExporterParameter.OUTPUT_STREAM);

            int w = (int) (jasperPrint.getPageWidth() * zoom);
            int h = (int) (jasperPrint.getPageHeight() * zoom);

            BufferedImage img = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);

            Graphics2D grx = img.createGraphics();
            if (grx == null)
            {
                throw new JRException("No output specified for the exporter. java.awt.Graphics2D object expected.");
            }
            drawVisitor = new PrintDrawVisitor(exporterContext.getJasperReportsContext(), 
                new RenderersCache(exporterContext.getJasperReportsContext()), true, false);
                
            // setDrawers();
            exportReportToGraphics2D(grx);

            img.flush();

            float resolution = 72 * zoom;
            PNGMetadata png = new PNGMetadata();
            int resX = (int) Math.ceil(resolution / 0.0254f);
            png.pHYs_pixelsPerUnitXAxis = resX;
            png.pHYs_pixelsPerUnitYAxis = resX;
            png.pHYs_unitSpecifier = 1;
            png.pHYs_present = true;

            // Set the compression quality and other header
            // information
            ImageWriteParam iwparam = new ImageWriteParam()
            {
                public int getProgressiveMode()
                {
                    return MODE_COPY_FROM_METADATA;
                }
            };

            IIOImage iioImage = new IIOImage(img, null, png);
            ImageOutputStream imageoutbufstreal = ImageIO.createImageOutputStream(os);
            ImageWriter writerbuf = ImageIO.getImageWritersByFormatName("png").next();
            writerbuf.setOutput(imageoutbufstreal);
            writerbuf.write(png, iioImage, iwparam);
            writerbuf.dispose();

        }
        catch (IOException e)
        {
            throw new ProgrammationException(e);
        }
        finally
        {
            resetExportContext();
        }
    }
}
