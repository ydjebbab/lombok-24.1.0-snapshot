/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 */
package fr.gouv.finances.lombok.test.controle;

import static org.junit.Assert.fail;
import static org.junit.Assert.assertEquals;
import fr.gouv.finances.lombok.util.exception.IngerableException;

/**
 * Utilitaire de vérification de l'exécution d'une méthode.
 *
 * @author Christophe Breheret-Girardin
 */
public final class VerificationExecution
{

    private VerificationExecution()
    {
        super();
    }

    /**
     * Méthode permettant de tester qu'une méthode exécutée lève bien une exception. 
     *
     * @param exceptionAttendue exception levée par l'exécution de la méthode (comportement attendu)
     * @param executable traitement à exécuter 
     */
    public static void verifierException(Class<? extends Throwable> exceptionAttendue, Executable executable)
    {
        try
        {
            // Execution du traitement
            executable.execute();

            // Une exception aurait dû être remontée
            echouer(exceptionAttendue);
        }
        catch (Throwable exceptionLevee)
        {
            // Vérification de l'exception
            verifierException(exceptionAttendue, exceptionLevee);
        }
    }

    /**
     * Méthode permettant de tester qu'une méthode exécutée lève bien une exception 
     * (héritant d'IngerableException). 
     *
     * @param exceptionAttendue exception levée par l'exécution de la méthode (comportement attendu)
     * @param element élément notable lié à l'erreur
     * @param executable traitement à exécuter 
     */
    public static void verifierException(Class<? extends IngerableException> exceptionAttendue
        , String element, Executable executable)
    {
        try
        {
            // Execution du traitement
            executable.execute();

            // Une exception aurait dû être remontée
            echouer(exceptionAttendue);
        }
        catch (Throwable exceptionLevee)
        {
            // Vérification de l'exception
            verifierException(exceptionAttendue, exceptionLevee);

            // Vérification de l'élément notable
            assertEquals(element, ((IngerableException) exceptionLevee).getElement());
        }
    }

    /**
     * Méthode permettant de faire échouer le test.
     *
     * @param exception exception attendue mais qui n'a pas été levée
     */
    private static void echouer(Class<?> exception)
    {
        fail("Une exception '" + exception + "' aurait dû être levée");
    }

    /**
     * Méthode permettant de vérifier une exception levée avec une exception attendue
     *
     * @param exceptionAttendue exception attendue
     * @param exceptionLevee exception levée
     */
    private static void verifierException(Class<?> exceptionAttendue, Throwable exceptionLevee)
    {
        if (!exceptionAttendue.isInstance(exceptionLevee))
        {
            fail("Exception '" + exceptionAttendue + "' attendue mais exception '"
                + exceptionLevee.getClass() + "' levée");
        }
    }
}