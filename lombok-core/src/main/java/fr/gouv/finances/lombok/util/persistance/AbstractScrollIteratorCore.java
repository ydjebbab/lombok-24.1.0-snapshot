/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 *
 */
package fr.gouv.finances.lombok.util.persistance;

import java.util.NoSuchElementException;

/**
 * Socle de l'implémentation d'un itérateur en mode curseur.
 *
 * @author Christophe Breheret-Girardin
 */
public abstract class AbstractScrollIteratorCore implements ScrollIterator
{
    /** L'itérateur est-il fermé ? */
    protected boolean isClosed = false;

    /** Le prochain objet à retourner. */
    protected Object nextRow = null;

    /**
     * Constructeur.
     */
    public AbstractScrollIteratorCore()
    {
        super();
    }

    /** 
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.util.persistance.ScrollIterator#isClosed()
     */
    @Override
    public boolean isClosed()
    {
        return isClosed;
    }

    /** 
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.util.persistance.ScrollIterator#next()
     */
    @Override
    public Object next()
    {
        // S'il n'y a plus d'élément à récupérer, levée d'une exception
        if (!hasNext())
        {
            throw new NoSuchElementException();
        }

        // Récupération de l'élément suivant
        Object result = nextRow;
        nextRow = null;
        return result;
    }

    /** 
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.util.persistance.ScrollIterator#nextObjetMetier()
     */
    @Override
    public Object nextObjetMetier()
    {
        Object resultnext = this.next();
        Object result = resultnext;

        if (resultnext instanceof Object[])
        {
            Object[] tabresult = ((Object[]) resultnext);
            result = tabresult[0];
        }
        return result;
    }

    /** 
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.util.persistance.ScrollIterator#remove()
     */
    @Override
    public void remove()
    {
        // Non implémentée
        throw new UnsupportedOperationException();
    }

    /** 
     * {@inheritDoc}
     * @throws Throwable 
     * @see java.lang.Object#finalize()
     */
    @Override
    protected void finalize() throws Throwable
    {
        close();
        super.finalize();
    }
}