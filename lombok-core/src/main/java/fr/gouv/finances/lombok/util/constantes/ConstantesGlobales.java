/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : ConstantesGlobales.java
 *
 */
package fr.gouv.finances.lombok.util.constantes;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.net.URLConnection;
import java.util.Enumeration;
import java.util.Properties;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.gouv.finances.lombok.util.exception.ExploitationException;
import fr.gouv.finances.lombok.util.exception.ProgrammationException;

/**
 * Class ConstantesGlobales 
 * 
 * @author amleplatinec
 * @version $Revision: 1.3 $ Date: 11 déc. 2009
 */
@Deprecated
public class ConstantesGlobales
{
    private static final Logger logger = LoggerFactory.getLogger(ConstantesGlobales.class);
    
    /**
     * Constant : CONF_DIR_PARAM. Nom du paramètre système à utiliser en paramètre de la JVM pour indiquer la
     * localisation du répertoire contenant les fichiers de configuration de l'application (*.properties). Par défaut
     * (si ce paramètre n'est pas utilisé, les fichiers de configuration sont recherchés dans le classpath. exemples :
     * -Dconf_dir=/app/appli/conf, -Dconf_dir=/D:/conf
     */
    public static final String CONF_DIR_PARAM = "spring.config.location";

    public static final String LOCALISATION_BUNDLE_NAME = "localisations";

    public static final ResourceBundle LOCALISATION_RESOURCE_BUNDLE = identifieLocalisation();

    public static final String LOCALISATION_CLE_BATCH_ROOT = "appli.batch.root";

    public static final String LOCALISATION_CLE_WEB_ROOT = "appli.web.root";

    public static final String LOCALISATION_CLE_COMMUN_ROOT = "appli.commun.root";

    public static final String CONTEXTE_SUFFIXE = "context";

    public static final String IMPL_SUFFIXE = "impl";

    public static final String WEB_ROOT_PATH = LOCALISATION_RESOURCE_BUNDLE.getString(LOCALISATION_CLE_WEB_ROOT);

    public static final String BATCH_ROOT_PATH = LOCALISATION_RESOURCE_BUNDLE.getString(LOCALISATION_CLE_BATCH_ROOT);


    public static final String LOG4JPROPERTIES_PATH = Thread.currentThread().getContextClassLoader().getResource("log4j2.properties")
        .getFile();

    public static final String APPLIPROPERTIES_PATH = localiserParEnvironnementOuClasspath("application.properties");

    public static final String COMMUNPROPERTIES_PATH = determineCheminCommunProperties();

    public static final String TESTPROPERTIES_PATH = localiserParEnvironnementOuClasspath("test.properties");

    public static final String BATCHPROPERTIES_PATH = localiserParEnvironnementOuClasspath("application.properties");


    /**
     * methode Determine chemin commun properties 
     *
     * @return string
     */
    public static String determineCheminCommunProperties()
    {
        String locaDir = System.getProperty(CONF_DIR_PARAM);
        if (StringUtils.isBlank(locaDir))
        {
            return LOCALISATION_RESOURCE_BUNDLE.getString(LOCALISATION_CLE_COMMUN_ROOT) + "/properties/commun.properties";
        }
        else
            return locaDir + "/commun.properties";
    }

    /**
     * methode Determine repertoire configuration.
     *
     * @return string
     */
    public static final String determineRepertoireConfiguration()
    {
        return determineUrlRepertoireConfiguration().getPath();
    }

    /**
     * methode Determine ur iexternal form repertoire configuration .
     *
     * @return string
     */
    public static final String determineURIexternalFormRepertoireConfiguration()
    {
        return determineUrlRepertoireConfiguration().toExternalForm();
    }

    /**
     * methode Determine url repertoire configuration .
     *
     * @return url
     */
    public static final URL determineUrlRepertoireConfiguration()
    {
        URL result = null;

        String locaURL = System.getProperty(CONF_DIR_PARAM);

        if (StringUtils.isBlank(locaURL))
        {
            ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
            result = classLoader.getResource("/");

            if (result == null)
            {
                try
                {
                    result = new File(ClassLoader.getSystemResource("").getFile()).toURI().toURL();
                }
                catch (MalformedURLException mae)
                {
                    throw new ProgrammationException(mae);
                }
            }
        }
        else
        {
            try
            {
                
                result = new URL(locaURL);
            }
            catch (MalformedURLException male)
            {
                StringBuilder msg = new StringBuilder();
                msg.append("localisations_uri incorrecte [");
                msg.append(locaURL);
                msg.append(']');
  
                logger.error(male.getMessage(), male);
                throw new ExploitationException(msg.toString(), male);
            }
        }

        return result;

    }

    /**
     * methode Identifie localisation .
     *
     * @return resource bundle
     */
    public static final ResourceBundle identifieLocalisation()
    {
        ResourceBundle bundle = null;

        if (StringUtils.isNotBlank(System.getProperty(CONF_DIR_PARAM)))
        {
            String locaDir = System.getProperty(CONF_DIR_PARAM) + "/";

            URL[] urls = new URL[1];

            try
            {
                File file = new File(locaDir);
                urls[0] = file.toURI().toURL();
            }
            catch (MalformedURLException male)
            {
                StringBuilder msg = new StringBuilder();
                msg.append("localisations_uri incorrecte [");
                msg.append(locaDir);
                msg.append(']');
                throw new ExploitationException(msg.toString(), male);
            }
            ClassLoader loader = new URLClassLoader(urls, null);

            Properties properties = new Properties();
            Enumeration<URL> urlsre;
            try
            {
                urlsre = loader.getResources(LOCALISATION_BUNDLE_NAME + ".properties");
                while (urlsre.hasMoreElements())
                {
                    URL url = (URL) urlsre.nextElement();
                    InputStream is = null;
                    try
                    {
                        URLConnection con = url.openConnection();
                        con.setUseCaches(false);
                        is = con.getInputStream();
                        bundle = new PropertyResourceBundle(is);
                        properties.load(is);
                    }
                    finally
                    {
                        if (is != null)
                        {
                            is.close();
                        }
                    }

                }

            }
            catch (IOException ioe)
            {

                logger.error(ioe.getMessage(), ioe);
                throw new ExploitationException("", ioe);
            }

        }
        else
        {
            bundle = ResourceBundle.getBundle(LOCALISATION_BUNDLE_NAME);
        }

        return bundle;

    }

    /**
     * methode Localiser par environnement ou classpath .
     *
     * @param nomFichier
     * @return string
     */
    public static final String localiserParEnvironnementOuClasspath(String nomFichier)
    {

        String locaDir = System.getProperty(CONF_DIR_PARAM);

        if (StringUtils.isBlank(locaDir))
        {
            URL url = Thread.currentThread().getContextClassLoader().getResource(nomFichier);
            if (url == null)
            {
                String msgError = "Fichier " + nomFichier + " introuvable dans le classpath";
             
                // petite rustine en attendant de réécrire ConstantesGlobales et ResolveurDeLocalisations
                if (!nomFichier.contains("test.properties"))
                {

                    logger.error(msgError);
                    throw new ExploitationException(msgError);
                }
                else
                {
                    logger.warn(msgError);
                }
                return null;
            }
            else
            {
                return url.getFile();
            }
        }
        else
        {
            File file = new File(locaDir + "/" + nomFichier);
            if (!file.exists())
            {
                String msgError = "Fichier " + nomFichier + " introuvable dans le répertoire de configuration: " + locaDir;
                logger.error(msgError);
                throw new ExploitationException(msgError);
            }
            return file.getPath();
        }
    }
}