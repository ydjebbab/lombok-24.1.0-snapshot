/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : AvantGenerationMenuService.java
 *
 */
package fr.gouv.finances.lombok.menu.service;

import fr.gouv.finances.lombok.menu.techbean.BarreDeMenusHierarchiques;

/**
 * Interface AvantGenerationMenuService --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.2 $ Date: 11 déc. 2009
 */
public interface AvantGenerationMenuService
{

    /**
     * methode Executer apres filtrage menu : --.
     * 
     * @param uneBarreDeMenusDynamique --
     * @param parametreIntercepteur --
     */
    void executerApresFiltrageMenu(BarreDeMenusHierarchiques uneBarreDeMenusDynamique, Object parametreIntercepteur);

}
