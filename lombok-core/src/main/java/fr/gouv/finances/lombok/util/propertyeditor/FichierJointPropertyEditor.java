/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : FichierJointPropertyEditor.java
 *
 */
package fr.gouv.finances.lombok.util.propertyeditor;

import java.beans.PropertyEditorSupport;
import java.io.IOException;
import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.multipart.MultipartFile;

import fr.gouv.finances.lombok.upload.bean.FichierJoint;

/**
 * Class FichierJointPropertyEditor --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.2 $ Date: 11 déc. 2009
 */
public class FichierJointPropertyEditor extends PropertyEditorSupport
{

    /** Constant : log. */
    private static final Log LOG = LogFactory.getLog(FichierJointPropertyEditor.class);

    /**
     * Instanciation de fichier joint property editor.
     */
    public FichierJointPropertyEditor()
    {
        super();
    }

    /**
     * Retourne le nom du fichier joint.
     * 
     * @return Nom du fichier joint
     */
    public String getAsText()
    {
        final String result;
        final Object value = getValue();

        if (value == null)
        {
            result = "";
        }
        else
        {
            result = ((FichierJoint) getValue()).getNomFichierOriginal();
        }
        return result;

    }

    /**
     * Surdéfinit la méthode setAsText de façon à ce qu'aucune exception ne soit levée.
     * 
     * @param text --
     * @throws IllegalArgumentException the illegal argument exception
     */
    public void setAsText(String text) throws IllegalArgumentException
    {
    }

    /**
     * Place le Document dans le property editor.
     * 
     * @param value Le Document ou un MultipartFile
     */
    public void setValue(Object value)
    {
        this.trace(value);

        if (value == null || value instanceof FichierJoint)
        {
            super.setValue(value);
        }
        else if (value instanceof MultipartFile)
        {

            if (((MultipartFile) value).getSize() == 0)
            {
                throw new IllegalArgumentException("Aucun fichier à transférer");
            }

            final FichierJoint unfichierjoint = new FichierJoint();
            unfichierjoint.setNomFichierOriginal(((MultipartFile) value).getOriginalFilename());
            unfichierjoint.setTailleFichier(((MultipartFile) value).getSize());
            unfichierjoint.setTypeMimeFichier(((MultipartFile) value).getContentType());
            unfichierjoint.setDateHeureSoumission(new Date());
            try
            {
                unfichierjoint.getLeContenuDuFichier().setData(((MultipartFile) value).getBytes());
            }
            catch (IOException exception)
            {
                LOG.error("Lecture du fichier impossible", exception);
                throw new IllegalArgumentException("Lecture du fichier impossible: " + exception.getMessage(), exception);
            }
            super.setValue(unfichierjoint);
        }
        else
        {
            final String msg =
                "L'objet être du type FichierJoint ou MultipartFile. Le type " + value.getClass().getName()
                    + "n'est pas supporté par ce propertyEditor";
            LOG.error(msg);
            throw new IllegalArgumentException(msg);
        }
    }

    /**
     * methode Trace : --.
     * 
     * @param value --
     */
    private void trace(final Object value)
    {
        if (LOG.isDebugEnabled())
        {
            if (value == null)
            {
                LOG.debug("Object = null");
            }
            else
            {
                LOG.debug("Object = " + value.getClass().getName());
            }
        }
    }
}
