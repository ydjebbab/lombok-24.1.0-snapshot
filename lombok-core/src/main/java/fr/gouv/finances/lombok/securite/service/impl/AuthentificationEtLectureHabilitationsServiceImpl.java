/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : AuthentificationEtLectureHabilitationsServiceImpl.java
 *
 */
package fr.gouv.finances.lombok.securite.service.impl;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;

import javax.naming.AuthenticationException;
import javax.naming.Context;
import javax.naming.NamingException;
import javax.naming.directory.DirContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.gouv.finances.lombok.securite.service.AuthentificationEtLectureHabilitationsService;
import fr.gouv.finances.lombok.securite.service.LecturePersonneService;
import fr.gouv.finances.lombok.securite.service.RessourceAnnuaireService;
import fr.gouv.finances.lombok.securite.techbean.AuthentificationRegleGestionException;
import fr.gouv.finances.lombok.securite.techbean.HabilitationAnnuaire;
import fr.gouv.finances.lombok.securite.techbean.HabilitationRegleGestionException;
import fr.gouv.finances.lombok.securite.techbean.PersonneAnnuaire;
import fr.gouv.finances.lombok.util.base.BaseServiceImpl;
import fr.gouv.finances.lombok.util.exception.ProgrammationException;
import fr.gouv.finances.lombok.util.format.FormaterDate;

/**
 * Classe authentification et lecture des habilitations APTERA et caractéristiques d'une personne <br>
 * dans l'annuaire DGCP.
 * 
 * @author chouard-cp
 * @version $Revision: 1.6 $ Date: 15 déc. 2009
 */
public class AuthentificationEtLectureHabilitationsServiceImpl extends BaseServiceImpl implements
    AuthentificationEtLectureHabilitationsService
{
    private static final Logger log = LoggerFactory.getLogger(AuthentificationEtLectureHabilitationsServiceImpl.class);
    /*
     * ****************** messages d'erreurs * ******************.
     */

    /** Constant : AUCUNE_HABILITATION. */
    private static final String AUCUNE_HABILITATION = "Aucune habilitation pour cette application";

    /** Constant : MOT_DE_PASSE_ERRONE. */
    private static final String MOT_DE_PASSE_ERRONE = "Mot de passe errone";

    /** Constant : MOT_DE_PASSE_MANQUANT. */
    private static final String MOT_DE_PASSE_MANQUANT = "Mot de passe manquant";

    /** Constant : UID_MANQUANT. */
    private static final String UID_MANQUANT = "UID manquant";

    /** Constant : UID_ERRONE. */
    private static final String UID_ERRONE = "Identifiant erroné";

    /** Constant : DGFIP. */
    private static final String DGFIP = "DGFIP";

    /** USAGERS - String,. */
    private static final String USAGERS = "USAGERS";

    /** Constant : suffixeusagers. */
    private static final String suffixeusagers = "ou=particuliers,ou=mefi,o=gouv,c=fr";

    /* acces au service lecture personne annuaire */
    /** lecturepersonneso. */
    LecturePersonneService lecturepersonneso;

    /**
     * Constructeur de la classe AuthentificationEtLectureHabilitationsServiceImpl.java
     */
    public AuthentificationEtLectureHabilitationsServiceImpl()
    {
        super();

    }

    /*
     * Type d'authentification de l'utilisateur ("simple" ou "strong") utilisé pour vérifier l'identifiant / mot de
     * passe sur l'annuaire
     */
    /** type authentification ldap. */
    private final String typeAuthentificationLDAP = "simple";

    /** ressourceannuaireso. */
    private RessourceAnnuaireService ressourceannuaireso;

    /** typeAnnuaire - String,attribut qui déterminera si annuaire fusionné (si vaut DGFIP sinon filiere ex dgcp). */
    private String typeAnnuaire;

    /**
     * (methode de remplacement) {@inheritDoc} évolution vers l'annuiare fusionné amlp sept 2011.
     * 
     * @param uid "documenté"
     * @param motDePasse "documenté"
     * @return string
     * @see fr.gouv.finances.lombok.securite.service.AuthentificationEtLectureHabilitationsService#authentifier(java.lang.String,
     *      java.lang.String)
     */
    @Override
    public String authentifier(String uid, String motDePasse)
    {
        String dn;

        if ((motDePasse == null) || (motDePasse.length() == 0))
        {
            throw new AuthentificationRegleGestionException(MOT_DE_PASSE_MANQUANT);
        }
        if ((uid == null) || (uid.length() == 0))
        {
            throw new AuthentificationRegleGestionException(UID_MANQUANT);
        }

        /* recherche du DN de l'utilisateur dans l'arborescence annuaire */

        // cas annuaire fusionné connexion via prenom.nom
        if (this.getTypeAnnuaire() != null && this.getTypeAnnuaire().compareToIgnoreCase(DGFIP) == 0)
        {
            dn = this.lecturepersonneso.rechercherDNCompletUtilisateurPourUnUidFonctionnel(uid);
        }

        else if (this.getTypeAnnuaire() != null && this.getTypeAnnuaire().compareToIgnoreCase(USAGERS) == 0)
        {
            dn = this.lecturepersonneso.rechercherDNCompletUtilisateurPourUnUID(uid);
            return dn;
        }
        else
        // cas annuaire ex dgcp connexion via xx-cp
        {
            dn = this.lecturepersonneso.rechercherDNCompletUtilisateurPourUnUID(uid);
        }

        if (dn != null)
        {
            this.testerLDAPConnexionUtilisateur(dn, motDePasse);
        }
        else
        {
            throw new AuthentificationRegleGestionException(UID_ERRONE);
        }
        return dn;

    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param uid "documenté"
     * @param motDePasse "documenté"
     * @param application "documenté"
     * @return personne annuaire
     * @see fr.gouv.finances.lombok.securite.service.AuthentificationEtLectureHabilitationsService#authentifierEtRechercherHabilitation(java.lang.String,
     *      java.lang.String, java.lang.String)
     */
    @Override
    public PersonneAnnuaire authentifierEtRechercherHabilitation(String uid, String motDePasse, String application)
    {

        PersonneAnnuaire unePersonneAnnuaire = null;
        String dn = this.authentifier(uid, motDePasse);

        if (estUserNameDOrigineParticuliers(dn))
        {
            unePersonneAnnuaire = this.rechercherPersonneAnnuaireParticuliersAvecHabilitationParDN(dn);
        }

        else
        {
            unePersonneAnnuaire = this.rechercherPersonneAnnuaireAvecHabilitationParDN(dn, application);

        }
        return unePersonneAnnuaire;
    }

    /**
     * Accesseur de l attribut lecturepersonneso.
     * 
     * @return lecturepersonneso
     */
    public LecturePersonneService getLecturepersonneso()
    {
        return lecturepersonneso;
    }

    /**
     * Accesseur de l attribut ressourceannuaireso.
     * 
     * @return ressourceannuaireso
     */
    public RessourceAnnuaireService getRessourceannuaireso()
    {
        return ressourceannuaireso;
    }

    /**
     * Accesseur de l attribut typeAnnuaire.
     * 
     * @return the typeAnnuaire - String,attribut qui déterminera si annuaire fusionné (si vaut DGFIP sinon filiere ex
     *         dgcp)
     */
    public String getTypeAnnuaire()
    {
        return typeAnnuaire;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param dn "documenté"
     * @param application "documenté"
     * @return personne annuaire
     * @see fr.gouv.finances.lombok.securite.service.AuthentificationEtLectureHabilitationsService#rechercherPersonneAnnuaireAvecHabilitationParDN(java.lang.String,
     *      java.lang.String)
     */
    @Override
    public PersonneAnnuaire rechercherPersonneAnnuaireAvecHabilitationParDN(String dn, String application)
    {
        PersonneAnnuaire personneAnnuaire = null;
        if (dn != null)
        {
            boolean estHabilitee;

            if (log.isDebugEnabled())
            {
                log.debug("dn personne à rechercher : " + dn);
            }

            /* si le code application est null, une exception est renvoyee */
            if (application == null)
            {
                throw new ProgrammationException(
                    "Le code application est obligatoire pour tester l'habilitation de la personne correspondant au dn "
                        + dn);
            }

            personneAnnuaire = this.lecturepersonneso.rechercherPersonneAnnuaireParDN(dn);

            estHabilitee = personneAnnuaire.calculerHabilitationSurApplication(application);

            if (!estHabilitee)
            {
                throw new HabilitationRegleGestionException(AUCUNE_HABILITATION);
            }
        }
        else
        {
            throw new HabilitationRegleGestionException(UID_ERRONE);
        }

        personneAnnuaire.getPersonalTitle();

        return personneAnnuaire;
    }

    /**
     * plus de profil dans l'annuaire des usagers on simule une habilitation.
     * 
     * @param dn "documenté"
     * @return personne annuaire
     */
    @Override
    public PersonneAnnuaire rechercherPersonneAnnuaireParticuliersAvecHabilitationParDN(String dn)
    {
        if (log.isDebugEnabled())
        {
            log.debug("entrée rechercherPersonneAnnuaireParticuliersAvecHabilitationParDN ");
        }
        PersonneAnnuaire unePersonneAnnuaire = null;
        if (dn != null)
        {
            boolean estHabilitee;

            if (log.isDebugEnabled())
            {
                log.debug("dn personne à rechercher : " + dn);
            }

            /* si le code application est null, une exception est renvoyee */
            /**
             * if (application == null) { throw new ProgrammationException( "Le code application est obligatoire pour
             * tester l'habilitation de la personne correspondant au dn " + dn); }
             **/
            unePersonneAnnuaire = this.rechercherPersonneAnnuaireSansHabilitationParDN(dn);
            if (unePersonneAnnuaire != null)
            {
                HabilitationAnnuaire unehabilitation = new HabilitationAnnuaire();

                DateFormat dateFormat = FormaterDate.getFormatDate("dd/MM/yyyy");
                Date dateDebut = null;
                Date dateFin = null;

                try
                {
                    dateDebut = dateFormat.parse("01/01/2012");
                    dateFin = dateFormat.parse("01/01/2020");
                }
                catch (ParseException e)
                {
                    log.debug("Problème parse date ", e);
                }

                // cas usagers on met un profil bidon 0 à remettre dans applicationContext-security

                Set<HabilitationAnnuaire> listeHabilitations = new HashSet<HabilitationAnnuaire>();
                unehabilitation.setNomProfil("0");
                unehabilitation.setDateDebut(dateDebut);
                unehabilitation.setDateFin(dateFin);
                unehabilitation.setDateCreation(dateDebut);
                unehabilitation.setNomAdministrateur("Administrateur");
                unehabilitation.setLibelleCourtAppli("codeapplication");
                listeHabilitations.add(unehabilitation);

                unePersonneAnnuaire.setListeHabilitations(listeHabilitations);

                if (log.isDebugEnabled())
                {
                    log.debug("Construction utilisateur   usagers avec uid : " + unePersonneAnnuaire.getUid());
                    log.debug("Sur application  : " + unehabilitation.getLibelleCourtAppli());
                }
            }
        }

        else

        {
            log.debug("probléme de construction utilisateur   usagers avec dn  : " + dn + "utilisateur non trouve dans l'annuaire");

        }

        return unePersonneAnnuaire;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.securite.service.AuthentificationEtLectureHabilitationsService#rechercherPersonneAnnuaireSansHabilitationParDN(java.lang.String)
     */
    @Override
    public PersonneAnnuaire rechercherPersonneAnnuaireSansHabilitationParDN(String dn)
    {
        PersonneAnnuaire unePersonneAnnuaire = null;
        if (dn != null)
        {

            if (log.isDebugEnabled())
            {
                log.debug("dn personne à rechercher : " + dn);
            }

            unePersonneAnnuaire = this.lecturepersonneso.rechercherPersonneAnnuaireParDN(dn);
        }
        return unePersonneAnnuaire;
    }

    /**
     * Modificateur de l attribut lecturepersonneso.
     * 
     * @param lecturepersonneso le nouveau lecturepersonneso
     */
    public void setLecturepersonneso(LecturePersonneService lecturepersonneso)
    {
        this.lecturepersonneso = lecturepersonneso;
    }

    /**
     * Modificateur de l attribut ressourceannuaireso.
     * 
     * @param ressourceannuaireso le nouveau ressourceannuaireso
     */
    public void setRessourceannuaireso(RessourceAnnuaireService ressourceannuaireso)
    {
        this.ressourceannuaireso = ressourceannuaireso;
    }

    /**
     * Modificateur de l attribut typeAnnuaire.
     * 
     * @param typeAnnuaire the new typeAnnuaire - String,attribut qui déterminera si annuaire fusionné (si vaut DGFIP
     *        sinon filiere ex dgcp)
     */
    public void setTypeAnnuaire(String typeAnnuaire)
    {
        this.typeAnnuaire = typeAnnuaire;
    }

    /**
     * recherche l'orignie de l'utilisateur.
     * 
     * @param username "documenté"
     * @return true, si c'est vrai
     */
    protected boolean estUserNameDOrigineParticuliers(String username)
    {
        boolean estIl = false;
        if (username.contains(suffixeusagers))
        {
            if (log.isDebugEnabled())
            {
                log.debug("User Name d'origine particuliers detecte : " + username);
            }
            estIl = true;
        }
        else
        {
            estIl = false;
        }
        return estIl;
    }

    /**
     * methode Rechercher personne annuaire habilitee par dn : --.
     * 
     * @param dn --
     * @param application --
     * @return personne annuaire
     */
    protected PersonneAnnuaire rechercherPersonneAnnuaireHabiliteeParDN(String dn, String application)
    {

        boolean estHabilitee;

        if (log.isDebugEnabled())
        {
            log.debug("dn personne à rechercher : " + dn);
        }

        /* si le code application est null, une exception est renvoyee */
        if (application == null)
        {
            throw new ProgrammationException(
                "Le code application est obligatoire pour tester l'habilitation de la personne correspondant au dn "
                    + dn);
        }

        PersonneAnnuaire unePersonneAnnuaire = this.lecturepersonneso.rechercherPersonneAnnuaireParDN(dn);
        estHabilitee = unePersonneAnnuaire.calculerHabilitationSurApplication(application);

        if (!estHabilitee)
        {
            throw new HabilitationRegleGestionException(AUCUNE_HABILITATION);
        }
        return unePersonneAnnuaire;
    }

    /**
     * methode Tester ldap connexion utilisateur : --.
     * 
     * @param DNUtilisateur --
     * @param motDePasseUtilisateur --
     */
    protected void testerLDAPConnexionUtilisateur(String DNUtilisateur, String motDePasseUtilisateur)
    {

        Properties props = ressourceannuaireso.fournirUnSqueletteDEnvironnement();

        props.put(Context.SECURITY_AUTHENTICATION, typeAuthentificationLDAP);
        props.put(Context.SECURITY_CREDENTIALS, motDePasseUtilisateur);
        props.put(Context.SECURITY_PRINCIPAL, DNUtilisateur);

        if (log.isDebugEnabled())
        {
            log.debug("Ouverture de la connexion utilisateur " + DNUtilisateur);
        }

        /* validation identifiant mot de passe utilisateur */
        DirContext unUtilisateurDirContext = null;
        try
        {
            unUtilisateurDirContext = ressourceannuaireso.ouvreLdapConnexion(props);
        }
        catch (AuthenticationException exception)
        {
            log.debug("la connexion utilisateur est non valide message NamingException :" + exception.getMessage());
            throw new AuthentificationRegleGestionException(MOT_DE_PASSE_ERRONE, exception);
        }
        catch (NamingException e)
        {
            throw new ProgrammationException(
                "Impossible de se vérifier l'uid / mot de passe de l'utilisateur : voir la pile d'exception", e);
        }
        finally
        {
            log.debug("Fermeture de la connexion utilisateur");
            ressourceannuaireso.fermerLDAPConnexion(unUtilisateurDirContext);
        }
    }

}
