/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : DgcpPropertyPlaceholderConfigurer.java
 *
 */
package fr.gouv.finances.lombok.util.spring;

import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.springframework.core.io.Resource;

/**
 * Class DgcpPropertyPlaceholderConfigurer
 * 
 * @author amleplatinec
 * @version $Revision: 1.2 $ Date: 11 déc. 2009
 */
public class DgcpPropertyPlaceholderConfigurer extends PropertyPlaceholderConfigurer
{

    public DgcpPropertyPlaceholderConfigurer()
    {
        super();
    }

    /**
     * (methode de remplacement) {@inheritDoc}. Attention cette méthode fait appel à plein de code static
     * (ResolveurDeLocalisations et ConstantesGlobales qui peuvent provoquer des erreur d'initialisation...
     * 
     * @param location le nouveau location
     * @see org.springframework.core.io.support.PropertiesLoaderSupport#setLocation(org.springframework.core.io.Resource)
     */
    @Override
    public void setLocation(Resource location)
    {
        Resource locationResolved;
        locationResolved = ResolveurDeLocalisations.resoudreURLLocalisation(location);
        super.setLocation(locationResolved);
    }

    /**
     * (methode de remplacement) {@inheritDoc}. Attention cette méthode fait appel à plein de code static
     * (ResolveurDeLocalisations et ConstantesGlobales qui peuvent provoquer des erreur d'initialisation...
     * 
     * @param locations le nouveau locations
     * @see org.springframework.core.io.support.PropertiesLoaderSupport#setLocations(org.springframework.core.io.Resource[])
     */
    @Override
    public void setLocations(Resource... locations)
    {
        Resource[] locationsResolved;
        locationsResolved = ResolveurDeLocalisations.resoudreURLLocalisation(locations);
        super.setLocations(locationsResolved);
    }

}
