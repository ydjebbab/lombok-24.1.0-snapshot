/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : TauxDeChancellerieServiceImpl.java
 *
 */
package fr.gouv.finances.lombok.monnaie.service.impl;

import fr.gouv.finances.lombok.monnaie.bean.Monnaie;
import fr.gouv.finances.lombok.monnaie.bean.TauxDeChancellerie;
import fr.gouv.finances.lombok.monnaie.dao.TauxDeChancellerieDao;
import fr.gouv.finances.lombok.monnaie.service.TauxDeChancellerieService;
import fr.gouv.finances.lombok.util.base.BaseServiceImpl;

/**
 * Class TauxDeChancellerieServiceImpl reprise d'élémts communs à TauxDeChancellerieServiceImpl dans eaf et coriolis.
 * 
 * @author amleplatinec-cp
 * @version $Revision: 1.2 $ Date: 15 déc. 2009
 */
public class TauxDeChancellerieServiceImpl extends BaseServiceImpl implements TauxDeChancellerieService
{

    private TauxDeChancellerieDao tauxdechancelleriedao;

    public TauxDeChancellerieServiceImpl()
    {
        super();        
    }

    /**
     *
     * 
     * @param unTauxDeChancellerie --
     * @param uneMonnaie --
     */
    @Override
    public void ajouterTauxChancellerieAMonnaie(TauxDeChancellerie unTauxDeChancellerie, Monnaie uneMonnaie)
    {
        uneMonnaie.setTauxChancellerie(unTauxDeChancellerie);
        this.creerTauxChancellerie(unTauxDeChancellerie);
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param unTauxDeChancellerie
     * @see fr.gouv.finances.lombok.monnaie.service.TauxDeChancellerieService#creerTauxChancellerie(fr.gouv.finances.lombok.monnaie.bean.TauxDeChancellerie)
     */
    @Override
    public void creerTauxChancellerie(TauxDeChancellerie unTauxDeChancellerie)
    {
        tauxdechancelleriedao.saveTauxChancellerie(unTauxDeChancellerie);
    }

    /**
     * Accesseur de l attribut tauxdechancelleriedao.
     * 
     * @return tauxdechancelleriedao
     */
    public TauxDeChancellerieDao getTauxdechancelleriedao()
    {
        return tauxdechancelleriedao;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param unTauxDeChancellerie
     * @see fr.gouv.finances.lombok.monnaie.service.TauxDeChancellerieService#modifierTauxChancellerie(fr.gouv.finances.lombok.monnaie.bean.TauxDeChancellerie)
     */
    @Override
    public void modifierTauxChancellerie(TauxDeChancellerie unTauxDeChancellerie)
    {
        tauxdechancelleriedao.modifyTauxChancellerie(unTauxDeChancellerie);
    }

    /**
     * Modificateur de l attribut tauxdechancelleriedao.
     * 
     * @param tauxdechancelleriedao le nouveau tauxdechancelleriedao
     */
    public void setTauxdechancelleriedao(TauxDeChancellerieDao tauxdechancelleriedao)
    {
        this.tauxdechancelleriedao = tauxdechancelleriedao;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param unTauxDeChancellerie
     * @see fr.gouv.finances.lombok.monnaie.service.TauxDeChancellerieService#supprimerTauxChancellerie(fr.gouv.finances.lombok.monnaie.bean.TauxDeChancellerie)
     */
    @Override
    public void supprimerTauxChancellerie(TauxDeChancellerie unTauxDeChancellerie)
    {
        tauxdechancelleriedao.deleteTauxChancellerie(unTauxDeChancellerie);

    }

}
