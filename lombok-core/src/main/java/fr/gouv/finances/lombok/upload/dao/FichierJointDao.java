/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 */
package fr.gouv.finances.lombok.upload.dao;

import java.util.Collection;

import fr.gouv.finances.lombok.upload.bean.FichierJoint;
import fr.gouv.finances.lombok.util.base.CoreBaseDao;

/**
 * Interface de gestion des données des fichiers joints.
 *
 * @author amleplatinec
 * @author Christophe Breheret-Girardin
 */
public interface FichierJointDao extends CoreBaseDao
{

    /**
     * Méthode permettant de supprimer une collection de fichiers joints.
     * 
     * @param fichiersasupprimer collection des fichiers joints à supprimer.
     */
    public void deleteCollectionFichiersJoints(Collection<FichierJoint> fichiersasupprimer);

    /**
     * Méthode permettant de charger un fichier joint et son contenu éventuel.
     * 
     * @param identifiant identifiant du fichier joint à charger
     * @return fichier joint chargé
     */
    public FichierJoint findFichierJointEtSonContenuParId(Long identifiant);
}
