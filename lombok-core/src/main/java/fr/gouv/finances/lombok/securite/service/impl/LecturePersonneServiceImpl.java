/*
 * Copyright (c) 2014 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.securite.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.SizeLimitExceededException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.gouv.finances.lombok.securite.service.LecturePersonneService;
import fr.gouv.finances.lombok.securite.techbean.AnnuaireException;
import fr.gouv.finances.lombok.securite.techbean.HabilitationAnnuaire;
import fr.gouv.finances.lombok.securite.techbean.PersonneAnnuaire;

/**
 * Classe permettant la lecture des caractéristiques d'une personne via ses attributs fournis par l'annuaire
 * 
 * @author chouard-cp
 * @author amleplatinec
 * @author Christophe Breheret-Girardin
 */
public class LecturePersonneServiceImpl extends AbstractLectureInformationsAnnuaireServiceImpl implements
    LecturePersonneService
{

    /** Initialisation de la journalisation. */
    protected static final Logger LOG = LoggerFactory.getLogger(LecturePersonneServiceImpl.class);

    /** Constante de message d'erreur : PB_LECTURE_UN_ATTRIBUT_PERSONNE. */
    public static final String PB_LECTURE_UN_ATTRIBUT_PERSONNE =
        "Probleme avec annuaire lors de la lecture d'un attribut : ";

    /** Constante de message d'erreur : PB_LECTURE_DES_ATTRIBUTS_PAR_DN. */
    public static final String PB_LECTURE_DES_ATTRIBUTS_PAR_DN =
        "Probleme avec annuaire lors de la lecture des attributs associés à un DN  : ";

    /** Constante de message d'erreur : PB_RECHERCHE_PERSONNE_ANNUAIRE. */
    public static final String PB_RECHERCHE_PERSONNE_ANNUAIRE =
        "Probleme avec annuaire lors de la recherche de personnes avec critere : ";

    /** Constante de message d'erreur : PB_CONNEXION_ANNUAIRE_DN. */
    public static final String PB_CONNEXION_ANNUAIRE_DN =
        "Problème de connexion à l'annuaire lors de la recherche d'un DN par UID : ";

    /** Constante de message d'erreur : UID_ERRONE. */
    public static final String UID_ERRONE = "Identifiant erroné";

    /** Constante de message d'erreur : PB_NBE_PERSONNES_SUPERIEURE_MAX. */
    public static final String PB_NBE_PERSONNES_SUPERIEURE_MAX =
        " La liste des personnes répondant à vos critères est trop importante, veuillez affiner votre recherche";

    /** Expression permettant une recherche dans l'annuaire LDAP. */
    private String baseRechercheLDAP;

    /**
     * Le type d'annuaire correspondant aux constantes ci-dessous
     */
    private String typeAnnuaire;

    /* 
     * Liste des attributs à lire quand on cherche par le DN, permet de ne récupérer que le SPI par exemple dans le cas d'une appli
     * qui derrière un portail "USAGERS", évite de ramener des données inutiles. null par défaut : on ramène tous les attributs disponibles,
     * sinon liste définissable par la property annuaire.listeAttributsAnnuaireALire
     */
    private String[] listeAttributsAnnuaireALire = null;

    /*************************/
    /** Constantes désignant les annuaires. */
    /*************************/
    /** Constante DGFIP. */
    private static final String DGFIP = "DGFIP";

    /** Constante DGI. */
    private static final String DGI = "DGI";

    /**
     * Constructeur.
     */
    public LecturePersonneServiceImpl()
    {
        super();

    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param filtreRecherche "documenté"
     * @param unApplicationDirContext "documenté"
     * @return list
     * @see fr.gouv.finances.lombok.securite.service.LecturePersonneService#
     *      rechercherDesPersonnesAnnuaireAvecFiltreComplexe(java.lang.String, javax.naming.directory.DirContext)
     */
    @Override
    public List<PersonneAnnuaire> rechercherDesPersonnesAnnuaireAvecFiltreComplexe(String filtreRecherche,
        DirContext unApplicationDirContext)
    {
        LOG.debug(">>> Debut methode rechercherDesPersonnesAnnuaireAvecFiltreComplexe");
        return rechercherDesPersonnesAnnuaireAvecFiltreComplexe(filtreRecherche, unApplicationDirContext, null, null);
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param filtreRecherche "documenté"
     * @param unApplicationDirContext "documenté"
     * @param maxReponses "documenté"
     * @return list
     * @see fr.gouv.finances.lombok.securite.service.LecturePersonneService#
     *      rechercherDesPersonnesAnnuaireAvecFiltreComplexe(java.lang.String, javax.naming.directory.DirContext,
     *      java.lang.Long)
     */
    @Override
    public List<PersonneAnnuaire> rechercherDesPersonnesAnnuaireAvecFiltreComplexe(String filtreRecherche,
        DirContext unApplicationDirContext, Long maxReponses)
    {
        LOG.debug(">>> Debut methode rechercherDesPersonnesAnnuaireAvecFiltreComplexe");
        return rechercherDesPersonnesAnnuaireAvecFiltreComplexe(filtreRecherche, unApplicationDirContext, null,
            maxReponses);
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param filtreRecherche "documenté"
     * @param unApplicationDirContext "documenté"
     * @param listeAttributsAnnuaireALire "documenté"
     * @return list
     * @see fr.gouv.finances.lombok.securite.service.LecturePersonneService#
     *      rechercherDesPersonnesAnnuaireAvecFiltreComplexe(java.lang.String, javax.naming.directory.DirContext,
     *      java.lang.String[])
     */
    @Override
    public List<PersonneAnnuaire> rechercherDesPersonnesAnnuaireAvecFiltreComplexe(String filtreRecherche,
        DirContext unApplicationDirContext, String[] listeAttributsAnnuaireALire)
    {
        LOG.debug(">>> Debut methode rechercherDesPersonnesAnnuaireAvecFiltreComplexe");
        return rechercherDesPersonnesAnnuaireAvecFiltreComplexe(filtreRecherche, unApplicationDirContext,
            listeAttributsAnnuaireALire, null);
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param filtreRecherche "documenté"
     * @param unApplicationDirContext "documenté"
     * @param listeAttributsAnnuaireALire "documenté"
     * @param maxReponses "documenté"
     * @return list
     * @see fr.gouv.finances.lombok.securite.service.LecturePersonneService#
     *      rechercherDesPersonnesAnnuaireAvecFiltreComplexe(java.lang.String, javax.naming.directory.DirContext,
     *      java.lang.String[], java.lang.Long)
     */
    @Override
    public List<PersonneAnnuaire> rechercherDesPersonnesAnnuaireAvecFiltreComplexe(String filtreRecherche,
        DirContext unApplicationDirContext, String[] listeAttributsAnnuaireALire, Long maxReponses)
    {
        LOG.debug(">>> Debut methode rechercherDesPersonnesAnnuaireAvecFiltreComplexe");
        List<PersonneAnnuaire> listePersonnesAnnuaires = null;
        SearchControls controlesRecherche = new SearchControls();

        controlesRecherche.setSearchScope(SearchControls.SUBTREE_SCOPE);
        controlesRecherche.setReturningAttributes(listeAttributsAnnuaireALire);

        if (maxReponses != null)
        {
            controlesRecherche.setCountLimit(maxReponses.longValue());
        }

        try
        {
            NamingEnumeration<?> resultats =
                unApplicationDirContext.search(baseRechercheLDAP, filtreRecherche, controlesRecherche);

            listePersonnesAnnuaires = mapperResultatsRechercheAvecListeDePersonneAnnuaire(resultats);
        }

        catch (SizeLimitExceededException e)
        {
            LOG.debug(PB_NBE_PERSONNES_SUPERIEURE_MAX + " " + filtreRecherche);
            throw new AnnuaireException(PB_NBE_PERSONNES_SUPERIEURE_MAX, e);
        }
        catch (NamingException e)
        {
            throw new AnnuaireException(PB_RECHERCHE_PERSONNE_ANNUAIRE + " " + filtreRecherche, e);
        }

        return listePersonnesAnnuaires;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param uid "documenté"
     * @return string
     * @see fr.gouv.finances.lombok.securite.service.LecturePersonneService#
     *      rechercherDNCompletUtilisateurPourUnUID(java.lang.String)
     */
    @Override
    public String rechercherDNCompletUtilisateurPourUnUID(String uid)
    {
        LOG.debug(">>> Debut methode rechercherDNCompletUtilisateurPourUnUID");
        String dnn;
        DirContext unApplicationDirContextNouveau = null;

        try
        {
            LOG.debug("ouverture d'un nouveau contexte annuaire existant");
            unApplicationDirContextNouveau = this.ouvreLDAPConnexionApplication();
            dnn = this.rechercherDNCompletUtilisateurPourUnUID(uid, unApplicationDirContextNouveau);
        }
        finally
        {
            LOG.debug("Fermeture de la connexion applicative au ldap");
            this.fermerLDAPConnexionApplication(unApplicationDirContextNouveau);
        }
        return dnn;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param uid "documenté"
     * @param unApplicationDirContext "documenté"
     * @return string
     * @see fr.gouv.finances.lombok.securite.service.LecturePersonneService#
     *      rechercherDNCompletUtilisateurPourUnUID(java.lang.String, javax.naming.directory.DirContext)
     */
    @Override
    public String rechercherDNCompletUtilisateurPourUnUID(String uid, DirContext unApplicationDirContext)
    {
        LOG.debug(">>> Debut methode rechercherDNCompletUtilisateurPourUnUID");
        String dnn = null;
        String filtreRecherche = "UID=" + uid;
        SearchControls controlesRecherche = new SearchControls();

        controlesRecherche.setSearchScope(SearchControls.SUBTREE_SCOPE);
        String[] listeAttributs = {"dn"};
        controlesRecherche.setReturningAttributes(listeAttributs);

        LOG.debug("Recherche du DN utilisateur  " + uid);

        try
        {

            NamingEnumeration<?> resultats =
                unApplicationDirContext.search(baseRechercheLDAP, filtreRecherche, controlesRecherche);

            /*
             * Exploration des résultats : à priori un seul car dans uid il y a "unique"
             */
            if (resultats.hasMore())
            {

                /* extraction du 1er resultat */
                SearchResult resultat = (SearchResult) resultats.next();

                /* extraction du DN : il faut rajouter le BaseDN */
                dnn = resultat.getName() + "," + baseRechercheLDAP;

                if (LOG.isDebugEnabled())
                {
                    LOG.debug("DN utilisateur trouve :  {}", dnn);
                }
            }
            else
            {
                /* si rien trouvé */
                if (LOG.isDebugEnabled())
                {
                    LOG.debug("DN utilisateur non trouve ");
                }
            }
        }
        catch (NamingException e)
        {
            throw new AnnuaireException(PB_CONNEXION_ANNUAIRE_DN + uid, e);
        }
        return dnn;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param uidFonctionnel "documenté"
     * @return string
     * @see fr.gouv.finances.lombok.securite.service.LecturePersonneService#rechercherDNCompletUtilisateurPourUnUidFonctionnel(java.lang.String)
     */
    @Override
    public String rechercherDNCompletUtilisateurPourUnUidFonctionnel(String uidFonctionnel)
    {
        LOG.debug(">>> Debut methode rechercherDNCompletUtilisateurPourUnUidFonctionnel");
        String dnn;
        DirContext unApplicationDirContextNouveau = null;

        try
        {
            LOG.debug("ouverture d'un nouveau contexte annuaire existant");
            unApplicationDirContextNouveau = this.ouvreLDAPConnexionApplication();
            dnn = this.rechercherDNCompletUtilisateurPourUnUIDFonctionnel(uidFonctionnel, unApplicationDirContextNouveau);
        }
        finally
        {
            LOG.debug("Fermeture de la connexion applicative au ldap");
            this.fermerLDAPConnexionApplication(unApplicationDirContextNouveau);
        }
        return dnn;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param uidFonctionnel "documenté"
     * @param unApplicationDirContext "documenté"
     * @return string
     * @see fr.gouv.finances.lombok.securite.service.LecturePersonneService#
     *      rechercherDNCompletUtilisateurPourUnUIDFonctionnel(java.lang.String, javax.naming.directory.DirContext)
     */
    public String rechercherDNCompletUtilisateurPourUnUIDFonctionnel(String uidFonctionnel, DirContext unApplicationDirContext)
    {
        LOG.debug(">>> Debut methode rechercherDNCompletUtilisateurPourUnUIDFonctionnel");
        String dnn = null;
        String filtreRecherche = "uidFonctionnel=" + uidFonctionnel;
        SearchControls controlesRecherche = new SearchControls();

        controlesRecherche.setSearchScope(SearchControls.SUBTREE_SCOPE);
        String[] listeAttributs = {"dn"};
        controlesRecherche.setReturningAttributes(listeAttributs);

        LOG.debug("Recherche du DN utilisateur  " + uidFonctionnel);

        try
        {

            NamingEnumeration<?> resultats =
                unApplicationDirContext.search(baseRechercheLDAP, filtreRecherche, controlesRecherche);

            /*
             * exploration des résultats : a priori un seul car un UID est unique :
             */
            if (resultats.hasMore())
            {

                /* extraction du 1er resultat */
                SearchResult resultat = (SearchResult) resultats.next();

                /* extraction du DN : il faut rajouter le BaseDN */
                dnn = resultat.getName() + "," + baseRechercheLDAP;

                if (LOG.isDebugEnabled())
                {
                    LOG.debug("DN utilisateur trouve  " + dnn);
                }
            }
            else
            {
                /* si rien trouvé */
                if (LOG.isDebugEnabled())
                {
                    LOG.debug("DN utilisateur non trouve ");
                }
            }
        }
        catch (NamingException e)
        {
            throw new AnnuaireException(PB_CONNEXION_ANNUAIRE_DN + uidFonctionnel, e);
        }
        return dnn;
    }

    /**
     * recherche des personnes habilitées sur une application avec libelle court :application et dont le nom commence
     * par debutnom. evolution vers le nouvel annuiare fusionné ,on change dans ce cas les attributs sur lesquels se
     * font la recherche (amlp sept2011)
     * 
     * @param application application
     * @param debutNom debutNom
     * @return the list< personne annuaire>
     */
    @Override
    public List<PersonneAnnuaire> rechercherPersonneAnnuaireAvecHabilitationSurApplicationAvecDebutNom(
        String application, String debutNom)
    {
        LOG.debug(">>> Debut methode rechercherPersonneAnnuaireAvecHabilitationSurApplicationAvecDebutNom");
        DirContext unApplicationDirContext = null;
        List<PersonneAnnuaire> listePersonnesAnnuaire = null;

        if ("".equals(application))
        {
            throw new AnnuaireException("Nom d'application obligatoire");
        }
        if ("".equals(debutNom))
        {
            throw new AnnuaireException("début nom obligatoire");
        }

        String filtre;

        // cas de l'annuaire fusionné
        if (this.getTypeAnnuaire() != null && this.getTypeAnnuaire().compareToIgnoreCase(DGFIP) == 0)
        {
            filtre =
                "(&(sn=" + debutNom + "*) (" + PersonneAnnuaire.ATTRIBUT_HABILITATION_PROFAPPLICATIF + "=" + application + "*))";

        }
        else
        // annuaire ex dgcp
        {
            filtre =
                "(&(sn=" + debutNom + "*) (" + PersonneAnnuaire.ATTRIBUT_HABILITATION + "=" + application + "*))";
        }
        try
        {
            /* ouverture de la connexion applicative vers l'annuaire */
            unApplicationDirContext = this.ouvreLDAPConnexionApplication();

            /* recherche du DN de l'utilisateur dans l'arborescence annuaire */
            listePersonnesAnnuaire = rechercherDesPersonnesAnnuaireAvecFiltreComplexe(filtre, unApplicationDirContext);
        }
        finally
        {
            LOG.debug("Fermeture de la connexion applicative au ldap");
            this.fermerLDAPConnexionApplication(unApplicationDirContext);
        }
        return listePersonnesAnnuaire;
    }

    /**
     * avril 2015 suite à la généralisation d'agir size limit exceeded sur le répartiteur on doit donc changer la
     * maniére de traiter cette méthode 2 possibilités : 1- on se sert toujours de calculerPrefixeCasRepartiteur , or il
     * y a des personnes qui ne peuvent être que sur 2 caractéres donc pas possible à mettre en place , il va falloir
     * reprendre la méthode pour le cas dgfip 2-pour l'annuaire ex dgcp on va faire une recherche directement sur
     * mefiapplidgcp.
     *
     * @param application
     * @param prefixeNom
     * @param profil
     * @return list
     */
    public List<PersonneAnnuaire> rechercherPersonneAnnuaireAvecHabilitationSurApplicationAvecDebutNomAvecProfil(
        String application, String prefixeNom, String profil)
    {
        LOG.debug(">>> Debut methode rechercherPersonneAnnuaireAvecHabilitationSurApplicationAvecDebutNomAvecProfil");
        DirContext unApplicationDirContext = null;
        List<PersonneAnnuaire> listePersonnesAnnuaire = null;

        if ("".equals(application))
        {
            throw new AnnuaireException("Nom d'application obligatoire");
        }
        if ("".equals(prefixeNom))
        {
            throw new AnnuaireException("Préfix du nom obligatoire");
        }

        String filtre;

        if (this.getTypeAnnuaire() != null && this.getTypeAnnuaire().compareToIgnoreCase(DGFIP) == 0)
        {
            filtre =
                "(&(sn=" + prefixeNom + "*) (" + PersonneAnnuaire.ATTRIBUT_HABILITATION_PROFAPPLICATIF + "=" + application + ";" + profil
                    + "*))";

        }
        else
        // annuaire ex dgcp
        {
            filtre =
                "(&(sn=" + prefixeNom + "*) (" + PersonneAnnuaire.ATTRIBUT_HABILITATION_EXDGCP + "=" + application + ";" + profil + ";*))";
        }
        try
        {
            /* ouverture de la connexion applicative vers l'annuaire */
            unApplicationDirContext = this.ouvreLDAPConnexionApplication();

            /* recherche du DN de l'utilisateur dans l'arborescence annuaire */
            listePersonnesAnnuaire = rechercherDesPersonnesAnnuaireAvecFiltreComplexe(filtre, unApplicationDirContext);
        }
        finally
        {
            LOG.debug("Fermeture de la connexion applicative au ldap");
            this.fermerLDAPConnexionApplication(unApplicationDirContext);
        }

        return listePersonnesAnnuaire;
    }

    /**
     * (methode de remplacement) {@inheritDoc} évolution vers les nouveaux attributs de l'annuiare fusionné quand on est
     * dans ce cas (amlp sept 11).
     * 
     * @param application "documenté"
     * @param prefixeCodique "documenté"
     * @return list
     * @see fr.gouv.finances.lombok.securite.service.LecturePersonneService#
     *      rechercherPersonneAnnuaireAvecHabilitationSurApplicationAvecPrefixeCodique(java.lang.String,
     *      java.lang.String) !!! Attention !!!!!!!!: cas annuaire fusionné : affectationCODIQUE non indexé
     */
    @Override
    public List<PersonneAnnuaire> rechercherPersonneAnnuaireAvecHabilitationSurApplicationAvecPrefixeCodique(
        String application, String prefixeCodique)
    {
        LOG.debug(">>> Debut methode rechercherPersonneAnnuaireAvecHabilitationSurApplicationAvecPrefixeCodique");
        DirContext unApplicationDirContext = null;
        List<PersonneAnnuaire> listePersonnesAnnuaire = null;

        if ("".equals(application))
        {
            throw new AnnuaireException("Nom d'application obligatoire");
        }
        if ("".equals(prefixeCodique))
        {
            throw new AnnuaireException("Prefixe codique obligatoire");
        }

        String filtre;
        // cas de l'annuaire fusionné

        if (this.getTypeAnnuaire() != null && this.getTypeAnnuaire().compareToIgnoreCase(DGFIP) == 0)
        {
            filtre =
                "(&(affectationCODIQUE=" + prefixeCodique + "*)  (" + PersonneAnnuaire.ATTRIBUT_HABILITATION_PROFAPPLICATIF + "="
                    + application
                    + "*))";
        }
        else
        {
            filtre =
                "(&(affectation=" + prefixeCodique + "*) (" + PersonneAnnuaire.ATTRIBUT_HABILITATION + "=" + application
                    + "*))";
        }

        try
        {
            /* ouverture de la connexion applicative vers l'annuaire */
            unApplicationDirContext = this.ouvreLDAPConnexionApplication();

            /* recherche du DN de l'utilisateur dans l'arborescence annuaire */
            listePersonnesAnnuaire = rechercherDesPersonnesAnnuaireAvecFiltreComplexe(filtre, unApplicationDirContext);
        }
        finally
        {
            LOG.debug("Fermeture de la connexion applicative au ldap");
            this.fermerLDAPConnexionApplication(unApplicationDirContext);
        }
        return listePersonnesAnnuaire;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param application "documenté"
     * @param prefixeCodique "documenté"
     * @param profil "documenté"
     * @return list
     * @see fr.gouv.finances.lombok.securite.service.LecturePersonneService#
     *      rechercherPersonneAnnuaireAvecHabilitationSurApplicationAvecPrefixeCodiqueAvecProfil(java.lang.String,
     *      java.lang.String, java.lang.String) !!! Attention !!!!!!!!: cas annuaire fusionné : affectationCODIQUE non
     *      indexé
     */
    @Override
    public List<PersonneAnnuaire> rechercherPersonneAnnuaireAvecHabilitationSurApplicationAvecPrefixeCodiqueAvecProfil(
        String application, String prefixeCodique, String profil)
    {
        LOG.debug(">>> Debut methode rechercherPersonneAnnuaireAvecHabilitationSurApplicationAvecPrefixeCodiqueAvecProfil");
        List<PersonneAnnuaire> listePersonnesAnnuaire =
            this
                .rechercherPersonneAnnuaireAvecHabilitationSurApplicationAvecPrefixeCodique(application, prefixeCodique);

        List<PersonneAnnuaire> listePAnnuaireAvecProfil = new ArrayList<>();

        for (Iterator<PersonneAnnuaire> iterator = listePersonnesAnnuaire.iterator(); iterator.hasNext();)
        {
            PersonneAnnuaire personneAnnuaire = (PersonneAnnuaire) iterator.next();
            boolean estHabilite = personneAnnuaire.calculerHabilitationSurApplication(application);
            boolean profilmatch = false;
            if (estHabilite)
            {
                Set<HabilitationAnnuaire> listeHabilitations = personneAnnuaire.getListeHabilitations();
                for (Iterator<HabilitationAnnuaire> iterator2 = listeHabilitations.iterator(); iterator2.hasNext();)
                {
                    HabilitationAnnuaire habilitationAnnuaire = (HabilitationAnnuaire) iterator2.next();
                    if (habilitationAnnuaire.getNomProfil().equalsIgnoreCase(profil))
                    {
                        profilmatch = true;
                    }
                }
            }
            if (profilmatch)
            {
                listePAnnuaireAvecProfil.add(personneAnnuaire);
            }
        }
        return listePAnnuaireAvecProfil;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param application "documenté"
     * @param prefixeSages "documenté"
     * @param profil "documenté"
     * @return list
     * @see fr.gouv.finances.lombok.securite.service.LecturePersonneService#
     *      rechercherPersonneAnnuaireAvecHabilitationSurApplicationAvecPrefixeSagesAvecProfilDansAnnuaireFusionne(java.lang.String,
     *      java.lang.String, java.lang.String)
     */
    @Override
    public List<PersonneAnnuaire> rechercherPersonneAnnuaireAvecHabilitationSurApplicationAvecPrefixeSagesAvecProfilDansAnnuaireFusionne(
        String application, String prefixeSages, String profil)
    {
        LOG.debug(
            ">>> Debut methode rechercherPersonneAnnuaireAvecHabilitationSurApplicationAvecPrefixeSagesAvecProfilDansAnnuaireFusionne");
        List<PersonneAnnuaire> listePersonnesAnnuaire =
            this
                .rechercherPersonneAnnuaireAvecHabilitationSurApplicationAvecPrefixeSagesDansAnnuaireFusionne(application, prefixeSages);

        List<PersonneAnnuaire> listePAnnuaireAvecProfil = new ArrayList<>();

        for (Iterator<PersonneAnnuaire> iterator = listePersonnesAnnuaire.iterator(); iterator.hasNext();)
        {
            PersonneAnnuaire personneAnnuaire = (PersonneAnnuaire) iterator.next();
            boolean estHabilite = personneAnnuaire.calculerHabilitationSurApplication(application);
            boolean profilmatch = false;
            if (estHabilite)
            {
                Set<HabilitationAnnuaire> listeHabilitations = personneAnnuaire.getListeHabilitations();
                for (Iterator<HabilitationAnnuaire> iterator2 = listeHabilitations.iterator(); iterator2.hasNext();)
                {
                    HabilitationAnnuaire habilitationAnnuaire = (HabilitationAnnuaire) iterator2.next();
                    if (habilitationAnnuaire.getNomProfil().equalsIgnoreCase(profil))
                    {
                        profilmatch = true;
                    }
                }
            }
            if (profilmatch)
            {
                listePAnnuaireAvecProfil.add(personneAnnuaire);
            }
        }
        return listePAnnuaireAvecProfil;
    }

    /**
     * (methode de remplacement) {@inheritDoc} évolution vers les nouveaux attributs de l'annuiare fusionné quand on est
     * dans ce cas (amlp sept 11).
     * 
     * @param application "documenté"
     * @param prefixeSages "documenté"
     * @return list
     * @see fr.gouv.finances.lombok.securite.service.LecturePersonneService#
     *      rechercherPersonneAnnuaireAvecHabilitationSurApplicationAvecPrefixeCodique(java.lang.String,
     *      java.lang.String)
     */
    @Override
    public List<PersonneAnnuaire> rechercherPersonneAnnuaireAvecHabilitationSurApplicationAvecPrefixeSagesDansAnnuaireFusionne(
        String application, String prefixeSages)
    {
        LOG.debug(">>> Debut methode rechercherPersonneAnnuaireAvecHabilitationSurApplicationAvecPrefixeSagesDansAnnuaireFusionne");
        DirContext unApplicationDirContext = null;
        List<PersonneAnnuaire> listePersonnesAnnuaire = null;

        if ("".equals(application))
        {
            throw new AnnuaireException("Nom d'application obligatoire");
        }
        if ("".equals(prefixeSages))
        {
            throw new AnnuaireException("prefixe Sages obligatoire");
        }

        String filtre = "";

        if (this.getTypeAnnuaire() != null && this.getTypeAnnuaire().compareToIgnoreCase(DGFIP) == 0)
        {
            filtre =
                "(&(affectationSAGES=" + prefixeSages + "*)  (" + PersonneAnnuaire.ATTRIBUT_HABILITATION_PROFAPPLICATIF + "=" + application
                    + "*))";
        }
        else
        {
            LOG.debug("interrogation sur un mauvais annuaire");
            return null;
        }

        try
        {
            /* ouverture de la connexion applicative vers l'annuaire */
            unApplicationDirContext = this.ouvreLDAPConnexionApplication();

            /* recherche du DN de l'utilisateur dans l'arborescence annuaire */
            listePersonnesAnnuaire = rechercherDesPersonnesAnnuaireAvecFiltreComplexe(filtre, unApplicationDirContext);
        }
        finally
        {
            LOG.debug("Fermeture de la connexion applicative au ldap");
            this.fermerLDAPConnexionApplication(unApplicationDirContext);
        }
        return listePersonnesAnnuaire;
    }

    /*
     * (non-Javadoc)
     * @seefr.gouv.finances.lombok.securite.service.LecturePersonneService#
     * rechercherPersonneAnnuaireAvecHabilitationSurApplicationAvecProfil(java.lang.String, java.lang.String)le critere
     * application + profil ramene parfois (meêm souvent ) plus de 500 champs , d'ou ereur ldapon prend donc le parti de
     * balayer l'alphabet sous forme (aa, ab, ac...) afin de rechercher en plus les personnes dont le nomcommence par aa
     * , puis ab , puis ac (meêm logique que dans aptera)ATTENTION / : CETTE METHODE EST DONC TRES LONGUE ET DOIT DONC
     * ETRE INSEREE DANS UN BATCH DE PREFERENCE +remplir dans le contrat annuaire que on lit le champ
     * mefiapplihabilitdgcp !!!!!!!!!cas annuaire fusionné : méthode trés longue à insérer dans 1 batch
     */
    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param application "documenté"
     * @param profil "documenté"
     * @return list
     * @see fr.gouv.finances.lombok.securite.service.LecturePersonneService#
     *      rechercherPersonneAnnuaireAvecHabilitationSurApplicationAvecProfil(java.lang.String, java.lang.String)
     */
    /**
     * avril 2015 suite à la généralisation d'agir size limit exceeded sur le répartiteur on doit donc changer la
     * maniére de traiter cette méthode 2 possibilités : 1- on se sert toujours de calculerPrefixeCasRepartiteur , or il
     * y a des personnes qui ne peuvent être que sur 2 caractéres donc pas possible à mettre en place , il va falloir
     * reprendre la méthode pour le cas dgfip on revient donc au cas par 2 caracteres 2-pour l'annuaire ex dgcp on va
     * faire une recherche directement sur mefiapplidgcp
     **/
    @Override
    public List<PersonneAnnuaire> rechercherPersonneAnnuaireAvecHabilitationSurApplicationAvecProfil(
        String application, String profil)
    {
        LOG.debug(">>> Debut methode rechercherPersonneAnnuaireAvecHabilitationSurApplicationAvecProfil");
        List<PersonneAnnuaire> listePersonnesAnnuaire = new ArrayList<PersonneAnnuaire>();

        if ("".equals(application))
        {
            throw new AnnuaireException("Nom court de l 'application obligatoire");
        }
        if ("".equals(profil))
        {
            throw new AnnuaireException("libellé court du profil obligatoire");
        }

        LdapMethodesCommunes unLdapMethodesCommunes = new LdapMethodesCommunes();
        List<String> leTableauDebutNom = new ArrayList<String>();

        // cas annuaire fusionné : size limit exception < coté ex dgcp
        // if (this.getTypeAnnuaire() != null && this.getTypeAnnuaire().compareToIgnoreCase(DGFIP) == 0)
        // {
        // leTableauDebutNom = unLdapMethodesCommunes.calculerPrefixeCasRepartiteur();
        // }
        // else
        // {
        leTableauDebutNom = unLdapMethodesCommunes.calculerPrefixe();
        // }

        for (Iterator<String> iterator = leTableauDebutNom.iterator(); iterator.hasNext();)
        {
            String unelettre1 = (String) iterator.next();

            List<PersonneAnnuaire> lesPersonnesAnnuaireAvecDebutNomHabAvecProfilSurAppli =
                this.rechercherPersonneAnnuaireAvecHabilitationSurApplicationAvecDebutNomAvecProfil(application,
                    unelettre1, profil);
            if (lesPersonnesAnnuaireAvecDebutNomHabAvecProfilSurAppli != null
                && lesPersonnesAnnuaireAvecDebutNomHabAvecProfilSurAppli.size() > 0)
            {

                {
                    listePersonnesAnnuaire.addAll(lesPersonnesAnnuaireAvecDebutNomHabAvecProfilSurAppli);
                }
            }
        }

        return listePersonnesAnnuaire;

    }

    /*
     * (non-Javadoc)
     * @see
     * fr.gouv.finances.lombok.securite.service.impl.LecturePersonneService#rechercherPersonneAnnuaireParDebutNomPrenom
     * (java.lang.String, java.lang.String)
     */
    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param prefixePrenom "documenté"
     * @param prefixeNom "documenté"
     * @return list
     * @see fr.gouv.finances.lombok.securite.service.LecturePersonneService#
     *      rechercherPersonneAnnuaireParDebutPrenomNom(java.lang.String, java.lang.String)
     */
    @Override
    public List<PersonneAnnuaire> rechercherPersonneAnnuaireParDebutPrenomNom(String prefixePrenom, String prefixeNom)
    {
        LOG.debug(">>> Debut methode rechercherPersonneAnnuaireParDebutPrenomNom");
        return rechercherPersonneAnnuaireParDebutPrenomNom(prefixePrenom, prefixeNom, null);
    }

    /*
     * (non-Javadoc)
     * @see
     * fr.gouv.finances.lombok.securite.service.impl.LecturePersonneService#rechercherPersonneAnnuaireParDebutNomPrenom
     * (java.lang.String, java.lang.String)
     */
    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param prefixePrenom "documenté"
     * @param prefixeNom "documenté"
     * @param maxReponses "documenté"
     * @return list
     * @see fr.gouv.finances.lombok.securite.service.LecturePersonneService#
     *      rechercherPersonneAnnuaireParDebutPrenomNom(java.lang.String, java.lang.String, java.lang.Long)
     */
    @Override
    public List<PersonneAnnuaire> rechercherPersonneAnnuaireParDebutPrenomNom(String prefixePrenom, String prefixeNom, Long maxReponses)
    {
        LOG.debug(">>> Debut methode rechercherPersonneAnnuaireParDebutPrenomNom");
        DirContext unApplicationDirContext = null;
        List<PersonneAnnuaire> listePersonnesAnnuaire = null;
        String filtre = null;

        if ("".equals(prefixePrenom) && "".equals(prefixeNom))
        {
            throw new AnnuaireException("Au moins un prefixeprenom ou un prefixenom doit être specifie");
        }
        if ("".equals(prefixePrenom) && !"".equals(prefixeNom))
        {
            filtre = "sn=" + prefixeNom + "*";
        }
        if (!"".equals(prefixePrenom) && "".equals(prefixeNom))
        {
            filtre = "givenname=" + prefixeNom + "*";
        }
        if (!"".equals(prefixePrenom) && !"".equals(prefixeNom))
        {
            filtre = "(&(sn=" + prefixeNom + "*) (givenname=" + prefixePrenom + "*))";
        }

        try
        {
            /* ouverture de la connexion applicative vers l'annuaire */
            unApplicationDirContext = this.ouvreLDAPConnexionApplication();

            /* recherche du DN de l'utilisateur dans l'arborescence annuaire */
            listePersonnesAnnuaire =
                rechercherDesPersonnesAnnuaireAvecFiltreComplexe(filtre, unApplicationDirContext, null, maxReponses);

        }
        finally
        {
            LOG.debug("Fermeture de la connexion applicative au ldap");
            this.fermerLDAPConnexionApplication(unApplicationDirContext);
        }
        return listePersonnesAnnuaire;
    }

    /*
     * (non-Javadoc)
     * @see
     * fr.gouv.finances.lombok.securite.service.impl.LecturePersonneService#rechercherPersonneAnnuaireParDN(java.lang
     * .String)
     */
    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param dnn "documenté"
     * @return personne annuaire
     * @see fr.gouv.finances.lombok.securite.service.LecturePersonneService#
     *      rechercherPersonneAnnuaireParDN(java.lang.String)
     */
    @Override
    public PersonneAnnuaire rechercherPersonneAnnuaireParDN(String dnn)
    {
        LOG.debug(">>> Debut methode rechercherPersonneAnnuaireParDN");
        PersonneAnnuaire unepersonneannuaire;
        DirContext unApplicationDirContextNouveau = null;

        try
        {
            LOG.debug("ouverture d'un nouveau contexte annuaire existant");
            unApplicationDirContextNouveau = this.ouvreLDAPConnexionApplication();
            unepersonneannuaire = this.rechercherPersonneAnnuaireParDN(dnn, unApplicationDirContextNouveau);
        }
        finally
        {
            LOG.debug("Fermeture de la connexion applicative au ldap");
            this.fermerLDAPConnexionApplication(unApplicationDirContextNouveau);
        }

        return unepersonneannuaire;
    }

    /**
     * (non-Javadoc).
     *
     * @param dnn
     * @param applicationContextExistant
     * @return personne annuaire
     * @see fr.gouv.finances.lombok.securite.service.impl.LecturePersonneService#rechercherPersonneAnnuaireParDN(java.lang
     *      .String, javax.naming.directory.DirContext) evolution vers le nouvel annuiare fusionné (amlp sept
     *      11)(methode de remplacement) {@inheritDoc}
     * @see fr.gouv.finances.lombok.securite.service.LecturePersonneService#rechercherPersonneAnnuaireParDN
     *      (java.lang.String, javax.naming.directory.DirContext)
     */
    @Override
    public PersonneAnnuaire rechercherPersonneAnnuaireParDN(String dnn, DirContext applicationContextExistant)
    {
        LOG.debug(">>> Debut methode rechercherPersonneAnnuaireParDN");
        PersonneAnnuaire unePersonneAnnuaire = null;

        if (LOG.isDebugEnabled())
        {
            LOG.debug("dn personne à rechercher : " + dnn);
        }

        /* lecture de tous les attributs de la personne */
        Attributes attrs;
        try
        {
            attrs = applicationContextExistant.getAttributes(dnn, listeAttributsAnnuaireALire);

            if (LOG.isDebugEnabled())
            {
                LOG.debug("liste des attributs récupérés dans l'annuaire (si null, tous les attributs sont récupérés) : " + Arrays.toString(listeAttributsAnnuaireALire));
            }
            unePersonneAnnuaire = this.mapperAttributsRechercheAnnuaireAvecUnePersonneAnnuaire(attrs);

            // on remplit en + des attributs renommés pour ne pas avoir à changer des jsp
            if (unePersonneAnnuaire.estUtilisateurOrigineDGFIP())
            {
                unePersonneAnnuaire = this.gererMappingAttributsAvecChangementNomDansFusionAnnuaire(unePersonneAnnuaire);
            }
        }
        catch (NamingException e)
        {
            LOG.warn("exception en lisant les attributs de la personne : " + e.getMessage());
            throw new AnnuaireException(PB_LECTURE_DES_ATTRIBUTS_PAR_DN + dnn, e);
        }

        return unePersonneAnnuaire;
    }

    /*
     * (non-Javadoc)
     * @see
     * fr.gouv.finances.lombok.securite.service.impl.LecturePersonneService#rechercherPersonneAnnuaireParUID(java.lang
     * .String)
     */
    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param uid "documenté"
     * @return personne annuaire
     * @see fr.gouv.finances.lombok.securite.service.LecturePersonneService#
     *      rechercherPersonneAnnuaireParUID(java.lang.String)
     */
    @Override
    public PersonneAnnuaire rechercherPersonneAnnuaireParUID(String uid)
    {
        LOG.debug(">>> Debut methode rechercherPersonneAnnuaireParUID");
        DirContext unApplicationDirContext = null;
        PersonneAnnuaire unepersonneannuaire = null;

        try
        {
            /* ouverture de la connexion applicative vers l'annuaire */
            unApplicationDirContext = this.ouvreLDAPConnexionApplication();

            /* recherche du DN de l'utilisateur dans l'arborescence annuaire */
            String dnn = rechercherDNCompletUtilisateurPourUnUID(uid, unApplicationDirContext);

            if (dnn != null)
            {
                unepersonneannuaire = this.rechercherPersonneAnnuaireParDN(dnn, unApplicationDirContext);
            }
            else
            {
                throw new AnnuaireException(UID_ERRONE);
            }
        }
        finally
        {
            LOG.debug("Fermeture de la connexion applicative au ldap");
            this.fermerLDAPConnexionApplication(unApplicationDirContext);
        }
        return unepersonneannuaire;
    }

    /**
     * rechercherPersonneAnnuaireParUIDFonctionnel.
     * 
     * @param uidFonctionnel "documenté"
     * @return personne annuaire
     */
    @Override
    public PersonneAnnuaire rechercherPersonneAnnuaireParUIDFonctionnel(String uidFonctionnel)
    {
        LOG.debug(">>> Debut methode rechercherPersonneAnnuaireParUIDFonctionnel");
        DirContext unApplicationDirContext = null;
        PersonneAnnuaire unepersonneannuaire = null;

        try
        {
            /* ouverture de la connexion applicative vers l'annuaire */
            unApplicationDirContext = this.ouvreLDAPConnexionApplication();

            /* recherche du DN de l'utilisateur dans l'arborescence annuaire */
            String dnn = rechercherDNCompletUtilisateurPourUnUIDFonctionnel(uidFonctionnel, unApplicationDirContext);

            if (dnn != null)
            {
                unepersonneannuaire = this.rechercherPersonneAnnuaireParDN(dnn, unApplicationDirContext);
            }
            else
            {
                throw new AnnuaireException(UID_ERRONE);
            }
        }
        finally
        {
            LOG.debug("Fermeture de la connexion applicative au ldap");
            this.fermerLDAPConnexionApplication(unApplicationDirContext);
        }
        return unepersonneannuaire;
    }

    /*
     * (non-Javadoc)
     * @see fr.gouv.finances.lombok.securite.service.impl.LecturePersonneService#setBaseRechercheLDAP(java.lang.String)
     */
    /**
     * Modificateur de l attribut base recherche ldap.
     * 
     * @param baseRechercheLDAP le nouveau base recherche ldap
     */
    public void setBaseRechercheLDAP(String baseRechercheLDAP)
    {
        this.baseRechercheLDAP = baseRechercheLDAP;
    }

    /**
     * Modificateur de l attribut typeAnnuaire.
     * 
     * @param typeAnnuaire the new typeAnnuaire - String, attribut qui déterminera si annuaire fusionné (si vaut DGFIP
     *        sinon filiere ex dgcp) sept 11- amlp
     */
    public void setTypeAnnuaire(String typeAnnuaire)
    {
        this.typeAnnuaire = typeAnnuaire;
    }

    /**
     * Accesseur de l attribut listeAttributsAnnuaireALire.
     * 
     * @return listeAttributsAnnuaireALire : liste des attributs a récupérer dans l'annuaire dans le cas d'une recherche par dn 
     */
    public String[] getListeAttributsAnnuaireALire()
    {
        return listeAttributsAnnuaireALire;
    }

    /**
     * Modificateur de l attribut typeAnnuaire.
     * 
     * @param listeAttributsAnnuaireALire liste des attributs a récupérer dans l'annuaire dans le cas d'une recherche par dn 
     */
    public void setListeAttributsAnnuaireALire(String[] listeAttributsAnnuaireALire)
    {
        this.listeAttributsAnnuaireALire = listeAttributsAnnuaireALire;
    }

    /**
     * pour compatibilité annuaire fusionné /ex ennuaire dgcp , les attributs qui ont fait l'objet d'un renommage sont
     * complétés amlp sept 2011.
     * 
     * @param personneAnnuaire "documenté"
     * @return personne annuaire
     */
    protected PersonneAnnuaire gererMappingAttributsAvecChangementNomDansFusionAnnuaire(PersonneAnnuaire personneAnnuaire)
    {
        LOG.debug(">>> Debut methode gererMappingAttributsAvecChangementNomDansFusionAnnuaire");
        // cas des codiques et annexes et sages

        if (StringUtils.isNotEmpty(personneAnnuaire.getAffectationANNEXE()))
        {
            personneAnnuaire.setAnnexe(personneAnnuaire.getAffectationANNEXE());
        }

        if (StringUtils.isNotEmpty(personneAnnuaire.getAffectationCODIQUE()))
        {
            personneAnnuaire.setAffectation(personneAnnuaire.getAffectationCODIQUE());
        }

        if (StringUtils.isNotEmpty(personneAnnuaire.getAffectationSAGES()))
        {
            personneAnnuaire.setSages(personneAnnuaire.getAffectationSAGES());
        }
        return personneAnnuaire;
    }

    /**
     * Méthode permettant d'associer les attributs de l'annuaire avec les propriétés de PersonneAnnuaire.
     * 
     * @param attrs attributs de l'annuaire
     * @return PersonneAnnuaire enrichi des attributs reçus de l'annuaire
     * @throws NamingException Exception levée en cas de problème de lecture d'un attribut de l'annuraire
     */
    protected PersonneAnnuaire mapperAttributsRechercheAnnuaireAvecUnePersonneAnnuaire(Attributes attrs)
        throws NamingException
    {
        LOG.debug(">>> Debut methode mapperAttributsRechercheAnnuaireAvecUnePersonneAnnuaire");
        // Initialisation de l'objet en retour de la méthode
        PersonneAnnuaire unePersonneAnnuaire = new PersonneAnnuaire();

        // Récupération des attributs reçus de l'annuaire
        NamingEnumeration<?> uneEnum = attrs.getAll();

        // Parcours des attributs de l'annuaire
        while (uneEnum.hasMore())
        {
            Attribute personneAttr;
            try
            {
                // Récupération de l'attribut en cours de lecture
                personneAttr = (Attribute) uneEnum.next();
            }
            catch (NamingException e)
            {
                throw new AnnuaireException(PB_LECTURE_UN_ATTRIBUT_PERSONNE, e);
            }

            // Récupération du nom de l'attribut
            String nomAttr = personneAttr.getID();

            try
            {
                // Détermination de l'attribut
                switch (AttributsAnnuaire.valueOf(nomAttr.toUpperCase(Locale.FRENCH)))
                {
                    /** Attributs historiques de l'annuaire */
                    case UID:
                        unePersonneAnnuaire.setUid(this.recupererValeurAttributMonoValue(personneAttr));
                        break;
                    case AFFECTATION:
                        unePersonneAnnuaire.setAffectation(this.recupererValeurAttributMonoValue(personneAttr));
                        break;
                    case ANNEXE:
                        unePersonneAnnuaire.setAnnexe(this.recupererValeurAttributMonoValue(personneAttr));
                        break;
                    case BUSINESSCATEGORY:
                        unePersonneAnnuaire.setBusinessCategory(this.recupererValeurAttributMonoValue(personneAttr));
                        break;
                    case CN:
                        unePersonneAnnuaire.setCn(this.recupererValeurAttributMonoValue(personneAttr));
                        break;
                    case CODEFONCTION:
                        unePersonneAnnuaire.setCodeFonction(this.recupererValeurAttributMonoValue(personneAttr));
                        break;
                    case CODEGRADE:
                        unePersonneAnnuaire.setCodeGrade(this.recupererValeurAttributMonoValue(personneAttr));
                        break;
                    case DEPARTEMENT:
                        unePersonneAnnuaire.setDepartement(this.recupererValeurAttributMonoValue(personneAttr));
                        break;
                    case DEPARTMENT:
                        unePersonneAnnuaire.setDepartment(this.recupererValeurAttributMonoValue(personneAttr));
                        break;
                    case FONCTION:
                        unePersonneAnnuaire.setFonction(this.recupererValeurAttributMonoValue(personneAttr));
                        break;
                    case GIVENNAME:
                        unePersonneAnnuaire.setGivenName(this.recupererValeurAttributMonoValue(personneAttr));
                        break;
                    case IDENTDGI:
                        unePersonneAnnuaire.setIdentDGI(this.recupererValeurAttributMonoValue(personneAttr));
                        break;
                    case MAIL:
                        unePersonneAnnuaire.setMail(this.recupererValeurAttributMonoValue(personneAttr));
                        break;
                    case MAILDELIVERYOPTION:
                        unePersonneAnnuaire.setMailDeliveryOption(recupererValeurAttributMonoValue(personneAttr));
                        break;
                    case MEFIPOINTMAJF:
                        unePersonneAnnuaire.setMefiPointMAJF(recupererValeurAttributMonoValue(personneAttr));
                        break;
                    case OUCOMPLET:
                        unePersonneAnnuaire.setOuComplet(recupererValeurAttributMonoValue(personneAttr));
                        break;
                    case OUSIGLE:
                        unePersonneAnnuaire.setOuSigle(recupererValeurAttributMonoValue(personneAttr));
                        break;
                    case OUDESCRIPTION:
                        unePersonneAnnuaire.setOuDescription(recupererValeurAttributMonoValue(personneAttr));
                        break;
                    case PERSONALTITLE:
                        unePersonneAnnuaire.setPersonalTitle(recupererValeurAttributMonoValue(personneAttr));
                        break;
                    case PHYSICALDELIVERYOFFICENAME:
                        unePersonneAnnuaire.setPhysicalDeliveryOfficeName(recupererValeurAttributMonoValue(personneAttr));
                        break;
                    case POSTALADDRESS:
                        unePersonneAnnuaire.setPostalAddress(recupererValeurAttributMonoValue(personneAttr));
                        break;
                    case POSTALCODE:
                        unePersonneAnnuaire.setPostalCode(recupererValeurAttributMonoValue(personneAttr));
                        break;
                    case REGION:
                        unePersonneAnnuaire.setRegion(recupererValeurAttributMonoValue(personneAttr));
                        break;
                    case SAGES:
                        unePersonneAnnuaire.setSages(recupererValeurAttributMonoValue(personneAttr));
                        break;
                    case ROOMNUMBER:
                        unePersonneAnnuaire.setRoomNumber(recupererValeurAttributMonoValue(personneAttr));
                        break;
                    case SN:
                        unePersonneAnnuaire.setSn(recupererValeurAttributMonoValue(personneAttr));
                        break;
                    case TELEPHONENUMBER:
                        unePersonneAnnuaire.setTelephoneNumber(recupererValeurAttributMonoValue(personneAttr));
                        break;
                    case TITLE:
                        unePersonneAnnuaire.setTitle(recupererValeurAttributMonoValue(personneAttr));
                        break;
                    case MEFIAPPLIHABILITDGCP:
                        unePersonneAnnuaire.getListeBruteMefiAppliHabilitDgcp().addAll(
                            recupererValeursAttributMultiValue(personneAttr));
                        break;
                    case MEFIAPPLIDGI:
                        unePersonneAnnuaire.getListeBruteMefiAppliDgi().addAll(
                            recupererValeursAttributMultiValue(personneAttr));
                        break;
                    case PROFILAPPLICATIF:
                        /**
                         * Accès direct à l'annuaire DGI : mefiapplidgi vaut profilapplicatif Dans le cas de l'annuaire
                         * fusionné : contient les habilitations au format court
                         */
                        if (this.getTypeAnnuaire() != null
                            && this.getTypeAnnuaire().compareToIgnoreCase(DGI) == 0)
                        {
                            unePersonneAnnuaire.getListeBruteMefiAppliDgi()
                                .addAll(recupererValeursAttributMultiValue(personneAttr));
                        }
                        else
                        {
                            unePersonneAnnuaire.getListeBruteProfilApplicatif()
                                .addAll(recupererValeursAttributMultiValue(personneAttr));
                        }
                        break;

                    /** Nouveaux attributs de l'annuaire fusionné */
                    case UIDFONCTIONNEL:
                        unePersonneAnnuaire.setUidFonctionnel(recupererValeurAttributMonoValue(personneAttr));
                        break;
                    case UIDRH:
                        unePersonneAnnuaire.setUidRH(recupererValeurAttributMonoValue(personneAttr));
                        break;
                    case SAMBANTPASSWORD:
                        unePersonneAnnuaire.setSambaNTPassword(recupererValeurAttributMonoValue(personneAttr));
                        break;
                    case PASSWORDEXPIRATIONTIME:
                        unePersonneAnnuaire.setPasswordExpirationTime(recupererValeurAttributMonoValue(personneAttr));
                        break;
                    case CODEGRADERH:
                        unePersonneAnnuaire.setCodeGradeRH(recupererValeurAttributMonoValue(personneAttr));
                        break;
                    case EMPSUP:
                        unePersonneAnnuaire.setEmpsup(recupererValeurAttributMonoValue(personneAttr));
                        break;
                    case CATEGORIE:
                        unePersonneAnnuaire.setCategorie(recupererValeurAttributMonoValue(personneAttr));
                        break;
                    case NIVEAURESPONSABILITE:
                        unePersonneAnnuaire.setNiveauResponsabilite(recupererValeurAttributMonoValue(personneAttr));
                        break;
                    case OU:
                        unePersonneAnnuaire.setOu(recupererValeurAttributMonoValue(personneAttr));
                        break;
                    case AFFECTATIONCODIQUE:
                        unePersonneAnnuaire.setAffectationCODIQUE(recupererValeurAttributMonoValue(personneAttr));
                        break;
                    case AFFECTATIONANNEXE:
                        unePersonneAnnuaire.setAffectationANNEXE(recupererValeurAttributMonoValue(personneAttr));
                        break;
                    case AFFECTATIONSAGES:
                        unePersonneAnnuaire.setAffectationSAGES(recupererValeurAttributMonoValue(personneAttr));
                        break;
                    case AFFECTATIONSSECONDAIRES:
                        unePersonneAnnuaire.getListeBruteAffectationsSecondaires().addAll(
                            recupererValeursAttributMultiValue(personneAttr));
                        break;
                    case PROFILAPPLICATIFLONG:
                        unePersonneAnnuaire.getListeBruteProfilApplicatifLong().addAll(
                            recupererValeursAttributMultiValue(personneAttr));
                        break;
                    case USERCERTIFICATE:
                        unePersonneAnnuaire.setUserCertificate(recupererValeurAttributMonoValue(personneAttr));
                        break;
                    case USERPASSWORD:
                        unePersonneAnnuaire.setUserPassword(recupererValeurAttributMonoValue(personneAttr));
                        break;
                    case QUESTIONREPONSE:
                        unePersonneAnnuaire.setQuestionreponse(recupererValeurAttributMonoValue(personneAttr));
                        break;

                    /** Nouveaux attributs liés à la migration SIRHIUS en remplacement d'AGORA */
                    case UIDSIRHIUS:
                        unePersonneAnnuaire.setUidSirhius(recupererValeurAttributMonoValue(personneAttr));
                        break;
                    case CODEGRADESIRHIUS:
                        unePersonneAnnuaire.setCodeGradeSirhius(recupererValeurAttributMonoValue(personneAttr));
                        break;
                    case CODECSRH:
                        unePersonneAnnuaire.setCodeCSRH(recupererValeurAttributMonoValue(personneAttr));
                        break;

                    /** Cas annuaire des usagers */
                    case SPI:
                        unePersonneAnnuaire.setSpi(recupererValeurAttributMonoValue(personneAttr));
                        break;
                    case TYPECERTIFICAT:
                        unePersonneAnnuaire.setTypeCertificat(recupererValeurAttributMonoValue(personneAttr));
                        break;

                    /** Cas annuaire des usagers */
                    case IDORGANISME:
                        unePersonneAnnuaire.setListeBruteIdOrganisme(recupererValeursAttributMultiValue(personneAttr));
                        break;

                        /** Autres cas */
                        default:final String PREFIX_HABILITATION = "MEFIAPPLI";
                        if (nomAttr.toUpperCase(Locale.FRENCH).startsWith(PREFIX_HABILITATION))
                        {
                            unePersonneAnnuaire.getListeBruteMefiAppliXxx().addAll(
                                recupererValeursAttributMultiValue(personneAttr));
                        }
                        else
                        {
                            LOG.info("Attribut '{}', en provenance de l'annuaire, non pris en compte", nomAttr);
                        }
                }
            }
            catch (IllegalArgumentException iae)
            {
                LOG.debug("Attribut '{}', en provenance de l'annuaire, non géré ['{}']", nomAttr, iae);
            }
        }

        return unePersonneAnnuaire;
    }

    /**
     * methode Mapper resultats recherche avec liste de personne annuaire :.
     * 
     * @param resultats resultats
     * @return list< personne annuaire>
     * @throws NamingException the naming exception
     */
    protected List<PersonneAnnuaire> mapperResultatsRechercheAvecListeDePersonneAnnuaire(NamingEnumeration<?> resultats)
        throws NamingException
    {
        LOG.debug(">>> Debut methode mapperResultatsRechercheAvecListeDePersonneAnnuaire");
        List<PersonneAnnuaire> listePersonnesAnnuaires = new ArrayList<>();
        while (resultats.hasMore())
        {
            SearchResult resultat = (SearchResult) resultats.next();
            Attributes attrs = resultat.getAttributes();

            listePersonnesAnnuaires.add(mapperAttributsRechercheAnnuaireAvecUnePersonneAnnuaire(attrs));
        }
        return listePersonnesAnnuaires;
    }

    /**
     * methode Recuperer valeur attribut mono value : recupererValeurAttributMonoValue.
     * 
     * @param personneAttr personneAttr
     * @return string
     * @throws NamingException the naming exception
     */
    private String recupererValeurAttributMonoValue(Attribute personneAttr) throws NamingException
    {
        LOG.debug(">>> Debut methode recupererValeurAttributMonoValue");
        String valAttr = "";
        if (personneAttr.size() >= 1)
        {
            valAttr = (String) personneAttr.get(0).toString();
            if (personneAttr.size() > 1)
            {
                if (LOG.isDebugEnabled())
                {
                    LOG.debug("L'attribut monovalué '" + personneAttr.getID() + "' contient plus d'une valeur");
                }
            }
        }
        else
        {
            if (LOG.isDebugEnabled())
            {
                LOG.debug("L'attribut '" + personneAttr.getID() + "' ne contient pas de valeurs");
            }
        }
        return valAttr;
    }

    /**
     * methode Recuperer valeurs attribut multi value : recupererValeursAttributMultiValue.
     * 
     * @param personneAttr personneAttr
     * @return list< string>
     * @throws NamingException the naming exception
     */
    private List<String> recupererValeursAttributMultiValue(Attribute personneAttr) throws NamingException
    {
        LOG.debug(">>> Debut methode recupererValeursAttributMultiValue");
        String valAttr;
        List<String> listevaleurs = new ArrayList<>();
        for (int i = 0; i < personneAttr.size(); i++)
        {
            valAttr = (String) personneAttr.get(i);
            listevaleurs.add(valAttr);
        }
        return listevaleurs;
    }

    /**
     * Accesseur de l attribut base recherche ldap.
     * 
     * @return base recherche ldap
     */
    public String getBaseRechercheLDAP()
    {
        return baseRechercheLDAP;
    }

    /**
     * Accesseur de l attribut typeAnnuaire.
     * 
     * @return the typeAnnuaire - String, attribut qui déterminera si annuaire fusionné (si vaut DGFIP sinon filiere ex
     *         dgcp) sept 11- amlp
     */
    public String getTypeAnnuaire()
    {
        return typeAnnuaire;
    }
}
