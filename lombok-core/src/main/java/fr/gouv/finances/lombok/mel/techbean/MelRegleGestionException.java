/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : MelRegleGestionException.java
 *
 */
package fr.gouv.finances.lombok.mel.techbean;

import fr.gouv.finances.lombok.util.exception.RegleGestionException;

/**
 * Class MelRegleGestionException.
 * 
 * @author lcontinsouzas-cp
 * @version $Revision: 1.2 $ Date: 15 déc. 2009
 */
public class MelRegleGestionException extends RegleGestionException
{

    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /**
     * Instanciation de mel regle gestion exception.
     * 
     * @param message --
     */
    public MelRegleGestionException(String message)
    {
        super(message);
    }

    /**
     * Instanciation de mel regle gestion exception.
     * 
     * @param message --
     * @param cause --
     */
    public MelRegleGestionException(String message, Throwable cause)
    {
        super(message, cause);
    }

}
