/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : BooleanFormat.java
 *
 */
package fr.gouv.finances.lombok.util.format;

import java.text.FieldPosition;
import java.text.Format;
import java.text.MessageFormat;
import java.text.ParseException;
import java.text.ParsePosition;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

import org.apache.commons.lang.StringUtils;

/**
 * Class BooleanFormat
 * 
 * @author amleplatinec
 * @version $Revision: 1.2 $ Date: 11 déc. 2009
 */
public class BooleanFormat extends Format
{

    /** true text. */
    private String trueText;

    /** false text. */
    private String falseText;

    /** blank egal false. */
    private boolean blankEgalFalse = false;

    /** true set. */
    private final Set<String> trueSet = new HashSet<>();

    /** false set. */
    private final Set<String> falseSet = new HashSet<>();
    
    /** Constant : UN_ZERO. */
    public static final int UN_ZERO = 5;

    // Format prédéfinis
    /** Constant : OUI_NON. */
    public static final int OUI_NON = 1;

    /** Constant : O_N. */
    public static final int O_N = 2;

    /** Constant : VRAI_FAUX. */
    public static final int VRAI_FAUX = 3;

    /** Constant : V_F. */
    public static final int V_F = 4;

    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** Constant : MSGERR. */
    private static final String MSGERR = "La chaîne \"{0}\" ne peut pas être convertie en objet de type Boolean";

    /** Liste des noms pour la valeur TRUE. */
    private static final String[] TRUE_STRING = {"VRAI", "OUI", "O", "V", "YES", "Y", "TRUE", "T", "1"};

    /** Liste des noms pour la valeur FALSE. */
    private static final String[] FALSE_STRING = {"FAUX", "NON", "N", "F", "NO", "FALSE", "F", "0"};

    
    public BooleanFormat()
    {
        super();
        initialize();
        this.trueText = TypeFormat.getTrueString(TypeFormat.OUI_NON);
        this.falseText = TypeFormat.getFalseString(TypeFormat.OUI_NON);
    }

    public BooleanFormat(String trueText, String falseText)
    {
        super();
        initialize();
        this.falseText = falseText;
        this.trueText = trueText;
    }

    /**
     * Enum TypeFormat --.
     * 
     * @author amleplatinec
     * @version $Revision: 1.2 $ Date: 11 déc. 2009
     */
    public static enum TypeFormat
    {

        /** OU i_ non. */
        OUI_NON,

        /** O_ n. */
        O_N,

        /** VRA i_ faux. */
        VRAI_FAUX,

        /** V_ f. */
        V_F,

        /** U n_ zero. */
        UN_ZERO;

        /**
         * Accesseur de l attribut false string.
         * 
         * @param typeFormat --
         * @return false string
         */
        public static String getFalseString(final TypeFormat typeFormat)
        {
            String result;
            switch (typeFormat)
            {
                case O_N:
                    result = "N";
                    break;
                case VRAI_FAUX:
                    result = "Faux";
                    break;
                case V_F:
                    result = "F";
                    break;
                case UN_ZERO:
                    result = "0";
                    ;
                    break;
                case OUI_NON:
                default:
                    result = "Non";
                    break;
            }
            return result;
        }

        /**
         * Accesseur de l attribut true string.
         * 
         * @param typeFormat --
         * @return true string
         */
        public static String getTrueString(final TypeFormat typeFormat)
        {
            String result;
            switch (typeFormat)
            {
                case O_N:
                    result = "O";
                    break;
                case VRAI_FAUX:
                    result = "Vrai";
                    break;
                case V_F:
                    result = "V";
                    break;
                case UN_ZERO:
                    result = "1";
                    ;
                    break;
                case OUI_NON:
                default:
                    result = "Oui";
                    break;
            }
            return result;
        }
    }

    
    /**
     * Retourne une instance de BooleanFormat.
     * 
     * @param typeFormat --
     * @return the instance
     */
    public static BooleanFormat getInstance(final TypeFormat typeFormat)
    {
        return new BooleanFormat(TypeFormat.getTrueString(typeFormat), TypeFormat.getFalseString(typeFormat));
    }

   

    /**
     * Ajoute un nouveau nom pour la valeur FALSE.
     * 
     * @param noText --
     */
    public void addFalseString(final String noText)
    {
        this.falseSet.add(noText.toUpperCase(Locale.FRANCE));
    }

    /**
     * Ajoute un nouveau nom pour la valeur TRUE.
     * 
     * @param yes --
     */
    public void addTrueString(final String yes)
    {
        this.trueSet.add(yes.toUpperCase(Locale.FRANCE));
    }

    /**
     * Formate un objet et ajoute la valeur formatée au StringBuilder en paramètre.
     * 
     * @param obj --
     * @param toAppendTo --
     * @param pos --
     * @return the string buffer
     */
    public StringBuilder format(final Object obj, final StringBuilder toAppendTo, final FieldPosition pos)
    {
        if (obj == null)
        {
            if (this.blankEgalFalse)
            {
                toAppendTo.append(this.falseText);
            }
            else
            {
                toAppendTo.append("");
            }
        }

        else
        {
            if (!(obj instanceof Boolean))
            {
                throw new IllegalArgumentException("L'objet à formater n'est pas de type Boolean");
            }

            if (((Boolean) obj).booleanValue())
            {
                toAppendTo.append(this.trueText);
            }
            else
            {
                toAppendTo.append(this.falseText);
            }
        }
        return toAppendTo;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see java.text.Format#format(java.lang.Object, java.lang.StringBuffer, java.text.FieldPosition)
     */
    @Override
    public StringBuffer format(Object obj, StringBuffer toAppendTo, FieldPosition pos)
    {
        if (obj == null)
        {
            if (this.blankEgalFalse)
            {
                toAppendTo.append(this.falseText);
            }
            else
            {
                toAppendTo.append("");
            }
        }

        else
        {
            if (!(obj instanceof Boolean))
            {
                throw new IllegalArgumentException("L'objet à formater n'est pas de type Boolean");
            }

            if (((Boolean) obj).booleanValue())
            {
                toAppendTo.append(this.trueText);
            }
            else
            {
                toAppendTo.append(this.falseText);
            }
        }
        return toAppendTo;
    }

    /**
     * Accesseur de l attribut false text.
     * 
     * @return false text
     */
    public String getFalseText()
    {
        return falseText;
    }

    /**
     * Accesseur de l attribut true text.
     * 
     * @return true text
     */
    public String getTrueText()
    {
        return trueText;
    }

    /**
     * Verifie si blank egal false.
     * 
     * @return true, si c'est blank egal false
     */
    public boolean isBlankEgalFalse()
    {
        return blankEgalFalse;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param source
     * @return object
     * @throws ParseException the parse exception
     * @see java.text.Format#parseObject(java.lang.String)
     */
    @Override
    public Object parseObject(final String source) throws ParseException
    {
        return parseObject(source, new ParsePosition(0));
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param source
     * @param pos
     * @return object
     * @see java.text.Format#parseObject(java.lang.String, java.text.ParsePosition)
     */
    @Override
    public Object parseObject(final String source, final ParsePosition pos)
    {
        Boolean result = null;

        // Si la chaîne source est composée de blancs
        if (source == null || (StringUtils.isBlank(source) && isBlankEgalFalse()))
        {
            pos.setIndex(0);
            result = Boolean.FALSE;
        }
        // Sinon
        else
        {
            String sUpperCase = source.toUpperCase(Locale.FRANCE);
            if (trueSet.contains(sUpperCase))
            {
                pos.setIndex(sUpperCase.length());
                result = Boolean.TRUE;
            }
            else if (falseSet.contains(sUpperCase))
            {
                pos.setIndex(sUpperCase.length());
                result = Boolean.FALSE;
            }
            else
            {
                Object[] args = {source};
                throw new IllegalArgumentException(MessageFormat.format(MSGERR, args));
            }
        }

        return result;
    }

    /**
     * Modificateur de l attribut blank egal false.
     * 
     * @param blankEgalFalse le nouveau blank egal false
     */
    public void setBlankEgalFalse(boolean blankEgalFalse)
    {
        this.blankEgalFalse = blankEgalFalse;
    }

    /**
     * Modificateur de l attribut false text.
     * 
     * @param falseText le nouveau false text
     */
    public void setFalseText(String falseText)
    {
        this.falseText = falseText;
    }

    /**
     * Modificateur de l attribut true text.
     * 
     * @param trueText le nouveau true text
     */
    public void setTrueText(String trueText)
    {
        this.trueText = trueText;
    }

    /**
     * methode Initialize : --.
     */
    private void initialize()
    {
        for (int i = 0; i < TRUE_STRING.length; i++)
        {
            trueSet.add(TRUE_STRING[i]);
        }
        for (int i = 0; i < FALSE_STRING.length; i++)
        {
            falseSet.add(FALSE_STRING[i]);
        }
    }

}
