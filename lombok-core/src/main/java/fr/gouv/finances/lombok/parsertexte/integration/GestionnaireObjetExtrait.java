/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : GestionnaireObjetExtrait.java
 *
 */
package fr.gouv.finances.lombok.parsertexte.integration;

import java.util.Map;

import fr.gouv.finances.lombok.parsertexte.parametrage.DefinitionCustomPropertyEditor;

/**
 * Interface GestionnaireObjetExtrait --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.2 $ Date: 11 déc. 2009
 */
public interface GestionnaireObjetExtrait
{

    /** Constant : TRAITEMENT_CONTINUER. */
    public static final int TRAITEMENT_CONTINUER = 0;

    /** Constant : TRAITEMENT_ARRETER_LIGNE. */
    public static final int TRAITEMENT_ARRETER_LIGNE = 1;

    /** Constant : TRAITEMENT_ARRETER_FICHIER. */
    public static final int TRAITEMENT_ARRETER_FICHIER = 2;

    /**
     * methode Anomalie extraction : --.
     * 
     * @param pIdLigne le id ligne
     * @param pintNumLigne --
     * @param pAttribut le attribut
     * @param pMessage le message
     * @param pContenuLigne le contenu ligne
     * @return int
     */
    public int anomalieExtraction(String pIdLigne, int pintNumLigne, String pAttribut, String pMessage,
        String pContenuLigne);

    /**
     * methode Extraction ligne inconnue : --.
     * 
     * @param pLigne le ligne
     * @param pintNumLigne --
     * @return string
     */
    public String extractionLigneInconnue(LigneFichierSource pLigne, int pintNumLigne);

    /**
     * methode Extraction objet : --.
     * 
     * @param pObjetsExtraits le objets extraits
     * @param pIdLigne le id ligne
     * @param pintNumLigne --
     * @param pAnomalieDetecte le anomalie detecte
     * @param pContenuLigne le contenu ligne
     * @return int
     */
    public int extractionObjet(Map pObjetsExtraits, String pIdLigne, int pintNumLigne, boolean pAnomalieDetecte,
        String pContenuLigne);

    /**
     * methode Fin traitement fichier : --.
     * 
     * @param pPresenceAnomalieDansFichier le presence anomalie dans fichier
     */
    public void finTraitementFichier(boolean pPresenceAnomalieDansFichier);

    /**
     * methode Instanciation custom property editor : --.
     * 
     * @param pDefinition le definition
     * @return object
     */
    public Object instanciationCustomPropertyEditor(DefinitionCustomPropertyEditor pDefinition);

    /**
     * Modificateur de l attribut gestionnaire importation fichiers.
     * 
     * @param gestionnaireImportation le nouveau gestionnaire importation fichiers
     */
    public void setGestionnaireImportationFichiers(GestionnaireImportationFichiers gestionnaireImportation);
}
