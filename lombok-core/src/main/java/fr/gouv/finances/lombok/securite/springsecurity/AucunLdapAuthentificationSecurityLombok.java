/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : LdapAuthentificationSecurityLombok.java
 *
 */
package fr.gouv.finances.lombok.securite.springsecurity;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;

import fr.gouv.finances.lombok.securite.techbean.AuthentificationRegleGestionException;
import fr.gouv.finances.lombok.securite.techbean.HabilitationAnnuaire;
import fr.gouv.finances.lombok.securite.techbean.HabilitationRegleGestionException;
import fr.gouv.finances.lombok.securite.techbean.PersonneAnnuaire;
import fr.gouv.finances.lombok.util.format.FormaterDate;

/**
 * Cette classe détourne le login / mot de passe si la clé annuaire.typeannuaire vaut AUCUN.
 * Le username devient alors le DN et l e password devient alors le profil.
 * 
 * @author chouard-cp
 * @version $Revision: 1.4 $ Date: 15 déc. 2009
 */
public class AucunLdapAuthentificationSecurityLombok extends AbstractAuthentificationSecurityLombok
{
    private static final Logger log = LoggerFactory.getLogger(AucunLdapAuthentificationSecurityLombok.class);

    private String typeAnnuaire;

    public AucunLdapAuthentificationSecurityLombok()
    {
        super();
    }

    /**
     * Accesseur de l attribut type annuaire.
     *
     * @return type annuaire
     */
    public String getTypeAnnuaire()
    {
        return typeAnnuaire;
    }

    /**
     * Modificateur de l attribut type annuaire.
     *
     * @param typeAnnuaire le nouveau type annuaire
     */
    public void setTypeAnnuaire(String typeAnnuaire)
    {
        this.typeAnnuaire = typeAnnuaire;
    }

    /*
     * (non-Javadoc)
     * @see org.springframework.security.providers.dao.AbstractUserDetailsAuthenticationProvider#retrieveUser(java.lang.
     * String , org.springframework.security.providers.UsernamePasswordAuthenticationToken)
     */
    /**
     * Cette méthode est appelée par l'authenticate, elle ne vérifiera pas le mot de passe qui sera utilisé comme
     * profil. (methode de remplacement) {@inheritDoc}.
     * 
     * @param username "documenté"
     * @param auth "documenté"
     * @return user details
     * @throws AuthenticationException the authentication exception
     * @see org.springframework.security.providers.dao.AbstractUserDetailsAuthenticationProvider#retrieveUser(java.lang.String,
     *      org.springframework.security.providers.UsernamePasswordAuthenticationToken)
     */
    @Override
    public UserDetails retrieveUser(String username, UsernamePasswordAuthenticationToken auth)
        throws AuthenticationException
    {
        log.debug(">>> Debut methode retrieveUser()");
        String passwordprofil = (String) auth.getCredentials();

        PersonneAnnuaire unePersonneAnnuaire;
        try
        {
            if (log.isDebugEnabled())
            {
                log.debug("entree retrieveUser ");
            }
            // verifier du mode d'accés à l'appli
            verifierModeAuthentificationParLApplication();
            // verifier messages srm (pas de bloquant)
            verifierPasDeMessageBloquantSRM();
            // recherche personne avec habilitations aprés authentification
            unePersonneAnnuaire =
                this.construirePersonneAnnuaireSansLdap(username, passwordprofil);

            /*
             * on enleve les profils bloqués de sireme de la personne +exception si tous le sprofils sont bloqués
             */

            boolean desProfilsOntEteFiltres = filtrerProfilsBloquesParSireme(unePersonneAnnuaire);

            if (unePersonneAnnuaire.getListeHabilitations() == null || unePersonneAnnuaire.getListeHabilitations().isEmpty())
            {
                jeterExceptionPasDHabilitation(desProfilsOntEteFiltres);
            }

            appelerIntercepteurAvantHabilitation(unePersonneAnnuaire);

            String urlAppli = UtilisateurSecurityLombokContainer.getUrl();
            // construction utilisateur
            return this.construitUtilisateurSecurityLombok(unePersonneAnnuaire, username, passwordprofil, urlAppli);

        }

        /*
         * Exception Coca : Il ne faut pas ajouter l'exception e en paramètre du constructeur de l'exception
         * BadCredentialsException des 3 blocs catch suivants car le texte remonté à l'utilisateur affiche une
         * information de type "Nested exception..." qui nuit à la clarté du message
         */
        catch (AuthentificationRegleGestionException exception)
        {
            throw new BadCredentialsException(exception.getMessage(), exception);
        }
        catch (HabilitationRegleGestionException exception)
        {
            throw new BadCredentialsException(exception.getMessage(), exception);
        }
        catch (RuntimeException exception)
        {
            log.error("Erreur imprevue lors de l'authentification ", exception);
            throw new BadCredentialsException(exception.getMessage(), exception);
        }

    }

    /**
     * methode Construire personne annuaire sans LDAP
     *
     * @param usernamedn DOCUMENTEZ_MOI
     * @param passwordprofil DOCUMENTEZ_MOI
     * @return personne annuaire
     */
    private PersonneAnnuaire construirePersonneAnnuaireSansLdap(String usernamedn, String passwordprofil)
    {
        log.debug(">>> Debut methode construirePersonneAnnuaireSansLdap()");
        PersonneAnnuaire unePersonneAnnuaire;
        unePersonneAnnuaire = new PersonneAnnuaire();

        /* on décompose quand même le dnn pour obtenir l'uid */
        String[] tableauDecomposeDn =
            StringUtils.split(usernamedn, PortailAuthenticationProcessingFilter.SEPARATEUR_NIVEAUX_DANS_LE_DN);
        String uid = tableauDecomposeDn[0];

        DateFormat dateFormat = FormaterDate.getFormatDate("dd/MM/yyyy");
        Date dateDebut = null;
        Date dateFin = null;

        try
        {
            dateDebut = dateFormat.parse("01/01/2016");
            dateFin = dateFormat.parse("01/01/2099");
        }
        catch (ParseException e)
        {
            log.debug("Problème de parsing de la date qui sera utilisée pour l'habilitation selon le profil " + passwordprofil, e);
        }
        unePersonneAnnuaire = new PersonneAnnuaire();

        Set<HabilitationAnnuaire> listeHabilitations = new HashSet<HabilitationAnnuaire>();
        HabilitationAnnuaire unehabilitation = new HabilitationAnnuaire();

        unehabilitation.setNomProfil(passwordprofil);
        unehabilitation.setDateDebut(dateDebut);
        unehabilitation.setDateFin(dateFin);
        unehabilitation.setDateCreation(dateDebut);
        unehabilitation.setNomAdministrateur("Inconnu");
        unehabilitation.setLibelleCourtAppli("Inconnu");

        listeHabilitations.add(unehabilitation);
        unePersonneAnnuaire.setListeHabilitations(listeHabilitations);

        unePersonneAnnuaire.setUid(uid);
        unePersonneAnnuaire.setAffectation("Inconnu");
        unePersonneAnnuaire.setAnnexe("0");
        unePersonneAnnuaire.setCn(uid);
        unePersonneAnnuaire.setCodeFonction("Inconnu");
        unePersonneAnnuaire.setCodeGrade("Inconnu");
        unePersonneAnnuaire.setDepartement("Inconnu");
        unePersonneAnnuaire.setGivenName("Inconnu");
        unePersonneAnnuaire.setMail("Inconnu");
        unePersonneAnnuaire.setPersonalTitle("");
        unePersonneAnnuaire.setPostalAddress("Inconnu");
        unePersonneAnnuaire.setPostalCode("Inconnu");
        unePersonneAnnuaire.setSn("Inconnu");
        unePersonneAnnuaire.setTelephoneNumber("Inconnu");
        unePersonneAnnuaire.setTitle("Inconnu");

        if (log.isDebugEnabled())
        {
            log.debug("Construction utilisateur sans annuaire (annuaire.typeannuaire=AUCUN) avec uid : " + unePersonneAnnuaire.getUid());
            log.debug("Une habilitation avec profil  : " + unehabilitation.getNomProfil());

        }
        return unePersonneAnnuaire;

    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider#authenticate(org.springframework.security.core.Authentication)
     */
    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException
    {
        log.debug(">>> Debut methode authenticate()");
        Authentication auth = null;

        if ("AUCUN".equalsIgnoreCase(typeAnnuaire))
        {
            if (log.isDebugEnabled())
            {
                log.debug("Mode aucun LDAP (annuaire.typeannuaire=AUCUN) ");
            }
            auth = super.authenticate(authentication);
        }
        else
        {
            if (log.isDebugEnabled())
            {
                log.debug(
                    "Mode aucun LDAP non activé, on passe au suivant (voir applicationContext-commun-security.xml) à l'authentification avec LDAP ");
            }

        }

        return auth;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider#additionalAuthenticationChecks(org.springframework.security.core.userdetails.UserDetails,
     *      org.springframework.security.authentication.UsernamePasswordAuthenticationToken)
     */
    @Override
    protected void additionalAuthenticationChecks(UserDetails userDetails, UsernamePasswordAuthenticationToken authentication)
        throws AuthenticationException
    {
        // rien à faire
    }

}
