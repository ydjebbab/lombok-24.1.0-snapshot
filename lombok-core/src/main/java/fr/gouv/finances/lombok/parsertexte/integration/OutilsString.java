/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : OutilsString.java
 *
 */
package fr.gouv.finances.lombok.parsertexte.integration;

/**
 * Class OutilsString --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.2 $ Date: 11 déc. 2009
 */
public final class OutilsString
{

    private OutilsString()
    {
        super();
    }

    /**
     * methode Complete par des blancs : --.
     * 
     * @param pValeur le valeur
     * @param pLongueurValeurRequise le longueur valeur requise
     * @return string
     */
    public static String completeParDesBlancs(String pValeur, int pLongueurValeurRequise)
    {

        int blancsApresNom = pLongueurValeurRequise - pValeur.length();

        for (int i = 0; i < blancsApresNom; i++)
        {
            pValeur = pValeur + " ";
        }

        return pValeur;
    }

    /**
     * methode Deformatage numerique : --.
     * 
     * @param pValeur le valeur
     * @return string
     */
    public static String deformatageNumerique(String pValeur)
    {
        String tmp = pValeur.trim();
        StringBuilder pChaineResultat = new StringBuilder(tmp.length());
        if (!"".equals(tmp))
        {
            for (int i = 0; i < tmp.length(); i++)
            {
                char caractere = tmp.charAt(i);

                if (',' == caractere)
                {
                    pChaineResultat.append('.');
                }
                else if (' ' != caractere)
                {
                    pChaineResultat.append(caractere);
                }
            }
        }
        return pChaineResultat.toString();
    }

    /**
     * methode Deformatage numerique format anglais : --.
     * 
     * @param pValeur le valeur
     * @return string
     */
    public static String deformatageNumeriqueFormatAnglais(String pValeur)
    {
        String tmp = pValeur.trim();
        StringBuilder pChaineResultat = new StringBuilder(tmp.length());
        if (!"".equals(tmp))
        {
            for (int i = 0; i < tmp.length(); i++)
            {
                char caractere = tmp.charAt(i);

                if ('.' == caractere)
                {
                    pChaineResultat.append(',');
                }
                else if (',' != caractere)
                {
                    pChaineResultat.append(caractere);
                }
            }
        }
        return pChaineResultat.toString();
    }

    /**
     * Cette méthode prend en paramètre une chaine de caractère contenant un nombre et va insérer dans sa position n en
     * partant de la droite un séparateur décimal.
     * 
     * @param pValeur le valeur
     * @param pSeparateur le separateur
     * @param pPositionDepuisDroite le position depuis droite
     * @return the string
     */
    public static String insertionSeparateurDecimal(String pValeur, String pSeparateur, int pPositionDepuisDroite)
    {
        int tailleChaine = pValeur.length();

        if (tailleChaine > pPositionDepuisDroite)
        {
            return pValeur.substring(0, tailleChaine - pPositionDepuisDroite) + pSeparateur
                + pValeur.substring(tailleChaine - pPositionDepuisDroite);
        }
        else
        {
            // On doit compléter la chaine avec des zéros car la partie décimale est plus petite que la position du
            // séparateur souhaité
            for (int i = tailleChaine; i < pPositionDepuisDroite; i++)
            {
                pValeur = "0" + pValeur;
            }
            pValeur = "0" + pSeparateur + pValeur;
        }

        return "";
    }

    /**
     * Cette méthode prend en paramètre une chaine de caractère contenant un nombre et va insérer autant de zeros que
     * necessaires, pour atteindre la longueur requise de cette chaine .
     * 
     * @param pValeur le valeur
     * @param pLongueurValeurRequise le longueur valeur requise
     * @return valeur complétée par des zeros avant
     */
    public static String insertionZerosAvant(String pValeur, int pLongueurValeurRequise)
    {

        int zerosAvantMontant = pLongueurValeurRequise - pValeur.length();

        for (int i = 0; i < zerosAvantMontant; i++)
        {
            pValeur = "0" + pValeur;
        }

        return pValeur;
    }

    /**
     * methode Vide si tout a zero : --.
     * 
     * @param pValeur le valeur
     * @return string
     */
    public static String videSiToutAZero(String pValeur)
    {
        boolean bContinue = true;
        if (!"".equals(pValeur))
        {
            int i = 0;
            while ((bContinue) && (i < pValeur.length()))
            {
                if ('0' != pValeur.charAt(i++))
                {
                    bContinue = false;
                }
            }
        }

        String retour;
        if (bContinue)
        {
            retour = "";
        }
        else
        {
            // Si on a décidé d'arreter c'est qu'il y a autre chose que des zéros dans cette chaine.
            retour = pValeur;
        }
        return retour;
    }

    /**
     * methode Vide si tout a zero ou espace : --.
     * 
     * @param pValeur le valeur
     * @return string
     */
    public static String videSiToutAZeroOuEspace(String pValeur)
    {
        if (!"".equals(pValeur))
        {
            for (int i = 0; i < pValeur.length(); i++)
            {
                // On regarde si le caractère est un espace ou un 0.
                // si ce n'est pas le cas on retourne la valeur
                String caractere = pValeur.substring(i, i + 1);
                if (!(("0".equals(caractere)) || (" ".equals(caractere))))
                {
                    return pValeur;
                }
            }
        }
        // S'il n y a que des espaces ou des 0, on retourne vide
        return "";
    }

}