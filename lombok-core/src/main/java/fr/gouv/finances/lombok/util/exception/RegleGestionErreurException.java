/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 */
package fr.gouv.finances.lombok.util.exception;

import java.util.List;

/**
 * Class RegleGestionErreurException RegleGestionErreurException Sous classe de RegleGestionException qui permet de
 * distinguer les exceptions avec severite maximale.
 * 
 * @see RegleGestionException
 * @author chouard-cp
 * @version $Revision: 1.3 $ Date: 15 déc. 2009
 */
public class RegleGestionErreurException extends RegleGestionException
{

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /**
     * Instantiates a new regle gestion erreur exception.
     */
    public RegleGestionErreurException()
    {
        super();

    }

    /**
     * Instanciation de regle gestion erreur exception.
     * 
     * @param listeMessages --
     */
    public RegleGestionErreurException(List<String> listeMessages)
    {
        super(listeMessages);
    }

    /**
     * Constructeur de la classe RegleGestionErreurException.java on ajoute un message de libellé "arg0" qui se
     * retrouvera en premier dans la liste des messages
     * 
     * @param listeMessages
     * @param arg0
     */
    public RegleGestionErreurException(List<String> listeMessages, String arg0)
    {
        super(listeMessages, arg0);
    }

    /**
     * Instantiates a new regle gestion erreur exception.
     * 
     * @param arg0 the arg0
     */
    public RegleGestionErreurException(String arg0)
    {
        super(arg0);

    }

    /**
     * Instantiates a new regle gestion erreur exception.
     * 
     * @param arg0 the arg0
     * @param arg1 the arg1
     */
    public RegleGestionErreurException(String arg0, Throwable arg1)
    {
        super(arg0, arg1);

    }

    /**
     * Instantiates a new regle gestion erreur exception.
     * 
     * @param arg0 the arg0
     */
    public RegleGestionErreurException(Throwable arg0)
    {
        super(arg0);

    }

}
