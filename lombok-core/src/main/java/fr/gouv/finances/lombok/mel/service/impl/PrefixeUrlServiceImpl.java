/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : PrefixeUrlServiceImpl.java
 *
 */
package fr.gouv.finances.lombok.mel.service.impl;

import java.util.ArrayList;
import java.util.List;

import fr.gouv.finances.lombok.mel.service.PrefixeUrlService;
import fr.gouv.finances.lombok.securite.springsecurity.UtilisateurSecurityLombokContainer;
import fr.gouv.finances.lombok.util.base.BaseServiceImpl;

/**
 * Class PrefixeUrlServiceImpl --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.2 $ Date: 11 déc. 2009
 */
public class PrefixeUrlServiceImpl extends BaseServiceImpl implements PrefixeUrlService
{
    // évolutions : avril 2012 : suite à la suppression de surls en dur dans application.properties pour les modes
    // portail classique et portail internet ,
    // on ne récupére plus à la connexion utilisateur qu'une seule url .
    // suppression de toutes les autres méthodes inutiles
    // Evolutions : fevrier 2014 : bug agir sur les personnes accédant via ader
    // on continue à compléter les url pour les portails externes
    // Url du module applicatif web
    /** urlportailprefixe. */
    private String urlportailprefixe = null;

    // préfixe url pour accès direct à l'application
    /** urlappliprefixe. */
    private String urlappliprefixe = null;

    // préfixe url pour accès à l'application via le portail internet
    /** urlportailinternetprefixe. */
    private String urlportailinternetprefixe = null;

    // préfixe url pour accès à l'application via le portail sso interne
    /** urlportailappli. */
    private String urlportailappli = null;

    // mode d'authentification de l'application
    /** modeauthentification. */
    private String modeauthentification = null;

    /**
     * Constructeur de la classe PrefixeUrlServiceImpl.java
     */
    public PrefixeUrlServiceImpl()
    {
        super();

    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @return string
     * @see fr.gouv.finances.lombok.mel.service.PrefixeUrlService#construitPrefixeUrlAccessALApplicationAvecPortailInternet()
     */
    @Override
    public String construitPrefixeUrlAccessALApplicationAvecPortailInternet()
    {
        String urlprefixe = "!!valeurs non initialisées!!";
        if (this.urlportailinternetprefixe != null && this.urlportailappli != null)
        {
            urlprefixe = this.urlportailinternetprefixe + "/" + this.urlportailappli;
        }
        return urlprefixe;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @return string
     * @see fr.gouv.finances.lombok.mel.service.PrefixeUrlService#construitPrefixeUrlAccessALApplicationAvecPortailSSO()
     */
    @Override
    public String construitPrefixeUrlAccessALApplicationAvecPortailSSO()
    {
        String urlprefixe = "!!valeurs non initialisées!!";
        urlprefixe = UtilisateurSecurityLombokContainer.getUrl();

        return urlprefixe;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @return string
     * @see fr.gouv.finances.lombok.mel.service.PrefixeUrlService#construitPrefixeUrlAccessALApplicationSansPortail()
     */
    @Override
    public String construitPrefixeUrlAccessALApplicationSansPortail()
    {
        String urlprefixe = "!!valeurs non initialisées!!";
        if (this.urlappliprefixe != null && this.urlportailappli != null)
        {
            urlprefixe = this.urlappliprefixe + "/" + this.urlportailappli;
        }
        return urlprefixe;

    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @return list
     * @see fr.gouv.finances.lombok.mel.service.PrefixeUrlService#donnerListeDesPrefixesUrlPossiblesPourAccesALapplication()
     */
    @Override
    public List<String> donnerListeDesPrefixesUrlPossiblesPourAccesALapplication()
    {
        List<String> listeprefixes = new ArrayList<>();
        if (this.modeauthentification != null)
        {
            if (("appli").equalsIgnoreCase(this.modeauthentification))
            {
                listeprefixes.add(this.construitPrefixeUrlAccessALApplicationSansPortail());
            }
            else if (("appliportail").equalsIgnoreCase(this.modeauthentification))
            {
                listeprefixes.add(this.construitPrefixeUrlAccessALApplicationSansPortail());
                listeprefixes.add(this.construitPrefixeUrlAccessALApplicationAvecPortailSSO());
            }
            else
            {
                listeprefixes.add(this.construitPrefixeUrlAccessALApplicationAvecPortailSSO());
                listeprefixes.add(this.construitPrefixeUrlAccessALApplicationAvecPortailInternet());
            }
        }
        return listeprefixes;

    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @return the modeauthentification
     * @see fr.gouv.finances.lombok.mel.service.PrefixeUrlService#getModeauthentification()
     */
    @Override
    public String getModeauthentification()
    {
        return modeauthentification;
    }

    /**
     * Modificateur de l attribut modeauthentification.
     * 
     * @param modeauthentification le nouveau modeauthentification
     */
    public void setModeauthentification(String modeauthentification)
    {
        this.modeauthentification = modeauthentification;
    }

    /**
     * Modificateur de l attribut urlappliprefixe.
     * 
     * @param urlappliprefixe le nouveau urlappliprefixe
     */
    public void setUrlappliprefixe(String urlappliprefixe)
    {
        this.urlappliprefixe = urlappliprefixe;
    }

    /**
     * Modificateur de l attribut urlportailappli.
     * 
     * @param urlportailappli le nouveau urlportailappli
     */
    public void setUrlportailappli(String urlportailappli)
    {
        this.urlportailappli = urlportailappli;
    }

    /**
     * Modificateur de l attribut urlportailinternetprefixe.
     * 
     * @param urlportailinternetprefixe le nouveau urlportailinternetprefixe
     */
    public void setUrlportailinternetprefixe(String urlportailinternetprefixe)
    {
        this.urlportailinternetprefixe = urlportailinternetprefixe;
    }

    /**
     * Modificateur de l attribut urlportailprefixe.
     * 
     * @param urlportailprefixe le nouveau urlportailprefixe
     */
    public void setUrlportailprefixe(String urlportailprefixe)
    {
        this.urlportailprefixe = urlportailprefixe;
    }

}
