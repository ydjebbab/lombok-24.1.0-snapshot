/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : RetourTraitement.java
 *
 */
package fr.gouv.finances.lombok.parsertexte.integration;

import java.util.Map;

/**
 * Class RetourTraitement --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.2 $ Date: 11 déc. 2009
 */
public class RetourTraitement
{

    /** map resultat. */
    private Map mapResultat = null;

    /** retour traitement. */
    private int retourTraitement = 0;

    /** anomalie detectee. */
    private boolean anomalieDetectee = false;

    /**
     * Instanciation de retour traitement.
     * 
     * @param mapResultat --
     * @param retourTraitement --
     * @param anomalieDetectee --
     */
    public RetourTraitement(Map mapResultat, int retourTraitement, boolean anomalieDetectee)
    {
        super();
        this.mapResultat = mapResultat;
        this.retourTraitement = retourTraitement;
        this.anomalieDetectee = anomalieDetectee;
    }

    /**
     * Accesseur de l attribut map resultat.
     * 
     * @return map resultat
     */
    public Map getMapResultat()
    {
        return mapResultat;
    }

    /**
     * Accesseur de l attribut retour traitement.
     * 
     * @return retour traitement
     */
    public int getRetourTraitement()
    {
        return retourTraitement;
    }

    /**
     * Verifie si anomalie detectee.
     * 
     * @return true, si c'est anomalie detectee
     */
    public boolean isAnomalieDetectee()
    {
        return anomalieDetectee;
    }

    /**
     * Modificateur de l attribut anomalie detectee.
     * 
     * @param anomalieDetectee le nouveau anomalie detectee
     */
    public void setAnomalieDetectee(boolean anomalieDetectee)
    {
        this.anomalieDetectee = anomalieDetectee;
    }

    /**
     * Modificateur de l attribut liste resultat.
     * 
     * @param listeResultat le nouveau liste resultat
     */
    public void setListeResultat(Map listeResultat)
    {
        this.mapResultat = listeResultat;
    }

    /**
     * Modificateur de l attribut retour traitement.
     * 
     * @param retourTraitement le nouveau retour traitement
     */
    public void setRetourTraitement(int retourTraitement)
    {
        this.retourTraitement = retourTraitement;
    }
}
