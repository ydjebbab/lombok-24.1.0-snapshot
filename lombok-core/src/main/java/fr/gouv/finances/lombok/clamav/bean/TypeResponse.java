/*
 * Copyright (c) 2014 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.clamav.bean;

/**
 * Enumération des types de réponse ClamAV .
 */
public enum TypeResponse
{

    /** Recherche Virus. */
    RECHERCHE_VIRUS,
    /** Version. */
    VERSION,
    /** Statistiques. */
    STATS,
    /** Ping */
    PING
}
