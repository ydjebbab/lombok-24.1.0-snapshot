/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 *
 */
package fr.gouv.finances.lombok.securite.springsecurity;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.access.WebInvocationPrivilegeEvaluator;

import fr.gouv.finances.lombok.securite.service.VerificationDroitAccesService;
import fr.gouv.finances.lombok.util.base.BaseServiceImpl;

/**
 * Classe permettant de tester l'accessibilité d'une URL pour un utilisateur en fonction du paramétrage fait dans l'API
 * Spring Security
 *
 * @author lcontinsouzas-cp
 * @author chouard-cp
 */
public class VerificationDroitAccesSecurityServiceImpl extends BaseServiceImpl implements VerificationDroitAccesService
{
    private static final Logger log = LoggerFactory.getLogger(VerificationDroitAccesSecurityServiceImpl.class);

    /**
     * Constructeur.
     */
    public VerificationDroitAccesSecurityServiceImpl()
    {
        super();
    }

    /** Evaluateur permettant de déterminer si les utilisateurs ont les privilèges pour une URL donnée */
    private WebInvocationPrivilegeEvaluator webInvocPrivEval;

    @Override
    public boolean isURLaccessible(String url, Object objetauthentication)
    {
        log.debug(">>> Debut methode isURLaccessible()");
        if ((url == null) || (objetauthentication == null))
        {
            throw new IllegalArgumentException("Les paramètres url et objetauthentication ne peuvent être nul");
        }
        Authentication auth = (Authentication) (objetauthentication);
        if (log.isDebugEnabled())
        {
            log.debug("début de la vérification des droits d'accès URL : " + url + "pour l'utilisateur : "
                + auth.getName());
        }
        boolean estAutorise = webInvocPrivEval.isAllowed(url, auth);

        if (log.isDebugEnabled())
        {
            /*
             * puis on passe le tout au décision manager qui vérifie si l'utilisateur à accès à cette url si
             * l'utilisateur n'a pas accès on reçoit une AccessDeniedException si le configattributedefinition est null,
             * cela signifie qu'il n'y a pas de règles de filtrage pour cette URL, l'url est donc autorisée
             */

            if (estAutorise)
            {
                log.debug("Accès autorisé à l'utilisateur");
            }
            else
            {
                log.debug("Accès interdit à l'utilisateur");
                log.debug("Fin de la vérification des droits d'accès URL : " + url + "pour l'utilisateur : "
                    + auth.getName());
            }
        }

        return estAutorise;
    }

    /**
     * Accesseur de webInvocPrivEval
     *
     * @return webInvocPrivEval
     */
    public WebInvocationPrivilegeEvaluator getWebInvocPrivEval()
    {
        return webInvocPrivEval;
    }

    /**
     * Mutateur de webInvocPrivEval
     *
     * @param webInvocPrivEval webInvocPrivEval
     */
    public void setWebInvocPrivEval(WebInvocationPrivilegeEvaluator webInvocPrivEval)
    {
        this.webInvocPrivEval = webInvocPrivEval;
    }

}
