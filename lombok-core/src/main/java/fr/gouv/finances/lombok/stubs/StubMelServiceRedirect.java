/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : StubMelServiceRedirect.java
 *
 */

package fr.gouv.finances.lombok.stubs;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import javax.mail.Session;
import javax.mail.internet.InternetAddress;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import fr.gouv.finances.lombok.mel.service.MelService;
import fr.gouv.finances.lombok.mel.techbean.Mel;
import fr.gouv.finances.lombok.mel.techbean.PieceJointe;
import fr.gouv.finances.lombok.mel.techbean.PieceJointeInline;
import fr.gouv.finances.lombok.util.exception.ExploitationException;
import fr.gouv.finances.lombok.util.exception.ProgrammationException;

/**
 * Class StubMelServiceRedirect The Class StubMelServiceRedirect.
 * 
 * @author lcontinsouzas-cp Cette classe doit être utilisée comme un stub pour permettre le reroutage des mails vers une
 *         ou des adresses particulières à des fins de test Pour cela la clé notenvoimel du fichier application.properties
 *         doit être valorisée avec une valeur de la forme redirect:adressemel1,...,adressemeln Tous les envois de mel
 *         sont alors interceptés et les destinataires sont remplacés par les adresses mel lus dans la clé
 *         appli.notenvoimel
 * @author chouard-cp
 * @version $Revision: 1.4 $ Date: 15 déc. 2009
 */
public class StubMelServiceRedirect implements MelService
{
    /*
     * injection du contexte Spring complet le mel service ne peut être injecté directement car il n'est plus intercepté
     * par le StubBeanPostProcessor
     */
    /** log. */
    protected final Log log = LogFactory.getLog(getClass());

    /** Constant : STUB_MEL_REDIRECT. */
    private static final String STUB_MEL_REDIRECT = "redirect:";

    /** Constant : STUB_MEL_REDIRECT_SEPARATOR. */
    private static final String STUB_MEL_REDIRECT_SEPARATOR = ",";
    
    /** serveurmelstubredirect. */
    private MelService serveurmelstubredirect;

    /** notenvoimel. */
    private String notenvoimel;

    /** activer complement message reroutage. */
    private boolean activerComplementMessageReroutage = true;

    /** liste destinataires redirection. */
    private List<String> listeDestinatairesRedirection;

    /**
     * Constructeur de la classe StubMelServiceRedirect.java
     *
     */
    public StubMelServiceRedirect()
    {
        super();  
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param unmel
     * @see fr.gouv.finances.lombok.mel.service.MelService#envoyerMel(fr.gouv.finances.lombok.mel.techbean.Mel)
     */
    @Override
    public void envoyerMel(Mel unmel)
    {
        Mel unnouveaumel = new Mel();

        List<PieceJointe> lpj = unmel.getListePiecesJointes();
        for (Iterator<PieceJointe> iterator = lpj.iterator(); iterator.hasNext();)
        {
            PieceJointe pieceJointe = iterator.next();
            unnouveaumel.ajouterPieceJointe(pieceJointe);
        }

        List<PieceJointeInline> lpji = unmel.getListePiecesJointesInline();
        for (Iterator<PieceJointeInline> iterator = lpji.iterator(); iterator.hasNext();)
        {
            PieceJointeInline pieceJointeInline = iterator.next();
            unnouveaumel.ajouterPieceJointeInline(pieceJointeInline);
        }

        unnouveaumel.setDe(unmel.getDe());
        unnouveaumel.setEncodage(unmel.getEncodage());

        String message = unmel.getMesssage();

        if (activerComplementMessageReroutage)
        {
            message = ecrireUnComplementMessageReroutage(unmel, message);
        }

        unnouveaumel.setMesssage(message);

        unnouveaumel.setObjet(unmel.getObjet());

        try
        {
            ajouterDestinatairesRedirection(unnouveaumel);

            serveurmelstubredirect.envoyerMel(unnouveaumel);
        }
        catch (ExploitationException e)
        {
            throw new ExploitationException(
                "Erreur dans la redirection des mails - verifier la valeur de la clé appli.notenvoimel "
                    + " valeur attendue : redirect:adressemel1,...,adressemeln" + " valeur lue : " + this.notenvoimel,
                e);

        }

    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param unmel
     * @see fr.gouv.finances.lombok.mel.service.MelService#envoyerMelAvecContenuHTML(fr.gouv.finances.lombok.mel.techbean.Mel)
     */
    @Override
    public void envoyerMelAvecContenuHTML(Mel unmel)
    {
        Mel unnouveaumel = new Mel();

        List<PieceJointe> lpj = unmel.getListePiecesJointes();
        for (Iterator<PieceJointe> iterator = lpj.iterator(); iterator.hasNext();)
        {
            PieceJointe pieceJointe = iterator.next();
            unnouveaumel.ajouterPieceJointe(pieceJointe);
        }

        List<PieceJointeInline> lpji = unmel.getListePiecesJointesInline();
        for (Iterator<PieceJointeInline> iterator = lpji.iterator(); iterator.hasNext();)
        {
            PieceJointeInline pieceJointeInline = iterator.next();
            unnouveaumel.ajouterPieceJointeInline(pieceJointeInline);
        }

        unnouveaumel.setDe(unmel.getDe());
        unnouveaumel.setEncodage(unmel.getEncodage());

        String message = unmel.getMesssage();

        if (activerComplementMessageReroutage)
        {
            message = ecrireUnComplementMessageReroutage(unmel, message);
        }

        unnouveaumel.setMesssage(message);

        unnouveaumel.setObjet(unmel.getObjet());

        try
        {
            ajouterDestinatairesRedirection(unnouveaumel);

            serveurmelstubredirect.envoyerMelAvecContenuHTML(unnouveaumel);
        }
        catch (ExploitationException e)
        {
            throw new ExploitationException(
                "Erreur dans la redirection des mails - verifier la valeur de la clé appli.notenvoimel "
                    + " valeur attendue : redirect:adressemel1,...,adressemeln" + " valeur lue : " + this.notenvoimel,
                e);

        }

    }

    /**
     * Accesseur de l attribut activer complement message reroutage.
     * 
     * @return activer complement message reroutage
     */
    public boolean getActiverComplementMessageReroutage()
    {
        return activerComplementMessageReroutage;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @return host
     * @see fr.gouv.finances.lombok.mel.service.MelService#getHost()
     */
    @Override
    public String getHost()
    {
        return null;
    }

    /**
     * Accesseur de l attribut javamailproperties.
     * 
     * @return javamailproperties
     */
    public Properties getJavamailproperties()
    {
        return null;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @return java mail properties
     * @see fr.gouv.finances.lombok.mel.service.MelService#getJavaMailProperties()
     */
    @Override
    public Properties getJavaMailProperties()
    {
        return null;
    }

    /**
     * Accesseur de l attribut notenvoimel.
     * 
     * @return notenvoimel
     */
    public String getNotenvoimel()
    {
        return notenvoimel;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @return password
     * @see fr.gouv.finances.lombok.mel.service.MelService#getPassword()
     */
    @Override
    public String getPassword()
    {
        return null;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @return port
     * @see fr.gouv.finances.lombok.mel.service.MelService#getPort()
     */
    @Override
    public int getPort()
    {
        return 0;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @return protocol
     * @see fr.gouv.finances.lombok.mel.service.MelService#getProtocol()
     */
    @Override
    public String getProtocol()
    {
        return null;
    }

    /**
     * Accesseur de l attribut serveurmelstubredirect.
     * 
     * @return serveurmelstubredirect
     */
    public MelService getServeurmelstubredirect()
    {
        return serveurmelstubredirect;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @return session
     * @see fr.gouv.finances.lombok.mel.service.MelService#getSession()
     */
    @Override
    public Session getSession()
    {
        return null;
    }

    /**
     * Accesseur de l attribut username.
     * 
     * @return username
     */
    public String getUsername()
    {
        return null;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @return user name
     * @see fr.gouv.finances.lombok.mel.service.MelService#getUserName()
     */
    public String getUserName()
    {
        return null;
    }

    /**
     * Modificateur de l attribut activer complement message reroutage.
     * 
     * @param activerComplementMessageReroutage le nouveau activer complement message reroutage
     */
    public void setActiverComplementMessageReroutage(boolean activerComplementMessageReroutage)
    {
        this.activerComplementMessageReroutage = activerComplementMessageReroutage;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param arg0 le nouveau host
     * @see fr.gouv.finances.lombok.mel.service.MelService#setHost(java.lang.String)
     */
    @Override
    public void setHost(String arg0)
    {
        // stub :méthode non remplie
    }

    /**
     * Modificateur de l attribut javamailproperties.
     * 
     * @param arg0 le nouveau javamailproperties
     */
    public void setJavamailproperties(Properties arg0)
    {
        // stub :méthode non remplie
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param javamailproperties le nouveau java mail properties
     * @see fr.gouv.finances.lombok.mel.service.MelService#setJavaMailProperties(java.util.Properties)
     */
    @Override
    public void setJavaMailProperties(Properties javamailproperties)
    {
        // stub :méthode non remplie
    }

    /**
     * Modificateur de l attribut notenvoimel.
     * 
     * @param notenvoimel le nouveau notenvoimel
     */
    public void setNotenvoimel(String notenvoimel)
    {
        this.notenvoimel = notenvoimel;

        /*
         * Calcul declenche dans le setter pour obtenir une éventuelle erreur directement au démarrage (ne peut être mis
         * dans le constructeur attribut notenvoimel à null
         */
        this.listeDestinatairesRedirection = this.extraireListeDestinatairesDeLaRedirection();
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param arg0 le nouveau password
     * @see fr.gouv.finances.lombok.mel.service.MelService#setPassword(java.lang.String)
     */
    @Override
    public void setPassword(String arg0)
    {
        // stub :méthode non remplie
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param arg0 le nouveau port
     * @see fr.gouv.finances.lombok.mel.service.MelService#setPort(int)
     */
    @Override
    public void setPort(int arg0)
    {
        // stub :méthode non remplie
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param arg0 le nouveau protocol
     * @see fr.gouv.finances.lombok.mel.service.MelService#setProtocol(java.lang.String)
     */
    @Override
    public void setProtocol(String arg0)
    {
        // stub :méthode non remplie
    }

    /**
     * Modificateur de l attribut serveurmelstubredirect.
     * 
     * @param serveurmelstubredirect le nouveau serveurmelstubredirect
     */
    public void setServeurmelstubredirect(MelService serveurmelstubredirect)
    {
        this.serveurmelstubredirect = serveurmelstubredirect;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param arg0 le nouveau session
     * @see fr.gouv.finances.lombok.mel.service.MelService#setSession(javax.mail.Session)
     */
    @Override
    public void setSession(Session arg0)
    {
        // stub :méthode non remplie
    }

    /**
     * Modificateur de l attribut username.
     * 
     * @param arg0 le nouveau username
     */
    public void setUsername(String arg0)
    {
        // stub :méthode non remplie
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param username le nouveau user name
     * @see fr.gouv.finances.lombok.mel.service.MelService#setUserName(java.lang.String)
     */
    @Override
    public void setUserName(String username)
    {
        // stub :méthode non remplie
    }

    /**
     * methode Ajouter destinataires redirection : --.
     * 
     * @param unnouveaumel --
     */
    private void ajouterDestinatairesRedirection(Mel unnouveaumel)
    {
        if (this.listeDestinatairesRedirection == null)
        {
            throw new ProgrammationException("Problème de configuration le stub est activé à tort alors que la valeur "
                + STUB_MEL_REDIRECT
                + "n'est pas présente au début de la propriété notenvoimel (valeur lue : " + notenvoimel + ")");
        }

        for (Iterator<String> iterator = this.listeDestinatairesRedirection.iterator(); iterator.hasNext();)
        {
            String unDestinataireRedirection = iterator.next();
            unnouveaumel.ajouterDestinataireA(unDestinataireRedirection);
        }
    }

    /**
     * methode Ecrire un complement message reroutage : --.
     * 
     * @param unmel --
     * @param message --
     * @return string
     */
    private String ecrireUnComplementMessageReroutage(Mel unmel, String message)
    {
        String lfe = System.getProperty("line.separator");
        message = message + lfe;
        message = message + "------------------------------" + lfe;
        message = message + "message reroute par stub melserviceredirect" + lfe;
        message = message + "destinataires initiaux listés ci-dessous" + lfe;

        List<InternetAddress> listeAdresses = unmel.getListeDestinatairesA();

        if (!listeAdresses.isEmpty())
        {
            for (Iterator<InternetAddress> iterator = listeAdresses.iterator(); iterator.hasNext();)
            {
                InternetAddress internetAddress = iterator.next();
                message = message + internetAddress.getAddress() + "(" + internetAddress.getPersonal() + ")" + lfe;
            }
        }
        else if (unmel.getListeDestinatairesA() != null)
        {
            message =
                message + unmel.getListeDestinatairesA().get(0).getAddress() + "("
                    + unmel.getListeDestinatairesA().get(0).getPersonal() + ")" + lfe;
        }
        else
        {
            message =
                message
                    + "!!!Attention aucun destinataire initial : il y aura une erreur en fonctionnement normal !!!!"
                    + lfe;
        }
        message = message + "------------------------------";
        return message;
    }

    /**
     * methode Extraire liste destinataires de la redirection : --.
     * 
     * @return list< string>
     */
    private List<String> extraireListeDestinatairesDeLaRedirection()
    {
        List<String> listeDestiDeLaRedirection = new ArrayList<>();
        if (!notenvoimel.startsWith(STUB_MEL_REDIRECT))
        {
            listeDestiDeLaRedirection = null;
        }
        else
        {
            String adressesMelRedirection =
                this.getNotenvoimel().substring(STUB_MEL_REDIRECT.length());

            String[] champs = adressesMelRedirection.split(STUB_MEL_REDIRECT_SEPARATOR);

            for (int i = 0; i < champs.length; i++)
            {
                listeDestiDeLaRedirection.add(champs[i]);
                log.warn("Destinataire spécifique pour l'envoi des mails : " + champs[i]);
            }
        }

        return listeDestiDeLaRedirection;
    }

}
