/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : BaseMethodBeforeAdvice.java
 *
 */
package fr.gouv.finances.lombok.util.spring;

import java.lang.reflect.Method;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.aop.MethodBeforeAdvice;

import fr.gouv.finances.lombok.util.exception.ProgrammationException;

/**
 * Class BaseMethodBeforeAdvice.
 * 
 * @author hbesson2-cp
 * @description exemple d'implementation de l'AOP. Interception à l'entrée de la méthode. Trace du nom de la méthode, du
 *              type de chacun de ses paramètres. Si un param est null -> renvoi d'une exception de type
 *              ProgrammationException
 * @version $Revision: 1.4 $ Date: 15 déc. 2009
 */
public class BaseMethodBeforeAdvice implements MethodBeforeAdvice
{
    protected final Log log = LogFactory.getLog(getClass());

    public BaseMethodBeforeAdvice()
    {
        super();
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param methode
     * @param listeParam
     * @param arg2
     * @throws Throwable the throwable
     * @see org.springframework.aop.MethodBeforeAdvice#before(java.lang.reflect.Method, java.lang.Object[],
     *      java.lang.Object)
     */
    @Override
    public void before(Method methode, Object[] listeParam, Object arg2) throws Throwable
    {
        // affichage du nom de la methode
        if (log.isDebugEnabled())
        {
            log.debug("Methode = " + methode.getName());

            if (listeParam != null)
            {
                for (int i = 0; i < listeParam.length; i++)
                {
                    if (listeParam[i] != null)
                    {
                        // Journalisation des paramètres
                        if (log.isDebugEnabled())
                        {
                            log.debug("type param = " + listeParam[i].getClass());
                        }
                    }
                    else
                    {
                        ProgrammationException programmationException =
                            new ProgrammationException(
                                " Erreur de programmation : appel d'un service métier avec au moins un paramètre null. Méthode : "
                                    + methode.getClass().getName() + "; Méthode : " + methode.getName() + "; param : " + i
                                    + " = null");

                        log.error(programmationException.getMessage());
                    }
                }
            }
        }
    }
}
