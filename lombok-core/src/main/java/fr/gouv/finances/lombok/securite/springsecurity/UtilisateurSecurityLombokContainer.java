/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : UtilisateurSecurityLombokContainer.java
 *
 */
package fr.gouv.finances.lombok.securite.springsecurity;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import fr.gouv.finances.lombok.securite.techbean.PersonneAnnuaire;

/**
 * Classe permettant d'accéder au contexte sécurisé à partir d'une JSP, ou dans la couche présentation
 * 
 * @author wpetit-cp
 * @version $Revision: 1.3 $ Date: 15 déc. 2009
 */
public class UtilisateurSecurityLombokContainer
{
    private static final Logger log = LoggerFactory.getLogger(UtilisateurSecurityLombokContainer.class);
    
    /** utilisateur security lombok. */
    private UtilisateurSecurityLombok utilisateurSecurityLombok;
    
    /**
     * Instanciation de utilisateur security lombok container.
     */
    public UtilisateurSecurityLombokContainer()
    {
        Authentication pa = SecurityContextHolder.getContext().getAuthentication();
        if (pa != null && !(pa instanceof AnonymousAuthenticationToken))
        {
            utilisateurSecurityLombok = (UtilisateurSecurityLombok) pa.getPrincipal();
        }
    }

    /**
     * Gets the personne annuaire.
     * 
     * @return Objet PersonneAnnuaire du contexte sécurisé
     */
    public static final PersonneAnnuaire getPersonneAnnuaire()
    {
        log.debug(">>> Debut methode getPersonneAnnuaire()");
        PersonneAnnuaire result = null;
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        if (authentication != null)
        {
            UtilisateurSecurityLombok utilisateurSecurityLombok =
                (UtilisateurSecurityLombok) authentication.getPrincipal();

            if (utilisateurSecurityLombok != null)
            {
                result = utilisateurSecurityLombok.getPersonneAnnuaire();
            }
        }
        return result;
    }

    /**
     * Gets the url.
     * 
     * @return Objet url mise dans UtilisateurSecurityLombok
     */
    public static final String getUrl()
    {
        log.debug(">>> Debut methode getUrl()");
        String result = "";
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        if (authentication != null)
        {
            UtilisateurSecurityLombok utilisateurSecurityLombok =
                (UtilisateurSecurityLombok) authentication.getPrincipal();

            if (utilisateurSecurityLombok != null)
            {
                result = utilisateurSecurityLombok.getUrlPortail();
            }
        }
        return result;
    }


   

    /**
     * Gets the utilisateur security lombok.
     * 
     * @return Objet UtilisateurSecurityLombok du contexte sécurisé
     */
    public UtilisateurSecurityLombok getUtilisateurSecurityLombok()
    {
        return utilisateurSecurityLombok;
    }
}
