/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.apptags.table.view.jrxmlinfo;

import java.io.Serializable;

import net.sf.jasperreports.engine.type.HorizontalAlignEnum;
import net.sf.jasperreports.engine.type.LineSpacingEnum;
import net.sf.jasperreports.engine.type.VerticalAlignEnum;

/**
 * Class AbstractBandInfo
 */
public abstract class AbstractBandInfo implements Serializable, InterfaceBandInfo
{

    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** height. */
    private int height;

    /** style. */
    private FontStyleInfo style;

    /** top padding. */
    private int topPadding;

    /** bottom padding. */
    private int bottomPadding;

    /** right padding. */
    private int rightPadding;

    /** left padding. */
    private int leftPadding;

    /** horizontal alignment. */
    private HorizontalAlignEnum horizontalAlignment;

    /** vertical alignment. */
    private VerticalAlignEnum verticalAlignment;

    /** line spacing. */
    private LineSpacingEnum lineSpacing;

    /**
     * Instanciation de abstract band info.
     */
    public AbstractBandInfo()
    {
        super();
        this.style = new FontStyleInfo();
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.view.jrxmlinfo.InterfaceBandInfo#getHeight()
     */
    @Override
    public int getHeight()
    {
        return height;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.view.jrxmlinfo.InterfaceBandInfo#setHeight(int)
     */
    @Override
    public void setHeight(int height)
    {
        this.height = height;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.view.jrxmlinfo.InterfaceBandInfo#getStyle()
     */
    @Override
    public FontStyleInfo getStyle()
    {
        return style;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.view.jrxmlinfo.InterfaceBandInfo#setStyle(fr.gouv.finances.lombok.apptags.table.view.jrxmlinfo.FontStyleInfo)
     */
    @Override
    public void setStyle(FontStyleInfo style)
    {
        this.style = style;
    }

    /**
     * Accesseur de l attribut bottom padding.
     * 
     * @return the bottomPadding
     */
    public int getBottomPadding()
    {
        return bottomPadding;
    }

    /**
     * Modificateur de l attribut bottom padding.
     * 
     * @param bottomPadding the bottomPadding to set
     */
    public void setBottomPadding(int bottomPadding)
    {
        this.bottomPadding = bottomPadding;
    }

    /**
     * Accesseur de l attribut horizontal alignment.
     * 
     * @return the horizontalAlignment
     */
    public HorizontalAlignEnum getHorizontalAlignment()
    {
        return horizontalAlignment;
    }

    /**
     * Modificateur de l attribut horizontal alignment.
     * 
     * @param horizontalAlignment the horizontalAlignment to set
     */
    public void setHorizontalAlignment(HorizontalAlignEnum horizontalAlignment)
    {
        this.horizontalAlignment = horizontalAlignment;
    }

    /**
     * Accesseur de l attribut left padding.
     * 
     * @return the leftPadding
     */
    public int getLeftPadding()
    {
        return leftPadding;
    }

    /**
     * Modificateur de l attribut left padding.
     * 
     * @param leftPadding the leftPadding to set
     */
    public void setLeftPadding(int leftPadding)
    {
        this.leftPadding = leftPadding;
    }

    /**
     * Accesseur de l attribut line spacing.
     * 
     * @return the lineSpacing
     */
    public LineSpacingEnum getLineSpacing()
    {
        return lineSpacing;
    }

    /**
     * Modificateur de l attribut line spacing.
     * 
     * @param lineSpacing the lineSpacing to set
     */
    public void setLineSpacing(LineSpacingEnum lineSpacing)
    {
        this.lineSpacing = lineSpacing;
    }

    /**
     * Accesseur de l attribut right padding.
     * 
     * @return the rightPadding
     */
    public int getRightPadding()
    {
        return rightPadding;
    }

    /**
     * Modificateur de l attribut right padding.
     * 
     * @param rightPadding the rightPadding to set
     */
    public void setRightPadding(int rightPadding)
    {
        this.rightPadding = rightPadding;
    }

    /**
     * Accesseur de l attribut top padding.
     * 
     * @return the topPadding
     */
    public int getTopPadding()
    {
        return topPadding;
    }

    /**
     * Modificateur de l attribut top padding.
     * 
     * @param topPadding the topPadding to set
     */
    public void setTopPadding(int topPadding)
    {
        this.topPadding = topPadding;
    }

    /**
     * Accesseur de l attribut vertical alignment.
     * 
     * @return the verticalAlignment
     */
    public VerticalAlignEnum getVerticalAlignment()
    {
        return verticalAlignment;
    }

    /**
     * Modificateur de l attribut vertical alignment.
     * 
     * @param verticalAlignment the verticalAlignment to set
     */
    public void setVerticalAlignment(VerticalAlignEnum verticalAlignment)
    {
        this.verticalAlignment = verticalAlignment;
    }

}
