/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.apptags.table.view.jrxmlinfo;

import java.util.ArrayList;
import java.util.List;

/**
 * Class SubgroupHeaderInfo
 */
public class SubgroupHeaderInfo implements HeaderCellInfoI
{

    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** previous subgroup. */
    private SubgroupHeaderInfo previousSubgroup;

    /** titles headers infos. */
    private List<TitleHeaderInfo> titlesHeadersInfos = new ArrayList<>();

    /** value. */
    private String value;

    /** height. */
    private int height;

    /** parent header. */
    private HeaderCellInfoI parentHeader;

    /** space before. */
    private int spaceBefore;

    /** absorbe. */
    private boolean absorbe = false;

    /** merge with child. */
    private boolean mergeWithChild = false;

    /**
     * Instanciation de subgroup header info.
     */
    public SubgroupHeaderInfo()
    {
        super();
    }

    /**
     * calcule la largeur d'un sous groupe.
     * 
     * @return width calcule
     */
    @Override
    public int getWidthCalcule()
    {
        int wid = 0;
        for (TitleHeaderInfo title : titlesHeadersInfos)
        {
            wid = wid + title.getWidth();
        }

        return wid;
    }

    /**
     * Calcule l'abscisse de coin haut gauche de la cellule représentant un groupe.
     * 
     * @return x left corner calcule
     */
    @Override
    public int getXLeftCornerCalcule()
    {
        int abs = 0;
        if (previousSubgroup != null)
        {
            abs = abs + previousSubgroup.getXLeftCornerCalcule() + previousSubgroup.getWidthCalcule();
        }

        return abs;
    }

    /**
     * Calcule l'ordonnée du coin haut gauche de la cellule représentant un sous-groupe.
     * 
     * @return y left corner calcule
     */
    @Override
    public int getYLeftCornerCalcule()
    {
        int ord = 0;
        if (parentHeader == null)
        {
            ord = this.spaceBefore;
        }
        else
        {
            ord = this.parentHeader.getYLeftCornerCalcule() + this.parentHeader.getHeight();
        }

        return ord;
    }

    /**
     * methode Rejoindre le groupe :
     * 
     * @param parentHeader
     */
    public void rejoindreLeGroupe(HeaderCellInfoI parentHeader)
    {
        this.parentHeader = parentHeader;
    }

    /**
     * methode Ajouter un titre au sous groupe :
     * 
     * @param titleHeaderInfo
     * @return subgroup header info
     */
    public SubgroupHeaderInfo ajouterUnTitreAuSousGroupe(TitleHeaderInfo titleHeaderInfo)
    {
        titleHeaderInfo.rejoindreSousGroupeOuGroupe(this);
        this.titlesHeadersInfos.add(titleHeaderInfo);

        return this;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.view.jrxmlinfo.HeaderCellInfoI#isMergeWithChild()
     */
    @Override
    public boolean isMergeWithChild()
    {
        return mergeWithChild;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.view.jrxmlinfo.HeaderCellInfoI#setMergeWithChild(boolean)
     */
    @Override
    public void setMergeWithChild(boolean mergeWithChild)
    {
        this.mergeWithChild = mergeWithChild;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.view.jrxmlinfo.HeaderCellInfoI#isAbsorbe()
     */
    @Override
    public boolean isAbsorbe()
    {
        return absorbe;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.view.jrxmlinfo.HeaderCellInfoI#setAbsorbe(boolean)
     */
    @Override
    public void setAbsorbe(boolean absorbe)
    {
        this.absorbe = absorbe;
    }

    /**
     * Accesseur de l attribut previous subgroup.
     * 
     * @return previous subgroup
     */
    public SubgroupHeaderInfo getPreviousSubgroup()
    {
        return previousSubgroup;
    }

    /**
     * Modificateur de l attribut previous subgroup.
     * 
     * @param previousSubgroup le nouveau previous subgroup
     */
    public void setPreviousSubgroup(SubgroupHeaderInfo previousSubgroup)
    {
        this.previousSubgroup = previousSubgroup;
    }

    /**
     * Accesseur de l attribut space before.
     * 
     * @return space before
     */
    public int getSpaceBefore()
    {
        return spaceBefore;
    }

    /**
     * Modificateur de l attribut space before.
     * 
     * @param spaceBefore le nouveau space before
     */
    public void setSpaceBefore(int spaceBefore)
    {
        this.spaceBefore = spaceBefore;
    }

    /**
     * Accesseur de l attribut parent header.
     * 
     * @return parent header
     */
    public HeaderCellInfoI getParentHeader()
    {
        return parentHeader;
    }

    /**
     * Modificateur de l attribut parent header.
     * 
     * @param parentHeader le nouveau parent header
     */
    public void setParentHeader(HeaderCellInfoI parentHeader)
    {
        this.parentHeader = parentHeader;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.view.jrxmlinfo.HeaderCellInfoI#getHeight()
     */
    @Override
    public int getHeight()
    {
        return height;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.view.jrxmlinfo.HeaderCellInfoI#setHeight(int)
     */
    @Override
    public void setHeight(int height)
    {
        this.height = height;
    }

    /**
     * Accesseur de l attribut titles headers infos.
     * 
     * @return the titlesHeadersInfos
     */
    public List<TitleHeaderInfo> getTitlesHeadersInfos()
    {
        return titlesHeadersInfos;
    }

    /**
     * Modificateur de l attribut titles headers infos.
     * 
     * @param titlesHeadersInfos the titlesHeadersInfos to set
     */
    public void setTitlesHeadersInfos(List<TitleHeaderInfo> titlesHeadersInfos)
    {
        this.titlesHeadersInfos = titlesHeadersInfos;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.view.jrxmlinfo.HeaderCellInfoI#getValue()
     */
    @Override
    public String getValue()
    {
        return value;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.view.jrxmlinfo.HeaderCellInfoI#setValue(java.lang.String)
     */
    @Override
    public void setValue(String value)
    {
        this.value = value;
    }

}
