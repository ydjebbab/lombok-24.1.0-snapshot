/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.apptags.table.view.jrxmlinfo;

import java.awt.Color;
import java.util.List;

import net.sf.jasperreports.engine.JRStyle;
import net.sf.jasperreports.engine.design.JRDesignBand;
import net.sf.jasperreports.engine.design.JRDesignExpression;
import net.sf.jasperreports.engine.design.JRDesignTextField;
import net.sf.jasperreports.engine.type.ModeEnum;
import net.sf.jasperreports.engine.type.PositionTypeEnum;
import net.sf.jasperreports.engine.type.StretchTypeEnum;

import org.apache.commons.lang.StringUtils;

import fr.gouv.finances.lombok.apptags.table.bean.Column;
import fr.gouv.finances.lombok.apptags.table.core.TableModel;

/**
 * Class ColumnHeaderUtil
 */
public class ColumnHeaderUtil
{

    /**
     * Enum HeaderLevel
     */
    private enum HeaderLevel
    {

        /** one. */
        ONE(1),
        /** two. */
        TWO(2),
        /** three. */
        THREE(3);

        /** level. */
        private int level;

        /**
         * Instanciation de header level.
         * 
         * @param level level
         */
        private HeaderLevel(final int level)
        {
            this.level = level;
        }

        /**
         * Accesseur de l attribut level.
         * 
         * @return level
         */
        public int getLevel()
        {
            return level;
        }
    }

    /** Constant : ATTR_GROUP. */
    private static final String ATTR_GROUP = "group";

    /** Constant : ATTR_SUBGROUP. */
    private static final String ATTR_SUBGROUP = "subGroup";

    /**
     * Retourne la valeur de l'attribut group d'une colonne d'un tableau ec.
     * 
     * @param column column
     * @return group
     */
    private static String getGroup(Column column)
    {
        return column.getAttributeAsString(ATTR_GROUP);
    }

    /**
     * Retourne la valeur de l'attribut subGroup d'une colonne d'un tableau ec.
     * 
     * @param column column
     * @return subgroup
     */
    private static String getSubgroup(Column column)
    {
        return column.getAttributeAsString(ATTR_SUBGROUP);
    }

    /**
     * Détermination du niveau d'imbrication maximum.
     * 
     * @param model model
     * @return HeaderLevel
     */
    private static HeaderLevel findColumnHeaderLevel(TableModel model)
    {
        List<Column> columns = model.getColumnHandler().getColumns();
        // Par défaut, le niveau est 1 : un titre seul
        HeaderLevel level = HeaderLevel.ONE;
        boolean levelFound = false;

        for (Column column : columns)
        {
            if (StringUtils.isNotBlank(getSubgroup(column)))
            {
                level = HeaderLevel.THREE;
                levelFound = true;
                break;
            }
        }

        if (!levelFound)
        {
            for (Column column : columns)
            {
                // Si au moins un attribut group est positionné, le niveau est 2
                if (StringUtils.isNotBlank(getGroup(column)))
                {
                    level = HeaderLevel.TWO;
                    levelFound = true;
                    break;
                }
            }
        }

        return level;
    }

    /**
     * Analyse les paramètres group, subGroup et title pour construire l'entête du tableau.
     * 
     * @param model model
     * @param documentInfo documentInfo
     * @param columnHeaderBand columnHeaderBand
     * @param jrStyle jrStyle
     */
    public static void groupSubgroupTitleHeaderAnalyse(TableModel model, DocumentInfo documentInfo,
        JRDesignBand columnHeaderBand, JRStyle jrStyle)
    {

        HeaderLevel headerLevel = findColumnHeaderLevel(model);
        documentInfo.getColumnHeader().setRows(headerLevel.getLevel());

        affecteTailleColumnHeader(model, documentInfo, columnHeaderBand);

        switch (headerLevel)
        {
            case THREE:
                ColumnHeaderUtil.construitColumnHeaderThreeLevel(model, documentInfo, columnHeaderBand, jrStyle);
                break;

            case TWO:
                ColumnHeaderUtil.construitColumnHeaderTwoLevel(model, documentInfo, columnHeaderBand, jrStyle);
                break;
            default:
                ColumnHeaderUtil.construitColumnHeaderOneLevel(model, documentInfo, columnHeaderBand, jrStyle);
                break;
        }
    }

    /**
     * Contruit un entête de colonne contenant trois niveaux (groupe, sous-groupe et titre).
     * 
     * @param model model
     * @param documentInfo documentInfo
     * @param columnHeaderBand columnHeaderBand
     * @param jrStyle jrStyle
     */
    private static void construitColumnHeaderThreeLevel(TableModel model, DocumentInfo documentInfo,
        JRDesignBand columnHeaderBand, JRStyle jrStyle)
    {
        ColumnHeaderUtil.construitColumnHeaderInfoThreeLevel(model, documentInfo, columnHeaderBand);

        ColumnHeaderUtil.insertColumnHeaderGroupSubGroupAndTitle(model, documentInfo, columnHeaderBand, jrStyle);
    }

    /**
     * Construit un entête de colonne contenant deux niveaux (groupe et titre).
     * 
     * @param model model
     * @param documentInfo documentInfo
     * @param columnHeaderBand columnHeaderBand
     * @param jrStyle jrStyle
     */
    private static void construitColumnHeaderTwoLevel(TableModel model, DocumentInfo documentInfo,
        JRDesignBand columnHeaderBand, JRStyle jrStyle)
    {

        ColumnHeaderUtil.construitColumnHeaderInfoTwoLevel(model, documentInfo, columnHeaderBand);

        ColumnHeaderUtil.insertColumnHeaderGroupAndTitle(model, documentInfo, columnHeaderBand, jrStyle);
    }

    /**
     * methode Calcule taille column header :
     * 
     * @param model model
     * @param documentInfo documentInfo
     * @param columnHeaderBand columnHeaderBand
     */
    private static void calculeTailleColumnHeader(TableModel model, DocumentInfo documentInfo,
        JRDesignBand columnHeaderBand)
    {
        int columnHeaderbandHeight;
        ColumnHeaderInfo columnHeaderInfo = documentInfo.getColumnHeader();

        switch (findColumnHeaderLevel(model))
        {
            case THREE:
                columnHeaderbandHeight =
                    columnHeaderInfo.getSpaceBefore() + columnHeaderInfo.getTitleHeight()
                        + columnHeaderInfo.getSubgroupHeight() + columnHeaderInfo.getGroupHeight() + 1;
                break;
            case TWO:
                columnHeaderbandHeight =
                    columnHeaderInfo.getSpaceBefore() + columnHeaderInfo.getTitleHeight()
                        + columnHeaderInfo.getGroupHeight() + 1;
                break;
            default:
                columnHeaderbandHeight = columnHeaderInfo.getSpaceBefore() + columnHeaderInfo.getTitleHeight() + 1;
                break;
        }
        documentInfo.getColumnHeader().setHeight(columnHeaderbandHeight);
    }

    /**
     * methode Affecte taille column header :
     * 
     * @param model model
     * @param documentInfo documentInfo
     * @param columnHeaderBand columnHeaderBand
     */
    private static void affecteTailleColumnHeader(TableModel model, DocumentInfo documentInfo,
        JRDesignBand columnHeaderBand)
    {
        calculeTailleColumnHeader(model, documentInfo, columnHeaderBand);
        columnHeaderBand.setHeight(documentInfo.getColumnHeader().getHeight());
    }

    /**
     * Construit un entête de colonne contenant un niveau.
     * 
     * @param model model
     * @param documentInfo documentInfo
     * @param columnHeaderBand columnHeaderBand
     * @param jrStyle jrStyle
     */
    private static void construitColumnHeaderOneLevel(TableModel model, DocumentInfo documentInfo,
        JRDesignBand columnHeaderBand, JRStyle jrStyle)
    {
        ColumnHeaderUtil.construitColumnHeaderInfoOneLevel(model, documentInfo, columnHeaderBand);

        ColumnHeaderUtil.ajoutEspaceAvantTitre(model, documentInfo, columnHeaderBand);

        ColumnHeaderUtil.insertColumnHeaderTitle(model, documentInfo, columnHeaderBand, jrStyle);

    }

    /**
     * * Trace les bordures des cellules des entêtes de colonnes du tableau.
     * 
     * @param documentInfo documentInfo
     * @param columnHeaderBand columnHeaderBand
     * @param headerCellInfo headerCellInfo
     */
    public static void traceCellBorder(DocumentInfo documentInfo, JRDesignBand columnHeaderBand,
        HeaderCellInfoI headerCellInfo)
    {

        // Ligne horizontale supérieure
        if (!headerCellInfo.isAbsorbe())
        {
            columnHeaderBand.addElement(JasperdesignUtil.ajouterLigneHorizontale(
                headerCellInfo.getXLeftCornerCalcule(), headerCellInfo.getYLeftCornerCalcule(), headerCellInfo
                    .getWidthCalcule(), 0, PositionTypeEnum.FIX_RELATIVE_TO_TOP, documentInfo.getLineWidth()));
        }

        // Ligne horizontale inférieure
        // Si l'element n'est pas absorbant
        if (!headerCellInfo.isMergeWithChild())
        {
            columnHeaderBand.addElement(JasperdesignUtil.ajouterLigneHorizontale(
                headerCellInfo.getXLeftCornerCalcule(), headerCellInfo.getYLeftCornerCalcule()
                    + headerCellInfo.getHeight(), headerCellInfo.getWidthCalcule(), 0,
                PositionTypeEnum.FIX_RELATIVE_TO_TOP, documentInfo.getLineWidth()));
        }

        // Ligne verticale gauche
        columnHeaderBand.addElement(JasperdesignUtil.ajouterLigneVerticale(headerCellInfo.getXLeftCornerCalcule(),
            headerCellInfo.getYLeftCornerCalcule(), 0, headerCellInfo.getHeight(),
            PositionTypeEnum.FIX_RELATIVE_TO_TOP, StretchTypeEnum.NO_STRETCH, documentInfo.getLineWidth()));

        // Ligne verticale droite
        columnHeaderBand.addElement(JasperdesignUtil.ajouterLigneVerticale(headerCellInfo.getXLeftCornerCalcule()
            + headerCellInfo.getWidthCalcule(), headerCellInfo.getYLeftCornerCalcule(), 0, headerCellInfo.getHeight(),
            PositionTypeEnum.FIX_RELATIVE_TO_TOP, StretchTypeEnum.NO_STRETCH, documentInfo.getLineWidth()));

    }

    /**
     * Insertion des entêtes de groupe et de titre.
     * 
     * @param model model
     * @param documentInfo documentInfo
     * @param columnHeaderBand columnHeaderBand
     * @param jrStyle jrStyle
     */
    public static void insertColumnHeaderGroupAndTitle(TableModel model, DocumentInfo documentInfo,
        JRDesignBand columnHeaderBand, JRStyle jrStyle)
    {
        for (GroupHeaderInfo groupHeaderInfo : documentInfo.getColumnHeader().getListeDesGroupesDeColonne())
        {
            JRDesignTextField textField =
                ColumnHeaderUtil.creerCelluleEnteteTableau(jrStyle, groupHeaderInfo, documentInfo);

            JRDesignExpression expression = new JRDesignExpression();
            expression.setValueClass(java.lang.String.class);

            expression.setText("\"" + groupHeaderInfo.getValue() + "\"");
            textField.setExpression(expression);

            columnHeaderBand.addElement(textField);

            // Trace les bordures de la cellule d'un groupe
            ColumnHeaderUtil.traceCellBorder(documentInfo, columnHeaderBand, groupHeaderInfo);

            // Insère les titres attachés au groupe en cours
            insertHeaderTitlecell(documentInfo, columnHeaderBand, groupHeaderInfo.getTitlesHeadersInfos(), jrStyle);
        }
    }

    /**
     * Insertion des entêtes de groupe, sous-groupe et de titre.
     * 
     * @param model model
     * @param documentInfo documentInfo
     * @param columnHeaderBand columnHeaderBand
     * @param jrStyle jrStyle
     */
    public static void insertColumnHeaderGroupSubGroupAndTitle(TableModel model, DocumentInfo documentInfo,
        JRDesignBand columnHeaderBand, JRStyle jrStyle)
    {
        for (GroupHeaderInfo groupHeaderInfo : documentInfo.getColumnHeader().getListeDesGroupesDeColonne())
        {
            JRDesignTextField textField =
                ColumnHeaderUtil.creerCelluleEnteteTableau(jrStyle, groupHeaderInfo, documentInfo);

            JRDesignExpression expression = new JRDesignExpression();
            expression.setValueClass(java.lang.String.class);

            expression.setText("\"" + groupHeaderInfo.getValue() + "\"");
            textField.setExpression(expression);

            columnHeaderBand.addElement(textField);

            // Trace les bordures de la cellule d'un groupe
            ColumnHeaderUtil.traceCellBorder(documentInfo, columnHeaderBand, groupHeaderInfo);

            // Insère les sous-groupes attachés au groupe en cours
            insertHeaderSubgroupcell(documentInfo, columnHeaderBand, groupHeaderInfo.getSubgroupsHeadersInfos(),
                jrStyle);
        }
    }

    /**
     * Insertion d'un titre de colonne.
     * 
     * @param model model
     * @param documentInfo documentInfo
     * @param columnHeaderBand columnHeaderBand
     * @param jrStyle jrStyle
     */
    public static void insertColumnHeaderTitle(TableModel model, DocumentInfo documentInfo,
        JRDesignBand columnHeaderBand, JRStyle jrStyle)
    {
        List<TitleHeaderInfo> titresDeColonnes = documentInfo.getColumnHeader().getListeDesTitresDeColonne();

        insertHeaderTitlecell(documentInfo, columnHeaderBand, titresDeColonnes, jrStyle);
    }

    /**
     * Création des cellules de la ligne de titre de l'entête d'un tableau.
     * 
     * @param documentInfo documentInfo
     * @param columnHeaderBand columnHeaderBand
     * @param titresDeColonnes titresDeColonnes
     * @param jrStyle jrStyle
     */
    private static void insertHeaderTitlecell(DocumentInfo documentInfo, JRDesignBand columnHeaderBand,
        List<TitleHeaderInfo> titresDeColonnes, JRStyle jrStyle)
    {
        for (TitleHeaderInfo titleHeaderInfo : titresDeColonnes)
        {
            JRDesignTextField textField =
                ColumnHeaderUtil.creerCelluleEnteteTableau(jrStyle, titleHeaderInfo, documentInfo);

            JRDesignExpression expression = new JRDesignExpression();
            expression.setValueClass(java.lang.String.class);

            expression.setText("\"" + titleHeaderInfo.getValue() + "\"");
            textField.setExpression(expression);

            columnHeaderBand.addElement(textField);

            // Trace les bordures de la cellule d'un titre
            ColumnHeaderUtil.traceCellBorder(documentInfo, columnHeaderBand, titleHeaderInfo);
        }
    }

    /**
     * Création de la ligne sous groupe dans l'entête du tableau.
     * 
     * @param documentInfo documentInfo
     * @param columnHeaderBand columnHeaderBand
     * @param sousGroupes sousGroupes
     * @param jrStyle jrStyle
     */
    private static void insertHeaderSubgroupcell(DocumentInfo documentInfo, JRDesignBand columnHeaderBand,
        List<SubgroupHeaderInfo> sousGroupes, JRStyle jrStyle)
    {
        for (SubgroupHeaderInfo subgroupHeaderInfo : sousGroupes)
        {
            JRDesignTextField textField =
                ColumnHeaderUtil.creerCelluleEnteteTableau(jrStyle, subgroupHeaderInfo, documentInfo);

            JRDesignExpression expression = new JRDesignExpression();
            expression.setValueClass(java.lang.String.class);

            expression.setText("\"" + subgroupHeaderInfo.getValue() + "\"");
            textField.setExpression(expression);

            columnHeaderBand.addElement(textField);

            // Trace les bordures de la cellule d'un titre
            ColumnHeaderUtil.traceCellBorder(documentInfo, columnHeaderBand, subgroupHeaderInfo);

            // Insère les titres contenus dans le sous-groupe
            insertHeaderTitlecell(documentInfo, columnHeaderBand, subgroupHeaderInfo.getTitlesHeadersInfos(), jrStyle);
        }
    }

    /**
     * Création du champ d'un titre de colonne.
     * 
     * @param styleCellEntete styleCellEntete
     * @param headerCellInfo headerCellInfo
     * @param documentInfo documentInfo
     * @return jR design text field
     */
    public static JRDesignTextField creerCelluleEnteteTableau(JRStyle styleCellEntete, HeaderCellInfoI headerCellInfo,
        DocumentInfo documentInfo)
    {

        JRDesignTextField textField = new JRDesignTextField();
        textField.setBlankWhenNull(true);
        textField.setX(headerCellInfo.getXLeftCornerCalcule());
        textField.setY(headerCellInfo.getYLeftCornerCalcule());
        textField.setWidth(headerCellInfo.getWidthCalcule());
        textField.setHeight(headerCellInfo.getHeight());
        textField.setStyle(styleCellEntete);
        textField.setStretchWithOverflow(false);

        if (documentInfo.getColumnHeader().isOpaque())
        {
            Color color =
                new Color(documentInfo.getColumnHeader().getColorR(), documentInfo.getColumnHeader().getColorG(),
                    documentInfo.getColumnHeader().getColorB());
            textField.setBackcolor(color);
            textField.setMode(ModeEnum.OPAQUE);
        }

        return textField;
    }

    /**
     * Contruit la définition du niveau sous groupe de l'entête de colonnes d'un tableau.
     * 
     * @param model model
     * @param documentInfo documentInfo
     * @param columnHeaderBand columnHeaderBand
     */
    public static void construitColumnHeaderSousGroupesInfo(TableModel model, DocumentInfo documentInfo,
        JRDesignBand columnHeaderBand)
    {
        ColumnHeaderInfo columnHeaderInfo = documentInfo.getColumnHeader();

        List<Column> columns = model.getColumnHandler().getColumns();

        SubgroupHeaderInfo previousSubgroupHeaderInfo = null;
        SubgroupHeaderInfo currentSubgroupHeaderInfo = new SubgroupHeaderInfo();
        String previousGroupAttribute = null;
        String currentGroupAttribute = null;

        int index = 0;

        for (Column column : columns)
        {
            TitleHeaderInfo currentTitleHeaderInfo =
                documentInfo.getColumnHeader().getListeDesTitresDeColonne().get(index);

            currentGroupAttribute = getGroup(column);

            // si aucun attribut sous-groupe, le titre devient sous-le groupe et les
            // deux cellules sont fusionnées
            if (StringUtils.isBlank(getSubgroup(column)))
            {
                currentSubgroupHeaderInfo =
                    columnHeaderInfo.ajouterUnNouveauSousGroupeAbsorbantLeTitre(currentTitleHeaderInfo.getValue(),
                        currentTitleHeaderInfo);
            }
            // Si le groupe est différent du groupe précédent, c'est un nouveau sous-groupe
            else if (currentGroupAttribute == null || (!currentGroupAttribute.equalsIgnoreCase(previousGroupAttribute)))
            {
                currentSubgroupHeaderInfo =
                    columnHeaderInfo.ajouterUnNouveauSousGroupeAvecTitre(getSubgroup(column), currentTitleHeaderInfo);
            }
            // si le sous-groupe précédent n'est pas null et sa valeur égale au sous-groupe courant
            // on considère que c'est le même sous-groupe
            else if (previousSubgroupHeaderInfo != null
                && getSubgroup(column).equalsIgnoreCase(previousSubgroupHeaderInfo.getValue()))
            {
                currentSubgroupHeaderInfo =
                    previousSubgroupHeaderInfo.ajouterUnTitreAuSousGroupe(currentTitleHeaderInfo);

            }
            // si le sous-groupe précédent n'est pas null et sa valeur différente de celle du sous-groupe courant
            // on considère que c'est un nouveau sous-groupe
            else if (previousSubgroupHeaderInfo != null
                && (!getSubgroup(column).equalsIgnoreCase(previousSubgroupHeaderInfo.getValue())))
            {
                currentSubgroupHeaderInfo =
                    columnHeaderInfo.ajouterUnNouveauSousGroupeAvecTitre(getSubgroup(column), currentTitleHeaderInfo);
            }
            // Dans les autres cas, c'est un nouveau sous-groupe
            else
            {
                currentSubgroupHeaderInfo =
                    columnHeaderInfo.ajouterUnNouveauSousGroupeAvecTitre(getSubgroup(column), currentTitleHeaderInfo);
            }

            // le sous-groupe courant devient le sous-groupe précédent
            previousSubgroupHeaderInfo = currentSubgroupHeaderInfo;

            previousGroupAttribute = currentGroupAttribute;
            index++;
        }
    }

    /**
     * Contruit la définition du niveau groupe de l'entête de colonnes d'un tableau.
     * 
     * @param model model
     * @param documentInfo documentInfo
     * @param columnHeaderBand columnHeaderBand
     */
    private static void construitColumnHeaderGroupesInfo(TableModel model, DocumentInfo documentInfo,
        JRDesignBand columnHeaderBand)
    {

        ColumnHeaderInfo columnHeaderInfo = documentInfo.getColumnHeader();

        List<Column> columns = model.getColumnHandler().getColumns();

        GroupHeaderInfo previousGroupHeaderInfo = null;
        GroupHeaderInfo currentGroupHeaderInfo = new GroupHeaderInfo();
        SubgroupHeaderInfo previousSubgroupHeaderInfo = null;

        int indexS = 0;

        for (Column column : columns)
        {
            TitleHeaderInfo currentTitleHeaderInfo =
                documentInfo.getColumnHeader().getListeDesTitresDeColonne().get(indexS);

            SubgroupHeaderInfo currentSubGroupHeaderInfo =
                (SubgroupHeaderInfo) currentTitleHeaderInfo.getParentHeader();

            // si aucun attribut groupe, le sous-groupe devient le groupe et les
            // deux cellules sont fusionnées
            if (StringUtils.isBlank(getGroup(column)))
            {
                currentGroupHeaderInfo =
                    columnHeaderInfo.ajouterUnNouveauGroupeAbsorbantLeSousGroupe(currentSubGroupHeaderInfo.getValue(),
                        currentSubGroupHeaderInfo);
            }

            // si le groupe précédent n'est pas null et sa valeur égale au groupe courant
            // on considère que c'est le même groupe
            else if (previousGroupHeaderInfo != null
                && getGroup(column).equalsIgnoreCase(previousGroupHeaderInfo.getValue()))
            {
                // Si le sous groupe précédent est égal au sous groupe courant,
                // on ajoute un nouveau groupe et on lui ajoute le sous-groupe courant
                if (previousSubgroupHeaderInfo != null && currentSubGroupHeaderInfo != null
                    && (!previousSubgroupHeaderInfo.getValue().equalsIgnoreCase(currentSubGroupHeaderInfo.getValue())))
                {
                    currentGroupHeaderInfo =
                        previousGroupHeaderInfo.ajouterUnSousGroupeAuGroupe(currentSubGroupHeaderInfo);
                }
                // sinon on ne fait rien car le sous groupe a déjà été ajouté

            }
            // si le groupe précédent n'est pas null et sa valeur différente de celle du groupe courant
            // on considère que c'est un nouveau groupe
            else if (previousGroupHeaderInfo != null
                && (!getGroup(column).equalsIgnoreCase(previousGroupHeaderInfo.getValue())))
            {
                currentGroupHeaderInfo =
                    columnHeaderInfo.ajouterUnNouveauGroupeAvecSousGroupe(getGroup(column), currentSubGroupHeaderInfo);
            }
            // Dans les autres cas, c'est un nouveau groupe
            else
            {
                currentGroupHeaderInfo =
                    columnHeaderInfo.ajouterUnNouveauGroupeAvecSousGroupe(getGroup(column), currentSubGroupHeaderInfo);
            }

            // le groupe courant devient le groupe précédent
            previousGroupHeaderInfo = currentGroupHeaderInfo;
            previousSubgroupHeaderInfo = currentSubGroupHeaderInfo;
            indexS++;
        }
    }

    /**
     * Construit la description d'un entête de colonne à trois niveaux.
     * 
     * @param model model
     * @param documentInfo documentInfo
     * @param columnHeaderBand columnHeaderBand
     */
    public static void construitColumnHeaderInfoThreeLevel(TableModel model, DocumentInfo documentInfo,
        JRDesignBand columnHeaderBand)
    {
        // Lecture des titres
        ColumnHeaderUtil.construitColumnHeaderInfoOneLevel(model, documentInfo, columnHeaderBand);

        // Lecture des sous groupes
        ColumnHeaderUtil.construitColumnHeaderSousGroupesInfo(model, documentInfo, columnHeaderBand);

        // Lecture des groupes
        ColumnHeaderUtil.construitColumnHeaderGroupesInfo(model, documentInfo, columnHeaderBand);

        // Trace la définition de l'entête
        /*
         * List<GroupHeaderInfo> groupes = documentInfo.getColumnHeader().getListeDesGroupesDeColonne(); for
         * (GroupHeaderInfo groupe : groupes) { StringBuilder   msg = new StringBuilder();
         * msg.append("Infos sur le niveau groupe\n"); msg.append(groupe.getValue()); msg.append("; x = ");
         * msg.append(groupe.getXLeftCornerCalcule()); msg.append(" ; y = ");
         * msg.append(groupe.getYLeftCornerCalcule()); msg.append(" ; width "); msg.append(groupe.getWidthCalcule());
         * msg.append(" ;height "); msg.append(groupe.getHeight()); log.debug(msg.toString()); List<SubgroupHeaderInfo>
         * subGroup = groupe.getSubgroupsHeadersInfos(); for (SubgroupHeaderInfo subgroup : subGroup) { StringBuffer
         * msgSubgroup = new StringBuilder(); msgSubgroup.append("Infos sur le niveau sous-groupe\n");
         * msgSubgroup.append(subgroup.getValue()); msgSubgroup.append("; x = ");
         * msgSubgroup.append(subgroup.getXLeftCornerCalcule()); msgSubgroup.append(" ; y = ");
         * msgSubgroup.append(subgroup.getYLeftCornerCalcule()); msgSubgroup.append(" ; width ");
         * msgSubgroup.append(subgroup.getWidthCalcule()); msgSubgroup.append(" ;height ");
         * msgSubgroup.append(subgroup.getHeight()); log.debug(msgSubgroup.toString()); List<TitleHeaderInfo> titles =
         * subgroup.getTitlesHeadersInfos(); for (TitleHeaderInfo title : titles) { StringBuilder   msgTitle = new
         * StringBuilder(); msgTitle.append("Infos sur le niveau titre\n"); msgTitle.append(title.getValue());
         * msgTitle.append("; x = "); msgTitle.append(title.getXLeftCornerCalcule()); msgTitle.append(" ; y = ");
         * msgTitle.append(title.getYLeftCornerCalcule()); msgTitle.append(" ; width ");
         * msgTitle.append(title.getWidthCalcule()); msgTitle.append(" ;height "); msgTitle.append(title.getHeight()); }
         * } }
         */

    }

    /**
     * Construit la description d'un entête de colonne à deux niveaux.
     * 
     * @param model model
     * @param documentInfo documentInfo
     * @param columnHeaderBand columnHeaderBand
     */
    public static void construitColumnHeaderInfoTwoLevel(TableModel model, DocumentInfo documentInfo,
        JRDesignBand columnHeaderBand)
    {
        // Lecture des titres
        ColumnHeaderUtil.construitColumnHeaderInfoOneLevel(model, documentInfo, columnHeaderBand);

        ColumnHeaderInfo columnHeaderInfo = documentInfo.getColumnHeader();

        List<Column> columns = model.getColumnHandler().getColumns();

        GroupHeaderInfo previousGroupHeaderInfo = null;
        GroupHeaderInfo currentGroupHeaderInfo = null;

        int index = 0;

        for (Column column : columns)
        {
            TitleHeaderInfo currentTitleHeaderInfo =
                documentInfo.getColumnHeader().getListeDesTitresDeColonne().get(index);

            // si aucun attribut groupe, le titre devient le groupe et les
            // deux cellules sont fusionnées
            if (StringUtils.isBlank(getGroup(column)))
            {
                currentGroupHeaderInfo =
                    columnHeaderInfo.ajouterUnNouveauGroupeAbsorbantUnTitre(currentTitleHeaderInfo.getValue(),
                        currentTitleHeaderInfo);
            }

            // si le groupe précédent n'est pas null et sa valeur égale au groupe courant
            // on considère que c'est le même groupe
            else if (previousGroupHeaderInfo != null
                && getGroup(column).equalsIgnoreCase(previousGroupHeaderInfo.getValue()))
            {
                currentGroupHeaderInfo = previousGroupHeaderInfo.ajouterUnTitreAuGroupe(currentTitleHeaderInfo);

            }
            // si le groupe précédent n'est pas null et sa valeur différente de celle du groupe courant
            // on considère que c'est un nouveau groupe
            else if (previousGroupHeaderInfo != null
                && (!getGroup(column).equalsIgnoreCase(previousGroupHeaderInfo.getValue())))
            {
                currentGroupHeaderInfo =
                    columnHeaderInfo.ajouterUnNouveauGroupeAvecTitre(getGroup(column), currentTitleHeaderInfo);
            }
            // Dans les autres cas, c'est un nouveau groupe
            else
            {
                currentGroupHeaderInfo =
                    columnHeaderInfo.ajouterUnNouveauGroupeAvecTitre(getGroup(column), currentTitleHeaderInfo);
            }

            // le groupe courant devient le groupe précédent
            previousGroupHeaderInfo = currentGroupHeaderInfo;

            index++;
        }
    }

    /**
     * Ajoute un espace avant les titres de colonnes.
     * 
     * @param model model
     * @param documentInfo documentInfo
     * @param columnHeaderBand columnHeaderBand
     */
    public static void ajoutEspaceAvantTitre(TableModel model, DocumentInfo documentInfo, JRDesignBand columnHeaderBand)
    {
        List<TitleHeaderInfo> titres = documentInfo.getColumnHeader().getListeDesTitresDeColonne();

        for (TitleHeaderInfo titleHeaderInfo : titres)
        {
            titleHeaderInfo.setSpaceBefore(documentInfo.getColumnHeader().getSpaceBefore());
        }
    }

    /**
     * Construit la liste des objet TitleHeaderInfo qui décrivent le niveau "titre" des entêtes de colonne.
     * 
     * @param model model
     * @param documentInfo documentInfo
     * @param columnHeaderBand columnHeaderBand
     */
    public static void construitColumnHeaderInfoOneLevel(TableModel model, DocumentInfo documentInfo,
        JRDesignBand columnHeaderBand)
    {
        List<Column> columns = model.getColumnHandler().getColumns();

        int index = 1;
        int abs = 0;

        for (Column col : columns)
        {
            int tailleCellule;

            // lecture de la taille de la colonne associée au titre
            // la taille de la colonne a été calculée au préalable

            if (index == documentInfo.getDetail().getColonnes().size())
            {
                // La dernière colonne est calculée de façon à compléter
                // la largeur de la page
                tailleCellule = documentInfo.calColumnWidth() - abs;
            }
            else
            {
                ColonneInfo colonneInfo = documentInfo.getDetail().getColonnes().get(index - 1);
                tailleCellule = colonneInfo.getTaille();
            }

            if (col.getTitlePdf() != null)
            { // rajout pour avoir la possibilité d'avoir un titre de ciolonne en pdf !=
                documentInfo.getColumnHeader().ajouterUnNouveauTitre(col.getTitlePdf(), tailleCellule, abs);
            }
            else
            {
                documentInfo.getColumnHeader().ajouterUnNouveauTitre(col.getTitle(), tailleCellule, abs);
            }

            abs = abs + tailleCellule;
            index++;

        }

    }
}
