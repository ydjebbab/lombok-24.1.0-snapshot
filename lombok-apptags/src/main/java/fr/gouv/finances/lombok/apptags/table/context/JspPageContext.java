/*
/*Copyright (c) 2014 DGFiP - Tous droits réservés/* * Copyright 2004 original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package fr.gouv.finances.lombok.apptags.table.context;

import java.io.Writer;
import java.util.Locale;
import java.util.Map;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.PageContext;

/**
 * Class JspPageContext
 */
public final class JspPageContext implements Context
{

    /** page context. */
    private PageContext pageContext;

    /**
     * Instanciation de jsp page context.
     * 
     * @param pageContext
     */
    public JspPageContext(PageContext pageContext)
    {
        this.pageContext = pageContext;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.context.Context#getApplicationInitParameter(java.lang.String)
     */
    @Override
    public Object getApplicationInitParameter(String name)
    {
        return pageContext.getServletContext().getInitParameter(name);
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.context.Context#getApplicationAttribute(java.lang.String)
     */
    @Override
    public Object getApplicationAttribute(String name)
    {
        return pageContext.getServletContext().getAttribute(name);
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.context.Context#setApplicationAttribute(java.lang.String,
     *      java.lang.Object)
     */
    @Override
    public void setApplicationAttribute(String name, Object value)
    {
        pageContext.getServletContext().setAttribute(name, value);
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.context.Context#removeApplicationAttribute(java.lang.String)
     */
    @Override
    public void removeApplicationAttribute(String name)
    {
        pageContext.getServletContext().removeAttribute(name);
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.context.Context#getPageAttribute(java.lang.String)
     */
    @Override
    public Object getPageAttribute(String name)
    {
        return pageContext.getAttribute(name);
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.context.Context#setPageAttribute(java.lang.String, java.lang.Object)
     */
    @Override
    public void setPageAttribute(String name, Object value)
    {
        pageContext.setAttribute(name, value);
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.context.Context#removePageAttribute(java.lang.String)
     */
    @Override
    public void removePageAttribute(String name)
    {
        pageContext.removeAttribute(name);
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.context.Context#getParameter(java.lang.String)
     */
    @Override
    public String getParameter(String name)
    {
        return pageContext.getRequest().getParameter(name);
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.context.Context#getParameterMap()
     */
    @SuppressWarnings("unchecked")
    @Override
    public Map getParameterMap()
    {
        return pageContext.getRequest().getParameterMap();
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.context.Context#getRequestAttribute(java.lang.String)
     */
    @Override
    public Object getRequestAttribute(String name)
    {
        return pageContext.getRequest().getAttribute(name);
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.context.Context#setRequestAttribute(java.lang.String,
     *      java.lang.Object)
     */
    @Override
    public void setRequestAttribute(String name, Object value)
    {
        pageContext.getRequest().setAttribute(name, value);
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.context.Context#removeRequestAttribute(java.lang.String)
     */
    @Override
    public void removeRequestAttribute(String name)
    {
        pageContext.getRequest().removeAttribute(name);
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.context.Context#getSessionAttribute(java.lang.String)
     */
    @Override
    public Object getSessionAttribute(String name)
    {
        return pageContext.getSession().getAttribute(name);
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.context.Context#setSessionAttribute(java.lang.String,
     *      java.lang.Object)
     */
    @Override
    public void setSessionAttribute(String name, Object value)
    {
        pageContext.getSession().setAttribute(name, value);
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.context.Context#removeSessionAttribute(java.lang.String)
     */
    @Override
    public void removeSessionAttribute(String name)
    {
        pageContext.getSession().removeAttribute(name);
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.context.Context#getWriter()
     */
    @Override
    public Writer getWriter()
    {
        return pageContext.getOut();
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.context.Context#getLocale()
     */
    @Override
    public Locale getLocale()
    {
        return pageContext.getRequest().getLocale();
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.context.Context#getContextPath()
     */
    @Override
    public String getContextPath()
    {
        ServletRequest request = pageContext.getRequest();
        if (request instanceof HttpServletRequest)
        {
            return ((HttpServletRequest) request).getContextPath();
        }

        throw new UnsupportedOperationException("There is no context path associated with the request.");
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.context.Context#getContextObject()
     */
    @Override
    public Object getContextObject()
    {
        return pageContext;
    }
}
