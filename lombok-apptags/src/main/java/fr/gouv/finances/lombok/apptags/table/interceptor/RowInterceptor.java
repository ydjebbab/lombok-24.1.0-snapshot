/*
/*Copyright (c) 2014 DGFiP - Tous droits réservés/* * Copyright 2004 original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package fr.gouv.finances.lombok.apptags.table.interceptor;

import fr.gouv.finances.lombok.apptags.table.bean.Row;
import fr.gouv.finances.lombok.apptags.table.core.TableModel;

/**
 * Interface RowInterceptor
 */
public interface RowInterceptor
{

    /**
     * methode Adds the row attributes :
     * 
     * @param tableModel
     * @param row
     */
    public void addRowAttributes(TableModel tableModel, Row row);

    /**
     * methode Modify row attributes :
     * 
     * @param model
     * @param row
     */
    public void modifyRowAttributes(TableModel model, Row row);
}
