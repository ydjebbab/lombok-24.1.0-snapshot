/*
/*Copyright (c) 2014 DGFiP - Tous droits réservés/* * Copyright 2004 original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package fr.gouv.finances.lombok.apptags.table.callback;

import java.util.Locale;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.collections.Predicate;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import fr.gouv.finances.lombok.apptags.table.bean.Column;
import fr.gouv.finances.lombok.apptags.table.core.TableModel;
import fr.gouv.finances.lombok.apptags.util.ExtremeUtils;

/**
 * Class FilterPredicate
 */
public final class FilterPredicate implements Predicate
{

    /** logger. */
    private static Log logger = LogFactory.getLog(FilterPredicate.class);

    /** model. */
    private TableModel model;

    /**
     * Instanciation de filter predicate.
     * 
     * @param model
     */
    public FilterPredicate(TableModel model)
    {
        this.model = model;
    }

    /**
     * Utilise les paramètres du filtre pour filtrer les lignes de la table.
     * 
     * @param bean
     * @return true, si c'est vrai
     */
    @Override
    public boolean evaluate(Object bean)
    {
        boolean match = false;

        try
        {
            for (Column column : model.getColumnHandler().getColumns())
            {
                String alias = column.getAlias();
                String filterValue = model.getLimit().getFilterSet().getFilterValue(alias);

                if (StringUtils.isEmpty(filterValue))
                {
                    continue;
                }

                String property = column.getProperty();
                Object value = PropertyUtils.getProperty(bean, property);

                if (value == null)
                {
                    continue;
                }

                if (column.isDate())
                {
                    Locale locale = model.getLocale();
                    value = ExtremeUtils.formatDate(column.getParse(), column.getFormat(), value, locale);
                }
                else if (column.isCurrency())
                {
                    Locale locale = model.getLocale();
                    value = ExtremeUtils.formatNumber(column.getFormat(), value, locale);
                }

                if (!isSearchMatch(value.toString(), filterValue, column.isFilterStrict()))
                {
                    match = false; // as soon as fail just short circuit

                    break;
                }

                match = true;

            }

        }
        catch (Exception exception)
        {
            logger.error("Erreur lors de l'évaluation du filte ", exception);
        }

        return match;
    }

    /**
     * Verifie si search match.
     * 
     * @param value
     * @param search
     * @param filterStrict
     * @return true, si c'est search match
     */
    private boolean isSearchMatch(String value, String search, boolean filterStrict)
    {
        boolean result = false;

        value = value.toLowerCase(Locale.FRANCE).trim();
        search = search.toLowerCase(Locale.FRANCE).trim();

        if (filterStrict)
        {
            result = StringUtils.equalsIgnoreCase(value, search);
        }
        else if (search.startsWith("*") && value.endsWith(StringUtils.replace(search, "*", "")))
        {
            result = true;
        }
        else if (search.endsWith("*") && value.startsWith(StringUtils.replace(search, "*", "")))
        {
            result = true;
        }
        else if (StringUtils.contains(value, search))
        {
            result = true;
        }

        return result;
    }
}
