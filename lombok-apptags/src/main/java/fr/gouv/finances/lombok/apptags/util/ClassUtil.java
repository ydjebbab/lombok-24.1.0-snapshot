/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : ClassUtil.java
 *
 */
package fr.gouv.finances.lombok.apptags.util;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Class ClassUtil.
 * 
 * @author wpetit-cp
 * @version $Revision: 1.2 $ Date: 15 déc. 2009
 */
public class ClassUtil
{

    /**
     * Accesseur de l attribut bean.
     * 
     * @param owner --
     * @param propertyName --
     * @return bean
     * @throws IllegalAccessException the illegal access exception
     * @throws InvocationTargetException the invocation target exception
     */
    public static Object getBean(Object owner, String propertyName) throws IllegalAccessException,
        InvocationTargetException
    {
        if (owner == null)
        {
            throw new NullPointerException("L'objet dans lequel cherché la propriété est null");
        }
        Method property = null;
        property = getProperty(owner, propertyName);

        // si la recherche de la propriété échoue, on recommence en ajoutant "get"
        if (property == null)
        {
            property = getProperty(owner, toGetterName(propertyName, "get"));
        }
        // on recommence en ajoutant "is"
        if (property == null)
        {
            property = getProperty(owner, toGetterName(propertyName, "is"));
        }

        if (property == null)
        {
            throw new IllegalArgumentException("L'objet de la classe " + owner.getClass()
                + " ne possède pas la propriété " + propertyName);
        }

        return property.invoke(owner, (Object[]) null);
    }

    /**
     * Accesseur de l attribut property.
     * 
     * @param owner --
     * @param propertyName --
     * @return property
     */
    private static Method getProperty(Object owner, String propertyName)
    {
        try
        {
            return owner.getClass().getMethod(propertyName, (Class[]) null);
        }
        catch (NoSuchMethodException e)
        {
            // Exception ignorée, on essaiera en ajoutant "get" puis "is" devant la propriété
        }
        return null;
    }

    /**
     * methode To getter name : --.
     * 
     * @param propertyName --
     * @param prefix --
     * @return string
     */
    private static String toGetterName(String propertyName, String prefix)
    {
        return prefix + propertyName.substring(0, 1).toUpperCase() + propertyName.substring(1);
    }
}
