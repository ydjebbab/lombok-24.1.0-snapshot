/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.apptags.table.view.jrxmlinfo;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRStyle;
import net.sf.jasperreports.engine.design.JRDesignBand;
import net.sf.jasperreports.engine.design.JRDesignExpression;
import net.sf.jasperreports.engine.design.JRDesignFrame;
import net.sf.jasperreports.engine.design.JRDesignTextField;
import net.sf.jasperreports.engine.design.JRDesignVariable;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.type.CalculationEnum;
import net.sf.jasperreports.engine.type.HorizontalAlignEnum;
import net.sf.jasperreports.engine.type.ModeEnum;
import net.sf.jasperreports.engine.type.PositionTypeEnum;
import net.sf.jasperreports.engine.type.ResetTypeEnum;
import net.sf.jasperreports.engine.type.StretchTypeEnum;
import net.sf.jasperreports.engine.type.VerticalAlignEnum;

import org.apache.commons.lang.StringUtils;

import fr.gouv.finances.lombok.apptags.table.bean.Column;
import fr.gouv.finances.lombok.apptags.table.calc.CpDecompteCalc;
import fr.gouv.finances.lombok.apptags.table.calc.CpDecompteNonNullsCalc;
import fr.gouv.finances.lombok.apptags.table.calc.CpMaximumCalc;
import fr.gouv.finances.lombok.apptags.table.calc.CpMinimumCalc;
import fr.gouv.finances.lombok.apptags.table.calc.CpMoyenneCalc;
import fr.gouv.finances.lombok.apptags.table.calc.CpPageTotalCalc;
import fr.gouv.finances.lombok.apptags.table.calc.CpSubTotalCalc;
import fr.gouv.finances.lombok.apptags.table.calc.CpTotalCalc;
import fr.gouv.finances.lombok.apptags.table.cell.NombreCellZeroDecimale;
import fr.gouv.finances.lombok.apptags.table.core.PreferencesConstants;
import fr.gouv.finances.lombok.apptags.table.core.TableModel;
import fr.gouv.finances.lombok.apptags.table.view.jrxmlinfo.CellColumnFooterInfo.InitialisationCalcul;
import fr.gouv.finances.lombok.apptags.table.view.jrxmlinfo.CellColumnFooterInfo.TypeCalcul;
import fr.gouv.finances.lombok.apptags.util.EcTableException;

/**
 * Class ColumnFooterUtil
 */
public class ColumnFooterUtil
{

    /** Constant : STYLE_CELLULE_PIED_COLONNE_EDITION. */
    private static final String STYLE_CELLULE_PIED_COLONNE_EDITION = "styleCellulePiedColonneEdition";

    /**
     * Analyse les paramètres calc et calcTitle pour construire les pieds de colonne.
     * 
     * @param model model
     * @param documentInfo documentInfo
     * @param columnFooterBand columnFooterBand
     * @param calcFrameInfo calcFrameInfo
     * @param rows rows
     */
    /*
     * public static void calcFooterAnalyse(TableModel model, DocumentInfo documentInfo, JRDesignBand columnFooterBand)
     * { documentInfo.getColumnFooter().setRows(findColumnFooterLevel(model)); // Teste de la cohérence des largeurs de
     * colonnes List<Column> tableColumns = model.getColumnHandler().getColumns(); // Création du squelette du pied de
     * colonne yc affectation de tailles // aux cellules createLigneCellColumnFooterInfo(tableColumns, documentInfo); //
     * Calcule la taille de la section pied de colonne affecteTailleColumnFooter(model, documentInfo, columnFooterBand);
     * // Index de la première colonne qui contient une cellule calculée List<Column> columns =
     * model.getColumnHandler().getColumns(); int firstCalcColumnIndex = findIndexFirstCalcCell(columns);
     * List<CalcRowFooterInfo> footerRows = documentInfo.getColumnFooter() .getFooterRows(); int currentRow = 0; for
     * (CalcRowFooterInfo calcRowFooterInfo : footerRows) { List<CellColumnFooterInfo> cellules = calcRowFooterInfo
     * .getCellules(); int currCellIndex = 0; boolean titreAffecte = false; for (CellColumnFooterInfo
     * cellColumnFooterInfo : cellules) { Column column = (Column) model.getColumnHandler().getColumns()
     * .get(currCellIndex); cellColumnFooterInfo.setTitre(""); // Si la cellule en cours est "découpée" if
     * (cellColumnFooterInfo.isSplit()) { affecteTitre(model, column, cellColumnFooterInfo, currentRow); titreAffecte =
     * true; } // Positionnement du titre s'il n'est pas encore affecté if ((!titreAffecte) && currCellIndex ==
     * (firstCalcColumnIndex - 1)) { affecteTitre(model, column, cellColumnFooterInfo, currentRow); } // Champ calculé
     * pour la ligne en cours if (column.getCalc() != null && (column.getCalc().size() - 1) >= currentRow &&
     * column.getCalc().get(currentRow) != null && StringUtils.isNotBlank(column.getCalc().get( currentRow))) {
     * affecteTypeDeCalcul(model, column, cellColumnFooterInfo, currentRow); } currCellIndex++; } currentRow++; } }
     */

    /**
     * calcFooterAnalyse
     * 
     * @param model model
     * @param documentInfo documentInfo
     * @param columnFooterBand columnFooterBand
     * @param calcFrameInfo calcFrameInfo
     * @param rows rows
     */
    public static void calcFooterAnalyse(TableModel model, DocumentInfo documentInfo, JRDesignBand columnFooterBand,
        CalcFrameInfo calcFrameInfo, int rows)
    {
        calcFrameInfo.setRows(rows);

        // Teste de la cohérence des largeurs de colonnes
        List<Column> tableColumns = model.getColumnHandler().getColumns();

        // Création du squelette du pied de colonne yc affectation de tailles
        // aux cellules
        createLigneCellColumnFooterInfo(tableColumns, documentInfo, calcFrameInfo);

        // Calcule la taille de la section pied de colonne
        affecteTailleColumnFooter(model, documentInfo, columnFooterBand);

        // Index de la première colonne qui contient une cellule calculée
        List<Column> columns = model.getColumnHandler().getColumns();
        int firstCalcColumnIndex = findIndexFirstCalcCell(columns);

        List<CalcRowFooterInfo> footerRows = calcFrameInfo.getFooterRows();

        int currentRow = 0;
        for (Integer calcIndex : calcFrameInfo.getIndexsCals())
        {

            CalcRowFooterInfo calcRowFooterInfo = footerRows.get(currentRow);
            List<CellColumnFooterInfo> cellules = calcRowFooterInfo.getCellules();
            int currCellIndex = 0;
            boolean titreAffecte = false;
            for (CellColumnFooterInfo cellColumnFooterInfo : cellules)
            {
                Column column = model.getColumnHandler().getColumns().get(currCellIndex);

                cellColumnFooterInfo.setTitre("");

                // Si la cellule en cours est "découpée"
                if (cellColumnFooterInfo.isSplit())
                {
                    affecteTitre(model, column, cellColumnFooterInfo, calcIndex.intValue());
                    titreAffecte = true;
                }

                // Positionnement du titre s'il n'est pas encore affecté
                if ((!titreAffecte) && currCellIndex == (firstCalcColumnIndex - 1))
                {
                    affecteTitre(model, column, cellColumnFooterInfo, calcIndex.intValue());
                }

                // Champ calculé pour la ligne en cours
                if (column.getCalc() != null && (column.getCalc().size() - 1) >= currentRow
                    && column.getCalc().get(currentRow) != null
                    && StringUtils.isNotBlank(column.getCalc().get(currentRow)))
                {
                    affecteTypeDeCalcul(model, column, cellColumnFooterInfo, calcIndex.intValue());
                }

                currCellIndex++;
            }
            currentRow++;
        }
    }

    /**
     * methode Affecte titre :
     * 
     * @param model model
     * @param column column
     * @param cellColumnFooterInfo cellColumnFooterInfo
     * @param currentIndex currentIndex
     */
    private static void affecteTitre(TableModel model, Column column, CellColumnFooterInfo cellColumnFooterInfo,
        int currentIndex)
    {
        cellColumnFooterInfo.setTitre("");
        int indexFirstCalcCol = findIndexFirstCalcTitleCellByRow(model, currentIndex);
        if (indexFirstCalcCol >= 0)
        {
            String titreCalc =
                (model.getColumnHandler().getColumns().get(indexFirstCalcCol)).getCalcTitle()
                    .get(currentIndex);

            cellColumnFooterInfo.setTitre(titreCalc);
        }
    }

    /**
     * methode Affecte type de calcul :
     * 
     * @param model model
     * @param column column
     * @param cellColumnFooterInfo cellColumnFooterInfo
     * @param currentIndex currentIndex
     */
    private static void affecteTypeDeCalcul(TableModel model, Column column, CellColumnFooterInfo cellColumnFooterInfo,
        int currentIndex)
    {
        String calcName = column.getCalc().get(currentIndex);

        String calcClassName = model.getPreferences().getPreference(PreferencesConstants.COLUMN_CALC + calcName);

        if (StringUtils.isBlank(calcClassName))
        {
            calcClassName = calcName;
        }

        Class<? extends Object> calcClass = findCalcClass(calcClassName);
        try
        {
            if (calcClass != null && calcClass.newInstance() instanceof CpTotalCalc)
            {
                cellColumnFooterInfo.setCellTypeCalcul(TypeCalcul.somme);
                cellColumnFooterInfo.setCellInitialisation(InitialisationCalcul.rapport);
            }
            else if (calcClass != null && calcClass.newInstance() instanceof CpSubTotalCalc)
            {
                cellColumnFooterInfo.setCellTypeCalcul(TypeCalcul.somme);
                cellColumnFooterInfo.setCellInitialisation(InitialisationCalcul.rapport);
            }
            else if (calcClass != null && calcClass.newInstance() instanceof CpPageTotalCalc)
            {
                cellColumnFooterInfo.setCellTypeCalcul(TypeCalcul.somme);
                cellColumnFooterInfo.setCellInitialisation(InitialisationCalcul.page);
            }
            else if (calcClass != null && calcClass.newInstance() instanceof CpMoyenneCalc)
            {
                cellColumnFooterInfo.setCellTypeCalcul(TypeCalcul.moyenne);
                cellColumnFooterInfo.setCellInitialisation(InitialisationCalcul.rapport);
            }
            else if (calcClass != null && calcClass.newInstance() instanceof CpMinimumCalc)
            {
                cellColumnFooterInfo.setCellTypeCalcul(TypeCalcul.min);
                cellColumnFooterInfo.setCellInitialisation(InitialisationCalcul.rapport);
            }
            else if (calcClass != null && calcClass.newInstance() instanceof CpMaximumCalc)
            {
                cellColumnFooterInfo.setCellTypeCalcul(TypeCalcul.max);
                cellColumnFooterInfo.setCellInitialisation(InitialisationCalcul.rapport);
            }
            else if (calcClass != null && calcClass.newInstance() instanceof CpDecompteCalc)
            {
                cellColumnFooterInfo.setCellTypeCalcul(TypeCalcul.decompte);
                cellColumnFooterInfo.setCellInitialisation(InitialisationCalcul.rapport);
            }

            else if (calcClass != null && calcClass.newInstance() instanceof CpDecompteNonNullsCalc)
            {
                cellColumnFooterInfo.setCellTypeCalcul(TypeCalcul.decomptenonnulls);
                cellColumnFooterInfo.setCellInitialisation(InitialisationCalcul.rapport);
            }
            else
            {
                cellColumnFooterInfo.setCellTypeCalcul(TypeCalcul.aucun);
            }
        }
        catch (InstantiationException exception)
        {
            throw new EcTableException(exception);
        }
        catch (IllegalAccessException exception)
        {
            throw new EcTableException(exception);
        }

    }

    /**
     * Construit le squelette de la description du pied de colonne.
     * 
     * @param tableColumns tableColumns
     * @param documentInfo documentInfo
     * @param calcFrameInfo calcFrameInfo
     */
    private static void createLigneCellColumnFooterInfo(List<Column> tableColumns, DocumentInfo documentInfo,
        CalcFrameInfo calcFrameInfo)
    {
        // Création du nombre de lignes de pieds de colonne nécessaire
        int nbCalcRows = calcFrameInfo.getRows();

        // Index de la première colonne qui contient une cellule calculé
        int firstCalcColumnIndex = findIndexFirstCalcCell(tableColumns);

        CalcRowFooterInfo calcRowFooterInfo = null;
        while (nbCalcRows > 0)
        {
            CalcRowFooterInfo previousCalcRowFooterInfo = calcRowFooterInfo;
            calcRowFooterInfo = new CalcRowFooterInfo();
            calcRowFooterInfo.setPreviousCalcRowFooterInfo(previousCalcRowFooterInfo);

            calcRowFooterInfo.setHeight(documentInfo.getDetail().getHeight());

            calcFrameInfo.addCalcRow(calcRowFooterInfo);

            int colno = 0;
            CellColumnFooterInfo cellColumnFooterInfo = null;
            CellColumnFooterInfo previousCellColumnFooterInfo = null;

            for (int i = 0; i < tableColumns.size(); i++)
            {
                previousCellColumnFooterInfo = cellColumnFooterInfo;
                cellColumnFooterInfo = new CellColumnFooterInfo();

                // Si la première colonne contient une cellule calculée, elle
                // est marquée
                // comme "découpée"
                if (firstCalcColumnIndex == 0 && colno == 0)
                {
                    cellColumnFooterInfo.setSplit(true);
                }

                cellColumnFooterInfo.setPreviousCell(previousCellColumnFooterInfo);

                // La taille des cellules de pied de colonne est identique à
                // celle des colonnes

                cellColumnFooterInfo.setHeight(documentInfo.getDetail().getHeight());

                // La dernière colonne est ajustée en fonction de la taille du
                // document
                if (colno == (tableColumns.size() - 1))
                {
                    int tailleCol = documentInfo.calColumnWidth() - cellColumnFooterInfo.getXLeftCornerCalcule();
                    cellColumnFooterInfo.setWidth(tailleCol);
                }
                else
                {
                    cellColumnFooterInfo.setWidth(documentInfo.getDetail().getColonnes().get(colno).getTaille());
                }
                cellColumnFooterInfo.setPropriete(documentInfo.getDetail().getColonnes().get(colno).getPropriete());

                cellColumnFooterInfo.setAlias(documentInfo.getDetail().getColonnes().get(colno).getAlias());

                cellColumnFooterInfo.setCellType(documentInfo.getDetail().getColonnes().get(colno).getCellType());

                cellColumnFooterInfo.setClazz(documentInfo.getDetail().getColonnes().get(colno).getClazz());

                cellColumnFooterInfo.setPattern(documentInfo.getDetail().getColonnes().get(colno).getPattern());

                cellColumnFooterInfo.setAlignement(documentInfo.getDetail().getColonnes().get(colno).getAlignement());

                calcRowFooterInfo.addCellule(cellColumnFooterInfo);
                colno++;
            }

            nbCalcRows--;
        }

    }

    /**
     * Calcule la taille de la section pied de colonne.
     * 
     * @param model model
     * @param documentInfo documentInfo
     * @param columnFooterBand columnFooterBand
     */
    private static void affecteTailleColumnFooter(TableModel model, DocumentInfo documentInfo,
        JRDesignBand columnFooterBand)
    {
        // Taille pour première page
        int rowsFirstPageColFooter = findColumnFooterLevelForFirstPage(model, documentInfo);
        // + 1 pour les bordures
        int heightFirstPageColumnFooter = (documentInfo.getDetail().getHeight() * rowsFirstPageColFooter) + 1;

        // Taille pour les pages du milieu
        int rowsMiddlePageColFooter = findColumnFooterLevelForMiddlePage(model, documentInfo);
        // + 1 pour les bordures
        int heightMiddlePageColumnFooter = (documentInfo.getDetail().getHeight() * rowsMiddlePageColFooter) + 1;

        // Taille pour la dernière page
        int rowsLastPageColFooter = findColumnFooterLevelForLastPage(model, documentInfo);
        // + 1 pour les bordures
        int heightLastPageColumnFooter = (documentInfo.getDetail().getHeight() * rowsLastPageColFooter) + 1;

        // On affecte la plus grande des trois hauteurs à la section qui
        // contiendra le pied de colonne
        int height =
            (heightFirstPageColumnFooter > heightMiddlePageColumnFooter ? heightFirstPageColumnFooter
                : heightMiddlePageColumnFooter);
        height = (height > heightLastPageColumnFooter ? height : heightLastPageColumnFooter);

        documentInfo.getColumnFooter().setHeight(height);
        columnFooterBand.setHeight(documentInfo.getColumnFooter().getHeight());
    }

    /**
     * Trace les bordures de la cellule.
     * 
     * @param documentInfo documentInfo
     * @param jrDesignFrame jrDesignFrame
     * @param cellColumnFooterInfo cellColumnFooterInfo
     * @param currentRow currentRow
     */
    public static void traceCellBorder(DocumentInfo documentInfo, JRDesignFrame jrDesignFrame,
        CellColumnFooterInfo cellColumnFooterInfo, int currentRow)
    {

        if (currentRow > 0)
        {
            jrDesignFrame.addElement(JasperdesignUtil.ajouterLigneHorizontale(cellColumnFooterInfo
                .getXLeftCornerCalcule(), cellColumnFooterInfo.getYLeftCornerCalcule(), cellColumnFooterInfo
                .getWidthCalcule(), 0, PositionTypeEnum.FIX_RELATIVE_TO_TOP, documentInfo.getLineWidth()));
        }

        // Ligne horizontale inférieure

        jrDesignFrame.addElement(JasperdesignUtil.ajouterLigneHorizontale(cellColumnFooterInfo.getXLeftCornerCalcule(),
            cellColumnFooterInfo.getYLeftCornerCalcule() + cellColumnFooterInfo.getHeight(), cellColumnFooterInfo
                .getWidthCalcule(), 0, PositionTypeEnum.FIX_RELATIVE_TO_TOP, documentInfo.getLineWidth()));

        // Si l'element n'est pas absorbant
        if (!cellColumnFooterInfo.isMergeWithChild())
        {
            // Ligne verticale gauche
            jrDesignFrame.addElement(JasperdesignUtil.ajouterLigneVerticale(cellColumnFooterInfo
                .getXLeftCornerCalcule(), cellColumnFooterInfo.getYLeftCornerCalcule(), 0, cellColumnFooterInfo
                .getHeight(), PositionTypeEnum.FIX_RELATIVE_TO_TOP, StretchTypeEnum.NO_STRETCH,
                documentInfo.getLineWidth()));
        }
        // Ligne verticale droite
        // Ligne horizontale supérieure
        if (!cellColumnFooterInfo.isAbsorbe())
        {
            jrDesignFrame.addElement(JasperdesignUtil.ajouterLigneVerticale(cellColumnFooterInfo
                .getXLeftCornerCalcule()
                + cellColumnFooterInfo.getWidthCalcule(), cellColumnFooterInfo.getYLeftCornerCalcule(), 0,
                cellColumnFooterInfo.getHeight(), PositionTypeEnum.FIX_RELATIVE_TO_TOP,
                StretchTypeEnum.NO_STRETCH, documentInfo.getLineWidth()));
        }

    }

    /**
     * Retourne l'index de la première colonne contenant une cellule calculée.
     * 
     * @param columns columns
     * @return int
     */
    private static int findIndexFirstCalcCell(List<Column> columns)
    {
        int index = 0;
        for (Column column : columns)
        {
            if (column != null && column.isCalculated())
            {
                break;
            }
            index++;
        }
        return index;
    }

    /**
     * Retourne l'index de la première colonne contenant une titre de cellule calculée ou -1 si aucun titre n'est
     * disponible pour cette ligne de pied de colonne.
     * 
     * @param model model
     * @param row row
     * @return int
     */
    private static int findIndexFirstCalcTitleCellByRow(TableModel model, int row)
    {
        List<Column> columns = model.getColumnHandler().getColumns();
        int index = 0;
        boolean titleFound = false;
        for (Column column : columns)
        {
            if (column != null && column.getCalcTitle() != null && (column.getCalcTitle().size() - 1) >= row
                && column.getCalcTitle().get(row) != null && StringUtils.isNotBlank(column.getCalcTitle().get(row)))
            {
                titleFound = true;
                break;
            }
            index++;
        }

        if (!titleFound)
        {
            index = -1;
        }
        return index;
    }

    /**
     * Find calc class.
     * 
     * @param calcClassName calcClassName
     * @return the class<? extends object>
     */
    private static Class<? extends Object> findCalcClass(String calcClassName)
    {
        Class<? extends Object> result = null;
        try
        {
            result = Class.forName(calcClassName);
        }
        catch (ClassNotFoundException exe)
        {
            throw new EcTableException("Erreur", exe);
        }

        return result;
    }

    /**
     * Détermine le nombre de ligne du pied de colonne à afficher dans au pied du tableau dans la première page de
     * l'édition.
     * 
     * @param model model
     * @param documentInfo documentInfo
     * @return int
     */
    public static int findColumnFooterLevelForFirstPage(TableModel model, DocumentInfo documentInfo)
    {
        return 0;

        // modification amlp : on n'affiche plus les totaux que sur la derniére page du tableau

        /*
         * Column firstCalcColumnExport = model.getColumnHandler() .getFirstCalcExportColumn(); // Si le tableau
         * contient un pied de colonne if (firstCalcColumnExport != null) { List<String> calcExports =
         * firstCalcColumnExport.getCalcExport(); documentInfo.getColumnFooter().getFirstPageFrameInfo()
         * .setIndexsCals(new ArrayList<Integer>()); int currentRow = 0; for (String calcExport : calcExports) { if
         * ("all".equalsIgnoreCase(calcExport) || "first".equalsIgnoreCase(calcExport) // cas où la première page n'est
         * pas la dernière || ("allbutlast".equalsIgnoreCase(calcExport) && !model.isLastPage()) // cas où la première
         * page est également la dernière || ("last".equalsIgnoreCase(calcExport) && model.isLastPage()) // cas où la
         * première page est également la dernière || ("firstandlast".equalsIgnoreCase(calcExport) &&
         * model.isLastPage()) // cas où la première page n'est pas la dernière ||
         * ("notfirstandlast".equalsIgnoreCase(calcExport) && !model.isLastPage())) { result++;
         * documentInfo.getColumnFooter().getFirstPageFrameInfo() .getIndexsCals().add(Integer.valueOf(currentRow)); }
         * currentRow++; } }
         */
    }

    /**
     * Détermine le nombre de ligne du pied de colonne à afficher dans au pied du tableau dans la dernière page de
     * l'édition.
     * 
     * @param model model
     * @param documentInfo documentInfo
     * @return int
     */
    public static int findColumnFooterLevelForLastPage(TableModel model, DocumentInfo documentInfo)
    {
        int result = 0;

        Column firstCalcColumnExport = model.getColumnHandler().getFirstCalcExportColumn();

        if (firstCalcColumnExport != null)
        {
            documentInfo.getColumnFooter().getLastPageFrameInfo().setIndexsCals(new ArrayList<Integer>());
            List<String> calcExports = firstCalcColumnExport.getCalcExport();
            int currentRow = 0;
            for (String calcExport : calcExports)
            {
                if ("last".equalsIgnoreCase(calcExport))
                {
                    result++;
                    documentInfo.getColumnFooter().getLastPageFrameInfo().getIndexsCals().add(
                        Integer.valueOf(currentRow));
                }
                currentRow++;
            }
        }
        return result;
    }

    /**
     * Détermine le nombre de ligne du pied de colonne à afficher dans au pied du tableau dans les pages autres que la
     * première et la dernière de l'édition.
     * 
     * @param model model
     * @param documentInfo documentInfo
     * @return int
     */
    public static int findColumnFooterLevelForMiddlePage(TableModel model, DocumentInfo documentInfo)
    {
        // modification amlp : on n'affiche plus les totaux que sur la derniére page du tableau

        /*
         * Column firstCalcColumnExport = model.getColumnHandler() .getFirstCalcExportColumn(); if
         * (firstCalcColumnExport != null) { documentInfo.getColumnFooter().getMiddlePageFrameInfo() .setIndexsCals(new
         * ArrayList<Integer>()); List<String> calcExports = firstCalcColumnExport.getCalcExport(); int currentRow = 0;
         * for (String calcExport : calcExports) { if ("all".equalsIgnoreCase(calcExport) ||
         * "allbutlast".equalsIgnoreCase(calcExport) || "allbutfirst".equalsIgnoreCase(calcExport) ||
         * "allbutfirstandlast".equalsIgnoreCase(calcExport) || "notfirstandlast".equalsIgnoreCase(calcExport)) {
         * result++; documentInfo.getColumnFooter().getLastPageFrameInfo()
         * .getIndexsCals().add(Integer.valueOf(currentRow)); } currentRow++; } }
         */
        return 0;
    }

    /**
     * Insertion d'une frame qui contient les pieds de colonne contenant les cellules calculées.
     * 
     * @param model model
     * @param documentInfo documentInfo
     * @param columnFooterBand columnFooterBand
     * @param jasperDesign jasperdesign
     * @return jR design frame
     */
    public static JRDesignFrame insertColumnFooterFrameFirstPage(TableModel model, DocumentInfo documentInfo,
        JRDesignBand columnFooterBand, JasperDesign jasperDesign)
    {
        JRDesignFrame firstPageColFooterFrame = new JRDesignFrame();
        firstPageColFooterFrame.setPositionType(PositionTypeEnum.FIX_RELATIVE_TO_TOP);

        documentInfo.getDetail();

        firstPageColFooterFrame.setHeight(documentInfo.getColumnFooter().getHeight());
        firstPageColFooterFrame.setWidth(documentInfo.calColumnWidth() + 1);
        firstPageColFooterFrame.setX(0);
        firstPageColFooterFrame.setY(0);

        int firstTablePageNumber = (documentInfo.isUseTitleOnNewPage() ? 2 : 1);

        JRDesignExpression expression = new JRDesignExpression();
        expression.setValueClass(Boolean.class);
        expression.setText("new Boolean((" + firstTablePageNumber + " == $V{PAGE_NUMBER}.intValue()  ))");

        firstPageColFooterFrame.setPrintWhenExpression(expression);
        columnFooterBand.addElement(firstPageColFooterFrame);

        return firstPageColFooterFrame;
    }

    /**
     * Insertion d'une frame qui contient les pieds de colonne contenant les cellules calculées.
     * 
     * @param model model
     * @param documentInfo documentInfo
     * @param columnFooterBand columnFooterBand
     * @param jasperDesign jasperdesign
     * @return jR design frame
     */
    public static JRDesignFrame insertColumnFooterFrameLastPage(TableModel model, DocumentInfo documentInfo,
        JRDesignBand columnFooterBand, JasperDesign jasperDesign)
    {
        JRDesignFrame firstPageColFooterFrame = new JRDesignFrame();
        firstPageColFooterFrame.setPositionType(PositionTypeEnum.FIX_RELATIVE_TO_TOP);

        documentInfo.getDetail();

        firstPageColFooterFrame.setHeight(documentInfo.getColumnFooter().getHeight());
        firstPageColFooterFrame.setWidth(documentInfo.calColumnWidth() + 1);
        firstPageColFooterFrame.setX(0);
        firstPageColFooterFrame.setY(0);
        // firstPageColFooterFrame.getLineBox().getPen().setLineStyle(JRPen.LINE_STYLE_SOLID);
        // firstPageColFooterFrame.getLineBox().getPen().setLineWidth(JRPen.LINE_WIDTH_1);

        JRDesignExpression expression = new JRDesignExpression();
        expression.setValueClass(Boolean.class);
        // modif amlp 04/11/11 pour fonctionnement avec tableau filtré
        expression.setText("new Boolean((" + model.getCollectionOfFilteredBeans().size()
            + " == $V{REPORT_COUNT}.intValue()  ))");

        firstPageColFooterFrame.setPrintWhenExpression(expression);
        columnFooterBand.addElement(firstPageColFooterFrame);

        return firstPageColFooterFrame;
    }

    /**
     * Insertion d'une frame qui contient les pieds de colonne contenant les cellules calculées.
     * 
     * @param model model
     * @param documentInfo documentInfo
     * @param columnFooterBand columnFooterBand
     * @param jasperDesign jasperdesign
     * @return jR design frame
     */
    public static JRDesignFrame insertColumnFooterFrameMiddlePage(TableModel model, DocumentInfo documentInfo,
        JRDesignBand columnFooterBand, JasperDesign jasperDesign)
    {
        JRDesignFrame firstPageColFooterFrame = new JRDesignFrame();
        firstPageColFooterFrame.setPositionType(PositionTypeEnum.FIX_RELATIVE_TO_TOP);

        documentInfo.getDetail();

        firstPageColFooterFrame.setHeight(documentInfo.getColumnFooter().getHeight());
        firstPageColFooterFrame.setWidth(documentInfo.calColumnWidth() + 1);
        firstPageColFooterFrame.setX(0);
        firstPageColFooterFrame.setY(0);

        int firstTablePageNumber = (documentInfo.isUseTitleOnNewPage() ? 2 : 1);

        JRDesignExpression expression = new JRDesignExpression();
        expression.setValueClass(Boolean.class);
        expression.setText("new Boolean((" + firstTablePageNumber + " != $V{PAGE_NUMBER}.intValue()  && "
            + model.getCollectionOfBeans().size() + " < $V{REPORT_COUNT}.intValue()))");

        firstPageColFooterFrame.setPrintWhenExpression(expression);
        columnFooterBand.addElement(firstPageColFooterFrame);

        return firstPageColFooterFrame;
    }

    /**
     * Insertion des lignes de pied de colonne.
     * 
     * @param model model
     * @param documentInfo documentInfo
     * @param jrDesignFrame jrDesignFrame
     * @param calcFrameInfo calcFrameInfo
     * @param jasperDesign jasperdesign
     */
    public static void insertColumnFooterRows(TableModel model, DocumentInfo documentInfo, JRDesignFrame jrDesignFrame,
        CalcFrameInfo calcFrameInfo, JasperDesign jasperDesign)
    {

        List<CalcRowFooterInfo> listCalcRows = calcFrameInfo.getFooterRows();

        int currentRow = 0;
        // On itère sur les lignes de pied de page
        for (CalcRowFooterInfo calcRowFooterInfo : listCalcRows)
        {
            // Création de l'expression qui détermine si la cellule calculée est
            // affichée
            JRDesignExpression expressionTest = new JRDesignExpression();
            expressionTest.setValueClass(Boolean.class);

            List<CellColumnFooterInfo> cellules = calcRowFooterInfo.getCellules();

            int currentCol = 0;
            boolean passeCalcTitle = false;

            // On itère sur les cellules de la ligne
            for (CellColumnFooterInfo cellColumnFooterInfo : cellules)
            {

                if (cellColumnFooterInfo.isSplit())
                {
                    List<JRDesignTextField> fields =
                        ColumnFooterUtil.creertwoSplitCellFooterCol(jasperDesign, cellColumnFooterInfo, documentInfo);

                    JRDesignTextField titleField = fields.get(0);
                    JRDesignTextField calcField = fields.get(1);

                    // Insertion du titre
                    JRDesignExpression titleExpression = createTitleCellExpression(cellColumnFooterInfo);
                    titleField.setExpression(titleExpression);

                    // Insertion du calcul
                    if (cellColumnFooterInfo.isCalculatedCell())
                    {
                        JRDesignExpression calcExpression =
                            createCalculatedCellExpression(model, documentInfo, jasperDesign, cellColumnFooterInfo,
                                currentRow);
                        calcField.setExpression(calcExpression);
                    }

                    // Correction du pattern de formatage dans le cas d'un
                    // calcul de type decompte
                    if (cellColumnFooterInfo.getCellTypeCalcul() != TypeCalcul.decompte
                        || cellColumnFooterInfo.getCellTypeCalcul() != TypeCalcul.decomptenonnulls)
                    {
                        calcField.setPattern(cellColumnFooterInfo.getPattern());
                    }
                    else
                    {
                        calcField.setPattern((new NombreCellZeroDecimale()).getFormatPattern());
                    }

                    jrDesignFrame.addElement(titleField);
                    jrDesignFrame.addElement(calcField);

                    // Trace la ligne de séparation entre le titre et la valeur
                    jrDesignFrame.addElement(JasperdesignUtil.ajouterLigneVerticale(calcField.getX(),
                        calcField.getY(),
                        // cellColumnFooterInfo.getXLeftCornerCalcule(),
                        // cellColumnFooterInfo.getYLeftCornerCalcule(),
                        0,
                        // cellColumnFooterInfo.getHeight(),
                        calcField.getHeight(), PositionTypeEnum.FIX_RELATIVE_TO_TOP,
                        StretchTypeEnum.NO_STRETCH,
                        documentInfo.getLineWidth()));

                    // Trace les bordures de la cellule
                    ColumnFooterUtil.traceCellBorder(documentInfo, jrDesignFrame, cellColumnFooterInfo, currentRow);
                }

                else
                {
                    JRDesignTextField textField =
                        ColumnFooterUtil.creerCellFooterCol(jasperDesign, cellColumnFooterInfo, documentInfo);

                    // Création de la variable de calcul
                    JRDesignExpression expression =
                        createCellExpression(model, documentInfo, jasperDesign, cellColumnFooterInfo, currentRow);

                    textField.setExpression(expression);

                    // Correction du pattern de formatage dans le cas d'un
                    // calcul de type decompte
                    if ((cellColumnFooterInfo.getCellTypeCalcul() != TypeCalcul.decompte)
                        || (cellColumnFooterInfo.getCellTypeCalcul() != TypeCalcul.decomptenonnulls))
                    {
                        textField.setPattern(cellColumnFooterInfo.getPattern());
                    }
                    else
                    {
                        textField.setPattern((new NombreCellZeroDecimale()).getFormatPattern());
                    }

                    jrDesignFrame.addElement(textField);

                    // Trace les bordures de la cellule
                    if (cellColumnFooterInfo.getCellTypeCalcul() == null)
                    {
                        if (cellColumnFooterInfo.getTitre().compareTo("") == 0)
                        {
                            // Trace les les lignes pour une cellule non calculée, qui ne contient pas de texte
                            if (currentCol == 0)
                            {
                                // Si première colonne trace ligne haut, bas, gauche
                                if (currentRow > 0)
                                {
                                    jrDesignFrame.addElement(JasperdesignUtil.ajouterLigneHorizontale(cellColumnFooterInfo
                                        .getXLeftCornerCalcule(), cellColumnFooterInfo.getYLeftCornerCalcule(), cellColumnFooterInfo
                                        .getWidthCalcule(), 0, PositionTypeEnum.FIX_RELATIVE_TO_TOP, documentInfo.getLineWidth()));
                                }

                                // Ligne horizontale inférieure
                                jrDesignFrame.addElement(JasperdesignUtil.ajouterLigneHorizontale(
                                    cellColumnFooterInfo.getXLeftCornerCalcule(),
                                    cellColumnFooterInfo.getYLeftCornerCalcule() + cellColumnFooterInfo.getHeight(), cellColumnFooterInfo
                                        .getWidthCalcule(), 0, PositionTypeEnum.FIX_RELATIVE_TO_TOP, documentInfo.getLineWidth()));

                                // Si l'element n'est pas absorbant
                                if (!cellColumnFooterInfo.isMergeWithChild())
                                {
                                    // Ligne verticale gauche
                                    jrDesignFrame.addElement(JasperdesignUtil.ajouterLigneVerticale(cellColumnFooterInfo
                                        .getXLeftCornerCalcule(), cellColumnFooterInfo.getYLeftCornerCalcule(), 0, cellColumnFooterInfo
                                        .getHeight(), PositionTypeEnum.FIX_RELATIVE_TO_TOP, StretchTypeEnum.NO_STRETCH,
                                        documentInfo.getLineWidth()));
                                }
                            }
                            // Si pas premiere colonne trace ligne haut et bas
                            else
                            {
                                if (currentRow > 0)
                                {
                                    jrDesignFrame.addElement(JasperdesignUtil.ajouterLigneHorizontale(cellColumnFooterInfo
                                        .getXLeftCornerCalcule(), cellColumnFooterInfo.getYLeftCornerCalcule(), cellColumnFooterInfo
                                        .getWidthCalcule(), 0, PositionTypeEnum.FIX_RELATIVE_TO_TOP, documentInfo.getLineWidth()));
                                }

                                // Ligne horizontale inférieure
                                jrDesignFrame.addElement(JasperdesignUtil.ajouterLigneHorizontale(
                                    cellColumnFooterInfo.getXLeftCornerCalcule(),
                                    cellColumnFooterInfo.getYLeftCornerCalcule() + cellColumnFooterInfo.getHeight(), cellColumnFooterInfo
                                        .getWidthCalcule(), 0, PositionTypeEnum.FIX_RELATIVE_TO_TOP, documentInfo.getLineWidth()));

                                // Si calcTitle deja traité alors trace ligne gauche et droite
                                if (passeCalcTitle)
                                {
                                    // Ligne verticale droite
                                    if (!cellColumnFooterInfo.isAbsorbe())
                                    {
                                        jrDesignFrame.addElement(JasperdesignUtil.ajouterLigneVerticale(cellColumnFooterInfo
                                            .getXLeftCornerCalcule()
                                            + cellColumnFooterInfo.getWidthCalcule(), cellColumnFooterInfo.getYLeftCornerCalcule(), 0,
                                            cellColumnFooterInfo.getHeight(), PositionTypeEnum.FIX_RELATIVE_TO_TOP,
                                            StretchTypeEnum.NO_STRETCH, documentInfo.getLineWidth()));
                                    }
                                    // Si l'element n'est pas absorbant
                                    if (!cellColumnFooterInfo.isMergeWithChild())
                                    {
                                        // Ligne verticale gauche
                                        jrDesignFrame.addElement(JasperdesignUtil.ajouterLigneVerticale(cellColumnFooterInfo
                                            .getXLeftCornerCalcule(), cellColumnFooterInfo.getYLeftCornerCalcule(), 0, cellColumnFooterInfo
                                            .getHeight(), PositionTypeEnum.FIX_RELATIVE_TO_TOP, StretchTypeEnum.NO_STRETCH,
                                            documentInfo.getLineWidth()));
                                    }
                                }
                            }
                        }
                        // Trace les lignes haut, bas, doite pour une cellule non calculée mais contenant du texte
                        // (calcTitle)
                        // de plus si il s'agit de la premier colonne trace la ligne gauche
                        else
                        {
                            // Alignement à droite du calcTitle et met à vrai passeCalcTitle
                            textField.setHorizontalAlignment(HorizontalAlignEnum.getByValue(new Byte("3")));
                            passeCalcTitle = true;

                            if (currentRow > 0)
                            {
                                jrDesignFrame.addElement(JasperdesignUtil.ajouterLigneHorizontale(cellColumnFooterInfo
                                    .getXLeftCornerCalcule(), cellColumnFooterInfo.getYLeftCornerCalcule(), cellColumnFooterInfo
                                    .getWidthCalcule(), 0, PositionTypeEnum.FIX_RELATIVE_TO_TOP, documentInfo.getLineWidth()));
                            }

                            // Ligne horizontale inférieure
                            jrDesignFrame.addElement(JasperdesignUtil.ajouterLigneHorizontale(cellColumnFooterInfo.getXLeftCornerCalcule(),
                                cellColumnFooterInfo.getYLeftCornerCalcule() + cellColumnFooterInfo.getHeight(), cellColumnFooterInfo
                                    .getWidthCalcule(), 0, PositionTypeEnum.FIX_RELATIVE_TO_TOP, documentInfo.getLineWidth()));

                            // Ligne verticale droite
                            if (!cellColumnFooterInfo.isAbsorbe())
                            {
                                jrDesignFrame.addElement(JasperdesignUtil.ajouterLigneVerticale(cellColumnFooterInfo
                                    .getXLeftCornerCalcule()
                                    + cellColumnFooterInfo.getWidthCalcule(), cellColumnFooterInfo.getYLeftCornerCalcule(), 0,
                                    cellColumnFooterInfo.getHeight(), PositionTypeEnum.FIX_RELATIVE_TO_TOP,
                                    StretchTypeEnum.NO_STRETCH, documentInfo.getLineWidth()));
                            }
                            // Si l'element n'est pas absorbant et premiere colonne
                            if (!cellColumnFooterInfo.isMergeWithChild() && currentCol == 0)
                            {
                                // Ligne verticale gauche
                                jrDesignFrame.addElement(JasperdesignUtil.ajouterLigneVerticale(cellColumnFooterInfo
                                    .getXLeftCornerCalcule(), cellColumnFooterInfo.getYLeftCornerCalcule(), 0, cellColumnFooterInfo
                                    .getHeight(), PositionTypeEnum.FIX_RELATIVE_TO_TOP, StretchTypeEnum.NO_STRETCH,
                                    documentInfo.getLineWidth()));
                            }
                        }
                    }
                    // trace les lignes haut, bas, droite et gauche pour l'ensemble des cellules calculées
                    else
                    {
                        if (currentRow > 0)
                        {
                            jrDesignFrame.addElement(JasperdesignUtil.ajouterLigneHorizontale(cellColumnFooterInfo
                                .getXLeftCornerCalcule(), cellColumnFooterInfo.getYLeftCornerCalcule(), cellColumnFooterInfo
                                .getWidthCalcule(), 0, PositionTypeEnum.FIX_RELATIVE_TO_TOP, documentInfo.getLineWidth()));
                        }

                        // Ligne horizontale inférieure
                        jrDesignFrame.addElement(JasperdesignUtil.ajouterLigneHorizontale(cellColumnFooterInfo.getXLeftCornerCalcule(),
                            cellColumnFooterInfo.getYLeftCornerCalcule() + cellColumnFooterInfo.getHeight(), cellColumnFooterInfo
                                .getWidthCalcule(), 0, PositionTypeEnum.FIX_RELATIVE_TO_TOP, documentInfo.getLineWidth()));

                        // Si l'element n'est pas absorbant
                        if (!cellColumnFooterInfo.isMergeWithChild())
                        {
                            // Ligne verticale gauche
                            jrDesignFrame.addElement(JasperdesignUtil.ajouterLigneVerticale(cellColumnFooterInfo
                                .getXLeftCornerCalcule(), cellColumnFooterInfo.getYLeftCornerCalcule(), 0, cellColumnFooterInfo
                                .getHeight(), PositionTypeEnum.FIX_RELATIVE_TO_TOP, StretchTypeEnum.NO_STRETCH,
                                documentInfo.getLineWidth()));
                        }
                        // Ligne verticale droite
                        if (!cellColumnFooterInfo.isAbsorbe())
                        {
                            jrDesignFrame.addElement(JasperdesignUtil.ajouterLigneVerticale(cellColumnFooterInfo
                                .getXLeftCornerCalcule()
                                + cellColumnFooterInfo.getWidthCalcule(), cellColumnFooterInfo.getYLeftCornerCalcule(), 0,
                                cellColumnFooterInfo.getHeight(), PositionTypeEnum.FIX_RELATIVE_TO_TOP,
                                StretchTypeEnum.NO_STRETCH, documentInfo.getLineWidth()));
                        }
                    }
                }
                currentCol++;
            }
            currentRow++;
        }
    }

    /**
     * methode Creates the calculated cell expression :
     * 
     * @param model model
     * @param documentInfo documentInfo
     * @param jasperDesign jasperdesign
     * @param cellFooterInfo cellFooterInfo
     * @param currentRow currentRow
     * @return jR design expression
     */
    @SuppressWarnings("unchecked")
    private static JRDesignExpression createCalculatedCellExpression(TableModel model, DocumentInfo documentInfo,
        JasperDesign jasperDesign, CellColumnFooterInfo cellFooterInfo, int currentRow)
    {

        JRDesignExpression expressionTextfield = new JRDesignExpression();

        if (cellFooterInfo != null && cellFooterInfo.getCellTypeCalcul() != null)
        {
            String variableName =
                cellFooterInfo.getAlias() + cellFooterInfo.getCellTypeCalcul().toString() + currentRow;

            JRDesignVariable cellVariable = new JRDesignVariable();
            cellVariable.setName(variableName);
            cellVariable.setValueClass(cellFooterInfo.getClazz());

            Map variableMap = jasperDesign.getVariablesMap();
            if (!variableMap.containsKey(variableName))
            {
                try
                {
                    jasperDesign.addVariable(cellVariable);
                }
                catch (JRException jrex)
                {
                    throw new EcTableException("Erreur ajout variable", jrex);
                }
            }

            JRDesignExpression expression = new JRDesignExpression();
            expression.setValueClass(cellFooterInfo.getClazz());
            expression.setText("$F{" + cellFooterInfo.getPropriete() + "}");

            cellVariable.setExpression(expression);

            switch (cellFooterInfo.getCellInitialisation())
            {
                case rapport:
                    cellVariable.setResetType(ResetTypeEnum.REPORT);
                    break;
                case page:
                    cellVariable.setResetType(ResetTypeEnum.PAGE);
                    break;
                default:
                    cellVariable.setResetType(ResetTypeEnum.REPORT);
            }

            switch (cellFooterInfo.getCellTypeCalcul())
            {
                case somme:
                    cellVariable.setCalculation(CalculationEnum.SUM);
                    break;

                case moyenne:
                    cellVariable.setCalculation(CalculationEnum.AVERAGE);
                    break;

                case min:
                    cellVariable.setCalculation(CalculationEnum.LOWEST);
                    break;

                case max:
                    cellVariable.setCalculation(CalculationEnum.HIGHEST);
                    break;

                /**
                 * modif amlp 06/01/09 doc jasper COLUMN_COUNT This variable contains the number of records that were
                 * processed when generating the current column. CALCULATION_COUNT The value is calculated by counting
                 * the non-null values of the variable expression with every iteration in the data source.
                 * CALCULATION_DISTINCT_COUNT The value is calculated by counting the distinct non-null values of the
                 * variable expression with every iteration in the data source. donc pour decompte on ne met plus
                 * CALCULATION_COUNT mais COLUMN_COUNT pourdecompte distinct on met CALCULATION_COUNT
                 */
                case decompte:
                    // cellVariable.setCalculation(JRDesignVariable.CALCULATION_COUNT);

                    cellVariable.setCalculation(CalculationEnum.SYSTEM);
                    JRDesignExpression expression1 = new JRDesignExpression();
                    expression1.setValueClass(java.lang.Integer.class);
                    // attention column_count ne marche pas si * pages
                    // remplacement (vu dans le forum) par REPORT_COUNT
                    expression1.setText("$V{REPORT_COUNT}");
                    return expression1;

                case decomptenonnulls:
                    // attention ne marche pas dans le cas de boollen , date ,string a revoir ...
                    // cellVariable
                    // .setCalculation(JRDesignVariable.CALCULATION_DISTINCT_COUNT);
                    cellVariable.setCalculation(CalculationEnum.COUNT);
                    break;
                default:
                    cellVariable.setCalculation(CalculationEnum.NOTHING);
                    break;
            }

            expressionTextfield.setValueClass(cellFooterInfo.getClazz());
            expressionTextfield.setText("$V{" + variableName + "}");
        }
        return expressionTextfield;
    }

    /**
     * methode Creates the cell expression :
     * 
     * @param model model
     * @param documentInfo documentInfo
     * @param jasperDesign jasperdesign
     * @param cellFooterInfo cellFooterInfo
     * @param currentRow currentRow
     * @return jR design expression
     */
    public static JRDesignExpression createCellExpression(TableModel model, DocumentInfo documentInfo,
        JasperDesign jasperDesign, CellColumnFooterInfo cellFooterInfo, int currentRow)
    {
        JRDesignExpression expressionTextfield;

        // S'il s'agit d'une cellules calculée
        // Création des variables de calcul si nécessaire
        if (cellFooterInfo.isCalculatedCell())
        {
            expressionTextfield =
                createCalculatedCellExpression(model, documentInfo, jasperDesign, cellFooterInfo, currentRow);
        }
        // S'il s'agit d'une colonne contenant un titre
        else
        {
            expressionTextfield = createTitleCellExpression(cellFooterInfo);
        }
        return expressionTextfield;
    }

    /**
     * methode Creates the title cell expression :
     * 
     * @param cellFooterInfo cellFooterInfo
     * @return jR design expression
     */
    private static JRDesignExpression createTitleCellExpression(CellColumnFooterInfo cellFooterInfo)
    {
        JRDesignExpression expressionTextfield;
        expressionTextfield = new JRDesignExpression();
        expressionTextfield.setValueClass(String.class);
        expressionTextfield.setText("\"" + cellFooterInfo.getTitre() + "\"");
        return expressionTextfield;
    }

    /**
     * methode Creertwo split cell footer col :
     * 
     * @param jasperdesign jasperdesign
     * @param cellColumnFooterInfo cellColumnFooterInfo
     * @param documentInfo documentInfo
     * @return list
     */
    public static List<JRDesignTextField> creertwoSplitCellFooterCol(JasperDesign jasperdesign,
        CellColumnFooterInfo cellColumnFooterInfo, DocumentInfo documentInfo)
    {

        List<JRDesignTextField> splitedFields = new ArrayList<JRDesignTextField>();

        int xleftCornerTitleField = cellColumnFooterInfo.getXLeftCornerCalcule();
        int widthTitleField = cellColumnFooterInfo.getWidthCalcule() / 2;

        int xleftCornerCalcField = cellColumnFooterInfo.getXLeftCornerCalcule() + widthTitleField;
        int widthCalcField = cellColumnFooterInfo.getWidthCalcule() - widthTitleField;

        JRDesignTextField textTitleField =
            createField(jasperdesign, cellColumnFooterInfo, documentInfo, xleftCornerTitleField, widthTitleField);

        JRDesignTextField calcTitleField =
            createField(jasperdesign, cellColumnFooterInfo, documentInfo, xleftCornerCalcField, widthCalcField);

        splitedFields.add(textTitleField);
        splitedFields.add(calcTitleField);

        return splitedFields;
    }

    /**
     * methode Creates the field :
     * 
     * @param jasperDesign jasperdesign
     * @param cellColumnFooterInfo cellColumnFooterInfo
     * @param documentInfo documentInfo
     * @param xLeftCorner xLeftCorner
     * @param width width
     * @return jR design text field
     */
    private static JRDesignTextField createField(JasperDesign jasperDesign, CellColumnFooterInfo cellColumnFooterInfo,
        DocumentInfo documentInfo, int xLeftCorner, int width)
    {
        JRStyle styleCellEntete =
            JasperdesignUtil.creerStyleComplet(jasperDesign, STYLE_CELLULE_PIED_COLONNE_EDITION, documentInfo
                .getColumnFooter().getStyle(), documentInfo.getDetail().getCellMargin(), documentInfo.getDetail()
                .getCellMargin(), documentInfo.getDetail().getCellMargin(), documentInfo.getDetail().getCellMargin(),
                cellColumnFooterInfo.getAlignement(), VerticalAlignEnum.MIDDLE);

        JRDesignTextField textTitleField = new JRDesignTextField();
        textTitleField.setBlankWhenNull(true);
        textTitleField.setX(xLeftCorner);
        textTitleField.setY(cellColumnFooterInfo.getYLeftCornerCalcule());
        textTitleField.setWidth(width);
        textTitleField.setHeight(cellColumnFooterInfo.getHeight());
        /*
         * textTitleField.setTopPadding(documentInfo.getDetail().getCellMargin());
         * textTitleField.setBottomPadding(documentInfo.getDetail().getCellMargin());
         * textTitleField.setRightPadding(documentInfo.getDetail().getCellMargin());
         * textTitleField.setLeftPadding(documentInfo.getDetail().getCellMargin())
         */;

        textTitleField.setStyle(styleCellEntete);
        textTitleField.setStretchWithOverflow(false);

        if (documentInfo.getColumnHeader().isOpaque())
        {
            Color color =
                new Color(documentInfo.getColumnHeader().getColorR(), documentInfo.getColumnHeader().getColorG(),
                    documentInfo.getColumnHeader().getColorB());
            textTitleField.setBackcolor(color);
            textTitleField.setMode(ModeEnum.OPAQUE);
        }

        /* amlp réactivation de la ligne suivante sinon le champs calculé ne tient pas compte de l'alignement* */
        textTitleField.setHorizontalAlignment(cellColumnFooterInfo.getAlignement());

        // textTitleField.setVerticalAlignment(JRAlignment.VERTICAL_ALIGN_MIDDLE);
        return textTitleField;
    }

    /**
     * Création du champ d'une cellule de pied de page.
     * 
     * @param jasperdesign jasperdesign
     * @param cellColumnFooterInfo cellColumnFooterInfo
     * @param documentInfo documentInfo
     * @return jR design text field
     */
    public static JRDesignTextField creerCellFooterCol(JasperDesign jasperdesign,
        CellColumnFooterInfo cellColumnFooterInfo, DocumentInfo documentInfo)
    {

        return createField(jasperdesign, cellColumnFooterInfo, documentInfo, cellColumnFooterInfo.getXLeftCornerCalcule(),
                cellColumnFooterInfo.getWidthCalcule());
    }

}
