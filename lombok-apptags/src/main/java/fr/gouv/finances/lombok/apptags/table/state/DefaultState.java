/*
/*Copyright (c) 2014 DGFiP - Tous droits réservés/* * Copyright 2004 original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package fr.gouv.finances.lombok.apptags.table.state;

import java.util.Map;

import fr.gouv.finances.lombok.apptags.table.context.Context;

/**
 * Class DefaultState
 */
public class DefaultState implements State
{

    public DefaultState()
    {
        super();       
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.state.State#saveParameters(fr.gouv.finances.lombok.apptags.table.context.Context,
     *      java.lang.String, java.util.Map)
     */
    @Override
    public void saveParameters(Context context, String tableId, Map<String, Object> parameterMap)
    {
        // RAS
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.state.State#getParameters(fr.gouv.finances.lombok.apptags.table.context.Context,
     *      java.lang.String, java.lang.String)
     */
    @Override
    public Map<String, Object> getParameters(Context context, String tableId, String stateAttr)
    {
        return null;
    }
}
