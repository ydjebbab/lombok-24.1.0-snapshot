/*
 * Copyright (c) 2014 DGFiP - Tous droits réservés
 *//*
/*Copyright (c) 2014 DGFiP - Tous droits réservés/* * Copyright 2004 original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package fr.gouv.finances.lombok.apptags.table.view.html;

import fr.gouv.finances.lombok.apptags.table.bean.Export;
import fr.gouv.finances.lombok.apptags.table.core.Messages;
import fr.gouv.finances.lombok.apptags.table.core.TableConstants;
import fr.gouv.finances.lombok.apptags.table.core.TableModel;
import fr.gouv.finances.lombok.apptags.table.view.html.toolbar.ButtonItem;
import fr.gouv.finances.lombok.apptags.table.view.html.toolbar.ImageItem;
import fr.gouv.finances.lombok.apptags.table.view.html.toolbar.TextItem;
import fr.gouv.finances.lombok.apptags.table.view.html.toolbar.ToolbarItemUtils;
import fr.gouv.finances.lombok.apptags.util.HtmlBuilder;

/**
 * Class ToolbarBuilder
 */
public class ToolbarBuilder
{

    /** html. */
    private HtmlBuilder html;

    /** model. */
    private TableModel model;

    /** messages. */
    private Messages messages;

    /**
     * Instanciation de toolbar builder.
     * 
     * @param model
     */
    public ToolbarBuilder(TableModel model)
    {
        this(new HtmlBuilder(), model);
    }

    /**
     * Instanciation de toolbar builder.
     * 
     * @param html
     * @param model
     */
    public ToolbarBuilder(HtmlBuilder html, TableModel model)
    {
        this.html = html;
        this.model = model;
        this.messages = model.getMessages();
    }

    /**
     * Accesseur de l attribut html builder.
     * 
     * @return html builder
     */
    public HtmlBuilder getHtmlBuilder()
    {
        return html;
    }

    /**
     * Accesseur de l attribut table model.
     * 
     * @return table model
     */
    protected TableModel getTableModel()
    {
        return model;
    }

    /**
     * Accesseur de l attribut messages.
     * 
     * @return messages
     */
    protected Messages getMessages()
    {
        return messages;
    }

    /**
     * methode First page item as button :
     */
    public void firstPageItemAsButton()
    {
        ButtonItem item = new ButtonItem();
        item.setTooltip(messages.getMessage(BuilderConstants.TOOLBAR_FIRST_PAGE_TOOLTIP));
        item.setContents(messages.getMessage(BuilderConstants.TOOLBAR_FIRST_PAGE_TEXT));
        item.setStyle("btn firstPageItemAsButton");
        item.setStyleClass("btn firstPageItemAsButton");
        html.style("btn firstPageItemAsButton html");
        html.styleClass("btn firstPageItemAsButton html");

        ToolbarItemUtils.buildFirstPage(html, model, item);
    }

    /**
     * methode First page item as image :
     */
    public void firstPageItemAsImage()
    {
        ImageItem item = new ImageItem();
        item.setTooltip(messages.getMessage(BuilderConstants.TOOLBAR_FIRST_PAGE_TOOLTIP));
        item.setDisabledImage(BuilderUtils.getImage(model, BuilderConstants.TOOLBAR_FIRST_PAGE_DISABLED_IMAGE));
        item.setImage(BuilderUtils.getImage(model, BuilderConstants.TOOLBAR_FIRST_PAGE_IMAGE));
        item.setAlt(messages.getMessage(BuilderConstants.TOOLBAR_FIRST_PAGE_TEXT));
        item.setStyle("border:0");
        ToolbarItemUtils.buildFirstPage(html, model, item);
    }

    /**
     * methode First page item as text :
     */
    public void firstPageItemAsText()
    {
        TextItem item = new TextItem();
        item.setTooltip(messages.getMessage(BuilderConstants.TOOLBAR_FIRST_PAGE_TOOLTIP));
        item.setText(messages.getMessage(BuilderConstants.TOOLBAR_FIRST_PAGE_TEXT));
        ToolbarItemUtils.buildFirstPage(html, model, item);
    }

    /**
     * methode Prev page item as button :
     */
    public void prevPageItemAsButton()
    {
        ButtonItem item = new ButtonItem();
        item.setTooltip(messages.getMessage(BuilderConstants.TOOLBAR_PREV_PAGE_TOOLTIP));
        item.setContents(messages.getMessage(BuilderConstants.TOOLBAR_PREV_PAGE_TEXT));
        ToolbarItemUtils.buildPrevPage(html, model, item);
    }

    /**
     * methode Prev page item as button bis :
     */
    public void prevPageItemAsButtonBis()
    {
        ButtonItem item = new ButtonItem();
        item.setTooltip(messages.getMessage(BuilderConstants.TOOLBAR_PREV_PAGE_TOOLTIP));
        item.setStyleClass("btn btn-mini prev");
        item.setContents("<i class='icon-chevron-left'></i>");
        ToolbarItemUtils.buildPrevPage(html, model, item);
    }

    /**
     * methode Prev page item as image :
     */
    public void prevPageItemAsImage()
    {
        ImageItem item = new ImageItem();
        item.setTooltip(messages.getMessage(BuilderConstants.TOOLBAR_PREV_PAGE_TOOLTIP));
        item.setDisabledImage(BuilderUtils.getImage(model, BuilderConstants.TOOLBAR_PREV_PAGE_DISABLED_IMAGE));
        item.setImage(BuilderUtils.getImage(model, BuilderConstants.TOOLBAR_PREV_PAGE_IMAGE));
        item.setAlt(messages.getMessage(BuilderConstants.TOOLBAR_PREV_PAGE_TEXT));
        item.setStyle("border:0");
        ToolbarItemUtils.buildPrevPage(html, model, item);
    }

    /**
     * methode Prev page item as text :
     */
    public void prevPageItemAsText()
    {
        TextItem item = new TextItem();
        item.setTooltip(messages.getMessage(BuilderConstants.TOOLBAR_PREV_PAGE_TOOLTIP));
        item.setText(messages.getMessage(BuilderConstants.TOOLBAR_PREV_PAGE_TEXT));
        ToolbarItemUtils.buildPrevPage(html, model, item);
    }

    /**
     * methode Some page item as text :
     * 
     * @param i
     */
    public void somePageItemAsText(int i)
    {
        TextItem item = new TextItem();
        item.setTooltip("page n°" + i);
        item.setText("" + i);
        item.setStyleClass("btn btn-mini page");
        ToolbarItemUtils.buildSomePage(html, model, item, i);
    }

    /**
     * methode Space page item as text :
     */
    public void spacePageItemAsText()
    {
        TextItem item = new TextItem();
        item.setTooltip("page intermédiaire");
        item.setText("...");
        ToolbarItemUtils.buildSpacePage(html, model, item);
    }

    /**
     * methode Next page item as button :
     */
    public void nextPageItemAsButton()
    {
        ButtonItem item = new ButtonItem();
        item.setTooltip(messages.getMessage(BuilderConstants.TOOLBAR_NEXT_PAGE_TOOLTIP));
        item.setContents(messages.getMessage(BuilderConstants.TOOLBAR_NEXT_PAGE_TEXT));
        ToolbarItemUtils.buildNextPage(html, model, item);
    }

    /**
     * methode Next page item as button bis :
     */
    public void nextPageItemAsButtonBis()
    {
        ButtonItem item = new ButtonItem();
        item.setTooltip(messages.getMessage(BuilderConstants.TOOLBAR_NEXT_PAGE_TOOLTIP));
        item.setStyleClass("btn btn-mini next");
        item.setContents("<i class='icon-chevron-right'></i>");
        ToolbarItemUtils.buildNextPage(html, model, item);
    }

    /**
     * methode Next page item as image :
     */
    public void nextPageItemAsImage()
    {
        ImageItem item = new ImageItem();
        item.setTooltip(messages.getMessage(BuilderConstants.TOOLBAR_NEXT_PAGE_TOOLTIP));
        item.setDisabledImage(BuilderUtils.getImage(model, BuilderConstants.TOOLBAR_NEXT_PAGE_DISABLED_IMAGE));
        item.setImage(BuilderUtils.getImage(model, BuilderConstants.TOOLBAR_NEXT_PAGE_IMAGE));
        item.setAlt(messages.getMessage(BuilderConstants.TOOLBAR_NEXT_PAGE_TEXT));
        item.setStyle("border:0");
        ToolbarItemUtils.buildNextPage(html, model, item);
    }

    /**
     * methode Next page item as text :
     */
    public void nextPageItemAsText()
    {
        TextItem item = new TextItem();
        item.setTooltip(messages.getMessage(BuilderConstants.TOOLBAR_NEXT_PAGE_TOOLTIP));
        item.setText(messages.getMessage(BuilderConstants.TOOLBAR_NEXT_PAGE_TEXT));
        ToolbarItemUtils.buildNextPage(html, model, item);
    }

    /**
     * methode Last page item as button :
     */
    public void lastPageItemAsButton()
    {
        ButtonItem item = new ButtonItem();
        item.setTooltip(messages.getMessage(BuilderConstants.TOOLBAR_LAST_PAGE_TOOLTIP));
        item.setContents(messages.getMessage(BuilderConstants.TOOLBAR_LAST_PAGE_TEXT));
        ToolbarItemUtils.buildLastPage(html, model, item);
    }

    /**
     * methode Last page item as image :
     */
    public void lastPageItemAsImage()
    {
        ImageItem item = new ImageItem();
        item.setTooltip(messages.getMessage(BuilderConstants.TOOLBAR_LAST_PAGE_TOOLTIP));
        item.setDisabledImage(BuilderUtils.getImage(model, BuilderConstants.TOOLBAR_LAST_PAGE_DISABLED_IMAGE));
        item.setImage(BuilderUtils.getImage(model, BuilderConstants.TOOLBAR_LAST_PAGE_IMAGE));
        item.setAlt(messages.getMessage(BuilderConstants.TOOLBAR_LAST_PAGE_TEXT));
        item.setStyle("border:0");
        ToolbarItemUtils.buildLastPage(html, model, item);
    }

    /**
     * methode Last page item as text :
     */
    public void lastPageItemAsText()
    {
        TextItem item = new TextItem();
        item.setTooltip(messages.getMessage(BuilderConstants.TOOLBAR_LAST_PAGE_TOOLTIP));
        item.setText(messages.getMessage(BuilderConstants.TOOLBAR_LAST_PAGE_TEXT));
        ToolbarItemUtils.buildLastPage(html, model, item);
    }

    /**
     * methode Filter item as button :
     */
    public void filterItemAsButton()
    {
        ButtonItem item = new ButtonItem();
        item.setTooltip(messages.getMessage(BuilderConstants.TOOLBAR_FILTER_TOOLTIP));
        item.setContents(messages.getMessage(BuilderConstants.TOOLBAR_FILTER_TEXT));
        ToolbarItemUtils.buildFilter(html, model, item);
    }

    /**
     * methode Filter item as image :
     */
    public void filterItemAsImage()
    {
        ImageItem item = new ImageItem();
        item.setTooltip(messages.getMessage(BuilderConstants.TOOLBAR_FILTER_TOOLTIP));
        item.setImage(BuilderUtils.getImage(model, BuilderConstants.TOOLBAR_FILTER_IMAGE));
        item.setAlt(messages.getMessage(BuilderConstants.TOOLBAR_FILTER_TEXT));
        item.setStyle("border:0");
        ToolbarItemUtils.buildFilter(html, model, item);
    }

    /**
     * methode Filter item as text :
     */
    public void filterItemAsText()
    {
        TextItem item = new TextItem();
        item.setTooltip(messages.getMessage(BuilderConstants.TOOLBAR_FILTER_TOOLTIP));
        item.setText(messages.getMessage(BuilderConstants.TOOLBAR_FILTER_TEXT));
        ToolbarItemUtils.buildFilter(html, model, item);
    }

    /**
     * methode Clear item as button :
     */
    public void clearItemAsButton()
    {
        ButtonItem item = new ButtonItem();
        item.setTooltip(messages.getMessage(BuilderConstants.TOOLBAR_CLEAR_TOOLTIP));
        item.setContents(messages.getMessage(BuilderConstants.TOOLBAR_CLEAR_TEXT));
        ToolbarItemUtils.buildClear(html, model, item);
    }

    /**
     * methode Clear item as image :
     */
    public void clearItemAsImage()
    {
        ImageItem item = new ImageItem();
        item.setTooltip(messages.getMessage(BuilderConstants.TOOLBAR_CLEAR_TOOLTIP));
        item.setImage(BuilderUtils.getImage(model, BuilderConstants.TOOLBAR_CLEAR_IMAGE));
        item.setAlt(messages.getMessage(BuilderConstants.TOOLBAR_CLEAR_TEXT));
        item.setStyle("border:0");
        ToolbarItemUtils.buildClear(html, model, item);
    }

    /**
     * methode Clear item as text :
     */
    public void clearItemAsText()
    {
        TextItem item = new TextItem();
        item.setTooltip(messages.getMessage(BuilderConstants.TOOLBAR_CLEAR_TOOLTIP));
        item.setText(messages.getMessage(BuilderConstants.TOOLBAR_CLEAR_TEXT));
        ToolbarItemUtils.buildClear(html, model, item);
    }

    /**
     * methode Export item as button :
     * 
     * @param export
     */
    public void exportItemAsButton(Export export)
    {
        ButtonItem item = new ButtonItem();
        item.setTooltip(export.getTooltip());
        item.setContents(export.getText());
        ToolbarItemUtils.buildExport(html, model, item, export);
    }

    /**
     * methode Export item as image :
     * 
     * @param export
     */
    public void exportItemAsImage(Export export)
    {
        ImageItem item = new ImageItem();
        item.setTooltip(export.getTooltip());
        item.setImage(BuilderUtils.getImage(model, export.getImageName()));
        item.setAlt(export.getText());
        item.setStyle("border:0");
        ToolbarItemUtils.buildExport(html, model, item, export);
    }

    /**
     * methode Export item as new image :
     * 
     * @param export
     */
    public void exportItemAsNewImage(Export export)
    {
        ImageItem item = new ImageItem();
        item.setTooltip(export.getTooltip());
        item.setImage(BuilderUtils.getImage(model, "new_" + export.getImageName()));
        item.setAlt(export.getText());
        item.setStyle("border:0");
        ToolbarItemUtils.buildExport(html, model, item, export);
    }

    /**
     * methode Export item as text :
     * 
     * @param export
     */
    public void exportItemAsText(Export export)
    {
        TextItem item = new TextItem();
        item.setTooltip(export.getTooltip());
        item.setText(export.getText());
        ToolbarItemUtils.buildExport(html, model, item, export);
    }

    /**
     * methode Rows displayed droplist :
     */
    public void rowsDisplayedDroplist()
    {
        int rowsDisplayed = model.getTableHandler().getTable().getRowsDisplayed();
        int medianRowsDisplayed = model.getTableHandler().getTable().getMedianRowsDisplayed();
        int maxRowsDisplayed = model.getTableHandler().getTable().getMaxRowsDisplayed();
        int currentRowsDisplayed = model.getLimit().getCurrentRowsDisplayed();

        html.select().name(model.getTableHandler().prefixWithTableId() + TableConstants.ROWS_DISPLAYED);

        StringBuilder   onchange = new StringBuilder();
        onchange.append(new TableActions(model).getRowsDisplayedAction());
        html.onchange(onchange.toString());

        html.close();

        html.newline();
        html.tabs(4);

        // default rows
        html.option().value(String.valueOf(rowsDisplayed));
        if (currentRowsDisplayed == rowsDisplayed)
        {
            html.selected();
        }
        html.close();
        html.append(String.valueOf(rowsDisplayed));
        html.optionEnd();

        // median rows
        html.option().value(String.valueOf(medianRowsDisplayed));
        if (currentRowsDisplayed == medianRowsDisplayed)
        {
            html.selected();
        }
        html.close();
        html.append(String.valueOf(medianRowsDisplayed));
        html.optionEnd();

        // max rows
        html.option().value(String.valueOf(maxRowsDisplayed));
        if (currentRowsDisplayed == maxRowsDisplayed)
        {
            html.selected();
        }
        html.close();
        html.append(String.valueOf(maxRowsDisplayed));
        html.optionEnd();

        html.newline();
        html.tabs(4);
        html.selectEnd();
    }

    /**
     * methode Separator :
     */
    public void separator()
    {
        html.img();
        html.src(BuilderUtils.getImage(model, BuilderConstants.TOOLBAR_SEPARATOR_IMAGE));
        html.alt(messages.getMessage(BuilderConstants.TOOLBAR_SEPARATOR_TEXT));
        html.style("border:0");
        html.xclose();
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        return html.toString();
    }
}
