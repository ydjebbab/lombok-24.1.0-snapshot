/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.apptags.table.calc;

import java.math.BigDecimal;
import java.util.Collection;

import fr.gouv.finances.lombok.apptags.table.bean.Column;
import fr.gouv.finances.lombok.apptags.table.core.TableModel;

/**
 * Class CpMoyenneCalc
 */
public class CpMoyenneCalc implements Calc
{

    /**
     * Constructeur de la classe CpMoyenneCalc.java
     *
     */
    public CpMoyenneCalc()
    {
        super();
        
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.calc.Calc#getCalcResult(fr.gouv.finances.lombok.apptags.table.core.TableModel,
     *      fr.gouv.finances.lombok.apptags.table.bean.Column)
     */
    @Override
    public Number getCalcResult(TableModel model, Column column)
    {
        Collection<Object> rows = model.getCollectionOfFilteredBeans();
        String property = column.getProperty();
        MoyenneValue moyenneValue = new MoyenneValue(rows.size());
        CpCalcUtils.eachRowCalcValue(moyenneValue, rows, property);

        return moyenneValue.getAverageValue();
    }

    /**
     * Class MoyenneValue
     */
    private static class MoyenneValue implements CalcHandler
    {

        /** total. */
        private BigDecimal total = new BigDecimal(0);

        /** row count. */
        private double rowCount;

        /**
         * Instanciation de moyenne value.
         * 
         * @param rowCount
         */
        public MoyenneValue(double rowCount)
        {
            this.rowCount = rowCount;
        }

        /**
         * (methode de remplacement) {@inheritDoc}
         * 
         * @see fr.gouv.finances.lombok.apptags.table.calc.CalcHandler#processCalcValue(java.lang.Number)
         */
        @Override
        public void processCalcValue(Number calcValue)
        {
            if (calcValue != null)
            {
                total = total.add(new BigDecimal(String.valueOf(calcValue)));
            }
        }

        /**
         * Accesseur de l attribut average value.
         * 
         * @return average value
         */
        public Number getAverageValue()
        {
            if (rowCount > 0)
            {
                return total.divide(new BigDecimal(rowCount), 4, BigDecimal.ROUND_HALF_DOWN);
            }

            return new BigDecimal("0.00");
        }
    }
}
