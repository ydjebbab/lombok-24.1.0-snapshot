/*
/*Copyright (c) 2014 DGFiP - Tous droits réservés/* * Copyright 2004 original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package fr.gouv.finances.lombok.apptags.table.filter;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;

import javax.servlet.ServletOutputStream;
import javax.servlet.WriteListener;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;

/**
 * Class ExportResponseWrapper.
 * refactor de la classe suite au passage servlet 3
 * 
 */
public final class ExportResponseWrapper extends HttpServletResponseWrapper
{

    /** capture. */
    private final ByteArrayOutputStream capture;

    /** output. */
    private ServletOutputStream output;

    /** writer. */
    private PrintWriter writer;

    /** content length. */
    private int contentLength;

    /** content type. */
    private String contentType;

    /**
     * Instanciation de export response wrapper.
     *
     * @param response DOCUMENTEZ_MOI
     */
    public ExportResponseWrapper(HttpServletResponse response)
    {
        super(response);
        capture = new ByteArrayOutputStream(response.getBufferSize());
    }

    /**
     * Accesseur de l attribut data.
     *
     * @return data
     * @throws IOException Signal qu'une execption de type I/O s'est produite.
     */
    public byte[] getData() throws IOException
    {
        return this.getCaptureAsBytes();
    }

    /** 
     * (methode de remplacement)
     * {@inheritDoc}
     * @see javax.servlet.ServletResponseWrapper#getOutputStream()
     */
    @Override
    public ServletOutputStream getOutputStream()
    {
        if (writer != null)
        {
            throw new IllegalStateException(
                "getWriter() has already been called on this response.");
        }

        if (output == null)
        {
            output = new ServletOutputStream()
            {
                @Override
                public void write(int b) throws IOException
                {
                    capture.write(b);
                }

                @Override
                public void flush() throws IOException
                {
                    capture.flush();
                }

                @Override
                public void close() throws IOException
                {
                    capture.close();
                }

                @Override
                public boolean isReady()
                {
                    return false;
                }

                @Override
                public void setWriteListener(WriteListener arg0)
                {
                }
            };
        }

        return output;
    }

    /** 
     * (methode de remplacement)
     * {@inheritDoc}
     * @see javax.servlet.ServletResponseWrapper#getWriter()
     */
    @Override
    public PrintWriter getWriter() throws IOException
    {
        if (output != null)
        {
            throw new IllegalStateException(
                "getOutputStream() has already been called on this response.");
        }

        if (writer == null)
        {
            writer = new PrintWriter(new OutputStreamWriter(capture,
                getCharacterEncoding()));
        }

        return writer;
    }

    /** 
     * (methode de remplacement)
     * {@inheritDoc}
     * @see javax.servlet.ServletResponseWrapper#flushBuffer()
     */
    @Override
    public void flushBuffer() throws IOException
    {
        super.flushBuffer();

        if (writer != null)
        {
            writer.flush();
        }
        else if (output != null)
        {
            output.flush();
        }
    }

    /**
     * Accesseur de l attribut capture as bytes.
     *
     * @return capture as bytes
     * @throws IOException Signal qu'une execption de type I/O s'est produite.
     */
    public byte[] getCaptureAsBytes() throws IOException
    {
        if (writer != null)
        {
            writer.close();
        }
        else if (output != null)
        {
            output.close();
        }

        return capture.toByteArray();
    }

    /**
     * Accesseur de l attribut capture as string.
     *
     * @return capture as string
     * @throws IOException Signal qu'une execption de type I/O s'est produite.
     */
    public String getCaptureAsString() throws IOException
    {
        return new String(getCaptureAsBytes(), getCharacterEncoding());
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see javax.servlet.ServletResponseWrapper#setContentLength(int)
     */
    @Override
    public void setContentLength(int length)
    {
        this.contentLength = length;
        super.setContentLength(length);
    }

    /**
     * Accesseur de l attribut content length.
     *
     * @return content length
     */
    public int getContentLength()
    {
        return contentLength;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see javax.servlet.ServletResponseWrapper#setContentType(java.lang.String)
     */
    @Override
    public void setContentType(String type)
    {
        this.contentType = type;
        super.setContentType(type);
    }

    /** 
     * (methode de remplacement)
     * {@inheritDoc}
     * @see javax.servlet.ServletResponseWrapper#getContentType()
     */
    @Override
    public String getContentType()
    {
        return contentType;
    }

}
