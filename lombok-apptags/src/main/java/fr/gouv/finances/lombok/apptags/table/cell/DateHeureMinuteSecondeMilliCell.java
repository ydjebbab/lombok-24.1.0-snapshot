/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.apptags.table.cell;

import fr.gouv.finances.lombok.apptags.util.format.FormaterDate;

/**
 * Class DateHeureMinuteSecondeMilliCell
 */
public class DateHeureMinuteSecondeMilliCell extends AbstractDateCell
{

    /** Constant : FORMAT_PATTERN. */
    private static final String FORMAT_PATTERN = FormaterDate.F_DA_HE_MI_SE_MIL_CT;

    /**
     * Instanciation de date heure minute seconde milli cell.
     */
    public DateHeureMinuteSecondeMilliCell()
    {
        super();
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.cell.AbstractDateCell#getFormatPattern()
     */
    @Override
    public String getFormatPattern()
    {
        return FORMAT_PATTERN;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.cell.PatternCell#getAlignmentCell()
     */
    @Override
    public int getAlignmentCell()
    {
        return PatternCell.HORIZONTAL_ALIGN_CENTER;
    }
}
