/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : Functions.java
 *
 */
package fr.gouv.finances.lombok.apptags;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

/**
 * Class Functions.
 * 
 * @author wpetit-cp Fonction appelée en interne dans les tags pour contourner les limitations dans le passage de
 *         paramètres Cette classe n'a pas vocation a être appelée directement dans du code applicatif
 * @version $Revision: 1.3 $ Date: 15 déc. 2009
 */
public final class Functions
{

    /** Constant : INPUT_MULTIPLE_PARAM_NB. */
    private static final int INPUT_MULTIPLE_PARAM_NB = 9;

    /** Constant : DEFAULT_MAXLENGTH. */
    private static final Integer DEFAULT_MAXLENGTH = Integer.valueOf("10");

    /** Constant : DEFAULT_SIZE. */
    private static final Integer DEFAULT_SIZE = Integer.valueOf("10");

    /** Constant : DEFAULT_LIBELLE. */
    private static final String DEFAULT_LIBELLE = "";


    /**
     * Constructeur de la classe Functions.java
     */
    private Functions()
    {
    }
    
    /**
     * Construit un tableau définissant le paramétrage d'un tag app:multiple.
     * 
     * @param f1attribut f1attribut
     * @param f1maxlength f1maxlength
     * @param f1size f1size
     * @param f1libelle f1libelle
     * @param f1consigne f1consigne
     * @param f1width f1width
     * @param f1id f1id
     * @param f1err f1err
     * @param f1warn f1warn
     * @param f2attribut f2attribut
     * @param f2maxlength f2maxlength
     * @param f2size f2size
     * @param f2libelle f2libelle
     * @param f2consigne f2consigne
     * @param f2width f2width
     * @param f2id f2id
     * @param f2err f2err
     * @param f2warn f2warn
     * @param f3attribut f3attribut
     * @param f3maxlength f3maxlength
     * @param f3size f3size
     * @param f3libelle f3libelle
     * @param f3consigne f3consigne
     * @param f3width f3width
     * @param f3id f3id
     * @param f3err f3err
     * @param f3warn f3warn
     * @param f4attribut f4attribut
     * @param f4maxlength f4maxlength
     * @param f4size f4size
     * @param f4libelle f4libelle
     * @param f4consigne f4consigne
     * @param f4width f4width
     * @param f4id f4id
     * @param f4err f4err
     * @param f4warn f4warn
     * @param f5attribut f5attribut
     * @param f5maxlength f5maxlength
     * @param f5size f5size
     * @param f5libelle f5libelle
     * @param f5consigne f5consigne
     * @param f5width f5width
     * @param f5id f5id
     * @param f5err f5err
     * @param f5warn f5warn
     * @param f6attribut f6attribut
     * @param f6maxlength f6maxlength
     * @param f6size f6size
     * @param f6libelle f6libelle
     * @param f6consigne f6consigne
     * @param f6width f6width
     * @param f6id f6id
     * @param f6err f6err
     * @param f6warn f6warn
     * @param f7attribut f6warn
     * @param f7maxlength f7maxlength
     * @param f7size f7size
     * @param f7libelle f7libelle
     * @param f7consigne f7consigne
     * @param f7width f7width
     * @param f7id f7id
     * @param f7err f7err
     * @param f7warn f7warn
     * @param f8attribut f8attribut
     * @param f8maxlength f8maxlength
     * @param f8size f8size
     * @param f8libelle f8libelle
     * @param f8consigne f8consigne
     * @param f8width f8width
     * @param f8id f8id
     * @param f8err f8err
     * @param f8warn f8warn
     * @param f9attribut f9attribut
     * @param f9maxlength f9maxlength
     * @param f9size f9size
     * @param f9libelle f9libelle
     * @param f9consigne f9consigne
     * @param f9width f9width
     * @param f9id f9id
     * @param f9err f9err
     * @param f9warn f9warn
     * @param f10attribut f10attribut
     * @param f10maxlength f10maxlength
     * @param f10size f10size
     * @param f10libelle f10libelle
     * @param f10consigne f10consigne
     * @param f10width f10width
     * @param f10id f10id
     * @param f10err f10err
     * @param f10warn f10warn
     * @return the list
     */
    public static List<Object> buildInputMultipleArray(String f1attribut, String f1maxlength, String f1size,
        String f1libelle, String f1consigne, String f1width, String f1id, String f1err, String f1warn,
        String f2attribut, String f2maxlength, String f2size, String f2libelle, String f2consigne, String f2width,
        String f2id, String f2err, String f2warn, String f3attribut, String f3maxlength, String f3size,
        String f3libelle, String f3consigne, String f3width, String f3id, String f3err, String f3warn,
        String f4attribut, String f4maxlength, String f4size, String f4libelle, String f4consigne, String f4width,
        String f4id, String f4err, String f4warn, String f5attribut, String f5maxlength, String f5size,
        String f5libelle, String f5consigne, String f5width, String f5id, String f5err, String f5warn,
        String f6attribut, String f6maxlength, String f6size, String f6libelle, String f6consigne, String f6width,
        String f6id, String f6err, String f6warn, String f7attribut, String f7maxlength, String f7size,
        String f7libelle, String f7consigne, String f7width, String f7id, String f7err, String f7warn,
        String f8attribut, String f8maxlength, String f8size, String f8libelle, String f8consigne, String f8width,
        String f8id, String f8err, String f8warn, String f9attribut, String f9maxlength, String f9size,
        String f9libelle, String f9consigne, String f9width, String f9id, String f9err, String f9warn,
        String f10attribut, String f10maxlength, String f10size, String f10libelle, String f10consigne,
        String f10width, String f10id, String f10err, String f10warn)
    {
        List<Object> elts = new ArrayList<Object>();

        Integer defaultMaxLength = DEFAULT_MAXLENGTH;

        Integer defaultSize = DEFAULT_SIZE;

        String defaultLibelle = DEFAULT_LIBELLE;

        ajoutItemUnElt(f1attribut, f1maxlength, f1size, f1libelle, f1consigne, f1width, f1id, f1err, f1warn, elts,
            defaultMaxLength, defaultSize, defaultLibelle);

        ajoutItemUnElt(f2attribut, f2maxlength, f2size, f2libelle, f2consigne, f2width, f2id, f2err, f2warn, elts,
            defaultMaxLength, defaultSize, defaultLibelle);

        ajoutItemUnElt(f3attribut, f3maxlength, f3size, f3libelle, f3consigne, f3width, f3id, f3err, f3warn, elts,
            defaultMaxLength, defaultSize, defaultLibelle);

        ajoutItemUnElt(f4attribut, f4maxlength, f4size, f4libelle, f4consigne, f4width, f4id, f4err, f4warn, elts,
            defaultMaxLength, defaultSize, defaultLibelle);

        ajoutItemUnElt(f5attribut, f5maxlength, f5size, f5libelle, f5consigne, f5width, f5id, f5err, f5warn, elts,
            defaultMaxLength, defaultSize, defaultLibelle);

        ajoutItemUnElt(f6attribut, f6maxlength, f6size, f6libelle, f6consigne, f6width, f6id, f6err, f6warn, elts,
            defaultMaxLength, defaultSize, defaultLibelle);

        ajoutItemUnElt(f7attribut, f7maxlength, f7size, f7libelle, f7consigne, f7width, f7id, f7err, f7warn, elts,
            defaultMaxLength, defaultSize, defaultLibelle);

        ajoutItemUnElt(f8attribut, f8maxlength, f8size, f8libelle, f8consigne, f8width, f8id, f8err, f8warn, elts,
            defaultMaxLength, defaultSize, defaultLibelle);

        ajoutItemUnElt(f9attribut, f9maxlength, f9size, f9libelle, f9consigne, f9width, f9id, f9err, f9warn, elts,
            defaultMaxLength, defaultSize, defaultLibelle);

        ajoutItemUnElt(f10attribut, f10maxlength, f10size, f10libelle, f10consigne, f10width, f10id, f10err, f10warn,
            elts, defaultMaxLength, defaultSize, defaultLibelle);

        return elts;

    }

    /**
     * methode Hash code : hash code.
     * 
     * @param object object
     * @return string string
     */
    public static String hashCode(Object object)
    {
        String retour = "";
        if (object != null)
        {
            retour = Integer.toString(object.hashCode());
        }
        return retour;
    }

    /**
     * Ajout item un elt.
     * 
     * @param f1attribut f1attribut
     * @param f1maxlength f1maxlength
     * @param f1size f1size
     * @param f1libelle f1libelle
     * @param fconsigne fconsigne
     * @param fwidth fwidth
     * @param idel idel
     * @param err err
     * @param warn warn
     * @param elts elts
     * @param defaultMaxLength defaultMaxLength
     * @param defaultSize defaultSize
     * @param defaultLibelle defaultLibelle
     */
    private static void ajoutItemUnElt(String f1attribut, String f1maxlength, String f1size, String f1libelle,
        String fconsigne, String fwidth, String idel, String err, String warn, List<Object> elts,
        Integer defaultMaxLength, Integer defaultSize, String defaultLibelle)
    {
        if (f1attribut != null && StringUtils.isNotBlank(f1attribut))
        {
            String[] tabParam = new String[INPUT_MULTIPLE_PARAM_NB];

            tabParam[0] = f1attribut;

            Integer maxlength = defaultMaxLength;
            if (f1maxlength != null && StringUtils.isNotBlank(f1maxlength))
            {
                try
                {
                    maxlength = Integer.valueOf(f1maxlength);
                }
                catch (NumberFormatException exception)
                {
                    maxlength = defaultMaxLength;
                }
            }

            tabParam[1] = maxlength.toString();

            Integer size = defaultSize;

            if (f1size != null && StringUtils.isNotBlank(f1size))
            {
                try
                {
                    size = Integer.valueOf(f1maxlength);
                }
                catch (NumberFormatException exception)
                {
                    size = defaultSize;
                }
            }

            tabParam[2] = size.toString();

            String libelle = defaultLibelle;

            if (f1libelle != null && StringUtils.isNotBlank(f1libelle))
            {
                libelle = f1libelle;
            }

            tabParam[3] = libelle;

            tabParam[4] = idel;

            tabParam[5] = err;

            tabParam[6] = warn;

            tabParam[7] = fconsigne;

            tabParam[8] = fwidth;

            elts.add(tabParam);
        }
    }


}
