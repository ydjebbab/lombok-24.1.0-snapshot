/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.apptags.table.cell;

/**
 * Class NombreCellSeptDecimales
 */
public class NombreCellSeptDecimales extends AbstractNombreCell
{

    /** Constant : PRECISION. */
    private static final int PRECISION = 7;

    /** Constant : FORMAT_PATTERN. */
    private static final String FORMAT_PATTERN = "#,##0.0000000";

    /**
     * Instanciation de nombre cell sept decimales.
     */
    public NombreCellSeptDecimales()
    {
        super();
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.cell.AbstractNombreCell#getPrecision()
     */
    @Override
    public int getPrecision()
    {
        return PRECISION;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.cell.PatternCell#getFormatPattern()
     */
    @Override
    public String getFormatPattern()
    {
        return FORMAT_PATTERN;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.cell.PatternCell#getAlignmentCell()
     */
    @Override
    public int getAlignmentCell()
    {
        return PatternCell.HORIZONTAL_ALIGN_RIGHT;
    }
}
