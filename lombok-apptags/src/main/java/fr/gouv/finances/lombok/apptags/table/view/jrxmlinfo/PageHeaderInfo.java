/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.apptags.table.view.jrxmlinfo;

/**
 * Class PageHeaderInfo
 */
public class PageHeaderInfo extends AbstractBandInfo
{

    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** rectangle11. */
    private RectangleInfo rectangle11;

    /** rectangle12. */
    private RectangleInfo rectangle12;

    /** rectangle13. */
    private RectangleInfo rectangle13;

    /** rectangle21. */
    private RectangleInfo rectangle21;

    /** use application logo. */
    private Boolean useApplicationLogo;

    /** logo application. */
    private LogoInfo logoApplication;

    /** top padding. */
    private int topPadding;

    /** bottom padding. */
    private int bottomPadding;

    /** left padding. */
    private int leftPadding;

    /** right padding. */
    private int rightPadding;

    /** title style. */
    private FontStyleInfo titleStyle;

    /** date style. */
    private FontStyleInfo dateStyle;

    /** texte complement style. */
    private FontStyleInfo texteComplementStyle;

    /**
     * Instanciation de page header info.
     */
    public PageHeaderInfo()
    {
        super();
        this.rectangle11 = new RectangleInfo();
        this.rectangle12 = new RectangleInfo();
        this.rectangle13 = new RectangleInfo();
        this.rectangle21 = new RectangleInfo();
    }

    /**
     * Accesseur de l attribut use application logo.
     * 
     * @return use application logo
     */
    public Boolean getUseApplicationLogo()
    {
        return useApplicationLogo;
    }

    /**
     * Modificateur de l attribut use application logo.
     * 
     * @param useApplicationLogo le nouveau use application logo
     */
    public void setUseApplicationLogo(Boolean useApplicationLogo)
    {
        this.useApplicationLogo = useApplicationLogo;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.view.jrxmlinfo.AbstractBandInfo#getBottomPadding()
     */
    @Override
    public int getBottomPadding()
    {
        return bottomPadding;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.view.jrxmlinfo.AbstractBandInfo#setBottomPadding(int)
     */
    @Override
    public void setBottomPadding(int bottomPadding)
    {
        this.bottomPadding = bottomPadding;
    }

    /**
     * Accesseur de l attribut date style.
     * 
     * @return date style
     */
    public FontStyleInfo getDateStyle()
    {
        return dateStyle;
    }

    /**
     * Modificateur de l attribut date style.
     * 
     * @param dateStyle le nouveau date style
     */
    public void setDateStyle(FontStyleInfo dateStyle)
    {
        this.dateStyle = dateStyle;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.view.jrxmlinfo.AbstractBandInfo#getLeftPadding()
     */
    @Override
    public int getLeftPadding()
    {
        return leftPadding;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.view.jrxmlinfo.AbstractBandInfo#setLeftPadding(int)
     */
    @Override
    public void setLeftPadding(int leftPadding)
    {
        this.leftPadding = leftPadding;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.view.jrxmlinfo.AbstractBandInfo#getRightPadding()
     */
    @Override
    public int getRightPadding()
    {
        return rightPadding;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.view.jrxmlinfo.AbstractBandInfo#setRightPadding(int)
     */
    @Override
    public void setRightPadding(int rightPadding)
    {
        this.rightPadding = rightPadding;
    }

    /**
     * Accesseur de l attribut texte complement style.
     * 
     * @return texte complement style
     */
    public FontStyleInfo getTexteComplementStyle()
    {
        return texteComplementStyle;
    }

    /**
     * Modificateur de l attribut texte complement style.
     * 
     * @param texteComplementStyle le nouveau texte complement style
     */
    public void setTexteComplementStyle(FontStyleInfo texteComplementStyle)
    {
        this.texteComplementStyle = texteComplementStyle;
    }

    /**
     * Accesseur de l attribut title style.
     * 
     * @return title style
     */
    public FontStyleInfo getTitleStyle()
    {
        return titleStyle;
    }

    /**
     * Modificateur de l attribut title style.
     * 
     * @param titleStyle le nouveau title style
     */
    public void setTitleStyle(FontStyleInfo titleStyle)
    {
        this.titleStyle = titleStyle;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.view.jrxmlinfo.AbstractBandInfo#getTopPadding()
     */
    @Override
    public int getTopPadding()
    {
        return topPadding;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.view.jrxmlinfo.AbstractBandInfo#setTopPadding(int)
     */
    @Override
    public void setTopPadding(int topPadding)
    {
        this.topPadding = topPadding;
    }

    /**
     * Accesseur de l attribut logo application.
     * 
     * @return logo application
     */
    public LogoInfo getLogoApplication()
    {
        return logoApplication;
    }

    /**
     * Modificateur de l attribut logo application.
     * 
     * @param logoApplication le nouveau logo application
     */
    public void setLogoApplication(LogoInfo logoApplication)
    {
        this.logoApplication = logoApplication;
    }

    /**
     * Accesseur de l attribut rectangle11.
     * 
     * @return rectangle11
     */
    public RectangleInfo getRectangle11()
    {
        return rectangle11;
    }

    /**
     * Modificateur de l attribut rectangle11.
     * 
     * @param rectangle11 le nouveau rectangle11
     */
    public void setRectangle11(RectangleInfo rectangle11)
    {
        this.rectangle11 = rectangle11;
    }

    /**
     * Accesseur de l attribut rectangle12.
     * 
     * @return rectangle12
     */
    public RectangleInfo getRectangle12()
    {
        return rectangle12;
    }

    /**
     * Modificateur de l attribut rectangle12.
     * 
     * @param rectangle12 le nouveau rectangle12
     */
    public void setRectangle12(RectangleInfo rectangle12)
    {
        this.rectangle12 = rectangle12;
    }

    /**
     * Accesseur de l attribut rectangle13.
     * 
     * @return rectangle13
     */
    public RectangleInfo getRectangle13()
    {
        return rectangle13;
    }

    /**
     * Modificateur de l attribut rectangle13.
     * 
     * @param rectangle13 le nouveau rectangle13
     */
    public void setRectangle13(RectangleInfo rectangle13)
    {
        this.rectangle13 = rectangle13;
    }

    /**
     * Accesseur de l attribut rectangle21.
     * 
     * @return rectangle21
     */
    public RectangleInfo getRectangle21()
    {
        return rectangle21;
    }

    /**
     * Modificateur de l attribut rectangle21.
     * 
     * @param rectangle21 le nouveau rectangle21
     */
    public void setRectangle21(RectangleInfo rectangle21)
    {
        this.rectangle21 = rectangle21;
    }

    /**
     * methode Somme taille ligne1 rectangles :
     * 
     * @return int
     */
    public int sommeTailleLigne1Rectangles()
    {
        return this.getRectangle11().getWidth() + this.getRectangle12().getWidth() + this.getRectangle13().getWidth();
    }

    /**
     * methode Prints the info width first line rectangles :
     * 
     * @return string
     */
    public String printInfoWidthFirstLineRectangles()
    {
        StringBuilder msg = new StringBuilder(100);
        msg.append("Largeur de la première rangée de rectangles  de l'entête : \n");
        msg.append(" (");
        msg.append(this.getRectangle11().getWidth());
        msg.append(" + ");
        msg.append(this.getRectangle12().getWidth());
        msg.append(" + ");
        msg.append(this.getRectangle13().getWidth());
        msg.append(" = ");
        msg.append(this.sommeTailleLigne1Rectangles());
        msg.append(" )");

        return msg.toString();
    }
}
