/*
/*Copyright (c) 2014 DGFiP - Tous droits réservés/* * Copyright 2004 original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package fr.gouv.finances.lombok.apptags.table.bean;

import org.apache.commons.lang.StringUtils;

import fr.gouv.finances.lombok.apptags.table.core.MessagesConstants;
import fr.gouv.finances.lombok.apptags.table.core.PreferencesConstants;
import fr.gouv.finances.lombok.apptags.table.core.TableConstants;
import fr.gouv.finances.lombok.apptags.table.core.TableModel;
import fr.gouv.finances.lombok.apptags.table.core.TableModelUtils;
import fr.gouv.finances.lombok.apptags.table.view.html.BuilderConstants;

/**
 * Pull the complicated values for the TableTag. Because the default values could be coming from the properties or
 * resource bundle this class will abstract that out.
 */
final class TableDefaults
{

    /**
     * Accesseur de l attribut auto include parameters.
     *
     * @param model model
     * @param autoIncludeParameters autoIncludeParameters
     * @return auto include parameters
     */
    static Boolean getAutoIncludeParameters(TableModel model, Boolean autoIncludeParameters)
    {
        if (autoIncludeParameters == null)
        {
            return Boolean.valueOf(model.getPreferences().getPreference(
                PreferencesConstants.TABLE_AUTO_INCLUDE_PARAMETERS));
        }

        return autoIncludeParameters;
    }

    /**
     * Accesseur de l attribut border.
     *
     * @param model model
     * @param border border
     * @return border
     */
    static String getBorder(TableModel model, String border)
    {
        if (StringUtils.isBlank(border))
        {
            return model.getPreferences().getPreference(PreferencesConstants.TABLE_BORDER);
        }

        return border;
    }

    /**
     * Accesseur de l attribut cellpadding.
     *
     * @param model model
     * @param cellpadding cellpadding
     * @return cellpadding
     */
    static String getCellpadding(TableModel model, String cellpadding)
    {
        if (StringUtils.isBlank(cellpadding))
        {
            return model.getPreferences().getPreference(PreferencesConstants.TABLE_CELLPADDING);
        }

        return cellpadding;
    }

    /**
     * Accesseur de l attribut cellspacing.
     *
     * @param model model
     * @param cellspacing cellspacing
     * @return cellspacing
     */
    static String getCellspacing(TableModel model, String cellspacing)
    {
        if (StringUtils.isBlank(cellspacing))
        {
            return model.getPreferences().getPreference(PreferencesConstants.TABLE_CELLSPACING);
        }

        return cellspacing;
    }

    /**
     * Accesseur de l attribut dataAttributes.
     *
     * @return dataAttributes
     */
    static String getDataAttributes(TableModel model, String dataAttributes)
    {
        if (dataAttributes == null)
        {
            return model.getPreferences().getPreference(PreferencesConstants.TABLE_DATA_ATTRIBUTES);
        }
        return dataAttributes;
    }

    /**
     * Accesseur de l attribut exportsClass.
     *
     * @return exportsClass
     */
    static String getExportsClass(TableModel model, String exportsClass)
    {
        if (exportsClass == null)
        {
            return model.getPreferences().getPreference(PreferencesConstants.TABLE_EXPORTS_CLASS);
        }
        return exportsClass;
    }

    /**
     * Accesseur de l attribut filter rows callback.
     *
     * @param model model
     * @param filterRowsCallback filterRowsCallback
     * @return filter rows callback
     */
    static String getFilterRowsCallback(TableModel model, String filterRowsCallback)
    {
        String result;

        if (StringUtils.isNotBlank(filterRowsCallback))
        {
            result =
                model.getPreferences().getPreference(
                    PreferencesConstants.TABLE_FILTER_ROWS_CALLBACK + filterRowsCallback);
            if (StringUtils.isBlank(result))
            {
                result = filterRowsCallback;
            }
        }
        else
        {
            result =
                model.getPreferences().getPreference(
                    PreferencesConstants.TABLE_FILTER_ROWS_CALLBACK + TableConstants.CALLBACK_DEFAULT);
        }

        return result;
    }

    /**
     * If the context path is not specified then put one in. However it will only provide a context if the path starts
     * with a slash to signal absolute and the contextPath is not already put in.
     *
     * @param model model
     * @param imagePath imagePath
     * @return image path
     */
    static String getImagePath(TableModel model, String imagePath)
    {
        if (StringUtils.isNotBlank(imagePath))
        {
            return imagePath;
        }

        String contextPath = model.getContext().getContextPath();

        String results = retrieveImagePath(model);

        if (results != null && results.startsWith("/") && !results.startsWith(contextPath))
        {
            return contextPath + results;
        }

        return results;
    }

    /**
     * Accesseur de l attribut libellenoresultsfound.
     *
     * @param model model
     * @param libellenoresultsfound libellenoresultsfound
     * @return libellenoresultsfound
     */
    static String getLibellenoresultsfound(TableModel model, String libellenoresultsfound)
    {
        if (StringUtils.isEmpty(libellenoresultsfound))
        {
            return model.getMessages().getMessage(BuilderConstants.STATUSBAR_NO_RESULTS_FOUND);
        }
        return libellenoresultsfound;
    }

    /**
     * Accesseur de l attribut max rows displayed.
     *
     * @param model model
     * @param maxRowsDisplayed maxRowsDisplayed
     * @return max rows displayed
     */
    static int getMaxRowsDisplayed(TableModel model, int maxRowsDisplayed)
    {
        if (maxRowsDisplayed == 0)
        {
            return Integer.valueOf(model.getPreferences().getPreference(PreferencesConstants.TABLE_MAX_ROWS_DISPLAYED))
                .intValue();
        }
        return maxRowsDisplayed;
    }

    /**
     * Accesseur de l attribut median rows displayed.
     *
     * @param model model
     * @param medianRowsDisplayed medianRowsDisplayed
     * @return median rows displayed
     */
    static int getMedianRowsDisplayed(TableModel model, int medianRowsDisplayed)
    {
        if (medianRowsDisplayed == 0)
        {
            return Integer.valueOf(model.getPreferences().getPreference(PreferencesConstants.TABLE_MEDIAN_ROWS_DISPLAYED))
                .intValue();
        }
        return medianRowsDisplayed;
    }

    /**
     * Accesseur de l attribut method.
     *
     * @param model model
     * @param method method
     * @return method
     */
    static String getMethod(TableModel model, String method)
    {
        if (StringUtils.isBlank(method))
        {
            return model.getPreferences().getPreference(PreferencesConstants.TABLE_METHOD);
        }

        return method;
    }

    /**
     * Accesseur de l attribut new pagination nb page.
     *
     * @param model model
     * @param showNewPaginationNbPage showNewPaginationNbPage
     * @return new pagination nb page
     */
    static int getNewPaginationNbPage(TableModel model, int showNewPaginationNbPage)
    {
        if (showNewPaginationNbPage == 0)
        {
            return Integer.valueOf(model.getPreferences().getPreference(PreferencesConstants.TABLE_NEW_PAGINATION_NB_PAGE))
                .intValue();
        }

        return showNewPaginationNbPage;
    }

    /**
     * Accesseur de l attribut paginationClass.
     *
     * @return paginationClass
     */
    static String getPaginationClass(TableModel model, String paginationClass)
    {
        if (paginationClass == null)
        {
            return model.getPreferences().getPreference(PreferencesConstants.TABLE_PAGINATION_CLASS);
        }
        return paginationClass;
    }

    /**
     * Accesseur de l attribut positionnoresultsfound.
     *
     * @param model model
     * @param positionnoresultsfound positionnoresultsfound
     * @return positionnoresultsfound
     */
    static String getPositionnoresultsfound(TableModel model, String positionnoresultsfound)
    {
        if (StringUtils.isEmpty(positionnoresultsfound))
        {
            return model.getPreferences().getPreference(PreferencesConstants.TABLE_SHOW_STATUS_BAR);
        }
        return positionnoresultsfound;
    }

    /**
     * Accesseur de l attribut retrieve rows callback.
     *
     * @param model model
     * @param retrieveRowsCallback retrieveRowsCallback
     * @return retrieve rows callback
     */
    static String getRetrieveRowsCallback(TableModel model, String retrieveRowsCallback)
    {
        String result;

        if (StringUtils.isNotBlank(retrieveRowsCallback))
        {
            result =
                model.getPreferences().getPreference(
                    PreferencesConstants.TABLE_RETRIEVE_ROWS_CALLBACK + retrieveRowsCallback);
            if (StringUtils.isBlank(result))
            {
                result = retrieveRowsCallback;
            }
        }
        else
        {
            result =
                model.getPreferences().getPreference(
                    PreferencesConstants.TABLE_RETRIEVE_ROWS_CALLBACK + TableConstants.CALLBACK_DEFAULT);
        }

        return result;
    }

    /**
     * Accesseur de l attribut rows displayed.
     *
     * @param model model
     * @param rowsDisplayed rowsDisplayed
     * @return rows displayed
     */
    static int getRowsDisplayed(TableModel model, int rowsDisplayed)
    {
        if (rowsDisplayed == 0)
        {
            return Integer.valueOf(model.getPreferences().getPreference(PreferencesConstants.TABLE_ROWS_DISPLAYED))
                .intValue();
        }

        return rowsDisplayed;
    }

    /**
     * Accesseur de l attribut sort rows callback.
     *
     * @param model model
     * @param sortRowsCallback sortRowsCallback
     * @return sort rows callback
     */
    static String getSortRowsCallback(TableModel model, String sortRowsCallback)
    {
        String result;

        if (StringUtils.isNotBlank(sortRowsCallback))
        {
            result =
                model.getPreferences().getPreference(PreferencesConstants.TABLE_SORT_ROWS_CALLBACK + sortRowsCallback);
            if (StringUtils.isBlank(result))
            {
                result = sortRowsCallback;
            }
        }
        else
        {
            result =
                model.getPreferences().getPreference(
                    PreferencesConstants.TABLE_SORT_ROWS_CALLBACK + TableConstants.CALLBACK_DEFAULT);
        }

        return result;
    }

    /**
     * Accesseur de l attribut startpage.
     *
     * @param model model
     * @param startpage startpage
     * @return startpage
     */
    static int getStartpage(TableModel model, int startpage)
    {
        int result = startpage;
        if (startpage == 0)
        {
            result = 1;
        }

        return result;
    }

    /**
     * Accesseur de l attribut state.
     *
     * @param model model
     * @param state state
     * @return state
     */
    static String getState(TableModel model, String state)
    {
        String result;

        if (StringUtils.isNotBlank(state))
        {
            result = model.getPreferences().getPreference(PreferencesConstants.TABLE_STATE + state);
            if (StringUtils.isBlank(result))
            {
                result = state;
            }
        }
        else
        {
            result =
                model.getPreferences().getPreference(PreferencesConstants.TABLE_STATE + TableConstants.STATE_DEFAULT);
        }

        return result;
    }

    /**
     * Accesseur de l attribut state attr.
     *
     * @param model model
     * @param stateAttr stateAttr
     * @return state attr
     */
    static String getStateAttr(TableModel model, String stateAttr)
    {
        if (StringUtils.isBlank(stateAttr))
        {
            return model.getPreferences().getPreference(PreferencesConstants.TABLE_STATE_ATTR);
        }

        return stateAttr;
    }

    /**
     * Accesseur de l attribut style class.
     *
     * @param model model
     * @param styleClass styleClass
     * @return style class
     */
    static String getStyleClass(TableModel model, String styleClass)
    {
        if (StringUtils.isBlank(styleClass))
        {
            return model.getPreferences().getPreference(PreferencesConstants.TABLE_STYLE_CLASS);
        }

        return styleClass;
    }

    /**
     * Accesseur de l attribut table id.
     *
     * @param tableId tableId
     * @return table id
     */
    static String getTableId(String tableId)
    {
        String rResult = TableConstants.EXTREME_COMPONENTS;

        if (StringUtils.isNotEmpty(tableId))
        {
            rResult = tableId;
        }

        return rResult;
    }

    /**
     * Accesseur de l attribut theme.
     *
     * @param model model
     * @param theme theme
     * @return theme
     */
    static String getTheme(TableModel model, String theme)
    {
        if (StringUtils.isEmpty(theme))
        {
            return model.getPreferences().getPreference(PreferencesConstants.TABLE_THEME);
        }

        return theme;
    }

    /**
     * Accesseur de l attribut title.
     *
     * @param model model
     * @param title title
     * @return title
     */
    static String getTitle(TableModel model, String title)
    {
        if (TableModelUtils.isResourceBundleProperty(title))
        {
            String resourceValue = model.getMessages().getMessage(title);
            if (resourceValue != null)
            {
                return resourceValue;
            }
        }

        return title;
    }

    /**
     * Accesseur de l attribut var.
     *
     * @param var var
     * @param tableId tableId
     * @return var
     */
    static String getVar(String var, String tableId)
    {
        String rResult = tableId;

        if (StringUtils.isNotEmpty(var))
        {
            rResult = var;
        }

        return rResult;
    }

    /**
     * Accesseur de l attribut view.
     *
     * @param view view
     * @return view
     */
    static String getView(String view)
    {
        if (StringUtils.isBlank(view))
        {
            return TableConstants.VIEW_HTML;
        }

        return view;
    }

    /**
     * Accesseur de l attribut width.
     *
     * @param model model
     * @param width width
     * @return width
     */
    static String getWidth(TableModel model, String width)
    {
        if (StringUtils.isBlank(width))
        {
            return model.getPreferences().getPreference(PreferencesConstants.TABLE_WIDTH);
        }

        return width;
    }

    /**
     * methode Checks if is buffer view :
     *
     * @param model model
     * @param bufferView bufferView
     * @return boolean
     */
    static Boolean isBufferView(TableModel model, Boolean bufferView)
    {
        if (bufferView == null)
        {
            return Boolean.valueOf(model.getPreferences().getPreference(PreferencesConstants.TABLE_BUFFER_VIEW));
        }

        return bufferView;
    }

    /**
     * methode Checks if is compact :
     *
     * @param model model
     * @param compact compact
     * @return boolean
     */
    static Boolean isCompact(TableModel model, Boolean compact)
    {
        if (compact == null)
        {
            return Boolean.valueOf(model.getPreferences().getPreference(PreferencesConstants.TABLE_COMPACT));
        }

        return compact;
    }

    /**
     * methode Checks if is filterable :
     *
     * @param model model
     * @param filterable filterable
     * @return boolean
     */
    static Boolean isFilterable(TableModel model, Boolean filterable)
    {
        if (filterable == null)
        {
            return Boolean.valueOf(model.getPreferences().getPreference(PreferencesConstants.TABLE_FILTERABLE));
        }

        return filterable;
    }

    /**
     * methode Checks if is show exports :
     *
     * @param model model
     * @param showExports showExports
     * @return boolean
     */
    static Boolean isShowExports(TableModel model, Boolean showExports)
    {
        if (showExports == null)
        {
            return Boolean.valueOf(model.getPreferences().getPreference(PreferencesConstants.TABLE_SHOW_EXPORTS));
        }

        return showExports;
    }

    /**
     * methode Checks if is show exports bottom :
     *
     * @param model model
     * @param showExportsbBottom showExportsbBottom
     * @return boolean
     */
    static Boolean isShowExportsBottom(TableModel model, Boolean showExportsbBottom)
    {
        if (showExportsbBottom == null)
        {
            return Boolean.valueOf(model.getPreferences().getPreference(PreferencesConstants.TABLE_SHOW_EXPORTS_BOTTOM));
        }

        return showExportsbBottom;
    }

    /**
     * methode Checks if is show exports with limit size csv :
     *
     * @param model model
     * @param showExportsWithLimitSizeCsv showExportsWithLimitSizeCsv
     * @return int
     */
    static int isShowExportsWithLimitSizeCsv(TableModel model, int showExportsWithLimitSizeCsv)
    {
        if (showExportsWithLimitSizeCsv == 0)
        {
            return Integer.valueOf(model.getPreferences().getPreference(PreferencesConstants.TABLE_SHOW_EXPORTS_WITH_LIMIT_SIZE_CSV))
                .intValue();
        }

        return showExportsWithLimitSizeCsv;
    }

    /**
     * methode Checks if is show exports with limit size pdf :
     *
     * @param model model
     * @param showExportsWithLimitSizePdf showExportsWithLimitSizePdf
     * @return int
     */
    static int isShowExportsWithLimitSizePdf(TableModel model, int showExportsWithLimitSizePdf)
    {
        if (showExportsWithLimitSizePdf == 0)
        {
            return Integer.valueOf(model.getPreferences().getPreference(PreferencesConstants.TABLE_SHOW_EXPORTS_WITH_LIMIT_SIZE_PDF))
                .intValue();
        }

        return showExportsWithLimitSizePdf;
    }

    /**
     * methode Checks if is show exports with limit size xls or ods :
     *
     * @param model model
     * @param showExportsWithLimitSizeXlsOrOds showExportsWithLimitSizeXlsOrOds
     * @return int
     */
    static int isShowExportsWithLimitSizeXlsOrOds(TableModel model, int showExportsWithLimitSizeXlsOrOds)
    {
        if (showExportsWithLimitSizeXlsOrOds == 0)
        {
            return Integer.valueOf(model.getPreferences().getPreference(PreferencesConstants.TABLE_SHOW_EXPORTS_WITH_LIMIT_SIZE_XLS_ODS));
        }

        return showExportsWithLimitSizeXlsOrOds;
    }

    /**
     * methode Checks if is show new filter :
     *
     * @param model model
     * @param showNewFilter showNewFilter
     * @return boolean
     */
    static Boolean isShowNewFilter(TableModel model, Boolean showNewFilter)
    {
        if (showNewFilter == null)
        {
            return Boolean.valueOf(model.getPreferences().getPreference(PreferencesConstants.TABLE_SHOW_NEW_FILTER));
        }

        return showNewFilter;
    }

    /**
     * methode Checks if is show new pagination :
     *
     * @param model model
     * @param showNewPagination showNewPagination
     * @return boolean
     */
    static Boolean isShowNewPagination(TableModel model, Boolean showNewPagination)
    {
        if (showNewPagination == null)
        {
            return Boolean.valueOf(model.getPreferences().getPreference(PreferencesConstants.TABLE_SHOW_NEW_PAGINATION));
        }

        return showNewPagination;
    }

    /**
     * methode Checks if is show pagination :
     *
     * @param model model
     * @param showPagination showPagination
     * @return boolean
     */
    static Boolean isShowPagination(TableModel model, Boolean showPagination)
    {
        if (showPagination == null)
        {
            return Boolean.valueOf(model.getPreferences().getPreference(PreferencesConstants.TABLE_SHOW_PAGINATION));
        }

        return showPagination;
    }

    /**
     * methode Checks if is show pagination bottom :
     *
     * @param model model
     * @param showPaginationbBottom showPaginationbBottom
     * @return boolean
     */
    static Boolean isShowPaginationBottom(TableModel model, Boolean showPaginationbBottom)
    {
        if (showPaginationbBottom == null)
        {
            return Boolean.valueOf(model.getPreferences().getPreference(PreferencesConstants.TABLE_SHOW_PAGINATION_BOTTOM));
        }

        return showPaginationbBottom;
    }

    /**
     * Accesseur de l attribut responsive.
     *
     * @return responsive (true or false)
     */
    static Boolean isShowResponsive(TableModel model, Boolean showResponsive)
    {
        if (showResponsive == null)
        {
            return Boolean.valueOf(model.getPreferences().getPreference(PreferencesConstants.TABLE_SHOW_RESPONSIVE));
        }
        return showResponsive;
    }

    /**
     * methode Checks if is show status bar :
     *
     * @param model model
     * @param showStatusBar showStatusBar
     * @return boolean
     */
    static Boolean isShowStatusBar(TableModel model, Boolean showStatusBar)
    {
        if (showStatusBar == null)
        {
            return Boolean.valueOf(model.getPreferences().getPreference(PreferencesConstants.TABLE_SHOW_STATUS_BAR));
        }

        return showStatusBar;
    }

    /**
     * methode Checks if is show title :
     *
     * @param model model
     * @param showTitle showTitle
     * @return boolean
     */
    static Boolean isShowTitle(TableModel model, Boolean showTitle)
    {
        if (showTitle == null)
        {
            return Boolean.valueOf(model.getPreferences().getPreference(PreferencesConstants.TABLE_SHOW_TITLE));
        }

        return showTitle;
    }

    /**
     * methode Checks if is show tooltips :
     *
     * @param model model
     * @param showTooltips showTooltips
     * @return boolean
     */
    static Boolean isShowTooltips(TableModel model, Boolean showTooltips)
    {
        if (showTooltips == null)
        {
            return Boolean.valueOf(model.getPreferences().getPreference(PreferencesConstants.TABLE_SHOW_TOOLTIPS));
        }

        return showTooltips;
    }

    /**
     * methode Checks if is sortable :
     *
     * @param model model
     * @param sortable sortable
     * @return boolean
     */
    static Boolean isSortable(TableModel model, Boolean sortable)
    {
        if (sortable == null)
        {
            return Boolean.valueOf(model.getPreferences().getPreference(PreferencesConstants.TABLE_SORTABLE));
        }

        return sortable;
    }

    /**
     * methode Retrieve image path :
     *
     * @param model model
     * @return string
     */
    static String retrieveImagePath(TableModel model)
    {
        String resourceValue = model.getMessages().getMessage(MessagesConstants.TABLE_IMAGE_PATH);
        if (resourceValue != null)
        {
            return resourceValue;
        }

        return model.getPreferences().getPreference(PreferencesConstants.TABLE_IMAGE_PATH);
    }
}
