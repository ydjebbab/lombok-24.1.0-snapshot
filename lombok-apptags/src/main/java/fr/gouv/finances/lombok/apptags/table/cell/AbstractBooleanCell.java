/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.apptags.table.cell;

import fr.gouv.finances.lombok.apptags.table.bean.Column;
import fr.gouv.finances.lombok.apptags.table.core.TableModel;
import fr.gouv.finances.lombok.apptags.util.format.BooleanFormat.TypeFormat;
import fr.gouv.finances.lombok.apptags.util.format.FormaterBoolean;

/**
 * Class AbstractBooleanCell
 */
public abstract class AbstractBooleanCell extends AbstractCell
{

    /**
     * Instanciation de abstract boolean cell.
     */
    public AbstractBooleanCell()
    {
        super();
    }

    /**
     * Accesseur de l attribut format.
     * 
     * @return format
     */
    public abstract TypeFormat getFormat();

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.cell.AbstractCell#getExportDisplay(fr.gouv.finances.lombok.apptags.table.core.TableModel,
     *      fr.gouv.finances.lombok.apptags.table.bean.Column)
     */
    @Override
    public String getExportDisplay(TableModel model, Column column)
    {
        return FormaterBoolean.getFormat(getFormat()).format(column.getPropertyValue());
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.cell.AbstractCell#getCellValue(fr.gouv.finances.lombok.apptags.table.core.TableModel,
     *      fr.gouv.finances.lombok.apptags.table.bean.Column)
     */
    @Override
    protected String getCellValue(TableModel model, Column column)
    {
        return FormaterBoolean.getFormat(getFormat()).format(column.getPropertyValue());
    }

    /**
     * Accesseur de l attribut length cellule formatee.
     * 
     * @param model
     * @param column
     * @return length cellule formatee
     */
    public int getLengthCelluleFormatee(TableModel model, Column column)
    {
        return (column.getPropertyValue() != null ? getCellValue(model, column).length() : 0);
    }
}
