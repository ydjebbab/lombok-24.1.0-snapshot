/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.apptags.table.view.jrxmlinfo;

import java.util.ArrayList;
import java.util.List;

/**
 * Class DetailInfo
 */
public class DetailInfo extends AbstractBandInfo
{

    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** cell margin. */
    private int cellMargin;

    /** colonnes. */
    private List<ColonneInfo> colonnes = new ArrayList<>();

    /**
     * Instanciation de detail info.
     */
    public DetailInfo()
    {
        super();
    }

    /**
     * Accesseur de l attribut colonnes.
     * 
     * @return the colonnes
     */
    public List<ColonneInfo> getColonnes()
    {
        return colonnes;
    }

    /**
     * Modificateur de l attribut colonnes.
     * 
     * @param colonnes the colonnes to set
     */
    public void setColonnes(List<ColonneInfo> colonnes)
    {
        this.colonnes = colonnes;
    }

    /**
     * Accesseur de l attribut cell margin.
     * 
     * @return cell margin
     */
    public int getCellMargin()
    {
        return cellMargin;
    }

    /**
     * Modificateur de l attribut cell margin.
     * 
     * @param cellMargin le nouveau cell margin
     */
    public void setCellMargin(int cellMargin)
    {
        this.cellMargin = cellMargin;
    }

}
