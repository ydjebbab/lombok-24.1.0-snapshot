/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.apptags.table.cell;

import fr.gouv.finances.lombok.apptags.util.format.BooleanFormat.TypeFormat;

/**
 * Class BooleanCell10
 */
public class BooleanCell10 extends AbstractBooleanCell
{

    /**
     * Instanciation de boolean cell10.
     */
    public BooleanCell10()
    {
        super();
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.cell.AbstractBooleanCell#getFormat()
     */
    @Override
    public TypeFormat getFormat()
    {
        return TypeFormat.UN_ZERO;
    }
}
