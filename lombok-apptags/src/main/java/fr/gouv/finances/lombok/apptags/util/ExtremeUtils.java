/*
/*Copyright (c) 2014 DGFiP - Tous droits réservés/* * Copyright 2004 original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package fr.gouv.finances.lombok.apptags.util;

import java.io.ByteArrayOutputStream;
import java.io.ObjectOutputStream;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.PageContext;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateFormatUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Class ExtremeUtils
 */
public final class ExtremeUtils
{

    /** logger. */
    private static Log logger = LogFactory.getLog(ExtremeUtils.class);

    /**
     * Instanciation de extreme utils.
     */
    private ExtremeUtils()
    {
    }

    /**
     * Convertit un texte au format 'camelCase' en une phrase Exemple : CompteDeDebit --> Compte De Debit.
     * 
     * @param camelCaseText
     * @return string
     */
    public static String camelCaseToWord(String camelCaseText)
    {
        String build;
        if (StringUtils.isEmpty(camelCaseText))
        {
            build = camelCaseText;
        }

        else if (camelCaseText.equals(camelCaseText.toUpperCase(Locale.getDefault())))
        {
            build = camelCaseText;
        }

        else
        {
            char[] ch = camelCaseText.toCharArray();
            String first = "" + ch[0];
            build = first.toUpperCase(Locale.getDefault());

            for (int i = 1; i < ch.length; i++)
            {
                String test = "" + ch[i];

                if (test.equals(test.toUpperCase(Locale.getDefault())))
                {
                    StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.append(build);
                    stringBuilder.append(" ");
                    build = stringBuilder.toString();
                }

                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append(build);
                stringBuilder.append(test);
                build = stringBuilder.toString();
            }
        }
        return build;
    }

    /**
     * methode Format date :
     * 
     * @param parse
     * @param format
     * @param value
     * @return string
     */
    public static String formatDate(String parse, String format, Object value)
    {
        return formatDate(parse, format, value, Locale.getDefault());
    }

    /**
     * methode Format date :
     * 
     * @param parse
     * @param format
     * @param value
     * @param locale
     * @return string
     */
    public static String formatDate(String parse, String format, Object value, Locale locale)
    {
        String result = null;

        if (value == null)
        {
        }
        else if (StringUtils.isBlank(format))
        {
            String valueAsString = value.toString();
            logger.error("Aucun format n'est définit pour la date [" + valueAsString + "].");
            result = valueAsString;
        }
        else
        {
            Date date = null;
            if (value instanceof Date)
            {
                date = (Date) value;
                result = DateFormatUtils.format(date, format, locale);
            }
            else
            {
                String valueAsString = value.toString();

                if (StringUtils.isBlank(valueAsString))
                {
                    result = valueAsString;
                }

                else if (StringUtils.isBlank(parse))
                {
                    logger.error("The parse was not defined for date String [" + valueAsString + "].");
                    result = valueAsString;
                }
                else
                {
                    try
                    {
                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(parse, locale);
                        date = simpleDateFormat.parse(valueAsString);
                    }
                    catch (Exception exc)
                    {
                        logger.error("Ce format ne s'applique pas à cette date [" + valueAsString + "].");

                    }
                    finally
                    {
                        result = valueAsString;
                    }
                }
            }
        }

        return result;
    }

    /**
     * methode Format number :
     * 
     * @param format
     * @param value
     * @return string
     */
    public static String formatNumber(String format, Object value)
    {
        return formatNumber(format, value, Locale.getDefault());
    }

    /**
     * methode Format number :
     * 
     * @param format
     * @param value
     * @param locale
     * @return string
     */
    public static String formatNumber(String format, Object value, Locale locale)
    {
        String result = null;

        if (value == null)
        {
        }
        else if (StringUtils.isBlank(format))
        {
            logger.error("The format was not defined for number [" + value.toString() + "].");
            result = value.toString();
        }
        else
        {
            NumberFormat nfi = NumberFormat.getNumberInstance(locale);
            DecimalFormat dff = (DecimalFormat) nfi;
            dff.applyLocalizedPattern(format);
            result = dff.format(Double.parseDouble(value.toString()));
        }
        return result;
    }

    /**
     * Lecture d'un bean dans un des contextes.
     * 
     * @param pageContext
     * @param name
     * @return object
     */
    public static Object retrieveFromScope(PageContext pageContext, String name)
    {
        return retrieveFromScope(pageContext, name, null);
    }

    /**
     * lecture d'un bean dans un contexte donné.
     * 
     * @param pageContext
     * @param name
     * @param scope
     * @return object
     */
    public static Object retrieveFromScope(PageContext pageContext, String name, String scope)
    {
        Object result;
        if (StringUtils.isBlank(scope))
        {
            result = pageContext.findAttribute(name);
        }
        else
        {
            int scopeType = PageContext.REQUEST_SCOPE;

            if (("page").equalsIgnoreCase(scope))
            {
                scopeType = PageContext.PAGE_SCOPE;
            }
            else if (("application").equalsIgnoreCase(scope))
            {
                scopeType = PageContext.APPLICATION_SCOPE;
            }
            else if (("session").equalsIgnoreCase(scope))
            {
                scopeType = PageContext.SESSION_SCOPE;
            }
            result = pageContext.getAttribute(name, scopeType);
        }

        return result;
    }

    /**
     * methode Session size :
     * 
     * @param session
     * @return int
     */
    public static int sessionSize(HttpSession session)
    {
        int total = 0;

        try
        {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ObjectOutputStream oos = new ObjectOutputStream(baos);
            Enumeration<String> enumeration = session.getAttributeNames();

            while (enumeration.hasMoreElements())
            {
                String name = (String) enumeration.nextElement();
                Object obj = session.getAttribute(name);
                oos.writeObject(obj);

                int size = baos.size();
                total += size;
                logger.debug("Le nom de la session est : " + name + " sa taille est de : " + size);
            }

            logger.debug("La taille totale de la session est : " + total);
        }
        catch (Exception exception)
        {
            logger.error("Impossible de lire la taille de la session - ", exception);
        }

        return total;
    }

    /**
     * Retourne la liste des nom des propriétés d'un bean ou d'une Map.
     * 
     * @param bean
     * @return list
     * @throws Exception the exception
     */

    @SuppressWarnings("unchecked")
    public static List<String> beanProperties(Object bean) throws Exception
    {
        List<String> properties = new ArrayList<>();

        if (bean instanceof Map)
        {
            properties.addAll(((Map) bean).keySet());
        }
        else
        {
            properties.addAll(BeanUtils.describe(bean).keySet());
        }

        return properties;
    }

    /**
     * Teste si une propriété est accessible pour un bean donné via réflexion PropertyUtils.isReadable() lève
     * l'exception IllegalArgumentException si la propriété d'un bean est null. C'est un problème pour descendre plus
     * loin dans l'arborescence des objets
     * 
     * @param bean
     * @param property
     * @return true, si c'est bean property readable
     */
    @SuppressWarnings("unchecked")
    public static boolean isBeanPropertyReadable(Object bean, String property)
    {
        boolean isReadable;
        if (bean instanceof Map)
        {
            isReadable = ((Map<String, Object>) bean).containsKey(property);
        }
        else
        {
            try
            {
                isReadable = PropertyUtils.isReadable(bean, property);
            }
            catch (IllegalArgumentException exception)
            {
                if (logger.isDebugEnabled())
                {
                    logger.debug("Impossible de trouver la propriété [" + property
                        + "]. Soit le bean, soit la propriété est null");
                }
                isReadable = false;
            }
        }
        return isReadable;
    }

    /**
     * Consulte les paramètres de la requête pour identifier les cases à cocher sélectionnées.
     * 
     * @param request
     * @param startsWithValue Token qui permet d'identifier les cases à cocher. Par exemple ckbox_.
     * @return list
     */
    public static List<String> checkboxesSelected(HttpServletRequest request, String startsWithValue)
    {
        List<String> results = new ArrayList<String>();

        Enumeration<String> parameterNames = request.getParameterNames();
        while (parameterNames.hasMoreElements())
        {
            String parameterName = (String) parameterNames.nextElement();
            if (parameterName.startsWith(startsWithValue))
            {
                results.add(StringUtils.substringAfter(parameterName, startsWithValue));
            }
        }

        return results;
    }

    /**
     * Crée une URI a partir d'une map de paramètres clé : valeurs.
     * 
     * @param parameterMap
     * @return query string
     */
    public static String getQueryString(Map<String, Object> parameterMap)
    {
        StringBuilder results = new StringBuilder();

        for (Iterator<String> iter = parameterMap.keySet().iterator(); iter.hasNext();)
        {
            String key = iter.next();
            String[] value = (String[]) parameterMap.get(key);

            if (results.length() == 0)
            {
                results.append('?');
            }
            else
            {
                results.append('&');
            }

            results.append(key);
            results.append('=');
            if (value != null && value.length > 0)
            {
                results.append(value[0]);
            }
        }

        return results.toString();
    }
}
