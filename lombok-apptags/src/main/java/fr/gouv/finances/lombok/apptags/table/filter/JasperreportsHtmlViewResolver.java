/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : JasperreportsHtmlViewResolver.java
 *
 */
package fr.gouv.finances.lombok.apptags.table.filter;

import java.util.Locale;
import java.util.Map;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import fr.gouv.finances.lombok.apptags.table.core.Preferences;
import fr.gouv.finances.lombok.apptags.table.view.jasper.CpJasperReportsHtmlView;
import fr.gouv.finances.lombok.upload.bean.FichierJoint;

/**
 * Class JasperreportsHtmlViewResolver --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.2 $ Date: 11 déc. 2009
 */
public class JasperreportsHtmlViewResolver implements ViewResolver
{

    /**
     * Instanciation de jasperreports html view resolver.
     */
    public JasperreportsHtmlViewResolver()
    {
        super();
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param request
     * @param response
     * @param preferences
     * @param viewData
     * @throws Exception the exception
     * @see fr.gouv.finances.lombok.apptags.table.filter.ViewResolver#resolveView(javax.servlet.ServletRequest,
     *      javax.servlet.ServletResponse, fr.gouv.finances.lombok.apptags.table.core.Preferences, java.lang.Object)
     */
    @Override
    public void resolveView(ServletRequest request, ServletResponse response, Preferences preferences, Object viewData)
        throws Exception
    {

        Map mapData = (Map) viewData;

        FichierJoint fichierJoint = (FichierJoint) mapData.get(CpJasperReportsHtmlView.FICHIER_JOINT_KEY);

        byte[] contents = fichierJoint.getLeContenuDuFichier().getData();

        response.setLocale(Locale.FRANCE);
        response.setContentLength((int) fichierJoint.getTailleFichier());
        response.setBufferSize((int) fichierJoint.getTailleFichier());
        response.setContentType(fichierJoint.getTypeMimeFichier());

        response.getOutputStream().write(contents);
        response.getOutputStream().flush();
    }
}
