/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.apptags.table.view.html;

import java.util.List;

import fr.gouv.finances.lombok.apptags.table.bean.Column;
import fr.gouv.finances.lombok.apptags.table.calc.CpCalcResult;
import fr.gouv.finances.lombok.apptags.table.calc.CpCalcUtils;
import fr.gouv.finances.lombok.apptags.table.core.TableModel;
import fr.gouv.finances.lombok.apptags.util.HtmlBuilder;

/**
 * Class CpCalcBuilder
 */
public class CpCalcBuilder
{

    /** html. */
    private HtmlBuilder html;

    /** model. */
    private TableModel model;

    /**
     * Instanciation de cp calc builder.
     * 
     * @param model
     */
    public CpCalcBuilder(TableModel model)
    {
        this(new HtmlBuilder(), model);
    }

    /**
     * Instanciation de cp calc builder.
     * 
     * @param html
     * @param model
     */
    public CpCalcBuilder(HtmlBuilder html, TableModel model)
    {
        this.html = html;
        this.model = model;
    }

    /**
     * Accesseur de l attribut html builder.
     * 
     * @return html builder
     */
    public HtmlBuilder getHtmlBuilder()
    {
        return html;
    }

    /**
     * Accesseur de l attribut table model.
     * 
     * @return table model
     */
    protected TableModel getTableModel()
    {
        return model;
    }

    /**
     * methode Default calc layout :
     */
    public void defaultCalcLayout()
    {
        Column calcColumn = model.getColumnHandler().getFirstCalcColumn();
        if (calcColumn != null)
        {
            this.multiRowCalcResults();
        }
    }

    /**
     * methode Multi row calc results :
     */
    public void multiRowCalcResults()
    {
        Column firstCalcColumn = model.getColumnHandler().getFirstCalcColumn();

        // nombre de lignes de cellules calculées
        int rows = firstCalcColumn.getCalc().size();

        Column firstColumnTitle = CpCalcUtils.getFirstCalTitleColumn(model);

        int colnocalctitle = model.getColumnHandler().getNumeroColonneCalcTitle();

        // On boucle sur les différentes lignes de cellules calculées
        for (int calcCellRowNum = 0; calcCellRowNum < rows; calcCellRowNum++)
        {
            // Si la cellule calculée ne doit pas être affichée -> on passe
            // à la colonne suivante - On recherche les

            Column firstCalcDisplayColumn = model.getColumnHandler().getFirstCalcDisplayColumn();

            boolean displayCalcFlag = true;

            if (firstCalcDisplayColumn != null)
            {

                List<String> calcsDisplays = firstCalcDisplayColumn.getCalcDisplay();

                String displayAttr = "all";

                // Si le display est défini pour chaque cellule de calcul
                // sinon onntravaille avec la valeur par défaut
                if (calcsDisplays.size() > calcCellRowNum)
                {
                    displayAttr = calcsDisplays.get(calcCellRowNum);

                    if ("all".equalsIgnoreCase(displayAttr))
                    {
                        displayCalcFlag = true;
                    }
                    else if ("first".equalsIgnoreCase(displayAttr))
                    {
                        displayCalcFlag = (model.isFirstPage() ? true : false);
                    }
                    else if ("last".equalsIgnoreCase(displayAttr))
                    {
                        displayCalcFlag = (model.isLastPage() ? true : false);
                    }
                    else if ("allbutlast".equalsIgnoreCase(displayAttr))
                    {
                        displayCalcFlag = (model.isLastPage() ? false : true);
                    }
                    else if ("allbutfirst".equalsIgnoreCase(displayAttr))
                    {
                        displayCalcFlag = (model.isFirstPage() ? false : true);
                    }
                    else if ("allbutfirstandlast".equalsIgnoreCase(displayAttr))
                    {
                        displayCalcFlag = (model.isFirstPage() || model.isLastPage() ? false : true);
                    }
                    else if ("firstandlast".equalsIgnoreCase(displayAttr))
                    {
                        displayCalcFlag = (model.isFirstPage() && model.isLastPage() ? true : false);
                    }
                    else if ("notfirstandlast".equalsIgnoreCase(displayAttr))
                    {
                        displayCalcFlag = (!(model.isFirstPage() && model.isLastPage()) ? true : false);
                    }
                }
            }

            if (displayCalcFlag)
            {
                insertOneRowOfCalccell(firstColumnTitle, colnocalctitle, calcCellRowNum);
            }
        }
    }

    /**
     * Insère une ligne de cellules calculées dans le tableau.
     * 
     * @param firstColumnTitle Première colonne qui contient la définition des titres de cellules calculées
     * @param colnocalctitle Numéro de la colonne qui contiendra les titres
     * @param calcCellRowNum Indice de la ligne de calcul à traiter
     */
    private void insertOneRowOfCalccell(Column firstColumnTitle, int colnocalctitle, int calcCellRowNum)
    {
        html.tr(1).styleClass(BuilderConstants.CALC_ROW_CSS).close();

        int colencours = 0;

        for (Column column : this.model.getColumnHandler().getColumns())
        {

            // S'il s'agit de la colonne qui doit contenir les titres des
            // cellules calculées
            if (colencours == colnocalctitle)
            {
                String calcTitle = CpCalcUtils.getFirstCalcColumnTitleByPosition(model, calcCellRowNum);

                String calcTitleClass = (firstColumnTitle == null ? "" : firstColumnTitle.getCalcTitleClass());
                String calcTitleStyle = (firstColumnTitle == null ? "" : firstColumnTitle.getCalcTitleStyle());

                // Les titres des cellules calculées sont placés dans la
                // première colonne du tableau
                if (column.isFirstColumn() && column.isCalculated())
                {
                    insertTitleAndCalcInFirstColumn(calcCellRowNum, column, calcTitle, calcTitleClass);
                }
                // Les titres des cellules sont placés seuls dans une colonne
                else
                {
                    insertTitleAloneInColumn(calcTitle, calcTitleClass, calcTitleStyle);
                }
            }
            // Si la colonne en cours ne contient pas les titres des
            // cellules et contient des cellules calculées
            else if (column.isCalculated())
            {
                insertCalcAloneInColumn(calcCellRowNum, column);
            }
            // Si la colonne en cours ne contient ni titre de cellule ni
            // cellule calculée, on insère une cellule vide
            else
            {
                insertEmptyCellInColumn();
            }

            colencours++;

        } // Fin for

        html.trEnd(1);
    }

    /**
     * Insère une cellule vide.
     */
    private void insertEmptyCellInColumn()
    {
        html.td(2).close();
        html.nbsp();
        html.tdEnd();
    }

    /**
     * Insère une cellule calculée (uniquement) dans une cellule de la colonne.
     * 
     * @param i
     * @param column
     */
    private void insertCalcAloneInColumn(int i, Column column)
    {
        html.td(2).styleClass(column.getCalcClass()).style(column.getCalcStyle()).close();

        CpCalcResult calcResult = CpCalcUtils.getCalcResultsByPosition(model, column, i);

        if (calcResult == null || calcResult.getValue() == null)
        {
            html.nbsp();
        }
        else
        {
            html.append(CpCalcUtils.findValueFormated(model, column, calcResult.getValue(), i));
        }
        html.tdEnd();
    }

    /**
     * Insère un titre de cellule calculée (uniquement) dans une cellule de la colonne.
     * 
     * @param calcTitle
     * @param calcTitleClass
     * @param calcTitleStyle
     */
    private void insertTitleAloneInColumn(String calcTitle, String calcTitleClass, String calcTitleStyle)
    {
        html.td(2).styleClass(calcTitleClass).style(calcTitleStyle).close();
        html.append(calcTitle);
        html.tdEnd();
    }

    /**
     * Insère un titre et la valeur d'une cellule calculée dans une cellule de la colonne.
     * 
     * @param i
     * @param column
     * @param calcTitle
     * @param calcTitleClass
     */
    private void insertTitleAndCalcInFirstColumn(int i, Column column, String calcTitle, String calcTitleClass)
    {
        // si la première colonne est calculée, la cellule
        // contient à la fois le titre et la valeur calculée

        html.td(2).styleClass(calcTitleClass).close();
        html.span().style("float: left;").close().append(calcTitle);
        html.spanEnd();

        CpCalcResult calcResult = CpCalcUtils.getCalcResultsByPosition(model, column, i);

        Number value;
        String toDisplay = "&nbsp;";
        if (calcResult != null)
        {
            value = calcResult.getValue();
            toDisplay = (value == null ? calcResult.getName() : CpCalcUtils.findValueFormated(model, column, value, i));
        }
        html.span().styleClass(column.getCalcClass()).style("float: right;").close().append(toDisplay);
        html.spanEnd();
        html.tdEnd();
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        return html.toString();
    }
}
