/*
 * Copyright (c) 2014 DGFiP - Tous droits réservés
 */
/*
/*Copyright (c) 2014 DGFiP - Tous droits réservés/* * Copyright 2004 original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package fr.gouv.finances.lombok.apptags.table.core;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import fr.gouv.finances.lombok.apptags.table.calc.Calc;
import fr.gouv.finances.lombok.apptags.table.callback.FilterRowsCallback;
import fr.gouv.finances.lombok.apptags.table.callback.RetrieveRowsCallback;
import fr.gouv.finances.lombok.apptags.table.callback.SortRowsCallback;
import fr.gouv.finances.lombok.apptags.table.cell.Cell;
import fr.gouv.finances.lombok.apptags.table.interceptor.ColumnInterceptor;
import fr.gouv.finances.lombok.apptags.table.interceptor.ExportInterceptor;
import fr.gouv.finances.lombok.apptags.table.interceptor.RowInterceptor;
import fr.gouv.finances.lombok.apptags.table.interceptor.TableInterceptor;
import fr.gouv.finances.lombok.apptags.table.state.State;

/**
 * Class TableCache
 */
public class TableCache
{

    /** logger. */
    private static Log logger = LogFactory.getLog(TableCache.class);

    /** table cache. */
    public static TableCache tableCache = new TableCache();

    /** cache. */
    private Map<Class<? extends Object>, Object> cache =
        Collections.synchronizedMap(new HashMap<Class<? extends Object>, Object>());

    /**
     * Instanciation de table cache.
     */
    private TableCache()
    {
    }

    /**
     * Acceder à l unique instance de TableCache.
     * 
     * @return l unique instance de TableCache
     */
    public static TableCache getInstance()
    {
        return tableCache;
    }

    /**
     * Accesseur de l attribut cached object.
     * 
     * @param className
     * @return cached object
     */
    private Object getCachedObject(String className)
    {
        Object cachedObject = null;

        try
        {
            Class<? extends Object> classForName = Class.forName(className);
            cachedObject = cache.get(classForName);
            if (cachedObject == null)
            {
                cachedObject = classForName.newInstance();
                cache.put(classForName, cachedObject);
            }

        }
        catch (Exception exception)
        {
            StringBuilder   mes = new StringBuilder(78);
            mes.append("Impossible de créer l'objet [");
            mes.append(className);
            mes.append("]. La classe n'existe pas ou n'a pas été trouvée.");
            logger.error(mes.toString(), exception);
            throw new IllegalStateException(mes.toString(), exception);
        }

        return cachedObject;
    }

    /**
     * Accesseur de l attribut cell.
     * 
     * @param cell
     * @return cell
     */
    public Cell getCell(String cell)
    {
        return (Cell) getCachedObject(cell);
    }

    /**
     * Accesseur de l attribut state.
     * 
     * @param state
     * @return state
     */
    public State getState(String state)
    {
        return (State) getCachedObject(state);
    }

    /**
     * Accesseur de l attribut retrieve rows callback.
     * 
     * @param model
     * @return retrieve rows callback
     */
    public RetrieveRowsCallback getRetrieveRowsCallback(TableModel model)
    {
        String retrieveRowsCallback = model.getTableHandler().getTable().getRetrieveRowsCallback();
        return (RetrieveRowsCallback) getCachedObject(retrieveRowsCallback);
    }

    /**
     * Accesseur de l attribut filter rows callback.
     * 
     * @param model
     * @return filter rows callback
     */
    public FilterRowsCallback getFilterRowsCallback(TableModel model)
    {
        String filterRowsCallback = model.getTableHandler().getTable().getFilterRowsCallback();
        return (FilterRowsCallback) getCachedObject(filterRowsCallback);
    }

    /**
     * Accesseur de l attribut sort rows callback.
     * 
     * @param model
     * @return sort rows callback
     */
    public SortRowsCallback getSortRowsCallback(TableModel model)
    {
        String sortRowsCallback = model.getTableHandler().getTable().getSortRowsCallback();
        return (SortRowsCallback) getCachedObject(sortRowsCallback);
    }

    /**
     * Accesseur de l attribut calc.
     * 
     * @param calc
     * @return calc
     */
    public Calc getCalc(String calc)
    {
        return (Calc) getCachedObject(calc);
    }

    /**
     * Accesseur de l attribut table interceptor.
     * 
     * @param tableInterceptor
     * @return table interceptor
     */
    public TableInterceptor getTableInterceptor(String tableInterceptor)
    {
        return (TableInterceptor) getCachedObject(tableInterceptor);
    }

    /**
     * Accesseur de l attribut row interceptor.
     * 
     * @param rowInterceptor
     * @return row interceptor
     */
    public RowInterceptor getRowInterceptor(String rowInterceptor)
    {
        return (RowInterceptor) getCachedObject(rowInterceptor);
    }

    /**
     * Accesseur de l attribut column interceptor.
     * 
     * @param columnInterceptor
     * @return column interceptor
     */
    public ColumnInterceptor getColumnInterceptor(String columnInterceptor)
    {
        return (ColumnInterceptor) getCachedObject(columnInterceptor);
    }

    /**
     * Accesseur de l attribut export interceptor.
     * 
     * @param exportInterceptor
     * @return export interceptor
     */
    public ExportInterceptor getExportInterceptor(String exportInterceptor)
    {
        return (ExportInterceptor) getCachedObject(exportInterceptor);
    }

    /**
     * Accesseur de l attribut auto generate columns.
     * 
     * @param autoGenerateColumns
     * @return auto generate columns
     */
    public AutoGenerateColumns getAutoGenerateColumns(String autoGenerateColumns)
    {
        return (AutoGenerateColumns) getCachedObject(autoGenerateColumns);
    }

    /**
     * Accesseur de l attribut colonne protection.
     * 
     * @param colonneProtection
     * @return colonne protection
     */
    public ColonneProtection getColonneProtection(String colonneProtection)
    {
        return (ColonneProtection) getCachedObject(colonneProtection);
    }
}
