/*
/*Copyright (c) 2014 DGFiP - Tous droits réservés/* * Copyright 2004 original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package fr.gouv.finances.lombok.apptags.table.context;

import java.io.Writer;
import java.util.Locale;
import java.util.Map;

/**
 * Interface Context
 */
public interface Context
{

    /**
     * Accesseur de l attribut application init parameter.
     * 
     * @param name
     * @return application init parameter
     */
    public Object getApplicationInitParameter(String name);

    /**
     * Accesseur de l attribut application attribute.
     * 
     * @param name
     * @return application attribute
     */
    public Object getApplicationAttribute(String name);

    /**
     * methode Sets the application attribute :
     * 
     * @param name
     * @param value
     */
    public void setApplicationAttribute(String name, Object value);

    /**
     * methode Removes the application attribute :
     * 
     * @param name
     */
    public void removeApplicationAttribute(String name);

    /**
     * Accesseur de l attribut page attribute.
     * 
     * @param name
     * @return page attribute
     */
    public Object getPageAttribute(String name);

    /**
     * methode Sets the page attribute :
     * 
     * @param name
     * @param value
     */
    public void setPageAttribute(String name, Object value);

    /**
     * methode Removes the page attribute :
     * 
     * @param name
     */
    public void removePageAttribute(String name);

    /**
     * Accesseur de l attribut parameter.
     * 
     * @param name
     * @return parameter
     */
    public String getParameter(String name);

    /**
     * Accesseur de l attribut parameter map.
     * 
     * @return parameter map
     */
    public Map<String, String[]> getParameterMap();

    /**
     * Accesseur de l attribut request attribute.
     * 
     * @param name
     * @return request attribute
     */
    public Object getRequestAttribute(String name);

    /**
     * methode Sets the request attribute :
     * 
     * @param name
     * @param value
     */
    public void setRequestAttribute(String name, Object value);

    /**
     * methode Removes the request attribute :
     * 
     * @param name
     */
    public void removeRequestAttribute(String name);

    /**
     * Accesseur de l attribut session attribute.
     * 
     * @param name
     * @return session attribute
     */
    public Object getSessionAttribute(String name);

    /**
     * methode Sets the session attribute :
     * 
     * @param name
     * @param value
     */
    public void setSessionAttribute(String name, Object value);

    /**
     * methode Removes the session attribute :
     * 
     * @param name
     */
    public void removeSessionAttribute(String name);

    /**
     * Accesseur de l attribut writer.
     * 
     * @return writer
     */
    public Writer getWriter();

    /**
     * Accesseur de l attribut locale.
     * 
     * @return locale
     */
    public Locale getLocale();

    /**
     * Accesseur de l attribut context path.
     * 
     * @return context path
     */
    public String getContextPath();

    /**
     * Accesseur de l attribut context object.
     * 
     * @return context object
     */
    public Object getContextObject();

}