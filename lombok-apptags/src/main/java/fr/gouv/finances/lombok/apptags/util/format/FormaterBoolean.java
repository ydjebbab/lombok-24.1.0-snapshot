/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.apptags.util.format;

import fr.gouv.finances.lombok.apptags.util.format.BooleanFormat.TypeFormat;

/**
 * Class FormaterBoolean
 */
public class FormaterBoolean
{

    /**
     * Instanciation de formater boolean.
     */
    private FormaterBoolean()
    {
        // Constructeur vide;
    }

    /**
     * Retourne un objet BooleanFormat Le parametre typeFormat peut prendre les valeurs suivantes :
     * <ul>
     * <li>TypeFormat.O_N</li>
     * <li>TypeFormat.OUI_NON</li>
     * <li>TypeFormat.V_F</li>
     * <li>TypeFormat.VRAI_FAUX</li>
     * <li>TypeFormat.UN_ZERO</li>
     * </ul>
     * 
     * @param typeFormat typeFormat
     * @return format
     */
    public static BooleanFormat getFormat(final TypeFormat typeFormat)
    {
        return BooleanFormat.getInstance(typeFormat);
    }

    /**
     * Retourne un objet BooleanFormat.
     * 
     * @param formatIndex formatIndex
     * @return format
     */
    @Deprecated
    public static BooleanFormat getFormat(final int formatIndex)
    {
        return BooleanFormat.getInstance(formatIndex);
    }

    /**
     * Formate un booléen de la façon suivante : Boolean.TRUE -> 1 Boolean.FALSE -> 0 NULL -> 0
     * 
     * @param value value
     * @return String
     */
    public static String boolean10(final Boolean value)
    {
        return getFormat(TypeFormat.UN_ZERO).format(value);
    }

    /**
     * Formate un booléen de la façon suivante : Boolean.TRUE -> O Boolean.FALSE -> N NULL -> N
     * 
     * @param value value
     * @return String
     */
    public static String booleanON(final Boolean value)
    {
        return getFormat(TypeFormat.O_N).format(value);
    }

    /**
     * Formate un booléen de la façon suivante : Boolean.TRUE -> OUI Boolean.FALSE -> NON NULL -> NON
     * 
     * @param value value
     * @return String
     */
    public static String booleanOuiNon(final Boolean value)
    {
        return getFormat(TypeFormat.OUI_NON).format(value);
    }

    /**
     * Formate un booléen de la façon suivante : Boolean.TRUE -> V Boolean.FALSE -> F NULL -> F
     * 
     * @param value value
     * @return String
     */
    public static String booleanVF(final Boolean value)
    {
        return getFormat(TypeFormat.V_F).format(value);
    }

    /**
     * Formate un booléen de la façon suivante : Boolean.TRUE -> VRAI Boolean.FALSE -> FAUX NULL -> FAUX
     * 
     * @param value value
     * @return String
     */
    public static String booleanVraiFaux(final Boolean value)
    {
        return getFormat(TypeFormat.VRAI_FAUX).format(value);
    }

    /**
     * Retourne un Object BooleanFormat utilisant la correspondance suivante Boolean.TRUE -> 1 Boolean.FALSE -> 0
     * 
     * @return BooleanFormat
     */
    public static BooleanFormat getFormatBoolean10()
    {
        return BooleanFormat.getInstance(TypeFormat.UN_ZERO);
    }

    /**
     * Retourne un Object BooleanFormat utilisant la correspondance suivante Boolean.TRUE -> O Boolean.FALSE -> N
     * 
     * @return BooleanFormat
     */
    public static BooleanFormat getFormatBooleanON()
    {
        return BooleanFormat.getInstance(TypeFormat.O_N);
    }

    /**
     * Retourne un Object BooleanFormat utilisant la correspondance suivante Boolean.TRUE -> OUI Boolean.FALSE -> NON
     * 
     * @return BooleanFormat
     */
    public static BooleanFormat getFormatBooleanOuiNon()
    {
        return BooleanFormat.getInstance(TypeFormat.OUI_NON);
    }

    /**
     * Retourne un Object BooleanFormat utilisant la correspondance suivante Boolean.TRUE -> V Boolean.FALSE -> F
     * 
     * @return BooleanFormat
     */
    public static BooleanFormat getFormatBooleanVF()
    {
        return BooleanFormat.getInstance(TypeFormat.V_F);
    }

    /**
     * Retourne un Object BooleanFormat utilisant la correspondance suivante Boolean.TRUE -> VRAI Boolean.FALSE -> FAUX
     * 
     * @return BooleanFormat
     */
    public static BooleanFormat getFormatBooleanVraiFaux()
    {
        return BooleanFormat.getInstance(TypeFormat.VRAI_FAUX);
    }

}
