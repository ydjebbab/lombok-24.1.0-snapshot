/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.apptags.table.cell;

import fr.gouv.finances.lombok.apptags.util.HtmlBuilder;

/**
 * Class DisabledSelectAllCell
 */
public class DisabledSelectAllCell extends SelectAllCell
{

    /**
     * Instanciation de disabled select all cell.
     */
    public DisabledSelectAllCell()
    {
        super();
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.cell.SelectAllCell#ajouterOptionsSurCheckBox(fr.gouv.finances.lombok.apptags.util.HtmlBuilder)
     */
    @Override
    protected HtmlBuilder ajouterOptionsSurCheckBox(HtmlBuilder html)
    {
        html.disabled();
        return html;
    }

}
