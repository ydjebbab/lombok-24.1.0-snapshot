/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.apptags.table.view.jrxmlinfo;

import java.util.ArrayList;
import java.util.List;

/**
 * Class ColumnHeaderInfo
 */
public class ColumnHeaderInfo extends AbstractBandInfo
{

    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** Espace vide inséré dans l'entête de colonne, avant l'insertion des titres de colonnes du tableau. */
    private int spaceBefore;

    /** Flag qui indique si l'arrière plan des titres de colonne est opaque. */
    private boolean opaque;

    /** color r. */
    private int colorR;

    /** color g. */
    private int colorG;

    /** color b. */
    private int colorB;

    /** Hauteur réservée au titre de colonne. */
    private int titleHeight;

    /** Hauteur réservée au sous groupe du titre de colonne. */
    private int subgroupHeight;

    /** Hauteur réservée au groupe du titre de colonne. */
    private int groupHeight;

    /** Nombre de niveau de l'entête de colonnes (groupes, sous-groupes, titres). */
    private int rows;

    /** liste des titres de colonne. */
    private List<TitleHeaderInfo> listeDesTitresDeColonne = new ArrayList<TitleHeaderInfo>();

    /** liste des sous groupes de colonne. */
    private List<SubgroupHeaderInfo> listeDesSousGroupesDeColonne = new ArrayList<SubgroupHeaderInfo>();

    /** liste des groupes de colonne. */
    private List<GroupHeaderInfo> listeDesGroupesDeColonne = new ArrayList<GroupHeaderInfo>();

    /**
     * Instanciation de column header info.
     */
    public ColumnHeaderInfo()
    {
        super();
    }

    /**
     * Ajoute un titre de colonne à la liste des titre.
     * 
     * @param value
     * @param width
     * @param xLeftCorner
     * @return title header info
     */
    public TitleHeaderInfo ajouterUnNouveauTitre(String value, int width, int xLeftCorner)
    {
        TitleHeaderInfo titleHeaderInfo = new TitleHeaderInfo();

        // Affectation de la Largeur
        titleHeaderInfo.setWidth(width);

        titleHeaderInfo.setHeight(this.titleHeight);

        // Affectation du la valeur du titre
        titleHeaderInfo.setValue(value);

        if (this.listeDesTitresDeColonne != null && this.listeDesTitresDeColonne.size() > 0)
        {
            TitleHeaderInfo previousTitle =
                this.listeDesTitresDeColonne.get(this.listeDesTitresDeColonne.size() - 1);
            titleHeaderInfo.setPreviousTitle(previousTitle);
        }

        this.listeDesTitresDeColonne.add(titleHeaderInfo);

        return titleHeaderInfo;
    }

    /**
     * Ajoute un nouveau groupe avec titre de colonne à la liste des groupes.
     * 
     * @param value
     * @param titleHeaderInfo
     * @return group header info
     */
    public GroupHeaderInfo ajouterUnNouveauGroupeAvecTitre(String value, TitleHeaderInfo titleHeaderInfo)
    {
        GroupHeaderInfo groupHeaderInfo = new GroupHeaderInfo();
        groupHeaderInfo.setValue(value);
        groupHeaderInfo.setHeight(this.groupHeight);
        groupHeaderInfo.setSpaceBefore(this.spaceBefore);

        groupHeaderInfo.ajouterUnTitreAuGroupe(titleHeaderInfo);

        if (this.listeDesGroupesDeColonne != null && this.listeDesGroupesDeColonne.size() > 0)
        {
            GroupHeaderInfo previousGroup =
                this.listeDesGroupesDeColonne.get(this.listeDesGroupesDeColonne.size() - 1);
            groupHeaderInfo.setPreviousGroup(previousGroup);
        }

        this.listeDesGroupesDeColonne.add(groupHeaderInfo);

        return groupHeaderInfo;
    }

    /**
     * Ajoute un nouveau groupe absorbant un titre de colonne (le titre de colonne vient se placer dans la ligne des
     * groupes).
     * 
     * @param value
     * @param titleHeaderInfo
     * @return group header info
     */
    public GroupHeaderInfo ajouterUnNouveauGroupeAbsorbantUnTitre(String value, TitleHeaderInfo titleHeaderInfo)
    {
        GroupHeaderInfo groupHeaderInfo = new GroupHeaderInfo();
        groupHeaderInfo.setValue(value);
        groupHeaderInfo.setHeight(this.groupHeight);
        groupHeaderInfo.setSpaceBefore(this.spaceBefore);
        groupHeaderInfo.setMergeWithChild(true);

        titleHeaderInfo.setValue("");
        groupHeaderInfo.ajouterUnTitreAuGroupe(titleHeaderInfo);

        if (this.listeDesGroupesDeColonne != null && this.listeDesGroupesDeColonne.size() > 0)
        {
            GroupHeaderInfo previousGroup =
                this.listeDesGroupesDeColonne.get(this.listeDesGroupesDeColonne.size() - 1);
            groupHeaderInfo.setPreviousGroup(previousGroup);
        }

        this.listeDesGroupesDeColonne.add(groupHeaderInfo);

        return groupHeaderInfo;
    }

    /**
     * Ajoute un nouveau sous groupe et un titre de colonne.
     * 
     * @param value
     * @param titleHeaderInfo
     * @return subgroup header info
     */
    public SubgroupHeaderInfo ajouterUnNouveauSousGroupeAvecTitre(String value, TitleHeaderInfo titleHeaderInfo)
    {
        SubgroupHeaderInfo sousgroupHeaderInfo = new SubgroupHeaderInfo();

        sousgroupHeaderInfo.setValue(value);
        sousgroupHeaderInfo.setHeight(this.subgroupHeight);

        sousgroupHeaderInfo.ajouterUnTitreAuSousGroupe(titleHeaderInfo);

        if (this.listeDesSousGroupesDeColonne != null && this.listeDesSousGroupesDeColonne.size() > 0)
        {
            SubgroupHeaderInfo previousSubgroup =
                this.listeDesSousGroupesDeColonne
                    .get(this.listeDesSousGroupesDeColonne.size() - 1);
            sousgroupHeaderInfo.setPreviousSubgroup(previousSubgroup);
        }

        this.listeDesSousGroupesDeColonne.add(sousgroupHeaderInfo);

        return sousgroupHeaderInfo;
    }

    /**
     * Ajoute un nouveau sous groupe absorbant un titre de colonne (le titre de colonne vient se placer dans la ligne
     * des sous-groupes).
     * 
     * @param value
     * @param titleHeaderInfo
     * @return subgroup header info
     */
    public SubgroupHeaderInfo ajouterUnNouveauSousGroupeAbsorbantLeTitre(String value, TitleHeaderInfo titleHeaderInfo)
    {
        SubgroupHeaderInfo sousgroupHeaderInfo = new SubgroupHeaderInfo();
        sousgroupHeaderInfo.setValue(value);
        sousgroupHeaderInfo.setHeight(this.subgroupHeight);
        sousgroupHeaderInfo.setMergeWithChild(true);

        titleHeaderInfo.setValue("");
        sousgroupHeaderInfo.ajouterUnTitreAuSousGroupe(titleHeaderInfo);

        if (this.listeDesSousGroupesDeColonne != null && this.listeDesSousGroupesDeColonne.size() > 0)
        {
            SubgroupHeaderInfo previousSubgroup =
                this.listeDesSousGroupesDeColonne
                    .get(this.listeDesSousGroupesDeColonne.size() - 1);
            sousgroupHeaderInfo.setPreviousSubgroup(previousSubgroup);
        }

        this.listeDesSousGroupesDeColonne.add(sousgroupHeaderInfo);

        return sousgroupHeaderInfo;
    }

    /**
     * Ajoute un nouveau groupe absorbant un sous-groupe de colonne.
     * 
     * @param value
     * @param subGroupHeaderInfo
     * @return group header info
     */
    public GroupHeaderInfo ajouterUnNouveauGroupeAbsorbantLeSousGroupe(String value,
        SubgroupHeaderInfo subGroupHeaderInfo)
    {
        GroupHeaderInfo groupHeaderInfo = new GroupHeaderInfo();
        groupHeaderInfo.setValue(value);
        groupHeaderInfo.setHeight(this.groupHeight);
        groupHeaderInfo.setSpaceBefore(this.spaceBefore);
        groupHeaderInfo.setMergeWithChild(true);

        subGroupHeaderInfo.setValue("");
        groupHeaderInfo.ajouterUnSousGroupeAuGroupe(subGroupHeaderInfo);

        if (this.listeDesGroupesDeColonne != null && this.listeDesGroupesDeColonne.size() > 0)
        {
            GroupHeaderInfo previousGroup =
                this.listeDesGroupesDeColonne.get(this.listeDesGroupesDeColonne.size() - 1);
            groupHeaderInfo.setPreviousGroup(previousGroup);
        }

        this.listeDesGroupesDeColonne.add(groupHeaderInfo);

        return groupHeaderInfo;
    }

    /**
     * Ajoute un nouveau groupe avec sous groupe.
     * 
     * @param value
     * @param subGroupHeaderInfo
     * @return group header info
     */
    public GroupHeaderInfo ajouterUnNouveauGroupeAvecSousGroupe(String value, SubgroupHeaderInfo subGroupHeaderInfo)
    {
        GroupHeaderInfo groupHeaderInfo = new GroupHeaderInfo();
        groupHeaderInfo.setValue(value);

        groupHeaderInfo.setHeight(this.groupHeight);

        groupHeaderInfo.setSpaceBefore(this.spaceBefore);

        groupHeaderInfo.ajouterUnSousGroupeAuGroupe(subGroupHeaderInfo);

        if (this.listeDesGroupesDeColonne != null && this.listeDesGroupesDeColonne.size() > 0)
        {
            GroupHeaderInfo previousGroup =
                this.listeDesGroupesDeColonne.get(this.listeDesGroupesDeColonne.size() - 1);
            groupHeaderInfo.setPreviousGroup(previousGroup);
        }

        this.listeDesGroupesDeColonne.add(groupHeaderInfo);

        return groupHeaderInfo;
    }

    /**
     * Gets the hauteur réservée au groupe du titre de colonne.
     * 
     * @return the hauteur réservée au groupe du titre de colonne
     */
    public int getGroupHeight()
    {
        return groupHeight;
    }

    /**
     * Sets the hauteur réservée au groupe du titre de colonne.
     * 
     * @param groupHeight the new hauteur réservée au groupe du titre de colonne
     */
    public void setGroupHeight(int groupHeight)
    {
        this.groupHeight = groupHeight;
    }

    /**
     * Gets the hauteur réservée au sous groupe du titre de colonne.
     * 
     * @return the hauteur réservée au sous groupe du titre de colonne
     */
    public int getSubgroupHeight()
    {
        return subgroupHeight;
    }

    /**
     * Sets the hauteur réservée au sous groupe du titre de colonne.
     * 
     * @param subgroupHeight the new hauteur réservée au sous groupe du titre de colonne
     */
    public void setSubgroupHeight(int subgroupHeight)
    {
        this.subgroupHeight = subgroupHeight;
    }

    /**
     * Gets the hauteur réservée au titre de colonne.
     * 
     * @return the hauteur réservée au titre de colonne
     */
    public int getTitleHeight()
    {
        return titleHeight;
    }

    /**
     * Sets the hauteur réservée au titre de colonne.
     * 
     * @param titleHeight the new hauteur réservée au titre de colonne
     */
    public void setTitleHeight(int titleHeight)
    {
        this.titleHeight = titleHeight;
    }

    /**
     * Accesseur de l attribut liste des groupes de colonne.
     * 
     * @return liste des groupes de colonne
     */
    public List<GroupHeaderInfo> getListeDesGroupesDeColonne()
    {
        return listeDesGroupesDeColonne;
    }

    /**
     * Accesseur de l attribut liste des sous groupes de colonne.
     * 
     * @return the listeDesSousGroupesDeColonne
     */
    public List<SubgroupHeaderInfo> getListeDesSousGroupesDeColonne()
    {
        return listeDesSousGroupesDeColonne;
    }

    /**
     * Modificateur de l attribut liste des sous groupes de colonne.
     * 
     * @param listeDesSousGroupesDeColonne the listeDesSousGroupesDeColonne to set
     */
    public void setListeDesSousGroupesDeColonne(List<SubgroupHeaderInfo> listeDesSousGroupesDeColonne)
    {
        this.listeDesSousGroupesDeColonne = listeDesSousGroupesDeColonne;
    }

    /**
     * Accesseur de l attribut liste des titres de colonne.
     * 
     * @return the listeDesTitresDeColonne
     */
    public List<TitleHeaderInfo> getListeDesTitresDeColonne()
    {
        return listeDesTitresDeColonne;
    }

    /**
     * Modificateur de l attribut liste des titres de colonne.
     * 
     * @param listeDesTitresDeColonne the listeDesTitresDeColonne to set
     */
    public void setListeDesTitresDeColonne(List<TitleHeaderInfo> listeDesTitresDeColonne)
    {
        this.listeDesTitresDeColonne = listeDesTitresDeColonne;
    }

    /**
     * Modificateur de l attribut liste des groupes de colonne.
     * 
     * @param listeDesGroupesDeColonne the listeDesGroupesDeColonne to set
     */
    public void setListeDesGroupesDeColonne(List<GroupHeaderInfo> listeDesGroupesDeColonne)
    {
        this.listeDesGroupesDeColonne = listeDesGroupesDeColonne;
    }

    /**
     * Gets the nombre de niveau de l'entête de colonnes (groupes, sous-groupes, titres).
     * 
     * @return the nombre de niveau de l'entête de colonnes (groupes, sous-groupes, titres)
     */
    public int getRows()
    {
        return rows;
    }

    /**
     * Sets the nombre de niveau de l'entête de colonnes (groupes, sous-groupes, titres).
     * 
     * @param rows the new nombre de niveau de l'entête de colonnes (groupes, sous-groupes, titres)
     */
    public void setRows(int rows)
    {
        this.rows = rows;
    }

    /**
     * Accesseur de l attribut color b.
     * 
     * @return color b
     */
    public int getColorB()
    {
        return colorB;
    }

    /**
     * Modificateur de l attribut color b.
     * 
     * @param colorB le nouveau color b
     */
    public void setColorB(int colorB)
    {
        this.colorB = colorB;
    }

    /**
     * Accesseur de l attribut color g.
     * 
     * @return color g
     */
    public int getColorG()
    {
        return colorG;
    }

    /**
     * Modificateur de l attribut color g.
     * 
     * @param colorG le nouveau color g
     */
    public void setColorG(int colorG)
    {
        this.colorG = colorG;
    }

    /**
     * Accesseur de l attribut color r.
     * 
     * @return color r
     */
    public int getColorR()
    {
        return colorR;
    }

    /**
     * Modificateur de l attribut color r.
     * 
     * @param colorR le nouveau color r
     */
    public void setColorR(int colorR)
    {
        this.colorR = colorR;
    }

    /**
     * Checks if is flag qui indique si l'arrière plan des titres de colonne est opaque.
     * 
     * @return the flag qui indique si l'arrière plan des titres de colonne est opaque
     */
    public boolean isOpaque()
    {
        return opaque;
    }

    /**
     * Sets the flag qui indique si l'arrière plan des titres de colonne est opaque.
     * 
     * @param opaque the new flag qui indique si l'arrière plan des titres de colonne est opaque
     */
    public void setOpaque(boolean opaque)
    {
        this.opaque = opaque;
    }

    /**
     * Gets the espace vide inséré dans l'entête de colonne, avant l'insertion des titres de colonnes du tableau.
     * 
     * @return the espace vide inséré dans l'entête de colonne, avant l'insertion des titres de colonnes du tableau
     */
    public int getSpaceBefore()
    {
        return spaceBefore;
    }

    /**
     * Sets the espace vide inséré dans l'entête de colonne, avant l'insertion des titres de colonnes du tableau.
     * 
     * @param spaceBefore the new espace vide inséré dans l'entête de colonne, avant l'insertion des titres de colonnes
     *        du tableau
     */
    public void setSpaceBefore(int spaceBefore)
    {
        this.spaceBefore = spaceBefore;
    }

}
