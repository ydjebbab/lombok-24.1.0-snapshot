/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : NodeLinkTag.java
 *
 */
package fr.gouv.finances.lombok.apptags.tree.tag;

import javax.servlet.jsp.JspException;

import fr.gouv.finances.lombok.apptags.base.BaseAppTags;
import fr.gouv.finances.lombok.apptags.base.TagWriter;

/**
 * Class NodeLinkTag --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.2 $ Date: 11 déc. 2009
 */
public class NodeLinkTag extends BaseAppTags
{
    /** Constant : FLOWEXECUTIONID_FIELD_NAME. */
    public static final String FLOWEXECUTIONID_FIELD_NAME = "_flowExecutionKey";

    /** Constant : EVENTID_FIELDURL_PREFIX. */
    public static final String EVENTID_FIELDURL_PREFIX = "_eventId";

    /** Constant : FLOWEXECUTIONID_VALUE. */
    public static final String FLOWEXECUTIONID_VALUE = "flowExecutionKey";

    /** Constant : EVENTID_FIELD_PREFIX. */
    public static final String EVENTID_FIELD_PREFIX = "_eventId_";

    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /**
     * The {@link TagWriter} instance being used.
     * <p>
     * Stored so we can close the tag on {@link #doEndTag()}.
     */
    private TagWriter tagWriter;

    /** Nom de l'action - URL qui permet d'accéder au contrôleur. */
    private String action;

    /** Nom de la transition définie dans le webflow. */
    private String transition;

    /** Uri vers laquelle pointe le lien. Utilisé si les paramètres action et transition ne sont pas utilisés. */
    private String uri;

    /** Attribut qui indique si le lien est actif. */
    private String active;

    /** _active. */
    private String _active;

    /** Nom de l'attribut passé en paramètre. */
    private String attribute;

    /** Valeur de l'attribut passé en paramètre. */
    private String value;

    /** Classe css de l'élément a. */
    private String aclass;

    /** _aclass. */
    private String _aclass;

    /** Nom de l'image qui sert pour matérialiser le lien. */
    private String image;

    /** Répertoire de stockage des images. */
    private String imagesrepertoire;

    /** _imagesrepertoire. */
    private String _imagesrepertoire;

    /** Titre de l'élément href et de l'image. */
    private String title;

    /** Texte affiché si le navigateur n'affiche pas les images. */
    private String alt;

    /** Libellé du lien. */
    private String label;

    /** true : ouvre une nouvelle fenêtre du navigateur. Par défaut : false */
    private String openwindow;

    /** _openwindow. */
    private String _openwindow;

    /** Nom de la fenêtre à ouvrir. */
    private String openwindowname;

    /** _openwindowname. */
    private String _openwindowname;

    /** Options d'ouverture d'une fenêtre window. */
    private String openwindowoptions;

    /** _openwindowoptions. */
    private String _openwindowoptions;

    /** true : affiche un message d'attente. Par défaut : false */
    private String waitingmsg;

    /** _waitingmsg. */
    private String _waitingmsg;

    /** true: le tag est évalué. Par défaut : true */
    private String displayed;

    /** _displayed. */
    private String _displayed;

    public NodeLinkTag()
    {
        super();        
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @return int
     * @throws JspException the jsp exception
     * @see javax.servlet.jsp.tagext.TagSupport#doEndTag()
     */
    @Override
    public int doEndTag() throws JspException
    {

        if (isTRUE(this._active))
        {
            this.tagWriter.endTag();
        }
        return EVAL_PAGE;
    }

    /**
     * Disposes of the {@link TagWriter} instance.
     */
    @Override
    public void doFinally()
    {
        super.doFinally();
        this.tagWriter = null;
    }

    /**
     * Accesseur de l attribut aclass.
     * 
     * @return aclass
     */
    public String getAclass()
    {
        return aclass;
    }

    /**
     * Accesseur de l attribut action.
     * 
     * @return action
     */
    public String getAction()
    {
        return action;
    }

    /**
     * Accesseur de l attribut active.
     * 
     * @return active
     */
    public String getActive()
    {
        return active;
    }

    /**
     * Accesseur de l attribut alt.
     * 
     * @return alt
     */
    public String getAlt()
    {
        return alt;
    }

    /**
     * Accesseur de l attribut attribute.
     * 
     * @return attribute
     */
    public String getAttribute()
    {
        return attribute;
    }

    /**
     * Accesseur de l attribut displayed.
     * 
     * @return displayed
     */
    public String getDisplayed()
    {
        return displayed;
    }

    /**
     * Accesseur de l attribut image.
     * 
     * @return image
     */
    public String getImage()
    {
        return image;
    }

    /**
     * Accesseur de l attribut imagesrepertoire.
     * 
     * @return imagesrepertoire
     */
    public String getImagesrepertoire()
    {
        return imagesrepertoire;
    }

    /**
     * Accesseur de l attribut label.
     * 
     * @return label
     */
    public String getLabel()
    {
        return label;
    }

    /**
     * Accesseur de l attribut openwindow.
     * 
     * @return openwindow
     */
    public String getOpenwindow()
    {
        return openwindow;
    }

    /**
     * Accesseur de l attribut title.
     * 
     * @return title
     */
    public String getTitle()
    {
        return title;
    }

    /**
     * Accesseur de l attribut transition.
     * 
     * @return transition
     */
    public String getTransition()
    {
        return transition;
    }

    /**
     * Accesseur de l attribut uri.
     * 
     * @return uri
     */
    public String getUri()
    {
        return uri;
    }

    /**
     * Accesseur de l attribut value.
     * 
     * @return value
     */
    public String getValue()
    {
        return value;
    }

    /**
     * Accesseur de l attribut waitingmsg.
     * 
     * @return waitingmsg
     */
    public String getWaitingmsg()
    {
        return waitingmsg;
    }

    /**
     * Modificateur de l attribut aclass.
     * 
     * @param aclass le nouveau aclass
     */
    public void setAclass(String aclass)
    {
        this.aclass = aclass;
    }

    /**
     * Modificateur de l attribut action.
     * 
     * @param action le nouveau action
     */
    public void setAction(String action)
    {
        this.action = action;
    }

    /**
     * Modificateur de l attribut active.
     * 
     * @param active le nouveau active
     */
    public void setActive(String active)
    {
        this.active = active;
    }

    /**
     * Modificateur de l attribut alt.
     * 
     * @param alt le nouveau alt
     */
    public void setAlt(String alt)
    {
        this.alt = alt;
    }

    /**
     * Modificateur de l attribut attribute.
     * 
     * @param attribute le nouveau attribute
     */
    public void setAttribute(String attribute)
    {
        this.attribute = attribute;
    }

    /**
     * Modificateur de l attribut displayed.
     * 
     * @param displayed le nouveau displayed
     */
    public void setDisplayed(String displayed)
    {
        this.displayed = displayed;
    }

    /**
     * Modificateur de l attribut image.
     * 
     * @param image le nouveau image
     */
    public void setImage(String image)
    {
        this.image = image;
    }

    /**
     * Modificateur de l attribut imagesrepertoire.
     * 
     * @param imagesrepertoire le nouveau imagesrepertoire
     */
    public void setImagesrepertoire(String imagesrepertoire)
    {
        this.imagesrepertoire = imagesrepertoire;
    }

    /**
     * Modificateur de l attribut label.
     * 
     * @param label le nouveau label
     */
    public void setLabel(String label)
    {
        this.label = label;
    }

    /**
     * Modificateur de l attribut openwindow.
     * 
     * @param openwindow le nouveau openwindow
     */
    public void setOpenwindow(String openwindow)
    {
        this.openwindow = openwindow;
    }

    /**
     * Modificateur de l attribut title.
     * 
     * @param title le nouveau title
     */
    public void setTitle(String title)
    {
        this.title = title;
    }

    /**
     * Modificateur de l attribut transition.
     * 
     * @param transition le nouveau transition
     */
    public void setTransition(String transition)
    {
        this.transition = transition;
    }

    /**
     * Modificateur de l attribut uri.
     * 
     * @param uri le nouveau uri
     */
    public void setUri(String uri)
    {
        this.uri = uri;
    }

    /**
     * Modificateur de l attribut value.
     * 
     * @param value le nouveau value
     */
    public void setValue(String value)
    {
        this.value = value;
    }

    /**
     * Modificateur de l attribut waitingmsg.
     * 
     * @param waitingmsg le nouveau waitingmsg
     */
    public void setWaitingmsg(String waitingmsg)
    {
        this.waitingmsg = waitingmsg;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param tagWriter
     * @return int
     * @throws JspException the jsp exception
     * @see fr.gouv.finances.lombok.apptags.base.BaseAppTags#writeTagContent(fr.gouv.finances.lombok.apptags.base.TagWriter)
     */
    @Override
    protected int writeTagContent(TagWriter tagWriter) throws JspException
    {
        this.initAttributes();

        String contextPath = this.getRequestContext().getContextPath();

        String href = this.contruithref();

        // le lien est actif
        if (isTRUE(this._active))
        {
            tagWriter.startTag(A_TAG);
            tagWriter.writeOptionalAttributeValue(TITLE_ATTRIBUTE, this.title);
            String hrefMod = this.metAJourHrefParModeDemande(href, this.transition);
            tagWriter.writeAttribute(HREF_ATTRIBUTE, hrefMod);
            tagWriter.writeOptionalAttributeValue(CLASS_ATTRIBUTE, this._aclass);
        }

        if (!isEmpty(this.image))
        {
            tagWriter.startTag(IMG_TAG);
            tagWriter.writeOptionalAttributeValue(TITLE_ATTRIBUTE, this.title);

            StringBuilder   src = new StringBuilder();
            src.append(contextPath);
            src.append(SLASH);
            src.append(this._imagesrepertoire);
            src.append(SLASH);
            src.append(this.image);
            tagWriter.writeAttribute(SRC_ATTRIBUTE, src.toString());
            tagWriter.writeOptionalAttributeValue(ALT_ATTRIBUTE, this.alt);
            tagWriter.writeOptionalAttributeValue(STYLE_ATTRIBUTE, "border:0;");
            tagWriter.endTag();
        }
        if (!isEmpty(this.label))
        {
            tagWriter.appendValue(this.label);
        }

        // Le lien est actif
        if (isTRUE(this._active))
        {
            tagWriter.forceBlock();
        }

        this.tagWriter = tagWriter;

        return EVAL_BODY_INCLUDE;
    }

    /**
     * methode Contruithref : --.
     * 
     * @return string
     * @throws JspException the jsp exception
     */
    private String contruithref() throws JspException
    {
        StringBuilder   href = new StringBuilder();
        String contextPath = this.getRequestContext().getContextPath();
        // L'indication du lien est transmise sous la forme action / transition
        // du webflow
        if ((!isEmpty(this.action)) && (!isEmpty(this.transition)))
        {
            String flowExecutinKey = (String) this.getBean(FLOWEXECUTIONID_VALUE, null, null);

            href.append(contextPath);
            href.append(SLASH);
            href.append(this.action);
            href.append('?');
            href.append(FLOWEXECUTIONID_FIELD_NAME);
            href.append('=');
            href.append(flowExecutinKey);
            href.append(AMPERSAND);
            href.append(EVENTID_FIELDURL_PREFIX);
            href.append('=');
            href.append(this.transition);

            if (!isEmpty(this.attribute))
            {
                href.append(AMPERSAND);
                href.append(this.attribute);
                href.append('=');
                href.append(this.value);
            }
        }
        // L'indication du lien est transmise sous la forme d'une action
        else if ((!isEmpty(this.action)) && (isEmpty(this.transition)))
        {
            href.append(contextPath);
            href.append(SLASH);
            href.append(this.action);

            if (!isEmpty(this.attribute))
            {
                href.append('?');
                href.append(this.attribute);
                href.append('=');
                href.append(this.value);
            }
        }
        // L'indication du lien est sous la forme d'une URL
        else if (!isEmpty(this.uri))
        {
            href.append(this.uri);
        }

        return href.toString();
    }

    /**
     * methode Inits the attributes : --.
     * 
     * @throws JspException the jsp exception
     */
    private void initAttributes() throws JspException
    {
        this._displayed = this.initvar(this.displayed, "app.nodelink.displayed", "true");
        this._imagesrepertoire =
            this.initvar(this.imagesrepertoire, "app.page.imagesrepertoire", "/composants/tagsapp/images/");
        this._aclass = this.initvar(this.aclass, "app.nodelink.aclass", "lien");
        this._openwindow = this.initvar(this.openwindow, "app.nodelink.openwindow", "false");
        this._openwindowname = this.initvar(this.openwindowname, "app.nodelink.openwindowname", "");
        this._openwindowoptions =
            this
                .initvar(
                    this.openwindowoptions,
                    "app.nodelink.openwindowoptions",
                    "toolbar=yes, directories=no, location=no, status=yes, menubar=no, resizable=yes, scrollbars=yes, width=500, height=400");
        this._active = this.initvar(this.active, "app.nodelink.active", "true");
        this._waitingmsg = this.initvar(this.waitingmsg, "app.nodelink.waitingmsg", "false");
    }

    /**
     * methode Met a jour href par mode demande : --.
     * 
     * @param href --
     * @param transition --
     * @return string
     */
    private String metAJourHrefParModeDemande(String href, String transition)
    {
        StringBuilder   hrefMod = new StringBuilder();

        if (!isEmpty(transition))
        {
            hrefMod.append("javascript:stocketransition('");
            hrefMod.append(transition);
            hrefMod.append("');");
        }

        if (this.isTRUE(this._openwindow))
        {
            hrefMod.append("javascript:send_url_openwindow('");
            hrefMod.append(href);
            hrefMod.append("','");
            hrefMod.append(this._openwindowname);
            hrefMod.append("','");
            hrefMod.append(this._openwindowoptions);
            hrefMod.append("')");
        }
        else if (this.isTRUE(this._waitingmsg))
        {
            hrefMod.append("javascript:send_url_waiting('");
            hrefMod.append(href);
            hrefMod.append("')");
        }
        else
        {
            hrefMod.append("send_url('");
            hrefMod.append(href);
            hrefMod.append("');");
        }

        return hrefMod.toString();
    }

}
