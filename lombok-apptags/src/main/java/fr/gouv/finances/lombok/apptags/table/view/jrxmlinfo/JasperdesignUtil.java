/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.apptags.table.view.jrxmlinfo;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import fr.gouv.finances.lombok.apptags.table.core.TableModel;
import fr.gouv.finances.lombok.apptags.util.EcTableException;
import net.sf.jasperreports.engine.DefaultJasperReportsContext;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRImageRenderer;
import net.sf.jasperreports.engine.JRRenderable;
import net.sf.jasperreports.engine.JRStyle;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.base.JRBaseStyle;
import net.sf.jasperreports.engine.design.JRDesignBand;
import net.sf.jasperreports.engine.design.JRDesignLine;
import net.sf.jasperreports.engine.design.JRDesignParameter;
import net.sf.jasperreports.engine.design.JRDesignStyle;
import net.sf.jasperreports.engine.design.JRDesignTextField;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.fonts.FontUtil;
import net.sf.jasperreports.engine.type.HorizontalAlignEnum;
import net.sf.jasperreports.engine.type.LineSpacingEnum;
import net.sf.jasperreports.engine.type.LineStyleEnum;
import net.sf.jasperreports.engine.type.PositionTypeEnum;
import net.sf.jasperreports.engine.type.StretchTypeEnum;
import net.sf.jasperreports.engine.type.VerticalAlignEnum;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.engine.util.JRStyledText;

/**
 * Class JasperdesignUtil
 */
public class JasperdesignUtil
{

    /** Constant : log. */
    private static final Log log = LogFactory.getLog(JasperdesignUtil.class);

    /** Constant : MIN_FONTSIZE. */
    private static final int MIN_FONTSIZE = 7;

    /**
     * methode Creates the jasper jasper report parameter :
     * 
     * @param name name
     * @return jR design parameter
     */
    public static JRDesignParameter createJasperJasperReportParameter(String name)
    {
        return createJasperParameter(name, JasperReport.class);
    }

    /**
     * methode Creates the jasper collection parameter :
     * 
     * @param name name
     * @return jR design parameter
     */
    public static JRDesignParameter createJasperCollectionParameter(String name)
    {
        return createJasperParameter(name, Collection.class);
    }

    /**
     * methode Creates the jasper string parameter :
     * 
     * @param name name
     * @return jR design parameter
     */
    public static JRDesignParameter createJasperStringParameter(String name)
    {
        return createJasperParameter(name, java.lang.String.class);
    }

    /**
     * Construit la chaîne de lecture d'un paramètre jasperreport.
     * 
     * @param key key
     * @return string
     */
    public static String buildJasperParam(String key)
    {
        StringBuilder   result = new StringBuilder();
        result.append("$P{");
        result.append(key);
        result.append('}');
        return result.toString();
    }

    /**
     * Créer un style de présentation.
     * 
     * @param jasperDesign jasperDesign
     * @param name name
     * @param bandInfo bandInfo
     * @param fontStyleInfo fontStyleInfo
     * @param horizontalAlignment horizontalAlignment
     * @param verticalAlignment verticalAlignment
     * @param pattern pattern
     * @return jR style
     */
    public static JRStyle creerStyleComplet(JasperDesign jasperDesign, String name, AbstractBandInfo bandInfo,
        FontStyleInfo fontStyleInfo, HorizontalAlignEnum horizontalAlignment, VerticalAlignEnum verticalAlignment, String pattern)
    {
        JRStyle jrStyle = creerStyle(jasperDesign, name, fontStyleInfo);
        // JRBaseLineBox
        jrStyle.getLineBox().setTopPadding(bandInfo.getTopPadding());
        jrStyle.getLineBox().setBottomPadding(bandInfo.getBottomPadding());
        jrStyle.getLineBox().setRightPadding(bandInfo.getRightPadding());
        jrStyle.getLineBox().setLeftPadding(bandInfo.getLeftPadding());

        if (bandInfo.getLineSpacing() == null)
        {
            jrStyle.setLineSpacing(LineSpacingEnum.SINGLE);
        }
        else
        {
            jrStyle.setLineSpacing(bandInfo.getLineSpacing());
        }

        if (pattern != null)
        {
            jrStyle.setPattern(pattern);
        }

        if (horizontalAlignment == null)
        {
            jrStyle.setHorizontalAlignment(HorizontalAlignEnum.CENTER);
        }
        else
        {
            jrStyle.setHorizontalAlignment(horizontalAlignment);
        }

        if (verticalAlignment == null)
        {
            jrStyle.setVerticalAlignment(VerticalAlignEnum.MIDDLE);
        }
        else
        {
            jrStyle.setVerticalAlignment(verticalAlignment);
        }
        return jrStyle;
    }

    /**
     * methode Creer style complet :
     * 
     * @param jasperDesign jasperDesign
     * @param name name
     * @param fontStyleInfo fontStyleInfo
     * @param topPadding topPadding
     * @param bottomPadding bottomPadding
     * @param rightPadding rightPadding
     * @param leftPadding leftPadding
     * @param horizontalAlignment horizontalAlignment
     * @param verticalAlignment verticalAlignment
     * @return jR style
     */
    public static JRStyle creerStyleComplet(JasperDesign jasperDesign, String name, FontStyleInfo fontStyleInfo,
        int topPadding, int bottomPadding, int rightPadding, int leftPadding, HorizontalAlignEnum horizontalAlignment,
        VerticalAlignEnum verticalAlignment)
    {
        JRStyle jrStyle = creerStyle(jasperDesign, name, fontStyleInfo);

        jrStyle.getLineBox().setTopPadding(topPadding);
        jrStyle.getLineBox().setBottomPadding(bottomPadding);
        jrStyle.getLineBox().setRightPadding(rightPadding);
        jrStyle.getLineBox().setLeftPadding(leftPadding);
        jrStyle.setHorizontalAlignment(horizontalAlignment);
        jrStyle.setVerticalAlignment(verticalAlignment);
        return jrStyle;
    }

    /**
     * Crée un style et l'ajoute au modèle jasper.
     * 
     * @param jasperDesign jasperDesign
     * @param name name
     * @param fontStyleInfo fontStyleInfo
     * @return jR style
     */
    @SuppressWarnings("unchecked")
    public static JRStyle creerStyle(JasperDesign jasperDesign, String name, FontStyleInfo fontStyleInfo)
    {
        Map stylesMap = jasperDesign.getStylesMap();
        JRStyle style;

        if (stylesMap != null && !stylesMap.containsKey(name))
        {
            style = new JRBaseStyle(name);
            style.setBold(fontStyleInfo.isBold());
            style.setFontName(fontStyleInfo.getFontName());
            style.setFontSize(fontStyleInfo.getFontSize());
            style.setPdfFontName(fontStyleInfo.getPdfFontName());
            style.setPdfEmbedded(fontStyleInfo.isPdfEmbedded());

            try
            {
                jasperDesign.addStyle(style);
            }
            catch (JRException stylee)
            {
                log.warn("Erreur lors de l'ajout d'un style", stylee);
                throw new EcTableException(stylee);
            }
        }
        else
        {
            style = (JRStyle) stylesMap.get(name);
        }
        return style;
    }

    /**
     * Crée un style.
     * 
     * @param fontStyleInfo fontStyleInfo
     * @return jR style
     */
    public static JRStyle creerStyle(FontStyleInfo fontStyleInfo)
    {
        JRStyle style = new JRDesignStyle();
        style.setBold(fontStyleInfo.isBold());
        style.setFontName(fontStyleInfo.getFontName());
        style.setFontSize(fontStyleInfo.getFontSize());
        style.setPdfFontName(fontStyleInfo.getPdfFontName());
        style.setPdfEmbedded(fontStyleInfo.isPdfEmbedded());
        return style;
    }

    /**
     * methode Creates the jasper jr renderable parameter :
     * 
     * @param name name
     * @return jR design parameter
     */
    public static JRDesignParameter createJasperJRRenderableParameter(String name)
    {
        return createJasperParameter(name, JRRenderable.class);
    }

    /**
     * methode Creates the jasper parameter :
     * 
     * @param name name
     * @param valueClass valueClass
     * @return jR design parameter
     */
    public static JRDesignParameter createJasperParameter(String name, Class<? extends Object> valueClass)
    {
        if (log.isDebugEnabled())
        {
            StringBuilder   msg = new StringBuilder();
            msg.append("Paramètre Jasper ajouté au modèle :");
            msg.append("(name =");
            msg.append(name);
            msg.append(";valueclass");
            msg.append(valueClass.toString());
            log.debug(msg.toString());
        }

        JRDesignParameter parameter = new JRDesignParameter();
        parameter.setName(name);
        parameter.setValueClass(valueClass);
        return parameter;
    }

    /**
     * Charge une image soit dans le répertoire imageBasePath, soit dans le classpath imageBaseClassPath.
     * 
     * @param imageBasePath imageBasePath
     * @param imageBaseClasspath imageBaseClasspath
     * @param imageName imageName
     * @return jR image renderer
     */
    public static JRImageRenderer chargeImage(String imageBasePath, String imageBaseClasspath, String imageName)
    {
        JRImageRenderer image = null;

        // On recherche tout d'abord l'image dans l'arborescence webapp
        StringBuilder   imageFullPath = new StringBuilder();
        imageFullPath.append(imageBasePath);
        imageFullPath.append("/");
        imageFullPath.append(imageName);
        try
        {
            // image = JRImageRenderer.getInstance(JRLoader.loadBytesFromLocation(imageFullPath.toString()));
            // 5.6.0
            image = JRImageRenderer.getInstance(JRLoader.loadBytesFromResource(imageFullPath.toString()));
        }
        catch (JRException exception)
        {
            // Si l'image n'est pas trouvé dans cette arborescence, on la
            // cherche dans le classpath
            imageFullPath.delete(0, imageFullPath.length());
            imageFullPath.append(imageBaseClasspath);
            imageFullPath.append("/");
            imageFullPath.append(imageName);

            try
            {
                image = JRImageRenderer.getInstance(JRLoader.loadBytesFromResource(imageFullPath.toString()));
            }
            catch (JRException excep)
            {
                log.warn("FICHIER NON TROUVE", exception);
                log.warn("FICHIER NON TROUVE", excep);
            }
        }
        return image;
    }

    /**
     * Calcule la hauteur du champ en fonction du texte qu'il contient et des caractéristiques de présentation du champ.
     * 
     * @param textField textField
     * @param text text
     * @param maxHeight maxHeight
     * @return taille du champ
     */
    @SuppressWarnings("unchecked")
    public static int calculeHauteurChamp(JRDesignTextField textField, String text, int maxHeight)
    {
        JRStyledText styledText = new JRStyledText();
        Map styledTextAttributes = new HashMap();

        String textAPlacer;

        if (text == null)
        {
            textAPlacer = "";
        }
        else
        {
            textAPlacer = text;
        }

        FontUtil.getInstance(DefaultJasperReportsContext.getInstance()).getAttributesWithoutAwtFont(styledTextAttributes, textField);
        styledText.addRun(new JRStyledText.Run(styledTextAttributes, 0, textAPlacer.length()));
        styledText.append(text);

        // Affectation de la hauteur max autorisée pour le champ titre
        textField.setHeight(maxHeight);

        ParagraphMeasurer textMeasurer = new ParagraphMeasurer(textField);

        int paragraphTitreHeight = textMeasurer.measure(styledText, textAPlacer, 0, 0) + 1;

        int fontSize = textField.getStyle().getFontSize().intValue();

        // Tant que la taille du texte est supérieure à la taille maxi
        // autorisée
        // pour le champ et la taille de la police > à la plus petite
        // police
        // autorisée, on réduit la police du champ.
        while (textMeasurer.isMaxHeightReached() && fontSize > MIN_FONTSIZE)
        {
            if (fontSize > MIN_FONTSIZE)
            {
                fontSize--;
            }

            textField.getStyle().setFontSize(fontSize);
            styledText = new JRStyledText();
            styledTextAttributes = new HashMap();

            FontUtil.getInstance(DefaultJasperReportsContext.getInstance()).getAttributesWithoutAwtFont(styledTextAttributes, textField);
            styledText.addRun(new JRStyledText.Run(styledTextAttributes, 0, textAPlacer.length()));
            styledText.append(textAPlacer);
            paragraphTitreHeight = textMeasurer.measure(styledText, textAPlacer, 0, 0);
        }

        return paragraphTitreHeight;
    }

    /**
     * methode Calcule la hauteur d'un textfield en fonction du nombre de ligne et de la taille de la police.
     * 
     * @param textField textField
     * @param text text
     * @return int
     */
    public static int calculeHauteurTexte(JRDesignTextField textField, String text)
    {
        JRStyledText styledText = new JRStyledText();
        Map styledTextAttributes = new HashMap();

        String textAPlacer;

        if (text == null)
        {
            textAPlacer = "";
        }
        else
        {
            textAPlacer = text;
        }

        FontUtil.getInstance(DefaultJasperReportsContext.getInstance()).getAttributesWithoutAwtFont(styledTextAttributes, textField);
        styledText.addRun(new JRStyledText.Run(styledTextAttributes, 0, textAPlacer.length()));
        styledText.append(text);

        ParagraphMeasurer textMeasurer = new ParagraphMeasurer(textField);

        return textMeasurer.measureSansMaxHeightReached(styledText, textAPlacer, 0, 0) + 1;
    }

    /**
     * Ajout d'une ligne verticale à une section.
     * 
     * @param posX abscisse de l'extrêmité gauche de la ligne
     * @param posY ordonnée de l'extrêmité gauche de la ligne
     * @param width longueur d'un segment horizontal
     * @param height longueur d'un segment vertical
     * @param positionType mode de positionnement (ex :JRElement.POSITION_TYPE_FLOAT)
     * @param stretchType mode d'extension (ex : JRElement.STRETCH_TYPE_RELATIVE_TO_TALLEST_OBJECT )
     * @param lineWidth lineWidth
     * @return jR design line
     */
    public static JRDesignLine ajouterLigneVerticale(int posX, int posY, int width, int height, PositionTypeEnum positionType,
        StretchTypeEnum stretchType, Float lineWidth)
    {
        JRDesignLine line = new JRDesignLine();
        line.setX(posX);
        line.setY(posY);
        line.setWidth(width);
        line.setHeight(height);
        line.setPositionType(positionType);
        line.setStretchType(stretchType);
        line.getLinePen().setLineWidth(lineWidth);
        line.getLinePen().setLineStyle(LineStyleEnum.SOLID);
        return line;
    }

    /**
     * Ajout d'une ligne horizontale à une section.
     * 
     * @param posX abscisse de l'extrêmité gauche de la ligne
     * @param posY ordonnée de l'extrêmité gauche de la ligne
     * @param width longueur d'un segment horizontal
     * @param height longueur d'un segment vertical
     * @param positionType (mode de positionnement (ex :JRElement.POSITION_TYPE_FLOAT)
     * @param lineWidth lineWidth
     * @return jR design line
     */
    public static JRDesignLine ajouterLigneHorizontale(int posX, int posY, int width, int height, PositionTypeEnum positionType,
        Float lineWidth)
    {
        JRDesignLine line = new JRDesignLine();
        line.setX(posX);
        line.setY(posY);
        line.setWidth(width);
        line.setHeight(height);
        line.getLinePen().setLineWidth(lineWidth);
        line.getLinePen().setLineStyle(LineStyleEnum.SOLID);
        line.setPositionType(positionType);
        return line;
    }

    /**
     * Ajoute un rectangle à une section.
     * 
     * @param jrDesignBand jrDesignBand
     * @param model model
     * @param rectangleInfo rectangleInfo
     * @param lineWidth lineWidth
     */
    public static void ajouterRectangle(JRDesignBand jrDesignBand, TableModel model, RectangleInfo rectangleInfo, Float lineWidth)
    {
        if (rectangleInfo.isDrawBorder() && rectangleInfo.getWidth() > 0 && rectangleInfo.getHeight() > 0)
        {
            // Ligne horizontale supérieure
            jrDesignBand.addElement(JasperdesignUtil.ajouterLigneHorizontale(rectangleInfo.getXleftcorner(),
                rectangleInfo.getYleftcorner(), rectangleInfo.getWidth(), 0,
                PositionTypeEnum.FIX_RELATIVE_TO_TOP, lineWidth));

            // Ligne horizontale inférieure
            jrDesignBand.addElement(JasperdesignUtil.ajouterLigneHorizontale(rectangleInfo.getXleftcorner(),
                rectangleInfo.getYleftcorner() + rectangleInfo.getHeight(), rectangleInfo.getWidth() + 1, 0,
                PositionTypeEnum.FIX_RELATIVE_TO_TOP, lineWidth));

            // Ligne verticale gauche
            jrDesignBand.addElement(JasperdesignUtil.ajouterLigneVerticale(rectangleInfo.getXleftcorner(),
                rectangleInfo.getYleftcorner(), 0, rectangleInfo.getHeight(),
                PositionTypeEnum.FIX_RELATIVE_TO_TOP, StretchTypeEnum.NO_STRETCH, lineWidth));

            // Ligne verticale droite
            jrDesignBand.addElement(JasperdesignUtil.ajouterLigneVerticale(rectangleInfo.getXleftcorner()
                + rectangleInfo.getWidth(), rectangleInfo.getYleftcorner(), 0, rectangleInfo.getHeight() + 1,
                PositionTypeEnum.FIX_RELATIVE_TO_TOP, StretchTypeEnum.NO_STRETCH, lineWidth));
        }
    }

    // public static JRStyle creerStyleComplet(JasperDesign jasperDesign, String string, DetailInfo detail,
    // FontStyleInfo style,
    // HorizontalAlignEnum alignment, VerticalAlignEnum middle, String pattern)
    // {
    // return null; // "documenté" Raccord de méthode auto-généré
    // }

    // public static JRStyle creerStyleComplet(JasperDesign jasperDesign, String styleCellulePiedColonneEdition,
    // FontStyleInfo style,
    // int cellMargin, int cellMargin2, int cellMargin3, int cellMargin4, byte alignement, VerticalAlignEnum middle)
    // {
    // return null; // "documenté" Raccord de méthode auto-généré
    // }

}
