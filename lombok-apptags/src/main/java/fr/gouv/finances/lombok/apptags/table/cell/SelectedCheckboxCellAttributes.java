/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.apptags.table.cell;

import java.io.Serializable;

import org.apache.commons.lang.StringUtils;

/**
 * Class SelectedCheckboxCellAttributes
 */
public class SelectedCheckboxCellAttributes implements Serializable
{

    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** pattern selected. */
    private String patternSelected;

    /** pattern unselected. */
    private String patternUnselected;

    /** on click javascript. */
    private String onClickJavascript;

    /** on click select all. */
    private String onClickSelectAll;

    /** prefix name hidden. */
    private String prefixNameHidden;

    /** prefix id hidden. */
    private String prefixIdHidden;

    /** prefix id checkbox. */
    private String prefixIdCheckbox;

    /** rowcount sep. */
    private String rowcountSep;

    /** row id. */
    private String rowId;

    /** table id. */
    private String tableId;

    /** selected rowids. */
    private String selectedRowids;

    /** elements in flow. */
    private String elementsInFlow;

    /** selected in flow. */
    private String selectedInFlow;

    /** msg error in flow. */
    private String msgErrorInFlow;

    /** msg not selected. */
    private String msgNotSelected;

    /** msg not many. */
    private String msgNotMany;

    /** checkboxattr storage scope. */
    private int checkboxattrStorageScope;

    /** selected rowids storage scope. */
    private int selectedRowidsStorageScope;

    /** selected elements storage scope. */
    private int selectedElementsStorageScope;

    /** msg storage scope. */
    private int msgStorageScope;

    /**
     * Instanciation de selected checkbox cell attributes.
     */
    public SelectedCheckboxCellAttributes()
    {
        super();
    }

    /**
     * Accesseur de l attribut msg storage scope.
     * 
     * @return msg storage scope
     */
    public int getMsgStorageScope()
    {
        return msgStorageScope;
    }

    /**
     * Accesseur de l attribut selected elements storage scope.
     * 
     * @return selected elements storage scope
     */
    public int getSelectedElementsStorageScope()
    {
        return selectedElementsStorageScope;
    }

    /**
     * Accesseur de l attribut selected rowids storage scope.
     * 
     * @return selected rowids storage scope
     */
    public int getSelectedRowidsStorageScope()
    {
        return selectedRowidsStorageScope;
    }

    /**
     * Modificateur de l attribut selected rowids storage scope.
     * 
     * @param selectedRowidsStorageScope le nouveau selected rowids storage scope
     */
    public void setSelectedRowidsStorageScope(int selectedRowidsStorageScope)
    {
        this.selectedRowidsStorageScope = selectedRowidsStorageScope;
    }

    /**
     * Accesseur de l attribut checkboxattr storage scope.
     * 
     * @return checkboxattr storage scope
     */
    public int getCheckboxattrStorageScope()
    {
        return checkboxattrStorageScope;
    }

    /**
     * Instanciation de selected checkbox cell attributes.
     * 
     * @param tableId
     * @param rowIdAttribute
     */
    public SelectedCheckboxCellAttributes(String tableId, String rowIdAttribute)
    {
        this(tableId, rowIdAttribute, PreferencesConstants.DEFAULT_CHEXBOXATTR_STORAGE_SCOPE);
    }

    /**
     * Instanciation de selected checkbox cell attributes.
     * 
     * @param tableId
     * @param rowIdAttribute
     * @param storageScope
     */
    public SelectedCheckboxCellAttributes(String tableId, String rowIdAttribute, int storageScope)
    {

        // Rowid
        if (rowIdAttribute == null || StringUtils.isBlank(rowIdAttribute))
        {
            this.rowId = PreferencesConstants.ROWID;
        }
        else
        {
            this.rowId = rowIdAttribute;
        }

        // tableId
        if (tableId == null || StringUtils.isBlank(tableId))
        {
            this.tableId = PreferencesConstants.TABLE_ID;
        }
        else
        {
            this.tableId = tableId;
        }

        // checkboxattrStorageScope
        if (storageScope != PreferencesConstants.FLOW_SCOPE && storageScope != PreferencesConstants.CONVERSATION_SCOPE)
        {
            this.checkboxattrStorageScope = PreferencesConstants.FLOW_SCOPE;
        }
        else
        {
            this.checkboxattrStorageScope = storageScope;
        }

        // Nom dans le scope de la liste des rowids des éléments
        // sélectionnés
        this.selectedRowids = this.tableId + "_" + PreferencesConstants.SELECTED_ROWIDS;

        // Scope de stockage de la liste des rowids des éléments
        // sélectionnés
        this.selectedRowidsStorageScope = PreferencesConstants.DEFAULT_CHEXBOXATTR_STORAGE_SCOPE;

        // Scope de stockage du ou des éléments sélectionnés
        this.selectedElementsStorageScope = PreferencesConstants.DEFAULT_SELECTED_STORAGE_SCOPE;

        // Nom dans le sous flux de la liste complète des sous éléments
        this.elementsInFlow = PreferencesConstants.ELEMENTS_IN_FLOW;

        // Messages
        this.msgErrorInFlow = PreferencesConstants.MSGERREUR;
        this.msgNotSelected = PreferencesConstants.MSG_NOT_SELECTED;
        this.msgNotMany = PreferencesConstants.MSG_NOT_MANY;

        // Scope de stockage des messages d'erreur
        this.msgStorageScope = PreferencesConstants.DEFAUT_MSG_STORAGE_SCOPE;

        this.prefixNameHidden = this.tableId + "_" + PreferencesConstants.PREFIX_HIDDEN;
        this.prefixIdCheckbox = this.tableId + "_" + PreferencesConstants.PREF_ID_CHECKBOX;
        this.prefixIdHidden = this.tableId + "_" + PreferencesConstants.PRE_ID_HIDDEN;
        this.rowcountSep = PreferencesConstants.ROWCOUNT_SEP;

        this.selectedInFlow = this.tableId + "_" + PreferencesConstants.SELECTED_IN_FLOW;

        this.patternSelected = PreferencesConstants.PATTERNSELECTED;
        this.patternUnselected = PreferencesConstants.PATTERNUNSELECTED;
        this.onClickJavascript = PreferencesConstants.ONCLICKJAVASCRIPT;
        this.onClickSelectAll = PreferencesConstants.ONCLICKSELECTALL;
    }

    /**
     * Accesseur de l attribut default.
     * 
     * @param tableId
     * @return default
     */
    public static SelectedCheckboxCellAttributes getDefault(final String tableId)
    {
        return new SelectedCheckboxCellAttributes(tableId, null);
    }

    /**
     * Accesseur de l attribut default.
     * 
     * @return default
     */
    public static SelectedCheckboxCellAttributes getDefault()
    {
        return new SelectedCheckboxCellAttributes(null, null);
    }

    /**
     * methode Utiliser liste elements :
     * 
     * @param elementsInFlow
     * @return selected checkbox cell attributes
     */
    public SelectedCheckboxCellAttributes utiliserListeElements(final String elementsInFlow)
    {
        this.elementsInFlow = elementsInFlow;
        return this;
    }

    /**
     * methode Utiliser rowid :
     * 
     * @param rowid
     * @return selected checkbox cell attributes
     */
    public SelectedCheckboxCellAttributes utiliserRowid(final String rowid)
    {
        this.rowId = rowid;
        return this;
    }

    /**
     * methode Stocker les row id des elements selectionnes dans request scope :
     * 
     * @return selected checkbox cell attributes
     */
    public SelectedCheckboxCellAttributes stockerLesRowIdDesElementsSelectionnesDansRequestScope()
    {
        this.selectedRowidsStorageScope = PreferencesConstants.REQUEST_SCOPE;
        return this;
    }

    /**
     * methode Stocker les row id des elements selectionnes dans flow scope :
     * 
     * @return selected checkbox cell attributes
     */
    public SelectedCheckboxCellAttributes stockerLesRowIdDesElementsSelectionnesDansFlowScope()
    {
        this.selectedRowidsStorageScope = PreferencesConstants.FLOW_SCOPE;
        return this;
    }

    /**
     * methode Stocker les row id des elements selectionnes dans conversation scope :
     * 
     * @return selected checkbox cell attributes
     */
    public SelectedCheckboxCellAttributes stockerLesRowIdDesElementsSelectionnesDansConversationScope()
    {
        this.selectedRowidsStorageScope = PreferencesConstants.CONVERSATION_SCOPE;
        return this;
    }

    /**
     * methode Stocker les elements selectionnes dans request scope :
     * 
     * @return selected checkbox cell attributes
     */
    public SelectedCheckboxCellAttributes stockerLesElementsSelectionnesDansRequestScope()
    {
        this.selectedElementsStorageScope = PreferencesConstants.REQUEST_SCOPE;
        return this;
    }

    /**
     * methode Stocker les elements selectionnes dans flow scope :
     * 
     * @return selected checkbox cell attributes
     */
    public SelectedCheckboxCellAttributes stockerLesElementsSelectionnesDansFlowScope()
    {
        this.selectedElementsStorageScope = PreferencesConstants.FLOW_SCOPE;
        return this;
    }

    /**
     * methode Stocker les elements selectionnes dans conversation scope :
     * 
     * @return selected checkbox cell attributes
     */
    public SelectedCheckboxCellAttributes stockerLesElementsSelectionnesDansConversationScope()
    {
        this.selectedElementsStorageScope = PreferencesConstants.CONVERSATION_SCOPE;
        return this;
    }

    /**
     * methode Stocker les messages dans request scope :
     * 
     * @return selected checkbox cell attributes
     */
    public SelectedCheckboxCellAttributes stockerLesMessagesDansRequestScope()
    {
        this.msgStorageScope = PreferencesConstants.REQUEST_SCOPE;
        return this;
    }

    /**
     * methode Stocker les messages flow scope :
     * 
     * @return selected checkbox cell attributes
     */
    public SelectedCheckboxCellAttributes stockerLesMessagesFlowScope()
    {
        this.msgStorageScope = PreferencesConstants.FLOW_SCOPE;
        return this;
    }

    /**
     * methode Stocker les messages dans conversation scope :
     * 
     * @return selected checkbox cell attributes
     */
    public SelectedCheckboxCellAttributes stockerLesMessagesDansConversationScope()
    {
        this.msgStorageScope = PreferencesConstants.CONVERSATION_SCOPE;
        return this;
    }

    /**
     * methode Utiliser msg aucun elt selectionne :
     * 
     * @param msg
     * @return selected checkbox cell attributes
     */
    public SelectedCheckboxCellAttributes utiliserMsgAucunEltSelectionne(final String msg)
    {
        this.msgNotSelected = msg;
        return this;
    }

    /**
     * methode Utiliser msg selectionne un seul element :
     * 
     * @param msg
     * @return selected checkbox cell attributes
     */
    public SelectedCheckboxCellAttributes utiliserMsgSelectionneUnSeulElement(final String msg)
    {
        this.msgNotMany = msg;
        return this;
    }

    /**
     * Accesseur de l attribut on click select all.
     * 
     * @return on click select all
     */
    public String getOnClickSelectAll()
    {
        return onClickSelectAll;
    }

    /**
     * Modificateur de l attribut on click select all.
     * 
     * @param onClickSelectAll le nouveau on click select all
     */
    public void setOnClickSelectAll(String onClickSelectAll)
    {
        this.onClickSelectAll = onClickSelectAll;
    }

    /**
     * Accesseur de l attribut prefix id checkbox.
     * 
     * @return prefix id checkbox
     */
    public String getPrefixIdCheckbox()
    {
        return prefixIdCheckbox;
    }

    /**
     * Modificateur de l attribut prefix id checkbox.
     * 
     * @param prefixIdCheckbox le nouveau prefix id checkbox
     */
    public void setPrefixIdCheckbox(String prefixIdCheckbox)
    {
        this.prefixIdCheckbox = prefixIdCheckbox;
    }

    /**
     * Accesseur de l attribut prefix id hidden.
     * 
     * @return prefix id hidden
     */
    public String getPrefixIdHidden()
    {
        return prefixIdHidden;
    }

    /**
     * Modificateur de l attribut prefix id hidden.
     * 
     * @param prefixIdHiddenField le nouveau prefix id hidden
     */
    public void setPrefixIdHidden(String prefixIdHiddenField)
    {
        this.prefixIdHidden = prefixIdHiddenField;
    }

    /**
     * Accesseur de l attribut row id.
     * 
     * @return row id
     */
    public String getRowId()
    {
        return rowId;
    }

    /**
     * Modificateur de l attribut row id.
     * 
     * @param rowId le nouveau row id
     */
    public void setRowId(String rowId)
    {
        this.rowId = rowId;
    }

    /**
     * Accesseur de l attribut on click javascript.
     * 
     * @return on click javascript
     */
    public String getOnClickJavascript()
    {
        return onClickJavascript;
    }

    /**
     * Modificateur de l attribut on click javascript.
     * 
     * @param onClickJavascript le nouveau on click javascript
     */
    public void setOnClickJavascript(String onClickJavascript)
    {
        this.onClickJavascript = onClickJavascript;
    }

    /**
     * Accesseur de l attribut pattern selected.
     * 
     * @return pattern selected
     */
    public String getPatternSelected()
    {
        return patternSelected;
    }

    /**
     * Modificateur de l attribut pattern selected.
     * 
     * @param patternSelected le nouveau pattern selected
     */
    public void setPatternSelected(String patternSelected)
    {
        this.patternSelected = patternSelected;
    }

    /**
     * Accesseur de l attribut pattern unselected.
     * 
     * @return pattern unselected
     */
    public String getPatternUnselected()
    {
        return patternUnselected;
    }

    /**
     * Modificateur de l attribut pattern unselected.
     * 
     * @param patternUnselected le nouveau pattern unselected
     */
    public void setPatternUnselected(String patternUnselected)
    {
        this.patternUnselected = patternUnselected;
    }

    /**
     * Accesseur de l attribut prefix name hidden.
     * 
     * @return prefix name hidden
     */
    public String getPrefixNameHidden()
    {
        return prefixNameHidden;
    }

    /**
     * Modificateur de l attribut prefix name hidden.
     * 
     * @param prefixNameHidden le nouveau prefix name hidden
     */
    public void setPrefixNameHidden(String prefixNameHidden)
    {
        this.prefixNameHidden = prefixNameHidden;
    }

    /**
     * Accesseur de l attribut selected rowids.
     * 
     * @return selected rowids
     */
    public String getSelectedRowids()
    {
        return selectedRowids;
    }

    /**
     * Modificateur de l attribut selected rowids.
     * 
     * @param selectedRowid le nouveau selected rowids
     */
    public void setSelectedRowids(String selectedRowid)
    {
        this.selectedRowids = selectedRowid;
    }

    /**
     * Accesseur de l attribut msg not selected.
     * 
     * @return msg not selected
     */
    public String getMsgNotSelected()
    {
        return msgNotSelected;
    }

    /**
     * Modificateur de l attribut msg not selected.
     * 
     * @param msgNotSelected le nouveau msg not selected
     */
    public void setMsgNotSelected(String msgNotSelected)
    {
        this.msgNotSelected = msgNotSelected;
    }

    /**
     * Accesseur de l attribut msg not many.
     * 
     * @return msg not many
     */
    public String getMsgNotMany()
    {
        return msgNotMany;
    }

    /**
     * Modificateur de l attribut msg not many.
     * 
     * @param msgNotMany le nouveau msg not many
     */
    public void setMsgNotMany(String msgNotMany)
    {
        this.msgNotMany = msgNotMany;
    }

    /**
     * Accesseur de l attribut msg error in flow.
     * 
     * @return msg error in flow
     */
    public String getMsgErrorInFlow()
    {
        return msgErrorInFlow;
    }

    /**
     * Modificateur de l attribut msg error in flow.
     * 
     * @param msgErrorInFlow le nouveau msg error in flow
     */
    public void setMsgErrorInFlow(String msgErrorInFlow)
    {
        this.msgErrorInFlow = msgErrorInFlow;
    }

    /**
     * Accesseur de l attribut selected in flow.
     * 
     * @return selected in flow
     */
    public String getSelectedInFlow()
    {
        return selectedInFlow;
    }

    /**
     * Modificateur de l attribut selected in flow.
     * 
     * @param selectedInFlow le nouveau selected in flow
     */
    public void setSelectedInFlow(String selectedInFlow)
    {
        this.selectedInFlow = selectedInFlow;
    }

    /**
     * Accesseur de l attribut elements in flow.
     * 
     * @return elements in flow
     */
    public String getElementsInFlow()
    {
        return elementsInFlow;
    }

    /**
     * Modificateur de l attribut elements in flow.
     * 
     * @param elementsInFlow le nouveau elements in flow
     */
    public void setElementsInFlow(String elementsInFlow)
    {
        this.elementsInFlow = elementsInFlow;
    }

    /**
     * Accesseur de l attribut table id.
     * 
     * @return table id
     */
    public String getTableId()
    {
        return tableId;
    }

    /**
     * Modificateur de l attribut table id.
     * 
     * @param tableId le nouveau table id
     */
    public void setTableId(String tableId)
    {
        this.tableId = tableId;
    }

    /**
     * Accesseur de l attribut scope name.
     * 
     * @return scope name
     */
    public String getScopeName()
    {
        return this.tableId + PreferencesConstants.CHBX_PARAM_ATTR;
    }

    /**
     * Accesseur de l attribut id hidden field.
     * 
     * @param tableId
     * @return id hidden field
     */
    public static String getIdHiddenField(final String tableId)
    {
        return tableId + "_" + PreferencesConstants.PRE_ID_HIDDEN;
    }

    /**
     * Accesseur de l attribut scope name.
     * 
     * @param tableId
     * @return scope name
     */
    public static String getScopeName(final String tableId)
    {
        return tableId + PreferencesConstants.CHBX_PARAM_ATTR;
    }

    /**
     * Accesseur de l attribut request scope name.
     * 
     * @param tableId
     * @return request scope name
     */
    public static String getRequestScopeName(String tableId)
    {
        return tableId + PreferencesConstants.CHBX_PARAM_ATTR;
    }

    /**
     * Accesseur de l attribut rowcount sep.
     * 
     * @return rowcount sep
     */
    public String getRowcountSep()
    {
        return rowcountSep;
    }
}
