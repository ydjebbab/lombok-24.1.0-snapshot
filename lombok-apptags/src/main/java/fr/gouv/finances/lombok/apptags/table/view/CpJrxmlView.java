/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.apptags.table.view;

import java.awt.Color;
import java.awt.Dimension;
import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.jsp.PageContext;

import org.apache.commons.configuration2.XMLConfiguration;
import org.apache.commons.configuration2.beanutils.BeanDeclaration;
import org.apache.commons.configuration2.beanutils.BeanHelper;
import org.apache.commons.configuration2.beanutils.XMLBeanDeclaration;
import org.apache.commons.configuration2.builder.fluent.Configurations;
import org.apache.commons.configuration2.ex.ConfigurationException;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import fr.gouv.finances.lombok.apptags.table.bean.Column;
import fr.gouv.finances.lombok.apptags.table.bean.ExportFile;
import fr.gouv.finances.lombok.apptags.table.cell.DisplayCell;
import fr.gouv.finances.lombok.apptags.table.cell.PatternCell;
import fr.gouv.finances.lombok.apptags.table.context.JspPageContext;
import fr.gouv.finances.lombok.apptags.table.core.TableModel;
import fr.gouv.finances.lombok.apptags.table.core.TableModelUtils;
import fr.gouv.finances.lombok.apptags.table.view.jrxmlinfo.ColonneInfo;
import fr.gouv.finances.lombok.apptags.table.view.jrxmlinfo.ColumnFooterUtil;
import fr.gouv.finances.lombok.apptags.table.view.jrxmlinfo.ColumnHeaderUtil;
import fr.gouv.finances.lombok.apptags.table.view.jrxmlinfo.DocumentInfo;
import fr.gouv.finances.lombok.apptags.table.view.jrxmlinfo.GraphiqueInfo;
import fr.gouv.finances.lombok.apptags.table.view.jrxmlinfo.JasperdesignUtil;
import fr.gouv.finances.lombok.apptags.table.view.jrxmlinfo.SousRapportInfo;
import fr.gouv.finances.lombok.apptags.util.EcTableException;
import fr.gouv.finances.lombok.apptags.util.format.FormaterDate;
import fr.gouv.finances.lombok.util.spring.ResolveurDeLocalisations;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;
import net.sf.jasperreports.engine.JRImageRenderer;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JRPen;
import net.sf.jasperreports.engine.JRPrintPage;
import net.sf.jasperreports.engine.JRRenderable;
import net.sf.jasperreports.engine.JRStyle;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JRDesignBand;
import net.sf.jasperreports.engine.design.JRDesignExpression;
import net.sf.jasperreports.engine.design.JRDesignField;
import net.sf.jasperreports.engine.design.JRDesignFrame;
import net.sf.jasperreports.engine.design.JRDesignGroup;
import net.sf.jasperreports.engine.design.JRDesignImage;
import net.sf.jasperreports.engine.design.JRDesignParameter;
import net.sf.jasperreports.engine.design.JRDesignRectangle;
import net.sf.jasperreports.engine.design.JRDesignSection;
import net.sf.jasperreports.engine.design.JRDesignSubreport;
import net.sf.jasperreports.engine.design.JRDesignSubreportParameter;
import net.sf.jasperreports.engine.design.JRDesignTextField;
import net.sf.jasperreports.engine.design.JRDesignVariable;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.type.CalculationEnum;
import net.sf.jasperreports.engine.type.EvaluationTimeEnum;
import net.sf.jasperreports.engine.type.HorizontalAlignEnum;
import net.sf.jasperreports.engine.type.LineSpacingEnum;
import net.sf.jasperreports.engine.type.LineStyleEnum;
import net.sf.jasperreports.engine.type.ModeEnum;
import net.sf.jasperreports.engine.type.PositionTypeEnum;
import net.sf.jasperreports.engine.type.ResetTypeEnum;
import net.sf.jasperreports.engine.type.ScaleImageEnum;
import net.sf.jasperreports.engine.type.SplitTypeEnum;
import net.sf.jasperreports.engine.type.StretchTypeEnum;
import net.sf.jasperreports.engine.type.VerticalAlignEnum;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

/**
 * Class CpJrxmlView
 */
public class CpJrxmlView implements View
{

    /** Constant : ATTR_FICHIER_CONF. */
    public static final String ATTR_FICHIER_CONF = "conf";

    /** Constant : ATTR_GRAPHIQUE1. */
    public static final String ATTR_GRAPHIQUE1 = "graphique1";

    /** Constant : ATTR_GRAPHIQUE2. */
    public static final String ATTR_GRAPHIQUE2 = "graphique2";

    /** Constant : ATTR_GRAPHIQUE3. */
    public static final String ATTR_GRAPHIQUE3 = "graphique3";

    /** Constant : ATTR_GRAPHIQUE4. */
    public static final String ATTR_GRAPHIQUE4 = "graphique4";

    /** Constant : ATTR_IMAGE_PATH. */
    public static final String ATTR_IMAGE_PATH = "repertoireimagesetconf";

    /** Constant : ATTR_MODELE. */
    public static final String ATTR_MODELE = "modele";

    /** Constant : ATTR_NOM_APPLI. */
    public static final String ATTR_NOM_APPLI = "nomapplication";

    /** Constant : ATTR_PAGE_DE_GARDE. */
    public static final String ATTR_PAGE_DE_GARDE = "pagedegarde";

    /** Constant : ATTR_SUBREPORT1. */
    public static final String ATTR_SUBREPORT1 = "subreport1";

    /** Constant : ATTR_SUBREPORT2. */
    public static final String ATTR_SUBREPORT2 = "subreport2";

    /** Constant : ATTR_SUBREPORT3. */
    public static final String ATTR_SUBREPORT3 = "subreport3";

    /** Constant : ATTR_SUBREPORT4. */
    public static final String ATTR_SUBREPORT4 = "subreport4";

    /** Constant : ATTR_TEXTE_ENTETE. */
    public static final String ATTR_TEXTE_ENTETE = "texteentete";

    /** Constant : ATTR_TEXTE_PIEDDEPAGE. */
    public static final String ATTR_TEXTE_PIEDDEPAGE = "textepieddepage";

    /** Constant : ATTR_TITRE_EDITION. */
    public static final String ATTR_TITRE_EDITION = "titreedition";

    /** Constant : ATTR_ZOOMRATIO1. */
    public static final String ATTR_ZOOMRATIO1 = "zoomratio1";

    /** Constant : ATTR_ZOOMRATIO2. */
    public static final String ATTR_ZOOMRATIO2 = "zoomratio2";

    /** Constant : ATTR_ZOOMRATIO3. */
    public static final String ATTR_ZOOMRATIO3 = "zoomratio3";

    /** Constant : ATTR_ZOOMRATIO4. */
    public static final String ATTR_ZOOMRATIO4 = "zoomratio4";

    /** Constant : EDITION_IMAGES_ET_CONF_DEF_CLASSPATH. */
    public static final String EDITION_IMAGES_ET_CONF_DEF_CLASSPATH =
        "fr/gouv/finances/lombok/apptags/table/view/jrxmlconfimg";

    /** Constant : CLE_DATE_PRODUCTION_EDITION. */
    private static final String CLE_DATE_PRODUCTION_EDITION = "CleDateProduction";

    /** Constant : CLE_GRAPH_DATA_SOURCE. */
    private static final String CLE_GRAPH_DATA_SOURCE = "CleGraphDataSource";

    /** Constant : CLE_LOGO. */
    private static final String CLE_LOGO = "CleLogo";

    /** Constant : CLE_LOGO_APPLICATION. */
    private static final String CLE_LOGO_APPLICATION = "CleLogoApplication";

    /** Constant : CLE_LOGO_MINISTERE. */
    private static final String CLE_LOGO_MINISTERE = "CleLogoMinistere";

    /** Constant : CLE_NOM_APPLI. */
    private static final String CLE_NOM_APPLI = "CleNomAppli";

    /** Constant : CLE_SUBREPORT_JASPER_REPORT. */
    private static final String CLE_SUBREPORT_JASPER_REPORT = "CleSubreportJasperReport";

    /** Constant : CLE_TEXTE_ENTETE. */
    private static final String CLE_TEXTE_ENTETE = "CleTexteEntete";

    /** Constant : CLE_TEXTE_PIED_DE_PAGE. */
    private static final String CLE_TEXTE_PIED_DE_PAGE = "CleTextePiedDePAge";

    /** Constant : CLE_TITRE_EDITION. */
    private static final String CLE_TITRE_EDITION = "CleTitreEdition";

    /** Constant : DEFAULT_EDITION_IMAGES_ET_CONF_PATH. */
    private static final String DEFAULT_EDITION_IMAGES_ET_CONF_PATH = "/application/exportec";

    /** Constant : DEFAUT_FICHIER_CONF. */
    private static final String DEFAUT_FICHIER_CONF = "documentinfo.xml";

    /** Constant : DEFAUT_MODELE. */
    private static final String DEFAUT_MODELE = "portrait";

    /** Constant : EDITION_IMAGES_ET_CONF_PATH. */
    private static final String EDITION_IMAGES_ET_CONF_PATH = "export.view.cpjrxml.repertoireImagesEtconf";

    /** Constant : GRAPHIQUE_REPORT_NAME. */
    private static final String GRAPHIQUE_REPORT_NAME = "graphiquereport";

    /** Constant : MAIN_REPORT_NAME. */
    private static final String MAIN_REPORT_NAME = "exportec";

    /** Constant : STYLE_CELLULE_TABLEAU_EDITION_PREFIX. */
    private static final String STYLE_CELLULE_TABLEAU_EDITION_PREFIX = "styleCelluleTableauEdition";

    /** Constant : STYLE_CELLULE_TITRE_COLONNE_EDITION. */
    private static final String STYLE_CELLULE_TITRE_COLONNE_EDITION = "styleCelluleTitreColonneEdition";

    /** Constant : STYLE_DATE_DANS_ENTETE_EDITION. */
    private static final String STYLE_DATE_DANS_ENTETE_EDITION = "styleDateDansEnteteEdition";

    /** Constant : STYLE_DATE_DANS_ENTETE_TITLE_EDITION. */
    private static final String STYLE_DATE_DANS_ENTETE_TITLE_EDITION = "styleDateDansEnteteTitleEdition";

    /** Constant : STYLE_DATE_PRODUCTION_EDITION. */
    private static final String STYLE_DATE_PRODUCTION_EDITION = "styleDateProductionEdition";

    /** Constant : STYLE_NOM_APPLICATION_EDITION. */
    private static final String STYLE_NOM_APPLICATION_EDITION = "styleNomApplicationEdition";

    /** Constant : STYLE_NOM_APPLICATION_TITLE_EDITION. */
    private static final String STYLE_NOM_APPLICATION_TITLE_EDITION = "styleNomApplicationTitleEdition";

    /** Constant : STYLE_NUMERO_PAGE_EDITION. */
    private static final String STYLE_NUMERO_PAGE_EDITION = "styleNumeroPageEdition";

    /** Constant : STYLE_TEXTE_COMPLEMENTAIRE_ENTETE_EDITION. */
    private static final String STYLE_TEXTE_COMPLEMENTAIRE_ENTETE_EDITION = "styleTexteComplementaireEnteteEdition";

    /** Constant : STYLE_TEXTE_COMPLEMENTAIRE_ENTETE_TITLE_EDITION. */
    private static final String STYLE_TEXTE_COMPLEMENTAIRE_ENTETE_TITLE_EDITION = "styleTexteComplementaireEnteteTitleEdition";

    /** Constant : STYLE_TEXTE_DANS_PIED_PAGE_EDITION. */
    private static final String STYLE_TEXTE_DANS_PIED_PAGE_EDITION = "styleTextePiedPageEdition";

    /** Constant : STYLE_TITRE_DANS_ENTETE_EDITION. */
    private static final String STYLE_TITRE_DANS_ENTETE_EDITION = "styleTitreDansEnteteEdition";

    /** Constant : STYLE_TITRE_DANS_ENTETE_TITLE_EDITION. */
    private static final String STYLE_TITRE_DANS_ENTETE_TITLE_EDITION = "styleTitreDansEnteteTitleEdition";

    /** Constant : STYLE_TITRE_EDITION. */
    private static final String STYLE_TITRE_EDITION = "styleTitreEdition";

    /** Constant : ZOOMRATIO_DEFAULT. */
    private static final int ZOOMRATIO_DEFAULT = 3;

    /** log. */
    protected final Log log = LogFactory.getLog(getClass());

    /**
     * Instanciation de cp jrxml view.
     */
    public CpJrxmlView()
    {
        super();
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.view.View#afterBody(fr.gouv.finances.lombok.apptags.table.core.TableModel)
     */
    @Override
    public Object afterBody(TableModel model)
    {
        // Lecture du fichier de configuration de l'édition
        DocumentInfo documentInfo = this.configurationEdition(model);

        // Ajustement des dimensions si nécessaire
        documentInfo.ajustementDimensions(model);

        // Complète documentInfo avec les colonnes du tableau ec
        this.analyseColonnesTableEc(model, documentInfo);

        // Prépare la mise en page des graphiques si nécessaire
        this.analyseAttributsGraphiquesTableEc(model, documentInfo);

        // Prépare la mise en page des sous-rapports si nécessaire
        this.analyseAttributsSousRapportTableEc(model, documentInfo);

        // Création du modèle jasper en fonction du modèle
        JasperDesign jasperDesign = this.creationDesign(model, documentInfo);

        // Création des paramètres jasperreports
        this.parametersConf(jasperDesign, model, documentInfo);

        // Configuration de la section title si nécessaire
        if (determineUtilisationPageDeGarde(model, documentInfo))
        {
            jasperDesign.setTitleNewPage(true);
            this.titleBandConf(jasperDesign, model, documentInfo);
        }
        else
        {
            if (documentInfo.getTitle().getHeight() != 0)
            {
                jasperDesign.setTitleNewPage(false);
                this.titleBandConf(jasperDesign, model, documentInfo);
            }
        }

        // Configuration de la section pageHeader
        if (documentInfo.getPageHeader().getHeight() != 0)
        {
            this.pageHeaderBandConf(jasperDesign, model, documentInfo);
        }

        // Création des champs jasperreports
        this.fieldConf(jasperDesign, documentInfo);

        // Configuration de la section columnHeader
        this.columnHeaderBandConf(jasperDesign, model, documentInfo);

        // Configuration de la section detail
        this.detailBandConf(jasperDesign, model, documentInfo);

        // Configuration de la section ColumnFooter
        // modification amlp
        // en cas de présence de graphiques on n'affiche plus les totaux dans le pdf
        // sauf si le graphique est dans le summary (isSummaryNewPageWithPageHeaderAndFooter == true)
        if (documentInfo.getSummaryInfo().getGraphiquesInfos().size() == 0 ||
            (documentInfo.getSummaryInfo().getGraphiquesInfos().size() != 0
                && documentInfo.getSummaryInfo().isSummaryNewPageWithPageHeaderAndFooter()))
        {
            this.columnFooterBandConf(jasperDesign, model, documentInfo);
        }

        // Configuration de la section pageFooter
        this.pageFooterBandConf(jasperDesign, model, documentInfo);

        // Configuration de la section lastPageFooter
        this.lastPageFooterBandConf(jasperDesign, model, documentInfo);

        // Configuration de la section summary
        List<JasperDesign> listGraphSubReportDesign = new ArrayList<JasperDesign>();
        List<JasperDesign> listSousRapportDesign = new ArrayList<JasperDesign>();
        if (documentInfo.isDocumentAvecGraphiques() || documentInfo.isDocumentAvecSousRapports())
        {
            if (documentInfo.getSummaryInfo().isSummaryNewPageWithPageHeaderAndFooter())
            {
                listGraphSubReportDesign = this.creationGraphSubReportDesign(model, documentInfo);
                listSousRapportDesign = this.creationSousRapportDesign(model, documentInfo);
                this.summaryBandConfNewPage(jasperDesign, model, documentInfo, listGraphSubReportDesign, listSousRapportDesign);
                jasperDesign.setSummaryNewPage(true);
                jasperDesign.setSummaryWithPageHeaderAndFooter(true);
            }
            else
            {
                listGraphSubReportDesign = this.creationGraphSubReportDesign(model, documentInfo);
                this.summaryBandConf(jasperDesign, model, documentInfo, listGraphSubReportDesign);
                jasperDesign.setSummaryNewPage(false);
            }
        }

        // Exécution
        return this.execute(jasperDesign, listGraphSubReportDesign, listSousRapportDesign, model, documentInfo);
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.view.View#beforeBody(fr.gouv.finances.lombok.apptags.table.core.TableModel)
     */
    @Override
    public void beforeBody(TableModel model)
    {
        // RAS
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.view.View#body(fr.gouv.finances.lombok.apptags.table.core.TableModel,
     *      fr.gouv.finances.lombok.apptags.table.bean.Column)
     */
    @Override
    public void body(TableModel model, Column column)
    {
        // RAS
    }

    /**
     * Lecture des attributs graphiques.
     * 
     * @param model
     * @param documentInfo
     */
    private void analyseAttributsGraphiquesTableEc(TableModel model, DocumentInfo documentInfo)
    {
        firstInitGraphiqueInfo(model, documentInfo, ATTR_GRAPHIQUE1, ATTR_ZOOMRATIO1);
        firstInitGraphiqueInfo(model, documentInfo, ATTR_GRAPHIQUE2, ATTR_ZOOMRATIO2);
        firstInitGraphiqueInfo(model, documentInfo, ATTR_GRAPHIQUE3, ATTR_ZOOMRATIO3);
        firstInitGraphiqueInfo(model, documentInfo, ATTR_GRAPHIQUE4, ATTR_ZOOMRATIO4);
    }

    /**
     * Lecture des attributs sous-rapport.
     * 
     * @param model
     * @param documentInfo
     */
    private void analyseAttributsSousRapportTableEc(TableModel model, DocumentInfo documentInfo)
    {
        firstInitSousRapportInfo(model, documentInfo, ATTR_SUBREPORT1);
        firstInitSousRapportInfo(model, documentInfo, ATTR_SUBREPORT2);
        firstInitSousRapportInfo(model, documentInfo, ATTR_SUBREPORT3);
        firstInitSousRapportInfo(model, documentInfo, ATTR_SUBREPORT4);
    }

    /**
     * Crée une liste d'objet ColonneInfo qui décrit les colonnes du tableau, en fonction des attributs du tableau ec.
     * Si les paramètres width affectés à chaque colonne du tableau ne sont pas cohérents, toutes les colonnes ont la
     * même taille.
     * 
     * @param model
     * @param documentInfo
     */
    private void analyseColonnesTableEc(TableModel model, DocumentInfo documentInfo)
    {
        // Teste de la cohérence des largeurs de colonnes
        List<Column> tableColumns = model.getColumnHandler().getColumns();

        // On ne travaille que sur les colonnes qui sont autorisées dans
        // la vue d'export
        // Si toutes les colonnes sont accessibles
        // on recrée une liste avec uniquement les colonnes autorisées

        // On crée une liste qui maintient les infos calculées sur les
        // colonnes
        createListColonneInfo(tableColumns, documentInfo);

        // Unité utilisé dans l'attribut width
        StringBuilder unit = new StringBuilder();

        // Affectation d'une taille à chaque colonne
        if ((!isAllColumnSized(tableColumns)) || (!isColumnUnitCoherent(tableColumns, unit))
            || (!isColumnWidthSumCoherent(tableColumns, unit, documentInfo)))
        {
            // On considère que toutes les colonnes possèdent la
            // même largeur
            int columnWidth = documentInfo.calColumnWidth() / documentInfo.getDetail().getColonnes().size();

            // On réinitialise les valeurs précédemment enregistrées
            for (ColonneInfo colonneInfo : documentInfo.getDetail().getColonnes())
            {
                colonneInfo.setTaille(columnWidth);
            }
        }

        int colonneEnCours = 0;
        for (Column column : tableColumns)
        {
            ColonneInfo colonneInfoEnCours = documentInfo.getDetail().getColonnes().get(colonneEnCours);

            // Affectation d'un titre à une colonne
            colonneInfoEnCours.setTitre(column.getTitle());

            // Affectation de la propriété du javabean concerné
            colonneInfoEnCours.setPropriete(column.getProperty());

            // Affectation du type de cellule associé à la colonne
            colonneInfoEnCours.setCellType(column.getCell());

            // Affectation de la classe de valeur de la colonne en cours
            colonneInfoEnCours.setClazz(this.findColumnValueClass(column, model));

            // Affectation de la pattern de formatage associée à la
            // cellule en cours
            colonneInfoEnCours.setPattern(this.findPattern(column));

            // Affectation du type d'alignement horizontal de la
            // cellule
            colonneInfoEnCours.setAlignement(this.findAlignment(column));

            // Affectation de l'alias
            colonneInfoEnCours.setAlias(column.getAlias());

            colonneEnCours++;

        }
    }

    /**
     * Calcule les dimensions d'un grpahique.
     * 
     * @param jrRenderable
     * @param model
     * @param graphique
     * @param zoomRatio
     * @return dimension
     * @throws JRException the JR exception
     */
    private Dimension calculGraphicDimension(JRRenderable jrRenderable, TableModel model, String graphique,
        String zoomRatio) throws JRException
    {
        Dimension result = new Dimension();

        double height = jrRenderable.getDimension().getHeight();

        double width = jrRenderable.getDimension().getWidth();

        int zoomratio = ((Integer) model.getExportHandler().getCurrentExport().getAttribute(zoomRatio)).intValue();

        zoomratio = (zoomratio > 0) ? zoomratio : ZOOMRATIO_DEFAULT;

        height = height / (zoomratio);

        width = width / (zoomratio);

        result.setSize(width, height);

        return result;
    }

    /**
     * methode Column footer band conf :
     * 
     * @param jasperDesign
     * @param model
     * @param documentInfo
     */
    private void columnFooterBandConf(JasperDesign jasperDesign, TableModel model, DocumentInfo documentInfo)
    {
        JRDesignBand columnFooterBand = (JRDesignBand) jasperDesign.getColumnFooter();

        // Le test est désormais effectué au niveau de chaque cellule
        // du pied de page
        /*
         * JRDesignExpression expressionTest = new JRDesignExpression(); expressionTest.setValueClass(Boolean.class);
         * expressionTest.setText("new Boolean((" + model.getCollectionOfBeans().size() +
         * " == $V{REPORT_COUNT}.intValue()  ))"); columnFooterBand.setPrintWhenExpression(expressionTest);
         */
        jasperDesign.setFloatColumnFooter(true);

        ColumnFooterUtil.calcFooterAnalyse(model, documentInfo, columnFooterBand, documentInfo.getColumnFooter()
            .getFirstPageFrameInfo(), ColumnFooterUtil.findColumnFooterLevelForFirstPage(model, documentInfo));

        ColumnFooterUtil.calcFooterAnalyse(model, documentInfo, columnFooterBand, documentInfo.getColumnFooter()
            .getLastPageFrameInfo(), ColumnFooterUtil.findColumnFooterLevelForLastPage(model, documentInfo));

        ColumnFooterUtil.calcFooterAnalyse(model, documentInfo, columnFooterBand, documentInfo.getColumnFooter()
            .getMiddlePageFrameInfo(), ColumnFooterUtil.findColumnFooterLevelForMiddlePage(model, documentInfo));

        JRDesignFrame frameFirstPageColFooter =
            ColumnFooterUtil.insertColumnFooterFrameFirstPage(model, documentInfo, columnFooterBand, jasperDesign);

        ColumnFooterUtil.insertColumnFooterRows(model, documentInfo, frameFirstPageColFooter, documentInfo
            .getColumnFooter().getFirstPageFrameInfo(), jasperDesign);

        JRDesignFrame frameLastPageColFooter =
            ColumnFooterUtil.insertColumnFooterFrameLastPage(model, documentInfo, columnFooterBand, jasperDesign);

        ColumnFooterUtil.insertColumnFooterRows(model, documentInfo, frameLastPageColFooter, documentInfo
            .getColumnFooter().getLastPageFrameInfo(), jasperDesign);

        JRDesignFrame frameMiddlePageColFooter =
            ColumnFooterUtil.insertColumnFooterFrameMiddlePage(model, documentInfo, columnFooterBand, jasperDesign);

        ColumnFooterUtil.insertColumnFooterRows(model, documentInfo, frameMiddlePageColFooter, documentInfo
            .getColumnFooter().getMiddlePageFrameInfo(), jasperDesign);

    }

    /**
     * Insertion des éléments dans la section ColumnHeader.
     * 
     * @param jasperDesign
     * @param model
     * @param documentInfo
     */
    private void columnHeaderBandConf(JasperDesign jasperDesign, TableModel model, DocumentInfo documentInfo)
    {

        JRDesignBand columnHeaderBand = (JRDesignBand) jasperDesign.getColumnHeader();

        columnHeaderBand.setHeight(documentInfo.getColumnHeader().getHeight());

        JRDesignExpression expressionTest = new JRDesignExpression();
        expressionTest.setValueClass(Boolean.class);
        expressionTest.setText("new Boolean((" + model.getCollectionOfBeans().size() + "-"
            + documentInfo.getSummaryInfo().getGraphiquesInfos().size() + " - $V{REPORT_COUNT}.intValue() >=0 ))");

        /*
         * expressionTest.setText("new Boolean((" + model.getCollectionOfBeans().size() +
         * " - $V{REPORT_COUNT}.intValue() >0 ))");
         */

        JRDesignExpression testPrintColumnHeader = new JRDesignExpression();
        testPrintColumnHeader.setValueClass(Boolean.class);
        ;
        testPrintColumnHeader.setText("$V{TEST_AFFICHAGE_COLUMN_HEADER}");

        columnHeaderBand.setPrintWhenExpression(expressionTest);

        JRStyle styleCellEntete =
            JasperdesignUtil.creerStyleComplet(jasperDesign, STYLE_CELLULE_TITRE_COLONNE_EDITION, documentInfo
                .getColumnHeader(), documentInfo.getColumnHeader().getStyle(), null, null, null);

        ColumnHeaderUtil.groupSubgroupTitleHeaderAnalyse(model, documentInfo, columnHeaderBand, styleCellEntete);

    }

    /**
     * Lecture du fichier de configuration de l'édition et construction de la description de l'édition.
     * 
     * @param model
     * @return objet DocumentInfo décrivant l'édition
     */
    private DocumentInfo configurationEdition(TableModel model)
    {
        DocumentInfo documentInfo = null;

        String fichierConfiguration = this.findNomFichierConf(model);

        String modele = this.findModele(model);

        // Lecture dans le modèle du nom du fichier de paramétrage s'il
        // est présent

        try
        {
            documentInfo = this.lectureFichierConfiguration(fichierConfiguration, modele, model);
        }
        catch (ConfigurationException exception)
        {
            log.error("Fichier de configuration " + fichierConfiguration + "ou modele " + modele + "incorrects",
                exception);
            log.error("Utilisation des paramètres par défaut");
            try
            {
                documentInfo = this.lectureFichierConfiguration(DEFAUT_FICHIER_CONF, DEFAUT_MODELE, model);
            }
            catch (ConfigurationException excep)
            {
                // ancien code
                // EcTableException re = new EcTableException(excep.getMessage(), excep);
                // throw re;

                // nouveau code
                throw new EcTableException(excep.getMessage(), excep);
            }
        }

        return documentInfo;
    }

    /**
     * Crée pour chaque colonne du tableau un objet ColonneInfo et l'ajoute dans la liste des colonnes.
     * 
     * @param tableColumns
     * @param documentInfo
     */
    private void createListColonneInfo(List<Column> tableColumns, DocumentInfo documentInfo)
    {
        for (int i = 0; i < tableColumns.size(); i++)
        {
            ColonneInfo colonneInfo = new ColonneInfo();
            documentInfo.getDetail().getColonnes().add(colonneInfo);
        }
    }

    /**
     * Création du modèle principal jasperreports.
     * 
     * @param model
     * @param documentInfo
     * @return jasper design
     */
    private JasperDesign creationDesign(TableModel model, DocumentInfo documentInfo)
    {
        JasperDesign jasperDesign = new JasperDesign();

        jasperDesign.setName(MAIN_REPORT_NAME);
        jasperDesign.setPageWidth(documentInfo.getPageWidth());
        jasperDesign.setPageHeight(documentInfo.getPageHeight());
        jasperDesign.setColumnWidth(documentInfo.calColumnWidth());
        jasperDesign.setColumnSpacing(0);
        jasperDesign.setLeftMargin(documentInfo.getLeftMargin());
        jasperDesign.setRightMargin(documentInfo.getRightMargin());
        jasperDesign.setTopMargin(documentInfo.getTopMargin());
        jasperDesign.setBottomMargin(documentInfo.getBottomMargin());

        JRDesignBand titleBand = new JRDesignBand();

        JRDesignBand pageHeaderBand = new JRDesignBand();

        JRDesignBand columnHeaderBand = new JRDesignBand();

        JRDesignBand detailBand = new JRDesignBand();

        JRDesignBand columnFooterBand = new JRDesignBand();

        JRDesignBand pageFooterBand = new JRDesignBand();

        JRDesignBand lastPageFooterBand = new JRDesignBand();

        JRDesignBand summaryBand = new JRDesignBand();

        jasperDesign.setPageHeader(pageHeaderBand);

        jasperDesign.setTitle(titleBand);

        jasperDesign.setColumnHeader(columnHeaderBand);

        // jasperDesign.setDetail(detailBand);

        ((JRDesignSection) jasperDesign.getDetailSection()).addBand(detailBand);

        jasperDesign.setColumnFooter(columnFooterBand);

        jasperDesign.setPageFooter(pageFooterBand);

        jasperDesign.setLastPageFooter(lastPageFooterBand);

        if (documentInfo.isDocumentAvecGraphiques())
        {
            jasperDesign.setSummary(summaryBand);
            jasperDesign.setSummaryNewPage(false);
        }

        return jasperDesign;
    }

    /**
     * Création d'un sous rapport par graphique à insérer dans l 'édition.
     * 
     * @param model
     * @param documentInfo
     * @return liste des sous-rapports utilisés pour inclure des graphiques
     */
    private List<JasperDesign> creationGraphSubReportDesign(TableModel model, DocumentInfo documentInfo)
    {
        List<JasperDesign> result = new ArrayList<JasperDesign>();

        List<GraphiqueInfo> graphiquesInfos = documentInfo.getSummaryInfo().getGraphiquesInfos();

        int graphEnCours = 0;
        for (GraphiqueInfo graphiqueInfo : graphiquesInfos)
        {
            int heightGraph =
                Math.min(documentInfo.getPageHeight() - documentInfo.getTopMargin() - documentInfo.getBottomMargin()
                    - documentInfo.getPageHeader().getHeight() - documentInfo.getPageFooter().getHeight()
                    - (documentInfo.getSummaryInfo().getPadding() * 2 + 5) - 60, graphiqueInfo.getRectangleInfo()
                        .getHeight());

            graphiqueInfo.getRectangleInfo().setHeight(heightGraph);

            JasperDesign jasperDesign = new JasperDesign();

            jasperDesign.setName(GRAPHIQUE_REPORT_NAME + graphEnCours);
            jasperDesign.setPageWidth(documentInfo.calColumnWidth());
            jasperDesign.setPageHeight(graphiqueInfo.getRectangleInfo().getHeight()
                + documentInfo.getSummaryInfo().getPadding() * 2 + 5);
            jasperDesign.setColumnWidth(documentInfo.calColumnWidth());
            jasperDesign.setColumnSpacing(0);
            jasperDesign.setLeftMargin(0);
            jasperDesign.setRightMargin(0);
            jasperDesign.setTopMargin(0);
            jasperDesign.setBottomMargin(0);
            jasperDesign.setSummaryNewPage(false);

            JRDesignBand summaryBand = new JRDesignBand();
            summaryBand.setHeight(graphiqueInfo.getRectangleInfo().getHeight()
                + documentInfo.getSummaryInfo().getPadding() * 2 + 2);
            jasperDesign.setSummary(summaryBand);

            JRDesignBand detailBand = new JRDesignBand();
            detailBand.setHeight(0);
            ((JRDesignSection) jasperDesign.getDetailSection()).addBand(detailBand);
            // jasperDesign.setDetail(detailBand);
            JRDesignBand pageHeaderBand = new JRDesignBand();
            pageHeaderBand.setHeight(0);
            jasperDesign.setPageHeader(pageHeaderBand);
            JRDesignBand lastPageFooterBand = new JRDesignBand();
            lastPageFooterBand.setHeight(0);
            jasperDesign.setLastPageFooter(lastPageFooterBand);
            JRDesignBand titleBand = new JRDesignBand();
            titleBand.setHeight(0);
            jasperDesign.setTitle(titleBand);
            JRDesignBand columnHeaderBand = new JRDesignBand();
            columnHeaderBand.setHeight(0);
            jasperDesign.setColumnHeader(columnHeaderBand);
            JRDesignBand columnFooterBand = new JRDesignBand();
            columnFooterBand.setHeight(0);
            jasperDesign.setColumnFooter(columnFooterBand);

            result.add(jasperDesign);
            graphEnCours++;
        }

        return result;
    }

    /**
     * methode Creation sous rapport design :
     * 
     * @param model
     * @param documentInfo
     * @return list
     */
    private List<JasperDesign> creationSousRapportDesign(TableModel model, DocumentInfo documentInfo)
    {
        List<JasperDesign> result = new ArrayList<JasperDesign>();

        List<SousRapportInfo> sousRapportsInfos = documentInfo.getSummaryInfo().getSousRapportsInfos();

        for (SousRapportInfo sousRapportInfo : sousRapportsInfos)
        {

            String designFilePath = ResolveurDeLocalisations
                .resoudreURLLocalisationEtTransformerEnPath("$localisations{appli.web.root}/WEB-INF/editions/rapports/"
                    + sousRapportInfo.getFileJRXML() + ".jrxml");

            JasperDesign reportDesign = null;

            try
            {
                reportDesign = JRXmlLoader.load(designFilePath);
                result.add(reportDesign);
            }
            catch (JRException e)
            { // mon commentaire
            }
        }
        return result;
    }

    /**
     * Création d'un champ correspondant à une cellule du tableau.
     * 
     * @param jasperDesign
     * @param colno
     * @param posX
     * @param posY
     * @param width
     * @param height
     * @param documentInfo
     * @param col
     * @return jR design text field
     */
    private JRDesignTextField creerCelluleTableau(JasperDesign jasperDesign, int colno, int posX, int posY, int width,
        int height, DocumentInfo documentInfo, ColonneInfo col)
    {

        StringBuilder styleName = new StringBuilder();
        styleName.append(STYLE_CELLULE_TABLEAU_EDITION_PREFIX);
        styleName.append(colno);

        // Format de la cellule
        String pattern = col.getPattern();

        // Alignement dans la cellule
        HorizontalAlignEnum alignment = col.getAlignement();

        JRStyle cellStyle =
            JasperdesignUtil.creerStyleComplet(jasperDesign, styleName.toString(), documentInfo.getDetail(),
                documentInfo.getDetail().getStyle(), alignment, VerticalAlignEnum.MIDDLE, pattern);

        JRDesignTextField textField = new JRDesignTextField();
        textField.setBlankWhenNull(true);
        textField.setX(posX + documentInfo.getDetail().getCellMargin());
        textField.setY(documentInfo.getDetail().getCellMargin());
        textField.setWidth(width - (2 * documentInfo.getDetail().getCellMargin()));
        textField.setHeight(height - (2 * documentInfo.getDetail().getCellMargin()));

        textField.setStyle(cellStyle);
        textField.setStretchWithOverflow(true);
        textField.setStretchType(StretchTypeEnum.RELATIVE_TO_TALLEST_OBJECT);
        return textField;
    }

    /**
     * Configuration de la section detail.
     * 
     * @param jasperDesign
     * @param model
     * @param documentInfo
     */
    private void detailBandConf(JasperDesign jasperDesign, TableModel model, DocumentInfo documentInfo)
    {
        JRDesignBand detailBand = (JRDesignBand) ((jasperDesign.getDetailSection().getBands())[0]);

        detailBand.setHeight(documentInfo.getDetail().getHeight());
        // a revooir
        detailBand.setSplitType(SplitTypeEnum.STRETCH);

        // Ligne verti tableau
        int absLi = 0;
        for (ColonneInfo col : documentInfo.getDetail().getColonnes())
        {
            detailBand.addElement(JasperdesignUtil.ajouterLigneVerticale(absLi, 0, 0, documentInfo.getDetail()
                .getHeight(), PositionTypeEnum.FLOAT, StretchTypeEnum.RELATIVE_TO_BAND_HEIGHT, documentInfo.getLineWidth()));
            absLi = absLi + col.getTaille();
        }

        // Ajout de la dernière ligne verticale
        // Compense les décalages
        detailBand.addElement(JasperdesignUtil.ajouterLigneVerticale(documentInfo.calColumnWidth(), 0, 0, documentInfo
            .getDetail().getHeight(), PositionTypeEnum.FLOAT, StretchTypeEnum.RELATIVE_TO_BAND_HEIGHT,
            documentInfo.getLineWidth()));

        // Ajout d'éléments dans la bande detail
        int abs = 0;
        int colno = 0;
        for (ColonneInfo col : documentInfo.getDetail().getColonnes())
        {
            JRDesignTextField textField =
                this.creerCelluleTableau(jasperDesign, colno, abs, 0, col.getTaille(), documentInfo.getDetail()
                    .getHeight(), documentInfo, col);

            JRDesignExpression expression = new JRDesignExpression();
            expression.setValueClass(col.getClazz());
            expression.setText("$F{" + col.getPropriete() + "}");
            textField.setExpression(expression);

            detailBand.addElement(textField);

            abs = abs + col.getTaille();
            colno++;
        }

        detailBand.addElement(JasperdesignUtil.ajouterLigneHorizontale(0, documentInfo.getDetail().getHeight() - 1,
            documentInfo.calColumnWidth(), 0, PositionTypeEnum.FIX_RELATIVE_TO_BOTTOM, documentInfo.getLineWidth()));

    }

    /**
     * Détermine si l'édition utilise une page de garde.
     * 
     * @param model
     * @param documentInfo
     * @return true si l'édition comporte une page de garde
     */
    private boolean determineUtilisationPageDeGarde(TableModel model, DocumentInfo documentInfo)
    {
        boolean utilisePageDeGarde = true;
        String utilisePageDeGardeAttr =
            (String) model.getExportHandler().getCurrentExport().getAttribute(CpJrxmlView.ATTR_PAGE_DE_GARDE);
        if (utilisePageDeGardeAttr != null && ("false").equalsIgnoreCase(utilisePageDeGardeAttr))
        {
            utilisePageDeGarde = false;
        }
        else
        {
            utilisePageDeGarde = documentInfo.isUseTitleOnNewPage();
        }
        return utilisePageDeGarde;
    }

    /**
     * Production de l'édition.
     * 
     * @param jasperDesign
     * @param listJasperSubReportDesign
     * @param listSousRapportDesign
     * @param model
     * @param documentInfo
     * @return export file
     */
    @SuppressWarnings("unchecked")
    private ExportFile execute(JasperDesign jasperDesign, List<JasperDesign> listJasperSubReportDesign,
        List<JasperDesign> listSousRapportDesign,
        TableModel model, DocumentInfo documentInfo)
    {
        ExportFile fichierJoint = null;

        try
        {
            // Affectation de la valeur des paramètres

            // avant object au lieu de string
            Map<String, Object> parameters = new HashMap<String, Object>();

            if (documentInfo.isDocumentAvecGraphiques() || documentInfo.isDocumentAvecSousRapports())
            {
                if (documentInfo.isDocumentAvecSousRapports())
                {
                    parameters.put(CLE_GRAPH_DATA_SOURCE, model.getCollectionOfFilteredBeans());
                }
                if (documentInfo.isDocumentAvecGraphiques() && !documentInfo.isDocumentAvecSousRapports())
                {
                    parameters.put(CLE_GRAPH_DATA_SOURCE, documentInfo.getSummaryInfo().getGraphiquesInfos());
                }

                int subReportEnCours = 0;
                for (JasperDesign jasperSubReportDesign : listJasperSubReportDesign)
                {
                    JasperReport jasperSubReport = JasperCompileManager.compileReport(jasperSubReportDesign);
                    parameters.put(CLE_SUBREPORT_JASPER_REPORT + subReportEnCours, jasperSubReport);

                    JRRenderable jrRenderable =
                        (documentInfo.getSummaryInfo().getGraphiquesInfos().get(subReportEnCours))
                            .getJrRenderable();
                    parameters.put(CLE_GRAPH_DATA_SOURCE + subReportEnCours, jrRenderable);

                    subReportEnCours++;
                }

                for (JasperDesign jasperSousRapportDesign : listSousRapportDesign)
                {
                    JasperReport jasperSubReport = JasperCompileManager.compileReport(jasperSousRapportDesign);
                    parameters.put(CLE_SUBREPORT_JASPER_REPORT + subReportEnCours, jasperSubReport);

                    subReportEnCours++;
                }

            }

            JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);

            parameters.put(CLE_NOM_APPLI, this.getNomAppliContent(model));
            parameters.put(CLE_TITRE_EDITION, this.getTitreEditionContent(model));
            parameters.put(CLE_LOGO, this.getLogoMarianeContent(documentInfo, model));
            parameters.put(CLE_LOGO_MINISTERE, this.getLogoMinistereContent(documentInfo, model));
            parameters.put(CLE_DATE_PRODUCTION_EDITION, this.getDateProductionEditionContent());

            parameters.put(CLE_LOGO_APPLICATION, this.getLogoApplicationContent(documentInfo, model));
            parameters.put(CLE_TEXTE_ENTETE, this.getTexteEnteteContent(model));

            if (isTextePiedDePageAInserer(model))
            {
                parameters.put(CLE_TEXTE_PIED_DE_PAGE, this.getTextePiedDepageContent(model));
            }

            parameters.put(JRParameter.REPORT_LOCALE, Locale.FRANCE);

            JRDataSource jrDataSource =
                new CpJRBeanCollectionDataSource(model.getCollectionOfFilteredBeans());

            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrDataSource);

            List<JRPrintPage> listeDesPages = jasperPrint.getPages();

            // Si la dernière page est vide, on la supprime
            int lastPage = listeDesPages.size() - 1;
            if (lastPage > 1)
            {
                JRPrintPage page = listeDesPages.get(lastPage);
                if (page.getElements() == null || page.getElements().size() == 0)
                {
                    listeDesPages.remove(lastPage);
                }
            }

            byte[] data = JasperExportManager.exportReportToPdf(jasperPrint);

            fichierJoint = new ExportFile(data);
            fichierJoint.setTypeMimeFichier("application/pdf");
            fichierJoint.setNomFichierOriginal(this.getNomFichierEditionContent(model));

        }
        catch (JRException exception)
        {
            throw new EcTableException(exception);
        }
        return fichierJoint;
    }

    /**
     * Ajoute la définitions des champs nécessaires à l'édition à produire.
     * 
     * @param jasperDesign
     * @param documentInfo
     */
    @SuppressWarnings("unchecked")
    private void fieldConf(JasperDesign jasperDesign, DocumentInfo documentInfo)
    {
        try
        {
            for (ColonneInfo colonneInfo : documentInfo.getDetail().getColonnes())
            {
                if (colonneInfo != null)
                {
                    JRDesignField field = new JRDesignField();

                    field.setName(colonneInfo.getPropriete());
                    field.setValueClass(colonneInfo.getClazz());

                    Map<String, JRField> reportFields = jasperDesign.getFieldsMap();
                    if (reportFields == null || (!reportFields.containsKey(field.getName())))
                    {
                        jasperDesign.addField(field);
                    }
                }
            }

        }
        catch (JRException exception)
        {
            throw new EcTableException(exception);
        }
    }

    /**
     * Récupère l'alignement associée à un type de cellule.
     * 
     * @param column
     * @return byte
     */
    private HorizontalAlignEnum findAlignment(Column column)
    {
        HorizontalAlignEnum result = HorizontalAlignEnum.CENTER;
        int alignment = PatternCell.HORIZONTAL_ALIGN_CENTER;
        String cellType = column.getCell();
        Object objectCell = null;

        try
        {
            objectCell = Class.forName(cellType).newInstance();
        }
        catch (ClassNotFoundException exception)
        {
            throw new EcTableException(exception);
        }
        catch (InstantiationException exception)
        {
            throw new EcTableException(exception);
        }
        catch (IllegalAccessException exception)
        {
            throw new EcTableException(exception);
        }

        if (objectCell instanceof PatternCell)
        {
            {
                alignment = ((PatternCell) objectCell).getAlignmentCell();
            }

            switch (alignment)
            {
                // ancien code
                // case PatternCell.HORIZONTAL_ALIGN_CENTER:
                // result = HorizontalAlignEnum.CENTER;
                // break;
                // case PatternCell.HORIZONTAL_ALIGN_LEFT:
                // result = HorizontalAlignEnum.LEFT;
                // break;
                // case PatternCell.HORIZONTAL_ALIGN_RIGHT:
                // result = HorizontalAlignEnum.RIGHT;
                // break;
                // default:
                // result = HorizontalAlignEnum.CENTER;
                // break;

                // nouveau code
                case PatternCell.HORIZONTAL_ALIGN_LEFT:
                    result = HorizontalAlignEnum.LEFT;
                    break;
                case PatternCell.HORIZONTAL_ALIGN_RIGHT:
                    result = HorizontalAlignEnum.RIGHT;
                    break;
                case PatternCell.HORIZONTAL_ALIGN_CENTER:
                default:
                    result = HorizontalAlignEnum.CENTER;
                    break;
            }
        }
        // modif amlp 21/10/09 : si cellule de type display , elle n'a pas de pattern
        // donc code ci dessus ne marche pas
        if (objectCell instanceof DisplayCell && column.getStyle() != null)
        {
            if (column.getStyle().compareToIgnoreCase("text-align: left") == 0)
            {
                result = HorizontalAlignEnum.LEFT;
            }
            else if (column.getStyle().compareToIgnoreCase("text-align: right") == 0)
            {
                result = HorizontalAlignEnum.RIGHT;
            }
            else
            {
                result = HorizontalAlignEnum.CENTER;
            }
        }
        return result;
    }

    /**
     * Détermine la classe des valeurs de la colonne.
     * 
     * @param column
     * @param model
     * @return the class<? extends object>
     */
    private Class<? extends Object> findColumnValueClass(Column column, TableModel model)
    {
        Class<? extends Object> result = java.lang.String.class;

        if (column == null)
        {
            throw new IllegalArgumentException("L'OBJET COLUMN NE DOIT PAS ETRE NULL.");
        }

        // Utilise la dernière valeur de la collection pour
        // déterminer le type de la colonne

        if (column.getPropertyValue() != null)
        {
            result = column.getPropertyValue().getClass();
        }
        // Si la valeur est nulle, on itère dans la collection de bean
        // jusqu'à trouver une valeur non nulle
        else
        {
            Collection<Object> beans = model.getCollectionOfBeans();
            for (Object bean : beans)
            {
                Object propertyValue = TableModelUtils.getColumnPropertyValue(bean, column.getProperty());
                if (propertyValue != null)
                {
                    result = propertyValue.getClass();
                    break;
                }
            }
        }

        // Dans le cas d'un Enum, la valeur sera traitée comme une chaîne de caractères
        if (result.isEnum())
        {
            result = java.lang.String.class;
        }

        return result;
    }

    /**
     * Détermine le modèle utilisé pour produire l'édition, à partir du paramètre passé dans le tag exportPDF ou en
     * utilisant la valeur par défaut de la vue.
     * 
     * @param model
     * @return le nom du modèle à utiliser
     */
    private String findModele(TableModel model)
    {
        String modele = (String) model.getExportHandler().getCurrentExport().getAttribute(ATTR_MODELE);

        if (modele == null || StringUtils.isBlank(modele))
        {
            modele = DEFAUT_MODELE;
        }
        else
        {
            modele = modele.trim();
        }
        return modele;
    }

    /**
     * Détermine le fichier de configuration à utiliser, à partir du paramètre passé dans le tag exportPDF ou en
     * utilisant la valeur par défaut de la vue.
     * 
     * @param model
     * @return le nom du fichier de configuration de l'édition
     */
    private String findNomFichierConf(TableModel model)
    {
        String fichierConfiguration =
            (String) model.getExportHandler().getCurrentExport().getAttribute(ATTR_FICHIER_CONF);

        StringBuilder fullPathConf = new StringBuilder();

        if (fichierConfiguration == null || StringUtils.isBlank(fichierConfiguration))
        {
            fullPathConf.append(DEFAUT_FICHIER_CONF);
        }
        else
        {
            fullPathConf.append(fichierConfiguration.trim());
        }
        return fullPathConf.toString();
    }

    /**
     * Récupère la pattern de formatage associée à un type de cellule.
     * 
     * @param column
     * @return string
     */
    private String findPattern(Column column)
    {
        String pattern = null;
        String cellType = column.getCell();
        Object objectCell = null;

        try
        {
            objectCell = Class.forName(cellType).newInstance();
        }
        catch (ClassNotFoundException exception)
        {
            throw new EcTableException(exception);
        }
        catch (InstantiationException exception)
        {
            throw new EcTableException(exception);
        }
        catch (IllegalAccessException exception)
        {
            throw new EcTableException(exception);
        }

        if (objectCell instanceof PatternCell)
        {
            pattern = ((PatternCell) objectCell).getFormatPattern();
        }

        return pattern;
    }

    /**
     * Constitution de la liste des graphiques à insérer dans l'édition.
     * 
     * @param model
     * @param documentInfo
     * @param graphique
     * @param zoomRatio
     */
    private void firstInitGraphiqueInfo(TableModel model, DocumentInfo documentInfo, String graphique, String zoomRatio)
    {
        if (model.getExportHandler().getCurrentExport().getAttribute(graphique) != null)
        {
            GraphiqueInfo graphiqueInfo = new GraphiqueInfo();

            graphiqueInfo.setCleParametre(graphique);

            graphiqueInfo.setJrRenderable(this.getGraphiqueContent(model, graphique));

            Dimension dimension;
            try
            {
                dimension = this.calculGraphicDimension(graphiqueInfo.getJrRenderable(), model, graphique, zoomRatio);

                graphiqueInfo.getRectangleInfo().setHeight((int) Math.round(dimension.getHeight()));
                graphiqueInfo.getRectangleInfo().setWidth((int) Math.round(dimension.getWidth()));

                documentInfo.getSummaryInfo().getGraphiquesInfos().add(graphiqueInfo);
            }
            catch (JRException exception)
            {
                log.error("ERREUR LORS DU CHARGEMENT DU GRAPHIQUE" + graphique);
            }
        }
    }

    /**
     * Constitution de la liste des s à insérer dans l'édition.
     * 
     * @param model
     * @param documentInfo
     * @param sousRapport
     */
    private void firstInitSousRapportInfo(TableModel model, DocumentInfo documentInfo, String sousRapport)
    {
        if (model.getExportHandler().getCurrentExport().getAttribute(sousRapport) != null)
        {
            SousRapportInfo sousRapportInfo = new SousRapportInfo();

            sousRapportInfo.setCleParametre(sousRapport);

            sousRapportInfo.setFileJRXML((String) model.getExportHandler().getCurrentExport().getAttribute(sousRapport));

            documentInfo.getSummaryInfo().getSousRapportsInfos().add(sousRapportInfo);
        }
    }

    /**
     * Retourne la date à insérer dans la section titre et dans l'entête de page.
     * 
     * @return date production edition content
     */
    private String getDateProductionEditionContent()
    {
        return FormaterDate.formatDateCourt(new Date());
    }

    /**
     * Charge l'image contenue dans un objet de type fichier joint dans un objet de type JRRenderable.
     * 
     * @param model
     * @param graphique
     * @return graphique content
     */
    private JRRenderable getGraphiqueContent(TableModel model, String graphique)
    {
        JRRenderable result = null;
        Object graph = model.getExportHandler().getCurrentExport().getAttribute(graphique);
        if (graph instanceof byte[])
        {
            result = JRImageRenderer.getInstance((byte[]) graph);
        }
        else
        {
            throw new IllegalArgumentException("LE GRAPHIQUE DOIT ETRE STOCKE DANS UN OBJET DE TYPE byte[]");
        }

        return result;
    }

    /**
     * Fournit le chemin complet vers le répertoire qui contient les fichiers de configuration et les images utilisées
     * par l'export.
     * 
     * @param model
     * @return image and conf base absolute path
     */
    private String getImageAndConfBaseAbsolutePath(TableModel model)
    {
        PageContext pageContext = (PageContext) ((JspPageContext) model.getContext()).getContextObject();

        String baseServletPath = pageContext.getServletContext().getRealPath("");

        String relativePath = retrieveBaseImageAndConfRelativePath(model);

        StringBuilder fullPath = new StringBuilder();

        fullPath.append(baseServletPath);
        fullPath.append(File.separator);
        fullPath.append(relativePath);

        return fullPath.toString();
    }

    /**
     * Accesseur de l attribut logo application content.
     * 
     * @param documentInfo
     * @param model
     * @return logo application content
     */
    private JRImageRenderer getLogoApplicationContent(DocumentInfo documentInfo, TableModel model)
    {
        JRImageRenderer result = null;
        if (documentInfo.getPageHeader().getUseApplicationLogo().booleanValue())
        {
            result =
                JasperdesignUtil.chargeImage(getImageAndConfBaseAbsolutePath(model),
                    EDITION_IMAGES_ET_CONF_DEF_CLASSPATH, getLogoApplicationName(documentInfo));
        }
        return result;
    }

    /**
     * Accesseur de l attribut logo application name.
     * 
     * @param documentInfo
     * @return logo application name
     */
    private String getLogoApplicationName(DocumentInfo documentInfo)
    {
        String result = null;
        if (documentInfo != null && documentInfo.getPageHeader() != null
            && documentInfo.getPageHeader().getLogoApplication() != null
            && documentInfo.getPageHeader().getLogoApplication().getImageName() != null
            && StringUtils.isNotBlank(documentInfo.getPageHeader().getLogoApplication().getImageName()))
        {
            result = documentInfo.getPageHeader().getLogoApplication().getImageName();
        }
        return result;
    }

    /**
     * Accesseur de l attribut logo mariane content.
     * 
     * @param documentInfo
     * @param model
     * @return logo mariane content
     */
    private JRImageRenderer getLogoMarianeContent(DocumentInfo documentInfo, TableModel model)
    {
        return JasperdesignUtil.chargeImage(getImageAndConfBaseAbsolutePath(model),
            EDITION_IMAGES_ET_CONF_DEF_CLASSPATH, getLogoMarianeName(documentInfo));
    }

    /**
     * Accesseur de l attribut logo mariane name.
     * 
     * @param documentInfo
     * @return logo mariane name
     */
    private String getLogoMarianeName(DocumentInfo documentInfo)
    {
        String result = null;
        if (documentInfo != null && documentInfo.getTitle() != null && documentInfo.getTitle().getLogoMariane() != null
            && documentInfo.getTitle().getLogoMariane().getImageName() != null
            && StringUtils.isNotBlank(documentInfo.getTitle().getLogoMariane().getImageName()))
        {
            result = documentInfo.getTitle().getLogoMariane().getImageName();
        }
        return result;
    }

    /**
     * Accesseur de l attribut logo ministere content.
     * 
     * @param documentInfo
     * @param model
     * @return logo ministere content
     */
    private JRImageRenderer getLogoMinistereContent(DocumentInfo documentInfo, TableModel model)
    {
        return JasperdesignUtil.chargeImage(getImageAndConfBaseAbsolutePath(model),
            EDITION_IMAGES_ET_CONF_DEF_CLASSPATH, getLogoMinistereName(documentInfo));
    }

    /**
     * Accesseur de l attribut logo ministere name.
     * 
     * @param documentInfo
     * @return logo ministere name
     */
    private String getLogoMinistereName(DocumentInfo documentInfo)
    {
        String result = null;
        if (documentInfo != null && documentInfo.getTitle() != null
            && documentInfo.getTitle().getLogoMinistere() != null
            && documentInfo.getTitle().getLogoMinistere().getImageName() != null
            && StringUtils.isNotBlank(documentInfo.getTitle().getLogoMinistere().getImageName()))
        {
            result = documentInfo.getTitle().getLogoMinistere().getImageName();
        }
        return result;
    }

    /**
     * Retourne le nom de l'application à insérer dans l'entête de page.
     * 
     * @param model
     * @return nom appli content
     */
    private String getNomAppliContent(TableModel model)
    {
        return (String) model.getExportHandler().getCurrentExport().getAttribute(CpJrxmlView.ATTR_NOM_APPLI);
    }

    /**
     * Retourne le nom du fichier qui se présenté à l'utilisateur.
     * 
     * @param model
     * @return nom fichier edition content
     */
    private String getNomFichierEditionContent(TableModel model)
    {
        return model.getExportHandler().getCurrentExport().getFileName();
    }

    /**
     * Retourne le texte complémentaire à insérer dans l'entête de page.
     * 
     * @param model
     * @return texte entete content
     */
    private String getTexteEnteteContent(TableModel model)
    {
        return (String) model.getExportHandler().getCurrentExport().getAttribute(CpJrxmlView.ATTR_TEXTE_ENTETE);
    }

    /**
     * Retourne le texte à insérer dans le pied de page.
     * 
     * @param model
     * @return texte pied depage content
     */
    private String getTextePiedDepageContent(TableModel model)
    {
        return (String) model.getExportHandler().getCurrentExport().getAttribute(CpJrxmlView.ATTR_TEXTE_PIEDDEPAGE);
    }

    /**
     * Retourne le titre à insérer dans la section titre et dans l'entête de page. Si l'attibut titreedition est utilisé
     * dans le tag de l'édition la valeur affectée à cet attribut est utilisée comme titre de l'édition Sinon,
     * l'attribut title du tableau est utilisé.
     * 
     * @param model
     * @return titre edition content
     */
    private String getTitreEditionContent(TableModel model)
    {
        String titreEdition;
        titreEdition =
            (String) model.getExportHandler().getCurrentExport().getAttribute(CpJrxmlView.ATTR_TITRE_EDITION);
        if (titreEdition == null || StringUtils.isBlank(titreEdition))
        {
            titreEdition = model.getTableHandler().getTable().getTitle();
            ;
        }
        return titreEdition;
    }

    /**
     * Insère la date de l'édition dans l'entête.
     * 
     * @param jasperDesign
     * @param documentInfo
     * @param pageHeaderBand
     */
    private void insererDateEditionDansEntete(JasperDesign jasperDesign, DocumentInfo documentInfo,
        JRDesignBand pageHeaderBand)
    {
        JRStyle dateStyle =
            JasperdesignUtil.creerStyleComplet(jasperDesign, STYLE_DATE_DANS_ENTETE_EDITION, documentInfo
                .getPageHeader(), documentInfo.getPageHeader().getDateStyle(),
                documentInfo.getPageHeader().getRectangle13().getHorizontalAlignment(),
                documentInfo.getPageHeader().getRectangle13().getVerticalAlignment(), null);

        JRDesignTextField dateTextField = new JRDesignTextField();
        dateTextField.setBlankWhenNull(true);
        dateTextField.setX(documentInfo.getPageHeader().getRectangle13().getXleftcorner());
        dateTextField.setY(documentInfo.getPageHeader().getRectangle13().getYleftcorner());
        dateTextField.setWidth(documentInfo.getPageHeader().getRectangle13().getWidth());
        dateTextField.setStyle(dateStyle);

        int maxDateHeight = documentInfo.getPageHeader().getRectangle13().getHeight();

        // Ajuste la police utilisée pour que le texte tienne dans le
        // rectangle
        JasperdesignUtil.calculeHauteurChamp(dateTextField, this.getDateProductionEditionContent(), maxDateHeight);

        JRDesignExpression expressionDate = new JRDesignExpression();
        expressionDate.setValueClass(java.lang.String.class);
        expressionDate.setText(JasperdesignUtil.buildJasperParam(CLE_DATE_PRODUCTION_EDITION));
        dateTextField.setExpression(expressionDate);
        pageHeaderBand.addElement(dateTextField);
    }

    /**
     * Insère la date de l'édition dans l'entête.
     * 
     * @param jasperDesign
     * @param documentInfo
     * @param titleBand
     */
    private void insererDateEditionDansEnteteDansTitle(JasperDesign jasperDesign, DocumentInfo documentInfo,
        JRDesignBand titleBand)
    {
        JRStyle dateStyle =
            JasperdesignUtil.creerStyleComplet(jasperDesign, STYLE_DATE_DANS_ENTETE_TITLE_EDITION, documentInfo
                .getTitle(), documentInfo.getTitle().getDateStyle(),
                documentInfo.getTitle().getRectangle13().getHorizontalAlignment(),
                documentInfo.getTitle().getRectangle13().getVerticalAlignment(), null);

        JRDesignTextField dateTextField = new JRDesignTextField();
        dateTextField.setBlankWhenNull(true);
        dateTextField.setX(documentInfo.getTitle().getRectangle13().getXleftcorner());
        dateTextField.setY(documentInfo.getTitle().getRectangle13().getYleftcorner());
        dateTextField.setWidth(documentInfo.getTitle().getRectangle13().getWidth());
        dateTextField.setStyle(dateStyle);

        int maxDateHeight = documentInfo.getTitle().getRectangle13().getHeight();

        // Ajuste la police utilisée pour que le texte tienne dans le
        // rectangle
        JasperdesignUtil.calculeHauteurChamp(dateTextField, this.getDateProductionEditionContent(), maxDateHeight);

        JRDesignExpression expressionDate = new JRDesignExpression();
        expressionDate.setValueClass(java.lang.String.class);
        expressionDate.setText(JasperdesignUtil.buildJasperParam(CLE_DATE_PRODUCTION_EDITION));
        dateTextField.setExpression(expressionDate);
        titleBand.addElement(dateTextField);
    }

    /**
     * Insère le logo de l'application dans l'entête.
     * 
     * @param jasperDesign
     * @param model
     * @param documentInfo
     * @param pageHeaderBand
     */
    private void insererLogoApplicationDansEntete(JasperDesign jasperDesign, TableModel model,
        DocumentInfo documentInfo, JRDesignBand pageHeaderBand)
    {
        JRDesignImage logoApplication = new JRDesignImage(jasperDesign);

        logoApplication.setScaleImage(ScaleImageEnum.RETAIN_SHAPE);
        logoApplication.setX(documentInfo.getPageHeader().getLogoApplication().getXleftCorner());
        logoApplication.setY(documentInfo.getPageHeader().getLogoApplication().getYleftCorner());
        logoApplication.setWidth(documentInfo.getPageHeader().getLogoApplication().getWidth());
        logoApplication.setHeight(documentInfo.getPageHeader().getLogoApplication().getHeight());

        logoApplication.getLineBox().setLeftPadding(documentInfo.getPageHeader().getLeftPadding());
        logoApplication.getLineBox().setRightPadding(documentInfo.getPageHeader().getRightPadding());
        logoApplication.getLineBox().setTopPadding(documentInfo.getPageHeader().getTopPadding());
        logoApplication.getLineBox().setBottomPadding(documentInfo.getPageHeader().getBottomPadding());
        logoApplication.setPositionType(PositionTypeEnum.FIX_RELATIVE_TO_TOP);

        JRDesignExpression logoAppliExpression = new JRDesignExpression();

        logoAppliExpression.setValueClass(JRRenderable.class);
        logoAppliExpression.setText(JasperdesignUtil.buildJasperParam(CLE_LOGO_APPLICATION));
        logoApplication.setExpression(logoAppliExpression);

        pageHeaderBand.addElement(logoApplication);

    }

    /**
     * Insère le logo de l'application dans l'entête.
     * 
     * @param jasperDesign
     * @param model
     * @param documentInfo
     * @param titleBand
     */
    private void insererLogoApplicationDansEnteteDansTitle(JasperDesign jasperDesign, TableModel model,
        DocumentInfo documentInfo, JRDesignBand titleBand)
    {
        JRDesignImage logoApplication = new JRDesignImage(jasperDesign);

        logoApplication.setScaleImage(ScaleImageEnum.RETAIN_SHAPE);
        logoApplication.setX(documentInfo.getTitle().getLogoApplication().getXleftCorner());
        logoApplication.setY(documentInfo.getTitle().getLogoApplication().getYleftCorner());
        logoApplication.setWidth(documentInfo.getTitle().getLogoApplication().getWidth());
        logoApplication.setHeight(documentInfo.getTitle().getLogoApplication().getHeight());

        logoApplication.getLineBox().setLeftPadding(documentInfo.getTitle().getLeftPadding());
        logoApplication.getLineBox().setRightPadding(documentInfo.getTitle().getRightPadding());
        logoApplication.getLineBox().setTopPadding(documentInfo.getTitle().getTopPadding());
        logoApplication.getLineBox().setBottomPadding(documentInfo.getTitle().getBottomPadding());
        logoApplication.setPositionType(PositionTypeEnum.FIX_RELATIVE_TO_TOP);

        JRDesignExpression logoAppliExpression = new JRDesignExpression();

        logoAppliExpression.setValueClass(JRRenderable.class);
        logoAppliExpression.setText(JasperdesignUtil.buildJasperParam(CLE_LOGO_APPLICATION));
        logoApplication.setExpression(logoAppliExpression);

        titleBand.addElement(logoApplication);

    }

    /**
     * Insère le nom de l'application dans l'entête.
     * 
     * @param jasperDesign
     * @param model
     * @param documentInfo
     * @param pageHeaderBand
     */
    private void insererNomApplicationDansEntete(JasperDesign jasperDesign, TableModel model,
        DocumentInfo documentInfo, JRDesignBand pageHeaderBand)
    {
        // Champ contenant le texte du titre
        JRStyle nomApplicationStyle =
            JasperdesignUtil.creerStyleComplet(jasperDesign, STYLE_NOM_APPLICATION_EDITION, documentInfo
                .getPageHeader(), documentInfo.getPageHeader().getTitleStyle(),
                documentInfo.getPageHeader().getRectangle11().getHorizontalAlignment(),
                documentInfo.getPageHeader().getRectangle11().getVerticalAlignment(), null);

        JRDesignTextField nomAppliTextField = new JRDesignTextField();
        nomAppliTextField.setBlankWhenNull(true);
        nomAppliTextField.setX(documentInfo.getPageHeader().getRectangle11().getXleftcorner());
        nomAppliTextField.setY(documentInfo.getPageHeader().getRectangle11().getYleftcorner());
        nomAppliTextField.setWidth(documentInfo.getPageHeader().getRectangle11().getWidth());
        nomAppliTextField.setStyle(nomApplicationStyle);

        int maxHeight = documentInfo.getPageHeader().getRectangle11().getHeight();

        // Ajuste la police utilisée pour que le texte tienne
        // dans le rectangle
        JasperdesignUtil.calculeHauteurChamp(nomAppliTextField, this.getTitreEditionContent(model), maxHeight);

        JRDesignExpression expression = new JRDesignExpression();
        expression.setValueClass(java.lang.String.class);
        expression.setText(JasperdesignUtil.buildJasperParam(CLE_NOM_APPLI));
        nomAppliTextField.setExpression(expression);
        pageHeaderBand.addElement(nomAppliTextField);
    }

    /**
     * Insère le nom de l'application dans l'entête.
     * 
     * @param jasperDesign
     * @param model
     * @param documentInfo
     * @param titleBand
     */
    private void insererNomApplicationDansEnteteDansTitle(JasperDesign jasperDesign, TableModel model,
        DocumentInfo documentInfo, JRDesignBand titleBand)
    {
        // Champ contenant le texte du titre
        JRStyle nomApplicationStyle =
            JasperdesignUtil.creerStyleComplet(jasperDesign, STYLE_NOM_APPLICATION_TITLE_EDITION, documentInfo
                .getTitle(), documentInfo.getTitle().getTitleStyle(),
                documentInfo.getTitle().getRectangle11().getHorizontalAlignment(),
                documentInfo.getTitle().getRectangle11().getVerticalAlignment(), null);

        JRDesignTextField nomAppliTextField = new JRDesignTextField();
        nomAppliTextField.setBlankWhenNull(true);
        nomAppliTextField.setX(documentInfo.getTitle().getRectangle11().getXleftcorner());
        nomAppliTextField.setY(documentInfo.getTitle().getRectangle11().getYleftcorner());
        nomAppliTextField.setWidth(documentInfo.getTitle().getRectangle11().getWidth());
        nomAppliTextField.setStyle(nomApplicationStyle);

        int maxHeight = documentInfo.getTitle().getRectangle11().getHeight();

        // Ajuste la police utilisée pour que le texte tienne
        // dans le rectangle
        JasperdesignUtil.calculeHauteurChamp(nomAppliTextField, this.getTitreEditionContent(model), maxHeight);

        JRDesignExpression expression = new JRDesignExpression();
        expression.setValueClass(java.lang.String.class);
        expression.setText(JasperdesignUtil.buildJasperParam(CLE_NOM_APPLI));
        nomAppliTextField.setExpression(expression);
        titleBand.addElement(nomAppliTextField);
    }

    /**
     * Insère la numérotation des pages dans le pied de page.
     * 
     * @param jasperDesign
     * @param documentInfo
     * @param pageFooterBand
     */
    private void insererNumerotationPages(JasperDesign jasperDesign, DocumentInfo documentInfo,
        JRDesignBand pageFooterBand)
    {
        // Champs contenant la numérotation des pages
        JRStyle numeroPageStyle =
            JasperdesignUtil.creerStyle(jasperDesign, STYLE_NUMERO_PAGE_EDITION, documentInfo.getPageFooter()
                .getPageNumeroStyle());

        // 1er champ
        int widthFirstPart = documentInfo.getPageFooter().getRectangle2().getWidth() / 2;

        JRDesignTextField numeroPageFirstTextField = new JRDesignTextField();
        numeroPageFirstTextField.setBlankWhenNull(true);
        numeroPageFirstTextField.setX(documentInfo.getPageFooter().getRectangle2().getXleftcorner());
        numeroPageFirstTextField.setY(documentInfo.getPageFooter().getRectangle2().getYleftcorner());
        numeroPageFirstTextField.setHeight(documentInfo.getLastPageFooter().getRectangle2().getHeight());
        numeroPageFirstTextField.setWidth(widthFirstPart);
        numeroPageFirstTextField.getLineBox().setTopPadding(documentInfo.getPageFooter().getTopPadding());
        numeroPageFirstTextField.getLineBox().setBottomPadding(documentInfo.getPageFooter().getBottomPadding());
        numeroPageFirstTextField.getLineBox().setRightPadding(documentInfo.getPageFooter().getRightPadding());
        numeroPageFirstTextField.getLineBox().setLeftPadding(documentInfo.getPageFooter().getLeftPadding());
        numeroPageFirstTextField.setHorizontalAlignment(HorizontalAlignEnum.RIGHT);
        numeroPageFirstTextField.setVerticalAlignment(VerticalAlignEnum.MIDDLE);
        numeroPageFirstTextField.setStyle(numeroPageStyle);
        numeroPageFirstTextField.getParagraph().setLineSpacing(LineSpacingEnum.SINGLE);

        JRDesignExpression expression = new JRDesignExpression();
        expression.setValueClass(java.lang.String.class);
        expression.setText("\"Page \" + $V{PAGE_NUMBER}");
        numeroPageFirstTextField.setExpression(expression);
        numeroPageFirstTextField.setEvaluationTime(EvaluationTimeEnum.NOW);
        pageFooterBand.addElement(numeroPageFirstTextField);

        // 2eme champ
        int widthSecondPart = documentInfo.getPageFooter().getRectangle2().getWidth() - widthFirstPart;

        JRDesignTextField numeroPageSecondTextField = new JRDesignTextField();
        numeroPageSecondTextField.setBlankWhenNull(true);
        numeroPageSecondTextField.setX(documentInfo.getPageFooter().getRectangle2().getXleftcorner() + widthFirstPart);
        numeroPageSecondTextField.setY(documentInfo.getPageFooter().getRectangle2().getYleftcorner());
        numeroPageSecondTextField.setHeight(documentInfo.getLastPageFooter().getRectangle2().getHeight());
        numeroPageSecondTextField.setWidth(widthSecondPart);
        numeroPageSecondTextField.getLineBox().setTopPadding(documentInfo.getPageFooter().getTopPadding());
        numeroPageSecondTextField.getLineBox().setBottomPadding(documentInfo.getPageFooter().getBottomPadding());
        numeroPageSecondTextField.getLineBox().setRightPadding(documentInfo.getPageFooter().getRightPadding());
        numeroPageSecondTextField.getLineBox().setLeftPadding(documentInfo.getPageFooter().getLeftPadding());
        numeroPageSecondTextField.setHorizontalAlignment(HorizontalAlignEnum.LEFT);
        numeroPageSecondTextField.setVerticalAlignment(VerticalAlignEnum.MIDDLE);
        numeroPageSecondTextField.setStyle(numeroPageStyle);
        numeroPageSecondTextField.getParagraph().setLineSpacing(LineSpacingEnum.SINGLE);

        JRDesignExpression Secondexpression = new JRDesignExpression();
        Secondexpression.setValueClass(java.lang.String.class);
        Secondexpression.setText("\" de \" + $V{PAGE_NUMBER}");
        numeroPageSecondTextField.setExpression(Secondexpression);
        numeroPageSecondTextField.setEvaluationTime(EvaluationTimeEnum.REPORT);
        pageFooterBand.addElement(numeroPageSecondTextField);
    }

    /**
     * Insère un texte complémentaire dans l'entête de l'édition.
     * 
     * @param jasperDesign
     * @param model
     * @param documentInfo
     * @param pageHeaderBand
     */
    private void insererTexteEntete(JasperDesign jasperDesign, TableModel model, DocumentInfo documentInfo,
        JRDesignBand pageHeaderBand)
    {
        JasperdesignUtil
            .ajouterRectangle(pageHeaderBand, model, documentInfo.getPageHeader().getRectangle21(), documentInfo.getLineWidth());

        JRStyle texteComplementStyle =
            JasperdesignUtil.creerStyleComplet(jasperDesign, STYLE_TEXTE_COMPLEMENTAIRE_ENTETE_EDITION, documentInfo
                .getPageHeader(), documentInfo.getPageHeader().getTexteComplementStyle(),
                documentInfo.getPageHeader().getRectangle21().getHorizontalAlignment(),
                documentInfo.getPageHeader().getRectangle21().getVerticalAlignment(), null);

        JRDesignTextField texteComplementField = new JRDesignTextField();
        texteComplementField.setBlankWhenNull(true);
        texteComplementField.setX(documentInfo.getPageHeader().getRectangle21().getXleftcorner());
        texteComplementField.setY(documentInfo.getPageHeader().getRectangle21().getYleftcorner());
        texteComplementField.setWidth(documentInfo.getPageHeader().getRectangle21().getWidth());
        texteComplementField.setStyle(texteComplementStyle);

        if (documentInfo.getPageHeader().getRectangle21().isOpaque())
        {
            Color color =
                new Color(documentInfo.getPageHeader().getRectangle21().getColorR(), documentInfo.getPageHeader().getRectangle21()
                    .getColorG(),
                    documentInfo.getPageHeader().getRectangle21().getColorB());
            texteComplementField.setBackcolor(color);
            texteComplementField.setMode(ModeEnum.OPAQUE);
        }

        int maxComplementHeight = documentInfo.getPageHeader().getRectangle21().getHeight();

        // Ajuste la police utilisée pour que le texte tienne
        // dans le rectangle
        JasperdesignUtil.calculeHauteurChamp(texteComplementField, this.getTexteEnteteContent(model),
            maxComplementHeight);

        JRDesignExpression expressionComplement = new JRDesignExpression();
        expressionComplement.setValueClass(java.lang.String.class);
        expressionComplement.setText(JasperdesignUtil.buildJasperParam(CLE_TEXTE_ENTETE));
        texteComplementField.setExpression(expressionComplement);

        pageHeaderBand.addElement(texteComplementField);
    }

    /**
     * Insère un texte complémentaire dans l'entête de l'édition.
     * 
     * @param jasperDesign
     * @param model
     * @param documentInfo
     * @param titleBand
     */
    private void insererTexteEnteteDansTitle(JasperDesign jasperDesign, TableModel model, DocumentInfo documentInfo,
        JRDesignBand titleBand)
    {
        JasperdesignUtil.ajouterRectangle(titleBand, model, documentInfo.getTitle().getRectangle21(), documentInfo.getLineWidth());

        JRStyle texteComplementStyle =
            JasperdesignUtil.creerStyleComplet(jasperDesign, STYLE_TEXTE_COMPLEMENTAIRE_ENTETE_TITLE_EDITION, documentInfo
                .getTitle(), documentInfo.getTitle().getTexteComplementStyle(),
                documentInfo.getTitle().getRectangle21().getHorizontalAlignment(),
                documentInfo.getTitle().getRectangle21().getVerticalAlignment(), null);

        JRDesignTextField texteComplementField = new JRDesignTextField();
        texteComplementField.setBlankWhenNull(true);
        texteComplementField.setX(documentInfo.getTitle().getRectangle21().getXleftcorner());
        texteComplementField.setY(documentInfo.getTitle().getRectangle21().getYleftcorner());
        texteComplementField.setWidth(documentInfo.getTitle().getRectangle21().getWidth());
        texteComplementField.setStyle(texteComplementStyle);

        if (documentInfo.getTitle().getRectangle21().isOpaque())
        {
            Color color =
                new Color(documentInfo.getTitle().getRectangle21().getColorR(), documentInfo.getTitle().getRectangle21().getColorG(),
                    documentInfo.getTitle().getRectangle21().getColorB());
            texteComplementField.setBackcolor(color);
            texteComplementField.setMode(ModeEnum.OPAQUE);
        }

        // calcule la hauteur du rectangle 21 en fonction du nombre de ligne et de la taille de la police
        int texteComplementFieldHeight = JasperdesignUtil.calculeHauteurTexte(texteComplementField, this.getTexteEnteteContent(model));

        titleBand.setHeight(titleBand.getHeight() - documentInfo.getTitle().getRectangle21().getHeight() + texteComplementFieldHeight);
        texteComplementField.setHeight(texteComplementFieldHeight);

        JRDesignExpression expressionComplement = new JRDesignExpression();
        expressionComplement.setValueClass(java.lang.String.class);
        expressionComplement.setText(JasperdesignUtil.buildJasperParam(CLE_TEXTE_ENTETE));
        texteComplementField.setExpression(expressionComplement);

        titleBand.addElement(texteComplementField);
    }

    /**
     * Insère un texte dans le pied de page de l'édition.
     * 
     * @param jasperDesign
     * @param model
     * @param documentInfo
     * @param pageFooterBand
     */
    private void insererTextePiedDePage(JasperDesign jasperDesign, TableModel model, DocumentInfo documentInfo,
        JRDesignBand pageFooterBand)
    {
        // Création des rectangles du pied de page
        // Rectangle contenant le nom du fichier
        JasperdesignUtil.ajouterRectangle(pageFooterBand, model, documentInfo.getPageFooter().getRectangle1(), documentInfo.getLineWidth());

        // Rectangle contenant la numérotation des pages
        JasperdesignUtil.ajouterRectangle(pageFooterBand, model, documentInfo.getPageFooter().getRectangle2(), documentInfo.getLineWidth());

        // Champs contenant le texte à insérer dans le pied de page
        JRStyle textePiedDePageStyle =
            JasperdesignUtil.creerStyleComplet(jasperDesign, STYLE_TEXTE_DANS_PIED_PAGE_EDITION, documentInfo
                .getPageFooter(), documentInfo.getPageFooter().getTextePiedDePageStyle(), null, null, null);

        JRDesignTextField textePiedDePagerTextField = new JRDesignTextField();

        textePiedDePagerTextField.setBlankWhenNull(true);
        textePiedDePagerTextField.setX(documentInfo.getPageFooter().getRectangle1().getXleftcorner());
        textePiedDePagerTextField.setY(documentInfo.getPageFooter().getRectangle1().getYleftcorner());
        textePiedDePagerTextField.setHeight(documentInfo.getLastPageFooter().getRectangle1().getHeight());
        textePiedDePagerTextField.setWidth(documentInfo.getPageFooter().getRectangle1().getWidth());
        textePiedDePagerTextField.setStyle(textePiedDePageStyle);
        textePiedDePagerTextField.setHorizontalAlignment(documentInfo.getPageFooter().getHorizontalAlignment());
        textePiedDePagerTextField.setVerticalAlignment(documentInfo.getPageFooter().getVerticalAlignment());

        JRDesignExpression nomFichierExpression = new JRDesignExpression();
        nomFichierExpression.setValueClass(java.lang.String.class);
        nomFichierExpression.setText(JasperdesignUtil.buildJasperParam(CLE_TEXTE_PIED_DE_PAGE));
        textePiedDePagerTextField.setExpression(nomFichierExpression);
        pageFooterBand.addElement(textePiedDePagerTextField);
    }

    /**
     * Insère le titre de l'édition dans l'entête.
     * 
     * @param jasperDesign
     * @param model
     * @param documentInfo
     * @param pageHeaderBand
     */
    private void insererTitreDansEntete(JasperDesign jasperDesign, TableModel model, DocumentInfo documentInfo,
        JRDesignBand pageHeaderBand)
    {
        // Champ contenant le texte du titre

        JRStyle titleStyle =
            JasperdesignUtil.creerStyleComplet(jasperDesign, STYLE_TITRE_DANS_ENTETE_EDITION, documentInfo
                .getPageHeader(), documentInfo.getPageHeader().getTitleStyle(),
                documentInfo.getPageHeader().getRectangle12().getHorizontalAlignment(),
                documentInfo.getPageHeader().getRectangle12().getVerticalAlignment(), null);

        JRDesignTextField titleTextField = new JRDesignTextField();
        titleTextField.setBlankWhenNull(true);
        titleTextField.setX(documentInfo.getPageHeader().getRectangle12().getXleftcorner());
        titleTextField.setY(documentInfo.getPageHeader().getRectangle12().getYleftcorner());
        titleTextField.setWidth(documentInfo.getPageHeader().getRectangle12().getWidth());
        titleTextField.setStyle(titleStyle);

        int maxHeight = documentInfo.getPageHeader().getRectangle12().getHeight();

        // Ajuste la police utilisée pour que le texte tienne dans le
        // rectangle
        JasperdesignUtil.calculeHauteurChamp(titleTextField, this.getTitreEditionContent(model), maxHeight);

        JRDesignExpression expression = new JRDesignExpression();
        expression.setValueClass(java.lang.String.class);
        expression.setText(JasperdesignUtil.buildJasperParam(CLE_TITRE_EDITION));
        titleTextField.setExpression(expression);
        pageHeaderBand.addElement(titleTextField);
    }

    /**
     * Insère le titre de l'édition dans l'entête.
     * 
     * @param jasperDesign
     * @param model
     * @param documentInfo
     * @param titleBand
     */
    private void insererTitreDansEnteteDansTitle(JasperDesign jasperDesign, TableModel model, DocumentInfo documentInfo,
        JRDesignBand titleBand)
    {
        // Champ contenant le texte du titre

        JRStyle titleStyle =
            JasperdesignUtil.creerStyleComplet(jasperDesign, STYLE_TITRE_DANS_ENTETE_TITLE_EDITION, documentInfo
                .getTitle(), documentInfo.getTitle().getTitleStyle(),
                documentInfo.getTitle().getRectangle12().getHorizontalAlignment(),
                documentInfo.getTitle().getRectangle12().getVerticalAlignment(), null);

        JRDesignTextField titleTextField = new JRDesignTextField();
        titleTextField.setBlankWhenNull(true);
        titleTextField.setX(documentInfo.getTitle().getRectangle12().getXleftcorner());
        titleTextField.setY(documentInfo.getTitle().getRectangle12().getYleftcorner());
        titleTextField.setWidth(documentInfo.getTitle().getRectangle12().getWidth());
        titleTextField.setStyle(titleStyle);

        int maxHeight = documentInfo.getTitle().getRectangle12().getHeight();

        // Ajuste la police utilisée pour que le texte tienne dans le
        // rectangle
        JasperdesignUtil.calculeHauteurChamp(titleTextField, this.getTitreEditionContent(model), maxHeight);

        JRDesignExpression expression = new JRDesignExpression();
        expression.setValueClass(java.lang.String.class);
        expression.setText(JasperdesignUtil.buildJasperParam(CLE_TITRE_EDITION));
        titleTextField.setExpression(expression);
        titleBand.addElement(titleTextField);
    }

    /**
     * Controle que toutes les colonnes possèdent l'attr width.
     * 
     * @param tableColumns
     * @return true, si c'est all column sized
     */
    private boolean isAllColumnSized(List<Column> tableColumns)
    {
        boolean allColumnsSized = true;
        for (Column column : tableColumns)
        {
            if (column.getWidth() == null || StringUtils.isBlank(column.getWidth()))
            {
                allColumnsSized = false;
                break;
            }
        }
        return allColumnsSized;
    }

    /**
     * Contrôle que toutes les unités utilisées dans l'attribut width sont identiques. et égales à 'px' ou '%'
     * 
     * @param tableColumns
     * @param unit
     * @return true, si c'est column unit coherent
     */
    private boolean isColumnUnitCoherent(List<Column> tableColumns, StringBuilder unit)
    {
        boolean allColumnHasSameUnit = true;
        boolean firstColumn = true;
        for (Column column : tableColumns)
        {
            // Suppression des blancs
            String newVal = column.getWidth().trim().toLowerCase(Locale.FRANCE);

            // TT de la première colonne
            if (firstColumn)
            {
                String unitString = "%";
                if (newVal.endsWith(unitString))
                {
                    unit.append(unitString);
                }
                else if (newVal.endsWith("px"))
                {
                    unitString = "px";
                    unit.append(unitString);
                }
                else
                {
                    log.warn("L'UNITE UTILISEE DANS L'ATTRIBUT WIDTH DOIT ETRE '%' OU 'px'.");
                    allColumnHasSameUnit = false;
                }
                firstColumn = false;
            }
            // TT des autres colonnes
            else
            {
                if (!newVal.endsWith(unit.toString()))
                {
                    log.warn("UN DES ATTRIBUTS WIDTH D'UNE COLONNE N'UTILISE PAS L'ATTRIBUT : " + unit.toString());
                    allColumnHasSameUnit = false;
                    break;
                }
            }
        }

        return allColumnHasSameUnit;
    }

    /**
     * Controle si la somme des largeurs de colonnes telle que fournie par les attributs width des colonnes est
     * inférieure à la taille de la colonne de l'édition Enregistre la taille de chaque colonne dans l'objet ColonneInfo
     * correspondant.
     * 
     * @param tableColumns
     * @param unit
     * @param documentInfo
     * @return true, si c'est column width sum coherent
     */
    private boolean isColumnWidthSumCoherent(List<Column> tableColumns, StringBuilder unit, DocumentInfo documentInfo)
    {
        // On controle la somme de la taille de toutes les colonnes
        boolean widthSumOk = true;
        int colWidthSum = 0;
        int colonneEnCours = 0;
        for (Column column : tableColumns)
        {
            int colWidthVal;
            String newVal = column.getWidth().trim().toLowerCase(Locale.FRANCE);
            String colWidth = StringUtils.replace(newVal, unit.toString(), "");
            try
            {
                colWidthVal = Integer.valueOf(colWidth).intValue();
            }
            catch (NumberFormatException exception)
            {
                log.warn("L'ATTRIBUT WIDTH (" + colWidth + ") D'UNE COLONNE D'UN TABLEAU EC EST INCORRECT.");
                widthSumOk = false;
                break;
            }

            // Au passage, on enregistre la taille des colonnes
            if ("%".equals(unit.toString()))
            {
                int colWidthCalc = colWidthVal * documentInfo.calColumnWidth() / 100;
                (documentInfo.getDetail().getColonnes().get(colonneEnCours)).setTaille(colWidthCalc);
                colWidthSum = colWidthSum + colWidthCalc;
            }
            else
            {
                (documentInfo.getDetail().getColonnes().get(colonneEnCours)).setTaille(colWidthVal);
                colWidthSum = colWidthSum + colWidthVal;
            }

            colonneEnCours++;
        }

        // Si la somme de la taille des colonnes est > à la taille du
        // rapport => incohérence
        if (colWidthSum > documentInfo.calColumnWidth())
        {
            log.warn("LA SOMME DES ATTR WIDTH DES COLONNES EST > A LA TAILLE DE L'EDITION.");
            widthSumOk = false;
        }

        // On réajuste la taille des colonnes en fonction de la somme
        // totale des largeurs de colonnes, de façon à utiliser la largeur
        // totale de la page
        for (ColonneInfo colonneInfo : documentInfo.getDetail().getColonnes())
        {
            colonneInfo.setTaille(colonneInfo.getTaille() * documentInfo.calColumnWidth() / colWidthSum);
        }
        return widthSumOk;
    }

    /**
     * Teste si un texte doit être insérer dans l'entête.
     * 
     * @param model
     * @return true, si c'est texte entete a inserer
     */
    private boolean isTexteEnteteAInserer(TableModel model)
    {
        String textePiedDePage = this.getTexteEnteteContent(model);
        return (textePiedDePage != null && StringUtils.isNotBlank(textePiedDePage));
    }

    /**
     * Teste si un texte doit être insérer dans le pied de page.
     * 
     * @param model
     * @return true, si c'est texte pied de page a inserer
     */
    private boolean isTextePiedDePageAInserer(TableModel model)
    {
        String textePiedDePage = this.getTextePiedDepageContent(model);
        return (textePiedDePage != null && StringUtils.isNotBlank(textePiedDePage));
    }

    /**
     * Configuration de la section lastPageFooter.
     * 
     * @param jasperDesign
     * @param model
     * @param documentInfo
     */
    private void lastPageFooterBandConf(JasperDesign jasperDesign, TableModel model, DocumentInfo documentInfo)
    {
        JRDesignBand lastPageFooterBand = (JRDesignBand) jasperDesign.getLastPageFooter();

        // Last page footer
        lastPageFooterBand.setHeight(documentInfo.getLastPageFooter().getHeight());

        if (isTextePiedDePageAInserer(model))
        {
            insererTextePiedDePage(jasperDesign, model, documentInfo, lastPageFooterBand);
        }

        insererNumerotationPages(jasperDesign, documentInfo, lastPageFooterBand);

    }

    /**
     * Construit un objet de type DocumentInfo, décrivant l'édition à produire en utilisant les paramètres du modèle
     * choisit dans le fichier de configuration passé en paramètre.
     * 
     * @param fichierConfiguration
     * @param modele
     * @param model
     * @return objet DocumentInfo décrivant l'édition
     * @throws ConfigurationException the configuration exception
     */
    private DocumentInfo lectureFichierConfiguration(String fichierConfiguration, String modele, TableModel model)
        throws ConfigurationException
    {
        StringBuilder fullPathConf = new StringBuilder();
        fullPathConf.append(getImageAndConfBaseAbsolutePath(model));
        fullPathConf.append(File.separator);
        fullPathConf.append(fichierConfiguration);

        if (log.isDebugEnabled())
        {
            log.debug("Fichier de configuration :" + fichierConfiguration);
        }
        XMLConfiguration config;
        BeanDeclaration declaration;
        DocumentInfo documentInfo;

        try
        {
            // nouveau code avec commons configuration 2:
            Configurations configs = new Configurations();
            config = configs.xml(fullPathConf.toString());
            // ancien ccode avec commons configuration 1 : config = new XMLConfiguration(fullPathConf.toString());
            declaration = new XMLBeanDeclaration(config, modele + ".documentinfo");
            // ancien code avec commons configuration 1 :documentInfo =
            // (DocumentInfo)BeanHelper.createBean(declaration);
            // nouveau code avec commons configuration 2:
            documentInfo = (DocumentInfo) new BeanHelper().createBean(declaration);
        }
        catch (ConfigurationException configException)
        {
            try
            {
                fullPathConf.delete(0, fullPathConf.length());
                fullPathConf.append(EDITION_IMAGES_ET_CONF_DEF_CLASSPATH);
                fullPathConf.append("/");
                fullPathConf.append(fichierConfiguration);
                // nouveau code avec commons configuration 2:
                Configurations configs = new Configurations();
                config = configs.xml(fullPathConf.toString());
                // ancien ccode avec commons configuration 1 : config = new XMLConfiguration(fullPathConf.toString());
                declaration = new XMLBeanDeclaration(config, modele + ".documentinfo");
                // ancien code avec commons configuration 1 :documentInfo =
                // (DocumentInfo)BeanHelper.createBean(declaration);
                documentInfo = (DocumentInfo) new BeanHelper().createBean(declaration);
            }
            catch (ConfigurationException configExcep)
            {
                log.warn(configException);
                // ancien code
                // EcTableException re = new EcTableException(configExcep.getMessage(),configExcep);
                // throw re;

                // nouveau code
                throw new EcTableException(configExcep.getMessage(), configExcep);
            }
        }

        return documentInfo;
    }

    /**
     * Configuration de la section pageFooter.
     * 
     * @param jasperDesign
     * @param model
     * @param documentInfo
     */
    private void pageFooterBandConf(JasperDesign jasperDesign, TableModel model, DocumentInfo documentInfo)
    {
        JRDesignBand pageFooterBand = (JRDesignBand) jasperDesign.getPageFooter();

        // Page footer
        pageFooterBand.setHeight(documentInfo.getPageFooter().getHeight());

        if (isTextePiedDePageAInserer(model))
        {
            insererTextePiedDePage(jasperDesign, model, documentInfo, pageFooterBand);
        }

        insererNumerotationPages(jasperDesign, documentInfo, pageFooterBand);

    }

    /**
     * Configuration de la section entête de page.
     * 
     * @param jasperDesign
     * @param model
     * @param documentInfo
     */
    private void pageHeaderBandConf(JasperDesign jasperDesign, TableModel model, DocumentInfo documentInfo)
    {
        JRDesignBand pageHeaderBand = (JRDesignBand) jasperDesign.getPageHeader();

        // Page header
        pageHeaderBand.setHeight(documentInfo.getPageHeader().getHeight());

        // Création des rectangles de l'entête de page
        JasperdesignUtil
            .ajouterRectangle(pageHeaderBand, model, documentInfo.getPageHeader().getRectangle11(), documentInfo.getLineWidth());
        JasperdesignUtil
            .ajouterRectangle(pageHeaderBand, model, documentInfo.getPageHeader().getRectangle12(), documentInfo.getLineWidth());
        JasperdesignUtil
            .ajouterRectangle(pageHeaderBand, model, documentInfo.getPageHeader().getRectangle13(), documentInfo.getLineWidth());

        // Insertion du logo de l'application dans le rectangle 11

        if (documentInfo.getPageHeader().getUseApplicationLogo() != null
            && documentInfo.getPageHeader().getUseApplicationLogo().booleanValue())
        {
            this.insererLogoApplicationDansEntete(jasperDesign, model, documentInfo, pageHeaderBand);
        }
        else
        // Sinon insertion du nom de l'application
        {
            this.insererNomApplicationDansEntete(jasperDesign, model, documentInfo, pageHeaderBand);
        }

        // Insertion du titre dans le rectangle 12
        insererTitreDansEntete(jasperDesign, model, documentInfo, pageHeaderBand);

        // Insertion de la date de l'édition dans le rectangle 13
        this.insererDateEditionDansEntete(jasperDesign, documentInfo, pageHeaderBand);

        if (this.isTexteEnteteAInserer(model))
        {
            // Insertion d'un texte complémentaire dans le rectangle 21
            this.insererTexteEntete(jasperDesign, model, documentInfo, pageHeaderBand);
        }
    }

    /**
     * Ajout des paramètres utilisés dans el modèle jasperreports.
     * 
     * @param jasperDesign
     * @param model
     * @param documentInfo
     */
    private void parametersConf(JasperDesign jasperDesign, TableModel model, DocumentInfo documentInfo)
    {
        try
        {
            // Parametres

            jasperDesign.addParameter(JasperdesignUtil.createJasperStringParameter(CLE_NOM_APPLI));
            jasperDesign.addParameter(JasperdesignUtil.createJasperStringParameter(CLE_TITRE_EDITION));
            jasperDesign.addParameter(JasperdesignUtil.createJasperStringParameter(CLE_DATE_PRODUCTION_EDITION));

            if (isTexteEnteteAInserer(model))
            {
                jasperDesign.addParameter(JasperdesignUtil.createJasperStringParameter(CLE_TEXTE_ENTETE));
            }

            if (isTextePiedDePageAInserer(model))
            {
                jasperDesign.addParameter(JasperdesignUtil.createJasperStringParameter(CLE_TEXTE_PIED_DE_PAGE));
            }

            jasperDesign.addParameter(JasperdesignUtil.createJasperJRRenderableParameter(CLE_LOGO));
            jasperDesign.addParameter(JasperdesignUtil.createJasperJRRenderableParameter(CLE_LOGO_MINISTERE));
            jasperDesign.addParameter(JasperdesignUtil.createJasperJRRenderableParameter(CLE_LOGO_APPLICATION));

            jasperDesign.addParameter(JasperdesignUtil.createJasperCollectionParameter(CLE_GRAPH_DATA_SOURCE));

            if (documentInfo.isDocumentAvecGraphiques() || documentInfo.isDocumentAvecSousRapports())
            {
                List<GraphiqueInfo> graphiquesInfo = documentInfo.getSummaryInfo().getGraphiquesInfos();
                List<SousRapportInfo> sousRapportsInfo = documentInfo.getSummaryInfo().getSousRapportsInfos();

                int graphEnCours = 0;
                for (int i = 0; i < graphiquesInfo.size(); i++)
                {
                    jasperDesign.addParameter(JasperdesignUtil.createJasperJRRenderableParameter(CLE_GRAPH_DATA_SOURCE
                        + graphEnCours));
                    jasperDesign.addParameter(JasperdesignUtil
                        .createJasperJasperReportParameter(CLE_SUBREPORT_JASPER_REPORT + graphEnCours));
                    graphEnCours++;
                }
                for (int i = 0; i < sousRapportsInfo.size(); i++)
                {
                    jasperDesign.addParameter(JasperdesignUtil
                        .createJasperJasperReportParameter(CLE_SUBREPORT_JASPER_REPORT + graphEnCours));
                    graphEnCours++;
                }
            }

        }
        catch (JRException exception)
        {
            throw new EcTableException(exception);
        }

    }

    /**
     * Lit dans le fichier extremecomponents.properties le nom du répertoire contenant les fichiers de configuration et
     * les images utilisées par l'export. Le répertoire est positionné en relatif.
     * 
     * @param model
     * @return répertoire de stockage des images et des fichiers de configuration
     */
    private String retrieveBaseImageAndConfRelativePath(TableModel model)
    {
        String resourceValue = model.getMessages().getMessage(EDITION_IMAGES_ET_CONF_PATH);
        if (resourceValue == null)
        {
            resourceValue = DEFAULT_EDITION_IMAGES_ET_CONF_PATH;
        }

        return resourceValue;
    }

    /**
     * Configuration de la section summary destinée à contenir les graphiques à inclure si nécessaire en fin d'édition.
     * 
     * @param jasperDesign
     * @param model
     * @param documentInfo
     * @param listGraphSubReportDesign
     */
    private void summaryBandConf(JasperDesign jasperDesign, TableModel model, DocumentInfo documentInfo,
        List<JasperDesign> listGraphSubReportDesign)
    {

        // Création d'un groupe fictif de façon à utiliser l'élément
        // groupFooter
        // comme une section summary comportant un pied de page et un
        // entête de page
        JRDesignGroup group = new JRDesignGroup();
        group.setName("groupefictif");

        // Variable de groupe fictive
        JRDesignVariable variable = new JRDesignVariable();
        variable.setName("variablegroupefictif");
        variable.setValueClass(java.lang.String.class);
        variable.setResetType(ResetTypeEnum.GROUP);
        variable.setResetGroup(group);
        variable.setCalculation(CalculationEnum.SYSTEM);
        JRDesignExpression expression = new JRDesignExpression();
        expression.setValueClass(java.lang.String.class);
        expression.setText("\"1\"");
        variable.setInitialValueExpression(expression);

        group.setMinHeightToStartNewPage(60);
        expression = new JRDesignExpression();
        expression.setValueClass(java.lang.String.class);
        expression.setText("\"1\"");
        group.setExpression(expression);

        JRDesignBand groupFooterBand = new JRDesignBand();
        try
        {
            jasperDesign.addVariable(variable);
            jasperDesign.addGroup(group);

            // group.setGroupFooter(groupFooterBand);
            ((JRDesignSection) group.getGroupFooterSection()).addBand(groupFooterBand);
        }
        catch (JRException exception)
        {
            throw new EcTableException(exception);
        }

        groupFooterBand.setHeight(70);
        // groupFooterBand.setSplitAllowed(true);
        groupFooterBand.setSplitType(SplitTypeEnum.STRETCH);

        int graphEnCours = 0;
        int ySubReport = 0;
        for (JasperDesign graphSubReportDesign : listGraphSubReportDesign)
        {
            JRDesignParameter parameterImage = new JRDesignParameter();
            parameterImage.setName(CLE_GRAPH_DATA_SOURCE + graphEnCours);
            parameterImage.setValueClass(JRRenderable.class);
            try
            {
                graphSubReportDesign.addParameter(parameterImage);
            }
            catch (JRException exception)
            {
                throw new EcTableException(exception);
            }

            GraphiqueInfo graphiqueInfo =
                documentInfo.getSummaryInfo().getGraphiquesInfos().get(graphEnCours);

            JRDesignImage graphique = new JRDesignImage(jasperDesign);

            int heightGraph =
                Math.min(documentInfo.getPageHeight() - documentInfo.getTopMargin() - documentInfo.getBottomMargin()
                    - documentInfo.getPageHeader().getHeight() - documentInfo.getPageFooter().getHeight()
                    - (documentInfo.getSummaryInfo().getPadding() * 2 + 4), graphiqueInfo.getRectangleInfo()
                        .getHeight());

            graphique.setScaleImage(ScaleImageEnum.RETAIN_SHAPE);
            graphique.setX(0);
            graphique.setY(0);
            graphique.setWidth(documentInfo.calColumnWidth());
            graphique.setHeight(heightGraph);
            graphique.getLineBox().setPadding(documentInfo.getSummaryInfo().getPadding());
            graphique.setPositionType(PositionTypeEnum.FIX_RELATIVE_TO_TOP);
            graphique.setHorizontalAlignment(HorizontalAlignEnum.CENTER);
            graphique.setVerticalAlignment(VerticalAlignEnum.MIDDLE);
            graphique.setPositionType(PositionTypeEnum.FLOAT);
            JRDesignExpression imageExpression = new JRDesignExpression();

            imageExpression.setValueClass(JRRenderable.class);
            imageExpression.setText(JasperdesignUtil.buildJasperParam(CLE_GRAPH_DATA_SOURCE + graphEnCours));
            graphique.setExpression(imageExpression);

            JRDesignBand subreportSummaryDesignBand = (JRDesignBand) graphSubReportDesign.getSummary();

            subreportSummaryDesignBand.setHeight(graphiqueInfo.getRectangleInfo().getHeight()
                + documentInfo.getSummaryInfo().getPadding() * 2 + 4);
            subreportSummaryDesignBand.addElement(graphique);

            // Insertion d'un sous-rapport dans la section summary
            JRDesignSubreport jrDesignSubreport = new JRDesignSubreport(graphSubReportDesign);
            jrDesignSubreport.setX(0);
            jrDesignSubreport.setY(ySubReport);
            jrDesignSubreport.setWidth(documentInfo.calColumnWidth());
            jrDesignSubreport.setHeight(10);
            jrDesignSubreport.setPositionType(PositionTypeEnum.FLOAT);

            ySubReport = ySubReport + 11;

            JRDesignExpression subReportExpression = new JRDesignExpression();
            subReportExpression.setValueClass(JasperReport.class);
            subReportExpression.setText(JasperdesignUtil.buildJasperParam(CLE_SUBREPORT_JASPER_REPORT + graphEnCours));
            jrDesignSubreport.setExpression(subReportExpression);

            JRDesignExpression subreportDSExpression = new JRDesignExpression();
            subreportDSExpression.setValueClass(JRBeanCollectionDataSource.class);
            subreportDSExpression.setText("new net.sf.jasperreports.engine.data.JRBeanCollectionDataSource($P{"
                + CLE_GRAPH_DATA_SOURCE + "})");
            jrDesignSubreport.setDataSourceExpression(subreportDSExpression);

            JRDesignExpression subreportRenderableExpression = new JRDesignExpression();
            subreportRenderableExpression.setValueClass(JRRenderable.class);
            subreportRenderableExpression.setText(JasperdesignUtil.buildJasperParam(CLE_GRAPH_DATA_SOURCE
                + graphEnCours));

            JRDesignSubreportParameter subreportParameter = new JRDesignSubreportParameter();
            subreportParameter.setExpression(subreportRenderableExpression);
            subreportParameter.setName(CLE_GRAPH_DATA_SOURCE + graphEnCours);
            try
            {
                jrDesignSubreport.addParameter(subreportParameter);
            }
            catch (JRException exception)
            {
                throw new EcTableException(exception);
            }

            groupFooterBand.addElement(jrDesignSubreport);

            graphEnCours++;
        }

    }

    /**
     * Configuration de la section summary destinée à contenir les graphiques à inclure si nécessaire en fin d'édition
     * sur une nouvelle page.
     * 
     * @param jasperDesign
     * @param model
     * @param documentInfo
     * @param listGraphSubReportDesign
     * @param listSousRapportDesign
     */
    private void summaryBandConfNewPage(JasperDesign jasperDesign, TableModel model, DocumentInfo documentInfo,
        List<JasperDesign> listGraphSubReportDesign, List<JasperDesign> listSousRapportDesign)
    {

        JRDesignBand summaryBand = new JRDesignBand();

        summaryBand.setHeight(88);
        // pas sur
        summaryBand.setSplitType(SplitTypeEnum.STRETCH);

        int graphEnCours = 0;
        int ySubReport = 0;
        for (JasperDesign graphSubReportDesign : listGraphSubReportDesign)
        {
            JRDesignParameter parameterImage = new JRDesignParameter();
            parameterImage.setName(CLE_GRAPH_DATA_SOURCE + graphEnCours);
            parameterImage.setValueClass(JRRenderable.class);
            try
            {
                graphSubReportDesign.addParameter(parameterImage);
            }
            catch (JRException exception)
            {
                throw new EcTableException(exception);
            }

            GraphiqueInfo graphiqueInfo =
                documentInfo.getSummaryInfo().getGraphiquesInfos().get(graphEnCours);

            JRDesignImage graphique = new JRDesignImage(jasperDesign);

            int heightGraph =
                Math.min(documentInfo.getPageHeight() - documentInfo.getTopMargin() - documentInfo.getBottomMargin()
                    - documentInfo.getPageHeader().getHeight() - documentInfo.getPageFooter().getHeight()
                    - (documentInfo.getSummaryInfo().getPadding() * 2 + 4), graphiqueInfo.getRectangleInfo()
                        .getHeight());

            graphique.setScaleImage(ScaleImageEnum.RETAIN_SHAPE);
            graphique.setX(0);
            graphique.setY(0);
            graphique.setWidth(documentInfo.calColumnWidth());
            graphique.setHeight(heightGraph);
            graphique.getLineBox().setPadding(documentInfo.getSummaryInfo().getPadding());
            graphique.setPositionType(PositionTypeEnum.FIX_RELATIVE_TO_TOP);
            graphique.setHorizontalAlignment(HorizontalAlignEnum.CENTER);
            graphique.setVerticalAlignment(VerticalAlignEnum.MIDDLE);
            graphique.setPositionType(PositionTypeEnum.FLOAT);
            JRDesignExpression imageExpression = new JRDesignExpression();

            imageExpression.setValueClass(JRRenderable.class);
            imageExpression.setText(JasperdesignUtil.buildJasperParam(CLE_GRAPH_DATA_SOURCE + graphEnCours));
            graphique.setExpression(imageExpression);

            JRDesignBand subreportSummaryDesignBand = (JRDesignBand) graphSubReportDesign.getSummary();

            subreportSummaryDesignBand.setHeight(graphiqueInfo.getRectangleInfo().getHeight()
                + documentInfo.getSummaryInfo().getPadding() * 2 + 4);
            subreportSummaryDesignBand.addElement(graphique);

            // Insertion d'un sous-rapport dans la section summary
            JRDesignSubreport jrDesignSubreport = new JRDesignSubreport(graphSubReportDesign);
            jrDesignSubreport.setX(0);
            jrDesignSubreport.setY(ySubReport);
            jrDesignSubreport.setWidth(documentInfo.calColumnWidth());
            jrDesignSubreport.setHeight(10);
            jrDesignSubreport.setPositionType(PositionTypeEnum.FLOAT);

            ySubReport = ySubReport + 11;

            JRDesignExpression subReportExpression = new JRDesignExpression();
            subReportExpression.setValueClass(JasperReport.class);
            subReportExpression.setText(JasperdesignUtil.buildJasperParam(CLE_SUBREPORT_JASPER_REPORT + graphEnCours));
            jrDesignSubreport.setExpression(subReportExpression);

            JRDesignExpression subreportDSExpression = new JRDesignExpression();
            subreportDSExpression.setValueClass(JRBeanCollectionDataSource.class);
            subreportDSExpression.setText("new net.sf.jasperreports.engine.data.JRBeanCollectionDataSource($P{"
                + CLE_GRAPH_DATA_SOURCE + "})");
            jrDesignSubreport.setDataSourceExpression(subreportDSExpression);

            JRDesignExpression subreportRenderableExpression = new JRDesignExpression();
            subreportRenderableExpression.setValueClass(JRRenderable.class);
            subreportRenderableExpression.setText(JasperdesignUtil.buildJasperParam(CLE_GRAPH_DATA_SOURCE
                + graphEnCours));

            JRDesignSubreportParameter subreportParameter = new JRDesignSubreportParameter();
            subreportParameter.setExpression(subreportRenderableExpression);
            subreportParameter.setName(CLE_GRAPH_DATA_SOURCE + graphEnCours);
            try
            {
                jrDesignSubreport.addParameter(subreportParameter);
            }
            catch (JRException exception)
            {
                throw new EcTableException(exception);
            }

            summaryBand.addElement(jrDesignSubreport);

            graphEnCours++;
        }

        for (JasperDesign sousRapportDesign : listSousRapportDesign)
        {

            // Insertion d'un sous-rapport dans la section summary
            JRDesignSubreport jrDesignSubreport = new JRDesignSubreport(sousRapportDesign);
            jrDesignSubreport.setX(0);
            jrDesignSubreport.setY(ySubReport);
            jrDesignSubreport.setWidth(documentInfo.calColumnWidth());
            jrDesignSubreport.setHeight(10);
            jrDesignSubreport.setPositionType(PositionTypeEnum.FLOAT);

            ySubReport = ySubReport + 11;

            JRDesignExpression subReportExpression = new JRDesignExpression();
            subReportExpression.setValueClass(JasperReport.class);
            subReportExpression.setText(JasperdesignUtil.buildJasperParam(CLE_SUBREPORT_JASPER_REPORT + graphEnCours));
            jrDesignSubreport.setExpression(subReportExpression);

            JRDesignExpression subreportDSExpression = new JRDesignExpression();
            subreportDSExpression.setValueClass(JRBeanCollectionDataSource.class);
            subreportDSExpression.setText("new net.sf.jasperreports.engine.data.JRBeanCollectionDataSource($P{"
                + CLE_GRAPH_DATA_SOURCE + "})");
            jrDesignSubreport.setDataSourceExpression(subreportDSExpression);

            summaryBand.addElement(jrDesignSubreport);

            graphEnCours++;
        }

        jasperDesign.setSummary(summaryBand);

    }

    /**
     * Configuration de la section Titre et insertion d'éléments.
     * 
     * @param jasperDesign
     * @param model
     * @param documentInfo
     */
    private void titleBandConf(JasperDesign jasperDesign, TableModel model, DocumentInfo documentInfo)
    {

        JRDesignBand titleBand = (JRDesignBand) jasperDesign.getTitle();

        titleBand.setSplitType(SplitTypeEnum.PREVENT);

        if (documentInfo.isUseTitleOnNewPage())
        {
            int titleHeight = documentInfo.getPageHeight() - documentInfo.getBottomMargin() - documentInfo.getTopMargin();

            // Hauteur de la section TITLE
            titleBand.setHeight(titleHeight);

            // Insertion du logo Mariane
            JRDesignImage logo = new JRDesignImage(jasperDesign);
            logo.setScaleImage(ScaleImageEnum.RETAIN_SHAPE);
            logo.setX(documentInfo.getTitle().getLogoMariane().getXleftCorner());
            logo.setY(documentInfo.getTitle().getLogoMariane().getYleftCorner());
            logo.setWidth(documentInfo.getTitle().getLogoMariane().getWidth());
            logo.setHeight(documentInfo.getTitle().getLogoMariane().getHeight());
            logo.setPositionType(PositionTypeEnum.FLOAT);

            JRDesignExpression logoExpression = new JRDesignExpression();
            logoExpression.setValueClass(JRRenderable.class);
            logoExpression.setText("$P{" + CLE_LOGO + "}");
            logo.setExpression(logoExpression);

            titleBand.addElement(logo);

            int posY = documentInfo.getTitle().getYleftCornerTitleText();

            JRStyle titleStyle =
                JasperdesignUtil.creerStyleComplet(jasperDesign, STYLE_TITRE_EDITION, documentInfo.getTitle(), documentInfo
                    .getTitle().getStyle(), null, null, null);

            JRDesignTextField textField = new JRDesignTextField();
            textField.setBlankWhenNull(true);
            textField.setX(0);
            textField.setY(posY);
            textField.setWidth(documentInfo.calColumnWidth());
            textField.setStyle(titleStyle);

            int maxHeight = documentInfo.getTitle().getMaxHeightTitleText();

            int paragraphTitreHeight =
                JasperdesignUtil.calculeHauteurChamp(textField, this.getTitreEditionContent(model), maxHeight);

            // Affectation de la hauteur calculée au champ titre
            textField.setHeight(paragraphTitreHeight);

            JRDesignExpression expression = new JRDesignExpression();
            expression.setValueClass(java.lang.String.class);
            expression.setText(JasperdesignUtil.buildJasperParam(CLE_TITRE_EDITION));
            textField.setExpression(expression);
            titleBand.addElement(textField);

            posY = posY + paragraphTitreHeight;

            JRStyle titleDateStyle =
                JasperdesignUtil.creerStyleComplet(jasperDesign, STYLE_DATE_PRODUCTION_EDITION, documentInfo.getTitle(),
                    documentInfo.getTitle().getDateStyle(), null, null, null);

            JRDesignTextField dateField = new JRDesignTextField();
            dateField.setX(0);
            dateField.setY(posY);
            dateField.setWidth(documentInfo.calColumnWidth());

            dateField.setStyle(titleDateStyle);

            String dateTitleContent = this.getDateProductionEditionContent();

            int maxHeightdateTitleContent = documentInfo.getTitle().getMaxHeightDateText();

            int paragraphDateTitreHeight =
                JasperdesignUtil.calculeHauteurChamp(dateField, dateTitleContent, maxHeightdateTitleContent);

            dateField.setHeight(paragraphDateTitreHeight);

            JRDesignExpression expressionDate = new JRDesignExpression();
            expressionDate.setValueClass(java.lang.String.class);
            expressionDate.setText("$P{" + CLE_DATE_PRODUCTION_EDITION + "}");
            dateField.setExpression(expressionDate);

            titleBand.addElement(dateField);

            posY = posY + paragraphDateTitreHeight;

            // Insertion du rectangle qui entoure le titre et la date
            JRDesignRectangle rectangle = new JRDesignRectangle();

            rectangle.setX(0);
            rectangle.setY(documentInfo.getTitle().getYleftCornerTitleText());
            rectangle.setWidth(documentInfo.calColumnWidth());
            rectangle.setHeight(posY - documentInfo.getTitle().getYleftCornerTitleText());
            rectangle.setForecolor(Color.BLACK);
            rectangle.setBackcolor(Color.WHITE);
            rectangle.setMode(ModeEnum.TRANSPARENT);
            rectangle.getLinePen().setLineWidth(JRPen.LINE_WIDTH_1);
            rectangle.getLinePen().setLineStyle(LineStyleEnum.SOLID);
            titleBand.addElement(rectangle);

            // Insertion du logo ministère
            JRDesignImage logoMinistere = new JRDesignImage(jasperDesign);

            logoMinistere.setScaleImage(ScaleImageEnum.RETAIN_SHAPE);
            logoMinistere.setX(documentInfo.getTitle().getLogoMinistere().getXleftCorner());
            logoMinistere.setY(documentInfo.getTitle().getLogoMinistere().getYleftCorner());
            logoMinistere.setWidth(documentInfo.getTitle().getLogoMinistere().getWidth());
            logoMinistere.setHeight(documentInfo.getTitle().getLogoMinistere().getHeight());
            logoMinistere.getLineBox().setPadding(0);
            logoMinistere.setPositionType(PositionTypeEnum.FIX_RELATIVE_TO_TOP);

            JRDesignExpression logoMinExpression = new JRDesignExpression();

            logoMinExpression.setValueClass(JRRenderable.class);
            logoMinExpression.setText("$P{" + CLE_LOGO_MINISTERE + "}");
            logoMinistere.setExpression(logoMinExpression);

            titleBand.addElement(logoMinistere);

        }
        else
        {
            titleBand.setHeight(documentInfo.getTitle().getHeight());

            // Création des rectangles de l'entête de page
            JasperdesignUtil.ajouterRectangle(titleBand, model, documentInfo.getTitle().getRectangle11(), documentInfo.getLineWidth());
            JasperdesignUtil.ajouterRectangle(titleBand, model, documentInfo.getTitle().getRectangle12(), documentInfo.getLineWidth());
            JasperdesignUtil.ajouterRectangle(titleBand, model, documentInfo.getTitle().getRectangle13(), documentInfo.getLineWidth());

            // Insertion du logo de l'application dans le rectangle 11
            if (documentInfo.getTitle().getUseApplicationLogo() != null
                && documentInfo.getTitle().getUseApplicationLogo().booleanValue())
            {
                this.insererLogoApplicationDansEnteteDansTitle(jasperDesign, model, documentInfo, titleBand);
            }
            else
            // Sinon insertion du nom de l'application
            {
                this.insererNomApplicationDansEnteteDansTitle(jasperDesign, model, documentInfo, titleBand);
            }

            // Insertion du titre dans le rectangle 12
            insererTitreDansEnteteDansTitle(jasperDesign, model, documentInfo, titleBand);

            // Insertion de la date de l'édition dans le rectangle 13
            this.insererDateEditionDansEnteteDansTitle(jasperDesign, documentInfo, titleBand);

            if (this.isTexteEnteteAInserer(model))
            {
                // Insertion d'un texte complémentaire dans le rectangle 21
                this.insererTexteEnteteDansTitle(jasperDesign, model, documentInfo, titleBand);
            }
        }
    }

}
