/*
 * Copyright (c) 2014 DGFiP - Tous droits réservés
 *//*
/*Copyright (c) 2014 DGFiP - Tous droits réservés/* * Copyright 2004 original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package fr.gouv.finances.lombok.apptags.table.view;

import fr.gouv.finances.lombok.apptags.table.bean.Column;
import fr.gouv.finances.lombok.apptags.table.core.TableModel;
import fr.gouv.finances.lombok.apptags.table.view.html.CpCalcBuilder;
import fr.gouv.finances.lombok.apptags.table.view.html.FormBuilder;
import fr.gouv.finances.lombok.apptags.table.view.html.RowBuilder;
import fr.gouv.finances.lombok.apptags.table.view.html.TableBuilder;
import fr.gouv.finances.lombok.apptags.util.HtmlBuilder;

/**
 * Class AbstractHtmlView
 */
public abstract class AbstractHtmlView implements View
{

    /** html. */
    private HtmlBuilder html;

    /** model. */
    private TableModel model;

    /** form builder. */
    private FormBuilder formBuilder;

    /** buffer view. */
    private boolean bufferView;

    /** table builder. */
    private TableBuilder tableBuilder;

    /** row builder. */
    private RowBuilder rowBuilder;

    /** cp calc builder. */
    private CpCalcBuilder cpCalcBuilder;

    /**
     * Constructeur de la classe AbstractHtmlView.java
     */
    public AbstractHtmlView()
    {
        super();

    }

    /**
     * Accesseur de l attribut html builder.
     * 
     * @return html builder
     */
    protected HtmlBuilder getHtmlBuilder()
    {
        return html;
    }

    /**
     * Accesseur de l attribut table model.
     * 
     * @return table model
     */
    protected TableModel getTableModel()
    {
        return model;
    }

    /**
     * Accesseur de l attribut table builder.
     * 
     * @return table builder
     */
    protected TableBuilder getTableBuilder()
    {
        return tableBuilder;
    }

    /**
     * Modificateur de l attribut table builder.
     * 
     * @param tableBuilder le nouveau table builder
     */
    protected void setTableBuilder(TableBuilder tableBuilder)
    {
        this.tableBuilder = tableBuilder;
    }

    /**
     * Accesseur de l attribut row builder.
     * 
     * @return row builder
     */
    public RowBuilder getRowBuilder()
    {
        return rowBuilder;
    }

    /**
     * Modificateur de l attribut row builder.
     * 
     * @param rowBuilder le nouveau row builder
     */
    protected void setRowBuilder(RowBuilder rowBuilder)
    {
        this.rowBuilder = rowBuilder;
    }

    /**
     * Accesseur de l attribut cp calc builder.
     * 
     * @return cp calc builder
     */
    public CpCalcBuilder getCpCalcBuilder()
    {
        return cpCalcBuilder;
    }

    /**
     * Modificateur de l attribut cp calc builder.
     * 
     * @param cpCalcBuilder le nouveau cp calc builder
     */
    protected void setCpCalcBuilder(CpCalcBuilder cpCalcBuilder)
    {
        this.cpCalcBuilder = cpCalcBuilder;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.view.View#beforeBody(fr.gouv.finances.lombok.apptags.table.core.TableModel)
     */
    @Override
    public final void beforeBody(TableModel model)
    {
        this.model = model;

        bufferView = model.getTableHandler().getTable().isBufferView();
        if (bufferView)
        {
            html = new HtmlBuilder();
        }
        else
        {
            html = new HtmlBuilder(model.getContext().getWriter());
        }

        formBuilder = new FormBuilder(html, model);

        init(html, model);

        formBuilder.formStart();

        tableBuilder.themeStart();

        beforeBodyInternal(model);
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.view.View#body(fr.gouv.finances.lombok.apptags.table.core.TableModel,
     *      fr.gouv.finances.lombok.apptags.table.bean.Column)
     */
    @Override
    public void body(TableModel model, Column column)
    {
        if (column.isFirstColumn())
        {
            rowBuilder.rowStart();
        }

        html.append(column.getCellDisplay());

        if (column.isLastColumn())
        {
            rowBuilder.rowEnd();
        }
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.view.View#afterBody(fr.gouv.finances.lombok.apptags.table.core.TableModel)
     */
    @Override
    public final Object afterBody(TableModel model)
    {
        afterBodyInternal(model);

        tableBuilder.themeEnd();

        formBuilder.formEnd();

        if (bufferView)
        {
            return html.toString();
        }

        return "";
    }

    /**
     * methode Inits the :
     * 
     * @param html
     * @param model
     */
    protected void init(HtmlBuilder html, TableModel model)
    {
        setTableBuilder(new TableBuilder(html, model));
        setRowBuilder(new RowBuilder(html, model));
        setCpCalcBuilder(new CpCalcBuilder(html, model));
    }

    /**
     * methode Before body internal :
     * 
     * @param model
     */
    protected abstract void beforeBodyInternal(TableModel model);

    /**
     * methode After body internal :
     * 
     * @param model
     */
    protected abstract void afterBodyInternal(TableModel model);

}
