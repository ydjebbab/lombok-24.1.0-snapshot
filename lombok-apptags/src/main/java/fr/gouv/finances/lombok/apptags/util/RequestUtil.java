/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
    Copyright 2004 Jenkov Development

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http:/www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : RequestUtil.java
 *
 */

package fr.gouv.finances.lombok.apptags.util;

import java.lang.reflect.InvocationTargetException;

import javax.servlet.jsp.PageContext;

/**
 * Class RequestUtil --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.3 $ Date: 11 déc. 2009
 */
public class RequestUtil
{

    /**
     * Searches request, session, page and application attributes for an object stored under the given name (name =
     * attribute key). If one is found it is returned. If scope is set to null, all four attribute sets will be
     * searched. If scope is not null only the attribute collection it represents will searched.
     * 
     * @param pageContext --
     * @param name --
     * @param scope null, or "request", "session", "page", "application" for specific attribute sets. "attribute" means
     *        servlet context attributes.
     * @return The first found object stored by a key matching the given name.
     */
    public static Object findObject(PageContext pageContext, String name, String scope)
    {
        Object o = null;
        if (scope == null)
        {
            o = pageContext.getRequest().getAttribute(name);
            if (o != null)
            {
                return o;
            }

            o = pageContext.getSession().getAttribute(name);
            if (o != null)
            {
                return o;
            }

            o = pageContext.getAttribute(name);
            if (o != null)
            {
                return o;
            }

            o = pageContext.getServletContext().getAttribute(name);
            if (o != null)
            {
                return o;
            }

            return null;
        }
        if ("request".equals(scope))
        {
            return pageContext.getRequest().getAttribute(name);
        }
        if ("session".equals(scope))
        {
            return pageContext.getSession().getAttribute(name);
        }
        if ("page".equals(scope))
        {
            return pageContext.getAttribute(name);
        }
        if ("application".equals(scope))
        {
            return pageContext.getServletContext().getAttribute(name);
        }

        return null;
    }

    /**
     * methode Find object : --.
     * 
     * @param pageContext --
     * @param name --
     * @param property --
     * @param scope --
     * @return object
     * @throws IllegalAccessException the illegal access exception
     * @throws InvocationTargetException the invocation target exception
     */
    public static Object findObject(PageContext pageContext, String name, String property, String scope)
        throws IllegalAccessException, InvocationTargetException
    {
        Object bean = findObject(pageContext, name, scope);
        if (property != null)
        {
            return ClassUtil.getBean(bean, property);
        }
        else
        {
            return bean;
        }
    }

    /**
     * methode Removes the object : --.
     * 
     * @param pageContext --
     * @param name --
     * @param scope --
     */
    public static void removeObject(PageContext pageContext, String name, String scope)
    {
        if (scope == null || "request".equals(scope))
        {
            pageContext.getRequest().removeAttribute(name);
            return;
        }
        if ("session".equals(scope))
        {
            pageContext.getSession().removeAttribute(name);
            return;
        }
        if ("page".equals(scope))
        {
            pageContext.removeAttribute(name);
            return;
        }
        if ("application".equals(scope))
        {
            pageContext.removeAttribute(name);
            return;
        }
    }

    /**
     * methode Store object : --.
     * 
     * @param pageContext --
     * @param name --
     * @param scope --
     * @param object --
     */
    public static void storeObject(PageContext pageContext, String name, String scope, Object object)
    {
        if (scope == null || "request".equals(scope))
        {
            pageContext.getRequest().setAttribute(name, object);
            return;
        }
        if ("session".equals(scope))
        {
            pageContext.getSession().setAttribute(name, object);
            return;
        }
        if ("page".equals(scope))
        {
            pageContext.setAttribute(name, object);
            return;
        }
        if ("application".equals(scope))
        {
            pageContext.getServletContext().setAttribute(name, object);
        }
    }

}
