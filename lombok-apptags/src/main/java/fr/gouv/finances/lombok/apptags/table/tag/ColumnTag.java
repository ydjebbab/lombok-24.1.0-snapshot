/*
/*Copyright (c) 2014 DGFiP - Tous droits réservés/* * Copyright 2004 original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package fr.gouv.finances.lombok.apptags.table.tag;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.BodyTagSupport;

import org.apache.commons.lang.StringUtils;

import fr.gouv.finances.lombok.apptags.table.bean.Column;
import fr.gouv.finances.lombok.apptags.table.core.TableModel;
import fr.gouv.finances.lombok.apptags.table.core.TableModelUtils;
import fr.gouv.finances.lombok.apptags.table.interceptor.ColumnInterceptor;

/**
 * Class ColumnTag.
 * 
 * jsp.tag : name="column" display-name="ColumnTag" body-content="JSP" description="Le conteneur qui maintient toutes les
 *          informations spécifiques à une colonne."
 */
public class ColumnTag extends BodyTagSupport implements ColumnInterceptor
{

    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** alias. */
    private String alias;

    /** calc. */
    private String calc;

    /** calc class. */
    private String calcClass;

    /** calc style. */
    private String calcStyle;

    /** calc title. */
    private String calcTitle;

    /** calc title class. */
    private String calcTitleClass;

    /** calc title style. */
    private String calcTitleStyle;

    /** calc display. */
    private String calcDisplay;

    /** calc export. */
    private String calcExport;

    /** cell. */
    private String cell;

    /** compact. */
    private String compact;

    /** filter options. */
    private Object filterOptions;

    /** filterable. */
    private String filterable;

    /** filter cell. */
    private String filterCell;

    /** filter class. */
    private String filterClass;

    /** filter style. */
    private String filterStyle;

    /** filter size. */
    private String filterSize;

    /** filter strict. */
    private String filterStrict;

    /** filter null option. */
    private String filterNullOption;

    /** responsive */
    private String showResponsive;

    /** filteredDroplist */
    private String filteredDroplist;

    /** escapeXml */
    private String escapeXml;

    /** dataAttributes */
    private String dataAttributes;

    /** format. */
    private String format;

    /** header cell. */
    private String headerCell;

    /** header class. */
    private String headerClass;

    /** header style. */
    private String headerStyle;

    /** interceptor. */
    private String interceptor;

    /** parse. */
    private String parse;

    /** property. */
    private String property;

    /** sortable. */
    private String sortable;

    /** style. */
    private String style;

    /** style class. */
    private String styleClass;

    /** title. */
    private String title;

    /** title pdf. */
    private String titlePdf;

    /** value. */
    private Object value;

    /** views allowed. */
    private String viewsAllowed;

    /** views denied. */
    private String viewsDenied;

    /** width. */
    private String width;

    /** escape auto format. */
    private String escapeAutoFormat;

    /** write or blank. */
    private String writeOrBlank;

    /**
     * Constructeur de la classe ColumnTag.java
     */
    public ColumnTag()
    {
        super();

    }

    /**
     * Modificateur de l attribut write or blank.
     * 
     * @param writeOrBlank le nouveau write or blank
     */
    public void setWriteOrBlank(String writeOrBlank)
    {
        this.writeOrBlank = writeOrBlank;
    }

    /**
     * Accesseur de l attribut calc display.
     * 
     * @return calc display
     */
    public String getCalcDisplay()
    {
        return calcDisplay;
    }

    /**
     * Modificateur de l attribut calc display.
     * 
     * @param calcDisplay le nouveau calc display
     */
    public void setCalcDisplay(String calcDisplay)
    {
        this.calcDisplay = calcDisplay;
    }

    /**
     * Accesseur de l attribut calc export.
     * 
     * @return calc export
     */
    public String getCalcExport()
    {
        return calcExport;
    }

    /**
     * Modificateur de l attribut calc export.
     * 
     * @param calcExport le nouveau calc export
     */
    public void setCalcExport(String calcExport)
    {
        this.calcExport = calcExport;
    }

    /**
     * Modificateur de l attribut calc class.
     * 
     * @param calcClass le nouveau calc class
     */
    public void setCalcClass(String calcClass)
    {
        this.calcClass = calcClass;
    }

    /**
     * Modificateur de l attribut calc style.
     * 
     * @param calcStyle le nouveau calc style
     */
    public void setCalcStyle(String calcStyle)
    {
        this.calcStyle = calcStyle;
    }

    /**
     * Modificateur de l attribut calc title class.
     * 
     * @param calcTitleClass le nouveau calc title class
     */
    public void setCalcTitleClass(String calcTitleClass)
    {
        this.calcTitleClass = calcTitleClass;
    }

    /**
     * Modificateur de l attribut calc title style.
     * 
     * @param calcTitleStyle le nouveau calc title style
     */
    public void setCalcTitleStyle(String calcTitleStyle)
    {
        this.calcTitleStyle = calcTitleStyle;
    }

    /**
     * Modificateur de l attribut alias.
     * 
     * @param alias le nouveau alias
     * jsp.attribute :  description="Utilisé pour identifier de façon unique une colonne quand la même propriété est
     *                utilisée par plusieurs colonnes du même tableau. Doit aussi être positionnée quand la propriété de
     *                la colonne utilise la notation pointée, pour éviter une erreur dans le javascript généré
     *                required="false" rtexprvalue="true"
     */
    public void setAlias(String alias)
    {
        this.alias = alias;
    }

    /**
     * Modificateur de l attribut calc.
     * 
     * @param calc le nouveau calc
     * jsp.attribute :  description="Le nom complet d'une classe qui implémente l'interface Calc. Peut aussi contenir un
     *                alias définit dans le fichier de préférences. Utilisé pour effectuer des calculs les valeurs de la
     *                colonne. Une colonne peut comporter plusieurs cellules de calcul, il faut alors séparer le nom des
     *                classes ou des alias par une virgule required="false" rtexprvalue="true"
     */
    public void setCalc(String calc)
    {
        this.calc = calc;
    }

    /**
     * Modificateur de l attribut calc title.
     * 
     * @param totalTitle le nouveau calc title
     * jsp.attribute :  description="Le ou les titres des cellules de calcul. Les différents titres doivent être séparés
     *                par des virgules." required="false" rtexprvalue="true"
     */
    public void setCalcTitle(String totalTitle)
    {
        this.calcTitle = totalTitle;
    }

    /**
     * Modificateur de l attribut cell.
     * 
     * @param cell le nouveau cell
     * jsp.attribute :  description="
     *                <p>
     *                Définit le format d'affichage des cellules d'une colonne. Les types de cellules disponibles par
     *                défaut sont les suivants :
     *                </p>
     *                <p>
     *                <b>Pour les chaînes de caractères</b> : cell="display". C'est la valeur par défaut.
     *                </p>
     *                <p>
     *                <b> Pour les objets de type Number : </b>
     *                <ul>
     *                <li>Formatage d'un nombre (nombrecellzerodecimale,nombrecellunedecimale, . .
     *                .,nombrecelldixdecimales)</li>
     *                <li>Formatage de monnaies (monnaiecellzerodecimale,monnaiecellunedecimale, . . . ,
     *                monnaiecelldixdecimales)</li>
     *                <li>Formatage de pourcentages (pourcentcellzerodecimale,pourcentcellunedecimale,. . .
     *                ,pourcentcelldixdecimales)</li>
     *                </ul>
     *                </p>
     *                <p>
     *                <b>Pour les objets de type Date : </b>
     *                <ul>
     *                <li>date (ex : 01/10/2006)</li>
     *                <li>datelong (ex : dimanche 1 octobre 2006)</li>
     *                <li>dateabrege (ex : dim. 1 oct. 2006)</li>
     *                <li>dateheure (ex : 01/10/2006 15 )</li>
     *                <li>dateheureminute (ex : 01/10/2006 15:12)</li>
     *                <li>dateheureminuteseconde (ex : 01/10/2006 15:12:45)</li>
     *                <li>heure (ex : 15)</li>
     *                <li>heureminute (ex : 15:12)</li>
     *                <li>heureminuteseconde (ex : 15:12:45)</li>
     *                </ul>
     *                </p>
     *                <p>
     *                <b>Pour les objets de type Boolean</b>
     *                <ul>
     *                <li>booleanvraifaux (true -> VRAI, false -> FAUX)</li>
     *                <li>booleanvf (true -> V, false -> F)</li>
     *                <li>booleanouinon (true -> OUI,false -> NON)</li>
     *                <li>booleanon (true -> O,false -> N)</li>
     *                <li>boolean10 (true -> 1,false -> 0)</li>
     *                </ul>
     *                </p>
     *                <p>
     *                <b>Pour obtenir le numéro de la ligne :</b> rowCount
     *                </p>
     *                <p>
     *                La valeur de l'attribut 'cell' peut aussi être le nom complet d'une classe qui implémente
     *                l'interface Cell ou AbstractCell
     *                </p>
     */
    public void setCell(String cell)
    {
        this.cell = cell;
    }

    /**
     * Modificateur de l attribut compact.
     * 
     * @param compact le nouveau compact
     * jsp.attribute :  description="Définit si la table est compacte. Par défaut : false." required="false"
     *                rtexprvalue="true"
     */
    public void setCompact(String compact)
    {
        this.compact = compact;
    }

    /**
     * Modificateur de l attribut escape auto format.
     * 
     * @param escapeAutoFormat le nouveau escape auto format
     * jsp.attribute :  description="Indique si le formattage automatique de la cellule est actif ou non. Par défaut
     *                'false'. Effectif uniquement si l'autoformatting' est implémenté dans la vue. required="false"
     *                rtexprvalue="true"
     */
    public void setEscapeAutoFormat(String escapeAutoFormat)
    {
        this.escapeAutoFormat = escapeAutoFormat;
    }

    /**
     * Modificateur de l attribut filterable.
     * 
     * @param filterable le nouveau filterable
     * jsp.attribute :  description="Indique si la colonne est filtrable ou non Les valeurs possibles sont <b>true</b> et
     *                <b>false</b>. La valeur par défaut est la valeur de l'attribut 'filterable' du tag 'table'"
     *                required="false" rtexprvalue="true"
     */
    public void setFilterable(String filterable)
    {
        this.filterable = filterable;
    }

    /**
     * Modificateur de l attribut filter cell.
     * 
     * @param filterCell le nouveau filter cell
     * jsp.attribute :  description="Indique le type du filtre utilisé le champ de filtre de la colonne. Les valeurs
     *                possibles sont 'filter' (champ de saisie),'droplist' (liste déroulante). La valeur par défaut est
     *                'filter'. La valeur de cet attribut peut aussi être le nom complet d'une classe qui implémente
     *                l'interface 'cell' required="false" rtexprvalue="true"
     */
    public void setFilterCell(String filterCell)
    {
        this.filterCell = filterCell;
    }

    /**
     * Modificateur de l attribut filter class.
     * 
     * @param filterClass le nouveau filter class
     * jsp.attribute :  description="La classe css utilisée pour définir la présentation de la colonne qui contient le
     *                filtre." required="false" rtexprvalue="true"
     */
    public void setFilterClass(String filterClass)
    {
        this.filterClass = filterClass;
    }

    /**
     * Modificateur de l attribut filter options.
     * 
     * @param filterOptions le nouveau filter options
     * jsp.attribute :  description="L'objet qui contient la collection d'éléments qui implémentent l'interface 'Option'"
     *                required="false" rtexprvalue="true"
     */
    public void setFilterOptions(Object filterOptions)
    {
        this.filterOptions = filterOptions;
    }

    /**
     * Modificateur de l attribut filter style.
     * 
     * @param filterStyle le nouveau filter style
     * jsp.attribute :  description="Le style css utilisé pour définir la présentation de la colonne qui contient le
     *                filtre." required="false" rtexprvalue="true"
     */
    public void setFilterStyle(String filterStyle)
    {
        this.filterStyle = filterStyle;
    }

    /**
     * Modificateur de l attribut filter strict.
     * 
     * @param filterStrict le nouveau filter strict
     * jsp.attribute :  description="Indique si le filtrage de la colonne est strict (égalité des valeurs) ou non Les
     *                valeurs possibles sont <b>true</b> et <b>false</b>." required="false" rtexprvalue="true"
     */
    public void setFilterStrict(String filterStrict)
    {
        this.filterStrict = filterStrict;
    }

    /**
     * Modificateur de l attribut filter null option.
     * 
     * @param filterNullOption le nouveau filter null option
     * jsp.attribute :  description="Indique quel est le libellé à afficher pour représenter la valeur nulle lors de la
     *                sélection d'un filtre via une liste déroulante." required="false" rtexprvalue="true"
     */
    public void setFilterNullOption(String filterNullOption)
    {
        this.filterNullOption = filterNullOption;
    }

    /**
     * Modificateur de l attribut format.
     * 
     * @param format le nouveau format
     * jsp.attribute :  description="Format utilisé pour présenter le contenu d'une cellule. required="false"
     *                rtexprvalue="true"
     * @deprecated Préférer l'utilisation d'un type de cellule qui porte elle-même le type de formatage à effectuer au
     *             contenu de la cellule. Ce qui permet une meilleure cohérence entre le format du tableau HTML et celui
     *             du tableau lorsqu'il est exporté en PDF et en EXCEL par exemple.
     */
    @Deprecated
    public void setFormat(String format)
    {
        this.format = format;
    }

    /**
     * Modificateur de l attribut header cell.
     * 
     * @param headerCell le nouveau header cell
     * jsp.attribute :  description="Définit la cellule utilisée pour afficher le contenu les cellules des titres de
     *                colonnes. La valeur par défaut est 'header'. Cet attribut peut aussi prendre le nom complet d'une
     *                classe qui implémente l'interface 'cell'." required="false" rtexprvalue="true"
     */
    public void setHeaderCell(String headerCell)
    {
        this.headerCell = headerCell;
    }

    /**
     * Modificateur de l attribut header class.
     * 
     * @param headerClass le nouveau header class
     * jsp.attribute :  description="The css class style sheet used to define what the table header column looks like."
     *                required="false" rtexprvalue="true"
     */

    /**
     * jsp.attribute :  description="La classe css utilisée pour définit la présentation des entêtes de colonne."
     *                required="false" rtexprvalue="true"
     * @param headerClass
     */
    public void setHeaderClass(String headerClass)
    {
        this.headerClass = headerClass;
    }

    /**
     * Modificateur de l attribut header style.
     * 
     * @param headerStyle le nouveau header style
     * jsp.attribute :  description="Le style css utilisée pour définit la présentation des entêtes de colonne."
     *                required="false" rtexprvalue="true"
     */
    public void setHeaderStyle(String headerStyle)
    {
        this.headerStyle = headerStyle;
    }

    /**
     * Modificateur de l attribut interceptor.
     * 
     * @param interceptor le nouveau interceptor
     * jsp.attribute :  description="Le nom complet d'une classe qui implémente l'interface InterceptColumn Peut aussi
     *                contenir un alias définit dans le fichier extremecomponents.properties Utiliser pour modifier ou
     *                ajouter des attributs à une colonne." required="false" rtexprvalue="true"
     */
    public void setInterceptor(String interceptor)
    {
        this.interceptor = interceptor;
    }

    /**
     * Modificateur de l attribut parses the.
     * 
     * @param parse le nouveau parses the
     * jsp.attribute :  description="Utilisé si le format doit être interprêté. Par exemple si une date doit être lue à
     *                partir d'un format particulier, comme MM-dd-yyyy" required="false" rtexprvalue="true"
     * @deprecated Préférer l'utilisation du type de cellule adapté
     */
    public void setParse(String parse)
    {
        this.parse = parse;
    }

    /**
     * Modificateur de l attribut property.
     * 
     * @param property le nouveau property
     * jsp.attribute :  description="La propriété du bean utilisée par la colonne." required="false" rtexprvalue="true"
     */
    public void setProperty(String property)
    {
        this.property = property;
    }

    /**
     * Modificateur de l attribut sortable.
     * 
     * @param sortable le nouveau sortable
     * jsp.attribute :  description="Indique si la colonne doit ou non être triable. Les valeurs possibles sont 'true' et
     *                'false'. Par défaut la colonne utilise la valeur de l'attribut 'sortable' défini au niveau de
     *                l'élément 'table'. required="false" rtexprvalue="true"
     */
    public void setSortable(String sortable)
    {
        this.sortable = sortable;
    }

    /**
     * Modificateur de l attribut style.
     * 
     * @param style le nouveau style
     * jsp.attribute :  description="Valeur de l'attribut 'style' appliqué à la cellule du tableau (td)." required="false"
     *                rtexprvalue="true"
     */
    public void setStyle(String style)
    {
        this.style = style;
    }

    /**
     * Modificateur de l attribut style class.
     * 
     * @param styleClass le nouveau style class
     * jsp.attribute :  description="Valeur de l'attribut 'class' appliqué à la cellule du tableau (td)." required="false"
     *                rtexprvalue="true"
     */
    public void setStyleClass(String styleClass)
    {
        this.styleClass = styleClass;
    }

    /**
     * Modificateur de l attribut title.
     * 
     * @param title le nouveau title
     * jsp.attribute :  description="Titre de la colonne. Si l'attribut 'title' n'est pas spécifié la valeur de l'attribut
     *                'property' sera utilisé, en utilisant la syntaxe 'camelcase' pour séparer les mots."
     *                required="false" rtexprvalue="true"
     */
    public void setTitle(String title)
    {
        this.title = title;
    }

    /**
     * Modificateur de l attribut value.
     * 
     * @param value le nouveau value
     * jsp.attribute :  description="Valeur de la cellule. Si l'attribut 'value' n'est pas spécifié, la valeur de la
     *                colonne sera obtenue en utilisant l'attribut 'property'. La valeur de la cellule peut aussi être
     *                définie dans le corps de l'élément 'column'." required="false" rtexprvalue="true"
     */
    public void setValue(Object value)
    {
        this.value = value;
    }

    /**
     * Modificateur de l attribut views allowed.
     * 
     * @param viewsAllowed le nouveau views allowed
     * jsp.attribute :  description="Liste des vues, séparées par une virgule, dans laquelle la colonne sera utilisée."
     *                required="false" rtexprvalue="true"
     */
    public void setViewsAllowed(String viewsAllowed)
    {
        this.viewsAllowed = viewsAllowed;
    }

    /**
     * Modificateur de l attribut views denied.
     * 
     * @param viewsDenied le nouveau views denied
     * jsp.attribute :  description="Liste des vues, séparées par une virgule, dans laquelle la colonne ne sera pas
     *                utilisée." required="false" rtexprvalue="true"
     */
    public void setViewsDenied(String viewsDenied)
    {
        this.viewsDenied = viewsDenied;
    }

    /**
     * Modificateur de l attribut width.
     * 
     * @param width le nouveau width
     * jsp.attribute :  description="Indique la largeur de la colonne." required="false" rtexprvalue="true"
     */
    public void setWidth(String width)
    {
        this.width = width;
    }

    /**
     * Récupère la valeur de la colonne. On regarde tout d'abord si le contenu à afficher est placé dans le corps de
     * l'élément 'column'. Si ce n'est pas le cas, on utilise l'attribut 'value'. Si celui-ci n'est pas spécifié, c'est
     * l'attribut 'property' qui sert pour lire la valeur dans le bean asscoié à la ligne du tableau.
     * 
     * @param propertyValue
     * @return column value
     * @throws JspException the jsp exception
     */
    protected Object getColumnValue(Object propertyValue) throws JspException
    {
        Object result = value;

        if (result == null && bodyContent != null)
        {
            result = getBodyContent().getString();
        }

        if (result != null)
        {
            result =
                EcExpressionEvaluatorManager.evaluate("result", result.toString(), Object.class, this, pageContext);
        }

        if (result == null || (result != null && result instanceof String && StringUtils.isBlank(result.toString())))
        {
            result = propertyValue;
        }

        return result;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see javax.servlet.jsp.tagext.BodyTagSupport#doStartTag()
     */
    @Override
    public int doStartTag() throws JspException
    {
        if (!TagUtils.isIteratingBody(this))
        {
            return SKIP_BODY;
        }

        return EVAL_BODY_BUFFERED;
    }

    /**
     * On efectue une copie de l'objet Column car le tag peut être réutilisé. La copie est injectée dans le modèle
     * 
     * @return int
     * @throws JspException the jsp exception
     */
    @Override
    public int doEndTag() throws JspException
    {
        try
        {
            TableModel model = TagUtils.getModel(this);

            if (TagUtils.isIteratingBody(this))
            {
                String alias = TagUtils.evaluateExpressionAsString("alias", this.alias, this, pageContext);
                String property = TagUtils.evaluateExpressionAsString("property", this.property, this, pageContext);
                Column column = model.getColumnHandler().getColumnByAlias(TableModelUtils.getAlias(alias, property));
                if (column != null)
                { // null if view not allowed
                    Object bean = TagUtils.getModel(this).getCurrentRowBean();
                    Object propertyValue = TableModelUtils.getColumnPropertyValue(bean, property);
                    column.setValue(getColumnValue(propertyValue));
                    column.setPropertyValue(propertyValue);

                    modifyColumnAttributes(model, column);
                    model.getColumnHandler().modifyColumnAttributes(column);
                    model.getViewHandler().addColumnValueToView(column);
                }
            }
            else
            {
                Column column = new Column(model);

                column
                    .setCalcClass(TagUtils.evaluateExpressionAsString("calcClass", this.calcClass, this, pageContext));
                column.setCalcTitleClass(TagUtils.evaluateExpressionAsString("calcTitleClass", this.calcTitleClass,
                    this, pageContext));
                column
                    .setCalcStyle(TagUtils.evaluateExpressionAsString("calcStyle", this.calcStyle, this, pageContext));
                column.setCalcTitleStyle(TagUtils.evaluateExpressionAsString("calcTitleStyle", this.calcTitleStyle,
                    this, pageContext));
                column.setCalcDisplay(TagUtils
                    .evaluateExpressionAsString("calcDisplay", calcDisplay, this, pageContext));
                column.setCalcExport(TagUtils.evaluateExpressionAsString("calcExport", calcExport, this, pageContext));
                column.setCalc(TagUtils.evaluateExpressionAsString("calc", this.calc, this, pageContext));
                column.setCalcTitle(TagUtils.evaluateExpressionAsString("calcTitle", calcTitle, this, pageContext));

                column.completeCalcInfo();

                column.setAlias(TagUtils.evaluateExpressionAsString("alias", this.alias, this, pageContext));
                column.setCell(TagUtils.evaluateExpressionAsString("cell", this.cell, this, pageContext));
                column.setEscapeAutoFormat(TagUtils.evaluateExpressionAsBoolean("escapeAutoFormat",
                    this.escapeAutoFormat, this, pageContext));
                column.setCompact(TagUtils.evaluateExpressionAsBoolean("compact", this.compact, this,
                    pageContext));
                column.setFilterable(TagUtils.evaluateExpressionAsBoolean("filterable", this.filterable, this,
                    pageContext));
                column.setFilterCell(TagUtils.evaluateExpressionAsString("filterCell", this.filterCell, this,
                    pageContext));
                column.setFilterClass(TagUtils.evaluateExpressionAsString("filterClass", this.filterClass, this,
                    pageContext));
                column.setFilterOptions(TagUtils.evaluateExpressionAsObject("filterOptions", this.filterOptions, this,
                    pageContext));
                column.setFilterStyle(TagUtils.evaluateExpressionAsString("filterStyle", this.filterStyle, this,
                    pageContext));
                column.setFilterSize(TagUtils.evaluateExpressionAsString("filterSize", this.filterSize, this,
                    pageContext));
                column.setFilterStrict(TagUtils.evaluateExpressionAsBoolean("filterStrict", this.filterStrict, this,
                    pageContext));
                column.setFilterNullOption(TagUtils.evaluateExpressionAsString("filterNullOption", this.filterNullOption, this,
                    pageContext));
                column.setFormat(TagUtils.evaluateExpressionAsString("format", this.format, this, pageContext));
                column.setHeaderCell(TagUtils.evaluateExpressionAsString("headerCell", this.headerCell, this,
                    pageContext));
                column.setHeaderClass(TagUtils.evaluateExpressionAsString("headerClass", this.headerClass, this,
                    pageContext));
                column.setHeaderStyle(TagUtils.evaluateExpressionAsString("headerStyle", this.headerStyle, this,
                    pageContext));
                column.setInterceptor(TagUtils.evaluateExpressionAsString("interceptor", this.interceptor, this,
                    pageContext));
                column.setParse(TagUtils.evaluateExpressionAsString("parse", this.parse, this, pageContext));
                column.setProperty(TagUtils.evaluateExpressionAsString("property", this.property, this, pageContext));
                column.setSortable(TagUtils.evaluateExpressionAsBoolean("sortable", this.sortable, this, pageContext));
                column.setStyle(TagUtils.evaluateExpressionAsString("style", this.style, this, pageContext));
                column.setStyleClass(TagUtils.evaluateExpressionAsString("styleClass", this.styleClass, this,
                    pageContext));
                column.setTitle(TagUtils.evaluateExpressionAsString("title", this.title, this, pageContext));
                column.setTitlePdf(TagUtils.evaluateExpressionAsString("titlePdf", this.titlePdf, this, pageContext));
                column.setViewsAllowed(TagUtils.evaluateExpressionAsString("viewsToAllow", this.viewsAllowed, this,
                    pageContext));
                column.setViewsDenied(TagUtils.evaluateExpressionAsString("viewsToDeny", this.viewsDenied, this,
                    pageContext));
                column.setWidth(TagUtils.evaluateExpressionAsString("width", this.width, this, pageContext));
                column.setWriteOrBlank(TagUtils.evaluateExpressionAsBoolean("writeIfEqualsLast", this.writeOrBlank,
                    this, pageContext));

                column.setShowResponsive(TagUtils.evaluateExpressionAsBoolean("showResponsive", this.showResponsive, this, pageContext));
                column.setDataAttributes(TagUtils.evaluateExpressionAsString("dataAttributes", this.dataAttributes, this, pageContext));
                column.setFilteredDroplist(
                    TagUtils.evaluateExpressionAsBoolean("filteredDroplist", this.filteredDroplist, this, pageContext));
                column.setEscapeXml(TagUtils.evaluateExpressionAsBoolean("escapeXml", this.escapeXml, this, pageContext));

                addColumnAttributes(model, column);
                model.getColumnHandler().addColumn(column);
            }

            if (bodyContent != null)
            {
                bodyContent.clearBody();
            }
        }
        catch (Exception exception)
        {
            throw new JspException("Erreur : ", exception);
        }

        return EVAL_PAGE;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.interceptor.ColumnInterceptor#addColumnAttributes(fr.gouv.finances.lombok.apptags.table.core.TableModel,
     *      fr.gouv.finances.lombok.apptags.table.bean.Column)
     */
    @Override
    public void addColumnAttributes(TableModel model, Column column)
    {
        // RAS
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.interceptor.ColumnInterceptor#modifyColumnAttributes(fr.gouv.finances.lombok.apptags.table.core.TableModel,
     *      fr.gouv.finances.lombok.apptags.table.bean.Column)
     */
    @Override
    public void modifyColumnAttributes(TableModel model, Column column)
    {
        // RAS
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see javax.servlet.jsp.tagext.BodyTagSupport#release()
     */
    @Override
    public void release()
    {
        alias = null;
        calc = null;
        calcTitle = null;
        calcStyle = null;
        calcClass = null;
        calcTitleStyle = null;
        calcTitleClass = null;
        cell = null;
        compact = null;
        escapeAutoFormat = null;
        filterable = null;
        filterCell = null;
        filterClass = null;
        filterStyle = null;
        filterSize = null;
        filterStrict = null;
        filterNullOption = null;
        showResponsive = null;
        dataAttributes = null;
        format = null;
        headerCell = null;
        headerClass = null;
        headerStyle = null;
        interceptor = null;
        parse = null;
        property = null;
        sortable = null;
        style = null;
        styleClass = null;
        title = null;
        titlePdf = null;
        value = null;
        viewsAllowed = null;
        viewsDenied = null;
        width = null;
        writeOrBlank = null;
        super.release();
    }

    /**
     * Accesseur de l attribut title pdf.
     * 
     * @return title pdf
     */
    public String getTitlePdf()
    {
        return titlePdf;
    }

    /**
     * Modificateur de l attribut title pdf.
     * 
     * @param titlePdf le nouveau title pdf
     */
    public void setTitlePdf(String titlePdf)
    {
        this.titlePdf = titlePdf;
    }

    /**
     * Modificateur de l attribut filter size.
     * 
     * @param filterSize le nouveau filter size
     */
    public void setFilterSize(String filterSize)
    {
        this.filterSize = filterSize;
    }

    /**
     * Accesseur de l attribut responsive.
     * 
     * @return show responsive
     */
    public String getShowResponsive()
    {
        return showResponsive;
    }

    /**
     * Modificateur de l attribut responsive.
     * 
     * @param showResponsive nouveau responsive
     */
    public void setShowResponsive(String showResponsive)
    {

        this.showResponsive = showResponsive;
    }

    /**
     * Accesseur de l attribut dataAttributes.
     * 
     * @return show dataAttributes
     */
    public String getDataAttributes()
    {
        return dataAttributes;
    }

    /**
     * Modificateur de l attribut dataAttributes.
     * 
     * @param dataAttributes le nouveau dataAttributes
     */
    public void setDataAttributes(String dataAttributes)
    {
        this.dataAttributes = dataAttributes;
    }

    public String getFilteredDroplist()
    {
        return filteredDroplist;
    }

    public void setFilteredDroplist(String filteredDroplist)
    {
        this.filteredDroplist = filteredDroplist;
    }

    public String getEscapeXml()
    {
        return escapeXml;
    }

    public void setEscapeXml(String escapeXml)
    {
        this.escapeXml = escapeXml;
    }

}