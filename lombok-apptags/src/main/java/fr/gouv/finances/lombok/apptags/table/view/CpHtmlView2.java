/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.apptags.table.view;

import org.apache.commons.lang.StringUtils;

import fr.gouv.finances.lombok.apptags.table.cell.SelectedCheckboxCellAttributes;
import fr.gouv.finances.lombok.apptags.table.core.TableModel;
import fr.gouv.finances.lombok.apptags.table.view.html.BuilderUtils;
import fr.gouv.finances.lombok.apptags.table.view.html.CpCalcBuilder;
import fr.gouv.finances.lombok.apptags.table.view.html.RowBuilder;
import fr.gouv.finances.lombok.apptags.table.view.html.TableBuilder;
import fr.gouv.finances.lombok.apptags.table.view.html.TwoColumnTableLayout;
import fr.gouv.finances.lombok.apptags.util.HtmlBuilder;

/**
 * Class CpHtmlView2
 */
public class CpHtmlView2 extends HtmlView
{

    /**
     * Instanciation de cp html view2.
     */
    public CpHtmlView2()
    {
        super();
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.view.HtmlView#afterBodyInternal(fr.gouv.finances.lombok.apptags.table.core.TableModel)
     */
    @Override
    protected void afterBodyInternal(TableModel model)
    {
        getCpCalcBuilder().defaultCalcLayout();

        getTableBuilder().tbodyEnd();

        getTableBuilder().tableEnd();

        boolean showPaginationBottom = BuilderUtils.showPaginationBottom(model);
        boolean showExportsBottom = BuilderUtils.showExportsBottom(model);

        if (showPaginationBottom || showExportsBottom)
        {
            BuilderUtils.setInPaginationBottom(model);
            toolbar(getHtmlBuilder(), getTableModel());
        }
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.view.AbstractHtmlView#init(fr.gouv.finances.lombok.apptags.util.HtmlBuilder,
     *      fr.gouv.finances.lombok.apptags.table.core.TableModel)
     */
    @Override
    protected void init(HtmlBuilder html, TableModel model)
    {
        setTableBuilder(new TableBuilder(html, model));
        setRowBuilder(new RowBuilder(html, model));
        setCpCalcBuilder(new CpCalcBuilder(html, model));
    }

    /**
     * methode Toolbar :
     * 
     * @param model
     */
    protected void toolbar(final TableModel model)
    {
        final TwoColumnTableLayout toolbar = new CpDefaultToolbar(getHtmlBuilder(), model);
        toolbar.layout();
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.view.HtmlView#beforeBodyInternal(fr.gouv.finances.lombok.apptags.table.core.TableModel)
     */
    @Override
    protected void beforeBodyInternal(final TableModel model)
    {

        boolean showPagination = BuilderUtils.showPagination(model);
        boolean showExports = BuilderUtils.showExports(model);

        if (showPagination || showExports)
        {
            toolbar(getHtmlBuilder(), getTableModel());
        }

        CpHtmlView2.formJavascript(getHtmlBuilder(), model);                
       
        getTableBuilder().statusRow();
        
        if (model.getTableHandler().getTable().isFilterable())
        {
            getTableBuilder().filterRow();
        }
        
        getTableBuilder().headerRow();

        getTableBuilder().tbodyStart();

    }

    protected void beforeBodyInternalResponsive(final TableModel model)
    {
        boolean showPagination = BuilderUtils.showPagination(model);
        boolean showExports = BuilderUtils.showExports(model);

        if (showPagination || showExports)
        {
            toolbar(getHtmlBuilder(), getTableModel());
        }

        CpHtmlView2.formJavascript(getHtmlBuilder(), model);

        getTableBuilder().tableStart(false);

        getTableBuilder().headerRow();            
        getTableBuilder().filterRow();
        statusBar(getHtmlBuilder(), getTableModel());       

        getTableBuilder().tbodyStart();

    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.view.HtmlView#statusBar(fr.gouv.finances.lombok.apptags.util.HtmlBuilder,
     *      fr.gouv.finances.lombok.apptags.table.core.TableModel)
     */
    @Override
    protected void statusBar(HtmlBuilder html, TableModel model)
    {
        new CpStatusBar(html, model).layout();
    }

    /**
     * methode Form javascript :
     * 
     * @param html
     * @param model
     */
    public static void formJavascript(final HtmlBuilder html, final TableModel model)
    {

        final String tableId = model.getTableHandler().getTable().getTableId();
        final String form = model.getTableHandler().getTable().getForm();

        if (tableId != null && StringUtils.isNotBlank(tableId))
        {

            final SelectedCheckboxCellAttributes checkboxCellAttr = SelectedCheckboxCellAttributes.getDefault(tableId);

            // Javascript pour la gestion des checkboxes
            final StringBuilder javascript = new StringBuilder();

            javascript.append("<script  type=\"text/javascript\">/*<![CDATA[*/\n ");
            javascript.append("function ");
            javascript.append(checkboxCellAttr.getOnClickJavascript());
            javascript.append(tableId);
            javascript.append("(chkbx)  { ");
            javascript.append("if (chkbx == null)");
            javascript.append('{');
            javascript.append("return;");
            javascript.append('}');
            javascript.append("var hiddenField;");
            javascript.append("hiddenField = document.getElementById('");
            javascript.append(checkboxCellAttr.getPrefixIdHidden());
            javascript.append("' + chkbx.name);");
            javascript.append("if (chkbx.checked) {");
            javascript.append("if (typeof(hiddenField) != 'undefined')");
            javascript.append('{');
            javascript.append("hiddenField.value='SELECTED';");
            javascript.append('}');
            javascript.append('}');
            javascript.append("else {");
            javascript.append("if (typeof(hiddenField) != 'undefined')");
            javascript.append('{');
            javascript.append("hiddenField.value='UNSELECTED';");
            javascript.append('}');
            javascript.append('}');
            javascript.append("}\n");

            // Javascript permettant de sélectionner /déselectionner
            // toutes les cases à cocher d'une page
            javascript.append("function ");
            javascript.append(checkboxCellAttr.getOnClickSelectAll());
            javascript.append(tableId);
            javascript.append("(selector)");
            javascript.append('{');
            javascript.append("leFormulaireActif = document.forms." + form + ";");
            javascript.append("if (leFormulaireActif != null)");
            javascript.append(" {");
            javascript.append("lesElementsDuFormulaire = leFormulaireActif.elements;");
            javascript.append("for (var i = 0; i < lesElementsDuFormulaire.length; ++i)");
            javascript.append('{');
            javascript.append("var tagName = lesElementsDuFormulaire[i].tagName;");
            javascript.append("if (tagName == 'INPUT' || tagName == 'input')");
            javascript.append('{');
            javascript.append("var type = lesElementsDuFormulaire[i].type;");
            javascript.append("if (type == 'hidden' || type == 'checkbox')");
            javascript.append('{');
            javascript.append("var id = lesElementsDuFormulaire[i].id;");
            javascript.append("if (id.search('");
            javascript.append(checkboxCellAttr.getPrefixIdHidden());
            javascript.append("') != -1)");
            javascript.append('{');
            // mise à jour du champ caché
            javascript.append("if (selector.checked == false)");
            javascript.append('{');
            javascript.append("lesElementsDuFormulaire[i].value = '");
            javascript.append(checkboxCellAttr.getPatternUnselected());
            javascript.append("';");
            javascript.append('}');
            javascript.append("if (selector.checked == true)");
            javascript.append('{');
            javascript.append("lesElementsDuFormulaire[i].value = '");
            javascript.append(checkboxCellAttr.getPatternSelected());
            javascript.append("';");
            javascript.append('}');
            javascript.append('}');
            javascript.append("if (id.search('");
            javascript.append(checkboxCellAttr.getPrefixIdCheckbox());
            javascript.append("') != -1)");
            javascript.append('{');
            // mise à jour de la checkbox
            javascript.append("lesElementsDuFormulaire[i].checked = selector.checked;");
            javascript.append('}');
            javascript.append('}');
            javascript.append('}');
            javascript.append('}');
            javascript.append('}');
            javascript.append('}');

            javascript.append('\n');
            javascript.append("/*]]>*/");
            javascript.append("</script>");
            html.append(javascript.toString());
        }
    }
}
