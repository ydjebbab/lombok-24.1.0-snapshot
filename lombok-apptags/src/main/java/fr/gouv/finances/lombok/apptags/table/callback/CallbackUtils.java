/*
/*Copyright (c) 2014 DGFiP - Tous droits réservés/* * Copyright 2004 original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package fr.gouv.finances.lombok.apptags.table.callback;

import java.util.Collection;

import fr.gouv.finances.lombok.apptags.table.bean.Table;
import fr.gouv.finances.lombok.apptags.table.core.RetrievalUtils;
import fr.gouv.finances.lombok.apptags.table.core.TableModel;

/**
 * Deal with callback specific utils.
 * 
 * @deprecated Use RetrievalUtils now.
 */
@Deprecated
public class CallbackUtils
{

    /**
     * Instanciation de callback utils.
     */
    private CallbackUtils()
    {
    }

    /**
     * Accesseur de l attribut items.
     * 
     * @param model
     * @param table
     * @return items
     * @throws Exception the exception
     * @deprecated Use RetrievalUtils.retrieveCollection now.
     */
    @SuppressWarnings("unchecked")
    @Deprecated
    public static Collection getItems(TableModel model, Table table) throws Exception
    {
        String items = String.valueOf(table.getItems());
        return RetrievalUtils.retrieveCollection(model.getContext(), items, table.getScope());
    }
}
