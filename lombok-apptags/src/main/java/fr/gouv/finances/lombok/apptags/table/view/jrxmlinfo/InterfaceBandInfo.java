/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.apptags.table.view.jrxmlinfo;

/**
 * Interface InterfaceBandInfo
 */
public interface InterfaceBandInfo
{

    /**
     * Accesseur de l attribut height.
     * 
     * @return height
     */
    public int getHeight();

    /**
     * Modificateur de l attribut height.
     * 
     * @param height le nouveau height
     */
    public void setHeight(int height);

    /**
     * Accesseur de l attribut style.
     * 
     * @return style
     */
    public FontStyleInfo getStyle();

    /**
     * Modificateur de l attribut style.
     * 
     * @param style le nouveau style
     */
    public void setStyle(FontStyleInfo style);

}
