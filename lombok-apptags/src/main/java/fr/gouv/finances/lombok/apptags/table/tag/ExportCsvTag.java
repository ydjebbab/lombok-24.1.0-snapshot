/*
/*Copyright (c) 2014 DGFiP - Tous droits réservés/* * Copyright 2004 original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package fr.gouv.finances.lombok.apptags.table.tag;

import org.apache.commons.lang.StringUtils;

import fr.gouv.finances.lombok.apptags.table.bean.Export;
import fr.gouv.finances.lombok.apptags.table.core.TableConstants;
import fr.gouv.finances.lombok.apptags.table.core.TableModel;
import fr.gouv.finances.lombok.apptags.table.view.CsvProtegeView;
import fr.gouv.finances.lombok.apptags.table.view.html.BuilderConstants;

/**
 * Class ExportCsvTag
 * 
 * jsp.tag : name="exportCsv" display-name="ExportCsvTag" body-content="JSP"
 *          description="Exporte les données au format csv."
 */
public class ExportCsvTag extends ExportTag
{

    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** delimiter. */
    private String delimiter;

    /** endsWithDelimiterdelimiter. */
    private String endsWithDelimiter;
    
    /** protection. */
    private String protection;

    /** include header. */
    private String includeHeader;

    /** colonne protection class. */
    private String colonneProtectionClass;

    /** includeTitle - String, inclusion d'un titre. */
    private String includeTitle;

    /**
     * Constructeur de la classe ExportCsvTag.java
     *
     */
    public ExportCsvTag()
    {
        super();
        
    }

    /**
     * Accesseur de l attribut colonne protection class.
     * 
     * @return colonne protection class
     */
    public String getColonneProtectionClass()
    {
        return TagUtils.evaluateExpressionAsString("colonneProtectionClass", colonneProtectionClass, this, pageContext);
    }

    /**
     * Modificateur de l attribut colonne protection class.
     * 
     * @param colonneProtectionClass le nouveau colonne protection class
     */
    public void setColonneProtectionClass(String colonneProtectionClass)
    {
        this.colonneProtectionClass = colonneProtectionClass;
    }

    /**
     * Accesseur de l attribut include header.
     * 
     * @return include header
     */
    public String getIncludeHeader()
    {
        return TagUtils.evaluateExpressionAsString("includeHeader", includeHeader, this, pageContext);
    }

    /**
     * Modificateur de l attribut include header.
     * 
     * @param includeHeader le nouveau include header
     */
    public void setIncludeHeader(String includeHeader)
    {
        this.includeHeader = includeHeader;
    }

    /**
     * Accesseur de l attribut delimiter.
     * 
     * @return delimiter
     */
    public String getDelimiter()
    {
        return TagUtils.evaluateExpressionAsString("delimiter", delimiter, this, pageContext);
    }

    /**
     * Modificateur de l attribut delimiter.
     * 
     * @param delimiter le nouveau delimiter
     * jsp.attribute :  description="Indique le séparateur de champs à utiliser. La valeur par défaut est la
     *                virgule." required="false" rtexprvalue="true"
     */
    public void setDelimiter(String delimiter)
    {
        this.delimiter = delimiter;
    }

    /**
     * Accesseur de l attribut endsWithDelimiter.
     * 
     * @return endsWithDelimiter
     */
    public String getEndsWithDelimiter()
    {
        return TagUtils.evaluateExpressionAsString("endsWithDelimiter", endsWithDelimiter, this, pageContext);
    }

    /**
     * Modificateur de l attribut endsWithDelimiter.
     * 
     * @param endsWithDelimiter le nouveau endsWithDelimiter
     * jsp.attribute :  description="Indique le séparateur de champs à utiliser. La valeur par défaut est la
     *                virgule." required="false" rtexprvalue="true"
     */
    public void setEndsWithDelimiter(String endsWithDelimiter)
    {
        this.endsWithDelimiter = endsWithDelimiter;
    }

    /**
     * Accesseur de l attribut protection.
     * 
     * @return protection
     */
    public String getProtection()
    {
        return TagUtils.evaluateExpressionAsString("protection", protection, this, pageContext);
    }

    /**
     * Modificateur de l attribut protection.
     * 
     * @param protection le nouveau protection
     * jsp.attribute :  description="Indique le protecteur de champs à utiliser. La valeur par défaut est la
     *                double-quote." required="false" rtexprvalue="true"
     */
    public void setProtection(String protection)
    {
        this.protection = protection;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.tag.ExportTag#addExportAttributes(fr.gouv.finances.lombok.apptags.table.core.TableModel,
     *      fr.gouv.finances.lombok.apptags.table.bean.Export)
     */
    @Override
    public void addExportAttributes(TableModel model, Export export)
    {
        if (StringUtils.isBlank(export.getView()))
        {
            export.setView(TableConstants.VIEW_CSV);
        }

        if (StringUtils.isBlank(export.getViewResolver()))
        {
            export.setViewResolver(TableConstants.VIEW_CSV);
        }

        if (StringUtils.isBlank(export.getImageName()))
        {
            export.setImageName(TableConstants.VIEW_CSV);
        }

        if (StringUtils.isBlank(export.getText()))
        {
            export.setText(BuilderConstants.TOOLBAR_CSV_TEXT);
        }

        export.addAttribute(CsvProtegeView.DELIMITER, TagUtils.evaluateExpressionAsString("delimiter", delimiter, this,
            pageContext));

        export.addAttribute(CsvProtegeView.ENDS_WITH_DELIMITER, TagUtils.evaluateExpressionAsString("endsWithDelimiter", endsWithDelimiter, this,
            pageContext));
        
        export.addAttribute(CsvProtegeView.PROTECTION, TagUtils.evaluateExpressionAsString("protection", protection,
            this, pageContext));

        export.addAttribute(CsvProtegeView.INCLUDE_HEADER, TagUtils.evaluateExpressionAsString("includeHeader", includeHeader, this,
            pageContext));

        export.addAttribute(CsvProtegeView.COLONNE_PROTECTION_CLASS,
            TagUtils.evaluateExpressionAsString("colonneProtectionClass", colonneProtectionClass,
                this, pageContext));

        export.addAttribute(CsvProtegeView.INCLUDE_TITLE, TagUtils.evaluateExpressionAsString("includeTitle", includeTitle, this,
            pageContext));

    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.tag.ExportTag#release()
     */
    @Override
    public void release()
    {
        delimiter = null;
        protection = null;
        super.release();
    }

    /**
     * Gets the includeTitle - String, inclusion d'un titre.
     * 
     * @return the includeTitle - String, inclusion d'un titre
     */
    public String getIncludeTitle()
    {
        return includeTitle;
    }

    /**
     * Sets the includeTitle - String, inclusion d'un titre.
     * 
     * @param includeTitle the new includeTitle - String, inclusion d'un titre
     */
    public void setIncludeTitle(String includeTitle)
    {
        this.includeTitle = includeTitle;
    }
}
