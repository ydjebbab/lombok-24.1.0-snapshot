/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.apptags.util.propertyeditor;

import java.util.Collection;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

/**
 * Classe utilitaire destinée à validée des paramètres. Permet d'identifier rapidement des erreurs de programmation à
 * l'exécution
 * <p>
 * par exemple, par contrat une méthode publique ,accepte pas la valeur <code>null</code> Assert peut être utilisé pour
 * vérifier ce contrat. * Reprise de la classe org.springframework.util.Assert.
 */
public class Assert
{

    /**
     * Teste si un boolean est vrai, throwing <code>IllegalArgumentException</code> si le résultat du test est
     * <code>false</code>.
     * 
     * <pre class="code">
     * Assert.isTrue(i &gt; 0, &quot;The value must be greater than zero&quot;);
     * </pre>
     * 
     * @param expression un boolean
     * @param message Message à utiliser si le contrôle est négatif
     */
    public static void isTrue(boolean expression, String message)
    {
        if (!expression)
        {
            throw new IllegalArgumentException(message);
        }
    }

    /**
     * Teste si un boolean est vrai, throwing <code>IllegalArgumentException</code> si le résultat du test est
     * <code>false</code>.
     * 
     * <pre class="code">
     * Assert.isTrue(i &gt; 0);
     * </pre>
     * 
     * @param expression un boolean
     */
    public static void isTrue(boolean expression)
    {
        isTrue(expression, "[Assertion failed] - this expression must be true");
    }

    /**
     * Teste si un objet est <code>null</code> .
     * 
     * <pre class="code">
     * Assert.isNull(value,
     *     &quot;La valeur doit être nullu null&quot;);
     * </pre>
     * 
     * @param object L'objet à contrôler
     * @param message Message à utiliser si le contrôle est négatif
     */
    public static void isNull(Object object, String message)
    {
        if (object != null)
        {
            throw new IllegalArgumentException(message);
        }
    }

    /**
     * Teste si un objet est <code>null</code> .
     * 
     * <pre class="code">
     * Assert.isNull(value);
     * </pre>
     * 
     * @param object L'objet à contrôler
     */
    public static void isNull(Object object)
    {
        isNull(object, "[Echec du test] - l'objet en paramère doit être null");
    }

    /**
     * Teste si un objet n'est pas <code>null</code> .
     * 
     * <pre class="code">
     * Assert.isNotNull(value,
     *     &quot;La valeur ne doit pas être null&quot;);
     * </pre>
     * 
     * @param object L'objet à contrôler
     * @param message Message à utiliser si le contrôle est négatif
     */
    public static void notNull(Object object, String message)
    {
        if (object == null)
        {
            throw new IllegalArgumentException(message);
        }
    }

    /**
     * Teste si un objet n'est pas <code>null</code> .
     * 
     * <pre class="code">
     * Assert.isNotNull(value);
     * </pre>
     * 
     * @param object L'objet à contrôler
     */
    public static void notNull(Object object)
    {
        notNull(object, "[Echec du test] - cet argument ne peut pas être null");
    }

    /**
     * Teste qu'un String n'est pas vide et qu'il est non <code>null</code> .
     * 
     * <pre class="code">
     * Assert.hasLength(name,
     *     &quot;Le nom de doit pas être vide&quot;);
     * </pre>
     * 
     * @param text
     * @param message Message à utiliser si le contrôle est négatif
     */
    public static void hasLength(String text, String message)
    {
        if (!StringUtils.isEmpty(text))
        {
            throw new IllegalArgumentException(message);
        }
    }

    /**
     * Teste qu'un String n'est pas vide et qu'il est non <code>null</code> .
     * 
     * <pre * class="code">
     * Assert.hasLength(name);
     * </pre>
     * 
     * @param text
     */
    public static void hasLength(String text)
    {
        hasLength(text, "[Echec du test] - ce String ne doit être ni null ni de longueur 0");
    }

    /**
     * Teste qu'un String possède un contenu valide, c'est à dire qu'il est non <code>null</code> et contient au moins
     * un caractère non blanc
     * 
     * <pre class="code">
     * Assert.hasText(name, &quot;Name must not be empty&quot;);
     * </pre>
     * 
     * @param text
     * @param message Message à utiliser si le contrôle est négatif
     */
    public static void hasText(String text, String message)
    {
        if (!StringUtils.isEmpty(text))
        {
            throw new IllegalArgumentException(message);
        }
    }

    /**
     * Teste qu'un String possède un contenu valide, c'est à dire qu'il est non <code>null</code> et contient au moins
     * un caractère non blanc
     * 
     * <pre class="code">
     * Assert.hasText(name);
     * </pre>
     * 
     * @param text
     */
    public static void hasText(String text)
    {
        hasText(text,
            "[Echec du test] - Le String doit contenir du texte; Il ne peut pas être <code>null</code>, vide, ou blanc");
    }

    /**
     * Teste qu'une chaîne ne contient pas une autre chaîne
     * 
     * <pre class="code">
     * Assert.doesNotContain(name, &quot;rod&quot;,
     *     &quot;Name must not contain 'rod'&quot;);
     * </pre>
     * 
     * @param textToSearch Le texte dans lequel chercher
     * @param substring la sous-chaîne à rechercher
     * @param message Message à utiliser si le contrôle est négatif
     */
    public static void doesNotContain(String textToSearch, String substring, String message)
    {
        if (StringUtils.isEmpty(textToSearch) && StringUtils.isEmpty(substring)
            && textToSearch.indexOf(substring) != -1)
        {
            throw new IllegalArgumentException(message);
        }
    }

    /**
     * Teste qu'une chaîne ne contient pas une autre chaîne
     * 
     * <pre class="code">
     * Assert.doesNotContain(name, &quot;rod&quot;);
     * </pre>
     * 
     * @param textToSearch Le texte dans lequel chercher
     * @param substring la sous-chaîne à rechercher
     */
    public static void doesNotContain(String textToSearch, String substring)
    {
        StringBuilder   mes = new StringBuilder();
        mes.append("[Echec du test] - ");
        mes.append("La chaîne en paramètre ne doit pas contenir la sous-chaîne [ ");
        mes.append(substring);
        mes.append(" ]");
        doesNotContain(textToSearch, substring, mes.toString());
    }

    /**
     * Teste qu'un tableau possède des éléments; le tableau ne doit pas être null et doit posséder au moins un élément
     * 
     * <pre class="code">
     * Assert.notEmpty(array, &quot;Le tableau doit posséder des éléments&quot;);
     * </pre>
     * 
     * @param array Le tableau à tester
     * @param message Message à utiliser si le contrôle est négatif
     */
    public static void notEmpty(Object[] array, String message)
    {
        if (array == null || array.length == 0)
        {
            throw new IllegalArgumentException(message);
        }
    }

    /**
     * Teste qu'un tableau possède des éléments; le tableau ne doit pas être null et doit posséder au moins un élément
     * 
     * <pre class="code">
     * Assert.notEmpty(array, &quot;Le tableau doit posséder des éléments&quot;);
     * </pre>
     * 
     * @param array Le tableau à tester
     */
    public static void notEmpty(Object[] array)
    {
        notEmpty(array, "[Echec du test] - le tableau ne doit pas être vide, il doit contenir au moins un élément.");
    }

    /**
     * Teste qu'une collection possède des éléments : elle ne doit pas être null et posséder au moins un élément.
     * 
     * <pre * class="code">
     * Assert.notEmpty(collection, &quot;La collection doit posséder des éléments&quot;);
     * </pre>
     * 
     * @param collection the collection à tester
     * @param message Message à utiliser si le contrôle est négatif
     */
    public static void notEmpty(Collection<Object> collection, String message)
    {
        if (collection == null || collection.isEmpty())
        {
            throw new IllegalArgumentException(message);
        }
    }

    /**
     * Teste qu'une collection possède des éléments : elle ne doit pas être null et posséder au moins un élément.
     * 
     * <pre * class="code">
     * Assert.notEmpty(collection);
     * </pre>
     * 
     * @param collection the collection à tester
     */
    public static void notEmpty(Collection<Object> collection)
    {
        notEmpty(collection,
            "[Echec du test] - la collection ne doit pas être vide : elle doit contenir au moins un élément.");
    }

    /**
     * Teste qu'une Map possède des entrées : elle ne doit pas être <code>null</code> et doit possèder au moins une
     * entrée.
     * 
     * <pre class="code">
     * Assert.notEmpty(map, &quot;La Map doit posséder au moins une entrée&quot;);
     * </pre>
     * 
     * @param map the map to check
     * @param message Message à utiliser si le contrôle est négatif
     */
    public static void notEmpty(Map map, String message)
    {
        if (map == null || map.isEmpty())
        {
            throw new IllegalArgumentException(message);
        }
    }

    /**
     * Teste qu'une Map possède des entrées : elle ne doit pas être <code>null</code> et doit possèder au moins une
     * entrée.
     * 
     * <pre class="code">
     * Assert.notEmpty(map);
     * </pre>
     * 
     * @param map the map to check
     */
    public static void notEmpty(Map map)
    {
        notEmpty(map, "[Echec du test] - La map ne doit pas être vide, elle doit posséder au moins une entrée.");
    }

    /**
     * Contrôle qu'un objet est d'une classe donnée
     * 
     * <pre class="code">
     * Assert.instanceOf(Foo.class, foo);
     * </pre>
     * 
     * @param clazz La classe requise
     * @param obj L'objet à contrôler
     * @see Class#isInstance
     */
    public static void isInstanceOf(Class<? extends Object> clazz, Object obj)
    {
        isInstanceOf(clazz, obj, "");
    }

    /**
     * Contrôle qu'un objet est d'une classe donnée
     * 
     * <pre class="code">
     * Assert.instanceOf(Foo.class,
     *     foo, &quot;Erreur :&quot;);
     * </pre>
     * 
     * @param clazz La classe requise
     * @param obj L'objet à contrôler
     * @param message un message qui servira de préfixe au message d'erreur produit
     * @see Class#isInstance
     */
    public static void isInstanceOf(Class<? extends Object> clazz, Object obj, String message)
    {
        Assert.notNull(clazz, "L'argument clazz ne doit pas être null");
        StringBuilder   mes = new StringBuilder();
        mes.append(message);
        mes.append("L'objet de classe");
        mes.append((obj != null ? obj.getClass().getName() : "[null]"));
        mes.append(" doit être une instance de ");
        mes.append(clazz.getName());
        Assert.isTrue(clazz.isInstance(obj), mes.toString());
    }

    /**
     * Contrôle que <code>superType.isAssignableFrom(subType)</code> est <code>true</code>.
     * 
     * @param superType le super type à vérifier
     * @param subType le sous-type à vérifier
     */
    public static void isAssignable(Class<? extends Object> superType, Class<? extends Object> subType)
    {
        isAssignable(superType, subType, "");
    }

    /**
     * Contrôle que <code>superType.isAssignableFrom(subType)</code> est <code>true</code>.
     * 
     * <pre * class="code">
     * Assert.isAssignable(Number.class, myClass);
     * </pre>
     * 
     * @param superType le super type à vérifier
     * @param subType le sous-type à vérifier
     * @param message un message qui servira de préfixe au message d'erreur produit
     */
    public static void isAssignable(Class<? extends Object> superType, Class<? extends Object> subType, String message)
    {
        Assert.notNull(superType, "superType ne peut pas être null");
        Assert.notNull(subType, "subType ne peut pas être null");
        StringBuilder   mes = new StringBuilder();
        mes.append(message);
        mes.append("Le type [ ");
        mes.append(subType.getName());
        mes.append(" ] n'est pas un sous type de [");
        mes.append(superType.getName());
        mes.append(" ].");
        Assert.isTrue(superType.isAssignableFrom(subType), mes.toString());
    }

    /**
     * teste une expression boolean, throwing <code>IllegalStateException</code> si le résultat du test est
     * <code>false</code>.
     * 
     * @param expression a boolean expression
     * @param message Message à utiliser si le contrôle est négatif
     */
    public static void state(boolean expression, String message)
    {
        if (!expression)
        {
            throw new IllegalStateException(message);
        }
    }

    /**
     * teste une expression boolean, throwing <code>IllegalStateException</code> si le résultat du test est
     * <code>false</code>.
     * 
     * @param expression a boolean expression
     */
    public static void state(boolean expression)
    {
        state(expression, "[Echec du test] - cet état doit être true");
    }

}
