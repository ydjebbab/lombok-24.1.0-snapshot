/*
 * Copyright (c) 2014 DGFiP - Tous droits réservés
 *//*
/*Copyright (c) 2014 DGFiP - Tous droits réservés/* * Copyright 2004 original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package fr.gouv.finances.lombok.apptags.table.view.html;

import org.apache.commons.lang.StringUtils;

import fr.gouv.finances.lombok.apptags.table.core.TableModel;

/**
 * Class BuilderUtils
 */
public class BuilderUtils
{

    /**
     * Instanciation de builder utils.
     */
    private BuilderUtils()
    {
    }

    /**
     * methode Show pagination :
     * 
     * @param model
     * @return true, si c'est vrai
     */
    public static boolean showPagination(TableModel model)
    {
        return model.getTableHandler().getTable().isShowPagination();
    }

    /**
     * methode Show new pagination :
     * 
     * @param model
     * @return true, si c'est vrai
     */
    public static boolean showNewPagination(TableModel model)
    {
        return model.getTableHandler().getTable().isShowNewPagination();
    }

    /**
     * methode Show new Filter :
     * 
     * @param model
     * @return true, si c'est vrai
     */
    public static boolean showNewFilter(TableModel model)
    {
        return model.getTableHandler().getTable().isShowNewFilter();
    }

    /**
     * methode Show pagination bottom :
     * 
     * @param model
     * @return true, si c'est vrai
     */
    public static boolean showPaginationBottom(TableModel model)
    {
        return model.getTableHandler().getTable().isShowPaginationBottom();
    }

    /**
     * methode In pagination bottom :
     * 
     * @param model
     * @return true, si c'est vrai
     */
    public static boolean inPaginationBottom(TableModel model)
    {
        return model.getTableHandler().getTable().isInPaginationBottom();
    }

    /**
     * Modificateur de l attribut in pagination bottom.
     * 
     * @param model le nouveau in pagination bottom
     */
    public static void setInPaginationBottom(TableModel model)
    {
        model.getTableHandler().getTable().setInPaginationBottom(true);
    }

    /**
     * methode Show exports bottom :
     * 
     * @param model
     * @return true, si c'est vrai
     */
    public static boolean showExportsBottom(TableModel model)
    {
        return model.getTableHandler().getTable().isShowExportsBottom();
    }

    
    /**
     * methode  :  showResponsive
     * 
     * @param model
     * @return true, si c'est vrai
     */
    public static boolean showResponsive(TableModel model)
    {    
        return model.getTableHandler().getTable().isShowResponsive();
    }
    
    /**
     * methode  : getDataAttributes
     * 
     * @param model
     * @return Data Attributes
     */
    public static String getDataAttributes(TableModel model)
    {    
        return model.getTableHandler().getTable().getDataAttributes();
    }
    
    
    /**
     * methode  : getPaginationClass
     * 
     * @param model
     * @return getPaginationClass
     */
    public static String getPaginationClass(TableModel model)
    {    
        return model.getTableHandler().getTable().getPaginationClass();
    }
    
    
    /**
     * methode  : getExportsClass
     * 
     * @param model
     * @return getExportsClass
     */
    public static String getExportsClass(TableModel model)
    {    
        return model.getTableHandler().getTable().getExportsClass();
    }
    
    
    /**
     * methode New pagination nb page :
     * 
     * @param model
     * @return int
     */
    public static int newPaginationNbPage(TableModel model)
    {
        return model.getTableHandler().getTable().getNewPaginationNbPage();
    }

    /**
     * methode Show exports :
     * 
     * @param model
     * @return true, si c'est vrai
     */
    public static boolean showExports(TableModel model)
    {
        if (!model.getTableHandler().getTable().isShowExports())
        {
            return false;
        }

        return model.getExportHandler().getExports().size() > 0;
    }

    /**
     * methode Show exports with limit size pdf :
     * 
     * @param model
     * @return int
     */
    public static int showExportsWithLimitSizePdf(TableModel model)
    {
        return model.getTableHandler().getTable().getShowExportsWithLimitSizePdf();
    }

    /**
     * methode Show exports with limit size xls or ods :
     * 
     * @param model
     * @return int
     */
    public static int showExportsWithLimitSizeXlsOrOds(TableModel model)
    {
        return model.getTableHandler().getTable().getShowExportsWithLimitSizeXlsOrOds();
    }

    /**
     * methode Show exports with limit size csv :
     * 
     * @param model
     * @return int
     */
    public static int showExportsWithLimitSizeCsv(TableModel model)
    {
        return model.getTableHandler().getTable().getShowExportsWithLimitSizeCsv();
    }

    /**
     * methode Show status bar :
     * 
     * @param model
     * @return true, si c'est vrai
     */
    public static boolean showStatusBar(TableModel model)
    {
        return model.getTableHandler().getTable().isShowStatusBar();
    }

    /**
     * methode Show title :
     * 
     * @param model
     * @return true, si c'est vrai
     */
    public static boolean showTitle(TableModel model)
    {
        return model.getTableHandler().getTable().isShowTitle();
    }

    /**
     * methode Compact :
     * 
     * @param model
     * @return true, si c'est vrai
     */
    public static boolean compact(TableModel model)
    {
        return model.getTableHandler().getTable().isCompact();
    }

    /**
     * methode Filterable :
     * 
     * @param model
     * @return true, si c'est vrai
     */
    public static boolean filterable(TableModel model)
    {
        return model.getTableHandler().getTable().isFilterable();
    }

    /**
     * Verifie si first page enabled.
     * 
     * @param page
     * @return true, si c'est first page enabled
     */
    public static boolean isFirstPageEnabled(int page)
    {
        if (page == 1)
        {
            return false;
        }

        return true;
    }

    /**
     * Verifie si prev page enabled.
     * 
     * @param page
     * @return true, si c'est prev page enabled
     */
    public static boolean isPrevPageEnabled(int page)
    {
        if (page - 1 < 1)
        {
            return false;
        }

        return true;
    }

    /**
     * Verifie si next page enabled.
     * 
     * @param page
     * @param totalPages
     * @return true, si c'est next page enabled
     */
    public static boolean isNextPageEnabled(int page, int totalPages)
    {
        if (page + 1 > totalPages)
        {
            return false;
        }

        return true;
    }

    /**
     * Verifie si last page enabled.
     * 
     * @param page
     * @param totalPages
     * @return true, si c'est last page enabled
     */
    public static boolean isLastPageEnabled(int page, int totalPages)
    {
        if (page == totalPages || totalPages == 0)
        {
            return false;
        }

        return true;
    }

    /**
     * Accesseur de l attribut total pages.
     * 
     * @param model
     * @return total pages
     */
    public static int getTotalPages(TableModel model)
    {
        int currentRowsDisplayed = model.getLimit().getCurrentRowsDisplayed();

        if (currentRowsDisplayed == 0)
        {
            currentRowsDisplayed = model.getLimit().getTotalRows();
        }

        int totalRows = model.getLimit().getTotalRows();

        int totalPages = 1;

        if (currentRowsDisplayed != 0)
        {
            totalPages = totalRows / currentRowsDisplayed;
        }

        if ((currentRowsDisplayed != 0) && ((totalRows % currentRowsDisplayed) > 0))
        {
            totalPages++;
        }

        return totalPages;
    }

    /**
     * Accesseur de l attribut image.
     * 
     * @param model
     * @param imageName
     * @return image
     */
    public static String getImage(TableModel model, String imageName)
    {
        String imagePath = model.getTableHandler().getTable().getImagePath();

        if (StringUtils.isNotBlank(imagePath))
        {
            int index = imagePath.indexOf("*.");
            return imagePath.substring(0, index) + imageName + imagePath.substring(index + 1);
        }

        return null;
    }

    /**
     * Get the form to use. If the Table form attribute is declared then use that, else use the tableId.
     * 
     * @param model
     * @return The name of the form.
     */
    public static String getForm(TableModel model)
    {
        String form = model.getTableHandler().getTable().getForm();
        if (StringUtils.isBlank(form))
        {
            form = model.getTableHandler().getTable().getTableId();
        }

        return form;
    }
}
