/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.apptags.table.cell;

import java.util.Date;

import fr.gouv.finances.lombok.apptags.table.bean.Column;
import fr.gouv.finances.lombok.apptags.table.core.TableModel;
import fr.gouv.finances.lombok.apptags.util.format.FormaterDate;

/**
 * Class AbstractDateCell
 */
public abstract class AbstractDateCell extends AbstractCell implements PatternCell
{

    /** Constant : NOT_A_DATE. */
    protected static final String NOT_A_DATE = "Erreur - ceci n'est pas une date";

    /** Constant : BLANK_CELL_HTML. */
    protected static final String BLANK_CELL_HTML = "&#160;";

    /**
     * Instanciation de abstract date cell.
     */
    public AbstractDateCell()
    {
        super();
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.cell.PatternCell#getFormatPattern()
     */
    public abstract String getFormatPattern();

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.cell.AbstractCell#getExportDisplay(fr.gouv.finances.lombok.apptags.table.core.TableModel,
     *      fr.gouv.finances.lombok.apptags.table.bean.Column)
     */
    @Override
    public String getExportDisplay(TableModel model, Column column)
    {
        String cellValue = null;
        Object value = column.getPropertyValue();

        if (value == null)
        {
        }
        else if (value instanceof Date)
        {
            cellValue = FormaterDate.getFormatDate(getFormatPattern()).format(value);
        }
        else
        {
            cellValue = NOT_A_DATE;
        }
        return cellValue;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.cell.AbstractCell#getCellValue(fr.gouv.finances.lombok.apptags.table.core.TableModel,
     *      fr.gouv.finances.lombok.apptags.table.bean.Column)
     */
    @Override
    protected String getCellValue(TableModel model, Column column)
    {
        String cellValue;
        Object value = column.getPropertyValue();

        if (value == null)
        {
            cellValue = BLANK_CELL_HTML;
        }
        else if (value instanceof Date)
        {
            cellValue = FormaterDate.getFormatDate(getFormatPattern()).format(value);
        }
        else
        {
            cellValue = NOT_A_DATE;
        }

        return cellValue;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.cell.PatternCell#getLengthCelluleFormatee(fr.gouv.finances.lombok.apptags.table.core.TableModel,
     *      fr.gouv.finances.lombok.apptags.table.bean.Column)
     */
    @Override
    public int getLengthCelluleFormatee(TableModel model, Column column)
    {
        int nbChar = 0;
        if (column.getPropertyValue() != null)
        {
            nbChar = getCellValue(model, column).length();
        }
        return nbChar;
    }
}
