/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.apptags.table.calc;

import java.math.BigDecimal;
import java.util.Collection;

import fr.gouv.finances.lombok.apptags.table.bean.Column;
import fr.gouv.finances.lombok.apptags.table.core.TableModel;

/**
 * Class CpMinimumCalc
 */
public class CpMinimumCalc implements Calc
{

    /**
     * Constructeur de la classe CpMinimumCalc.java
     *
     */
    public CpMinimumCalc()
    {
        super();
        
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.calc.Calc#getCalcResult(fr.gouv.finances.lombok.apptags.table.core.TableModel,
     *      fr.gouv.finances.lombok.apptags.table.bean.Column)
     */
    @Override
    public Number getCalcResult(TableModel model, Column column)
    {
        Collection<Object> rows = model.getCollectionOfFilteredBeans();
        String property = column.getProperty();
        MinimumValue minimumValue = new MinimumValue();
        CpCalcUtils.eachRowCalcValue(minimumValue, rows, property);

        return minimumValue.getMinimumValue();
    }

    /**
     * Class MinimumValue
     */
    private static class MinimumValue implements CalcHandler
    {

        /** minimum. */
        private BigDecimal minimum = new BigDecimal(0);

        /**
         * (methode de remplacement) {@inheritDoc}
         * 
         * @see fr.gouv.finances.lombok.apptags.table.calc.CalcHandler#processCalcValue(java.lang.Number)
         */
        @Override
        public void processCalcValue(Number calcValue)
        {
            if (minimum == null)
            {
                minimum = new BigDecimal(String.valueOf(calcValue));
            }

            else if (calcValue != null && new BigDecimal(String.valueOf(calcValue)).compareTo(minimum) < 0)
            {
                minimum = new BigDecimal(String.valueOf(calcValue));
            }
        }

        /**
         * Accesseur de l attribut minimum value.
         * 
         * @return minimum value
         */
        public Number getMinimumValue()
        {
            return minimum;
        }
    }
}
