/*
/*Copyright (c) 2014 DGFiP - Tous droits réservés/* * Copyright 2004 original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package fr.gouv.finances.lombok.apptags.table.handler;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import fr.gouv.finances.lombok.apptags.table.bean.Export;
import fr.gouv.finances.lombok.apptags.table.core.PreferencesConstants;
import fr.gouv.finances.lombok.apptags.table.core.TableCache;
import fr.gouv.finances.lombok.apptags.table.core.TableConstants;
import fr.gouv.finances.lombok.apptags.table.core.TableModel;
import fr.gouv.finances.lombok.apptags.table.core.TableModelUtils;

/**
 * Class ExportHandler
 */
public class ExportHandler
{

    /** log. */
    private static Log log = LogFactory.getLog(ExportHandler.class);

    /** model. */
    private TableModel model;

    /** exports. */
    private List<Export> exports = new ArrayList<Export>();

    /**
     * Instanciation de export handler.
     * 
     * @param model
     */
    public ExportHandler(TableModel model)
    {
        this.model = model;
    }

    /**
     * methode Adds the export :
     * 
     * @param export
     */
    public void addExport(Export export)
    {
        this.exports.add(export);
        addExportAttributes(export);
        export.defaults();
    }

    /**
     * methode Adds the export attributes :
     * 
     * @param export
     */
    public void addExportAttributes(Export export)
    {
        String interceptor =
            TableModelUtils.getInterceptPreference(this.model, export.getInterceptor(),
                PreferencesConstants.EXPORT_INTERCEPTOR);
        export.setInterceptor(interceptor);
        TableCache.getInstance().getExportInterceptor(interceptor).addExportAttributes(this.model, export);
    }

    /**
     * Retourne l'export identifié par une vue donnée.
     * 
     * @param view
     * @return export
     */
    public Export getExport(String view)
    {
        Export result = null;
        for (Export export : exports)
        {
            if (export.getView().equals(view))
            {
                result = export;
                break;
            }
        }

        return result;
    }

    /**
     * Retourne l'export courant.
     * 
     * @return current export
     */
    public Export getCurrentExport()
    {
        String prefixWithTableId = this.model.getTableHandler().prefixWithTableId();

        String exportView = model.getRegistry().getParameter(prefixWithTableId + TableConstants.EXPORT_VIEW);

        Export export = getExport(exportView);
        if (export == null)
        {
            StringBuilder   mesBuf = new StringBuilder(78);
            mesBuf.append("Aucun export n'est défini.");
            mesBuf.append("L'export doit être déclaré (Export ou ExportTag) avec les tags rows et columns");
            log.error(mesBuf.toString());
            throw new IllegalStateException(mesBuf.toString());
        }

        return export;
    }

    /**
     * Accesseur de l attribut exports.
     * 
     * @return exports
     */
    public List<Export> getExports()
    {
        return exports;
    }

    /**
     * Verifie si exported.
     * 
     * @return true, si c'est exported
     * @deprecated This functionality is available on the Limit.
     */
    @Deprecated
    public boolean isExported()
    {
        return model.getExportHandler().isExported();
    }

    /**
     * methode Show exports :
     * 
     * @return true, si c'est vrai
     * @deprecated Specific logic to building html. Moved to BuilderUtils.showExports.
     */
    @Deprecated
    public boolean showExports()
    {
        if (!model.getTableHandler().getTable().isShowExports())
        {
            return false;
        }

        return getExports().size() > 0;
    }
}
