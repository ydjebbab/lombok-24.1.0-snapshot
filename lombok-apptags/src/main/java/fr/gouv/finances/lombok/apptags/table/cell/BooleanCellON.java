/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.apptags.table.cell;

import fr.gouv.finances.lombok.apptags.util.format.BooleanFormat.TypeFormat;

/**
 * Class BooleanCellON
 */
public class BooleanCellON extends AbstractBooleanCell
{

    /**
     * Instanciation de boolean cell on.
     */
    public BooleanCellON()
    {
        super();
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.cell.AbstractBooleanCell#getFormat()
     */
    @Override
    public TypeFormat getFormat()
    {
        return TypeFormat.O_N;
    }
}
