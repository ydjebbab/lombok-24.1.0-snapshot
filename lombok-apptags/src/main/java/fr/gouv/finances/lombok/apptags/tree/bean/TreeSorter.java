/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : TreeSorter.java
 *
 */
package fr.gouv.finances.lombok.apptags.tree.bean;

import java.util.Collections;
import java.util.Comparator;

/**
 * Class TreeSorter.
 * 
 * @author wpetit-cp
 * @version $Revision: 1.2 $ Date: 15 déc. 2009
 */
public class TreeSorter
{

    /** Constant : ASCENDING. */
    private static final int ASCENDING = 1;

    /** Constant : DESCENDING. */
    private static final int DESCENDING = -1;

    /**
     * methode Sort : --.
     * 
     * @param node --
     * @param comparator --
     */
    public static void sort(TreeNode node, Comparator<TreeNode> comparator)
    {
        Collections.sort(node.getChildren(), comparator);
    }

    /**
     * methode Sort by id : --.
     * 
     * @param node --
     */
    public static void sortById(TreeNode node)
    {
        sort(node, createIdComparator(ASCENDING));
    }

    /**
     * methode Sort by id descending : --.
     * 
     * @param node --
     */
    public static void sortByIdDescending(TreeNode node)
    {
        sort(node, createIdComparator(DESCENDING));
    }

    /**
     * methode Sort by name : --.
     * 
     * @param node --
     */
    public static void sortByName(TreeNode node)
    {
        sort(node, createNameComparator(ASCENDING));
    }

    /**
     * methode Sort by name descending : --.
     * 
     * @param node --
     */
    public static void sortByNameDescending(TreeNode node)
    {
        sort(node, createNameComparator(DESCENDING));
    }

    /**
     * methode Sort by type : --.
     * 
     * @param node --
     */
    public static void sortByType(TreeNode node)
    {
        sort(node, createTypeComparator(ASCENDING));
    }

    /**
     * methode Sort by type descending : --.
     * 
     * @param node --
     */
    public static void sortByTypeDescending(TreeNode node)
    {
        sort(node, createTypeComparator(DESCENDING));
    }

    /**
     * methode Sort recursive : --.
     * 
     * @param node --
     * @param comparator --
     */
    public static void sortRecursive(TreeNode node, Comparator<TreeNode> comparator)
    {
        sort(node, comparator);
        for (TreeNode child : node.getChildren())
        {
            sortRecursive(child, comparator);
        }
    }

    /**
     * methode Sort recursive by id : --.
     * 
     * @param node --
     */
    public static void sortRecursiveById(TreeNode node)
    {
        sortRecursive(node, createIdComparator(ASCENDING));
    }

    /**
     * methode Sort recursive by id descending : --.
     * 
     * @param node --
     */
    public static void sortRecursiveByIdDescending(TreeNode node)
    {
        sortRecursive(node, createIdComparator(DESCENDING));
    }

    /**
     * methode Sort recursive by name : --.
     * 
     * @param node --
     */
    public static void sortRecursiveByName(TreeNode node)
    {
        sortRecursive(node, createNameComparator(ASCENDING));
    }

    /**
     * methode Sort recursive by name descending : --.
     * 
     * @param node --
     */
    public static void sortRecursiveByNameDescending(TreeNode node)
    {
        sortRecursive(node, createNameComparator(DESCENDING));
    }

    /**
     * methode Sort recursive by type : --.
     * 
     * @param node --
     */
    public static void sortRecursiveByType(TreeNode node)
    {
        sortRecursive(node, createTypeComparator(ASCENDING));
    }

    /**
     * methode Sort recursive by type descending : --.
     * 
     * @param node --
     */
    public static void sortRecursiveByTypeDescending(TreeNode node)
    {
        sortRecursive(node, createTypeComparator(DESCENDING));
    }

    /**
     * methode Creates the id comparator : --.
     * 
     * @param sortDirection --
     * @return comparator< tree node>
     */
    private static Comparator<TreeNode> createIdComparator(final int sortDirection)
    {
        return new Comparator<TreeNode>()
        {
            public int compare(TreeNode to1, TreeNode to2)
            {
                return sortDirection * (to1).getId().compareTo((to2).getId());
            }
        };
    }

    /**
     * methode Creates the name comparator : --.
     * 
     * @param sortDirection --
     * @return comparator< tree node>
     */
    private static Comparator<TreeNode> createNameComparator(final int sortDirection)
    {
        return new Comparator<TreeNode>()
        {
            public int compare(TreeNode to1, TreeNode to2)
            {
                return sortDirection * (to1.getName().compareTo(to2.getName()));
            }
        };
    }

    /**
     * methode Creates the type comparator : --.
     * 
     * @param sortDirection --
     * @return comparator< tree node>
     */
    private static Comparator<TreeNode> createTypeComparator(final int sortDirection)
    {
        return new Comparator<TreeNode>()
        {
            public int compare(TreeNode to1, TreeNode to2)
            {
                return sortDirection * (to1).getType().compareTo((to2).getType());
            }
        };
    }

}
