/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.apptags.table.view.jrxmlinfo;

import net.sf.jasperreports.engine.JRRenderable;

/**
 * Class GraphiqueInfo
 */
public class GraphiqueInfo
{

    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** jr renderable. */
    private JRRenderable jrRenderable;

    /** rectangle info. */
    private RectangleInfo rectangleInfo;

    /** cle parametre. */
    private String cleParametre;

    /**
     * Instanciation de graphique info.
     */
    public GraphiqueInfo()
    {
        super();
        this.rectangleInfo = new RectangleInfo();
    }

    /**
     * Accesseur de l attribut cle parametre.
     * 
     * @return cle parametre
     */
    public String getCleParametre()
    {
        return cleParametre;
    }

    /**
     * Modificateur de l attribut cle parametre.
     * 
     * @param cleParametre le nouveau cle parametre
     */
    public void setCleParametre(String cleParametre)
    {
        this.cleParametre = cleParametre;
    }

    /**
     * Accesseur de l attribut jr renderable.
     * 
     * @return jr renderable
     */
    public JRRenderable getJrRenderable()
    {
        return jrRenderable;
    }

    /**
     * Modificateur de l attribut jr renderable.
     * 
     * @param jrRenderable le nouveau jr renderable
     */
    public void setJrRenderable(JRRenderable jrRenderable)
    {
        this.jrRenderable = jrRenderable;
    }

    /**
     * Accesseur de l attribut rectangle info.
     * 
     * @return rectangle info
     */
    public RectangleInfo getRectangleInfo()
    {
        return rectangleInfo;
    }

    /**
     * Modificateur de l attribut rectangle info.
     * 
     * @param rectangleInfo le nouveau rectangle info
     */
    public void setRectangleInfo(RectangleInfo rectangleInfo)
    {
        this.rectangleInfo = rectangleInfo;
    }

}
