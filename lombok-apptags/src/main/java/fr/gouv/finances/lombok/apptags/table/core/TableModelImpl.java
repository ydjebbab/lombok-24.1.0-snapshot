/*
 * Copyright (c) 2014 DGFiP - Tous droits réservés
 */
/*
/*Copyright (c) 2014 DGFiP - Tous droits réservés/* * Copyright 2004 original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package fr.gouv.finances.lombok.apptags.table.core;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Locale;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import fr.gouv.finances.lombok.apptags.table.bean.Column;
import fr.gouv.finances.lombok.apptags.table.bean.Export;
import fr.gouv.finances.lombok.apptags.table.bean.Row;
import fr.gouv.finances.lombok.apptags.table.bean.Table;
import fr.gouv.finances.lombok.apptags.table.context.Context;
import fr.gouv.finances.lombok.apptags.table.handler.ColumnHandler;
import fr.gouv.finances.lombok.apptags.table.handler.ExportHandler;
import fr.gouv.finances.lombok.apptags.table.handler.RowHandler;
import fr.gouv.finances.lombok.apptags.table.handler.TableHandler;
import fr.gouv.finances.lombok.apptags.table.handler.ViewHandler;
import fr.gouv.finances.lombok.apptags.table.limit.Limit;
import fr.gouv.finances.lombok.apptags.table.limit.LimitFactory;
import fr.gouv.finances.lombok.apptags.table.limit.ModelLimitFactory;
import fr.gouv.finances.lombok.apptags.table.limit.TableLimit;

/**
 * Class TableModelImpl
 */
public final class TableModelImpl implements TableModel
{

    /** logger. */
    private static Log logger = LogFactory.getLog(TableModel.class);

    // model interfaces
    /** context. */
    private Context context;

    /** preferences. */
    private Preferences preferences;

    /** messages. */
    private Messages messages;

    /** registry. */
    private Registry registry;

    // model handlers
    /** table handler. */
    private TableHandler tableHandler = new TableHandler(this);

    /** row handler. */
    private RowHandler rowHandler = new RowHandler(this);

    /** column handler. */
    private ColumnHandler columnHandler = new ColumnHandler(this);

    /** view handler. */
    private ViewHandler viewHandler = new ViewHandler(this);

    /** export handler. */
    private ExportHandler exportHandler = new ExportHandler(this);

    // model objects
    /** current row bean. */
    private Object currentRowBean;

    /** collection of beans. */
    private Collection<Object> collectionOfBeans;

    /** collection of filtered beans. */
    private Collection<Object> collectionOfFilteredBeans;

    /** collection of page beans. */
    private Collection<Object> collectionOfPageBeans;

    /** collection of beans from start to current page. */
    private Collection<Object> collectionOfBeansFromStartToCurrentPage;

    /** limit. */
    private Limit limit;

    /** locale. */
    private Locale locale;

    // Infos sur le positionnement

    /** Numéro de la ligne en cours dans la collection traitée. */
    private int coRowCount;

    /** Numéro de la ligne dans la page en cours. */
    private int rowCount;

    /** Numéro de la page en cours. */
    private int pageCount;

    /** Nombre total de ligne de la collection sous-jacente du tableau. */
    private int totalRowCount;

    /** Nombre total de page du tableau. */
    private int totalPageCount;

    /** Indique si la page active est la dernière page. */
    private boolean lastPage;

    /** Indique si la page active est la première page. */
    private boolean firstPage;

    /**
     * Instanciation de table model impl.
     * 
     * @param context
     */
    public TableModelImpl(Context context)
    {
        this(context, null);
    }

    /**
     * Instanciation de table model impl.
     * 
     * @param context
     * @param locale
     */
    public TableModelImpl(Context context, String locale)
    {
        this.context = context;

        Preferences preferences = new TableProperties();
        preferences.init(context, TableModelUtils.getPreferencesLocation(context));
        this.preferences = preferences;

        this.locale = TableModelUtils.getLocale(context, preferences, locale);

        Messages messages = TableModelUtils.getMessages(this);
        messages.init(context, this.locale);
        this.messages = messages;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.core.TableModel#getContext()
     */
    @Override
    public Context getContext()
    {
        return context;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.core.TableModel#getPreferences()
     */
    @Override
    public Preferences getPreferences()
    {
        return preferences;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.core.TableModel#getMessages()
     */
    @Override
    public Messages getMessages()
    {
        return messages;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.core.TableModel#getRegistry()
     */
    @Override
    public Registry getRegistry()
    {
        return registry;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.core.TableModel#getTableInstance()
     */
    @Override
    public Table getTableInstance()
    {
        return new Table(this);
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.core.TableModel#getExportInstance()
     */
    @Override
    public Export getExportInstance()
    {
        return new Export(this);
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.core.TableModel#getRowInstance()
     */
    @Override
    public Row getRowInstance()
    {
        return new Row(this);
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.core.TableModel#getColumnInstance()
     */
    @Override
    public Column getColumnInstance()
    {
        return new Column(this);
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.core.TableModel#addTable(fr.gouv.finances.lombok.apptags.table.bean.Table)
     */
    @Override
    public void addTable(Table table)
    {
        this.tableHandler.addTable(table);

        // affectation des valeurs au registry
        this.registry = new TableRegistry(this);

        // affectation des limites
        LimitFactory limitFactory = new ModelLimitFactory(this);
        this.limit = new TableLimit(limitFactory);

    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.core.TableModel#addExport(fr.gouv.finances.lombok.apptags.table.bean.Export)
     */
    @Override
    public void addExport(Export export)
    {
        exportHandler.addExport(export);
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.core.TableModel#addRow(fr.gouv.finances.lombok.apptags.table.bean.Row)
     */
    @Override
    public void addRow(Row row)
    {
        rowHandler.addRow(row);
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.core.TableModel#addColumn(fr.gouv.finances.lombok.apptags.table.bean.Column)
     */
    @Override
    public void addColumn(Column column)
    {
        columnHandler.addAutoGenerateColumn(column);
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.core.TableModel#addColumns(java.lang.String)
     */
    @Override
    public void addColumns(String autoGenerateColumns)
    {
        autoGenerateColumns = TableModelUtils.getAutoGenerateColumnsPreference(this, autoGenerateColumns);
        TableCache.getInstance().getAutoGenerateColumns(autoGenerateColumns).addColumns(this);
    }

    /**
     * La valeur du paramètre peut être null, String, String[] ou List. Elle sera convertie en String[]
     * 
     * @param name Le nom du paramètre
     * @param value La valeur du paramètre
     */
    @Override
    public void addParameter(String name, Object value)
    {
        registry.addParameter(name, value);
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.core.TableModel#getTableHandler()
     */
    @Override
    public TableHandler getTableHandler()
    {
        return tableHandler;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.core.TableModel#getRowHandler()
     */
    @Override
    public RowHandler getRowHandler()
    {
        return rowHandler;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.core.TableModel#getColumnHandler()
     */
    @Override
    public ColumnHandler getColumnHandler()
    {
        return columnHandler;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.core.TableModel#getViewHandler()
     */
    @Override
    public ViewHandler getViewHandler()
    {
        return viewHandler;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.core.TableModel#getExportHandler()
     */
    @Override
    public ExportHandler getExportHandler()
    {
        return exportHandler;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.core.TableModel#getCurrentRowBean()
     */
    @Override
    public Object getCurrentRowBean()
    {
        return currentRowBean;
    }

    /**
     * Place dans le contexte pageScope le numéro de la ligne dans la collection traitée.
     */
    private void setCoRowCount()
    {
        this.coRowCount = ((limit.getPage() - 1) * limit.getCurrentRowsDisplayed()) + rowHandler.getRow().getRowCount();

        context.setPageAttribute(TableConstants.COROWCOUNT, String.valueOf(this.coRowCount));
    }

    /**
     * Place dans le contexte pageScope le numéro de la ligne dans la page en cours.
     */
    private void setRowCount()
    {
        this.rowCount = rowHandler.getRow().getRowCount();
        context.setPageAttribute(TableConstants.ROWCOUNT, String.valueOf(this.rowCount));
    }

    /**
     * Place dans le contexte pageScope la variable PAGECOUNT qui maintient le numéro de la page en cours.
     */
    private void setPageCount()
    {
        this.pageCount = limit.getPage();
        context.setPageAttribute(TableConstants.PAGECOUNT, String.valueOf(this.pageCount));
    }

    /**
     * Place dans le contexte pageScope la variable TOTALROWCOUNT qui maintient le nombre total de lignes de la
     * collection sous-jacente.
     * 
     * @param totalRows
     * @param rows
     */
    private void setTotalRowCount(Integer totalRows, Collection<Object> rows)
    {
        this.totalRowCount = (totalRows == null ? rows.size() : totalRows.intValue());

        context.setPageAttribute(TableConstants.TOTALROWCOUNT, String.valueOf(this.totalRowCount));
    }

    /**
     * Place dans le contexte pageScope la variable TOTALPAGECOUNT qui maintient le nombre total de page qui peuvent
     * être affichées.
     * 
     * @param totalRows
     * @param rows
     */
    private void setTotalPageCount(Integer totalRows, Collection<Object> rows)
    {
        int nbTotalRows = (totalRows == null ? rows.size() : totalRows.intValue());

        int currentRowsDisplayed = getLimit().getCurrentRowsDisplayed();
        if (currentRowsDisplayed > 0)
        {
            int restei =
                Double.valueOf(
                    Math.ceil(((double) (nbTotalRows % currentRowsDisplayed)) / ((double) currentRowsDisplayed)))
                    .intValue();
            this.totalPageCount = (nbTotalRows / currentRowsDisplayed) + restei;
        }
        else
        {
            this.totalPageCount = 0;
        }
        context.setPageAttribute(TableConstants.TOTALPAGECOUNT, String.valueOf(this.totalPageCount));
    }

    /**
     * Place dans le contexte pageScope la variable ISLASTPAGE qui maintient un flag indiquant si la page active est la
     * dernière page affichable par le tableau.
     */
    private void setLastPage()
    {
        this.lastPage = (this.pageCount == this.totalPageCount ? true : false);
        context.setPageAttribute(TableConstants.ISLASTPAGE, String.valueOf(this.lastPage));
    }

    /**
     * Place dans le contexte pageScope la variable ISLASTPAGE qui maintient un flag indiquant si la page active est la
     * première page affichable par le tableau.
     */
    private void setFirstPage()
    {
        this.firstPage = (this.pageCount == 1 ? true : false);
        context.setPageAttribute(TableConstants.ISFIRSTPAGE, String.valueOf(this.firstPage));
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.core.TableModel#setCurrentRowBean(java.lang.Object)
     */
    @Override
    public void setCurrentRowBean(Object bean)
    {
        // Incrémente le numéro de la ligne en cours de traitement
        rowHandler.increaseRowCount();

        this.currentRowBean = bean;

        this.setCoRowCount();
        this.setRowCount();
        this.setPageCount();
        this.setFirstPage();
        this.setLastPage();

        // Place dans le contexte pageScope la variable qui maintient
        // le bean correspondant à la ligne en cours de traitement
        context.setPageAttribute(tableHandler.getTable().getVar(), bean);
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.core.TableModel#getCollectionOfBeans()
     */
    @Override
    public Collection<Object> getCollectionOfBeans()
    {
        return collectionOfBeans;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.core.TableModel#setCollectionOfBeans(java.util.Collection)
     */
    @Override
    public void setCollectionOfBeans(Collection<Object> collectionOfBeans)
    {
        this.collectionOfBeans = collectionOfBeans;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.core.TableModel#getCollectionOfFilteredBeans()
     */
    @Override
    public Collection<Object> getCollectionOfFilteredBeans()
    {
        return collectionOfFilteredBeans;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.core.TableModel#setCollectionOfFilteredBeans(java.util.Collection)
     */
    @Override
    public void setCollectionOfFilteredBeans(Collection<Object> collectionOfFilteredBeans)
    {
        this.collectionOfFilteredBeans = collectionOfFilteredBeans;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.core.TableModel#getCollectionOfPageBeans()
     */
    @Override
    public Collection<Object> getCollectionOfPageBeans()
    {
        return collectionOfPageBeans;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.core.TableModel#setCollectionOfPageBeans(java.util.Collection)
     */
    @Override
    public void setCollectionOfPageBeans(Collection<Object> collectionOfPageBeans)
    {
        this.collectionOfPageBeans = collectionOfPageBeans;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.core.TableModel#getLimit()
     */
    @Override
    public Limit getLimit()
    {
        return limit;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.core.TableModel#setLimit(fr.gouv.finances.lombok.apptags.table.limit.Limit)
     */
    @Override
    public void setLimit(Limit limit)
    {
        this.limit = limit;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.core.TableModel#getLocale()
     */
    @Override
    public Locale getLocale()
    {
        return locale;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.core.TableModel#execute()
     */
    @Override
    public Collection<Object> execute() throws Exception
    {
        Collection<Object> rows = TableModelUtils.retrieveRows(this);

        rows = new ArrayList<Object>(rows); // on effectue une copie pour être
        // thread safe

        this.collectionOfBeans = rows;

        rows = TableModelUtils.filterRows(this, rows);

        rows = TableModelUtils.sortRows(this, rows);

        this.collectionOfFilteredBeans = rows;

        Integer totalRows = getTableHandler().getTotalRows();
        int defaultRowsDisplayed = getTableHandler().getTable().getRowsDisplayed();
        int defaultStartPage = getTableHandler().getTable().getStartpage();

        if (totalRows != null)
        {
            limit.setRowAttributes(totalRows.intValue(), defaultRowsDisplayed, defaultStartPage);
        }
        else
        {
            limit.setRowAttributes(rows.size(), defaultRowsDisplayed, defaultStartPage);
        }

        if (logger.isDebugEnabled())
        {
            logger.debug(limit.toString());
        }

        this.setTotalRowCount(totalRows, rows);
        this.setTotalPageCount(totalRows, rows);

        // Constitution de la liste des lignes de la première page jusqu'à la page courante
        Collection<Object> rowsfromstart = TableModelUtils.getRowsFromFirstPageToCurrent(this, rows);
        this.collectionOfBeansFromStartToCurrentPage = rowsfromstart;

        rows = TableModelUtils.getCurrentRows(this, rows);

        this.collectionOfPageBeans = rows;

        viewHandler.setView();

        return rows;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.core.TableModel#setColumnValues()
     */
    @Override
    public void setColumnValues() throws Exception
    {
        List<Column> columns = columnHandler.getColumns();
        for (Column column : columns)
        {
            if ("true".equals(column.getAttribute(TableConstants.IS_AUTO_GENERATE_COLUMN)))
            {
                String property = column.getProperty();
                Object propertyValue = TableModelUtils.getColumnPropertyValue(currentRowBean, property);
                column.setValue(propertyValue);
                column.setPropertyValue(propertyValue);
                columnHandler.modifyColumnAttributes(column);
                viewHandler.addColumnValueToView(column);
            }
        }
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.core.TableModel#getViewData()
     */
    @Override
    public Object getViewData() throws Exception
    {
        Object viewData = viewHandler.getView().afterBody(this);

        if (limit.isExported())
        {
            context.setRequestAttribute(TableConstants.VIEW_DATA, viewData);
            context.setRequestAttribute(TableConstants.VIEW_RESOLVER, exportHandler.getCurrentExport()
                .getViewResolver());
            context
                .setRequestAttribute(TableConstants.EXPORT_FILE_NAME, exportHandler.getCurrentExport().getFileName());
            return "";
        }

        return viewData;
    }

    /**
     * Execute le modèle et itère à travers les lignes. Utile pour créer une table dans le code java
     * 
     * @return object
     * @throws Exception the exception
     */
    @Override
    public Object assemble() throws Exception
    {
        for (Object bean : execute())
        {
            setCurrentRowBean(bean);

            // Appel le handler de modification des attribus
            getRowHandler().modifyRowAttributes();

            // affectation dea valeurs aux colonnes
            setColumnValues();
        }

        return getViewData();
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.core.TableModel#getCollectionOfBeansFromStartToCurrentPage()
     */
    @Override
    public Collection<Object> getCollectionOfBeansFromStartToCurrentPage()
    {
        return collectionOfBeansFromStartToCurrentPage;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.core.TableModel#setCollectionOfBeansFromStartToCurrentPage(java.util.Collection)
     */
    @Override
    public void setCollectionOfBeansFromStartToCurrentPage(Collection<Object> collectionOfBeansFromStartToCurrentPage)
    {
        this.collectionOfBeansFromStartToCurrentPage = collectionOfBeansFromStartToCurrentPage;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.core.TableModel#getRowCount()
     */
    @Override
    public int getRowCount()
    {
        return rowCount;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.core.TableModel#getPageCount()
     */
    @Override
    public int getPageCount()
    {
        return pageCount;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.core.TableModel#getTotalRowCount()
     */
    @Override
    public int getTotalRowCount()
    {
        return totalRowCount;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.core.TableModel#getTotalPageCount()
     */
    @Override
    public int getTotalPageCount()
    {
        return totalPageCount;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.core.TableModel#isLastPage()
     */
    @Override
    public boolean isLastPage()
    {
        return lastPage;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.core.TableModel#isFirstPage()
     */
    @Override
    public boolean isFirstPage()
    {
        return firstPage;
    }

}
