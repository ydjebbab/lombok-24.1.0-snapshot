/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.apptags.table.cell;

import fr.gouv.finances.lombok.apptags.table.bean.Column;
import fr.gouv.finances.lombok.apptags.table.core.TableModel;
import fr.gouv.finances.lombok.apptags.util.format.FormaterNombre;

/**
 * Class AbstractMonnaieCell
 */
public abstract class AbstractMonnaieCell extends AbstractCell implements PatternCell, NumericCell
{

    /**
     * Instanciation de abstract monnaie cell.
     */
    public AbstractMonnaieCell()
    {
        super();
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.cell.NumericCell#getPrecision()
     */
    public abstract int getPrecision();

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.cell.AbstractCell#getExportDisplay(fr.gouv.finances.lombok.apptags.table.core.TableModel,
     *      fr.gouv.finances.lombok.apptags.table.bean.Column)
     */
    @Override
    public String getExportDisplay(TableModel model, Column column)
    {
        return FormaterNombre.monnaieNDecimalesPourCsv(column.getPropertyValue(), getPrecision());
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.cell.AbstractCell#getCellValue(fr.gouv.finances.lombok.apptags.table.core.TableModel,
     *      fr.gouv.finances.lombok.apptags.table.bean.Column)
     */
    @Override
    protected String getCellValue(TableModel model, Column column)
    {
        return FormaterNombre.monnaieNDecimalesPourHtml(column.getPropertyValue(), getPrecision());
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.cell.PatternCell#getLengthCelluleFormatee(fr.gouv.finances.lombok.apptags.table.core.TableModel,
     *      fr.gouv.finances.lombok.apptags.table.bean.Column)
     */
    @Override
    public int getLengthCelluleFormatee(TableModel model, Column column)
    {
        int nbChar = 0;
        if (column.getPropertyValue() != null)
        {
            nbChar = getCellValue(model, column).length();
        }
        return nbChar;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.cell.NumericCell#getCellValueFormated(java.lang.Number)
     */
    @Override
    public String getCellValueFormated(Number value)
    {
        return FormaterNombre.monnaieNDecimalesPourHtml(value, getPrecision());
    }
}
