/*
 * Copyright (c) 2014 DGFiP - Tous droits réservés
 *//*
/*Copyright (c) 2014 DGFiP - Tous droits réservés/* * Copyright 2004 original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package fr.gouv.finances.lombok.apptags.table.view;

import fr.gouv.finances.lombok.apptags.table.bean.Export;
import fr.gouv.finances.lombok.apptags.table.core.Messages;
import fr.gouv.finances.lombok.apptags.table.core.TableModel;
import fr.gouv.finances.lombok.apptags.table.view.html.BuilderConstants;
import fr.gouv.finances.lombok.apptags.table.view.html.BuilderUtils;
import fr.gouv.finances.lombok.apptags.table.view.html.TableActions;
import fr.gouv.finances.lombok.apptags.table.view.html.TableBuilder;
import fr.gouv.finances.lombok.apptags.table.view.html.ToolbarBuilder;
import fr.gouv.finances.lombok.apptags.table.view.html.TwoColumnTableLayout;
import fr.gouv.finances.lombok.apptags.util.HtmlBuilder;

/**
 * Class DefaultToolbar
 */
public class DefaultToolbar extends TwoColumnTableLayout
{

    /** messages. */
    private Messages messages;

    /**
     * Instanciation de default toolbar.
     * 
     * @param html
     * @param model
     */
    public DefaultToolbar(HtmlBuilder html, TableModel model)
    {
        super(html, model);
        messages = model.getMessages();
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.view.html.TwoColumnTableLayout#showLayout(fr.gouv.finances.lombok.apptags.table.core.TableModel)
     */
    @Override
    protected boolean showLayout(TableModel model)
    {
        boolean result = true;
        boolean showPagination = BuilderUtils.showPagination(model);
        boolean showPaginationBottom = BuilderUtils.showPaginationBottom(model);
        boolean showExports = BuilderUtils.showExports(model);
        boolean showExportsBottom = BuilderUtils.showExportsBottom(model);
        boolean showTitle = BuilderUtils.showTitle(model);
        if (!showPagination && !showExports && !showExportsBottom && !showTitle && !showPaginationBottom)
        {
            result = false;
        }

        return result;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.view.html.TwoColumnTableLayout#columnLeft(fr.gouv.finances.lombok.apptags.util.HtmlBuilder,
     *      fr.gouv.finances.lombok.apptags.table.core.TableModel)
     */
    @Override
    protected void columnLeft(HtmlBuilder html, TableModel model)
    {
        if (model.getTableHandler().getTable().getTitle() != null)
        {
            html.td(2).close();
            new TableBuilder(html, model).title();
            html.tdEnd();
        }
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.view.html.TwoColumnTableLayout#columnLeft(fr.gouv.finances.lombok.apptags.util.HtmlBuilder,
     *      fr.gouv.finances.lombok.apptags.table.core.TableModel)
     */
    @Override
    protected void columnLeftRWD(HtmlBuilder html, TableModel model)
    {
        if (model.getTableHandler().getTable().getTitle() != null)
        {
            html.div().close();
            new TableBuilder(html, model).title();
            html.divEnd();
        }
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.view.html.TwoColumnTableLayout#columnRight(fr.gouv.finances.lombok.apptags.util.HtmlBuilder,
     *      fr.gouv.finances.lombok.apptags.table.core.TableModel)
     */
    @Override
    protected void columnRight(HtmlBuilder html, TableModel model)
    {
        boolean showPagination = BuilderUtils.showPagination(model);
        boolean showPaginationBottom = BuilderUtils.showPaginationBottom(model);
        boolean showExports = BuilderUtils.showExports(model);
        boolean showExportsBottom = BuilderUtils.showExportsBottom(model);
        boolean showNewPagination = BuilderUtils.showNewPagination(model);
        boolean inPaginationBottom = BuilderUtils.inPaginationBottom(model);

        ToolbarBuilder toolbarBuilder = new ToolbarBuilder(html, model);
        // amlp 09/09 rajout if pour eviter balise tr vide
        // dans classe toolbar
        if (showPagination || showExports || showPaginationBottom || showExportsBottom)
        {
            html.td(2).styleClass("alignRight").close();

            String newPagclass = "";
            if (showNewPagination)
            {
                newPagclass = " newPag";
            }

            html.table(2).styleClass(BuilderConstants.TOOLBAR_CSS + newPagclass + " cellSpacing1").close();

            html.tr(3).close();
        }

        if ((showPagination && !inPaginationBottom) 
            || (showPaginationBottom && inPaginationBottom))
        {
            if (showNewPagination)
            {
                displayNewPagination(html, model);
            }
            else
            {
                html.td(4).close();
                toolbarBuilder.firstPageItemAsImage();
                html.tdEnd();

                html.td(4).close();
                toolbarBuilder.prevPageItemAsImage();
                html.tdEnd();

                html.td(4).close();
                toolbarBuilder.nextPageItemAsImage();
                html.tdEnd();

                html.td(4).close();
                toolbarBuilder.lastPageItemAsImage();
                html.tdEnd();

                html.td(4).close();
                toolbarBuilder.separator();
                html.tdEnd();

                html.td(4).style("width:20px").close();
                html.newline();
                html.tabs(4);
                toolbarBuilder.rowsDisplayedDroplist();
                html.img();
                html.src(BuilderUtils.getImage(model, BuilderConstants.TOOLBAR_ROWS_DISPLAYED_IMAGE));
                html.alt(messages.getMessage(BuilderConstants.TOOLBAR_ROWS_DISPLAYED_TEXT));
                html.style("border:0");
                html.xclose();
                html.tdEnd();
            }

        }

        if ((showExports && !inPaginationBottom) 
            || (inPaginationBottom && showExportsBottom))
        {
            int showExportsWithLimitSizePdf = BuilderUtils.showExportsWithLimitSizePdf(model);
            int showExportsWithLimitSizeXlsOrOds = BuilderUtils.showExportsWithLimitSizeXlsOrOds(model);
            int showExportsWithLimitSizeCsv = BuilderUtils.showExportsWithLimitSizeCsv(model);
            for (Export export : model.getExportHandler().getExports())
            {
                html.td(4).close();
                // toolbarBuilder.exportItemAsNewImage(export);
                if (((export.getImageName().toUpperCase().endsWith("PDF") == true) && model.getCollectionOfFilteredBeans().size() < showExportsWithLimitSizePdf)
                    || ((export.getImageName().toUpperCase().endsWith("XLS") == true || export.getImageName().toUpperCase().endsWith("ODS") == true) && model
                        .getCollectionOfFilteredBeans().size() < showExportsWithLimitSizeXlsOrOds)
                    || (export.getImageName().toUpperCase().endsWith("CSV") == true && model.getCollectionOfFilteredBeans().size() < showExportsWithLimitSizeCsv))

                {

                    // new graphik
                    String action = new TableActions(model).getExportAction(export.getView(), export.getFileName());
                    String img = BuilderUtils.getImage(model, export.getImageName());
                    html.append("<a href=\"" + action + "\" title='" + export.getTooltip() + "'> " +
                        "<img src='" + img + "' />" +
                        "<br/> " +
                        "<span class='btn btn-mini'>" + export.getText() + "</span>" +
                        "</a>");
                }
                else
                {
                    html.append("veuillez filtrer votre liste pour accéder aux exports " + export.getImageName());
                }

                html.tdEnd();
            }

        }
        // amlp 09/09 rajout if pour eviter balise tr vide
        // dans classe toolbar
        if (showPagination || showExports || showPaginationBottom || showExportsBottom)
        {
            html.trEnd(3);

            html.tableEnd(2);
            html.newline();
            html.tabs(2);

            html.tdEnd();
        }
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.view.html.TwoColumnTableLayout#columnRight(fr.gouv.finances.lombok.apptags.util.HtmlBuilder,
     *      fr.gouv.finances.lombok.apptags.table.core.TableModel)
     */
    @Override
    protected void columnRightRWD(HtmlBuilder html, TableModel model)
    {
        boolean showPagination = BuilderUtils.showPagination(model);
        boolean showPaginationBottom = BuilderUtils.showPaginationBottom(model);
        boolean showExports = BuilderUtils.showExports(model);
        boolean showExportsBottom = BuilderUtils.showExportsBottom(model);
        boolean showNewPagination = BuilderUtils.showNewPagination(model);
        boolean inPaginationBottom = BuilderUtils.inPaginationBottom(model);
        
        String paginationClass = BuilderUtils.getPaginationClass(model);
        String exportsClass = BuilderUtils.getExportsClass(model);

        ToolbarBuilder toolbarBuilder = new ToolbarBuilder(html, model);
        // amlp 09/09 rajout if pour eviter balise tr vide
        // dans classe toolbar
        if (showPagination || showExports ||showExportsBottom || showPaginationBottom)
        {
            html.div().styleClass(BuilderConstants.TOOLBAR_CSS + (showNewPagination ? " newPag" : "") + " toolBarRWD container-fluid")
                .close();
        }

        if ((showPagination && !inPaginationBottom) 
            || (showPaginationBottom && inPaginationBottom))
        {
            if (showNewPagination)
            {
                displayNewPaginationRWD(html, model);
            }
            else
            {
                html.ul().styleClass(paginationClass).close();

                html.li("").close();
                toolbarBuilder.firstPageItemAsImage();
                html.liEnd();

                html.li("").close();
                toolbarBuilder.prevPageItemAsImage();
                html.liEnd();

                html.li("").close();
                toolbarBuilder.nextPageItemAsImage();
                html.liEnd();

                html.li("").close();
                toolbarBuilder.lastPageItemAsImage();
                html.liEnd();

                html.li("").close();
                toolbarBuilder.separator();
                html.liEnd();

                html.li("").close();
                html.newline();
                html.tabs(4);
                toolbarBuilder.rowsDisplayedDroplist();
                html.img();
                html.src(BuilderUtils.getImage(model, BuilderConstants.TOOLBAR_ROWS_DISPLAYED_IMAGE));
                html.alt(messages.getMessage(BuilderConstants.TOOLBAR_ROWS_DISPLAYED_TEXT));
                html.style("border:0");
                html.xclose();
                html.liEnd();

                html.ulEnd();
            }

        }

        if ((showExports && !inPaginationBottom) 
            || (inPaginationBottom && showExportsBottom))
        {
            int showExportsWithLimitSizePdf = BuilderUtils.showExportsWithLimitSizePdf(model);
            int showExportsWithLimitSizeXlsOrOds = BuilderUtils.showExportsWithLimitSizeXlsOrOds(model);
            int showExportsWithLimitSizeCsv = BuilderUtils.showExportsWithLimitSizeCsv(model);

            html.ul().styleClass("exports " + exportsClass).close();

            html.li("").close();
            html.a().append(".collapseExports").dataAttributes("data-toggle=collapse").styleClass("btn").close().append("exports").aEnd();
            html.liEnd();

            for (Export export : model.getExportHandler().getExports())
            {
                html.li("").styleClass("collapseExports collapse").close();
                // toolbarBuilder.exportItemAsNewImage(export);
                if (((export.getImageName().toUpperCase().endsWith("PDF") == true) && model.getCollectionOfFilteredBeans().size() < showExportsWithLimitSizePdf)
                    || ((export.getImageName().toUpperCase().endsWith("XLS") == true || export.getImageName().toUpperCase().endsWith("ODS") == true) && model
                        .getCollectionOfFilteredBeans().size() < showExportsWithLimitSizeXlsOrOds)
                    || (export.getImageName().toUpperCase().endsWith("CSV") == true && model.getCollectionOfFilteredBeans().size() < showExportsWithLimitSizeCsv))

                {

                    // new graphik
                    String action = new TableActions(model).getExportAction(export.getView(), export.getFileName());
                    String img = BuilderUtils.getImage(model, export.getImageName());
                    html.append("<a href=\"" + action + "\" title='" + export.getTooltip() + "'> " +
                        "<img src='" + img + "' />" +
                        "<br/> " +
                        "<span class='btn'>" + export.getText() + "</span>" +
                        "</a>");
                }
                else
                {
                    html.append("veuillez filtrer votre liste pour accéder aux exports " + export.getImageName());
                }

                html.liEnd();
            }
            html.ulEnd();

        }

        if (showPagination || showExports || showExportsBottom || showPaginationBottom)
        {
            html.divEnd();
        }

    }

    /**
     * methode Display new pagination :
     * 
     * @param html
     * @param model
     */
    private void displayNewPagination(HtmlBuilder html, TableModel model)
    {

        /** liste des pages dispo. **/
        int currentpage = model.getLimit().getPage();
        int totalPages = BuilderUtils.getTotalPages(model);
        int nbpageprint = BuilderUtils.newPaginationNbPage(model);
        int minpage = 1, maxpage = 1;
        boolean spaceBefore = false, spaceAfter = false;

        html.td(4).close();

        if (totalPages == 1)
        {
            html.td(4).close();
            html.append("<span  onclick=\"" + new TableActions(model).getPageAction(1) + "\" class='btn btn-mini page' tabindex=\"0\"> 1 </span>");
            html.tdEnd();

            return;
        }
        else
        {
            html.td(4).close();
            if (currentpage != 1)
            {
                html.append("<span aria-label=\"page précédente\" onclick=\"" + new TableActions(model).getPageAction(currentpage - 1) + "\" class='btn btn-mini page'" +
                    "title='" + messages.getMessage(BuilderConstants.TOOLBAR_PREV_PAGE_TOOLTIP)
                    + "' tabindex=\"0\" > <i class='icon-chevron-left'></i></span>");
                html.td(4).close();
                html.append("<span onclick=\"" + new TableActions(model).getPageAction(1) + "\" class='btn btn-mini page' tabindex=\"0\"> 1 </span>");

            }
            else
            {
                html.append("<span class='btn btn-mini page disabled' tabindex=\"0\"> <i class='icon-chevron-left'></i></span>");
                html.tdEnd();

                html.td(4).close();
                html.append("<span class='currentpage' tabindex=\"0\">1</span>");
            }
            html.tdEnd();
        }

        /* si on a moins de "nbpageprint" à afficher */
        if (totalPages <= nbpageprint + 1)
        {
            minpage = 2;
            maxpage = totalPages;
        }
        /* ex. (current = 2) : 1 2 3 4 5 ... 10 */
        else if (currentpage <= nbpageprint)
        {
            minpage = 2;
            maxpage = nbpageprint;
            spaceAfter = true;
        }

        /* ex. (current = 6) : 1 ... 3 4 5 6 7 ... 10 */
        else if (currentpage > nbpageprint && currentpage < (totalPages - nbpageprint + 1))
        {
            // ancien code
            // Math.round n'est pas utile parce que nbpageprint est un int, une division sur un int donnera un résultat
            // en int
            // minpage = currentpage - Math.round(nbpageprint / 2);
            // maxpage = currentpage + Math.round(nbpageprint / 2);
            minpage = currentpage - (nbpageprint / 2);
            maxpage = currentpage + (nbpageprint / 2);

            spaceAfter = true;
            spaceBefore = true;
        }
        /* ex. (current = 8) : 1 ... 6 7 8 9 10 */
        else
        {
            minpage = totalPages - nbpageprint + 1;
            maxpage = totalPages;
            spaceBefore = true;
        }

        if (spaceBefore)
        {
            // affichage d'une liste déroulante avec la liste complète des pages

            html.td(4).close();
            StringBuilder   onchangeselect = new StringBuilder();
            onchangeselect.append(new TableActions(model).getPageActionFromSelect());
            html.select().styleClass("btn btn-mini page");
            html.onchange(onchangeselect.toString());
            html.close();
            for (int i = 2; i < minpage; i++)
            {
                
                html.option().value(String.valueOf(i));
                html.close();
                html.append(String.valueOf(i));
                html.optionEnd();
            }
            html.selectEnd();
            html.tdEnd(); 
        }

        for (int i = minpage; i <= maxpage; i++)
        {
            html.td(4).close();

            if (i == currentpage)
            {
                html.append("<span class='currentpage' tabindex=\"0\">" + i + "</span>");
            }
            else
            {
                html.append("<span onclick=\"" + new TableActions(model).getPageAction(i) + "\" class='btn btn-mini page' tabindex=\"0\"> " + i + "</span>");
            }

            html.tdEnd();
        }

        if (spaceAfter)
        {
            
         // affichage d'une liste déroulante avec la liste complète des pages 
            html.td(4).close();
            StringBuilder   onchangeselect = new StringBuilder();
            onchangeselect.append(new TableActions(model).getPageActionFromSelect());
            html.select().styleClass("btn btn-mini page");
            html.onchange(onchangeselect.toString());
            html.close();
            for (int i = maxpage + 1; i < totalPages; i++)
            {
                
                html.option().value(String.valueOf(i));
                html.close();
                html.append(String.valueOf(i));
                html.optionEnd();
            }
            html.selectEnd();
            html.tdEnd(); 
            html.td(4).close();
            html.append("<span onclick=\"" + new TableActions(model).getPageAction(totalPages) + "\" class='btn btn-mini page' tabindex=\"0\">" + totalPages
                + "</span>");
            html.tdEnd();
            
            
            
            
        }

        html.td(4).close();
        if (currentpage < totalPages)
        {
            html.append("<span aria-label=\"page suivante\" onclick=\"" + new TableActions(model).getPageAction(currentpage + 1) + "\" class='btn btn-mini page'" +
                "title='" + messages.getMessage(BuilderConstants.TOOLBAR_NEXT_PAGE_TOOLTIP)
                + "' tabindex=\"0\"> <i class='icon-chevron-right'></i></span>");
        }
        else
        {
            html.append("<span class='btn btn-mini page disabled'  tabindex=\"0\"> <i class='icon-chevron-right'></i></span>");
        }
        html.tdEnd();

    }

    /**
     * methode Display new pagination RWD:
     * 
     * @param html
     * @param model
     */
    private void displayNewPaginationRWD(HtmlBuilder html, TableModel model)
    {

        /** liste des pages dispo. **/
        int currentpage = model.getLimit().getPage();
        int totalPages = BuilderUtils.getTotalPages(model);
        String paginationClass = BuilderUtils.getPaginationClass(model);
        if (totalPages == 1)
        {
            html.append("<a href=\"" + new TableActions(model).getPageAction(1) + "\" class='btn page'> 1 </a>");
            return;
        }
        else
        {
            html.div().styleClass(paginationClass).close();
            html.append("<span class='left' aria-label=\"page précédente\" ><i class='icon-chevron-left'></i></span>");
            html.ul().styleClass("pageScroll ").close();

            for (int i = 1; i <= totalPages; i++)
            {
                if (i == currentpage)
                {
                    html.li("").styleClass("currentpage").close();
                    html.append("<span class='currentpage'>" + i + "</span>");
                    html.liEnd();
                }
                else
                {
                    html.li("").close();
                    html.append("<a href=\"" + new TableActions(model).getPageAction(i) + "\" class='btn page'> " + i + "</a>");
                    html.liEnd();
                }

            }
            html.ulEnd();
            html.append("<span class='right' aria-label=\"page suivante\"><i class='icon-chevron-right'></i></span>");
            html.divEnd();
        }

    }

}
