/*
/*Copyright (c) 2014 DGFiP - Tous droits réservés/* * Copyright 2004 original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package fr.gouv.finances.lombok.apptags.table.cell;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import org.apache.commons.beanutils.BeanComparator;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.collections.comparators.NullComparator;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import fr.gouv.finances.lombok.apptags.table.bean.Column;
import fr.gouv.finances.lombok.apptags.table.core.Messages;
import fr.gouv.finances.lombok.apptags.table.core.TableConstants;
import fr.gouv.finances.lombok.apptags.table.core.TableModel;
import fr.gouv.finances.lombok.apptags.table.view.html.TableActions;
import fr.gouv.finances.lombok.apptags.util.ExtremeUtils;
import fr.gouv.finances.lombok.apptags.util.HtmlBuilder;

/**
 * Cellule de filtre qui contient une liste déroulante.
 */
public class FilterDroplistCell implements Cell
{

    /** logger. */
    private static Log logger = LogFactory.getLog(FilterDroplistCell.class);

    public FilterDroplistCell()
    {
        super();        
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.cell.Cell#getExportDisplay(fr.gouv.finances.lombok.apptags.table.core.TableModel,
     *      fr.gouv.finances.lombok.apptags.table.bean.Column)
     */
    @Override
    public String getExportDisplay(TableModel model, Column column)
    {
        return null;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.cell.Cell#getHtmlDisplay(fr.gouv.finances.lombok.apptags.table.core.TableModel,
     *      fr.gouv.finances.lombok.apptags.table.bean.Column)
     */
    @Override
    public String getHtmlDisplay(TableModel model, Column column)
    {
        HtmlBuilder html = new HtmlBuilder();

        html.td(2);
        if (!column.isFilterable())
        {
                        
            StringBuilder tdName = new StringBuilder();
            tdName.append(model.getTableHandler().prefixWithTableId());
            tdName.append(TableConstants.FILTER);
            tdName.append(column.getAlias());
            tdName.append(TableConstants.TDFILTER);
            html.id(tdName.toString());
            String styleClass = "";
            if (StringUtils.isNotEmpty(column.getFilterClass()))
            {
                styleClass = column.getFilterClass();
            }
            html.ariaHidden("true");
            html.close();
            
            html.span().ariaHidden("true").close();
            html.append("");
            html.spanEnd();            
        }
        else
        {
            Collection<FilterOption> filterOptions = column.getFilterOptions();
            if (filterOptions == null || filterOptions.isEmpty())
            {
                filterOptions = getFilterDropList(model, column);
            }
            html.append(dropListHtml(model, column, filterOptions));
        }
        html.tdEnd();

        return html.toString();
    }

    /**
     * Accesseur de l attribut filter drop list.
     * 
     * @param model
     * @param column
     * @return filter drop list
     */
    @SuppressWarnings("unchecked")
    protected Collection<FilterOption> getFilterDropList(TableModel model, Column column)
    {
        List<FilterOption> droplist = new ArrayList<FilterOption>();

        Set<Object> options = new HashSet<Object>();

        Collection<Object> beans = model.getCollectionOfBeans();
        for (Object bean : beans)
        {
            try
            {
                Object obj = getFilterOption(column, bean);
                if ((obj != null) && !options.contains(obj))
                {
                    droplist.add(new Option(obj));
                    options.add(obj);
                }
            }
            catch (Exception exception)
            {
                logger.debug("Erreur lors de la construction de la liste déroulante", exception);
            }
        }

        BeanComparator comparator = new BeanComparator("label", new NullComparator());
        Collections.sort(droplist, (Comparator<? super Object>) comparator);

        return droplist;
    }

    /**
     * Accesseur de l attribut filter option.
     * 
     * @param column
     * @param bean
     * @return filter option
     * @throws Exception the exception
     */
    protected Object getFilterOption(Column column, Object bean) throws Exception
    {
        return PropertyUtils.getProperty(bean, column.getProperty());
    }

    /**
     * methode Drop list html :
     * 
     * @param model
     * @param column
     * @param droplist
     * @return string
     */
    protected String dropListHtml(TableModel model, Column column, Collection<FilterOption> droplist)
    {
        HtmlBuilder html = new HtmlBuilder();
        Messages messages = model.getMessages();
        
        StringBuilder tdName = new StringBuilder();
        tdName.append(model.getTableHandler().prefixWithTableId());
        tdName.append(TableConstants.FILTER);
        tdName.append(column.getAlias());
        tdName.append(TableConstants.TDFILTER);
        html.id(tdName.toString());
        String styleClass = "";
        if (StringUtils.isNotEmpty(column.getFilterClass()))
        {
            styleClass = column.getFilterClass();
        }
        html.close();
        
        String title = column.getTitle();
        html.newline();
        html.tabs(2);
        html.select().name(model.getTableHandler().prefixWithTableId() + TableConstants.FILTER + column.getAlias());
        html.select().id(model.getTableHandler().prefixWithTableId() + TableConstants.FILTER + column.getAlias());
        html.ariaLabel("Filtrer " + column.getTitle() + ", " + html.SignToLitteralText("< <= >= > <> = *", true));

        StringBuilder   onkeypress = new StringBuilder();
        onkeypress.append(new TableActions(model).getFilterAction());
        html.onchange(onkeypress.toString());

        html.close();

        html.newline();
        html.tabs(2);
        html.option().value("").close();
        if (column.getFilterNullOption() != null && column.getFilterNullOption() != "")
        {
            html.append(column.getFilterNullOption());
        }
        html.optionEnd();

        Locale locale = model.getLocale();

        for (FilterOption filterOption : droplist)
        {
            String value = String.valueOf(filterOption.getValue());
            String label = String.valueOf(filterOption.getLabel());

            if (column.isDate())
            {
                value = ExtremeUtils.formatDate(column.getParse(), column.getFormat(), filterOption.getValue(), locale);
            }

            html.newline();
            html.tabs(2);
            html.option().value(value);

            if (value.equals(column.getValueAsString()))
            {
                html.selected();
            }

            html.close();
            html.append(label);
            html.optionEnd();
        }

        html.newline();
        html.tabs(2);
        html.selectEnd();        

        return html.toString();
    }

    /**
     * Class Option
     */
    protected static class Option implements FilterOption
    {

        /** label. */
        private final Object label;

        /** value. */
        private final Object value;

        /**
         * Instanciation de option.
         * 
         * @param obj
         */
        public Option(Object obj)
        {
            this.label = obj;
            this.value = obj;
        }

        /**
         * Instanciation de option.
         * 
         * @param label
         * @param value
         */
        public Option(Object label, Object value)
        {
            this.label = label;
            this.value = value;
        }

        /**
         * (methode de remplacement) {@inheritDoc}
         * 
         * @see fr.gouv.finances.lombok.apptags.table.cell.FilterOption#getLabel()
         */
        @Override
        public Object getLabel()
        {
            return label;
        }

        /**
         * (methode de remplacement) {@inheritDoc}
         * 
         * @see fr.gouv.finances.lombok.apptags.table.cell.FilterOption#getValue()
         */
        @Override
        public Object getValue()
        {
            return value;
        }
    }
}
