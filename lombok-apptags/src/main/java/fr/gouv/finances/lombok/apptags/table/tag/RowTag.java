/*
/*Copyright (c) 2014 DGFiP - Tous droits réservés/* * Copyright 2004 original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package fr.gouv.finances.lombok.apptags.table.tag;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

import fr.gouv.finances.lombok.apptags.table.bean.Row;
import fr.gouv.finances.lombok.apptags.table.core.TableModel;
import fr.gouv.finances.lombok.apptags.table.interceptor.RowInterceptor;

/**
 * Class RowTag
 * 
 * jsp.tag : name="row" display-name="RowTag" body-content="JSP" description="L'élément qui maintient toutes les
 *          informations relatives à une ligne du tableau."
 */
public class RowTag extends TagSupport implements RowInterceptor
{

    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** highlight class. */
    private String highlightClass;

    /** highlight row. */
    private String highlightRow;

    /** interceptor. */
    private String interceptor;

    /** onclick. */
    private String onclick;

    /** onmouseout. */
    private String onmouseout;

    /** onmouseover. */
    private String onmouseover;

    /** style. */
    private String style;

    /** style class. */
    private String styleClass;

    /** name property show if equals last. */
    private String namePropertyShowIfEqualsLast;

    /**
     * Constructeur de la classe RowTag.java
     *
     */
    public RowTag()
    {
        super();
        
    }

    /**
     * Modificateur de l attribut name property show if equals last.
     * 
     * @param namePropertyShowIfEqualsLast le nouveau name property show if equals last
     */
    public void setNamePropertyShowIfEqualsLast(String namePropertyShowIfEqualsLast)
    {
        this.namePropertyShowIfEqualsLast = namePropertyShowIfEqualsLast;
    }

    /**
     * Modificateur de l attribut highlight class.
     * 
     * @param highlightClass le nouveau highlight class
     * jsp.attribute :  description="La classe css à utiliser pour surligner une
     *                ligne." required="false" rtexprvalue="true"
     */
    public void setHighlightClass(String highlightClass)
    {
        this.highlightClass = highlightClass;
    }

    /**
     * Modificateur de l attribut highlight row.
     * 
     * @param showHighlight le nouveau highlight row
     * jsp.attribute :  description="Indique si le mode "surlignage" des lignes est actif ou non Les valeurs possibles
     *                sont 'true' et 'false'. La valeur par défaut est 'false'" required="false" rtexprvalue="true"
     */
    public void setHighlightRow(String showHighlight)
    {
        this.highlightRow = showHighlight;
    }

    /**
     * jsp.attribute description="Le nom complet d'une classe qui implémente l'interface RowInterceptor Peut aussi
     * contenir un alias définit dans le fichier extremecomponents.properties Utilisé pour modifier ou ajouter des
     * attributs à une colonne. required="false" rtexprvalue="true"
     * 
     * @param interceptor le nouveau interceptor
     */
    public void setInterceptor(String interceptor)
    {
        this.interceptor = interceptor;
    }

    /**
     * Modificateur de l attribut onclick.
     * 
     * @param onclick le nouveau onclick
     * jsp.attribute :  description="Définit l'attribut 'onclick' de l'élément 'tr' du
     *                tableau" required="false" rtexprvalue="true"
     */
    public void setOnclick(String onclick)
    {
        this.onclick = onclick;
    }

    /**
     * Modificateur de l attribut onmouseout.
     * 
     * @param onmouseout le nouveau onmouseout
     * jsp.attribute :  description="Définit l'attribut 'onmouseout' de l'élément 'tr' du
     *                tableau" required="false" rtexprvalue="true"
     */
    public void setOnmouseout(String onmouseout)
    {
        this.onmouseout = onmouseout;
    }

    /**
     * Modificateur de l attribut onmouseover.
     * 
     * @param onmouseover le nouveau onmouseover
     * jsp.attribute :  description="Définit l'attribut 'onmouseover' de l'élément 'tr' du
     *                tableau" required="false" rtexprvalue="true"
     */
    public void setOnmouseover(String onmouseover)
    {
        this.onmouseover = onmouseover;
    }

    /**
     * Modificateur de l attribut style.
     * 
     * @param style le nouveau style
     * jsp.attribute :  description="Définit l'attribut 'style' de l'élément 'tr' du
     *                tableau." required="false" rtexprvalue="true"
     */
    public void setStyle(String style)
    {
        this.style = style;
    }

    /**
     * Modificateur de l attribut style class.
     * 
     * @param styleClass le nouveau style class
     * jsp.attribute :  description="Définit l'attribut 'class' de l'élément 'tr' du
     *                tableau." required="false" rtexprvalue="true"
     */
    public void setStyleClass(String styleClass)
    {
        this.styleClass = styleClass;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see javax.servlet.jsp.tagext.TagSupport#doStartTag()
     */
    @Override
    public int doStartTag() throws JspException
    {
        try
        {
            TableModel model = TagUtils.getModel(this);

            if (TagUtils.isIteratingBody(this))
            {
                Row row = model.getRowHandler().getRow();
                row.setOnclick(TagUtils.evaluateExpressionAsString("onclick", onclick, this, pageContext));
                row.setOnmouseout(TagUtils.evaluateExpressionAsString("onmouseout", onmouseout, this, pageContext));
                row.setOnmouseover(TagUtils.evaluateExpressionAsString("onmouseover", onmouseover, this, pageContext));

                modifyRowAttributes(model, row);
                model.getRowHandler().modifyRowAttributes();
            }
            else
            {
                Row row = new Row(model);
                row.setHighlightClass(this.highlightClass);
                row.setHighlightRow(TagUtils.evaluateExpressionAsBoolean("highlightRow", this.highlightRow, this,
                    pageContext));
                row.setInterceptor(TagUtils.evaluateExpressionAsString("interceptor", this.interceptor, this,
                    pageContext));
                row.setOnclick(TagUtils.evaluateExpressionAsString("onclick", onclick, this, pageContext));
                row.setOnmouseout(TagUtils.evaluateExpressionAsString("onmouseout", onmouseout, this, pageContext));
                row.setOnmouseover(TagUtils.evaluateExpressionAsString("onmouseover", onmouseover, this, pageContext));
                row.setStyle(TagUtils.evaluateExpressionAsString("style", style, this, pageContext));
                row.setStyleClass(TagUtils.evaluateExpressionAsString("styleClass", styleClass, this, pageContext));
                row.setNamePropertyShowIfEqualsLast(TagUtils.evaluateExpressionAsString("namePropertyShowIfEqualsLast",
                    namePropertyShowIfEqualsLast, this, pageContext));

                addRowAttributes(model, row);
                model.addRow(row);
            }
        }
        catch (Exception exception)
        {
            StringBuilder   msg = new StringBuilder();
            msg.append("Erreur :");
            throw new JspException(msg.toString(), exception);
        }
        return EVAL_BODY_INCLUDE;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.interceptor.RowInterceptor#addRowAttributes(fr.gouv.finances.lombok.apptags.table.core.TableModel,
     *      fr.gouv.finances.lombok.apptags.table.bean.Row)
     */
    @Override
    public void addRowAttributes(TableModel model, Row row)
    {
        // RAS
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.interceptor.RowInterceptor#modifyRowAttributes(fr.gouv.finances.lombok.apptags.table.core.TableModel,
     *      fr.gouv.finances.lombok.apptags.table.bean.Row)
     */
    @Override
    public void modifyRowAttributes(TableModel model, Row row)
    {
        // RAS
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see javax.servlet.jsp.tagext.TagSupport#release()
     */
    @Override
    public void release()
    {
        highlightClass = null;
        highlightRow = null;
        interceptor = null;
        onclick = null;
        onmouseout = null;
        onmouseover = null;
        style = null;
        styleClass = null;
        namePropertyShowIfEqualsLast = null;
        super.release();
    }
}
