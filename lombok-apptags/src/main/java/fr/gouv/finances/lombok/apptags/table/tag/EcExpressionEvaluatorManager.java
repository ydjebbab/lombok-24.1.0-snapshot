/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.apptags.table.tag;

/*
 * 
 * Cette classe est un patch basé sur la version 1.1.2 de taglibs standard
 * pour ne pas utiliser le cache de ELEvaluator qui provoque une fuite de mémoire (memory leak)
 * 
 * 
 * 
 * 
 */

import java.util.HashMap;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.Tag;

import org.apache.taglibs.standard.lang.jstl.Coercions;
import org.apache.taglibs.standard.lang.jstl.ELException;
import org.apache.taglibs.standard.lang.jstl.Logger;
import org.apache.taglibs.standard.lang.support.ExpressionEvaluator;

/**
 * <p>
 * A conduit to the JSTL EL. Based on...
 * </p>
 * <p>
 * An implementation of the ExpressionEvaluatorManager called for by the JSTL rev1 draft. This class is responsible for
 * delegating a request for expression evaluating to the particular, "active" ExpressionEvaluator for the given point in
 * the PageContext object passed in.
 * </p>
 * 
 * @author Shawn Bayern
 */
public class EcExpressionEvaluatorManager
{
    public static final String EVALUATOR_CLASS = "fr.gouv.finances.lombok.apptags.table.tag.EcEvaluator";

    /** name map. */
    private static HashMap nameMap = new HashMap();

    /** logger. */
    private static Logger logger = new Logger(System.out);

    // *********************************************************************
    // Public static methods

    /**
     * Invokes the evaluate() method on the "active" ExpressionEvaluator for the given pageContext.
     * 
     * @param attributeName
     * @param expression
     * @param expectedType
     * @param tag
     * @param pageContext
     * @return object
     * @throws JspException the jsp exception
     */
    public static Object evaluate(String attributeName, String expression, Class expectedType, Tag tag,
        PageContext pageContext) throws JspException
    {

        // the evaluator we'll use
        ExpressionEvaluator target = getEvaluatorByName(EVALUATOR_CLASS);

        // delegate the call
        return (target.evaluate(attributeName, expression, expectedType, tag, pageContext));
    }

    /**
     * Invokes the evaluate() method on the "active" ExpressionEvaluator for the given pageContext.
     * 
     * @param attributeName
     * @param expression
     * @param expectedType
     * @param pageContext
     * @return object
     * @throws JspException the jsp exception
     */
    public static Object evaluate(String attributeName, String expression, Class expectedType, PageContext pageContext)
        throws JspException
    {

        // the evaluator we'll use
        ExpressionEvaluator target = getEvaluatorByName(EVALUATOR_CLASS);

        // delegate the call
        return (target.evaluate(attributeName, expression, expectedType, null, pageContext));
    }

    /**
     * Gets an ExpressionEvaluator from the cache, or seeds the cache if we haven't seen a particular
     * ExpressionEvaluator before.
     * 
     * @param name
     * @return evaluator by name
     * @throws JspException the jsp exception
     */
    public static ExpressionEvaluator getEvaluatorByName(String name) throws JspException
    {

        Object oEvaluator = nameMap.get(name);
        if (oEvaluator != null)
        {
            return ((ExpressionEvaluator) oEvaluator);
        }
        try
        {
            synchronized (nameMap)
            {
                oEvaluator = nameMap.get(name);
                if (oEvaluator != null)
                {
                    return ((ExpressionEvaluator) oEvaluator);
                }
                ExpressionEvaluator e = (ExpressionEvaluator) Class.forName(name).newInstance();
                nameMap.put(name, e);
                return (e);
            }
        }
        catch (ClassCastException ex)
        {
            // just to display a better error message
            throw new JspException("invalid ExpressionEvaluator: " + ex.toString(), ex);
        }
        catch (ClassNotFoundException ex)
        {
            throw new JspException("couldn't find ExpressionEvaluator: " + ex.toString(), ex);
        }
        catch (IllegalAccessException ex)
        {
            throw new JspException("couldn't access ExpressionEvaluator: " + ex.toString(), ex);
        }
        catch (InstantiationException ex)
        {
            throw new JspException("couldn't instantiate ExpressionEvaluator: " + ex.toString(), ex);
        }
    }

    /**
     * Performs a type conversion according to the EL's rules.
     * 
     * @param value
     * @param classe
     * @return object
     * @throws JspException the jsp exception
     */
    public static Object coerce(Object value, Class classe) throws JspException
    {
        try
        {
            // just delegate the call
            return Coercions.coerce(value, classe, logger);
        }
        catch (ELException ex)
        {
            throw new JspException(ex);
        }
    }

}
