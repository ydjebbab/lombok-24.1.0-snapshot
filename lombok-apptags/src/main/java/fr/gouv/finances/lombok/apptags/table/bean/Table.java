/*
/*Copyright (c) 2014 DGFiP - Tous droits réservés/* * Copyright 2004 original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package fr.gouv.finances.lombok.apptags.table.bean;

import fr.gouv.finances.lombok.apptags.table.core.TableModel;

/**
 * Class Table
 */
public class Table extends Attributes
{

    /** model. */
    private TableModel model;

    /** action. */
    private String action;

    /** auto include parameters. */
    private Boolean autoIncludeParameters;

    /** border. */
    private String border;

    /** buffer view. */
    private Boolean bufferView;

    /** cellpadding. */
    private String cellpadding;

    /** cellspacing. */
    private String cellspacing;

    /** compact. */
    private Boolean compact;

    /** filterable. */
    private Boolean filterable;

    /** filter rows callback. */
    private String filterRowsCallback;

    /** form. */
    private String form;

    /** image path. */
    private String imagePath;

    /** interceptor. */
    private String interceptor;

    /** items. */
    private Object items;

    /** locale. */
    private String locale;

    /** max rows displayed. */
    private int maxRowsDisplayed;

    /** median rows displayed. */
    private int medianRowsDisplayed;

    /** method. */
    private String method;

    /** on invoke action. */
    private String onInvokeAction;

    /** responsive flag */
    private Boolean showResponsive;

    /** responsive data attributes */
    private String dataAttributes;

    /** responsive pagination class */
    private String paginationClass;

    /** responsive pagination class */
    private String exportsClass;

    /** retrieve rows callback. */
    private String retrieveRowsCallback;

    /** rows displayed. */
    private int rowsDisplayed;

    /** save filter sort. */
    private Boolean saveFilterSort;

    /** scope. */
    private String scope;

    /** show exports. */
    private Boolean showExports;

    /** show exports with limit size pdf. */
    private int showExportsWithLimitSizePdf;

    /** show exports with limit size xls or ods. */
    private int showExportsWithLimitSizeXlsOrOds;

    /** show exports with limit size csv. */
    private int showExportsWithLimitSizeCsv;

    /** show pagination. */
    private Boolean showPagination;

    /** show pagination bottom. */
    private Boolean showPaginationBottom;

    /** in pagination bottom. */
    private Boolean inPaginationBottom;

    /** show exports bottom. */
    private Boolean showExportsBottom;

    /** show new pagination. */
    private Boolean showNewPagination;

    /** show new filter. */
    private Boolean showNewFilter;

    /** new pagination nb page. */
    private int newPaginationNbPage;

    /** show status bar. */
    private Boolean showStatusBar;

    /** show title. */
    private Boolean showTitle;

    /** show tooltips. */
    private Boolean showTooltips;

    /** sort rows callback. */
    private String sortRowsCallback;

    /** sortable. */
    private Boolean sortable;

    /** state. */
    private String state;

    /** state attr. */
    private String stateAttr;

    /** style. */
    private String style;

    /** style class. */
    private String styleClass;

    /** table id. */
    private String tableId;

    /** title. */
    private String title;

    /** theme. */
    private String theme;

    /** var. */
    private String var;

    /** view. */
    private String view;

    /** width. */
    private String width;

    /**
     * attribut pour personnaliser champs statusbar.noResultsFound
     */

    private String libellenoresultsfound;

    /** positionnoresultsfound. */
    private String positionnoresultsfound;

    /** Attribut pour définir la première page à afficher. */
    private int startpage;

    /**
     * Instanciation de table.
     *
     * @param model
     */
    public Table(TableModel model)
    {
        this.model = model;
    }

    /**
     * methode Defaults :
     */
    public void defaults()
    {
        this.tableId = TableDefaults.getTableId(tableId); // must be first

        this.autoIncludeParameters = TableDefaults.getAutoIncludeParameters(model, autoIncludeParameters);
        this.border = TableDefaults.getBorder(model, border);
        this.bufferView = TableDefaults.isBufferView(model, bufferView);
        this.cellpadding = TableDefaults.getCellpadding(model, cellpadding);
        this.cellspacing = TableDefaults.getCellspacing(model, cellspacing);
        this.compact = TableDefaults.isCompact(model, compact);
        this.filterable = TableDefaults.isFilterable(model, filterable);
        this.filterRowsCallback = TableDefaults.getFilterRowsCallback(model, filterRowsCallback);
        this.imagePath = TableDefaults.getImagePath(model, imagePath);
        this.maxRowsDisplayed = TableDefaults.getMaxRowsDisplayed(model, maxRowsDisplayed);
        this.medianRowsDisplayed = TableDefaults.getMedianRowsDisplayed(model, medianRowsDisplayed);
        this.method = TableDefaults.getMethod(model, method);
        this.retrieveRowsCallback = TableDefaults.getRetrieveRowsCallback(model, retrieveRowsCallback);
        this.rowsDisplayed = TableDefaults.getRowsDisplayed(model, rowsDisplayed);

        this.showPagination = TableDefaults.isShowPagination(model, showPagination);
        this.showPaginationBottom = TableDefaults.isShowPaginationBottom(model, showPaginationBottom);
        this.inPaginationBottom = false;

        this.showResponsive = TableDefaults.isShowResponsive(model, showResponsive);
        this.dataAttributes = TableDefaults.getDataAttributes(model, dataAttributes);
        this.paginationClass = TableDefaults.getPaginationClass(model, paginationClass);
        this.exportsClass = TableDefaults.getExportsClass(model, exportsClass);

        this.showExportsBottom = TableDefaults.isShowExportsBottom(model, showExportsBottom);
        this.showNewPagination = TableDefaults.isShowNewPagination(model, showNewPagination);
        this.showNewFilter = TableDefaults.isShowNewFilter(model, showNewFilter);
        this.newPaginationNbPage = TableDefaults.getNewPaginationNbPage(model, newPaginationNbPage);

        this.showExportsWithLimitSizeCsv = TableDefaults.isShowExportsWithLimitSizeCsv(model, showExportsWithLimitSizeCsv);
        this.showExportsWithLimitSizePdf = TableDefaults.isShowExportsWithLimitSizePdf(model, showExportsWithLimitSizePdf);
        this.showExportsWithLimitSizeXlsOrOds = TableDefaults.isShowExportsWithLimitSizeXlsOrOds(model, showExportsWithLimitSizeXlsOrOds);

        this.showExports = TableDefaults.isShowExports(model, showExports);
        this.showStatusBar = TableDefaults.isShowStatusBar(model, showStatusBar);
        this.showTitle = TableDefaults.isShowTitle(model, showTitle);
        this.showTooltips = TableDefaults.isShowTooltips(model, showTooltips);
        this.state = TableDefaults.getState(model, state);
        this.stateAttr = TableDefaults.getStateAttr(model, stateAttr);
        this.sortable = TableDefaults.isSortable(model, sortable);
        this.sortRowsCallback = TableDefaults.getSortRowsCallback(model, sortRowsCallback);
        this.styleClass = TableDefaults.getStyleClass(model, styleClass);
        this.theme = TableDefaults.getTheme(model, theme);
        this.title = TableDefaults.getTitle(model, title);
        this.var = TableDefaults.getVar(var, tableId);
        this.view = TableDefaults.getView(view);
        this.width = TableDefaults.getWidth(model, width);
        this.libellenoresultsfound = TableDefaults.getLibellenoresultsfound(model, libellenoresultsfound);
        this.positionnoresultsfound = TableDefaults.getLibellenoresultsfound(model, positionnoresultsfound);

        this.startpage = TableDefaults.getStartpage(model, this.startpage);
    }

    /**
     * Accesseur de l attribut action.
     *
     * @return action
     */
    public String getAction()
    {
        return action;
    }

    /**
     * Accesseur de l attribut border.
     *
     * @return border
     */
    public String getBorder()
    {
        return border;
    }

    /**
     * Accesseur de l attribut cellpadding.
     *
     * @return cellpadding
     */
    public String getCellpadding()
    {
        return cellpadding;
    }

    /**
     * Accesseur de l attribut cellspacing.
     *
     * @return cellspacing
     */
    public String getCellspacing()
    {
        return cellspacing;
    }

    /**
     * Accesseur de l attribut dataAttributes.
     * 
     * @return dataAttributes
     */
    public String getDataAttributes()
    {
        return dataAttributes;
    }

    /**
     * Accesseur de l attribut exportsClass.
     * 
     * @return exportsClass
     */
    public String getExportsClass()
    {
        return exportsClass;
    }

    /**
     * Accesseur de l attribut filter rows callback.
     *
     * @return filter rows callback
     */
    public String getFilterRowsCallback()
    {
        return filterRowsCallback;
    }

    /**
     * Accesseur de l attribut form.
     *
     * @return form
     */
    public String getForm()
    {
        return form;
    }

    /**
     * Accesseur de l attribut image path.
     *
     * @return image path
     */
    public String getImagePath()
    {
        return imagePath;
    }

    /**
     * Accesseur de l attribut interceptor.
     *
     * @return interceptor
     */
    public String getInterceptor()
    {
        return interceptor;
    }

    /**
     * Accesseur de l attribut items.
     *
     * @return items
     */
    public Object getItems()
    {
        return items;
    }

    /**
     * Gets the attribut pour personnaliser champs statusbar.
     *
     * @return the attribut pour personnaliser champs statusbar
     */
    public String getLibellenoresultsfound()
    {
        return libellenoresultsfound;
    }

    /**
     * Accesseur de l attribut locale.
     *
     * @return locale
     */
    public String getLocale()
    {
        return locale;
    }

    /**
     * Accesseur de l attribut max rows displayed.
     *
     * @return max rows displayed
     */
    public int getMaxRowsDisplayed()
    {
        return maxRowsDisplayed;
    }

    /**
     * Accesseur de l attribut median rows displayed.
     *
     * @return median rows displayed
     */
    public int getMedianRowsDisplayed()
    {
        return medianRowsDisplayed;
    }

    /**
     * Accesseur de l attribut method.
     *
     * @return method
     */
    public String getMethod()
    {
        return method;
    }

    /**
     * Accesseur de l attribut new pagination nb page.
     *
     * @return new pagination nb page
     */
    public int getNewPaginationNbPage()
    {
        return newPaginationNbPage;
    }

    /**
     * Accesseur de l attribut on invoke action.
     *
     * @return on invoke action
     */
    public String getOnInvokeAction()
    {
        return onInvokeAction;
    }

    /**
     * Accesseur de l attribut paginationClass.
     * 
     * @return paginationClass
     */
    public String getPaginationClass()
    {
        return paginationClass;
    }

    /**
     * Accesseur de l attribut positionnoresultsfound.
     *
     * @return positionnoresultsfound
     */
    public String getPositionnoresultsfound()
    {
        return positionnoresultsfound;
    }

    /**
     * Accesseur de l attribut retrieve rows callback.
     *
     * @return retrieve rows callback
     */
    public String getRetrieveRowsCallback()
    {
        return retrieveRowsCallback;
    }

    /**
     * Accesseur de l attribut rows displayed.
     *
     * @return rows displayed
     */
    public int getRowsDisplayed()
    {
        return rowsDisplayed;
    }

    /**
     * Accesseur de l attribut scope.
     *
     * @return scope
     */
    public String getScope()
    {
        return scope;
    }

    /**
     * Accesseur de l attribut show exports with limit size csv.
     *
     * @return show exports with limit size csv
     */
    public int getShowExportsWithLimitSizeCsv()
    {
        return showExportsWithLimitSizeCsv;
    }

    /**
     * Accesseur de l attribut show exports with limit size pdf.
     *
     * @return show exports with limit size pdf
     */
    public int getShowExportsWithLimitSizePdf()
    {
        return showExportsWithLimitSizePdf;
    }

    /**
     * Accesseur de l attribut show exports with limit size xls or ods.
     *
     * @return show exports with limit size xls or ods
     */
    public int getShowExportsWithLimitSizeXlsOrOds()
    {
        return showExportsWithLimitSizeXlsOrOds;
    }

    /**
     * Accesseur de l attribut sort rows callback.
     *
     * @return sort rows callback
     */
    public String getSortRowsCallback()
    {
        return sortRowsCallback;
    }

    /**
     * Gets the attribut pour définir la première page à afficher.
     *
     * @return the attribut pour définir la première page à afficher
     */
    public int getStartpage()
    {
        return startpage;
    }

    /**
     * Accesseur de l attribut state.
     *
     * @return state
     */
    public String getState()
    {
        return state;
    }

    /**
     * Accesseur de l attribut state attr.
     *
     * @return state attr
     */
    public String getStateAttr()
    {
        return stateAttr;
    }

    /**
     * Accesseur de l attribut style.
     *
     * @return style
     */
    public String getStyle()
    {
        return style;
    }

    /**
     * Accesseur de l attribut style class.
     *
     * @return style class
     */
    public String getStyleClass()
    {
        return styleClass;
    }

    /**
     * Accesseur de l attribut table id.
     *
     * @return table id
     */
    public String getTableId()
    {
        return tableId;
    }

    /**
     * Accesseur de l attribut theme.
     *
     * @return theme
     */
    public String getTheme()
    {
        return theme;
    }

    /**
     * Accesseur de l attribut title.
     *
     * @return title
     */
    public String getTitle()
    {
        return title;
    }

    /**
     * Accesseur de l attribut var.
     *
     * @return var
     */
    public String getVar()
    {
        return var;
    }

    /**
     * Accesseur de l attribut view.
     *
     * @return view
     */
    public String getView()
    {
        return view;
    }

    /**
     * Accesseur de l attribut width.
     *
     * @return width
     */
    public String getWidth()
    {
        return width;
    }

    /**
     * Verifie si auto include parameters.
     *
     * @return true, si c'est auto include parameters
     */
    public boolean isAutoIncludeParameters()
    {
        return autoIncludeParameters.booleanValue();
    }

    /**
     * Verifie si buffer view.
     *
     * @return true, si c'est buffer view
     */
    public boolean isBufferView()
    {
        return bufferView.booleanValue();
    }

    /**
     * Verifie si compact.
     *
     * @return true, si c'est compact
     */
    public boolean isCompact()
    {
        return compact.booleanValue();
    }

    /**
     * Verifie si filterable.
     *
     * @return true, si c'est filterable
     */
    public boolean isFilterable()
    {
        return filterable.booleanValue();
    }

    /**
     * methode Checks if is in pagination bottom :
     *
     * @return boolean
     */
    public Boolean isInPaginationBottom()
    {
        return inPaginationBottom;
    }

    /**
     * Verifie si save filter sort.
     *
     * @return true, si c'est save filter sort
     */
    public boolean isSaveFilterSort()
    {
        return saveFilterSort.booleanValue();
    }

    /**
     * Verifie si show exports.
     *
     * @return true, si c'est show exports
     */
    public boolean isShowExports()
    {
        return showExports.booleanValue();
    }

    /**
     * methode Checks if is show exports bottom :
     *
     * @return boolean
     */
    public Boolean isShowExportsBottom()
    {
        return showExportsBottom;
    }

    /**
     * methode Checks if is show new filter :
     *
     * @return boolean
     */
    public Boolean isShowNewFilter()
    {
        return showNewFilter;
    }

    /**
     * methode Checks if is show new pagination :
     *
     * @return boolean
     */
    public Boolean isShowNewPagination()
    {
        return showNewPagination;
    }

    /**
     * Verifie si show pagination.
     *
     * @return true, si c'est show pagination
     */
    public boolean isShowPagination()
    {
        return showPagination.booleanValue();
    }

    /**
     * methode Checks if is show pagination bottom :
     *
     * @return boolean
     */
    public Boolean isShowPaginationBottom()
    {
        return showPaginationBottom;
    }

    /**
     * Accesseur de l attribut responsive.
     *
     * @return responsive (true or false)
     */
    public Boolean isShowResponsive()
    {
        return showResponsive;
    }

    /**
     * Verifie si show status bar.
     *
     * @return true, si c'est show status bar
     */
    public boolean isShowStatusBar()
    {
        return showStatusBar.booleanValue();
    }

    /**
     * Verifie si show title.
     *
     * @return true, si c'est show title
     */
    public boolean isShowTitle()
    {
        return showTitle.booleanValue();
    }

    /**
     * Verifie si show tooltips.
     *
     * @return true, si c'est show tooltips
     */
    public boolean isShowTooltips()
    {
        return showTooltips.booleanValue();
    }

    /**
     * Verifie si sortable.
     *
     * @return true, si c'est sortable
     */
    public boolean isSortable()
    {
        return sortable.booleanValue();
    }

    /**
     * Modificateur de l attribut action.
     *
     * @param action le nouveau action
     */
    public void setAction(String action)
    {
        this.action = action;
    }

    /**
     * Modificateur de l attribut auto include parameters.
     *
     * @param autoIncludeParameters le nouveau auto include parameters
     */
    public void setAutoIncludeParameters(Boolean autoIncludeParameters)
    {
        this.autoIncludeParameters = autoIncludeParameters;
    }

    /**
     * Modificateur de l attribut border.
     *
     * @param border le nouveau border
     */
    public void setBorder(String border)
    {
        this.border = border;
    }

    /**
     * Modificateur de l attribut buffer view.
     *
     * @param bufferView le nouveau buffer view
     */
    public void setBufferView(Boolean bufferView)
    {
        this.bufferView = bufferView;
    }

    /**
     * Modificateur de l attribut cellpadding.
     *
     * @param cellpadding le nouveau cellpadding
     */
    public void setCellpadding(String cellpadding)
    {
        this.cellpadding = cellpadding;
    }

    /**
     * Modificateur de l attribut cellspacing.
     *
     * @param cellspacing le nouveau cellspacing
     */
    public void setCellspacing(String cellspacing)
    {
        this.cellspacing = cellspacing;
    }

    /**
     * Modificateur de l attribut compact.
     *
     * @param compact le nouveau compact
     */
    public void setCompact(Boolean compact)
    {
        this.compact = compact;
    }

    /**
     * Modificateur de l attribut dataAttributes.
     * 
     * @param dataAttributes
     */
    public void setDataAttributes(String dataAttributes)
    {
        this.dataAttributes = dataAttributes;
    }

    /**
     * Modificateur de l attribut exportsClass.
     *
     * @param exportsClass
     */
    public void setExportsClass(String exportsClass)
    {
        this.exportsClass = exportsClass;
    }

    /**
     * Modificateur de l attribut filterable.
     *
     * @param filterable le nouveau filterable
     */
    public void setFilterable(Boolean filterable)
    {
        this.filterable = filterable;
    }

    /**
     * Modificateur de l attribut filter rows callback.
     *
     * @param filterRowsCallback le nouveau filter rows callback
     */
    public void setFilterRowsCallback(String filterRowsCallback)
    {
        this.filterRowsCallback = filterRowsCallback;
    }

    /**
     * Modificateur de l attribut form.
     *
     * @param form le nouveau form
     */
    public void setForm(String form)
    {
        this.form = form;
    }

    /**
     * Modificateur de l attribut image path.
     *
     * @param imagePath le nouveau image path
     */
    public void setImagePath(String imagePath)
    {
        this.imagePath = imagePath;
    }

    /**
     * Modificateur de l attribut in pagination bottom.
     *
     * @param inPaginationBottom le nouveau in pagination bottom
     */
    public void setInPaginationBottom(Boolean inPaginationBottom)
    {
        this.inPaginationBottom = inPaginationBottom;
    }

    /**
     * Modificateur de l attribut interceptor.
     *
     * @param interceptor le nouveau interceptor
     */
    public void setInterceptor(String interceptor)
    {
        this.interceptor = interceptor;
    }

    /**
     * Modificateur de l attribut items.
     *
     * @param items le nouveau items
     */
    public void setItems(Object items)
    {
        this.items = items;
    }

    /**
     * Sets the attribut pour personnaliser champs statusbar.
     *
     * @param libellenoresultsfound the new attribut pour personnaliser champs statusbar
     */
    public void setLibellenoresultsfound(String libellenoresultsfound)
    {
        this.libellenoresultsfound = libellenoresultsfound;
    }

    /**
     * Modificateur de l attribut locale.
     *
     * @param locale le nouveau locale
     */
    public void setLocale(String locale)
    {
        this.locale = locale;
    }

    /**
     * Modificateur de l attribut max rows displayed.
     *
     * @param maxRowsDisplayed le nouveau max rows displayed
     */
    public void setMaxRowsDisplayed(int maxRowsDisplayed)
    {
        this.maxRowsDisplayed = maxRowsDisplayed;
    }

    /**
     * Modificateur de l attribut median rows displayed.
     *
     * @param medianRowsDisplayed le nouveau median rows displayed
     */
    public void setMedianRowsDisplayed(int medianRowsDisplayed)
    {
        this.medianRowsDisplayed = medianRowsDisplayed;
    }

    /**
     * Modificateur de l attribut method.
     *
     * @param method le nouveau method
     */
    public void setMethod(String method)
    {
        this.method = method;
    }

    /**
     * Modificateur de l attribut new pagination nb page.
     *
     * @param newPaginationNbPage le nouveau new pagination nb page
     */
    public void setNewPaginationNbPage(int newPaginationNbPage)
    {
        this.newPaginationNbPage = newPaginationNbPage;
    }

    /**
     * Modificateur de l attribut on invoke action.
     *
     * @param onInvokeAction le nouveau on invoke action
     */
    public void setOnInvokeAction(String onInvokeAction)
    {
        this.onInvokeAction = onInvokeAction;
    }

    /**
     * Modificateur de l attribut paginationClass.
     *
     * @param paginationClass
     */
    public void setPaginationClass(String paginationClass)
    {
        this.paginationClass = paginationClass;
    }

    /**
     * Modificateur de l attribut positionnoresultsfound.
     *
     * @param positionnoresultsfound le nouveau positionnoresultsfound
     */
    public void setPositionnoresultsfound(String positionnoresultsfound)
    {
        this.positionnoresultsfound = positionnoresultsfound;
    }

    /**
     * Modificateur de l attribut retrieve rows callback.
     *
     * @param retrieveRowsCallback le nouveau retrieve rows callback
     */
    public void setRetrieveRowsCallback(String retrieveRowsCallback)
    {
        this.retrieveRowsCallback = retrieveRowsCallback;
    }

    /**
     * Modificateur de l attribut rows displayed.
     *
     * @param rowsDisplayed le nouveau rows displayed
     */
    public void setRowsDisplayed(int rowsDisplayed)
    {
        this.rowsDisplayed = rowsDisplayed;
    }

    /**
     * Modificateur de l attribut save filter sort.
     *
     * @param saveFilterSort le nouveau save filter sort
     */
    public void setSaveFilterSort(Boolean saveFilterSort)
    {
        this.saveFilterSort = saveFilterSort;
    }

    /**
     * Modificateur de l attribut scope.
     *
     * @param scope le nouveau scope
     */
    public void setScope(String scope)
    {
        this.scope = scope;
    }

    /**
     * Modificateur de l attribut show exports.
     *
     * @param showExports le nouveau show exports
     */
    public void setShowExports(Boolean showExports)
    {
        this.showExports = showExports;
    }

    /**
     * Modificateur de l attribut show exports bottom.
     *
     * @param showExportsBottom le nouveau show exports bottom
     */
    public void setShowExportsBottom(Boolean showExportsBottom)
    {
        this.showExportsBottom = showExportsBottom;
    }

    /**
     * Modificateur de l attribut show exports with limit size csv.
     *
     * @param showExportsWithLimitSizeCsv le nouveau show exports with limit size csv
     */
    public void setShowExportsWithLimitSizeCsv(int showExportsWithLimitSizeCsv)
    {
        this.showExportsWithLimitSizeCsv = showExportsWithLimitSizeCsv;
    }

    /**
     * Modificateur de l attribut show exports with limit size pdf.
     *
     * @param showExportsWithLimitSizePdf le nouveau show exports with limit size pdf
     */
    public void setShowExportsWithLimitSizePdf(int showExportsWithLimitSizePdf)
    {
        this.showExportsWithLimitSizePdf = showExportsWithLimitSizePdf;
    }

    /**
     * Modificateur de l attribut show exports with limit size xls or ods.
     *
     * @param showExportsWithLimitSizeXlsOrOds le nouveau show exports with limit size xls or ods
     */
    public void setShowExportsWithLimitSizeXlsOrOds(int showExportsWithLimitSizeXlsOrOds)
    {
        this.showExportsWithLimitSizeXlsOrOds = showExportsWithLimitSizeXlsOrOds;
    }

    /**
     * Modificateur de l attribut show new filter.
     *
     * @param showNewFilter le nouveau show new filter
     */
    public void setShowNewFilter(Boolean showNewFilter)
    {
        this.showNewFilter = showNewFilter;
    }

    /**
     * Modificateur de l attribut show new pagination.
     *
     * @param showNewPagination le nouveau show new pagination
     */
    public void setShowNewPagination(Boolean showNewPagination)
    {
        this.showNewPagination = showNewPagination;
    }

    /**
     * Modificateur de l attribut show pagination.
     *
     * @param showPagination le nouveau show pagination
     */
    public void setShowPagination(Boolean showPagination)
    {
        this.showPagination = showPagination;
    }

    /**
     * Modificateur de l attribut show pagination bottom.
     *
     * @param showPaginationBottom le nouveau show pagination bottom
     */
    public void setShowPaginationBottom(Boolean showPaginationBottom)
    {
        this.showPaginationBottom = showPaginationBottom;
    }

    /**
     * Modificateur de l attribut responsive.
     * 
     * @param showResponsive (true or false)
     */
    public void setShowResponsive(Boolean showResponsive)
    {
        this.showResponsive = showResponsive;
    }

    /**
     * Modificateur de l attribut show status bar.
     *
     * @param showStatusBar le nouveau show status bar
     */
    public void setShowStatusBar(Boolean showStatusBar)
    {
        this.showStatusBar = showStatusBar;
    }

    /**
     * Modificateur de l attribut show title.
     *
     * @param showTitle le nouveau show title
     */
    public void setShowTitle(Boolean showTitle)
    {
        this.showTitle = showTitle;
    }

    /**
     * Modificateur de l attribut show tooltips.
     *
     * @param showTooltips le nouveau show tooltips
     */
    public void setShowTooltips(Boolean showTooltips)
    {
        this.showTooltips = showTooltips;
    }

    /**
     * Modificateur de l attribut sortable.
     *
     * @param sortable le nouveau sortable
     */
    public void setSortable(Boolean sortable)
    {
        this.sortable = sortable;
    }

    /**
     * Modificateur de l attribut sort rows callback.
     *
     * @param sortRowsCallback le nouveau sort rows callback
     */
    public void setSortRowsCallback(String sortRowsCallback)
    {
        this.sortRowsCallback = sortRowsCallback;
    }

    /**
     * Sets the attribut pour définir la première page à afficher.
     *
     * @param startpage the new attribut pour définir la première page à afficher
     */
    public void setStartpage(int startpage)
    {
        this.startpage = startpage;
    }

    /**
     * Modificateur de l attribut state.
     *
     * @param state le nouveau state
     */
    public void setState(String state)
    {
        this.state = state;
    }

    /**
     * Modificateur de l attribut state attr.
     *
     * @param stateAttr le nouveau state attr
     */
    public void setStateAttr(String stateAttr)
    {
        this.stateAttr = stateAttr;
    }

    /**
     * Modificateur de l attribut style.
     *
     * @param style le nouveau style
     */
    public void setStyle(String style)
    {
        this.style = style;
    }

    /**
     * Modificateur de l attribut style class.
     * 
     * @param styleClass le nouveau style class
     */
    public void setStyleClass(String styleClass)
    {
        this.styleClass = styleClass;
    }

    /**
     * Modificateur de l attribut table id.
     * 
     * @param tableId le nouveau table id
     */
    public void setTableId(String tableId)
    {
        this.tableId = tableId;
    }

    /**
     * Modificateur de l attribut theme.
     * 
     * @param theme le nouveau theme
     */
    public void setTheme(String theme)
    {
        this.theme = theme;
    }

    /**
     * Modificateur de l attribut title.
     * 
     * @param title le nouveau title
     */
    public void setTitle(String title)
    {
        this.title = title;
    }

    /**
     * Modificateur de l attribut var.
     * 
     * @param var le nouveau var
     */
    public void setVar(String var)
    {
        this.var = var;
    }

    /**
     * Modificateur de l attribut view.
     * 
     * @param view le nouveau view
     */
    public void setView(String view)
    {
        this.view = view;
    }

    /**
     * Modificateur de l attribut width.
     * 
     * @param width le nouveau width
     */
    public void setWidth(String width)
    {
        this.width = width;
    }

}