/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.apptags.table.view.jrxmlinfo;

import java.awt.font.FontRenderContext;
import java.awt.font.LineBreakMeasurer;
import java.awt.font.TextLayout;
import java.text.AttributedCharacterIterator;
import java.text.AttributedString;
import java.util.StringTokenizer;

import net.sf.jasperreports.engine.JRCommonText;
import net.sf.jasperreports.engine.JRTextElement;
import net.sf.jasperreports.engine.export.AwtTextRenderer;
import net.sf.jasperreports.engine.util.JRStyledText;
import net.sf.jasperreports.engine.util.MaxFontSizeFinder;

//import net.sf.jasperreports.engine.export.TextRenderer;

/**
 * Adaptation de la classe TextMeasurer contenue dans jasperreports permettant d'évaluer le nombre de lignes et la
 * hauteur d'un paragraphe en fonction de son contenu.
 * 
 * @author wpetit-cp
 */
public class ParagraphMeasurer
{

    /** Constant : FONT_RENDER_CONTEXT. */
    private static final FontRenderContext FONT_RENDER_CONTEXT = AwtTextRenderer.LINE_BREAK_FONT_RENDER_CONTEXT;

    /** text element. */
    private JRTextElement textElement = null;

    /** max font size finder. */
    private MaxFontSizeFinder maxFontSizeFinder = null;

    /** width. */
    private int width = 0;

    /** height. */
    private int height = 0;

    /** top padding. */
    private int topPadding = 0;

    /** left padding. */
    private int leftPadding = 0;

    /** bottom padding. */
    private int bottomPadding = 0;

    /** right padding. */
    private int rightPadding = 0;

    /** line spacing. */
    private float lineSpacing = 0;

    /** format width. */
    private float formatWidth = 0;

    /** max height. */
    private int maxHeight = 0;

    /** text offset. */
    private int textOffset = 0;

    /** lines. */
    private int lines = 0;

    /** font size sum. */
    private int fontSizeSum = 0;

    /** first line max font size. */
    private int firstLineMaxFontSize = 0;

    /** text height. */
    private float textHeight = 0;

    /** first line leading. */
    private float firstLineLeading = 0;

    /** is left to right. */
    private boolean isLeftToRight = true;

    /** is max height reached. */
    private boolean isMaxHeightReached = false;

    /**
     * Instanciation de paragraph measurer.
     * 
     * @param textElement
     */
    public ParagraphMeasurer(JRTextElement textElement)
    {
        this.textElement = textElement;
    }

    /**
     * Verifie si max height reached.
     * 
     * @return true, si c'est max height reached
     */
    public boolean isMaxHeightReached()
    {
        return isMaxHeightReached;
    }

    /**
     * methode Initialize :
     * 
     * @param availableStretchHeight
     */
    private void initialize(int availableStretchHeight)
    {
        width = textElement.getWidth();
        height = textElement.getHeight();

        topPadding = textElement.getLineBox().getTopPadding();
        leftPadding = textElement.getLineBox().getLeftPadding();
        bottomPadding = textElement.getLineBox().getBottomPadding();
        rightPadding = textElement.getLineBox().getRightPadding();

        switch (textElement.getRotationValue())
        {
            case LEFT:
            {
                width = textElement.getHeight();
                height = textElement.getWidth();
                int tmpPadding = topPadding;
                topPadding = leftPadding;
                leftPadding = bottomPadding;
                bottomPadding = rightPadding;
                rightPadding = tmpPadding;
                break;
            }
            case RIGHT:
            {
                width = textElement.getHeight();
                height = textElement.getWidth();
                int tmpPadding = topPadding;
                topPadding = rightPadding;
                rightPadding = bottomPadding;
                bottomPadding = leftPadding;
                leftPadding = tmpPadding;
                break;
            }
            case UPSIDE_DOWN:
            {
                int tmpPadding = topPadding;
                topPadding = bottomPadding;
                bottomPadding = tmpPadding;
                tmpPadding = leftPadding;
                leftPadding = rightPadding;
                rightPadding = tmpPadding;
                break;
            }
            case NONE:
            default:
            { // mon commentaire
            }
        }

        /*   */
        switch (textElement.getParagraph().getLineSpacing())
        {
            case SINGLE:
            {
                lineSpacing = 1f;
                break;
            }
            case ONE_AND_HALF:
            {
                lineSpacing = 1.5f;
                break;
            }
            case DOUBLE:
            {
                lineSpacing = 2f;
                break;
            }
            default:
            {
                lineSpacing = 1f;
            }
        }

        maxFontSizeFinder = MaxFontSizeFinder.getInstance(!JRCommonText.MARKUP_NONE.equals(textElement.getMarkup()));

        formatWidth = width - leftPadding - rightPadding;
        formatWidth = formatWidth < 0 ? 0 : formatWidth;
        maxHeight = height + availableStretchHeight - topPadding - bottomPadding;
        maxHeight = maxHeight < 0 ? 0 : maxHeight;
        textOffset = 0;
        lines = 0;
        fontSizeSum = 0;
        firstLineMaxFontSize = 0;
        textHeight = 0;
        firstLineLeading = 0;
        isLeftToRight = true;
        isMaxHeightReached = false;

    }

    /**
     * methode Measure :
     * 
     * @param styledText
     * @param remainingText
     * @param remainingTextStart
     * @param availableStretchHeight
     * @return int
     */
    public int measure(JRStyledText styledText, String remainingText, int remainingTextStart, int availableStretchHeight)
    {
        /*   */
        initialize(availableStretchHeight);

        AttributedCharacterIterator allParagraphs = styledText.getAttributedString().getIterator();

        int tokenPosition = remainingTextStart;
        int lastParagraphStart = remainingTextStart;
        String lastParagraphText = null;

        StringTokenizer tkzer = new StringTokenizer(remainingText, "\n", true);

        while (tkzer.hasMoreTokens() && !isMaxHeightReached)
        {
            String token = tkzer.nextToken();

            if ("\n".equals(token))
            {
                renderParagraph(allParagraphs, lastParagraphStart, lastParagraphText);

                lastParagraphStart = tokenPosition + (tkzer.hasMoreTokens() || tokenPosition == 0 ? 1 : 0);
                lastParagraphText = null;
            }
            else
            {
                lastParagraphStart = tokenPosition;
                lastParagraphText = token;
            }

            tokenPosition += token.length();
        }

        if (!isMaxHeightReached && lastParagraphStart < remainingTextStart + remainingText.length())
        {
            renderParagraph(allParagraphs, lastParagraphStart, lastParagraphText);
        }

        return (int) ((((float) (this.lines)) * this.firstLineMaxFontSize * this.lineSpacing) + this.firstLineLeading)
                + this.bottomPadding + this.topPadding;
    }

    /**
     * methode mesure la hauteur du paragraphe sans tenir compte de la hauteur maximum autorisée.
     * 
     * @param styledText styledText
     * @param remainingText remainingText
     * @param remainingTextStart remainingTextStart
     * @param availableStretchHeight availableStretchHeight
     * @return int
     */
    public int measureSansMaxHeightReached(JRStyledText styledText, String remainingText, int remainingTextStart, int availableStretchHeight)
    {
        /*   */
        initialize(availableStretchHeight);

        AttributedCharacterIterator allParagraphs = styledText.getAttributedString().getIterator();

        int tokenPosition = remainingTextStart;
        int lastParagraphStart = remainingTextStart;
        String lastParagraphText = null;

        StringTokenizer tkzer = new StringTokenizer(remainingText, "\n", true);

        while (tkzer.hasMoreTokens())
        {
            String token = tkzer.nextToken();

            if ("\n".equals(token))
            {
                renderParagraphSansMaxHeightReached(allParagraphs, lastParagraphStart, lastParagraphText);

                lastParagraphStart = tokenPosition + (tkzer.hasMoreTokens() || tokenPosition == 0 ? 1 : 0);
                lastParagraphText = null;
            }
            else
            {
                lastParagraphStart = tokenPosition;
                lastParagraphText = token;
            }

            tokenPosition += token.length();
        }

        if (lastParagraphStart < remainingTextStart + remainingText.length())
        {
            renderParagraphSansMaxHeightReached(allParagraphs, lastParagraphStart, lastParagraphText);
        }

        return (int) ((((float) (this.lines)) * (this.firstLineMaxFontSize + this.lineSpacing)) + this.firstLineLeading)
                + this.bottomPadding + this.topPadding;
    }

    /**
     * methode Render paragraph :
     * 
     * @param allParagraphs
     * @param lastParagraphStart
     * @param lastParagraphText
     */
    private void renderParagraph(AttributedCharacterIterator allParagraphs, int lastParagraphStart,
        String lastParagraphText)
    {
        AttributedCharacterIterator paragraph = null;

        if (lastParagraphText == null)
        {
            paragraph =
                new AttributedString(" ", new AttributedString(allParagraphs, lastParagraphStart,
                    lastParagraphStart + 1).getIterator().getAttributes()).getIterator();
        }
        else
        {
            paragraph =
                new AttributedString(allParagraphs, lastParagraphStart, lastParagraphStart + lastParagraphText.length())
                    .getIterator();
        }

        int positionWithinParagraph = 0;

        LineBreakMeasurer lineMeasurer = new LineBreakMeasurer(paragraph, FONT_RENDER_CONTEXT);

        while (lineMeasurer.getPosition() < paragraph.getEndIndex() && !isMaxHeightReached)
        {
            int lineStartPosition = lineMeasurer.getPosition();

            TextLayout layout = lineMeasurer.nextLayout(formatWidth);

            isLeftToRight = isLeftToRight && layout.isLeftToRight();

            textHeight += layout.getLeading() + lineSpacing * layout.getAscent();

            if (textHeight + layout.getDescent() <= maxHeight)
            {
                lines++;

                fontSizeSum +=
                    maxFontSizeFinder.findMaxFontSize(new AttributedString(paragraph, lineStartPosition,
                        lineStartPosition + layout.getCharacterCount()).getIterator(), textElement.getFontSize());

                if (lines == 1)
                {
                    firstLineLeading = textHeight;
                    firstLineMaxFontSize = fontSizeSum;
                }

                positionWithinParagraph = lineMeasurer.getPosition();
                // here is the Y offset where we would draw the
                // line
                // lastDrawPosY = drawPosY;
                //
                textHeight += layout.getDescent();
            }
            else
            {
                textHeight -= layout.getLeading() + lineSpacing * layout.getAscent();
                isMaxHeightReached = true;
            }
        }

        textOffset = lastParagraphStart + positionWithinParagraph;

    }

    /**
     * methode Render paragraph sans max height reached.
     * 
     * @param allParagraphs allParagraphs
     * @param lastParagraphStart lastParagraphStart
     * @param lastParagraphText lastParagraphText
     */
    private void renderParagraphSansMaxHeightReached(AttributedCharacterIterator allParagraphs, int lastParagraphStart,
        String lastParagraphText)
    {
        AttributedCharacterIterator paragraph = null;

        if (lastParagraphText == null)
        {
            paragraph =
                new AttributedString(" ", new AttributedString(allParagraphs, lastParagraphStart,
                    lastParagraphStart + 1).getIterator().getAttributes()).getIterator();
        }
        else
        {
            paragraph =
                new AttributedString(allParagraphs, lastParagraphStart, lastParagraphStart + lastParagraphText.length())
                    .getIterator();
        }

        int positionWithinParagraph = 0;

        LineBreakMeasurer lineMeasurer = new LineBreakMeasurer(paragraph, FONT_RENDER_CONTEXT);

        while (lineMeasurer.getPosition() < paragraph.getEndIndex())
        {
            int lineStartPosition = lineMeasurer.getPosition();

            TextLayout layout = lineMeasurer.nextLayout(formatWidth);

            isLeftToRight = isLeftToRight && layout.isLeftToRight();

            textHeight += layout.getLeading() + lineSpacing * layout.getAscent();

            lines++;

            fontSizeSum +=
                maxFontSizeFinder.findMaxFontSize(new AttributedString(paragraph, lineStartPosition,
                    lineStartPosition + layout.getCharacterCount()).getIterator(), textElement.getFontSize());

            if (lines == 1)
            {
                firstLineLeading = textHeight;
                firstLineMaxFontSize = fontSizeSum;
            }

            positionWithinParagraph = lineMeasurer.getPosition();
            // here is the Y offset where we would draw the
            // line
            // lastDrawPosY = drawPosY;
            //
            textHeight += layout.getDescent();

        }

        textOffset = lastParagraphStart + positionWithinParagraph;

    }

    /**
     * Verifie si left to right.
     * 
     * @return true, si c'est left to right
     */
    protected boolean isLeftToRight()
    {
        return isLeftToRight;
    }

    /**
     * Accesseur de l attribut text offset.
     * 
     * @return text offset
     */
    protected int getTextOffset()
    {
        return textOffset;
    }

    /**
     * Accesseur de l attribut text height.
     * 
     * @return text height
     */
    public float getTextHeight()
    {
        return textHeight;
    }

    /**
     * Accesseur de l attribut line spacing factor.
     * 
     * @return line spacing factor
     */
    public float getLineSpacingFactor()
    {
        if (lines > 0)
        {
            return textHeight / fontSizeSum;
        }
        return 0;
    }

    /**
     * Accesseur de l attribut leading offset.
     * 
     * @return leading offset
     */
    public float getLeadingOffset()
    {
        return firstLineLeading - firstLineMaxFontSize * getLineSpacingFactor();
    }

}
