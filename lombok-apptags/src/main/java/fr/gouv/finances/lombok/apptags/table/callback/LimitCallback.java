/*
/*Copyright (c) 2014 DGFiP - Tous droits réservés/* * Copyright 2004 original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package fr.gouv.finances.lombok.apptags.table.callback;

import java.util.Collection;

import fr.gouv.finances.lombok.apptags.table.bean.Table;
import fr.gouv.finances.lombok.apptags.table.core.RetrievalUtils;
import fr.gouv.finances.lombok.apptags.table.core.TableConstants;
import fr.gouv.finances.lombok.apptags.table.core.TableModel;

/**
 * Gestion de la pagination.
 */
public final class LimitCallback implements RetrieveRowsCallback, FilterRowsCallback, SortRowsCallback
{

    /** Constant : TOTAL_ROWS. */
    public static final String TOTAL_ROWS = "totalRows";

    /**
     * Constructeur de la classe LimitCallback.java
     *
     */
    public LimitCallback()
    {
        super();
        
    }

    /**
     * Retourne la Collection à partir du scope utilisé par le tableau. Requiert que la variable "totalRows" soit passée
     * dans un des contexte (page , request, session, application). La variable totalRows contient un Integer
     * 
     * @param model
     * @return collection
     * @throws Exception the exception
     */
    @Override
    public Collection<Object> retrieveRows(TableModel model) throws Exception
    {
        Table table = model.getTableHandler().getTable();
        Collection<Object> rows =
            RetrievalUtils.retrieveCollection(model.getContext(), table.getItems(), table.getScope());

        Object totalRows = RetrievalUtils.retrieve(model.getContext(), TableConstants.TOTAL_ROWS);
        if (totalRows == null)
        {
            totalRows =
                RetrievalUtils.retrieve(model.getContext(), model.getTableHandler().prefixWithTableId()
                    + TableConstants.TOTAL_ROWS);
        }

        if (totalRows instanceof Integer)
        {
            model.getTableHandler().setTotalRows((Integer) totalRows);
        }
        else
        {
            StringBuilder   msg = new StringBuilder();
            msg.append("La variable ");
            msg.append(TableConstants.TOTAL_ROWS);
            msg.append("doit être de type Integer pour utiliser la classe ");
            msg.append(this.getClass().getName());
            throw new Exception(msg.toString());
        }

        return rows;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.callback.FilterRowsCallback#filterRows(fr.gouv.finances.lombok.apptags.table.core.TableModel,
     *      java.util.Collection)
     */
    @Override
    public Collection<Object> filterRows(TableModel model, Collection<Object> rows) throws Exception
    {
        return rows;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.callback.SortRowsCallback#sortRows(fr.gouv.finances.lombok.apptags.table.core.TableModel,
     *      java.util.Collection)
     */
    @Override
    public Collection<Object> sortRows(TableModel model, Collection<Object> rows) throws Exception
    {
        return rows;
    }
}
