/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.apptags.table.filter;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

/**
 * Class CpExportFilter
 */
public class CpExportFilter extends CpAbstractExportFilter
{

    /** response headers set before do filter. */
    private boolean responseHeadersSetBeforeDoFilter;

    /**
     * Instanciation de cp export filter.
     */
    public CpExportFilter()
    {
        super();
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see javax.servlet.Filter#init(javax.servlet.FilterConfig)
     */
    @Override
    public void init(FilterConfig filterConfig) throws ServletException
    {
        String initResponseHeadersSetBeforeDoFilter = filterConfig.getInitParameter("responseHeadersSetBeforeDoFilter");

        if (StringUtils.isNotBlank(initResponseHeadersSetBeforeDoFilter))
        {
            this.responseHeadersSetBeforeDoFilter = new Boolean(initResponseHeadersSetBeforeDoFilter).booleanValue();
        }
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see javax.servlet.Filter#destroy()
     */
    @Override
    public void destroy()
    {
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.filter.CpAbstractExportFilter#doFilterInternal(javax.servlet.ServletRequest,
     *      javax.servlet.ServletResponse, javax.servlet.FilterChain, java.lang.String, java.lang.String)
     */
    @Override
    protected void doFilterInternal(ServletRequest request, ServletResponse response, FilterChain chain,
        String exportFileName, String view) throws IOException, ServletException
    {
        if (responseHeadersSetBeforeDoFilter)
        {
            setResponseHeaders((HttpServletResponse) response, exportFileName, view);
        }
        chain.doFilter(request, new ExportResponseWrapper((HttpServletResponse) response));
        if (!responseHeadersSetBeforeDoFilter)
        {
            setResponseHeaders((HttpServletResponse) response, exportFileName, view);
        }
    }

}
