/*
 * Copyright (c) 2014 DGFiP - Tous droits réservés
 */

/*
/*Copyright (c) 2014 DGFiP - Tous droits réservés/* * Copyright 2004 original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package fr.gouv.finances.lombok.apptags.table.core;

import java.util.Map;

import fr.gouv.finances.lombok.apptags.table.state.State;

/**
 * Class TableRegistry
 */
public final class TableRegistry extends AbstractRegistry
{

    /**
     * Instanciation de table registry.
     * 
     * @param model
     */
    public TableRegistry(TableModel model)
    {
        this.context = model.getContext();
        this.tableId = model.getTableHandler().getTable().getTableId();
        this.prefixWithTableId = model.getTableHandler().prefixWithTableId();
        this.state = model.getTableHandler().getTable().getState();
        this.stateAttr = model.getTableHandler().getTable().getStateAttr();
        this.autoIncludeParameters = model.getTableHandler().getTable().isAutoIncludeParameters();
        setParameterMap();
    }

    /**
     * Sauvegarde de l'état des paramètres.
     * 
     * @param state
     * @param tableParameterMap
     */
    @Override
    protected void handleStateInternal(State state, Map<String, Object> tableParameterMap)
    {
        state.saveParameters(context, tableId, tableParameterMap);
    }
}
