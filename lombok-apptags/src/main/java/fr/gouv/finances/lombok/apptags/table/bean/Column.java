/*
/*Copyright (c) 2014 DGFiP - Tous droits réservés/* * Copyright 2004 original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package fr.gouv.finances.lombok.apptags.table.bean;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import fr.gouv.finances.lombok.apptags.table.cell.FilterOption;
import fr.gouv.finances.lombok.apptags.table.core.PreferencesConstants;
import fr.gouv.finances.lombok.apptags.table.core.TableConstants;
import fr.gouv.finances.lombok.apptags.table.core.TableModel;

/**
 * Copie du tag column. Le modèle référence uniquement cette copie
 */
public class Column extends Attributes
{

    /** model. */
    private TableModel model;

    /** alias. */
    private String alias;

    /** calc title. */
    private List<String> calcTitle;

    /** calc. */
    private List<String> calc;

    /** calc display. */
    private List<String> calcDisplay;

    /** calc export. */
    private List<String> calcExport;

    /** calc class. */
    private String calcClass;

    /** calc style. */
    private String calcStyle;

    /** calc title class. */
    private String calcTitleClass;

    /** calc title style. */
    private String calcTitleStyle;

    /** cell. */
    private String cell;

    /** cell display. */
    private String cellDisplay;

    /** compact. */
    private Boolean compact;

    /** filter options. */
    private Object filterOptions;

    /** escape auto format. */
    private Boolean escapeAutoFormat;

    /** filterable. */
    private Boolean filterable;

    /** filter cell. */
    private String filterCell;

    /** filter class. */
    private String filterClass;

    /** filter style. */
    private String filterStyle;

    /** filter size. */
    private String filterSize;

    /** filter strict. */
    private Boolean filterStrict;

    /** filter null option. */
    private String filterNullOption;

    /** format. */
    private String format;

    /** header cell. */
    private String headerCell;

    /** header class. */
    private String headerClass;

    /** header style. */
    private String headerStyle;

    /** interceptor. */
    private String interceptor;

    /** parse. */
    private String parse;

    /** property. */
    private String property;

    /** property value. */
    private Object propertyValue;

    /** sortable. */
    private Boolean sortable;

    /** style. */
    private String style;

    /** style class. */
    private String styleClass;

    /** title. */
    private String title;

    /** title pdf. */
    private String titlePdf;

    /** value. */
    private Object value;

    /** views allowed. */
    private List<String> viewsAllowed;

    /** views denied. */
    private List<String> viewsDenied;

    /** width. */
    private String width;

    /** responsive */
    private Boolean showResponsive;

    /** dataAttributes */
    private String dataAttributes;
    
    /** filteredDroplist */
    private Boolean filteredDroplist;
    
    /** escapeXml */
    private Boolean escapeXml;

    /**
     * writeOrBlank - Boolean, vaut true si la colonne utilise le mécanisme qui permet de ne pas afficher le contenu de
     * la cellule si une propriété du bean courant est égale à celle du bean précédent.
     */
    private Boolean writeOrBlank;

    /** is first column. */
    private boolean isFirstColumn;

    /** is last column. */
    private boolean isLastColumn;

    /**
     * Instanciation de column.
     *
     * @param model
     */
    public Column(TableModel model)
    {
        this.model = model;
    }

    /**
     * Complète les List d'info sur les cellules calculées pour La base est définie par le nombre d'éméents dans
     * l'attribut 'calc'.
     */
    public void completeCalcInfo()
    {
        // ajustement de calcTitle
        int diffTitle = this.calc.size() - this.calcTitle.size();
        for (int i = 0; i < diffTitle; i++)
        {
            this.calcTitle.add("");
        }

        // ajustement de calcDisplay
        int diffDisplay = this.calc.size() - this.calcDisplay.size();
        for (int i = 0; i < diffDisplay; i++)
        {
            this.calcDisplay.add("all");
        }

        // ajustement de calcExport
        int diffExport = this.calc.size() - this.calcExport.size();
        for (int i = 0; i < diffExport; i++)
        {
            this.calcExport.add("all");
        }
    }

    /**
     * methode Defaults :
     */
    public void defaults()
    {
        this.cell = ColumnDefaults.getCell(model, cell);

        this.alias = ColumnDefaults.getAlias(alias, property);
        this.calcTitle = ColumnDefaults.getCalcTitle(model, calcTitle);
        this.calcClass = ColumnDefaults.getCalcClass(model, calcClass);
        this.calcTitleClass = ColumnDefaults.getCalcTitleClass(model, calcTitleClass);
        this.compact = ColumnDefaults.isCompact(model, compact);
        this.escapeAutoFormat = ColumnDefaults.isEscapeAutoFormat(model, escapeAutoFormat);
        this.format = ColumnDefaults.getFormat(model, this, format);
        this.filterable = ColumnDefaults.isFilterable(model, filterable);
        this.filterCell = ColumnDefaults.getFilterCell(model, filterCell);
        this.filterOptions = ColumnDefaults.getFilterOptions(model, filterOptions);
        this.filterStrict = ColumnDefaults.isFilterStrict(model, filterStrict);
        this.filterNullOption = ColumnDefaults.getFilterNullOption(model, filterNullOption);
        this.headerCell = ColumnDefaults.getHeaderCell(model, headerCell);
        this.headerClass = ColumnDefaults.getHeaderClass(model, headerClass);
        this.parse = ColumnDefaults.getParse(model, this, parse);
        this.sortable = ColumnDefaults.isSortable(model, sortable);
        this.title = ColumnDefaults.getTitle(model, title, alias);
        this.titlePdf = ColumnDefaults.getTitlePdf(model, titlePdf);
        this.writeOrBlank = ColumnDefaults.isWriteOrBlank(model, writeOrBlank);
        this.escapeXml = ColumnDefaults.getEscapeXml(model, escapeXml);
        this.showResponsive = ColumnDefaults.isShowResponsive(model, showResponsive);
        this.dataAttributes = ColumnDefaults.getDataAttributes(model, dataAttributes);
    }

    /**
     * Accesseur de l attribut alias.
     *
     * @return alias
     */
    public String getAlias()
    {
        return alias;
    }

    /**
     * Accesseur de l attribut calc.
     *
     * @return calc
     */
    public List<String> getCalc()
    {
        return calc;
    }

    /**
     * Accesseur de l attribut calc class.
     *
     * @return calc class
     */
    public String getCalcClass()
    {
        return calcClass;
    }

    /**
     * Accesseur de l attribut calc display.
     *
     * @return calc display
     */
    public List<String> getCalcDisplay()
    {
        return calcDisplay;
    }

    /**
     * Accesseur de l attribut calc export.
     *
     * @return calc export
     */
    public List<String> getCalcExport()
    {
        return calcExport;
    }

    /**
     * Accesseur de l attribut calc style.
     *
     * @return calc style
     */
    public String getCalcStyle()
    {
        return calcStyle;
    }

    /**
     * Accesseur de l attribut calc title.
     *
     * @return calc title
     */
    public List<String> getCalcTitle()
    {
        return calcTitle;
    }

    /**
     * Accesseur de l attribut calc title class.
     *
     * @return calc title class
     */
    public String getCalcTitleClass()
    {
        return calcTitleClass;
    }

    /**
     * Accesseur de l attribut calc title style.
     *
     * @return calc title style
     */
    public String getCalcTitleStyle()
    {
        return calcTitleStyle;
    }

    /**
     * Accesseur de l attribut cell.
     *
     * @return cell
     */
    public String getCell()
    {
        return cell;
    }

    /**
     * Accesseur de l attribut cell display.
     *
     * @return cell display
     */
    public String getCellDisplay()
    {
        return cellDisplay;
    }

    /**
     * Accesseur de l attribut dataAttributes.
     * 
     * @return dataAttributes
     */
    public String getDataAttributes()
    {
        return dataAttributes;
    }

    /**
     * Accesseur de l attribut filter cell.
     *
     * @return filter cell
     */
    public String getFilterCell()
    {
        return filterCell;
    }

    /**
     * Accesseur de l attribut filter class.
     *
     * @return filter class
     */
    public String getFilterClass()
    {
        return filterClass;
    }

    /**
     * Accesseur de l attribut filter null option.
     *
     * @return filter null option
     */
    public String getFilterNullOption()
    {
        return filterNullOption;
    }

    /**
     * Accesseur de l attribut filter options.
     *
     * @return filter options
     */
    @SuppressWarnings("unchecked")
    public Collection<FilterOption> getFilterOptions()
    {
        return (Collection<FilterOption>) filterOptions;
    }

    /**
     * Accesseur de l attribut filter size.
     *
     * @return filter size
     */
    public String getFilterSize()
    {
        return filterSize;
    }

    /**
     * Accesseur de l attribut filter style.
     *
     * @return filter style
     */
    public String getFilterStyle()
    {
        return filterStyle;
    }

    /**
     * Accesseur de l attribut format.
     *
     * @return format
     */
    public String getFormat()
    {
        return format;
    }

    /**
     * Accesseur de l attribut header cell.
     *
     * @return header cell
     */
    public String getHeaderCell()
    {
        return headerCell;
    }

    /**
     * Accesseur de l attribut header class.
     *
     * @return header class
     */
    public String getHeaderClass()
    {
        return headerClass;
    }

    /**
     * Accesseur de l attribut header style.
     *
     * @return header style
     */
    public String getHeaderStyle()
    {
        return headerStyle;
    }

    /**
     * Accesseur de l attribut interceptor.
     *
     * @return interceptor
     */
    public String getInterceptor()
    {
        return interceptor;
    }

    /**
     * Accesseur de l attribut parses the.
     *
     * @return parses the
     */
    public String getParse()
    {
        return parse;
    }

    /**
     * Accesseur de l attribut property.
     *
     * @return property
     */
    public String getProperty()
    {
        return property;
    }

    /**
     * Accesseur de l attribut property value.
     *
     * @return property value
     */
    public Object getPropertyValue()
    {
        return propertyValue;
    }

    /**
     * Accesseur de l attribut property value as string.
     *
     * @return property value as string
     */
    public String getPropertyValueAsString()
    {
        Object value = getPropertyValue();
        if (value != null)
        {
            return String.valueOf(value);
        }

        return "";
    }

    /**
     * Accesseur de l attribut style.
     *
     * @return style
     */
    public String getStyle()
    {
        return style;
    }

    /**
     * Accesseur de l attribut style class.
     *
     * @return style class
     */
    public String getStyleClass()
    {
        return styleClass;
    }

    /**
     * Accesseur de l attribut title.
     *
     * @return title
     */
    public String getTitle()
    {
        return title;
    }

    /**
     * Accesseur de l attribut title pdf.
     *
     * @return title pdf
     */
    public String getTitlePdf()
    {
        return titlePdf;
    }

    /**
     * Accesseur de l attribut value.
     *
     * @return value
     */
    public Object getValue()
    {
        return value;
    }

    /**
     * Accesseur de l attribut value as string.
     *
     * @return value as string
     */
    public String getValueAsString()
    {
        Object value = getValue();
        if (value != null)
        {
            return String.valueOf(value);
        }

        return "";
    }

    /**
     * Accesseur de l attribut views allowed.
     *
     * @return views allowed
     */
    public List<String> getViewsAllowed()
    {
        return viewsAllowed;
    }

    /**
     * Accesseur de l attribut views denied.
     *
     * @return views denied
     */
    public List<String> getViewsDenied()
    {
        return viewsDenied;
    }

    /**
     * Accesseur de l attribut width.
     *
     * @return width
     */
    public String getWidth()
    {
        return width;
    }

    /**
     * Gets the writeOrBlank - Boolean, vaut true si la colonne utilise le mécanisme qui permet de ne pas afficher le
     * contenu de la cellule si une propriété du bean courant est égale à celle du bean précédent.
     *
     * @return the writeOrBlank - Boolean, vaut true si la colonne utilise le mécanisme qui permet de ne pas afficher le
     *         contenu de la cellule si une propriété du bean courant est égale à celle du bean précédent
     */
    public Boolean getWriteOrBlank()
    {
        return writeOrBlank;
    }

    /**
     * Verifie si calculated.
     *
     * @return true, si c'est calculated
     */
    public boolean isCalculated()
    {
        return (calc != null && calc.size() > 0);
    }

    /**
     * Verifie si compact.
     *
     * @return true, si c'est compact
     */
    public boolean isCompact()
    {
        return compact.booleanValue();
    }

    /**
     * Verifie si currency.
     *
     * @return true, si c'est currency
     */
    public boolean isCurrency()
    {
        if (StringUtils.isNotBlank(getCell())
            && (getCell().equalsIgnoreCase(model.getPreferences().getPreference(
                PreferencesConstants.COLUMN_CELL + TableConstants.CURRENCY))))
        {
            return true;
        }

        return false;
    }

    /**
     * Verifie si date.
     *
     * @return true, si c'est date
     */
    public boolean isDate()
    {
        if (StringUtils.isNotEmpty(getCell())
            && getCell().equals(
                model.getPreferences().getPreference(PreferencesConstants.COLUMN_CELL + TableConstants.DATE)))
        {
            return true;
        }

        return false;
    }

    /**
     * Verifie si escape auto format.
     *
     * @return true, si c'est escape auto format
     */
    public boolean isEscapeAutoFormat()
    {
        return escapeAutoFormat.booleanValue();
    }

    /**
     * Verifie si filterable.
     *
     * @return true, si c'est filterable
     */
    public boolean isFilterable()
    {
        return filterable.booleanValue();
    }

    /**
     * Verifie si filter strict.
     *
     * @return true, si c'est filter strict
     */
    public boolean isFilterStrict()
    {
        return filterStrict.booleanValue();
    }

    /**
     * Verifie si first column.
     *
     * @return true, si c'est first column
     */
    public boolean isFirstColumn()
    {
        return isFirstColumn;
    }

    /**
     * Verifie si last column.
     *
     * @return true, si c'est last column
     */
    public boolean isLastColumn()
    {
        return isLastColumn;
    }

    /**
     * Accesseur de l attribut responsive.
     *
     * @return responsive (true or false)
     */
    public Boolean isShowResponsive()
    {
        return showResponsive;
    }

    /**
     * Verifie si sortable.
     *
     * @return true, si c'est sortable
     */
    public boolean isSortable()
    {
        return sortable.booleanValue();
    }

    /**
     * Modificateur de l attribut alias.
     *
     * @param alias le nouveau alias
     */
    public void setAlias(String alias)
    {
        this.alias = alias;
    }

    /**
     * Modificateur de l attribut calc.
     *
     * @param calc le nouveau calc
     */
    public void setCalc(String calc)
    {
        this.calc = new ArrayList<String>();
        if (calc != null)
        {
            String[] calcrow = StringUtils.split(calc, ",");
            this.calc = new ArrayList<String>();
            for (int i = 0; i < calcrow.length; i++)
            {
                this.calc.add(StringUtils.trim(calcrow[i]));
            }
        }
    }

    /**
     * Modificateur de l attribut calc class.
     *
     * @param calcClass le nouveau calc class
     */
    public void setCalcClass(String calcClass)
    {
        this.calcClass = calcClass;
    }

    /**
     * Contrôle des valeurs.
     *
     * @param calcDisplay le nouveau calc display
     */
    public void setCalcDisplay(String calcDisplay)
    {
        this.calcDisplay = new ArrayList<String>();
        if (calcDisplay != null)
        {
            String[] calcDisplayRow = StringUtils.split(calcDisplay, ",");
            this.calcDisplay = new ArrayList<String>();
            for (int i = 0; i < calcDisplayRow.length; i++)
            {
                String aValue = StringUtils.trim(calcDisplayRow[i]);
                this.calcDisplay.add(aValue);
            }
        }
    }

    /**
     * Modificateur de l attribut calc export.
     *
     * @param calcExport le nouveau calc export
     */
    public void setCalcExport(String calcExport)
    {
        this.calcExport = new ArrayList<String>();
        if (calcExport != null)
        {
            String[] calcDisplayRow = StringUtils.split(calcExport, ",");
            this.calcExport = new ArrayList<String>();
            for (int i = 0; i < calcDisplayRow.length; i++)
            {
                this.calcExport.add(StringUtils.trim(calcDisplayRow[i]));
            }
        }
    }

    /**
     * Modificateur de l attribut calc style.
     *
     * @param calcStyle le nouveau calc style
     */
    public void setCalcStyle(String calcStyle)
    {
        this.calcStyle = calcStyle;
    }

    /**
     * Modificateur de l attribut calc title.
     *
     * @param calcTitle le nouveau calc title
     */
    public void setCalcTitle(String calcTitle)
    {
        this.calcTitle = new ArrayList<String>();
        if (calcTitle != null)
        {
            String[] calctitlerow = StringUtils.split(calcTitle, ",");
            this.calcTitle = new ArrayList<String>();
            for (int i = 0; i < calctitlerow.length; i++)
            {
                this.calcTitle.add(StringUtils.trim(calctitlerow[i]));
            }
        }
    }

    /**
     * Modificateur de l attribut calc title class.
     *
     * @param calcTitleClass le nouveau calc title class
     */
    public void setCalcTitleClass(String calcTitleClass)
    {
        this.calcTitleClass = calcTitleClass;
    }

    /**
     * Modificateur de l attribut calc title style.
     *
     * @param calcTitleStyle le nouveau calc title style
     */
    public void setCalcTitleStyle(String calcTitleStyle)
    {
        this.calcTitleStyle = calcTitleStyle;
    }

    /**
     * Modificateur de l attribut cell.
     *
     * @param cell le nouveau cell
     */
    public void setCell(String cell)
    {
        this.cell = cell;
    }

    /**
     * Modificateur de l attribut cell display.
     *
     * @param cellDisplay le nouveau cell display
     */
    public void setCellDisplay(String cellDisplay)
    {
        this.cellDisplay = cellDisplay;
    }

    /**
     * Modificateur de l attribut compact.
     *
     * @param compact le nouveau compact
     */
    public void setCompact(Boolean compact)
    {
        this.compact = compact;
    }

    /**
     * Modificateur de l attribut dataAttributes.
     * 
     * @param dataAttributes
     */
    public void setDataAttributes(String dataAttributes)
    {
        this.dataAttributes = dataAttributes;
    }

    /**
     * Modificateur de l attribut escape auto format.
     *
     * @param escapeAutoFormat le nouveau escape auto format
     */
    public void setEscapeAutoFormat(Boolean escapeAutoFormat)
    {
        this.escapeAutoFormat = escapeAutoFormat;
    }

    /**
     * Modificateur de l attribut filterable.
     *
     * @param filterable le nouveau filterable
     */
    public void setFilterable(Boolean filterable)
    {
        this.filterable = filterable;
    }

    /**
     * Modificateur de l attribut filter cell.
     *
     * @param filterCell le nouveau filter cell
     */
    public void setFilterCell(String filterCell)
    {
        this.filterCell = filterCell;
    }

    /**
     * Modificateur de l attribut filter class.
     *
     * @param filterClass le nouveau filter class
     */
    public void setFilterClass(String filterClass)
    {
        this.filterClass = filterClass;
    }

    /**
     * Modificateur de l attribut filter null option.
     *
     * @param filterNullOption le nouveau filter null option
     */
    public void setFilterNullOption(String filterNullOption)
    {
        this.filterNullOption = filterNullOption;
    }

    /**
     * Modificateur de l attribut filter options.
     *
     * @param filterOptions le nouveau filter options
     */
    public void setFilterOptions(Object filterOptions)
    {
        this.filterOptions = filterOptions;
    }

    /**
     * Modificateur de l attribut filter size.
     *
     * @param filterSize le nouveau filter size
     */
    public void setFilterSize(String filterSize)
    {
        this.filterSize = filterSize;
    }

    /**
     * Modificateur de l attribut filter strict.
     *
     * @param filterStrict le nouveau filter strict
     */
    public void setFilterStrict(Boolean filterStrict)
    {
        this.filterStrict = filterStrict;
    }

    /**
     * Modificateur de l attribut filter style.
     *
     * @param filterStyle le nouveau filter style
     */
    public void setFilterStyle(String filterStyle)
    {
        this.filterStyle = filterStyle;
    }

    /**
     * Modificateur de l attribut first column.
     *
     * @param isFirstColumn le nouveau first column
     */
    public void setFirstColumn(boolean isFirstColumn)
    {
        this.isFirstColumn = isFirstColumn;
    }

    /**
     * Modificateur de l attribut format.
     *
     * @param format le nouveau format
     */
    public void setFormat(String format)
    {
        this.format = format;
    }

    /**
     * Modificateur de l attribut header cell.
     *
     * @param headerCell le nouveau header cell
     */
    public void setHeaderCell(String headerCell)
    {
        this.headerCell = headerCell;
    }

    /**
     * Modificateur de l attribut header class.
     *
     * @param headerClass le nouveau header class
     */
    public void setHeaderClass(String headerClass)
    {
        this.headerClass = headerClass;
    }

    /**
     * Modificateur de l attribut header style.
     *
     * @param headerStyle le nouveau header style
     */
    public void setHeaderStyle(String headerStyle)
    {
        this.headerStyle = headerStyle;
    }

    /**
     * Modificateur de l attribut interceptor.
     *
     * @param interceptor le nouveau interceptor
     */
    public void setInterceptor(String interceptor)
    {
        this.interceptor = interceptor;
    }

    /**
     * Modificateur de l attribut last column.
     *
     * @param isLastColumn le nouveau last column
     */
    public void setLastColumn(boolean isLastColumn)
    {
        this.isLastColumn = isLastColumn;
    }

    /**
     * Modificateur de l attribut parses the.
     *
     * @param parse le nouveau parses the
     */
    public void setParse(String parse)
    {
        this.parse = parse;
    }

    /**
     * Modificateur de l attribut property.
     *
     * @param property le nouveau property
     */
    public void setProperty(String property)
    {
        this.property = property;
    }

    /**
     * Modificateur de l attribut property value.
     *
     * @param propertyValue le nouveau property value
     */
    public void setPropertyValue(Object propertyValue)
    {
        this.propertyValue = propertyValue;
    }

    /**
     * Modificateur de l attribut responsive.
     * 
     * @param showResponsive (true or false)
     */
    public void setShowResponsive(Boolean showResponsive)
    {
        this.showResponsive = showResponsive;
    }

    /**
     * Modificateur de l attribut sortable.
     *
     * @param sortable le nouveau sortable
     */
    public void setSortable(Boolean sortable)
    {
        this.sortable = sortable;
    }

    /**
     * Modificateur de l attribut style.
     *
     * @param style le nouveau style
     */
    public void setStyle(String style)
    {
        this.style = style;
    }

    /**
     * Modificateur de l attribut style class.
     *
     * @param styleClass le nouveau style class
     */
    public void setStyleClass(String styleClass)
    {
        this.styleClass = styleClass;
    }

    /**
     * Modificateur de l attribut title.
     *
     * @param title le nouveau title
     */
    public void setTitle(String title)
    {
        this.title = title;
    }

    /**
     * Modificateur de l attribut title pdf.
     *
     * @param titlePdf le nouveau title pdf
     */
    public void setTitlePdf(String titlePdf)
    {
        this.titlePdf = titlePdf;
    }

    /**
     * Modificateur de l attribut value.
     *
     * @param value le nouveau value
     */
    public void setValue(Object value)
    {
        this.value = value;
    }

    /**
     * Modificateur de l attribut views allowed.
     *
     * @param viewsAllowed le nouveau views allowed
     */
    public void setViewsAllowed(String viewsAllowed)
    {
        if (viewsAllowed != null)
        {
            this.viewsAllowed = new ArrayList<String>();
            String[] extractViewAllowed = StringUtils.split(viewsAllowed, ",");
            for (int i = 0; i < extractViewAllowed.length; i++)
            {
                this.viewsAllowed.add(extractViewAllowed[i]);
            }
        }
    }

    /**
     * Modificateur de l attribut views denied.
     * 
     * @param viewsDenied le nouveau views denied
     */
    public void setViewsDenied(String viewsDenied)
    {
        if (viewsDenied != null)
        {
            this.viewsDenied = new ArrayList<String>();
            String[] extractViewdenied = StringUtils.split(viewsDenied, ",");
            for (int i = 0; i < extractViewdenied.length; i++)
            {
                this.viewsDenied.add(extractViewdenied[i]);
            }
        }
    }

    /**
     * Modificateur de l attribut width.
     * 
     * @param width le nouveau width
     */
    public void setWidth(String width)
    {
        this.width = width;
    }

    /**
     * Sets the writeOrBlank - Boolean, vaut true si la colonne utilise le mécanisme qui permet de ne pas afficher le
     * contenu de la cellule si une propriété du bean courant est égale à celle du bean précédent.
     * 
     * @param writeOrBlank the new writeOrBlank - Boolean, vaut true si la colonne utilise le mécanisme qui permet de ne
     *        pas afficher le contenu de la cellule si une propriété du bean courant est égale à celle du bean précédent
     */
    public void setWriteOrBlank(Boolean writeOrBlank)
    {
        this.writeOrBlank = writeOrBlank;
    }

    public Boolean getFilteredDroplist()
    {
        return filteredDroplist;
    }

    public void setFilteredDroplist(Boolean filteredDroplist)
    {
        this.filteredDroplist = filteredDroplist;
    }

    public Boolean getEscapeXml()
    {
        return escapeXml;
    }

    public void setEscapeXml(Boolean escapeXml)
    {
        this.escapeXml = escapeXml;
    }

}
