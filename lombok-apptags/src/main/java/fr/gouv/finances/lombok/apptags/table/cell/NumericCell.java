/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.apptags.table.cell;

/**
 * Interface NumericCell
 */
public interface NumericCell
{

    /**
     * Accesseur de l attribut precision.
     * 
     * @return precision
     */
    public int getPrecision();

    /**
     * Accesseur de l attribut cell value formated.
     * 
     * @param value
     * @return cell value formated
     */
    public String getCellValueFormated(Number value);

}
