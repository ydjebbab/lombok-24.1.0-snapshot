/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : NodeTypeTag.java
 *
 */
package fr.gouv.finances.lombok.apptags.tree.tag;

import javax.servlet.jsp.JspException;

import fr.gouv.finances.lombok.apptags.base.TagWriter;
import fr.gouv.finances.lombok.apptags.tree.bean.TreeIteratorElement;

/**
 * Class NodeTypeTag.
 * 
 * @author wpetit-cp
 * @version $Revision: 1.2 $ Date: 15 déc. 2009
 */
public class NodeTypeTag extends NodeBaseTag
{

    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /**
     * Instanciation de node type tag.
     */
    public NodeTypeTag()
    {
        super();
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param tagWriter
     * @return int
     * @throws JspException the jsp exception
     * @see fr.gouv.finances.lombok.apptags.base.BaseAppTags#writeTagContent(fr.gouv.finances.lombok.apptags.base.TagWriter)
     */
    @Override
    protected int writeTagContent(TagWriter tagWriter) throws JspException
    {
        this.initParentTreeTag();
        TreeIteratorElement element = this.parentTreeTag.getElement();
        tagWriter.appendValueOutsideTag(element.getNode().getType());
        return SKIP_BODY;
    }
}
