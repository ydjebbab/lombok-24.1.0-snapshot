/*
 * Copyright (c) 2014 DGFiP - Tous droits réservés
 *//*
/*Copyright (c) 2014 DGFiP - Tous droits réservés/* * Copyright 2004 original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package fr.gouv.finances.lombok.apptags.table.view;

import org.apache.commons.lang.StringUtils;

import fr.gouv.finances.lombok.apptags.table.bean.Column;
import fr.gouv.finances.lombok.apptags.table.bean.Export;
import fr.gouv.finances.lombok.apptags.table.core.TableModel;

/**
 * For producing a delimited datafile view of the table. Default delimiter is comma.
 */
public class CsvView implements View
{

    /** Constant : DELIMITER. */
    public static final String DELIMITER = "delimiter";

    /** Constant : DEFAULT_DELIMITER. */
    static final String DEFAULT_DELIMITER = ",";

    /** plain data. */
    private StringBuilder plainData = new StringBuilder();

    /**
     * Constructeur de la classe CsvView.java
     */
    public CsvView()
    {
        super();

    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.view.View#beforeBody(fr.gouv.finances.lombok.apptags.table.core.TableModel)
     */
    @Override
    public void beforeBody(TableModel model)
    {
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.view.View#body(fr.gouv.finances.lombok.apptags.table.core.TableModel,
     *      fr.gouv.finances.lombok.apptags.table.bean.Column)
     */
    @Override
    public void body(TableModel model, Column column)
    {
        Export export = model.getExportHandler().getCurrentExport();
        String delimiter = export.getAttributeAsString(DELIMITER);
        if (StringUtils.isBlank(delimiter))
        {
            delimiter = DEFAULT_DELIMITER;
        }

        String value = ExportViewUtils.parseCSV(column.getCellDisplay());

        plainData.append(value.trim());
        plainData.append(delimiter);
        if (column.isLastColumn())
        {
            plainData.append("\r\n");
        }
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.view.View#afterBody(fr.gouv.finances.lombok.apptags.table.core.TableModel)
     */
    @Override
    public Object afterBody(TableModel model)
    {
        return plainData.toString();
    }
}
