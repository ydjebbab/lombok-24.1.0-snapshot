/*
 * Copyright (c) 2014 DGFiP - Tous droits réservés
 *//*
/*Copyright (c) 2014 DGFiP - Tous droits réservés/* * Copyright 2004 original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package fr.gouv.finances.lombok.apptags.table.view.html;

import fr.gouv.finances.lombok.apptags.table.core.TableModel;
import fr.gouv.finances.lombok.apptags.util.HtmlBuilder;

/**
 * Class StatusBarBuilder
 */
public class StatusBarBuilder
{

    /** html. */
    private HtmlBuilder html;

    /** model. */
    private TableModel model;

    /**
     * Instanciation de status bar builder.
     * 
     * @param model
     */
    public StatusBarBuilder(TableModel model)
    {
        this(new HtmlBuilder(), model);
    }

    /**
     * Instanciation de status bar builder.
     * 
     * @param html
     * @param model
     */
    public StatusBarBuilder(HtmlBuilder html, TableModel model)
    {
        this.html = html;
        this.model = model;
    }

    /**
     * Accesseur de l attribut html builder.
     * 
     * @return html builder
     */
    public HtmlBuilder getHtmlBuilder()
    {
        return html;
    }

    /**
     * Accesseur de l attribut table model.
     * 
     * @return table model
     */
    protected TableModel getTableModel()
    {
        return model;
    }

    /**
     * methode Status message :
     */
    public void statusMessage()
    {
        if (model.getLimit().getTotalRows() == 0)
        {
            html.append(model.getMessages().getMessage(BuilderConstants.STATUSBAR_NO_RESULTS_FOUND));
        }
        else
        {
            Integer total = Integer.valueOf (model.getLimit().getTotalRows());
            Integer from = Integer.valueOf (model.getLimit().getRowStart() + 1);
            Integer to = Integer.valueOf (model.getLimit().getRowEnd());
            Object[] messageArguments = {total, from, to};
            html.append(model.getMessages().getMessage(BuilderConstants.STATUSBAR_RESULTS_FOUND, messageArguments));
        }
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        return html.toString();
    }
}
