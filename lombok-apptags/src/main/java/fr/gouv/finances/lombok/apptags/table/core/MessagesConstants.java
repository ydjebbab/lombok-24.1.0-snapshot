/*
 * Copyright (c) 2014 DGFiP - Tous droits réservés
 */
/*
/*Copyright (c) 2014 DGFiP - Tous droits réservés/* * Copyright 2004 original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package fr.gouv.finances.lombok.apptags.table.core;

/**
 * Class MessagesConstants.
 */
public class MessagesConstants
{

    /** Constant : TABLE_IMAGE_PATH. */
    public static final String TABLE_IMAGE_PATH = "table.imagePath";

    /** Constant : COLUMN_FORMAT. */
    public static final String COLUMN_FORMAT = "column.format.";

    /** Constant : COLUMN_HEADERCELL_TOOLTIP_SORT. */
    public static final String COLUMN_HEADERCELL_TOOLTIP_SORT = "column.headercell.tooltip.sort";
    

    public MessagesConstants()
    {        
    }
}
