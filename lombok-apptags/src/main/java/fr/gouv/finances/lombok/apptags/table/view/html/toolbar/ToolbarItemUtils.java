/*
/*Copyright (c) 2014 DGFiP - Tous droits réservés/* * Copyright 2004 original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package fr.gouv.finances.lombok.apptags.table.view.html.toolbar;

import fr.gouv.finances.lombok.apptags.table.bean.Export;
import fr.gouv.finances.lombok.apptags.table.core.TableModel;
import fr.gouv.finances.lombok.apptags.table.view.html.BuilderUtils;
import fr.gouv.finances.lombok.apptags.table.view.html.TableActions;
import fr.gouv.finances.lombok.apptags.util.HtmlBuilder;

/**
 * Class ToolbarItemUtils
 */
public final class ToolbarItemUtils
{

    /**
     * methode Builds the first page :
     * 
     * @param html html
     * @param model model
     * @param item item
     */
    public static void buildFirstPage(HtmlBuilder html, TableModel model, ToolbarItem item)
    {
        String action = new TableActions(model).getPageAction(1);
        item.setAction(action);
        int page = model.getLimit().getPage();
        if (!BuilderUtils.isFirstPageEnabled(page))
        {
            item.disabled(html);
        }
        else
        {
            item.enabled(html, model);
        }

    }

    /**
     * methode Builds the prev page :
     * 
     * @param html html
     * @param model model
     * @param item item
     */
    public static void buildPrevPage(HtmlBuilder html, TableModel model, ToolbarItem item)
    {
        int page = model.getLimit().getPage();

        String action = new TableActions(model).getPageAction(page - 1);
        item.setAction(action);

        if (!BuilderUtils.isPrevPageEnabled(page))
        {
            item.disabled(html);
        }
        else
        {
            item.enabled(html, model);
        }
    }

    /**
     * methode Builds the some page :
     * 
     * @param html html
     * @param model model
     * @param item item
     * @param i i
     */
    public static void buildSomePage(HtmlBuilder html, TableModel model, ToolbarItem item, int i)
    {
        int page = model.getLimit().getPage();

        String action = new TableActions(model).getPageAction(i);
        item.setAction(action);
        item.enabled(html, model);
    }

    /**
     * methode Builds the space page :
     * 
     * @param html html
     * @param model model
     * @param item item
     */
    public static void buildSpacePage(HtmlBuilder html, TableModel model, ToolbarItem item)
    {
        // String action = new TableActions(model).getPageAction(1);
        // item.setAction(action);
        item.disabled(html);
    }

    /**
     * methode Builds the next page :
     * 
     * @param html html
     * @param model model
     * @param item item
     */
    public static void buildNextPage(HtmlBuilder html, TableModel model, ToolbarItem item)
    {
        int page = model.getLimit().getPage();

        String action = new TableActions(model).getPageAction(page + 1);
        item.setAction(action);

        int totalPages = BuilderUtils.getTotalPages(model);
        if (!BuilderUtils.isNextPageEnabled(page, totalPages))
        {
            item.disabled(html);
        }
        else
        {
            item.enabled(html, model);
        }
    }

    /**
     * methode Builds the last page :
     * 
     * @param html html
     * @param model model
     * @param item item
     */
    public static void buildLastPage(HtmlBuilder html, TableModel model, ToolbarItem item)
    {
        int totalPages = BuilderUtils.getTotalPages(model);

        String action = new TableActions(model).getPageAction(totalPages);
        item.setAction(action);

        int page = model.getLimit().getPage();
        if (!BuilderUtils.isLastPageEnabled(page, totalPages))
        {
            item.disabled(html);
        }
        else
        {
            item.enabled(html, model);
        }
    }

    /**
     * methode Builds the filter :
     * 
     * @param html html
     * @param model model
     * @param item item
     */
    public static void buildFilter(HtmlBuilder html, TableModel model, ToolbarItem item)
    {
        item.setAction(new TableActions(model).getFilterAction());
        item.enabled(html, model);
    }

    /**
     * methode Builds the clear :
     * 
     * @param html html
     * @param model model
     * @param item item
     */
    public static void buildClear(HtmlBuilder html, TableModel model, ToolbarItem item)
    {
        item.setAction(new TableActions(model).getClearAction());
        item.enabled(html, model);
    }

    /**
     * methode Builds the export :
     * 
     * @param html html
     * @param model model
     * @param item item
     * @param export export
     */
    public static void buildExport(HtmlBuilder html, TableModel model, ToolbarItem item, Export export)
    {
        String action = new TableActions(model).getExportAction(export.getView(), export.getFileName());
        item.setAction(action);
        item.setStyle("btn buildExport");
        item.enabled(html, model);
    }

    /* export with download icon */
    /**
     * methode Builds the new export :
     * 
     * @param html html
     * @param model model
     * @param item item
     * @param export export
     */
    public static void buildNewExport(HtmlBuilder html, TableModel model, ToolbarItem item, Export export)
    {
        String action = new TableActions(model).getExportAction(export.getView(), export.getFileName());
        item.setAction(action);
        item.setStyle("btn buildExport");
        item.enabled(html, model);
    }
}
