/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.apptags.table.cell;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanWrapperImpl;
import org.springframework.webflow.core.collection.MutableAttributeMap;
import org.springframework.webflow.execution.RequestContext;

import fr.gouv.finances.lombok.apptags.util.EcTableException;

/**
 * Class PreferencesConstants
 */
public final class PreferencesConstants
{

    /** Valeur du champ caché qui indique qu'une ligne est sélectionnée. */
    public static final String PATTERNSELECTED = "SELECTED";

    /** Valeur du champ caché qui indique qu'une ligne n'est pas sélectionnée. */
    public static final String PATTERNUNSELECTED = "UNSELECTED";

    /** Fonction javascript exécutée lors de la sélection / déselection d'une case à cocher. */
    public static final String ONCLICKJAVASCRIPT = "setCheckboxState";

    /** Fonction javascript exécutée lors de la sélection / déselection de toutes les cases à cocher d'une page. */
    public static final String ONCLICKSELECTALL = "chkbxSelectionnerTout";

    /** Préfixe utilisé pour nommer le champ caché. */
    public static final String PREFIX_HIDDEN = "chkbx_";

    /** Préfixe utilisé pour nommer l'id du champ caché. */
    public static final String PRE_ID_HIDDEN = "h_chkbx_";

    /** Préfixe utilisé pour nommer l'id de la case à cocher. */
    public static final String PREF_ID_CHECKBOX = "c_chkbx_";

    /** Propriété utilisée pour identifier un élément dans l'ensemble des éléments. */
    public static final String ROWID = "id";

    /** Nom du bean qui stocke les id des éléments sélectionnés dans le contexte. */
    public static final String SELECTED_ROWIDS = "li_rowid_el_sel";

    /** Nom du bean qui stocke la liste des éléments dans le flux. */
    public static final String ELEMENTS_IN_FLOW = "li_el_sousflux";

    /** Nom du bean qui stocke la liste des éléments sélectionnés. */
    public static final String SELECTED_IN_FLOW = "element_selectionne";

    /** Nom du bean destiné à stocker un message d'erreur. */
    public static final String MSGERREUR = "erreurentreflux";

    /** Message d'erreur : aucun élément sélectionne. */
    public static final String MSG_NOT_SELECTED = "Aucun élément sélectionné.";

    /** Message d'erreur : sélection d'un seul élément à la fois. */
    public static final String MSG_NOT_MANY = "Il n'est possible de sélectionner qu'un élément à la fois.";

    /** Constant : CHBX_PARAM_ATTR. */
    public static final String CHBX_PARAM_ATTR = "_parametrage";

    /** Constant : CHBX_LISTE. */
    public static final String CHBX_LISTE = "parametrage_checkbox_liste_par_defaut";

    /** Valeur par défaut de l'id de la table produite par extremecomponents. */
    public static final String TABLE_ID = "ec";

    /** Séparateur utilisé pour distinguer la position dans le tableau et l'identifiant de la ligne. */
    public static final String ROWCOUNT_SEP = ":";

    /** Valeur de l'attribut rowid qui indique que la sélection s'effectue à partir du numéro de ligne dans le tableau. */
    public static final String ROWNUMBER_SELECTION = "ROW";

    /** Valeur de l'attribut rowid qui indique que la sélection s'effectue à partir du hashCode du bean dans le tableau. */
    public static final String HASHCODE_SELECTION = "HASHCODE";

    /** Définition des contextes de stockage. */
    public static final int CONVERSATION_SCOPE = 1;

    /** Constant : CONVERSATION_SCOPE_S. */
    public static final String CONVERSATION_SCOPE_S = "CONVERSATION";

    /** Constant : FLOW_SCOPE. */
    public static final int FLOW_SCOPE = 2;

    /** Constant : FLOW_SCOPE_S. */
    public static final String FLOW_SCOPE_S = "FLOW";

    /** Constant : REQUEST_SCOPE. */
    public static final int REQUEST_SCOPE = 3;

    /** Constant : REQUEST_SCOPE_S. */
    public static final String REQUEST_SCOPE_S = "REQUEST";

    /**
     * Valeur par défaut du paramètre storageScope qui définit le contexte de stockage de la liste des identifiants
     * sélectionné.
     */
    public static final int DEFAULT_SELECTED_ROWIDS_STORAGE_SCOPE = FLOW_SCOPE;

    /** Constant : DEFAULT_SELECTED_ROWIDS_STORAGE_SCOPE_S. */
    public static final String DEFAULT_SELECTED_ROWIDS_STORAGE_SCOPE_S = FLOW_SCOPE_S;

    /**
     * Valeur par défaut du paramètre storageScope qui définit le contexte de stockage de l'objet de paramétrage des
     * checkboxes.
     */
    public static final int DEFAULT_CHEXBOXATTR_STORAGE_SCOPE = FLOW_SCOPE;

    /** Constant : DEFAULT_CHEXBOXATTR_STORAGE_SCOPE_S. */
    public static final String DEFAULT_CHEXBOXATTR_STORAGE_SCOPE_S = FLOW_SCOPE_S;

    /** Constant : DEFAULT_SELECTED_STORAGE_SCOPE. */
    public static final int DEFAULT_SELECTED_STORAGE_SCOPE = FLOW_SCOPE;

    /** Constant : DEFAULT_SELECTED_STORAGE_SCOPE_S. */
    public static final String DEFAULT_SELECTED_STORAGE_SCOPE_S = FLOW_SCOPE_S;

    /** Constant : DEFAUT_MSG_STORAGE_SCOPE. */
    public static final int DEFAUT_MSG_STORAGE_SCOPE = FLOW_SCOPE;

    /** Valeur par défaut du maximum de lignes utilisées par un tableau ec. */
    public static final int MAXFETCH = 1000;

    /** Constant : REPONSE_COMPLETE. */
    public static final String REPONSE_COMPLETE = "REPONSE_COMPLETE";

    /** Constant : DEFAULT_MAXFETCHATTR_STORAGE_SCOPE. */
    public static final int DEFAULT_MAXFETCHATTR_STORAGE_SCOPE = FLOW_SCOPE;

    /** Constant : MAXFETCH_PARAM_ATTR. */
    public static final String MAXFETCH_PARAM_ATTR = "_param_maxfetch";

    /** Constant : ATTR_NULL_OR_BLANK. */
    private static final String ATTR_NULL_OR_BLANK = "L'attribut est null ou ne contient que des blancs";

    /**
     * Instanciation de preferences constants.
     */
    private PreferencesConstants()
    {
        // Classe ne contenant que des méthodes statiques - ne pas
        // instancier
    }

    /**
     * retourne le contexte associé à l'entier scope.
     * 
     * @param context
     * @param scope
     * @return used scope
     */
    public static MutableAttributeMap getUsedScope(final RequestContext context, final int scope)
    {
        MutableAttributeMap contextAttributeMap;
        switch (scope)
        {
            case PreferencesConstants.REQUEST_SCOPE:
                contextAttributeMap = context.getRequestScope();
                break;
            case PreferencesConstants.CONVERSATION_SCOPE:
                contextAttributeMap = context.getConversationScope();
                break;

            /*
             * contextAttributeMap = new MutableAttributeMap(context.getConversationScope().union(
             * context.getFlowScope()).union(context.getRequestScope()).getMap());
             */

            case PreferencesConstants.FLOW_SCOPE:
            default:
                contextAttributeMap = context.getFlowScope();
                break;
        }
        return contextAttributeMap;
    }

    /**
     * Sépare le nom du bean des propriétés Retourne une map dont la clé est le nom du bean et la valeur l'expression
     * permettant d'accéder à une propriété.
     * 
     * @param attributeName
     * @return Map
     */
    public static Map<String, String> isoleBeanNameEtExpression(final String attributeName)
    {

        if (attributeName == null || StringUtils.isBlank(attributeName))
        {
            throw new EcTableException(ATTR_NULL_OR_BLANK);
        }

        Map<String, String> result = new HashMap<String, String>();

        int dotPos = attributeName.indexOf('.');
        if (dotPos == -1)
        {
            result.put("beanName", attributeName);
            result.put("expression", null);
        }
        else
        {
            result.put("beanName", attributeName.substring(0, dotPos));
            result.put("expression", attributeName.substring(dotPos + 1));
        }
        return result;
    }

    /**
     * Retourne un objet lu dans le RequestContext dans un des scopes du webflow à partir d'un attribut qui peut être
     * une sous forme d'expression.
     * 
     * @param context
     * @param attributeName
     * @param scope
     * @return Object
     */
    public static Object getObjectFromScope(final RequestContext context, String attributeName, final int scope)
    {
        Object result = null;
        Object target = null;

        Map<String, String> uneMap = PreferencesConstants.isoleBeanNameEtExpression(attributeName);
        String beanName = uneMap.get("beanName");
        String expression = uneMap.get("expression");

        MutableAttributeMap usedScope = PreferencesConstants.getUsedScope(context, scope);
        target = usedScope.get(beanName);

        if (expression == null)
        {
            result = target;
        }
        else if (target != null)
        {
            BeanWrapperImpl bw = new BeanWrapperImpl(target);
            result = bw.getPropertyValue(expression);
        }

        return result;
    }

}
