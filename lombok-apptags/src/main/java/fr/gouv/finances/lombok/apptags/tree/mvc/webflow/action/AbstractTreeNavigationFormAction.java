/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : AbstractTreeNavigationFormAction.java
 *
 */
package fr.gouv.finances.lombok.apptags.tree.mvc.webflow.action;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import org.springframework.webflow.action.FormAction;
import org.springframework.webflow.execution.Event;
import org.springframework.webflow.execution.RequestContext;

import fr.gouv.finances.lombok.apptags.tree.bean.Tree;
import fr.gouv.finances.lombok.apptags.tree.bean.TreeNode;
import fr.gouv.finances.lombok.apptags.tree.mvc.form.TreeForm;
import fr.gouv.finances.lombok.apptags.util.Constantes;

/**
 * Class AbstractTreeNavigationFormAction --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.4 $ Date: 11 déc. 2009
 */
public abstract class AbstractTreeNavigationFormAction extends FormAction
{

    public AbstractTreeNavigationFormAction()
    {
        super();        
    }

    /**
     * Constructeur de la classe AbstractTreeNavigationFormAction.java
     *
     * @param formObjectClass
     */
    public AbstractTreeNavigationFormAction(Class<?> formObjectClass)
    {
        super(formObjectClass);          
    }

    /**
     * Ferme tous les noeuds de l'arbre.
     * 
     * @param requestContext --
     * @return the event
     */
    public Event collapseAll(RequestContext requestContext)
    {
        Event result = new Event(this, "success");

        Tree tree = (Tree) requestContext.getFlowScope().get(getTreeModelName(requestContext));

        tree.collapseAll();

        this.initializeScrollParamInContext(requestContext);

        return result;
    }

    /**
     * Action effectuée lors de la réduction d'un noeud.
     * 
     * @param requestContext --
     * @return the event
     * @throws Exception the exception
     */
    public Event collapseNode(RequestContext requestContext) throws Exception
    {
        this.putScrollParamInContext(requestContext);
        TreeForm treeForm = (TreeForm) requestContext.getFlowScope().get(getFormObjectName());
        Tree tree = (Tree) requestContext.getFlowScope().get(getTreeModelName(requestContext));
        String nodeIdString = treeForm.getNodeId();

        if (tree.isExpanded(nodeIdString))
        {
            tree.collapse(nodeIdString);
        }
        return new Event(this, "success");
    }

    /**
     * Création de l'arbre à afficher.
     * 
     * @param requestContext --
     * @return success() si l'arbre a été créé
     */
    public Event creationArbre(RequestContext requestContext)
    {
        Event result = success();

        Tree tree = this.lectureDuNoeudRacineEtDeSesEnfants(requestContext);

        requestContext.getFlowScope().put(this.getTreeModelName(requestContext), tree);

        return result;
    }

    /**
     * Développe tous les noeuds de l'arbre.
     * 
     * @param requestContext --
     * @return the event
     */
    public Event expandAll(RequestContext requestContext)
    {
        Event result = new Event(this, "success");

        Tree tree = (Tree) requestContext.getFlowScope().get(getTreeModelName(requestContext));

        if (tree != null)
        {
            TreeNode node = tree.getRoot();

            this.expandNode(tree, node, requestContext);
            this.expandDescendants(tree, node, requestContext);
        }

        this.initializeScrollParamInContext(requestContext);

        return result;
    }

    /**
     * Action effectuée lors du développement d'un noeud.
     * 
     * @param requestContext --
     * @return the event
     */
    public Event expandNode(RequestContext requestContext)
    {
        Event result = new Event(this, "success");

        this.putScrollParamInContext(requestContext);

        TreeForm treeForm = (TreeForm) requestContext.getFlowScope().get(getFormObjectName());

        Tree tree = (Tree) requestContext.getFlowScope().get(getTreeModelName(requestContext));

        String nodeIdString = treeForm.getNodeId();

        TreeNode node = tree.findNode(nodeIdString);

        this.expandNode(tree, node, requestContext);

        return result;
    }

    /**
     * Recherche d'un noeud par son nom.
     * 
     * @param requestContext --
     * @return the event
     * @throws Exception the exception
     */
    public Event findNodesByName(RequestContext requestContext) throws Exception
    {
        TreeForm treeForm = (TreeForm) requestContext.getFlowScope().get(getFormObjectName());
        Tree tree = (Tree) requestContext.getFlowScope().get(getTreeModelName(requestContext));
        Set<TreeNode> nodesFound = this.findNodesWithNameContainingText(tree, treeForm.getText(), requestContext);

        treeForm.setFoundNodes(nodesFound);

        return new Event(this, "success");
    }

    /**
     * methode Find nodes with name containing text : --.
     * 
     * @param tree --
     * @param text --
     * @param requestContext --
     * @return set< tree node>
     */
    public Set<TreeNode> findNodesWithNameContainingText(Tree tree, String text, RequestContext requestContext)
    {
        Set<TreeNode> treeNodes = new HashSet<>();
        this.findNodesWithNameContainingText(tree, tree.getRoot(), text, treeNodes, requestContext);
        return treeNodes;
    }

    /**
     * Retourne le nom sous lequel l'objet de type Tree est stocké dans le contexte de flux.
     * 
     * @param requestContext --
     * @return nom de stockage dans le contexte de flux de l'objet de type Tree
     */
    public abstract String getTreeModelName(RequestContext requestContext);

    /**
     * Lecture du noeud racine et d'au moins ses enfants directs.
     * 
     * @param requestContext --
     * @return Tree
     */
    public abstract Tree lectureDuNoeudRacineEtDeSesEnfants(RequestContext requestContext);

    /**
     * Lecture du noeud en paramètre, de ses enfants et petits enfants.
     * 
     * @param tree --
     * @param node --
     * @param requestContext --
     */
    public abstract void lectureEnfantsEtPetitsEnfants(Tree tree, TreeNode node, RequestContext requestContext);

    /**
     * Action effectuée lors de la sélection d'un noeud.
     * 
     * @param requestContext --
     * @return the event
     * @throws Exception the exception
     */
    public Event selectNode(RequestContext requestContext) throws Exception
    {
        this.putScrollParamInContext(requestContext);
        TreeForm treeForm = (TreeForm) requestContext.getFlowScope().get(getFormObjectName());
        Tree tree = (Tree) requestContext.getFlowScope().get(getTreeModelName(requestContext));
        String nodeIdString = treeForm.getNodeId();

        if (tree.isSingleSelectionMode())
        {
            tree.unSelectAll();
        }

        if (!tree.isSelected(nodeIdString))
        {
            tree.select(nodeIdString);
        }

        // Les noeuds sélectionnés sont stockés dans le formulaire
        treeForm.setSelectedNodes(tree.getSelectedNodes());

        return new Event(this, "success");
    }

    /**
     * Sélection d'un noeud, Réduction de tous les noeuds de l'arbre Développement de tous les ancêtres du noeud
     * sélectionné.
     * 
     * @param requestContext --
     * @return the event
     * @throws Exception the exception
     */
    public Event selectNodeAndExpandAncestor(RequestContext requestContext) throws Exception
    {
        TreeForm treeForm = (TreeForm) requestContext.getFlowScope().get(getFormObjectName());
        Tree tree = (Tree) requestContext.getFlowScope().get(getTreeModelName(requestContext));
        String nodeIdString = treeForm.getNodeId();

        if (tree.isSingleSelectionMode())
        {
            tree.unSelectAll();
        }

        if (!tree.isSelected(nodeIdString))
        {
            tree.select(nodeIdString);
        }

        tree.collapseAll();
        tree.expandAncestors(nodeIdString);

        // Les noeuds sélectionnés sont stockés dans le formulaire
        treeForm.setSelectedNodes(tree.getSelectedNodes());

        return new Event(this, "success");

    }

    /**
     * Dé-sélectionne tous les noeuds de l'arbre.
     * 
     * @param requestContext --
     * @return the event
     */
    public Event unSelectAll(RequestContext requestContext)
    {
        Event result = new Event(this, "success");

        Tree tree = (Tree) requestContext.getFlowScope().get(getTreeModelName(requestContext));

        tree.unSelectAll();

        this.initializeScrollParamInContext(requestContext);

        return result;
    }

    /**
     * Action effectuée lors de la dé-sélection d'un noeud.
     * 
     * @param requestContext --
     * @return the event
     * @throws Exception the exception
     */
    public Event unSelectNode(RequestContext requestContext) throws Exception
    {
        this.putScrollParamInContext(requestContext);
        TreeForm treeForm = (TreeForm) requestContext.getFlowScope().get(getFormObjectName());
        Tree tree = (Tree) requestContext.getFlowScope().get(getTreeModelName(requestContext));
        String nodeIdString = treeForm.getNodeId();

        if (tree.isSelected(nodeIdString))
        {
            tree.unSelect(nodeIdString);
        }

        // Les noeuds sélectionnés sont stockés dans le formulaire
        treeForm.setSelectedNodes(tree.getSelectedNodes());

        return new Event(this, "success");
    }

    /**
     * methode Find nodes with name containing text : --.
     * 
     * @param tree --
     * @param treeNode --
     * @param text --
     * @param treeNodes --
     * @param requestContext --
     */
    protected void findNodesWithNameContainingText(Tree tree, TreeNode treeNode, String text, Set<TreeNode> treeNodes,
        RequestContext requestContext)
    {
        if (treeNode != null && text != null)
        {
            if (treeNode.getHasUnloadedChildren())
            {
                this.lectureEnfantsEtPetitsEnfants(tree, treeNode, requestContext);
            }
            if (treeNode.getName() != null && searchWordsInProperty(text, treeNode.getName()))
            {
                treeNodes.add(treeNode);
            }

            for (TreeNode child : treeNode.getChildren())
            {
                this.findNodesWithNameContainingText(tree, child, text, treeNodes, requestContext);
            }
        }
    }

    /**
     * methode Expand descendants : --.
     * 
     * @param tree --
     * @param node --
     * @param requestContext --
     */
    private void expandDescendants(Tree tree, TreeNode node, RequestContext requestContext)
    {
        if (node != null)
        {
            for (TreeNode child : node.getChildren())
            {
                if (child.hasChildren())
                {
                    this.expandNode(tree, child, requestContext);
                    this.expandDescendants(tree, child, requestContext);
                }
            }
        }
    }

    /**
     * methode Expand node : --.
     * 
     * @param tree --
     * @param node --
     * @param requestContext --
     */
    private void expandNode(Tree tree, TreeNode node, RequestContext requestContext)
    {
        if (node != null)
        {
            if (!tree.isExpanded(node.getId()))
            {
                if (node.getHasUnloadedChildren())
                {
                    this.lectureEnfantsEtPetitsEnfants(tree, node, requestContext);
                    node.setHasUnloadedChildren(Boolean.FALSE);
                }
                tree.expand(node.getId());
            }
        }
    }

    /**
     * methode Initialize scroll param in context : .
     * 
     * @param requestContext
     */
    private void initializeScrollParamInContext(RequestContext requestContext)
    {
        /** on remet les parametres des ascenceurs à 0 **/
        requestContext.getFlowScope().put(Constantes.SCROLLX_CONTEXT_PARAMETER, "0");
        requestContext.getFlowScope().put(Constantes.SCROLLY_CONTEXT_PARAMETER, "0");
        requestContext.getFlowScope().put(Constantes.SCROLLYY_HIDDEN_ID, "0");
    }

    /**
     * methode Put scroll param in context : --.
     * 
     * @param requestContext --
     */
    private void putScrollParamInContext(RequestContext requestContext)
    {
        TreeForm treeForm = (TreeForm) requestContext.getFlowScope().get(getFormObjectName());
        String scrollX = treeForm.getScrollX();
        String scrollY = treeForm.getScrollY();

        // requestContext.getRequestScope().put(Constantes.SCROLLX_CONTEXT_PARAMETER, scrollX);
        // requestContext.getRequestScope().put(Constantes.SCROLLY_CONTEXT_PARAMETER, scrollY);
        // modif amlp 25/03/2011 pour conserver et afficher la ligne sélectionnée
        requestContext.getFlowScope().put(Constantes.SCROLLX_CONTEXT_PARAMETER, scrollX);
        requestContext.getFlowScope().put(Constantes.SCROLLY_CONTEXT_PARAMETER, scrollY);
        /** rajout pour gestion ascenceur de l'arbre **/
        requestContext.getFlowScope().put(Constantes.SCROLLYY_HIDDEN_ID, requestContext.getRequestParameters().get("scrollYY"));
    }

    /**
     * methode Search words in property : --.
     * 
     * @param phrase --
     * @param property --
     * @return true, si c'est vrai
     */
    private boolean searchWordsInProperty(String phrase, String property)
    {
        boolean result = true;
        List<String> words = splitTextInWords(phrase);

        if (property == null)
        {
            result = false;
        }
        else
        {
            for (String word : words)
            {

                if (!property.toUpperCase(Locale.FRANCE).contains(word.toUpperCase(Locale.FRANCE)))
                {
                    result = false;
                    break;
                }
            }
        }

        return result;
    }

    /**
     * methode Split text in words : --.
     * 
     * @param text --
     * @return list< string>
     */
    private List<String> splitTextInWords(String text)
    {
        List<String> result = null;
        if (text != null)
        {
            String[] tempWords = text.split("\\s+");
            result = Arrays.asList(tempWords);
        }
        else
        {
            result = new ArrayList<>();
        }

        return result;
    }

}
