/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.apptags.table.cell;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.ParseException;

import fr.gouv.finances.lombok.apptags.table.bean.Column;
import fr.gouv.finances.lombok.apptags.table.core.TableModel;
import fr.gouv.finances.lombok.apptags.util.EcTableException;
import fr.gouv.finances.lombok.apptags.util.format.FormaterNombre;

/**
 * Class AbstractNombreCell
 */
public abstract class AbstractNombreCell extends AbstractCell implements PatternCell, NumericCell
{

    /**
     * Instanciation de abstract nombre cell.
     */
    public AbstractNombreCell()
    {
        super();
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.cell.NumericCell#getPrecision()
     */
    public abstract int getPrecision();

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.cell.AbstractCell#getExportDisplay(fr.gouv.finances.lombok.apptags.table.core.TableModel,
     *      fr.gouv.finances.lombok.apptags.table.bean.Column)
     */
    @Override
    public String getExportDisplay(TableModel model, Column column)
    {
        return FormaterNombre.nombreNDecimalesPourCsv(column.getPropertyValue(), getPrecision());
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.cell.AbstractCell#getCellValue(fr.gouv.finances.lombok.apptags.table.core.TableModel,
     *      fr.gouv.finances.lombok.apptags.table.bean.Column)
     */
    @Override
    protected String getCellValue(TableModel model, Column column)
    {
        return FormaterNombre.nombreNDecimalesPourHtml(column.getPropertyValue(), getPrecision());
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.cell.PatternCell#getLengthCelluleFormatee(fr.gouv.finances.lombok.apptags.table.core.TableModel,
     *      fr.gouv.finances.lombok.apptags.table.bean.Column)
     */
    @Override
    public int getLengthCelluleFormatee(TableModel model, Column column)
    {
        int nbChar = 0;
        if (column.getPropertyValue() != null)
        {
            nbChar = getCellValue(model, column).length();
        }
        return nbChar;
    }

    /**
     * Accesseur de l attribut double value.
     * 
     * @param model
     * @param column
     * @return double value
     */
    public double getDoubleValue(TableModel model, Column column)
    {
        double result = 0;
        DecimalFormat numberFormat = (DecimalFormat) FormaterNombre.getFormatNombreNDecimales(getPrecision(), false);

        Object propertyValue = column.getPropertyValue();
        if (propertyValue instanceof Double)
        {
            Double doubleValue = (Double) propertyValue;
            String doubleValueFormate = numberFormat.format(doubleValue);
            try
            {
                result = ((Double) numberFormat.parse(doubleValueFormate)).doubleValue();
            }
            catch (ParseException exception)
            {
                throw new EcTableException(exception);
            }
        }
        else if (propertyValue instanceof BigDecimal)
        {
            BigDecimal bigDecimalValue = (BigDecimal) propertyValue;

            Double doubleValue = bigDecimalValue.doubleValue();
            // formater er reconvertir en double
            result = doubleValue;
        }

        return result;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.cell.NumericCell#getCellValueFormated(java.lang.Number)
     */
    @Override
    public String getCellValueFormated(Number value)
    {
        return FormaterNombre.nombreNDecimalesPourHtml(value, getPrecision());
    }

}
