/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.apptags.table.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import fr.gouv.finances.lombok.apptags.table.context.Context;
import fr.gouv.finances.lombok.apptags.table.context.HttpServletRequestContext;
import fr.gouv.finances.lombok.apptags.table.core.Preferences;
import fr.gouv.finances.lombok.apptags.table.core.TableConstants;
import fr.gouv.finances.lombok.apptags.table.core.TableModelUtils;
import fr.gouv.finances.lombok.apptags.table.core.TableProperties;
import fr.gouv.finances.lombok.apptags.util.MimeUtils;

/**
 * Class CpAbstractExportFilter
 */
public abstract class CpAbstractExportFilter implements Filter
{

    /** logger. */
    private static Log logger = LogFactory.getLog(CpAbstractExportFilter.class);

    /** Constant : EXPIRES_INCREMENT. */
    private static final int EXPIRES_INCREMENT = 1000;

    /**
     * Instanciation de cp abstract export filter.
     */
    public CpAbstractExportFilter()
    {
        super();
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see javax.servlet.Filter#doFilter(javax.servlet.ServletRequest, javax.servlet.ServletResponse,
     *      javax.servlet.FilterChain)
     */
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException,
        ServletException
    {
        Context context = new HttpServletRequestContext((HttpServletRequest) request);
        boolean isExported = ExportFilterUtils.isExported(context);

        if (isExported)
        {
            String exportFileName = ExportFilterUtils.getExportFileName(context);
            String view = getViewResolver(context);

            doFilterInternal(request, response, chain, exportFileName, view);
            handleExport((HttpServletRequest) request, (HttpServletResponse) response, context);
        }
        else
        {
            chain.doFilter(request, response);
        }
    }

    /**
     * Accesseur de l attribut table id.
     * 
     * @param context
     * @return table id
     */
    private static String getTableId(Context context)
    {
        return context.getParameter(TableConstants.EXPORT_TABLE_ID);
    }

    /**
     * Accesseur de l attribut view resolver.
     * 
     * @param context
     * @return view resolver
     */
    private static String getViewResolver(Context context)
    {
        String tableId = getTableId(context);

        if (StringUtils.isNotBlank(tableId))
        {
//            ancien code
//            String exportViewStr = tableId + "_" + TableConstants.EXPORT_VIEW;
//            String exportView = context.getParameter(exportViewStr);
//
//            return exportView;
            
//            nouveau code
            String exportViewStr = tableId + "_" + TableConstants.EXPORT_VIEW;
            return context.getParameter(exportViewStr);
        }

        return null;
    }

    /**
     * methode Handle export :
     * 
     * @param request
     * @param response
     * @param context
     */
    protected void handleExport(HttpServletRequest request, HttpServletResponse response, Context context)
    {
        try
        {
            Object viewData = request.getAttribute(TableConstants.VIEW_DATA);

            if (viewData != null)
            {
                Preferences preferences = new TableProperties();

                preferences.init(null, TableModelUtils.getPreferencesLocation(context));

                String viewResolver = (String) request.getAttribute(TableConstants.VIEW_RESOLVER);
                Class<? extends Object> classDefinition = Class.forName(viewResolver);
                ViewResolver handleExportViewResolver = (ViewResolver) classDefinition.newInstance();

                handleExportViewResolver.resolveView(request, response, preferences, viewData);
                response.getOutputStream().flush();
                response.getOutputStream().close();
            }

        }
        catch (IOException exception)
        {
            String simpleName = exception.getCause().getClass().getSimpleName();
            if (simpleName.equals("SocketException")) {logger.warn("ClientAbortException:java.net.SocketException caught");}
            else logger.error("", exception);
        }
        catch (Exception exception)
        {
            logger.error("", exception);
        }
    }

    /**
     * methode Sets the response headers :
     * 
     * @param response
     * @param exportFileName
     * @param view
     */
    protected void setResponseHeaders(HttpServletResponse response, String exportFileName, String view)
    {
        String mimeType = MimeUtils.getFileMimeType(exportFileName);

        if (StringUtils.isNotBlank(mimeType))
        {
            response.setContentType(mimeType);
        }

        if (!("jasperhtml".equals(view)))
        {
            response.setHeader("Content-Disposition", "attachment;filename=\"" + exportFileName + "\"");
            response.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
            response.setHeader("Pragma", "public");
            response.setDateHeader("Expires", (System.currentTimeMillis() + EXPIRES_INCREMENT));
        }
    }

    /**
     * methode Do filter internal :
     * 
     * @param request
     * @param response
     * @param chain
     * @param exportFileName
     * @param view
     * @throws IOException Signal qu'une execption de type I/O s'est produite.
     * @throws ServletException the servlet exception
     */
    protected abstract void doFilterInternal(ServletRequest request, ServletResponse response, FilterChain chain,
        String exportFileName, String view) throws IOException, ServletException;

}
