/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.apptags.table.view.jrxmlinfo;

/**
 * Class TitleHeaderInfo
 */
public class TitleHeaderInfo implements HeaderCellInfoI
{

    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** previous title. */
    private TitleHeaderInfo previousTitle;

    /** value. */
    private String value;

    /** width. */
    private int width;

    /** height. */
    private int height;

    /** space before. */
    private int spaceBefore;

    /** absorbe. */
    private boolean absorbe = false;

    /** merge with child. */
    private boolean mergeWithChild = false;

    /** parent header. */
    private HeaderCellInfoI parentHeader;

    /**
     * Instanciation de title header info.
     */
    public TitleHeaderInfo()
    {
        super();
    }

    /**
     * Calcule la largeur d'une cellule représentant un titre.
     * 
     * @return width calcule
     */
    @Override
    public int getWidthCalcule()
    {
        return this.width;
    }

    /**
     * Calcule l'abscisse de coin haut gauche de la cellule représentant un titre.
     * 
     * @return x left corner calcule
     */
    @Override
    public int getXLeftCornerCalcule()
    {
        int abs = 0;
        if (previousTitle != null)
        {
            abs = abs + this.previousTitle.getXLeftCornerCalcule() + this.previousTitle.getWidthCalcule();
        }

        return abs;
    }

    /**
     * Calcule l'ordonnée du coin haut gauche de la cellule représentant un titre.
     * 
     * @return y left corner calcule
     */
    @Override
    public int getYLeftCornerCalcule()
    {
        int ord = 0;
        if (parentHeader == null)
        {
            ord = this.spaceBefore;
        }
        else
        {
            ord = this.parentHeader.getYLeftCornerCalcule() + this.parentHeader.getHeight();
        }

        return ord;
    }

    /**
     * methode Rejoindre sous groupe ou groupe :
     * 
     * @param parentHeader
     */
    public void rejoindreSousGroupeOuGroupe(HeaderCellInfoI parentHeader)
    {
        this.parentHeader = parentHeader;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.view.jrxmlinfo.HeaderCellInfoI#isAbsorbe()
     */
    @Override
    public boolean isAbsorbe()
    {
        return absorbe;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.view.jrxmlinfo.HeaderCellInfoI#setAbsorbe(boolean)
     */
    @Override
    public void setAbsorbe(boolean absorbe)
    {
        this.absorbe = absorbe;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.view.jrxmlinfo.HeaderCellInfoI#isMergeWithChild()
     */
    @Override
    public boolean isMergeWithChild()
    {
        return mergeWithChild;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.view.jrxmlinfo.HeaderCellInfoI#setMergeWithChild(boolean)
     */
    @Override
    public void setMergeWithChild(boolean mergeWithChild)
    {
        this.mergeWithChild = mergeWithChild;
    }

    /**
     * Verifie si titre absorbe.
     * 
     * @return true, si c'est titre absorbe
     */
    public boolean isTitreAbsorbe()
    {
        return absorbe;
    }

    /**
     * Modificateur de l attribut titre absorbe.
     * 
     * @param titreAbsorbe le nouveau titre absorbe
     */
    public void setTitreAbsorbe(boolean titreAbsorbe)
    {
        this.absorbe = titreAbsorbe;
    }

    /**
     * Accesseur de l attribut previous title.
     * 
     * @return previous title
     */
    public TitleHeaderInfo getPreviousTitle()
    {
        return previousTitle;
    }

    /**
     * Modificateur de l attribut previous title.
     * 
     * @param previousTitle le nouveau previous title
     */
    public void setPreviousTitle(TitleHeaderInfo previousTitle)
    {
        this.previousTitle = previousTitle;
    }

    /**
     * Accesseur de l attribut space before.
     * 
     * @return space before
     */
    public int getSpaceBefore()
    {
        return spaceBefore;
    }

    /**
     * Modificateur de l attribut space before.
     * 
     * @param spaceBefore le nouveau space before
     */
    public void setSpaceBefore(int spaceBefore)
    {
        this.spaceBefore = spaceBefore;
    }

    /**
     * Accesseur de l attribut parent header.
     * 
     * @return parent header
     */
    public HeaderCellInfoI getParentHeader()
    {
        return parentHeader;
    }

    /**
     * Modificateur de l attribut parent header.
     * 
     * @param parentHeader le nouveau parent header
     */
    public void setParentHeader(HeaderCellInfoI parentHeader)
    {
        this.parentHeader = parentHeader;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.view.jrxmlinfo.HeaderCellInfoI#getHeight()
     */
    @Override
    public int getHeight()
    {
        return height;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.view.jrxmlinfo.HeaderCellInfoI#setHeight(int)
     */
    @Override
    public void setHeight(int height)
    {
        this.height = height;
    }

    /**
     * Accesseur de l attribut width.
     * 
     * @return width
     */
    public int getWidth()
    {
        return width;
    }

    /**
     * Modificateur de l attribut width.
     * 
     * @param width le nouveau width
     */
    public void setWidth(int width)
    {
        this.width = width;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.view.jrxmlinfo.HeaderCellInfoI#getValue()
     */
    @Override
    public String getValue()
    {
        return value;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.view.jrxmlinfo.HeaderCellInfoI#setValue(java.lang.String)
     */
    @Override
    public void setValue(String value)
    {
        this.value = value;
    }

}
