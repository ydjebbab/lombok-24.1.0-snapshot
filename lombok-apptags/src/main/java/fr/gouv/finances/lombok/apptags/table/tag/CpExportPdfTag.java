/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.apptags.table.tag;

import org.apache.commons.lang.StringUtils;

import fr.gouv.finances.lombok.apptags.table.bean.Export;
import fr.gouv.finances.lombok.apptags.table.core.TableConstants;
import fr.gouv.finances.lombok.apptags.table.core.TableModel;
import fr.gouv.finances.lombok.apptags.table.view.PdfView;
import fr.gouv.finances.lombok.apptags.table.view.html.BuilderConstants;

/**
 * Class CpExportPdfTag
 */
public class CpExportPdfTag extends ExportTag
{

    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** header background color. */
    private String headerBackgroundColor;

    /** header title. */
    private String headerTitle;

    /** header color. */
    private String headerColor;

    /** actionedition. */
    private String actionedition;

    /**
     * Instanciation de cp export pdf tag.
     */
    public CpExportPdfTag()
    {
        super();
    }

    /**
     * Modificateur de l attribut header background color.
     * 
     * @param headerBackgroundColor le nouveau header background color
     * jsp.attribute :  description="The background color on the header column." required="false" rtexprvalue="true"
     */
    public void setHeaderBackgroundColor(String headerBackgroundColor)
    {
        this.headerBackgroundColor = headerBackgroundColor;
    }

    /**
     * Modificateur de l attribut header color.
     * 
     * @param headerColor le nouveau header color
     * jsp.attribute :  description="The font color for the header column." required="false" rtexprvalue="true"
     */
    public void setHeaderColor(String headerColor)
    {
        this.headerColor = headerColor;
    }

    /**
     * Modificateur de l attribut header title.
     * 
     * @param headerTitle le nouveau header title
     * jsp.attribute :  description="The title displayed at the top of the page." required="false" rtexprvalue="true"
     */
    public void setHeaderTitle(String headerTitle)
    {
        this.headerTitle = headerTitle;
    }

    /**
     * Modificateur de l attribut actionedition.
     * 
     * @param actionedition le nouveau actionedition
     */
    public void setActionedition(String actionedition)
    {
        this.actionedition = actionedition;
    }

    /**
     * Accesseur de l attribut actionedition.
     * 
     * @return actionedition
     */
    public String getActionedition()
    {
        return TagUtils.evaluateExpressionAsString("actionedition", actionedition, this, pageContext);
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.tag.ExportTag#addExportAttributes(fr.gouv.finances.lombok.apptags.table.core.TableModel,
     *      fr.gouv.finances.lombok.apptags.table.bean.Export)
     */
    @Override
    public void addExportAttributes(TableModel model, Export export)
    {
        if (StringUtils.isBlank(export.getView()))
        {
            export.setView(TableConstants.VIEW_PDF);
        }

        if (StringUtils.isBlank(export.getViewResolver()))
        {
            export.setViewResolver(TableConstants.VIEW_PDF);
        }

        if (StringUtils.isBlank(export.getImageName()))
        {
            export.setImageName(TableConstants.VIEW_PDF);
        }

        if (StringUtils.isBlank(export.getText()))
        {
            export.setText(BuilderConstants.TOOLBAR_PDF_TEXT);
        }

        export.addAttribute(PdfView.HEADER_BACKGROUND_COLOR, TagUtils.evaluateExpressionAsString(
            "headerBackgroundColor", headerBackgroundColor, this, pageContext));
        export.addAttribute(PdfView.HEADER_COLOR, TagUtils.evaluateExpressionAsString("headerColor", headerColor, this,
            pageContext));
        export.addAttribute(PdfView.HEADER_TITLE, TagUtils.evaluateExpressionAsString("headerTitle", headerTitle, this,
            pageContext));
        export.addAttribute("actionedition", TagUtils.evaluateExpressionAsString("actionedition", actionedition, this,
            pageContext));
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.tag.ExportTag#release()
     */
    @Override
    public void release()
    {
        headerBackgroundColor = null;
        headerTitle = null;
        headerColor = null;
        super.release();
    }

}
