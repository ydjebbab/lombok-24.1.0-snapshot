/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.apptags.table.view.jrxmlinfo;

import java.util.ArrayList;
import java.util.List;

/**
 * Class SummaryInfo
 */
public class SummaryInfo extends AbstractBandInfo
{

    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** graphiques infos. */
    private List<GraphiqueInfo> graphiquesInfos = new ArrayList<GraphiqueInfo>();

    /** sous rapports infos. */
    private List<SousRapportInfo> sousRapportsInfos = new ArrayList<SousRapportInfo>();

    /** padding. */
    private int padding;

    /** summary new page with page header and footer. */
    private boolean summaryNewPageWithPageHeaderAndFooter;

    /**
     * Instanciation de summary info.
     */
    public SummaryInfo()
    {
        super();
    }
    
    /**
     * Verifie si summary new page with page header and footer.
     * 
     * @return true, si c'est summary new page with page header and footer
     */
    public boolean isSummaryNewPageWithPageHeaderAndFooter()
    {
        return summaryNewPageWithPageHeaderAndFooter;
    }

    /**
     * Modificateur de l attribut summary new page with page header and footer.
     * 
     * @param summaryNewPageWithPageHeaderAndFooter le nouveau summary new page with page header and footer
     */
    public void setSummaryNewPageWithPageHeaderAndFooter(boolean summaryNewPageWithPageHeaderAndFooter)
    {
        this.summaryNewPageWithPageHeaderAndFooter = summaryNewPageWithPageHeaderAndFooter;
    }

    /**
     * Accesseur de l attribut padding.
     * 
     * @return padding
     */
    public int getPadding()
    {
        return padding;
    }

    /**
     * Modificateur de l attribut padding.
     * 
     * @param padding le nouveau padding
     */
    public void setPadding(int padding)
    {
        this.padding = padding;
    }

    /**
     * Accesseur de l attribut graphiques infos.
     * 
     * @return the graphiquesInfos
     */
    public List<GraphiqueInfo> getGraphiquesInfos()
    {
        return graphiquesInfos;
    }

    /**
     * Modificateur de l attribut graphiques infos.
     * 
     * @param graphiquesInfos the graphiquesInfos to set
     */
    public void setGraphiquesInfos(List<GraphiqueInfo> graphiquesInfos)
    {
        this.graphiquesInfos = graphiquesInfos;
    }

    /**
     * Accesseur de sousRapportsInfos.
     * 
     * @return sousRapportsInfos
     */
    public List<SousRapportInfo> getSousRapportsInfos()
    {
        return sousRapportsInfos;
    }

    /**
     * Mutateur de sousRapportsInfos.
     * 
     * @param sousRapportsInfos sousRapportsInfos
     */
    public void setSousRapportsInfos(List<SousRapportInfo> sousRapportsInfos)
    {
        this.sousRapportsInfos = sousRapportsInfos;
    }

}
