/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.apptags.table.view;

import fr.gouv.finances.lombok.apptags.table.bean.Attributes;
import fr.gouv.finances.lombok.apptags.table.bean.Table;
import fr.gouv.finances.lombok.apptags.table.core.TableModel;
import fr.gouv.finances.lombok.apptags.table.tag.TableTag;

/**
 * Class CpTableTag
 */
public class CpTableTag extends TableTag
{

    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = 459094623915915012L;

    /** Constant : ACTION_EDITION. */
    public static final String ACTION_EDITION = "actionEdition";

    /** action edition. */
    private String actionEdition;

    /**
     * Instanciation de cp table tag.
     */
    public CpTableTag()
    {
        super();
    }

    /**
     * Accesseur de l attribut action edition.
     * 
     * @return action edition
     */
    public String getActionEdition()
    {
        return actionEdition;
    }

    /**
     * Modificateur de l attribut action edition.
     * 
     * @param actionEdition le nouveau action edition
     */
    public void setActionEdition(String actionEdition)
    {
        this.actionEdition = actionEdition;
    }

    /**
     * methode Adds the extended attributes :
     * 
     * @param attributes
     */
    public void addExtendedAttributes(Attributes attributes)
    {
        attributes.addAttribute(CpTableTag.ACTION_EDITION, actionEdition);

    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.tag.TableTag#addTableAttributes(fr.gouv.finances.lombok.apptags.table.core.TableModel,
     *      fr.gouv.finances.lombok.apptags.table.bean.Table)
     */
    @Override
    public void addTableAttributes(TableModel model, Table table)
    {
        table.addAttribute(CpTableTag.ACTION_EDITION, actionEdition);
    }
}
