/*
/*Copyright (c) 2014 DGFiP - Tous droits réservés/* * Copyright 2004 original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package fr.gouv.finances.lombok.apptags.table.cell;

import org.apache.commons.lang.StringUtils;

import fr.gouv.finances.lombok.apptags.table.bean.Column;
import fr.gouv.finances.lombok.apptags.table.core.TableModel;
import fr.gouv.finances.lombok.apptags.util.HtmlBuilder;

/**
 * Class SelectAllHeaderCell
 */
public class SelectAllHeaderCell implements Cell
{

    public SelectAllHeaderCell()
    {
        super();        
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.cell.Cell#getExportDisplay(fr.gouv.finances.lombok.apptags.table.core.TableModel,
     *      fr.gouv.finances.lombok.apptags.table.bean.Column)
     */
    @Override
    public String getExportDisplay(TableModel model, Column column)
    {
        return column.getTitle();
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.cell.Cell#getHtmlDisplay(fr.gouv.finances.lombok.apptags.table.core.TableModel,
     *      fr.gouv.finances.lombok.apptags.table.bean.Column)
     */
    @Override
    public String getHtmlDisplay(TableModel model, Column column)
    {
        HtmlBuilder html = new HtmlBuilder();

        html.td(2);

        if (StringUtils.isNotEmpty(column.getHeaderClass()))
        {
            html.styleClass(column.getHeaderClass());
        }

        if (column.isShowResponsive())
        {
            html.dataAttributes(column.getDataAttributes());
        }
        
        StringBuilder   styleValue = new StringBuilder();

        if (!column.isShowResponsive() && StringUtils.isNotEmpty(column.getWidth()))
        {
            styleValue.append("width:");
            styleValue.append(column.getWidth());
            styleValue.append(";");

            // html.width(column.getWidth());
        }

        if (StringUtils.isNotEmpty(column.getHeaderStyle()))
        {
            styleValue.append(column.getHeaderStyle());
            // html.style(column.getHeaderStyle());
            
            if (column.isShowResponsive())
            {
                html.dataAttributes(column.getDataAttributes());
            }
            
        }

        if (StringUtils.isNotEmpty(styleValue.toString()))
        {
            html.style(styleValue.toString());
        }

        html.close();

        String controlName = column.getAlias() + "_selector";
        String selectableControlName = column.getAlias();

        html.input("checkbox");
        html.id(controlName);
        html.name(controlName);
        html.title("(Un)Select All");

        html.onclick("for(i = 0; i < document.getElementsByName('" + selectableControlName
            + "').length; i++)document.getElementsByName('" + selectableControlName
            + "').item(i).checked=document.getElementById('" + controlName + "').checked;");
        html.close();

        html.tdEnd();

        return html.toString();
    }
}
