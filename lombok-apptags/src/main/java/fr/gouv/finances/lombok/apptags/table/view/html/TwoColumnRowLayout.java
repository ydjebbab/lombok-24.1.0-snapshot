/*
 * Copyright (c) 2014 DGFiP - Tous droits réservés
 *//*
/*Copyright (c) 2014 DGFiP - Tous droits réservés/* * Copyright 2004 original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package fr.gouv.finances.lombok.apptags.table.view.html;

import fr.gouv.finances.lombok.apptags.table.core.TableModel;
import fr.gouv.finances.lombok.apptags.util.HtmlBuilder;

/**
 * Class TwoColumnRowLayout
 */
public abstract class TwoColumnRowLayout
{

    /** html. */
    private HtmlBuilder html;

    /** model. */
    private TableModel model;

    /**
     * Instanciation de two column row layout.
     * 
     * @param html
     * @param model
     */
    public TwoColumnRowLayout(HtmlBuilder html, TableModel model)
    {
        this.html = html;
        this.model = model;
    }

    /**
     * Accesseur de l attribut html builder.
     * 
     * @return html builder
     */
    protected HtmlBuilder getHtmlBuilder()
    {
        return html;
    }

    /**
     * Accesseur de l attribut table model.
     * 
     * @return table model
     */
    protected TableModel getTableModel()
    {
        return model;
    }

    /**
     * methode Layout :
     */
    public void layout()
    {
        if (!showLayout(model))
        {
            return;
        }

        if (model.getTableHandler().getTable().isShowResponsive())
        {
            html.div().styleClass("tableBarHeader").close();
            columnLeftRWD(html, model);
            columnRightRWD(html, model);
            html.divEnd();
        } else
        {

            html.tr(1).styleClass("noPadding").close();

            html.td(2).colSpan(String.valueOf(model.getColumnHandler().columnCount())).close();

            html.table(2).styleClass("noBorder noCellPadding noCellSpacing fullWidth").width("100%").close();
            html.tr(3).close();

        // layout area left
        columnLeft(html, model);

        // layout area right
        columnRight(html, model);

        html.trEnd(3);
        html.tableEnd(2);
        html.newline();
        html.tabs(2);

        html.tdEnd();
        html.trEnd(1);
        html.tabs(2);
        html.newline();
    }
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        return html.toString();
    }

    /**
     * methode Show layout :
     * 
     * @param model
     * @return true, si c'est vrai
     */
    protected abstract boolean showLayout(TableModel model);

    /**
     * methode Column left :
     * 
     * @param html
     * @param model
     */
    protected abstract void columnLeft(HtmlBuilder html, TableModel model);

    
    /**
     * methode Column left RWD :
     * 
     * @param html
     * @param model
     */
    protected abstract void columnLeftRWD(HtmlBuilder html, TableModel model);

    

    /**
     * methode Column right :
     * 
     * @param html
     * @param model
     */
    protected abstract void columnRight(HtmlBuilder html, TableModel model);
    
    
    /**
     * methode Column right RWD :
     * 
     * @param html
     * @param model
     */
    protected abstract void columnRightRWD(HtmlBuilder html, TableModel model);
    
}
