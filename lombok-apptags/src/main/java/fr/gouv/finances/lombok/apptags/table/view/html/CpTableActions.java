/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.apptags.table.view.html;

import org.apache.commons.lang.StringUtils;

import fr.gouv.finances.lombok.apptags.table.bean.Column;
import fr.gouv.finances.lombok.apptags.table.core.TableConstants;
import fr.gouv.finances.lombok.apptags.table.core.TableModel;
import fr.gouv.finances.lombok.apptags.table.limit.Sort;

/**
 * Class CpTableActions
 */
public class CpTableActions
{

    /** model. */
    private final TableModel model;

    /**
     * Instanciation de cp table actions.
     * 
     * @param model
     */
    public CpTableActions(TableModel model)
    {
        this.model = model;
    }

    /**
     * Accesseur de l attribut table model.
     * 
     * @return table model
     */
    protected TableModel getTableModel()
    {
        return model;
    }

    /**
     * Accesseur de l attribut on invoke action.
     * 
     * @return on invoke action
     */
    public String getOnInvokeAction()
    {
        String result = "";
        String onInvokeAction = model.getTableHandler().getTable().getOnInvokeAction();

        if (StringUtils.isNotBlank(onInvokeAction))
        {
            result = onInvokeAction;
        }
        else
        {
            result = getSubmitAction();
        }
        return result;
    }

    /**
     * Accesseur de l attribut submit action.
     * 
     * @return submit action
     */
    public String getSubmitAction()
    {
        return getSubmitAction(null);
    }

    /**
     * Accesseur de l attribut submit action.
     * 
     * @param parameters
     * @return submit action
     */
    public String getSubmitAction(String parameters)
    {
        StringBuilder   result = new StringBuilder(100);
        String form = BuilderUtils.getForm(model);
        StringBuilder   action = new StringBuilder(50);

        action.append(model.getTableHandler().getTable().getAction());
        if (parameters != null && StringUtils.isNotBlank(parameters))
        {
            action.append(parameters);
        }

        result.append("document.forms.");
        result.append(form);
        result.append(".setAttribute('action', '");
        result.append(action);
        result.append("');");

        String method = model.getTableHandler().getTable().getMethod();

        result.append("document.forms.");
        result.append(form);
        result.append(".setAttribute('method', '");
        result.append(method);
        result.append("');");

        result.append("document.forms.");
        result.append(form);
        result.append(".submit()");

        return result.toString();
    }

    /**
     * Accesseur de l attribut form parameter.
     * 
     * @param name
     * @param value
     * @return form parameter
     */
    public String getFormParameter(String name, String value)
    {
        StringBuilder   result = new StringBuilder(30);
        String form = BuilderUtils.getForm(model);

        result.append("document.forms.");
        result.append(form);
        result.append('.');
        result.append(model.getTableHandler().prefixWithTableId());
        result.append(name);
        result.append(".value='");
        result.append(value);
        result.append("';");

        return result.toString();
    }

    /**
     * Accesseur de l attribut export action.
     * 
     * @param exportView
     * @param exportFileName
     * @return export action
     */
    public String getExportAction(String exportView, String exportFileName)
    {
        StringBuilder   result = new StringBuilder("javascript:");

        result.append(getFormParameter(TableConstants.EXPORT_VIEW, exportView));
        result.append(getFormParameter(TableConstants.EXPORT_FILE_NAME, exportFileName));

        StringBuilder   newwin = new StringBuilder(60);
        String form = BuilderUtils.getForm(model);

        newwin.append("document.forms.");
        newwin.append(form).append('.');
        newwin.append("target");
        newwin.append("='_blank'");
        newwin.append(';');
        result.append(newwin);

        String tableId = model.getTableHandler().getTable().getTableId();

        StringBuilder   tableidstr = new StringBuilder(40);

        tableidstr.append("document.forms.");
        tableidstr.append(form);
        tableidstr.append('.');
        tableidstr.append(TableConstants.EXPORT_TABLE_ID);
        tableidstr.append(".value='");
        tableidstr.append(tableId);
        tableidstr.append("';");
        result.append(tableidstr);

        result.append(getSubmitAction(""));
        return result.toString();
    }

    /**
     * Accesseur de l attribut page action.
     * 
     * @param page
     * @return page action
     */
    public String getPageAction(int page)
    {
        StringBuilder   action = new StringBuilder("javascript:");

        action.append(getFormParameter(TableConstants.PAGE, "" + page));
        action.append(getOnInvokeAction());

        return action.toString();
    }

    /**
     * Accesseur de l attribut filter action.
     * 
     * @return filter action
     */
    public String getFilterAction()
    {
        StringBuilder   action = new StringBuilder("javascript:");

        action.append(getFormParameter(TableConstants.FILTER + TableConstants.ACTION, TableConstants.FILTER_ACTION));
        action.append(getFormParameter(TableConstants.PAGE, "1"));
        action.append(getOnInvokeAction());

        return action.toString();
    }

    /**
     * Accesseur de l attribut clear action.
     * 
     * @return clear action
     */
    public String getClearAction()
    {
        StringBuilder   action = new StringBuilder("javascript:");

        action.append(getFormParameter(TableConstants.FILTER + TableConstants.ACTION, TableConstants.CLEAR_ACTION));
        action.append(getFormParameter(TableConstants.PAGE, "1"));
        action.append(getOnInvokeAction());

        return action.toString();
    }

    /**
     * Accesseur de l attribut sort action.
     * 
     * @param column
     * @param sortOrder
     * @return sort action
     */
    public String getSortAction(Column column, String sortOrder)
    {
        StringBuilder   action = new StringBuilder("javascript:");
        Sort sort = model.getLimit().getSort();

        if (sort.isSorted())
        {
            // set the old sort back
            action.append(getFormParameter(TableConstants.SORT + sort.getAlias(), ""));
        }

        // set sort on current column
        action.append(getFormParameter(TableConstants.SORT + column.getAlias(), sortOrder));
        action.append(getFormParameter(TableConstants.PAGE, "1"));
        action.append(getOnInvokeAction());

        return action.toString();
    }

    /**
     * Accesseur de l attribut rows displayed action.
     * 
     * @return rows displayed action
     */
    public String getRowsDisplayedAction()
    {
        StringBuilder   action = new StringBuilder("javascript:");

        action.append(getRowsDisplayedFormParameter(TableConstants.CURRENT_ROWS_DISPLAYED));
        action.append(getFormParameter(TableConstants.PAGE, "1"));
        action.append(getOnInvokeAction());

        return action.toString();
    }

    /**
     * Accesseur de l attribut rows displayed form parameter.
     * 
     * @param name
     * @return rows displayed form parameter
     */
    protected String getRowsDisplayedFormParameter(String name)
    {
        StringBuilder   result = new StringBuilder(50);
        String form = BuilderUtils.getForm(model);
        String selectedOption = "this.options[this.selectedIndex].value";

        result.append("document.forms.");
        result.append(form).append('.');
        result.append(model.getTableHandler().prefixWithTableId());
        result.append(name);
        result.append(".value=");
        result.append(selectedOption);
        result.append(';');

        return result.toString();
    }
}
