/*
 * Copyright (c) 2014 DGFiP - Tous droits réservés
 */
/*
/*Copyright (c) 2014 DGFiP - Tous droits réservés/* * Copyright 2004 original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package fr.gouv.finances.lombok.apptags.table.core;

/**
 * Class PreferencesConstants
 */
public class PreferencesConstants
{

    /** Constant : TABLE_AUTO_INCLUDE_PARAMETERS. */
    public static final String TABLE_AUTO_INCLUDE_PARAMETERS = "table.autoIncludeParameters";

    /** Constant : TABLE_BORDER. */
    public static final String TABLE_BORDER = "table.border";

    /** Constant : TABLE_BUFFER_VIEW. */
    public static final String TABLE_BUFFER_VIEW = "table.bufferView";

    /** Constant : TABLE_CELLPADDING. */
    public static final String TABLE_CELLPADDING = "table.cellpadding";

    /** Constant : TABLE_CELLSPACING. */
    public static final String TABLE_CELLSPACING = "table.cellspacing";

    /** Constant : TABLE_COMPACT. */
    public static final String TABLE_COMPACT = "table.compact";

    /** Constant : TABLE_EXPORTABLE. */
    public static final String TABLE_EXPORTABLE = "table.exportable";

    /** Constant : TABLE_FILTERABLE. */
    public static final String TABLE_FILTERABLE = "table.filterable";

    /** Constant : TABLE_FILTER_ROWS_CALLBACK. */
    public static final String TABLE_FILTER_ROWS_CALLBACK = "table.filterRowsCallback.";

    /** Constant : TABLE_HEADER_CLASS. */
    public static final String TABLE_HEADER_CLASS = "table.headerClass";

    /** Constant : TABLE_HEADER_SORT_CLASS. */
    public static final String TABLE_HEADER_SORT_CLASS = "table.headerSortClass";

    /** Constant : TABLE_IMAGE_PATH. */
    public static final String TABLE_IMAGE_PATH = "table.imagePath";

    /** Constant : TABLE_INTERCEPTOR. */
    public static final String TABLE_INTERCEPTOR = "table.interceptor.";

    /** Constant : TABLE_LOCALE. */
    public static final String TABLE_LOCALE = "table.locale";

    /** Constant : TABLE_MAX_ROWS_DISPLAYED. */
    public static final String TABLE_MAX_ROWS_DISPLAYED = "table.maxRowsDisplayed";

    /** Constant : TABLE_MEDIAN_ROWS_DISPLAYED. */
    public static final String TABLE_MEDIAN_ROWS_DISPLAYED = "table.medianRowsDisplayed";

    /** Constant : TABLE_METHOD. */
    public static final String TABLE_METHOD = "table.method";

    /** Constant : TABLE_SHOW_RESPONSIVE. */
    public static final String TABLE_SHOW_RESPONSIVE = "table.showResponsive";

    /** Constant : TABLE_DATA_ATTRIBUTES. */
    public static final String TABLE_DATA_ATTRIBUTES = "table.dataAttributes";

    /** Constant : TABLE_PAGINATION_CLASS */
    public static final String TABLE_PAGINATION_CLASS = "table.paginationClass";

    /** Constant : TABLE_EXPORTS_CLASS */
    public static final String TABLE_EXPORTS_CLASS = "table.exportsClass";

    /** Constant : TABLE_RETRIEVE_ROWS_CALLBACK. */
    public static final String TABLE_RETRIEVE_ROWS_CALLBACK = "table.retrieveRowsCallback.";

    /** Constant : TABLE_ROWS_DISPLAYED. */
    public static final String TABLE_ROWS_DISPLAYED = "table.rowsDisplayed";

    /** Constant : TABLE_SHOW_EXPORTS. */
    public static final String TABLE_SHOW_EXPORTS = "table.showExports";

    /** Constant : TABLE_SHOW_PAGINATION. */
    public static final String TABLE_SHOW_PAGINATION = "table.showPagination";

    /** Constant : TABLE_SHOW_PAGINATION_BOTTOM. */
    public static final String TABLE_SHOW_PAGINATION_BOTTOM = "table.showPaginationBottom";

    /** Constant : TABLE_SHOW_EXPORTS_BOTTOM. */
    public static final String TABLE_SHOW_EXPORTS_BOTTOM = "table.showExportsBottom";

    /** Constant : TABLE_SHOW_NEW_PAGINATION. */
    public static final String TABLE_SHOW_NEW_PAGINATION = "table.showNewPagination";

    /** Constant : TABLE_SHOW_NEW_FILTER. */
    public static final String TABLE_SHOW_NEW_FILTER = "table.showNewFilter";

    /** Constant : TABLE_NEW_PAGINATION_NB_PAGE. */
    public static final String TABLE_NEW_PAGINATION_NB_PAGE = "table.newPaginationNbPage";

    /** Constant : TABLE_SHOW_STATUS_BAR. */
    public static final String TABLE_SHOW_STATUS_BAR = "table.showStatusBar";

    /** Constant : TABLE_SHOW_TITLE. */
    public static final String TABLE_SHOW_TITLE = "table.showTitle";

    /** Constant : TABLE_SHOW_TOOLTIPS. */
    public static final String TABLE_SHOW_TOOLTIPS = "table.showTooltips";

    /** Constant : TABLE_SORTABLE. */
    public static final String TABLE_SORTABLE = "table.sortable";

    /** Constant : TABLE_SORT_ROWS_CALLBACK. */
    public static final String TABLE_SORT_ROWS_CALLBACK = "table.sortRowsCallback.";

    /** Constant : TABLE_STATE. */
    public static final String TABLE_STATE = "table.state.";

    /** Constant : TABLE_STATE_ATTR. */
    public static final String TABLE_STATE_ATTR = "table.stateAttr";

    /** Constant : TABLE_STYLE_CLASS. */
    public static final String TABLE_STYLE_CLASS = "table.styleClass";

    /** Constant : TABLE_THEME. */
    public static final String TABLE_THEME = "table.theme";

    /** Constant : TABLE_VIEW. */
    public static final String TABLE_VIEW = "table.view.";

    /** Constant : TABLE_WIDTH. */
    public static final String TABLE_WIDTH = "table.width";

    /** Constant : ROW_HIGHLIGHT_CLASS. */
    public static final String ROW_HIGHLIGHT_CLASS = "row.highlightClass";

    /** Constant : ROW_HIGHLIGHT_ROW. */
    public static final String ROW_HIGHLIGHT_ROW = "row.highlightRow";

    /** Constant : ROW_INTERCEPTOR. */
    public static final String ROW_INTERCEPTOR = "row.interceptor.";

    /** Constant : COLUMN_ESCAPE_XML. */
    public static final String COLUMN_ESCAPE_XML = "column.escapeXml";

    /** Constant : COLUMN_CALC. */
    public static final String COLUMN_CALC = "column.calc.";

    /** Constant : COLUMN_CELL. */
    public static final String COLUMN_CELL = "column.cell.";

    /** Constant : COLUMN_CALC_CLASS. */
    public static final String COLUMN_CALC_CLASS = "column.calc.class";

    /** Constant : COLUMN_CALCTITLE_CLASS. */
    public static final String COLUMN_CALCTITLE_CLASS = "column.calctitle.class";

    /** Constant : COLUMN_ESCAPE_AUTO_FORMAT. */
    public static final String COLUMN_ESCAPE_AUTO_FORMAT = "column.escapeAutoFormat";

    /** Constant : COLUMN_FILTER_CELL. */
    public static final String COLUMN_FILTER_CELL = "column.filterCell.";

    /** Constant : COLUMN_FORMAT. */
    public static final String COLUMN_FORMAT = "column.format.";

    /** Constant : COLUMN_HEADER_CELL. */
    public static final String COLUMN_HEADER_CELL = "column.headerCell.";

    /** Constant : COLUMN_INTERCEPTOR. */
    public static final String COLUMN_INTERCEPTOR = "column.interceptor.";

    /** Constant : COLUMN_PARSE. */
    public static final String COLUMN_PARSE = "column.parse.";

    /** Constant : COLUMN_FILTER_STRICT. */
    public static final String COLUMN_FILTER_STRICT = "column.filterStrict";

    /** Constant : COLUMN_FILTER_NULL_OPTION. */
    public static final String COLUMN_FILTER_NULL_OPTION = "column.filterNullOption";

    /** Constant : COLUMNS_AUTO_GENERATE_COLUMNS. */
    public static final String COLUMNS_AUTO_GENERATE_COLUMNS = "columns.autoGenerateColumns.";

    /** Constant : EXPORT_ENCODING. */
    public static final String EXPORT_ENCODING = "export.encoding";

    /** Constant : EXPORT_INTERCEPTOR. */
    public static final String EXPORT_INTERCEPTOR = "export.interceptor.";

    /** Constant : EXPORT_VIEW. */
    public static final String EXPORT_VIEW = "export.view.";

    /** Constant : EXPORT_VIEW_RESOLVER. */
    public static final String EXPORT_VIEW_RESOLVER = "export.viewResolver.";

    /** Constant : EXPORT_EDITION_SERVICE. */
    public static final String EXPORT_EDITION_SERVICE = "export.editionService.";

    /** Constant : DEFAULT_CALC_LAYOUT. */
    public static final String DEFAULT_CALC_LAYOUT = "defaultCalcLayout";

    /** Constant : MESSAGES. */
    public static final String MESSAGES = "messages";

    /** Constant : TOOLBAR_SHOW_ROWS_DISPLAYED. */
    public static final String TOOLBAR_SHOW_ROWS_DISPLAYED = "toolbar.showRowsDisplayed";

    /** Constant : TABLE_SHOW_EXPORTS_WITH_LIMIT_SIZE_PDF. */
    public static final String TABLE_SHOW_EXPORTS_WITH_LIMIT_SIZE_PDF = "table.showExportsWithLimitSizePdf";

    /** Constant : TABLE_SHOW_EXPORTS_WITH_LIMIT_SIZE_XLS_ODS. */
    public static final String TABLE_SHOW_EXPORTS_WITH_LIMIT_SIZE_XLS_ODS = "table.showExportsWithLimitSizeXlsOrOds";

    /** Constant : TABLE_SHOW_EXPORTS_WITH_LIMIT_SIZE_CSV. */
    public static final String TABLE_SHOW_EXPORTS_WITH_LIMIT_SIZE_CSV = "table.showExportsWithLimitSizeCsv";

    public PreferencesConstants()
    {
    }

}
