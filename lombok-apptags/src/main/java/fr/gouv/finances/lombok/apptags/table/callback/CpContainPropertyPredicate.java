/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.apptags.table.callback;

import java.lang.reflect.InvocationTargetException;

import org.apache.commons.beanutils.PropertyUtilsBean;
import org.apache.commons.collections.Predicate;

/**
 * Class CpContainPropertyPredicate
 */
public class CpContainPropertyPredicate implements Predicate
{

    /** La propriété dont on cherche le getter. */
    private final String property;

    /**
     * Instanciation de cp contain property predicate.
     * 
     * @param property
     */
    public CpContainPropertyPredicate(String property)
    {
        super();
        this.property = property;
    }

    /**
     * Acceder à l unique instance de CpContainPropertyPredicate.
     * 
     * @param property
     * @return l unique instance de CpContainPropertyPredicate
     */
    public static Predicate getInstance(String property)
    {
        if (property == null)
        {
            throw new IllegalArgumentException("La propriété ne doit pas être nulle");
        }
        return new CpContainPropertyPredicate(property);
    }

    /**
     * methode Evaluate :
     * 
     * @param object
     * @return true, si c'est vrai
     */
    @Override
    public boolean evaluate(Object object)
    {

        boolean objectProperty = true;

        PropertyUtilsBean propUtilsBean = new PropertyUtilsBean();

        if (this.property == null || object == null)
        {
            objectProperty = false;
        }
        else
        {
            try
            {
                propUtilsBean.getProperty(object, this.property);
            }
            catch (IllegalAccessException e)
            {
                objectProperty = false;
            }
            catch (InvocationTargetException e)
            {
                objectProperty = false;
            }
            catch (NoSuchMethodException e)
            {
                objectProperty = false;
            }
            catch (IllegalArgumentException e)
            {
                objectProperty = false;
            }
        }
        return objectProperty;
    }
}
