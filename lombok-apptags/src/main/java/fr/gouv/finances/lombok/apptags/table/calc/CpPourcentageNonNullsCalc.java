/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.apptags.table.calc;

import java.math.BigDecimal;
import java.util.Collection;

import fr.gouv.finances.lombok.apptags.table.bean.Column;
import fr.gouv.finances.lombok.apptags.table.core.TableModel;

/**
 * Class CpPourcentageNonNullsCalc
 */
public class CpPourcentageNonNullsCalc implements Calc
{

    /**
     * Constructeur de la classe CpPourcentageNonNullsCalc.java
     *
     */
    public CpPourcentageNonNullsCalc()
    {
        super();
        
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.calc.Calc#getCalcResult(fr.gouv.finances.lombok.apptags.table.core.TableModel,
     *      fr.gouv.finances.lombok.apptags.table.bean.Column)
     */
    @Override
    public Number getCalcResult(TableModel model, Column column)
    {
        Collection<Object> rows = model.getCollectionOfFilteredBeans();
        String property = column.getProperty();
        DecompteValue decompteValue = new DecompteValue(rows.size());
        CpCalcUtils.eachRowCalcValueDecompteNonNull(decompteValue, rows, property);

        return decompteValue.getAverageValue();
    }

    /**
     * Class DecompteValue
     */
    private static class DecompteValue implements CalcHandler
    {

        /** decompte. */
        private double decompte = 0;

        /** row count. */
        private double rowCount;

        /**
         * Instanciation de decompte value.
         * 
         * @param rowCount
         */
        public DecompteValue(double rowCount)
        {
            this.rowCount = rowCount;
        }

        /**
         * (methode de remplacement) {@inheritDoc}
         * 
         * @see fr.gouv.finances.lombok.apptags.table.calc.CalcHandler#processCalcValue(java.lang.Number)
         */
        @Override
        public void processCalcValue(Number calcValue)
        {
            if (calcValue != null)
            {
                decompte++;
            }
        }

        /**
         * Accesseur de l attribut average value.
         * 
         * @return average value
         */
        public Number getAverageValue()
        {
            if (rowCount > 0)
            {
                return new BigDecimal(decompte / rowCount * 100);
            }

            return new BigDecimal("0.00");
        }
    }

}
