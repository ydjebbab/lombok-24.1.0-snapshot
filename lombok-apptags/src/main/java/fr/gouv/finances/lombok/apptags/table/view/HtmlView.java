/*
 * Copyright (c) 2014 DGFiP - Tous droits réservés
 *//*
/*Copyright (c) 2014 DGFiP - Tous droits réservés/* * Copyright 2004 original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package fr.gouv.finances.lombok.apptags.table.view;

import fr.gouv.finances.lombok.apptags.table.core.TableModel;
import fr.gouv.finances.lombok.apptags.table.view.html.BuilderUtils;
import fr.gouv.finances.lombok.apptags.util.HtmlBuilder;

/**
 * Class HtmlView
 */
public class HtmlView extends AbstractHtmlView
{

    /**
     * Constructeur de la classe HtmlView.java
     */
    public HtmlView()
    {
        super();

    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.view.AbstractHtmlView#beforeBodyInternal(fr.gouv.finances.lombok.apptags.table.core.TableModel)
     */
    @Override
    protected void beforeBodyInternal(TableModel model)
    {
        boolean showPagination = BuilderUtils.showPagination(model);
        boolean showExports = BuilderUtils.showExports(model);
        
        if (showPagination || showExports)
        {
            toolbar(getHtmlBuilder(), getTableModel());
        }

        getTableBuilder().tableStart();

        getTableBuilder().theadStart();

        if (model.getTableHandler().getTable().isShowResponsive())
        {
            getTableBuilder().headerRow();
            getTableBuilder().filterRow();
            statusBar(getHtmlBuilder(), getTableModel());
        }
        else
        {
            statusBar(getHtmlBuilder(), getTableModel());

            getTableBuilder().filterRow();

            getTableBuilder().headerRow();
        }

        getTableBuilder().theadEnd();

      //  getTableBuilder().tbodyStart();
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.view.AbstractHtmlView#afterBodyInternal(fr.gouv.finances.lombok.apptags.table.core.TableModel)
     */
    @Override
    protected void afterBodyInternal(TableModel model)
    {
        boolean showPaginationBottom = BuilderUtils.showPaginationBottom(model);
        boolean showExportsBottom = BuilderUtils.showExportsBottom(model);

        getCpCalcBuilder().defaultCalcLayout();

      //  getTableBuilder().tbodyEnd();

        getTableBuilder().tableEnd();
        if (showPaginationBottom || showExportsBottom)
        {
            toolbarBottom(getHtmlBuilder(), getTableModel());
        }
    }

    /**
     * methode Toolbar :
     * 
     * @param html
     * @param model
     */
    protected void toolbar(HtmlBuilder html, TableModel model)
    {
        new DefaultToolbar(html, model).layout();
    }

    /**
     * methode Toolbar bottom :
     * 
     * @param html
     * @param model
     */
    protected void toolbarBottom(HtmlBuilder html, TableModel model)
    {
        new DefaultToolbar(html, model).layout();
    }

    /**
     * methode Status bar :
     * 
     * @param html
     * @param model
     */
    protected void statusBar(HtmlBuilder html, TableModel model)
    {
        new DefaultStatusBar(html, model).layout();
    }
}
