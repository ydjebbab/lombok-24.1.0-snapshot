/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.apptags.table.view.jrxmlinfo;

import java.util.ArrayList;
import java.util.List;

/**
 * Class GroupHeaderInfo
 */
public class GroupHeaderInfo implements HeaderCellInfoI
{

    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** previous group. */
    private GroupHeaderInfo previousGroup;

    /** subgroups headers infos. */
    private List<SubgroupHeaderInfo> subgroupsHeadersInfos = new ArrayList<SubgroupHeaderInfo>();

    /** titles headers infos. */
    private List<TitleHeaderInfo> titlesHeadersInfos = new ArrayList<TitleHeaderInfo>();

    /** value. */
    private String value;

    /** height. */
    private int height;

    /** space before. */
    private int spaceBefore;

    /** merge with child. */
    private boolean mergeWithChild = false;

    /** absorbe. */
    private boolean absorbe = false;
    
    /**
     * Instanciation de group header info.
     */
    public GroupHeaderInfo()
    {
        super();
    }

    /**
     * Calcule la largeur de la cellule représentant le groupe.
     * 
     * @return width calcule
     */
    @Override
    public int getWidthCalcule()
    {
        int wid = 0;
        if (titlesHeadersInfos != null && titlesHeadersInfos.size() > 0)
        {
            for (TitleHeaderInfo title : titlesHeadersInfos)
            {
                wid = wid + title.getWidth();
            }
        }
        else if (subgroupsHeadersInfos != null && subgroupsHeadersInfos.size() > 0)
        {
            for (SubgroupHeaderInfo title : subgroupsHeadersInfos)
            {
                wid = wid + title.getWidthCalcule();
            }
        }

        return wid;
    }

    /**
     * Calcule l'abscisse du coin haut gauche de la cellule représentant le groupe.
     * 
     * @return x left corner calcule
     */
    @Override
    public int getXLeftCornerCalcule()
    {
        int abs = 0;
        if (this.previousGroup != null)
        {
            abs = abs + this.previousGroup.getXLeftCornerCalcule() + this.previousGroup.getWidthCalcule();
        }

        return abs;
    }

    /**
     * methode Ajouter un titre au groupe :
     * 
     * @param titleHeaderInfo
     * @return group header info
     */
    public GroupHeaderInfo ajouterUnTitreAuGroupe(TitleHeaderInfo titleHeaderInfo)
    {
        titleHeaderInfo.rejoindreSousGroupeOuGroupe(this);
        this.titlesHeadersInfos.add(titleHeaderInfo);

        return this;
    }

    /**
     * methode Ajouter un sous groupe au groupe :
     * 
     * @param subgroupHeaderInfo
     * @return group header info
     */
    public GroupHeaderInfo ajouterUnSousGroupeAuGroupe(SubgroupHeaderInfo subgroupHeaderInfo)
    {
        subgroupHeaderInfo.rejoindreLeGroupe(this);
        this.subgroupsHeadersInfos.add(subgroupHeaderInfo);

        return this;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.view.jrxmlinfo.HeaderCellInfoI#isAbsorbe()
     */
    @Override
    public boolean isAbsorbe()
    {
        return absorbe;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.view.jrxmlinfo.HeaderCellInfoI#setAbsorbe(boolean)
     */
    @Override
    public void setAbsorbe(boolean absorbe)
    {
        this.absorbe = absorbe;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.view.jrxmlinfo.HeaderCellInfoI#isMergeWithChild()
     */
    @Override
    public boolean isMergeWithChild()
    {
        return mergeWithChild;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.view.jrxmlinfo.HeaderCellInfoI#setMergeWithChild(boolean)
     */
    @Override
    public void setMergeWithChild(boolean mergeWithChild)
    {
        this.mergeWithChild = mergeWithChild;
    }

    /**
     * Accesseur de l attribut previous group.
     * 
     * @return previous group
     */
    public GroupHeaderInfo getPreviousGroup()
    {
        return previousGroup;
    }

    /**
     * Modificateur de l attribut previous group.
     * 
     * @param previousGroup le nouveau previous group
     */
    public void setPreviousGroup(GroupHeaderInfo previousGroup)
    {
        this.previousGroup = previousGroup;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.view.jrxmlinfo.HeaderCellInfoI#getYLeftCornerCalcule()
     */
    @Override
    public int getYLeftCornerCalcule()
    {
        return this.spaceBefore;
    }

    /**
     * Accesseur de l attribut space before.
     * 
     * @return space before
     */
    public int getSpaceBefore()
    {
        return spaceBefore;
    }

    /**
     * Modificateur de l attribut space before.
     * 
     * @param spaceBefore le nouveau space before
     */
    public void setSpaceBefore(int spaceBefore)
    {
        this.spaceBefore = spaceBefore;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.view.jrxmlinfo.HeaderCellInfoI#getHeight()
     */
    @Override
    public int getHeight()
    {
        return height;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.view.jrxmlinfo.HeaderCellInfoI#setHeight(int)
     */
    @Override
    public void setHeight(int height)
    {
        this.height = height;
    }

    /**
     * Accesseur de l attribut subgroups headers infos.
     * 
     * @return the subgroupsHeadersInfos
     */
    public List<SubgroupHeaderInfo> getSubgroupsHeadersInfos()
    {
        return subgroupsHeadersInfos;
    }

    /**
     * Modificateur de l attribut subgroups headers infos.
     * 
     * @param subgroupsHeadersInfos the subgroupsHeadersInfos to set
     */
    public void setSubgroupsHeadersInfos(List<SubgroupHeaderInfo> subgroupsHeadersInfos)
    {
        this.subgroupsHeadersInfos = subgroupsHeadersInfos;
    }

    /**
     * Accesseur de l attribut titles headers infos.
     * 
     * @return the titlesHeadersInfos
     */
    public List<TitleHeaderInfo> getTitlesHeadersInfos()
    {
        return titlesHeadersInfos;
    }

    /**
     * Modificateur de l attribut titles headers infos.
     * 
     * @param titlesHeadersInfos the titlesHeadersInfos to set
     */
    public void setTitlesHeadersInfos(List<TitleHeaderInfo> titlesHeadersInfos)
    {
        this.titlesHeadersInfos = titlesHeadersInfos;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.view.jrxmlinfo.HeaderCellInfoI#getValue()
     */
    @Override
    public String getValue()
    {
        return value;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.view.jrxmlinfo.HeaderCellInfoI#setValue(java.lang.String)
     */
    @Override
    public void setValue(String value)
    {
        this.value = value;
    }

}
