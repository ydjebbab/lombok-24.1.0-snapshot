/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.apptags.table.view.jrxmlinfo;

/**
 * Class PageFooterInfo
 */
public class PageFooterInfo extends AbstractBandInfo
{

    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** rectangle1. */
    private RectangleInfo rectangle1;

    /** rectangle2. */
    private RectangleInfo rectangle2;

    /** page numero style. */
    private FontStyleInfo pageNumeroStyle;

    /** texte pied de page style. */
    private FontStyleInfo textePiedDePageStyle;

    /** top padding. */
    private int topPadding;

    /** bottom padding. */
    private int bottomPadding;

    /** left padding. */
    private int leftPadding;

    /** right padding. */
    private int rightPadding;

    /**
     * Instanciation de page footer info.
     */
    public PageFooterInfo()
    {
        super();
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.view.jrxmlinfo.AbstractBandInfo#getBottomPadding()
     */
    @Override
    public int getBottomPadding()
    {
        return bottomPadding;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.view.jrxmlinfo.AbstractBandInfo#setBottomPadding(int)
     */
    @Override
    public void setBottomPadding(int bottomPadding)
    {
        this.bottomPadding = bottomPadding;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.view.jrxmlinfo.AbstractBandInfo#getLeftPadding()
     */
    @Override
    public int getLeftPadding()
    {
        return leftPadding;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.view.jrxmlinfo.AbstractBandInfo#setLeftPadding(int)
     */
    @Override
    public void setLeftPadding(int leftPadding)
    {
        this.leftPadding = leftPadding;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.view.jrxmlinfo.AbstractBandInfo#getRightPadding()
     */
    @Override
    public int getRightPadding()
    {
        return rightPadding;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.view.jrxmlinfo.AbstractBandInfo#setRightPadding(int)
     */
    @Override
    public void setRightPadding(int rightPadding)
    {
        this.rightPadding = rightPadding;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.view.jrxmlinfo.AbstractBandInfo#getTopPadding()
     */
    @Override
    public int getTopPadding()
    {
        return topPadding;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.view.jrxmlinfo.AbstractBandInfo#setTopPadding(int)
     */
    @Override
    public void setTopPadding(int topPadding)
    {
        this.topPadding = topPadding;
    }

    /**
     * Accesseur de l attribut texte pied de page style.
     * 
     * @return texte pied de page style
     */
    public FontStyleInfo getTextePiedDePageStyle()
    {
        return textePiedDePageStyle;
    }

    /**
     * Modificateur de l attribut texte pied de page style.
     * 
     * @param textePiedDePageStyle le nouveau texte pied de page style
     */
    public void setTextePiedDePageStyle(FontStyleInfo textePiedDePageStyle)
    {
        this.textePiedDePageStyle = textePiedDePageStyle;
    }

    /**
     * Accesseur de l attribut page numero style.
     * 
     * @return page numero style
     */
    public FontStyleInfo getPageNumeroStyle()
    {
        return pageNumeroStyle;
    }

    /**
     * Modificateur de l attribut page numero style.
     * 
     * @param pageNumeroStyle le nouveau page numero style
     */
    public void setPageNumeroStyle(FontStyleInfo pageNumeroStyle)
    {
        this.pageNumeroStyle = pageNumeroStyle;
    }

    /**
     * Accesseur de l attribut rectangle1.
     * 
     * @return rectangle1
     */
    public RectangleInfo getRectangle1()
    {
        return rectangle1;
    }

    /**
     * Modificateur de l attribut rectangle1.
     * 
     * @param rectangle1 le nouveau rectangle1
     */
    public void setRectangle1(RectangleInfo rectangle1)
    {
        this.rectangle1 = rectangle1;
    }

    /**
     * Accesseur de l attribut rectangle2.
     * 
     * @return rectangle2
     */
    public RectangleInfo getRectangle2()
    {
        return rectangle2;
    }

    /**
     * Modificateur de l attribut rectangle2.
     * 
     * @param rectangle2 le nouveau rectangle2
     */
    public void setRectangle2(RectangleInfo rectangle2)
    {
        this.rectangle2 = rectangle2;
    }

}
