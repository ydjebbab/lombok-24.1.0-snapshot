/*
 * Copyright (c) 2014 DGFiP - Tous droits réservés
 *//*
/*Copyright (c) 2014 DGFiP - Tous droits réservés/* * Copyright 2004 original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package fr.gouv.finances.lombok.apptags.table.view.html;

import java.util.List;

import org.apache.commons.beanutils.MethodUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import fr.gouv.finances.lombok.apptags.table.bean.Column;
import fr.gouv.finances.lombok.apptags.table.calc.CalcResult;
import fr.gouv.finances.lombok.apptags.table.calc.CalcUtils;
import fr.gouv.finances.lombok.apptags.table.core.PreferencesConstants;
import fr.gouv.finances.lombok.apptags.table.core.TableModel;
import fr.gouv.finances.lombok.apptags.util.ExtremeUtils;
import fr.gouv.finances.lombok.apptags.util.HtmlBuilder;

/**
 * Class CalcBuilder
 */
public class CalcBuilder
{

    /** logger. */
    private Log logger = LogFactory.getLog(CalcBuilder.class);

    /** html. */
    private HtmlBuilder html;

    /** model. */
    private TableModel model;

    /**
     * Instanciation de calc builder.
     * 
     * @param model
     */
    public CalcBuilder(TableModel model)
    {
        this(new HtmlBuilder(), model);
    }

    /**
     * Instanciation de calc builder.
     * 
     * @param html
     * @param model
     */
    public CalcBuilder(HtmlBuilder html, TableModel model)
    {
        this.html = html;
        this.model = model;
    }

    /**
     * Accesseur de l attribut html builder.
     * 
     * @return html builder
     */
    public HtmlBuilder getHtmlBuilder()
    {
        return html;
    }

    /**
     * Accesseur de l attribut table model.
     * 
     * @return table model
     */
    protected TableModel getTableModel()
    {
        return model;
    }

    /**
     * methode Default calc layout :
     */
    public void defaultCalcLayout()
    {
        Column calcColumn = model.getColumnHandler().getFirstCalcColumn();
        if (calcColumn == null)
        {
            return;
        }

        String layout = model.getPreferences().getPreference(PreferencesConstants.DEFAULT_CALC_LAYOUT);

        try
        {
            MethodUtils.invokeExactMethod(this, layout, null);
        }
        catch (Exception e)
        {
            logger.error("There is no method with the layout [" + layout + "].", e);
        }
    }

    /**
     * methode Single row calc results :
     */
    public void singleRowCalcResults()
    {
        html.tr(1).styleClass(BuilderConstants.CALC_ROW_CSS).close();

        for (Column column : model.getColumnHandler().getColumns())
        {
            if (column.isFirstColumn())
            {
                String[] calcTitle = CalcUtils.getFirstCalcColumnTitles(model);
                if (calcTitle != null && calcTitle.length > 0)
                {
                    html.td(2).styleClass(BuilderConstants.CALC_TITLE_CSS).close();
                    for (int i = 0; i < calcTitle.length; i++)
                    {
                        String title = calcTitle[i];
                        html.append(title);
                        if (calcTitle.length > 0 && i + 1 != calcTitle.length)
                        {
                            html.append(" / ");
                        }
                    }
                    html.tdEnd();
                }

                continue;
            }

            if (column.isCalculated())
            {
                html.td(2).styleClass(BuilderConstants.CALC_RESULT_CSS).close();
                List<CalcResult> calcResults = CalcUtils.getCalcResults(model, column);
                for (int i = 0; i < calcResults.size(); i++)
                {
                    CalcResult calcResult = calcResults.get(i);
                    Number value = calcResult.getValue();
                    if (value == null)
                    {
                        html.append(calcResult.getName());
                    }
                    else
                    {
                        html.append(ExtremeUtils.formatNumber(column.getFormat(), value, model.getLocale()));
                    }

                    if (calcResults.size() > 0 && i + 1 != calcResults.size())
                    {
                        html.append(" / ");
                    }
                }
            }
            else
            {
                html.td(2).close();
                html.nbsp();
            }

            html.tdEnd();

        }

        html.trEnd(1);
    }

    /**
     * methode Multi row calc results :
     */
    public void multiRowCalcResults()
    {
        Column firstCalcColumn = model.getColumnHandler().getFirstCalcColumn();
        int rows = firstCalcColumn.getCalc().size();

        for (int i = 0; i < rows; i++)
        {
            html.tr(1).styleClass(BuilderConstants.CALC_ROW_CSS).close();

            for (Column column : model.getColumnHandler().getColumns())
            {
                if (column.isFirstColumn())
                {
                    String calcTitle = CalcUtils.getFirstCalcColumnTitleByPosition(model, i);
                    html.td(2).styleClass(BuilderConstants.CALC_TITLE_CSS).close();
                    html.append(calcTitle);
                    html.tdEnd();

                    continue;
                }

                if (column.isCalculated())
                {
                    html.td(2).styleClass(BuilderConstants.CALC_RESULT_CSS).close();
                    CalcResult calcResult = CalcUtils.getCalcResultsByPosition(model, column, i);
                    Number value = calcResult.getValue();
                    if (value == null)
                    {
                        html.append(calcResult.getName());
                    }
                    else
                    {
                        html.append(ExtremeUtils.formatNumber(column.getFormat(), value, model.getLocale()));
                    }
                }
                else
                {
                    html.td(2).close();
                    html.nbsp();
                }

                html.tdEnd();
            }

            html.trEnd(1);
        }
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        return html.toString();
    }
}
