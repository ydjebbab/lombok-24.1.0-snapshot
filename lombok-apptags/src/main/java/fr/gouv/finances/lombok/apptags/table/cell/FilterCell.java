/*
/*Copyright (c) 2014 DGFiP - Tous droits réservés/* * Copyright 2004 original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package fr.gouv.finances.lombok.apptags.table.cell;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;

import fr.gouv.finances.lombok.apptags.table.bean.Column;
import fr.gouv.finances.lombok.apptags.table.core.Messages;
import fr.gouv.finances.lombok.apptags.table.core.TableConstants;
import fr.gouv.finances.lombok.apptags.table.core.TableModel;
import fr.gouv.finances.lombok.apptags.table.view.html.TableActions;
import fr.gouv.finances.lombok.apptags.util.HtmlBuilder;

/**
 * Une cellule de filtre de type champ de saisie de type texte.
 */
public class FilterCell implements Cell
{

    public FilterCell()
    {
        super();
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.cell.Cell#getExportDisplay(fr.gouv.finances.lombok.apptags.table.core.TableModel,
     *      fr.gouv.finances.lombok.apptags.table.bean.Column)
     */
    @Override
    public String getExportDisplay(TableModel model, Column column)
    {
        return null;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.cell.Cell#getHtmlDisplay(fr.gouv.finances.lombok.apptags.table.core.TableModel,
     *      fr.gouv.finances.lombok.apptags.table.bean.Column)
     */
    @Override
    public String getHtmlDisplay(TableModel model, Column column)
    {
        HtmlBuilder html = new HtmlBuilder();
        Messages messages = model.getMessages();

        html.td(2);
        if (!column.isFilterable())
        {     
            StringBuilder tdName = new StringBuilder();
            tdName.append(model.getTableHandler().prefixWithTableId());
            tdName.append(TableConstants.FILTER);
            tdName.append(column.getAlias());
            tdName.append(TableConstants.TDFILTER);
            html.id(tdName.toString());
            String styleClass = "";
            if (StringUtils.isNotEmpty(column.getFilterClass()))
            {
                styleClass = column.getFilterClass();
            }
            html.ariaHidden("true");
            html.close();

            html.span().ariaHidden("true").close();
            html.append("");
            html.spanEnd();
        }
        else
        {

            StringBuilder tdName = new StringBuilder();
            tdName.append(model.getTableHandler().prefixWithTableId());
            tdName.append(TableConstants.FILTER);
            tdName.append(column.getAlias());
            tdName.append(TableConstants.TDFILTER);
            html.id(tdName.toString());

            String styleClass = "";
            if (StringUtils.isNotEmpty(column.getFilterClass()))
            {
                styleClass = column.getFilterClass();
            }

            if (column.isFirstColumn())
            {
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append(styleClass);
                stringBuilder.append(" first");
                styleClass = stringBuilder.toString();
            }

            html.styleClass(styleClass);

            StringBuilder styleValue = new StringBuilder();

            
            if (StringUtils.isNotEmpty(column.getFilterStyle()))
            {
                styleValue.append(column.getFilterStyle());
            }

            if (StringUtils.isNotEmpty(styleValue.toString()))
            {
                html.style(styleValue.toString());
            }

            html.close();

            html.append(input(model, column));
        }
        html.tdEnd();

        return html.toString();
    }

    /**
     * Si l'utilisation du filtre est activée, le tag <input type=text> est utilisé par défaut ajout de 'return false;'
     * dans la section 'if(event.keycode == 13)' quand l'attribut onInvokeAction est spécifié par l'utilisateur
     * 
     * @param model
     * @param column
     * @return string
     */
    private String input(TableModel model, Column column)
    {
        HtmlBuilder html = new HtmlBuilder();
        StringBuilder inputName = new StringBuilder();
        inputName.append(model.getTableHandler().prefixWithTableId());
        inputName.append(TableConstants.FILTER);
        inputName.append(column.getAlias());
        String title = column.getTitle();
        // html.labelFor(inputName.toString()).close().append("Filtrer "+title).labelForEnd();
        // html.br();

        html.input("text");
        StringBuilder styleValue = new StringBuilder();

        if (StringUtils.isNotEmpty(column.getWidth()) && (!column.isShowResponsive()))
        {
            styleValue.append("width:");
            styleValue.append(column.getWidth());
            styleValue.append(';');
        }

        if (StringUtils.isNotEmpty(column.getFilterStyle()))
        {
            styleValue.append(column.getFilterStyle());
        }

        if (StringUtils.isNotEmpty(styleValue.toString()))
        {
            html.style(styleValue.toString());
        }
        html.name(inputName.toString());
        html.id(inputName.toString());
        String value = column.getValueAsString();
        if (StringUtils.isNotBlank(value))
        {
            html.value(StringEscapeUtils.escapeHtml(value));
        }

        if (StringUtils.isNotEmpty(column.getFilterSize()))
        {
            html.size(column.getFilterSize());
        }

        StringBuilder onkeypress = new StringBuilder(41);
        onkeypress.append("if (event.keyCode == 13) {");
        TableActions ta = new TableActions(model);
        onkeypress.append(ta.getFilterAction());

        if (ta.hasOnInvokeAction())
        {
            onkeypress.append(";return false;");
        }

        onkeypress.append('}');
        html.onkeypress(onkeypress.toString());

        // indication des operations de filtres possibles que dans le cas ou cpfiltreinfsup
        if (model.getTableHandler().getTable().getFilterRowsCallback() != null
            && model
            .getTableHandler()
            .getTable()
            .getFilterRowsCallback()
            .compareToIgnoreCase(
                "fr.gouv.finances.lombok.apptags.table.callback.ProcessRowsCallBackFiltreAvecComparateurInferieurSuperieur") == 0)
        {
            html.title("< <= >= > <> = *");
            html.ariaLabel("Filtrer " + column.getTitle() + ", " + html.SignToLitteralText("< <= >= > <> = *", true));
        }
        // html.append("placeholder='&lt; &lt;= &gt;= &gt; &lt;&gt; = *'");

        html.xclose();

        return html.toString();
    }
}
