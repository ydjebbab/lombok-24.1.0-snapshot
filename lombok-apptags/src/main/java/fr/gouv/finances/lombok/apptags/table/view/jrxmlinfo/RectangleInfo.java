/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.apptags.table.view.jrxmlinfo;

import java.io.Serializable;

import net.sf.jasperreports.engine.type.HorizontalAlignEnum;
import net.sf.jasperreports.engine.type.VerticalAlignEnum;

/**
 * Class RectangleInfo
 */
public class RectangleInfo implements Serializable
{

    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** xleftcorner. */
    private int xleftcorner;

    /** yleftcorner. */
    private int yleftcorner;

    /** width. */
    private int width;

    /** height. */
    private int height;

    /** draw border. */
    private boolean drawBorder;

    /** include. */
    private boolean include;

    /** horizontal alignment. */
    private HorizontalAlignEnum horizontalAlignment;

    /** vertical alignment. */
    private VerticalAlignEnum verticalAlignment;

    /** opaque. */
    private boolean opaque;

    /** color r. */
    private int colorR;

    /** color g. */
    private int colorG;

    /** color b. */
    private int colorB;
    
    /**
     * Instanciation de rectangle info.
     */
    public RectangleInfo()
    {
        super();
    }


    /**
     * Verifie si include.
     * 
     * @return true, si c'est include
     */
    public boolean isInclude()
    {
        return include;
    }

    /**
     * Modificateur de l attribut include.
     * 
     * @param include le nouveau include
     */
    public void setInclude(boolean include)
    {
        this.include = include;
    }

    /**
     * Verifie si draw border.
     * 
     * @return true, si c'est draw border
     */
    public boolean isDrawBorder()
    {
        return drawBorder;
    }

    /**
     * Modificateur de l attribut draw border.
     * 
     * @param drawBorder le nouveau draw border
     */
    public void setDrawBorder(boolean drawBorder)
    {
        this.drawBorder = drawBorder;
    }


    /**
     * Accesseur de l attribut xleftcorner.
     * 
     * @return xleftcorner
     */
    public int getXleftcorner()
    {
        return xleftcorner;
    }

    /**
     * Modificateur de l attribut xleftcorner.
     * 
     * @param leftCorner le nouveau xleftcorner
     */
    public void setXleftcorner(int leftCorner)
    {
        xleftcorner = leftCorner;
    }

    /**
     * Accesseur de l attribut yleftcorner.
     * 
     * @return yleftcorner
     */
    public int getYleftcorner()
    {
        return yleftcorner;
    }

    /**
     * Modificateur de l attribut yleftcorner.
     * 
     * @param leftCorner le nouveau yleftcorner
     */
    public void setYleftcorner(int leftCorner)
    {
        yleftcorner = leftCorner;
    }

    /**
     * Accesseur de l attribut height.
     * 
     * @return height
     */
    public int getHeight()
    {
        return height;
    }

    /**
     * Modificateur de l attribut height.
     * 
     * @param height le nouveau height
     */
    public void setHeight(int height)
    {
        this.height = height;
    }

    /**
     * Accesseur de l attribut width.
     * 
     * @return width
     */
    public int getWidth()
    {
        return width;
    }

    /**
     * Modificateur de l attribut width.
     * 
     * @param width le nouveau width
     */
    public void setWidth(int width)
    {
        this.width = width;
    }

    /**
     * Accesseur de horizontalAlignment.
     * 
     * @return horizontalAlignment
     */
    public HorizontalAlignEnum getHorizontalAlignment()
    {
        return horizontalAlignment;
    }

    /**
     * Mutateur de horizontalAlignment.
     * 
     * @param horizontalAlignment horizontalAlignment
     */
    public void setHorizontalAlignment(HorizontalAlignEnum horizontalAlignment)
    {
        this.horizontalAlignment = horizontalAlignment;
    }

    /**
     * Accesseur de verticalAlignment.
     * 
     * @return verticalAlignment
     */
    public VerticalAlignEnum getVerticalAlignment()
    {
        return verticalAlignment;
    }

    /**
     * Mutateur de verticalAlignment.
     * 
     * @param verticalAlignment verticalAlignment
     */
    public void setVerticalAlignment(VerticalAlignEnum verticalAlignment)
    {
        this.verticalAlignment = verticalAlignment;
    }

    /**
     * Accesseur de opaque.
     * 
     * @return opaque
     */
    public boolean isOpaque()
    {
        return opaque;
    }

    /**
     * Mutateur de opaque.
     * 
     * @param opaque opaque
     */
    public void setOpaque(boolean opaque)
    {
        this.opaque = opaque;
    }

    /**
     * Accesseur de colorR.
     * 
     * @return colorR
     */
    public int getColorR()
    {
        return colorR;
    }

    /**
     * Mutateur de colorR.
     * 
     * @param colorR colorR
     */
    public void setColorR(int colorR)
    {
        this.colorR = colorR;
    }

    /**
     * Accesseur de colorG.
     * 
     * @return colorG
     */
    public int getColorG()
    {
        return colorG;
    }

    /**
     * Mutateur de colorG.
     * 
     * @param colorG colorG
     */
    public void setColorG(int colorG)
    {
        this.colorG = colorG;
    }

    /**
     * Accesseur de colorB.
     * 
     * @return colorB
     */
    public int getColorB()
    {
        return colorB;
    }

    /**
     * Mutateur de colorB.
     * 
     * @param colorB colorB
     */
    public void setColorB(int colorB)
    {
        this.colorB = colorB;
    }

}
