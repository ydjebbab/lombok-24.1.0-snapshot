/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : ITreeFilter.java
 *
 */
package fr.gouv.finances.lombok.apptags.tree.bean;

/**
 * Pour ne pas afficher tous les noeuds visibles d'un arbre, sans pour autant les retirer de
 * l'arbre, il est possible d'utiliser un filtre. Le filtre indique à l'itérateur TreeIterator les noeuds qui doivent
 * être inclus ou exclus de l'arbre affiché.
 * 
 * @author chouard-cp
 * @version $Revision: 1.2 $ Date: 15 déc. 2009
 */
public interface ITreeFilter
{

    /**
     * Return true if the node is to be included when iterating the visible nodes of a tree. False if the node should be
     * ignored, and thus not displayed in the JSP tree. Retourne true si le noeud doit être inclus dans l'arbre affiché,
     * false s'il doit être ignoré et donc non affiché par la JSP
     * 
     * @param tree L'arbre auquel le noeud appartient
     * @param node Un noeud de l'arbre
     * @return true, if accept
     */
    public boolean accept(Tree tree, TreeIteratorElement node);

    /**
     * Initialise le filtre. Si par exemple, on souhaite naviguer dans l'arbre dans un ordre différent pour déterminer
     * les noeuds qui doivent être visibles, c'est dans cette méthode qu'un opération de ce type est à effectuer
     * 
     * @param tree L'arbre concernant l'initialisation du filtre
     */
    public void init(Tree tree);
}
