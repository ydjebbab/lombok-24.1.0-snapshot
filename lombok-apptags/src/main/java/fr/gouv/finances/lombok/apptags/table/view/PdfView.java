/*
 * Copyright (c) 2014 DGFiP - Tous droits réservés
 *//*
/*Copyright (c) 2014 DGFiP - Tous droits réservés/* * Copyright 2004 original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package fr.gouv.finances.lombok.apptags.table.view;

import java.util.List;

import fr.gouv.finances.lombok.apptags.table.bean.Column;
import fr.gouv.finances.lombok.apptags.table.bean.Export;
import fr.gouv.finances.lombok.apptags.table.calc.CalcResult;
import fr.gouv.finances.lombok.apptags.table.calc.CalcUtils;
import fr.gouv.finances.lombok.apptags.table.core.TableModel;
import fr.gouv.finances.lombok.apptags.util.ExtremeUtils;

/**
 * Class PdfView
 */
public class PdfView implements View
{

    /** Constant : FONT. */
    public static final String FONT = "exportPdf.font";

    /** Constant : HEADER_BACKGROUND_COLOR. */
    public static final String HEADER_BACKGROUND_COLOR = "headerBackgroundColor";

    /** Constant : HEADER_TITLE. */
    public static final String HEADER_TITLE = "headerTitle";

    /** Constant : HEADER_COLOR. */
    public static final String HEADER_COLOR = "headerColor";

    /** xlsfo. */
    private StringBuilder xlsfo = new StringBuilder(196);

    /** font. */
    private String font;

    /**
     * Instanciation de pdf view.
     */
    public PdfView()
    {
        // RAS
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.view.View#beforeBody(fr.gouv.finances.lombok.apptags.table.core.TableModel)
     */
    @Override
    public void beforeBody(TableModel model)
    {
        this.font = model.getPreferences().getPreference(FONT);

        xlsfo.append(startRoot());
        xlsfo.append(regionBefore(model));
        xlsfo.append(regionAfter());
        xlsfo.append(columnDefinitions(model));
        xlsfo.append(header(model));
        xlsfo.append(" <fo:table-body> ");
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.view.View#body (fr.gouv.finances.lombok.apptags.table.core.TableModel,
     *      fr.gouv.finances.lombok.apptags.table.bean.Column)
     */
    @Override
    public void body(TableModel model, Column column)
    {
        if (column.isFirstColumn())
        {
            xlsfo.append(" <fo:table-row> ");
        }

        String value = ExportViewUtils.parsePDF(column.getCellDisplay());

        xlsfo.append(" <fo:table-cell border=\"solid silver .5px\" display-align=\"center\" padding=\"3pt\"> ");
        xlsfo.append(" <fo:block" + getFont() + ">" + value + "</fo:block> ");
        xlsfo.append(" </fo:table-cell> ");

        if (column.isLastColumn())
        {
            xlsfo.append(" </fo:table-row> ");
        }
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.view.View#afterBody(fr.gouv.finances.lombok.apptags.table.core.TableModel)
     */
    @Override
    public Object afterBody(TableModel model)
    {
        if (model.getLimit().getTotalRows() != 0)
        {
            xlsfo.append(totals(model));
        }
        xlsfo.append(" </fo:table-body> ");
        xlsfo.append(endRoot());
        return xlsfo.toString();
    }

    /**
     * methode Start root :
     * 
     * @return string
     */
    public String startRoot()
    {
        StringBuilder sbuf = new StringBuilder(567);

        sbuf.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");

        sbuf.append("<fo:root xmlns:fo=\"http://www.w3.org/1999/XSL/Format\">");

        sbuf.append(" <fo:layout-master-set> ");
        sbuf.append(" <fo:simple-page-master master-name=\"simple\" ");
        sbuf.append(" page-height=\"8.5in\" ");
        sbuf.append(" page-width=\"11in\" ");
        sbuf.append(" margin-top=\".5in\" ");
        sbuf.append(" margin-bottom=\".25in\" ");
        sbuf.append(" margin-left=\".5in\" ");
        sbuf.append(" margin-right=\".5in\"> ");
        sbuf.append(" <fo:region-body margin-top=\".5in\" margin-bottom=\".25in\"/> ");
        sbuf.append(" <fo:region-before extent=\".5in\"/> ");
        sbuf.append(" <fo:region-after extent=\".25in\"/> ");
        sbuf.append(" </fo:simple-page-master> ");
        sbuf.append(" </fo:layout-master-set> ");

        sbuf.append(" <fo:page-sequence master-reference=\"simple\" initial-page-number=\"1\"> ");

        return sbuf.toString();
    }

    /**
     * methode Region before :
     * 
     * @param model
     * @return string
     */
    public String regionBefore(TableModel model)
    {
        StringBuilder sbuf = new StringBuilder(181);

        Export export = model.getExportHandler().getCurrentExport();

        String headerBackgroundColor = export.getAttributeAsString(HEADER_BACKGROUND_COLOR);

        sbuf.append(" <fo:static-content flow-name=\"xsl-region-before\"> ");

        String title = export.getAttributeAsString(HEADER_TITLE);

        sbuf.append(" <fo:block space-after.optimum=\"15pt\" color=\"" + headerBackgroundColor
            + "\" font-size=\"17pt\" font-family=\"" + getHeadFont() + "'Times'\">" + title + "</fo:block> ");

        sbuf.append(" </fo:static-content> ");

        return sbuf.toString();
    }

    /**
     * methode Region after :
     * 
     * @return string
     */
    public String regionAfter()
    {
        StringBuilder sbuf = new StringBuilder();

        sbuf.append(" <fo:static-content flow-name=\"xsl-region-after\" display-align=\"after\"> ");

        sbuf.append(" <fo:block text-align=\"end\">Page <fo:page-number/></fo:block> ");

        sbuf.append(" </fo:static-content> ");

        return sbuf.toString();
    }

    /**
     * methode Column definitions :
     * 
     * @param model
     * @return string
     */
    public String columnDefinitions(TableModel model)
    {
        StringBuilder sbuf = new StringBuilder();

        sbuf.append(" <fo:flow flow-name=\"xsl-region-body\"> ");

        sbuf.append(" <fo:block" + getFont() + ">");

        sbuf.append(" <fo:table table-layout=\"fixed\" font-size=\"10pt\"> ");

        double columnCount = model.getColumnHandler().columnCount();

        double colwidth = 10 / columnCount;

        for (int i = 1; i <= columnCount; i++)
        {
            sbuf.append(" <fo:table-column column-number=\"" + i + "\" column-width=\"" + colwidth + "in\"/> ");
        }

        return sbuf.toString();
    }

    /**
     * methode Header :
     * 
     * @param model
     * @return string
     */
    public String header(TableModel model)
    {
        StringBuilder sbuf = new StringBuilder();

        Export export = model.getExportHandler().getCurrentExport();
        String headerColor = export.getAttributeAsString(HEADER_COLOR);
        String headerBackgroundColor = export.getAttributeAsString(HEADER_BACKGROUND_COLOR);

        sbuf.append(" <fo:table-header background-color=\"" + headerBackgroundColor + "\" color=\"" + headerColor
            + "\"> ");

        sbuf.append(" <fo:table-row> ");

        List<Column> columns = model.getColumnHandler().getHeaderColumns();

        for (Column column : columns)
        {
            String title = column.getCellDisplay();
            sbuf
                .append(" <fo:table-cell border=\"solid silver .5px\" " +
                    "text-align=\"center\" display-align=\"center\" padding=\"3pt\"> ");
            sbuf.append(" <fo:block" + getFont() + ">" + title + "</fo:block> ");
            sbuf.append(" </fo:table-cell> ");
        }

        sbuf.append(" </fo:table-row> ");

        sbuf.append(" </fo:table-header> ");

        return sbuf.toString();
    }

    /**
     * methode End root :
     * 
     * @return string
     */
    public String endRoot()
    {
        StringBuilder sbuf = new StringBuilder();

        sbuf.append(" </fo:table> ");

        sbuf.append(" </fo:block> ");

        sbuf.append(" </fo:flow> ");

        sbuf.append(" </fo:page-sequence> ");

        sbuf.append(" </fo:root> ");

        return sbuf.toString();
    }

    /**
     * Accesseur de l attribut font.
     * 
     * @return font
     */
    protected String getFont()
    {
        return font == null ? "" : " font-family=\"" + font + "\"";
    }

    /**
     * Accesseur de l attribut head font.
     * 
     * @return head font
     */
    protected String getHeadFont()
    {
        return font == null ? "" : font + ",";
    }

    /**
     * methode Totals :
     * 
     * @param model
     * @return string buffer
     */
    public StringBuilder totals(TableModel model)
    {

        StringBuilder sbuf = new StringBuilder();
        Export export = model.getExportHandler().getCurrentExport();
        String headerColor = export.getAttributeAsString(HEADER_COLOR);
        String headerBackgroundColor = export.getAttributeAsString(HEADER_BACKGROUND_COLOR);

        Column firstCalcColumn = model.getColumnHandler().getFirstCalcColumn();

        if (firstCalcColumn != null)
        {
            int rows = firstCalcColumn.getCalc().size();
            for (int i = 0; i < rows; i++)
            {
                sbuf.append("<fo:table-row>");

                for (Column column : model.getColumnHandler().getColumns())
                {
                    if (column.isFirstColumn())
                    {
                        String calcTitle = CalcUtils.getFirstCalcColumnTitleByPosition(model, i);
                        sbuf
                            .append(" <fo:table-cell border=\"solid silver .5px\" " +
                                "text-align=\"center\" display-align=\"center\" padding=\"3pt\" background-color=\"");
                        sbuf.append(headerBackgroundColor + "\" color=\"" + headerColor + "\">");
                        sbuf.append(" <fo:block " + getFont() + ">" + calcTitle);
                        sbuf.append(" </fo:block></fo:table-cell> ");
                        continue;
                    }
                    if (column.isCalculated())
                    {
                        sbuf
                            .append(" <fo:table-cell border=\"solid silver .5px\" " +
                                "text-align=\"center\" display-align=\"center\" padding=\"3pt\" background-color=\"");
                        sbuf.append(headerBackgroundColor + "\" color=\"" + headerColor + "\"> ");
                        sbuf.append(" <fo:block " + getFont() + ">");
                        CalcResult calcResult = CalcUtils.getCalcResultsByPosition(model, column, i);
                        Number value = calcResult.getValue();
                        if (value != null)
                        {
                            sbuf.append(ExtremeUtils.formatNumber(column.getFormat(), value, model.getLocale()));
                        }
                        else
                        {
                            sbuf.append("n/a");
                        }
                        sbuf.append("</fo:block> ");
                    }
                    else
                    {
                        sbuf
                            .append(" <fo:table-cell border=\"solid silver .5px\" " +
                                "text-align=\"center\" display-align=\"center\" padding=\"3pt\" background-color=\"");
                        sbuf.append(headerBackgroundColor + "\" color=\"" + headerColor + "\"> ");
                        sbuf.append(" <fo:block " + getFont() + ">");
                        sbuf.append(" ");
                        sbuf.append("</fo:block> ");
                    }
                    sbuf.append(" </fo:table-cell> ");
                }
                sbuf.append("</fo:table-row>");

            }

        }
        return sbuf;
    }
}
