/*
/*Copyright (c) 2014 DGFiP - Tous droits réservés/* * Copyright 2004 original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package fr.gouv.finances.lombok.apptags.table.interceptor;

import fr.gouv.finances.lombok.apptags.table.bean.Column;
import fr.gouv.finances.lombok.apptags.table.bean.Export;
import fr.gouv.finances.lombok.apptags.table.bean.Row;
import fr.gouv.finances.lombok.apptags.table.bean.Table;
import fr.gouv.finances.lombok.apptags.table.core.TableModel;

/**
 * Class DefaultInterceptor
 */
public class DefaultInterceptor implements TableInterceptor, RowInterceptor, ColumnInterceptor, ExportInterceptor
{

    /**
     * Constructeur de la classe DefaultInterceptor.java
     *
     */
    public DefaultInterceptor()
    {
        super();
        
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.interceptor.TableInterceptor#addTableAttributes(fr.gouv.finances.lombok.apptags.table.core.TableModel,
     *      fr.gouv.finances.lombok.apptags.table.bean.Table)
     */
    @Override
    public void addTableAttributes(TableModel tableModel, Table table)
    {
        // RAS
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.interceptor.RowInterceptor#addRowAttributes(fr.gouv.finances.lombok.apptags.table.core.TableModel,
     *      fr.gouv.finances.lombok.apptags.table.bean.Row)
     */
    @Override
    public void addRowAttributes(TableModel tableModel, Row row)
    {
        // RAS
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.interceptor.RowInterceptor#modifyRowAttributes(fr.gouv.finances.lombok.apptags.table.core.TableModel,
     *      fr.gouv.finances.lombok.apptags.table.bean.Row)
     */
    @Override
    public void modifyRowAttributes(TableModel model, Row row)
    {
        // RAS
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.interceptor.ColumnInterceptor#addColumnAttributes(fr.gouv.finances.lombok.apptags.table.core.TableModel,
     *      fr.gouv.finances.lombok.apptags.table.bean.Column)
     */
    @Override
    public void addColumnAttributes(TableModel tableModel, Column column)
    {
        // RAS
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.interceptor.ColumnInterceptor#modifyColumnAttributes(fr.gouv.finances.lombok.apptags.table.core.TableModel,
     *      fr.gouv.finances.lombok.apptags.table.bean.Column)
     */
    @Override
    public void modifyColumnAttributes(TableModel model, Column column)
    {
        // RAS
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.interceptor.ExportInterceptor#addExportAttributes(fr.gouv.finances.lombok.apptags.table.core.TableModel,
     *      fr.gouv.finances.lombok.apptags.table.bean.Export)
     */
    @Override
    public void addExportAttributes(TableModel tableModel, Export export)
    {
        // RAS
    }
}
