/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.apptags.table.filter;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import fr.gouv.finances.lombok.apptags.table.core.Preferences;

/**
 * Class CpCsvViewResolver
 */
public class CpCsvViewResolver implements ViewResolver
{

    /**
     * Instanciation de cp csv view resolver.
     */
    public CpCsvViewResolver()
    {
        super();
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.filter.ViewResolver#resolveView(javax.servlet.ServletRequest,
     *      javax.servlet.ServletResponse, fr.gouv.finances.lombok.apptags.table.core.Preferences, java.lang.Object)
     */
    @Override
    public void resolveView(ServletRequest request, ServletResponse response, Preferences preferences, Object viewData)
        throws Exception
    {
        byte[] contents = ((String) viewData).getBytes("ISO-8859-15");

        response.setContentType("text/plain; charset=ISO-8859-15");
        response.setContentLength(contents.length);
        response.getOutputStream().write(contents);

        // The charset for the MIME body response can be specified with
        // setContentType(java.lang.String).
        // For example, "text/html; charset=Shift_JIS".
        // The charset can alternately be set using
        // setLocale(java.util.Locale).
        // If no charset is specified, ISO-8859-1 will be used. The
        // setContentType or setLocale method must be called before
        // getWriter for the charset to affect the construction of the
        // writer.
    }
}
