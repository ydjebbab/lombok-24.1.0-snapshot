/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.apptags.table.core;

import fr.gouv.finances.lombok.apptags.table.bean.Column;

/**
 * Interface ColonneProtection
 */
public interface ColonneProtection
{

    /**
     * Accesseur de l attribut colonne csv protection.
     * 
     * @param column
     * @return colonne csv protection
     */
    public String getColonneCsvProtection(Column column);
}
