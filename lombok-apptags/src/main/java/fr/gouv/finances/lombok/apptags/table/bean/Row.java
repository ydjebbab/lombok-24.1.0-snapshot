/*
/*Copyright (c) 2014 DGFiP - Tous droits réservés/* * Copyright 2004 original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package fr.gouv.finances.lombok.apptags.table.bean;

import fr.gouv.finances.lombok.apptags.table.core.TableModel;

/**
 * Class Row
 */
public class Row extends Attributes
{

    /** model. */
    private TableModel model;

    /** highlight class. */
    private String highlightClass;

    /** highlight row. */
    private Boolean highlightRow;

    /** interceptor. */
    private String interceptor;

    /** onclick. */
    private String onclick;

    /** onmouseout. */
    private String onmouseout;

    /** onmouseover. */
    private String onmouseover;

    /** style. */
    private String style;

    /** style class. */
    private String styleClass;

    /**
     * namePropertyShowIfEqualsLast - propriété qui sert de discriminant pour l affichage cellule si propriété du bean
     * courant est égale à celle du bean précédent.
     */
    private String namePropertyShowIfEqualsLast;

    /** row count. */
    private int rowCount;

    /**
     * Instanciation de row.
     *
     * @param model
     */
    public Row(TableModel model)
    {
        this.model = model;
    }

    /**
     * methode Defaults :
     */
    public void defaults()
    {
        this.highlightClass = RowDefaults.getHighlightClass(model, highlightClass);
        this.highlightRow = RowDefaults.isHighlightRow(model, highlightRow);
    }

    /**
     * Accesseur de l attribut highlight class.
     *
     * @return highlight class
     */
    public String getHighlightClass()
    {
        return highlightClass;
    }

    /**
     * Accesseur de l attribut interceptor.
     *
     * @return interceptor
     */
    public String getInterceptor()
    {
        return interceptor;
    }

    /**
     * Gets the namePropertyShowIfEqualsLast - propriété qui sert de discriminant pour l affichage cellule si propriété
     * du bean courant est égale à celle du bean précédent.
     *
     * @return the namePropertyShowIfEqualsLast - propriété qui sert de discriminant pour l affichage cellule si
     *         propriété du bean courant est égale à celle du bean précédent
     */
    public String getNamePropertyShowIfEqualsLast()
    {
        return namePropertyShowIfEqualsLast;
    }

    /**
     * Accesseur de l attribut onclick.
     *
     * @return onclick
     */
    public String getOnclick()
    {
        return onclick;
    }

    /**
     * Accesseur de l attribut onmouseout.
     *
     * @return onmouseout
     */
    public String getOnmouseout()
    {
        return onmouseout;
    }

    /**
     * Accesseur de l attribut onmouseover.
     *
     * @return onmouseover
     */
    public String getOnmouseover()
    {
        return onmouseover;
    }

    /**
     * Accesseur de l attribut row count.
     *
     * @return row count
     */
    public int getRowCount()
    {
        return rowCount;
    }

    /**
     * Accesseur de l attribut style.
     *
     * @return style
     */
    public String getStyle()
    {
        return style;
    }

    /**
     * Accesseur de l attribut style class.
     *
     * @return style class
     */
    public String getStyleClass()
    {
        return styleClass;
    }

    /**
     * Verifie si highlight row.
     *
     * @return true, si c'est highlight row
     */
    public boolean isHighlightRow()
    {
        return highlightRow.booleanValue();
    }

    /**
     * Modificateur de l attribut highlight class.
     *
     * @param highlightClass le nouveau highlight class
     */
    public void setHighlightClass(String highlightClass)
    {
        this.highlightClass = highlightClass;
    }

    /**
     * Modificateur de l attribut highlight row.
     *
     * @param highlightRow le nouveau highlight row
     */
    public void setHighlightRow(Boolean highlightRow)
    {
        this.highlightRow = highlightRow;
    }

    /**
     * Modificateur de l attribut interceptor.
     *
     * @param interceptor le nouveau interceptor
     */
    public void setInterceptor(String interceptor)
    {
        this.interceptor = interceptor;
    }

    /**
     * Sets the namePropertyShowIfEqualsLast - propriété qui sert de discriminant pour l affichage cellule si propriété
     * du bean courant est égale à celle du bean précédent.
     *
     * @param namePropertyShowIfEqualsLast the new namePropertyShowIfEqualsLast - propriété qui sert de discriminant
     *        pour l affichage cellule si propriété du bean courant est égale à celle du bean précédent
     */
    public void setNamePropertyShowIfEqualsLast(String namePropertyShowIfEqualsLast)
    {
        this.namePropertyShowIfEqualsLast = namePropertyShowIfEqualsLast;
    }

    /**
     * Modificateur de l attribut onclick.
     *
     * @param onclick le nouveau onclick
     */
    public void setOnclick(String onclick)
    {
        this.onclick = onclick;
    }

    /**
     * Modificateur de l attribut onmouseout.
     *
     * @param onmouseout le nouveau onmouseout
     */
    public void setOnmouseout(String onmouseout)
    {
        this.onmouseout = onmouseout;
    }

    /**
     * Modificateur de l attribut onmouseover.
     *
     * @param onmouseover le nouveau onmouseover
     */
    public void setOnmouseover(String onmouseover)
    {
        this.onmouseover = onmouseover;
    }

    /**
     * Modificateur de l attribut row count.
     *
     * @param rowcount le nouveau row count
     */
    public void setRowCount(int rowcount)
    {
        this.rowCount = rowcount;
    }

    /**
     * Modificateur de l attribut style.
     *
     * @param style le nouveau style
     */
    public void setStyle(String style)
    {
        this.style = style;
    }

    /**
     * Modificateur de l attribut style class.
     *
     * @param styleClass le nouveau style class
     */
    public void setStyleClass(String styleClass)
    {
        this.styleClass = styleClass;
    }
}
