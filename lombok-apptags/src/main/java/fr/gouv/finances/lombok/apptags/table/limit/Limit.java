/*
/*Copyright (c) 2014 DGFiP - Tous droits réservés/* * Copyright 2004 original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package fr.gouv.finances.lombok.apptags.table.limit;

/**
 * Interface Limit
 */
public interface Limit
{

    /**
     * Accesseur de l attribut filter set.
     * 
     * @return filter set
     */
    public FilterSet getFilterSet();

    /**
     * Accesseur de l attribut row end.
     * 
     * @return row end
     */
    public int getRowEnd();

    /**
     * Accesseur de l attribut row start.
     * 
     * @return row start
     */
    public int getRowStart();

    /**
     * Accesseur de l attribut sort.
     * 
     * @return sort
     */
    public Sort getSort();

    /**
     * Accesseur de l attribut page.
     * 
     * @return page
     */
    public int getPage();

    /**
     * Accesseur de l attribut current rows displayed.
     * 
     * @return current rows displayed
     */
    public int getCurrentRowsDisplayed();

    /**
     * Accesseur de l attribut total rows.
     * 
     * @return total rows
     */
    public int getTotalRows();

    /**
     * Verifie si filtered.
     * 
     * @return true, si c'est filtered
     */
    public boolean isFiltered();

    /**
     * Verifie si cleared.
     * 
     * @return true, si c'est cleared
     */
    public boolean isCleared();

    /**
     * Verifie si sorted.
     * 
     * @return true, si c'est sorted
     */
    public boolean isSorted();

    /**
     * Verifie si exported.
     * 
     * @return true, si c'est exported
     */
    public boolean isExported();

    /**
     * methode Sets the row attributes :
     * 
     * @param totalRows
     * @param defaultRowsDisplayed
     * @param defaultStartPage
     */
    public void setRowAttributes(int totalRows, int defaultRowsDisplayed, int defaultStartPage);
}
