/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.apptags.util.comparators;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;
import java.util.Locale;

/**
 * <pre>
 * Comparateur pour comparer des dates du type dd/MM/yyyy
 * Utilisation pour l'instant dans CpFilterdropListCell
 * </pre>
 * 
 * @author amleplatinec Date: 4 juil. 2011
 */
public class DateComparator implements Comparator<Object>, Serializable
{

    private static final long serialVersionUID = 1L;

    public DateComparator()
    {
        super();
    }

    /**
     * compare deux objets de type string mis au format date (dd/MM/yyyy) {@inheritDoc}.
     * 
     * @param s1
     * @param s2
     * @return int
     * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
     */
    @Override
    public int compare(Object s1, Object s2)
    {
        if (s1 == null && s2 == null)
        {
            return 0;
        }
        else if (s1 == null)
        {
            return -1;
        }
        else if (s2 == null)
        {
            return 1;
        }

        Date o1 = new Date();
        Date o2 = new Date();
        try
        {
            o1 = (new SimpleDateFormat(("dd/MM/yyyy"), Locale.FRANCE)).parse(s1.toString());
            o2 = (new SimpleDateFormat(("dd/MM/yyyy"), Locale.FRANCE)).parse(s2.toString());

        }
        catch (ParseException e)
        { // mon commentaire
        }

        if (o1 instanceof Date)
        {
            if (o2 instanceof Date)
            {
                Date l = (Date) o1;
                Date r = (Date) o2;

                if (l.before(r))
                    return -1;
                else if (l.equals(r))
                    return 0;
                else
                    return 1;
            }
            else
            {
                // o2 wasn't comparable
                throw new ClassCastException(
                    "The first argument of this method was not a Date but " +
                        o2.getClass().getName());
            }
        }
        else if (o2 instanceof Date)
        {
            // o1 wasn't comparable
            throw new ClassCastException(
                "The second argument of this method was not a Date but " +
                    o1.getClass().getName());
        }
        else
        {
            // neither were comparable
            throw new ClassCastException(
                "Both arguments of this method were not Dates. They are " +
                    o1.getClass().getName() + " and " + o2.getClass().getName());
        }
    }

}
