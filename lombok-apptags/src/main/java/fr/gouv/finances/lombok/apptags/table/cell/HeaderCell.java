/*
/*Copyright (c) 2014 DGFiP - Tous droits réservés/* * Copyright 2004 original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package fr.gouv.finances.lombok.apptags.table.cell;

import org.apache.commons.lang.StringUtils;

import fr.gouv.finances.lombok.apptags.table.bean.Column;
import fr.gouv.finances.lombok.apptags.table.core.MessagesConstants;
import fr.gouv.finances.lombok.apptags.table.core.PreferencesConstants;
import fr.gouv.finances.lombok.apptags.table.core.TableConstants;
import fr.gouv.finances.lombok.apptags.table.core.TableModel;
import fr.gouv.finances.lombok.apptags.table.core.TableModelUtils;
import fr.gouv.finances.lombok.apptags.table.view.html.BuilderConstants;
import fr.gouv.finances.lombok.apptags.table.view.html.BuilderUtils;
import fr.gouv.finances.lombok.apptags.table.view.html.TableActions;
import fr.gouv.finances.lombok.apptags.util.HtmlBuilder;

/**
 * Une cellule d'entête de colonne.
 */
public class HeaderCell implements Cell
{

    /**
     * Constructeur de la classe HeaderCell.java
     */
    public HeaderCell()
    {
        super();

    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.cell.Cell#getExportDisplay(fr.gouv.finances.lombok.apptags.table.core.TableModel,
     *      fr.gouv.finances.lombok.apptags.table.bean.Column)
     */
    @Override
    public String getExportDisplay(TableModel model, Column column)
    {
        return column.getTitle();
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.cell.Cell#getHtmlDisplay(fr.gouv.finances.lombok.apptags.table.core.TableModel,
     *      fr.gouv.finances.lombok.apptags.table.bean.Column)
     */
    @Override
    public String getHtmlDisplay(TableModel model, Column column)
    {
        HtmlBuilder html = new HtmlBuilder();

        String headerClass = null;
        String sortImage = null;
        String sortOrder = null;

        headerClass = column.getHeaderClass();

        if (TableModelUtils.isSorted(model, column.getAlias()))
        {
            sortOrder = model.getLimit().getSort().getSortOrder();

            if (sortOrder.equals(TableConstants.SORT_DEFAULT))
            {
                sortOrder = TableConstants.SORT_ASC;
            }
            else if (sortOrder.equals(TableConstants.SORT_ASC))
            {
                headerClass = model.getPreferences().getPreference(PreferencesConstants.TABLE_HEADER_SORT_CLASS);
                sortImage = BuilderUtils.getImage(model, BuilderConstants.SORT_ASC_IMAGE);
                sortOrder = TableConstants.SORT_DESC;
            }
            else if (sortOrder.equals(TableConstants.SORT_DESC))
            {
                headerClass = model.getPreferences().getPreference(PreferencesConstants.TABLE_HEADER_SORT_CLASS);
                sortImage = BuilderUtils.getImage(model, BuilderConstants.SORT_DESC_IMAGE);
                sortOrder = TableConstants.SORT_DEFAULT;
            }
        }
        else
        {
            sortOrder = TableConstants.SORT_ASC; // défaut
        }

        buildHeaderHtml(html, model, column, headerClass, sortImage, sortOrder);

        return html.toString();
    }

    /**
     * methode Builds the header html :
     * 
     * @param html
     * @param model
     * @param column
     * @param headerClass
     * @param sortImage
     * @param sortOrder
     */
    protected void buildHeaderHtml(HtmlBuilder html, TableModel model, Column column, String headerClass,
        String sortImage, String sortOrder)
    {
        html.th(2);
        StringBuilder columnId = new StringBuilder();
        columnId.append(model.getTableHandler().prefixWithTableId());
        columnId.append(TableConstants.COLUMN);
        columnId.append(column.getAlias());
        html.id(columnId.toString());

        html.tabindex("0");

        if (column.isSortable())
        {
            headerClass += " " + BuilderConstants.TABLE_HEADER_SORT_CSS;

        }

        if (StringUtils.isNotEmpty(headerClass))
        {
            html.styleClass(headerClass);
        }

        StringBuilder styleValue = new StringBuilder();

        if (StringUtils.isNotEmpty(column.getWidth()) && (!column.isShowResponsive()))
        {
            styleValue.append("width:");
            styleValue.append(column.getWidth());
            styleValue.append(";");
        }

        if (StringUtils.isNotEmpty(column.getHeaderStyle()))
        {
            styleValue.append(column.getHeaderStyle());
        }

        if (StringUtils.isNotEmpty(styleValue.toString()))
        {
            html.style(styleValue.toString());
        }

        if (column.isShowResponsive())
        {
            html.dataAttributes(column.getDataAttributes());
        }

        if (column.isSortable())
        {
            if (sortOrder.equals(TableConstants.SORT_ASC))
            {
                StringBuilder onMouseOverVal = new StringBuilder(29);
                onMouseOverVal.append("this.className='");
                onMouseOverVal.append(BuilderConstants.TABLE_HEADER_SORT_CSS);
                onMouseOverVal.append("';this.style.cursor='pointer'");
                html.onmouseover(onMouseOverVal.toString());

                if (StringUtils.isNotEmpty(headerClass))
                {
                    StringBuilder onMouseOut = new StringBuilder();
                    onMouseOverVal.append("this.className='");
                    onMouseOverVal.append(headerClass);
                    onMouseOverVal.append("';this.style.cursor='default'");
                    html.onmouseout(onMouseOut.toString());
                }
                else
                {
                    StringBuilder onMouseOut = new StringBuilder();
                    onMouseOverVal.append("this.className='");
                    onMouseOverVal.append(BuilderConstants.TABLE_HEADER_CSS);
                    onMouseOverVal.append("';this.style.cursor='default'");
                    html.onmouseout(onMouseOut.toString());
                }
            }

            if (sortOrder.equals(TableConstants.SORT_DEFAULT) || sortOrder.equals(TableConstants.SORT_DESC))
            {
                html.onmouseover("this.style.cursor='pointer'");
                html.onmouseout("this.style.cursor='default'");
            }

            html.onclick(new TableActions(model).getSortAction(column, sortOrder));

            boolean showTooltips = model.getTableHandler().getTable().isShowTooltips();
            if (showTooltips)
            {
                String headercellTooltip =
                    model.getMessages().getMessage(MessagesConstants.COLUMN_HEADERCELL_TOOLTIP_SORT);
                StringBuilder titleMsg = new StringBuilder();
                titleMsg.append(headercellTooltip);
                titleMsg.append(" ");
                titleMsg.append(column.getTitle());
                html.title(titleMsg.toString());
            }
        }

        html.close();

        html.append(column.getTitle());

        if (column.isSortable() && StringUtils.isNotEmpty(sortImage))
        {
            html.nbsp();
            html.img();
            html.src(sortImage);
            html.alt(sortImage);
            html.style("border:0");
            html.xclose();
        }

        html.thEnd();
    }
}
