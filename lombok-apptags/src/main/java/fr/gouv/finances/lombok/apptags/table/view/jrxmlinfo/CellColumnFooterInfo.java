/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.apptags.table.view.jrxmlinfo;

import java.io.Serializable;

import net.sf.jasperreports.engine.type.HorizontalAlignEnum;

/**
 * Class CellColumnFooterInfo
 */
public class CellColumnFooterInfo implements Serializable
{

    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /**
     * Enum TypeCalcul
     */
    public static enum TypeCalcul
    {

        /** somme. */
        somme,
        /** moyenne. */
        moyenne,
        /** min. */
        min,
        /** max. */
        max,
        /** decompte. */
        decompte,
        /** decomptenonnulls. */
        decomptenonnulls,
        /** pourcentagedistinct. */
        pourcentagedistinct,
        /** aucun. */
        aucun
    };

    /**
     * Enum InitialisationCalcul
     */
    public static enum InitialisationCalcul
    {

        /** rapport. */
        rapport,
        /** page. */
        page
    };

    /** titre. */
    private String titre;

    /** Propriété qui sert de base au calcul. */
    private String propriete;

    /** Alias du nom de la colonne - sert pour nommer la variable de calcul. */
    private String alias;

    /** Type de calcul effectué (total, moyenne, decompte, minimum, maximum). */
    private TypeCalcul cellTypeCalcul;

    /** Définit quand le calcul est initialisé. */
    private InitialisationCalcul cellInitialisation;

    /** clazz. */
    private Class<? extends Object> clazz;

    /** Pattern de formatage à appliquer à la cellule. */
    private String pattern;

    /** Type des cellules de la colonne (au sens ec). */
    private String cellType;

    /** Unité utilisée pour indiquer la largeur de la colonne (non utilisé). */
    private String unit;

    /** Alignement du contenu de la cellule. */
    private HorizontalAlignEnum alignement;

    /** Référence vers la cellule précédente dans la ligne de pied de colonne. */
    private CellColumnFooterInfo previousCell;

    /** Référence vers la ligne précédente de pied de colonne. */
    private CalcRowFooterInfo parentRowFooterInfo;

    /** Largeur de la cellule. */
    private int width;

    /** Hauteur de la cellule. */
    private int height;

    /** Non utilisé. */
    private int spaceBefore;

    /** Inique si la cellule est abosrbér par la cellule parent. */
    private boolean absorbe = false;

    /** merge with child. */
    private boolean mergeWithChild = false;

    /**
     * Indique si la cellule est découpée en deux ou non pour contenir à la fois le titre et la valeur de la celulle
     * calculée.
     */
    private boolean split = false;

    /**
     * Instanciation de cell column footer info.
     */
    public CellColumnFooterInfo()
    {
        super();
    }

    /**
     * Verifie si calculated cell.
     * 
     * @return true, si c'est calculated cell
     */
    public boolean isCalculatedCell()
    {
        return (this.getCellTypeCalcul() != null && this.getCellTypeCalcul() != TypeCalcul.aucun);
    }

    /**
     * Calcule la largeur d'une cellule représentant un titre.
     * 
     * @return width calcule
     */
    public int getWidthCalcule()
    {
        return this.width;
    }

    /**
     * Calcule l'abscisse de coin haut gauche de la cellule représentant un titre.
     * 
     * @return x left corner calcule
     */
    public int getXLeftCornerCalcule()
    {
        int abs = 0;
        if (previousCell != null)
        {
            abs = abs + this.previousCell.getXLeftCornerCalcule() + this.previousCell.getWidthCalcule();
        }

        return abs;
    }

    /**
     * Calcule l'ordonnée du coin haut gauche de la cellule représentant un titre.
     * 
     * @return y left corner calcule
     */
    public int getYLeftCornerCalcule()
    {
        int ord = 0;
        if (parentRowFooterInfo != null && parentRowFooterInfo.getPreviousCalcRowFooterInfo() == null)
        {
            // ord = this.spaceBefore;
        }
        else if (parentRowFooterInfo != null && parentRowFooterInfo.getPreviousCalcRowFooterInfo() != null)
        {
            CellColumnFooterInfo cellColumnFooterInfo =
                parentRowFooterInfo.getPreviousCalcRowFooterInfo().getCellules().get(0);
            ord = cellColumnFooterInfo.getYLeftCornerCalcule() + parentRowFooterInfo.getHeight();
        }

        return ord;
    }

    /**
     * Checks if is indique si la cellule est découpée en deux ou non pour contenir à la fois le titre et la valeur de
     * la celulle calculée.
     * 
     * @return the indique si la cellule est découpée en deux ou non pour contenir à la fois le titre et la valeur de la
     *         celulle calculée
     */
    public boolean isSplit()
    {
        return split;
    }

    /**
     * Sets the indique si la cellule est découpée en deux ou non pour contenir à la fois le titre et la valeur de la
     * celulle calculée.
     * 
     * @param split the new indique si la cellule est découpée en deux ou non pour contenir à la fois le titre et la
     *        valeur de la celulle calculée
     */
    public void setSplit(boolean split)
    {
        this.split = split;
    }

    /**
     * Gets the référence vers la ligne précédente de pied de colonne.
     * 
     * @return the référence vers la ligne précédente de pied de colonne
     */
    public CalcRowFooterInfo getParentRowFooterInfo()
    {
        return parentRowFooterInfo;
    }

    /**
     * Sets the référence vers la ligne précédente de pied de colonne.
     * 
     * @param parentRowFooterInfo the new référence vers la ligne précédente de pied de colonne
     */
    public void setParentRowFooterInfo(CalcRowFooterInfo parentRowFooterInfo)
    {
        this.parentRowFooterInfo = parentRowFooterInfo;
    }

    /**
     * Gets the référence vers la cellule précédente dans la ligne de pied de colonne.
     * 
     * @return the référence vers la cellule précédente dans la ligne de pied de colonne
     */
    public CellColumnFooterInfo getPreviousCell()
    {
        return previousCell;
    }

    /**
     * Sets the référence vers la cellule précédente dans la ligne de pied de colonne.
     * 
     * @param previousCell the new référence vers la cellule précédente dans la ligne de pied de colonne
     */
    public void setPreviousCell(CellColumnFooterInfo previousCell)
    {
        this.previousCell = previousCell;
    }

    /**
     * Gets the largeur de la cellule.
     * 
     * @return the largeur de la cellule
     */
    public int getWidth()
    {
        return width;
    }

    /**
     * Sets the largeur de la cellule.
     * 
     * @param width the new largeur de la cellule
     */
    public void setWidth(int width)
    {
        this.width = width;
    }

    /**
     * Gets the hauteur de la cellule.
     * 
     * @return the hauteur de la cellule
     */
    public int getHeight()
    {
        return height;
    }

    /**
     * Sets the hauteur de la cellule.
     * 
     * @param height the new hauteur de la cellule
     */
    public void setHeight(int height)
    {
        this.height = height;
    }

    /**
     * Gets the non utilisé.
     * 
     * @return the non utilisé
     */
    public int getSpaceBefore()
    {
        return spaceBefore;
    }

    /**
     * Sets the non utilisé.
     * 
     * @param spaceBefore the new non utilisé
     */
    public void setSpaceBefore(int spaceBefore)
    {
        this.spaceBefore = spaceBefore;
    }

    /**
     * Checks if is inique si la cellule est abosrbér par la cellule parent.
     * 
     * @return the inique si la cellule est abosrbér par la cellule parent
     */
    public boolean isAbsorbe()
    {
        return absorbe;
    }

    /**
     * Sets the inique si la cellule est abosrbér par la cellule parent.
     * 
     * @param absorbe the new inique si la cellule est abosrbér par la cellule parent
     */
    public void setAbsorbe(boolean absorbe)
    {
        this.absorbe = absorbe;
    }

    /**
     * Verifie si merge with child.
     * 
     * @return true, si c'est merge with child
     */
    public boolean isMergeWithChild()
    {
        return mergeWithChild;
    }

    /**
     * Modificateur de l attribut merge with child.
     * 
     * @param mergeWithChild le nouveau merge with child
     */
    public void setMergeWithChild(boolean mergeWithChild)
    {
        this.mergeWithChild = mergeWithChild;
    }

    /**
     * Gets the type de calcul effectué (total, moyenne, decompte, minimum, maximum).
     * 
     * @return the type de calcul effectué (total, moyenne, decompte, minimum, maximum)
     */
    public TypeCalcul getCellTypeCalcul()
    {
        return cellTypeCalcul;
    }

    /**
     * Sets the type de calcul effectué (total, moyenne, decompte, minimum, maximum).
     * 
     * @param cellTypeCalcul the new type de calcul effectué (total, moyenne, decompte, minimum, maximum)
     */
    public void setCellTypeCalcul(TypeCalcul cellTypeCalcul)
    {
        this.cellTypeCalcul = cellTypeCalcul;
    }

    /**
     * Gets the alignement du contenu de la cellule.
     * 
     * @return the alignement du contenu de la cellule
     */
    public HorizontalAlignEnum getAlignement()
    {
        return alignement;
    }

    /**
     * Sets the alignement du contenu de la cellule.
     * 
     * @param alignement the new alignement du contenu de la cellule
     */
    public void setAlignement(HorizontalAlignEnum alignement)
    {
        this.alignement = alignement;
    }

    /**
     * Gets the propriété qui sert de base au calcul.
     * 
     * @return the propriété qui sert de base au calcul
     */
    public String getPropriete()
    {
        return propriete;
    }

    /**
     * Sets the propriété qui sert de base au calcul.
     * 
     * @param propriete the new propriété qui sert de base au calcul
     */
    public void setPropriete(String propriete)
    {
        this.propriete = propriete;
    }

    /**
     * Gets the unité utilisée pour indiquer la largeur de la colonne (non utilisé).
     * 
     * @return the unité utilisée pour indiquer la largeur de la colonne (non utilisé)
     */
    public String getUnit()
    {
        return unit;
    }

    /**
     * Sets the unité utilisée pour indiquer la largeur de la colonne (non utilisé).
     * 
     * @param unit the new unité utilisée pour indiquer la largeur de la colonne (non utilisé)
     */
    public void setUnit(String unit)
    {
        this.unit = unit;
    }

    /**
     * Gets the type des cellules de la colonne (au sens ec).
     * 
     * @return the type des cellules de la colonne (au sens ec)
     */
    public String getCellType()
    {
        return cellType;
    }

    /**
     * Sets the type des cellules de la colonne (au sens ec).
     * 
     * @param cellType the new type des cellules de la colonne (au sens ec)
     */
    public void setCellType(String cellType)
    {
        this.cellType = cellType;
    }

    /**
     * Gets the clazz.
     * 
     * @return the clazz
     */
    public Class<? extends Object> getClazz()
    {
        return clazz;
    }

    /**
     * Modificateur de l attribut clazz.
     * 
     * @param clazz le nouveau clazz
     */
    public void setClazz(Class<? extends Object> clazz)
    {
        this.clazz = clazz;
    }

    /**
     * Gets the pattern de formatage à appliquer à la cellule.
     * 
     * @return the pattern de formatage à appliquer à la cellule
     */
    public String getPattern()
    {
        return pattern;
    }

    /**
     * Sets the pattern de formatage à appliquer à la cellule.
     * 
     * @param pattern the new pattern de formatage à appliquer à la cellule
     */
    public void setPattern(String pattern)
    {
        this.pattern = pattern;
    }

    /**
     * Accesseur de l attribut titre.
     * 
     * @return titre
     */
    public String getTitre()
    {
        return titre;
    }

    /**
     * Modificateur de l attribut titre.
     * 
     * @param titre le nouveau titre
     */
    public void setTitre(String titre)
    {
        this.titre = titre;
    }

    /**
     * Gets the alias du nom de la colonne - sert pour nommer la variable de calcul.
     * 
     * @return the alias du nom de la colonne - sert pour nommer la variable de calcul
     */
    public String getAlias()
    {
        return alias;
    }

    /**
     * Sets the alias du nom de la colonne - sert pour nommer la variable de calcul.
     * 
     * @param alias the new alias du nom de la colonne - sert pour nommer la variable de calcul
     */
    public void setAlias(String alias)
    {
        this.alias = alias;
    }

    /**
     * Gets the définit quand le calcul est initialisé.
     * 
     * @return the définit quand le calcul est initialisé
     */
    public InitialisationCalcul getCellInitialisation()
    {
        return cellInitialisation;
    }

    /**
     * Sets the définit quand le calcul est initialisé.
     * 
     * @param cellInitialisation the new définit quand le calcul est initialisé
     */
    public void setCellInitialisation(InitialisationCalcul cellInitialisation)
    {
        this.cellInitialisation = cellInitialisation;
    }

}
