/*
/*Copyright (c) 2014 DGFiP - Tous droits réservés/* * Copyright 2004 original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package fr.gouv.finances.lombok.apptags.table.tag;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

import fr.gouv.finances.lombok.apptags.util.ExceptionUtils;

/**
 * Class ParameterTag
 * 
 * jsp.tag : name="parameter" display-name="ParameterTag" body-content="JSP" description="Ajoute un attribut lors de la
 *          soumission d'une requête de filtre, pagination ou tri. Dans l'URL, ajout d'un paramètre sous la forme
 *          &name=value. Dans le formulaire, ajout d'un champ caché <input type=hidden name= value=>"
 */
public class ParameterTag extends TagSupport
{

    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** name. */
    private String name;

    /** value. */
    private Object value;

    /**
     * Constructeur de la classe ParameterTag.java
     *
     */
    public ParameterTag()
    {
        super();
        
    }

    /**
     * Modificateur de l attribut name.
     * 
     * @param name le nouveau name
     * jsp.attribute :  description="Nom du paramètre." required="true" rtexprvalue="true"
     */
    public void setName(String name)
    {
        this.name = name;
    }

    /**
     * Modificateur de l attribut value.
     * 
     * @param value le nouveau value
     * jsp.attribute :  description="Valeur du paramètre." required="false" rtexprvalue="true"
     */
    public void setValue(Object value)
    {
        this.value = value;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see javax.servlet.jsp.tagext.TagSupport#doEndTag()
     */
    @Override
    public int doEndTag() throws JspException
    {
        try
        {
            if (TagUtils.isIteratingBody(this))
            {
                return EVAL_PAGE;
            }

            String name = TagUtils.evaluateExpressionAsString("name", this.name, this, pageContext);
            Object value = TagUtils.evaluateExpressionAsObject("value", this.value, this, pageContext);

            if (value == null)
            {
                value = pageContext.getRequest().getParameterValues(name);
            }

            TagUtils.getModel(this).addParameter(name, value);

        }
        catch (Exception e)
        {
            throw new JspException("ParameterTag.doEndTag() Problem: " + ExceptionUtils.formatStackTrace(e));
        }

        return EVAL_PAGE;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see javax.servlet.jsp.tagext.TagSupport#release()
     */
    @Override
    public void release()
    {
        name = null;
        value = null;
        super.release();
    }
}
