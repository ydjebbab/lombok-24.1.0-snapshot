/*
 * Copyright (c) 2014 DGFiP - Tous droits réservés
 */
/*
/*Copyright (c) 2014 DGFiP - Tous droits réservés/* * Copyright 2004 original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package fr.gouv.finances.lombok.apptags.table.core;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Locale;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import fr.gouv.finances.lombok.apptags.table.bean.Column;
import fr.gouv.finances.lombok.apptags.table.callback.FilterRowsCallback;
import fr.gouv.finances.lombok.apptags.table.callback.RetrieveRowsCallback;
import fr.gouv.finances.lombok.apptags.table.callback.SortRowsCallback;
import fr.gouv.finances.lombok.apptags.table.cell.Cell;
import fr.gouv.finances.lombok.apptags.table.context.Context;
import fr.gouv.finances.lombok.apptags.table.limit.Limit;
import fr.gouv.finances.lombok.apptags.table.limit.Sort;
import fr.gouv.finances.lombok.apptags.util.ExtremeUtils;

/**
 * Class TableModelUtils
 */
public final class TableModelUtils
{

    /** logger. */
    private static Log logger = LogFactory.getLog(TableModelUtils.class);

    /**
     * Instanciation de table model utils.
     */
    private TableModelUtils()
    {
    }

    /**
     * methode Retrieve rows :
     * 
     * @param model model
     * @return collection
     * @throws Exception the exception
     */
    public static Collection<Object> retrieveRows(TableModel model) throws Exception
    {
        RetrieveRowsCallback retrieveRowsCallback = TableCache.getInstance().getRetrieveRowsCallback(model);

        return retrieveRowsCallback.retrieveRows(model);
    }

    /**
     * methode Filter rows :
     * 
     * @param model model
     * @param rows rows
     * @return collection
     * @throws Exception the exception
     */
    public static Collection<Object> filterRows(TableModel model, Collection<Object> rows) throws Exception
    {
        FilterRowsCallback filterRowsCallback = TableCache.getInstance().getFilterRowsCallback(model);

        return filterRowsCallback.filterRows(model, rows);
    }

    /**
     * methode Sort rows :
     * 
     * @param model model
     * @param rows rows
     * @return collection
     * @throws Exception the exception
     */
    public static Collection<Object> sortRows(TableModel model, Collection<Object> rows) throws Exception
    {
        SortRowsCallback sortRowsCallback = TableCache.getInstance().getSortRowsCallback(model);

        return sortRowsCallback.sortRows(model, rows);
    }

    /**
     * Retourne toutes les lignes depuis la première page jusqu'à la page courante.
     * 
     * @param model model
     * @param rows rows La collection de beans après filtrage et tri
     * @return Les lignes depuis la première page jusqu'à la page courante
     */
    public static Collection<Object> getRowsFromFirstPageToCurrent(TableModel model, Collection<Object> rows)
    {
        Limit limit = model.getLimit();

        int rowStart = 0;
        int rowEnd = limit.getRowEnd();

        Collection<Object> results = null;

        // Cas normal . utilisation de Limit et pagination pour une ensemble
        // donné de lignes
        if (rowStart >= rows.size())
        {
            if (logger.isDebugEnabled())
            {
                logger
                    .debug("La première ligne à afficher est supérieure à  items.size(). On retourne les lignes disponibles.");
            }

            results = rows;
        }

        else
        {
            if (rowEnd > rows.size())
            {
                if (logger.isWarnEnabled())
                {
                    logger
                        .warn("La dernière ligne à afficher est supérieure à items.size(). Il est impossible de retourner autant d'éléments.");
                }

                rowEnd = rows.size();
            }

            results = new ArrayList<Object>();
            for (int i = rowStart; i < rowEnd; i++)
            {
                Object bean = ((List<Object>) rows).get(i);
                results.add(bean);
            }
        }
        return results;
    }

    /**
     * Retourne les lignes de la page courante.
     * 
     * @param model model
     * @param rows rows La collection de bean après filtrage et tri
     * @return Les lignes de la page courante
     */
    public static Collection<Object> getCurrentRows(TableModel model, Collection<Object> rows)
    {
        Limit limit = model.getLimit();

        int rowStart = limit.getRowStart();
        int rowEnd = limit.getRowEnd();

        Collection<Object> results = null;

        // Cas normal . utilisation de Limit et pagination pour une ensemble
        // donné de lignes
        if (rowStart >= rows.size())
        {
            if (logger.isDebugEnabled())
            {
                logger
                    .debug("La première ligne à afficher est supérieure à  items.size(). On retourne les lignes disponibles.");
            }

            results = rows;
        }

        else
        {
            if (rowEnd > rows.size())
            {
                if (logger.isWarnEnabled())
                {
                    logger
                        .warn("La dernière ligne à afficher est supérieure à items.size(). Il est impossible de retourner autant d'éléments.");
                }

                rowEnd = rows.size();
            }

            results = new ArrayList<>();
            for (int i = rowStart; i < rowEnd; i++)
            {
                Object bean = ((List<Object>) rows).get(i);
                results.add(bean);
            }
        }
        return results;
    }

    /**
     * Verifie si sorted.
     * 
     * @param model model
     * @param alias alias
     * @return true, si c'est sorted
     */
    public static boolean isSorted(TableModel model, String alias)
    {
        boolean result = false;
        Sort sort = model.getLimit().getSort();

        if (sort.isSorted() && alias.equals(sort.getAlias()))
        {
            result = true;
        }

        return result;
    }

    /**
     * Détermine si le texte en paramètre est une clé d'un fichier properties. Si la chaîne contient le caractère '.',
     * on considère que c'est une clé
     * 
     * @param value Le texte à tester
     * @return True Si le texte en paramètre est une clé d'un fichier properties
     */
    public static boolean isResourceBundleProperty(String value)
    {
        boolean result = false;
        if (StringUtils.contains(value, "."))
        {
            result = true;
        }

        return result;
    }

    /**
     * Accesseur de l attribut locale.
     * 
     * @param context
     * @param preferences
     * @param locale
     * @return locale
     */
    public static Locale getLocale(Context context, Preferences preferences, String locale)
    {
        if (StringUtils.isEmpty(locale))
        {
            locale = preferences.getPreference(PreferencesConstants.TABLE_LOCALE);
        }

        if (StringUtils.isBlank(locale))
        {
            return context.getLocale();
        }

        Locale result = null;

        String[] parts = StringUtils.split(locale, "_");
        String language = parts[0];
        if (parts.length == 2)
        {
            String country = parts[1];
            result = new Locale(language, country);
        }
        else
        {
            result = new Locale(language, "");
        }

        return result;
    }

    /**
     * Accesseur de l attribut preferences location.
     * 
     * @param context context
     * @return preferences location
     */
    public static String getPreferencesLocation(Context context)
    {
        String result = (String) context.getApplicationInitParameter(TableConstants.PREFERENCES_LOCATION);

        if (StringUtils.isBlank(result))
        {
            result = null;
            if (logger.isDebugEnabled())
            {
                logger
                    .debug("Aucune préférence préférence spécifique n'est définie.Il faut inclure l'élément context-param extremecomponentsPreferencesLocation adns le fichioer web.xml ");
            }
        }

        return result;
    }

    /**
     * Accesseur de l attribut messages location.
     * 
     * @param context context
     * @return messages location
     */
    public static String getMessagesLocation(Context context)
    {
        String result = (String) context.getApplicationInitParameter(TableConstants.MESSAGES_LOCATION);
        if (StringUtils.isBlank(result))
        {
            result = null;

            if (logger.isDebugEnabled())
            {
                logger.debug("There are no custom messages defined. You need to include the context-param "
                    + "extremecomponentsMessagesLocation in the web.xml to include custom messages.");
            }
        }
        return result;
    }

    /**
     * Accesseur de l attribut cell.
     * 
     * @param column column
     * @return cell
     */
    public static Cell getCell(Column column)
    {
        return TableCache.getInstance().getCell(column.getCell());
    }

    /**
     * Accesseur de l attribut filter cell.
     * 
     * @param column column
     * @param value value
     * @return filter cell
     */
    public static Cell getFilterCell(Column column, Object value)
    {
        Cell cell = TableCache.getInstance().getCell(column.getFilterCell());
        column.setValue(value);
        return cell;
    }

    /**
     * Accesseur de l attribut header cell.
     * 
     * @param column column
     * @param value value
     * @return header cell
     */
    public static Cell getHeaderCell(Column column, Object value)
    {
        Cell cell = TableCache.getInstance().getCell(column.getHeaderCell());
        column.setValue(value);
        return cell;
    }

    /**
     * Accesseur de l attribut intercept column preference.
     * 
     * @param model model
     * @param intercept intercept
     * @return intercept column preference
     */
    public static String getInterceptColumnPreference(TableModel model, String intercept)
    {
        return getInterceptPreference(model, intercept, PreferencesConstants.COLUMN_INTERCEPTOR);
    }

    /**
     * Accesseur de l attribut intercept row preference.
     * 
     * @param model model
     * @param intercept intercept
     * @return intercept row preference
     */
    public static String getInterceptRowPreference(TableModel model, String intercept)
    {
        return getInterceptPreference(model, intercept, PreferencesConstants.ROW_INTERCEPTOR);
    }

    /**
     * Accesseur de l attribut edition service preference.
     * 
     * @param model model
     * @param editionService editionService
     * @return edition service preference
     */
    public static String getEditionServicePreference(TableModel model, String editionService)
    {
        return getInterceptPreference(model, editionService, PreferencesConstants.EXPORT_EDITION_SERVICE);
    }

    /**
     * Accesseur de l attribut edition service preference.
     * 
     * @param model model
     * @param editionService editionService
     * @param editionServicePreference editionServicePreference
     * @return edition service preference
     */
    public static String getEditionServicePreference(TableModel model, String editionService,
        String editionServicePreference)
    {
        String result;

        if (StringUtils.isNotBlank(editionService))
        {
            result = model.getPreferences().getPreference(editionServicePreference + editionService);
            if (StringUtils.isBlank(result))
            {
                result = editionService;
            }
        }
        else
        {
            result =
                model.getPreferences().getPreference(editionServicePreference + TableConstants.DEFAULT_EDITION_SERVICE);
        }

        return result;
    }

    /**
     * Accesseur de l attribut intercept preference.
     * 
     * @param model model
     * @param intercept intercept
     * @param interceptPreference interceptPreference
     * @return intercept preference
     */
    public static String getInterceptPreference(TableModel model, String intercept, String interceptPreference)
    {
        String result;

        if (StringUtils.isNotBlank(intercept))
        {
            result = model.getPreferences().getPreference(interceptPreference + intercept);
            if (StringUtils.isBlank(result))
            {
                result = intercept;
            }
        }
        else
        {
            result = model.getPreferences().getPreference(interceptPreference + TableConstants.DEFAULT_INTERCEPT);
        }

        return result;
    }

    /**
     * Accesseur de l attribut alias.
     * 
     * @param alias alias
     * @param property property
     * @return alias
     */
    public static String getAlias(String alias, String property)
    {
        if (StringUtils.isBlank(alias) && StringUtils.isNotBlank(property))
        {
            return property;
        }

        return alias;
    }

    /**
     * Accesseur de l attribut column property value.
     * 
     * @param bean bean
     * @param property property
     * @return column property value
     */
    public static Object getColumnPropertyValue(Object bean, String property)
    {
        Object result = null;

        try
        {
            if (ExtremeUtils.isBeanPropertyReadable(bean, property))
            {
                result = PropertyUtils.getProperty(bean, property);
            }
        }
        catch (Exception e)
        {
            if (logger.isDebugEnabled())
            {
                StringBuilder   mes = new StringBuilder(79);
                mes.append("Impossible d'accéder à la propriété [");
                mes.append(property);
                mes.append("] . Le bean ou la propriété sont nulls.");
                logger.debug(mes.toString());
            }
        }

        return result;
    }

    /**
     * Accesseur de l attribut auto generate columns preference.
     * 
     * @param model model
     * @param autoGenerateColumns autoGenerateColumns
     * @return auto generate columns preference
     */
    public static String getAutoGenerateColumnsPreference(TableModel model, String autoGenerateColumns)
    {
        String result = autoGenerateColumns;

        if (StringUtils.isNotBlank(autoGenerateColumns))
        {
            result =
                model.getPreferences().getPreference(
                    PreferencesConstants.COLUMNS_AUTO_GENERATE_COLUMNS + autoGenerateColumns);
            if (StringUtils.isBlank(result))
            {
                result = autoGenerateColumns;
            }
        }

        return result;
    }

    /**
     * Accesseur de l attribut messages.
     * 
     * @param model model
     * @return messages
     */
    public static Messages getMessages(TableModel model)
    {
        String messages = model.getPreferences().getPreference(PreferencesConstants.MESSAGES);
        try
        {
            Class<? extends Object> classDefinition = Class.forName(messages);
            return (Messages) classDefinition.newInstance();

        }
        catch (Exception exception)
        {
            StringBuilder mes = new StringBuilder(39);
            mes.append("Impossible de créer les messages [");
            mes.append(messages);
            mes.append("La classe n'existe pas ou n'a pas été trouvée");
            logger.error(mes.toString(), exception);
            throw new IllegalStateException(mes.toString(), exception);
        }
    }

    /**
     * Le paramètre doit être un String[]. Les objects NULL, String ou List sont convertis en String[]. On essaiera de
     * transformer en String les autres objets
     * 
     * @param value La valeur à convertir en String[]
     * @return un String[]
     */
    @SuppressWarnings("unchecked")
    public static String[] getValueAsArray(Object value)
    {
        String[] result;
        if (value == null)
        {
            result = new String[] {}; // put in a placeholder
        }

        else if (value instanceof String[])
        {
            result = (String[]) value;
        }
        else if (value instanceof List)
        {
            List<Object> valueList = (List<Object>) value;
            result = valueList.toArray(new String[valueList.size()]);
        }
        else
        {
            result = new String[] {value.toString()};
        }
        return result;
    }
}
