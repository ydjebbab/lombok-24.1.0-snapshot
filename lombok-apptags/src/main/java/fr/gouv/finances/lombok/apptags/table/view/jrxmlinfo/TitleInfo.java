/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.apptags.table.view.jrxmlinfo;

/**
 * Class TitleInfo
 */
public class TitleInfo extends AbstractBandInfo
{

    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** rectangle11. */
    private RectangleInfo rectangle11;

    /** rectangle12. */
    private RectangleInfo rectangle12;

    /** rectangle13. */
    private RectangleInfo rectangle13;

    /** rectangle21. */
    private RectangleInfo rectangle21;

    /** yleft corner title text. */
    private int yleftCornerTitleText;

    /** max height title text. */
    private int maxHeightTitleText;

    /** max height date text. */
    private int maxHeightDateText;

    /** date style. */
    private FontStyleInfo dateStyle;

    /** logo mariane. */
    private LogoInfo logoMariane;

    /** logo ministere. */
    private LogoInfo logoMinistere;

    /** use application logo. */
    private Boolean useApplicationLogo;

    /** logo application. */
    private LogoInfo logoApplication;

    /** title style. */
    private FontStyleInfo titleStyle;

    /** texte complement style. */
    private FontStyleInfo texteComplementStyle;

    /**
     * Instanciation de title info.
     */
    public TitleInfo()
    {
        super();
    }

    /**
     * Accesseur de l attribut logo application.
     * 
     * @return logo application
     */
    public LogoInfo getLogoApplication()
    {
        return logoApplication;
    }

    /**
     * Modificateur de l attribut logo application.
     * 
     * @param logoApplication le nouveau logo application
     */
    public void setLogoApplication(LogoInfo logoApplication)
    {
        this.logoApplication = logoApplication;
    }

    /**
     * Accesseur de l attribut max height date text.
     * 
     * @return max height date text
     */
    public int getMaxHeightDateText()
    {
        return maxHeightDateText;
    }

    /**
     * Modificateur de l attribut max height date text.
     * 
     * @param maxHeightDateText le nouveau max height date text
     */
    public void setMaxHeightDateText(int maxHeightDateText)
    {
        this.maxHeightDateText = maxHeightDateText;
    }

    /**
     * Accesseur de l attribut yleft corner title text.
     * 
     * @return yleft corner title text
     */
    public int getYleftCornerTitleText()
    {
        return yleftCornerTitleText;
    }

    /**
     * Modificateur de l attribut yleft corner title text.
     * 
     * @param yleftCornerTitleText le nouveau yleft corner title text
     */
    public void setYleftCornerTitleText(int yleftCornerTitleText)
    {
        this.yleftCornerTitleText = yleftCornerTitleText;
    }

    /**
     * Accesseur de l attribut max height title text.
     * 
     * @return max height title text
     */
    public int getMaxHeightTitleText()
    {
        return maxHeightTitleText;
    }

    /**
     * Modificateur de l attribut max height title text.
     * 
     * @param maxHeightTitleText le nouveau max height title text
     */
    public void setMaxHeightTitleText(int maxHeightTitleText)
    {
        this.maxHeightTitleText = maxHeightTitleText;
    }

    /**
     * Accesseur de l attribut logo ministere.
     * 
     * @return logo ministere
     */
    public LogoInfo getLogoMinistere()
    {
        return logoMinistere;
    }

    /**
     * Modificateur de l attribut logo ministere.
     * 
     * @param logoMinistere le nouveau logo ministere
     */
    public void setLogoMinistere(LogoInfo logoMinistere)
    {
        this.logoMinistere = logoMinistere;
    }

    /**
     * Accesseur de l attribut logo mariane.
     * 
     * @return logo mariane
     */
    public LogoInfo getLogoMariane()
    {
        return logoMariane;
    }

    /**
     * Modificateur de l attribut logo mariane.
     * 
     * @param logoMariane le nouveau logo mariane
     */
    public void setLogoMariane(LogoInfo logoMariane)
    {
        this.logoMariane = logoMariane;
    }

    /**
     * Accesseur de l attribut date style.
     * 
     * @return date style
     */
    public FontStyleInfo getDateStyle()
    {
        return dateStyle;
    }

    /**
     * Modificateur de l attribut date style.
     * 
     * @param dateStyle le nouveau date style
     */
    public void setDateStyle(FontStyleInfo dateStyle)
    {
        this.dateStyle = dateStyle;
    }

    /**
     * Accesseur de l attribut rectangle11.
     * 
     * @return rectangle11
     */
    public RectangleInfo getRectangle11()
    {
        return rectangle11;
    }

    /**
     * Modificateur de l attribut rectangle11.
     * 
     * @param rectangle11 le nouveau rectangle11
     */
    public void setRectangle11(RectangleInfo rectangle11)
    {
        this.rectangle11 = rectangle11;
    }

    /**
     * Accesseur de l attribut rectangle12.
     * 
     * @return rectangle12
     */
    public RectangleInfo getRectangle12()
    {
        return rectangle12;
    }

    /**
     * Modificateur de l attribut rectangle12.
     * 
     * @param rectangle12 le nouveau rectangle12
     */
    public void setRectangle12(RectangleInfo rectangle12)
    {
        this.rectangle12 = rectangle12;
    }

    /**
     * Accesseur de l attribut rectangle13.
     * 
     * @return rectangle13
     */
    public RectangleInfo getRectangle13()
    {
        return rectangle13;
    }

    /**
     * Modificateur de l attribut rectangle13.
     * 
     * @param rectangle13 le nouveau rectangle13
     */
    public void setRectangle13(RectangleInfo rectangle13)
    {
        this.rectangle13 = rectangle13;
    }

    /**
     * Accesseur de l attribut rectangle21.
     * 
     * @return rectangle21
     */
    public RectangleInfo getRectangle21()
    {
        return rectangle21;
    }

    /**
     * Modificateur de l attribut rectangle21.
     * 
     * @param rectangle21 le nouveau rectangle21
     */
    public void setRectangle21(RectangleInfo rectangle21)
    {
        this.rectangle21 = rectangle21;
    }

    /**
     * Accesseur de l attribut title style.
     * 
     * @return title style
     */
    public FontStyleInfo getTitleStyle()
    {
        return titleStyle;
    }

    /**
     * Modificateur de l attribut title style.
     * 
     * @param titleStyle le nouveau title style
     */
    public void setTitleStyle(FontStyleInfo titleStyle)
    {
        this.titleStyle = titleStyle;
    }

    /**
     * Accesseur de l attribut texte complement style.
     * 
     * @return texte complement style
     */
    public FontStyleInfo getTexteComplementStyle()
    {
        return texteComplementStyle;
    }

    /**
     * Modificateur de l attribut texte complement style.
     * 
     * @param texteComplementStyle le nouveau texte complement style
     */
    public void setTexteComplementStyle(FontStyleInfo texteComplementStyle)
    {
        this.texteComplementStyle = texteComplementStyle;
    }

    /**
     * Accesseur de l attribut use application logo.
     * 
     * @return use application logo
     */
    public Boolean getUseApplicationLogo()
    {
        return useApplicationLogo;
    }

    /**
     * Modificateur de l attribut use application logo.
     * 
     * @param useApplicationLogo le nouveau use application logo
     */
    public void setUseApplicationLogo(Boolean useApplicationLogo)
    {
        this.useApplicationLogo = useApplicationLogo;
    }

}
