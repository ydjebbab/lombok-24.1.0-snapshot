/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.apptags.util;

/**
 * Class EcTableException
 */
public class EcTableException extends RuntimeException
{

    private static final long serialVersionUID = 1L;

    public EcTableException()
    {
        super();
    }

    /**
     * Instanciation de ec table exception.
     * 
     * @param message
     * @param cause
     */
    public EcTableException(String message, Throwable cause)
    {
        super(message, cause);
    }

    /**
     * Instanciation de ec table exception.
     * 
     * @param message
     */
    public EcTableException(String message)
    {
        super(message);
    }

    /**
     * Instanciation de ec table exception.
     * 
     * @param cause
     */
    public EcTableException(Throwable cause)
    {
        super(cause);
    }

}
