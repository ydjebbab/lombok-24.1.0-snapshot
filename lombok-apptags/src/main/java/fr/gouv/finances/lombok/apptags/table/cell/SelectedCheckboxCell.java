/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.apptags.table.cell;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import fr.gouv.finances.lombok.apptags.table.bean.Column;
import fr.gouv.finances.lombok.apptags.table.core.RetrievalUtils;
import fr.gouv.finances.lombok.apptags.table.core.TableModel;
import fr.gouv.finances.lombok.apptags.table.limit.Limit;
import fr.gouv.finances.lombok.apptags.table.view.html.ColumnBuilder;
import fr.gouv.finances.lombok.apptags.util.EcTableException;
import fr.gouv.finances.lombok.apptags.util.HtmlBuilder;

/*Associe un champs caché avec chaque checkbox
 * (une checkbox sans valeur n'est pas soumise avec le formulaire
 * alors que le champs caché est soumis)
 * Un javascript est inclu dans CpHtmlView qui met à jour le
 * champs caché
 * Un commentaire en plus pour le fun
 */

/**
 * Class SelectedCheckboxCell
 */
public class SelectedCheckboxCell extends DisplayCell
{

    private static final Log log = LogFactory.getLog(SelectedCheckboxCell.class);

    public SelectedCheckboxCell()
    {
        super();
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.cell.DisplayCell#getExportDisplay(fr.gouv.finances.lombok.apptags.table.core.TableModel,
     *      fr.gouv.finances.lombok.apptags.table.bean.Column)
     */
    @Override
    public String getExportDisplay(final TableModel model, final Column column)
    {
        return column.getPropertyValueAsString();
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.cell.AbstractCell#getHtmlDisplay(fr.gouv.finances.lombok.apptags.table.core.TableModel,
     *      fr.gouv.finances.lombok.apptags.table.bean.Column)
     */
    @Override
    public String getHtmlDisplay(final TableModel model, final Column column)
    {
        final ColumnBuilder columnbuilder = new ColumnBuilder(column);

        columnbuilder.tdStart();
        columnbuilder.tdClose();
        columnbuilder.tdBody(getCellValue(model, column));
        columnbuilder.tdEnd();
        return columnbuilder.toString();
    }

    /**
     * Accesseur de l attribut collection des elements selectionnes.
     * 
     * @param model
     * @param selectedRowids
     * @return collection des elements selectionnes
     */
    private Collection getCollectionDesElementsSelectionnes(final TableModel model, final String selectedRowids)
    {
        Collection result = null;

        result = (Collection) model.getContext().getRequestAttribute(selectedRowids);
        return result;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.cell.DisplayCell#getCellValue(fr.gouv.finances.lombok.apptags.table.core.TableModel,
     *      fr.gouv.finances.lombok.apptags.table.bean.Column)
     */
    @Override
    protected String getCellValue(final TableModel model, final Column column)
    {
        String result;

        // Récupération de l'attribut rowid passé en paramètre dans le
        // tag <ec:column>
        final String rowIdAttribute = column.getAttributeAsString("rowid");

        // Lecture de l'attribut id du tableau
        final String tableId = model.getTableHandler().getTable().getTableId();

        // Contruction de l'objet de paramétage de la case à cocher
        final SelectedCheckboxCellAttributes checkboxCellAttr =
            new SelectedCheckboxCellAttributes(tableId, rowIdAttribute);

        if (PreferencesConstants.ROWNUMBER_SELECTION.equalsIgnoreCase(checkboxCellAttr.getRowId()))
        {
            result = this.getCellValueFromRowcount(model, column, checkboxCellAttr);
        }
        else if (PreferencesConstants.HASHCODE_SELECTION.equalsIgnoreCase(checkboxCellAttr.getRowId()))
        {
            result = this.getCellValueFromHashcode(model, column, checkboxCellAttr);
        }
        else
        {
            result = this.getCellValueFromId(model, column, checkboxCellAttr);
        }

        return result;
    }

    /**
     * Accesseur de l attribut cell value from hashcode.
     * 
     * @param model
     * @param column
     * @param checkboxCellAttr
     * @return cell value from hashcode
     */
    protected String getCellValueFromHashcode(final TableModel model, final Column column,
        SelectedCheckboxCellAttributes checkboxCellAttr)
    {
        HtmlBuilder html = new HtmlBuilder();

        // Numéro de la ligne, suivi de son séparateur
        final String rowid = Integer.toString(model.getCurrentRowBean().hashCode());
        final String hashcode = rowid + checkboxCellAttr.getRowcountSep();

        // id html de la case à cocher
        final String idCheckbox = checkboxCellAttr.getPrefixIdCheckbox() + hashcode;

        // id html du champ caché associé
        final String idHiddenField = checkboxCellAttr.getPrefixIdHidden() + hashcode;

        // Lecture de la liste contenant les rowid des éléments
        // sélectionnés
        final Collection rowidSelected =
            this.getCollectionDesElementsSelectionnes(model, checkboxCellAttr.getSelectedRowids());

        // Si l'élément appartient à la liste, la case est cochée
        if (rowidSelected != null && rowidSelected.contains(rowid))
        {
            html.input("hidden").id(idHiddenField).name(checkboxCellAttr.getPrefixNameHidden() + hashcode).value(
                checkboxCellAttr.getPatternSelected()).xclose();

            html.input("checkbox").id(idCheckbox).name(hashcode);
            html.onclick(checkboxCellAttr.getOnClickJavascript() + checkboxCellAttr.getTableId() + "(this)");
            html.checked();
            html = this.ajouterOptionsSurCheckBox(html);
            html.xclose();
        }
        // Sinon la case n'est pas cochée
        else
        {
            html.input("hidden").id(idHiddenField).name(checkboxCellAttr.getPrefixNameHidden() + hashcode).value(
                checkboxCellAttr.getPatternUnselected()).xclose();
            html.input("checkbox").id(idCheckbox).name(hashcode);
            html.onclick(checkboxCellAttr.getOnClickJavascript() + checkboxCellAttr.getTableId() + "(this)");
            html = this.ajouterOptionsSurCheckBox(html);
            html.xclose();
        }

        return html.toString();

    }

    // Classe à surcharger pour ajouter des options html sur les checkbox
    // (disabled par exemple)

    /**
     * methode Ajouter options sur check box :
     * 
     * @param html
     * @return html builder
     */
    protected HtmlBuilder ajouterOptionsSurCheckBox(HtmlBuilder html)
    {
        return html;
    }

    /**
     * Accesseur de l attribut cell value from rowcount.
     * 
     * @param model
     * @param column
     * @param checkboxCellAttr
     * @return cell value from rowcount
     */
    protected String getCellValueFromRowcount(final TableModel model, final Column column,
        SelectedCheckboxCellAttributes checkboxCellAttr)
    {
        HtmlBuilder html = new HtmlBuilder();

        // Numéro de la ligne, suivi de son séparateur
        final String rowid = this.getRowPosition(model, column);
        final String rowcount = rowid + checkboxCellAttr.getRowcountSep();

        // id html de la case à cocher
        final String idCheckbox = checkboxCellAttr.getPrefixIdCheckbox() + rowcount;

        // id html du champ caché associé
        final String idHiddenField = checkboxCellAttr.getPrefixIdHidden() + rowcount;

        // Lecture de la liste contenant les rowid des éléments
        // sélectionnés
        final Collection rowidSelected =
            this.getCollectionDesElementsSelectionnes(model, checkboxCellAttr.getSelectedRowids());

        // Si l'élément appartient à la liste, la case est cochée
        if (rowidSelected != null && rowidSelected.contains(rowid))
        {
            html.input("hidden").id(idHiddenField).name(checkboxCellAttr.getPrefixNameHidden() + rowcount).value(
                checkboxCellAttr.getPatternSelected()).xclose();

            html.input("checkbox").id(idCheckbox).name(rowcount);
            html.onclick(checkboxCellAttr.getOnClickJavascript() + checkboxCellAttr.getTableId() + "(this)");
            html.checked();
            html = this.ajouterOptionsSurCheckBox(html);
            html.xclose();
        }
        // Sinon la case n'est pas cochée
        else
        {
            html.input("hidden").id(idHiddenField).name(checkboxCellAttr.getPrefixNameHidden() + rowcount).value(
                checkboxCellAttr.getPatternUnselected()).xclose();
            html.input("checkbox").id(idCheckbox).name(rowcount);
            html.onclick(checkboxCellAttr.getOnClickJavascript() + checkboxCellAttr.getTableId() + "(this)");
            html = this.ajouterOptionsSurCheckBox(html);
            html.xclose();
        }
        // retourne l'élément construit
        return html.toString();

    }

    /**
     * Accesseur de l attribut cell value from id.
     * 
     * @param model
     * @param column
     * @param checkboxCellAttr
     * @return cell value from id
     */
    protected String getCellValueFromId(final TableModel model, final Column column,
        SelectedCheckboxCellAttributes checkboxCellAttr)
    {

        HtmlBuilder html = new HtmlBuilder();

        final Object bean = model.getCurrentRowBean();

        // Numéro de la ligne, suivi de son séparateur
        final String rowcount = this.getRowCount(model, column) + checkboxCellAttr.getRowcountSep();
        final String rowid;

        try
        {
            // Lecture de la valeur de la propriété identifiant le
            // bean
            rowid = BeanUtils.getProperty(bean, checkboxCellAttr.getRowId());
        }
        catch (NoSuchMethodException exception)
        {
            throw new IllegalArgumentException("LA PROPRIETE " + checkboxCellAttr.getRowId()
                + "NE DISPOSE PAS DE GETTER DANS LE BEAN " + bean.toString(), exception);
        }
        catch (IllegalAccessException exception)
        {
            throw new IllegalArgumentException("LA PROPRIETE " + checkboxCellAttr.getRowId()
                + "NE DISPOSE PAS DE GETTER DANS LE BEAN " + bean.toString(), exception);
        }
        catch (InvocationTargetException exception)
        {
            throw new IllegalArgumentException("LA PROPRIETE " + checkboxCellAttr.getRowId()
                + "NE DISPOSE PAS DE GETTER DANS LE BEAN " + bean.toString(), exception);
        }

        // id html de la case à cocher
        final String idCheckbox = checkboxCellAttr.getPrefixIdCheckbox() + rowcount + rowid;

        // id html du champ caché associé
        final String idHiddenField = checkboxCellAttr.getPrefixIdHidden() + rowcount + rowid;

        // Lecture de la liste contenant les rowid des éléments
        // sélectionnés
        final Collection rowidSelected =
            this.getCollectionDesElementsSelectionnes(model, checkboxCellAttr.getSelectedRowids());

        // Si l'élément appartient à la liste, la case est cochée
        if (rowidSelected != null && rowidSelected.contains(rowid))
        {
            html.input("hidden").id(idHiddenField).name(checkboxCellAttr.getPrefixNameHidden() + rowcount + rowid)
                .value(checkboxCellAttr.getPatternSelected()).xclose();

            html.input("checkbox").id(idCheckbox).name(rowcount + rowid);
            html.onclick(checkboxCellAttr.getOnClickJavascript() + checkboxCellAttr.getTableId() + "(this)");
            html.checked();
            html = this.ajouterOptionsSurCheckBox(html);
            html.xclose();
        }
        // Sinon la case n'est pas cochée
        else
        {
            html.input("hidden").id(idHiddenField).name(checkboxCellAttr.getPrefixNameHidden() + rowcount + rowid)
                .value(checkboxCellAttr.getPatternUnselected()).xclose();
            html.input("checkbox").id(idCheckbox).name(rowcount + rowid);
            html.onclick(checkboxCellAttr.getOnClickJavascript() + checkboxCellAttr.getTableId() + "(this)");
            html = this.ajouterOptionsSurCheckBox(html);
            html.xclose();
        }

        return html.toString();
    }

    /**
     * Accesseur de l attribut row count.
     * 
     * @param model
     * @param column
     * @return row count
     */
    private String getRowCount(TableModel model, Column column)
    {
        int rowcount = 0;

        rowcount =
            ((model.getLimit().getPage() - 1) * model.getLimit().getCurrentRowsDisplayed())
                + model.getRowHandler().getRow().getRowCount() - 1;

        return String.valueOf(rowcount);
    }

    /**
     * Accesseur de l attribut row position.
     * 
     * @param model
     * @param column
     * @return row position
     */
    private String getRowPosition(TableModel model, Column column)
    {
        int rowcount = 0;
        Limit limit = model.getLimit();
        Object collItem;

        if (limit.getSort().isSorted() || limit.getFilterSet().isFiltered())
        {
            String items = String.valueOf(model.getTableHandler().getTable().getItems());

            try
            {
                collItem =
                    RetrievalUtils.retrieveCollection(model.getContext(), items, model.getTableHandler().getTable()
                        .getScope());
            }
            catch (Exception exception)
            {
                log.warn("ERREUR : ", exception);
                throw new EcTableException("PB LORS DE LA LECTURE DE LA COLLECTION SUPPORT DU TABLEAU", exception);
            }
            if (!(collItem instanceof List))
            {
                throw new IllegalArgumentException(
                    "POUR UTILISER L'OPTION ROWID=\"ROW\", LA COLLECTION SUPPORT DU TABLEAU DOIT ETRE DE TYPE LIST");
            }
            Collection collection = (Collection) collItem;

            for (Iterator iter = collection.iterator(); iter.hasNext();)
            {
                Object element = iter.next();

                if (model.getCurrentRowBean() == element)
                {
                    break;
                }
                rowcount++;
            }
        }
        else
        {
            rowcount =
                ((model.getLimit().getPage() - 1) * model.getLimit().getCurrentRowsDisplayed())
                    + model.getRowHandler().getRow().getRowCount() - 1;
        }

        return String.valueOf(rowcount);
    }

}
