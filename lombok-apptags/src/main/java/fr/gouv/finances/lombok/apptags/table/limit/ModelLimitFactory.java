/*
/*Copyright (c) 2014 DGFiP - Tous droits réservés/* * Copyright 2004 original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package fr.gouv.finances.lombok.apptags.table.limit;

import fr.gouv.finances.lombok.apptags.table.core.TableModel;

/**
 * Une factory pour creer des objets ModelLimit .
 */
public final class ModelLimitFactory extends AbstractLimitFactory
{

    /** model. */
    TableModel model;

    /**
     * Instanciation de model limit factory.
     * 
     * @param model
     */
    public ModelLimitFactory(TableModel model)
    {
        this.model = model;
        this.tableId = model.getTableHandler().getTable().getTableId();
        this.prefixWithTableId = model.getTableHandler().prefixWithTableId();
        this.context = model.getContext();
        this.registry = model.getRegistry();
        this.isExported = getExported();
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.limit.AbstractLimitFactory#showPagination()
     */
    @Override
    protected boolean showPagination()
    {
        return model.getTableHandler().getTable().isShowPagination() || model.getTableHandler().getTable().isShowPaginationBottom();
    }
}
