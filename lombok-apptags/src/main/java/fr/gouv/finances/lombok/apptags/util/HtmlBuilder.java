/*
/*Copyright (c) 2014 DGFiP - Tous droits réservés/* * Copyright 2004 original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package fr.gouv.finances.lombok.apptags.util;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Will build up an Html String piece by piece. Note: unless otherwise noted if the parameter used for any of the
 * methods is null or empty then that html element will not be appended. The advantage of this is you do not have to do
 * any null or empty string checking to use this class.
 */
public class HtmlBuilder
{
    /** Constant : log. */
    private static final Log log = LogFactory.getLog(CheckboxSelectUtil.class);

    /** writer. */
    private Writer writer;

    /**
     * Default constructor using a StringWriter, which is really just a StringBuffer.
     */
    public HtmlBuilder()
    {
        this.writer = new StringWriter();
    }

    /**
     * A builder with the specified Writer.
     * 
     * @param writer The Writer to use.
     */
    public HtmlBuilder(Writer writer)
    {
        this.writer = writer;
    }

    /**
     * Flush the writer.
     */
    public void flushBuilder()
    {
        try
        {
            writer.flush();
        }
        catch (IOException e)
        {
            log.info(e);
        }
    }

    /**
     * Close the writer.
     */
    public void closeBuilder()
    {
        try
        {
            writer.close();
        }
        catch (IOException e)
        {
            log.info(e);
        }
    }

    /**
     * Write out the content to the internal writer.
     * 
     * @param text text
     * @return html builder
     */
    private HtmlBuilder write(String text)
    {
        try
        {
            writer.write(text);
        }
        catch (IOException e)
        {
            log.info(e);
        }
        return this;
    }

    /**
     * The length of the internal Writer.
     * 
     * @return int
     */
    public int length()
    {
        return writer.toString().length();
    }

    /**
     * The Object to append. Will call the Object's toString() method.
     * 
     * @param text text
     * @return html builder
     */
    public HtmlBuilder append(Object text)
    {
        if (text != null)
        {
            write(text.toString());
        }

        return this;
    }

    /**
     * The String to append.
     * 
     * @param text text
     * @return html builder
     */
    public HtmlBuilder append(String text)
    {
        write(text);

        return this;
    }

    /**
     * <p>
     * Append tabs [\t] and newlines [\n].
     * </p>
     * 
     * @param tabs The number of tab spaces \t to put in.
     * @param newlines The number of newlines \n to put in.
     * @return html builder
     */
    public HtmlBuilder format(int tabs, int newlines)
    {
        tabs(tabs);
        newlines(newlines);

        return this;
    }

    /**
     * <p>
     * Append tabs.
     * </p>
     * 
     * @param tabs The number of tab spaces [\t] to put in.
     * @return html builder
     */
    public HtmlBuilder tabs(int tabs)
    {
        for (int i = 0; i < tabs; i++)
        {
            tab();
        }

        return this;
    }

    /**
     * <p>
     * Append newlines [\n].
     * </p>
     * 
     * @param newlines The number of newlines \n to put in.
     * @return html builder
     */
    public HtmlBuilder newlines(int newlines)
    {
        for (int i = 0; i < newlines; i++)
        {
            newline();
        }

        return this;
    }

    /**
     * <p>
     * Append tab [\t].
     * </p>
     * 
     * @return html builder
     */
    public HtmlBuilder tab()
    {
        write("\t");

        return this;
    }

    /**
     * <p>
     * Append newline [\n].
     * </p>
     * 
     * @return html builder
     */
    public HtmlBuilder newline()
    {
        write("\n");

        return this;
    }

    /**
     * <p>
     * Close the element [>]
     * </p>
     * .
     * 
     * @return html builder
     */
    public HtmlBuilder close()
    {
        write(">");

        return this;
    }

    /**
     * <p>
     * Close the element with a slash to comply with xhtml [/>]
     * </p>
     * .
     * 
     * @return html builder
     */
    public HtmlBuilder xclose()
    {
        write("/>");

        return this;
    }
    /**
     * <p>
     * The start of the table element avec role presentation [
     * <table
     * ].
     * </p>
     * <p>
     * Also appends a newline [\n] and the specified number of tabs [\t] before the table.
     * </p>
     * 
     * @param tabs The number of tab spaces [\t] to put in.
     * @return html builder
     */
    public HtmlBuilder tablePres(int tabs)
    {
        newline();
        tabs(tabs);        
        write("<table role=\"presentation\" ");

        return this;
    }   

    /**
     * <p>
     * The start of the table element [
     * <table
     * ].
     * </p>
     * <p>
     * Also appends a newline [\n] and the specified number of tabs [\t] before the table.
     * Ici on parle du cas d'un table sans le role presentation
     * </p>
     * 
     * @param tabs The number of tab spaces [\t] to put in.
     * @return html builder
     */
    public HtmlBuilder table(int tabs)
    {
        newline();
        tabs(tabs);
        write("<table ");

        return this;
    }   
    

    /**
     * <p>
     * The close tag of the table element [</table>].
     * </p>
     * <p>
     * Also appends a newline [\n] and the specified number of tabs [\t] before the table ends.
     * </p>
     * 
     * @param tabs The number of tab spaces [\t] to put in.
     * @return html builder
     */
    public HtmlBuilder tableEnd(int tabs)
    {
        newline();
        tabs(tabs);
        write("</table>");

        return this;
    }

    /**
     * <p>
     * The start of the button element [<button].
     * </p>
     * 
     * @return html builder
     */
    public HtmlBuilder button()
    {
        write("<button");
        return this;
    }

    /**
     * <p>
     * The close tag of the button element [</button>].
     * </p>
     * 
     * @return html builder
     */
    public HtmlBuilder buttonEnd()
    {
        write("</button>");

        return this;
    }

    /**
     * <p>
     * The start of the tr element [
     * <tr
     * ].
     * </p>
     * <p>
     * Also appends a newline [\n] and the specified number of tabs [\t] before the tr.
     * </p>
     * 
     * @param tabs The number of tab spaces [\t] to put in.
     * @return html builder
     */
    public HtmlBuilder tr(int tabs)
    {
        newline();
        tabs(tabs);
        write("<tr");

        return this;
    }

    /**
     * <p>
     * The close tag of the tr element [</tr>].
     * </p>
     * <p>
     * Also appends a newline [\n] and the specified number of tabs [\t] before the tr ends.
     * </p>
     * 
     * @param tabs The number of tab spaces [\t] to put in.
     * @return html builder
     */
    public HtmlBuilder trEnd(int tabs)
    {
        newline();
        tabs(tabs);
        write("</tr>");

        return this;
    }

    /**
     * <p>
     * The start of the th element [
     * <th].
     * </p>
     * <p>
     * Also appends a newline [\n] and the specified number of tabs [\t] before the th.
     * </p>
     * 
     * @param tabs The number of tab spaces [\t] to put in.
     * @return html builder
     */
    public HtmlBuilder th(int tabs)
    {
        newline();
        tabs(tabs);
        write("<th");

        return this;
    }

    /**
     * <p>
     * The close tag of the th element [</th>].
     * </p>
     * 
     * @return html builder
     */
    public HtmlBuilder thEnd()
    {
        write("</th>");

        return this;
    }

    /**
     * <p>
     * The start of the td element [
     * <td].
     * </p>
     * <p>
     * Also appends a newline [\n] and the specified number of tabs [\t] before the td.
     * </p>
     * 
     * @param tabs The number of tab spaces [\t] to put in.
     * @return html builder
     */
    public HtmlBuilder td(int tabs)
    {
        newline();
        tabs(tabs);
        write("<td");

        return this;
    }

    /**
     * <p>
     * The close tag of the td element [</td>].
     * </p>
     * 
     * @return html builder
     */
    public HtmlBuilder tdEnd()
    {
        write("</td>");

        return this;
    }

    /**
     * <p>
     * The start of the input element [<input].
     * </p>
     * 
     * @return html builder
     */
    public HtmlBuilder input()
    {
        write("<input");

        return this;
    }

    /**
     * <p>
     * The type attribute [type=].
     * </p>
     * 
     * @param type type
     * @return html builder
     */
    public HtmlBuilder type(String type)
    {
        if (StringUtils.isNotBlank(type))
        {
            write(" type=\"").write(type).write("\" ");
        }

        return this;
    }

    /**
     * <p>
     * Combines the start of the input element and the type attribute [<input type=].
     * </p>
     * 
     * @param type type
     * @return html builder
     */
    public HtmlBuilder input(String type)
    {
        write("<input type=\"").write(type).write("\" ");

        return this;
    }

    /**
     * <p>
     * The start of the select element [<select].
     * </p>
     * 
     * @return html builder
     */
    public HtmlBuilder select()
    {
        write("<select");

        return this;
    }

    /**
     * <p>
     * The close tag of the select element [</select>].
     * </p>
     * 
     * @return html builder
     */
    public HtmlBuilder selectEnd()
    {
        write("</select>");

        return this;
    }

    /**
     * <p>
     * The start of the option element [<option].
     * </p>
     * 
     * @return html builder
     */
    public HtmlBuilder option()
    {
        write("<option");

        return this;
    }
    

    /**
     * <p>
     * The close tag of the option element [</option>].
     * </p>
     * 
     * @return html builder
     */
    public HtmlBuilder optionEnd()
    {
        write("</option>");

        return this;
    }

    /**
     * <p>
     * The start of the form element [<form].
     * </p>
     * <p>
     * Also appends a newline [\n] before the form.
     * </p>
     * 
     * @return html builder
     */
    public HtmlBuilder form()
    {
        newline();
        write("<form");

        return this;
    }

    /**
     * <p>
     * The close tag of the form element [</form>].
     * </p>
     * <p>
     * Also appends a newline [\n] before the end.
     * </p>
     * 
     * @return html builder
     */
    public HtmlBuilder formEnd()
    {
        newline();
        write("</form>");

        return this;
    }

    /**
     * <p>
     * The name attribute [name=].
     * </p>
     * 
     * @param name name
     * @return html builder
     */
    public HtmlBuilder name(String name)
    {
        if (StringUtils.isNotBlank(name))
        {
            write(" name=\"").write(name).write("\" ");
        }

        return this;
    }

    /**
     * <p>
     * The value attribute [value=].
     * </p>
     * <p>
     * If the value parameter is null or empty then will append a empty value element.
     * </p>
     * 
     * @param value value
     * @return html builder
     */
    public HtmlBuilder value(String value)
    {
        if (StringUtils.isNotBlank(value))
        {
            write(" value=\"").write(value).write("\" ");
        }
        else
        {
            write(" value=\"").write("\" ");
        }

        return this;
    }
    
    /**
     * <p>
     * The aria-label attribute [aria-label=].
     * </p>
     * 
     * @param alabel alabel
     * @return html builder
     */
    public HtmlBuilder ariaLabel(String alabel)
    {
        if (StringUtils.isNotBlank(alabel))
        {
            write(" aria-label=\"").write(alabel).write("\" ");
        }

        return this;
    }
    
    /**
     * <p>
     * The aria-hidden attribute [aria-hidden=].
     * </p>
     * 
     * @param ahidden ahidden
     * @return html builder
     */
    public HtmlBuilder ariaHidden(String ahidden)
    {
        if (StringUtils.isNotBlank(ahidden))
        {
            write(" aria-hidden=\"").write(ahidden).write("\" ");
        }

        return this;
    }
    
    
    /**
     * <p>
     * The tabindex of the element [tabindex=]
     * </p>
     
     * @param order tab order
     * @return html builder
     */
    public HtmlBuilder tabindex(String order)
    {
        write(" tabindex=\"").write(order).write("\" ");

        return this;
    }

    /**
     * <p>
     * The title attribute [title=].
     * </p>
     * 
     * @param title title
     * @return html builder
     */
    public HtmlBuilder title(String title)
    {
        if (StringUtils.isNotBlank(title))
        {
            write(" title=\"").write(title).write("\" ");
        }

        return this;
    }

    /**
     * <p>
     * The action attribute [action=].
     * </p>
     * 
     * @param action action
     * @return html builder
     */
    public HtmlBuilder action(String action)
    {
        write(" action=\"");
        if (StringUtils.isNotBlank(action))
        {
            write(action);
        }
        write("\" ");

        return this;
    }

    /**
     * <p>
     * The method attribute [method=].
     * </p>
     * 
     * @param method method
     * @return html builder
     */
    public HtmlBuilder method(String method)
    {
        if (StringUtils.isNotBlank(method))
        {
            write(" method=\"").write(method).write("\" ");
        }

        return this;
    }

    /**
     * <p>
     * The enctype attribute [enctype=].
     * </p>
     * 
     * @param enctype enctype
     * @return html builder
     */
    public HtmlBuilder enctype(String enctype)
    {
        if (StringUtils.isNotBlank(enctype))
        {
            write(" enctype=\"").write(enctype).write("\" ");
        }

        return this;
    }

    /**
     * <p>
     * The onchange attribute [onchange=].
     * </p>
     * 
     * @param onchange onchange
     * @return html builder
     */
    public HtmlBuilder onchange(String onchange)
    {
        if (StringUtils.isNotBlank(onchange))
        {
            write(" onchange=\"").write(onchange).write("\" ");
        }

        return this;
    }

    /**
     * <p>
     * The onsubmit attribute [onsubmit=].
     * </p>
     * 
     * @param onsubmit onsubmit
     * @return html builder
     */
    public HtmlBuilder onsubmit(String onsubmit)
    {
        if (StringUtils.isNotBlank(onsubmit))
        {
            write(" onsubmit=\"").write(onsubmit).write("\" ");
        }

        return this;
    }

    /**
     * <p>
     * The onclick attribute [onclick=].
     * </p>
     * 
     * @param onclick onclick
     * @return html builder
     */
    public HtmlBuilder onclick(String onclick)
    {
        if (StringUtils.isNotBlank(onclick))
        {
            write(" onclick=\"").write(onclick).write("\" ");
        }

        return this;
    }

    /**
     * <p>
     * The onmouseover attribute [onmouseover=].
     * </p>
     * 
     * @param onmouseover onmouseover
     * @return html builder
     */
    public HtmlBuilder onmouseover(String onmouseover)
    {
        if (StringUtils.isNotBlank(onmouseover))
        {
            write(" onmouseover=\"").write(onmouseover).write("\" ");
        }

        return this;
    }

    /**
     * <p>
     * The onmouseout attribute [onmouseout=].
     * </p>
     * 
     * @param onmouseout onmouseout
     * @return html builder
     */
    public HtmlBuilder onmouseout(String onmouseout)
    {
        if (StringUtils.isNotBlank(onmouseout))
        {
            write(" onmouseout=\"").write(onmouseout).write("\" ");
        }

        return this;
    }

    /**
     * <p>
     * The onkeypress attribute [onkeypress=].
     * </p>
     * 
     * @param onkeypress onkeypress
     * @return html builder
     */
    public HtmlBuilder onkeypress(String onkeypress)
    {
        if (StringUtils.isNotBlank(onkeypress))
        {
            write(" onkeypress=\"").write(onkeypress).write("\" ");
        }

        return this;
    }
    
    
    /**
     * <p>
     * The headers attribute [headers=].
     * </p>
     * 
     * @param idColumn idColumn
     * @return html builder
     */
    public HtmlBuilder headers(String idColumn)
    {
        if (StringUtils.isNotBlank(idColumn))
        {
            write(" headers=\"").write(idColumn).write("\" ");
        }

        return this;
    }

    /**
     * <p>
     * The id attribute [id=].
     * </p>
     * 
     * @param id id
     * @return html builder
     */
    public HtmlBuilder id(String id)
    {
        if (StringUtils.isNotBlank(id))
        {
            write(" id=\"").write(id).write("\" ");
        }

        return this;
    }

    /**
     * <p>
     * The class attribute [class=].
     * </p>
     * 
     * @param styleClass styleClass
     * @return html builder
     */
    public HtmlBuilder styleClass(String styleClass)
    {
        if (StringUtils.isNotBlank(styleClass))
        {
            write(" class=\"").write(styleClass).write("\" ");
        }

        return this;
    }

    /**
     * <p>
     * The dataAttributes attribute [].
     * </p>
     * 
     * @param dataAttributes dataAttributes
     * @return html builder
     */
    public HtmlBuilder dataAttributes(String dataAttributes)
    {
        if (StringUtils.isNotBlank(dataAttributes))
        {
            write(" ").write(dataAttributes).write(" ");
        }

        return this;
    }

    /**
     * <p>
     * The style attribute [style=].
     * </p>
     * 
     * @param style style
     * @return html builder
     */
    public HtmlBuilder style(String style)
    {
        if (StringUtils.isNotBlank(style))
        {
            write(" style=\"").write(style).write("\" ");
        }

        return this;
    }

    /**
     * <p>
     * The width attribute [width=].
     * </p>
     * 
     * @param width width
     * @return html builder
     */
    public HtmlBuilder width(String width)
    {
        if (StringUtils.isNotBlank(width))
        {
            write(" width=\"").write(width).write("\" ");
        }

        return this;
    }

    /**
     * <p>
     * The align attribute [align=].
     * </p>
     * 
     * @param align align
     * @return html builder
     */
    public HtmlBuilder align(String align)
    {
        if (StringUtils.isNotBlank(align))
        {
            write(" align=\"").write(align).write("\" ");
        }

        return this;
    }

    /**
     * <p>
     * The valign attribute [valign=].
     * </p>
     * 
     * @param valign valign
     * @return html builder
     */
    public HtmlBuilder valign(String valign)
    {
        if (StringUtils.isNotBlank(valign))
        {
            write(" valign=\"").write(valign).write("\" ");
        }

        return this;
    }

    /**
     * <p>
     * The border attribute [border=].
     * </p>
     * 
     * @param border border
     * @return html builder
     */
    public HtmlBuilder border(String border)
    {
        if (StringUtils.isNotBlank(border))
        {
            write(" border=\"").write(border).write("\" ");
        }

        return this;
    }

    /**
     * <p>
     * The cellpadding attribute [cellpadding=].
     * </p>
     * 
     * @param cellPadding cellPadding
     * @return html builder
     */
    public HtmlBuilder cellPadding(String cellPadding)
    {
        if (StringUtils.isNotBlank(cellPadding))
        {
            write(" cellpadding=\"").write(cellPadding).write("\" ");
        }

        return this;
    }

    /**
     * <p>
     * The cellspacing attribute [cellspacing=].
     * </p>
     * 
     * @param cellSpacing cellSpacing
     * @return html builder
     */
    public HtmlBuilder cellSpacing(String cellSpacing)
    {
        if (StringUtils.isNotBlank(cellSpacing))
        {
            write(" cellspacing=\"").write(cellSpacing).write("\" ");
        }

        return this;
    }

    /**
     * <p>
     * The colspan attribute [colspan=].
     * </p>
     * 
     * @param colspan colspan
     * @return html builder
     */
    public HtmlBuilder colSpan(String colspan)
    {
        if (StringUtils.isNotBlank(colspan))
        {
            write(" colspan=\"").write(colspan).write("\" ");
        }

        return this;
    }

    /**
     * <p>
     * The rowspan attribute [rowspan=].
     * </p>
     * 
     * @param rowspan rowspan
     * @return html builder
     */
    public HtmlBuilder rowSpan(String rowspan)
    {
        if (StringUtils.isNotBlank(rowspan))
        {
            write(" rowspan=\"").write(rowspan).write("\" ");
        }

        return this;
    }

    /**
     * <p>
     * The size attribute [size=].
     * </p>
     * 
     * @param size size
     * @return html builder
     */
    public HtmlBuilder size(String size)
    {
        if (StringUtils.isNotBlank(size))
        {
            write(" size=\"").write(size).write("\" ");
        }

        return this;
    }
    
    /**
     * <label for>
     * The start of the span element [<label for].
     * </label>
     * 
     * @return html builder
     */
    public HtmlBuilder labelFor(String forIdentifiant)
    {
        write("<label for=");
        quote();
        append(forIdentifiant);
        quote();

        return this;
    }

    /**
     * <label for>
     * The close tag of the span element [</label for>].
     * </label>
     * 
     * @return html builder
     */
    public HtmlBuilder labelForEnd()
    {
        write("</label>");

        return this;
    }

    /**
     * <p>
     * The start of the span element [<span].
     * </p>
     * 
     * @return html builder
     */
    public HtmlBuilder span()
    {
        write("<span");

        return this;
    }

    /**
     * <p>
     * The close tag of the span element [</span>].
     * </p>
     * 
     * @return html builder
     */
    public HtmlBuilder spanEnd()
    {
        write("</span>");

        return this;
    }
    
    /**
     * <p>
     * The start of the legend element [<legend].
     * </p>
     * 
     * @return html builder
     */
    public HtmlBuilder legend()
    {
        write("<legend");

        return this;
    }

    /**
     * <p>
     * The close tag of the legend element [</legend>].
     * </p>
     * 
     * @return html builder
     */
    public HtmlBuilder legendEnd()
    {
        write("</legend>");

        return this;
    }
    
    
    /**
     * <p>
     * The start of the fieldset element [<fieldset].
     * </p>
     * 
     * @return html builder
     */
    public HtmlBuilder fieldset()
    {
        write("<fieldset");

        return this;
    }

    /**
     * <p>
     * The close tag of the fieldset element [</fieldset>].
     * </p>
     * 
     * @return html builder
     */
    public HtmlBuilder fieldsetEnd()
    {
        write("</fieldset>");

        return this;
    }

    /**
     * <p>
     * The start of the div element [<div].
     * </p>
     * 
     * @return html builder
     */
    public HtmlBuilder div()
    {
        write("<div");

        return this;
    }

    /**
     * <p>
     * The close tag of the div element [</div>].
     * </p>
     * 
     * @return html builder
     */
    public HtmlBuilder divEnd()
    {
        write("</div>");

        return this;
    }

    /**
     * <p>
     * A URL parameter name/value [name=value]
     * </p>
     * .
     * 
     * @param name name
     * @param value value
     * @return html builder
     */
    public HtmlBuilder param(String name, String value)
    {
        append(name);
        equals();
        append(value);

        return this;
    }

    /**
     * <p>
     * The start of the a element plus the href attribute [<a href=].
     * </p>
     * 
     * @param href href
     * @return html builder
     */
    public HtmlBuilder a(String href)
    {
        append("<a href=");
        quote();
        append(href);
        quote();

        return this;
    }

    /**
     * <p>
     * The start of the a element plus the href attribute [<a href=].
     * </p>
     * 
     * @return html builder
     */
    public HtmlBuilder a()
    {
        write("<a href=");

        return this;
    }

    /**
     * <p>
     * The close tag of the a element [</div>].
     * </p>
     * 
     * @return html builder
     */
    public HtmlBuilder aEnd()
    {
        write("</a>");

        return this;
    }

    /**
     * <p>
     * The bold element [<b>].
     * </p>
     * 
     * @return html builder
     */
    public HtmlBuilder bold()
    {
        write("<b>");

        return this;
    }

    /**
     * <p>
     * The close tag of the bold element [</b>].
     * </p>
     * 
     * @return html builder
     */
    public HtmlBuilder boldEnd()
    {
        write("</b>");

        return this;
    }

    /**
     * <p>
     * A single quote ["].
     * </p>
     * 
     * @return html builder
     */
    public HtmlBuilder quote()
    {
        write("\"");

        return this;
    }

    /**
     * <p>
     * A single question mark [?].
     * </p>
     * 
     * @return html builder
     */
    public HtmlBuilder question()
    {
        write("?");

        return this;
    }

    /**
     * <p>
     * A single equals [=].
     * </p>
     * 
     * @return html builder
     */
    public HtmlBuilder equals()
    {
        write("=");

        return this;
    }

    /**
     * <p>
     * A single ampersand [&].
     * </p>
     * 
     * @return html builder
     */
    public HtmlBuilder ampersand()
    {
        write("&");

        return this;
    }

    /**
     * <p>
     * The start of the img element [<img].
     * </p>
     * 
     * @return html builder
     */
    public HtmlBuilder img()
    {
        write("<img");

        return this;
    }

    /**
     * <p>
     * The src attribute [src=].
     * </p>
     * 
     * @param src src
     * @return html builder
     */
    public HtmlBuilder src(String src)
    {
        if (StringUtils.isNotBlank(src))
        {
            write(" src=\"").write(src).write("\" ");
        }

        return this;
    }

    /**
     * <p>
     * The alt attribute [alt=].
     * </p>
     * 
     * @param alt alt
     * @return html builder
     */
    public HtmlBuilder alt(String alt)
    {
        if (StringUtils.isNotBlank(alt))
        {
            write(" alt=\"").write(alt).write("\" ");
        }

        return this;
    }

    /**
     * <p>
     * The start of the src element plus the src attribute [<img src= style=border:0/>].
     * </p>
     * 
     * @param src src
     * @return html builder
     */
    public HtmlBuilder img(String src)
    {
        write("<img src=\"").write(src).write("\" style=\"border:0\"/> ");

        return this;
    }

    /**
     * <p>
     * The start of the src element plus the src attribute [<img src="" tooltip="" style="border:0">].
     * </p>
     * 
     * @param img img
     * @param tooltip tooltip
     * @return html builder
     */
    public HtmlBuilder img(String img, String tooltip)
    {
        write("<img src=\"").write(img).write("\" style=\"border:0\"");

        if (tooltip != null)
        {
            write(" title=\"").write(tooltip).write("\">");
        }

        return this;
    }

    /**
     * <p>
     * The start of the textarea element [<textarea].
     * </p>
     * 
     * @return html builder
     */
    public HtmlBuilder textarea()
    {
        write("<textarea");

        return this;
    }

    /**
     * <p>
     * The close tag of the textarea element [</textarea>].
     * </p>
     * 
     * @return html builder
     */
    public HtmlBuilder textareaEnd()
    {
        write("</textarea>");

        return this;
    }

    /**
     * <p>
     * The cols attribute [cols=].
     * </p>
     * 
     * @param cols cols
     * @return html builder
     */
    public HtmlBuilder cols(String cols)
    {
        if (StringUtils.isNotBlank(cols))
        {
            write(" cols=\"").write(cols).write("\" ");
        }

        return this;
    }

    /**
     * <p>
     * The rows attribute [rows=].
     * </p>
     * 
     * @param rows rows
     * @return html builder
     */
    public HtmlBuilder rows(String rows)
    {
        if (StringUtils.isNotBlank(rows))
        {
            write(" rows=\"").write(rows).write("\" ");
        }

        return this;
    }

    /**
     * <p>
     * The checked attribute [checked="checked"].
     * </p>
     * 
     * @return html builder
     */
    public HtmlBuilder checked()
    {
        write(" checked=\"checked\"");

        return this;
    }

    /**
     * <p>
     * The selected attribute [selected="selected"].
     * </p>
     * 
     * @return html builder
     */
    public HtmlBuilder selected()
    {
        write(" selected=\"selected\"");

        return this;
    }

    /**
     * <p>
     * The readonly attribute [readonly="readonly"].
     * </p>
     * 
     * @return html builder
     */
    public HtmlBuilder readonly()
    {
        write(" readonly=\"readonly\"");

        return this;
    }

    /**
     * <p>
     * The non-breaking space [&nbsp;].
     * </p>
     * 
     * @return html builder
     */
    public HtmlBuilder nbsp()
    {
        write("&#160;");

        return this;
    }

    /**
     * <p>
     * The comment [<!-- -->].
     * </p>
     * 
     * @param comment comment
     * @return html builder
     */
    public HtmlBuilder comment(String comment)
    {
        if (StringUtils.isNotBlank(comment))
        {
            write(" <!-- ").write(comment).write(" -->");
        }

        return this;
    }

    /**
     * <p>
     * The ul element [
     * <ul>
     * ].
     * </p>
     * 
     * @return html builder
     */
    public HtmlBuilder ul()
    {
        write("<ul");

        return this;
    }

    /**
     * <p>
     * The close tag of the ul element [</ul>].
     * </p>
     * 
     * @return html builder
     */
    public HtmlBuilder ulEnd()
    {
        write("</ul>");

        return this;
    }

    /**
     * <p>
     * The li element [
     * <li></li>].
     * </p>
     * 
     * @param text text
     * @return html builder
     */
    public HtmlBuilder li(String text)
    {
        if (StringUtils.isNotBlank(text))
        {
            write("<li>").write(text).write("</li>");
        }
        else
        {
            write("<li");
        }

        return this;
    }

    /**
     * <p>
     * The li end element [ </li>].
     * </p>
     * 
     * @return html builder
     */
    public HtmlBuilder liEnd()
    {
        write("</li>");

        return this;
    }

    /**
     * <p>
     * The br element [<br/>
     * ].
     * </p>
     * 
     * @return html builder
     */
    public HtmlBuilder br()
    {
        write("<br/>");

        return this;
    }

    /**
     * <p>
     * The disabled attribute [disabled="disabled"].
     * </p>
     * 
     * @return html builder
     */
    public HtmlBuilder disabled()
    {
        write(" disabled=\"disabled\" ");

        return this;
    }

    /**
     * <p>
     * The nowrap attribute [nowrap="nowrap"].
     * </p>
     * 
     * @return html builder
     */
    public HtmlBuilder nowrap()
    {
        write(" nowrap=\"nowrap\" ");

        return this;
    }

    /**
     * <p>
     * The maxlength attribute [maxlength=].
     * </p>
     * 
     * @param maxlength maxlength
     * @return html builder
     */
    public HtmlBuilder maxlength(String maxlength)
    {
        if (StringUtils.isNotBlank(maxlength))
        {
            write(" maxlength=\"").write(maxlength).write("\" ");
        }

        return this;
    }

    /**
     * <p>
     * The start of the tbody element [<tbody].
     * </p>
     * 
     * @param tabs tabs
     * @return html builder
     */
    public HtmlBuilder tbody(int tabs)
    {
        newline();
        tabs(tabs);
        write("<tbody");

        return this;
    }

    /**
     * <p>
     * The end tag of the tbody element [</tbody>].
     * </p>
     * <p>
     * Also appends a newline [\n] and the specified number of tabs [\t] before the </tbody>.
     * </p>
     * 
     * @param tabs The number of tab spaces [\t] to put in.
     * @return html builder
     */
    public HtmlBuilder tbodyEnd(int tabs)
    {
        newline();
        tabs(tabs);
        write("</tbody><span></span>");

        return this;
    }

    /**
     * <p>
     * The start of the thead element [<thead].
     * </p>
     * 
     * @param tabs tabs
     * @return html builder
     */
    public HtmlBuilder thead(int tabs)
    {
        newline();
        tabs(tabs);
        write("<thead");

        return this;
    }

    /**
     * <p>
     * The end tag of the thead element [</thead>].
     * </p>
     * <p>
     * Also appends a newline [\n] and the specified number of tabs [\t] before the </thead>.
     * </p>
     * 
     * @param tabs The number of tab spaces [\t] to put in.
     * @return html builder
     */
    public HtmlBuilder theadEnd(int tabs)
    {
        newline();
        tabs(tabs);
        write("</thead>");

        return this;
    }

    /**
     * <p>
     * The start of the p element [
     * <p
     * ].
     * </p>
     * 
     * @return html builder
     */
    public HtmlBuilder p()
    {
        write("<p");

        return this;
    }

    /**
     * <p>
     * The close tag of the p element [
     * </p>
     * ].</p>
     * 
     * @return html builder
     */
    public HtmlBuilder pEnd()
    {
        write("</p>");

        return this;
    }

    /**
     * <p>
     * The start of the h1 element [
     * <h1].
     * </p>
     * 
     * @return html builder
     */
    public HtmlBuilder h1()
    {
        write("<h1");

        return this;
    }

    /**
     * <p>
     * The close tag of the h1 element [</h1>].
     * </p>
     * 
     * @return html builder
     */
    public HtmlBuilder h1End()
    {
        write("</h1>");

        return this;
    }

    /**
     * <p>
     * The start of the h2 element [
     * <h2].
     * </p>
     * 
     * @return html builder
     */
    public HtmlBuilder h2()
    {
        write("<h2");

        return this;
    }

    /**
     * <p>
     * The close tag of the h2 element [</h2>].
     * </p>
     * 
     * @return html builder
     */
    public HtmlBuilder h2End()
    {
        write("</h2>");

        return this;
    }

    /**
     * <p>
     * The start of the h3 element [
     * <h3].
     * </p>
     * 
     * @return html builder
     */
    public HtmlBuilder h3()
    {
        write("<h3");

        return this;
    }

    /**
     * <p>
     * The close tag of the h3 element [</h3>].
     * </p>
     * 
     * @return html builder
     */
    public HtmlBuilder h3End()
    {
        write("</h3>");

        return this;
    }

    /**
     * <p>
     * The start of the h4 element [
     * <h4].
     * </p>
     * 
     * @return html builder
     */
    public HtmlBuilder h4()
    {
        write("<h4");

        return this;
    }

    /**
     * <p>
     * The close tag of the h4 element [</h4>].
     * </p>
     * 
     * @return html builder
     */
    public HtmlBuilder h4End()
    {
        write("</h4>");

        return this;
    }

    /**
     * <p>
     * The start of the h5 element [
     * <h5].
     * </p>
     * 
     * @return html builder
     */
    public HtmlBuilder h5()
    {
        write("<h5");

        return this;
    }

    /**
     * <p>
     * The close tag of the h5 element [</h5>].
     * </p>
     * 
     * @return html builder
     */
    public HtmlBuilder h5End()
    {
        write("</h5>");

        return this;
    }
    
    /**
     * <p>
     *  convert String Signs to litteral text for vocal synthetisation.
     * </p>
     * 
     * @return String 
     */
    public String SignToLitteralText(String Signs,boolean isOnlySign)
    {
        String[] partsString;
        if(isOnlySign){
            partsString = Signs.split(" ");
        }else{
            partsString = Signs.split("");
        }

        
        String litteralText = "";

        for (int i = 0; i <= (partsString.length-1); i++)
        {        
            if(isOnlySign && i != 0 && i != (partsString.length-1)){
                litteralText+=",";
            }
            if(partsString[i].equals("<")){
                litteralText+="signe inférieur ";
            }else if (partsString[i].equals("<=")){
                litteralText+="signe inférieur ou égale ";
            }else if(partsString[i].equals(">")){
                litteralText+="signe supérieur ";
            }else if(partsString[i].equals(">=")){
                litteralText+="signe supérieur ou égale ";
            }else if(partsString[i].equals("*")){
                litteralText+="signe étoile ";
            }else if(partsString[i].equals("+")){
                litteralText+="signe plus ";
            }else if(partsString[i].equals("-")){
                litteralText+="signe moins ";
            }else if(partsString[i].equals("=")){
                litteralText+="signe égale ";
            }else if(partsString[i].equals("<>")){
                litteralText+="signe inférieur ou supérieur ";
            }else if(partsString[i].equals("\\")){
                litteralText+="signe barre oblique ";
            }else{
                litteralText+=partsString[i]+" ";
            }
        } 
        return litteralText;
    }
    
    

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see java.lang.Object#toString()
     */
    public String toString()
    {
        return writer.toString();
    }
}