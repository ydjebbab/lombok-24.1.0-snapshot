/*
/*Copyright (c) 2014 DGFiP - Tous droits réservés/* * Copyright 2004 original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package fr.gouv.finances.lombok.apptags.util;

import java.io.File;
import java.util.Locale;
import java.util.Properties;

import org.apache.commons.lang.StringUtils;

/**
 * Class MimeUtils
 */
public class MimeUtils
{

    /** Constant : MIMETYPES_PROPERTIES. */
    public static final String MIMETYPES_PROPERTIES = "mimeTypes.properties";

    /** properties. */
    private static Properties properties;

    /**
     * Return the mime type for a file. The extension is parsed out and the method to get mime type by extension is
     * called. Extension mappings are found in the mimeTypes.properties file in the fr.gouv.finances.lombok.apptags.util
     * package.
     * 
     * @param file
     * @return file mime type
     */
    public static String getFileMimeType(File file)
    {
        if (file == null)
        {
            return null;
        }

        return getFileMimeType(file.getName());
    }

    /**
     * Return the mime type for a file name. The extension is parsed out and the method to get mime type by extension is
     * called. Extension mappings are found in the mimeTypes.properties file in the fr.gouv.finances.lombok.apptags.util
     * package.
     * 
     * @param fileName
     * @return file mime type
     */
    public static String getFileMimeType(String fileName)
    {
        if (StringUtils.isBlank(fileName) || (fileName.indexOf(".") == -1))
        {
            return null;
        }

        fileName = fileName.substring(fileName.lastIndexOf("."));

        return getExtensionMimeType(fileName);
    }

    /**
     * Return the mime type for a given extension. Extensions can include the dot or just be the extension without the
     * dot. Extension mappings are found in the mimeTypes.properties file in the fr.gouv.finances.lombok.apptags.util
     * package.
     * 
     * @param extension
     * @return extension mime type
     */
    public static String getExtensionMimeType(String extension)
    {
        String result = null;

        if (StringUtils.isBlank(extension))
        {
            return result;
        }

        init();

        extension = extension.toLowerCase(Locale.FRANCE);

        if (!extension.startsWith("."))
        {
            extension = "." + extension;
        }

        result = (String) properties.get(extension);

        return result;
    }

    /**
     * methode Inits the :
     */
    private static void init()
    {
        if (properties != null)
        {
            return;
        }

        try
        {
            properties = new Properties();
            properties.load(new MimeUtils().getClass().getResourceAsStream(MIMETYPES_PROPERTIES));
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    /**
     * Just a test harness.
     * 
     * @param args
     */
    public static void main(String[] args)
    {
        System.out.println("MimeUtils.getExtensionMimeType(.gif)=" + MimeUtils.getExtensionMimeType(".gif"));
        System.out.println("MimeUtils.getExtensionMimeType(.pdf)=" + MimeUtils.getExtensionMimeType(".pdf"));
        System.out.println("MimeUtils.getExtensionMimeType(.xls)=" + MimeUtils.getExtensionMimeType(".xls"));
        System.out.println("MimeUtils.getFileMimeType(foo.gif)=" + MimeUtils.getFileMimeType("foo.gif"));
        System.out.println("MimeUtils.getFileMimeType(foo.pdf)=" + MimeUtils.getFileMimeType("foo.pdf"));
        System.out.println("MimeUtils.getFileMimeType(foo.xls)=" + MimeUtils.getFileMimeType("foo.xls"));
        System.out.println("MimeUtils.getFileMimeType(foo.badextension)="
            + MimeUtils.getFileMimeType("foo.badextension"));
    }
}
