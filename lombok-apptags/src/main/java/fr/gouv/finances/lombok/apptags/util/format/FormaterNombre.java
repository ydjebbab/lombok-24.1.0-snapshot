/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.apptags.util.format;

import java.text.DecimalFormat;
import java.text.MessageFormat;
import java.text.NumberFormat;
import java.util.Locale;

import org.apache.commons.lang.math.NumberUtils;

/**
 * Class FormaterNombre
 */
public class FormaterNombre
{

    /** Constant : NOT_NB. */
    private static final String NOT_NB = "Erreur - \"{0}\"  n'est pas un nombre";

    /** Constant : BLANK_HTML. */
    private static final String BLANK_HTML = "&#160;";

    /** Constant : BLANK_XML. */
    private static final String BLANK_XML = "";

    /** Constant : BLANK_CSV. */
    private static final String BLANK_CSV = "";

    /** Constant : BLANK_EXCEL. */
    private static final String BLANK_EXCEL = "";

    /** Constant : BLANK_PDF. */
    private static final String BLANK_PDF = "";

    /** Constant : POURCENT_HTML. */
    private static final String POURCENT_HTML = " %";

    /** Constant : POURCENT_PDF. */
    private static final String POURCENT_PDF = " %";

    /** Constant : POURCENT_CSV. */
    private static final String POURCENT_CSV = " %";

    /** Constant : POURCENT_XML. */
    private static final String POURCENT_XML = " %";

    /** Constant : POURCENT_EXCEL. */
    private static final String POURCENT_EXCEL = " %";

    /** Constant : SEP_MILLIER_HTML. */
    private static final boolean SEP_MILLIER_HTML = true;

    /** Constant : SEP_MILLIER_PDF. */
    private static final boolean SEP_MILLIER_PDF = true;

    /** Constant : SEP_MILLIER_XML. */
    private static final boolean SEP_MILLIER_XML = true;

    /** Constant : SEP_MILLIER_CSV. */
    private static final boolean SEP_MILLIER_CSV = false;

    /** Constant : SEP_MILLIER_EXCEL. */
    private static final boolean SEP_MILLIER_EXCEL = false;

    /** Constant : SYM_MNE_CSV. */
    private static final String SYM_MNE_CSV = "";

    /** Constant : SYM_MNE_EXCEL. */
    private static final String SYM_MNE_EXCEL = "";

    /** Constant : SYM_MNE_HTML. */
    private static final String SYM_MNE_HTML = " €";

    /** Constant : SYM_MNE_XML. */
    private static final String SYM_MNE_XML = " €";

    /** Constant : SYM_MNE_PDF. */
    private static final String SYM_MNE_PDF = " €";

    /** Constant : NOT_NB_HTML. */
    private static final String NOT_NB_HTML = NOT_NB;

    /** Constant : NOT_NB_XML. */
    private static final String NOT_NB_XML = NOT_NB;

    /** Constant : NOT_NB_CSV. */
    private static final String NOT_NB_CSV = NOT_NB;

    /** Constant : NOT_NB_EXCEL. */
    private static final String NOT_NB_EXCEL = NOT_NB;

    /** Constant : NOT_NB_PDF. */
    private static final String NOT_NB_PDF = NOT_NB;

    /** Constant : SANS_SUFFIXE. */
    private static final String SANS_SUFFIXE = "";

    /**
     * Formate un nombre en utilisant N décimales.
     * 
     * @param value value
     * @param blankValue chaîne utilisée pour un nombre null
     * @param notANumberValue chaîne utilisée lorsque l'objet n'est pas de type Number
     * @param suffixe suffixe à utiliser
     * @param separateurMillier indique si le séparateur de millier doit être utilisé
     * @param nombreDecimales indique le nombre de décimales à utilisée
     * @return chaîne de texte représentant le nombre
     */
    private static String nombreNDecimales(final Object value, final String blankValue, final String notANumberValue,
        final String suffixe, final boolean separateurMillier, final int nombreDecimales)
    {
        StringBuilder   valueFormate = new StringBuilder();
        if (value == null)
        {
            valueFormate.append(blankValue);
        }
        else
        {
            if (value instanceof Number || NumberUtils.isNumber(String.valueOf(value)))
            {
                NumberFormat unNumberFormat = getFormatNombreNDecimales(nombreDecimales, separateurMillier);
                DecimalFormat decimalFormat = (DecimalFormat) unNumberFormat;
                valueFormate.append(decimalFormat.format(value));
                valueFormate.append(suffixe);
            }
            else
            {
                Object[] args = {value};
                valueFormate.append(MessageFormat.format(notANumberValue, args));
            }
        }

        return valueFormate.toString();
    }

    /**
     * Formate un nombre avec N décimales pour une vue XML.
     * 
     * @param value value
     * @param nombreDecimales nombreDecimales
     * @return chaîne de texte représentant le nombre
     */
    public static String nombreNDecimalesPourXml(final Object value, final int nombreDecimales)
    {
        return nombreNDecimales(value, BLANK_XML, NOT_NB_XML, SANS_SUFFIXE, SEP_MILLIER_XML, nombreDecimales);
    }

    /**
     * Formate un nombre avec N décimales pour une vue CSV.
     * 
     * @param value value
     * @param nombreDecimales nombreDecimales
     * @return chaîne de texte représentant le nombre
     */
    public static String nombreNDecimalesPourCsv(final Object value, final int nombreDecimales)
    {
        return nombreNDecimales(value, BLANK_CSV, NOT_NB_CSV, SANS_SUFFIXE, SEP_MILLIER_CSV, nombreDecimales);
    }

    /**
     * Formate un nombre avec N décimales pour une vue EXCEL.
     * 
     * @param value value
     * @param nombreDecimales nombreDecimales
     * @return chaîne de texte représentant le nombre
     */
    public static String nombreNDecimalesPourExcel(final Object value, final int nombreDecimales)
    {
        return nombreNDecimales(value, BLANK_EXCEL, NOT_NB_EXCEL, SANS_SUFFIXE, SEP_MILLIER_EXCEL, nombreDecimales);
    }

    /**
     * Formate un nombre avec N décimales pour une vue PDF.
     * 
     * @param value value
     * @param nombreDecimales nombreDecimales
     * @return chaîne de texte représentant le nombre
     */
    public static String nombreNDecimalesPourPdf(final Object value, final int nombreDecimales)
    {
        return nombreNDecimales(value, BLANK_PDF, NOT_NB_PDF, SANS_SUFFIXE, SEP_MILLIER_PDF, nombreDecimales);
    }

    /**
     * Formate un nombre avec N décimales pour une vue HTML.
     * 
     * @param value value
     * @param nombreDecimales nombreDecimales
     * @return chaîne de texte représentant le nombre
     */
    public static String nombreNDecimalesPourHtml(final Object value, final int nombreDecimales)
    {
        return nombreNDecimales(value, BLANK_HTML, NOT_NB_HTML, SANS_SUFFIXE, SEP_MILLIER_HTML, nombreDecimales);
    }

    /**
     * Formate un nombre avec N décimales pour une vue XML, sans séparateur de milliers.
     * 
     * @param value value
     * @param nombreDecimales nombreDecimales
     * @return chaîne de texte représentant le nombre
     */
    public static String nombreNDecimalesSansSepMilliersPourXml(final Object value, final int nombreDecimales)
    {
        return nombreNDecimales(value, BLANK_HTML, NOT_NB_HTML, SANS_SUFFIXE, false, nombreDecimales);
    }

    /**
     * Formate un nombre avec N décimales pour une vue PDF, sans séparateur de milliers.
     * 
     * @param value value
     * @param nombreDecimales nombreDecimales
     * @return chaîne de texte représentant le nombre
     */
    public static String nombreNDecimalesSansSepMilliersPourPdf(final Object value, final int nombreDecimales)
    {
        return nombreNDecimales(value, BLANK_PDF, NOT_NB_PDF, SANS_SUFFIXE, false, nombreDecimales);
    }

    /**
     * Formate un nombre avec N décimales pour une vue EXCEL, sans séparateur de milliers.
     * 
     * @param value value
     * @param nombreDecimales nombreDecimales
     * @return chaîne de texte représentant le nombre
     */
    public static String nombreNDecimalesSansSepMilliersPourExcel(final Object value, final int nombreDecimales)
    {
        return nombreNDecimales(value, BLANK_EXCEL, NOT_NB_EXCEL, SANS_SUFFIXE, false, nombreDecimales);
    }

    /**
     * Formate un nombre avec N décimales pour une vue CSV, sans séparateur de milliers.
     * 
     * @param value value
     * @param nombreDecimales nombreDecimales
     * @return chaîne de texte représentant le nombre
     */
    public static String nombreNDecimalesSansSepMilliersPourCsv(final Object value, final int nombreDecimales)
    {
        return nombreNDecimales(value, BLANK_CSV, NOT_NB_CSV, SANS_SUFFIXE, false, nombreDecimales);
    }

    /**
     * Formate un nombre avec N décimales pour une vue HTML, sans séparateur de milliers.
     * 
     * @param value value
     * @param nombreDecimales nombreDecimales
     * @return chaîne de texte représentant le nombre
     */
    public static String nombreNDecimaleSansSepMilliersPourHtml(final Object value, final int nombreDecimales)
    {
        return nombreNDecimales(value, BLANK_HTML, NOT_NB_HTML, SANS_SUFFIXE, false, nombreDecimales);
    }

    /**
     * Formate un nombre sous forme de pourcentage avec N décimales pour une vue XML.
     * 
     * @param value value
     * @param nombreDecimales nombreDecimales
     * @return chaîne de texte représentant le nombre
     */
    public static String pourcentNDecimalesPourXml(final Object value, final int nombreDecimales)
    {
        return nombreNDecimales(value, BLANK_XML, NOT_NB_XML, POURCENT_XML, SEP_MILLIER_XML, nombreDecimales);
    }

    /**
     * Formate un nombre sous forme de pourcentage avec N décimales pour une vue HTML.
     * 
     * @param value value
     * @param nombreDecimales nombreDecimales
     * @return chaîne de texte représentant le nombre
     */
    public static String pourcentNDecimalesPourHtml(final Object value, final int nombreDecimales)
    {
        return nombreNDecimales(value, BLANK_HTML, NOT_NB_HTML, POURCENT_HTML, SEP_MILLIER_HTML, nombreDecimales);
    }

    /**
     * Formate un nombre sous forme de pourcentage avec N décimales pour une vue PDF.
     * 
     * @param value value
     * @param nombreDecimales nombreDecimales
     * @return chaîne de texte représentant le nombre
     */
    public static String pourcentNDecimalesPourPdf(final Object value, final int nombreDecimales)
    {
        return nombreNDecimales(value, BLANK_PDF, NOT_NB_PDF, POURCENT_PDF, SEP_MILLIER_PDF, nombreDecimales);
    }

    /**
     * Formate un nombre sous forme de pourcentage avec N décimales pour une vue EXCEL.
     * 
     * @param value value
     * @param nombreDecimales
     * @return chaîne de texte représentant le nombre
     */
    public static String pourcentNDecimalesPourExcel(final Object value, final int nombreDecimales)
    {
        return nombreNDecimales(value, BLANK_EXCEL, NOT_NB_EXCEL, POURCENT_EXCEL, SEP_MILLIER_EXCEL, nombreDecimales);
    }

    /**
     * Formate un nombre sous forme de pourcentage avec N décimales pour une vue CSV.
     * 
     * @param value value
     * @param nombreDecimales nombreDecimales
     * @return chaîne de texte représentant le nombre
     */
    public static String pourcentNDecimalesPourCsv(final Object value, final int nombreDecimales)
    {
        return nombreNDecimales(value, BLANK_CSV, NOT_NB_CSV, POURCENT_CSV, SEP_MILLIER_CSV, nombreDecimales);
    }

    /**
     * Formate un nombre sous forme de monnaie avec N décimales pour une vue XML.
     * 
     * @param value value
     * @param nombreDecimales nombreDecimales
     * @return chaîne de texte représentant le nombre
     */
    public static String monnaieNDecimalesPourXml(final Object value, final int nombreDecimales)
    {
        return nombreNDecimales(value, BLANK_XML, NOT_NB_XML, SYM_MNE_XML, SEP_MILLIER_XML, nombreDecimales);
    }

    /**
     * Formate un nombre sous forme de monnaie avec N décimales pour une vue HTML.
     * 
     * @param value value
     * @param nombreDecimales nombreDecimales
     * @return chaîne de texte représentant le nombre
     */
    public static String monnaieNDecimalesPourHtml(final Object value, final int nombreDecimales)
    {
        return nombreNDecimales(value, BLANK_HTML, NOT_NB_HTML, SYM_MNE_HTML, SEP_MILLIER_HTML, nombreDecimales);
    }

    /**
     * Formate un nombre sous forme de monnaie avec N décimales pour une vue PDF.
     * 
     * @param value value
     * @param nombreDecimales nombreDecimales
     * @return chaîne de texte représentant le nombre
     */
    public static String monnaieNDecimalesPourPdf(final Object value, final int nombreDecimales)
    {
        return nombreNDecimales(value, BLANK_PDF, NOT_NB_PDF, SYM_MNE_PDF, SEP_MILLIER_PDF, nombreDecimales);
    }

    /**
     * Formate un nombre sous forme de monnaie avec N décimales pour une vue CSV.
     * 
     * @param value value
     * @param nombreDecimales nombreDecimales
     * @return chaîne de texte représentant le nombre
     */
    public static String monnaieNDecimalesPourCsv(final Object value, final int nombreDecimales)
    {
        return nombreNDecimales(value, BLANK_CSV, NOT_NB_CSV, SYM_MNE_CSV, SEP_MILLIER_CSV, nombreDecimales);
    }

    /**
     * Formate un nombre sous forme de monnaie avec N décimales pour une vue EXCEL.
     * 
     * @param value value
     * @param nombreDecimales nombreDecimales
     * @return chaîne de texte représentant le nombre
     */
    public static String monnaieNDecimalesPourExcel(final Object value, final int nombreDecimales)
    {
        return nombreNDecimales(value, BLANK_EXCEL, NOT_NB_EXCEL, SYM_MNE_EXCEL, SEP_MILLIER_EXCEL, nombreDecimales);
    }

    /**
     * Retourne un format nombre avec deux décimales avec séparateurs de milliers.
     * 
     * @return format nombre deux decimales
     */
    public static NumberFormat getFormatNombreDeuxDecimales()
    {
        return getFormatNombreNDecimales(2, true);
    }

    /**
     * Retourne un format nombre avec dix décimales et séparateur de milliers.
     * 
     * @return format nombre dix decimales
     */
    public static NumberFormat getFormatNombreDixDecimales()
    {
        return getFormatNombreNDecimales(2, true);
    }

    /**
     * Retourne un format nombre avec N décimales.
     * 
     * @param nombreDecimales nombreDecimales
     * @param separateurMillier separateurMillier
     * @return format nombre n decimales
     */
    public static NumberFormat getFormatNombreNDecimales(final int nombreDecimales, final boolean separateurMillier)
    {
        NumberFormat unNumberFormat = NumberFormat.getInstance(Locale.FRANCE);
        unNumberFormat.setGroupingUsed(separateurMillier);
        unNumberFormat.setMaximumFractionDigits(nombreDecimales);
        unNumberFormat.setMinimumFractionDigits(nombreDecimales);
        return unNumberFormat;
    }

    /**
     * Retourne un format nombre avec une décimale avec séparateurs de milliers.
     * 
     * @return format nombre une decimale
     */
    public static NumberFormat getFormatNombreUneDecimale()
    {
        return getFormatNombreNDecimales(1, true);
    }

    /**
     * Retourne un format nombre avec zéro décimale avec séparateurs de milliers.
     * 
     * @return format nombre zero decimale
     */
    public static NumberFormat getFormatNombreZeroDecimale()
    {
        return getFormatNombreNDecimales(0, true);
    }

}
