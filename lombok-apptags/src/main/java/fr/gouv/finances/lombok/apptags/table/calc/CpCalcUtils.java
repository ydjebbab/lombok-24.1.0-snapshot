/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.apptags.table.calc;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import fr.gouv.finances.lombok.apptags.table.bean.Column;
import fr.gouv.finances.lombok.apptags.table.cell.DisplayCell;
import fr.gouv.finances.lombok.apptags.table.cell.NombreCellZeroDecimale;
import fr.gouv.finances.lombok.apptags.table.cell.NumericCell;
import fr.gouv.finances.lombok.apptags.table.cell.PourCentCellDeuxDecimales;
import fr.gouv.finances.lombok.apptags.table.core.PreferencesConstants;
import fr.gouv.finances.lombok.apptags.table.core.TableCache;
import fr.gouv.finances.lombok.apptags.table.core.TableModel;
import fr.gouv.finances.lombok.apptags.util.EcTableException;
import fr.gouv.finances.lombok.apptags.util.ExtremeUtils;

/**
 * Class CpCalcUtils
 */
public final class CpCalcUtils
{

    /** logger. */
    private static Log logger = LogFactory.getLog(CpCalcUtils.class);

    /**
     * Instanciation de cp calc utils.
     */
    private CpCalcUtils()
    {
    }

    /**
     * Retourne le tableau des objets CpCalcResult associé à une colonne.
     * 
     * @param model
     * @param column
     * @return calc results
     */
    public static CpCalcResult[] getCalcResults(TableModel model, Column column)
    {
        List<CpCalcResult> values = new ArrayList<>();

        List<String> calcs = column.getCalc();
        for (int i = 0; i < calcs.size(); i++)
        {
            values.add(getCalcResultsByPosition(model, column, i));
        }

        return values.toArray(new CpCalcResult[values.size()]);
    }

    /**
     * Retourne un object CpCalcResult pour une colonne et une ligne de calcul données.
     * 
     * @param model
     * @param column
     * @param position
     * @return calc results by position
     */
    public static CpCalcResult getCalcResultsByPosition(TableModel model, Column column, int position)
    {
        CpCalcResult result = null;
        String calcClassName = CpCalcUtils.getCalcClassNameByPosition(model, column, position);

        if (!isCalcClassName(calcClassName))
        {
            result = new CpCalcResult(calcClassName, null);
        }
        else
        {
            Calc calc = TableCache.getInstance().getCalc(calcClassName);
            result = new CpCalcResult(calcClassName, calc.getCalcResult(model, column));
        }
        return result;
    }

    /**
     * Retourne la première colonne qui contient un titre de cellule calculée.
     * 
     * @param model
     * @return la première colonne qui contient un titre de cellule calculée
     */
    public static Column getFirstCalTitleColumn(TableModel model)
    {
        return model.getColumnHandler().getFirstCalcTitleColumn();
    }

    /**
     * Retourne le titre de la première cellule calculée pour la ligne de calcul concernée. Si la première colonne
     * calculée ne contient pas de titre, retourne une chaîne vide
     * 
     * @param model
     * @param position
     * @return first calc column title by position
     */
    public static String getFirstCalcColumnTitleByPosition(TableModel model, int position)
    {
        String result = "";
        Column column = model.getColumnHandler().getFirstCalcTitleColumn();
        if (column != null)
        {
            List<String> calcTitle = column.getCalcTitle();
            if (calcTitle != null && ((calcTitle.size()) - 1 >= position))
            {
                result = calcTitle.get(position);
            }
        }
        return result;
    }

    /**
     * Retourne la classe css du titre de la première cellule calculée pour la ligne de calcul concernée. Si la première
     * colonne calculée ne contient pas de titre, retourne une chaîne vide
     * 
     * @param model
     * @return first calc column title class
     */
    public static String getFirstCalcColumnTitleClass(TableModel model)
    {
        return model.getColumnHandler().getFirstCalcColumn().getCalcTitleClass();
    }

    /**
     * Retourne le nom de la classe de calcul à utiliser.
     * 
     * @param model
     * @param column
     * @param position
     * @return calc class name by position
     */
    public static String getCalcClassNameByPosition(TableModel model, Column column, int position)
    {
        String calcName = "";
        List<String> calcs = column.getCalc();
        if (calcs != null && ((calcs.size() - 1) >= position))
        {
            calcName = calcs.get(position);
        }
        String calcClassName = model.getPreferences().getPreference(PreferencesConstants.COLUMN_CALC + calcName);

        if (StringUtils.isBlank(calcClassName))
        {
            calcClassName = calcName;
        }

        return calcClassName;
    }

    /**
     * Teste si une classe donnée par son nom existe.
     * 
     * @param calcClassName
     * @return true, si c'est calc class name
     */
    private static boolean isCalcClassName(String calcClassName)
    {
        boolean result;
        try
        {
            Class.forName(calcClassName);
            result = true;
        }
        catch (ClassNotFoundException exe)
        {
            result = false;
        }
        return result;
    }

    /**
     * Exe.
     * 
     * @param handler
     * @param rows
     * @param property
     */
    public static void eachRowCalcValue(CalcHandler handler, Collection<Object> rows, String property)
    {
        if (rows != null)
        {

            for (Iterator<Object> listIter = rows.iterator(); listIter.hasNext();)
            {
                Object row = listIter.next();
                Object value = null;

                if (ExtremeUtils.isBeanPropertyReadable(row, property))
                {
                    try
                    {
                        value = PropertyUtils.getProperty(row, property);

                        if (value instanceof Number)
                        {
                            handler.processCalcValue((Number) value);
                        }
                        else
                        {
                            handler.processCalcValue(getValue(property, value));
                        }
                    }
                    catch (Exception exception)
                    {
                        StringBuilder   mes = new StringBuilder(55);
                        mes.append("La propriété [ ");
                        mes.append(property);
                        mes.append(" ] ne peut pas être convertie en une valeur numérique. ");

                        logger.error(mes.toString(), exception);
                    }
                }
            }
        }
    }

    /**
     * Accesseur de l attribut value.
     * 
     * @param property
     * @param value
     * @return value
     */
    private static Number getValue(String property, Object value)
    {
        Number result = new BigDecimal("0.00");
        String valueAsString = String.valueOf(value);
        // 08/01/09 : rajout du controle sur != null
        // pour éviter erreur quand value est null
        if (valueAsString.compareTo("null") != 0 && StringUtils.isNotBlank(valueAsString))
        {
            try
            {
                result = new BigDecimal(valueAsString);
            }
            catch (NumberFormatException exception)
            {
                StringBuilder   mes = new StringBuilder();
                mes.append("Erreur lors de la construction d'un nombre pour la propriété [");
                mes.append(property);
                mes.append("] avec la valeur [");
                mes.append(valueAsString);
                mes.append("]");
                logger.error(mes.toString(), exception);
            }
        }

        return result;
    }

    /**
     * Récupère la valeur formatée de la cellule.
     * 
     * @param model
     * @param column
     * @param value
     * @param position
     * @return string
     */
    public static String findValueFormated(TableModel model, Column column, Number value, int position)
    {
        String valueFormated = null;
        String cellType = column.getCell();
        Object objectCell = null;

        // si le type de calcul est de type "decompte", on utilise
        // un formatage sans décimales
        String calcClassName = CpCalcUtils.getCalcClassNameByPosition(model, column, position);

        Calc calc = TableCache.getInstance().getCalc(calcClassName);
        if ((calc instanceof CpDecompteCalc) || (calc instanceof CpDecompteNonNullsCalc))
        {
            objectCell = new NombreCellZeroDecimale();
        }

        else if (calc instanceof CpPourcentageNonNullsCalc)
        {
            objectCell = new PourCentCellDeuxDecimales();
        }
        else
        {

            try
            {
                objectCell = Class.forName(cellType).newInstance();
            }
            catch (ClassNotFoundException exception)
            {
                throw new EcTableException(exception);
            }
            catch (InstantiationException exception)
            {
                throw new EcTableException(exception);
            }
            catch (IllegalAccessException exception)
            {
                throw new EcTableException(exception);
            }
        }

        // Si aucune cellule de type NumberCell n'est définie sur la colonne
        // on utilise par défaut la classe NombreCellZeroDecimale
        if (objectCell instanceof DisplayCell)
        {
            objectCell = new NombreCellZeroDecimale();
        }

        if (objectCell instanceof NumericCell)
        {
            valueFormated = ((NumericCell) objectCell).getCellValueFormated(value);
        }

        return valueFormated;
    }

    /**
     * dans les cas de sommation de type décompte ou decompte non nulls s'appliquant plus au contenu de la colonne qu'à
     * la valeur elle même on modifie la valeur.
     * 
     * @param handler
     * @param rows
     * @param property
     */
    public static void eachRowCalcValueDecompteNonNull(CalcHandler handler, Collection<Object> rows, String property)
    {
        if (rows != null)
        {

            for (Iterator<Object> listIter = rows.iterator(); listIter.hasNext();)
            {
                Object row = listIter.next();
                Object value = null;

                if (ExtremeUtils.isBeanPropertyReadable(row, property))
                {
                    try
                    {
                        value = PropertyUtils.getProperty(row, property);
                        if (value != null)
                        {
                            value = Integer.valueOf(1);
                        }

                        if (value instanceof Number)
                        {
                            handler.processCalcValue((Number) value);
                        }
                        // else
                        // {
                        // handler.processCalcValue(getValue(property, value));
                        // }
                    }
                    catch (Exception exception)
                    {
                        StringBuilder   mes = new StringBuilder(55);
                        mes.append("La propriété [ ");
                        mes.append(property);
                        mes.append(" ] ne peut pas être convertie en une valeur numérique. ");

                        logger.error(mes.toString(), exception);
                    }
                }
            }
        }
    }

    /**
     * methode Each row calc value decompte :
     * 
     * @param handler
     * @param rows
     * @param property
     */
    public static void eachRowCalcValueDecompte(CalcHandler handler, Collection<Object> rows, String property)
    {
        if (rows != null)
        {

            for (Iterator<Object> listIter = rows.iterator(); listIter.hasNext();)
            {
                Object row = listIter.next();
                Object value = null;

                if (ExtremeUtils.isBeanPropertyReadable(row, property))
                {
                    try
                    {
                        value = PropertyUtils.getProperty(row, property);
                        // dans le cas d un decompte ,on compte aussi les cellules nulles
                        // if (value!=null){
                        value = Integer.valueOf(1);
                        // }

                        if (value instanceof Number)
                        {
                            handler.processCalcValue((Number) value);
                        }
                        // else
                        // {
                        // handler.processCalcValue(getValue(property, value));
                        // }
                    }
                    catch (Exception exception)
                    {
                        StringBuilder   mes = new StringBuilder(55);
                        mes.append("La propriété [ ");
                        mes.append(property);
                        mes.append(" ] ne peut pas être convertie en une valeur numérique. ");

                        logger.error(mes.toString(), exception);
                    }
                }
            }
        }
    }

}
