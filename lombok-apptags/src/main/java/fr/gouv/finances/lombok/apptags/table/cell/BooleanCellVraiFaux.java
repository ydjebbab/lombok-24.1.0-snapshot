/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.apptags.table.cell;

import fr.gouv.finances.lombok.apptags.util.format.BooleanFormat.TypeFormat;

/**
 * Class BooleanCellVraiFaux
 */
public class BooleanCellVraiFaux extends AbstractBooleanCell
{

    /**
     * Instanciation de boolean cell vrai faux.
     */
    public BooleanCellVraiFaux()
    {
        super();
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.cell.AbstractBooleanCell#getFormat()
     */
    @Override
    public TypeFormat getFormat()
    {
        return TypeFormat.VRAI_FAUX;
    }
}
