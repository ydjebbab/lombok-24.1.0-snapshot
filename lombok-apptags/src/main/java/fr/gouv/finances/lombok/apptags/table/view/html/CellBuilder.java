/*
 * Copyright (c) 2014 DGFiP - Tous droits réservés
 *//*
/*Copyright (c) 2014 DGFiP - Tous droits réservés/* * Copyright 2004 original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package fr.gouv.finances.lombok.apptags.table.view.html;

import org.apache.commons.lang.StringUtils;

import fr.gouv.finances.lombok.apptags.table.bean.Column;
import fr.gouv.finances.lombok.apptags.util.HtmlBuilder;

/**
 * Class CellBuilder
 * 
 * @deprecated Use the non-static ColumnBuilder class instead.
 */
@Deprecated
public class CellBuilder
{

    /**
     * Instanciation de cell builder.
     */
    private CellBuilder()
    {
    }

    /**
     * methode Td start :
     * 
     * @param html
     * @param column
     * @deprecated Use the non-static ColumnBuilder class instead.
     */
    @Deprecated
    public static void tdStart(HtmlBuilder html, Column column)
    {
        html.td(2);
        styleClass(html, column);
        style(html, column);

        if (!column.isShowResponsive())
        {
            width(html, column);
        }
        html.close();
    }

    /**
     * methode Td end :
     * 
     * @param html
     * @deprecated Use the non-static ColumnBuilder class instead.
     */
    @Deprecated
    public static void tdEnd(HtmlBuilder html)
    {
        html.tdEnd();
    }

    /**
     * methode Style :
     * 
     * @param html
     * @param column
     * @deprecated Use the non-static ColumnBuilder class instead.
     */
    @Deprecated
    public static void style(HtmlBuilder html, Column column)
    {
        String style = column.getStyle();
        html.style(style);
    }

    /**
     * methode Style class :
     * 
     * @param html
     * @param column
     * @deprecated Use the non-static ColumnBuilder class instead.
     */
    @Deprecated
    public static void styleClass(HtmlBuilder html, Column column)
    {
        String styleClass = column.getStyleClass();
        html.styleClass(styleClass);
    }

    /**
     * methode Width :
     * 
     * @param html
     * @param column
     * @deprecated Use the non-static ColumnBuilder class instead.
     */
    @Deprecated
    public static void width(HtmlBuilder html, Column column)
    {
        if (!column.isShowResponsive()) 
        {
            String width = column.getWidth();
            html.width(width);
        }
    }

    /**
     * methode Td body :
     * 
     * @param html
     * @param value
     * @deprecated Use the non-static ColumnBuilder class instead.
     */
    @Deprecated
    public static void tdBody(HtmlBuilder html, String value)
    {
        if (StringUtils.isNotBlank(value))
        {
            html.append(value);
        }
        else
        {
            html.nbsp();
        }
    }
}
