/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.apptags.table.calc;

/**
 * Class CpCalcResult
 */
public final class CpCalcResult
{

    /** name. */
    private final String name;

    /** value. */
    private final Number value;

    /**
     * Instanciation de cp calc result.
     * 
     * @param name
     * @param value
     */
    public CpCalcResult(String name, Number value)
    {
        this.name = name;
        this.value = value;
    }

    /**
     * Accesseur de l attribut name.
     * 
     * @return name
     */
    public String getName()
    {
        return name;
    }

    /**
     * Accesseur de l attribut value.
     * 
     * @return value
     */
    public Number getValue()
    {
        return value;
    }
}
