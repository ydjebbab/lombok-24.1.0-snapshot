/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.apptags.table.view.html;

/**
 * Class CpBuilderConstants
 */
public final class CpBuilderConstants
{

    /** Constant : STATUS_BAR_NB_RESULTATS_TROUVES. */
    public static final String STATUS_BAR_NB_RESULTATS_TROUVES = "statusbar.nbresultatstrouves";

    /** Constant : STATUS_BAR_NB_RESULTATS_TROUVES_DFLT. */
    public static final String STATUS_BAR_NB_RESULTATS_TROUVES_DFLT = "{0} r&eacute;sultat(s) trouv&eacute;(s)";

    /** Constant : STATUS_BAR_RESULTATS_AFFICHES. */
    public static final String STATUS_BAR_RESULTATS_AFFICHES = "statusbar.resultatsaffiches";

    /** Constant : STATUS_BAR_RESULTATS_AFFICHES_DFLT. */
    public static final String STATUS_BAR_RESULTATS_AFFICHES_DFLT = ", affichage de {0} &agrave; {1}";

    /** Constant : STATUS_BAR_IMG_RESULT_INCOMP. */
    public static final String STATUS_BAR_IMG_RESULT_INCOMP = "statusbar.imageresultatincomplet";

    /** Constant : STATUS_BAR_IMG_RESULT_INCOMP_DFLT. */
    public static final String STATUS_BAR_IMG_RESULT_INCOMP_DFLT = "complet";

    /** Constant : STATUS_BAR_MSG_RESULT_INCOMP. */
    public static final String STATUS_BAR_MSG_RESULT_INCOMP = "statusbar.msgresultatincomplet";

    /** Constant : STATUS_BAR_MSG_RESULT_INCOMP_DFLT. */
    public static final String STATUS_BAR_MSG_RESULT_INCOMP_DFLT =
        "Affinez vos crit&egrave;res de recherche pour consulter les donn&eacute;es non affich&eacute;es";

    /**
     * Instanciation de cp builder constants.
     */
    private CpBuilderConstants()
    {
        // Classe à ne pas instancier

    }

}
