/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.apptags.table.cell;

import fr.gouv.finances.lombok.apptags.table.bean.Column;
import fr.gouv.finances.lombok.apptags.table.core.TableModel;

/**
 * Permet l'affichage d'une image dans une cellule du composant ectable.
 */
public class ImageCell extends AbstractCell
{

    /**
     * Constructeur de la classe ImageCell.java
     *
     */
    public ImageCell()
    {
        super();        
    }

    /*
     * (non-Javadoc)
     * @see
     * fr.gouv.finances.lombok.apptags.table.cell.AbstractCell#getExportDisplay(fr.gouv.finances.lombok.apptags.table
     * .core.TableModel, fr.gouv.finances.lombok.apptags.table.bean.Column)
     */
    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.cell.AbstractCell#getExportDisplay(fr.gouv.finances.lombok.apptags.table.core.TableModel,
     *      fr.gouv.finances.lombok.apptags.table.bean.Column)
     */
    @Override
    public String getExportDisplay(TableModel model, Column column)
    {
        return column.getPropertyValueAsString();
    }

    /*
     * (non-Javadoc)
     * @see
     * fr.gouv.finances.lombok.apptags.table.cell.AbstractCell#getCellValue(fr.gouv.finances.lombok.apptags.table.core
     * .TableModel, fr.gouv.finances.lombok.apptags.table.bean.Column)
     */
    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.cell.AbstractCell#getCellValue(fr.gouv.finances.lombok.apptags.table.core.TableModel,
     *      fr.gouv.finances.lombok.apptags.table.bean.Column)
     */
    @Override
    protected String getCellValue(TableModel model, Column column)
    {
        return null;
    }

}
