/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : NavigableNodeTextTag.java
 *
 */
package fr.gouv.finances.lombok.apptags.tree.tag;

import javax.servlet.jsp.JspException;

import fr.gouv.finances.lombok.apptags.base.TagWriter;
import fr.gouv.finances.lombok.apptags.tree.bean.TreeIteratorElement;
import fr.gouv.finances.lombok.apptags.util.Constantes;

/**
 * Class NavigableNodeTextTag --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.2 $ Date: 11 déc. 2009
 */
public class NavigableNodeTextTag extends NodeBaseTag
{

    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** select node transition. */
    private String selectNodeTransition;

    /** un select node transition. */
    private String unSelectNodeTransition;

    /** class node selected. */
    private String classNodeSelected;

    /** class node un selected. */
    private String classNodeUnSelected;

    /** selectionable. */
    private String selectionable;

    /** tag writer. */
    private TagWriter tagWriter;

    /**
     * Instanciation de navigable node text tag.
     */
    public NavigableNodeTextTag()
    {
        super();
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @return int
     * @throws JspException the jsp exception
     * @see javax.servlet.jsp.tagext.TagSupport#doAfterBody()
     */
    @Override
    public int doAfterBody() throws JspException
    {
        int result = EVAL_PAGE;

        // Fermeture du lien de sélection ou de dé-selection
        if (isTRUE(this.selectionable))
        {
            // Fin du span
            tagWriter.endTag();
            // Fin du lien
            tagWriter.endTag();
        }

        // Fermeture de la cellule du tableau contenant le texte du noeud
        tagWriter.endTag();

        return result;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @see org.springframework.web.servlet.tags.RequestContextAwareTag#doFinally()
     */
    @Override
    public void doFinally()
    {
        super.doFinally();
        this.tagWriter = null;
    }

    /**
     * Accesseur de l attribut class node selected.
     * 
     * @return class node selected
     */
    public String getClassNodeSelected()
    {
        return classNodeSelected;
    }

    /**
     * Accesseur de l attribut class node un selected.
     * 
     * @return class node un selected
     */
    public String getClassNodeUnSelected()
    {
        return classNodeUnSelected;
    }

    /**
     * Accesseur de l attribut selectionable.
     * 
     * @return selectionable
     */
    public String getSelectionable()
    {
        return selectionable;
    }

    /**
     * Accesseur de l attribut select node transition.
     * 
     * @return select node transition
     */
    public String getSelectNodeTransition()
    {
        return selectNodeTransition;
    }

    /**
     * Accesseur de l attribut un select node transition.
     * 
     * @return un select node transition
     */
    public String getUnSelectNodeTransition()
    {
        return unSelectNodeTransition;
    }

    /**
     * Modificateur de l attribut class node selected.
     * 
     * @param classNodeSelected le nouveau class node selected
     */
    public void setClassNodeSelected(String classNodeSelected)
    {
        this.classNodeSelected = classNodeSelected;
    }

    /**
     * Modificateur de l attribut class node un selected.
     * 
     * @param classNodeUnSelected le nouveau class node un selected
     */
    public void setClassNodeUnSelected(String classNodeUnSelected)
    {
        this.classNodeUnSelected = classNodeUnSelected;
    }

    /**
     * Modificateur de l attribut selectionable.
     * 
     * @param selectionable le nouveau selectionable
     */
    public void setSelectionable(String selectionable)
    {
        this.selectionable = selectionable;
    }

    /**
     * Modificateur de l attribut select node transition.
     * 
     * @param selectNodeTransition le nouveau select node transition
     */
    public void setSelectNodeTransition(String selectNodeTransition)
    {
        this.selectNodeTransition = selectNodeTransition;
    }

    /**
     * Modificateur de l attribut un select node transition.
     * 
     * @param unSelectNodeTransition le nouveau un select node transition
     */
    public void setUnSelectNodeTransition(String unSelectNodeTransition)
    {
        this.unSelectNodeTransition = unSelectNodeTransition;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param tagWriter
     * @return int
     * @throws JspException the jsp exception
     * @see fr.gouv.finances.lombok.apptags.base.BaseAppTags#writeTagContent(fr.gouv.finances.lombok.apptags.base.TagWriter)
     */
    @Override
    protected int writeTagContent(TagWriter tagWriter) throws JspException
    {
        this.initParentTreeTag();

        this.tagWriter = tagWriter;

        // Initialise les attributs du tag
        this.initAttributes();

        TreeIteratorElement element = this.parentTreeTag.getElement();

        // Ouverture de la cellule du tableau contenant le texte du noeud
        tagWriter.startTag(TD_TAG);
        // tagWriter.writeOptionalAttributeValue("width", "30px");
        tagWriter.writeOptionalAttributeValue(CLASS_ATTRIBUTE, this.parentTreeTag.getCssClass() + "texte");
        tagWriter.forceBlock();

        if (isTRUE(this.selectionable))
        {
            this.writeOpenLinkSelectOrUnselect(element);
        }

        return EVAL_PAGE;
    }

    /**
     * Initialise les paramètres du tag en fonction de la valeur des attributs passés en paramètre au tag et des
     * paramètres par défaut.
     * 
     * @throws JspException the jsp exception
     */
    private void initAttributes() throws JspException
    {
        // Utilisation d'un NodeParametres qui porte les valeurs affectées
        // soit par un paramètre soit par défaut
        this.selectNodeTransition =
            this.initvar(this.selectNodeTransition, Constantes.SELECT_NODE_TRANSITION_KEY,
                Constantes.SELECT_NODE_TRANSITION_DEFAULT);

        this.unSelectNodeTransition =
            this.initvar(this.unSelectNodeTransition, Constantes.UNSELECT_NODE_TRANSITION_KEY,
                Constantes.UNSELECT_NODE_TRANSITION_DEFAULT);

        this.classNodeSelected =
            this.initvar(this.classNodeSelected, Constantes.CLASS_NODE_SELECTED_KEY,
                Constantes.CLASS_NODE_SELECTED_DEFAULT);

        this.classNodeUnSelected =
            this.initvar(this.classNodeUnSelected, Constantes.CLASS_NODE_UNSELECTED_KEY,
                Constantes.CLASS_NODE_UNSELECTED_DEFAULT);

        this.selectionable = this.initvar(this.selectionable, Constantes.SELECTABLE_KEY, Constantes.SELECTABLE_DEFAULT);
    }

    /**
     * Ouvre le lien de sélection et de déselection d'un noeud.
     * 
     * @param element --
     * @throws JspException the jsp exception
     */
    private void writeOpenLinkSelectOrUnselect(TreeIteratorElement element) throws JspException
    {
        tagWriter.startTag(A_TAG);
        tagWriter.writeOptionalAttributeValue(TITLE_ATTRIBUTE, "");
        tagWriter.writeOptionalAttributeValue(CLASS_ATTRIBUTE, this.parentTreeTag.getCssClass());

        if (element.isSelected())
        {
            tagWriter.writeAttribute(HREF_ATTRIBUTE, this.contruithref(this.unSelectNodeTransition, element.getId()));
            tagWriter.startTag(DIV_TAG);
            tagWriter.forceBlock();
        }
        else
        {
            tagWriter.writeAttribute(HREF_ATTRIBUTE, this.contruithref(this.selectNodeTransition, element.getId()));
            tagWriter.startTag(DIV_TAG);
            tagWriter.forceBlock();
        }

    }
}
