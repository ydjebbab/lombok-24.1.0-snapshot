/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.apptags.table.cell;

import org.apache.commons.lang.StringUtils;

import fr.gouv.finances.lombok.apptags.table.bean.Column;
import fr.gouv.finances.lombok.apptags.table.core.TableModel;
import fr.gouv.finances.lombok.apptags.util.HtmlBuilder;

/**
 * Class SelectAllCell
 */
public class SelectAllCell implements Cell
{

    /**
     * Instanciation de select all cell.
     */
    public SelectAllCell()
    {
        super();
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.cell.Cell#getExportDisplay(fr.gouv.finances.lombok.apptags.table.core.TableModel,
     *      fr.gouv.finances.lombok.apptags.table.bean.Column)
     */
    @Override
    public String getExportDisplay(final TableModel model, final Column column)
    {
        return column.getTitle();
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.cell.Cell#getHtmlDisplay(fr.gouv.finances.lombok.apptags.table.core.TableModel,
     *      fr.gouv.finances.lombok.apptags.table.bean.Column)
     */
    @Override
    public String getHtmlDisplay(final TableModel model, final Column column)
    {
        HtmlBuilder html = new HtmlBuilder();

        html.td(2);

        if (StringUtils.isNotEmpty(column.getHeaderClass()))
        {
            html.styleClass(column.getHeaderClass());
        }

        
        if (column.isShowResponsive()) 
        {
            html.dataAttributes(column.getDataAttributes());
        }
        
        
        StringBuilder styleValue = new StringBuilder();

        if (StringUtils.isNotEmpty(column.getWidth()) && (!column.isShowResponsive()))
        {
            styleValue.append("width:");
            styleValue.append(column.getWidth());
            styleValue.append(";");
        }

        if (StringUtils.isNotEmpty(column.getHeaderStyle()))
        {
            styleValue.append(column.getHeaderStyle());
        }

        if (StringUtils.isNotEmpty(styleValue.toString()))
        {
            html.style(styleValue.toString());
        }

        html.close();

        final SelectedCheckboxCellAttributes checkboxCellAttr = SelectedCheckboxCellAttributes.getDefault();

        final String controlName = column.getAlias() + "_selector";

        final String tableId = model.getTableHandler().getTable().getTableId();

        html.input("checkbox");
        html.id(controlName);
        html.name(controlName);
        html.title("tout sélectionner /tout désélectionner ");
        html.onclick(checkboxCellAttr.getOnClickSelectAll() + tableId + "(this)");
        html = ajouterOptionsSurCheckBox(html);
        html.xclose();

        html.tdEnd();

        return html.toString();
    }

    // Classe à surcharger pour ajouter des options html sur les checkbox
    // (disabled par exemple)
    /**
     * methode Ajouter options sur check box :
     * 
     * @param html
     * @return html builder
     */
    protected HtmlBuilder ajouterOptionsSurCheckBox(HtmlBuilder html)
    {
        return html;
    }
}
