/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.apptags.table.view.html.toolbar;

import fr.gouv.finances.lombok.apptags.table.bean.Export;
import fr.gouv.finances.lombok.apptags.table.core.TableModel;
import fr.gouv.finances.lombok.apptags.table.view.html.BuilderUtils;
import fr.gouv.finances.lombok.apptags.table.view.html.CpTableActions;
import fr.gouv.finances.lombok.apptags.table.view.html.TableActions;
import fr.gouv.finances.lombok.apptags.util.HtmlBuilder;

/**
 * Class CpToolbarItemUtils
 */
public final class CpToolbarItemUtils
{

    /**
     * Instanciation de cp toolbar item utils.
     */
    private CpToolbarItemUtils()
    {
        // Classe ne contenant que des méthodes statiques - non
        // instanciable
    }

    /**
     * methode Builds the first page :
     * 
     * @param html
     * @param model
     * @param item
     */
    public static void buildFirstPage(HtmlBuilder html, TableModel model, ToolbarItem item)
    {
        String action = new TableActions(model).getPageAction(1);

        item.setAction(action);

        int page = model.getLimit().getPage();

        if (!BuilderUtils.isFirstPageEnabled(page))
        {
            item.disabled(html);
        }
        else
        {
            item.enabled(html, model);
        }
    }

    /**
     * methode Builds the prev page :
     * 
     * @param html
     * @param model
     * @param item
     */
    public static void buildPrevPage(HtmlBuilder html, TableModel model, ToolbarItem item)
    {
        int page = model.getLimit().getPage();
        String action = new TableActions(model).getPageAction(page - 1);

        item.setAction(action);

        if (!BuilderUtils.isPrevPageEnabled(page))
        {
            item.disabled(html);
        }
        else
        {
            item.enabled(html, model);
        }
    }

    /**
     * methode Builds the next page :
     * 
     * @param html
     * @param model
     * @param item
     */
    public static void buildNextPage(HtmlBuilder html, TableModel model, ToolbarItem item)
    {
        int page = model.getLimit().getPage();
        String action = new TableActions(model).getPageAction(page + 1);

        item.setAction(action);

        int totalPages = BuilderUtils.getTotalPages(model);

        if (!BuilderUtils.isNextPageEnabled(page, totalPages))
        {
            item.disabled(html);
        }
        else
        {
            item.enabled(html, model);
        }
    }

    /**
     * methode Builds the last page :
     * 
     * @param html
     * @param model
     * @param item
     */
    public static void buildLastPage(HtmlBuilder html, TableModel model, ToolbarItem item)
    {
        int totalPages = BuilderUtils.getTotalPages(model);
        String action = new TableActions(model).getPageAction(totalPages);

        item.setAction(action);

        int page = model.getLimit().getPage();

        if (!BuilderUtils.isLastPageEnabled(page, totalPages))
        {
            item.disabled(html);
        }
        else
        {
            item.enabled(html, model);
        }
    }

    /**
     * methode Builds the filter :
     * 
     * @param html
     * @param model
     * @param item
     */
    public static void buildFilter(HtmlBuilder html, TableModel model, ToolbarItem item)
    {
        item.setAction(new TableActions(model).getFilterAction());
        item.enabled(html, model);
    }

    /**
     * methode Builds the clear :
     * 
     * @param html
     * @param model
     * @param item
     */
    public static void buildClear(HtmlBuilder html, TableModel model, ToolbarItem item)
    {
        item.setAction(new TableActions(model).getClearAction());
        item.enabled(html, model);
    }

    /**
     * methode Builds the export :
     * 
     * @param html
     * @param model
     * @param item
     * @param export
     */
    public static void buildExport(HtmlBuilder html, TableModel model, ToolbarItem item, Export export)
    {
        String action;

        if ("jasperhtml".equals(export.getView()))
        // Vue de type export html - ouvrir une nouvelle fenêtre avant
        // d'exécuter la requête
        {
            action = new CpTableActions(model).getExportAction(export.getView(), export.getFileName());
        }
        else
        // Autres vues - la requête est effectuée de la fenêtre
        // courante
        {
            action = new TableActions(model).getExportAction(export.getView(), export.getFileName());
        }
        item.setStyle("btn buildExportCP");
        item.setAction(action);
        item.enabled(html, model);
    }
}
