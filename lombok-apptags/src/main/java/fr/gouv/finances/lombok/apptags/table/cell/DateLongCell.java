/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.apptags.table.cell;

import fr.gouv.finances.lombok.apptags.util.format.FormaterDate;

/**
 * Class DateLongCell
 */
public class DateLongCell extends AbstractDateCell
{

    /** Constant : FORMAT_PATTERN. */
    private static final String FORMAT_PATTERN = FormaterDate.F_DA_LONG;

    /**
     * Instanciation de date long cell.
     */
    public DateLongCell()
    {
        super();
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.cell.AbstractDateCell#getFormatPattern()
     */
    @Override
    public String getFormatPattern()
    {
        return FORMAT_PATTERN;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.cell.PatternCell#getAlignmentCell()
     */
    @Override
    public int getAlignmentCell()
    {
        return PatternCell.HORIZONTAL_ALIGN_CENTER;
    }
}
