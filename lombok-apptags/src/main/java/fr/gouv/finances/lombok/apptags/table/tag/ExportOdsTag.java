/*
/*Copyright (c) 2014 DGFiP - Tous droits réservés/* * Copyright 2004 original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package fr.gouv.finances.lombok.apptags.table.tag;

import org.apache.commons.lang.StringUtils;

import fr.gouv.finances.lombok.apptags.table.bean.Export;
import fr.gouv.finances.lombok.apptags.table.core.TableConstants;
import fr.gouv.finances.lombok.apptags.table.core.TableModel;
import fr.gouv.finances.lombok.apptags.table.view.OdsView;
import fr.gouv.finances.lombok.apptags.table.view.html.BuilderConstants;

/**
 * Class ExportOdsTag
 * 
 * jsp.tag : name="exportXls" display-name="ExportOdsTag" body-content="JSP"
 *          description="Exporte les données au format ods."
 */
public class ExportOdsTag extends ExportTag
{

    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** includeTitle - String, inclusion éventuelle d'un titre. */
    private String includeTitle;

    /**
     * Constructeur de la classe ExportOdsTag.java
     *
     */
    public ExportOdsTag()
    {
        super();
        
    }

    /**
     * Gets the includeTitle - String, inclusion éventuelle d'un titre.
     * 
     * @return the includeTitle - String, inclusion éventuelle d'un titre
     */
    public String getIncludeTitle()
    {
        return includeTitle;
    }

    /**
     * Sets the includeTitle - String, inclusion éventuelle d'un titre.
     * 
     * @param includeTitle the new includeTitle - String, inclusion éventuelle d'un titre
     */
    public void setIncludeTitle(String includeTitle)
    {
        this.includeTitle = includeTitle;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.tag.ExportTag#addExportAttributes(fr.gouv.finances.lombok.apptags.table.core.TableModel,
     *      fr.gouv.finances.lombok.apptags.table.bean.Export)
     */
    @Override
    public void addExportAttributes(TableModel model, Export export)
    {
        if (StringUtils.isBlank(export.getView()))
        {
            export.setView(TableConstants.VIEW_ODS);
        }

        if (StringUtils.isBlank(export.getViewResolver()))
        {
            export.setViewResolver(TableConstants.VIEW_ODS);
        }

        if (StringUtils.isBlank(export.getImageName()))
        {
            export.setImageName(TableConstants.VIEW_ODS);
        }

        if (StringUtils.isBlank(export.getText()))
        {
            export.setText(BuilderConstants.TOOLBAR_ODS_TEXT);
        }

        export.addAttribute(OdsView.INCLUDE_TITLE, TagUtils.evaluateExpressionAsString("includeTitle", includeTitle, this,
            pageContext));
    }
}
