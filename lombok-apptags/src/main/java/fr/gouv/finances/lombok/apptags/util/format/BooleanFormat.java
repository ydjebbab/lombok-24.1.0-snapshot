/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.apptags.util.format;

import java.text.FieldPosition;
import java.text.Format;
import java.text.MessageFormat;
import java.text.ParseException;
import java.text.ParsePosition;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

import org.apache.commons.lang.StringUtils;

/**
 * Class BooleanFormat
 */
public class BooleanFormat extends Format
{

    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /**
     * Enum TypeFormat
     */
    public static enum TypeFormat
    {

        /** oui non. */
        OUI_NON,
        /** o n. */
        O_N,
        /** vrai faux. */
        VRAI_FAUX,
        /** v f. */
        V_F,
        /** un zero. */
        UN_ZERO;

        /**
         * Accesseur de l attribut true string.
         * 
         * @param typeFormat typeFormat
         * @return true string
         */
        public static String getTrueString(final TypeFormat typeFormat)
        {
            String result;
            switch (typeFormat)
            {
                case OUI_NON:
                    result = "Oui";
                    break;
                case O_N:
                    result = "O";
                    break;
                case VRAI_FAUX:
                    result = "Vrai";
                    break;
                case V_F:
                    result = "V";
                    break;
                case UN_ZERO:
                    result = "1";
                    ;
                    break;
                default:
                    result = "Oui";
                    break;
            }
            return result;
        }

        /**
         * Accesseur de l attribut false string.
         * 
         * @param typeFormat typeFormat
         * @return false string
         */
        public static String getFalseString(final TypeFormat typeFormat)
        {
            String result;
            switch (typeFormat)
            {
                case OUI_NON:
                    result = "Non";
                    break;
                case O_N:
                    result = "N";
                    break;
                case VRAI_FAUX:
                    result = "Faux";
                    break;
                case V_F:
                    result = "F";
                    break;
                case UN_ZERO:
                    result = "0";
                    ;
                    break;
                default:
                    result = "Non";
                    break;
            }
            return result;
        }
    }

    // Format prédéfinis
    /** Constant : OUI_NON. */
    public static final int OUI_NON = 1;

    /** Constant : O_N. */
    public static final int O_N = 2;

    /** Constant : VRAI_FAUX. */
    public static final int VRAI_FAUX = 3;

    /** Constant : V_F. */
    public static final int V_F = 4;

    /** Constant : UN_ZERO. */
    public static final int UN_ZERO = 5;

    // Représentation d'une valeur "vrai" selon le format
    /** Constant : OUI_NON_TRUE. */
    private static final String OUI_NON_TRUE = "Oui";

    /** Constant : O_N_TRUE. */
    private static final String O_N_TRUE = "O";

    /** Constant : VRAI_FAUX_TRUE. */
    private static final String VRAI_FAUX_TRUE = "Vrai";

    /** Constant : V_F_TRUE. */
    private static final String V_F_TRUE = "V";

    /** Constant : UN_ZERO_TRUE. */
    private static final String UN_ZERO_TRUE = "1";

    // Représentation d'une valeur "fausse" selon le format
    /** Constant : OUI_NON_FALSE. */
    private static final String OUI_NON_FALSE = "Non";

    /** Constant : O_N_FALSE. */
    private static final String O_N_FALSE = "N";

    /** Constant : VRAI_FAUX_FALSE. */
    private static final String VRAI_FAUX_FALSE = "Faux";

    /** Constant : V_F_FALSE. */
    private static final String V_F_FALSE = "F";

    /** Constant : UN_ZERO_FALSE. */
    private static final String UN_ZERO_FALSE = "0";

    /** Constant : MSGERR. */
    private static final String MSGERR = "La chaîne \"{0}\" ne peut pas être convertie en objet de type Boolean";

    /** true set. */
    final private Set<String> trueSet = new HashSet<>();

    /** false set. */
    final private Set<String> falseSet = new HashSet<>();

    /** true text. */
    private String trueText;

    /** false text. */
    private String falseText;

    /** blank egal false. */
    private boolean blankEgalFalse = false;

    /** Liste des noms pour la valeur TRUE. */
    private static final String[] TRUE_STRING = {"VRAI", "OUI", "O", "V", "YES", "Y", "TRUE", "T", "1"};

    /** Liste des noms pour la valeur FALSE. */
    private static final String[] FALSE_STRING = {"FAUX", "NON", "N", "F", "NO", "FALSE", "F", "0"};

    /**
     * Constructeur.
     */
    public BooleanFormat()
    {
        super();
        initialize();
        this.trueText = TypeFormat.getTrueString(TypeFormat.OUI_NON);
        this.falseText = TypeFormat.getFalseString(TypeFormat.OUI_NON);
    }

    /**
     * Instanciation de boolean format.
     * 
     * @param trueText trueText
     * @param falseText falseText
     */
    public BooleanFormat(String trueText, String falseText)
    {
        super();
        initialize();
        this.falseText = falseText;
        this.trueText = trueText;
    }

    /**
     * Retourne une instance de BooleanFormat.
     * 
     * @param style style
     * @return l unique instance de BooleanFormat
     */
    @Deprecated
    public static BooleanFormat getInstance(final int style)
    {
        BooleanFormat format;

        switch (style)
        {
            case O_N:
                format = new BooleanFormat(O_N_TRUE, O_N_FALSE);
                break;
            case VRAI_FAUX:
                format = new BooleanFormat(VRAI_FAUX_TRUE, VRAI_FAUX_FALSE);
                break;
            case V_F:
                format = new BooleanFormat(V_F_TRUE, V_F_FALSE);
                break;
            case UN_ZERO:
                format = new BooleanFormat(UN_ZERO_TRUE, UN_ZERO_FALSE);
                break;
            case OUI_NON:
            default:
                format = new BooleanFormat(OUI_NON_TRUE, OUI_NON_FALSE);
                break;
        }

        return format;
    }

    /**
     * Retourne une instance de BooleanFormat.
     * 
     * @param typeFormat typeFormat
     * @return l unique instance de BooleanFormat
     */
    public static BooleanFormat getInstance(final TypeFormat typeFormat)
    {
        return new BooleanFormat(TypeFormat.getTrueString(typeFormat), TypeFormat.getFalseString(typeFormat));
    }

    /**
     * methode Initialize :
     */
    private void initialize()
    {
        for (int i = 0; i < TRUE_STRING.length; i++)
        {
            trueSet.add(TRUE_STRING[i]);
        }
        for (int i = 0; i < FALSE_STRING.length; i++)
        {
            falseSet.add(FALSE_STRING[i]);
        }
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see java.text.Format#parseObject(java.lang.String)
     */
    @Override
    public Object parseObject(final String source) throws ParseException
    {
        return parseObject(source, new ParsePosition(0));
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see java.text.Format#parseObject(java.lang.String, java.text.ParsePosition)
     */
    @Override
    public Object parseObject(final String source, final ParsePosition pos)
    {
        Boolean result = null;

        // Si la chaîne source est composée de blancs
        if (StringUtils.isBlank(source) && isBlankEgalFalse())
        {
            pos.setIndex(0);
            result = Boolean.FALSE;
        }
        // Sinon
        else
        {
            String sUpperCase = source.toUpperCase(Locale.FRANCE);
            if (trueSet.contains(sUpperCase))
            {
                pos.setIndex(sUpperCase.length());
                result = Boolean.TRUE;
            }
            else if (falseSet.contains(sUpperCase))
            {
                pos.setIndex(sUpperCase.length());
                result = Boolean.FALSE;
            }
            else
            {
                Object[] args = {source};
                throw new IllegalArgumentException(MessageFormat.format(MSGERR, args));
            }
        }

        return result;
    }

    /**
     * Ajoute un nouveau nom pour la valeur TRUE.
     * 
     * @param yes yes
     */
    public void addTrueString(final String yes)
    {
        this.trueSet.add(yes.toUpperCase(Locale.FRANCE));
    }

    /**
     * Ajoute un nouveau nom pour la valeur FALSE.
     * 
     * @param noText noText
     */
    public void addFalseString(final String noText)
    {
        this.falseSet.add(noText.toUpperCase(Locale.FRANCE));
    }

    /**
     * Formate un objet et ajoute la valeur formatée au StringBuilder en paramètre.
     * 
     * @param obj obj
     * @param toAppendTo toAppendTo
     * @param pos pos
     * @return string buffer
     */
    public StringBuilder format(final Object obj, final StringBuilder toAppendTo, final FieldPosition pos)
    {
        if (obj == null)
        {
            if (this.blankEgalFalse)
            {
                toAppendTo.append(this.falseText);
            }
            else
            {
                toAppendTo.append("");
            }
        }

        else
        {
            if (!(obj instanceof Boolean))
            {
                throw new IllegalArgumentException("L'objet à formater n'est pas de type Boolean");
            }

            if (((Boolean) obj).booleanValue())
            {
                toAppendTo.append(this.trueText);
            }
            else
            {
                toAppendTo.append(this.falseText);
            }
        }
        return toAppendTo;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see java.text.Format#format(java.lang.Object, java.lang.StringBuffer, java.text.FieldPosition)
     */
    @Override
    public StringBuffer format(Object obj, StringBuffer toAppendTo, FieldPosition pos)
    {
        if (obj == null)
        {
            if (this.blankEgalFalse)
            {
                toAppendTo.append(this.falseText);
            }
            else
            {
                toAppendTo.append("");
            }
        }

        else
        {
            if (!(obj instanceof Boolean))
            {
                throw new IllegalArgumentException("L'objet à formater n'est pas de type Boolean");
            }

            if (((Boolean) obj).booleanValue())
            {
                toAppendTo.append(this.trueText);
            }
            else
            {
                toAppendTo.append(this.falseText);
            }
        }
        return toAppendTo;
    }

    /**
     * Verifie si blank egal false.
     * 
     * @return true, si c'est blank egal false
     */
    public boolean isBlankEgalFalse()
    {
        return blankEgalFalse;
    }

    /**
     * Modificateur de l attribut blank egal false.
     * 
     * @param blankEgalFalse le nouveau blank egal false
     */
    public void setBlankEgalFalse(boolean blankEgalFalse)
    {
        this.blankEgalFalse = blankEgalFalse;
    }

    /**
     * Accesseur de l attribut false text.
     * 
     * @return false text
     */
    public String getFalseText()
    {
        return falseText;
    }

    /**
     * Modificateur de l attribut false text.
     * 
     * @param falseText le nouveau false text
     */
    public void setFalseText(String falseText)
    {
        this.falseText = falseText;
    }

    /**
     * Accesseur de l attribut true text.
     * 
     * @return true text
     */
    public String getTrueText()
    {
        return trueText;
    }

    /**
     * Modificateur de l attribut true text.
     * 
     * @param trueText le nouveau true text
     */
    public void setTrueText(String trueText)
    {
        this.trueText = trueText;
    }

}
