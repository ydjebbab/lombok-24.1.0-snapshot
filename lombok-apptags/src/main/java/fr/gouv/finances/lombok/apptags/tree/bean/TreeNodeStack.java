/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : TreeNodeStack.java
 *
 */
package fr.gouv.finances.lombok.apptags.tree.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Class TreeNodeStack.
 * 
 * @param <T> noeud
 * @author wpetit-cp
 * @version $Revision: 1.2 $ Date: 15 déc. 2009
 */
public class TreeNodeStack<T> implements Serializable
{

    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** stack contents. */
    protected List<T> stackContents = new ArrayList<T>();

    /**
     * Constructeur de la classe TreeNodeStack.java
     *
     */
    public TreeNodeStack()
    {
        super();
        
    }

    /**
     * methode Pop : --.
     * 
     * @return t
     */
    public T pop()
    {
        T node = this.stackContents.get(this.stackContents.size() - 1);
        this.stackContents.remove(this.stackContents.size() - 1);
        return node;
    }

    /**
     * methode Push : --.
     * 
     * @param node --
     */
    public void push(T node)
    {
        this.stackContents.add(node);
    }

    /**
     * methode Size : --.
     * 
     * @return int
     */
    public int size()
    {
        return this.stackContents.size();
    }

    /**
     * methode Top : --.
     * 
     * @return t
     */
    public T top()
    {
        return this.stackContents.get(this.stackContents.size() - 1);
    }
}
