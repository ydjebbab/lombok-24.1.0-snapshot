/*
/*Copyright (c) 2014 DGFiP - Tous droits réservés/* * Copyright 2004 original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package fr.gouv.finances.lombok.apptags.table.state;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import fr.gouv.finances.lombok.apptags.table.context.Context;
import fr.gouv.finances.lombok.apptags.table.core.TableConstants;

/**
 * Class AbstractState
 */
abstract class AbstractState implements State
{

    /**
     * Constructeur de la classe AbstractState.java
     *
     */
    public AbstractState()
    {
        super();
        
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.state.State#saveParameters(fr.gouv.finances.lombok.apptags.table.context.Context,
     *      java.lang.String, java.util.Map)
     */
    @Override
    public void saveParameters(Context context, String tableId, Map<String, Object> parameterMap)
    {
        Map<String, Object> savedAttributes = new HashMap<>();

        Set<String> keys = parameterMap.keySet();
        for (Iterator<String> iter = keys.iterator(); iter.hasNext();)
        {
            String key = iter.next();
            Object value = parameterMap.get(key);
            savedAttributes.put(key, value);
        }

        context.setSessionAttribute(TableConstants.STATE + tableId, savedAttributes);
    }
}
