/*
/*Copyright (c) 2014 DGFiP - Tous droits réservés/* * Copyright 2004 original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package fr.gouv.finances.lombok.apptags.table.tag;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

import fr.gouv.finances.lombok.apptags.table.bean.Export;
import fr.gouv.finances.lombok.apptags.table.core.TableModel;
import fr.gouv.finances.lombok.apptags.table.interceptor.ExportInterceptor;

/**
 * Class ExportTag
 * 
 * jsp.tag :  name="export" display-name="ExportTag" body-content="JSP"
 *          description="Exporte les données dans une vue donnée. Par exemple pdf ou xls."
 */
public class ExportTag extends TagSupport implements ExportInterceptor
{

    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** encoding. */
    private String encoding;

    /** file name. */
    private String fileName;

    /** image name. */
    private String imageName;

    /** interceptor. */
    private String interceptor;

    /** text. */
    private String text;

    /** tooltip. */
    private String tooltip;

    /** view. */
    private String view;

    /** view resolver. */
    private String viewResolver;

    /**
     * Constructeur de la classe ExportTag.java
     *
     */
    public ExportTag()
    {
        super();
        
    }

    /**
     * Modificateur de l attribut encoding.
     * 
     * @param encoding le nouveau encoding
     * jsp.attribute :  description="Encodage (UTF-8)." required="false" rtexprvalue="true"
     */
    public void setEncoding(String encoding)
    {
        this.encoding = encoding;
    }

    /**
     * Modificateur de l attribut file name.
     * 
     * @param fileName le nouveau file name
     * jsp.attribute :  description="Nom du fichier d'export." required="true" rtexprvalue="true"
     */
    public void setFileName(String fileName)
    {
        this.fileName = fileName;
    }

    /**
     * Modificateur de l attribut image name.
     * 
     * @param imageName le nouveau image name
     * jsp.attribute :  description="Nom de l'image qui permet d'accéder à l'export." required="false" rtexprvalue="true"
     */
    public void setImageName(String imageName)
    {
        this.imageName = imageName;
    }

    /**
     * Modificateur de l attribut interceptor.
     * 
     * @param interceptor le nouveau interceptor
     * jsp.attribute :  description="Le nom complet d'une classe qui implémente l'interface InterceptExport Peut aussi
     *                contenir un alias définit dans le fichier extremecomponents.properties Utilisé pour modifier ou
     *                ajouter des attributs à une colonne." required="false" rtexprvalue="true"
     */
    public void setInterceptor(String interceptor)
    {
        this.interceptor = interceptor;
    }

    /**
     * Modificateur de l attribut view.
     * 
     * @param view le nouveau view
     * jsp.attribute :  description="Le nom complet d'une classe qui implémente l'interface 'View' Peut aussi contenir un
     *                alias définit dans le fichier extremecomponents.properties Les types utilisables par défaut sont
     *                cppdf, cpxls, pdf, xls, csv." required="false" rtexprvalue="true"
     */
    public void setView(String view)
    {
        this.view = view;
    }

    /**
     * Modificateur de l attribut view resolver.
     * 
     * @param viewResolver le nouveau view resolver
     * jsp.attribute :  description="le nom complet d'une classe qui implémente l'interface 'ViewResolver' Peut aussi
     *                contenir un alias définit dans le fichier extremecomponents.properties Les types utilisables par
     *                défaut sont pdf, xls, csv." required="false" rtexprvalue="true"
     */
    public void setViewResolver(String viewResolver)
    {
        this.viewResolver = viewResolver;
    }

    /**
     * Modificateur de l attribut text.
     * 
     * @param text le nouveau text
     * jsp.attribute :  description=
     *                "Texte utilisé pour accéder à l'export, si l'image servant de point d'accès ne peut pas être affichée."
     *                required="false" rtexprvalue="true"
     */
    public void setText(String text)
    {
        this.text = text;
    }

    /**
     * Modificateur de l attribut tooltip.
     * 
     * @param tooltip le nouveau tooltip
     * jsp.attribute :  
     *                description="Texte affiché lorsque la souris survole l'image qui sert de point d'accès à l'export."
     *                required="false" rtexprvalue="true"
     */
    public void setTooltip(String tooltip)
    {
        this.tooltip = tooltip;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see javax.servlet.jsp.tagext.TagSupport#doEndTag()
     */
    @Override
    public int doEndTag() throws JspException
    {
        if (TagUtils.isIteratingBody(this))
        {
            return EVAL_PAGE;
        }

        try
        {
            TableModel model = TagUtils.getModel(this);

            Export export = new Export(model);
            export.setEncoding(TagUtils.evaluateExpressionAsString("encoding", this.encoding, this, pageContext));
            export.setFileName(TagUtils.evaluateExpressionAsString("fileName", this.fileName, this, pageContext));
            export.setImageName(TagUtils.evaluateExpressionAsString("imageName", this.imageName, this, pageContext));
            export.setInterceptor(TagUtils.evaluateExpressionAsString("interceptor", this.interceptor, this,
                pageContext));
            export.setText(TagUtils.evaluateExpressionAsString("text", this.text, this, pageContext));
            export.setTooltip(TagUtils.evaluateExpressionAsString("tooltip", this.tooltip, this, pageContext));
            export.setView(TagUtils.evaluateExpressionAsString("view", view, this, pageContext));
            export.setViewResolver(TagUtils.evaluateExpressionAsString("viewResolver", this.viewResolver, this,
                pageContext));

            addExportAttributes(model, export);
            model.addExport(export);
        }
        catch (Exception exception)
        {
            StringBuilder   msg = new StringBuilder();
            msg.append("Erreur : ");
            throw new JspException(msg.toString(), exception);
        }

        return EVAL_PAGE;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.interceptor.ExportInterceptor#addExportAttributes(fr.gouv.finances.lombok.apptags.table.core.TableModel,
     *      fr.gouv.finances.lombok.apptags.table.bean.Export)
     */
    @Override
    public void addExportAttributes(TableModel model, Export export)
    {
        // RAS
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see javax.servlet.jsp.tagext.TagSupport#release()
     */
    @Override
    public void release()
    {
        encoding = null;
        fileName = null;
        imageName = null;
        interceptor = null;
        view = null;
        viewResolver = null;
        text = null;
        tooltip = null;
        super.release();
    }
}
