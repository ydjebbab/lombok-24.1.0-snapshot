/*
/*Copyright (c) 2014 DGFiP - Tous droits réservés/* * Copyright 2004 original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package fr.gouv.finances.lombok.apptags.table.tag;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import fr.gouv.finances.lombok.apptags.table.core.TableModel;
import fr.gouv.finances.lombok.apptags.util.ExceptionUtils;

/**
 * Class ColumnsTag
 * 
 * jsp.tag :  name="columns" display-name="ColumnsTag" body-content="JSP" description="Colonnes générées."
 */

public class ColumnsTag extends TagSupport
{

    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** logger. */
    private static Log logger = LogFactory.getLog(ColumnsTag.class);

    /** auto generate columns. */
    private String autoGenerateColumns;

    /**
     * Constructeur de la classe ColumnsTag.java
     *
     */
    public ColumnsTag()
    {
        super();
        
    }

    /**
     * Modificateur de l attribut auto generate columns.
     * 
     * @param autoGenerateColumns le nouveau auto generate columns
     * jsp.attribute : description="Nom complet d'une classe qui implémente l'interface AutoGenerateColumns. Peut aussi
     *                utiliser un alias définit dans le fichier extremecomponents.properties. Utilisé pour générer des
     *                colonnes à la volée. required="true" rtexprvalue="true"
     */
    public void setAutoGenerateColumns(String autoGenerateColumns)
    {
        this.autoGenerateColumns = autoGenerateColumns;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see javax.servlet.jsp.tagext.TagSupport#doEndTag()
     */
    @Override
    public int doEndTag() throws JspException
    {
        try
        {
            TableModel model = TagUtils.getModel(this);

            if (!TagUtils.isIteratingBody(this))
            {
                String autoGenerateColumns =
                    TagUtils.evaluateExpressionAsString("autoGenerateColumns", this.autoGenerateColumns, this,
                        pageContext);
                model.addColumns(autoGenerateColumns);
            }
            else
            {
                model.setColumnValues();
            }

            return EVAL_PAGE;
        }
        catch (Exception e)
        {
            logger.error(ExceptionUtils.formatStackTrace(e));
            throw new JspException("ColumnsTag.doEndTag() Problem: " + ExceptionUtils.formatStackTrace(e));
        }
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see javax.servlet.jsp.tagext.TagSupport#release()
     */
    @Override
    public void release()
    {
        autoGenerateColumns = null;
        super.release();
    }
}
