/*
/*Copyright (c) 2014 DGFiP - Tous droits réservés/* * Copyright 2004 original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package fr.gouv.finances.lombok.apptags.table.limit;

import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * Class Sort
 */
public final class Sort
{

    /** alias. */
    private final String alias;

    /** property. */
    private final String property;

    /** sort order. */
    private final String sortOrder;

    /**
     * Instanciation de sort.
     */
    public Sort()
    {
        this.alias = null;
        this.property = null;
        this.sortOrder = null;
    }

    /**
     * Instanciation de sort.
     * 
     * @param alias
     * @param property
     * @param sortOrder
     */
    public Sort(String alias, String property, String sortOrder)
    {
        this.alias = alias;
        this.property = property;
        this.sortOrder = sortOrder;
    }

    /**
     * Accesseur de l attribut alias.
     * 
     * @return alias
     */
    public String getAlias()
    {
        return alias;
    }

    /**
     * Accesseur de l attribut property.
     * 
     * @return property
     */
    public String getProperty()
    {
        return property;
    }

    /**
     * Accesseur de l attribut sort order.
     * 
     * @return sort order
     */
    public String getSortOrder()
    {
        return sortOrder;
    }

    /**
     * Verifie si sorted.
     * 
     * @return true, si c'est sorted
     */
    public boolean isSorted()
    {
        return sortOrder != null;
    }

    /**
     * Verifie si aliased.
     * 
     * @return true, si c'est aliased
     */
    public boolean isAliased()
    {
        return !alias.equals(property);
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        ToStringBuilder builder = new ToStringBuilder(this);
        builder.append("alias", alias);
        builder.append("property", property);
        builder.append("sortOrder", sortOrder);
        return builder.toString();
    }
}