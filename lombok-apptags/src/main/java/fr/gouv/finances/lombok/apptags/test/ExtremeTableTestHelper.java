/*
/*Copyright (c) 2014 DGFiP - Tous droits réservés/* * Copyright 2004 original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package fr.gouv.finances.lombok.apptags.test;

import fr.gouv.finances.lombok.apptags.table.core.TableConstants;

/**
 * Class ExtremeTableTestHelper
 */
public class ExtremeTableTestHelper
{

    /** parameters. */
    private TableParameters parameters;

    /** prefix with table id. */
    private String prefixWithTableId;

    /**
     * Instanciation de extreme table test helper.
     * 
     * @param parameters
     */
    public ExtremeTableTestHelper(TableParameters parameters)
    {
        this(parameters, TableConstants.EXTREME_COMPONENTS);
    }

    /**
     * Instanciation de extreme table test helper.
     * 
     * @param parameters
     * @param tableId
     */
    public ExtremeTableTestHelper(TableParameters parameters, String tableId)
    {
        this.parameters = parameters;
        this.prefixWithTableId = tableId + "_";
        parameters.addParameter(TableConstants.EXPORT_TABLE_ID, tableId);
    }

    /**
     * methode Adds the filter :
     * 
     * @param property
     * @param value
     */
    public void addFilter(String property, String value)
    {
        parameters.addParameter(getFilter(property), value);
    }

    /**
     * methode Adds the page :
     * 
     * @param page
     */
    public void addPage(int page)
    {
        parameters.addParameter(prefixWithTableId + TableConstants.PAGE, String.valueOf(page));
    }

    /**
     * methode Adds the sort asc :
     * 
     * @param property
     */
    public void addSortAsc(String property)
    {
        parameters.addParameter(prefixWithTableId + TableConstants.SORT + property, TableConstants.SORT_ASC);
    }

    /**
     * methode Adds the sort desc :
     * 
     * @param property
     */
    public void addSortDesc(String property)
    {
        parameters.addParameter(prefixWithTableId + TableConstants.SORT + property, TableConstants.SORT_ASC);
    }

    /**
     * methode Adds the sort default :
     * 
     * @param property
     */
    public void addSortDefault(String property)
    {
        parameters.addParameter(prefixWithTableId + TableConstants.SORT + property, TableConstants.SORT_DEFAULT);
    }

    /**
     * methode Do filter :
     */
    public void doFilter()
    {
        parameters.addParameter(prefixWithTableId + TableConstants.FILTER + TableConstants.ACTION,
            TableConstants.FILTER_ACTION);
    }

    /**
     * methode Do clear :
     */
    public void doClear()
    {
        parameters.addParameter(prefixWithTableId + TableConstants.FILTER + TableConstants.ACTION,
            TableConstants.CLEAR_ACTION);
    }

    /**
     * methode Adds the export view :
     * 
     * @param view
     */
    public void addExportView(String view)
    {
        parameters.addParameter(prefixWithTableId + TableConstants.EXPORT_VIEW, view);
    }

    /**
     * methode Adds the export file name :
     * 
     * @param fileName
     */
    public void addExportFileName(String fileName)
    {
        parameters.addParameter(prefixWithTableId + TableConstants.EXPORT_FILE_NAME, fileName);
    }

    /**
     * Accesseur de l attribut filter.
     * 
     * @param property
     * @return filter
     */
    public String getFilter(String property)
    {
        return prefixWithTableId + TableConstants.FILTER + property;
    }
}
