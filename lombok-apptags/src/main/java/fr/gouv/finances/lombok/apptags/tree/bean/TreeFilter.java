/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : TreeFilter.java
 *
 */
package fr.gouv.finances.lombok.apptags.tree.bean;

import java.io.Serializable;

/**
 * Class TreeFilter --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.2 $ Date: 11 déc. 2009
 */
public class TreeFilter implements ITreeFilter, Serializable
{

    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** chaine recherche. */
    private String chaineRecherche;

    /**
     * Instanciation de tree filter.
     * 
     * @param chaineRecherche --
     */
    public TreeFilter(String chaineRecherche)
    {
        super();
        this.chaineRecherche = chaineRecherche;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param tree
     * @param node
     * @return true, si c'est vrai
     * @see fr.gouv.finances.lombok.apptags.tree.bean.ITreeFilter#accept(fr.gouv.finances.lombok.apptags.tree.bean.Tree,
     *      fr.gouv.finances.lombok.apptags.tree.bean.TreeIteratorElement)
     */
    @Override
    public boolean accept(Tree tree, TreeIteratorElement node)
    {
        boolean result = false;
        CharSequence charSequ = chaineRecherche;

        if (node.getName() != null && node.getName().contains(charSequ))
        {
            result = true;
        }
        return result;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param tree
     * @see fr.gouv.finances.lombok.apptags.tree.bean.ITreeFilter#init(fr.gouv.finances.lombok.apptags.tree.bean.Tree)
     */
    @Override
    public void init(Tree tree)
    {
        // Ne fait rien par défaut
    }

}
