/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.apptags.table.view.jasper;

import java.util.Map;

import org.springframework.web.servlet.view.jasperreports.AbstractJasperReportsSingleFormatView;

import net.sf.jasperreports.engine.JRExporter;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.export.HtmlExporter;

/**
 * Class CpJasperReportsHtmlView
 */
public class CpJasperReportsHtmlView extends AbstractJasperReportsSingleFormatView
{

    /** Constant : JASPER_PRINT_KEY. */
    public static final String JASPER_PRINT_KEY = "jasper_print_key";

    /** Constant : FICHIER_JOINT_KEY. */
    public static final String FICHIER_JOINT_KEY = "fichier_joint_key";

    /**
     * Constructeur.
     */
    public CpJasperReportsHtmlView()
    {
        setContentType("text/html");
    }

    /**
     * Surcharge de la méthode pour la rendre finale, pour que l'appel, au sein du constructeur,
     * ne soit plus une mauvaise pratique puisqu'elle ne peut plus ainsi être surchargée dans les
     * classes dérivées.
     *
     * @see org.springframework.web.servlet.view.AbstractView#setContentType(java.lang.String)
     */
    @Override
    public final void setContentType(String contentType)
    {
        super.setContentType(contentType);
    }
    
    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see org.springframework.web.servlet.view.jasperreports.AbstractJasperReportsView#postProcessReport(net.sf.jasperreports.engine.JasperPrint,
     *      java.util.Map)
     */
    @SuppressWarnings("unchecked")
    @Override
    protected void postProcessReport(JasperPrint populatedReport, Map model) throws Exception
    {
        model.put(JASPER_PRINT_KEY, populatedReport);
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see org.springframework.web.servlet.view.jasperreports.AbstractJasperReportsSingleFormatView#createExporter()
     */
    @Override
    protected JRExporter createExporter()
    {
        return new HtmlExporter();
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see org.springframework.web.servlet.view.jasperreports.AbstractJasperReportsSingleFormatView#useWriter()
     */
    @Override
    protected boolean useWriter()
    {
        return true;
    }

}
