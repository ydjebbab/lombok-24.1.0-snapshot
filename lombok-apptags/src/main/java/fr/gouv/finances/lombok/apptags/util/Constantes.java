/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : Constantes.java
 *
 */
package fr.gouv.finances.lombok.apptags.util;

import java.io.Serializable;

/**
 * Class Constantes --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.3 $ Date: 11 déc. 2009
 */
public class Constantes implements Serializable
{

    /** Constant : SCROLL_JAVASCRIPT_SCROLLBACK. */
    public static final String SCROLL_JAVASCRIPT_SCROLLBACK = "scrollback()";

    /** Constant : SCROLL_JAVASCRIPT_GETCOORD. */
    public static final String SCROLL_JAVASCRIPT_GETCOORD = "scrollGetCoord()";

    /** Constant : SCROLLX_HIDDEN_ID. */
    public static final String SCROLLX_HIDDEN_ID = "treescrollxid";

    /** Constant : SCROLLY_HIDDEN_ID. */
    public static final String SCROLLY_HIDDEN_ID = "treescrollyid";

    /** Constant : SCROLLYY_HIDDEN_ID. */
    public static final String SCROLLYY_HIDDEN_ID = "scroll";

    /** Constant : SCROLLX_PARAMETER. */
    public static final String SCROLLX_PARAMETER = "scrollX";

    /** Constant : SCROLLY_PARAMETER. */
    public static final String SCROLLY_PARAMETER = "scrollY";

    /** Constant : SCROLLYY_PARAMETER. */
    public static final String SCROLLYY_PARAMETER = "scrollYY";

    /** Constant : SCROLLX_CONTEXT_PARAMETER. */
    public static final String SCROLLX_CONTEXT_PARAMETER = "treescrollx";

    /** Constant : SCROLLY_CONTEXT_PARAMETER. */
    public static final String SCROLLY_CONTEXT_PARAMETER = "treescrolly";

    /** Constant : SCROLLYY_CONTEXT_PARAMETER. */
    public static final String SCROLLYY_CONTEXT_PARAMETER = "treescroll";

    /*
     * Paramètres du tag 'tree'
     */
    /** Constant : INCLUDE_NODE_ROOT_KEY. */
    public static final String INCLUDE_NODE_ROOT_KEY = "app.tree.includerootnode";

    /** Constant : INCLUDE_NODE_ROOT_DEFAULT. */
    public static final String INCLUDE_NODE_ROOT_DEFAULT = "true";

    /** Constant : CSS_CLASS_KEY. */
    public static final String CSS_CLASS_KEY = "app.tree.cssclass";

    /** Constant : CSS_CLASS_DEFAULT. */
    public static final String CSS_CLASS_DEFAULT = "tree";

    /** Constant : IMAGES_DIR_KEY. */
    public static final String IMAGES_DIR_KEY = "app.tree.imagesdir";

    /** Constant : IMAGES_DIR_DEFAULT. */
    public static final String IMAGES_DIR_DEFAULT = "/composants/tree/images";

    /** Constant : SCOPE_KEY. */
    public static final String SCOPE_KEY = "app.tree.scope";

    /** Constant : SCOPE_DEFAULT. */
    public static final String SCOPE_DEFAULT = null;

    /** Constant : NODE_ID_KEY. */
    public static final String NODE_ID_KEY = "app.tree.nodeid";

    /** Constant : NODE_ID_DEFAULT. */
    public static final String NODE_ID_DEFAULT = "nodeId";

    /** Constant : FLOW_EXECUTION_KEY_BEAN_NAME_KEY. */
    public static final String FLOW_EXECUTION_KEY_BEAN_NAME_KEY = "app.tree.flowexecutionkeybeanName";

    /** Constant : FLOW_EXECUTION_KEY_BEAN_NAME_DEFAULT. */
    public static final String FLOW_EXECUTION_KEY_BEAN_NAME_DEFAULT = "flowExecutionKey";

    /** Constant : FLOW_EXECUTION_KEY_FIELD_NAME_KEY. */
    public static final String FLOW_EXECUTION_KEY_FIELD_NAME_KEY = "app.tree.flowexecutionfieldbeanName";

    /** Constant : FLOW_EXECUTION_KEY_FIELD_NAME_DEFAULT. */
    public static final String FLOW_EXECUTION_KEY_FIELD_NAME_DEFAULT = "_flowExecutionKey";

    /** Constant : EVENT_ID_FIELD_NAME_KEY. */
    public static final String EVENT_ID_FIELD_NAME_KEY = "app.tree.eventidfieldname";

    /** Constant : EVENT_ID_FIELD_NAME_DEFAULT. */
    public static final String EVENT_ID_FIELD_NAME_DEFAULT = "_eventId";

    /** Paramètres du tag nodenavigation. */
    public static final String COLLAPSE_NODE_TRANSITION_KEY = "app.nodenavigation.collapsenodetransition";

    /** Constant : COLLAPSE_NODE_TRANSITION_DEFAULT. */
    public static final String COLLAPSE_NODE_TRANSITION_DEFAULT = "collapseNode";

    /** Constant : EXPAND_NODE_TRANSITION_KEY. */
    public static final String EXPAND_NODE_TRANSITION_KEY = "app.nodenavigation.expandnodetransition";

    /** Constant : EXPAND_NODE_TRANSITION_DEFAULT. */
    public static final String EXPAND_NODE_TRANSITION_DEFAULT = "expandNode";

    /** Constant : BLANK_IMAGE_KEY. */
    public static final String BLANK_IMAGE_KEY = "app.nodenavigation.blankimage";

    /** Constant : BLANK_IMAGE_DEFAULT. */
    public static final String BLANK_IMAGE_DEFAULT = "blankSpace.gif";

    /** Constant : VERTICAL_LINE_IMAGE_KEY. */
    public static final String VERTICAL_LINE_IMAGE_KEY = "app.nodenavigation.verticallineimage";

    /** Constant : VERTICAL_LINE_IMAGE_DEFAULT. */
    public static final String VERTICAL_LINE_IMAGE_DEFAULT = "verticalLine.gif";

    /** Constant : CLOSED_FOLDER_KEY. */
    public static final String CLOSED_FOLDER_KEY = "app.nodenavigation.closedfolder";

    /** Constant : CLOSED_FOLDER_DEFAULT. */
    public static final String CLOSED_FOLDER_DEFAULT = "closedFolder.gif";

    /** Constant : OPEN_FOLDER_KEY. */
    public static final String OPEN_FOLDER_KEY = "app.nodenavigation.openfolder";

    /** Constant : OPEN_FOLDER_DEFAULT. */
    public static final String OPEN_FOLDER_DEFAULT = "openFolder.gif";

    /** Constant : NOT_FOLDER_KEY. */
    public static final String NOT_FOLDER_KEY = "app.nodenavigation.notfolder";

    /** Constant : NOT_FOLDER_DEFAULT. */
    public static final String NOT_FOLDER_DEFAULT = "notFolder.gif";

    /** Constant : EXPANDED_LAST_NODE_KEY. */
    public static final String EXPANDED_LAST_NODE_KEY = "app.nodenavigation.expandedlastnod";

    /** Constant : EXPANDED_LAST_NODE_DEFAULT. */
    public static final String EXPANDED_LAST_NODE_DEFAULT = "expandedLastNode.gif";

    /** Constant : EXPANDED_MID_NODE_KEY. */
    public static final String EXPANDED_MID_NODE_KEY = "app.nodenavigation.expandedmidnode";

    /** Constant : EXPANDED_MID_NODE_DEFAULT. */
    public static final String EXPANDED_MID_NODE_DEFAULT = "expandedMidNode.gif";

    /** Constant : COLLAPSED_MID_NODE_KEY. */
    public static final String COLLAPSED_MID_NODE_KEY = "app.nodenavigation.collapsedmidnode";

    /** Constant : COLLAPSED_MID_NODE_DEFAULT. */
    public static final String COLLAPSED_MID_NODE_DEFAULT = "collapsedMidNode.gif";

    /** Constant : COLLAPSED_LAST_NODE_KEY. */
    public static final String COLLAPSED_LAST_NODE_KEY = "app.nodenavigation.collapsedlastnode";

    /** Constant : COLLAPSED_LAST_NODE_DEFAULT. */
    public static final String COLLAPSED_LAST_NODE_DEFAULT = "collapsedLastNode.gif";

    /** Constant : NO_CHILDREN_LAST_NODE_KEY. */
    public static final String NO_CHILDREN_LAST_NODE_KEY = "app.nodenavigation.nochildrenlastnode";

    /** Constant : NO_CHILDREN_LAST_NODE_DEFAULT. */
    public static final String NO_CHILDREN_LAST_NODE_DEFAULT = "noChildrenLastNode.gif";

    /** Constant : NO_CHILDREN_MID_NODE_KEY. */
    public static final String NO_CHILDREN_MID_NODE_KEY = "app.nodenavigation.nochildrenmidnode";

    /** Constant : NO_CHILDREN_MID_NODE_DEFAULT. */
    public static final String NO_CHILDREN_MID_NODE_DEFAULT = "noChildrenMidNode.gif";

    /** Constant : ALT_EXPAND_NODE_KEY. */
    public static final String ALT_EXPAND_NODE_KEY = "app.nodenavigation.altexpandnode";

    /** Constant : ALT_EXPAND_NODE_DEFAULT. */
    public static final String ALT_EXPAND_NODE_DEFAULT = "Développer";

    /** Constant : ALT_COLLAPSE_NODE_KEY. */
    public static final String ALT_COLLAPSE_NODE_KEY = "app.nodenavigation.altcollapsenode";

    /** Constant : ALT_COLLAPSE_NODE_DEFAULT. */
    public static final String ALT_COLLAPSE_NODE_DEFAULT = "Réduire";

    /** Paramètres du tag nodetexte. */
    public static final String SELECT_NODE_TRANSITION_KEY = "app.nodetexte.selectnodetransition";

    /** Constant : SELECT_NODE_TRANSITION_DEFAULT. */
    public static final String SELECT_NODE_TRANSITION_DEFAULT = "selectNode";

    /** Constant : UNSELECT_NODE_TRANSITION_KEY. */
    public static final String UNSELECT_NODE_TRANSITION_KEY = "app.nodetexte.unselectnodetransition";

    /** Constant : UNSELECT_NODE_TRANSITION_DEFAULT. */
    public static final String UNSELECT_NODE_TRANSITION_DEFAULT = "unSelectNode";

    /** Constant : CLASS_NODE_SELECTED_KEY. */
    public static final String CLASS_NODE_SELECTED_KEY = "app.nodetexte.classnodeselected";

    /** Constant : CLASS_NODE_SELECTED_DEFAULT. */
    public static final String CLASS_NODE_SELECTED_DEFAULT = "nodeselected";

    /** Constant : CLASS_NODE_UNSELECTED_KEY. */
    public static final String CLASS_NODE_UNSELECTED_KEY = "app.nodetexte.classnodeunselected";

    /** Constant : CLASS_NODE_UNSELECTED_DEFAULT. */
    public static final String CLASS_NODE_UNSELECTED_DEFAULT = "nodeunselected";

    /** Constant : SELECTABLE_KEY. */
    public static final String SELECTABLE_KEY = "app.nodetexte.selectable";

    /** Constant : SELECTABLE_DEFAULT. */
    public static final String SELECTABLE_DEFAULT = "false";

    /** Constant : NAVIGABLE_KEY. */
    public static final String NAVIGABLE_KEY = "app.nodetexte.navigable";

    /** Constant : NAVIGABLE_DEFAULT. */
    public static final String NAVIGABLE_DEFAULT = "true";

    /** Constant : INLINE_KEY. */
    public static final String INLINE_KEY = "app.nodetexte.inline";

    /** Constant : INLINE_DEFAULT. */
    public static final String INLINE_DEFAULT = "false";

    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /**
     * Constructeur de la classe Constantes.java
     */
    public Constantes()
    {
        // RAS
    }

}
