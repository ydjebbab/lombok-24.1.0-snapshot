/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.apptags.table.cell;

import fr.gouv.finances.lombok.apptags.table.bean.Column;
import fr.gouv.finances.lombok.apptags.table.core.TableModel;

/**
 * A FormattedCell is a cell of a specific type that is formatted by eXtremeTable (e.g. Date, Number). FormattedCell,
 * however, allows for the case where the user puts content between the column begin and end elements. Thus, if the
 * column value (between the begin/end elements) contains a String, it is returned. Otherwise, the property value is
 * formatted and returned.
 * 
 * @see fr.gouv.finances.lombok.apptags.table.cell.DateCell
 * @see fr.gouv.finances.lombok.apptags.table.cell.NumberCell
 */
public abstract class FormattedCell extends AbstractCell
{
    
    /**
     * Constructeur de la classe FormattedCell.java
     *
     */
    public FormattedCell()
    {
        super();
        
    }

    /*
     * (non-Javadoc)
     * @see
     * fr.gouv.finances.lombok.apptags.table.cell.AbstractCell#getCellValue(fr.gouv.finances.lombok.apptags.table.core
     * .TableModel, fr.gouv.finances.lombok.apptags.table.bean.Column)
     */
    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.cell.AbstractCell#getCellValue(fr.gouv.finances.lombok.apptags.table.core.TableModel,
     *      fr.gouv.finances.lombok.apptags.table.bean.Column)
     */
    @Override
    protected String getCellValue(TableModel model, Column column)
    {
        Object value = column.getValue();

        if (value == null)
        {
            return "";
        }

        String result = null;

        if (value instanceof String)
        {
            result = value.toString();
        }
        else
        {
            result = formatColumnValue(model, column);
        }

        return result;
    }

    /**
     * methode Format column value :
     * 
     * @param model
     * @param column
     * @return string
     */
    protected abstract String formatColumnValue(TableModel model, Column column);
}
