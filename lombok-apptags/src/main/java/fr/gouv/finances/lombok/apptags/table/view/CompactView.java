/*
 * Copyright (c) 2014 DGFiP - Tous droits réservés
 *//*
/*Copyright (c) 2014 DGFiP - Tous droits réservés/* * Copyright 2004 original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package fr.gouv.finances.lombok.apptags.table.view;

import fr.gouv.finances.lombok.apptags.table.core.TableModel;
import fr.gouv.finances.lombok.apptags.table.view.html.BuilderUtils;
import fr.gouv.finances.lombok.apptags.util.HtmlBuilder;

/**
 * Class CompactView
 */
public class CompactView extends AbstractHtmlView
{

    /**
     * Constructeur de la classe CompactView.java
     */
    public CompactView()
    {
        super();

    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.view.AbstractHtmlView#beforeBodyInternal
     *      (fr.gouv.finances.lombok.apptags.table.core.TableModel)
     */
    @Override
    protected void beforeBodyInternal(TableModel model)
    {
        boolean showPagination = BuilderUtils.showPagination(model);
        boolean showExports = BuilderUtils.showExports(model);
        getTableBuilder().tableStart();

        getTableBuilder().theadStart();

        getTableBuilder().titleRowSpanColumns();

        if (model.getTableHandler().getTable().isShowResponsive())
        {
            getTableBuilder().headerRow();
            if (showPagination || showExports)
            {
                toolbar(getHtmlBuilder(), getTableModel());
            }
            getTableBuilder().filterRow();
        }
        else
        {
            if (showPagination || showExports)
            {
                toolbar(getHtmlBuilder(), getTableModel());
            }

            getTableBuilder().filterRow();

            getTableBuilder().headerRow();
        }

        getTableBuilder().theadEnd();

      //  getTableBuilder().tbodyStart();
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.view.AbstractHtmlView#afterBodyInternal
     *      (fr.gouv.finances.lombok.apptags.table.core.TableModel)
     */
    @Override
    protected void afterBodyInternal(TableModel model)
    {

        boolean showPaginationBottom = BuilderUtils.showPaginationBottom(model);
        boolean showExportsBottom = BuilderUtils.showExportsBottom(model);

        getCpCalcBuilder().defaultCalcLayout();

     //   getTableBuilder().tbodyEnd();

        getTableBuilder().tableEnd();
        if (showPaginationBottom || showExportsBottom)
        {
            BuilderUtils.setInPaginationBottom(model);
            toolbar(getHtmlBuilder(), getTableModel());
        }
    }

    /**
     * methode Toolbar :
     * 
     * @param html
     * @param model
     */
    protected void toolbar(HtmlBuilder html, TableModel model)
    {
        new CompactToolbar(html, model).layout();
    }
}
