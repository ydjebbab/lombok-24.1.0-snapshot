/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : TreeIterator.java
 *
 */
package fr.gouv.finances.lombok.apptags.tree.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Class TreeIterator.
 * 
 * @author wpetit-cp
 * @version $Revision: 1.2 $ Date: 15 déc. 2009
 */
public class TreeIterator implements Iterator<TreeIteratorElement>, Serializable
{

    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** tree. */
    protected Tree tree = null;

    /** stack. */
    protected TreeNodeStack<TreeIteratorElement> stack = null;

    /** level. */
    protected int level = 0;

    /**
     * Instanciation de tree iterator.
     * 
     * @param tree --
     * @param includeRootNode --
     */
    public TreeIterator(Tree tree, boolean includeRootNode)
    {
        this.stack = new TreeNodeStack<>();
        this.tree = tree;
        if (includeRootNode)
        {
            TreeIteratorElement root =
                new TreeIteratorElement(tree.getRoot(), new ArrayList<Boolean>(), tree.isExpanded(tree.getRoot()
                    .getId()), tree.isSelected(tree.getRoot().getId()), true, true);
            this.stack.push(root);
        }
        else
        {
            pushChildren(new TreeIteratorElement(tree.getRoot(), new ArrayList<Boolean>(), tree.isExpanded(tree
                .getRoot().getId()), tree.isSelected(tree.getRoot().getId()), true, true), false);
        }
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @return true, si c'est vrai
     * @see java.util.Iterator#hasNext()
     */
    @Override
    public boolean hasNext()
    {
        return this.stack.size() > 0;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @return tree iterator element
     * @see java.util.Iterator#next()
     */
    public TreeIteratorElement next()
    {
        TreeIteratorElement element = this.stack.pop();
        if (this.tree.isExpanded(element.getNode().getId()))
        {
            pushChildren(element, true);
        }
        return element;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @see java.util.Iterator#remove()
     */
    @Override
    public void remove()
    {
        // not implemented
    }

    /**
     * methode Copy indentation profile : --.
     * 
     * @param treeIteratorElement --
     * @return list< boolean>
     */
    protected List<Boolean> copyIndentationProfile(TreeIteratorElement treeIteratorElement)
    {
        List<Boolean> copy = new ArrayList<>();
        if (treeIteratorElement != null)
        {
            for (Boolean element : treeIteratorElement.getIndendationProfile())
            {
                copy.add(element);
            }
        }

        return copy;
    }

    /**
     * methode Push children : --.
     * 
     * @param element --
     * @param indent --
     */
    protected void pushChildren(TreeIteratorElement element, boolean indent)
    {
        List<Boolean> indentationProfile = copyIndentationProfile(element);

        if (indent)
        {
            indentationProfile.add(new Boolean(element.isLastChild()));
        }

        List<TreeNode> children = element.getNode().getChildren();
        for (int i = 0; i < children.size(); i++)
        {
            TreeNode node = children.get(children.size() - i - 1);
            this.stack.push(new TreeIteratorElement(node, indentationProfile, this.tree.isExpanded(node.getId()),
                this.tree.isSelected(node.getId()), i == children.size() - 1, i == 0));
        }
    }

}
