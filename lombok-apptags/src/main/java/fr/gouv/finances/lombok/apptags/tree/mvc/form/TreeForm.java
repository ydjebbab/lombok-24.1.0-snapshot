/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : TreeForm.java
 *
 */
package fr.gouv.finances.lombok.apptags.tree.mvc.form;

import java.io.Serializable;
import java.util.Set;

import fr.gouv.finances.lombok.apptags.tree.bean.TreeNode;

/**
 * Class TreeForm
 * 
 * @author amleplatinec
 * @version $Revision: 1.2 $ Date: 11 déc. 2009
 */
public class TreeForm implements Serializable
{

    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** Déplacement horizontal de la fenêtre - utilisé pour repositionner l'utilisateur dans l'arbre. */
    private String scrollX;

    /** Déplacement vertical de la fenêtre - utilisé pour repositionner l'utilisateur dans l'arbre. */
    private String ScrollY;

    /** Identifiant du noeud sur lequel l'action est demandée. */
    private String nodeId;

    /** Ensemble des noeuds sélectionnés. */
    private Set<TreeNode> selectedNodes;

    /** Texte dont les mots seront recherchés dans la propriété 'name' des noeuds de l'arbre. */
    private String text;

    /** Ensemble des noeuds trouvés lors de la recherche des mots dans la propriété 'name' des noeuds de l'arbre. */
    private Set<TreeNode> foundNodes;

    public TreeForm()
    {
        super();       
    }

    /**
     * Accesseur de l attribut found nodes.
     * 
     * @return found nodes
     */
    public Set<TreeNode> getFoundNodes()
    {
        return foundNodes;
    }

    /**
     * Accesseur de l attribut node id.
     * 
     * @return node id
     */
    public String getNodeId()
    {
        return nodeId;
    }

    /**
     * Accesseur de l attribut scroll x.
     * 
     * @return scroll x
     */
    public String getScrollX()
    {
        return scrollX;
    }

    /**
     * Accesseur de l attribut scroll y.
     * 
     * @return scroll y
     */
    public String getScrollY()
    {
        return ScrollY;
    }

    /**
     * Accesseur de l attribut selected nodes.
     * 
     * @return selected nodes
     */
    public Set<TreeNode> getSelectedNodes()
    {
        return selectedNodes;
    }

    /**
     * Accesseur de l attribut text.
     * 
     * @return text
     */
    public String getText()
    {
        return text;
    }

    /**
     * Modificateur de l attribut found nodes.
     * 
     * @param foundNodes le nouveau found nodes
     */
    public void setFoundNodes(Set<TreeNode> foundNodes)
    {
        this.foundNodes = foundNodes;
    }

    /**
     * Modificateur de l attribut node id.
     * 
     * @param nodeId le nouveau node id
     */
    public void setNodeId(String nodeId)
    {
        this.nodeId = nodeId;
    }

    /**
     * Modificateur de l attribut scroll x.
     * 
     * @param scrollX le nouveau scroll x
     */
    public void setScrollX(String scrollX)
    {
        this.scrollX = scrollX;
    }

    /**
     * Modificateur de l attribut scroll y.
     * 
     * @param scrollY le nouveau scroll y
     */
    public void setScrollY(String scrollY)
    {
        ScrollY = scrollY;
    }

    /**
     * Modificateur de l attribut selected nodes.
     * 
     * @param selectedNodes le nouveau selected nodes
     */
    public void setSelectedNodes(Set<TreeNode> selectedNodes)
    {
        this.selectedNodes = selectedNodes;
    }

    /**
     * Modificateur de l attribut text.
     * 
     * @param text le nouveau text
     */
    public void setText(String text)
    {
        this.text = text;
    }

}
