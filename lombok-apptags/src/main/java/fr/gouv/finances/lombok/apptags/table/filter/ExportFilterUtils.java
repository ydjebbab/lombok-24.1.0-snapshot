/*
/*Copyright (c) 2014 DGFiP - Tous droits réservés/* * Copyright 2004 original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package fr.gouv.finances.lombok.apptags.table.filter;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import fr.gouv.finances.lombok.apptags.table.context.Context;
import fr.gouv.finances.lombok.apptags.table.core.TableConstants;

/**
 * Class ExportFilterUtils
 */
public final class ExportFilterUtils
{

    /** logger. */
    private static Log logger = LogFactory.getLog(ExportFilterUtils.class);

    /**
     * Instanciation de export filter utils.
     */
    private ExportFilterUtils()
    {
    }

    /**
     * Verifie si exported.
     * 
     * @param context
     * @return true, si c'est exported
     */
    public static boolean isExported(Context context)
    {
        return StringUtils.isNotBlank(getTableId(context));
    }

    /**
     * Accesseur de l attribut export file name.
     * 
     * @param context
     * @return export file name
     */
    public static String getExportFileName(Context context)
    {
        String tableId = getTableId(context);

        if (StringUtils.isNotBlank(tableId))
        {
            String exportFileNameStr = tableId + "_" + TableConstants.EXPORT_FILE_NAME;
            String exportFileName = context.getParameter(exportFileNameStr);

            if (logger.isDebugEnabled())
            {
                logger.debug("eXtremeTable export file name [" + exportFileNameStr + "] is [" + exportFileName + "]");
            }

            return exportFileName;
        }

        return null;
    }

    /**
     * There can only be one table instance (tableId) per form. If the instance variable exists that means there is an
     * export being done.
     * 
     * @param context
     * @return table id
     */
    public static String getTableId(Context context)
    {
        return context.getParameter(TableConstants.EXPORT_TABLE_ID);
    }
}
