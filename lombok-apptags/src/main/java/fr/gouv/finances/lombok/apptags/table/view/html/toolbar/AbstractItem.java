/*
/*Copyright (c) 2014 DGFiP - Tous droits réservés/* * Copyright 2004 original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package fr.gouv.finances.lombok.apptags.table.view.html.toolbar;

import fr.gouv.finances.lombok.apptags.table.core.TableModel;
import fr.gouv.finances.lombok.apptags.util.HtmlBuilder;

/**
 * Class AbstractItem
 */
public abstract class AbstractItem
{

    /** action. */
    private String action;

    /** onmouseover. */
    private String onmouseover;

    /** onmouseout. */
    private String onmouseout;

    /** style class. */
    private String styleClass;

    /** style. */
    private String style;

    /** tooltip. */
    private String tooltip;

    /**
     * Constructeur de la classe AbstractItem.java
     *
     */
    public AbstractItem()
    {
        super();
        
    }

    /**
     * Accesseur de l attribut action.
     * 
     * @return action
     */
    public String getAction()
    {
        return action;
    }

    /**
     * Modificateur de l attribut action.
     * 
     * @param action le nouveau action
     */
    public void setAction(String action)
    {
        this.action = action;
    }

    /**
     * Accesseur de l attribut tooltip.
     * 
     * @return tooltip
     */
    public String getTooltip()
    {
        return tooltip;
    }

    /**
     * Modificateur de l attribut tooltip.
     * 
     * @param tooltip le nouveau tooltip
     */
    public void setTooltip(String tooltip)
    {
        this.tooltip = tooltip;
    }

    /**
     * Accesseur de l attribut onmouseout.
     * 
     * @return onmouseout
     */
    public String getOnmouseout()
    {
        return onmouseout;
    }

    /**
     * Modificateur de l attribut onmouseout.
     * 
     * @param onmouseout le nouveau onmouseout
     */
    public void setOnmouseout(String onmouseout)
    {
        this.onmouseout = onmouseout;
    }

    /**
     * Accesseur de l attribut onmouseover.
     * 
     * @return onmouseover
     */
    public String getOnmouseover()
    {
        return onmouseover;
    }

    /**
     * Modificateur de l attribut onmouseover.
     * 
     * @param onmouseover le nouveau onmouseover
     */
    public void setOnmouseover(String onmouseover)
    {
        this.onmouseover = onmouseover;
    }

    /**
     * Accesseur de l attribut style.
     * 
     * @return style
     */
    public String getStyle()
    {
        return style;
    }

    /**
     * Modificateur de l attribut style.
     * 
     * @param style le nouveau style
     */
    public void setStyle(String style)
    {
        this.style = style;
    }

    /**
     * Accesseur de l attribut style class.
     * 
     * @return style class
     */
    public String getStyleClass()
    {
        return styleClass;
    }

    /**
     * Modificateur de l attribut style class.
     * 
     * @param styleClass le nouveau style class
     */
    public void setStyleClass(String styleClass)
    {
        this.styleClass = styleClass;
    }

    /**
     * methode Disabled :
     * 
     * @param html
     */
    protected abstract void disabled(HtmlBuilder html);

    /**
     * methode Enabled :
     * 
     * @param html
     * @param model
     */
    protected abstract void enabled(HtmlBuilder html, TableModel model);
}
