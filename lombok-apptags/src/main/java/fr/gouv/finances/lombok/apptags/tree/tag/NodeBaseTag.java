/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : NodeBaseTag.java
 *
 */
package fr.gouv.finances.lombok.apptags.tree.tag;

import javax.servlet.jsp.JspException;

import org.springframework.security.web.csrf.CsrfToken;

import fr.gouv.finances.lombok.apptags.base.BaseAppTags;
import fr.gouv.finances.lombok.apptags.base.TagWriter;
import fr.gouv.finances.lombok.apptags.util.Constantes;
import fr.gouv.finances.lombok.util.exception.ProgrammationException;

/**
 * Class NodeBaseTag --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.2 $ Date: 11 déc. 2009
 */
public abstract class NodeBaseTag extends BaseAppTags
{

    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** parent tree tag. */
    protected TreeTag parentTreeTag;

    /**
     * Instanciation de node base tag.
     */
    public NodeBaseTag()
    {
        super();
    }

    /**
     * methode Builds the image uri : --.
     * 
     * @param image --
     * @param imageDir --
     * @return string
     */
    protected String buildImageUri(String image, String imageDir)
    {
        String contextPath = this.getRequestContext().getContextPath();
        StringBuilder   src = new StringBuilder();

        src.append(contextPath);
        src.append(SLASH);
        src.append(imageDir);
        src.append(SLASH);
        src.append(image);
        return src.toString();
    }

    /**
     * Construit l'url d'accès à une transition pour effectuer une action sur un noeud.
     * 
     * @param transition --
     * @param nodeId --
     * @return the string
     * @throws JspException the jsp exception
     */
    protected String contruithref(String transition, String nodeId) throws JspException
    {
        StringBuilder   href = new StringBuilder();
        String contextPath = this.getRequestContext().getContextPath();
        // L'indication du lien est transmise sous la forme action / transition
        // du webflow
        
        // récupération du token csrf 
        CsrfToken csrfToken = (CsrfToken) this.pageContext.getRequest().getAttribute("_csrf");
        
        String flowExecutionKey = (String) this.getBean(this.parentTreeTag.getFlowExecutionKeyBeanName(), null, null);
        href.append("javascript:location.href='");
        href.append(contextPath);
        href.append(SLASH);
        href.append(this.parentTreeTag.getAction());
        href.append('?');
        href.append(this.parentTreeTag.getFlowExecutionKeyFieldName());
        href.append('=');
        href.append(flowExecutionKey);
        href.append(AMPERSAND);
        href.append(this.parentTreeTag.getEventIdFieldName());
        href.append('=');
        href.append(transition);
        href.append(AMPERSAND);
        href.append(this.parentTreeTag.getNodeIdParameter());
        href.append('=');
        href.append(nodeId);
        // injection tu token csrf, s'il est défini
        if (csrfToken != null && csrfToken.getParameterName() != null && csrfToken.getToken() != null)
        {
            href.append(AMPERSAND);
            href.append(csrfToken.getParameterName());
            href.append('=');
            href.append(csrfToken.getToken());
        }
        href.append("&amp;'+");
        href.append(Constantes.SCROLL_JAVASCRIPT_GETCOORD);
        href.append(';');
        return href.toString();
    }

    /**
     * Inits the parent tree tag.
     */
    protected void initParentTreeTag()
    {
        this.parentTreeTag = (TreeTag) this.getAncestor(TreeTag.class);
        if (this.parentTreeTag == null && (!this.getClass().equals(TreeTag.class)))
        {
            StringBuilder   msg = new StringBuilder();
            msg.append("Le tag ");
            msg.append(this.getClass().toString());
            msg.append("doit être inclus dans un tag <TreeTag>");
            throw new ProgrammationException(msg.toString());
        }
    }

    /**
     * Ouvre un lien.
     * 
     * @param tagWriter --
     * @param alt --
     * @param title --
     * @param transition --
     * @param nodeId --
     * @throws JspException the jsp exception
     */
    protected void openLinkTagWriter(TagWriter tagWriter, String alt, String title, String transition, String nodeId)
        throws JspException
    {
        tagWriter.startTag(A_TAG);
        tagWriter.writeOptionalAttributeValue(TITLE_ATTRIBUTE, title);
        tagWriter.writeAttribute(HREF_ATTRIBUTE, this.contruithref(transition, nodeId));
        tagWriter.writeOptionalAttributeValue(CLASS_ATTRIBUTE, this.parentTreeTag.getCssClass());
        tagWriter.forceBlock();
    }

    /**
     * Insère une image.
     * 
     * @param tagWriter --
     * @param image --
     * @param alt --
     * @param imageDir --
     * @throws JspException the jsp exception
     */
    protected void writeImage(TagWriter tagWriter, String image, String alt, String imageDir) throws JspException
    {
        tagWriter.startTag(IMG_TAG);
        tagWriter.writeOptionalAttributeValue(TITLE_ATTRIBUTE, "");
        String srcImage = this.buildImageUri(image, imageDir);
        tagWriter.writeAttribute(SRC_ATTRIBUTE, srcImage);
        tagWriter.writeOptionalAttributeValue(ALT_ATTRIBUTE, alt);
        tagWriter.writeOptionalAttributeValue(STYLE_ATTRIBUTE, "border:0;");
        tagWriter.endTag();
    }

    /**
     * Insère un lien contenant une image.
     * 
     * @param tagWriter --
     * @param image --
     * @param alt --
     * @param title --
     * @param transition --
     * @param nodeId --
     * @throws JspException the jsp exception
     */
    protected void writeLinkWithImage(TagWriter tagWriter, String image, String alt, String title, String transition,
        String nodeId) throws JspException
    {
        tagWriter.startTag(A_TAG);
        tagWriter.writeOptionalAttributeValue(TITLE_ATTRIBUTE, title);
        tagWriter.writeAttribute(HREF_ATTRIBUTE, this.contruithref(transition, nodeId));
        tagWriter.writeOptionalAttributeValue(CLASS_ATTRIBUTE, this.parentTreeTag.getCssClass());

        this.writeImage(tagWriter, image, alt, this.parentTreeTag.getImagesDir());

        tagWriter.endTag();
    }
}
