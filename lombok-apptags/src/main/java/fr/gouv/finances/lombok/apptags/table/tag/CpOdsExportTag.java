/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.apptags.table.tag;

import org.apache.commons.lang.StringUtils;

import fr.gouv.finances.lombok.apptags.table.bean.Export;
import fr.gouv.finances.lombok.apptags.table.core.TableConstants;
import fr.gouv.finances.lombok.apptags.table.core.TableModel;
import fr.gouv.finances.lombok.apptags.table.view.CpJrxmlView;
import fr.gouv.finances.lombok.apptags.table.view.html.BuilderConstants;

/**
 * Class CpOdsExportTag
 */
public class CpOdsExportTag extends ExportTag
{

    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** nomapplication. */
    private String nomapplication;

    /** conf. */
    private String conf;

    /** modele. */
    private String modele;

    /** titreedition. */
    private String titreedition;

    /** pagedegarde. */
    private String pagedegarde;

    /** texteentete. */
    private String texteentete;

    /** textepieddepage. */
    private String textepieddepage;

    /**
     * Instanciation de cp ods export tag.
     */
    public CpOdsExportTag()
    {
        super();
    }

    /**
     * Accesseur de l attribut nomapplication.
     * 
     * @return nomapplication
     */
    public String getNomapplication()
    {
        return nomapplication;
    }

    /**
     * Modificateur de l attribut nomapplication.
     * 
     * @param nomapplication le nouveau nomapplication
     */
    public void setNomapplication(String nomapplication)
    {
        this.nomapplication = nomapplication;
    }

    /**
     * Accesseur de l attribut modele.
     * 
     * @return modele
     */
    public String getModele()
    {
        return modele;
    }

    /**
     * Modificateur de l attribut modele.
     * 
     * @param modele le nouveau modele
     */
    public void setModele(String modele)
    {
        this.modele = modele;
    }

    /**
     * Accesseur de l attribut conf.
     * 
     * @return conf
     */
    public String getConf()
    {
        return conf;
    }

    /**
     * Modificateur de l attribut conf.
     * 
     * @param conf le nouveau conf
     */
    public void setConf(String conf)
    {
        this.conf = conf;
    }

    /**
     * Accesseur de l attribut texteentete.
     * 
     * @return texteentete
     */
    public String getTexteentete()
    {
        return texteentete;
    }

    /**
     * Modificateur de l attribut texteentete.
     * 
     * @param texteentete le nouveau texteentete
     */
    public void setTexteentete(String texteentete)
    {
        this.texteentete = texteentete;
    }

    /**
     * Accesseur de l attribut textepieddepage.
     * 
     * @return textepieddepage
     */
    public String getTextepieddepage()
    {
        return textepieddepage;
    }

    /**
     * Modificateur de l attribut textepieddepage.
     * 
     * @param textepieddepage le nouveau textepieddepage
     */
    public void setTextepieddepage(String textepieddepage)
    {
        this.textepieddepage = textepieddepage;
    }

    /**
     * Accesseur de l attribut pagedegarde.
     * 
     * @return pagedegarde
     */
    public String getPagedegarde()
    {
        return pagedegarde;
    }

    /**
     * Modificateur de l attribut pagedegarde.
     * 
     * @param pagedegarde le nouveau pagedegarde
     */
    public void setPagedegarde(String pagedegarde)
    {
        this.pagedegarde = pagedegarde;
    }

    /**
     * Accesseur de l attribut titreedition.
     * 
     * @return titreedition
     */
    public String getTitreedition()
    {
        return titreedition;
    }

    /**
     * Modificateur de l attribut titreedition.
     * 
     * @param titreedition le nouveau titreedition
     */
    public void setTitreedition(String titreedition)
    {
        this.titreedition = titreedition;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.tag.ExportTag#addExportAttributes(fr.gouv.finances.lombok.apptags.table.core.TableModel,
     *      fr.gouv.finances.lombok.apptags.table.bean.Export)
     */
    @Override
    public void addExportAttributes(TableModel model, Export export)
    {
        if (StringUtils.isBlank(export.getView()))
        {
            export.setView(TableConstants.VIEW_PDF);
        }

        if (StringUtils.isBlank(export.getViewResolver()))
        {
            export.setViewResolver(TableConstants.VIEW_PDF);
        }

        if (StringUtils.isBlank(export.getImageName()))
        {
            export.setImageName(TableConstants.VIEW_PDF);
        }

        if (StringUtils.isBlank(export.getText()))
        {
            export.setText(BuilderConstants.TOOLBAR_PDF_TEXT);
        }

        export.addAttribute(CpJrxmlView.ATTR_FICHIER_CONF, TagUtils.evaluateExpressionAsString(
            CpJrxmlView.ATTR_FICHIER_CONF, conf, this, pageContext));

        export.addAttribute(CpJrxmlView.ATTR_MODELE, TagUtils.evaluateExpressionAsString(CpJrxmlView.ATTR_MODELE,
            modele, this, pageContext));

        export.addAttribute(CpJrxmlView.ATTR_NOM_APPLI, TagUtils.evaluateExpressionAsString(CpJrxmlView.ATTR_NOM_APPLI,
            nomapplication, this, pageContext));

        export.addAttribute(CpJrxmlView.ATTR_PAGE_DE_GARDE, TagUtils.evaluateExpressionAsString(
            CpJrxmlView.ATTR_PAGE_DE_GARDE, pagedegarde, this, pageContext));

        export.addAttribute(CpJrxmlView.ATTR_TITRE_EDITION, TagUtils.evaluateExpressionAsString(
            CpJrxmlView.ATTR_TITRE_EDITION, titreedition, this, pageContext));

        export.addAttribute(CpJrxmlView.ATTR_TEXTE_ENTETE, TagUtils.evaluateExpressionAsString(
            CpJrxmlView.ATTR_TEXTE_ENTETE, texteentete, this, pageContext));

        export.addAttribute(CpJrxmlView.ATTR_TEXTE_PIEDDEPAGE, TagUtils.evaluateExpressionAsString(
            CpJrxmlView.ATTR_TEXTE_PIEDDEPAGE, textepieddepage, this, pageContext));

    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.tag.ExportTag#release()
     */
    @Override
    public void release()
    {
        conf = null;
        modele = null;
        texteentete = null;
        titreedition = null;
        pagedegarde = null;
        textepieddepage = null;
        nomapplication = null;
        super.release();
    }

}
