/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.apptags.table.view.jrxmlinfo;

/**
 * Interface HeaderCellInfoI
 */
public interface HeaderCellInfoI
{

    /**
     * Accesseur de l attribut y left corner calcule.
     * 
     * @return y left corner calcule
     */
    public int getYLeftCornerCalcule();

    /**
     * Accesseur de l attribut x left corner calcule.
     * 
     * @return x left corner calcule
     */
    public int getXLeftCornerCalcule();

    /**
     * Accesseur de l attribut width calcule.
     * 
     * @return width calcule
     */
    public int getWidthCalcule();

    /**
     * Accesseur de l attribut height.
     * 
     * @return height
     */
    public int getHeight();

    /**
     * Modificateur de l attribut height.
     * 
     * @param height le nouveau height
     */
    public void setHeight(int height);

    /**
     * Accesseur de l attribut value.
     * 
     * @return value
     */
    public String getValue();

    /**
     * Modificateur de l attribut value.
     * 
     * @param value le nouveau value
     */
    public void setValue(String value);

    /**
     * Verifie si merge with child.
     * 
     * @return true, si c'est merge with child
     */
    public boolean isMergeWithChild();

    /**
     * Modificateur de l attribut merge with child.
     * 
     * @param mergeWithChild le nouveau merge with child
     */
    public void setMergeWithChild(boolean mergeWithChild);

    /**
     * Verifie si absorbe.
     * 
     * @return true, si c'est absorbe
     */
    public boolean isAbsorbe();

    /**
     * Modificateur de l attribut absorbe.
     * 
     * @param titreAbsorbe le nouveau absorbe
     */
    public void setAbsorbe(boolean titreAbsorbe);

}
