/*
/*Copyright (c) 2014 DGFiP - Tous droits réservés/* * Copyright 2004 original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package fr.gouv.finances.lombok.apptags.table.calc;

import java.math.BigDecimal;
import java.util.Collection;

import fr.gouv.finances.lombok.apptags.table.bean.Column;
import fr.gouv.finances.lombok.apptags.table.core.TableModel;

/**
 * Class AverageCalc
 */
public class AverageCalc implements Calc
{

    /**
     * Constructeur de la classe AverageCalc.java
     *
     */
    public AverageCalc()
    {
        super();
        
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.calc.Calc#getCalcResult(fr.gouv.finances.lombok.apptags.table.core.TableModel,
     *      fr.gouv.finances.lombok.apptags.table.bean.Column)
     */
    public Number getCalcResult(TableModel model, Column column)
    {
        Collection<Object> rows = model.getCollectionOfFilteredBeans();
        String property = column.getProperty();
        AverageValue totalValue = new AverageValue(rows.size());
        CalcUtils.eachRowCalcValue(totalValue, rows, property);

        return totalValue.getAverageValue();
    }

    /**
     * Class AverageValue
     */
    private static class AverageValue implements CalcHandler
    {

        /** total. */
        private BigDecimal total = new BigDecimal(0);

        /** row count. */
        private double rowCount;

        /**
         * Instanciation de average value.
         * 
         * @param rowCount
         */
        public AverageValue(double rowCount)
        {
            this.rowCount = rowCount;
        }

        /**
         * (methode de remplacement) {@inheritDoc}
         * 
         * @see fr.gouv.finances.lombok.apptags.table.calc.CalcHandler#processCalcValue(java.lang.Number)
         */
        public void processCalcValue(Number calcValue)
        {
            total = total.add(new BigDecimal(String.valueOf(calcValue)));
        }

        /**
         * Accesseur de l attribut average value.
         * 
         * @return average value
         */
        public Number getAverageValue()
        {
            if (rowCount > 0)
            {
                return (total.divide(new BigDecimal(rowCount)));
            }

            return new BigDecimal("0.00");
        }
    }
}
