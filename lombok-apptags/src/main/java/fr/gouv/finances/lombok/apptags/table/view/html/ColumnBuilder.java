/*
 * Copyright (c) 2014 DGFiP - Tous droits réservés
 *//*
/*Copyright (c) 2014 DGFiP - Tous droits réservés/* * Copyright 2004 original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package fr.gouv.finances.lombok.apptags.table.view.html;

import org.apache.commons.lang.StringUtils;

import fr.gouv.finances.lombok.apptags.table.bean.Column;
import fr.gouv.finances.lombok.apptags.util.HtmlBuilder;

/**
 * Class ColumnBuilder
 */
public class ColumnBuilder
{

    /** html. */
    private HtmlBuilder html;

    /** column. */
    private Column column;

    /**
     * Instanciation de column builder.
     * 
     * @param column
     */
    public ColumnBuilder(Column column)
    {
        this(new HtmlBuilder(), column);
    }

    /**
     * Instanciation de column builder.
     * 
     * @param html
     * @param column
     */
    public ColumnBuilder(HtmlBuilder html, Column column)
    {
        this.html = html;
        this.column = column;
    }

    /**
     * Accesseur de l attribut html builder.
     * 
     * @return html builder
     */
    public HtmlBuilder getHtmlBuilder()
    {
        return html;
    }

    /**
     * Accesseur de l attribut column.
     * 
     * @return column
     */
    protected Column getColumn()
    {
        return column;
    }
    
    
    /**
     * methode headers  :
     */
    public void headers(String idColumn)
    {
        html.headers(idColumn);
    }

    /**
     * methode Td start :
     */
    public void tdStart()
    {
        html.td(2);
        styleClass();
        style();
        // width();
       
    }
    /**
     * methode Td close :
     */
    public void tdClose(){
        html.close();
    }


    /**
     * methode Td end :
     */
    public void tdEnd()
    {
        html.tdEnd();
    }

    /**
     * methode Style :
     */
    public void style()
    {

        StringBuilder   stylevalue = new StringBuilder();

        if (StringUtils.isNotEmpty(column.getWidth())  &&  (!column.isShowResponsive()))
        {
            stylevalue.append("width:");
            stylevalue.append(column.getWidth());
            stylevalue.append(";");
        }

        if (StringUtils.isNotEmpty(column.getStyle()))
        {
            stylevalue.append(column.getStyle());
        }
        html.style(stylevalue.toString());
    }

    /**
     * methode Style class :
     */
    public void styleClass()
    {
        String styleClass = column.getStyleClass();
        html.styleClass(styleClass);
    }

    /**
     * methode Width :
     */
    public void width()
    {
        if (!column.isShowResponsive()) 
        {
            String width = column.getWidth();
            html.width(width);
        }
    }

    /**
     * methode Td body :
     * 
     * @param value
     */
    public void tdBody(String value)
    {
        if (StringUtils.isNotBlank(value))
        {
            html.append(value);
        }
        else
        {
            html.nbsp();
        }
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        return html.toString();
    }
    
    /**
     * methode data Attributes :
     */
    public void dataAttributes() {
        html.dataAttributes(column.getDataAttributes());
    }
    
}
