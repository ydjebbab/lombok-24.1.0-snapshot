/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.apptags.table.callback;

import java.lang.reflect.InvocationTargetException;
import java.text.MessageFormat;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.collections.Predicate;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import fr.gouv.finances.lombok.apptags.table.bean.Column;
import fr.gouv.finances.lombok.apptags.table.cell.AbstractBooleanCell;
import fr.gouv.finances.lombok.apptags.table.cell.AbstractDateCell;
import fr.gouv.finances.lombok.apptags.table.cell.AbstractMonnaieCell;
import fr.gouv.finances.lombok.apptags.table.cell.AbstractNombreCell;
import fr.gouv.finances.lombok.apptags.table.cell.AbstractPourCentCell;
import fr.gouv.finances.lombok.apptags.table.cell.Cell;
import fr.gouv.finances.lombok.apptags.table.core.TableModel;
import fr.gouv.finances.lombok.apptags.table.core.TableModelUtils;

/**
 * Class FilterPredicateFiltreAvecComparateurInferieurSuperieur
 * 
 * @author lcontinsouzas-cp Cette classe sert à filtrer en utilisant la classe FiltreAvecComparateurInferieurSuperieur
 *         on offre ainsi la possibilité de traiter des filtres de type < <= > >= <> =
 * @see FiltreAvecComparateurInferieurSuperieur
 */
public final class FilterPredicateFiltreAvecComparateurInferieurSuperieur implements Predicate
{

    /** Constant : log. */
    private static final Log log = LogFactory.getLog(FilterPredicateFiltreAvecComparateurInferieurSuperieur.class);

    /** model. */
    private final TableModel model;

    /** Constant : MSGERR. */
    private static final String MSGERR = "La propriété  \"{O}\" ne dispose pas de getter dans le bean \"{1}\"";

    /**
     * Instanciation de filter predicate filtre avec comparateur inferieur superieur.
     * 
     * @param model
     */
    public FilterPredicateFiltreAvecComparateurInferieurSuperieur(TableModel model)
    {
        this.model = model;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see org.apache.commons.collections.Predicate#evaluate(java.lang.Object)
     */
    @Override
    public boolean evaluate(Object bean)
    {
        boolean match = false;

        for (Column column : model.getColumnHandler().getColumns())
        {
            String alias = column.getAlias();
            String filterValue = model.getLimit().getFilterSet().getFilterValue(alias).trim();

            if (StringUtils.isEmpty(filterValue))
            {
                continue;
            }

            String property = column.getProperty();

            boolean isBeanContainProperty = CpContainPropertyPredicate.getInstance(property).evaluate(bean);

            if (!isBeanContainProperty)
            {
                continue;
            }

            Object valeurATester = null;
            try
            {
                valeurATester = PropertyUtils.getProperty(bean, property);
                if (log.isDebugEnabled())
                {
                    StringBuilder msg = new StringBuilder();
                    msg.append("VALEUR A TESTER : ");
                    msg.append(valeurATester);
                    log.debug(msg.toString());
                }
            }
            catch (IllegalAccessException exception)
            {
                Object[] args = {property, bean.toString()};
                throw new IllegalArgumentException(MessageFormat.format(MSGERR, args), exception);
            }
            catch (InvocationTargetException exception)
            {
                Object[] args = {property, bean.toString()};
                throw new IllegalArgumentException(MessageFormat.format(MSGERR, args), exception);
            }
            catch (NoSuchMethodException exception)
            {
                Object[] args = {property, bean.toString()};
                throw new IllegalArgumentException(MessageFormat.format(MSGERR, args), exception);
            }

            if (valeurATester == null)
            {
                match = false;
            }
            else
            {

                Cell cell = TableModelUtils.getCell(column);
                FiltreAvecComparateurInferieurSuperieur filtre = construireFiltreAvecTypeEtPattern(cell);
                filtre.setTypeComparaison(this.extraireTypeDeComparaison(filterValue));
                filtre.setFiltreStrict(column.isFilterStrict());
                if ((filtre.getTypeComparaison().equalsIgnoreCase(FiltreAvecComparateurInferieurSuperieur.COMPARATEUR_EGAL))
                    && (!filterValue.startsWith(FiltreAvecComparateurInferieurSuperieur.COMPARATEUR_EGAL)))
                {
                    filtre.setValeurFiltreSousFormeString(filterValue);
                }
                else
                {
                    filtre.setValeurFiltreSousFormeString(filterValue.substring(filtre.getTypeComparaison().length()));
                }

                match = filtre.estValeurAssortieAuFiltre(valeurATester);

            }
            if (match == false)
            {
                break;
            }

        }

        return match;
    }

    /**
     * methode Construire filtre avec type et pattern :
     * 
     * @param cell
     * @return filtre avec comparateur inferieur superieur
     */
    private FiltreAvecComparateurInferieurSuperieur construireFiltreAvecTypeEtPattern(Cell cell)
    {
        FiltreAvecComparateurInferieurSuperieur cfi = new FiltreAvecComparateurInferieurSuperieur();

        if (cell instanceof AbstractMonnaieCell)
        {
            cfi.setPatternPourConstruireLeFiltre(((AbstractMonnaieCell) cell).getFormatPattern());
            cfi.setTypeDuFiltre(FiltreAvecComparateurInferieurSuperieur.TYPE_FILTRAGE_MONNAIE);
        }

        else if (cell instanceof AbstractDateCell)
        {
            cfi.setPatternPourConstruireLeFiltre(((AbstractDateCell) cell).getFormatPattern());
            cfi.setTypeDuFiltre(FiltreAvecComparateurInferieurSuperieur.TYPE_FILTRAGE_DATE);
        }

        else if (cell instanceof AbstractNombreCell)
        {
            cfi.setPatternPourConstruireLeFiltre(((AbstractNombreCell) cell).getFormatPattern());
            cfi.setTypeDuFiltre(FiltreAvecComparateurInferieurSuperieur.TYPE_FILTRAGE_NOMBRE);
        }

        else if (cell instanceof AbstractPourCentCell)
        {
            cfi.setPatternPourConstruireLeFiltre(((AbstractPourCentCell) cell).getFormatPattern());
            cfi.setTypeDuFiltre(FiltreAvecComparateurInferieurSuperieur.TYPE_FILTRAGE_POURCENTAGE);
        }

        else if (cell instanceof AbstractBooleanCell)
        {
            cfi.setFormatPourBooleen(((AbstractBooleanCell) cell).getFormat());
            cfi.setTypeDuFiltre(FiltreAvecComparateurInferieurSuperieur.TYPE_FILTRAGE_BOOLEEN);
        }
        else
        {
            cfi.setTypeDuFiltre(FiltreAvecComparateurInferieurSuperieur.TYPE_FILTRAGE_STRING);
        }
        return cfi;
    }

    /**
     * methode Extraire type de comparaison :
     * 
     * @param filterValue
     * @return string
     */
    private String extraireTypeDeComparaison(String filterValue)
    {
        String typeComparaison;

        if (filterValue.trim().startsWith(FiltreAvecComparateurInferieurSuperieur.COMPARATEUR_INFERIEUR_OU_EGAL))
        {
            typeComparaison = FiltreAvecComparateurInferieurSuperieur.COMPARATEUR_INFERIEUR_OU_EGAL;
        }
        else if (filterValue.startsWith(FiltreAvecComparateurInferieurSuperieur.COMPARATEUR_SUPERIEUR_OU_EGAL))
        {
            typeComparaison = FiltreAvecComparateurInferieurSuperieur.COMPARATEUR_SUPERIEUR_OU_EGAL;
        }
        else if (filterValue.startsWith(FiltreAvecComparateurInferieurSuperieur.COMPARATEUR_DIFFERENT))
        {
            typeComparaison = FiltreAvecComparateurInferieurSuperieur.COMPARATEUR_DIFFERENT;
        }
        else if (filterValue.startsWith(FiltreAvecComparateurInferieurSuperieur.COMPARATEUR_INFERIEUR))
        {
            typeComparaison = FiltreAvecComparateurInferieurSuperieur.COMPARATEUR_INFERIEUR;
        }
        else if (filterValue.startsWith(FiltreAvecComparateurInferieurSuperieur.COMPARATEUR_SUPERIEUR))
        {
            typeComparaison = FiltreAvecComparateurInferieurSuperieur.COMPARATEUR_SUPERIEUR;
        }
        else if (filterValue.startsWith(FiltreAvecComparateurInferieurSuperieur.COMPARATEUR_EGAL))
        {
            typeComparaison = FiltreAvecComparateurInferieurSuperieur.COMPARATEUR_EGAL;
        }
        else
        {
            typeComparaison = FiltreAvecComparateurInferieurSuperieur.COMPARATEUR_EGAL;
        }
        return typeComparaison;
    }

}
