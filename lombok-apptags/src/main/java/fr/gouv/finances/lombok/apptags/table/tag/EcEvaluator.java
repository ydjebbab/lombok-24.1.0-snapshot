/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.apptags.table.tag;

/*
 * 
 * Cette classe est un patch basé sur la version 1.1.2 de taglibs standard
 * pour ne pas utiliser le cache de ELEvaluator qui provoque une fuite de mémoire (memory leak)
 * 
 * 
 * 
 * 
 */
import java.text.MessageFormat;
import java.util.Map;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.Tag;

import org.apache.taglibs.standard.lang.jstl.Constants;
import org.apache.taglibs.standard.lang.jstl.ELException;
import org.apache.taglibs.standard.lang.jstl.JSTLVariableResolver;
import org.apache.taglibs.standard.lang.support.ExpressionEvaluator;

/**
 * <p>
 * This is the expression evaluator "adapter" that customizes it for use with the JSP Standard Tag Library. It uses a
 * VariableResolver implementation that looks up variables from the PageContext and also implements its implicit
 * objects. It also wraps ELExceptions in JspExceptions that describe the attribute name and value causing the error.
 * 
 * @author Nathan Abramson - Art Technology Group
 * @author Shawn Bayern
 * @version $Change: 181177 $$DateTime: 2001/06/26 08:45:09 $$Author: ageffroy $
 **/

public class EcEvaluator implements ExpressionEvaluator
{
    // -------------------------------------
    // Properties
    // -------------------------------------

    // -------------------------------------
    // Member variables
    // -------------------------------------

    /** The singleton instance of the evaluator *. */
    static EcElEvaluator sEvaluator = new EcElEvaluator(new JSTLVariableResolver(), true);

    // -------------------------------------
    // ExpressionEvaluator methods
    // -------------------------------------
    /**
     * Translation time validation of an attribute value. This method will return a null String if the attribute value
     * is valid; otherwise an error message.
     * 
     * @param pAttributeName le attribute name
     * @param pAttributeValue le attribute value
     * @return string
     */
    @Override
    public String validate(String pAttributeName, String pAttributeValue)
    {
        try
        {
            sEvaluator.parseExpressionString(pAttributeValue);
            return null;
        }
        catch (ELException exc)
        {
            return MessageFormat.format(Constants.ATTRIBUTE_PARSE_EXCEPTION,
                new Object[] {"" + pAttributeName,
                        "" + pAttributeValue, exc.getMessage()});
        }
    }

    // -------------------------------------
    /**
     * Evaluates the expression at request time.
     * 
     * @param pAttributeName le attribute name
     * @param pAttributeValue le attribute value
     * @param pExpectedType le expected type
     * @param pTag le tag
     * @param pPageContext le page context
     * @param functions
     * @param defaultPrefix
     * @return object
     * @throws JspException the jsp exception
     */
    public Object evaluate(String pAttributeName, String pAttributeValue, Class pExpectedType, Tag pTag,
        PageContext pPageContext, Map functions, String defaultPrefix) throws JspException
    {
        try
        {
            return sEvaluator.evaluate(pAttributeValue, pPageContext, pExpectedType, functions, defaultPrefix);
        }
        catch (ELException exc)
        {
            throw new JspException(MessageFormat.format(Constants.ATTRIBUTE_EVALUATION_EXCEPTION, new Object[] {
                    "" + pAttributeName, "" + pAttributeValue, exc.getMessage(),
                    exc.getRootCause()}), exc);
        }
    }

    /**
     * Conduit to old-style call for convenience.
     * 
     * @param pAttributeName le attribute name
     * @param pAttributeValue le attribute value
     * @param pExpectedType le expected type
     * @param pTag le tag
     * @param pPageContext le page context
     * @return object
     * @throws JspException the jsp exception
     */
    @Override
    public Object evaluate(String pAttributeName, String pAttributeValue, Class pExpectedType, Tag pTag,
        PageContext pPageContext) throws JspException
    {
        return evaluate(pAttributeName, pAttributeValue, pExpectedType, pTag, pPageContext, null, null);
    }

    // -------------------------------------
    // Testing methods
    // -------------------------------------
    /**
     * Parses the given attribute value, then converts it back to a String in its canonical form.
     * 
     * @param pAttributeValue le attribute value
     * @return string
     * @throws JspException the jsp exception
     */
    public static String parseAndRender(String pAttributeValue) throws JspException
    {
        try
        {
            return sEvaluator.parseAndRender(pAttributeValue);
        }
        catch (ELException exc)
        {
            throw new JspException(MessageFormat.format(Constants.ATTRIBUTE_PARSE_EXCEPTION,
                new Object[] {"test",
                        "" + pAttributeValue, exc.getMessage()}), exc);
        }
    }

    // -------------------------------------

}
