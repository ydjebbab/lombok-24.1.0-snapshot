/*
/*Copyright (c) 2014 DGFiP - Tous droits réservés/* * Copyright 2004 original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package fr.gouv.finances.lombok.apptags.table.limit;

import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * Class TableLimit
 */
public final class TableLimit implements Limit
{

    /** limit factory. */
    protected LimitFactory limitFactory;

    /** filter set. */
    protected FilterSet filterSet;

    /** sort. */
    protected Sort sort;

    /** exported. */
    protected boolean exported;

    /** row start. */
    protected int rowStart;

    /** row end. */
    protected int rowEnd;

    /** current rows displayed. */
    protected int currentRowsDisplayed;

    /** page. */
    protected int page;

    /** total rows. */
    protected int totalRows;

    /**
     * Instanciation de table limit.
     * 
     * @param limitFactory
     */
    public TableLimit(LimitFactory limitFactory)
    {
        this.limitFactory = limitFactory;
        this.filterSet = limitFactory.getFilterSet();
        this.sort = limitFactory.getSort();
        this.exported = limitFactory.isExported();
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.limit.Limit#getFilterSet()
     */
    @Override
    public FilterSet getFilterSet()
    {
        return filterSet;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.limit.Limit#getRowEnd()
     */
    @Override
    public int getRowEnd()
    {
        return rowEnd;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.limit.Limit#getRowStart()
     */
    @Override
    public int getRowStart()
    {
        return rowStart;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.limit.Limit#getSort()
     */
    @Override
    public Sort getSort()
    {
        return sort;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.limit.Limit#getPage()
     */
    @Override
    public int getPage()
    {
        return page;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.limit.Limit#getCurrentRowsDisplayed()
     */
    @Override
    public int getCurrentRowsDisplayed()
    {
        return currentRowsDisplayed;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.limit.Limit#getTotalRows()
     */
    @Override
    public int getTotalRows()
    {
        return totalRows;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.limit.Limit#isFiltered()
     */
    @Override
    public boolean isFiltered()
    {
        return filterSet.isFiltered();
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.limit.Limit#isCleared()
     */
    @Override
    public boolean isCleared()
    {
        return filterSet.isCleared();
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.limit.Limit#isSorted()
     */
    @Override
    public boolean isSorted()
    {
        return sort.isSorted();
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.limit.Limit#isExported()
     */
    @Override
    public boolean isExported()
    {
        return exported;
    }

    /**
     * Lors de l'utilisation de l'interface Limit, cette méthode doit être appelée pour positionner les attributs
     * suivants : rowStart, rowEnd, currentRowsDisplayed et page.
     * 
     * @param totalRows Le nombre total de lignes de la page
     * @param rowsDisplayed Le nombre de lignes affichées par le tableau défini soit dans l'élément 'table' soit dans
     *        les préférences du tableau.
     * @param defaultStartPage
     */
    @Override
    public void setRowAttributes(int totalRows, int rowsDisplayed, int defaultStartPage)
    {
        int currentRowsDisplayed = limitFactory.getCurrentRowsDisplayed(totalRows, rowsDisplayed);

        int page = limitFactory.getPage(defaultStartPage);

        page = getValidPage(page, totalRows, currentRowsDisplayed);

        int rowStart = (page - 1) * currentRowsDisplayed;

        int rowEnd = rowStart + currentRowsDisplayed;

        if (rowEnd > totalRows)
        {
            rowEnd = totalRows;
        }

        this.page = page;
        this.currentRowsDisplayed = currentRowsDisplayed;
        this.totalRows = totalRows;
        this.rowStart = rowStart;
        this.rowEnd = rowEnd;
    }

    /**
     * Le numéro de la page retournée (qui ne doit pas être > au nombre de pages qui peuvent être affichées).
     * 
     * @param page
     * @param totalRows
     * @param currentRowsDisplayed
     * @return valid page
     */
    private int getValidPage(int page, int totalRows, int currentRowsDisplayed)
    {
        int result = page;

        if (!isValidPage(page, totalRows, currentRowsDisplayed))
        {
            result = getValidPage(--page, totalRows, currentRowsDisplayed);
        }

        return result;
    }

    /**
     * Teste si le numéro de page retournée n'est pas supérieur aux nombres de pages que le tableau peut afficher. Ce
     * problème peut se produire lorsque le mécanisme de conservation de l'état est activé et que des lignes ont été
     * supprimées dans le tableau sous-jacent
     * 
     * @param page
     * @param totalRows
     * @param currentRowsDisplayed
     * @return true, si c'est valid page
     */
    private boolean isValidPage(int page, int totalRows, int currentRowsDisplayed)
    {
        boolean result = true;

        if (page == 1)
        {
            result = true;
        }
        else
        {
            int rowStart = (page - 1) * currentRowsDisplayed;
            int rowEnd = rowStart + currentRowsDisplayed;
            if (rowEnd > totalRows)
            {
                rowEnd = totalRows;
            }
            result = rowEnd > rowStart;
        }
        return result;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        ToStringBuilder builder = new ToStringBuilder(this);
        builder.append("rowStart", rowStart);
        builder.append("rowEnd", rowEnd);
        builder.append("currentRowsDisplayed", currentRowsDisplayed);
        builder.append("page", page);
        builder.append("totalRows", totalRows);
        builder.append("exported", exported);
        builder.append("sort", sort);
        builder.append("filterSet", filterSet);
        return builder.toString();
    }
}
