/*
/*Copyright (c) 2014 DGFiP - Tous droits réservés/* * Copyright 2004 original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package fr.gouv.finances.lombok.apptags.table.tag;

import java.util.Collection;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.Tag;
import javax.servlet.jsp.tagext.TagSupport;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import fr.gouv.finances.lombok.apptags.table.core.TableModel;
import fr.gouv.finances.lombok.apptags.util.ExceptionUtils;

/**
 * Class TagUtils
 */
public final class TagUtils
{

    /** logger. */
    private static Log logger = LogFactory.getLog(ColumnTag.class);

    /**
     * Instanciation de tag utils.
     */
    private TagUtils()
    {
    }

    /**
     * methode Evaluate expression as string :
     * 
     * @param attributeName
     * @param attribute
     * @param tag
     * @param pageContext
     * @return string
     */
    public static final String evaluateExpressionAsString(String attributeName, String attribute, Tag tag,
        PageContext pageContext)
    {
        try
        {
            if (attribute != null)
            {
                attribute =
                    (String) EcExpressionEvaluatorManager.evaluate(attributeName, attribute, String.class, tag,
                        pageContext);
            }
        }
        catch (JspException e)
        {
            logger.error("Could not resolve EL for [" + attributeName + "] - " + ExceptionUtils.formatStackTrace(e));
        }

        return attribute;
    }

    /**
     * methode Evaluate expression as object :
     * 
     * @param attributeName
     * @param attribute
     * @param tag
     * @param pageContext
     * @return object
     */
    public static final Object evaluateExpressionAsObject(String attributeName, Object attribute, Tag tag,
        PageContext pageContext)
    {
        try
        {
            if (attribute != null)
            {
                attribute =
                    EcExpressionEvaluatorManager.evaluate(attributeName, attribute.toString(), Object.class, tag,
                        pageContext);
            }
        }
        catch (JspException e)
        {
            logger.error("Could not resolve EL for [" + attributeName + "] - " + ExceptionUtils.formatStackTrace(e));
        }

        return attribute;
    }

    /**
     * methode Evaluate expression as collection :
     * 
     * @param attributeName
     * @param attribute
     * @param tag
     * @param pageContext
     * @return collection
     */
    public static final Collection evaluateExpressionAsCollection(String attributeName, Object attribute, Tag tag,
        PageContext pageContext)
    {
        attribute = evaluateExpressionAsObject(attributeName, attribute, tag, pageContext);

        if (attribute == null || !(attribute instanceof Collection))
        {
            if (logger.isDebugEnabled())
            {
                logger.debug("The attribute [" + attributeName + "] is null or not a Collection.");
            }
            return null;
        }

        return (Collection) attribute;
    }

    /**
     * methode Evaluate expression as boolean :
     * 
     * @param attributeName
     * @param attribute
     * @param tag
     * @param pageContext
     * @return boolean
     */
    public static final Boolean evaluateExpressionAsBoolean(String attributeName, String attribute, Tag tag,
        PageContext pageContext)
    {
        attribute = evaluateExpressionAsString(attributeName, attribute, tag, pageContext);

        if (attribute == null)
        {
            return null;
        }

        return Boolean.valueOf(attribute);
    }

    /**
     * methode Evaluate expression as int :
     * 
     * @param attributeName
     * @param attribute
     * @param tag
     * @param pageContext
     * @return int
     */
    public static final int evaluateExpressionAsInt(String attributeName, String attribute, Tag tag,
        PageContext pageContext)
    {
        attribute = evaluateExpressionAsString(attributeName, attribute, tag, pageContext);

        if (attribute == null)
        {
            return 0;
        }

        return Integer.valueOf(attribute).intValue();
    }

    /**
     * Accesseur de l attribut model.
     * 
     * @param tag
     * @return model
     */
    public static TableModel getModel(Tag tag)
    {
        TableTag tableTag = (TableTag) TagSupport.findAncestorWithClass(tag, TableTag.class);
        return tableTag.getModel();
    }

    /**
     * Verifie si iterating body.
     * 
     * @param tag
     * @return true, si c'est iterating body
     */
    public static boolean isIteratingBody(Tag tag)
    {
        return getModel(tag).getCurrentRowBean() != null;
    }
}
