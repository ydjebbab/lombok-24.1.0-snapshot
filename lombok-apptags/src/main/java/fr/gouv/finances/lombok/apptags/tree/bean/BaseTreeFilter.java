/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : BaseTreeFilter.java
 *
 */
package fr.gouv.finances.lombok.apptags.tree.bean;

import java.io.Serializable;

/**
 * Class BaseTreeFilter.
 * 
 * @author wpetit-cp
 * @version $Revision: 1.2 $ Date: 15 déc. 2009
 */
public class BaseTreeFilter implements ITreeFilter, Serializable
{
    private static final long serialVersionUID = 1L;

    public BaseTreeFilter()
    {
        super();       
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param tree
     * @param node
     * @return true, si c'est vrai
     * @see fr.gouv.finances.lombok.apptags.tree.bean.ITreeFilter#accept(fr.gouv.finances.lombok.apptags.tree.bean.Tree,
     *      fr.gouv.finances.lombok.apptags.tree.bean.TreeIteratorElement)
     */
    @Override
    public boolean accept(Tree tree, TreeIteratorElement node)
    {
        return true;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param tree
     * @see fr.gouv.finances.lombok.apptags.tree.bean.ITreeFilter#init(fr.gouv.finances.lombok.apptags.tree.bean.Tree)
     */
    @Override
    public void init(Tree tree)
    {
        // RAS
    }
}
