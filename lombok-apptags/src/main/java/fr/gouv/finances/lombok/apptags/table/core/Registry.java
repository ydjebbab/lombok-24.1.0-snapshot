/*
 * Copyright (c) 2014 DGFiP - Tous droits réservés
 */
/*
/*Copyright (c) 2014 DGFiP - Tous droits réservés/* * Copyright 2004 original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package fr.gouv.finances.lombok.apptags.table.core;

import java.util.Map;

/**
 * L'interface Registry maintient tous les paramètres qu'ils soient spécifiques à la table ou définis par l'utilisteur.
 */
public interface Registry
{

    /**
     * methode Handle state :
     * 
     * @param tableParameterMap
     * @return map
     */
    public Map<String, Object> handleState(Map<String, Object> tableParameterMap);

    /**
     * methode Adds the parameter :
     * 
     * @param name
     * @param value
     */
    public void addParameter(String name, Object value);

    /**
     * Accesseur de l attribut parameter.
     * 
     * @param parameter
     * @return parameter
     */
    public String getParameter(String parameter);

    /**
     * methode Sets the parameter map :
     */
    public void setParameterMap();

    /**
     * Accesseur de l attribut parameter map.
     * 
     * @return parameter map
     */
    public Map<String, Object> getParameterMap();

    /**
     * methode Removes the parameter :
     * 
     * @param parameter
     */
    public void removeParameter(String parameter);
}