/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : NodeTextTag.java
 *
 */
package fr.gouv.finances.lombok.apptags.tree.tag;

import javax.servlet.jsp.JspException;

import fr.gouv.finances.lombok.apptags.base.TagWriter;
import fr.gouv.finances.lombok.apptags.tree.bean.TreeIteratorElement;
import fr.gouv.finances.lombok.apptags.util.Constantes;

/**
 * Class NodeTextTag --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.2 $ Date: 11 déc. 2009
 */
public class NodeTextTag extends NodeBaseTag
{

    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** select node transition. */
    private String selectNodeTransition;

    /** un select node transition. */
    private String unSelectNodeTransition;

    /** expand node transition. */
    private String expandNodeTransition;

    /** collapse node transition. */
    private String collapseNodeTransition;

    /** class node selected. */
    private String classNodeSelected;

    /** class node un selected. */
    private String classNodeUnSelected;

    /** selectable. */
    private String selectable;

    /** navigable. */
    private String navigable;

    /** inline. */
    private String inline;

    // Elements paramétrables par un fichier properties

    /** alt collapse node. */
    private String altCollapseNode;

    /** alt expand node. */
    private String altExpandNode;

    /** tag writer. */
    private TagWriter tagWriter;

    /**
     * Instanciation de node text tag.
     */
    public NodeTextTag()
    {
        super();
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @return int
     * @throws JspException the jsp exception
     * @see javax.servlet.jsp.tagext.TagSupport#doAfterBody()
     */
    @Override
    public int doAfterBody() throws JspException
    {
        int result = EVAL_PAGE;

        // Fermeture du lien de sélection ou de dé-selection
        if (isTRUE(this.selectable))
        {
            // Fin du span
            // tagWriter.endTag();
            // Fin du lien
            this.writecloseLinkSelectOrUnselect(tagWriter);
        }
        else if (isTRUE(this.navigable) && this.parentTreeTag.getElement().getNode().isFolder()
            && this.parentTreeTag.getElement().getNode().hasChildren())
        {
            // Fin du lien
            this.writecloseLinkExpandOrCollapse(tagWriter);
        }
        else if (!isTRUE(this.inline))
        {
            // Fin du div
            tagWriter.endTag();
        }

        // Fermeture de la cellule du tableau contenant le texte du noeud
        tagWriter.endTag();

        return result;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @see org.springframework.web.servlet.tags.RequestContextAwareTag#doFinally()
     */
    @Override
    public void doFinally()
    {
        super.doFinally();
        this.tagWriter = null;
    }

    /**
     * Accesseur de l attribut alt collapse node.
     * 
     * @return alt collapse node
     */
    public String getAltCollapseNode()
    {
        return altCollapseNode;
    }

    /**
     * Accesseur de l attribut alt expand node.
     * 
     * @return alt expand node
     */
    public String getAltExpandNode()
    {
        return altExpandNode;
    }

    /**
     * Accesseur de l attribut class node selected.
     * 
     * @return class node selected
     */
    public String getClassNodeSelected()
    {
        return classNodeSelected;
    }

    /**
     * Accesseur de l attribut class node un selected.
     * 
     * @return class node un selected
     */
    public String getClassNodeUnSelected()
    {
        return classNodeUnSelected;
    }

    /**
     * Accesseur de l attribut collapse node transition.
     * 
     * @return collapse node transition
     */
    public String getCollapseNodeTransition()
    {
        return collapseNodeTransition;
    }

    /**
     * Accesseur de l attribut expand node transition.
     * 
     * @return expand node transition
     */
    public String getExpandNodeTransition()
    {
        return expandNodeTransition;
    }

    /**
     * Accesseur de l attribut inline.
     * 
     * @return inline
     */
    public String getInline()
    {
        return inline;
    }

    /**
     * Accesseur de l attribut navigable.
     * 
     * @return navigable
     */
    public String getNavigable()
    {
        return navigable;
    }

    /**
     * Accesseur de l attribut selectable.
     * 
     * @return selectable
     */
    public String getSelectable()
    {
        return selectable;
    }

    /**
     * Accesseur de l attribut select node transition.
     * 
     * @return select node transition
     */
    public String getSelectNodeTransition()
    {
        return selectNodeTransition;
    }

    /**
     * Accesseur de l attribut un select node transition.
     * 
     * @return un select node transition
     */
    public String getUnSelectNodeTransition()
    {
        return unSelectNodeTransition;
    }

    /**
     * Modificateur de l attribut alt collapse node.
     * 
     * @param altCollapseNode le nouveau alt collapse node
     */
    public void setAltCollapseNode(String altCollapseNode)
    {
        this.altCollapseNode = altCollapseNode;
    }

    /**
     * Modificateur de l attribut alt expand node.
     * 
     * @param altExpandNode le nouveau alt expand node
     */
    public void setAltExpandNode(String altExpandNode)
    {
        this.altExpandNode = altExpandNode;
    }

    /**
     * Modificateur de l attribut class node selected.
     * 
     * @param classNodeSelected le nouveau class node selected
     */
    public void setClassNodeSelected(String classNodeSelected)
    {
        this.classNodeSelected = classNodeSelected;
    }

    /**
     * Modificateur de l attribut class node un selected.
     * 
     * @param classNodeUnSelected le nouveau class node un selected
     */
    public void setClassNodeUnSelected(String classNodeUnSelected)
    {
        this.classNodeUnSelected = classNodeUnSelected;
    }

    /**
     * Modificateur de l attribut collapse node transition.
     * 
     * @param collapseNodeTransition le nouveau collapse node transition
     */
    public void setCollapseNodeTransition(String collapseNodeTransition)
    {
        this.collapseNodeTransition = collapseNodeTransition;
    }

    /**
     * Modificateur de l attribut expand node transition.
     * 
     * @param expandNodeTransition le nouveau expand node transition
     */
    public void setExpandNodeTransition(String expandNodeTransition)
    {
        this.expandNodeTransition = expandNodeTransition;
    }

    /**
     * Modificateur de l attribut inline.
     * 
     * @param inline le nouveau inline
     */
    public void setInline(String inline)
    {
        this.inline = inline;
    }

    /**
     * Modificateur de l attribut navigable.
     * 
     * @param navigable le nouveau navigable
     */
    public void setNavigable(String navigable)
    {
        this.navigable = navigable;
    }

    /**
     * Modificateur de l attribut selectable.
     * 
     * @param selectable le nouveau selectable
     */
    public void setSelectable(String selectable)
    {
        this.selectable = selectable;
    }

    /**
     * Modificateur de l attribut select node transition.
     * 
     * @param selectNodeTransition le nouveau select node transition
     */
    public void setSelectNodeTransition(String selectNodeTransition)
    {
        this.selectNodeTransition = selectNodeTransition;
    }

    /**
     * Modificateur de l attribut un select node transition.
     * 
     * @param unSelectNodeTransition le nouveau un select node transition
     */
    public void setUnSelectNodeTransition(String unSelectNodeTransition)
    {
        this.unSelectNodeTransition = unSelectNodeTransition;
    }

    /**
     * methode Write open in block : --.
     * 
     * @throws JspException the jsp exception
     */
    public void writeOpenInBlock() throws JspException
    {
        this.tagWriter.startTag(DIV_TAG);
        tagWriter.forceBlock();
    }

    /**
     * methode Writeclose link expand or collapse : --.
     * 
     * @param tagWriter --
     * @throws JspException the jsp exception
     */
    protected void writecloseLinkExpandOrCollapse(TagWriter tagWriter) throws JspException
    {
        // Fin du lien
        tagWriter.endTag();
    }

    /**
     * Ferme un lien.
     * 
     * @param tagWriter --
     * @throws JspException the jsp exception
     */
    protected void writecloseLinkSelectOrUnselect(TagWriter tagWriter) throws JspException
    {
        // Fin du span
        tagWriter.endTag();
        // Fin du lien
        tagWriter.endTag();
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param tagWriter
     * @return int
     * @throws JspException the jsp exception
     * @see fr.gouv.finances.lombok.apptags.base.BaseAppTags#writeTagContent(fr.gouv.finances.lombok.apptags.base.TagWriter)
     */
    @Override
    protected int writeTagContent(TagWriter tagWriter) throws JspException
    {
        this.initParentTreeTag();

        this.tagWriter = tagWriter;

        // Initialise les attributs du tag
        this.initAttributes();

        TreeIteratorElement element = this.parentTreeTag.getElement();

        // Ouverture de la cellule du tableau contenant le texte du noeud
        tagWriter.startTag(TD_TAG);
        tagWriter.writeOptionalAttributeValue(CLASS_ATTRIBUTE, this.parentTreeTag.getCssClass() + "texte");
        tagWriter.forceBlock();

        if (isTRUE(this.selectable))
        {
            this.writeOpenLinkSelectOrUnselect(element);
        }
        else if (isTRUE(this.navigable) && element.getNode().isFolder() && element.getNode().hasChildren())
        {
            this.writeOpenLinkExpandOrCollapse(element);
        }
        else if (!isTRUE(this.inline))
        {
            this.writeOpenInBlock();
        }
        else
        {
            tagWriter.forceBlock();
        }

        return EVAL_PAGE;
    }

    /**
     * Initialise les paramètres du tag en fonction de la valeur des attributs passés en paramètre au tag et des
     * paramètres par défaut.
     * 
     * @throws JspException the jsp exception
     */
    private void initAttributes() throws JspException
    {
        // Utilisation d'un NodeParametres qui porte les valeurs affectées
        // soit par un paramètre soit par défaut
        this.selectNodeTransition =
            this.initvar(this.selectNodeTransition, Constantes.SELECT_NODE_TRANSITION_KEY,
                Constantes.SELECT_NODE_TRANSITION_DEFAULT);

        this.unSelectNodeTransition =
            this.initvar(this.unSelectNodeTransition, Constantes.UNSELECT_NODE_TRANSITION_KEY,
                Constantes.UNSELECT_NODE_TRANSITION_DEFAULT);

        this.classNodeSelected =
            this.initvar(this.classNodeSelected, Constantes.CLASS_NODE_SELECTED_KEY,
                Constantes.CLASS_NODE_SELECTED_DEFAULT);

        this.classNodeUnSelected =
            this.initvar(this.classNodeUnSelected, Constantes.CLASS_NODE_UNSELECTED_KEY,
                Constantes.CLASS_NODE_UNSELECTED_DEFAULT);

        this.selectable = this.initvar(this.selectable, Constantes.SELECTABLE_KEY, Constantes.SELECTABLE_DEFAULT);

        this.navigable = this.initvar(this.navigable, Constantes.NAVIGABLE_KEY, Constantes.NAVIGABLE_DEFAULT);

        this.inline = this.initvar(this.inline, Constantes.INLINE_KEY, Constantes.INLINE_DEFAULT);

        this.altCollapseNode =
            this.initvar(null, Constantes.ALT_COLLAPSE_NODE_KEY, Constantes.ALT_COLLAPSE_NODE_DEFAULT);

        this.altExpandNode = this.initvar(null, Constantes.ALT_EXPAND_NODE_KEY, Constantes.ALT_EXPAND_NODE_DEFAULT);

        this.collapseNodeTransition =
            this.initvar(this.collapseNodeTransition, Constantes.COLLAPSE_NODE_TRANSITION_KEY,
                Constantes.COLLAPSE_NODE_TRANSITION_DEFAULT);

        this.expandNodeTransition =
            this.initvar(this.expandNodeTransition, Constantes.EXPAND_NODE_TRANSITION_KEY,
                Constantes.EXPAND_NODE_TRANSITION_DEFAULT);
    }

    /**
     * Encadre le contenu du noeud dans un lien (élément a) qui permet de développer ou de réduire le noeud. Cette
     * option est incompatible avec l'utilisation d'un lien pour sélectionner un noeud Possible uniquement si le contenu
     * du noeud est de type inline
     * 
     * @param element --
     * @throws JspException the jsp exception
     */
    private void writeOpenLinkExpandOrCollapse(TreeIteratorElement element) throws JspException
    {
        // Traitement des noeuds qui possèdent des enfants
        if (element.getNode().hasChildren())
        {
            // Noeuds ouverts
            if (element.isExpanded())
            {
                this.openLinkTagWriter(this.tagWriter, this.altCollapseNode, this.altCollapseNode,
                    this.collapseNodeTransition, element.getNode().getId());
            }
            // Noeuds fermés
            else
            {
                this.openLinkTagWriter(tagWriter, this.altExpandNode, this.altExpandNode, this.expandNodeTransition,
                    element.getNode().getId());
            }
        }

    }

    /**
     * Ouvre le lien de sélection et de déselection d'un noeud.
     * 
     * @param element --
     * @throws JspException the jsp exception
     */
    private void writeOpenLinkSelectOrUnselect(TreeIteratorElement element) throws JspException
    {
        tagWriter.startTag(A_TAG);
        tagWriter.writeOptionalAttributeValue(TITLE_ATTRIBUTE, "");
        tagWriter.writeOptionalAttributeValue(CLASS_ATTRIBUTE, this.parentTreeTag.getCssClass());

        if (element.isSelected())
        {
            tagWriter.writeAttribute(HREF_ATTRIBUTE, this.contruithref(this.unSelectNodeTransition, element.getId()));
            tagWriter.startTag(SPAN_TAG);
            tagWriter.writeOptionalAttributeValue(CLASS_ATTRIBUTE, this.getClassNodeSelected());
            tagWriter.forceBlock();
        }
        else
        {
            tagWriter.writeAttribute(HREF_ATTRIBUTE, this.contruithref(this.selectNodeTransition, element.getId()));
            tagWriter.startTag(SPAN_TAG);
            tagWriter.writeOptionalAttributeValue(CLASS_ATTRIBUTE, this.getClassNodeUnSelected());
            tagWriter.forceBlock();
        }
    }
}
