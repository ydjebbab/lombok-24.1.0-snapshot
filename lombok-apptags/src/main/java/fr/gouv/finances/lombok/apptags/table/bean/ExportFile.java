/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.apptags.table.bean;

import java.io.File;
import java.util.Date;

/**
 * Class ExportFile
 */
public class ExportFile
{

    /** id. */
    private Long id;

    /** version. */
    private int version;

    /** Nom du fichier. */
    private String nomFichierOriginal;

    /** Type MIME. */
    private String typeMimeFichier;

    /** Taille. */
    private long tailleFichier;

    /** Date de création du fichier. */
    private Date dateHeureSoumission;

    /** Données contenues dans le fichier. */
    private FileContent leContenuDuFichier = new FileContent();

    /**
     * Flag qui indique si le contenu du fichier est stocké directement dans l'objet ContenuFichier via un tableau de
     * bytes ou si seule la référence à un fichier stocké dans le SF est utilisée.
     */
    private boolean dataInByteArray;

    /**
     * Instanciation de export file.
     */
    public ExportFile()
    {
        super();
    }

    /**
     * Création d'un fichier à partir d'un tableau de bytes.
     *
     * @param data données contenues dans le fichier
     */
    public ExportFile(byte[] data)
    {
        super();
        if (data == null)
        {
            throw new IllegalArgumentException("LE PARAMETRE DATA NE DOIT PAS ETRE NULL.");
        }
        else
        {
            FileContent unContenuFichier = new FileContent();
            this.dataInByteArray = true;
            unContenuFichier.setData(data);
            this.dateHeureSoumission = new Date();
            this.leContenuDuFichier = unContenuFichier;
            this.tailleFichier = unContenuFichier.getData().length;
        }
    }

    /**
     * Création d'un fichier à partir d'un tableau de bytes.
     *
     * @param fichierTemporaire
     */
    public ExportFile(File fichierTemporaire)
    {
        super();
        if (fichierTemporaire == null)
        {
            throw new IllegalArgumentException("LE PARAMETRE FICHIERTEMPORAIRE NE DOIT PAS ETRE NULL.");
        }
        else if (!fichierTemporaire.exists())
        {
            throw new IllegalArgumentException("LE FICHIER " + fichierTemporaire.getAbsolutePath() + " N'EXISTE PAS");
        }
        else if (!(fichierTemporaire.canRead() && fichierTemporaire.canWrite()))
        {
            throw new IllegalArgumentException("LE FICHIER " + fichierTemporaire.getAbsolutePath()
                + " N'EST PAS ACCESSIBLE EN LECTURE / ECRITURE");
        }
        else
        {
            FileContent unContenuFichier = new FileContent();
            this.dataInByteArray = false;
            unContenuFichier.setFichierTemporaire(fichierTemporaire);
            this.dateHeureSoumission = new Date();
            this.leContenuDuFichier = unContenuFichier;
            this.tailleFichier = unContenuFichier.getFichierTemporaire().length();
        }
    }
    
    /**
     * Retourne un objet FichierJoint contenant le tableau de bytes passé en paramètre.
     *
     * @param data
     * @return l unique instance de ExportFile
     */
    public static final ExportFile getInstance(byte[] data)
    {
        ExportFile unFichierJoint = new ExportFile();

        FileContent unContenuFichier = new FileContent();
        unFichierJoint.dataInByteArray = true;
        unContenuFichier.setData(data);
        unFichierJoint.setDateHeureSoumission(new Date());
        unFichierJoint.setLeContenuDuFichier(unContenuFichier);
        unFichierJoint.setTailleFichier(unContenuFichier.getData().length);

        return unFichierJoint;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     *
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
        {
            return true;
        }
        if (obj == null)
        {
            return false;
        }
        if (getClass() != obj.getClass())
        {
            return false;
        }
        final ExportFile other = (ExportFile) obj;
        if (dateHeureSoumission == null)
        {
            if (other.dateHeureSoumission != null)
            {
                return false;
            }
        }
        else if (!dateHeureSoumission.equals(other.dateHeureSoumission))
        {
            return false;
        }
        if (nomFichierOriginal == null)
        {
            if (other.nomFichierOriginal != null)
            {
                return false;
            }
        }
        else if (!nomFichierOriginal.equals(other.nomFichierOriginal))
        {
            return false;
        }
        if (tailleFichier != other.tailleFichier)
        {
            return false;
        }
        if (typeMimeFichier == null)
        {
            if (other.typeMimeFichier != null)
            {
                return false;
            }
        }
        else if (!typeMimeFichier.equals(other.typeMimeFichier))
        {
            return false;
        }
        return true;
    }

    /**
     * Gets the date de création du fichier.
     *
     * @return the date de création du fichier
     */
    public Date getDateHeureSoumission()
    {
        return dateHeureSoumission;
    }

    /**
     * Accesseur de l attribut id.
     *
     * @return id
     */
    public Long getId()
    {
        return id;
    }

    /**
     * Gets the données contenues dans le fichier.
     *
     * @return the données contenues dans le fichier
     */
    public FileContent getLeContenuDuFichier()
    {
        return leContenuDuFichier;
    }

    /**
     * Gets the nom du fichier.
     *
     * @return the nom du fichier
     */
    public String getNomFichierOriginal()
    {
        return nomFichierOriginal;
    }

    /**
     * Gets the taille.
     *
     * @return the taille
     */
    public long getTailleFichier()
    {
        return tailleFichier;
    }

    /**
     * Gets the type MIME.
     *
     * @return the type MIME
     */
    public String getTypeMimeFichier()
    {
        return typeMimeFichier;
    }

    /**
     * Accesseur de l attribut version.
     *
     * @return version
     */
    public int getVersion()
    {
        return version;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     *
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((dateHeureSoumission == null) ? 0 : dateHeureSoumission.hashCode());
        result = prime * result + ((nomFichierOriginal == null) ? 0 : nomFichierOriginal.hashCode());
        result = prime * result + (int) (tailleFichier ^ (tailleFichier >>> 32));
        result = prime * result + ((typeMimeFichier == null) ? 0 : typeMimeFichier.hashCode());
        return result;
    }

    /**
     * Checks if is flag qui indique si le contenu du fichier est stocké directement dans l'objet ContenuFichier via un
     * tableau de bytes ou si seule la référence à un fichier stocké dans le SF est utilisée.
     *
     * @return the flag qui indique si le contenu du fichier est stocké directement dans l'objet ContenuFichier via un
     *         tableau de bytes ou si seule la référence à un fichier stocké dans le SF est utilisée
     */
    public boolean isDataInByteArray()
    {
        return dataInByteArray;
    }

    /**
     * Sets the flag qui indique si le contenu du fichier est stocké directement dans l'objet ContenuFichier via un
     * tableau de bytes ou si seule la référence à un fichier stocké dans le SF est utilisée.
     *
     * @param dataInByteArray the new flag qui indique si le contenu du fichier est stocké directement dans l'objet
     *        ContenuFichier via un tableau de bytes ou si seule la référence à un fichier stocké dans le SF est
     *        utilisée
     */
    public void setDataInByteArray(boolean dataInByteArray)
    {
        this.dataInByteArray = dataInByteArray;
    }

    /**
     * Sets the date de création du fichier.
     *
     * @param dateHeureSoumission the new date de création du fichier
     */
    public void setDateHeureSoumission(Date dateHeureSoumission)
    {
        this.dateHeureSoumission = dateHeureSoumission;
    }

    /**
     * Modificateur de l attribut id.
     *
     * @param id le nouveau id
     */
    public void setId(Long id)
    {
        this.id = id;
    }

    /**
     * Sets the données contenues dans le fichier.
     *
     * @param leContenuDuFichier the new données contenues dans le fichier
     */
    public void setLeContenuDuFichier(FileContent leContenuDuFichier)
    {
        this.leContenuDuFichier = leContenuDuFichier;
    }

    /**
     * Sets the nom du fichier.
     *
     * @param nomFichierOriginal the new nom du fichier
     */
    public void setNomFichierOriginal(String nomFichierOriginal)
    {
        this.nomFichierOriginal = nomFichierOriginal;
    }

    /**
     * Sets the taille.
     *
     * @param tailleFichier the new taille
     */
    public void setTailleFichier(long tailleFichier)
    {
        this.tailleFichier = tailleFichier;
    }

    /**
     * Sets the type MIME.
     *
     * @param typeMimeFichier the new type MIME
     */
    public void setTypeMimeFichier(String typeMimeFichier)
    {
        this.typeMimeFichier = typeMimeFichier;
    }

    /**
     * Modificateur de l attribut version.
     *
     * @param version le nouveau version
     */
    public void setVersion(int version)
    {
        this.version = version;
    }

}
