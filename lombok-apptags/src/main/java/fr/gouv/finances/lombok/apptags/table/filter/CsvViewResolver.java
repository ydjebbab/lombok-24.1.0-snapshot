/*
/*Copyright (c) 2014 DGFiP - Tous droits réservés/* * Copyright 2004 original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package fr.gouv.finances.lombok.apptags.table.filter;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import fr.gouv.finances.lombok.apptags.table.core.Preferences;

/**
 * Class CsvViewResolver
 */
public class CsvViewResolver implements ViewResolver
{

    /**
     * Constructeur de la classe CsvViewResolver.java
     *
     */
    public CsvViewResolver()
    {
        super();
        
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.filter.ViewResolver#resolveView(javax.servlet.ServletRequest,
     *      javax.servlet.ServletResponse, fr.gouv.finances.lombok.apptags.table.core.Preferences, java.lang.Object)
     */
    @Override
    public void resolveView(ServletRequest request, ServletResponse response, Preferences preferences, Object viewData)
        throws Exception
    {
        byte[] contents = ((String) viewData).getBytes("ISO-8859-15");
        response.setContentLength(contents.length);
        response.getOutputStream().write(contents);
    }
}
