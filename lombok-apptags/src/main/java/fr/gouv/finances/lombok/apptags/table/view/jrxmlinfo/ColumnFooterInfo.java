/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.apptags.table.view.jrxmlinfo;

/**
 * Class ColumnFooterInfo
 */
public class ColumnFooterInfo extends AbstractBandInfo
{

    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /**
     * Instanciation de column footer info.
     */
    public ColumnFooterInfo()
    {
        super();
    }

    /** first page frame info. */
    private CalcFrameInfo firstPageFrameInfo = new CalcFrameInfo();

    /** last page frame info. */
    private CalcFrameInfo lastPageFrameInfo = new CalcFrameInfo();

    /** middle page frame info. */
    private CalcFrameInfo middlePageFrameInfo = new CalcFrameInfo();

    /**
     * Accesseur de l attribut first page frame info.
     * 
     * @return first page frame info
     */
    public CalcFrameInfo getFirstPageFrameInfo()
    {
        return firstPageFrameInfo;
    }

    /**
     * Modificateur de l attribut first page frame info.
     * 
     * @param firstPageFrameInfo le nouveau first page frame info
     */
    public void setFirstPageFrameInfo(CalcFrameInfo firstPageFrameInfo)
    {
        this.firstPageFrameInfo = firstPageFrameInfo;
    }

    /**
     * Accesseur de l attribut last page frame info.
     * 
     * @return last page frame info
     */
    public CalcFrameInfo getLastPageFrameInfo()
    {
        return lastPageFrameInfo;
    }

    /**
     * Modificateur de l attribut last page frame info.
     * 
     * @param lastPageFrameInfo le nouveau last page frame info
     */
    public void setLastPageFrameInfo(CalcFrameInfo lastPageFrameInfo)
    {
        this.lastPageFrameInfo = lastPageFrameInfo;
    }

    /**
     * Accesseur de l attribut middle page frame info.
     * 
     * @return middle page frame info
     */
    public CalcFrameInfo getMiddlePageFrameInfo()
    {
        return middlePageFrameInfo;
    }

    /**
     * Modificateur de l attribut middle page frame info.
     * 
     * @param middlePageFrameInfo le nouveau middle page frame info
     */
    public void setMiddlePageFrameInfo(CalcFrameInfo middlePageFrameInfo)
    {
        this.middlePageFrameInfo = middlePageFrameInfo;
    }

}
