/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.apptags.table.callback;

import java.util.ArrayList;
import java.util.Collection;

import org.apache.commons.collections.CollectionUtils;

import fr.gouv.finances.lombok.apptags.table.core.TableModel;

/**
 * Class ProcessRowsCallBackFiltreAvecComparateurInferieurSuperieur
 * 
 * @author lcontinsouzas-cp
 */
public class ProcessRowsCallBackFiltreAvecComparateurInferieurSuperieur extends CpProcessRowsCallBack
{

    /**
     * Instanciation de process rows call back filtre avec comparateur inferieur superieur.
     */
    public ProcessRowsCallBackFiltreAvecComparateurInferieurSuperieur()
    {
        super();
    }

    /*
     * Surcharge pour permettre d'appeler FilterPredicateAvecCompatateurInferieurSuperieur Cette classe doit être
     * @see
     * fr.gouv.finances.lombok.apptags.table.callback.ProcessRowsCallBack#filterRows(fr.gouv.finances.lombok.apptags
     * .table.core.TableModel, java.util.Collection)
     */
    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.callback.CpProcessRowsCallBack#filterRows(fr.gouv.finances.lombok.apptags.table.core.TableModel,
     *      java.util.Collection)
     */
    @Override
    public Collection<Object> filterRows(TableModel model, Collection<Object> rows)
    {
        boolean filtered = model.getLimit().isFiltered();
        boolean cleared = model.getLimit().isCleared();

        if (!filtered || cleared)
        {
            return rows;
        }

        if (filtered)
        {
            Collection<Object> collection = new ArrayList<>();
            FilterPredicateFiltreAvecComparateurInferieurSuperieur filterPredicate =
                new FilterPredicateFiltreAvecComparateurInferieurSuperieur(model);
            CollectionUtils.select(rows, filterPredicate, collection);

            return collection;
        }

        return rows;
    }

}
