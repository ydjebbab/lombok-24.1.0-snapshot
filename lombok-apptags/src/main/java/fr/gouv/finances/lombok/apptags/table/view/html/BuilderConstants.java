/*
 * Copyright (c) 2014 DGFiP - Tous droits réservés
 *//*
/*Copyright (c) 2014 DGFiP - Tous droits réservés/* * Copyright 2004 original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package fr.gouv.finances.lombok.apptags.table.view.html;

/**
 * Class BuilderConstants
 */
public class BuilderConstants
{

    /** Constant : CALC_RESULT_CSS. */
    public static final String CALC_RESULT_CSS = "calcResult";

    // style sheet attributes
    /** Constant : CALC_ROW_CSS. */
    public static final String CALC_ROW_CSS = "calcRow";

    /** Constant : CALC_TITLE_CSS. */
    public static final String CALC_TITLE_CSS = "calcTitle";

    /** Constant : COMPACT_TOOLBAR_CSS. */
    public static final String COMPACT_TOOLBAR_CSS = "compactToolbar";

    /** Constant : FILTER_BUTTONS_CSS. */
    public static final String FILTER_BUTTONS_CSS = "filterButtons";

    /** Constant : FILTER_CSS. */
    public static final String FILTER_CSS = "filter";

    /** Constant : ROW_EVEN_CSS. */
    public static final String ROW_EVEN_CSS = "even";

    /** Constant : ROW_HIGHLIGHT_CSS. */
    public static final String ROW_HIGHLIGHT_CSS = "highlight";

    /** Constant : ROW_ODD_CSS. */
    public static final String ROW_ODD_CSS = "odd";

    /** Constant : SORT_ASC_IMAGE. */
    public static final String SORT_ASC_IMAGE = "sortAsc";

    /** Constant : SORT_DESC_IMAGE. */
    public static final String SORT_DESC_IMAGE = "sortDesc";

    /** Constant : STATUS_BAR_CSS. */
    public static final String STATUS_BAR_CSS = "statusBar";

    /** Constant : STATUSBAR_NO_RESULTS_FOUND. */
    public static final String STATUSBAR_NO_RESULTS_FOUND = "statusbar.noResultsFound";

    // text statusbar messages
    /** Constant : STATUSBAR_RESULTS_FOUND. */
    public static final String STATUSBAR_RESULTS_FOUND = "statusbar.resultsFound";

    /** Constant : STATUSBARNORESULTSFOUND_CSS. */
    public static final String STATUSBARNORESULTSFOUND_CSS = "statusbarnoresultsfound";

    // id attributes
    /** Constant : TABLE. */
    public static final String TABLE = "table";    
 
    /** Constant : TABLEFILTER. */
    public static final String TABLEFILTER = "table_filter";    
 
    /** Constant : TABLEDATA. */
    public static final String TABLEDATA = "table_data";

    /** Constant : TABLE_BODY_CSS. */
    public static final String TABLE_BODY_CSS = "tableBody";

    /** Constant : TABLE_HEADER_CSS. */
    public static final String TABLE_HEADER_CSS = "tableHeader";

    /** Constant : TABLE_HEADER_SORT_CSS. */
    public static final String TABLE_HEADER_SORT_CSS = "tableHeaderSort";

    /** Constant : TABLE_REGION_CSS. */
    public static final String TABLE_REGION_CSS = "tableRegion";

    /** Constant : TITLE_CSS. */
    public static final String TITLE_CSS = "title";

    /** Constant : TITLE_ROW_CSS. */
    public static final String TITLE_ROW_CSS = "titleRow";

    /** Constant : TOOLBAR_CLEAR_FILTER_IMAGE. */
    public static final String TOOLBAR_CLEAR_FILTER_IMAGE = "clear_filter";

    /** Constant : TOOLBAR_CLEAR_FILTER_TEXT. */
    public static final String TOOLBAR_CLEAR_FILTER_TEXT = "toolbar.text.clear.filter";

    // image names
    /** Constant : TOOLBAR_CLEAR_IMAGE. */
    public static final String TOOLBAR_CLEAR_IMAGE = "clear";

    /** Constant : TOOLBAR_CLEAR_TEXT. */
    public static final String TOOLBAR_CLEAR_TEXT = "toolbar.text.clear";

    /** Constant : TOOLBAR_CLEAR_TOOLTIP. */
    public static final String TOOLBAR_CLEAR_TOOLTIP = "toolbar.tooltip.clear";

    /** Constant : TOOLBAR_CSS. */
    public static final String TOOLBAR_CSS = "toolbar";

    /** Constant : TOOLBAR_CSV_TEXT. */
    public static final String TOOLBAR_CSV_TEXT = "toolbar.text.csv";

    /** Constant : TOOLBAR_CSV_TOOLTIP. */
    public static final String TOOLBAR_CSV_TOOLTIP = "toolbar.tooltip.csv";

    /** Constant : TOOLBAR_FILTER_ARROW_IMAGE. */
    public static final String TOOLBAR_FILTER_ARROW_IMAGE = "filterArrow";

    /** Constant : TOOLBAR_FILTER_ARROW_TEXT. */
    public static final String TOOLBAR_FILTER_ARROW_TEXT = "toolbar.text.arrow";

    /** Constant : TOOLBAR_FILTER_IMAGE. */
    public static final String TOOLBAR_FILTER_IMAGE = "filter";

    /** Constant : TOOLBAR_FILTER_TEXT. */
    public static final String TOOLBAR_FILTER_TEXT = "toolbar.text.filter";

    /** Constant : TOOLBAR_FILTER_TOOLTIP. */
    public static final String TOOLBAR_FILTER_TOOLTIP = "toolbar.tooltip.filter";

    /** Constant : TOOLBAR_FIRST_PAGE_DISABLED_IMAGE. */
    public static final String TOOLBAR_FIRST_PAGE_DISABLED_IMAGE = "firstPageDisabled";

    /** Constant : TOOLBAR_FIRST_PAGE_IMAGE. */
    public static final String TOOLBAR_FIRST_PAGE_IMAGE = "firstPage";

    // text messages
    /** Constant : TOOLBAR_FIRST_PAGE_TEXT. */
    public static final String TOOLBAR_FIRST_PAGE_TEXT = "toolbar.text.firstPage";

    // text tooltip messages
    /** Constant : TOOLBAR_FIRST_PAGE_TOOLTIP. */
    public static final String TOOLBAR_FIRST_PAGE_TOOLTIP = "toolbar.tooltip.firstPage";

    /** Constant : TOOLBAR_LAST_PAGE_DISABLED_IMAGE. */
    public static final String TOOLBAR_LAST_PAGE_DISABLED_IMAGE = "lastPageDisabled";

    /** Constant : TOOLBAR_LAST_PAGE_IMAGE. */
    public static final String TOOLBAR_LAST_PAGE_IMAGE = "lastPage";

    /** Constant : TOOLBAR_LAST_PAGE_TEXT. */
    public static final String TOOLBAR_LAST_PAGE_TEXT = "toolbar.text.lastPage";

    /** Constant : TOOLBAR_LAST_PAGE_TOOLTIP. */
    public static final String TOOLBAR_LAST_PAGE_TOOLTIP = "toolbar.tooltip.lastPage";

    /** Constant : TOOLBAR_NEXT_PAGE_DISABLED_IMAGE. */
    public static final String TOOLBAR_NEXT_PAGE_DISABLED_IMAGE = "nextPageDisabled";

    /** Constant : TOOLBAR_NEXT_PAGE_IMAGE. */
    public static final String TOOLBAR_NEXT_PAGE_IMAGE = "nextPage";

    /** Constant : TOOLBAR_NEXT_PAGE_TEXT. */
    public static final String TOOLBAR_NEXT_PAGE_TEXT = "toolbar.text.nextPage";

    /** Constant : TOOLBAR_NEXT_PAGE_TOOLTIP. */
    public static final String TOOLBAR_NEXT_PAGE_TOOLTIP = "toolbar.tooltip.nextPage";

    /** Constant : TOOLBAR_ODS_TEXT. */
    public static final String TOOLBAR_ODS_TEXT = "toolbar.text.ods";

    /** Constant : TOOLBAR_ODS_TOOLTIP. */
    public static final String TOOLBAR_ODS_TOOLTIP = "toolbar.tooltip.ods";

    /** Constant : TOOLBAR_PDF_TEXT. */
    public static final String TOOLBAR_PDF_TEXT = "toolbar.text.pdf";

    /** Constant : TOOLBAR_PDF_TOOLTIP. */
    public static final String TOOLBAR_PDF_TOOLTIP = "toolbar.tooltip.pdf";

    /** Constant : TOOLBAR_PREV_PAGE_DISABLED_IMAGE. */
    public static final String TOOLBAR_PREV_PAGE_DISABLED_IMAGE = "prevPageDisabled";

    /** Constant : TOOLBAR_PREV_PAGE_IMAGE. */
    public static final String TOOLBAR_PREV_PAGE_IMAGE = "prevPage";

    /** Constant : TOOLBAR_PREV_PAGE_TEXT. */
    public static final String TOOLBAR_PREV_PAGE_TEXT = "toolbar.text.prevPage";

    /** Constant : TOOLBAR_PREV_PAGE_TOOLTIP. */
    public static final String TOOLBAR_PREV_PAGE_TOOLTIP = "toolbar.tooltip.prevPage";

    /** Constant : TOOLBAR_ROWS_DISPLAYED_IMAGE. */
    public static final String TOOLBAR_ROWS_DISPLAYED_IMAGE = "rowsDisplayed";

    /** Constant : TOOLBAR_ROWS_DISPLAYED_TEXT. */
    public static final String TOOLBAR_ROWS_DISPLAYED_TEXT = "toolbar.text.rowsDisplayed";

    /** Constant : TOOLBAR_SEPARATOR_CSS. */
    public static final String TOOLBAR_SEPARATOR_CSS = "separator";

    /** Constant : TOOLBAR_SEPARATOR_IMAGE. */
    public static final String TOOLBAR_SEPARATOR_IMAGE = "separator";

    /** Constant : TOOLBAR_SEPARATOR_TEXT. */
    public static final String TOOLBAR_SEPARATOR_TEXT = "toolbar.text.separator";

    /** Constant : TOOLBAR_XLS_TEXT. */
    public static final String TOOLBAR_XLS_TEXT = "toolbar.text.xls";

    /** Constant : TOOLBAR_XLS_TOOLTIP. */
    public static final String TOOLBAR_XLS_TOOLTIP = "toolbar.tooltip.xls";

    /** Constant : TOOLBAR_XML_TEXT. */
    public static final String TOOLBAR_XML_TEXT = "toolbar.text.xml";

    /** Constant : TOOLBAR_XML_TOOLTIP. */
    public static final String TOOLBAR_XML_TOOLTIP = "toolbar.tooltip.xml";

    /**
     * Instanciation de builder constants.
     */
    private BuilderConstants()
    {
    }
}
