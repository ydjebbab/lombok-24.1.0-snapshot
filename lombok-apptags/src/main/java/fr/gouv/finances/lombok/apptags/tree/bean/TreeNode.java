/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : TreeNode.java
 *
 */
package fr.gouv.finances.lombok.apptags.tree.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Class TreeNode --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.3 $ Date: 11 déc. 2009
 */
public class TreeNode implements Serializable
{

    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** name. */
    protected String name = "";

    /** id. */
    protected String id = "";

    /** parent id. */
    protected String parentId = "";

    /** type. */
    protected String type = "";

    /** folder. */
    protected boolean folder = true;

    /** tool tip. */
    protected String toolTip = "";

    /** expanded image url. */
    protected String expandedImageUrl = "";

    /** collapsed image url. */
    protected String collapsedImageUrl = "";

    /** object. */
    protected Serializable object = null;

    /** children. */
    protected List<TreeNode> children = new ArrayList<TreeNode>();

    /** parent. */
    protected TreeNode parent = null;

    /** Flag qui indique si le noeud posssède des fils non chargés. */
    protected Boolean hasUnloadedChildren = Boolean.TRUE;

    /** Préfixe utilisé pour assurer l'unicité des id au sein de l'arbre. */
    protected String prefixNodeId = "";

    /** id identifiant l'objet dans le modèle mappé sur l'arbre. */
    protected String objectId;

    /**
     * Instanciation de tree node.
     */
    public TreeNode()
    {
        // RAS
    }

    /**
     * Instanciation de tree node.
     * 
     * @param idd --
     * @param name --
     */
    public TreeNode(String idd, String name)
    {
        this.id = idd;
        this.name = name;
    }

    /**
     * Instanciation de tree node.
     * 
     * @param idd --
     * @param name --
     * @param type --
     * @param prefixNodeId --
     */
    public TreeNode(String idd, String name, String type, String prefixNodeId)
    {
        this(idd, name);
        this.prefixNodeId = prefixNodeId;
        this.type = type;
    }

    /**
     * Instanciation de tree node.
     * 
     * @param idd --
     * @param name --
     * @param type --
     * @param prefixNodeId --
     * @param parentNode --
     */
    public TreeNode(String idd, String name, String type, String prefixNodeId, TreeNode parentNode)
    {
        this(idd, name, type, prefixNodeId);
        this.parent = parentNode;
    }

    /**
     * Instanciation de tree node.
     * 
     * @param idd --
     * @param name --
     * @param parentNode --
     */
    public TreeNode(String idd, String name, TreeNode parentNode)
    {
        this(idd, name);
        this.parent = parentNode;

    }

    /**
     * Instanciation de tree node.
     * 
     * @param parentNode --
     */
    public TreeNode(TreeNode parentNode)
    {
        this.parent = parentNode;
    }

    /**
     * Ajoute un fils à ce noeud comme n'eme noeud fils, n étant le paramètre index. Positionne sur le noeud fils la
     * référence au noeud parent en appelant <code>child.setParentOnly(this);</code>
     * 
     * @param index Index d'ajout du noeud fils
     * @param node Le noeud fils à ajouter
     */
    public void addChild(int index, TreeNode node)
    {
        addChildOnly(index, node);
        node.setParentOnly(this);
    }

    /**
     * Ajoute un fils à ce noeud et positionne sur le noeud fils la référence au noeud parent en appelant
     * <code>child.setParentOnly(this);</code>
     * 
     * @param node Le noeud fils à ajouter
     */
    public void addChild(TreeNode node)
    {
        addChildOnly(node);
        node.setParentOnly(this);
    }

    /**
     * Ajoute un fils à ce noeud comme n'eme noeud fils, n étant le paramètre index, sans affecter au noeud fils la
     * référence vers le noeud parent.
     * 
     * @param index Index d'ajout du noeud fils
     * @param node Le noeud fils à ajouter
     */
    public void addChildOnly(int index, TreeNode node)
    {
        if (this == node)
        {
            throw new IllegalArgumentException("UN TreeNode NE PEUT ETRE SON PROPRE FILS");
        }
        this.children.add(index, node);
    }

    /**
     * Ajoute un fils à ce noeud, sans affecter au noeud fils la référence vers le noeud parent.
     * 
     * @param node Le noeud fils à ajouter
     */
    public void addChildOnly(TreeNode node)
    {
        if (this == node)
        {
            throw new IllegalArgumentException("UN TreeNode NE PEUT ETRE SON PROPRE FILS");
        }
        this.children.add(node);
    }

    /**
     * methode Copy : --.
     * 
     * @param node --
     */
    public void copy(TreeNode node)
    {
        setParentId(node.getParentId());
        setCollapsedImageUrl(node.getCollapsedImageUrl());
        setExpandedImageUrl(node.getExpandedImageUrl());
        setName(node.getName());
        setObject(node.getObject());
        setToolTip(node.getToolTip());
        setType(node.getType());
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param ooo
     * @return true, si c'est vrai
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object ooo)
    {
        boolean result = true;
        if (ooo == null)
        {
            result = false;
        }
        if (!(ooo instanceof TreeNode))
        {
            result = false;
        }

        TreeNode node = (TreeNode) ooo;
        if (!this.id.equals(node.getId()))
        {
            result = false;
        }

        return result;
    }

    /**
     * Retourne le nombre d'ancêtre de ce noeud.
     * 
     * @return int Nombre d'ancêtre
     */
    public int getAncestorCount()
    {
        int ancestorCount = 0;
        TreeNode ancestor = this.getParent();

        while (ancestor != null)
        {
            ancestorCount++;
            ancestor = ancestor.getParent();
        }
        return ancestorCount;
    }

    /**
     * Retourne le nombre de fils de ce noeud.
     * 
     * @return int Nombre de fils
     */
    public int getChildCount()
    {
        return this.children.size();
    }

    /**
     * retourne la liste des fils de ce noeud.
     * 
     * @return Liste des fils d'un noeud
     */
    public List<TreeNode> getChildren()
    {
        return this.children;
    }

    /**
     * Retourne l'url de l'image de ce noeud lorsqu'il est dans l'état 'collapsed', si elle est positionnée. Le moyen
     * par défaut de positionnement des images associées aux noeuds est d'utiliser le tag tree:nodeMatch qui n'utilise
     * pas l'url de l'image du noeud. Pour utiliser ces urls d'image, il faut afficher l'image à l'aide d'un tag
     * standard HTML img et le tag tree:collapsedImageUrl
     * 
     * @return L'url de l'image du noeud si elle est positionnée
     */
    public String getCollapsedImageUrl()
    {
        return collapsedImageUrl;
    }

    /**
     * Retourne le nombre de descendant de ce noeud.
     * 
     * @return int Nombre de descendant
     */
    public int getDescendantCount()
    {
        int descendantCount = this.children.size();

        for (TreeNode node : this.children)
        {
            if (node.hasChildren())
            {
                descendantCount += node.getDescendantCount();
            }
        }
        return descendantCount;
    }

    /**
     * Retourne l'url de l'image de ce noeud lorsqu'il est dans l'état 'expanded', si elle est positionnée. Le moyen par
     * défaut de positionnement des images associées aux noeuds est d'utiliser le tag tree:nodeMatch qui n'utilise pas
     * l'url de l'image du noeud. Pour utiliser ces urls d'image, il faut afficher l'image à l'aide d'un tag standard
     * HTML img et le tag tree:expandedImageUrl
     * 
     * @return L'url de l'image du noeud si elle est positionnée
     */
    public String getExpandedImageUrl()
    {
        return expandedImageUrl;
    }

    /**
     * Retourne true si le noeud dispose de fils non chargés.
     * 
     * @return the checks for unloaded children
     */
    public Boolean getHasUnloadedChildren()
    {
        return hasUnloadedChildren;
    }

    /**
     * Assigne le nom du noeud, qui sera généralement affiché par le tag tree.
     * 
     * @return the id
     */
    public String getId()
    {
        return this.id;
    }

    /**
     * Retourne le nom du noeud. Ce nom est généralement affiché par le tag tree comme le texte du noeud
     * 
     * @return Le nom du noeud
     */
    public String getName()
    {
        return this.name;
    }

    /**
     * Retourne l'objet attaché. Un objet attaché peut être pratique pour afficher des informations relatives à cet
     * objet de l'arbre dans une page JSP
     * 
     * @return L'objet attaché à ce noeud de l'arbre
     */
    public Serializable getObject()
    {
        return this.object;
    }

    /**
     * Retourne l'identifiant de l'objet associé au noeud.
     * 
     * @return the object id
     */
    public String getObjectId()
    {
        return this.objectId;
    }

    /**
     * Retourne le noeud parent de ce noeud, si ce noeud est le fils d'un autre noeud, sinon retourne null.
     * 
     * @return le noeud parent de ce noeud ou null si le noeud ne possède pas de parent
     */
    public TreeNode getParent()
    {
        return this.parent;
    }

    /**
     * Retourne l'id du noeud parent du noeud courant. Cette id est rarement utilisé directement par le programmeur ou
     * lors de l'affichage du noeud. Il est par contre utile lors de la connexion de bnoeuds fils à leur parent dans les
     * phases de chargement de l'implémentation Itreedao. Parfois les endants de pluieurs noeuds sont lus simultanément
     * et la seule façon de relier des noeuds à leur parent est de faire porter par des noeuds enfants l'id de leur
     * parent
     * 
     * @return L'id du noeud parent
     */
    public String getParentId()
    {
        return parentId;
    }

    /**
     * Retourne le préfixe placé l'Id pour assurer l'unicité des id entre les différents niveaux de l'arbre.
     * 
     * @return the prefix node id
     */
    public String getPrefixNodeId()
    {
        return this.prefixNodeId;
    }

    /**
     * Retourne l'info bulle de ce noeud. Les info bulles peuvent par exemple est affichées par le navigateur quand la
     * souris survole une image ou un lien
     * 
     * @return L'info bullle de ce noeud
     */
    public String getToolTip()
    {
        return toolTip;
    }

    /**
     * Retourne le type du noeud. Le type est normalement utilisé pour déterminer l'icone utilisée pour afficher le
     * noeud
     * 
     * @return Le type du noeud
     */
    public String getType()
    {
        return this.type;
    }

    /**
     * Retourne 'true' si ce noeud possède des enfants.
     * 
     * @return True si le noeud possède des enfants, sinon false
     */
    public boolean hasChildren()
    {
        boolean result = false;
        if (this.hasUnloadedChildren)
        {
            result = true;
        }
        else
        {
            result = (this.getChildren().size() > 0);
        }
        return result;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @return int
     * @see java.lang.Object#hashCode()
     */
    public int hashCode()
    {
        return this.id.hashCode();
    }

    /**
     * Retourne true si le noeud représente un répertoire.
     * 
     * @return true, if checks if is folder
     */
    public boolean isFolder()
    {
        return folder;
    }

    /**
     * Removes all children from this node. This will also remove the parent node of each of the removed nodes. Retire
     * tous les fils de ce noeud et efface la référence au noeud parent dans chaque noeud fils
     */
    public void removeAllChildren()
    {
        for (TreeNode node : this.children)
        {
            if (node != null)
            {
                node.setParentOnly(null);
            }
        }
        this.children.clear();
    }

    /**
     * Retire un noeud fils à ce noeud. Efface la référence au noeud parent dans le noeud fils, en appelant
     * <code>child.setParentOnly(null);</code>
     * 
     * @param node Le Noeud fils à retirer
     */
    public void removeChild(TreeNode node)
    {
        this.children.remove(node);
        node.setParentOnly(null);
    }

    /**
     * Retire un noeud fils à ce noeud, sans effacer la référence au noeud parent dans le noeud fils.
     * 
     * @param node Le Noeud fils à retirer
     */
    public void removeChildOnly(TreeNode node)
    {
        this.children.remove(node);
    }

    /**
     * Positionne l'url de l'image de ce noeud lorsqu'il est dans l'état 'collapsed', si elle est positionnée. Le moyen
     * par défaut de positionnement des images associées aux noeuds est d'utiliser le tag tree:nodeMatch qui n'utilise
     * pas l'url de l'image du noeud. Pour utiliser ces urls d'image, il faut afficher l'image à l'aide d'un tag
     * standard HTML img et le tag tree:collapsedImageUrl
     * 
     * @param collapsedImageUrl --
     */

    public void setCollapsedImageUrl(String collapsedImageUrl)
    {
        this.collapsedImageUrl = collapsedImageUrl;
    }

    /**
     * Positionne l'url de l'image de ce noeud lorsqu'il est dans l'état 'expanded', si elle est positionnée. Le moyen
     * par défaut de positionnement des images associées aux noeuds est d'utiliser le tag tree:nodeMatch qui n'utilise
     * pas l'url de l'image du noeud. Pour utiliser ces urls d'image, il faut afficher l'image à l'aide d'un tag
     * standard HTML img et le tag tree:expandedImageUrl
     * 
     * @param expandedImageUrl --
     */
    public void setExpandedImageUrl(String expandedImageUrl)
    {
        this.expandedImageUrl = expandedImageUrl;
    }

    /**
     * Indique si le noeud représente ou non un répertoire.
     * 
     * @param folder --
     */
    public void setFolder(boolean folder)
    {
        this.folder = folder;
    }

    /**
     * Indique si le noeud dispose de fils non chargés.
     * 
     * @param hasUnloadedChildren --
     */
    public void setHasUnloadedChildren(Boolean hasUnloadedChildren)
    {
        this.hasUnloadedChildren = hasUnloadedChildren;
    }

    /**
     * Affecte l'id du noeud en concaténant le préfixe utilisé pour ce type de noeud et l'identifiant de l'objet associé
     * au noeud.
     * 
     * @param idd identifiant de l'objet associé au noeud
     */
    public void setId(String idd)
    {
        this.objectId = idd;
        this.id = new StringBuilder().append(this.prefixNodeId).append(this.objectId).toString();
    }

    /**
     * Assigne le nom du noeud, qui sera généralement affiché par le tag tree.
     * 
     * @param name Le nom du noeud
     */
    public void setName(String name)
    {
        this.name = name;
    }

    /**
     * Atatche un objet à ce noeud. Un objet attaché peut être pratique pour afficher des informations relatives à cet
     * objet de l'arbre dans une page JSP
     * 
     * @param object L'objet à attacher à ce noeud
     */
    public void setObject(Serializable object)
    {
        this.object = object;
    }

    /**
     * Positionne le noeud parent de ce noeud <br/>
     * <br/>
     * Le noeud appela la méthode <code>parent.addChildOnly(this)</code>, si le noeud n'est pas déjà un fils du nouveau
     * parent <br/>
     * <br/>
     * Si cette méthode est appelée avec un paramètre non null et un parent qui n'est pas le même que le parent courant
     * du noeud, alors le parent courant sera retiré du noeud par la méthode <code>parent.removeChildOnly(this)</code>
     * 
     * @param newParent --
     */
    public void setParent(TreeNode newParent)
    {
        if (this == newParent)
        {
            throw new IllegalArgumentException("Un 'TreeNode' ne peut pas être son propre parent");
        }

        if (shouldRemoveSelfFromOldParent(newParent))
        {
            this.parent.removeChildOnly(this);
        }

        if (shouldAddSelfToNewParent(newParent))
        {
            newParent.addChildOnly(this);
        }

        this.parent = newParent;
        this.setParentId(this.parent.getId());
    }

    /**
     * Positionne l'id du noeud parent du noeud courant.
     * 
     * @param parentId --
     */
    public void setParentId(String parentId)
    {
        this.parentId = parentId;
    }

    /**
     * Positionne le noeud parent de ce noeud, sans appeler les méthodes <code>parent.addChild()</code>
     * <code>parent.addChildOnly()</code>
     * 
     * @param newParent --
     */
    public void setParentOnly(TreeNode newParent)
    {
        this.parent = newParent;
    }

    /**
     * Définit le préfixe à placer devant l'Id pour assurer l'unicité des id entre les différents niveaux de l'arbre Met
     * à jour l'id du noeud en concaténant le préfixe et l'id de l'objet associé au noeud.
     * 
     * @param prefixId --
     */
    public void setPrefixNodeId(String prefixId)
    {
        this.prefixNodeId = prefixId;
        this.id = new StringBuilder().append(this.prefixNodeId).append(this.objectId).toString();
    }

    /**
     * Positionne l'info bulle de ce noeud. Les info bulles peuvent par exemple est affichées par le navigateur quand la
     * souris survole une image ou un lien
     * 
     * @param text L'info bullle de ce noeud
     */
    public void setToolTip(String text)
    {
        this.toolTip = text;
    }

    /**
     * Positionne le type du noeud. Le type est normalement utilisé pour déterminer l'icone utilisée pour afficher le
     * noeud
     * 
     * @param type Le type du noeud
     */
    public void setType(String type)
    {
        this.type = type;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @return string
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        StringBuilder   buffer = new StringBuilder(30);
        buffer.append("( id: ");
        buffer.append(getId());
        buffer.append(",    name: ");
        buffer.append(getName());
        buffer.append(",    type: ");
        buffer.append(getType());
        buffer.append(')');

        return buffer.toString();
    }

    /**
     * methode Should add self to new parent : --.
     * 
     * @param newParentNode --
     * @return true, si c'est vrai
     */
    private boolean shouldAddSelfToNewParent(TreeNode newParentNode)
    {
        return ((newParentNode != null) && (!newParentNode.getChildren().contains(this)));
    }

    /**
     * methode Should remove self from old parent : --.
     * 
     * @param newParentNode --
     * @return true, si c'est vrai
     */
    private boolean shouldRemoveSelfFromOldParent(TreeNode newParentNode)
    {
        return ((this.parent != null) && (this.parent != newParentNode));
    }

}
