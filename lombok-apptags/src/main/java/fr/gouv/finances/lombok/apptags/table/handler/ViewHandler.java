/*
/*Copyright (c) 2014 DGFiP - Tous droits réservés/* * Copyright 2004 original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package fr.gouv.finances.lombok.apptags.table.handler;

import org.apache.commons.lang.StringUtils;

import fr.gouv.finances.lombok.apptags.table.bean.Column;
import fr.gouv.finances.lombok.apptags.table.cell.Cell;
import fr.gouv.finances.lombok.apptags.table.core.PreferencesConstants;
import fr.gouv.finances.lombok.apptags.table.core.TableModel;
import fr.gouv.finances.lombok.apptags.table.core.TableModelUtils;
import fr.gouv.finances.lombok.apptags.table.view.View;

/**
 * Class ViewHandler
 */
public class ViewHandler
{

    /** model. */
    private TableModel model;

    /** view. */
    private View view;

    /**
     * Instanciation de view handler.
     * 
     * @param model
     */
    public ViewHandler(TableModel model)
    {
        this.model = model;
    }

    /**
     * Accesseur de l attribut view.
     * 
     * @return view
     */
    public View getView()
    {
        return view;
    }

    /**
     * methode Sets the view :
     * 
     * @throws Exception the exception
     */
    public void setView() throws Exception
    {
        boolean isExported = model.getLimit().isExported();

        String currentView = null;
        if (isExported)
        {
            currentView = model.getExportHandler().getCurrentExport().getView();
            String preference = model.getPreferences().getPreference(PreferencesConstants.EXPORT_VIEW + currentView);
            if (StringUtils.isNotBlank(preference))
            {
                currentView = preference;
            }
        }
        else
        {
            currentView = model.getTableHandler().getTable().getView();
            String preference = model.getPreferences().getPreference(PreferencesConstants.TABLE_VIEW + currentView);
            if (StringUtils.isNotBlank(preference))
            {
                currentView = preference;
            }
        }

        Class classDefinition = Class.forName(currentView);
        this.view = (View) classDefinition.newInstance();
        getView().beforeBody(model);
    }

    /**
     * methode Adds the column value to view :
     * 
     * @param column
     */
    public void addColumnValueToView(Column column)
    {
        Cell cell = TableModelUtils.getCell(column);

        boolean isExported = model.getLimit().isExported();
        if (!isExported)
        {
            column.setCellDisplay(cell.getHtmlDisplay(model, column));
        }
        else
        {
            column.setCellDisplay(cell.getExportDisplay(model, column));
        }

        getView().body(model, column);
    }
}
