/*
 * Copyright (c) 2014 DGFiP - Tous droits réservés
 */
/*
 /*Copyright (c) 2014 DGFiP - Tous droits réservés/* * Copyright 2004 original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package fr.gouv.finances.lombok.apptags.table.view;

import fr.gouv.finances.lombok.apptags.table.core.Messages;
import fr.gouv.finances.lombok.apptags.table.core.TableModel;
import fr.gouv.finances.lombok.apptags.table.view.html.BuilderConstants;
import fr.gouv.finances.lombok.apptags.table.view.html.BuilderUtils;
import fr.gouv.finances.lombok.apptags.table.view.html.StatusBarBuilder;
import fr.gouv.finances.lombok.apptags.table.view.html.ToolbarBuilder;
import fr.gouv.finances.lombok.apptags.table.view.html.TwoColumnRowLayout;
import fr.gouv.finances.lombok.apptags.util.HtmlBuilder;

/**
 * Class DefaultStatusBar
 */
public class DefaultStatusBar extends TwoColumnRowLayout
{

    /** messages. */
    private Messages messages;

    /**
     * Instanciation de default status bar.
     * 
     * @param html
     * @param model
     */
    public DefaultStatusBar(HtmlBuilder html, TableModel model)
    {
        super(html, model);
        messages = model.getMessages();
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.view.html.TwoColumnRowLayout#showLayout(fr.gouv.finances.lombok.apptags.table.core.TableModel)
     */
    @Override
    protected boolean showLayout(TableModel model)
    {
        boolean showStatusBar = BuilderUtils.showStatusBar(model);
        boolean filterable = BuilderUtils.filterable(model);
        if (!showStatusBar && !filterable)
        {
            return false;
        }

        return true;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.view.html.TwoColumnRowLayout#columnLeft(fr.gouv.finances.lombok.apptags.util.HtmlBuilder,
     *      fr.gouv.finances.lombok.apptags.table.core.TableModel)
     */
    @Override
    protected void columnLeft(HtmlBuilder html, TableModel model)
    {
        boolean showStatusBar = BuilderUtils.showStatusBar(model);
        if (!showStatusBar)
        {
            return;
        }

        html.td(4).styleClass(BuilderConstants.STATUS_BAR_CSS).close();

        new StatusBarBuilder(html, model).statusMessage();

        if (BuilderUtils.showNewPagination(model))
        {
            html.append(" | ");
            html.append(messages.getMessage(BuilderConstants.TOOLBAR_ROWS_DISPLAYED_TEXT) + ": ");
            new ToolbarBuilder(html, model).rowsDisplayedDroplist(); // ADDED

        }

        html.tdEnd();
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.view.html.TwoColumnRowLayout#columnLeft(fr.gouv.finances.lombok.apptags.util.HtmlBuilder,
     *      fr.gouv.finances.lombok.apptags.table.core.TableModel)
     */
    @Override
    protected void columnLeftRWD(HtmlBuilder html, TableModel model)
    {
        boolean showStatusBar = BuilderUtils.showStatusBar(model);
        if (!showStatusBar)
        {
            return;
        }

        html.div().styleClass(BuilderConstants.STATUS_BAR_CSS).close();

        new StatusBarBuilder(html, model).statusMessage();

        if (BuilderUtils.showNewPagination(model))
        {
            html.append(" | ");
            html.append(messages.getMessage(BuilderConstants.TOOLBAR_ROWS_DISPLAYED_TEXT) + ": ");
            new ToolbarBuilder(html, model).rowsDisplayedDroplist(); // ADDED

        }

        html.divEnd();
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.view.html.TwoColumnRowLayout#columnRight(fr.gouv.finances.lombok.apptags.util.HtmlBuilder,
     *      fr.gouv.finances.lombok.apptags.table.core.TableModel)
     */
    @Override
    protected void columnRight(HtmlBuilder html, TableModel model)
    {
        boolean filterable = BuilderUtils.filterable(model);
        boolean showNewFilter = BuilderUtils.showNewFilter(model);
        ToolbarBuilder toolbarBuilder = new ToolbarBuilder(html, model);

        html.td(4).styleClass(BuilderConstants.FILTER_BUTTONS_CSS).close();

        if (!filterable)
        {
            return;
        }

        if (showNewFilter)
        {
            toolbarBuilder.filterItemAsButton();

            html.nbsp();

            toolbarBuilder.clearItemAsButton();
        }
        else
        {

            html.img();
            html.src(BuilderUtils.getImage(model, BuilderConstants.TOOLBAR_FILTER_ARROW_IMAGE));
            html.alt(messages.getMessage(BuilderConstants.TOOLBAR_FILTER_TEXT));
            html.style("border:0");
            html.xclose();
            html.nbsp();

            toolbarBuilder.filterItemAsImage();

            html.nbsp();

            toolbarBuilder.clearItemAsImage();
        }

        html.tdEnd();
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.view.html.TwoColumnRowLayout#columnRight(fr.gouv.finances.lombok.apptags.util.HtmlBuilder,
     *      fr.gouv.finances.lombok.apptags.table.core.TableModel)
     */
    @Override
    protected void columnRightRWD(HtmlBuilder html, TableModel model)
    {
        boolean filterable = BuilderUtils.filterable(model);
        boolean showNewFilter = BuilderUtils.showNewFilter(model);
        ToolbarBuilder toolbarBuilder = new ToolbarBuilder(html, model);

        html.div().styleClass(BuilderConstants.FILTER_BUTTONS_CSS).close();

        if (filterable)
        {
            toolbarBuilder.filterItemAsButton();
            html.nbsp();
            toolbarBuilder.clearItemAsButton();
            html.divEnd();
        }

    }

}
