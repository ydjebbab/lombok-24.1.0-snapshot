/*
 * Copyright (c) 2011 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.apptags.table.tag;

import java.util.Iterator;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import javax.servlet.jsp.tagext.TryCatchFinally;

import fr.gouv.finances.lombok.apptags.table.bean.Table;
import fr.gouv.finances.lombok.apptags.table.context.JspPageContext;
import fr.gouv.finances.lombok.apptags.table.core.TableConstants;
import fr.gouv.finances.lombok.apptags.table.core.TableModel;
import fr.gouv.finances.lombok.apptags.table.core.TableModelImpl;
import fr.gouv.finances.lombok.apptags.table.interceptor.TableInterceptor;

/**
 * Class TableTag
 */
public class TableTag extends TagSupport implements TryCatchFinally, TableInterceptor
{

    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** action. */
    private String action;

    /** auto include parameters. */
    private String autoIncludeParameters;

    /** border. */
    private String border;

    /** buffer view. */
    private String bufferView;

    /** cellpadding. */
    private String cellpadding;

    /** cellspacing. */
    private String cellspacing;

    /** compact. */
    private String compact;

    /** filterable. */
    private String filterable;

    /** filter rows callback. */
    private String filterRowsCallback;

    /** form. */
    private String form;

    /** image path. */
    private String imagePath;

    /** interceptor. */
    private String interceptor;

    /** items. */
    private Object items;

    /** locale. */
    private String locale;

    /** method. */
    private String method;

    /** on invoke action. */
    private String onInvokeAction;

    /** responsive */
    private String showResponsive;

    /** dataAttributes */
    private String dataAttributes;
   
    /** paginationClass */
    private String paginationClass;
    
    /** exportsClass */
    private String exportsClass;
    
    /** retrieve rows callback. */
    private String retrieveRowsCallback;

    /** rows displayed. */
    private String rowsDisplayed;

    /** rows displayed. */
    private String medianRowsDisplayed;
    
    /** rows displayed. */
    private String maxRowsDisplayed;

    /** scope. */
    private String scope;

    /** show exports. */
    private String showExports;

    /** show pagination. */
    private String showPagination;

    /** show pagination bottom. */
    private String showPaginationBottom;

    /** show exports bottom. */
    private String showExportsBottom;

    /** show exports with limit size pdf. */
    private String showExportsWithLimitSizePdf;

    /** show exports with limit size xls or ods. */
    private String showExportsWithLimitSizeXlsOrOds;

    /** show exports with limit size csv. */
    private String showExportsWithLimitSizeCsv;

    /** show new pagination. */
    private String showNewPagination;

    /** show new filter. */
    private String showNewFilter;

    /** new pagination nb page. */
    private String newPaginationNbPage;

    /** show status bar. */
    private String showStatusBar;

    /** show title. */
    private String showTitle;

    /** show tooltips. */
    private String showTooltips;

    /** sort rows callback. */
    private String sortRowsCallback;

    /** sortable. */
    private String sortable;

    /** state. */
    private String state;

    /** state attr. */
    private String stateAttr;

    /** style. */
    private String style;

    /** style class. */
    private String styleClass;

    /** table id. */
    private String tableId;

    /** theme. */
    private String theme;

    /** title. */
    private String title;

    /** var. */
    private String var;

    /** view. */
    private String view;

    /** width. */
    private String width;

    /**
     * attribut pour personnaliser champs statusbar.noResultsFound
     */
    private String libellenoresultsfound;

    /** positionnoresultsfound. */
    private String positionnoresultsfound;

    /** Attribut pour définir la page à afficher lors du premier accès au tableau. */
    private String startpage;

    /** model. */
    protected TableModel model;

    /** iterator. */
    private Iterator<Object> iterator;

    /**
     * Constructeur de la classe TableTag.java
     *
     */
    public TableTag()
    {
        super();
        
    }

    /**
     * Accesseur de l attribut model.
     * 
     * @return model
     */
    public TableModel getModel()
    {
        return model;
    }

    /**
     * Modificateur de l attribut action.
     * 
     * @param action le nouveau action
     * L'URI qui est appelée quand les actions de filtre, tri ou pagination sont utilisées."
     *                required="false" rtexprvalue="true"
     */
    public void setAction(String action)
    {
        this.action = action;
    }

    /**
     * Modificateur de l attribut auto include parameters.
     * 
     * @param autoIncludeParameters le nouveau auto include parameters
     * description=" Indique si les paramètres doivent automatiquement inclure les paramètres comme des
     *                champs cachés." required="false" rtexprvalue="true"
     */
    public void setAutoIncludeParameters(String autoIncludeParameters)
    {
        this.autoIncludeParameters = autoIncludeParameters;
    }

    /**
     * Modificateur de l attribut border.
     * 
     * @param border le nouveau border
     * description="L'attribut 'border' de l'élément 'table'. La valeur par défaut est
     *                0." required="false" rtexprvalue="true"
     */
    public void setBorder(String border)
    {
        this.border = border;
    }

    /**
     * Modificateur de l attribut buffer view.
     * 
     * @param bufferView le nouveau buffer view
     * description="Indique si la production de la vue doit utiliser un buffer. Par défaut :
     *                false." required="false" rtexprvalue="true"
     */
    public void setBufferView(String bufferView)
    {
        this.bufferView = bufferView;
    }

    /**
     * Modificateur de l attribut cellpadding.
     * 
     * @param cellpadding le nouveau cellpadding
     * description="[Définit l'attribut 'cellpadding' de l'élément 'table'. La valeur par défaut est 0.]"
     *                required="false" rtexprvalue="true"
     */
    public void setCellpadding(String cellpadding)
    {
        this.cellpadding = cellpadding;
    }

    /**
     * Modificateur de l attribut cellspacing.
     * 
     * @param cellspacing le nouveau cellspacing
     * description="Définit l'attribut 'cellspacing' de l'élément 'table'. La valeur par défaut est
     *                0." required="false" rtexprvalue="true"
     */
    public void setCellspacing(String cellspacing)
    {
        this.cellspacing = cellspacing;
    }

    /**
     * Modificateur de l attribut compact.
     * 
     * @param compact le nouveau compact
     * description="Définit si la table est compacte. Par défaut :
     *                false." required="false" rtexprvalue="true"
     */
    public void setCompact(String compact)
    {
        this.compact = compact;
    }

    /**
     * Modificateur de l attribut filterable.
     * 
     * @param filterable le nouveau filterable
     * description="Définit si les colonnes de la table sont filtrables. Par défaut :
     *                true." required="false" rtexprvalue="true"
     */
    public void setFilterable(String filterable)
    {
        this.filterable = filterable;
    }

    /**
     * Modificateur de l attribut filter rows callback.
     * 
     * @param filterRowsCallback le nouveau filter rows callback
     * description="Nom complet d'une classe qui implémente l'interface FilterRowsCallback. Peut aussi
     *                utiliser un alias définit dans le fichier extremecomponents.properties. Permet de filter des
     *                collections de beans." required="false" rtexprvalue="true"
     */
    public void setFilterRowsCallback(String filterRowsCallback)
    {
        this.filterRowsCallback = filterRowsCallback;
    }

    /**
     * Modificateur de l attribut form.
     * 
     * @param form le nouveau form
     * description="Référence à l'élément 'form' dans lequel s'insère le tableau." 
     * required="false" rtexprvalue="true"
     */
    public void setForm(String form)
    {
        this.form = form;
    }

    /**
     * Modificateur de l attribut image path.
     * 
     * @param imagePath le nouveau image path
     * description="Chemin permettant d'accéder aux images." required="false" rtexprvalue="true"
     */
    public void setImagePath(String imagePath)
    {
        this.imagePath = imagePath;
    }

    /**
     * Modificateur de l attribut interceptor.
     * 
     * @param interceptor le nouveau interceptor
     * description="Le nom complet d'une classe qui implémente l'interface TableInterceptor Peut aussi
     *                contenir un alias définit dans le fichier extremecomponents.properties Utilisé pour modifier ou
     *                ajouter des attributs à une colonne." required="false" rtexprvalue="true"
     */
    public void setInterceptor(String interceptor)
    {
        this.interceptor = interceptor;
    }

    /**
     * Modificateur de l attribut items.
     * 
     * @param items le nouveau items
     * description="Collection utilisée pour produire le tableau." required="false" rtexprvalue="true"
     */
    public void setItems(Object items)
    {
        this.items = items;
    }

    /**
     * Modificateur de l attribut locale.
     * 
     * @param locale le nouveau locale
     * description="[Locale utilisée par le tableau. Par exemple fr_FR pour un tableau en
     *                français." required="false" rtexprvalue="true"
     */
    public void setLocale(String locale)
    {
        this.locale = locale;
    }

    /**
     * Modificateur de l attribut method.
     * 
     * @param method le nouveau method
     * description="Indique si les actions de filtre, tri, paginations doivent être effectuées par un
     *                POST ou un GET." required="false" rtexprvalue="true"
     */
    public void setMethod(String method)
    {
        this.method = method;
    }

    /**
     * Modificateur de l attribut on invoke action.
     * 
     * @param onInvokeAction le nouveau on invoke action
     * description="Javascript exécuté lorsqu'une action de tri, filtre ou pagination est
     *                effectuée." required="false" rtexprvalue="true"
     */
    public void setOnInvokeAction(String onInvokeAction)
    {
        this.onInvokeAction = onInvokeAction;
    }

    /**
     * Modificateur de l attribut retrieve rows callback.
     * 
     * @param retrieveRowsCallback le nouveau retrieve rows callback
     * description="Nom complet d'une classe qui implémente l'interface RetrieveRowsCallback. Peut aussi
     *                utiliser un alias définit dans le fichier extremecomponents.properties. Permet de récupérer la
     *                collection de beans utilisée par la table." required="false" rtexprvalue="true"
     */
    public void setRetrieveRowsCallback(String retrieveRowsCallback)
    {
        this.retrieveRowsCallback = retrieveRowsCallback;
    }

    /**
     * Modificateur de l attribut rows displayed.
     * 
     * @param rowsDisplayed le nouveau rows displayed
     * description="Nombre de lignes à afficher par page." required="false" rtexprvalue="true"
     */
    public void setRowsDisplayed(String rowsDisplayed)
    {
        this.rowsDisplayed = rowsDisplayed;
    }

    /**
     * Modificateur de l attribut median rows displayed.
     * 
     * @param medianRowsDisplayed le nouveau median rows displayed
     * description="Nombre median de lignes à afficher par page." required="false" rtexprvalue="true"
     */
    public void setMedianRowsDisplayed(String medianRowsDisplayed)
    {
        this.medianRowsDisplayed = medianRowsDisplayed;
    }

    /**
     * Modificateur de l attribut max rows displayed.
     * 
     * @param maxRowsDisplayed le nouveau max rows displayed
     * description="Nombre maximum de lignes à afficher par page." required="false" rtexprvalue="true"
     */
    public void setMaxRowsDisplayed(String maxRowsDisplayed)
    {
        this.maxRowsDisplayed = maxRowsDisplayed;
    }

    /**
     * Modificateur de l attribut scope.
     * 
     * @param scope le nouveau scope
     * description="Le contexte (page, request, session, or application) où trouver la collection de
     *                beans définie par l'attribut 'items'." required="false" rtexprvalue="true"
     */
    public void setScope(String scope)
    {
        this.scope = scope;
    }

    /**
     * Modificateur de l attribut show pagination.
     * 
     * @param showPagination le nouveau show pagination
     * description="Indique si le tableau doit être paginé ou non. Par défaut : true" required="false"
     *                rtexprvalue="true"
     */
    public void setShowPagination(String showPagination)
    {
        this.showPagination = showPagination;
    }

    /**
     * Modificateur de l attribut show exports.
     * 
     * @param showExports le nouveau show exports
     * description="Indique si le tableau doit utiliser les exports ou non. Par défaut :
     *                true" required="false" rtexprvalue="true"
     */
    public void setShowExports(String showExports)
    {
        this.showExports = showExports;
    }

    /**
     * Modificateur de l attribut show status bar.
     * 
     * @param showStatusBar le nouveau show status bar
     * description="Indique si le tableau doit utiliser la barre de statut. Par défaut :
     *                true." required="false" rtexprvalue="true"
     */
    public void setShowStatusBar(String showStatusBar)
    {
        this.showStatusBar = showStatusBar;
    }

    /**
     * Modificateur de l attribut show title.
     * 
     * @param showTitle le nouveau show title
     * description="Indique si le titre du tableau doit être affiché. Par défaut :
     *                true." required="false" rtexprvalue="true"
     */
    public void setShowTitle(String showTitle)
    {
        this.showTitle = showTitle;
    }

    /**
     * Modificateur de l attribut show tooltips.
     * 
     * @param showTooltips le nouveau show tooltips
     * description="Indique si les infos bulles du tableau doivent être affichés. Par défaut :
     *                true" required="false" rtexprvalue="true"
     */
    public void setShowTooltips(String showTooltips)
    {
        this.showTooltips = showTooltips;
    }

    /**
     * Modificateur de l attribut sort rows callback.
     * 
     * @param sortRowsCallback le nouveau sort rows callback
     * description="Nom complet d'une classe qui implémente l'interface SortRowsCallback. Peut aussi
     *                utiliser un alias définit dans le fichier extremecomponents.properties. Permet de trier la
     *                collection de beans utilisée par la table." required="false" rtexprvalue="true"
     */
    public void setSortRowsCallback(String sortRowsCallback)
    {
        this.sortRowsCallback = sortRowsCallback;
    }

    /**
     * Modificateur de l attribut sortable.
     * 
     * @param sortable le nouveau sortable
     * description="Indique si la table est triable ou non. Par défaut :
     *                true." required="false" rtexprvalue="true"
     */
    public void setSortable(String sortable)
    {
        this.sortable = sortable;
    }

    /**
     * Modificateur de l attribut state.
     * 
     * @param state le nouveau state
     * description="Définit l'état dans lequel la table doit se trouver lorsque l'utilisateur retourne
     *                sur la table. Les valeurs possibles sont : default, notifyToDefault, persist,
     *                notifyToPersist." rtexprvalue="true" required="false"
     */
    public void setState(String state)
    {
        this.state = state;
    }

    /**
     * Modificateur de l attribut state attr.
     * 
     * @param stateAttr le nouveau state attr
     * description="The table attribute used to invoke the state change of the
     *                table." required="false" rtexprvalue="true"
     */

    /**
     * description="L'attribut utiliser pour provoquer un changement d'état du tableau." required="false"
     *                rtexprvalue="true"
     * @param stateAttr
     */
    public void setStateAttr(String stateAttr)
    {
        this.stateAttr = stateAttr;
    }

    /**
     * Modificateur de l attribut style.
     * 
     * @param style le nouveau style
     * description="Définit l'attribut 'style' de l'élément 'table'." required="false" rtexprvalue="true"
     */
    public void setStyle(String style)
    {
        this.style = style;
    }

    /**
     * Modificateur de l attribut style class.
     * 
     * @param styleClass le nouveau style class
     * description="Définit l'attribut 'class' de l'élément 'table'." required="false" rtexprvalue="true"
     */
    public void setStyleClass(String styleClass)
    {
        this.styleClass = styleClass;
    }

    /**
     * Modificateur de l attribut table id.
     * 
     * @param tableId le nouveau table id
     * description="Identifiant unique du tableau dans la JSP." required="false" rtexprvalue="true"
     */
    public void setTableId(String tableId)
    {
        this.tableId = tableId;
    }

    /**
     * Modificateur de l attribut theme.
     * 
     * @param theme le nouveau theme
     * description="Le theme utilisé pour définir les classes css qui seront utilisées par le tableau.par
     *                défaut : eXtremeTable." required="false" rtexprvalue="true"
     */
    public void setTheme(String theme)
    {
        this.theme = theme;
    }

    /**
     * Modificateur de l attribut title.
     * 
     * @param title le nouveau title
     * description="Le titre du tableau. Il est affiché au dessus du
     *                tableau." required="false" rtexprvalue="true"
     */
    public void setTitle(String title)
    {
        this.title = title;
    }

    /**
     * Modificateur de l attribut var.
     * 
     * @param var le nouveau var
     * description="Définit le nom de la variable qui stocke le bean de la ligne
     *                courante." required="false" rtexprvalue="true"
     */
    public void setVar(String var)
    {
        this.var = var;
    }

    /**
     * Modificateur de l attribut view.
     * 
     * @param view le nouveau view
     * description="Indique la classe qui implémente l'interface 'View' et produit le tableau au format
     *                HTML." required="false" rtexprvalue="true"
     */
    public void setView(String view)
    {
        this.view = view;
    }

    /**
     * Modificateur de l attribut width.
     * 
     * @param width le nouveau width
     * description="Largeur du tableau." required="false" rtexprvalue="true"
     */
    public void setWidth(String width)
    {
        this.width = width;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see javax.servlet.jsp.tagext.TagSupport#doStartTag()
     */
    @Override
    public int doStartTag() throws JspException
    {
        try
        {
            // initialisation des attributs
            this.iterator = null;
            this.pageContext.setAttribute(TableConstants.ROWCOUNT, "0");

            // Construit le modèle à partir du contexte, des préféreences et des messages
            this.model =
                new TableModelImpl(new JspPageContext(this.pageContext), TagUtils.evaluateExpressionAsString("locale",
                    this.locale, this, this.pageContext));

            // Construit le tableau
            Table table = new Table(this.model);
            table.setAction(TagUtils.evaluateExpressionAsString("action", this.action, this, this.pageContext));
            table.setAutoIncludeParameters(TagUtils.evaluateExpressionAsBoolean("autoIncludeParameters",
                this.autoIncludeParameters, this, this.pageContext));
            table.setBorder(TagUtils.evaluateExpressionAsString("border", this.border, this, this.pageContext));
            table.setBufferView(TagUtils.evaluateExpressionAsBoolean("bufferView", this.bufferView, this,
                this.pageContext));
            table.setCellpadding(TagUtils.evaluateExpressionAsString("cellpadding", this.cellpadding, this,
                this.pageContext));
            table.setCellspacing(TagUtils
                .evaluateExpressionAsString("cellspacing", this.cellspacing, this, pageContext));
            table.setCompact(TagUtils.evaluateExpressionAsBoolean("compact", this.compact, this, pageContext));
            table.setFilterable(TagUtils.evaluateExpressionAsBoolean("filterable", this.filterable, this, pageContext));
            table.setFilterRowsCallback(TagUtils.evaluateExpressionAsString("filterRowsCallback",
                this.filterRowsCallback, this, pageContext));
            table.setForm(TagUtils.evaluateExpressionAsString("form", this.form, this, pageContext));
            table.setImagePath(TagUtils.evaluateExpressionAsString("imagePath", this.imagePath, this, pageContext));
            table.setInterceptor(TagUtils
                .evaluateExpressionAsString("interceptor", this.interceptor, this, pageContext));
            table.setItems(TagUtils.evaluateExpressionAsObject("items", this.items, this, pageContext));
            table.setLocale(TagUtils.evaluateExpressionAsString("locale", this.locale, this, pageContext));
            table.setMethod(TagUtils.evaluateExpressionAsString("method", this.method, this, pageContext));
            table.setOnInvokeAction(TagUtils.evaluateExpressionAsString("onInvokeAction", this.onInvokeAction, this,
                pageContext));
            table.setRetrieveRowsCallback(TagUtils.evaluateExpressionAsString("retrieveRowsCallback",
                this.retrieveRowsCallback, this, pageContext));
            table.setRowsDisplayed(TagUtils.evaluateExpressionAsInt("rowsDisplayed", this.rowsDisplayed, this,
                pageContext));
            table.setMedianRowsDisplayed(TagUtils.evaluateExpressionAsInt("medianRowsDisplayed", this.medianRowsDisplayed, this,
                pageContext));
            table.setMaxRowsDisplayed(TagUtils.evaluateExpressionAsInt("maxRowsDisplayed", this.maxRowsDisplayed, this,
                pageContext));
            table.setScope(TagUtils.evaluateExpressionAsString("scope", scope, this, pageContext));
            table.setShowExports(TagUtils.evaluateExpressionAsBoolean("showExports", this.showExports, this,
                pageContext));
            table.setShowExportsWithLimitSizePdf(TagUtils.evaluateExpressionAsInt("showExportsWithLimitSizePdf",
                this.showExportsWithLimitSizePdf, this,
                pageContext));
            table.setShowExportsWithLimitSizeXlsOrOds(TagUtils.evaluateExpressionAsInt("showExportsWithLimitSizeXlsOrOds",
                this.showExportsWithLimitSizeXlsOrOds, this,
                pageContext));
            table.setShowExportsWithLimitSizeCsv(TagUtils.evaluateExpressionAsInt("showExportsWithLimitSizeCsv",
                this.showExportsWithLimitSizeCsv, this,
                pageContext));
            table.setShowPagination(TagUtils.evaluateExpressionAsBoolean("showPagination", this.showPagination, this,
                pageContext));
            table.setShowNewPagination(TagUtils.evaluateExpressionAsBoolean("showNewPagination", this.showNewPagination, this,
                pageContext));
            table.setShowNewFilter(TagUtils.evaluateExpressionAsBoolean("showNewFilter", this.showNewFilter, this,
                pageContext));
            table.setNewPaginationNbPage(TagUtils.evaluateExpressionAsInt("newPaginationNbPage", this.newPaginationNbPage, this,
                pageContext));
            table.setShowPaginationBottom(TagUtils.evaluateExpressionAsBoolean("showPaginationBottom", this.showPaginationBottom, this,
                pageContext));

            table.setShowExportsBottom(TagUtils.evaluateExpressionAsBoolean("showExportsBottom", this.showExportsBottom, this,
                pageContext));

            table.setShowStatusBar(TagUtils.evaluateExpressionAsBoolean("showStatusBar", this.showStatusBar, this,
                pageContext));
            table.setShowTitle(TagUtils.evaluateExpressionAsBoolean("showTitle", this.showTitle, this, pageContext));
            table.setShowTooltips(TagUtils.evaluateExpressionAsBoolean("showTooltips", this.showTooltips, this,
                pageContext));
            table.setSortRowsCallback(TagUtils.evaluateExpressionAsString("sortRowsCallback", this.sortRowsCallback,
                this, pageContext));
            table.setSortable(TagUtils.evaluateExpressionAsBoolean("sortable", this.sortable, this, pageContext));
            table.setState(TagUtils.evaluateExpressionAsString("state", this.state, this, pageContext));
            table.setStateAttr(TagUtils.evaluateExpressionAsString("stateAttr", this.stateAttr, this, pageContext));
            table.setStyle(TagUtils.evaluateExpressionAsString("style", style, this, pageContext));
            table.setStyleClass(TagUtils.evaluateExpressionAsString("styleClass", this.styleClass, this, pageContext));
            table.setTableId(TagUtils.evaluateExpressionAsString("tableId", tableId, this, pageContext));
            table.setTheme(TagUtils.evaluateExpressionAsString("theme", this.theme, this, pageContext));
            table.setTitle(TagUtils.evaluateExpressionAsString("title", this.title, this, pageContext));
            table.setVar(TagUtils.evaluateExpressionAsString("var", this.var, this, pageContext));
            table.setView(TagUtils.evaluateExpressionAsString("view", this.view, this, pageContext));
            table.setWidth(TagUtils.evaluateExpressionAsString("width", this.width, this, pageContext));
            table.setLibellenoresultsfound(TagUtils.evaluateExpressionAsString("libellenoresultsfound",
                this.libellenoresultsfound, this, pageContext));
            table.setPositionnoresultsfound(TagUtils.evaluateExpressionAsString("positionnoresultsfound",
                this.positionnoresultsfound, this, pageContext));
            table.setStartpage(TagUtils.evaluateExpressionAsInt("startpage", this.startpage, this, pageContext));

            table.setShowResponsive(TagUtils.evaluateExpressionAsBoolean("showResponsive", this.showResponsive, this, pageContext));
            table.setDataAttributes(TagUtils.evaluateExpressionAsString("dataAttributes", this.dataAttributes, this, pageContext));
            table.setPaginationClass(TagUtils.evaluateExpressionAsString("paginationClass", this.paginationClass, this, pageContext));
            table.setExportsClass(TagUtils.evaluateExpressionAsString("exportsClass", this.exportsClass, this, pageContext));
            addTableAttributes(model, table);
            model.addTable(table);
        }
        catch (Exception exception)
        {
            throw new JspException("Erreur", exception);
        }

        return EVAL_BODY_INCLUDE;
    }

    /**
     * Deux tâches sont accomplies dans cette méthode. En premier lieu, on itère tout d'abord sur toutes les colonnes
     * pour en charger les attributs. Dans un second temps, on itère sur toutes les colonnes autant de fois qu'indiqué
     * par l'attribut rowsDisplayed. A chaque itération, sur le bean courant de la ligne en courant est passé à la jsp
     * via le pageScope
     * 
     * @return int
     * @throws JspException the jsp exception
     */
    @Override
    public int doAfterBody() throws JspException
    {
        try
        {
            if (this.iterator == null)
            {
                this.iterator = this.model.execute().iterator();
            }

            if (this.iterator != null && this.iterator.hasNext())
            {
                Object bean = iterator.next();
                this.model.setCurrentRowBean(bean);
                return EVAL_BODY_AGAIN;
            }
        }
        catch (Exception exception)
        {
            throw new JspException("Erreur :", exception);
        }

        return SKIP_BODY;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see javax.servlet.jsp.tagext.TagSupport#doEndTag()
     */
    @Override
    public int doEndTag() throws JspException
    {
        try
        {
            this.pageContext.getOut().println(model.getViewData());
        }
        catch (Exception exception)
        {
            throw new JspException("Erreur :", exception);
        }

        return EVAL_PAGE;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.interceptor.TableInterceptor#addTableAttributes(fr.gouv.finances.lombok.apptags.table.core.TableModel,
     *      fr.gouv.finances.lombok.apptags.table.bean.Table)
     */
    @Override
    public void addTableAttributes(TableModel model, Table table)
    {
        // RAS
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see javax.servlet.jsp.tagext.TryCatchFinally#doCatch(java.lang.Throwable)
     */
    @Override
    public void doCatch(Throwable exception) throws Throwable
    {
        throw new JspException("Erreur ", exception);
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see javax.servlet.jsp.tagext.TryCatchFinally#doFinally()
     */
    @Override
    public void doFinally()
    {
        iterator = null;
        model = null;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see javax.servlet.jsp.tagext.TagSupport#release()
     */
    @Override
    public void release()
    {
        action = null;
        autoIncludeParameters = null;
        border = null;
        cellpadding = null;
        cellspacing = null;
        compact = null;
        filterable = null;
        filterRowsCallback = null;
        form = null;
        imagePath = null;
        interceptor = null;
        items = null;
        locale = null;
        method = null;
        onInvokeAction = null;
        showResponsive = null;
        dataAttributes = null;
        paginationClass = null;
        exportsClass = null;
        retrieveRowsCallback = null;
        rowsDisplayed = null;
        medianRowsDisplayed = null;        
        maxRowsDisplayed = null;        
        scope = null;
        showExports = null;
        showPagination = null;
        showNewPagination = null;
        showNewFilter = null;
        newPaginationNbPage = null;
        showPaginationBottom = null;
        showExportsBottom = null;
        showStatusBar = null;
        sortRowsCallback = null;
        sortable = null;
        state = null;
        stateAttr = null;
        style = null;
        styleClass = null;
        tableId = null;
        title = null;
        var = null;
        view = null;
        width = null;
        libellenoresultsfound = null;
        positionnoresultsfound = null;

        super.release();
    }

    /**
     * Sets the attribut pour personnaliser champs statusbar.
     * 
     * @param libellenoresultsfound the new attribut pour personnaliser champs statusbar
     */
    public void setLibellenoresultsfound(String libellenoresultsfound)
    {
        this.libellenoresultsfound = libellenoresultsfound;
    }

    /**
     * Modificateur de l attribut positionnoresultsfound.
     * 
     * @param positionnoresultsfound le nouveau positionnoresultsfound
     */
    public void setPositionnoresultsfound(String positionnoresultsfound)
    {
        this.positionnoresultsfound = positionnoresultsfound;
    }

    /**
     * Sets the attribut pour définir la page à afficher lors du premier accès au tableau.
     * 
     * @param page the new attribut pour définir la page à afficher lors du premier accès au tableau
     */
    public void setStartpage(String page)
    {
        this.startpage = page;
    }

    /**
     * Accesseur de l attribut show pagination bottom.
     * 
     * @return show pagination bottom
     */
    public String getShowPaginationBottom()
    {
        return showPaginationBottom;
    }

    /**
     * Modificateur de l attribut show pagination bottom.
     * 
     * @param showPaginationBottom le nouveau show pagination bottom
     */
    public void setShowPaginationBottom(String showPaginationBottom)
    {
        this.showPaginationBottom = showPaginationBottom;
    }

    /**
     * Accesseur de l attribut show new pagination.
     * 
     * @return show new pagination
     */
    public String getShowNewPagination()
    {
        return showNewPagination;
    }

    /**
     * Modificateur de l attribut show new pagination.
     * 
     * @param showNewPagination le nouveau show new pagination
     */
    public void setShowNewPagination(String showNewPagination)
    {
        this.showNewPagination = showNewPagination;
    }

    /**
     * Accesseur de l attribut show new filter.
     * 
     * @return show new filter
     */
    public String getShowNewFilter()
    {
        return showNewFilter;
    }

    /**
     * Modificateur de l attribut show new Filter.
     * 
     * @param showNewFilter le nouveau show new Filter
     */
    public void setShowNewFilter(String showNewFilter)
    {
        this.showNewFilter = showNewFilter;
    }

    /**
     * Accesseur de l attribut new pagination nb page.
     * 
     * @return new pagination nb page
     */
    public String getNewPaginationNbPage()
    {
        return newPaginationNbPage;
    }

    /**
     * Modificateur de l attribut new pagination nb page.
     * 
     * @param newPaginationNbPage le nouveau new pagination nb page
     */
    public void setNewPaginationNbPage(String newPaginationNbPage)
    {
        this.newPaginationNbPage = newPaginationNbPage;
    }

    /**
     * Accesseur de l attribut show exports bottom.
     * 
     * @return show exports bottom
     */
    public String getShowExportsBottom()
    {
        return showExportsBottom;
    }

    /**
     * Modificateur de l attribut show exports bottom.
     * 
     * @param showExportsBottom le nouveau show exports bottom
     */
    public void setShowExportsBottom(String showExportsBottom)
    {
        this.showExportsBottom = showExportsBottom;
    }

    
    /**
     * Accesseur de l attribut responsive.
     * 
     * @return show responsive
     */
    public String getShowResponsive()
    {
        return showResponsive;
    }

    /**
     * Modificateur de l attribut responsive.
     * 
     * @param showResponsive nouveau responsive
     */
    public void setShowResponsive(String showResponsive)
    {
        this.showResponsive = showResponsive;
    } 
    
    
    /**
     * Accesseur de l attribut dataAttributes.
     * 
     * @return show dataAttributes
     */
    public String getDataAttributes()
    {
        return dataAttributes;
    }
    
    /**
     * Modificateur de l attribut dataAttributes.
     * 
     * @param dataAttributes le nouveau dataAttributes
     */
    public void setDataAttributes(String dataAttributes)
    {
        this.dataAttributes = dataAttributes;
    }
   
    
    /**
     * Modificateur de l attribut paginationClass.
     *  
     */
    public String getPaginationClass()
    {
        return paginationClass;
    }
    
    
    /**
     * Modificateur de l attribut paginationClass.
     * 
     * @param paginationClass le nouveau paginationClass
     */
    public void setPaginationClass(String paginationClass)
    {
        this.paginationClass = paginationClass;
    }
    
    
    
    /**
     * Modificateur de l attribut exportClass.
     *  
     */
    public String getExportsClass()
    {
        return exportsClass;
    }
    
    
    /**
     * Modificateur de l attribut exportsClass.
     * 
     * @param exportsClass le nouveau exportsClass
     */
    public void setExportsClass(String exportsClass)
    {
        this.exportsClass = exportsClass;
    }
    
    
    
    
    
    /**
     * Accesseur de l attribut show exports with limit size pdf.
     * 
     * @return show exports with limit size pdf
     */
    public String getShowExportsWithLimitSizePdf()
    {
        return showExportsWithLimitSizePdf;
    }

    /**
     * Modificateur de l attribut show exports with limit size pdf.
     * 
     * @param showExportsWithLimitSizePdf le nouveau show exports with limit size pdf
     */
    public void setShowExportsWithLimitSizePdf(String showExportsWithLimitSizePdf)
    {
        this.showExportsWithLimitSizePdf = showExportsWithLimitSizePdf;
    }

    /**
     * Accesseur de l attribut show exports with limit size xls or ods.
     * 
     * @return show exports with limit size xls or ods
     */
    public String getShowExportsWithLimitSizeXlsOrOds()
    {
        return showExportsWithLimitSizeXlsOrOds;
    }

    /**
     * Modificateur de l attribut show exports with limit size xls or ods.
     * 
     * @param showExportsWithLimitSizeXlsOrOds le nouveau show exports with limit size xls or ods
     */
    public void setShowExportsWithLimitSizeXlsOrOds(String showExportsWithLimitSizeXlsOrOds)
    {
        this.showExportsWithLimitSizeXlsOrOds = showExportsWithLimitSizeXlsOrOds;
    }

    /**
     * Accesseur de l attribut show exports with limit size csv.
     * 
     * @return show exports with limit size csv
     */
    public String getShowExportsWithLimitSizeCsv()
    {
        return showExportsWithLimitSizeCsv;
    }

    /**
     * Modificateur de l attribut show exports with limit size csv.
     * 
     * @param showExportsWithLimitSizeCsv le nouveau show exports with limit size csv
     */
    public void setShowExportsWithLimitSizeCsv(String showExportsWithLimitSizeCsv)
    {
        this.showExportsWithLimitSizeCsv = showExportsWithLimitSizeCsv;
    }

}