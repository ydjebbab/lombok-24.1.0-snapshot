/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : TagWriter.java
 * Copyright 2002-2006 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.gouv.finances.lombok.apptags.base;

import java.io.IOException;
import java.io.Writer;
import java.util.Stack;

import javax.servlet.jsp.JspException;

import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

/**
 * Class TagWriter Utility class for writing HTML content to a {@link Writer} instance.
 * <p>
 * Intended to support output from JSP tag libraries.
 * 
 * @author Rob Harrop
 * @since 2.0
 * @author chouard-cp
 * @version $Revision: 1.3 $ Date: 15 déc. 2009
 */
public class TagWriter
{

    /**
     * Simple {@link Writer} wrapper that wraps all {@link IOException IOExceptions} in {@link JspException
     * JspExceptions}.
     */
    private static final class SafeWriter
    {

        /** writer. */
        private final Writer writer;

        /**
         * Instanciation de safe writer.
         * 
         * @param writer --
         */
        public SafeWriter(Writer writer)
        {
            this.writer = writer;
        }

        /**
         * methode Append : --.
         * 
         * @param value --
         * @return safe writer
         * @throws JspException the jsp exception
         */
        public SafeWriter append(String value) throws JspException
        {
            try
            {
                this.writer.write(String.valueOf(value));
                return this;
            }
            catch (IOException ex)
            {
                throw new JspException("Unable to write to JspWriter", ex);
            }
        }

    }

    /**
     * Holds state about a tag and its rendered behaviour.
     */
    private static final class TagStateEntry
    {

        /** tag name. */
        private final String tagName;

        /** block tag. */
        private boolean blockTag;

        /**
         * Instanciation de tag state entry.
         * 
         * @param tagName --
         */
        public TagStateEntry(String tagName)
        {
            this.tagName = tagName;
        }

        /**
         * Accesseur de l attribut tag name.
         * 
         * @return tag name
         */
        public String getTagName()
        {
            return tagName;
        }

        /**
         * Verifie si block tag.
         * 
         * @return true, si c'est block tag
         */
        public boolean isBlockTag()
        {
            return blockTag;
        }

        /**
         * methode Mark as block tag : --.
         */
        public void markAsBlockTag()
        {
            this.blockTag = true;
        }

    }

    /** The {@link SafeWriter} to write to. */
    private final SafeWriter writer;

    /** Stores {@link TagStateEntry tag state}. Stack model naturally supports tag nesting. */
    private Stack<TagStateEntry> tagState = new Stack<TagStateEntry>();

    /**
     * Creates a new instance of the {@link TagWriter} class that writes to the supplied {@link Writer}.
     * 
     * @param writer the {@link Writer} to write tag content to
     */
    public TagWriter(Writer writer)
    {
        Assert.notNull(writer);
        this.writer = new SafeWriter(writer);
    }

    /**
     * Closes the current opening tag (if necessary) and appends the supplied value as inner text.
     * 
     * @param value --
     * @throws JspException the jsp exception
     */
    public void appendValue(String value) throws JspException
    {
        if (!inTag())
        {
            throw new IllegalStateException("Cannot write tag value. No open tag available.");
        }
        closeTagAndMarkAsBlock();
        this.writer.append(value);
    }

    /**
     * Ajoute du texte sans contrôler si un tag html est déjà ouvert.
     * 
     * @param value --
     * @throws JspException the jsp exception
     */
    public void appendValueOutsideTag(String value) throws JspException
    {
        this.writer.append(value);
    }

    /**
     * Closes the current tag.
     * <p>
     * Correctly writes an empty tag if no inner text or nested tags have been written.
     * 
     * @throws JspException the jsp exception
     */
    public void endTag() throws JspException
    {
        if (!inTag())
        {
            throw new IllegalStateException("Cannot write end of tag. No open tag available.");
        }
        if (currentState().isBlockTag())
        {
            // writing the end of the block - the opening tag was closed earlier.
            this.writer.append("</").append(currentState().getTagName()).append(">");
            this.writer.append("\n");
        }
        else
        {
            this.writer.append("/>");
            this.writer.append("\n");
        }
        this.tagState.pop();
    }

    /**
     * Indicates that the currently open tag should be closed and marked as a block level element.
     * <p>
     * Useful when you plan to write additional content in the body outside the context of the current {@link TagWriter}.
     * 
     * @throws JspException the jsp exception
     */
    public void forceBlock() throws JspException
    {
        if (currentState().isBlockTag())
        {
            return; // just ignore since we are already in the block
        }
        closeTagAndMarkAsBlock();
    }

    /**
     * Starts a new tag with the supplied name. Leaves the tag open so that attributes, inner text or nested tags can be
     * written into it.
     * 
     * @param tagName --
     * @throws JspException the jsp exception
     * @see #endTag()
     */
    public void startTag(String tagName) throws JspException
    {
        if (inTag())
        {
            closeTagAndMarkAsBlock();
        }
        push(tagName);
        this.writer.append("<").append(tagName);
    }

    /**
     * Writes an HTML attribute with the specified name and value.
     * <p>
     * Be sure to write all attributes <strong>before</strong> writing any inner text or nested tags.
     * 
     * @param attributeName --
     * @param attributeValue --
     * @throws JspException the jsp exception
     */
    public void writeAttribute(String attributeName, String attributeValue) throws JspException
    {
        if (currentState().isBlockTag())
        {
            throw new IllegalStateException("Cannot write attributes after opening tag is closed.");
        }
        this.writer.append(" ").append(attributeName).append("=\"").append(attributeValue).append("\"");
    }

    /**
     * Writes an HTML attribute if the supplied value is not <code>null</code> or zero length.
     * 
     * @param attributeName --
     * @param attributeValue --
     * @throws JspException the jsp exception
     * @see #writeAttribute(String, String)
     */
    public void writeOptionalAttributeValue(String attributeName, String attributeValue) throws JspException
    {
        if (StringUtils.hasText(attributeValue))
        {
            writeAttribute(attributeName, attributeValue);
        }
    }

    /**
     * Closes the current opening tag and marks it as a block tag.
     * 
     * @throws JspException the jsp exception
     */
    private void closeTagAndMarkAsBlock() throws JspException
    {
        if (!currentState().isBlockTag())
        {
            currentState().markAsBlockTag();
            this.writer.append(">");
        }
    }

    /**
     * methode Current state : --.
     * 
     * @return tag state entry
     */
    private TagStateEntry currentState()
    {
        return this.tagState.peek();
    }

    /**
     * methode In tag : --.
     * 
     * @return true, si c'est vrai
     */
    private boolean inTag()
    {
        return !this.tagState.isEmpty();
    }

    /**
     * Adds the supplied tag name to the {@link #tagState tag state}.
     * 
     * @param tagName --
     */
    private void push(String tagName)
    {
        this.tagState.push(new TagStateEntry(tagName));
    }

}
