/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : JasperreportsViewResolver.java
 *
 */
package fr.gouv.finances.lombok.apptags.table.filter;

import java.util.Locale;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;

import fr.gouv.finances.lombok.apptags.table.core.Preferences;
import fr.gouv.finances.lombok.upload.bean.FichierJoint;

/**
 * Class JasperreportsViewResolver --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.2 $ Date: 11 déc. 2009
 */
public class JasperreportsViewResolver implements ViewResolver
{

    /**
     * Instanciation de jasperreports view resolver.
     */
    public JasperreportsViewResolver()
    {
        super();
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param request
     * @param response
     * @param preferences
     * @param viewData
     * @throws Exception the exception
     * @see fr.gouv.finances.lombok.apptags.table.filter.ViewResolver#resolveView(javax.servlet.ServletRequest,
     *      javax.servlet.ServletResponse, fr.gouv.finances.lombok.apptags.table.core.Preferences, java.lang.Object)
     */
    @Override
    public void resolveView(ServletRequest request, ServletResponse response, Preferences preferences, Object viewData)
        throws Exception
    {

        FichierJoint fichierJoint = (FichierJoint) viewData;

        byte[] contents = fichierJoint.getLeContenuDuFichier().getData();

        response.setLocale(Locale.FRANCE);
        response.setContentLength((int) fichierJoint.getTailleFichier());
        response.setBufferSize((int) fichierJoint.getTailleFichier());
        response.setContentType(fichierJoint.getTypeMimeFichier());

        if (response instanceof HttpServletResponse
            && fichierJoint != null
            && (!(fichierJoint.getTypeMimeFichier() != null && fichierJoint.getTypeMimeFichier()
                .startsWith("text/html"))))
        {
            ((HttpServletResponse) response).setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
            ((HttpServletResponse) response).setHeader("Content-Disposition", "attachment; filename=\""
                + fichierJoint.getNomFichierOriginal() + "\"");
        }

        response.getOutputStream().write(contents);
        response.getOutputStream().flush();
    }
}
