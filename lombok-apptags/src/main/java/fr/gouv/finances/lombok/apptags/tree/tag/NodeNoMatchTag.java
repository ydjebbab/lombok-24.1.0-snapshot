/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : NodeNoMatchTag.java
 *
 */
package fr.gouv.finances.lombok.apptags.tree.tag;

import javax.servlet.jsp.JspException;

import fr.gouv.finances.lombok.apptags.base.TagWriter;
import fr.gouv.finances.lombok.apptags.tree.bean.TreeIteratorElement;

/**
 * Class NodeNoMatchTag.
 * 
 * @author wpetit-cp
 * @version $Revision: 1.2 $ Date: 15 déc. 2009
 */
public class NodeNoMatchTag extends NodeBaseTag
{

    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** type. */
    protected String type = null;

    /** name. */
    protected String name = null;

    /** id. */
    protected String id = null;

    /** expanded. */
    protected String expanded = null;

    /** selected. */
    protected String selected = null;

    /** has children. */
    protected String hasChildren = null;

    /** is first child. */
    protected String isFirstChild = null;

    /** is last child. */
    protected String isLastChild = null;

    /**
     * Instanciation de node no match tag.
     */
    public NodeNoMatchTag()
    {
        super();
    }

    /**
     * methode Attribute not set : attributeNotSet.
     * 
     * @param attributeValue attributeValue
     * @return true, si c'est vrai
     */
    public boolean attributeNotSet(String attributeValue)
    {
        return attributeValue == null;
    }

    /**
     * Accesseur de l attribut expanded.
     * 
     * @return expanded
     */
    public String getExpanded()
    {
        return this.expanded;
    }

    /**
     * Accesseur de l attribut checks for children.
     * 
     * @return checks for children
     */
    public String getHasChildren()
    {
        return hasChildren;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @return the id
     * @see javax.servlet.jsp.tagext.TagSupport#getId()
     */
    @Override
    public String getId()
    {
        return id;
    }

    /**
     * Accesseur de l attribut checks if is first child.
     * 
     * @return checks if is first child
     */
    public String getIsFirstChild()
    {
        return this.isFirstChild;
    }

    /**
     * Accesseur de l attribut checks if is last child.
     * 
     * @return checks if is last child
     */
    public String getIsLastChild()
    {
        return this.isLastChild;
    }

    /**
     * Accesseur de l attribut name.
     * 
     * @return name name
     */
    public String getName()
    {
        return name;
    }

    /**
     * Accesseur de l attribut selected.
     * 
     * @return selected
     */
    public String getSelected()
    {
        return selected;
    }

    /**
     * Accesseur de l attribut type.
     * 
     * @return type
     */
    public String getType()
    {
        return this.type;
    }

    /**
     * Modificateur de l attribut expanded.
     * 
     * @param expanded le nouveau expanded
     */
    public void setExpanded(String expanded)
    {
        this.expanded = expanded;
    }

    /**
     * Modificateur de l attribut checks for children.
     * 
     * @param hasChildren le nouveau checks for children
     */
    public void setHasChildren(String hasChildren)
    {
        this.hasChildren = hasChildren;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param idd the new id
     * @see javax.servlet.jsp.tagext.TagSupport#setId(java.lang.String)
     */
    @Override
    public void setId(String idd)
    {
        this.id = idd;
    }

    /**
     * Modificateur de l attribut checks if is first child.
     * 
     * @param isFirstChild le nouveau checks if is first child
     */
    public void setIsFirstChild(String isFirstChild)
    {
        this.isFirstChild = isFirstChild;
    }

    /**
     * Modificateur de l attribut checks if is last child.
     * 
     * @param isLastChild le nouveau checks if is last child
     */
    public void setIsLastChild(String isLastChild)
    {
        this.isLastChild = isLastChild;
    }

    /**
     * Modificateur de l attribut name.
     * 
     * @param name le nouveau name
     */
    public void setName(String name)
    {
        this.name = name;
    }

    /**
     * Modificateur de l attribut selected.
     * 
     * @param selected le nouveau selected
     */
    public void setSelected(String selected)
    {
        this.selected = selected;
    }

    /**
     * Modificateur de l attribut type.
     * 
     * @param type le nouveau type
     */
    public void setType(String type)
    {
        this.type = type;
    }

    /**
     * methode Value not set : valueNotSet.
     * 
     * @param attributeValue attributeValue
     * @return true, si c'est vrai
     */
    public boolean valueNotSet(String attributeValue)
    {
        return attributeValue == null;
    }

    /**
     * Note : retourner false quand un attribut n'est pas positionné est différent du matches() dans le tag nodeMatch Si
     * un attribut n'est pas positionné dans le tag nodeNoMatch il est ignoré en retournant false (Ce n'est pas un
     * match).
     * 
     * @param attribute attribute
     * @param value value
     * @return true, if matches
     */
    protected boolean matches(String attribute, String value)
    {
        boolean result = false;

        if (attributeNotSet(attribute) || valueNotSet(value))
        {
            result = false;
        }
        else if (attribute.equals(value) || matchesWithWildCard(attribute, value))
        {
            result = true;
        }
        return result;
    }

    /**
     * methode Matches boolean : boolean.
     * 
     * @param attribute attribute
     * @param booleanValue booleanValue
     * @return true, si c'est vrai
     * @throws JspException the jsp exception
     */
    protected boolean matchesBoolean(String attribute, boolean booleanValue) throws JspException
    {
        boolean result = false;

        if (attributeNotSet(attribute))
        {
            result = false;
        }
        else if (!"true".equalsIgnoreCase(attribute) && !"false".equalsIgnoreCase(attribute))
        {
            throw new JspException("LES BOOLEENS DOIVENT ETRE TRUE OU FALSE");
        }
        else if (("true".equalsIgnoreCase(attribute) && booleanValue == true)
            || ("false".equalsIgnoreCase(attribute) && booleanValue == false))
        {
            result = true;
        }

        return result;
    }

    /**
     * methode Matches with wild card : matchesWithWildCard.
     * 
     * @param attribute attribute
     * @param value value
     * @return true, si c'est vrai
     */
    protected boolean matchesWithWildCard(String attribute, String value)
    {
        boolean result = false;
        int index = attribute.indexOf('*');

        if (index == -1)
        {
            result = false;
        }
        else if ("*".equals(attribute))
        {
            result = true;
        }
        else if (attribute.startsWith("*") && (value.endsWith(attribute.substring(1, attribute.length()))))
        {
            result = true;
        }
        else if (attribute.endsWith("*") && (value.startsWith(attribute.substring(0, attribute.length() - 1))))

        {
            result = true;
        }
        else
        {
            String start = attribute.substring(0, index);
            String end = attribute.substring(index + 1, attribute.length());

            if (value.startsWith(start) && value.endsWith(end))
            {
                result = true;
            }
        }
        return result;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param tagWriter
     * @return int
     * @throws JspException the jsp exception
     * @see fr.gouv.finances.lombok.apptags.base.BaseAppTags#writeTagContent(fr.gouv.finances.lombok.apptags.base.TagWriter)
     */
    @Override
    protected int writeTagContent(TagWriter tagWriter) throws JspException
    {
        this.initParentTreeTag();
        int result = EVAL_BODY_INCLUDE;

        TreeIteratorElement element = this.parentTreeTag.getElement();
        if (element == null)
        {
            throw new JspException("ELEMENT NULL");
        }
        if (element.getNode() == null)
        {
            throw new JspException("NODE NULL");
        }

        if (matches(getType(), element.getNode().getType()) || matches(getId(), element.getNode().getId())
            || matches(getName(), element.getNode().getName()) || matchesBoolean(getExpanded(), element.isExpanded())
            || matchesBoolean(getSelected(), element.isSelected())
            || matchesBoolean(getHasChildren(), element.getNode().hasChildren())
            || matchesBoolean(getIsFirstChild(), element.isFirstChild())
            || matchesBoolean(getIsLastChild(), element.isLastChild()))
        {
            result = SKIP_BODY;

        }

        return result;
    }

}
