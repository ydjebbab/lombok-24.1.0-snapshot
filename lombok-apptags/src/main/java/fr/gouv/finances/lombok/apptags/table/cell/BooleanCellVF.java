/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.apptags.table.cell;

import fr.gouv.finances.lombok.apptags.util.format.BooleanFormat.TypeFormat;

/**
 * Class BooleanCellVF
 */
public class BooleanCellVF extends AbstractBooleanCell
{

    /**
     * Instanciation de boolean cell vf.
     */
    public BooleanCellVF()
    {
        super();
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.cell.AbstractBooleanCell#getFormat()
     */
    @Override
    public TypeFormat getFormat()
    {
        return TypeFormat.V_F;
    }
}
