/*
/*Copyright (c) 2014 DGFiP - Tous droits réservés/* * Copyright 2004 original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package fr.gouv.finances.lombok.apptags.table.filter;

import java.util.Locale;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;

import fr.gouv.finances.lombok.apptags.table.bean.ExportFile;
import fr.gouv.finances.lombok.apptags.table.core.Preferences;

/**
 * Class JasperjrxmlViewResolver
 */
public class JasperjrxmlViewResolver implements ViewResolver
{

    /**
     * Instanciation de jasperjrxml view resolver.
     */
    public JasperjrxmlViewResolver()
    {
        super();
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.filter.ViewResolver#resolveView(javax.servlet.ServletRequest,
     *      javax.servlet.ServletResponse, fr.gouv.finances.lombok.apptags.table.core.Preferences, java.lang.Object)
     */
    @Override
    public void resolveView(ServletRequest request, ServletResponse response, Preferences preferences, Object viewData)
        throws Exception
    {

        ExportFile fichierJoint = (ExportFile) viewData;

        byte[] contents = fichierJoint.getLeContenuDuFichier().getData();

        response.setLocale(Locale.FRANCE);
        response.setContentLength((int) fichierJoint.getTailleFichier());
        response.setBufferSize((int) fichierJoint.getTailleFichier());
        response.setContentType(fichierJoint.getTypeMimeFichier());

        if (response instanceof HttpServletResponse
            && fichierJoint != null
            && (!(fichierJoint.getTypeMimeFichier() != null && fichierJoint.getTypeMimeFichier()
                .startsWith("text/html"))))
        {
            ((HttpServletResponse) response).setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
            ((HttpServletResponse) response).setHeader("Content-Disposition", "attachment; filename=\""
                + fichierJoint.getNomFichierOriginal() + "\"");
        }

        response.getOutputStream().write(contents);
        response.getOutputStream().flush();
    }
}
