/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.apptags.table.cell;

import fr.gouv.finances.lombok.apptags.util.HtmlBuilder;

/*Ajoute un attribut readonly sur une SelectedCheckBoxCell	 */

/**
 * Class DisabledSelectedCheckboxCell
 */
public class DisabledSelectedCheckboxCell extends SelectedCheckboxCell
{

    /**
     * Instanciation de disabled selected checkbox cell.
     */
    public DisabledSelectedCheckboxCell()
    {
        super();
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.cell.SelectedCheckboxCell#ajouterOptionsSurCheckBox(fr.gouv.finances.lombok.apptags.util.HtmlBuilder)
     */
    @Override
    protected HtmlBuilder ajouterOptionsSurCheckBox(HtmlBuilder html)
    {
        html.disabled();
        return html;
    }

}
