/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.apptags.util;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.webflow.core.collection.MutableAttributeMap;
import org.springframework.webflow.execution.RequestContext;

import fr.gouv.finances.lombok.apptags.table.cell.PreferencesConstants;
import fr.gouv.finances.lombok.apptags.table.view.MaxFetchTableAttributes;

/**
 * Class MaxFetchTableUtil
 */
public class MaxFetchTableUtil
{

    /** Constant : log. */
    private static final Log log = LogFactory.getLog(MaxFetchTableUtil.class);

    /**
     * Enregistre dans le contexte de flux l'objet de paramétrage de gestion du maxfetch.
     * 
     * @param context
     * @return l'objet de paramétrage
     */
    public static MaxFetchTableAttributes parametrer(final RequestContext context)
    {
        return MaxFetchTableUtil.parametrer(context, PreferencesConstants.TABLE_ID);
    }

    /**
     * Enregistre dans le contexte de flux l'objet de paramétrage de gestion du maxfetch.
     * 
     * @param context
     * @param tableId valeur du paramètre tableId utilisée dans le tag <ec:table>
     * @return l'objet de paramétrage
     */
    public static MaxFetchTableAttributes parametrer(final RequestContext context, final String tableId)
    {
        MaxFetchTableAttributes maxFetchAttr = MaxFetchTableAttributes.getDefault(tableId);
        // Sauvegarde de l'objet de paramétrage dans le FlowScope
        // Si plusieurs liste sont présentes dans la vue contenant le
        // tableau, il
        // faut stocker l'objet de paramétrage dans un attribut du
        // FlowScope qui
        // est nommé <valeur de l'attribut tableId indiqué dans
        // <ec:column> , suffixé par

        PreferencesConstants.getUsedScope(context, maxFetchAttr.getMaxFetchAttrStorageScope()).put(
            maxFetchAttr.getScopeName(), maxFetchAttr);

        return maxFetchAttr;
    }

    /**
     * methode Gerer max fetch :
     * 
     * @param context
     * @param tableId
     * @param tableList
     * @param maxFetch
     * @return max fetch table attributes
     */
    public static MaxFetchTableAttributes gererMaxFetch(final RequestContext context, final String tableId,
        String tableList, int maxFetch)
    {
        MaxFetchTableAttributes maxFetchAttr =
            MaxFetchTableUtil.parametrer(context, tableId).utiliserListeElements(tableList).utiliserMaxFetch(maxFetch);

        Collection elements = litLesElementsDuTableau(context, tableId);

        if (elements == null || elements.size() < maxFetch || maxFetch == 0)
        {
            PreferencesConstants.getUsedScope(context, maxFetchAttr.getMaxFetchAttrStorageScope()).put(
                maxFetchAttr.getReponseComplete(), Boolean.TRUE);
        }
        else
        {
            // Si la collection est de type List, on retire le
            // dernier élément jusqu'à ce que la liste
            // soit d'une taille inférieure à maxFetch
            if (elements instanceof List)
            {
                while (elements.size() >= maxFetch)
                {
                    ((List) elements).remove(elements.size() - 1);
                }
            }
            // sinon on retire le dernier élément retourné par
            // l'iterateur jusqu'à ce que la collection
            // soit d'une taille inférieure à maxFetch
            else
            {
                while (elements.size() >= maxFetch)
                {
                    Iterator iter = elements.iterator();
                    while (iter.hasNext())
                    {
                        iter.next();
                    }
                    iter.remove();
                }
            }
            PreferencesConstants.getUsedScope(context, maxFetchAttr.getMaxFetchAttrStorageScope()).put(
                maxFetchAttr.getReponseComplete(), Boolean.FALSE);
        }

        return maxFetchAttr;
    }

    /**
     * Lit dans le contexte de flux la collection utilisée par le tableau.
     * 
     * @param context
     * @return la collection support du tableau
     */
    public static Collection litLesElementsDuTableau(final RequestContext context)
    {
        return MaxFetchTableUtil.litLesElementsDuTableau(context, PreferencesConstants.TABLE_ID);
    }

    /**
     * Lit dans le contexte de flux la collection utilisée par le tableau.
     * 
     * @param context
     * @param tableId valeur du paramètre tableId utilisée dans le tag <ec:table>
     * @return la collection support du tableau
     */
    public static Collection litLesElementsDuTableau(final RequestContext context, final String tableId)
    {
        Collection result;
        // Récupère l'objet de paramétrage
        MaxFetchTableAttributes maxfetchAttr =
            MaxFetchTableUtil.getSelectedMaxfetchAttributesFromScopeOrDefault(context, tableId);
        result =
            (Collection) PreferencesConstants.getObjectFromScope(context, maxfetchAttr.getElementsInFlow(),
                PreferencesConstants.FLOW_SCOPE);
        return result;
    }

    /**
     * Lit dans le contexte (flowScope) l'objet de paramétrage de gestion du maxfetch Le nom sous lequel l'objet est
     * stocké dans le contexte est construit à partir du paramètre tableId. Si l'objet n'est pas dans le
     * contexte,l'objet de paramétrage par défaut est construit, puis stocké dans le contexte de flux
     * 
     * @param context
     * @param tableId valeur du paramètre tableId utilisée dans le tag <ec:table>
     * @return objet de paramétrage de gestion du maxfetch
     */
    public static MaxFetchTableAttributes getSelectedMaxfetchAttributesFromScopeOrDefault(final RequestContext context,
        final String tableId)
    {
        return MaxFetchTableUtil.getSelectedMaxfetchAttributesFromScopeOrDefault(context, tableId,
            PreferencesConstants.DEFAULT_MAXFETCHATTR_STORAGE_SCOPE);
    }

    /**
     * Lit dans le contexte (flowScope ou conversationScope) l'objet de paramétrage de gestion du maxfetch. Le nom sous
     * lequel l'objet est stocké dans le contexte est construit à partir du paramètre tableId. Si l'objet n'est pas dans
     * le contexte,l'objet de paramétrage par défaut est construit, puis stocké dans le contexte de flux
     * 
     * @param context
     * @param tableId valeur du paramètre tableId utilisée dans le tag <ec:table>
     * @param scope
     * @return objet de paramétrage de gestion du maxfetch
     */
    public static MaxFetchTableAttributes getSelectedMaxfetchAttributesFromScopeOrDefault(final RequestContext context,
        final String tableId, final int scope)
    {
        MaxFetchTableAttributes result = null;
        String attributeName = MaxFetchTableAttributes.getScopeName(tableId);

        MutableAttributeMap usedScope = PreferencesConstants.getUsedScope(context, scope);

        // On cherche l'objet de paramétrage dans le contexte de flux
        if (attributeName != null && StringUtils.isNotBlank(attributeName) && usedScope.contains(attributeName))
        {
            Object parametrage = usedScope.get(attributeName);
            if (parametrage == null)
            {
                StringBuilder msg = new StringBuilder();
                msg.append("L'attribut ");
                msg.append(attributeName);
                msg.append(" est null. L'objet de paramétrage de type ");
                msg.append("MaxFetchTableAttributes n'est pas dans le contexte");
                throw new EcTableException(msg.toString());
            }

            if (parametrage instanceof MaxFetchTableAttributes)
            {
                result = (MaxFetchTableAttributes) parametrage;
            }
            else
            {
                StringBuilder msg = new StringBuilder();
                msg.append("L'attribut ");
                msg.append(attributeName);
                msg.append(" stocké dans le FlowScope ou ConversationScope n'est pas du type MaxFetchTableAttributes");
                log.warn(msg.toString());
                throw new EcTableException(msg.toString());
            }
        }

        // Si on ne le trouve pas ou si on utilise la valeur par défaut
        // de l'objet de paramétrage
        else
        {
            result = MaxFetchTableAttributes.getDefault(tableId);
            usedScope.put(attributeName, result);
        }

        return result;
    }

}
