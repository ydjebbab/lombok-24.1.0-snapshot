/*
/*Copyright (c) 2014 DGFiP - Tous droits réservés/* * Copyright 2004 original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package fr.gouv.finances.lombok.apptags.table.tag;

import org.apache.commons.lang.StringUtils;

import fr.gouv.finances.lombok.apptags.table.bean.Export;
import fr.gouv.finances.lombok.apptags.table.core.TableConstants;
import fr.gouv.finances.lombok.apptags.table.core.TableModel;
import fr.gouv.finances.lombok.apptags.table.view.PdfView;
import fr.gouv.finances.lombok.apptags.table.view.html.BuilderConstants;

/**
 * Class ExportPdfTag
 * 
 * jsp.tag : name="exportPdf" display-name="ExportPdfTag" body-content="JSP"
 *          description="Exporte les données au format PDF."
 */
public class ExportPdfTag extends ExportTag
{

    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** header background color. */
    private String headerBackgroundColor;

    /** header title. */
    private String headerTitle;

    /** header color. */
    private String headerColor;

    /**
     * Constructeur de la classe ExportPdfTag.java
     *
     */
    public ExportPdfTag()
    {
        super();
        
    }

    /**
     * Modificateur de l attribut header background color.
     * 
     * @param headerBackgroundColor le nouveau header background color
     * jsp.attribute :  description="Couleur d'arrière plan de l'entête des colonnes." required="false" rtexprvalue="true"
     */
    public void setHeaderBackgroundColor(String headerBackgroundColor)
    {
        this.headerBackgroundColor = headerBackgroundColor;
    }

    /**
     * Modificateur de l attribut header color.
     * 
     * @param headerColor le nouveau header color
     * jsp.attribute :  description="Couleur de la police de l'entête des colonnes." required="false" rtexprvalue="true"
     */
    public void setHeaderColor(String headerColor)
    {
        this.headerColor = headerColor;
    }

    /**
     * Modificateur de l attribut header title.
     * 
     * @param headerTitle le nouveau header title
     * jsp.attribute :  description="Définit le titre affiché en haut de la page." required="false" rtexprvalue="true"
     */
    public void setHeaderTitle(String headerTitle)
    {
        this.headerTitle = headerTitle;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.tag.ExportTag#addExportAttributes(fr.gouv.finances.lombok.apptags.table.core.TableModel,
     *      fr.gouv.finances.lombok.apptags.table.bean.Export)
     */
    @Override
    public void addExportAttributes(TableModel model, Export export)
    {
        if (StringUtils.isBlank(export.getView()))
        {
            export.setView(TableConstants.VIEW_PDF);
        }

        if (StringUtils.isBlank(export.getViewResolver()))
        {
            export.setViewResolver(TableConstants.VIEW_PDF);
        }

        if (StringUtils.isBlank(export.getImageName()))
        {
            export.setImageName(TableConstants.VIEW_PDF);
        }

        if (StringUtils.isBlank(export.getText()))
        {
            export.setText(BuilderConstants.TOOLBAR_PDF_TEXT);
        }

        export.addAttribute(PdfView.HEADER_BACKGROUND_COLOR, TagUtils.evaluateExpressionAsString(
            "headerBackgroundColor", headerBackgroundColor, this, pageContext));
        export.addAttribute(PdfView.HEADER_COLOR, TagUtils.evaluateExpressionAsString("headerColor", headerColor, this,
            pageContext));
        export.addAttribute(PdfView.HEADER_TITLE, TagUtils.evaluateExpressionAsString("headerTitle", headerTitle, this,
            pageContext));
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.tag.ExportTag#release()
     */
    @Override
    public void release()
    {
        headerBackgroundColor = null;
        headerTitle = null;
        headerColor = null;
        super.release();
    }
}
