/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.apptags.table.view.jrxmlinfo;

import net.sf.jasperreports.engine.type.HorizontalAlignEnum;

/**
 * Class ColonneInfo
 */
public class ColonneInfo extends AbstractBandInfo
{

    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** titre. */
    private String titre;

    /** propriete. */
    private String propriete;

    /** alias. */
    private String alias;

    /** taille. */
    private int taille;

    /** clazz. */
    private Class<? extends Object> clazz;

    /** pattern. */
    private String pattern;

    /** cell type. */
    private String cellType;

    /** unit. */
    private String unit;

    /** alignement. */
    private HorizontalAlignEnum alignement;

    /**
     * Instanciation de colonne info.
     */
    public ColonneInfo()
    {
        super();
    }

    /**
     * Accesseur de l attribut alignement.
     * 
     * @return alignement
     */
    public HorizontalAlignEnum getAlignement()
    {
        return alignement;
    }

    /**
     * Modificateur de l attribut alignement.
     * 
     * @param alignement le nouveau alignement
     */
    public void setAlignement(HorizontalAlignEnum alignement)
    {
        this.alignement = alignement;
    }

    /**
     * Accesseur de l attribut propriete.
     * 
     * @return propriete
     */
    public String getPropriete()
    {
        return propriete;
    }

    /**
     * Modificateur de l attribut propriete.
     * 
     * @param propriete le nouveau propriete
     */
    public void setPropriete(String propriete)
    {
        this.propriete = propriete;
    }

    /**
     * Accesseur de l attribut unit.
     * 
     * @return unit
     */
    public String getUnit()
    {
        return unit;
    }

    /**
     * Modificateur de l attribut unit.
     * 
     * @param unit le nouveau unit
     */
    public void setUnit(String unit)
    {
        this.unit = unit;
    }

    /**
     * Accesseur de l attribut cell type.
     * 
     * @return cell type
     */
    public String getCellType()
    {
        return cellType;
    }

    /**
     * Modificateur de l attribut cell type.
     * 
     * @param cellType le nouveau cell type
     */
    public void setCellType(String cellType)
    {
        this.cellType = cellType;
    }

    /**
     * Gets the clazz.
     * 
     * @return the clazz
     */
    public Class<? extends Object> getClazz()
    {
        return clazz;
    }

    /**
     * Modificateur de l attribut clazz.
     * 
     * @param clazz le nouveau clazz
     */
    public void setClazz(Class<? extends Object> clazz)
    {
        this.clazz = clazz;
    }

    /**
     * Accesseur de l attribut pattern.
     * 
     * @return pattern
     */
    public String getPattern()
    {
        return pattern;
    }

    /**
     * Modificateur de l attribut pattern.
     * 
     * @param pattern le nouveau pattern
     */
    public void setPattern(String pattern)
    {
        this.pattern = pattern;
    }

    /**
     * Accesseur de l attribut taille.
     * 
     * @return taille
     */
    public int getTaille()
    {
        return taille;
    }

    /**
     * Modificateur de l attribut taille.
     * 
     * @param taille le nouveau taille
     */
    public void setTaille(int taille)
    {
        this.taille = taille;
    }

    /**
     * Accesseur de l attribut titre.
     * 
     * @return titre
     */
    public String getTitre()
    {
        return titre;
    }

    /**
     * Modificateur de l attribut titre.
     * 
     * @param titre le nouveau titre
     */
    public void setTitre(String titre)
    {
        this.titre = titre;
    }

    /**
     * Accesseur de l attribut alias.
     * 
     * @return alias
     */
    public String getAlias()
    {
        return alias;
    }

    /**
     * Modificateur de l attribut alias.
     * 
     * @param alias le nouveau alias
     */
    public void setAlias(String alias)
    {
        this.alias = alias;
    }

}
