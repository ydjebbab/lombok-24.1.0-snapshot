/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.apptags.table.calc;

import java.math.BigDecimal;
import java.util.Collection;

import fr.gouv.finances.lombok.apptags.table.bean.Column;
import fr.gouv.finances.lombok.apptags.table.core.TableModel;

/**
 * Class CpMaximumCalc
 */
public class CpMaximumCalc implements Calc
{

    /**
     * Constructeur de la classe CpMaximumCalc.java
     *
     */
    public CpMaximumCalc()
    {
        super();
        
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.calc.Calc#getCalcResult(fr.gouv.finances.lombok.apptags.table.core.TableModel,
     *      fr.gouv.finances.lombok.apptags.table.bean.Column)
     */
    @Override
    public Number getCalcResult(TableModel model, Column column)
    {
        Collection<Object> rows = model.getCollectionOfFilteredBeans();
        String property = column.getProperty();
        MaximumValue maximumValue = new MaximumValue();
        CpCalcUtils.eachRowCalcValue(maximumValue, rows, property);

        return maximumValue.getMaximumValue();
    }

    /**
     * Class MaximumValue
     */
    private static class MaximumValue implements CalcHandler
    {

        /** maximum. */
        private BigDecimal maximum = new BigDecimal(0);

        /**
         * (methode de remplacement) {@inheritDoc}
         * 
         * @see fr.gouv.finances.lombok.apptags.table.calc.CalcHandler#processCalcValue(java.lang.Number)
         */
        @Override
        public void processCalcValue(Number calcValue)
        {
            if (calcValue != null && maximum.compareTo(new BigDecimal(String.valueOf(calcValue))) < 0)
            {
                maximum = new BigDecimal(String.valueOf(calcValue));
            }
        }

        /**
         * Accesseur de l attribut maximum value.
         * 
         * @return maximum value
         */
        public Number getMaximumValue()
        {
            return maximum;
        }
    }
}
