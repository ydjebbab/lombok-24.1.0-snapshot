/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.apptags.table.cell;

import fr.gouv.finances.lombok.apptags.util.format.FormaterDate;

/**
 * Class HeureMinuteSecondeCell
 */
public class HeureMinuteSecondeCell extends AbstractDateCell
{

    /** format pattern. */
    protected String FORMAT_PATTERN = FormaterDate.F_HE_MI_SE;

    /**
     * Instanciation de heure minute seconde cell.
     */
    public HeureMinuteSecondeCell()
    {
        super();
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.cell.AbstractDateCell#getFormatPattern()
     */
    @Override
    public String getFormatPattern()
    {
        return FORMAT_PATTERN;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.cell.PatternCell#getAlignmentCell()
     */
    @Override
    public int getAlignmentCell()
    {
        return PatternCell.HORIZONTAL_ALIGN_CENTER;
    }
}
