/*
 * Copyright (c) 2014 DGFiP - Tous droits réservés
 *//*
/*Copyright (c) 2014 DGFiP - Tous droits réservés/* * Copyright 2004 original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package fr.gouv.finances.lombok.apptags.table.view;

import fr.gouv.finances.lombok.apptags.table.bean.Export;
import fr.gouv.finances.lombok.apptags.table.core.Messages;
import fr.gouv.finances.lombok.apptags.table.core.TableModel;
import fr.gouv.finances.lombok.apptags.table.view.html.BuilderConstants;
import fr.gouv.finances.lombok.apptags.table.view.html.BuilderUtils;
import fr.gouv.finances.lombok.apptags.table.view.html.TableBuilder;
import fr.gouv.finances.lombok.apptags.table.view.html.ToolbarBuilder;
import fr.gouv.finances.lombok.apptags.table.view.html.TwoColumnTableLayout;
import fr.gouv.finances.lombok.apptags.util.HtmlBuilder;

/**
 * Class LimitToolbar
 */
public class LimitToolbar extends TwoColumnTableLayout
{

    /** messages. */
    private Messages messages;

    /**
     * Instanciation de limit toolbar.
     * 
     * @param html
     * @param model
     */
    public LimitToolbar(HtmlBuilder html, TableModel model)
    {
        super(html, model);
        messages = model.getMessages();
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.view.html.TwoColumnTableLayout#showLayout(fr.gouv.finances.lombok.apptags.table.core.TableModel)
     */
    @Override
    protected boolean showLayout(TableModel model)
    {
        boolean result = true;
        boolean showPagination = BuilderUtils.showPagination(model);
        boolean showExports = BuilderUtils.showExports(model);

        if (!showPagination && !showExports)
        {
            result = false;
        }

        return result;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.view.html.TwoColumnTableLayout#columnLeft(fr.gouv.finances.lombok.apptags.util.HtmlBuilder,
     *      fr.gouv.finances.lombok.apptags.table.core.TableModel)
     */
    @Override
    protected void columnLeft(HtmlBuilder html, TableModel model)
    {
        html.td(2).close();
        new TableBuilder(html, model).title();
        html.tdEnd();
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.view.html.TwoColumnTableLayout#columnLeft(fr.gouv.finances.lombok.apptags.util.HtmlBuilder,
     *      fr.gouv.finances.lombok.apptags.table.core.TableModel)
     */
    @Override
    protected void columnLeftRWD(HtmlBuilder html, TableModel model)
    {
        html.div().close();
        new TableBuilder(html, model).title();
        html.divEnd();
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.view.html.TwoColumnTableLayout#columnRight(fr.gouv.finances.lombok.apptags.util.HtmlBuilder,
     *      fr.gouv.finances.lombok.apptags.table.core.TableModel)
     */
    @Override
    protected void columnRight(HtmlBuilder html, TableModel model)
    {
        boolean showPagination = BuilderUtils.showPagination(model);
        boolean showExports = BuilderUtils.showExports(model);

        ToolbarBuilder toolbarBuilder = new ToolbarBuilder(html, model);
        // amlp 09/09 rajout if pour eviter balise tr vide
        // dans classe toolbar

        if (showPagination || showExports)
        {
            html.td(2).styleClass("alignRight").close();

            html.table(2).styleClass(BuilderConstants.TOOLBAR_CSS + " noBorder noCellPadding cellSpacing1").close();

            html.tr(3).close();
        }

        if (showPagination)
        {

            html.td(4).close();
            toolbarBuilder.firstPageItemAsImage();
            html.tdEnd();

            html.td(4).close();
            toolbarBuilder.prevPageItemAsImage();
            html.tdEnd();

            html.td(4).close();
            toolbarBuilder.nextPageItemAsImage();
            html.tdEnd();

            html.td(4).close();
            toolbarBuilder.separator();
            html.tdEnd();

            html.td(4).style("width:20px").close();
            html.newline();
            html.tabs(4);
            toolbarBuilder.rowsDisplayedDroplist();
            html.img();
            html.src(BuilderUtils.getImage(model, BuilderConstants.TOOLBAR_ROWS_DISPLAYED_IMAGE));
            html.alt(messages.getMessage(BuilderConstants.TOOLBAR_ROWS_DISPLAYED_TEXT));
            html.style("border:0");
            html.xclose();
            html.tdEnd();

            if (showExports)
            {
                html.td(4).close();
                toolbarBuilder.separator();
                html.tdEnd();
            }
        }

        if (showExports)
        {
            int showExportsWithLimitSizePdf = BuilderUtils.showExportsWithLimitSizePdf(model);
            int showExportsWithLimitSizeXlsOrOds = BuilderUtils.showExportsWithLimitSizeXlsOrOds(model);
            int showExportsWithLimitSizeCsv = BuilderUtils.showExportsWithLimitSizeCsv(model);
            for (Export export : model.getExportHandler().getExports())
            {
                html.td(4).close();
                if (((export.getImageName().toUpperCase().endsWith("PDF") == true) && model.getCollectionOfFilteredBeans().size() < showExportsWithLimitSizePdf)
                    || ((export.getImageName().toUpperCase().endsWith("XLS") == true || export.getImageName().toUpperCase().endsWith("ODS") == true) && model
                        .getCollectionOfFilteredBeans().size() < showExportsWithLimitSizeXlsOrOds)
                    || (export.getImageName().toUpperCase().endsWith("CSV") == true && model.getCollectionOfFilteredBeans().size() < showExportsWithLimitSizeCsv))

                {
                    toolbarBuilder.exportItemAsImage(export);
                }
                else
                {
                    html.append("veuillez filtrer votre liste pour accéder aux exports " + export.getImageName());
                }
                html.tdEnd();
            }
        }
        // amlp 09/09 rajout if pour eviter balise tr vide
        // dans classe toolbar
        if (showPagination || showExports)
        {
            html.trEnd(3);

            html.tableEnd(2);
            html.newline();
            html.tabs(2);

            html.tdEnd();
        }
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.view.html.TwoColumnRowLayout#columnRight
     *      (fr.gouv.finances.lombok.apptags.util.HtmlBuilder, fr.gouv.finances.lombok.apptags.table.core.TableModel)
     */
    @Override
    protected void columnRightRWD(HtmlBuilder html, TableModel model)
    {
        boolean showPagination = BuilderUtils.showPagination(model);
        boolean showExports = BuilderUtils.showExports(model);
        String paginationClass = BuilderUtils.getPaginationClass(model);
        String exportsClass = BuilderUtils.getExportsClass(model);

        ToolbarBuilder toolbarBuilder = new ToolbarBuilder(html, model);

        // amlp 09/09 rajout if pour eviter balise tr vide
        // dans classe toolbar

        if (showPagination || showExports)
        {
            html.div().styleClass("toolBarRWD container-fluid").close();
        }

        if (showPagination)
        {
            html.ul().styleClass("pageScroll " + paginationClass).close();

            html.li("").close();
            toolbarBuilder.firstPageItemAsImage();
            html.liEnd();

            html.li("").close();
            toolbarBuilder.prevPageItemAsImage();
            html.liEnd();

            html.li("").close();
            toolbarBuilder.nextPageItemAsImage();
            html.liEnd();

            html.li("").close();
            toolbarBuilder.lastPageItemAsImage();
            html.liEnd();

            html.li("").close();
            toolbarBuilder.separator();
            html.liEnd();

            html.li("").close();
            toolbarBuilder.rowsDisplayedDroplist();
            html.liEnd();

            html.ulEnd();
        }

        if (showExports)
        {

            int showExportsWithLimitSizePdf = BuilderUtils.showExportsWithLimitSizePdf(model);
            int showExportsWithLimitSizeXlsOrOds = BuilderUtils.showExportsWithLimitSizeXlsOrOds(model);
            int showExportsWithLimitSizeCsv = BuilderUtils.showExportsWithLimitSizeCsv(model);

            html.ul().styleClass("exports " + exportsClass).close();

            html.li("").close();
            html.a().append(".collapseExports").dataAttributes("data-toggle=collapse").styleClass("btn").close().append("exports").aEnd();
            html.liEnd();

            for (Export export : model.getExportHandler().getExports())
            {
                html.li("").styleClass("collapseExports collapse").close();
                if (((export.getImageName().toUpperCase().endsWith("PDF") == true) && model.getCollectionOfFilteredBeans().size() < showExportsWithLimitSizePdf)
                    || ((export.getImageName().toUpperCase().endsWith("XLS") == true || export.getImageName().toUpperCase().endsWith("ODS") == true) && model
                        .getCollectionOfFilteredBeans().size() < showExportsWithLimitSizeXlsOrOds)
                    || (export.getImageName().toUpperCase().endsWith("CSV") == true && model.getCollectionOfFilteredBeans().size() < showExportsWithLimitSizeCsv))
                {
                    toolbarBuilder.exportItemAsImage(export);
                }
                else
                {
                    html.append("veuillez filtrer votre liste pour accéder aux exports " + export.getImageName());
                }
                html.liEnd();
            }
            html.ulEnd();
        }

        if (showPagination || showExports)
        {
            html.divEnd();
        }
    }

}
