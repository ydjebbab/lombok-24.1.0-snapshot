/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : NamePropertyTag.java
 *
 */
package fr.gouv.finances.lombok.apptags.base;

import javax.servlet.jsp.JspException;

/**
 * Class NamePropertyTag --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.2 $ Date: 11 déc. 2009
 */
public abstract class NamePropertyTag extends BaseAppTags
{

    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** name. */
    protected String name = null;

    /** property. */
    protected String property = null;

    /** key. */
    protected Object key = null;

    /** keyed object property. */
    protected String keyedObjectProperty = null;

    /** scope. */
    protected String scope = null;

    /**
     * Constructeur de la classe NamePropertyTag.java
     *
     */
    public NamePropertyTag()
    {
        super(); 
        
    }

    /**
     * Accesseur de l attribut key.
     * 
     * @return key
     */
    public Object getKey()
    {
        return key;
    }

    /**
     * Accesseur de l attribut keyed object property.
     * 
     * @return keyed object property
     */
    public String getKeyedObjectProperty()
    {
        return keyedObjectProperty;
    }

    /**
     * Accesseur de l attribut name.
     * 
     * @return name
     */
    public String getName()
    {
        return name;
    }

    /**
     * Accesseur de l attribut property.
     * 
     * @return property
     */
    public String getProperty()
    {
        return property;
    }

    /**
     * Accesseur de l attribut scope.
     * 
     * @return scope
     */
    public String getScope()
    {
        return scope;
    }

    /**
     * Modificateur de l attribut key.
     * 
     * @param key le nouveau key
     */
    public void setKey(Object key)
    {
        this.key = key;
    }

    /**
     * Modificateur de l attribut keyed object property.
     * 
     * @param keyedObjectProperty le nouveau keyed object property
     */
    public void setKeyedObjectProperty(String keyedObjectProperty)
    {
        this.keyedObjectProperty = keyedObjectProperty;
    }

    /**
     * Modificateur de l attribut name.
     * 
     * @param name le nouveau name
     */
    public void setName(String name)
    {
        this.name = name;
    }

    /**
     * Modificateur de l attribut property.
     * 
     * @param property le nouveau property
     */
    public void setProperty(String property)
    {
        this.property = property;
    }

    /**
     * Modificateur de l attribut scope.
     * 
     * @param scope le nouveau scope
     */
    public void setScope(String scope)
    {
        this.scope = scope;
    }

    /**
     * Accesseur de l attribut bean.
     * 
     * @return bean
     * @throws JspException the jsp exception
     */
    protected Object getBean() throws JspException
    {
        return getBean(getName(), getProperty(), getKey(), getKeyedObjectProperty(), getScope());
    }

}
