/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.apptags.table.view;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;

import fr.gouv.finances.lombok.apptags.table.bean.Column;
import fr.gouv.finances.lombok.apptags.table.bean.Export;
import fr.gouv.finances.lombok.apptags.table.calc.CalcUtils;
import fr.gouv.finances.lombok.apptags.table.calc.CpCalcResult;
import fr.gouv.finances.lombok.apptags.table.calc.CpCalcUtils;
import fr.gouv.finances.lombok.apptags.table.core.TableModel;
import fr.gouv.finances.lombok.apptags.util.format.FormaterNombre;

/**
 * Class CpPaysagePdfView
 */
public class CpPaysagePdfView implements View
{

    /** Constant : PACKAGE_CELL. */
    private static final String PACKAGE_CELL = "fr.gouv.finances.lombok.apptags.table.cell";

    /** Constant : PAT_RIGHT_ALIGNED. */
    private static final String PAT_RIGHT_ALIGNED = "text-align:right";

    /** Constant : PAT_LEFT_ALIGNED. */
    private static final String PAT_LEFT_ALIGNED = "text-align:left";

    /** Constant : PAT_CENTER_ALIGNED. */
    private static final String PAT_CENTER_ALIGNED = "text-align:center";

    /** Constant : FONT. */
    public static final String FONT = "exportPdf.font";

    /** Constant : HEADER_BACKGROUND_COLOR. */
    public static final String HEADER_BACKGROUND_COLOR = "headerBackgroundColor";

    /** Constant : HEADER_TITLE. */
    public static final String HEADER_TITLE = "headerTitle";

    /** Constant : HEADER_COLOR. */
    public static final String HEADER_COLOR = "headerColor";

    /** xlsfo. */
    private final StringBuilder xlsfo = new StringBuilder(200);

    /** font. */
    private String font;
    
    /**
     * Instanciation de cp paysage pdf view.
     */
    public CpPaysagePdfView()
    {
    }

    /**
     * Verifie si numeric.
     * 
     * @param value
     * @return true, si c'est numeric
     */
    private static final boolean isNumeric(Object value)
    {
        boolean result = false;
        if (value instanceof Number)
        {
            result = true;
        }

        return result;
    }

    // fin ajouts jvp



    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.view.View#beforeBody
     *      (fr.gouv.finances.lombok.apptags.table.core.TableModel)
     */
    @Override
    public void beforeBody(TableModel model)
    {
        this.font = model.getPreferences().getPreference(FONT);

        xlsfo.append(startRoot());
        xlsfo.append(regionBefore(model));
        xlsfo.append(regionAfter());
        xlsfo.append(columnDefinitions(model));
        xlsfo.append(header(model));
        xlsfo.append(" <fo:table-body> ");
    }

    /**
     * Verifie si aligned.
     * 
     * @param patternS
     * @param leTypeDeStyle
     * @return true, si c'est aligned
     */
    private boolean isAligned(String patternS, String leTypeDeStyle)
    {
        Pattern pattern = Pattern.compile(patternS);
        String syleEpure = StringUtils.replace(leTypeDeStyle, " ", "");
        Matcher matcher = pattern.matcher(syleEpure);
        return matcher.find();
    }

    /**
     * methode Affect style :
     * 
     * @param column
     * @return string
     */
    private String affectStyle(Column column)
    {
        StringBuilder leStyleApplique = new StringBuilder(30);
        String leTypeDeStyle = column.getStyle();

        if (leTypeDeStyle != null)
        {
            if (isAligned(PAT_RIGHT_ALIGNED, leTypeDeStyle))
            {
                leStyleApplique.append(" text-align=\"right\" ");
            }

            else if (isAligned(PAT_LEFT_ALIGNED, leTypeDeStyle))
            {
                leStyleApplique.append(" text-align=\"left\" ");
            }

            else if (isAligned(PAT_CENTER_ALIGNED, leTypeDeStyle))
            {
                leStyleApplique.append(" text-align=\"center\" ");
            }
        }
        return leStyleApplique.toString();
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.view.View#body (fr.gouv.finances.lombok.apptags.table.core.TableModel,
     *      fr.gouv.finances.lombok.apptags.table.bean.Column)
     */
    @Override
    public void body(TableModel model, Column column)
    {
        if (column.isFirstColumn())
        {
            xlsfo.append(" <fo:table-row> ");
        }

        String valeur = ExportViewUtils.parsePDF(column.getCellDisplay());
        Object value = column.getPropertyValue();
        StringBuilder valeurPourPdf = new StringBuilder();

        xlsfo.append(" <fo:table-cell border=\"solid silver .5px\" display-align=\"center\" padding=\"3pt\"> ");

        // on teste si le string est numérique
        if (isNumeric(value))
        {

            String leTypeDeCellule = column.getCell();

            if (leTypeDeCellule.compareTo(PACKAGE_CELL + ".NombreCellZeroDecimale") == 0)
            {
                valeurPourPdf.append(FormaterNombre.nombreNDecimalesPourPdf(value, 0));
            }

            else if (leTypeDeCellule.compareTo(PACKAGE_CELL + ".NombreCellUneDecimale") == 0)
            {
                valeurPourPdf.append(FormaterNombre.nombreNDecimalesPourPdf(value, 1));
            }

            else if (leTypeDeCellule.compareTo(PACKAGE_CELL + ".NombreCellDeuxDecimales") == 0)
            {
                valeurPourPdf.append(FormaterNombre.nombreNDecimalesPourPdf(value, 2));
            }

            else if (leTypeDeCellule.compareTo(PACKAGE_CELL + ".NombreCellTroisDecimales") == 0)
            {
                valeurPourPdf.append(FormaterNombre.nombreNDecimalesPourPdf(value, 3));
            }

            else if (leTypeDeCellule.compareTo(PACKAGE_CELL + ".NombreCellQuatreDecimales") == 0)
            {
                valeurPourPdf.append(FormaterNombre.nombreNDecimalesPourPdf(value, 4));
            }

            else if (leTypeDeCellule.compareTo(PACKAGE_CELL + ".NombreCellCinqDecimales") == 0)
            {
                valeurPourPdf.append(FormaterNombre.nombreNDecimalesPourPdf(value, 5));
            }

            else if (leTypeDeCellule.compareTo(PACKAGE_CELL + ".NombreCellSixDecimales") == 0)
            {
                valeurPourPdf.append(FormaterNombre.nombreNDecimalesPourPdf(value, 6));
            }

            else if (leTypeDeCellule.compareTo(PACKAGE_CELL + ".NombreCellSeptDecimales") == 0)
            {
                valeurPourPdf.append(FormaterNombre.nombreNDecimalesPourPdf(value, 7));
            }

            else if (leTypeDeCellule.compareTo(PACKAGE_CELL + ".NombreCellHuitDecimales") == 0)
            {
                valeurPourPdf.append(FormaterNombre.nombreNDecimalesPourPdf(value, 8));
            }

            else if (leTypeDeCellule.compareTo(PACKAGE_CELL + ".NombreCellNeufDecimales") == 0)
            {
                valeurPourPdf.append(FormaterNombre.nombreNDecimalesPourPdf(value, 9));
            }

            else if (leTypeDeCellule.compareTo(PACKAGE_CELL + ".NombreCellDixDecimales") == 0)
            {
                valeurPourPdf.append(FormaterNombre.nombreNDecimalesPourPdf(value, 10));
            }

            else if (leTypeDeCellule.compareTo(PACKAGE_CELL + ".MonnaieCellZeroDecimale") == 0)
            {
                valeurPourPdf.append(FormaterNombre.monnaieNDecimalesPourPdf(value, 0));
            }

            else if (leTypeDeCellule.compareTo(PACKAGE_CELL + ".MonnaieCellUneDecimale") == 0)
            {
                valeurPourPdf.append(FormaterNombre.monnaieNDecimalesPourPdf(value, 1));
            }

            else if (leTypeDeCellule.compareTo(PACKAGE_CELL + ".MonnaieCellDeuxDecimales") == 0)
            {
                valeurPourPdf.append(FormaterNombre.monnaieNDecimalesPourPdf(value, 2));
            }

            else if (leTypeDeCellule.compareTo(PACKAGE_CELL + ".MonnaieCellTroisDecimales") == 0)
            {
                valeurPourPdf.append(FormaterNombre.monnaieNDecimalesPourPdf(value, 3));
            }

            else if (leTypeDeCellule.compareTo(PACKAGE_CELL + ".MonnaieCellQuatreDecimales") == 0)
            {
                valeurPourPdf.append(FormaterNombre.monnaieNDecimalesPourPdf(value, 4));
            }

            else if (leTypeDeCellule.compareTo(PACKAGE_CELL + ".MonnaieCellCinqDecimales") == 0)
            {
                valeurPourPdf.append(FormaterNombre.monnaieNDecimalesPourPdf(value, 5));
            }

            else if (leTypeDeCellule.compareTo(PACKAGE_CELL + ".MonnaieCellSixDecimales") == 0)
            {
                valeurPourPdf.append(FormaterNombre.monnaieNDecimalesPourPdf(value, 6));
            }

            else if (leTypeDeCellule.compareTo(PACKAGE_CELL + ".MonnaieCellSeptDecimales") == 0)
            {
                valeurPourPdf.append(FormaterNombre.monnaieNDecimalesPourPdf(value, 7));
            }

            else if (leTypeDeCellule.compareTo(PACKAGE_CELL + ".MonnaieCellHuitDecimales") == 0)
            {
                valeurPourPdf.append(FormaterNombre.monnaieNDecimalesPourPdf(value, 8));
            }

            else if (leTypeDeCellule.compareTo(PACKAGE_CELL + ".MonnaieCellNeufDecimales") == 0)
            {
                valeurPourPdf.append(FormaterNombre.monnaieNDecimalesPourPdf(value, 9));
            }

            else if (leTypeDeCellule.compareTo(PACKAGE_CELL + ".MonnaieCellDixDecimales") == 0)
            {
                valeurPourPdf.append(FormaterNombre.monnaieNDecimalesPourPdf(value, 10));
            }

            else if (leTypeDeCellule.compareTo(PACKAGE_CELL + ".PourCentCellZeroDecimale") == 0)
            {
                valeurPourPdf.append(FormaterNombre.pourcentNDecimalesPourPdf(value, 0));
            }

            else if (leTypeDeCellule.compareTo(PACKAGE_CELL + ".PourCentCellUneDecimale") == 0)
            {
                valeurPourPdf.append(FormaterNombre.pourcentNDecimalesPourPdf(value, 1));
            }

            else if (leTypeDeCellule.compareTo(PACKAGE_CELL + ".PourCentCellDeuxDecimales") == 0)
            {
                valeurPourPdf.append(FormaterNombre.pourcentNDecimalesPourPdf(value, 2));
            }

            else if (leTypeDeCellule.compareTo(PACKAGE_CELL + ".PourCentCellTroisDecimales") == 0)
            {
                valeurPourPdf.append(FormaterNombre.pourcentNDecimalesPourPdf(value, 3));
            }

            else if (leTypeDeCellule.compareTo(PACKAGE_CELL + ".PourCentCellQuatreDecimales") == 0)
            {
                valeurPourPdf.append(FormaterNombre.pourcentNDecimalesPourPdf(value, 4));
            }

            else if (leTypeDeCellule.compareTo(PACKAGE_CELL + ".PourCentCellCinqDecimales") == 0)
            {
                valeurPourPdf.append(FormaterNombre.pourcentNDecimalesPourPdf(value, 5));
            }

            else if (leTypeDeCellule.compareTo(PACKAGE_CELL + ".PourCentCellSixDecimales") == 0)
            {
                valeurPourPdf.append(FormaterNombre.pourcentNDecimalesPourPdf(value, 6));
            }

            else if (leTypeDeCellule.compareTo(PACKAGE_CELL + ".PourCentCellSeptDecimales") == 0)
            {
                valeurPourPdf.append(FormaterNombre.pourcentNDecimalesPourPdf(value, 7));
            }

            else if (leTypeDeCellule.compareTo(PACKAGE_CELL + ".PourCentCellHuitDecimales") == 0)
            {
                valeurPourPdf.append(FormaterNombre.pourcentNDecimalesPourPdf(value, 8));
            }

            else if (leTypeDeCellule.compareTo(PACKAGE_CELL + ".PourCentCellNeufDecimales") == 0)
            {
                valeurPourPdf.append(FormaterNombre.pourcentNDecimalesPourPdf(value, 9));
            }

            else if (leTypeDeCellule.compareTo(PACKAGE_CELL + ".PourCentCellDixDecimales") == 0)
            {
                valeurPourPdf.append(FormaterNombre.pourcentNDecimalesPourPdf(value, 10));
            }

            else
            {
                valeurPourPdf.append(valeur);
            }

        }
        else
        {

            valeurPourPdf.append(valeur);

        }

        String leStyleApplique = affectStyle(column);
        xlsfo.append(" <fo:block");
        xlsfo.append(getFont());
        xlsfo.append(leStyleApplique);
        xlsfo.append('>');
        xlsfo.append(valeurPourPdf);
        xlsfo.append("</fo:block> ");

        // fin tests jvp

        xlsfo.append(" </fo:table-cell> ");

        if (column.isLastColumn())
        {
            xlsfo.append(" </fo:table-row> ");
        }
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.view.View#afterBody
     *      (fr.gouv.finances.lombok.apptags.table.core.TableModel)
     */
    @Override
    public Object afterBody(TableModel model)
    {
        if (model.getLimit().getTotalRows() != 0)
        {
            xlsfo.append(totals(model));
        }
        xlsfo.append(" </fo:table-body> ");
        xlsfo.append(endRoot());
        return xlsfo.toString();
    }

    /**
     * methode Start root :
     * 
     * @return string
     */
    public String startRoot()
    {
        StringBuilder sb = new StringBuilder(600);

        sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");

        sb.append("<fo:root xmlns:fo=\"http://www.w3.org/1999/XSL/Format\">");

        sb.append(" <fo:layout-master-set> ");
        sb.append(" <fo:simple-page-master master-name=\"simple\" ");
        sb.append(" page-height=\"8.5in\" ");
        sb.append(" page-width=\"11in\" ");
        sb.append(" margin-top=\".5in\" ");
        sb.append(" margin-bottom=\".25in\" ");
        sb.append(" margin-left=\".25in\" ");
        sb.append(" margin-right=\".25in\"> ");
        sb.append(" <fo:region-body margin-top=\".5in\" margin-bottom=\".25in\"/> ");
        sb.append(" <fo:region-before extent=\".5in\"/> ");
        sb.append(" <fo:region-after extent=\".25in\"/> ");
        sb.append(" </fo:simple-page-master> ");
        sb.append(" </fo:layout-master-set> ");

        // WP ajout de la langue pour permettre la cesure des mots
        sb.append(" <fo:page-sequence  language=\"fr\" master-reference=\"simple\" initial-page-number=\"1\"> ");

        return sb.toString();
    }

    /**
     * methode Region before :
     * 
     * @param model
     * @return string
     */
    public String regionBefore(TableModel model)
    {
        StringBuilder sbf = new StringBuilder(200);

        Export export = model.getExportHandler().getCurrentExport();

        String headerBackgroundColor = export.getAttributeAsString(HEADER_BACKGROUND_COLOR);

        sbf.append(" <fo:static-content flow-name=\"xsl-region-before\"> ");

        String title = export.getAttributeAsString(HEADER_TITLE);

        // WP ajout propriété hyphenate
        sbf.append(" <fo:block hyphenate=\"true\" space-after.optimum=\"15pt\" color=\"");
        sbf.append(headerBackgroundColor);
        sbf.append("\" font-size=\"17pt\" font-family=\"");
        sbf.append(getHeadFont());
        sbf.append("'Times'\">");
        sbf.append(title);
        sbf.append("</fo:block> ");

        sbf.append(" </fo:static-content> ");

        return sbf.toString();
    }

    /**
     * methode Region after :
     * 
     * @return string
     */
    public String regionAfter()
    {
        StringBuilder sbf = new StringBuilder(200);

        sbf.append(" <fo:static-content flow-name=\"xsl-region-after\" display-align=\"after\"> ");

        // WP ajout propriété hyphenate
        sbf.append(" <fo:block hyphenate=\"true\" text-align=\"end\">Page <fo:page-number/></fo:block> ");

        sbf.append(" </fo:static-content> ");

        return sbf.toString();
    }

    /**
     * methode Column definitions :
     * 
     * @param model
     * @return string
     */
    public String columnDefinitions(TableModel model)
    {
        StringBuilder sbf = new StringBuilder(200);

        sbf.append(" <fo:flow flow-name=\"xsl-region-body\"> ");

        // WP ajout propriété hyphenate
        sbf.append(" <fo:block hyphenate=\"true\" ");
        sbf.append(getFont());
        sbf.append('>');

        sbf.append(" <fo:table table-layout=\"fixed\" font-size=\"8pt\"> ");

        double columnCount = model.getColumnHandler().columnCount();

        // test jvp

        List<Column> listeDesColonnes = model.getColumnHandler().getColumns();

        // on teste si le paramètre width est servi pour toutes les
        // colonnes

        boolean widthServiPourToutesColonnes = true;

        for (int i = 0; i < columnCount; i++)
        {

            Column uneColonne = (Column) listeDesColonnes.get(i);

            if (uneColonne.getWidth() == null)
            {

                widthServiPourToutesColonnes = false;
                break;

            }

        }
        // si le paramètre width est connu pour toutes les colonnes =>
        // on l'utilise
        if (widthServiPourToutesColonnes)
        {

            double colwidth = 0;

            for (int i = 1; i <= columnCount; i++)
            {

                Column uneColonne = (Column) listeDesColonnes.get(i - 1);

                String colwidthString = uneColonne.getWidth();

                String[] colwidthPourCent = colwidthString.split("%");

                String colwidthPourCentString = colwidthPourCent[0];

                Double colwidthPourCentDouble = Double.valueOf(colwidthPourCentString);

                colwidth = colwidthPourCentDouble.doubleValue() / 10;

                sbf.append(" <fo:table-column column-number=\"" + i + "\" column-width=\"" + colwidth + "in\"/> ");

            }

        }
        // si le paramètre width est inconnu pour 1 colonne au moins, on
        // divise le tableau en N colonnes de même largeur
        else
        {

            double colwidth = 10 / columnCount;

            for (int i = 1; i <= columnCount; i++)
            {

                sbf.append(" <fo:table-column column-number=\"" + i + "\" column-width=\"" + colwidth + "in\"/> ");

            }
        }

        return sbf.toString();
    }

    /**
     * methode Header :
     * 
     * @param model
     * @return string
     */
    public String header(TableModel model)
    {
        StringBuilder sbf = new StringBuilder(300);

        Export export = model.getExportHandler().getCurrentExport();
        String headerColor = export.getAttributeAsString(HEADER_COLOR);
        String headerBackgroundColor = export.getAttributeAsString(HEADER_BACKGROUND_COLOR);

        sbf.append(" <fo:table-header background-color=\"");
        sbf.append(headerBackgroundColor);
        sbf.append("\" color=\"");
        sbf.append(headerColor);
        sbf.append("\"> ");

        sbf.append(" <fo:table-row> ");

        List<Column> columns = model.getColumnHandler().getHeaderColumns();
        for (Column column : columns)
        {
            String title = column.getCellDisplay();
            sbf
                .append(" <fo:table-cell border=\"solid silver .5px\" " +
                    "text-align=\"center\" display-align=\"center\" padding=\"3pt\"> ");
            sbf.append(" <fo:block hyphenate=\"true\" ");
            sbf.append(getFont());
            sbf.append('>');
            sbf.append(title);
            sbf.append("</fo:block> ");
            sbf.append(" </fo:table-cell> ");
        }

        sbf.append(" </fo:table-row> ");

        sbf.append(" </fo:table-header> ");

        return sbf.toString();
    }

    /**
     * methode End root :
     * 
     * @return string
     */
    public String endRoot()
    {
        StringBuilder sb = new StringBuilder(71);

        sb.append(" </fo:table> ");

        sb.append(" </fo:block> ");

        sb.append(" </fo:flow> ");

        sb.append(" </fo:page-sequence> ");

        sb.append(" </fo:root> ");

        return sb.toString();
    }

    /**
     * Accesseur de l attribut font.
     * 
     * @return font
     */
    protected String getFont()
    {
        return font == null ? "" : " font-family=\"" + font + "\"";
    }

    /**
     * Accesseur de l attribut head font.
     * 
     * @return head font
     */
    protected String getHeadFont()
    {
        return font == null ? "" : font + ",";
    }

    /**
     * TWEST - New Method that answers a StringBuilder containing the totals information. If no totals exist on the
     * model answer an empty buffer. The totals row will be given the same style as the header row.
     * 
     * @param model
     * @return StringBuilder containing the complete fo statement for totals
     */
    public StringBuilder totals(TableModel model)
    {

        StringBuilder sbf = new StringBuilder();
        Export export = model.getExportHandler().getCurrentExport();
        String headerColor = export.getAttributeAsString(HEADER_COLOR);
        String headerBackgroundColor = export.getAttributeAsString(HEADER_BACKGROUND_COLOR);

        Column firstCalcColumn = model.getColumnHandler().getFirstCalcColumn();

        if (firstCalcColumn != null)
        {
            int rows = firstCalcColumn.getCalc().size();
            for (int i = 0; i < rows; i++)
            {
                sbf.append("<fo:table-row>");

                for (Column column : model.getColumnHandler().getColumns())
                {
                    if (column.isFirstColumn())
                    {
                        String calcTitle = CalcUtils.getFirstCalcColumnTitleByPosition(model, i);
                        sbf
                            .append(" <fo:table-cell border=\"solid silver .5px\" " +
                                "text-align=\"center\" display-align=\"center\" padding=\"3pt\" background-color=\"");
                        sbf.append(headerBackgroundColor);
                        sbf.append("\" color=\"");
                        sbf.append(headerColor);
                        sbf.append("\">");
                        sbf.append(" <fo:block ");
                        sbf.append(getFont());
                        sbf.append('>');
                        sbf.append(calcTitle);
                        sbf.append(" </fo:block></fo:table-cell> ");
                        continue;
                    }
                    if (column.isCalculated())
                    {
                        sbf
                            .append(" <fo:table-cell border=\"solid silver .5px\" " +
                                "text-align=\"center\" display-align=\"center\" padding=\"3pt\" background-color=\"");
                        sbf.append(headerBackgroundColor);
                        sbf.append("\" color=\"");
                        sbf.append(headerColor);
                        sbf.append("\"> ");
                        String leStyleApplique = affectStyle(column);
                        sbf.append(" <fo:block");
                        sbf.append(getFont());
                        sbf.append(leStyleApplique);
                        sbf.append('>');
                        CpCalcResult calcResult = CpCalcUtils.getCalcResultsByPosition(model, column, i);
                        Number value = calcResult.getValue();
                        String toDisplay =
                            (value == null ? calcResult.getName() : CpCalcUtils.findValueFormated(model, column, value,
                                i));

                        if (value != null)
                        {
                            sbf.append(toDisplay);
                        }
                        else
                        {
                            sbf.append("");
                        }

                        sbf.append("</fo:block> ");

                    }
                    else
                    {
                        sbf
                            .append(" <fo:table-cell border=\"solid silver .5px\"" +
                                " text-align=\"center\" display-align=\"center\" padding=\"3pt\" background-color=\"");
                        sbf.append(headerBackgroundColor);
                        sbf.append("\" color=\"");
                        sbf.append(headerColor);
                        sbf.append("\"> ");
                        sbf.append(" <fo:block ");
                        sbf.append(getFont());
                        sbf.append('>');
                        sbf.append(" ");
                        sbf.append("</fo:block> ");
                    }
                    sbf.append(" </fo:table-cell> ");
                }
                sbf.append("</fo:table-row>");
            }

        }
        return sbf;
    }

}
