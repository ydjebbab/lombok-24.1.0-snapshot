/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : BaseTag.java
 *
 */
package fr.gouv.finances.lombok.apptags.base;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Map;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.Tag;
import javax.servlet.jsp.tagext.TagSupport;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import fr.gouv.finances.lombok.apptags.util.ClassUtil;
import fr.gouv.finances.lombok.apptags.util.RequestUtil;

/**
 * Class BaseTag --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.2 $ Date: 11 déc. 2009
 */
public class BaseTag extends TagSupport
{

    /** Constant : NODE_PAGE_ATTRIBUTE. */
    public static final String NODE_PAGE_ATTRIBUTE = "fr.gouv.finances.lombok.apptags.tree.impl.BaseTag.node";

    /** Constant : NODE_INDENTATION_TYPE_PAGE_ATTRIBUTE. */
    public static final String NODE_INDENTATION_TYPE_PAGE_ATTRIBUTE =
        "fr.gouv.finances.lombok.apptags.tree.impl.BaseTag.nodeIndentationType";

    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** log. */
    protected final Log log = LogFactory.getLog(getClass());

    /** debug. */
    protected String debug = null;

    /**
     * Constructeur de la classe BaseTag.java
     *
     */
    public BaseTag()
    {
        super();
        
    }

    /**
     * methode Debug : --.
     * 
     * @param text --
     * @throws JspException the jsp exception
     */
    public void debug(String text) throws JspException
    {
        if (log.isDebugEnabled())
        {
            log.debug(text);
        }
    }

    /**
     * Accesseur de l attribut ancestor.
     * 
     * @param type --
     * @return ancestor
     */
    protected Tag getAncestor(Class type)
    {
        Tag parent = getParent();
        while (parent != null && !type.equals(parent.getClass()))
        {
            parent = parent.getParent();
        }
        return parent;
    }

    /**
     * Retourne la propriété d'un objet stocké dans un scope donné. Si le bean identifié par son nom ou propriété est de
     * type java.util.Map et si le paramètre 'key' n'est pas null, le paramètre 'key' est utilisé pour rechercher le
     * bean dans la Map. Si le paramètre 'key' est non null et si la propriété n'est pas une Map, une exception est
     * levée
     * 
     * @param name --
     * @param property la propriété à lire
     * @param key La clé du bean stocké dans la Map localisée par les paramètres name : property
     * @param scope Le scope où le bean est stocké. Null indique que la recherche doit être effectuée dans tous les
     *        scopes. Les scopes valides sont 'request', 'session', 'page' et 'application'
     * @return La propriété du bean si elle est trouvée
     * @throws JspException Si une erreur se produit lors de la lecture de la propriété
     */
    protected Object getBean(String name, String property, Object key, String scope) throws JspException
    {
        Object result;
        Object bean = getBean(name, property, scope);

        if (key != null & bean != null)
        {
            if (!(bean instanceof Map))
            {
                StringBuilder   msg = new StringBuilder(44);
                msg.append("LE BEAN ");
                msg.append(name);
                msg.append('.');
                msg.append(property);
                msg.append(" N'EST PAS UNE INSTANCE DE java.util.Map. LE BEAN EST : ");
                String beanType =
                    (bean == null) ? "NULL" : (new StringBuilder()).append(bean.toString()).append(" (").append(
                        bean.getClass().getName()).append(")").toString();
                msg.append(beanType);

                throw new JspException(msg.toString());
            }
            result = ((Map) bean).get(key);
        }
        else
        {
            result = bean;
        }
        return result;
    }

    /**
     * Retourne la propriété d'un objet stocké dans un scope donné. Si le bean identifié par son nom ou propriété est de
     * type java.util.Map et si le paramètre 'key' n'est pas null, le paramètre 'key' est utilisé pour rechercher le
     * bean dans la Map. Si le paramètre 'key' est non null et si la propriété n'est pas une Map, une exception est
     * levée. Si le paramètre 'key' est null et le bean identifié par les paramètres name /property n'est pas de type
     * Map, le bean lui même est retourné. Si le paramètre 'property' est null, le bean identifié par le paramètre
     * 'name' sera utilisé comme Map
     * 
     * @param name --
     * @param property la propriété à lire
     * @param key La clé du bean stocké dans la Map localisée par les paramètres name : property
     * @param keyedObjectProperty --
     * @param scope Le scope où le bean est stocké. Null indique que la recherche doit être effectuée dans tous les
     *        scopes. Les scopes valides sont 'request', 'session', 'page' et 'application'
     * @return La propriété du bean si elle est trouvée
     * @throws JspException Si une erreur se produit lors de la lecture de la propriété
     */
    protected Object getBean(String name, String property, Object key, String keyedObjectProperty, String scope)
        throws JspException
    {
        Object result = null;
        Object bean = getBean(name, property, key, scope);

        if (bean == null)
        {
        }
        else if (keyedObjectProperty == null)
        {
            result = bean;
        }
        else
        {

            try
            {
                result = getProperty(keyedObjectProperty, bean);
            }
            catch (NoSuchMethodException nsme)
            {
                StringBuilder   msg = new StringBuilder(44);
                msg.append("AUCUNE PROPRIETE ");
                msg.append(keyedObjectProperty);
                msg.append("SUR L'OBJET STOCKE AVEC LA CLE ");
                msg.append(key);
                msg.append("L'OBJET EST ");
                msg.append(bean.toString());
                throw new JspException(msg.toString(), nsme);
            }
            catch (IllegalAccessException ilae)
            {
                StringBuilder   msg = new StringBuilder(44);
                msg.append("AUCUNE PROPRIETE ");
                msg.append(keyedObjectProperty);
                msg.append("SUR L'OBJET STOCKE AVEC LA CLE ");
                msg.append(key);
                msg.append("L'OBJET EST ");
                msg.append(bean.toString());
                throw new JspException(msg.toString(), ilae);
            }
            catch (InvocationTargetException ite)
            {
                StringBuilder   msg = new StringBuilder(44);
                msg.append("AUCUNE PROPRIETE ");
                msg.append(keyedObjectProperty);
                msg.append("SUR L'OBJET STOCKE AVEC LA CLE ");
                msg.append(key);
                msg.append("L'OBJET EST ");
                msg.append(bean.toString());
                throw new JspException(msg.toString(), ite);
            }
        }
        return result;
    }

    /**
     * Retourne la propriété d'un objet stocké dans un scope donné, si elle est présente.
     * 
     * @param name --
     * @param property la propriété à lire
     * @param scope Le scope où le bean est stocké. Null indique que la recherche doit être effectuée dans tous les
     *        scopes. Les scopes valides sont 'request', 'session', 'page' et 'application'
     * @return La propriété du bean si elle est trouvée
     * @throws JspException Si une erreur se produit lors de la lecture de la propriété
     */
    protected Object getBean(String name, String property, String scope) throws JspException
    {
        try
        {
            return RequestUtil.findObject(pageContext, name, property, scope);
        }
        catch (IllegalAccessException iae)
        {
            StringBuilder   msg = new StringBuilder(44);
            msg.append("ERREUR LORS DE LA LECTURE DE LA PROPRIETE : ");
            msg.append(property);
            throw new JspException(msg.toString(), iae);
        }
        catch (InvocationTargetException iae)
        {
            StringBuilder   msg = new StringBuilder(44);
            msg.append("ERREUR LORS DE LA LECTURE DE LA PROPRIETE : ");
            msg.append(property);
            throw new JspException(msg.toString(), iae);
        }
    }

    /**
     * Retourne la propriété de l'objet si elle est présente.
     * 
     * @param object l'objet contenant la propriété
     * @param property la propriété à lire
     * @return La propriété du bean si elle est trouvée
     * @throws JspException Si une erreur se produit lors de la lecture de la propriété
     */
    protected Object getBeanProperty(Object object, String property) throws JspException
    {
        try
        {
            return ClassUtil.getBean(object, property);
        }
        catch (IllegalAccessException ilae)
        {
            StringBuilder   msg = new StringBuilder(44);
            msg.append("ERREUR LORS DE LA LECTURE DE LA PROPRIETE : ");
            msg.append(property);
            throw new JspException(msg.toString(), ilae);
        }
        catch (InvocationTargetException ilae)
        {
            StringBuilder   msg = new StringBuilder(44);
            msg.append("ERREUR LORS DE LA LECTURE DE LA PROPRIETE : ");
            msg.append(property);
            throw new JspException(msg.toString(), ilae);
        }
    }

    /**
     * Accesseur de l attribut property.
     * 
     * @param keyedObjectProperty --
     * @param bean --
     * @return property
     * @throws NoSuchMethodException the no such method exception
     * @throws JspException the jsp exception
     * @throws IllegalAccessException the illegal access exception
     * @throws InvocationTargetException the invocation target exception
     */
    protected Object getProperty(String keyedObjectProperty, Object bean) throws NoSuchMethodException, JspException,
        IllegalAccessException, InvocationTargetException
    {
        String keyedObjectPropertyGetter = null;
        if (keyedObjectProperty.startsWith("get"))
        {
            keyedObjectPropertyGetter = keyedObjectProperty;
        }
        else
        {
            keyedObjectPropertyGetter =
                "get" + keyedObjectProperty.substring(0, 1).toUpperCase() + keyedObjectProperty.substring(1);
        }
        Method method = bean.getClass().getMethod(keyedObjectPropertyGetter, (Class[]) null);
        return method.invoke(bean, (Object[]) null);
    }

    /**
     * Stocke le 'bean' sous le nom passé en paramètre ('name') dans un scope donné. Si le paramètre scope est null, le
     * paramètre defautScope est utilisé Stores the given bean by the given name in the given scope, meaning the bean is
     * stored using the name as key in either request, session, page, or application context attributes.
     * 
     * @param name Nom sous lequel stocké le bean
     * @param scope Scope dans lequel le bean est enregistré. Si null, le paramètre defaultScope est utilisé. Les scopes
     *        valides sont 'request', 'session', 'page' et 'application'
     * @param defaultScope Scope a utiliser par défaut, si null utilisation du scope 'request'
     * @param bean Bean à stocké, si null, aucune opération n'est effectuée
     * @throws JspException Si le paramètre 'name' est null
     */
    protected void setBean(String name, String scope, String defaultScope, Object bean) throws JspException
    {
        if (bean == null)
        {
            return;
        }

        if (name == null)
        {
            throw new JspException("IMPOSSIBLE DE STOCKER LE BEAN, LE PARAMETRE 'NAME' EST NULL");
        }

        String theScope = scope;

        if (theScope == null)
        {
            theScope = defaultScope;
        }
        if (theScope == null)
        {
            theScope = "request";
        }

        if ("request".equals(theScope))
        {
            pageContext.getRequest().setAttribute(name, bean);
        }
        else if ("session".equals(theScope))
        {
            pageContext.getSession().setAttribute(name, bean);
        }
        else if ("page".equals(theScope))
        {
            pageContext.setAttribute(name, bean);
        }
        else if ("application".equals(theScope))
        {
            pageContext.getServletContext().setAttribute(name, bean);
        }
        else
        {
            StringBuilder   msg = new StringBuilder(71);
            msg.append("IMPOSSIBLE DE STOCKER LE BEAN. LE SCOPE : ");
            msg.append(scope);
            msg.append("EST INVALIDE.\n");
            msg.append("LES SCOPES VALIDES SONT : 'request', 'session','page' et 'appliocation'");
            throw new JspException(msg.toString());
        }
    }

    /**
     * methode Write : --.
     * 
     * @param number --
     * @throws JspException the jsp exception
     */
    protected void write(long number) throws JspException
    {
        try
        {
            pageContext.getOut().print(number);
        }
        catch (IOException ioe)
        {
            throw new JspException("ECRITURE IMPOSSIBLE SUR LE JSPWRITER", ioe);
        }
    }

    /**
     * methode Write : --.
     * 
     * @param text --
     * @throws JspException the jsp exception
     */
    protected void write(String text) throws JspException
    {
        if (text == null)
        {
            return;
        }
        try
        {
            pageContext.getOut().print(text);
        }
        catch (IOException ioe)
        {
            throw new JspException("ECRITURE IMPOSSIBLE SUR LE JSPWRITER", ioe);
        }
    }

}
