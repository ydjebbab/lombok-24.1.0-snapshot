/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.apptags.table.view.jrxmlinfo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Class CalcRowFooterInfo
 */
public class CalcRowFooterInfo implements Serializable
{

    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** Ligne précédente dans le pied de colonne. */
    private CalcRowFooterInfo previousCalcRowFooterInfo;

    /** Cellules d'une ligne du pied de colonne. */
    private List<CellColumnFooterInfo> cellules = new ArrayList<>();

    /** Hauteur de la ligne. */
    private int height;

    public CalcRowFooterInfo()
    {
        super();
        
    }

    /**
     * Gets the ligne précédente dans le pied de colonne.
     * 
     * @return the ligne précédente dans le pied de colonne
     */
    public CalcRowFooterInfo getPreviousCalcRowFooterInfo()
    {
        return previousCalcRowFooterInfo;
    }

    /**
     * Sets the ligne précédente dans le pied de colonne.
     * 
     * @param previousCalcRowFooterInfo the new ligne précédente dans le pied de colonne
     */
    public void setPreviousCalcRowFooterInfo(CalcRowFooterInfo previousCalcRowFooterInfo)
    {
        this.previousCalcRowFooterInfo = previousCalcRowFooterInfo;
    }

    /**
     * Gets the hauteur de la ligne.
     * 
     * @return the hauteur de la ligne
     */
    public int getHeight()
    {
        return height;
    }

    /**
     * Sets the hauteur de la ligne.
     * 
     * @param height the new hauteur de la ligne
     */
    public void setHeight(int height)
    {
        this.height = height;
    }

    /**
     * Gets the cellules d'une ligne du pied de colonne.
     * 
     * @return the cellules d'une ligne du pied de colonne
     */
    public List<CellColumnFooterInfo> getCellules()
    {
        return cellules;
    }

    /**
     * Sets the cellules d'une ligne du pied de colonne.
     * 
     * @param cellules the new cellules d'une ligne du pied de colonne
     */
    public void setCellules(List<CellColumnFooterInfo> cellules)
    {
        this.cellules = cellules;
    }

    /**
     * Ajoute une cellule à la ligne de pied de colonne.
     * 
     * @param cellColumnFooterInfo
     */
    public void addCellule(CellColumnFooterInfo cellColumnFooterInfo)
    {
        if (cellColumnFooterInfo != null)
        {
            this.cellules.add(cellColumnFooterInfo);
            cellColumnFooterInfo.setParentRowFooterInfo(this);
        }
    }

}
