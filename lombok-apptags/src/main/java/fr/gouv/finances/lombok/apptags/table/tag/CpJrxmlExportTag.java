/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.apptags.table.tag;

import org.apache.commons.lang.StringUtils;

import fr.gouv.finances.lombok.apptags.table.bean.Export;
import fr.gouv.finances.lombok.apptags.table.core.TableConstants;
import fr.gouv.finances.lombok.apptags.table.core.TableModel;
import fr.gouv.finances.lombok.apptags.table.view.CpJrxmlView;
import fr.gouv.finances.lombok.apptags.table.view.html.BuilderConstants;

/**
 * Class CpJrxmlExportTag
 */
public class CpJrxmlExportTag extends ExportTag
{

    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** nomapplication. */
    private String nomapplication;

    /** conf. */
    private String conf;

    /** modele. */
    private String modele;

    /** titreedition. */
    private String titreedition;

    /** pagedegarde. */
    private String pagedegarde;

    /** texteentete. */
    private String texteentete;

    /** textepieddepage. */
    private String textepieddepage;

    /** repertoireimagesetconf. */
    private String repertoireimagesetconf;

    /** zoomratio1. */
    private String zoomratio1;

    /** zoomratio2. */
    private String zoomratio2;

    /** zoomratio3. */
    private String zoomratio3;

    /** zoomratio4. */
    private String zoomratio4;

    /** graphique1. */
    private Object graphique1;

    /** graphique2. */
    private Object graphique2;

    /** graphique3. */
    private Object graphique3;

    /** graphique4. */
    private Object graphique4;

    /** subreport1. */
    private Object subreport1;

    /** subreport2. */
    private Object subreport2;

    /** subreport3. */
    private Object subreport3;

    /** subreport4. */
    private Object subreport4;

    /**
     * Instanciation de cp jrxml export tag.
     */
    public CpJrxmlExportTag()
    {
        super();
    }

    /**
     * Accesseur de l attribut repertoireimagesetconf.
     * 
     * @return repertoireimagesetconf
     */
    public String getRepertoireimagesetconf()
    {
        return repertoireimagesetconf;
    }

    /**
     * Modificateur de l attribut repertoireimagesetconf.
     * 
     * @param repertoireimages le nouveau repertoireimagesetconf
     */
    public void setRepertoireimagesetconf(String repertoireimages)
    {
        this.repertoireimagesetconf = repertoireimages;
    }

    /**
     * Accesseur de l attribut nomapplication.
     * 
     * @return nomapplication
     */
    public String getNomapplication()
    {
        return nomapplication;
    }

    /**
     * Modificateur de l attribut nomapplication.
     * 
     * @param nomapplication le nouveau nomapplication
     */
    public void setNomapplication(String nomapplication)
    {
        this.nomapplication = nomapplication;
    }

    /**
     * Accesseur de l attribut modele.
     * 
     * @return modele
     */
    public String getModele()
    {
        return modele;
    }

    /**
     * Modificateur de l attribut modele.
     * 
     * @param modele le nouveau modele
     */
    public void setModele(String modele)
    {
        this.modele = modele;
    }

    /**
     * Accesseur de l attribut conf.
     * 
     * @return conf
     */
    public String getConf()
    {
        return conf;
    }

    /**
     * Modificateur de l attribut conf.
     * 
     * @param conf le nouveau conf
     */
    public void setConf(String conf)
    {
        this.conf = conf;
    }

    /**
     * Accesseur de l attribut texteentete.
     * 
     * @return texteentete
     */
    public String getTexteentete()
    {
        return texteentete;
    }

    /**
     * Modificateur de l attribut texteentete.
     * 
     * @param texteentete le nouveau texteentete
     */
    public void setTexteentete(String texteentete)
    {
        this.texteentete = texteentete;
    }

    /**
     * Accesseur de l attribut graphique2.
     * 
     * @return graphique2
     */
    public Object getGraphique2()
    {
        return graphique2;
    }

    /**
     * Modificateur de l attribut graphique2.
     * 
     * @param graphique2 le nouveau graphique2
     */
    public void setGraphique2(Object graphique2)
    {
        this.graphique2 = graphique2;
    }

    /**
     * Accesseur de l attribut graphique3.
     * 
     * @return graphique3
     */
    public Object getGraphique3()
    {
        return graphique3;
    }

    /**
     * Modificateur de l attribut graphique3.
     * 
     * @param graphique3 le nouveau graphique3
     */
    public void setGraphique3(Object graphique3)
    {
        this.graphique3 = graphique3;
    }

    /**
     * Accesseur de l attribut graphique4.
     * 
     * @return graphique4
     */
    public Object getGraphique4()
    {
        return graphique4;
    }

    /**
     * Modificateur de l attribut graphique4.
     * 
     * @param graphique4 le nouveau graphique4
     */
    public void setGraphique4(Object graphique4)
    {
        this.graphique4 = graphique4;
    }

    /**
     * Modificateur de l attribut graphique1.
     * 
     * @param graphique1 le nouveau graphique1
     */
    public void setGraphique1(Object graphique1)
    {
        this.graphique1 = graphique1;
    }

    /**
     * Accesseur de l attribut zoomratio1.
     * 
     * @return zoomratio1
     */
    public String getZoomratio1()
    {
        return zoomratio1;
    }

    /**
     * Modificateur de l attribut zoomratio1.
     * 
     * @param zoomratio1 le nouveau zoomratio1
     */
    public void setZoomratio1(String zoomratio1)
    {
        this.zoomratio1 = zoomratio1;
    }

    /**
     * Accesseur de l attribut zoomratio2.
     * 
     * @return zoomratio2
     */
    public String getZoomratio2()
    {
        return zoomratio2;
    }

    /**
     * Modificateur de l attribut zoomratio2.
     * 
     * @param zoomratio2 le nouveau zoomratio2
     */
    public void setZoomratio2(String zoomratio2)
    {
        this.zoomratio2 = zoomratio2;
    }

    /**
     * Accesseur de l attribut zoomratio3.
     * 
     * @return zoomratio3
     */
    public String getZoomratio3()
    {
        return zoomratio3;
    }

    /**
     * Modificateur de l attribut zoomratio3.
     * 
     * @param zoomratio3 le nouveau zoomratio3
     */
    public void setZoomratio3(String zoomratio3)
    {
        this.zoomratio3 = zoomratio3;
    }

    /**
     * Accesseur de l attribut zoomratio4.
     * 
     * @return zoomratio4
     */
    public String getZoomratio4()
    {
        return zoomratio4;
    }

    /**
     * Modificateur de l attribut zoomratio4.
     * 
     * @param zoomratio4 le nouveau zoomratio4
     */
    public void setZoomratio4(String zoomratio4)
    {
        this.zoomratio4 = zoomratio4;
    }

    /**
     * Accesseur de l attribut graphique1.
     * 
     * @return graphique1
     */
    public Object getGraphique1()
    {
        return graphique1;
    }

    /**
     * Accesseur de l attribut textepieddepage.
     * 
     * @return textepieddepage
     */
    public String getTextepieddepage()
    {
        return textepieddepage;
    }

    /**
     * Modificateur de l attribut textepieddepage.
     * 
     * @param textepieddepage le nouveau textepieddepage
     */
    public void setTextepieddepage(String textepieddepage)
    {
        this.textepieddepage = textepieddepage;
    }

    /**
     * Accesseur de l attribut pagedegarde.
     * 
     * @return pagedegarde
     */
    public String getPagedegarde()
    {
        return pagedegarde;
    }

    /**
     * Modificateur de l attribut pagedegarde.
     * 
     * @param pagedegarde le nouveau pagedegarde
     */
    public void setPagedegarde(String pagedegarde)
    {
        this.pagedegarde = pagedegarde;
    }

    /**
     * Accesseur de l attribut titreedition.
     * 
     * @return titreedition
     */
    public String getTitreedition()
    {
        return titreedition;
    }

    /**
     * Modificateur de l attribut titreedition.
     * 
     * @param titreedition le nouveau titreedition
     */
    public void setTitreedition(String titreedition)
    {
        this.titreedition = titreedition;
    }

    /**
     * Accesseur de l attribut subreport1.
     * 
     * @return subreport1
     */
    public Object getSubreport1()
    {
        return subreport1;
    }

    /**
     * Modificateur de l attribut subreport1.
     * 
     * @param subreport1 le nouveau subreport1
     */
    public void setSubreport1(Object subreport1)
    {
        this.subreport1 = subreport1;
    }

    /**
     * Accesseur de l attribut subreport2.
     * 
     * @return subreport2
     */
    public Object getSubreport2()
    {
        return subreport2;
    }

    /**
     * Modificateur de l attribut subreport2.
     * 
     * @param subreport2 le nouveau subreport2
     */
    public void setSubreport2(Object subreport2)
    {
        this.subreport2 = subreport2;
    }

    /**
     * Accesseur de l attribut subreport3.
     * 
     * @return subreport3
     */
    public Object getSubreport3()
    {
        return subreport3;
    }

    /**
     * Modificateur de l attribut subreport3.
     * 
     * @param subreport3 le nouveau subreport3
     */
    public void setSubreport3(Object subreport3)
    {
        this.subreport3 = subreport3;
    }

    /**
     * Accesseur de l attribut subreport4.
     * 
     * @return subreport4
     */
    public Object getSubreport4()
    {
        return subreport4;
    }

    /**
     * Modificateur de l attribut subreport4.
     * 
     * @param subreport4 le nouveau subreport4
     */
    public void setSubreport4(Object subreport4)
    {
        this.subreport4 = subreport4;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.tag.ExportTag#addExportAttributes(fr.gouv.finances.lombok.apptags.table.core.TableModel,
     *      fr.gouv.finances.lombok.apptags.table.bean.Export)
     */
    @Override
    public void addExportAttributes(TableModel model, Export export)
    {
        if (StringUtils.isBlank(export.getView()))
        {
            export.setView(TableConstants.VIEW_CPJRXML);
        }

        if (StringUtils.isBlank(export.getViewResolver()))
        {
            export.setViewResolver(TableConstants.VIEW_CPJRXML);
        }

        if (StringUtils.isBlank(export.getImageName()))
        {
            export.setImageName(TableConstants.VIEW_PDF);
        }

        if (StringUtils.isBlank(export.getText()))
        {
            export.setText(BuilderConstants.TOOLBAR_PDF_TEXT);
        }

        export.addAttribute(CpJrxmlView.ATTR_FICHIER_CONF, TagUtils.evaluateExpressionAsString(
            CpJrxmlView.ATTR_FICHIER_CONF, conf, this, pageContext));

        export.addAttribute(CpJrxmlView.ATTR_MODELE, TagUtils.evaluateExpressionAsString(CpJrxmlView.ATTR_MODELE,
            modele, this, pageContext));

        export.addAttribute(CpJrxmlView.ATTR_NOM_APPLI, TagUtils.evaluateExpressionAsString(CpJrxmlView.ATTR_NOM_APPLI,
            nomapplication, this, pageContext));

        export.addAttribute(CpJrxmlView.ATTR_PAGE_DE_GARDE, TagUtils.evaluateExpressionAsString(
            CpJrxmlView.ATTR_PAGE_DE_GARDE, pagedegarde, this, pageContext));

        export.addAttribute(CpJrxmlView.ATTR_TITRE_EDITION, TagUtils.evaluateExpressionAsString(
            CpJrxmlView.ATTR_TITRE_EDITION, titreedition, this, pageContext));

        export.addAttribute(CpJrxmlView.ATTR_TEXTE_ENTETE, TagUtils.evaluateExpressionAsString(
            CpJrxmlView.ATTR_TEXTE_ENTETE, texteentete, this, pageContext));

        export.addAttribute(CpJrxmlView.ATTR_TEXTE_PIEDDEPAGE, TagUtils.evaluateExpressionAsString(
            CpJrxmlView.ATTR_TEXTE_PIEDDEPAGE, textepieddepage, this, pageContext));

        export.addAttribute(CpJrxmlView.ATTR_IMAGE_PATH, TagUtils.evaluateExpressionAsString(
            CpJrxmlView.ATTR_IMAGE_PATH, repertoireimagesetconf, this, pageContext));

        export.addAttribute(CpJrxmlView.ATTR_GRAPHIQUE1, graphique1);

        export.addAttribute(CpJrxmlView.ATTR_ZOOMRATIO1, Integer.valueOf(TagUtils.evaluateExpressionAsInt(
            CpJrxmlView.ATTR_ZOOMRATIO1, zoomratio1, this, pageContext)));

        export.addAttribute(CpJrxmlView.ATTR_GRAPHIQUE2, graphique2);

        export.addAttribute(CpJrxmlView.ATTR_ZOOMRATIO2, Integer.valueOf(TagUtils.evaluateExpressionAsInt(
            CpJrxmlView.ATTR_ZOOMRATIO2, zoomratio2, this, pageContext)));

        export.addAttribute(CpJrxmlView.ATTR_GRAPHIQUE3, graphique3);

        export.addAttribute(CpJrxmlView.ATTR_ZOOMRATIO3, Integer.valueOf(TagUtils.evaluateExpressionAsInt(
            CpJrxmlView.ATTR_ZOOMRATIO3, zoomratio3, this, pageContext)));

        export.addAttribute(CpJrxmlView.ATTR_GRAPHIQUE4, graphique4);

        export.addAttribute(CpJrxmlView.ATTR_ZOOMRATIO4, Integer.valueOf(TagUtils.evaluateExpressionAsInt(
            CpJrxmlView.ATTR_ZOOMRATIO4, zoomratio4, this, pageContext)));

        export.addAttribute(CpJrxmlView.ATTR_SUBREPORT1, subreport1);

        export.addAttribute(CpJrxmlView.ATTR_SUBREPORT2, subreport2);

        export.addAttribute(CpJrxmlView.ATTR_SUBREPORT3, subreport3);

        export.addAttribute(CpJrxmlView.ATTR_SUBREPORT4, subreport4);
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.tag.ExportTag#release()
     */
    @Override
    public void release()
    {
        zoomratio1 = null;
        zoomratio2 = null;
        zoomratio3 = null;
        zoomratio4 = null;
        graphique1 = null;
        graphique2 = null;
        graphique3 = null;
        graphique4 = null;
        subreport1 = null;
        subreport2 = null;
        subreport3 = null;
        subreport4 = null;
        conf = null;
        modele = null;
        texteentete = null;
        titreedition = null;
        pagedegarde = null;
        textepieddepage = null;
        nomapplication = null;
        super.release();
    }

}
