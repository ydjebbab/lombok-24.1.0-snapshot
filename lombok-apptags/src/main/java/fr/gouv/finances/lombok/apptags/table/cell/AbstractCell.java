/*
 * Copyright (c) 2014 DGFiP - Tous droits réservés
 *//*
/*Copyright (c) 2014 DGFiP - Tous droits réservés/* * Copyright 2004 original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package fr.gouv.finances.lombok.apptags.table.cell;

import java.lang.reflect.Field;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.reflect.FieldUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import fr.gouv.finances.lombok.apptags.table.core.TableConstants;
import fr.gouv.finances.lombok.apptags.table.bean.Column;
import fr.gouv.finances.lombok.apptags.table.bean.Row;
import fr.gouv.finances.lombok.apptags.table.core.TableModel;
import fr.gouv.finances.lombok.apptags.table.view.html.ColumnBuilder;

/**
 * Class AbstractCell
 */
public abstract class AbstractCell implements Cell
{

    /** log. */
    private static Log log = LogFactory.getLog(AbstractCell.class);

    /** valeur courante de l'attribut discriminant. */
    private static final String LAST_VALUE = "lastValue";

    /** vaut true si discriminant bean courant = discriminant bean courant. */
    private static final String EGALITE_BEAN = "egalitebean";

    /** numéro de row du bean courant. */
    private static final String NUM_ROW_BEAN = "numrowbean";

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.cell.Cell#getExportDisplay(fr.gouv.finances.lombok.apptags.table.core.TableModel,
     *      fr.gouv.finances.lombok.apptags.table.bean.Column)
     */
    @Override
    public String getExportDisplay(TableModel model, Column column)
    {
        return getCellValue(model, column);
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.cell.Cell#getHtmlDisplay(fr.gouv.finances.lombok.apptags.table.core.TableModel,
     *      fr.gouv.finances.lombok.apptags.table.bean.Column)
     */
    @Override
    public String getHtmlDisplay(TableModel model, Column column)
    {

        ColumnBuilder columnBuilder = new ColumnBuilder(column);
        columnBuilder.tdStart();
        if(column.isSortable()){
            StringBuilder columnId = new StringBuilder();
            columnId.append(model.getTableHandler().prefixWithTableId());
            columnId.append(TableConstants.COLUMN);
            columnId.append(column.getAlias());
            columnBuilder.headers(columnId.toString());
        } 
        columnBuilder.tdClose();
      

        // on regarde les valeurs du bean courant sur son discriminant
        // et on complete les attributs
        if (model.getRowHandler().getRow().getNamePropertyShowIfEqualsLast() != null
            && ("").equalsIgnoreCase(model.getRowHandler().getRow().getNamePropertyShowIfEqualsLast()) == false)
        {
            try
            {
                getEgaliteBeanSurAttributDiscriminant(model, column);
            }
            catch (NoSuchFieldException e)
            {
                log.debug(e.getMessage());
            }
        }

        // si discrimant bean courant =discriminant bean précédent
        // sur les colonnes ou writeOrBlank est vrai ,on passe la valeur de la cellule à vide
        if ((model.getRowCount() == (Integer.parseInt(model.getTableHandler().getTable().getAttributeAsString(
            NUM_ROW_BEAN))))
            && (model.getTableHandler().getTable().getAttributeAsString(EGALITE_BEAN).compareToIgnoreCase("true") == 0)
            && (column.getWriteOrBlank() != null && column.getWriteOrBlank() == true))
        {
            columnBuilder.tdBody("");

        }
        else
        {
            columnBuilder.tdBody(getCellValue(model, column));

        }
        columnBuilder.tdEnd();
        return columnBuilder.toString();
    }

    /**
     * A convenience method to get the display value.
     * 
     * @param model
     * @param column
     * @return cell value
     */
    protected abstract String getCellValue(TableModel model, Column column);

    /**
     * on regarde les valeurs du bean courant sur son discriminant et on complete les attributs.
     * 
     * @param model
     * @param column
     * @return egalite bean sur attribut discriminant
     */
    protected static Boolean getEgaliteBeanSurAttributDiscriminant(TableModel model, Column column) throws NoSuchFieldException
    {
        boolean egaliteBean = false;
        Row rowCourante = model.getRowHandler().getRow();
        String discriminant = rowCourante.getNamePropertyShowIfEqualsLast();
        Field attribut;
        Field attribut1;
        Object valueAttributDiscriminant;

        Class maClass = model.getCurrentRowBean().getClass();

        if ((model.getRowCount() != (Integer.parseInt(model.getTableHandler().getTable().getAttributeAsString(
            NUM_ROW_BEAN).toString()))))
        {
            try
            { // on retrouve la valeur de l attribut discriminant dans le bean courant

                // cas d'un attribut pointé
                if (discriminant.contains("."))
                {

                    String[] tableauDiscriminant = StringUtils.split(discriminant, ".");
                    attribut = FieldUtils.getField(maClass, tableauDiscriminant[0], true);
                    attribut.setAccessible(true);
                    Object valueAttributDiscriminant1 = attribut.get(model.getCurrentRowBean());
                    Class maClass1 = valueAttributDiscriminant1.getClass();
                    attribut1 = FieldUtils.getField(maClass1, tableauDiscriminant[1], true);
                    attribut1.setAccessible(true);
                    valueAttributDiscriminant = attribut1.get(valueAttributDiscriminant1);

                }
                else
                {
                    attribut = FieldUtils.getField(maClass, discriminant, true);
                    attribut.setAccessible(true);
                    valueAttributDiscriminant = attribut.get(model.getCurrentRowBean());

                }

                String lastValue = model.getTableHandler().getTable().getAttributeAsString(LAST_VALUE);

                if (valueAttributDiscriminant != null && valueAttributDiscriminant.toString().equals(lastValue))
                {
                    egaliteBean = true;
                    model.getTableHandler().getTable().addAttribute(EGALITE_BEAN, true);

                }
                else
                {
                    model.getTableHandler().getTable().addAttribute(EGALITE_BEAN, false);
                }
                // on enregistre la valeur de l attribut discriminant

                model.getTableHandler().getTable().addAttribute(LAST_VALUE, valueAttributDiscriminant);
                // on enregistre la ligne pour gestion de l affichage
                model.getTableHandler().getTable().addAttribute(NUM_ROW_BEAN, rowCourante.getRowCount());

            }
            catch (SecurityException e1)
            {
                log.debug(e1.getMessage());
            }
            catch (IllegalArgumentException e)
            {
                log.debug(e.getMessage());
            }
            catch (IllegalAccessException e)
            {
                log.debug(e.getMessage());
            }
        }
        else
        {
            // on travaille encore sur la même ligne on ne fait rien
        }

        return egaliteBean;
    }

}
