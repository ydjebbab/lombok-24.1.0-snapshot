/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.apptags.table.view.html;

import java.text.MessageFormat;

import org.apache.commons.lang.StringUtils;

import fr.gouv.finances.lombok.apptags.table.core.TableModel;
import fr.gouv.finances.lombok.apptags.util.HtmlBuilder;

/**
 * Class CpStatusBarBuilder
 * 
 * @author Jeff Johnston
 */
public class CpStatusBarBuilder
{

    /** html. */
    private HtmlBuilder html;

    /** model. */
    private TableModel model;

    /**
     * Instanciation de cp status bar builder.
     * 
     * @param model
     */
    public CpStatusBarBuilder(TableModel model)
    {
        this(new HtmlBuilder(), model);
    }

    /**
     * Instanciation de cp status bar builder.
     * 
     * @param html
     * @param model
     */
    public CpStatusBarBuilder(HtmlBuilder html, TableModel model)
    {
        this.html = html;
        this.model = model;
    }

    /**
     * Accesseur de l attribut html builder.
     * 
     * @return html builder
     */
    public HtmlBuilder getHtmlBuilder()
    {
        return html;
    }

    /**
     * Accesseur de l attribut table model.
     * 
     * @return table model
     */
    protected TableModel getTableModel()
    {
        return model;
    }

    /**
     * methode Status message :
     */
    public void statusMessage()
    {
        if (model.getLimit().getTotalRows() == 0)
        {
            if (model.getTableHandler().getTable().getPositionnoresultsfound() != null
                && model.getTableHandler().getTable().getPositionnoresultsfound().compareToIgnoreCase("body") != 0)
            {
                if (model.getTableHandler().getTable().getLibellenoresultsfound() != null
                    && model.getTableHandler().getTable().getLibellenoresultsfound().compareToIgnoreCase("") != 0)
                {
                    html.append(model.getTableHandler().getTable().getLibellenoresultsfound());
                }
                else
                {
                    html.append(model.getMessages().getMessage(BuilderConstants.STATUSBAR_NO_RESULTS_FOUND));
                }
            }
        }
        else
        {
            Integer total = Integer.valueOf(model.getLimit().getTotalRows());
            Integer from = Integer.valueOf(model.getLimit().getRowStart() + 1);
            Integer to = Integer.valueOf(model.getLimit().getRowEnd());
            Object[] messageArguments = {total, from, to};
            html.append(model.getMessages().getMessage(BuilderConstants.STATUSBAR_RESULTS_FOUND, messageArguments));
        }
    }

    /**
     * methode Status message nb lignes trouvees :
     */
    public void statusMessageNbLignesTrouvees()
    {
        if (model.getLimit().getTotalRows() == 0)
        {

            if (model.getTableHandler().getTable().getPositionnoresultsfound() != null
                && model.getTableHandler().getTable().getPositionnoresultsfound().compareToIgnoreCase("body") != 0)
            {
                if (model.getTableHandler().getTable().getLibellenoresultsfound() != null
                    && model.getTableHandler().getTable().getLibellenoresultsfound().compareToIgnoreCase("") != 0)
                {
                    html.append(model.getTableHandler().getTable().getLibellenoresultsfound());
                }

                else
                {
                    html.append(model.getMessages().getMessage(BuilderConstants.STATUSBAR_NO_RESULTS_FOUND));
                }
            }
        }
        else
        {
            Integer total = Integer.valueOf(model.getLimit().getTotalRows());
            Object[] messageArguments = {total};

            String resultatsTrouve =
                model.getMessages().getMessage(CpBuilderConstants.STATUS_BAR_NB_RESULTATS_TROUVES, messageArguments);
            if (resultatsTrouve == null || StringUtils.isBlank(resultatsTrouve))
            {
                resultatsTrouve = CpBuilderConstants.STATUS_BAR_NB_RESULTATS_TROUVES;

                MessageFormat formatter = new MessageFormat("");
                formatter.applyPattern(resultatsTrouve);
                resultatsTrouve = formatter.format(messageArguments);
            }

            html.append(resultatsTrouve);

        }
    }

    /**
     * methode Status message lignes affichees :
     */
    public void statusMessageLignesAffichees()
    {
        if (model.getLimit().getTotalRows() > 0)
        {
            Integer from = Integer.valueOf(model.getLimit().getRowStart() + 1);
            Integer to = Integer.valueOf(model.getLimit().getRowEnd());
            Object[] messageArguments = {from, to};

            String resultatsAffiches =
                model.getMessages().getMessage(CpBuilderConstants.STATUS_BAR_RESULTATS_AFFICHES, messageArguments);
            if (resultatsAffiches == null || StringUtils.isBlank(resultatsAffiches))
            {
                resultatsAffiches = CpBuilderConstants.STATUS_BAR_RESULTATS_AFFICHES_DFLT;

                MessageFormat formatter = new MessageFormat("");
                formatter.applyPattern(resultatsAffiches);
                resultatsAffiches = formatter.format(messageArguments);

            }

            html.append(resultatsAffiches);
        }
    }

    /**
     * methode Status message alerte reponse incomplete :
     */
    public void statusMessageAlerteReponseIncomplete()
    {
        String imageName = model.getMessages().getMessage(CpBuilderConstants.STATUS_BAR_IMG_RESULT_INCOMP);
        if (imageName == null || StringUtils.isBlank(imageName))
        {
            imageName = CpBuilderConstants.STATUS_BAR_IMG_RESULT_INCOMP_DFLT;
        }

        String msgResultatIncomplet = model.getMessages().getMessage(CpBuilderConstants.STATUS_BAR_MSG_RESULT_INCOMP);
        if (msgResultatIncomplet == null || StringUtils.isBlank(msgResultatIncomplet))
        {
            msgResultatIncomplet = CpBuilderConstants.STATUS_BAR_MSG_RESULT_INCOMP_DFLT;
        }

        html.append("&#160;");
        html.img().border("0").src(BuilderUtils.getImage(model, imageName));
        html.append(" width='14' height='14' ");
        html.alt(msgResultatIncomplet);
        html.title(msgResultatIncomplet);
        html.xclose();
        html.append("&#160;");
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        return html.toString();
    }
}
