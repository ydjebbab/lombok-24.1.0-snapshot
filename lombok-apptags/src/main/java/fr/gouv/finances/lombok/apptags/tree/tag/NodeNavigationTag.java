/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : NodeNavigationTag.java
 *
 */
package fr.gouv.finances.lombok.apptags.tree.tag;

import javax.servlet.jsp.JspException;

import fr.gouv.finances.lombok.apptags.base.TagWriter;
import fr.gouv.finances.lombok.apptags.tree.bean.TreeIteratorElement;
import fr.gouv.finances.lombok.apptags.util.Constantes;

/**
 * Class NodeNavigationTag --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.2 $ Date: 11 déc. 2009
 */
public class NodeNavigationTag extends NodeBaseTag
{

    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = 1L;

    // Paramètres du tag

    /** expand node transition. */
    private String expandNodeTransition;

    /** collapse node transition. */
    private String collapseNodeTransition;

    /** blank image. */
    private String blankImage;

    /** vertical line image. */
    private String verticalLineImage;

    /** collapsed mid node. */
    private String collapsedMidNode;

    /** collapsed last node. */
    private String collapsedLastNode;

    /** expanded mid node. */
    private String expandedMidNode;

    /** expanded last node. */
    private String expandedLastNode;

    /** no children mid node. */
    private String noChildrenMidNode;

    /** no children last node. */
    private String noChildrenLastNode;

    /** open folder. */
    private String openFolder;

    /** closed folder. */
    private String closedFolder;

    /** not folder. */
    private String notFolder;

    // Elements paramétrables par un fichier properties

    /** alt collapse node. */
    private String altCollapseNode;

    /** alt expand node. */
    private String altExpandNode;

    /** tag writer. */
    private TagWriter tagWriter;

    /**
     * Instanciation de node navigation tag.
     */
    public NodeNavigationTag()
    {
        super();
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @return int
     * @throws JspException the jsp exception
     * @see javax.servlet.jsp.tagext.TagSupport#doEndTag()
     */
    @Override
    public int doEndTag() throws JspException
    {
        return EVAL_PAGE;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @see org.springframework.web.servlet.tags.RequestContextAwareTag#doFinally()
     */
    @Override
    public void doFinally()
    {
        super.doFinally();
        this.tagWriter = null;
    }

    /**
     * Accesseur de l attribut blank image.
     * 
     * @return blank image
     */
    public String getBlankImage()
    {
        return blankImage;
    }

    /**
     * Accesseur de l attribut closed folder.
     * 
     * @return closed folder
     */
    public String getClosedFolder()
    {
        return closedFolder;
    }

    /**
     * Accesseur de l attribut collapsed last node.
     * 
     * @return collapsed last node
     */
    public String getCollapsedLastNode()
    {
        return collapsedLastNode;
    }

    /**
     * Accesseur de l attribut collapsed mid node.
     * 
     * @return collapsed mid node
     */
    public String getCollapsedMidNode()
    {
        return collapsedMidNode;
    }

    /**
     * Accesseur de l attribut collapse node transition.
     * 
     * @return collapse node transition
     */
    public String getCollapseNodeTransition()
    {
        return collapseNodeTransition;
    }

    /**
     * Accesseur de l attribut expanded last node.
     * 
     * @return expanded last node
     */
    public String getExpandedLastNode()
    {
        return expandedLastNode;
    }

    /**
     * Accesseur de l attribut expanded mid node.
     * 
     * @return expanded mid node
     */
    public String getExpandedMidNode()
    {
        return expandedMidNode;
    }

    /**
     * Accesseur de l attribut expand node transition.
     * 
     * @return expand node transition
     */
    public String getExpandNodeTransition()
    {
        return expandNodeTransition;
    }

    /**
     * Accesseur de l attribut no children last node.
     * 
     * @return no children last node
     */
    public String getNoChildrenLastNode()
    {
        return noChildrenLastNode;
    }

    /**
     * Accesseur de l attribut no children mid node.
     * 
     * @return no children mid node
     */
    public String getNoChildrenMidNode()
    {
        return noChildrenMidNode;
    }

    /**
     * Accesseur de l attribut not folder.
     * 
     * @return not folder
     */
    public String getNotFolder()
    {
        return notFolder;
    }

    /**
     * Accesseur de l attribut open folder.
     * 
     * @return open folder
     */
    public String getOpenFolder()
    {
        return openFolder;
    }

    /**
     * Accesseur de l attribut tag writer.
     * 
     * @return tag writer
     */
    public TagWriter getTagWriter()
    {
        return tagWriter;
    }

    /**
     * Accesseur de l attribut vertical line image.
     * 
     * @return vertical line image
     */
    public String getVerticalLineImage()
    {
        return verticalLineImage;
    }

    /**
     * Modificateur de l attribut blank image.
     * 
     * @param blankImage le nouveau blank image
     */
    public void setBlankImage(String blankImage)
    {
        this.blankImage = blankImage;
    }

    /**
     * Modificateur de l attribut closed folder.
     * 
     * @param closedFolder le nouveau closed folder
     */
    public void setClosedFolder(String closedFolder)
    {
        this.closedFolder = closedFolder;
    }

    /**
     * Modificateur de l attribut collapsed last node.
     * 
     * @param collapsedLastNode le nouveau collapsed last node
     */
    public void setCollapsedLastNode(String collapsedLastNode)
    {
        this.collapsedLastNode = collapsedLastNode;
    }

    /**
     * Modificateur de l attribut collapsed mid node.
     * 
     * @param collapsedMidNode le nouveau collapsed mid node
     */
    public void setCollapsedMidNode(String collapsedMidNode)
    {
        this.collapsedMidNode = collapsedMidNode;
    }

    /**
     * Modificateur de l attribut collapse node transition.
     * 
     * @param collapseNodeTransition le nouveau collapse node transition
     */
    public void setCollapseNodeTransition(String collapseNodeTransition)
    {
        this.collapseNodeTransition = collapseNodeTransition;
    }

    /**
     * Modificateur de l attribut expanded last node.
     * 
     * @param expandedLastNode le nouveau expanded last node
     */
    public void setExpandedLastNode(String expandedLastNode)
    {
        this.expandedLastNode = expandedLastNode;
    }

    /**
     * Modificateur de l attribut expanded mid node.
     * 
     * @param expandedMidNode le nouveau expanded mid node
     */
    public void setExpandedMidNode(String expandedMidNode)
    {
        this.expandedMidNode = expandedMidNode;
    }

    /**
     * Modificateur de l attribut expand node transition.
     * 
     * @param expandNodeTransition le nouveau expand node transition
     */
    public void setExpandNodeTransition(String expandNodeTransition)
    {
        this.expandNodeTransition = expandNodeTransition;
    }

    /**
     * Modificateur de l attribut no children last node.
     * 
     * @param noChildrenLastNode le nouveau no children last node
     */
    public void setNoChildrenLastNode(String noChildrenLastNode)
    {
        this.noChildrenLastNode = noChildrenLastNode;
    }

    /**
     * Modificateur de l attribut no children mid node.
     * 
     * @param noChildrenMidNode le nouveau no children mid node
     */
    public void setNoChildrenMidNode(String noChildrenMidNode)
    {
        this.noChildrenMidNode = noChildrenMidNode;
    }

    /**
     * Modificateur de l attribut not folder.
     * 
     * @param notFolder le nouveau not folder
     */
    public void setNotFolder(String notFolder)
    {
        this.notFolder = notFolder;
    }

    /**
     * Modificateur de l attribut open folder.
     * 
     * @param openFolder le nouveau open folder
     */
    public void setOpenFolder(String openFolder)
    {
        this.openFolder = openFolder;
    }

    /**
     * Modificateur de l attribut tag writer.
     * 
     * @param tagWriter le nouveau tag writer
     */
    public void setTagWriter(TagWriter tagWriter)
    {
        this.tagWriter = tagWriter;
    }

    /**
     * Modificateur de l attribut vertical line image.
     * 
     * @param verticalLineImage le nouveau vertical line image
     */
    public void setVerticalLineImage(String verticalLineImage)
    {
        this.verticalLineImage = verticalLineImage;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param tagWriter
     * @return int
     * @throws JspException the jsp exception
     * @see fr.gouv.finances.lombok.apptags.base.BaseAppTags#writeTagContent(fr.gouv.finances.lombok.apptags.base.TagWriter)
     */
    @Override
    protected int writeTagContent(TagWriter tagWriter) throws JspException
    {
        this.initParentTreeTag();

        this.tagWriter = tagWriter;

        // Initialise les attributs du tag
        this.initAttributes();

        TreeIteratorElement element = this.parentTreeTag.getElement();

        // Indentation
        this.writeNodeIndentation(element);

        // Icones d'ouverture et de fermeture des noeuds
        this.writeOpenOrCloseNodeIcons(element);

        writeIconTypeNode(element);

        return EVAL_BODY_INCLUDE;
    }

    /**
     * Initialise les paramètres du tag en fonction de la valeur des attributs passés en paramètre au tag et des
     * paramètres par défaut.
     * 
     * @throws JspException the jsp exception
     */
    private void initAttributes() throws JspException
    {
        // Utilisation d'un NodeParametres qui porte les valeurs affectées
        // soit par un paramètre soit par défaut

        this.altCollapseNode =
            this.initvar(null, Constantes.ALT_COLLAPSE_NODE_KEY, Constantes.ALT_COLLAPSE_NODE_DEFAULT);

        this.altExpandNode = this.initvar(null, Constantes.ALT_EXPAND_NODE_KEY, Constantes.ALT_EXPAND_NODE_DEFAULT);

        this.collapseNodeTransition =
            this.initvar(this.collapseNodeTransition, Constantes.COLLAPSE_NODE_TRANSITION_KEY,
                Constantes.COLLAPSE_NODE_TRANSITION_DEFAULT);

        this.expandNodeTransition =
            this.initvar(this.expandNodeTransition, Constantes.EXPAND_NODE_TRANSITION_KEY,
                Constantes.EXPAND_NODE_TRANSITION_DEFAULT);

        this.blankImage = this.initvar(this.blankImage, Constantes.BLANK_IMAGE_KEY, Constantes.BLANK_IMAGE_DEFAULT);

        this.verticalLineImage =
            this.initvar(this.verticalLineImage, Constantes.VERTICAL_LINE_IMAGE_KEY,
                Constantes.VERTICAL_LINE_IMAGE_DEFAULT);

        this.closedFolder =
            this.initvar(this.closedFolder, Constantes.CLOSED_FOLDER_KEY, Constantes.CLOSED_FOLDER_DEFAULT);

        this.openFolder = this.initvar(this.openFolder, Constantes.OPEN_FOLDER_KEY, Constantes.OPEN_FOLDER_DEFAULT);

        this.notFolder = this.initvar(this.notFolder, Constantes.NOT_FOLDER_KEY, Constantes.NOT_FOLDER_DEFAULT);

        this.expandedLastNode =
            this.initvar(this.expandedLastNode, Constantes.EXPANDED_LAST_NODE_KEY,
                Constantes.EXPANDED_LAST_NODE_DEFAULT);

        this.expandedMidNode =
            this.initvar(this.expandedMidNode, Constantes.EXPANDED_MID_NODE_KEY, Constantes.EXPANDED_MID_NODE_DEFAULT);

        this.collapsedMidNode =
            this.initvar(this.collapsedMidNode, Constantes.COLLAPSED_MID_NODE_KEY,
                Constantes.COLLAPSED_MID_NODE_DEFAULT);

        this.collapsedLastNode =
            this.initvar(this.collapsedLastNode, Constantes.COLLAPSED_LAST_NODE_KEY,
                Constantes.COLLAPSED_LAST_NODE_DEFAULT);

        this.noChildrenLastNode =
            this.initvar(this.noChildrenLastNode, Constantes.NO_CHILDREN_LAST_NODE_KEY,
                Constantes.NO_CHILDREN_LAST_NODE_DEFAULT);

        this.noChildrenMidNode =
            this.initvar(this.noChildrenMidNode, Constantes.NO_CHILDREN_MID_NODE_KEY,
                Constantes.NO_CHILDREN_MID_NODE_DEFAULT);

    }

    /**
     * Insère les éléments graphiques qui permettent de différencier les noeuds de type folder des autres.
     * 
     * @param element --
     * @throws JspException the jsp exception
     */
    private void writeIconTypeNode(TreeIteratorElement element) throws JspException
    {
        tagWriter.startTag(TD_TAG);
        tagWriter.writeOptionalAttributeValue(CLASS_ATTRIBUTE, this.parentTreeTag.getCssClass());

        if (element.getNode().isFolder())
        {
            if (element.isExpanded())
            {
                this.writeLinkWithImage(tagWriter, this.openFolder, this.altCollapseNode, this.altCollapseNode,
                    this.collapseNodeTransition, element.getNode().getId());
            }
            else
            {
                this.writeLinkWithImage(tagWriter, this.closedFolder, this.altExpandNode, this.altExpandNode,
                    this.expandNodeTransition, element.getNode().getId());
            }
        }
        else
        {
            this.writeImage(tagWriter, this.notFolder, "notFolder", this.parentTreeTag.getImagesDir());
        }

        tagWriter.endTag();
    }

    /**
     * Insère les éléments permettant d'indenter les noeuds.
     * 
     * @param element --
     * @throws JspException the jsp exception
     */
    private void writeNodeIndentation(TreeIteratorElement element) throws JspException
    {
        for (Boolean type : element.getIndendationProfile())
        {
            tagWriter.startTag(TD_TAG);

            if (type != null && type.booleanValue())
            {
                // Blank indentation
                tagWriter.writeOptionalAttributeValue(CLASS_ATTRIBUTE, this.parentTreeTag.getCssClass());
                this.writeImage(tagWriter, this.blankImage, "blankImage", this.parentTreeTag.getImagesDir());
            }
            else
            {
                // Vertical line indentation
                tagWriter.writeOptionalAttributeValue(CLASS_ATTRIBUTE, this.parentTreeTag.getCssClass() + " line");
                tagWriter.writeOptionalAttributeValue(STYLE_ATTRIBUTE, "background-image:url("
                    + this.buildImageUri(this.verticalLineImage, this.parentTreeTag.getImagesDir()) + ");");
                this.writeImage(tagWriter, this.verticalLineImage, "verticalLineImage", this.parentTreeTag
                    .getImagesDir());
            }
            tagWriter.endTag();
        }
    }

    /**
     * Insère les éléments graphique permettant l'ouverture ou la fermeture des noeuds.
     * 
     * @param element --
     * @throws JspException the jsp exception
     */
    private void writeOpenOrCloseNodeIcons(TreeIteratorElement element) throws JspException
    {
        tagWriter.startTag(TD_TAG);
        tagWriter.writeOptionalAttributeValue(CLASS_ATTRIBUTE, this.parentTreeTag.getCssClass());

        // Traitement des noeuds qui possèdent des enfants
        if (element.getNode().hasChildren())
        {
            // Noeuds ouverts
            if (element.isExpanded())
            {
                if (element.isExpanded() && element.isLastChild())
                {
                    this.writeLinkWithImage(tagWriter, this.expandedLastNode, this.altCollapseNode,
                        this.altCollapseNode, this.collapseNodeTransition, element.getNode().getId());
                }
                else
                {
                    tagWriter.writeOptionalAttributeValue(STYLE_ATTRIBUTE, "background-image:url("
                        + this.buildImageUri(this.verticalLineImage, this.parentTreeTag.getImagesDir()) + ");");
                    this.writeLinkWithImage(tagWriter, this.expandedMidNode, this.altCollapseNode,
                        this.altCollapseNode, this.collapseNodeTransition, element.getNode().getId());
                }
            }
            // Noeuds fermés
            else
            {
                // Noeud fermé, avec enfant, dernier enfant
                if (element.isLastChild())
                {
                    this.writeLinkWithImage(tagWriter, this.collapsedLastNode, this.altExpandNode, this.altExpandNode,
                        this.expandNodeTransition, element.getNode().getId());
                }
                else
                {
                    tagWriter.writeOptionalAttributeValue(STYLE_ATTRIBUTE, "background-image:url("
                        + this.buildImageUri(this.verticalLineImage, this.parentTreeTag.getImagesDir()) + ");");
                    this.writeLinkWithImage(tagWriter, this.collapsedMidNode, this.altExpandNode, this.altExpandNode,
                        this.expandNodeTransition, element.getNode().getId());
                }

            }

        }
        // Traitement des noeuds sans enfants
        else
        {
            if (element.isLastChild())
            {
                this.writeImage(tagWriter, this.noChildrenLastNode, "noChildrenLastNode", this.parentTreeTag
                    .getImagesDir());
            }
            else
            {
                tagWriter.writeOptionalAttributeValue(STYLE_ATTRIBUTE, "background-image:url("
                    + this.buildImageUri(this.verticalLineImage, this.parentTreeTag.getImagesDir()) + ");");
                this.writeImage(tagWriter, this.noChildrenMidNode, "noChildrenMidNode", this.parentTreeTag
                    .getImagesDir());
            }
        }

        tagWriter.endTag();
    }

}
