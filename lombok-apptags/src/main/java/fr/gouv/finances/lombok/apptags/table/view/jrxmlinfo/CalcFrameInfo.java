/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.apptags.table.view.jrxmlinfo;

import java.util.ArrayList;
import java.util.List;

/**
 * Class CalcFrameInfo
 */
public class CalcFrameInfo
{

    /** Nombre de lignes du pied de colonne. */
    private int rows;

    /** footer rows. */
    private List<CalcRowFooterInfo> footerRows = new ArrayList<>();

    /** indexs cals. */
    private List<Integer> indexsCals = new ArrayList<>();

    public CalcFrameInfo()
    {
        super();       
    }

    /**
     * Accesseur de l attribut indexs cals.
     * 
     * @return indexs cals
     */
    public List<Integer> getIndexsCals()
    {
        return indexsCals;
    }

    /**
     * Modificateur de l attribut indexs cals.
     * 
     * @param indexsCals le nouveau indexs cals
     */
    public void setIndexsCals(List<Integer> indexsCals)
    {
        this.indexsCals = indexsCals;
    }

    /**
     * Accesseur de l attribut footer rows.
     * 
     * @return footer rows
     */
    public List<CalcRowFooterInfo> getFooterRows()
    {
        return footerRows;
    }

    /**
     * Modificateur de l attribut footer rows.
     * 
     * @param footerRows le nouveau footer rows
     */
    public void setFooterRows(List<CalcRowFooterInfo> footerRows)
    {
        this.footerRows = footerRows;
    }

    /**
     * Gets the nombre de lignes du pied de colonne.
     * 
     * @return the nombre de lignes du pied de colonne
     */
    public int getRows()
    {
        return rows;
    }

    /**
     * Sets the nombre de lignes du pied de colonne.
     * 
     * @param rows the new nombre de lignes du pied de colonne
     */
    public void setRows(int rows)
    {
        this.rows = rows;
    }

    /**
     * Ajoute une ligne au pied de colonne.
     * 
     * @param calcRowFooterInfo
     */
    public void addCalcRow(CalcRowFooterInfo calcRowFooterInfo)
    {
        this.footerRows.add(calcRowFooterInfo);
    }
}
