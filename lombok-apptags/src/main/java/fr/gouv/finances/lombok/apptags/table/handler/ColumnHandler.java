/*
/*Copyright (c) 2014 DGFiP - Tous droits réservés/* * Copyright 2004 original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package fr.gouv.finances.lombok.apptags.table.handler;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import fr.gouv.finances.lombok.apptags.table.bean.Column;
import fr.gouv.finances.lombok.apptags.table.calc.CalcResult;
import fr.gouv.finances.lombok.apptags.table.calc.CalcUtils;
import fr.gouv.finances.lombok.apptags.table.cell.Cell;
import fr.gouv.finances.lombok.apptags.table.core.PreferencesConstants;
import fr.gouv.finances.lombok.apptags.table.core.TableCache;
import fr.gouv.finances.lombok.apptags.table.core.TableConstants;
import fr.gouv.finances.lombok.apptags.table.core.TableModel;
import fr.gouv.finances.lombok.apptags.table.core.TableModelUtils;

/**
 * La première scrutation de la table permet de charger les propriétés des colonnes.
 */
public class ColumnHandler
{

    /** model. */
    private TableModel model;

    /** columns. */
    private List<Column> columns = new ArrayList<>();

    /** first column. */
    private Column firstColumn;

    /** last column. */
    private Column lastColumn;

    /**
     * Instanciation de column handler.
     * 
     * @param model
     */
    public ColumnHandler(TableModel model)
    {
        this.model = model;
    }

    /**
     * methode Adds the auto generate column :
     * 
     * @param column
     */
    public void addAutoGenerateColumn(Column column)
    {
        column.addAttribute(TableConstants.IS_AUTO_GENERATE_COLUMN, "true");
        this.addColumn(column);
    }

    /**
     * Ajoute une colonne à la liste des colonnes.
     * 
     * @param column
     */
    public void addColumn(Column column)
    {
        column.defaults();
        this.addColumnAttributes(column);
        if (isViewAllowed(column))
        {
            if (this.firstColumn == null)
            {
                this.firstColumn = column;
                column.setFirstColumn(true);
            }

            if (this.lastColumn != null)
            {
                this.lastColumn.setLastColumn(false);
            }

            this.lastColumn = column;
            column.setLastColumn(true);

            this.columns.add(column);
        }
    }

    /**
     * Ajout des attributs à une colonne.
     * 
     * @param column
     */
    public void addColumnAttributes(Column column)
    {
        String interceptor =
            TableModelUtils.getInterceptPreference(model, column.getInterceptor(),
                PreferencesConstants.COLUMN_INTERCEPTOR);
        column.setInterceptor(interceptor);
        TableCache.getInstance().getColumnInterceptor(interceptor).addColumnAttributes(model, column);
    }

    /**
     * Modification des attributs d'une colonne.
     * 
     * @param column
     */
    public void modifyColumnAttributes(Column column)
    {
        TableCache.getInstance().getColumnInterceptor(column.getInterceptor()).modifyColumnAttributes(model, column);
    }

    /**
     * Retourne le nombre de colonnes.
     * 
     * @return int nombre de colonnes
     */
    public int columnCount()
    {
        return (this.columns != null ? this.columns.size() : 0);
    }

    /**
     * Retourne la liste des colonnes.
     * 
     * @return columns
     */
    public List<Column> getColumns()
    {
        return this.columns;
    }

    /**
     * Retourne une colonne identifiée par son alias.
     * 
     * @param alias alias de la colonne
     * @return Column
     */
    public Column getColumnByAlias(String alias)
    {
        Column result = null;
        for (Column column : this.columns)
        {
            String columnAlias = column.getAlias();
            if (columnAlias != null && columnAlias.equals(alias))
            {
                result = column;
            }
        }
        return result;
    }

    /**
     * methode Checks for metat data :
     * 
     * @return true, si c'est vrai
     */
    public boolean hasMetatData()
    {
        return columnCount() > 0;
    }

    /**
     * Accesseur de l attribut filter columns.
     * 
     * @return filter columns
     */
    public List<Column> getFilterColumns()
    {
        boolean cleared = this.model.getLimit().isCleared();
        String html = "";

        for (Column column : this.columns)
        {
            String value = this.model.getLimit().getFilterSet().getFilterValue(column.getAlias());
            if (cleared || StringUtils.isEmpty(value))
            {
                value = "";
            }
            Cell cell = TableModelUtils.getFilterCell(column, value);

            html = cell.getHtmlDisplay(this.model, column);

            column.setCellDisplay(html);
        }

        return this.columns;
    }

    /**
     * Accesseur de l attribut header columns.
     * 
     * @return header columns
     */
    public List<Column> getHeaderColumns()
    {
        for (Column column : this.columns)
        {
            Cell cell = TableModelUtils.getHeaderCell(column, column.getTitle());

            boolean isExported = this.model.getLimit().isExported();
            if (!isExported)
            {
                column.setCellDisplay(cell.getHtmlDisplay(this.model, column));
            }
            else
            {
                column.setCellDisplay(cell.getExportDisplay(this.model, column));
            }

        }

        return columns;
    }

    /**
     * Teste si la vue courante est utilisée pour une colonne donnée.
     * 
     * @param column
     * @return true, si c'est view allowed
     */
    private boolean isViewAllowed(Column column)
    {
        boolean result = false;

        String view = this.model.getTableHandler().getTable().getView();

        boolean isExported = this.model.getLimit().isExported();
        if (isExported)
        {
            view = this.model.getExportHandler().getCurrentExport().getView();
        }

        boolean allowView = allowView(column, view);
        boolean denyView = denyView(column, view);

        if (allowView & !denyView)
        {
            result = true;
        }

        return result;
    }

    /**
     * Teste si une vue est autorisée pour une colonne.
     * 
     * @param column
     * @param view
     * @return true, si c'est vrai
     */
    private boolean allowView(Column column, String view)
    {
        boolean result = false;
        List<String> viewsAllowed = column.getViewsAllowed();
        if (viewsAllowed == null || viewsAllowed.size() == 0)
        {
            result = true;
        }
        else
        {
            for (String aView : viewsAllowed)
            {
                if (view.equals(aView))
                {
                    result = true;
                    break;
                }
            }
        }

        return result;
    }

    /**
     * Teste si une vue est interdite pour une colonne.
     * 
     * @param column
     * @param view
     * @return boolean
     */
    private boolean denyView(Column column, String view)
    {
        boolean result = false;
        List<String> viewsDenied = column.getViewsDenied();
        if (viewsDenied == null || viewsDenied.size() == 0)
        {
            result = false;
        }
        else
        {
            for (String aView : viewsDenied)
            {
                if (view.equals(aView))
                {
                    result = true;
                    break;
                }
            }

        }
        return result;
    }

    /**
     * Retourne la première colonne contenant une cellule calculée.
     * 
     * @return Column
     */
    public Column getFirstCalcColumn()
    {
        Column result = null;
        for (Column column : this.columns)
        {
            if (column.isCalculated())
            {
                result = column;
                break;
            }
        }

        return result;
    }

    /**
     * Retourne la première colonne contenant l'attribut calcDisplay L'attribut calcDisplay ne doit être positionné que
     * dans une colonne, Toutes les cellules calculées d'une ligne doivent se comporter de la même façon (1 titre pas
     * ligne).
     * 
     * @return Column
     */
    public Column getFirstCalcDisplayColumn()
    {
        Column result = null;
        for (Column column : this.columns)
        {
            if (column.getCalcDisplay() != null && column.getCalcDisplay().size() > 0)
            {
                result = column;
                break;
            }
        }

        return result;
    }

    /**
     * Retourne la première colonne contenant l'attribut calcExport L'attribut calcExport ne doit être positionné que
     * dans une colonne, Toutes les cellules calculées d'une ligne doivent se comporter de la même façon (1 titre pas
     * ligne).
     * 
     * @return Column
     */
    public Column getFirstCalcExportColumn()
    {
        Column result = null;
        for (Column column : this.columns)
        {
            if (column.getCalcExport() != null && column.getCalcExport().size() > 0)
            {
                result = column;
                break;
            }
        }

        return result;
    }

    /**
     * Retourne la première colonne qui contient un titre de colonne calculée.
     * 
     * @return la première colonne qui contient un titre de colonne calculée
     */
    public Column getFirstCalcTitleColumn()
    {
        Column result = null;
        for (Column column : this.columns)
        {
            if (column.getCalcTitle() != null && column.getCalcTitle().size() > 0)
            {
                result = column;
                break;
            }
        }

        return result;
    }

    /**
     * Retourne la position de colonne qui contiendra les titres des cellules calculées. Si la première colonne qui
     * contient les définitions des titres de colonnes n'est pas la première colonne du tableau, la colonne dans
     * laquelle les titres seront affichés est la colonne précédente, sinon, c'est la première colonne.
     * 
     * @return Position de colonne qui contiendra les titres des cellules calculées
     */
    public int getNumeroColonneCalcTitle()
    {
        Column firstCalcColumn = this.getFirstCalcColumn();
        int colnocalctitle = 0;

        for (Column column : model.getColumnHandler().getColumns())
        {
            if (column == firstCalcColumn)
            {
                break;
            }
            colnocalctitle++;
        }

        if (!firstCalcColumn.isFirstColumn())
        {
            colnocalctitle = colnocalctitle - 1;
        }
        return colnocalctitle;
    }

    /**
     * Retourne la liste des objets CalcResult pour la colonne en paramètre.
     * 
     * @param column
     * @return Liste des objects CalcResult d'une colonne
     */
    public List<CalcResult> getCalcResults(Column column)
    {
        List<CalcResult> result = null;
        if (column.isCalculated())
        {
            result = CalcUtils.getCalcResults(this.model, column);
        }

        return result;
    }
}