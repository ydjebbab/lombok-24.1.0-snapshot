/*
/*Copyright (c) 2014 DGFiP - Tous droits réservés/* * Copyright 2004 original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package fr.gouv.finances.lombok.apptags.table.handler;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import fr.gouv.finances.lombok.apptags.table.bean.Row;
import fr.gouv.finances.lombok.apptags.table.core.PreferencesConstants;
import fr.gouv.finances.lombok.apptags.table.core.TableCache;
import fr.gouv.finances.lombok.apptags.table.core.TableModel;
import fr.gouv.finances.lombok.apptags.table.core.TableModelUtils;

/**
 * Class RowHandler
 */
public class RowHandler
{

    /** logger. */
    private static Log logger = LogFactory.getLog(RowHandler.class);

    /** model. */
    private TableModel model;

    /** row. */
    private Row row;

    /**
     * Instanciation de row handler.
     * 
     * @param model
     */
    public RowHandler(TableModel model)
    {
        this.model = model;
    }

    /**
     * Accesseur de l attribut model.
     * 
     * @return model
     */
    public TableModel getModel()
    {
        return model;
    }

    /**
     * Accesseur de l attribut row.
     * 
     * @return row
     */
    public Row getRow()
    {
        return row;
    }

    /**
     * methode Adds the row :
     * 
     * @param row
     */
    public void addRow(Row row)
    {
        this.row = row;
        addRowAttributes();
        row.defaults();
    }

    /**
     * methode Adds the row attributes :
     */
    public void addRowAttributes()
    {
        String interceptor =
            TableModelUtils.getInterceptPreference(model, row.getInterceptor(), PreferencesConstants.ROW_INTERCEPTOR);
        row.setInterceptor(interceptor);
        TableCache.getInstance().getRowInterceptor(interceptor).addRowAttributes(model, row);
    }

    /**
     * methode Modify row attributes :
     */
    public void modifyRowAttributes()
    {
        TableCache.getInstance().getRowInterceptor(row.getInterceptor()).modifyRowAttributes(model, row);
    }

    /**
     * Détermine si la cellule appartient à une ligne "impaire".
     * 
     * @return true, si c'est row even
     */
    public boolean isRowEven()
    {
        boolean result = false;
        if (row.getRowCount() != 0 && (row.getRowCount() % 2) == 0)
        {
            result = true;
        }

        return result;
    }

    /**
     * Détermine si la cellule appartient à une ligne "paire".
     * 
     * @return true, si c'est row odd
     */
    public boolean isRowOdd()
    {
        boolean result = true;
        if (row.getRowCount() != 0 && (row.getRowCount() % 2) == 0)
        {
            result = false;
        }

        return result;
    }

    /**
     * methode Increase row count :
     * 
     * @return int
     */
    public int increaseRowCount()
    {
        if (row == null)
        {
            StringBuilder   mes = new StringBuilder(92);
            mes.append("Aucun élément 'row' n'est défini : un élément 'row' doit contenir la définition des colonnes");
            logger.error(mes.toString());
            throw new IllegalStateException(mes.toString());
        }

        row.setRowCount(row.getRowCount() + 1);
        return row.getRowCount();
    }
}
