/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.apptags.table.view;

import java.util.Collection;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;
import net.sf.jasperreports.engine.data.JRAbstractBeanDataSource;

import org.apache.commons.beanutils.PropertyUtils;

/**
 * Class CpJRBeanCollectionDataSource
 */
public class CpJRBeanCollectionDataSource extends JRAbstractBeanDataSource
{

    /** data. */
    private Collection<Object> data = null;

    /** iterator. */
    private Iterator<Object> iterator = null;

    /** current bean. */
    private Object currentBean = null;

    /**
     * Instanciation de cp jr bean collection data source.
     * 
     * @param beanCollection
     */
    public CpJRBeanCollectionDataSource(Collection<Object> beanCollection)
    {
        this(beanCollection, true);
    }

    /**
     * Instanciation de cp jr bean collection data source.
     * 
     * @param beanCollection
     * @param isUseFieldDescription
     */
    public CpJRBeanCollectionDataSource(Collection<Object> beanCollection, boolean isUseFieldDescription)
    {
        super(isUseFieldDescription);

        this.data = beanCollection;

        if (this.data != null)
        {
            this.iterator = this.data.iterator();
        }
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see net.sf.jasperreports.engine.JRDataSource#next()
     */
    @Override
    public boolean next()
    {
        boolean hasNext = false;

        if (this.iterator != null)
        {
            hasNext = this.iterator.hasNext();

            if (hasNext)
            {
                this.currentBean = this.iterator.next();
            }
        }

        return hasNext;
    }

    /**
     * Accesseur de l attribut bean property.
     * 
     * @param bean
     * @param propertyName
     * @return bean property
     * @throws JRException the JR exception
     */
    protected static Object getBeanProperty(Object bean, String propertyName) throws JRException
    {
        Object value = null;

        if (bean != null)
        {
            try
            {
                value = PropertyUtils.getProperty(bean, propertyName);
                // Dans le cas d'un Enum, on retourne le résultat de la méthode toString de l'Enum
                if (value != null && value.getClass().isEnum())
                {
                    value = value.toString();
                }

            }
            catch (java.lang.IllegalAccessException exception)
            {
                throw new JRException("Error retrieving field value from bean : " + propertyName, exception);
            }
            catch (java.lang.reflect.InvocationTargetException exception)
            {
                throw new JRException("Error retrieving field value from bean : " + propertyName, exception);
            }
            catch (java.lang.NoSuchMethodException exception)
            {
                // throw new JRException("Error retrieving field
                // value from bean : " + propertyName, e);
                // Les propriétés inexistantes d'un objet
                // retournent une valeur nulle
            }
            catch (IllegalArgumentException exception)
            {
                // / replace with NestedNullException when
                // upgrading to BeanUtils 1.7
                if (!exception.getMessage().startsWith("Null property value for "))
                {
                    throw exception;
                }
            }
        }

        return value;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see net.sf.jasperreports.engine.data.JRAbstractBeanDataSource#getFieldValue(java.lang.Object,
     *      net.sf.jasperreports.engine.JRField)
     */
    @Override
    protected Object getFieldValue(Object bean, JRField field) throws JRException
    {
        return getBeanProperty(bean, getPropertyName(field));
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see net.sf.jasperreports.engine.JRDataSource#getFieldValue(net.sf.jasperreports.engine.JRField)
     */
    @Override
    public Object getFieldValue(JRField field) throws JRException
    {
        return getFieldValue(currentBean, field);
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see net.sf.jasperreports.engine.JRRewindableDataSource#moveFirst()
     */
    @Override
    public void moveFirst()
    {
        if (this.data != null)
        {
            this.iterator = this.data.iterator();
        }
    }

}
