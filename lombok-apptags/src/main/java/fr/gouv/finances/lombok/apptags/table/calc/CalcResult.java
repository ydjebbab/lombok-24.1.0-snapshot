/*
/*Copyright (c) 2014 DGFiP - Tous droits réservés/* * Copyright 2004 original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package fr.gouv.finances.lombok.apptags.table.calc;

/**
 * Class CalcResult
 */
public final class CalcResult
{

    /** name. */
    private final String name;

    /** value. */
    private final Number value;

    /**
     * Instanciation de calc result.
     * 
     * @param name
     * @param value
     */
    public CalcResult(String name, Number value)
    {
        this.name = name;
        this.value = value;
    }

    /**
     * Accesseur de l attribut name.
     * 
     * @return name
     */
    public String getName()
    {
        return name;
    }

    /**
     * Accesseur de l attribut value.
     * 
     * @return value
     */
    public Number getValue()
    {
        return value;
    }
}
