/*
 * Copyright (c) 2014 DGFiP - Tous droits réservés
 */
/*
/*Copyright (c) 2014 DGFiP - Tous droits réservés/* * Copyright 2004 original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package fr.gouv.finances.lombok.apptags.table.core;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import fr.gouv.finances.lombok.apptags.table.context.Context;
import fr.gouv.finances.lombok.apptags.table.state.State;

/**
 * Class AbstractRegistry
 */
public abstract class AbstractRegistry implements Registry
{

    /** parameter map. */
    protected Map<String, Object> parameterMap;

    /** context. */
    protected Context context;

    /** table id. */
    protected String tableId;

    /** prefix with table id. */
    protected String prefixWithTableId;

    /** state. */
    protected String state;

    /** state attr. */
    protected String stateAttr;

    /** auto include parameters. */
    protected boolean autoIncludeParameters;

    /**
     * Constructeur de la classe AbstractRegistry.java
     *
     */
    public AbstractRegistry()
    {
        super();
        
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.core.Registry#setParameterMap()
     */
    public void setParameterMap()
    {
        Map<String, Object> tableParameterMap = new HashMap<String, Object>();
        Map<String, Object> userDefinedParameterMap = new HashMap<String, Object>();

        Map<String, String[]> params = context.getParameterMap();
        for (Iterator<String> iter = params.keySet().iterator(); iter.hasNext();)
        {
            String paramName = iter.next();

            // Les paramètres liés aux exports ne sont jamais stockés
            if (paramName.equals(TableConstants.EXPORT_TABLE_ID)
                || paramName.equals(TableConstants.EXTREME_COMPONENTS_INSTANCE))
            {
                continue;
            }

            if (paramName.startsWith(prefixWithTableId + TableConstants.PAGE)
                || paramName.startsWith(prefixWithTableId + TableConstants.CURRENT_ROWS_DISPLAYED)
                || paramName.startsWith(prefixWithTableId + TableConstants.SORT)
                || paramName.startsWith(prefixWithTableId + TableConstants.FILTER)
                || paramName.startsWith(prefixWithTableId + TableConstants.EXPORT_VIEW)
                || paramName.startsWith(prefixWithTableId + TableConstants.EXPORT_FILE_NAME)
                || paramName.startsWith(prefixWithTableId + TableConstants.ALIAS))
            {
                String[] paramValues = TableModelUtils.getValueAsArray(params.get(paramName));
                tableParameterMap.put(paramName, paramValues);
            }
            else
            {
                if (autoIncludeParameters)
                {
                    String[] paramValues = TableModelUtils.getValueAsArray(params.get(paramName));
                    userDefinedParameterMap.put(paramName, paramValues);
                }
            }
        }

        this.parameterMap = handleState(tableParameterMap);
        parameterMap.putAll(userDefinedParameterMap);
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.core.Registry#handleState(java.util.Map)
     */
    @Override
    public Map<String, Object> handleState(Map<String, Object> tableParameterMap)
    {
        State state = TableCache.getInstance().getState(this.state);

        if (tableParameterMap.isEmpty())
        {
            Map<String, Object> stateParameters = state.getParameters(context, tableId, stateAttr);
            if (stateParameters != null)
            {
                tableParameterMap = stateParameters;
            }
        }

        handleStateInternal(state, tableParameterMap);

        return tableParameterMap;
    }

    /**
     * Ajout d'un paramètre supplémentaire dans le registre.
     * 
     * @param name
     * @param value
     */
    @Override
    public void addParameter(String name, Object value)
    {
        String[] paramValues = TableModelUtils.getValueAsArray(value);
        parameterMap.put(name, paramValues);
    }

    /**
     * Lecture d'un paramètre dans la Map parameterMap. Si on trouve plus d'un paramètre, seul le premier est retourné
     * 
     * @param parameter Le paramètre à lire
     * @return parameter
     */
    @Override
    public String getParameter(String parameter)
    {
        String[] values = (String[]) parameterMap.get(parameter);
        if (values != null && values.length > 0)
        {
            return values[0];
        }

        return null;
    }

    /**
     * retourne la Map des paramètres.
     * 
     * @return parameter map
     */
    @Override
    public Map<String, Object> getParameterMap()
    {
        return parameterMap;
    }

    /**
     * Supprime un paramètre donné du registre.
     * 
     * @param parameter Le paramètre à supprimer
     */
    @Override
    public void removeParameter(String parameter)
    {
        parameterMap.remove(parameter);
    }

    /**
     * methode Handle state internal :
     * 
     * @param state
     * @param tableParameterMap
     */
    protected abstract void handleStateInternal(State state, Map<String, Object> tableParameterMap);
}
