/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.apptags.table.view.jrxmlinfo;

import java.util.Collection;

import net.sf.jasperreports.engine.type.HorizontalAlignEnum;
import fr.gouv.finances.lombok.apptags.table.bean.Column;
import fr.gouv.finances.lombok.apptags.table.cell.PatternCell;
import fr.gouv.finances.lombok.apptags.table.core.TableModel;
import fr.gouv.finances.lombok.apptags.table.core.TableModelUtils;
import fr.gouv.finances.lombok.apptags.util.EcTableException;

/**
 * Class CelluleUtils
 */
public class CelluleUtils
{

    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /**
     * Détermine la classe des valeurs de la colonne.
     * 
     * @param column
     * @param model
     * @return the class<? extends object>
     */
    public static Class<? extends Object> findColumnValueClass(Column column, TableModel model)
    {
        Class<? extends Object> result = java.lang.String.class;

        if (column == null)
        {
            throw new IllegalArgumentException("L'OBJET COLUMN NE DOIT PAS ETRE NULL.");
        }

        // Utilise la dernière valeur de la collection pour
        // déterminer le type de la colonne
        if (column.getPropertyValue() != null)
        {
            result = column.getPropertyValue().getClass();
        }
        // Si la valeur est nulle, on itère dans la collection de bean
        // jusqu'à trouver une valeur non nulle
        else
        {
            Collection<Object> beans = model.getCollectionOfBeans();
            for (Object bean : beans)
            {
                Object propertyValue = TableModelUtils.getColumnPropertyValue(bean, column.getProperty());
                if (propertyValue != null)
                {
                    result = propertyValue.getClass();
                    break;
                }
            }

        }

        return result;
    }

    /**
     * Récupère la pattern de formatage associée à un type de cellule.
     * 
     * @param column
     * @return string
     */
    public static String findPattern(Column column)
    {
        String pattern = null;
        String cellType = column.getCell();
        Object objectCell = null;

        try
        {
            objectCell = Class.forName(cellType).newInstance();
        }
        catch (ClassNotFoundException exception)
        {
            throw new EcTableException(exception);
        }
        catch (InstantiationException exception)
        {
            throw new EcTableException(exception);
        }
        catch (IllegalAccessException exception)
        {
            throw new EcTableException(exception);
        }

        if (objectCell instanceof PatternCell)
        {
            pattern = ((PatternCell) objectCell).getFormatPattern();
        }

        return pattern;
    }

    /**
     * Récupère le type d'alignement associée à une cellule.
     * 
     * @param column
     * @return byte
     */
    public static HorizontalAlignEnum findAlignment(Column column)
    {
        HorizontalAlignEnum result = HorizontalAlignEnum.CENTER;
        int alignment = PatternCell.HORIZONTAL_ALIGN_CENTER;
        String cellType = column.getCell();
        Object objectCell = null;

        try
        {
            objectCell = Class.forName(cellType).newInstance();
        }
        catch (ClassNotFoundException exception)
        {
            throw new EcTableException(exception);
        }
        catch (InstantiationException exception)
        {
            throw new EcTableException(exception);
        }
        catch (IllegalAccessException exception)
        {
            throw new EcTableException(exception);
        }

        if (objectCell instanceof PatternCell)
        {
            alignment = ((PatternCell) objectCell).getAlignmentCell();
        }

        switch (alignment)
        {
            case PatternCell.HORIZONTAL_ALIGN_CENTER:
                result = HorizontalAlignEnum.CENTER;
                break;
            case PatternCell.HORIZONTAL_ALIGN_LEFT:
                result = HorizontalAlignEnum.LEFT;
                break;
            case PatternCell.HORIZONTAL_ALIGN_RIGHT:
                result = HorizontalAlignEnum.RIGHT;
                break;
            default:
                result = HorizontalAlignEnum.CENTER;
                break;
        }
        return result;
    }

}
