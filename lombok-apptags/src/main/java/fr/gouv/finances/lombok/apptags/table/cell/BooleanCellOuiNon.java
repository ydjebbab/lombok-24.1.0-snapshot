/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.apptags.table.cell;

import fr.gouv.finances.lombok.apptags.util.format.BooleanFormat.TypeFormat;

/**
 * Class BooleanCellOuiNon
 */
public class BooleanCellOuiNon extends AbstractBooleanCell
{

    /**
     * Instanciation de boolean cell oui non.
     */
    public BooleanCellOuiNon()
    {
        super();
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.cell.AbstractBooleanCell#getFormat()
     */
    @Override
    public TypeFormat getFormat()
    {
        return TypeFormat.OUI_NON;
    }
}
