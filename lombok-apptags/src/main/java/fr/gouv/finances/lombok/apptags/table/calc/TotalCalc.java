/*
/*Copyright (c) 2014 DGFiP - Tous droits réservés/* * Copyright 2004 original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package fr.gouv.finances.lombok.apptags.table.calc;

import java.math.BigDecimal;
import java.util.Collection;

import fr.gouv.finances.lombok.apptags.table.bean.Column;
import fr.gouv.finances.lombok.apptags.table.core.TableModel;

/**
 * Class TotalCalc
 */
public class TotalCalc implements Calc
{

    /**
     * Constructeur de la classe TotalCalc.java
     *
     */
    public TotalCalc()
    {
        super(); 
        
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.calc.Calc#getCalcResult(fr.gouv.finances.lombok.apptags.table.core.TableModel,
     *      fr.gouv.finances.lombok.apptags.table.bean.Column)
     */
    @Override
    public Number getCalcResult(TableModel model, Column column)
    {
        Collection<Object> rows = model.getCollectionOfFilteredBeans();
        String property = column.getProperty();
        TotalValue totalValue = new TotalValue();
        CalcUtils.eachRowCalcValue(totalValue, rows, property);

        return totalValue.getTotalValue();
    }

    /**
     * Class TotalValue
     */
    private static class TotalValue implements CalcHandler
    {

        /** total. */
        BigDecimal total = new BigDecimal(0);

        /**
         * (methode de remplacement) {@inheritDoc}
         * 
         * @see fr.gouv.finances.lombok.apptags.table.calc.CalcHandler#processCalcValue(java.lang.Number)
         */
        @Override
        public void processCalcValue(Number calcValue)
        {
            if (calcValue != null)
            {
                (total).add(new BigDecimal(String.valueOf(calcValue)));
            }
        }

        /**
         * Accesseur de l attribut total value.
         * 
         * @return total value
         */
        public Number getTotalValue()
        {
            return total;
        }
    }
}
