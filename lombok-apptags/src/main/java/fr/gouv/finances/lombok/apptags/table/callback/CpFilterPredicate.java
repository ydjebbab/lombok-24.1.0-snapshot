/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.apptags.table.callback;

import java.lang.reflect.InvocationTargetException;
import java.text.MessageFormat;
import java.util.Locale;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.collections.Predicate;
import org.apache.commons.lang.StringUtils;

import fr.gouv.finances.lombok.apptags.table.bean.Column;
import fr.gouv.finances.lombok.apptags.table.cell.PatternCell;
import fr.gouv.finances.lombok.apptags.table.core.TableModel;
import fr.gouv.finances.lombok.apptags.util.EcTableException;
import fr.gouv.finances.lombok.apptags.util.ExtremeUtils;

/**
 * Class CpFilterPredicate
 */
public final class CpFilterPredicate implements Predicate
{

    /** model. */
    private final TableModel model;

    /** Constant : MSGERR. */
    private static final String MSGERR = "La propriété {0} ne dispose pas de getter dans le bean {1}";

    /**
     * Instanciation de cp filter predicate.
     * 
     * @param model
     */
    public CpFilterPredicate(TableModel model)
    {
        this.model = model;
    }

    /**
     * Utilise les paramètres du filtre pour filtrer les lignes de la table.
     * 
     * @param bean
     * @return true, si c'est vrai
     */
    @Override
    public boolean evaluate(Object bean)
    {
        boolean match = false;

        for (Column column : model.getColumnHandler().getColumns())
        {
            String alias = column.getAlias();
            String filterValue = model.getLimit().getFilterSet().getFilterValue(alias);

            if (StringUtils.isEmpty(filterValue))
            {
                continue;
            }

            String property = column.getProperty();

            boolean isBeanContainProperty = CpContainPropertyPredicate.getInstance(property).evaluate(bean);

            if (!isBeanContainProperty)
            {
                continue;
            }

            Object value = null;
            try
            {
                value = PropertyUtils.getProperty(bean, property);
            }
            catch (IllegalAccessException exception)
            {
                Object[] args = {property, bean.toString()};
                String msg = MessageFormat.format(MSGERR, args);
                throw new IllegalArgumentException(msg, exception);
            }
            catch (InvocationTargetException exception)
            {
                Object[] args = {property, bean.toString()};
                String msg = MessageFormat.format(MSGERR, args);
                throw new IllegalArgumentException(msg, exception);
            }
            catch (NoSuchMethodException exception)
            {
                Object[] args = {property, bean.toString()};
                String msg = MessageFormat.format(MSGERR, args);
                throw new IllegalArgumentException(msg, exception);
            }

            if (value == null)
            {
                continue;
            }

            if (column.isDate())
            {
                Locale locale = model.getLocale();
                value = ExtremeUtils.formatDate(this.findPattern(column), this.findPattern(column), value, locale);
            }
            else if (column.isCurrency())
            {
                Locale locale = model.getLocale();
                value = ExtremeUtils.formatNumber(this.findPattern(column), value, locale);
            }

            if (!isSearchMatch(value.toString(), filterValue, column.isFilterStrict()))
            {
                match = false;

                break;
            }

            match = true;

        }

        return match;
    }

    /**
     * Verifie si search match.
     * 
     * @param value
     * @param search
     * @param filterStrict
     * @return true, si c'est search match
     */
    private boolean isSearchMatch(String value, String search, boolean filterStrict)
    {
        boolean result = false;
        String valueTrim = value.toLowerCase(Locale.FRANCE).trim();
        String searchTrim = search.toLowerCase(Locale.FRANCE).trim();

        if (filterStrict)
        {
            result = StringUtils.equalsIgnoreCase(value, search);
        }
        else if (searchTrim.charAt(0) == '*' && valueTrim.endsWith(StringUtils.replace(searchTrim, "*", "")))
        {
            result = true;
        }
        else if (searchTrim.endsWith("*") && valueTrim.startsWith(StringUtils.replace(searchTrim, "*", "")))
        {
            result = true;
        }
        else if (StringUtils.contains(valueTrim, searchTrim))
        {
            result = true;
        }

        return result;
    }

    /**
     * Récupère la pattern de formatage associée à un type de cellule.
     * 
     * @param column
     * @return string
     */
    private String findPattern(Column column)
    {
        String pattern = null;
        String cellType = column.getCell();
        Object objectCell = null;

        try
        {
            objectCell = Class.forName(cellType).newInstance();
        }
        catch (ClassNotFoundException exception)
        {
            throw new EcTableException(exception);
        }
        catch (InstantiationException exception)
        {
            throw new EcTableException(exception);
        }
        catch (IllegalAccessException exception)
        {
            throw new EcTableException(exception);
        }

        if (objectCell instanceof PatternCell)
        {
            pattern = ((PatternCell) objectCell).getFormatPattern();
        }

        return pattern;
    }
}
