/*
 * Copyright (c) 2014 DGFiP - Tous droits réservés
 */
/*
/*Copyright (c) 2014 DGFiP - Tous droits réservés/* * Copyright 2004 original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package fr.gouv.finances.lombok.apptags.table.core;

import java.util.Collection;
import java.util.Locale;

import fr.gouv.finances.lombok.apptags.table.bean.Column;
import fr.gouv.finances.lombok.apptags.table.bean.Export;
import fr.gouv.finances.lombok.apptags.table.bean.Row;
import fr.gouv.finances.lombok.apptags.table.bean.Table;
import fr.gouv.finances.lombok.apptags.table.context.Context;
import fr.gouv.finances.lombok.apptags.table.handler.ColumnHandler;
import fr.gouv.finances.lombok.apptags.table.handler.ExportHandler;
import fr.gouv.finances.lombok.apptags.table.handler.RowHandler;
import fr.gouv.finances.lombok.apptags.table.handler.TableHandler;
import fr.gouv.finances.lombok.apptags.table.handler.ViewHandler;
import fr.gouv.finances.lombok.apptags.table.limit.Limit;

/**
 * Interface TableModel
 */
public interface TableModel
{

    /**
     * Accesseur de l attribut context.
     * 
     * @return context
     */
    public Context getContext();

    /**
     * Accesseur de l attribut preferences.
     * 
     * @return preferences
     */
    public Preferences getPreferences();

    /**
     * Accesseur de l attribut messages.
     * 
     * @return messages
     */
    public Messages getMessages();

    /**
     * Accesseur de l attribut registry.
     * 
     * @return registry
     */
    public Registry getRegistry();

    /**
     * Accesseur de l attribut table instance.
     * 
     * @return table instance
     */
    public Table getTableInstance();

    /**
     * Accesseur de l attribut export instance.
     * 
     * @return export instance
     */
    public Export getExportInstance();

    /**
     * Accesseur de l attribut row instance.
     * 
     * @return row instance
     */
    public Row getRowInstance();

    /**
     * Accesseur de l attribut column instance.
     * 
     * @return column instance
     */
    public Column getColumnInstance();

    /**
     * methode Adds the table :
     * 
     * @param table
     */
    public void addTable(Table table);

    /**
     * methode Adds the export :
     * 
     * @param export
     */
    public void addExport(Export export);

    /**
     * methode Adds the row :
     * 
     * @param row
     */
    public void addRow(Row row);

    /**
     * methode Adds the column :
     * 
     * @param column
     */
    public void addColumn(Column column);

    /**
     * methode Adds the columns :
     * 
     * @param autoGenerateColumns
     */
    public void addColumns(String autoGenerateColumns);

    /**
     * methode Adds the parameter :
     * 
     * @param name
     * @param value
     */
    public void addParameter(String name, Object value);

    /**
     * Accesseur de l attribut table handler.
     * 
     * @return table handler
     */
    public TableHandler getTableHandler();

    /**
     * Accesseur de l attribut row handler.
     * 
     * @return row handler
     */
    public RowHandler getRowHandler();

    /**
     * Accesseur de l attribut column handler.
     * 
     * @return column handler
     */
    public ColumnHandler getColumnHandler();

    /**
     * Accesseur de l attribut view handler.
     * 
     * @return view handler
     */
    public ViewHandler getViewHandler();

    /**
     * Accesseur de l attribut export handler.
     * 
     * @return export handler
     */
    public ExportHandler getExportHandler();

    /**
     * Accesseur de l attribut current row bean.
     * 
     * @return current row bean
     */
    public Object getCurrentRowBean();

    /**
     * Modificateur de l attribut current row bean.
     * 
     * @param bean le nouveau current row bean
     */
    public void setCurrentRowBean(Object bean);

    /**
     * Accesseur de l attribut collection of beans.
     * 
     * @return collection of beans
     */
    public Collection<Object> getCollectionOfBeans();

    /**
     * Modificateur de l attribut collection of beans.
     * 
     * @param collectionOfBeans le nouveau collection of beans
     */
    public void setCollectionOfBeans(Collection<Object> collectionOfBeans);

    /**
     * Accesseur de l attribut collection of filtered beans.
     * 
     * @return collection of filtered beans
     */
    public Collection<Object> getCollectionOfFilteredBeans();

    /**
     * Modificateur de l attribut collection of filtered beans.
     * 
     * @param collectionOfFilteredBeans le nouveau collection of filtered beans
     */
    public void setCollectionOfFilteredBeans(Collection<Object> collectionOfFilteredBeans);

    /**
     * Accesseur de l attribut collection of page beans.
     * 
     * @return collection of page beans
     */
    public Collection<Object> getCollectionOfPageBeans();

    /**
     * Modificateur de l attribut collection of page beans.
     * 
     * @param collectionOfPageBeans le nouveau collection of page beans
     */
    public void setCollectionOfPageBeans(Collection<Object> collectionOfPageBeans);

    /**
     * Accesseur de l attribut collection of beans from start to current page.
     * 
     * @return collection of beans from start to current page
     */
    public Collection<Object> getCollectionOfBeansFromStartToCurrentPage();

    /**
     * Modificateur de l attribut collection of beans from start to current page.
     * 
     * @param collectionOfBeansFromStartToCurrentPage le nouveau collection of beans from start to current page
     */
    public void setCollectionOfBeansFromStartToCurrentPage(Collection<Object> collectionOfBeansFromStartToCurrentPage);

    /**
     * Accesseur de l attribut limit.
     * 
     * @return limit
     */
    public Limit getLimit();

    /**
     * Modificateur de l attribut limit.
     * 
     * @param limit le nouveau limit
     */
    public void setLimit(Limit limit);

    /**
     * Accesseur de l attribut locale.
     * 
     * @return locale
     */
    public Locale getLocale();

    /**
     * methode Execute :
     * 
     * @return collection
     * @throws Exception the exception
     */
    public Collection<Object> execute() throws Exception;

    /**
     * methode Sets the column values :
     * 
     * @throws Exception the exception
     */
    public void setColumnValues() throws Exception;

    /**
     * Accesseur de l attribut view data.
     * 
     * @return view data
     * @throws Exception the exception
     */
    public Object getViewData() throws Exception;

    /**
     * methode Assemble :
     * 
     * @return object
     * @throws Exception the exception
     */
    public Object assemble() throws Exception;

    /**
     * Accesseur de l attribut row count.
     * 
     * @return row count
     */
    public int getRowCount();

    /**
     * Accesseur de l attribut page count.
     * 
     * @return page count
     */
    public int getPageCount();

    /**
     * Accesseur de l attribut total row count.
     * 
     * @return total row count
     */
    public int getTotalRowCount();

    /**
     * Accesseur de l attribut total page count.
     * 
     * @return total page count
     */
    public int getTotalPageCount();

    /**
     * Verifie si last page.
     * 
     * @return true, si c'est last page
     */
    public boolean isLastPage();

    /**
     * Verifie si first page.
     * 
     * @return true, si c'est first page
     */
    public boolean isFirstPage();

}