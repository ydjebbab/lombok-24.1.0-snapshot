/*
/*Copyright (c) 2014 DGFiP - Tous droits réservés/* * Copyright 2004 original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package fr.gouv.finances.lombok.apptags.table.limit;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.ToStringBuilder;

import fr.gouv.finances.lombok.apptags.table.context.Context;
import fr.gouv.finances.lombok.apptags.table.core.Registry;
import fr.gouv.finances.lombok.apptags.table.core.TableConstants;

/**
 * Une factory pour creer des objets AbstractLimit .
 */
public abstract class AbstractLimitFactory implements LimitFactory
{

    /** table id. */
    protected String tableId;

    /** prefix with table id. */
    protected String prefixWithTableId;

    /** is exported. */
    protected boolean isExported;

    /** registry. */
    protected Registry registry;

    /** context. */
    protected Context context;

    /**
     * Constructeur de la classe AbstractLimitFactory.java
     */
    public AbstractLimitFactory()
    {
        super();

    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.limit.LimitFactory#isExported()
     */
    @Override
    public boolean isExported()
    {
        return isExported;
    }

    /**
     * Accesseur de l attribut exported.
     * 
     * @return exported
     */
    public boolean getExported()
    {
        boolean result = false;
        String exportTableId = context.getParameter(TableConstants.EXPORT_TABLE_ID);

        if (StringUtils.isBlank(exportTableId))
        {
            result = false;
        }

        else if (exportTableId.equals(tableId))
        {
            result = true;
        }

        return result;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.limit.LimitFactory#getCurrentRowsDisplayed(int, int)
     */
    @Override
    public int getCurrentRowsDisplayed(int totalRows, int rowsDisplayed)
    {
        int result = rowsDisplayed;
        if (isExported || !showPagination())
        {
            result = totalRows;
        }
        else
        {

            String currentRowsDisplayed =
                registry.getParameter(prefixWithTableId + TableConstants.CURRENT_ROWS_DISPLAYED);
            if (StringUtils.isNotBlank(currentRowsDisplayed))
            {
                return Integer.parseInt(currentRowsDisplayed);
            }
        }

        return result;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.limit.LimitFactory#getPage(int)
     */
    @Override
    public int getPage(int startPage)
    {
        int result = startPage;
        if (!isExported)
        {
            String page = registry.getParameter(prefixWithTableId + TableConstants.PAGE);
            if (!StringUtils.isEmpty(page))
            {
                result = Integer.parseInt(page);
            }
        }

        return result;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.limit.LimitFactory#getSort()
     */
    @Override
    public Sort getSort()
    {
        Sort result = new Sort();
        Map<String, String> sortedParameters = getSortedOrFilteredParameters(TableConstants.SORT);

        if (sortedParameters != null)
        {
            for (Iterator<String> iter = sortedParameters.keySet().iterator(); iter.hasNext();)
            {
                String propertyOrAlias = iter.next();
                String value = sortedParameters.get(propertyOrAlias);

                if (value.equals(TableConstants.SORT_DEFAULT))
                {
                    result = new Sort();
                    break;
                }
                else
                {
                    String property = getProperty(propertyOrAlias);
                    result = new Sort(propertyOrAlias, property, value);
                }
            }
        }
        return result;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.limit.LimitFactory#getFilterSet()
     */
    @Override
    public FilterSet getFilterSet()
    {
        Map<String, String> filteredParameters = getSortedOrFilteredParameters(TableConstants.FILTER);
        FilterSet filterSet = getFilterSet(filteredParameters);

        if (filterSet.isCleared())
        {
            removeFilterParameters();
            filterSet = new FilterSet(filterSet.getAction(), new Filter[] {});
        }

        return filterSet;
    }

    /**
     * Supprime les paramètres de filtre du registry.
     */
    public void removeFilterParameters()
    {
        Set<String> set = registry.getParameterMap().keySet();
        for (Iterator<String> iter = set.iterator(); iter.hasNext();)
        {
            String name = iter.next();
            if (name.startsWith(prefixWithTableId + TableConstants.FILTER))
            {
                iter.remove();
            }
        }
    }

    /**
     * Accesseur de l attribut filter set.
     * 
     * @param filteredParameters
     * @return filter set
     */
    public FilterSet getFilterSet(Map<String, String> filteredParameters)
    {
        FilterSet result = new FilterSet();
        if (filteredParameters != null)
        {
            String action = filteredParameters.get(TableConstants.ACTION);
            List<Filter> filters = new ArrayList<Filter>();

            for (Iterator<String> iter = filteredParameters.keySet().iterator(); iter.hasNext();)
            {
                String propertyOrAlias = iter.next();
                String value = filteredParameters.get(propertyOrAlias);

                if (StringUtils.isBlank(value) || propertyOrAlias.equals(TableConstants.ACTION))
                {
                    continue;
                }

                String property = getProperty(propertyOrAlias);
                filters.add(new Filter(propertyOrAlias, property, value));
            }
            result = new FilterSet(action, filters.toArray(new Filter[filters.size()]));
        }
        return result;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.limit.LimitFactory#getSortedOrFilteredParameters(java.lang.String)
     */
    @Override
    public Map<String, String> getSortedOrFilteredParameters(String parameter)
    {
        Map<String, String> subset = new HashMap<String, String>();

        String find = prefixWithTableId + parameter;

        Set<String> set = registry.getParameterMap().keySet();
        for (String key : set)
        {
            if (key.startsWith(find))
            {
                String value = registry.getParameter(key);
                if (StringUtils.isNotBlank(value))
                {
                    String propertyOrAlias = StringUtils.substringAfter(key, find);
                    subset.put(propertyOrAlias, value);
                }
            }
        }

        return subset;
    }

    /**
     * Accesseur de l attribut property.
     * 
     * @param propertyOrAlias
     * @return property
     */
    private String getProperty(String propertyOrAlias)
    {
        String result = propertyOrAlias;
        String property = registry.getParameter(prefixWithTableId + TableConstants.ALIAS + propertyOrAlias);
        if (StringUtils.isNotBlank(property))
        {
            result = property;
        }

        return result;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        ToStringBuilder builder = new ToStringBuilder(this);
        builder.append("tableId", tableId);
        return builder.toString();
    }

    /**
     * methode Show pagination :
     * 
     * @return true, si c'est vrai
     */
    protected abstract boolean showPagination();
}
