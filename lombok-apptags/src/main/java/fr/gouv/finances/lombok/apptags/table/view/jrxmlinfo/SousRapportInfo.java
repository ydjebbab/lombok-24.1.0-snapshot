/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.apptags.table.view.jrxmlinfo;

/**
 * Class SousRapportInfo
 */
public class SousRapportInfo
{

    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** cle parametre. */
    private String cleParametre;

    /** file jrxml. */
    private String fileJRXML;

    /**
     * Instanciation de sous rapport info.
     */
    public SousRapportInfo()
    {
        super();
    }

    /**
     * Accesseur de l attribut cle parametre.
     * 
     * @return cle parametre
     */
    public String getCleParametre()
    {
        return cleParametre;
    }

    /**
     * Modificateur de l attribut cle parametre.
     * 
     * @param cleParametre le nouveau cle parametre
     */
    public void setCleParametre(String cleParametre)
    {
        this.cleParametre = cleParametre;
    }

    /**
     * Accesseur de l attribut file jrxml.
     * 
     * @return file jrxml
     */
    public String getFileJRXML()
    {
        return fileJRXML;
    }

    /**
     * Modificateur de l attribut file jrxml.
     * 
     * @param fileJRXML le nouveau file jrxml
     */
    public void setFileJRXML(String fileJRXML)
    {
        this.fileJRXML = fileJRXML;
    }

}
