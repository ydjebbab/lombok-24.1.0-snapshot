/*
 * Copyright (c) 2014 DGFiP - Tous droits réservés
 */
/*
/*Copyright (c) 2014 DGFiP - Tous droits réservés/* * Copyright 2004 original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package fr.gouv.finances.lombok.apptags.table.core;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import fr.gouv.finances.lombok.apptags.table.context.Context;
import fr.gouv.finances.lombok.apptags.util.ExtremeUtils;

/**
 * Class RetrievalUtils
 */
public class RetrievalUtils
{

    /** logger. */
    private static Log logger = LogFactory.getLog(RetrievalUtils.class);

    /**
     * methode Retrieve :
     * 
     * @param context
     * @param name
     * @return object
     * @see #retrieve(Context, String, String)
     */
    public static Object retrieve(Context context, String name)
    {
        return retrieve(context, name, null);
    }

    /**
     * Recherche un objet par son nom dans le contexte de servlet spécifié en paramètre. Si le paramètre 'scope' est
     * null, recherche dans tous les contextes dans l'ordre suivant : page, request, session et application
     * 
     * @param context
     * @param name
     * @param scope
     * @return object
     */
    public static Object retrieve(Context context, String name, String scope)
    {
        Object value = null;
        if (StringUtils.isNotBlank(scope))
        {
            if (scope.equalsIgnoreCase(TableConstants.PAGE_SCOPE))
            {
                value = context.getPageAttribute(name);
            }
            else if (scope.equalsIgnoreCase(TableConstants.REQUEST_SCOPE))
            {
                value = context.getRequestAttribute(name);
            }
            else if (scope.equalsIgnoreCase(TableConstants.SESSION_SCOPE))
            {
                value = context.getSessionAttribute(name);
            }
            else if (scope.equalsIgnoreCase(TableConstants.APPLICATION_SCOPE))
            {
                value = context.getApplicationAttribute(name);
            }
        }
        else
        {
            value = context.getPageAttribute(name);
            if (value == null)
            {
                value = context.getRequestAttribute(name);
            }
            else if (value == null)
            {
                value = context.getSessionAttribute(name);
            }
            else if (value == null)
            {
                value = context.getApplicationAttribute(name);
            }
        }
        return value;
    }

    /**
     * methode Retrieve collection :
     * 
     * @param <T>
     * @param context
     * @param collection
     * @return collection
     * @throws Exception the exception
     * @see #retrieveCollection(Context, Object, String)
     */
    public static <T> Collection<T> retrieveCollection(Context context, Object collection) throws Exception
    {
        return retrieveCollection(context, collection, null);
    }

    /**
     * If the collection variable passed in is in fact a Collection then just return it. If it is a Map then return the
     * Map values. If it is a String then look in the specified servlet scope for the Collection. If the scope is null
     * look through the scopes in order (page, request, session, and application). If the collection variable is
     * specified with a dot (.) notation then will look for a nested collection. For example foo.values will first look
     * for an object called foo in the various servlet scopes (as described above). Once it finds it then it will look
     * for an attribute called values, which is assumed to be a Collection.
     * 
     * @param <T>
     * @param context
     * @param collection Either a String or Object that will represent the Collection.
     * @param scope
     * @return A Collection.
     * @throws Exception the exception
     */
    @SuppressWarnings("unchecked")
    public static <T> Collection<T> retrieveCollection(Context context, Object collection, String scope)
        throws Exception
    {
        Collection<T> result = Collections.emptyList();
        if (collection instanceof Collection)
        {
            result = (Collection<T>) collection;
        }
        else if (collection instanceof Map)
        {
            result = ((Map) collection).values();
        }
        else if (collection instanceof String)
        {
            result = retrieveCollectionFromScope(context, String.valueOf(collection), scope);
        }
        else
        {
            if (logger.isDebugEnabled())
            {
                logger.debug("Impossible de trouver la collection");
            }
        }
        return result;
    }

    /**
     * methode Retrieve collection from scope :
     * 
     * @param <T>
     * @param context
     * @param collection
     * @param scope
     * @return collection
     * @throws Exception the exception
     */
    public static <T> Collection<T> retrieveCollectionFromScope(Context context, String collection, String scope)
        throws Exception
    {
        Collection<T> results = null;

        if (StringUtils.isBlank(collection) || "null".equals(collection))
        {
            if (logger.isDebugEnabled())
            {
                logger.debug("The collection is not defined.");
            }

            results = Collections.emptyList();
        }

        else if (StringUtils.contains(collection, "."))
        {
            results = retrieveNestedCollection(context, collection, scope);
        }
        else
        {
            results = retrieveCollectionAsObject(context, collection, scope);
        }

        if (results == null)
        {
            if (logger.isDebugEnabled())
            {
                logger.debug("Could not find the Collection.");
            }

            results = Collections.emptyList();
        }

        return results;
    }

    /**
     * methode Retrieve nested collection :
     * 
     * @param <T>
     * @param context
     * @param collection
     * @param scope
     * @return collection
     * @throws Exception the exception
     */
    @SuppressWarnings("unchecked")
    public static <T> Collection<T> retrieveNestedCollection(Context context, String collection, String scope)
        throws Exception
    {
        Collection<T> result = Collections.emptyList();
        String[] split = StringUtils.split(collection, ".");
        Object obj = RetrievalUtils.retrieve(context, split[0], scope);
        String collectionToFind = StringUtils.substringAfter(collection, ".");
        if (ExtremeUtils.isBeanPropertyReadable(obj, collectionToFind))
        {
            obj = PropertyUtils.getProperty(obj, collectionToFind);
        }

        if (!(obj instanceof Collection))
        {
            if (logger.isDebugEnabled())
            {
                logger.debug("The object is not of type Collection.");
            }

            result = Collections.emptyList();
        }
        else
        {
            result = (Collection<T>) obj;
        }

        return result;
    }

    /**
     * methode Retrieve collection as object :
     * 
     * @param <T>
     * @param context
     * @param collection
     * @param scope
     * @return collection
     * @throws Exception the exception
     */
    @SuppressWarnings("unchecked")
    public static <T> Collection<T> retrieveCollectionAsObject(Context context, String collection, String scope)
        throws Exception
    {
        Collection<T> result = Collections.emptyList();
        Object obj = RetrievalUtils.retrieve(context, collection, scope);
        if (obj instanceof Collection)
        {
            result = (Collection<T>) obj;
        }
        else if (obj instanceof Map)
        {
            result = ((Map) obj).values();
        }
        else
        {
            if (logger.isDebugEnabled())
            {
                logger.debug("The object is not of type Collection.");
            }

            return Collections.emptyList();
        }
        return result;
    }
}
