/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.apptags.table.interceptor;

import fr.gouv.finances.lombok.apptags.table.bean.Row;
import fr.gouv.finances.lombok.apptags.table.core.TableModel;

/**
 * Class RowCountInterceptor
 */
public class RowCountInterceptor implements RowInterceptor
{

    /**
     * Constructeur de la classe RowCountInterceptor.java
     *
     */
    public RowCountInterceptor()
    {
        super();
        
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.interceptor.RowInterceptor#addRowAttributes(fr.gouv.finances.lombok.apptags.table.core.TableModel,
     *      fr.gouv.finances.lombok.apptags.table.bean.Row)
     */
    @Override
    public void addRowAttributes(TableModel arg0, Row arg1)
    {
        // RAS
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.interceptor.RowInterceptor#modifyRowAttributes(fr.gouv.finances.lombok.apptags.table.core.TableModel,
     *      fr.gouv.finances.lombok.apptags.table.bean.Row)
     */
    @Override
    public void modifyRowAttributes(TableModel tableModel, Row row)
    {
        int rowcount =
            ((tableModel.getLimit().getPage() - 1) * tableModel.getLimit().getCurrentRowsDisplayed())
                + tableModel.getRowHandler().getRow().getRowCount();
        tableModel.getContext().setPageAttribute("COROWCOUNT", String.valueOf(rowcount));
    }
}
