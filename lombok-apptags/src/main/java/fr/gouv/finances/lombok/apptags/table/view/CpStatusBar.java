/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.apptags.table.view;

import fr.gouv.finances.lombok.apptags.table.core.Messages;
import fr.gouv.finances.lombok.apptags.table.core.TableModel;
import fr.gouv.finances.lombok.apptags.table.view.html.BuilderConstants;
import fr.gouv.finances.lombok.apptags.table.view.html.BuilderUtils;
import fr.gouv.finances.lombok.apptags.table.view.html.CpStatusBarBuilder;
import fr.gouv.finances.lombok.apptags.table.view.html.ToolbarBuilder;
import fr.gouv.finances.lombok.apptags.table.view.html.SingleColumnRowLayout;
import fr.gouv.finances.lombok.apptags.util.HtmlBuilder;

/**
 * Class CpStatusBar
 * 
 * @author Jeff Johnston
 */
public class CpStatusBar extends SingleColumnRowLayout
{

    /** messages. */
    private Messages messages;

    /**
     * Instanciation de cp status bar.
     * 
     * @param html
     * @param model
     */
    public CpStatusBar(HtmlBuilder html, TableModel model)
    {
        super(html, model);
        messages = model.getMessages();
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.view.html.SingleColumnRowLayout#showLayout(fr.gouv.finances.lombok.apptags.table.core.TableModel)
     */
    @Override
    protected boolean showLayout(TableModel model)
    {
        boolean showStatusBar = BuilderUtils.showStatusBar(model);
        boolean filterable = BuilderUtils.filterable(model);
        if (!showStatusBar && !filterable)
        {
            return false;
        }

        return true;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.view.html.SingleColumnRowLayout#singleColumn(fr.gouv.finances.lombok.apptags.util.HtmlBuilder,
     *      fr.gouv.finances.lombok.apptags.table.core.TableModel)
     */
    @Override
    protected void singleColumn(HtmlBuilder html, TableModel model)
    {
        boolean showStatusBar = BuilderUtils.showStatusBar(model);
        if (!showStatusBar)
        {
            return;
        }

        html.td(4).styleClass(BuilderConstants.STATUS_BAR_CSS).close();

        new CpStatusBarBuilder(html, model).statusMessageNbLignesTrouvees();

        // Lecture de l'attribut id du tableau
        final String tableId = model.getTableHandler().getTable().getTableId();

        MaxFetchTableAttributes maxFetchAttr = MaxFetchTableAttributes.getDefault(tableId);
        Object maxFetchAttrInContext = model.getContext().getRequestAttribute(maxFetchAttr.getScopeName());

        if (maxFetchAttrInContext instanceof MaxFetchTableAttributes)
        {
            Boolean crit =
                (Boolean) model.getContext().getRequestAttribute(
                    ((MaxFetchTableAttributes) maxFetchAttrInContext).getReponseComplete());

            if ((crit != null) && !(crit.booleanValue()))
            {
                new CpStatusBarBuilder(html, model).statusMessageAlerteReponseIncomplete();
            }
        }
        new CpStatusBarBuilder(html, model).statusMessageLignesAffichees();

        if (BuilderUtils.showNewPagination(model))
        {
            html.append(" | ");
            html.append(messages.getMessage(BuilderConstants.TOOLBAR_ROWS_DISPLAYED_TEXT) + ": ");
            new ToolbarBuilder(html, model).rowsDisplayedDroplist(); // ADDED

        }

        html.tdEnd();
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.view.html.SingleColumnRowLayout#singleColumn(fr.gouv.finances.lombok.apptags.util.HtmlBuilder,
     *      fr.gouv.finances.lombok.apptags.table.core.TableModel)
     */
    @Override
    protected void singleColumnRWD(HtmlBuilder html, TableModel model)
    {
        boolean showStatusBar = BuilderUtils.showStatusBar(model);
        if (!showStatusBar)
        {
            return;
        }

        html.div().styleClass(BuilderConstants.STATUS_BAR_CSS).close();

        new CpStatusBarBuilder(html, model).statusMessageNbLignesTrouvees();

        // Lecture de l'attribut id du tableau
        final String tableId = model.getTableHandler().getTable().getTableId();

        MaxFetchTableAttributes maxFetchAttr = MaxFetchTableAttributes.getDefault(tableId);
        Object maxFetchAttrInContext = model.getContext().getRequestAttribute(maxFetchAttr.getScopeName());

        if (maxFetchAttrInContext instanceof MaxFetchTableAttributes)
        {
            Boolean crit =
                (Boolean) model.getContext().getRequestAttribute(
                    ((MaxFetchTableAttributes) maxFetchAttrInContext).getReponseComplete());

            if ((crit != null) && !(crit.booleanValue()))
            {
                new CpStatusBarBuilder(html, model).statusMessageAlerteReponseIncomplete();
            }
        }
        new CpStatusBarBuilder(html, model).statusMessageLignesAffichees();

        if (BuilderUtils.showNewPagination(model))
        {
            html.append(" | ");
            html.append(messages.getMessage(BuilderConstants.TOOLBAR_ROWS_DISPLAYED_TEXT) + ": ");
            new ToolbarBuilder(html, model).rowsDisplayedDroplist(); // ADDED

        }

        html.divEnd(); 
    }

}
