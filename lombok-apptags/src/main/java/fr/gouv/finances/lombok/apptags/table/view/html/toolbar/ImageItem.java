/*
/*Copyright (c) 2014 DGFiP - Tous droits réservés/* * Copyright 2004 original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package fr.gouv.finances.lombok.apptags.table.view.html.toolbar;

import fr.gouv.finances.lombok.apptags.table.core.TableModel;
import fr.gouv.finances.lombok.apptags.util.HtmlBuilder;

/**
 * Class ImageItem
 */
public class ImageItem extends AbstractItem implements ToolbarItem
{

    /** image. */
    String image;

    /** disabled image. */
    String disabledImage;

    /** alt. */
    String alt;

    /**
     * Constructeur de la classe ImageItem.java
     *
     */
    public ImageItem()
    {
        super();
        
    }

    /**
     * Accesseur de l attribut disabled image.
     * 
     * @return disabled image
     */
    public String getDisabledImage()
    {
        return disabledImage;
    }

    /**
     * Modificateur de l attribut disabled image.
     * 
     * @param disabledImage le nouveau disabled image
     */
    public void setDisabledImage(String disabledImage)
    {
        this.disabledImage = disabledImage;
    }

    /**
     * Accesseur de l attribut image.
     * 
     * @return image
     */
    public String getImage()
    {
        return image;
    }

    /**
     * Modificateur de l attribut image.
     * 
     * @param image le nouveau image
     */
    public void setImage(String image)
    {
        this.image = image;
    }

    /**
     * methode Sets the image :
     */
    public void setImage()
    {

    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.view.html.toolbar.AbstractItem#disabled(fr.gouv.finances.lombok.apptags.util.HtmlBuilder)
     */
    public void disabled(HtmlBuilder html)
    {
        html.img().src(getDisabledImage()).style(getStyle()).alt(getAlt()).xclose();
    }

    /**
     * Accesseur de l attribut alt.
     * 
     * @return alt
     */
    public String getAlt()
    {
        return alt;
    }

    /**
     * Modificateur de l attribut alt.
     * 
     * @param alt le nouveau alt
     */
    public void setAlt(String alt)
    {
        this.alt = alt;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.view.html.toolbar.AbstractItem#enabled(fr.gouv.finances.lombok.apptags.util.HtmlBuilder,
     *      fr.gouv.finances.lombok.apptags.table.core.TableModel)
     */
    public void enabled(HtmlBuilder html, TableModel model)
    {
        html.a();
        html.quote();
        html.append(getAction());
        html.quote().close();

        boolean showTooltips = model.getTableHandler().getTable().isShowTooltips();
        if (showTooltips)
        {
            html.img().src(getImage()).style(getStyle()).title(getTooltip()).onmouseover(getOnmouseover()).onmouseout(
                getOnmouseout()).alt(getAlt()).xclose();
        }
        else
        {
            html.img().src(getImage()).style(getStyle()).onmouseover(getOnmouseover()).onmouseout(getOnmouseout()).alt(
                getAlt()).xclose();
        }

        html.aEnd();
    }
}
