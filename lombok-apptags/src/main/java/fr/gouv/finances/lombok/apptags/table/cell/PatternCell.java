/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.apptags.table.cell;

import fr.gouv.finances.lombok.apptags.table.bean.Column;
import fr.gouv.finances.lombok.apptags.table.core.TableModel;

/**
 * Interface PatternCell
 */
public interface PatternCell
{

    /** Alignement à gauche du contenu d'un cellule. */
    public static int HORIZONTAL_ALIGN_LEFT = 1;

    /** Centrage du contenu d'un cellule. */
    public static int HORIZONTAL_ALIGN_CENTER = 2;

    /** Alignement à droite du contenu d'un cellule. */
    public static int HORIZONTAL_ALIGN_RIGHT = 3;

    /**
     * Retourne la pattern de formatage d'une cellule.
     * 
     * @return format pattern
     */
    public String getFormatPattern();

    /**
     * Retourne le mode d'alignement du contenu d'une cellule HORIZONTAL_ALIGN_LEFT ou HORIZONTAL_ALIGN_CENTER ou
     * HORIZONTAL_ALIGN_RIGHT.
     * 
     * @return alignment cell
     */
    public int getAlignmentCell();

    /**
     * Retourne le nombre de caractères de la cellule une fois formatée.
     * 
     * @param model
     * @param column
     * @return int
     */
    public int getLengthCelluleFormatee(TableModel model, Column column);

}
