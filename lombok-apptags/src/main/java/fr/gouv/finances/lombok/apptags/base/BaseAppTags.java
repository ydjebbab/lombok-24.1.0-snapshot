/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : BaseAppTags.java
 *
 */
package fr.gouv.finances.lombok.apptags.base;

import java.beans.PropertyEditor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Map;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.Tag;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.NoSuchMessageException;
import org.springframework.web.servlet.tags.HtmlEscapingAwareTag;

import fr.gouv.finances.lombok.apptags.util.ClassUtil;
import fr.gouv.finances.lombok.apptags.util.RequestUtil;

/**
 * Class BaseAppTags --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.2 $ Date: 11 déc. 2009
 */
public abstract class BaseAppTags extends HtmlEscapingAwareTag
{
    private static final long serialVersionUID = 164565436L;

    /** Constant : TREE_PARAM_PAGE_ATTRIBUTE. */
    public static final String TREE_PARAM_PAGE_ATTRIBUTE =
        "fr.gouv.finances.lombok.apptags.tree.impl.BaseTag.treeParam";

    /** Constant : NODE_PAGE_ATTRIBUTE. */
    public static final String NODE_PAGE_ATTRIBUTE = "fr.gouv.finances.lombok.apptags.tree.impl.BaseTag.node";

    /** Constant : NODE_INDENTATION_TYPE_PAGE_ATTRIBUTE. */
    public static final String NODE_INDENTATION_TYPE_PAGE_ATTRIBUTE =
        "fr.gouv.finances.lombok.apptags.tree.impl.BaseTag.nodeIndentationType";

    /** Constant : SLASH. */
    public static final String SLASH = "/";

    /** Constant : AMPERSAND. */
    public static final String AMPERSAND = "&amp;";

    /** Constant : IMG_TAG. */
    public static final String IMG_TAG = "img";

    /** Constant : TABLE_TAG. */
    public static final String TABLE_TAG = "table";

    /** Constant : TR_TAG. */
    public static final String TR_TAG = "tr";

    /** Constant : TD_TAG. */
    public static final String TD_TAG = "td";

    /** Constant : A_TAG. */
    public static final String A_TAG = "a";

    /** Constant : SCRIPT_TAG. */
    public static final String SCRIPT_TAG = "script";

    /** Constant : INPUT_TAG. */
    public static final String INPUT_TAG = "input";

    /** Constant : DIV_TAG. */
    public static final String DIV_TAG = "div";

    /** Constant : SPAN_TAG. */
    public static final String SPAN_TAG = "span";

    /** Constant : NAME_ATTRIBUTE. */
    public static final String NAME_ATTRIBUTE = "name";

    /** Constant : VALUE_ATTRIBUTE. */
    public static final String VALUE_ATTRIBUTE = "value";

    /** Constant : ID_ATTRIBUTE. */
    public static final String ID_ATTRIBUTE = "id";

    /** Constant : TYPE_ATTRIBUTE. */
    public static final String TYPE_ATTRIBUTE = "type";

    /** Constant : ALT_ATTRIBUTE. */
    public static final String ALT_ATTRIBUTE = "alt";

    /** Constant : CLASS_ATTRIBUTE. */
    public static final String CLASS_ATTRIBUTE = "class";

    /** Constant : STYLE_ATTRIBUTE. */
    public static final String STYLE_ATTRIBUTE = "style";

    /** Constant : SRC_ATTRIBUTE. */
    public static final String SRC_ATTRIBUTE = "src";

    /** Constant : TITLE_ATTRIBUTE. */
    public static final String TITLE_ATTRIBUTE = "title";

    /** Constant : HREF_ATTRIBUTE. */
    public static final String HREF_ATTRIBUTE = "href";

    /** Constant : CELLSPACING_ATTRIBUTE. */
    public static final String CELLSPACING_ATTRIBUTE = "cellspacing";

    /** Constant : CELLPADDING_ATTRIBUTE. */
    public static final String CELLPADDING_ATTRIBUTE = "cellpadding";

    /** Constant : BORDER_ATTRIBUTE. */
    public static final String BORDER_ATTRIBUTE = "border";

    /** Helper for rendering values into HTML. */
    private final ValueFormatter valueFormatter = new ValueFormatter();

    /**
     * Constructeur de la classe BaseAppTags.java
     *
     */
    public BaseAppTags()
    {
        super();       
    }

    /**
     * Creates the {@link TagWriter} which all output will be written to. By default, the {@link TagWriter} writes its
     * output to the {@link javax.servlet.jsp.JspWriter} for the current {@link javax.servlet.jsp.PageContext}.
     * Subclasses may choose to change the {@link java.io.Writer} to which output is actually written.
     * 
     * @return the tag writer
     */
    protected TagWriter createTagWriter()
    {
        return new TagWriter(this.pageContext.getOut());
    }

    /**
     * Provides a simple template method that calls {@link #createTagWriter()} and passes the created {@link TagWriter}
     * to the {@link #writeTagContent(TagWriter)} method.
     * 
     * @return the value returned by {@link #writeTagContent(TagWriter)}
     * @throws Exception the exception
     */
    protected final int doStartTagInternal() throws Exception
    {
        return writeTagContent(createTagWriter());
    }

    /**
     * Evaluates the supplied value for the supplied attribute name. If the supplied value is <code>null</code> then
     * <code>null</code> is returned, otherwise evaluation is handled using
     * 
     * @param attributeName --
     * @param value --
     * @return the object
     * @throws JspException the jsp exception
     *         {@link ExpressionEvaluationUtils#evaluate(String, String, javax.servlet.jsp.PageContext)}.
     */
    // protected Object evaluate(String attributeName, Object value) throws JspException
    // {
    // if (value instanceof String)
    // {
    // return ExpressionEvaluationUtils.evaluate(attributeName, (String) value, this.pageContext);
    // }
    // else
    // {
    // return value;
    // }
    // }

    /**
     * Accesseur de l attribut ancestor.
     * 
     * @param type --
     * @return ancestor
     */
    protected Tag getAncestor(Class type)
    {
        Tag parent = getParent();
        while (parent != null && !type.equals(parent.getClass()))
        {
            parent = parent.getParent();
        }
        return parent;
    }

    /**
     * Retourne la propriété d'un objet stocké dans un scope donné. Si le bean identifié par son nom ou propriété est de
     * type java.util.Map et si le paramètre 'key' n'est pas null, le paramètre 'key' est utilisé pour rechercher le
     * bean dans la Map. Si le paramètre 'key' est non null et si la propriété n'est pas une Map, une exception est
     * levée
     * 
     * @param name --
     * @param property la propriété à lire
     * @param key La clé du bean stocké dans la Map localisée par les paramètres name : property
     * @param scope Le scope où le bean est stocké. Null indique que la recherche doit être effectuée dans tous les
     *        scopes. Les scopes valides sont 'request', 'session', 'page' et 'application'
     * @return La propriété du bean si elle est trouvée
     * @throws JspException Si une erreur se produit lors de la lecture de la propriété
     */
    protected Object getBean(String name, String property, Object key, String scope) throws JspException
    {
        Object result;
        Object bean = getBean(name, property, scope);

        if (key != null & bean != null)
        {
            if (!(bean instanceof Map))
            {
                StringBuilder   msg = new StringBuilder(44);
                msg.append("LE BEAN ");
                msg.append(name);
                msg.append('.');
                msg.append(property);
                msg.append(" N'EST PAS UNE INSTANCE DE java.util.Map. LE BEAN EST : ");
                String beanType =
                    (bean == null) ? "NULL" : (new StringBuilder()).append(bean.toString()).append(" (").append(
                        bean.getClass().getName()).append(")").toString();
                msg.append(beanType);

                throw new JspException(msg.toString());
            }
            result = ((Map) bean).get(key);
        }
        else
        {
            result = bean;
        }
        return result;
    }

    /**
     * Retourne la propriété d'un objet stocké dans un scope donné. Si le bean identifié par son nom ou propriété est de
     * type java.util.Map et si le paramètre 'key' n'est pas null, le paramètre 'key' est utilisé pour rechercher le
     * bean dans la Map. Si le paramètre 'key' est non null et si la propriété n'est pas une Map, une exception est
     * levée. Si le paramètre 'key' est null et le bean identifié par les paramètres name /property n'est pas de type
     * Map, le bean lui même est retourné. Si le paramètre 'property' est null, le bean identifié par le paramètre
     * 'name' sera utilisé comme Map
     * 
     * @param name --
     * @param property la propriété à lire
     * @param key La clé du bean stocké dans la Map localisée par les paramètres name : property
     * @param keyedObjectProperty --
     * @param scope Le scope où le bean est stocké. Null indique que la recherche doit être effectuée dans tous les
     *        scopes. Les scopes valides sont 'request', 'session', 'page' et 'application'
     * @return La propriété du bean si elle est trouvée
     * @throws JspException Si une erreur se produit lors de la lecture de la propriété
     */
    protected Object getBean(String name, String property, Object key, String keyedObjectProperty, String scope)
        throws JspException
    {
        Object result = null;
        Object bean = getBean(name, property, key, scope);

        if (bean == null)
        {
        }
        else if (keyedObjectProperty == null)
        {
            result = bean;
        }
        else
        {

            try
            {
                result = getProperty(keyedObjectProperty, bean);
            }
            catch (NoSuchMethodException nsme)
            {
                StringBuilder   msg = new StringBuilder();
                msg.append("AUCUNE PROPRIETE ");
                msg.append(keyedObjectProperty);
                msg.append("SUR L'OBJET STOCKE AVEC LA CLE ");
                msg.append(key);
                msg.append("L'OBJET EST ");
                msg.append(bean.toString());
                throw new JspException(msg.toString(), nsme);
            }
            catch (IllegalAccessException ilae)
            {
                StringBuilder   msg = new StringBuilder();
                msg.append("AUCUNE PROPRIETE ");
                msg.append(keyedObjectProperty);
                msg.append("SUR L'OBJET STOCKE AVEC LA CLE ");
                msg.append(key);
                msg.append("L'OBJET EST ");
                msg.append(bean.toString());
                throw new JspException(msg.toString(), ilae);
            }
            catch (InvocationTargetException ite)
            {
                StringBuilder   msg = new StringBuilder();
                msg.append("AUCUNE PROPRIETE ");
                msg.append(keyedObjectProperty);
                msg.append("SUR L'OBJET STOCKE AVEC LA CLE ");
                msg.append(key);
                msg.append("L'OBJET EST ");
                msg.append(bean.toString());
                throw new JspException(msg.toString(), ite);
            }
        }
        return result;
    }

    /**
     * Retourne la propriété d'un objet stocké dans un scope donné, si elle est présente.
     * 
     * @param name --
     * @param property la propriété à lire
     * @param scope Le scope où le bean est stocké. Null indique que la recherche doit être effectuée dans tous les
     *        scopes. Les scopes valides sont 'request', 'session', 'page' et 'application'
     * @return La propriété du bean si elle est trouvée
     * @throws JspException Si une erreur se produit lors de la lecture de la propriété
     */
    protected Object getBean(String name, String property, String scope) throws JspException
    {
        try
        {
            return RequestUtil.findObject(pageContext, name, property, scope);
        }
        catch (IllegalAccessException iae)
        {
            StringBuilder   msg = new StringBuilder(44);
            msg.append("ERREUR LORS DE LA LECTURE DE LA PROPRIETE : ");
            msg.append(property);
            throw new JspException(msg.toString(), iae);
        }
        catch (InvocationTargetException iae)
        {
            StringBuilder   msg = new StringBuilder(44);
            msg.append("ERREUR LORS DE LA LECTURE DE LA PROPRIETE : ");
            msg.append(property);
            throw new JspException(msg.toString(), iae);
        }
    }

    /**
     * Retourne la propriété de l'objet si elle est présente.
     * 
     * @param object l'objet contenant la propriété
     * @param property la propriété à lire
     * @return La propriété du bean si elle est trouvée
     * @throws JspException Si une erreur se produit lors de la lecture de la propriété
     */
    protected Object getBeanProperty(Object object, String property) throws JspException
    {
        try
        {
            return ClassUtil.getBean(object, property);
        }
        catch (IllegalAccessException ilae)
        {
            StringBuilder   msg = new StringBuilder(44);
            msg.append("ERREUR LORS DE LA LECTURE DE LA PROPRIETE : ");
            msg.append(property);
            throw new JspException(msg.toString(), ilae);
        }
        catch (InvocationTargetException ilae)
        {
            StringBuilder   msg = new StringBuilder(44);
            msg.append("ERREUR LORS DE LA LECTURE DE LA PROPRIETE : ");
            msg.append(property);
            throw new JspException(msg.toString(), ilae);
        }
    }

    /**
     * Gets the display value of the supplied <code>Object</code>, HTML escaped as required. This version is
     * <strong>not</strong> {@link PropertyEditor}-aware.
     * 
     * @param value --
     * @return the display string
     */
    protected String getDisplayString(Object value)
    {
        return this.valueFormatter.getDisplayString(value, isHtmlEscape());
    }

    /**
     * Gets the display value of the supplied <code>Object</code>, HTML escaped as required. If the supplied value is
     * not a {@link String} and the supplied {@link PropertyEditor} is not null then the {@link PropertyEditor} is used
     * to obtain the display value.
     * 
     * @param value --
     * @param propertyEditor --
     * @return the display string
     */
    protected String getDisplayString(Object value, PropertyEditor propertyEditor)
    {
        return this.valueFormatter.getDisplayString(value, propertyEditor, isHtmlEscape());
    }

    /**
     * Accesseur de l attribut property.
     * 
     * @param keyedObjectProperty --
     * @param bean --
     * @return property
     * @throws NoSuchMethodException the no such method exception
     * @throws JspException the jsp exception
     * @throws IllegalAccessException the illegal access exception
     * @throws InvocationTargetException the invocation target exception
     */
    protected Object getProperty(String keyedObjectProperty, Object bean) throws NoSuchMethodException, JspException,
        IllegalAccessException, InvocationTargetException
    {
        String keyedObjectPropertyGetter = null;
        if (keyedObjectProperty.startsWith("get"))
        {
            keyedObjectPropertyGetter = keyedObjectProperty;
        }
        else
        {
            keyedObjectPropertyGetter =
                "get" + keyedObjectProperty.substring(0, 1).toUpperCase() + keyedObjectProperty.substring(1);
        }
        Method method = bean.getClass().getMethod(keyedObjectPropertyGetter, (Class[]) null);
        return method.invoke(bean, (Object[]) null);
    }

    /**
     * methode Initvar : --.
     * 
     * @param attributeValue --
     * @param bundlekey --
     * @param defaultValue --
     * @return string
     * @throws JspException the jsp exception
     */
    protected String initvar(String attributeValue, String bundlekey, String defaultValue) throws JspException
    {
        String result;
        if (!isEmpty(attributeValue))
        {
            result = attributeValue;
        }
        else
        {
            result = lectureValeurDansFichierPropriete(bundlekey);
            if (isEmpty(result))
            {
                result = defaultValue;
            }
        }
        return result;
    }

    /**
     * Verifie si empty.
     * 
     * @param text --
     * @return true, si c'est empty
     */
    protected boolean isEmpty(String text)
    {
        return (text == null || StringUtils.isBlank(text) ? true : false);
    }

    /**
     * Teste si la châine en paramètre est égale à TRUE, sans tenir compte de la casse et en ignorant les blancs en
     * début et fin de chaîne.
     * 
     * @param text --
     * @return true, if checks if is true
     */
    protected boolean isTRUE(String text)
    {
        boolean result;
        if (this.isEmpty(text))
        {
            result = false;
        }
        else
        {
            String textEpure = text.trim();
            result = "TRUE".equalsIgnoreCase(textEpure);
        }
        return result;
    }

    /**
     * methode Lecture valeur dans fichier propriete : --.
     * 
     * @param key --
     * @return string
     * @throws JspException the jsp exception
     */
    protected String lectureValeurDansFichierPropriete(String key) throws JspException
    {
        String valeur;
        try
        {
            valeur = this.getRequestContext().getMessage(key);
        }
        catch (NoSuchMessageException noMessageE)
        {
            valeur = "";
        }
        return valeur;
    }

    /**
     * Stocke le 'bean' sous le nom passé en paramètre ('name') dans un scope donné. Si le paramètre scope est null, le
     * paramètre defautScope est utilisé Stores the given bean by the given name in the given scope, meaning the bean is
     * stored using the name as key in either request, session, page, or application context attributes.
     * 
     * @param name Nom sous lequel stocké le bean
     * @param scope Scope dans lequel le bean est enregistré. Si null, le paramètre defaultScope est utilisé. Les scopes
     *        valides sont 'request', 'session', 'page' et 'application'
     * @param defaultScope Scope a utiliser par défaut, si null utilisation du scope 'request'
     * @param bean Bean à stocké, si null, aucune opération n'est effectuée
     * @throws JspException Si le paramètre 'name' est null
     */
    protected void setBean(String name, String scope, String defaultScope, Object bean) throws JspException
    {
        if (bean == null)
        {
            return;
        }

        if (name == null)
        {
            throw new JspException("IMPOSSIBLE DE STOCKER LE BEAN, LE PARAMETRE 'NAME' EST NULL");
        }

        String theScope = scope;

        if (theScope == null)
        {
            theScope = defaultScope;
        }
        if (theScope == null)
        {
            theScope = "request";
        }

        if ("request".equals(theScope))
        {
            pageContext.getRequest().setAttribute(name, bean);
        }
        else if ("session".equals(theScope))
        {
            pageContext.getSession().setAttribute(name, bean);
        }
        else if ("page".equals(theScope))
        {
            pageContext.setAttribute(name, bean);
        }
        else if ("application".equals(theScope))
        {
            pageContext.getServletContext().setAttribute(name, bean);
        }
        else
        {
            StringBuilder   msg = new StringBuilder(70);
            msg.append("IMPOSSIBLE DE STOCKER LE BEAN. LE SCOPE : ");
            msg.append(scope);
            msg.append("EST INVALIDE.\n");
            msg.append("LES SCOPES VALIDES SONT : 'request', 'session','page' et 'application'");
            throw new JspException(msg.toString());
        }
    }

    /**
     * Optionally writes the supplied value under the supplied attribute name into the supplied {@link TagWriter}. In
     * this case, the supplied value is {@link #evaluate evaluated} first and then the
     * 
     * @param tagWriter --
     * @param attributeName --
     * @param value --
     * @throws JspException the jsp exception {@link ObjectUtils#getDisplayString String representation} is written as
     *         the attribute value. If the resultant <code>String</code> representation is <code>null</code> or empty,
     *         no attribute is written.
     * @see TagWriter#writeOptionalAttributeValue(String, String)
     */
    // protected final void writeOptionalAttribute(TagWriter tagWriter, String attributeName, String value)
    // throws JspException
    // {
    //
    // if (value != null)
    // {
    // tagWriter.writeOptionalAttributeValue(attributeName, getDisplayString(evaluate(attributeName, value)));
    // }
    // }

    /**
     * Subclasses should implement this method to perform tag content rendering.
     * 
     * @param tagWriter --
     * @return valid tag render instruction as per {@link javax.servlet.jsp.tagext.Tag#doStartTag()}.
     * @throws JspException the jsp exception
     */
    protected abstract int writeTagContent(TagWriter tagWriter) throws JspException;

}
