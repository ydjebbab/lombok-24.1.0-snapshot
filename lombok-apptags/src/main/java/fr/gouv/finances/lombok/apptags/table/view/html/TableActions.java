/*
 * Copyright (c) 2014 DGFiP - Tous droits réservés
 *//*
/*Copyright (c) 2014 DGFiP - Tous droits réservés/* * Copyright 2004 original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package fr.gouv.finances.lombok.apptags.table.view.html;

import org.apache.commons.lang.StringUtils;

import fr.gouv.finances.lombok.apptags.table.bean.Column;
import fr.gouv.finances.lombok.apptags.table.core.TableConstants;
import fr.gouv.finances.lombok.apptags.table.core.TableModel;
import fr.gouv.finances.lombok.apptags.table.limit.Sort;

/**
 * Class TableActions
 */
public class TableActions
{

    /** model. */
    private TableModel model;

    /**
     * Instanciation de table actions.
     * 
     * @param model
     */
    public TableActions(TableModel model)
    {
        this.model = model;
    }

    /**
     * Accesseur de l attribut table model.
     * 
     * @return table model
     */
    protected TableModel getTableModel()
    {
        return model;
    }

    /*
     * Added 16 AUG 2007 - Todd Fredrich
     */
    /**
     * methode Checks for on invoke action :
     * 
     * @return true, si c'est vrai
     */
    public boolean hasOnInvokeAction()
    {
        return StringUtils.isNotBlank(getOnInvokeAction());
    }

    /*
     * Modified 16 AUG 2007 - Todd Fredrich - To simply return user-set onInvokeAction.
     */
    /**
     * Accesseur de l attribut on invoke action.
     * 
     * @return on invoke action
     */
    public String getOnInvokeAction()
    {
        return model.getTableHandler().getTable().getOnInvokeAction();
    }

    /*
     * Added 16 AUG 2007 - Todd Fredrich - To duplicate previous functionality of getOnInvokeAction().
     */
    /**
     * Accesseur de l attribut on invoke or submit action.
     * 
     * @return on invoke or submit action
     */
    public String getOnInvokeOrSubmitAction()
    {
        String onInvokeAction = getOnInvokeAction();
        if (StringUtils.isNotBlank(onInvokeAction))
        {
            return onInvokeAction;
        }

        return getSubmitAction();
    }

    /**
     * Accesseur de l attribut submit action.
     * 
     * @return submit action
     */
    public String getSubmitAction()
    {
        StringBuilder   result = new StringBuilder();

        String form = BuilderUtils.getForm(model);
        String action = model.getTableHandler().getTable().getAction();
        result.append("document.forms.").append(form).append(".setAttribute('action','").append(action).append("');");

        String method = model.getTableHandler().getTable().getMethod();
        result.append("document.forms.").append(form).append(".setAttribute('method','").append(method).append("');");

        result.append("document.forms.").append(form).append(".submit()");

        return result.toString();
    }

    /**
     * Accesseur de l attribut form parameter.
     * 
     * @param name
     * @param value
     * @return form parameter
     */
    public String getFormParameter(String name, String value)
    {
        StringBuilder   result = new StringBuilder();
        
        String form = BuilderUtils.getForm(model);
        result.append("document.forms.").append(form).append(".");
        result.append(model.getTableHandler().prefixWithTableId()).append(name);
        result.append(".value='").append(value).append("';");
        
        return result.toString();
    }
    
    
    /**
     * Accesseur de l attribut form parameter.
     * 
     * @param name
     * @param value
     * @return form parameter
     */
    public String getFormParameterFromAbstractValue(String name, String value)
    {
        
        StringBuilder   result = new StringBuilder();
        String form = BuilderUtils.getForm(model);
        result.append("document.forms.").append(form).append(".");
        result.append(model.getTableHandler().prefixWithTableId()).append(name);
        result.append(".value=").append(value).append(";");
        return result.toString();
    }
  

    /**
     * Accesseur de l attribut export table id parameter.
     * 
     * @param value
     * @return export table id parameter
     */
    public String getExportTableIdParameter(String value)
    {
        StringBuilder   result = new StringBuilder();

        String form = BuilderUtils.getForm(model);

        result.append("if(document.forms.").append(form).append(".");
        result.append(TableConstants.EXPORT_TABLE_ID);
        result.append(")document.forms.").append(form).append(".");
        result.append(TableConstants.EXPORT_TABLE_ID);
        result.append(".value='").append(value).append("';");

        return result.toString();
    }

    /**
     * Accesseur de l attribut export action.
     * 
     * @param exportView
     * @param exportFileName
     * @return export action
     */
    public String getExportAction(String exportView, String exportFileName)
    {
        StringBuilder   action = new StringBuilder("javascript:");

        action.append(getExportTableIdParameter(model.getTableHandler().getTable().getTableId()));
        action.append(getFormParameter(TableConstants.EXPORT_VIEW, exportView));
        action.append(getFormParameter(TableConstants.EXPORT_FILE_NAME, exportFileName));

        action.append(getSubmitAction());

        return action.toString();
    }

    /**
     * Accesseur de l attribut page action.
     * 
     * @param page
     * @return page action
     */
    public String getPageAction(int page)
    {
        StringBuilder   action = new StringBuilder("javascript:");

        action.append(getClearedExportTableIdParameters());

        action.append(getFormParameter(TableConstants.PAGE, "" + page));
        action.append(getOnInvokeOrSubmitAction());

        return action.toString();
    }
    
    public String getPageActionFromSelect()
    {
        StringBuilder   action = new StringBuilder("javascript:");
        action.append(getClearedExportTableIdParameters());
        action.append(getFormParameterFromAbstractValue(TableConstants.PAGE, "this.value"));
        action.append(getOnInvokeOrSubmitAction());
             
        return action.toString();
    }

    /**
     * Accesseur de l attribut filter action.
     * 
     * @return filter action
     */
    public String getFilterAction()
    {
        StringBuilder   action = new StringBuilder("javascript:");

        action.append(getClearedExportTableIdParameters());

        action.append(getFormParameter(TableConstants.FILTER + TableConstants.ACTION, TableConstants.FILTER_ACTION));
        action.append(getFormParameter(TableConstants.PAGE, "1"));
        action.append(getOnInvokeOrSubmitAction());

        return action.toString();
    }

    /**
     * Accesseur de l attribut clear action.
     * 
     * @return clear action
     */
    public String getClearAction()
    {
        StringBuilder   action = new StringBuilder("javascript:");

        action.append(getClearedExportTableIdParameters());

        action.append(getFormParameter(TableConstants.FILTER + TableConstants.ACTION, TableConstants.CLEAR_ACTION));
        action.append(getFormParameter(TableConstants.PAGE, "1"));
        action.append(getOnInvokeOrSubmitAction());

        return action.toString();
    }

    /**
     * Accesseur de l attribut sort action.
     * 
     * @param column
     * @param sortOrder
     * @return sort action
     */
    public String getSortAction(Column column, String sortOrder)
    {
        StringBuilder   action = new StringBuilder("javascript:");

        Sort sort = model.getLimit().getSort();
        if (sort.isSorted())
        {
            // set the old sort back
            action.append(getFormParameter(TableConstants.SORT + sort.getAlias(), ""));
        }

        action.append(getClearedExportTableIdParameters());

        // set sort on current column
        action.append(getFormParameter(TableConstants.SORT + column.getAlias(), sortOrder));
        action.append(getFormParameter(TableConstants.PAGE, "1"));
        action.append(getOnInvokeOrSubmitAction());

        return action.toString();
    }

    /**
     * Accesseur de l attribut rows displayed action.
     * 
     * @return rows displayed action
     */
    public String getRowsDisplayedAction()
    {
        StringBuilder   action = new StringBuilder("javascript:");

        action.append(getClearedExportTableIdParameters());

        action.append(getRowsDisplayedFormParameter(TableConstants.CURRENT_ROWS_DISPLAYED));
        action.append(getFormParameter(TableConstants.PAGE, "1"));
        action.append(getOnInvokeOrSubmitAction());

        return action.toString();
    }

    /**
     * Need to clear the export table id parameter.
     * 
     * @return The javascript to clear the table id
     */
    public String getClearedExportTableIdParameters()
    {
        return getExportTableIdParameter("");
    }

    /**
     * Accesseur de l attribut rows displayed form parameter.
     * 
     * @param name
     * @return rows displayed form parameter
     */
    protected String getRowsDisplayedFormParameter(String name)
    {
        StringBuilder   result = new StringBuilder();

        String form = BuilderUtils.getForm(model);
        String selectedOption = "this.options[this.selectedIndex].value";
        result.append("document.forms.").append(form).append(".");
        result.append(model.getTableHandler().prefixWithTableId()).append(name);
        result.append(".value=").append(selectedOption).append(";");

        return result.toString();
    }
}
