/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.apptags.table.view;

import fr.gouv.finances.lombok.apptags.table.bean.Export;
import fr.gouv.finances.lombok.apptags.table.core.Messages;
import fr.gouv.finances.lombok.apptags.table.core.TableModel;
import fr.gouv.finances.lombok.apptags.table.view.html.BuilderConstants;
import fr.gouv.finances.lombok.apptags.table.view.html.BuilderUtils;
import fr.gouv.finances.lombok.apptags.table.view.html.CpToolbarBuilder;
import fr.gouv.finances.lombok.apptags.table.view.html.StatusBarBuilder;
import fr.gouv.finances.lombok.apptags.table.view.html.TableActions;
import fr.gouv.finances.lombok.apptags.table.view.html.TwoColumnRowLayout;
import fr.gouv.finances.lombok.apptags.util.HtmlBuilder;

/**
 * Class CpCompactToolbar
 */
public class CpCompactToolbar extends TwoColumnRowLayout
{

    /** messages. */
    private Messages messages;

    /**
     * Instanciation de cp compact toolbar.
     * 
     * @param html
     * @param model
     */
    public CpCompactToolbar(HtmlBuilder html, TableModel model)
    {
        super(html, model);
        messages = model.getMessages();
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.view.html.TwoColumnRowLayout#showLayout
     *      (fr.gouv.finances.lombok.apptags.table.core.TableModel)
     */
    @Override
    protected boolean showLayout(TableModel model)
    {
        boolean result = true;
        boolean showStatusBar = BuilderUtils.showStatusBar(model);
        boolean filterable = BuilderUtils.filterable(model);
        boolean showExports = BuilderUtils.showExports(model);
        boolean showPagination = BuilderUtils.showPagination(model);
        boolean showTitle = BuilderUtils.showTitle(model);

        if (!showStatusBar && !filterable && !showExports && !showPagination && !showTitle)
        {
            result = false;
        }

        return result;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.view.html.TwoColumnRowLayout#columnLeft
     *      (fr.gouv.finances.lombok.apptags.util.HtmlBuilder, fr.gouv.finances.lombok.apptags.table.core.TableModel)
     */
    @Override
    protected void columnLeft(HtmlBuilder html, TableModel model)
    {
        boolean showStatusBar = BuilderUtils.showStatusBar(model);

        if (showStatusBar)
        {
            html.td(5).styleClass(BuilderConstants.STATUS_BAR_CSS).close();

            new StatusBarBuilder(html, model).statusMessage();

            html.tdEnd();
        }
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.view.html.TwoColumnRowLayout#columnLeft
     *      (fr.gouv.finances.lombok.apptags.util.HtmlBuilder, fr.gouv.finances.lombok.apptags.table.core.TableModel)
     */
    @Override
    protected void columnLeftRWD(HtmlBuilder html, TableModel model)
    {
        boolean showStatusBar = BuilderUtils.showStatusBar(model);

        if (showStatusBar)
        {
            html.div().styleClass(BuilderConstants.STATUS_BAR_CSS).close();

            new StatusBarBuilder(html, model).statusMessage();

            html.divEnd();
        }
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.view.html.TwoColumnRowLayout#columnRight
     *      (fr.gouv.finances.lombok.apptags.util.HtmlBuilder, fr.gouv.finances.lombok.apptags.table.core.TableModel)
     */
    @Override
    protected void columnRight(HtmlBuilder html, TableModel model)
    {
        boolean filterable = BuilderUtils.filterable(model);
        boolean showPagination = BuilderUtils.showPagination(model);
        boolean showNewPagination = BuilderUtils.showNewPagination(model);
        boolean showNewFilter = BuilderUtils.showNewFilter(model);

        boolean showExports = BuilderUtils.showExports(model);
        boolean showResponsive = BuilderUtils.showResponsive(model);

        CpToolbarBuilder toolbarBuilder = new CpToolbarBuilder(html, model);

        html.td(4).styleClass(BuilderConstants.COMPACT_TOOLBAR_CSS + " alignRight").close();

        html.table(4).styleClass("noBorder cellPadding1 cellSpacing2").close();
        html.tr(5).close();

        if (showPagination)
        {
            if (showNewPagination)
            {
                displayNewPagination(html, model);
            }
            else
            {
                html.td(5).close();
                toolbarBuilder.firstPageItemAsImage();
                html.tdEnd();

                html.td(5).close();
                toolbarBuilder.prevPageItemAsImage();
                html.tdEnd();

                html.td(5).close();
                toolbarBuilder.nextPageItemAsImage();
                html.tdEnd();

                html.td(5).close();
                toolbarBuilder.lastPageItemAsImage();
                html.tdEnd();

            }

            html.td(5).close();
            toolbarBuilder.separator();
            html.tdEnd();
        }

        html.td(5).close();
        toolbarBuilder.rowsDisplayedDroplist();
        html.tdEnd();

        if (showExports)
        {
            html.td(5).close();
            toolbarBuilder.separator();
            html.tdEnd();

            int showExportsWithLimitSizePdf = BuilderUtils.showExportsWithLimitSizePdf(model);
            int showExportsWithLimitSizeXlsOrOds = BuilderUtils.showExportsWithLimitSizeXlsOrOds(model);
            int showExportsWithLimitSizeCsv = BuilderUtils.showExportsWithLimitSizeCsv(model);
            for (Export export : model.getExportHandler().getExports())
            {
                html.td(5).close();
                if (((export.getImageName().toUpperCase().endsWith("PDF") == true)
                    && model.getCollectionOfFilteredBeans().size() < showExportsWithLimitSizePdf)
                    || ((export.getImageName().toUpperCase().endsWith("XLS") == true
                        || export.getImageName().toUpperCase().endsWith("ODS") == true) && model
                            .getCollectionOfFilteredBeans().size() < showExportsWithLimitSizeXlsOrOds)
                    || (export.getImageName().toUpperCase().endsWith("CSV") == true
                        && model.getCollectionOfFilteredBeans().size() < showExportsWithLimitSizeCsv))
                {
                    toolbarBuilder.exportItemAsImage(export);
                }
                else
                {
                    html.append("veuillez filtrer votre liste pour accéder aux exports " + export.getImageName());
                }
                html.tdEnd();
            }
        }

        if (filterable)
        {
            if (showExports || showPagination)
            {
                html.td(5).close();
                toolbarBuilder.separator();
                html.tdEnd();
            }

            html.td(5).close();

            if (showNewFilter)
            {
                toolbarBuilder.filterItemAsButton();
                html.nbsp();
                toolbarBuilder.clearItemAsButton();
            }
            else
            {
                toolbarBuilder.filterItemAsImage();
                html.tdEnd();
                html.td(5).close();
                toolbarBuilder.clearItemAsImage();
            }
            html.tdEnd();
        }

        html.trEnd(5);

        html.tableEnd(4);

        html.tdEnd();
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.view.html.TwoColumnRowLayout#columnRight
     *      (fr.gouv.finances.lombok.apptags.util.HtmlBuilder, fr.gouv.finances.lombok.apptags.table.core.TableModel)
     */
    @Override
    protected void columnRightRWD(HtmlBuilder html, TableModel model)
    {
        boolean filterable = BuilderUtils.filterable(model);
        boolean showPagination = BuilderUtils.showPagination(model);
        boolean showExports = BuilderUtils.showExports(model);
        String paginationClass = BuilderUtils.getPaginationClass(model);
        String exportsClass = BuilderUtils.getExportsClass(model);

        CpToolbarBuilder toolbarBuilder = new CpToolbarBuilder(html, model);

        html.div().styleClass("toolBarRWD container-fluid").close();

        if (showPagination)
        {
            html.ul().styleClass(paginationClass).close();
            html.li("").close();
            toolbarBuilder.firstPageItemAsImage();
            html.liEnd();

            html.li("").close();
            toolbarBuilder.prevPageItemAsImage();
            html.liEnd();

            html.li("").close();
            toolbarBuilder.nextPageItemAsImage();
            html.liEnd();

            html.li("").close();
            toolbarBuilder.lastPageItemAsImage();
            html.liEnd();

            html.li("").close();
            toolbarBuilder.separator();
            html.liEnd();

            html.li("").close();
            toolbarBuilder.rowsDisplayedDroplist();
            html.liEnd();

            html.ulEnd();
        }

        if (showExports)
        {

            int showExportsWithLimitSizePdf = BuilderUtils.showExportsWithLimitSizePdf(model);
            int showExportsWithLimitSizeXlsOrOds = BuilderUtils.showExportsWithLimitSizeXlsOrOds(model);
            int showExportsWithLimitSizeCsv = BuilderUtils.showExportsWithLimitSizeCsv(model);

            html.ul().styleClass("exports " + exportsClass).close();

            html.li("").close();
            html.a().append(".collapseExports").dataAttributes("data-toggle=collapse").styleClass("btn").close().append("exports").aEnd();
            html.liEnd();

            for (Export export : model.getExportHandler().getExports())
            {
                html.li("").styleClass("collapseExports collapse").close();
                if (((export.getImageName().toUpperCase().endsWith("PDF") == true)
                    && model.getCollectionOfFilteredBeans().size() < showExportsWithLimitSizePdf)
                    || ((export.getImageName().toUpperCase().endsWith("XLS") == true
                        || export.getImageName().toUpperCase().endsWith("ODS") == true) && model
                            .getCollectionOfFilteredBeans().size() < showExportsWithLimitSizeXlsOrOds)
                    || (export.getImageName().toUpperCase().endsWith("CSV") == true
                        && model.getCollectionOfFilteredBeans().size() < showExportsWithLimitSizeCsv))
                {
                    toolbarBuilder.exportItemAsImage(export);
                }
                else
                {
                    html.append("veuillez filtrer votre liste pour accéder aux exports " + export.getImageName());
                }
                html.liEnd();
            }
            html.ulEnd();
        }

        if (filterable)
        {
            toolbarBuilder.filterItemAsImage();
            toolbarBuilder.clearItemAsImage();
        }
        html.divEnd();
    }

    /**
     * methode Display new pagination :
     * 
     * @param html
     * @param model
     */
    private void displayNewPagination(HtmlBuilder html, TableModel model)
    {

        /** liste des pages dispo. **/
        int currentpage = model.getLimit().getPage();
        int totalPages = BuilderUtils.getTotalPages(model);
        int nbpageprint = BuilderUtils.newPaginationNbPage(model);
        int minpage = 1;
        int maxpage = 1;
        boolean spaceBefore = false;
        boolean spaceAfter = false;

        html.td(5).close();

        if (totalPages == 1)
        {
            html.td(5).close();
            html.append("<a href=\"" + new TableActions(model).getPageAction(1) + "\" class='btn btn-mini page'> 1 </a>");
            html.tdEnd();

            return;
        }
        else
        {
            html.td(5).close();
            if (currentpage != 1)
            {
                html.append("<a href=\"" + new TableActions(model).getPageAction(currentpage - 1) + "\" class='btn btn-mini page'" +
                    "title='" + messages.getMessage(BuilderConstants.TOOLBAR_PREV_PAGE_TOOLTIP)
                    + "' > <i class='icon-chevron-left'></i></a>");
                html.td(4).close();
                html.append("<a href=\"" + new TableActions(model).getPageAction(1) + "\" class='btn btn-mini page'> 1 </a>");

            }
            else
            {
                html.append("<span class='btn btn-mini page disabled'> <i class='icon-chevron-left'></i></span>");
                html.tdEnd();

                html.td(5).close();
                html.append("<span class='currentpage'>1</span>");
            }
            html.tdEnd();
        }

        /* si on a moins de "nbpageprint" à afficher */
        if (totalPages <= nbpageprint + 1)
        {
            minpage = 2;
            maxpage = totalPages;
        }
        /* ex. (current = 2) : 1 2 3 4 5 ... 10 */
        else if (currentpage <= nbpageprint)
        {
            minpage = 2;
            maxpage = nbpageprint;
            spaceAfter = true;
        }

        /* ex. (current = 6) : 1 ... 3 4 5 6 7 ... 10 */
        else if (currentpage > nbpageprint && currentpage < (totalPages - nbpageprint + 1))
        {
            // ancien code
            // Math.round n'est pas utile parce que nbpageprint est un int, une division sur un int donnera un résultat
            // en int
            // minpage = currentpage - Math.round(nbpageprint / 2);
            // maxpage = currentpage + Math.round(nbpageprint / 2);
            minpage = currentpage - (nbpageprint / 2);
            maxpage = currentpage + (nbpageprint / 2);

            spaceAfter = true;
            spaceBefore = true;
        }
        /* ex. (current = 8) : 1 ... 6 7 8 9 10 */
        else
        {
            minpage = totalPages - nbpageprint + 1;
            maxpage = totalPages;
            spaceBefore = true;
        }

        if (spaceBefore)
        {
            // affichage d'une liste déroulante avec la liste complète des pages

            html.td(5).close();
            html.append("<div class='otherpage'> <span class='btn btn-mini page'>... <i class='icon-chevron-down'></i></span>");
            html.append("<div>");

            for (int i = 2; i < minpage; i++)
            {
                html.append("<a href=\"" + new TableActions(model).getPageAction(i) + "\" class='btn btn-mini page'> " + i + "</a>");
            }

            html.append("</div>");
            html.append("</div>");
            html.tdEnd();
        }

        for (int i = minpage; i <= maxpage; i++)
        {
            html.td(5).close();

            if (i == currentpage)
            {
                html.append("<span class='currentpage'>" + i + "</span>");
            }
            else
            {
                html.append("<a href=\"" + new TableActions(model).getPageAction(i) + "\" class='btn btn-mini page'> " + i + "</a>");
            }

            html.tdEnd();
        }

        if (spaceAfter)
        {
            // affichage d'une liste déroulante avec la liste complète des pages

            html.td(5).close();
            html.append("<div class='otherpage'> <span class='btn btn-mini page'>... <i class='icon-chevron-down'></i> </span>");
            html.append("<div>");

            for (int i = maxpage + 1; i < totalPages; i++)
            {
                html.append("<a href=\"" + new TableActions(model).getPageAction(i) + "\" class='btn btn-mini page'> " + i + "</a>");
            }

            html.append("</div>");
            html.append("</div>");
            html.tdEnd();

            html.td(5).close();
            html.append("<a href=\"" + new TableActions(model).getPageAction(totalPages) + "\" class='btn btn-mini page'>" + totalPages
                + "</a>");
            html.tdEnd();
        }

        html.td(5).close();
        if (currentpage < totalPages)
        {
            html.append("<a href=\"" + new TableActions(model).getPageAction(currentpage + 1) + "\" class='btn btn-mini page'" +
                "title='" + messages.getMessage(BuilderConstants.TOOLBAR_NEXT_PAGE_TOOLTIP)
                + "' > <i class='icon-chevron-right'></i></a>");
        }
        else
        {
            html.append("<span class='btn btn-mini page disabled'> <i class='icon-chevron-right'></i></span>");
        }
        html.tdEnd();

    }

    /**
     * methode Display new pagination RWD:
     * 
     * @param html
     * @param model
     */
    private void displayNewPaginationRWD(HtmlBuilder html, TableModel model)
    {

        /** liste des pages dispo. **/
        int currentpage = model.getLimit().getPage();
        int totalPages = BuilderUtils.getTotalPages(model);
        String paginationClass = BuilderUtils.getPaginationClass(model);

        if (totalPages == 1)
        {
            html.append("<a href=\"" + new TableActions(model).getPageAction(1) + "\" class='btn page'> 1 </a>");
            return;
        }
        else
        {

            html.append("<span><i class='icon-chevron-left'></i></span>");
            html.ul().styleClass("pageScroll " + paginationClass).close();

            for (int i = 1; i <= totalPages; i++)
            {
                html.li("").close();
                if (i == currentpage)
                {
                    html.append("<span class='currentpage'>" + i + "</span>");
                }
                else
                {
                    html.append("<a href=\"" + new TableActions(model).getPageAction(i) + "\" class='btn page'> " + i + "</a>");
                }
                html.liEnd();
            }
            html.ulEnd();

            html.append("<span><i class='icon-chevron-right'></i></span>");
        }

    }

}
