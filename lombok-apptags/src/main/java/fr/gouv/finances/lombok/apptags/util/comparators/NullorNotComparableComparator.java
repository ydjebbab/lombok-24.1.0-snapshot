/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.apptags.util.comparators;

import java.io.Serializable;
import java.util.Comparator;

import org.apache.commons.collections.comparators.ComparableComparator;

/**
 * Adaptation de la classe NullComparator pour qu'elle accepte des propriétés qui n'implémentent pas l'interface Comparable.
 * 
 * @author wpetit-cp
 */
public class NullorNotComparableComparator implements Comparator<Object>, Serializable
{

    /** Serialization version. */
    private static final long serialVersionUID = -5820772575483504339L;

    /** Comparator utilisé pour comparer deux objets non nulls. */
    private Comparator<Object> nonNullComparator;

    /** Indique si un objet null est considéré comme supérieur aux objets non nulls. */
    private boolean nullsAreHigh;

    // -----------------------------------------------------------------------
    /**
     * Construit une instance qui, lors du tri, considère que les objets nulls sont supérieurs aux objets non nulls.
     * Lors de la comparaison de deux objets non nulls, le comparateur {@link ComparableComparator} est utilisé
     */
    @SuppressWarnings("unchecked")
    public NullorNotComparableComparator()
    {
        this(ComparableComparator.getInstance(), true);
    }

    /**
     * Construit une instance qui, lors du tri, considère que les objets nulls sont supérieurs aux objets non nulls.
     * Lors de la comparaison de deux objets non nulls, le comparateur {@link ComparableComparator} est utilisé
     * 
     * @param nonNullComparator Comparator a utilisé pour comparer deux objets non null
     */
    public NullorNotComparableComparator(Comparator<Object> nonNullComparator)
    {
        this(nonNullComparator, true);
    }

    /**
     * Construit une instance qui, lors du tri, considère que les objets nulls sont supérieurs ou inférieurs aux objets
     * non nulls. Lors de la comparaison de deux objets non nulls, le comparateur {@link ComparableComparator} est
     * utilisé
     * 
     * @param nullsAreHigh <code>true</code> indique que les objets <code>null</code> sont considérés comme supérieurs
     *        aux objects non-<code>null</code> <code>false</code>indique que les objets <code>null</code> sont
     *        considérés comme inférieurs aux objets non-<code>null</code>
     */
    @SuppressWarnings("unchecked")
    public NullorNotComparableComparator(boolean nullsAreHigh)
    {
        this(ComparableComparator.getInstance(), nullsAreHigh);
    }

    /**
     * Construit une instance qui, lors du tri, considère que les objets nulls sont supérieurs ou inférieurs aux objets
     * non nulls. Lors de la comparaison de deux objets non nulls, le comparateur {@link ComparableComparator} est
     * utilisé
     * 
     * @param nonNullComparator Comparator a utilisé pour comparer deux objets non null
     * @param nullsAreHigh <code>true</code> indique que les objets <code>null</code> sont considérés comme supérieurs
     *        aux objects non-<code>null</code> <code>false</code>indique que les objets <code>null</code> sont
     *        considérés comme inférieurs aux objets non-<code>null</code>
     */
    public NullorNotComparableComparator(Comparator<Object> nonNullComparator, boolean nullsAreHigh)
    {
        this.nonNullComparator = nonNullComparator;
        this.nullsAreHigh = nullsAreHigh;

        if (nonNullComparator == null)
        {
            throw new IllegalArgumentException("null nonNullComparator");
        }
    }

    /**
     * Effectue uen comparaison entre deux objets. Si les deux objets sont null, retourne 0. Si un des objets est null
     * et l'autre non, le résultat dépend si le comparator est construit pour considérér que les objets null sont
     * supérieurs aux objets non null, ou inversement. Si aucun des objets n'est null, le comparator fournit lors de la
     * construction est utilisé pour comparer les deux objets
     * 
     * @param obj1 Le premier objet à comparer
     * @param obj2 Le deuxième objet à comparer
     * @return <code>-1</code> si <code>obj1</code> est "inférieur" à <code>obj2</code> <code>1</code> si
     *         <code>obj1</code> est "supérieur" à <code>obj2</code> <code>0</code> si <code>obj1</code> et
     *         <code>o2</code> sont égaux
     */
    @Override
    public int compare(Object obj1, Object obj2)
    {
        int result;
        Object o1b = obj1;
        Object o2b = obj2;
        if (obj1 == obj2)
        {
            result = 0;
        }
        else if (obj1 == null)
        {
            result = (this.nullsAreHigh ? 1 : -1);
        }
        else if (obj2 == null)
        {
            result = (this.nullsAreHigh ? -1 : 1);
        }
        else if ((obj1 instanceof Comparable == false) || (obj2 instanceof Comparable == false))
        {
            o1b = obj1.toString();
            o2b = obj2.toString();
            result = this.nonNullComparator.compare(o1b, o2b);
        }
        else
        {
            result = this.nonNullComparator.compare(o1b, o2b);
        }
        return result;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode()
    {
        return (nullsAreHigh ? -1 : 1) * nonNullComparator.hashCode();
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj)
    {
        if (obj == null)
        {
            return false;
        }
        if (obj == this)
        {
            return true;
        }
        if (!obj.getClass().equals(this.getClass()))
        {
            return false;
        }

        NullorNotComparableComparator other = (NullorNotComparableComparator) obj;

        return ((this.nullsAreHigh == other.nullsAreHigh) && (this.nonNullComparator.equals(other.nonNullComparator)));
    }

}
