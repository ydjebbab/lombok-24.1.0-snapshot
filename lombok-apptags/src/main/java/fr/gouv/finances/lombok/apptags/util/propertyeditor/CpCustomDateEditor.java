/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.apptags.util.propertyeditor;

import java.beans.PropertyEditorSupport;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.util.StringUtils;

/**
 * PropertyEditor pour <code>java.util.Date</code>, qui utilise les formats définis par la classe
 * <code>java.text.SimpleDateFormat</code>.
 */
public class CpCustomDateEditor extends PropertyEditorSupport
{

    /** Constant : DATE_FORMAT_PARSE. */
    private static final String DATE_FORMAT_PARSE = "d/M/y";

    /** Constant : DATE_FORMAT. */
    private static final String DATE_FORMAT = "dd/MM/yyyy";

    /** Constant : MSG_ERREUR. */
    private static final String MSG_ERREUR = "Format de date incorrect";

    /** Constant : SEPARATEUR_SLASH. */
    private static final String SEPARATEUR_SLASH = "/";

    /** Constant : SEPARATEUR_POINT. */
    private static final String SEPARATEUR_POINT = ".";

    /** Constant : SEPARATEUR_BLANC. */
    private static final String SEPARATEUR_BLANC = " ";

    /** date format. */
    private final DateFormat dateFormat;

    /** allow empty. */
    private final boolean allowEmpty;

    /** allow lenient. */
    private final boolean allowLenient;

    /**
     * Crée une nouvelle instance de CpCustomDateEditor, en utilisant par défaut le format de présentation (JJ/MM/AAAA).
     * Les dates nulles sont autorisées. Les dates hors calendier sont refusées.
     */
    public CpCustomDateEditor()
    {
        super();
        this.dateFormat = new SimpleDateFormat(DATE_FORMAT, Locale.FRANCE);
        this.allowLenient = false;
        this.allowEmpty = true;
    }

    /**
     * Crée une nouvelle instance de CpCustomDateEditor, en utilisant le format de présentation de la date passée en
     * paramètre.
     * <p>
     * Le paramètre "allowEmpty" indique si une chaine vide est autorisée et interprêtée comme une valeur nulle
     * </p>
     * 
     * @param dateFormat
     * @param allowEmpty : indique si les dates nulles sont autorisées ou non
     * @param allowLenient : indique si les dates hors calendier sont refusées ou non.
     */
    public CpCustomDateEditor(DateFormat dateFormat, boolean allowEmpty, boolean allowLenient)
    {
        super();
        this.dateFormat = dateFormat;
        this.allowEmpty = allowEmpty;
        this.allowLenient = allowLenient;
    }

    /**
     * Convertit une chaîne de texte en date en utilisant le format DATE_FORMAT_PARSE.
     * 
     * @param text
     * @return une date
     */
    private Date parse(String text)
    {
        Date result = null;

        try
        {
            DateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT_PARSE, Locale.FRANCE);
            dateFormat.setLenient(this.allowLenient);
            result = new Timestamp(dateFormat.parse(text).getTime());
        }
        catch (ParseException e)
        {
            throw new IllegalArgumentException(MSG_ERREUR, e);
        }
        return result;
    }

    /**
     * methode Epure :
     * 
     * @param text
     * @return string
     */
    private String epure(String text)
    {
        return text.trim();
    }

    /**
     * Normalise le caractère utilisé pour séparer les mois, jours et années.
     * 
     * @param separatorText
     * @return string
     */
    private String normalizeSeparator(String separatorText)
    {
        String result = "";

        if (separatorText == null)
        {
            throw new IllegalArgumentException(MSG_ERREUR);
        }
        else
        {
            if (separatorText.length() > 0 && separatorText.indexOf(SEPARATEUR_SLASH) != -1)
            {
                result = SEPARATEUR_SLASH;
            }
            else if (separatorText.length() > 0 && separatorText.indexOf(SEPARATEUR_POINT) != -1)
            {
                result = SEPARATEUR_POINT;
            }
            else if (separatorText.length() > 0 && separatorText.indexOf(SEPARATEUR_BLANC) != -1)
            {
                result = SEPARATEUR_BLANC;
            }
            else
            {
                throw new IllegalArgumentException(MSG_ERREUR);
            }
        }

        return result;
    }

    /**
     * Teste si deux séparateurs sont identiques.
     * 
     * @param sep1Normalise
     * @param sep2Normalise
     */
    private void areSepEquals(String sep1Normalise, String sep2Normalise)
    {
        // Si le séparateur 1 n'est pas égal au séparateur 2
        if (sep1Normalise == null || sep2Normalise == null || !sep1Normalise.equals(sep2Normalise))
        {
            throw new IllegalArgumentException(MSG_ERREUR);
        }
    }

    /**
     * Si possible retraite la chaîne de caractères pour la conformer au format d/M/y.
     * 
     * @param text
     * @return la chaîne retraitée
     */
    private String constructNormalizeString(String text)
    {
        StringBuilder   result = new StringBuilder();
        String patternWithSep = "(\\d+)([/.\\s]+)(\\d+)([/.\\s]+)(\\d+)";
        String patternWithoutSep = "(\\d\\d)(\\d\\d)(\\d\\d\\d\\d)";
        String sep1;
        String sep2;
        String bloc1;
        String bloc2;
        String bloc3;
        String sep1Normalise;
        String sep2Normalise;

        if (text == null)
        {
            throw new IllegalArgumentException(MSG_ERREUR);
        }

        Matcher matcherWithSep = Pattern.compile(patternWithSep).matcher(text);

        if (matcherWithSep.matches())
        {
            // Décomposition de la chaîne en blocs
            bloc1 = matcherWithSep.group(1);
            sep1 = matcherWithSep.group(2);
            bloc2 = matcherWithSep.group(3);
            sep2 = matcherWithSep.group(4);
            bloc3 = matcherWithSep.group(5);

            // Normalisation des séparateurs
            sep1Normalise = this.normalizeSeparator(sep1);
            sep2Normalise = this.normalizeSeparator(sep2);

            this.areSepEquals(sep1Normalise, sep2Normalise);

            // Normalisation du bloc années
            if (bloc3.length() == 1)
            {
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append('0');
                stringBuilder.append(bloc3);
                bloc3 = stringBuilder.toString();
            }

            // Les années du type 0XX sont transformée en XX
            if (bloc3.length() == 3 && bloc3.charAt(0) == '0')
            {
                bloc3 = bloc3.substring(1);
            }
        }
        else
        {
            Matcher matcherWithoutSep = Pattern.compile(patternWithoutSep).matcher(text);

            if (matcherWithoutSep.matches())
            {
                bloc1 = matcherWithoutSep.group(1);
                bloc2 = matcherWithoutSep.group(2);
                bloc3 = matcherWithoutSep.group(3);
            }
            else
            {
                throw new IllegalArgumentException(MSG_ERREUR);
            }

        }

        result.append(bloc1);
        result.append(SEPARATEUR_SLASH);
        result.append(bloc2);
        result.append(SEPARATEUR_SLASH);
        result.append(bloc3);

        return result.toString();
    }

    /**
     * Construit une date à partir de la chaîne passée en paramètre, après l'avoir retraitée.
     * 
     * @param text le nouveau as text
     * @throws IllegalArgumentException the illegal argument exception
     */
    @Override
    public void setAsText(String text) throws IllegalArgumentException
    {

        Date result = null;
        String texteEpure = this.epure(text);

        if (this.allowEmpty && !StringUtils.hasText(texteEpure))
        {
            setValue(result);
        }
        else
        {
            String normalizeString = this.constructNormalizeString(texteEpure);
            result = new Timestamp(this.parse(normalizeString).getTime());
            setValue(result);
        }
    }

    /**
     * Formate la date en utilisant le format.
     * 
     * @return as text
     */
    @Override
    public String getAsText()
    {
        Date value = (Date) getValue();
        return (value == null ? "" : this.dateFormat.format(value));
    }

}
