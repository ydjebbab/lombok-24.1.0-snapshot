/*
 * Copyright (c) 2014 DGFiP - Tous droits réservés
 *//*
/*Copyright (c) 2014 DGFiP - Tous droits réservés/* * Copyright 2004 original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package fr.gouv.finances.lombok.apptags.table.view;

import fr.gouv.finances.lombok.apptags.table.bean.Export;
import fr.gouv.finances.lombok.apptags.table.core.TableModel;
import fr.gouv.finances.lombok.apptags.table.view.html.BuilderConstants;
import fr.gouv.finances.lombok.apptags.table.view.html.BuilderUtils;
import fr.gouv.finances.lombok.apptags.table.view.html.StatusBarBuilder;
import fr.gouv.finances.lombok.apptags.table.view.html.ToolbarBuilder;
import fr.gouv.finances.lombok.apptags.table.view.html.TwoColumnRowLayout;
import fr.gouv.finances.lombok.apptags.util.HtmlBuilder;

/**
 * Class CompactToolbar
 */
public class CompactToolbar extends TwoColumnRowLayout
{

    /**
     * Instanciation de compact toolbar.
     * 
     * @param html
     * @param model
     */
    public CompactToolbar(HtmlBuilder html, TableModel model)
    {
        super(html, model);
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.view.html.TwoColumnRowLayout#showLayout
     *      (fr.gouv.finances.lombok.apptags.table.core.TableModel)
     */
    @Override
    protected boolean showLayout(TableModel model)
    {
        boolean result = true;
        boolean showStatusBar = BuilderUtils.showStatusBar(model);
        boolean filterable = BuilderUtils.filterable(model);
        boolean showExports = BuilderUtils.showExports(model);
        boolean showExportsBottom = BuilderUtils.showExportsBottom(model);
        boolean showPagination = BuilderUtils.showPagination(model);
        boolean showPaginationBottom = BuilderUtils.showPaginationBottom(model);
        boolean showTitle = BuilderUtils.showTitle(model);
        if (!showStatusBar && !filterable && !showExports && !showExportsBottom && !showPagination && !showTitle && !showPaginationBottom)
        {
            result = false;
        }

        return result;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.view.html.TwoColumnRowLayout#columnLeft
     *      (fr.gouv.finances.lombok.apptags.util.HtmlBuilder, fr.gouv.finances.lombok.apptags.table.core.TableModel)
     */
    @Override
    protected void columnLeft(HtmlBuilder html, TableModel model)
    {
        boolean showStatusBar = BuilderUtils.showStatusBar(model);
        if (showStatusBar)
        {
            html.td(4).styleClass(BuilderConstants.STATUS_BAR_CSS).close();
            new StatusBarBuilder(html, model).statusMessage();
            html.tdEnd();
        }
    }

    /**
     * showResponsive="true" (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.view.html.TwoColumnRowLayout#columnLeft
     *      (fr.gouv.finances.lombok.apptags.util.HtmlBuilder, fr.gouv.finances.lombok.apptags.table.core.TableModel)
     */
    @Override
    protected void columnLeftRWD(HtmlBuilder html, TableModel model)
    {
        boolean showStatusBar = BuilderUtils.showStatusBar(model);
        if (showStatusBar)
        {
            html.div().styleClass(BuilderConstants.STATUS_BAR_CSS).close();
            new StatusBarBuilder(html, model).statusMessage();
            html.divEnd();
        }
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.view.html.TwoColumnRowLayout#columnRight
     *      (fr.gouv.finances.lombok.apptags.util.HtmlBuilder, fr.gouv.finances.lombok.apptags.table.core.TableModel)
     */
    @Override
    protected void columnRight(HtmlBuilder html, TableModel model)
    {
        boolean filterable = BuilderUtils.filterable(model);
        boolean showPagination = BuilderUtils.showPagination(model);
        boolean showPaginationBottom = BuilderUtils.showPaginationBottom(model);
        boolean showExports = BuilderUtils.showExports(model);
        boolean showExportsBottom = BuilderUtils.showExportsBottom(model);
        boolean inPaginationBottom = BuilderUtils.inPaginationBottom(model);

        ToolbarBuilder toolbarBuilder = new ToolbarBuilder(html, model);

        html.td(4).styleClass(BuilderConstants.COMPACT_TOOLBAR_CSS +" alignRight").close();

        html.table(4).styleClass("noBorder cellPadding1 cellSpacing2").close();
        html.tr(5).close();

        if ((showPagination && !inPaginationBottom) 
            || (showPaginationBottom && inPaginationBottom))
        {
            html.td(5).close();
            toolbarBuilder.firstPageItemAsImage();
            html.tdEnd();

            html.td(5).close();
            toolbarBuilder.prevPageItemAsImage();
            html.tdEnd();

            html.td(5).close();
            toolbarBuilder.nextPageItemAsImage();
            html.tdEnd();

            html.td(5).close();
            toolbarBuilder.lastPageItemAsImage();
            html.tdEnd();

            html.td(5).close();
            toolbarBuilder.separator();
            html.tdEnd();

            html.td(5).close();
            toolbarBuilder.rowsDisplayedDroplist();
            html.tdEnd();

            if (showExports)
            {
                html.td(5).close();
                toolbarBuilder.separator();
                html.tdEnd();
            }
        }

        if ((showExports && !inPaginationBottom) 
            || (inPaginationBottom && showExportsBottom))
        {
            int showExportsWithLimitSizePdf = BuilderUtils.showExportsWithLimitSizePdf(model);
            int showExportsWithLimitSizeXlsOrOds = BuilderUtils.showExportsWithLimitSizeXlsOrOds(model);
            int showExportsWithLimitSizeCsv = BuilderUtils.showExportsWithLimitSizeCsv(model);

            for (Export export : model.getExportHandler().getExports())
            {
                html.td(5).close();
                if (((export.getImageName().toUpperCase().endsWith("PDF") == true) && model.getCollectionOfFilteredBeans().size() < showExportsWithLimitSizePdf)
                    || ((export.getImageName().toUpperCase().endsWith("XLS") == true || export.getImageName().toUpperCase().endsWith("ODS") == true) && model
                        .getCollectionOfFilteredBeans().size() < showExportsWithLimitSizeXlsOrOds)
                    || (export.getImageName().toUpperCase().endsWith("CSV") == true && model.getCollectionOfFilteredBeans().size() < showExportsWithLimitSizeCsv))
                {
                    toolbarBuilder.exportItemAsImage(export);
                }
                else
                {
                    html.append("veuillez filtrer votre liste pour accéder aux exports " + export.getImageName());
                }
                html.tdEnd();
            }
        }

        if (filterable)
        {
            if (showExports || showPagination || showPaginationBottom || showExportsBottom)
            {
                html.td(5).close();
                toolbarBuilder.separator();
                html.tdEnd();
            }

            html.td(5).close();
            toolbarBuilder.filterItemAsImage();
            html.tdEnd();

            html.td(5).close();
            toolbarBuilder.clearItemAsImage();
            html.tdEnd();
        }

        html.trEnd(5);

        html.tableEnd(4);

        html.tdEnd();
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.view.html.TwoColumnRowLayout#columnRight
     *      (fr.gouv.finances.lombok.apptags.util.HtmlBuilder, fr.gouv.finances.lombok.apptags.table.core.TableModel)
     */
    @Override
    protected void columnRightRWD(HtmlBuilder html, TableModel model)
    {
        boolean filterable = BuilderUtils.filterable(model);
        boolean showPagination = BuilderUtils.showPagination(model);
        boolean showPaginationBottom = BuilderUtils.showPaginationBottom(model);
        boolean showExports = BuilderUtils.showExports(model);
        String paginationClass = BuilderUtils.getPaginationClass(model);
        String exportsClass = BuilderUtils.getExportsClass(model);

        ToolbarBuilder toolbarBuilder = new ToolbarBuilder(html, model);
        html.div().styleClass("toolBarRWD container-fluid").close();

        if (showPagination || showPaginationBottom)
        {
            html.ul().styleClass(paginationClass).close();

            html.li("").close();
            toolbarBuilder.firstPageItemAsImage();
            html.liEnd();

            html.li("").close();
            toolbarBuilder.prevPageItemAsImage();
            html.liEnd();

            html.li("").close();
            toolbarBuilder.nextPageItemAsImage();
            html.liEnd();

            html.li("").close();
            toolbarBuilder.lastPageItemAsImage();
            html.liEnd();

            html.li("").close();
            toolbarBuilder.separator();
            html.liEnd();

            html.li("").close();
            toolbarBuilder.rowsDisplayedDroplist();
            html.liEnd();

            html.ulEnd();
        }

        if (showExports)
        {

            int showExportsWithLimitSizePdf = BuilderUtils.showExportsWithLimitSizePdf(model);
            int showExportsWithLimitSizeXlsOrOds = BuilderUtils.showExportsWithLimitSizeXlsOrOds(model);
            int showExportsWithLimitSizeCsv = BuilderUtils.showExportsWithLimitSizeCsv(model);

            html.ul().styleClass("exports " + exportsClass).close();

            html.li("").close();
            html.a().append(".collapseExports").dataAttributes("data-toggle=collapse").styleClass("btn").close().append("exports").aEnd();
            html.liEnd();

            for (Export export : model.getExportHandler().getExports())
            {
                html.li("").styleClass("collapseExports collapse").close();
                if (((export.getImageName().toUpperCase().endsWith("PDF") == true) && model.getCollectionOfFilteredBeans().size() < showExportsWithLimitSizePdf)
                    || ((export.getImageName().toUpperCase().endsWith("XLS") == true || export.getImageName().toUpperCase().endsWith("ODS") == true) && model
                        .getCollectionOfFilteredBeans().size() < showExportsWithLimitSizeXlsOrOds)
                    || (export.getImageName().toUpperCase().endsWith("CSV") == true && model.getCollectionOfFilteredBeans().size() < showExportsWithLimitSizeCsv))
                {
                    toolbarBuilder.exportItemAsImage(export);
                }
                else
                {
                    html.append("veuillez filtrer votre liste pour accéder aux exports " + export.getImageName());
                }
                html.liEnd();
            }
            html.ulEnd();
        }

        if (filterable)
        {
            toolbarBuilder.filterItemAsImage();
            toolbarBuilder.clearItemAsImage();
        }
        html.divEnd();
    }

}
