/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.apptags.table.bean;

import java.io.File;
import java.util.Arrays;

/**
 * Class FileContent
 */
public class FileContent
{

    /** id. */
    private Long id;

    /** version. */
    private int version;

    /** Contenu du fichier sous forme de tableau de bytes. */
    private byte[] data;

    /**
     * Fichier temporaire dans lequel les données peuvent éventuellement être stockée. Destiné à stocker les fichiers de
     * taille importante avant leur stockage en base ou lors de la phase de restitution à l'utilisateur. Cette propriété
     * n'est pas stockée directement en base de donnée. Les données contenue dans le fichier temporaire sont placées
     * dans le champ 'data'
     */
    private File fichierTemporaire;

    /**
     * Constructeur de la classe FileContent.java
     */
    public FileContent()
    {
        super();

    }

    /**
     * (methode de remplacement) {@inheritDoc}
     *
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
        {
            return true;
        }
        if (obj == null)
        {
            return false;
        }
        if (getClass() != obj.getClass())
        {
            return false;
        }
        final FileContent other = (FileContent) obj;
        if (!Arrays.equals(data, other.data))
        {
            return false;
        }
        return true;
    }

    /**
     * Gets the contenu du fichier sous forme de tableau de bytes.
     *
     * @return the contenu du fichier sous forme de tableau de bytes
     */
    public byte[] getData()
    {
        return data;
    }

    /**
     * Gets the fichier temporaire dans lequel les données peuvent éventuellement être stockée.
     *
     * @return the fichier temporaire dans lequel les données peuvent éventuellement être stockée
     */
    public File getFichierTemporaire()
    {
        return fichierTemporaire;
    }

    /**
     * Accesseur de l attribut id.
     *
     * @return id
     */
    public Long getId()
    {
        return id;
    }

    /**
     * Accesseur de l attribut version.
     *
     * @return version
     */
    public int getVersion()
    {
        return version;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     *
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + Arrays.hashCode(data);
        return result;
    }

    /**
     * Sets the contenu du fichier sous forme de tableau de bytes.
     *
     * @param data the new contenu du fichier sous forme de tableau de bytes
     */
    public void setData(byte[] data)
    {
        this.data = data;
    }

    /**
     * Sets the fichier temporaire dans lequel les données peuvent éventuellement être stockée.
     *
     * @param fichierTemporaire the new fichier temporaire dans lequel les données peuvent éventuellement être stockée
     */
    public void setFichierTemporaire(File fichierTemporaire)
    {
        this.fichierTemporaire = fichierTemporaire;
    }

    /**
     * Modificateur de l attribut id.
     *
     * @param id le nouveau id
     */
    public void setId(Long id)
    {
        this.id = id;
    }

    /**
     * Modificateur de l attribut version.
     *
     * @param version le nouveau version
     */
    public void setVersion(int version)
    {
        this.version = version;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     *
     * @see java.lang.Object#finalize()
     */
    protected void finalize()
    {
        if (this.fichierTemporaire != null)
        {
            this.fichierTemporaire.delete();
        }
    }

}
