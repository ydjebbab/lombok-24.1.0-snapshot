/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : TreeIteratorElement.java
 *
 */
package fr.gouv.finances.lombok.apptags.tree.bean;

import java.io.Serializable;
import java.util.List;

/**
 * Class TreeIteratorElement.
 * 
 * @author wpetit-cp Les instances de cette classe encapsulent les instances <code>TreeNode</code> quand le TreeIterator
 *         itère sur l'arbre dans le tag tree
 * @version $Revision: 1.2 $ Date: 15 déc. 2009
 */
public class TreeIteratorElement implements Serializable
{

    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** node. */
    protected TreeNode node = null;

    /** indentation profile. */
    protected List<Boolean> indentationProfile = null;

    /** is expanded. */
    protected boolean isExpanded = false;

    /** is selected. */
    protected boolean isSelected = false;

    /** is first child. */
    protected boolean isFirstChild = false;

    /** is last child. */
    protected boolean isLastChild = false;

    /**
     * Instanciation de tree iterator element.
     * 
     * @param node --
     * @param indentationProfile --
     * @param isExpanded --
     * @param isSelected --
     * @param isFirstChild --
     * @param isLastChild --
     */
    public TreeIteratorElement(TreeNode node, List<Boolean> indentationProfile, boolean isExpanded, boolean isSelected,
        boolean isFirstChild, boolean isLastChild)
    {
        this.node = node;
        this.indentationProfile = indentationProfile;
        this.isExpanded = isExpanded;
        this.isSelected = isSelected;
        this.isFirstChild = isFirstChild;
        this.isLastChild = isLastChild;
    }

    /**
     * Retourne l'id du noeud encapsulé.
     * 
     * @return L'id du noeud encapsulé
     */
    public String getId()
    {
        return getNode().getId();
    }

    /**
     * Returns the indentation profile of the wrapped node. The indendation profile is a <code>List</code> of
     * <code>Boolean</code> instances, containing one instance for each ancestor of the wrapped node. The
     * <code>Boolean</code> instances tells whether that particular ancestor was the last child of it's parent or not.
     * If the ancestor was not the last child of it's parent, a vertical line can be drawn from this ancestor to it's
     * next sibling. Else a blank space can be drawn. Retourne le profil d'indentation du noeud encapsulé. Il s'agit
     * d'une <code>List</code> de <code>Boolean</code> contenant une instance pour chaque ancêtre du noeud. L'instance
     * de <code>Boolean</code> indique si l'ancêtre est ou non le dernier enfant de son parent. Si l'ancêtre n'est pas
     * le dernier enfant de son parent, une ligne verticale peut être tracée de l'ancêtre vers son successeur
     * 
     * @return Une <code>List</code> de <code>Boolean</code> indiquant comment connecter les ancêtres du noeud à leur
     *         ancêtre suivant avec des lignes verticales
     */
    public List<Boolean> getIndendationProfile()
    {
        return this.indentationProfile;
    }

    /**
     * Retourne le nom du noeud encapsulé.
     * 
     * @return Le nom du noeud encapsulé.
     */
    public String getName()
    {
        return getNode().getName();
    }

    /**
     * Retourne le noeud encapsulé par l'objet <code>ITreeIteratorElement</code> .
     * 
     * @return Une instance <code>TreeNode</code>
     */
    public TreeNode getNode()
    {
        return this.node;
    }

    /**
     * Accesseur de l attribut number of rows.
     * 
     * @return number of rows
     */
    public int getNumberOfRows()
    {
        int result = 1;

        int maxCharactersPerLine = 30;
        int nbChar = getNode().getName().length();

        result = nbChar / maxCharactersPerLine;

        return result;
    }

    /**
     * Retourne True si le noeud encapsulé est ouvert.
     * 
     * @return True si le noeud encapsulé est ouvert
     */
    public boolean isExpanded()
    {
        return this.isExpanded;
    }

    /**
     * Retourne True si le noeud encapsulé est le premier enfant.
     * 
     * @return True si le noeud encapsulé est le premier enfant
     */
    public boolean isFirstChild()
    {
        return this.isFirstChild;
    }

    /**
     * Retourne True si le noeud encapsulé est le dernier enfant.
     * 
     * @return True si le noeud encapsulé est le dernier enfant
     */
    public boolean isLastChild()
    {
        return this.isLastChild;
    }

    /**
     * Retourne True si le noeud encapsulé est sélectionné.
     * 
     * @return True si le noeud encapsulé est ouvert
     */
    public boolean isSelected()
    {
        return this.isSelected;
    }
}
