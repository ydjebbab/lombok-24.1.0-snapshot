/*
 * Copyright (c) 2014 DGFiP - Tous droits réservés
 *//*
/*Copyright (c) 2014 DGFiP - Tous droits réservés/* * Copyright 2004 original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package fr.gouv.finances.lombok.apptags.table.view;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFPrintSetup;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.util.CellRangeAddress;

import fr.gouv.finances.lombok.apptags.table.bean.Column;
import fr.gouv.finances.lombok.apptags.table.bean.Export;
import fr.gouv.finances.lombok.apptags.table.core.TableModel;

/**
 * Class OdsView
 */
public class OdsView implements View
{

    /** Constant : logger. */
    private static final Log logger = LogFactory.getLog(OdsView.class);

    /** Constant : WIDTH_MULT. */
    public static final int WIDTH_MULT = 241;

    /** Constant : MIN_CHARS. */
    public static final int MIN_CHARS = 8;

    /** Constant : MAX_CHARS. */
    public static final int MAX_CHARS = 255;

    /** Constant : DEF_FONT_HEIGHT. */
    public static final short DEF_FONT_HEIGHT = 8;

    /** Constant : DEF_ROW_HEIGHT. */
    public static final float DEF_ROW_HEIGHT = 18;

    /** Constant : PACKAGE_CELL. */
    private static final String PACKAGE_CELL = "fr.gouv.finances.lombok.apptags.table.cell";

    // Formatage des monnaies
    /** Constant : F_MNE_ZERO_DEC. */
    private static final String F_MNE_ZERO_DEC = "#,##0";

    /** Constant : C_MNE_ZERO_DEC. */
    private static final String C_MNE_ZERO_DEC = PACKAGE_CELL + ".MonnaieCellZeroDecimale";

    /** Constant : F_MNE_UNE_DEC. */
    private static final String F_MNE_UNE_DEC = "#,##0.0";

    /** Constant : C_MNE_UNE_DEC. */
    private static final String C_MNE_UNE_DEC = PACKAGE_CELL + ".MonnaieCellUneDecimale";

    /** Constant : F_MNE_DEUX_DEC. */
    private static final String F_MNE_DEUX_DEC = "#,##0.00";

    /** Constant : C_MNE_DEUX_DEC. */
    private static final String C_MNE_DEUX_DEC = PACKAGE_CELL + ".MonnaieCellDeuxDecimales";

    /** Constant : F_MNE_TROIS_DEC. */
    private static final String F_MNE_TROIS_DEC = "#,##0.000";

    /** Constant : C_MNE_TROIS_DEC. */
    private static final String C_MNE_TROIS_DEC = PACKAGE_CELL + ".MonnaieCellTroisDecimales";

    /** Constant : F_MNE_QUATRE_DEC. */
    private static final String F_MNE_QUATRE_DEC = "#,##0.0000";

    /** Constant : C_MNE_QUATRE_DEC. */
    private static final String C_MNE_QUATRE_DEC = PACKAGE_CELL + ".MonnaieCellQuatreDecimales";

    /** Constant : F_MNE_CINQ_DEC. */
    private static final String F_MNE_CINQ_DEC = "#,##0.00000";

    /** Constant : C_MNE_CINQ_DEC. */
    private static final String C_MNE_CINQ_DEC = PACKAGE_CELL + ".MonnaieCellCinqDecimales";

    /** Constant : F_MNE_SIX_DEC. */
    private static final String F_MNE_SIX_DEC = "#,##0.000000";

    /** Constant : C_MNE_SIX_DEC. */
    private static final String C_MNE_SIX_DEC = PACKAGE_CELL + ".MonnaieCellSixDecimales";

    /** Constant : F_MNE_SEPT_DEC. */
    private static final String F_MNE_SEPT_DEC = "#,##0.0000000";

    /** Constant : C_MNE_SEPT_DEC. */
    private static final String C_MNE_SEPT_DEC = PACKAGE_CELL + ".MonnaieCellSeptDecimales";

    /** Constant : F_MNE_HUIT_DEC. */
    private static final String F_MNE_HUIT_DEC = "#,##0.00000000";

    /** Constant : C_MNE_HUIT_DEC. */
    private static final String C_MNE_HUIT_DEC = PACKAGE_CELL + ".MonnaieCellHuitDecimales";

    /** Constant : F_MNE_NEUF_DEC. */
    private static final String F_MNE_NEUF_DEC = "#,##0.000000000";

    /** Constant : C_MNE_NEUF_DEC. */
    private static final String C_MNE_NEUF_DEC = PACKAGE_CELL + ".MonnaieCellNeufDecimales";

    /** Constant : F_MNE_DIX_DEC. */
    private static final String F_MNE_DIX_DEC = "#,##0.0000000000";

    /** Constant : C_MNE_DIX_DEC. */
    private static final String C_MNE_DIX_DEC = PACKAGE_CELL + ".MonnaieCellDixDecimales";

    // Formatage des nombres
    /** Constant : F_NB_ZERO_DEC. */
    private static final String F_NB_ZERO_DEC = "#,##0";

    /** Constant : C_NB_ZERO_DEC. */
    private static final String C_NB_ZERO_DEC = PACKAGE_CELL + ".NombreCellZeroDecimale";

    /** Constant : F_NB_UNE_DEC. */
    private static final String F_NB_UNE_DEC = "#,##0.0";

    /** Constant : C_NB_UNE_DEC. */
    private static final String C_NB_UNE_DEC = PACKAGE_CELL + ".NombreCellUneDecimale";

    /** Constant : F_NB_DEUX_DEC. */
    private static final String F_NB_DEUX_DEC = "#,##0.00";

    /** Constant : C_NB_DEUX_DEC. */
    private static final String C_NB_DEUX_DEC = PACKAGE_CELL + ".NombreCellDeuxDecimales";

    /** Constant : F_NB_TROIS_DEC. */
    private static final String F_NB_TROIS_DEC = "#,##0.000";

    /** Constant : C_NB_TROIS_DEC. */
    private static final String C_NB_TROIS_DEC = PACKAGE_CELL + ".NombreCellTroisDecimales";

    /** Constant : F_NB_QUATRE_DEC. */
    private static final String F_NB_QUATRE_DEC = "#,##0.0000";

    /** Constant : C_NB_QUATRE_DEC. */
    private static final String C_NB_QUATRE_DEC = PACKAGE_CELL + ".NombreCellQuatreDecimales";

    /** Constant : F_NB_CINQ_DEC. */
    private static final String F_NB_CINQ_DEC = "#,##0.00000";

    /** Constant : C_NB_CINQ_DEC. */
    private static final String C_NB_CINQ_DEC = PACKAGE_CELL + ".NombreCellCinqDecimales";

    /** Constant : F_NB_SIX_DEC. */
    private static final String F_NB_SIX_DEC = "#,##0.000000";

    /** Constant : C_NB_SIX_DEC. */
    private static final String C_NB_SIX_DEC = PACKAGE_CELL + ".NombreCellSixDecimales";

    /** Constant : F_NB_SEPT_DEC. */
    private static final String F_NB_SEPT_DEC = "#,##0.0000000";

    /** Constant : C_NB_SEPT_DEC. */
    private static final String C_NB_SEPT_DEC = PACKAGE_CELL + ".NombreCellSeptDecimales";

    /** Constant : F_NB_HUIT_DEC. */
    private static final String F_NB_HUIT_DEC = "#,##0.00000000";

    /** Constant : C_NB_HUIT_DEC. */
    private static final String C_NB_HUIT_DEC = PACKAGE_CELL + ".NombreCellHuitDecimales";

    /** Constant : F_NB_NEUF_DEC. */
    private static final String F_NB_NEUF_DEC = "#,##0.000000000";

    /** Constant : C_NB_NEUF_DEC. */
    private static final String C_NB_NEUF_DEC = PACKAGE_CELL + ".NombreCellNeufDecimales";

    /** Constant : F_NB_DIX_DEC. */
    private static final String F_NB_DIX_DEC = "#,##0.0000000000";

    /** Constant : C_NB_DIX_DEC. */
    private static final String C_NB_DIX_DEC = PACKAGE_CELL + ".NombreCellDixDecimales";

    // Formatage des nombres sans séparateurs de milliers
    /** Constant : F_NB_ZERODECNOSEP. */
    private static final String F_NB_ZERODECNOSEP = "0";

    /** Constant : C_NB_ZERODECNOSEP. */
    private static final String C_NB_ZERODECNOSEP = PACKAGE_CELL + ".NombreCellZeroDecimaleSansSep";

    // Formatage des pourcentages
    /** Constant : F_PCT_ZERO_DEC. */
    private static final String F_PCT_ZERO_DEC = "0%";

    /** Constant : C_PCT_ZERO_DEC. */
    private static final String C_PCT_ZERO_DEC = PACKAGE_CELL + ".PourCentCellZeroDecimale";

    /** Constant : F_PCT_UNE_DEC. */
    private static final String F_PCT_UNE_DEC = "0.0%";

    /** Constant : C_PCT_UNE_DEC. */
    private static final String C_PCT_UNE_DEC = PACKAGE_CELL + ".PourCentCellUneDecimale";

    /** Constant : F_PCT_DEUX_DEC. */
    private static final String F_PCT_DEUX_DEC = "0.00%";

    /** Constant : C_PCT_DEUX_DEC. */
    private static final String C_PCT_DEUX_DEC = PACKAGE_CELL + ".PourCentCellDeuxDecimales";

    /** Constant : F_PCT_TROIS_DEC. */
    private static final String F_PCT_TROIS_DEC = "0.000%";

    /** Constant : C_PCT_TROIS_DEC. */
    private static final String C_PCT_TROIS_DEC = PACKAGE_CELL + ".PourCentCellTroisDecimales";

    /** Constant : F_PCT_QUATRE_DEC. */
    private static final String F_PCT_QUATRE_DEC = "0.0000%";

    /** Constant : C_PCT_QUATRE_DEC. */
    private static final String C_PCT_QUATRE_DEC = PACKAGE_CELL + ".PourCentCellQuatreDecimales";

    /** Constant : F_PCT_CINQ_DEC. */
    private static final String F_PCT_CINQ_DEC = "0.00000%";

    /** Constant : C_PCT_CINQ_DEC. */
    private static final String C_PCT_CINQ_DEC = PACKAGE_CELL + ".PourCentCellCinqDecimales";

    /** Constant : F_PCT_SIX_DEC. */
    private static final String F_PCT_SIX_DEC = "0.000000%";

    /** Constant : C_PCT_SIX_DEC. */
    private static final String C_PCT_SIX_DEC = PACKAGE_CELL + ".PourCentCellSixDecimales";

    /** Constant : F_PCT_SEPT_DEC. */
    private static final String F_PCT_SEPT_DEC = "0.0000000%";

    /** Constant : C_PCT_SEPT_DEC. */
    private static final String C_PCT_SEPT_DEC = PACKAGE_CELL + ".PourCentCellSeptDecimales";

    /** Constant : F_PCT_HUIT_DEC. */
    private static final String F_PCT_HUIT_DEC = "0.00000000%";

    /** Constant : C_PCT_HUIT_DEC. */
    private static final String C_PCT_HUIT_DEC = PACKAGE_CELL + ".PourCentCellHuitDecimales";

    /** Constant : F_PCT_NEUF_DEC. */
    private static final String F_PCT_NEUF_DEC = "0.000000000%";

    /** Constant : C_PCT_NEUF_DEC. */
    private static final String C_PCT_NEUF_DEC = PACKAGE_CELL + ".PourCentCellNeufDecimales";

    /** Constant : F_PCT_DIX_DEC. */
    private static final String F_PCT_DIX_DEC = "0.0000000000%";

    /** Constant : C_PCT_DIX_DEC. */
    private static final String C_PCT_DIX_DEC = PACKAGE_CELL + ".PourCentCellDixDecimales";

    // Formatage des Dates
    /** Constant : F_DA. */
    private static final String F_DA = "dd/MM/yyyy";

    /** Constant : C_DA. */
    private static final String C_DA = PACKAGE_CELL + ".DateCell";

    /** Constant : F_DA_HE. */
    private static final String F_DA_HE = "dd/MM/yyyy hh";

    /** Constant : C_DA_HE. */
    private static final String C_DA_HE = PACKAGE_CELL + ".DateHeureCell";

    /** Constant : F_DA_HE_MI. */
    private static final String F_DA_HE_MI = "dd/MM/yyyy hh:mm";

    /** Constant : C_DA_HE_MI. */
    private static final String C_DA_HE_MI = PACKAGE_CELL + ".DateHeureMinuteCell";

    /** Constant : F_DA_HE_MI_S. */
    private static final String F_DA_HE_MI_S = "dd/MM/yyyy hh:mm:ss";

    /** Constant : C_DA_HE_MI_S. */
    private static final String C_DA_HE_MI_S = PACKAGE_CELL + ".DateHeureMinuteSecondeCell";

    /** Constant : C_HE. */
    private static final String C_HE = PACKAGE_CELL + ".HeureCell";

    /** Constant : F_HE. */
    private static final String F_HE = "hh";

    /** Constant : C_HE_MI. */
    private static final String C_HE_MI = PACKAGE_CELL + ".HeureMinuteCell";

    /** Constant : F_HE_MI. */
    private static final String F_HE_MI = "hh:mm";

    /** Constant : C_HE_MI_S. */
    private static final String C_HE_MI_S = PACKAGE_CELL + ".HeureMinuteSecondeCell";

    /** Constant : F_HE_MI_S. */
    private static final String F_HE_MI_S = "hh:mm:ss";

    // Formatage des booléens
    /** Constant : C_BOO_OUI_NON. */
    private static final String C_BOO_OUI_NON = PACKAGE_CELL + ".BooleanCellOuiNon";

    /** Constant : C_BOO_O_N. */
    private static final String C_BOO_O_N = PACKAGE_CELL + ".BooleanCellON";

    /** Constant : C_BOO_VRAI_FAUX. */
    private static final String C_BOO_VRAI_FAUX = PACKAGE_CELL + ".BooleanCellVraiFaux";

    /** Constant : C_BOO_V_F. */
    private static final String C_BOO_V_F = PACKAGE_CELL + ".BooleanCellVF";

    /** Constant : C_BOO_1_0. */
    private static final String C_BOO_1_0 = PACKAGE_CELL + ".BooleanCell10";

    /** Constant : NBSP. */
    private static final String NBSP = "&#160;";

    /** workbook. */
    private HSSFWorkbook workbook;

    /** sheet. */
    private HSSFSheet sheet;

    /** printer setup. */
    private HSSFPrintSetup printerSetup;

    /** styles. */
    private Map<String, HSSFCellStyle> styles;

    /** styles numerics. */
    private Map<String, HSSFCellStyle> stylesNumerics;

    /** styles dates. */
    private Map<String, HSSFCellStyle> stylesDates;

    /** styles booleans. */
    private Map<String, HSSFCellStyle> stylesBooleans;

    /** styles pourcents. */
    private Map<String, HSSFCellStyle> stylesPourcents;

    /** rownum. */
    private int rownum;

    /** cellnum. */
    private int cellnum;

    /** hssf row. */
    private HSSFRow hssfRow;

    /** font bold. */
    private HSSFFont fontBold;

    /** font. */
    private HSSFFont font;

    /** format. */
    private HSSFDataFormat format;

    /** Constant : INCLUDE_TITLE. */
    public static final String INCLUDE_TITLE = "includeTitle";

    /**
     * Instanciation de ods view.
     */
    public OdsView()
    {
        super();
        workbook = new HSSFWorkbook();
        sheet = workbook.createSheet();
        format = workbook.createDataFormat();
        font = initFont(workbook, DEF_FONT_HEIGHT);
        fontBold = initFontBold(workbook, DEF_FONT_HEIGHT);
        styles = initStyles(workbook, font, fontBold);
        stylesNumerics = initStylesNumerics(workbook, font);
        stylesDates = initStylesDates(workbook, font);
        stylesBooleans = initStylesBooleans(workbook, font);
        stylesPourcents = initStylesPourcents(workbook, font);
        printerSetup = sheet.getPrintSetup();
        sheet.setAutobreaks(true);
        printerSetup.setFitHeight((short) 1);
        printerSetup.setFitWidth((short) 1);
        printerSetup.setPaperSize(HSSFPrintSetup.A4_PAPERSIZE);
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.view.View#beforeBody(fr.gouv.finances.lombok.apptags.table.core.TableModel)
     */
    @Override
    public void beforeBody(TableModel model)
    {
        logger.debug("XlsView.init()");
        String encoding = model.getExportHandler().getCurrentExport().getEncoding();

        if (("UTF").equalsIgnoreCase(encoding)
            || ("UNICODE").equalsIgnoreCase(encoding))
        {
            workbook.setSheetName(0, "Export");
        }

        createHeader(model);
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.view.View#body(fr.gouv.finances.lombok.apptags.table.core.TableModel,
     *      fr.gouv.finances.lombok.apptags.table.bean.Column)
     */
    @Override
    public void body(final TableModel model, final Column column)
    {
        if (column.isFirstColumn())
        {
            rownum++;
            cellnum = 0;
            hssfRow = sheet.createRow(rownum);
            hssfRow.setHeightInPoints(DEF_ROW_HEIGHT);
        }

        String value = ExportViewUtils.parseXLS(column.getCellDisplay());
        Object propertyValue = column.getPropertyValue();
        String cellType = column.getCell();
        HSSFCell hssfCell = hssfRow.createCell(cellnum);

        if (column.isEscapeAutoFormat())
        {

            writeToCellAsText(hssfCell, value, "");
        }
        else
        {
            writeToCellFormatted(hssfCell, value, propertyValue, cellType, "");
        }
        cellnum++;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.view.View#afterBody(fr.gouv.finances.lombok.apptags.table.core.TableModel)
     */
    @Override
    public Object afterBody(final TableModel model)
    {
        return workbook;
    }

    /**
     * methode Creates the header :
     * 
     * @param model
     */
    private void createHeader(final TableModel model)
    {
        rownum = 0;
        cellnum = 0;

        List<Column> columns = model.getColumnHandler().getHeaderColumns();
        if (isIncludeTitle(model) != null && !("").equalsIgnoreCase(isIncludeTitle(model)))
        {
            HSSFRow row = sheet.createRow(0);
            HSSFCell cell = row.createCell(0);

            writeToCellAsText(cell, isIncludeTitle(model), "");
            sheet.addMergedRegion(new CellRangeAddress(0, 0, 0, columns.size() - 1));
            sheet.addMergedRegion(new CellRangeAddress(1, 1, 0, columns.size() - 1));
            row = sheet.createRow(1);

            writeToCellAsText(row.createCell(1), "", "");
            rownum = 2;

        }
        if (StringUtils.isNotBlank(getGroup(model.getColumnHandler().getHeaderColumns().get(0))))
        {
            // création de la ligne pour l'attribut group
            HSSFRow row = sheet.createRow(rownum);
            row.setHeightInPoints(DEF_ROW_HEIGHT);
            String oldTitleGroup = "";
            for (Column column : columns)
            {
                if (column.isFirstColumn())
                {
                    rownum++;
                    cellnum = 0;
                    hssfRow = sheet.createRow(rownum);
                    hssfRow.setHeightInPoints(DEF_ROW_HEIGHT);
                }

                String propertyValueGroup = this.getGroup(column);

                if (StringUtils.isNotBlank(propertyValueGroup))
                {
                    HSSFCell hssfCell = row.createCell(cellnum);
                    hssfCell.setCellStyle((HSSFCellStyle) styles.get("titleStyle"));
                    hssfCell.setCellType(HSSFCell.CELL_TYPE_STRING);
                    HSSFRichTextString titleR = new HSSFRichTextString(propertyValueGroup);
                    hssfCell.setCellValue(titleR);
                    int valWidth = (propertyValueGroup + "").length() * WIDTH_MULT;
                    if (oldTitleGroup.compareTo(titleR.getString()) == 0)
                    {
                        sheet.addMergedRegion(
                            new CellRangeAddress(rownum - 1,  rownum - 1, 
                            hssfCell.getColumnIndex() - 1, hssfCell.getColumnIndex()));
                    }
                    else
                    {
                        sheet.setColumnWidth(hssfCell.getColumnIndex(), valWidth);
                    }
                    oldTitleGroup = titleR.getString();
                    cellnum++;
                }
            }
        }

        // création de la ligne pour l'attribut subGroup
        if (StringUtils.isNotBlank(getSubGroup(model.getColumnHandler().getHeaderColumns().get(0))))
        {
            HSSFRow row2 = sheet.createRow(rownum);
            row2.setHeightInPoints(DEF_ROW_HEIGHT);
            String oldTitleSubGroup = "";
            for (Column column : columns)
            {
                if (column.isFirstColumn())
                {
                    rownum++;
                    cellnum = 0;
                    hssfRow = sheet.createRow(rownum);
                    hssfRow.setHeightInPoints(DEF_ROW_HEIGHT);
                }

                String propertyValueSubGroup = this.getSubGroup(column);
                if (StringUtils.isNotBlank(propertyValueSubGroup))
                {
                    HSSFCell hssfCell = row2.createCell(cellnum);
                    hssfCell.setCellStyle((HSSFCellStyle) styles.get("titleStyle"));
                    hssfCell.setCellType(HSSFCell.CELL_TYPE_STRING);
                    HSSFRichTextString titleR = new HSSFRichTextString(propertyValueSubGroup);
                    hssfCell.setCellValue(titleR);
                    int valWidth = (propertyValueSubGroup + "").length() * WIDTH_MULT;
                    if (oldTitleSubGroup.compareTo(titleR.getString()) == 0)
                    {
                        sheet.addMergedRegion(
                            new CellRangeAddress(rownum - 1, rownum - 1,
                                hssfCell.getColumnIndex() - 1, hssfCell.getColumnIndex()));
                    }
                    else
                    {
                        sheet.setColumnWidth(hssfCell.getColumnIndex(), valWidth);
                    }
                    oldTitleSubGroup = titleR.getString();
                    cellnum++;
                }
            }
        }
        // remplissage du tableau avec le titre "classique" du tableau ectable
        HSSFRow row1 = sheet.createRow(rownum);
        row1.setHeightInPoints(DEF_ROW_HEIGHT);
        for (Column column : columns)
        {
            if (column.isFirstColumn())
            {
                cellnum = 0;
                hssfRow = sheet.createRow(rownum);
                hssfRow.setHeightInPoints(DEF_ROW_HEIGHT);
            }

            String title = column.getCellDisplay();

            HSSFCell hssfCell = row1.createCell(cellnum);
            hssfCell.setCellStyle((HSSFCellStyle) styles.get("titleStyle"));
            hssfCell.setCellType(HSSFCell.CELL_TYPE_STRING);

            HSSFRichTextString titleR = new HSSFRichTextString(title);
            hssfCell.setCellValue(titleR);
            int valWidth = (title + "").length() * WIDTH_MULT;
            sheet.setColumnWidth(hssfCell.getColumnIndex(), valWidth);
            cellnum++;
        }

    }

    /**
     * methode Write to cell as text :
     * 
     * @param cell
     * @param value
     * @param styleModifier
     */
    private void writeToCellAsText(final HSSFCell cell, final String value, final String styleModifier)
    {
        String newValue = value;
        // format text
        if (value.trim().equals(NBSP))
        {
            newValue = "";
        }
        cell.setCellStyle((HSSFCellStyle) styles.get("textStyle" + styleModifier));
        fixWidthAndPopulateText(cell, newValue);
    }

    /**
     * methode Affect style pourcent :
     * 
     * @param pCell le cell
     * @param pkeyStyle
     */
    private void affectStylePourcent(final HSSFCell pCell, final Object pkeyStyle)
    {
        HSSFCellStyle style = stylesPourcents.get(pkeyStyle);
        if (style != null)
        {
            pCell.setCellStyle(style);
        }
    }

    /**
     * methode Affect style numeric :
     * 
     * @param pCell le cell
     * @param pkeyStyle
     */
    private void affectStyleNumeric(final HSSFCell pCell, final Object pkeyStyle)
    {
        HSSFCellStyle style = stylesNumerics.get(pkeyStyle);
        if (style != null)
        {
            pCell.setCellStyle(style);
        }
    }

    /**
     * methode Affect style date :
     * 
     * @param pCell le cell
     * @param pkeyStyle
     */
    private void affectStyleDate(final HSSFCell pCell, final Object pkeyStyle)
    {
        HSSFCellStyle style = stylesDates.get(pkeyStyle);
        if (style != null)
        {
            pCell.setCellStyle(style);
        }
    }

    /**
     * methode Affect style boolean :
     * 
     * @param pCell le cell
     * @param pkeyStyle
     */
    private void affectStyleBoolean(final HSSFCell pCell, final Object pkeyStyle)
    {
        HSSFCellStyle style = stylesBooleans.get(pkeyStyle);
        if (style != null)
        {
            pCell.setCellStyle(style);
        }
    }

    /**
     * Verifie si numeric.
     * 
     * @param value
     * @return true, si c'est numeric
     */
    private static final boolean isNumeric(Object value)
    {
        return value instanceof Number;
    }

    /**
     * Verifie si date.
     * 
     * @param value
     * @return true, si c'est date
     */
    private static final boolean isDate(Object value)
    {
        return value instanceof Date;
    }

    /**
     * Verifie si boolean.
     * 
     * @param value
     * @return true, si c'est boolean
     */
    private static final boolean isBoolean(Object value)
    {
        return value instanceof Boolean;
    }

    /**
     * methode Write to cell formatted :
     * 
     * @param cell
     * @param value
     * @param propertyValue
     * @param cellType
     * @param styleModifier
     */
    private void writeToCellFormatted(HSSFCell cell, String value, Object propertyValue, String cellType,
        String styleModifier)
    {
        String newValue = value;
        if (stylesNumerics.containsKey(cellType) && isNumeric(propertyValue))
        {
            affectStyleNumeric(cell, cellType);
            fixWidthAndPopulateNumeric(cell, (Number) propertyValue);
        }
        else if (stylesDates.containsKey(cellType) && isDate(propertyValue))
        {
            affectStyleDate(cell, cellType);
            fixWidthAndPopulateDate(cell, (Date) propertyValue);
        }
        else if (stylesBooleans.containsKey(cellType) && isBoolean(propertyValue))
        {
            affectStyleBoolean(cell, cellType);
            fixWidthAndPopulateBoolean(cell, value);
        }
        else if (stylesPourcents.containsKey(cellType) && isNumeric(propertyValue))
        {
            affectStylePourcent(cell, cellType);
            fixWidthAndPopulatePourcent(cell, (Number) propertyValue);
        }
        else
        {
            if (value.equals(NBSP))
            {
                newValue = "";
            }
            cell.setCellStyle((HSSFCellStyle) styles.get("textStyle" + styleModifier));
            fixWidthAndPopulateText(cell, newValue);
        }
    }

    /**
     * methode Fix width and populate pourcent :
     * 
     * @param cell
     * @param value
     */
    private void fixWidthAndPopulatePourcent(HSSFCell cell, Number value)
    {
        int valWidth;

        BigDecimal propValue = new BigDecimal(value.toString());
        propValue = propValue.divide(new BigDecimal("100"), BigDecimal.ROUND_HALF_UP);

        cell.setCellValue(propValue.doubleValue());
        valWidth = (cell.getNumericCellValue() + "% .").length() * WIDTH_MULT;

        if (valWidth > sheet.getColumnWidth(cell.getColumnIndex()))
        {
            sheet.setColumnWidth(cell.getColumnIndex(), valWidth);
        }
    }

    /**
     * methode Fix width and populate numeric :
     * 
     * @param cell
     * @param value
     */
    private void fixWidthAndPopulateNumeric(HSSFCell cell, Number value)
    {
        int valWidth;

        cell.setCellValue(value.doubleValue());
        valWidth = (cell.getNumericCellValue() + " .").length() * WIDTH_MULT;

        if (valWidth > sheet.getColumnWidth(cell.getColumnIndex()))
        {
            sheet.setColumnWidth(cell.getColumnIndex(), valWidth);
        }
    }

    /**
     * methode Fix width and populate date :
     * 
     * @param cell
     * @param value
     */
    private void fixWidthAndPopulateDate(HSSFCell cell, Date value)
    {
        int valWidth;

        cell.setCellValue(value);
        valWidth = (cell.getDateCellValue() + "").length() * WIDTH_MULT;

        if (valWidth > sheet.getColumnWidth(cell.getColumnIndex()))
        {
            sheet.setColumnWidth(cell.getColumnIndex(), valWidth);
        }
    }

    /**
     * methode Fix width and populate boolean :
     * 
     * @param cell
     * @param value
     */
    private void fixWidthAndPopulateBoolean(HSSFCell cell, String value)
    {
        int valWidth;
        HSSFRichTextString valueR = new HSSFRichTextString(value);
        cell.setCellValue(valueR);

        valWidth = (cell.getRichStringCellValue().getString()).length() * WIDTH_MULT;

        if (valWidth > sheet.getColumnWidth(cell.getColumnIndex()))
        {
            sheet.setColumnWidth(cell.getColumnIndex(), valWidth);
        }
    }

    /**
     * methode Fix width and populate text :
     * 
     * @param cell
     * @param value
     */
    private void fixWidthAndPopulateText(HSSFCell cell, String value)
    {
        int valWidth;
        HSSFRichTextString valueR = new HSSFRichTextString(value);
        cell.setCellValue(valueR);
        valWidth = (cell.getRichStringCellValue().getString()).length() * WIDTH_MULT;

        if (valWidth < (WIDTH_MULT * MIN_CHARS))
        {
            valWidth = WIDTH_MULT * MIN_CHARS;
        }

        if (valWidth > (WIDTH_MULT * MAX_CHARS))
        {
            valWidth = WIDTH_MULT * MAX_CHARS;
        }

        if (valWidth > sheet.getColumnWidth(cell.getColumnIndex()))
        {
            sheet.setColumnWidth(cell.getColumnIndex(), valWidth);
        }

    }

    /**
     * methode Inits the font bold :
     * 
     * @param workbook
     * @param fontHeight
     * @return hSSF font
     */
    private HSSFFont initFontBold(HSSFWorkbook workbook, short fontHeight)
    {
        HSSFFont uneFontBold = workbook.createFont();
        uneFontBold.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        uneFontBold.setColor(HSSFColor.BLACK.index);
        uneFontBold.setFontName(HSSFFont.FONT_ARIAL);
        uneFontBold.setFontHeightInPoints(fontHeight);

        return uneFontBold;
    }

    /**
     * methode Inits the font :
     * 
     * @param workbook
     * @param fontHeight
     * @return hSSF font
     */
    private HSSFFont initFont(HSSFWorkbook workbook, short fontHeight)
    {
        HSSFFont uneFont = workbook.createFont();
        uneFont.setBoldweight(HSSFFont.BOLDWEIGHT_NORMAL);
        uneFont.setColor(HSSFColor.BLACK.index);
        uneFont.setFontName(HSSFFont.FONT_ARIAL);
        uneFont.setFontHeightInPoints(fontHeight);

        return uneFont;
    }

    /**
     * methode Creates the boolean hssf style :
     * 
     * @param workbook
     * @param font
     * @return hSSF cell style
     */
    private HSSFCellStyle createBooleanHSSFStyle(HSSFWorkbook workbook, HSSFFont font)
    {
        HSSFCellStyle result = workbook.createCellStyle();

        result.setFont(font);
        result.setAlignment(HSSFCellStyle.ALIGN_CENTER);
        return result;
    }

    /**
     * methode Inits the styles booleans :
     * 
     * @param workbook
     * @param font
     * @return map
     */
    private Map<String, HSSFCellStyle> initStylesBooleans(HSSFWorkbook workbook, HSSFFont font)
    {
        Map<String, HSSFCellStyle> result = new HashMap<>();
        result.put(C_BOO_OUI_NON, createBooleanHSSFStyle(workbook, font));
        result.put(C_BOO_O_N, createBooleanHSSFStyle(workbook, font));
        result.put(C_BOO_VRAI_FAUX, createBooleanHSSFStyle(workbook, font));
        result.put(C_BOO_V_F, createBooleanHSSFStyle(workbook, font));
        result.put(C_BOO_1_0, createBooleanHSSFStyle(workbook, font));
        return result;
    }

    /**
     * methode Creates the pourcent hssf style :
     * 
     * @param workbook
     * @param font
     * @param typeFormatage
     * @return hSSF cell style
     */
    private HSSFCellStyle createPourcentHSSFStyle(HSSFWorkbook workbook, HSSFFont font, String typeFormatage)
    {
        HSSFCellStyle result = workbook.createCellStyle();

        result.setFont(font);
        result.setAlignment(HSSFCellStyle.ALIGN_RIGHT);
        result.setDataFormat(format.getFormat(typeFormatage));

        return result;
    }

    /**
     * methode Inits the styles pourcents :
     * 
     * @param workbook
     * @param font
     * @return map
     */
    private Map<String, HSSFCellStyle> initStylesPourcents(HSSFWorkbook workbook, HSSFFont font)
    {
        Map<String, HSSFCellStyle> result = new HashMap<>();

        // Formatage des pourcentages
        result.put(C_PCT_ZERO_DEC, createPourcentHSSFStyle(workbook, font, F_PCT_ZERO_DEC));
        result.put(C_PCT_UNE_DEC, createPourcentHSSFStyle(workbook, font, F_PCT_UNE_DEC));
        result.put(C_PCT_DEUX_DEC, createPourcentHSSFStyle(workbook, font, F_PCT_DEUX_DEC));
        result.put(C_PCT_TROIS_DEC, createPourcentHSSFStyle(workbook, font, F_PCT_TROIS_DEC));
        result.put(C_PCT_QUATRE_DEC, createPourcentHSSFStyle(workbook, font, F_PCT_QUATRE_DEC));
        result.put(C_PCT_CINQ_DEC, createPourcentHSSFStyle(workbook, font, F_PCT_CINQ_DEC));
        result.put(C_PCT_SIX_DEC, createPourcentHSSFStyle(workbook, font, F_PCT_SIX_DEC));
        result.put(C_PCT_SEPT_DEC, createPourcentHSSFStyle(workbook, font, F_PCT_SEPT_DEC));
        result.put(C_PCT_HUIT_DEC, createPourcentHSSFStyle(workbook, font, F_PCT_HUIT_DEC));
        result.put(C_PCT_NEUF_DEC, createPourcentHSSFStyle(workbook, font, F_PCT_NEUF_DEC));
        result.put(C_PCT_DIX_DEC, createPourcentHSSFStyle(workbook, font, F_PCT_DIX_DEC));
        return result;
    }

    /**
     * methode Creates the numeric hssf style :
     * 
     * @param workbook
     * @param font
     * @param typeFormatage
     * @return hSSF cell style
     */
    private HSSFCellStyle createNumericHSSFStyle(HSSFWorkbook workbook, HSSFFont font, String typeFormatage)
    {
        HSSFCellStyle result = workbook.createCellStyle();

        result.setFont(font);
        result.setAlignment(HSSFCellStyle.ALIGN_RIGHT);
        result.setDataFormat(format.getFormat(typeFormatage));

        return result;
    }

    /**
     * methode Inits the styles numerics :
     * 
     * @param workbook
     * @param font
     * @return map
     */
    private Map<String, HSSFCellStyle> initStylesNumerics(HSSFWorkbook workbook, HSSFFont font)
    {
        Map<String, HSSFCellStyle> result = new HashMap<>();

        // Formatage des monnaies
        result.put(C_MNE_ZERO_DEC, createNumericHSSFStyle(workbook, font, F_MNE_ZERO_DEC));
        result.put(C_MNE_UNE_DEC, createNumericHSSFStyle(workbook, font, F_MNE_UNE_DEC));
        result.put(C_MNE_DEUX_DEC, createNumericHSSFStyle(workbook, font, F_MNE_DEUX_DEC));
        result.put(C_MNE_TROIS_DEC, createNumericHSSFStyle(workbook, font, F_MNE_TROIS_DEC));
        result.put(C_MNE_QUATRE_DEC, createNumericHSSFStyle(workbook, font, F_MNE_QUATRE_DEC));
        result.put(C_MNE_CINQ_DEC, createNumericHSSFStyle(workbook, font, F_MNE_CINQ_DEC));
        result.put(C_MNE_SIX_DEC, createNumericHSSFStyle(workbook, font, F_MNE_SIX_DEC));
        result.put(C_MNE_SEPT_DEC, createNumericHSSFStyle(workbook, font, F_MNE_SEPT_DEC));
        result.put(C_MNE_HUIT_DEC, createNumericHSSFStyle(workbook, font, F_MNE_HUIT_DEC));
        result.put(C_MNE_NEUF_DEC, createNumericHSSFStyle(workbook, font, F_MNE_NEUF_DEC));
        result.put(C_MNE_DIX_DEC, createNumericHSSFStyle(workbook, font, F_MNE_DIX_DEC));

        // Formatage des nombres avec séparateurs de milliers
        result.put(C_NB_ZERO_DEC, createNumericHSSFStyle(workbook, font, F_NB_ZERO_DEC));
        result.put(C_NB_UNE_DEC, createNumericHSSFStyle(workbook, font, F_NB_UNE_DEC));
        result.put(C_NB_DEUX_DEC, createNumericHSSFStyle(workbook, font, F_NB_DEUX_DEC));
        result.put(C_NB_TROIS_DEC, createNumericHSSFStyle(workbook, font, F_NB_TROIS_DEC));
        result.put(C_NB_QUATRE_DEC, createNumericHSSFStyle(workbook, font, F_NB_QUATRE_DEC));
        result.put(C_NB_CINQ_DEC, createNumericHSSFStyle(workbook, font, F_NB_CINQ_DEC));
        result.put(C_NB_SIX_DEC, createNumericHSSFStyle(workbook, font, F_NB_SIX_DEC));
        result.put(C_NB_SEPT_DEC, createNumericHSSFStyle(workbook, font, F_NB_SEPT_DEC));
        result.put(C_NB_HUIT_DEC, createNumericHSSFStyle(workbook, font, F_NB_HUIT_DEC));
        result.put(C_NB_NEUF_DEC, createNumericHSSFStyle(workbook, font, F_NB_NEUF_DEC));
        result.put(C_NB_DIX_DEC, createNumericHSSFStyle(workbook, font, F_NB_DIX_DEC));

        // Formatage des nombres sans séparateurs de milliers
        result.put(C_NB_ZERODECNOSEP, createNumericHSSFStyle(workbook, font, F_NB_ZERODECNOSEP));

        return result;
    }

    /**
     * methode Creates the date hssf style :
     * 
     * @param workbook
     * @param font
     * @param typeFormatage
     * @return hSSF cell style
     */
    private HSSFCellStyle createDateHSSFStyle(HSSFWorkbook workbook, HSSFFont font, String typeFormatage)
    {
        HSSFCellStyle result = workbook.createCellStyle();

        result.setFont(font);
        result.setAlignment(HSSFCellStyle.ALIGN_CENTER);
        result.setDataFormat(format.getFormat(typeFormatage));

        return result;
    }

    /**
     * methode Inits the styles dates :
     * 
     * @param workbook
     * @param font
     * @return map
     */
    private Map<String, HSSFCellStyle> initStylesDates(HSSFWorkbook workbook, HSSFFont font)
    {
        Map<String, HSSFCellStyle> result = new HashMap<>();

        // Formatage des dates et heures
        result.put(C_DA, createDateHSSFStyle(workbook, font, F_DA));
        result.put(C_DA_HE, createDateHSSFStyle(workbook, font, F_DA_HE));
        result.put(C_DA_HE_MI, createDateHSSFStyle(workbook, font, F_DA_HE_MI));
        result.put(C_DA_HE_MI_S, createDateHSSFStyle(workbook, font, F_DA_HE_MI_S));
        result.put(C_HE, createDateHSSFStyle(workbook, font, F_HE));
        result.put(C_HE_MI, createDateHSSFStyle(workbook, font, F_HE_MI));
        result.put(C_HE_MI_S, createDateHSSFStyle(workbook, font, F_HE_MI_S));

        return result;
    }

    /**
     * methode Inits the styles :
     * 
     * @param workbook
     * @param font
     * @param fontBold
     * @return map
     */
    private Map<String, HSSFCellStyle> initStyles(HSSFWorkbook workbook, HSSFFont font, HSSFFont fontBold)
    {
        Map<String, HSSFCellStyle> result = new HashMap<>();

        HSSFCellStyle titleStyle = workbook.createCellStyle();
        HSSFCellStyle textStyle = workbook.createCellStyle();
        HSSFCellStyle boldStyle = workbook.createCellStyle();

        // Add to export totals
        HSSFCellStyle moneyStyleTotals = workbook.createCellStyle();
        HSSFCellStyle naStyleTotals = workbook.createCellStyle();
        HSSFCellStyle numericStyleTotals = workbook.createCellStyle();
        HSSFCellStyle percentStyleTotals = workbook.createCellStyle();
        HSSFCellStyle textStyleTotals = workbook.createCellStyle();

        result.put("titleStyle", titleStyle);
        result.put("textStyle", textStyle);
        result.put("boldStyle", boldStyle);

        // Add to export totals
        result.put("moneyStyle_Totals", moneyStyleTotals);
        result.put("naStyle_Totals", naStyleTotals);
        result.put("numericStyle_Totals", numericStyleTotals);
        result.put("percentStyle_Totals", percentStyleTotals);
        result.put("textStyle_Totals", textStyleTotals);

        // Title Style
        titleStyle.setFont(fontBold);
        titleStyle.setFillForegroundColor(HSSFColor.PALE_BLUE.index);
        titleStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        titleStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        titleStyle.setBottomBorderColor(HSSFColor.BLACK.index);
        titleStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        titleStyle.setLeftBorderColor(HSSFColor.BLACK.index);
        titleStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
        titleStyle.setRightBorderColor(HSSFColor.BLACK.index);
        titleStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);
        titleStyle.setTopBorderColor(HSSFColor.BLACK.index);
        titleStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
        titleStyle.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);

        // Standard Text Style
        textStyle.setFont(font);
        textStyle.setWrapText(true);

        return result;
    }

    /**
     * methode Checks if is include title :
     * 
     * @param model
     * @return string
     */
    public String isIncludeTitle(TableModel model)
    {
        String includeTitle = "";
        Export export = model.getExportHandler().getCurrentExport();
        if (export != null)
        {
            includeTitle = export.getAttributeAsString(INCLUDE_TITLE);
        }
        return includeTitle;
    }

    /**
     * recuperer la valeur de "group"
     * 
     * @param column
     * @return
     */
    private static String getGroup(Column column)
    {
        return column.getAttributeAsString("group");
    }

    /**
     * recuperer la valeur de "subGroup"
     * 
     * @param column
     * @return
     */
    private static String getSubGroup(Column column)
    {
        return column.getAttributeAsString("subGroup");
    }

}