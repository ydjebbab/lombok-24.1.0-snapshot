/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.apptags.table.calc;

import java.math.BigDecimal;
import java.util.Collection;

import fr.gouv.finances.lombok.apptags.table.bean.Column;
import fr.gouv.finances.lombok.apptags.table.core.TableModel;

/**
 * Class CpPageTotalCalc
 */
public class CpPageTotalCalc implements Calc
{

    /**
     * Constructeur de la classe CpPageTotalCalc.java
     *
     */
    public CpPageTotalCalc()
    {
        super();
        
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.calc.Calc#getCalcResult(fr.gouv.finances.lombok.apptags.table.core.TableModel,
     *      fr.gouv.finances.lombok.apptags.table.bean.Column)
     */
    @Override
    public Number getCalcResult(TableModel model, Column column)
    {
        Collection<Object> rows = model.getCollectionOfPageBeans();

        String property = column.getProperty();
        TotalValue totalValue = new TotalValue();
        CpCalcUtils.eachRowCalcValue(totalValue, rows, property);

        return totalValue.getTotalValue();
    }

    /**
     * Class TotalValue
     */
    private static class TotalValue implements CalcHandler
    {

        /** total. */
        private BigDecimal total = new BigDecimal(0);

        /**
         * (methode de remplacement) {@inheritDoc}
         * 
         * @see fr.gouv.finances.lombok.apptags.table.calc.CalcHandler#processCalcValue(java.lang.Number)
         */
        @Override
        public void processCalcValue(Number calcValue)
        {
            if (calcValue != null)
            {
                total = (total).add(new BigDecimal(String.valueOf(calcValue)));
            }
        }

        /**
         * Accesseur de l attribut total value.
         * 
         * @return total value
         */
        public Number getTotalValue()
        {
            return total;
        }
    }
}
