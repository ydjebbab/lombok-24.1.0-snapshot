/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.apptags.table.tag;

import org.apache.commons.lang.StringUtils;

import fr.gouv.finances.lombok.apptags.table.bean.Export;
import fr.gouv.finances.lombok.apptags.table.core.TableConstants;
import fr.gouv.finances.lombok.apptags.table.core.TableModel;
import fr.gouv.finances.lombok.apptags.table.view.CsvView;
import fr.gouv.finances.lombok.apptags.table.view.html.BuilderConstants;

/**
 * Class CpExportCsvTag
 */
public class CpExportCsvTag extends ExportTag
{

    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** delimiter. */
    private String delimiter;

    /** actionedition. */
    private String actionedition;

    /**
     * Instanciation de cp export csv tag.
     */
    public CpExportCsvTag()
    {
        super();
    }

    /**
     * Accesseur de l attribut delimiter.
     * 
     * @return delimiter
     */
    public String getDelimiter()
    {
        return TagUtils.evaluateExpressionAsString("delimiter", delimiter, this, pageContext);
    }

    /**
     * Accesseur de l attribut actionedition.
     * 
     * @return actionedition
     */
    public String getActionedition()
    {
        return TagUtils.evaluateExpressionAsString("actionedition", actionedition, this, pageContext);
    }

    /**
     * Modificateur de l attribut delimiter.
     * 
     * @param delimiter le nouveau delimiter
     * jsp.attribute :  description="What to use as the file delimiter. The default is a comma." required="false"
     *                rtexprvalue="true"
     */
    public void setDelimiter(String delimiter)
    {
        this.delimiter = delimiter;
    }

    /**
     * Modificateur de l attribut actionedition.
     * 
     * @param actionedition le nouveau actionedition
     */
    public void setActionedition(String actionedition)
    {
        this.actionedition = actionedition;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.tag.ExportTag#addExportAttributes(fr.gouv.finances.lombok.apptags.table.core.TableModel,
     *      fr.gouv.finances.lombok.apptags.table.bean.Export)
     */
    @Override
    public void addExportAttributes(TableModel model, Export export)
    {
        if (StringUtils.isBlank(export.getView()))
        {
            export.setView(TableConstants.VIEW_CSV);
        }

        if (StringUtils.isBlank(export.getViewResolver()))
        {
            export.setViewResolver(TableConstants.VIEW_CSV);
        }

        if (StringUtils.isBlank(export.getImageName()))
        {
            export.setImageName(TableConstants.VIEW_CSV);
        }

        if (StringUtils.isBlank(export.getText()))
        {
            export.setText(BuilderConstants.TOOLBAR_CSV_TEXT);
        }

        export.addAttribute(CsvView.DELIMITER, TagUtils.evaluateExpressionAsString("delimiter", delimiter, this,
            pageContext));
        export.addAttribute("actionedition", TagUtils.evaluateExpressionAsString("actionedition", actionedition, this,
            pageContext));
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.tag.ExportTag#release()
     */
    @Override
    public void release()
    {
        delimiter = null;
        super.release();
    }

}
