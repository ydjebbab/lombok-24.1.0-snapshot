/*
/*Copyright (c) 2014 DGFiP - Tous droits réservés/* * Copyright 2004 original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package fr.gouv.finances.lombok.apptags.table.bean;

import fr.gouv.finances.lombok.apptags.table.core.TableModel;

/**
 * Class Export
 */
public class Export extends Attributes
{

    /** model. */
    private TableModel model;

    /** encoding. */
    private String encoding;

    /** file name. */
    private String fileName;

    /** image name. */
    private String imageName;

    /** interceptor. */
    private String interceptor;

    /** view. */
    private String view;

    /** view resolver. */
    private String viewResolver;

    /** text. */
    private String text;

    /** tooltip. */
    private String tooltip;

    /** Id du bean de l'édition à produire. */
    private String editionBeanId;

    /**
     * Instanciation de export.
     *
     * @param model
     */
    public Export(TableModel model)
    {
        this.model = model;
    }

    /**
     * methode Defaults :
     */
    public void defaults()
    {
        this.encoding = ExportDefaults.getEncoding(model, encoding);
        this.text = ExportDefaults.getText(model, text);
        this.tooltip = ExportDefaults.getTooltip(model, tooltip);
        this.viewResolver = ExportDefaults.getviewResolver(model, viewResolver);
    }

    /**
     * Accesseur de l attribut attribute as boolean.
     *
     * @param key
     * @return attribute as boolean
     */
    public boolean getAttributeAsBoolean(String key)
    {
        // ancien code
        // Boolean result = Boolean.parseBoolean(getAttributeAsString(key));
        // return result;

        // nouveau code
        return Boolean.parseBoolean(getAttributeAsString(key));
    }

    /**
     * Gets the id du bean de l'édition à produire.
     *
     * @return the id du bean de l'édition à produire
     */
    public String getEditionBeanId()
    {
        return editionBeanId;
    }

    /**
     * Accesseur de l attribut encoding.
     *
     * @return encoding
     */
    public String getEncoding()
    {
        return encoding;
    }

    /**
     * Accesseur de l attribut file name.
     *
     * @return file name
     */
    public String getFileName()
    {
        return fileName;
    }

    /**
     * Accesseur de l attribut image name.
     *
     * @return image name
     */
    public String getImageName()
    {
        return imageName;
    }

    /**
     * Accesseur de l attribut interceptor.
     *
     * @return interceptor
     */
    public String getInterceptor()
    {
        return interceptor;
    }

    /**
     * Accesseur de l attribut text.
     *
     * @return text
     */
    public String getText()
    {
        return text;
    }

    /**
     * Accesseur de l attribut tooltip.
     *
     * @return tooltip
     */
    public String getTooltip()
    {
        return tooltip;
    }

    /**
     * Accesseur de l attribut view.
     *
     * @return view
     */
    public String getView()
    {
        return view;
    }

    /**
     * Accesseur de l attribut view resolver.
     *
     * @return view resolver
     */
    public String getViewResolver()
    {
        return viewResolver;
    }

    /**
     * Sets the id du bean de l'édition à produire.
     *
     * @param editionBeanId the new id du bean de l'édition à produire
     */
    public void setEditionBeanId(String editionBeanId)
    {
        this.editionBeanId = editionBeanId;
    }

    /**
     * Modificateur de l attribut encoding.
     *
     * @param encoding le nouveau encoding
     */
    public void setEncoding(String encoding)
    {
        this.encoding = encoding;
    }

    /**
     * Modificateur de l attribut file name.
     *
     * @param fileName le nouveau file name
     */
    public void setFileName(String fileName)
    {
        this.fileName = fileName;
    }

    /**
     * Modificateur de l attribut image name.
     *
     * @param imageName le nouveau image name
     */
    public void setImageName(String imageName)
    {
        this.imageName = imageName;
    }

    /**
     * Modificateur de l attribut interceptor.
     *
     * @param interceptor le nouveau interceptor
     */
    public void setInterceptor(String interceptor)
    {
        this.interceptor = interceptor;
    }

    /**
     * Modificateur de l attribut text.
     *
     * @param text le nouveau text
     */
    public void setText(String text)
    {
        this.text = text;
    }

    /**
     * Modificateur de l attribut tooltip.
     *
     * @param tooltip le nouveau tooltip
     */
    public void setTooltip(String tooltip)
    {
        this.tooltip = tooltip;
    }

    /**
     * Modificateur de l attribut view.
     *
     * @param view le nouveau view
     */
    public void setView(String view)
    {
        this.view = view;
    }

    /**
     * Modificateur de l attribut view resolver.
     *
     * @param viewResolver le nouveau view resolver
     */
    public void setViewResolver(String viewResolver)
    {
        this.viewResolver = viewResolver;
    }

}
