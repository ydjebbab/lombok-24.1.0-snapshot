/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.apptags.table.calc;

import java.math.BigDecimal;
import java.util.Collection;

import fr.gouv.finances.lombok.apptags.table.bean.Column;
import fr.gouv.finances.lombok.apptags.table.core.TableModel;

/**
 * Class CpDecompteCalc
 */
public class CpDecompteCalc implements Calc
{

    /**
     * Constructeur de la classe CpDecompteCalc.java
     *
     */
    public CpDecompteCalc()
    {
        super();
        
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.calc.Calc#getCalcResult(fr.gouv.finances.lombok.apptags.table.core.TableModel,
     *      fr.gouv.finances.lombok.apptags.table.bean.Column)
     */
    public Number getCalcResult(TableModel model, Column column)
    {
        Collection<Object> rows = model.getCollectionOfFilteredBeans();
        String property = column.getProperty();
        DecompteValue decompteValue = new DecompteValue();
        CpCalcUtils.eachRowCalcValueDecompte(decompteValue, rows, property);

        return decompteValue.getDecompteValue();
    }

    /**
     * Class DecompteValue
     */
    private static class DecompteValue implements CalcHandler
    {

        /** decompte. */
        private double decompte = 0;

        /**
         * (methode de remplacement) {@inheritDoc}
         * 
         * @see fr.gouv.finances.lombok.apptags.table.calc.CalcHandler#processCalcValue(java.lang.Number)
         */
        public void processCalcValue(Number calcValue)
        {
            decompte++;
        }

        /**
         * Accesseur de l attribut decompte value.
         * 
         * @return decompte value
         */
        public Number getDecompteValue()
        {
            return new BigDecimal(decompte);
        }
    }
}
