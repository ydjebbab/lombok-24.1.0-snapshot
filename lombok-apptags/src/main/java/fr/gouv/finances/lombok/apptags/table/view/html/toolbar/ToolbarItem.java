/*
/*Copyright (c) 2014 DGFiP - Tous droits réservés/* * Copyright 2004 original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package fr.gouv.finances.lombok.apptags.table.view.html.toolbar;

import fr.gouv.finances.lombok.apptags.table.core.TableModel;
import fr.gouv.finances.lombok.apptags.util.HtmlBuilder;

/**
 * Interface ToolbarItem
 */
public interface ToolbarItem
{

    /**
     * Accesseur de l attribut action.
     * 
     * @return action
     */
    public String getAction();

    /**
     * Modificateur de l attribut action.
     * 
     * @param action le nouveau action
     */
    public void setAction(String action);

    /**
     * Accesseur de l attribut tooltip.
     * 
     * @return tooltip
     */
    public String getTooltip();

    /**
     * Modificateur de l attribut tooltip.
     * 
     * @param tooltip le nouveau tooltip
     */
    public void setTooltip(String tooltip);

    /**
     * Accesseur de l attribut onmouseout.
     * 
     * @return onmouseout
     */
    public String getOnmouseout();

    /**
     * Modificateur de l attribut onmouseout.
     * 
     * @param onmouseout le nouveau onmouseout
     */
    public void setOnmouseout(String onmouseout);

    /**
     * Accesseur de l attribut onmouseover.
     * 
     * @return onmouseover
     */
    public String getOnmouseover();

    /**
     * Modificateur de l attribut onmouseover.
     * 
     * @param onmouseover le nouveau onmouseover
     */
    public void setOnmouseover(String onmouseover);

    /**
     * Accesseur de l attribut style.
     * 
     * @return style
     */
    public String getStyle();

    /**
     * Modificateur de l attribut style.
     * 
     * @param style le nouveau style
     */
    public void setStyle(String style);

    /**
     * Accesseur de l attribut style class.
     * 
     * @return style class
     */
    public String getStyleClass();

    /**
     * Modificateur de l attribut style class.
     * 
     * @param styleClass le nouveau style class
     */
    public void setStyleClass(String styleClass);

    /**
     * methode Disabled :
     * 
     * @param html
     */
    public void disabled(HtmlBuilder html);

    /**
     * methode Enabled :
     * 
     * @param html
     * @param model
     */
    public void enabled(HtmlBuilder html, TableModel model);
}
