/*
 * Copyright (c) 2014 DGFiP - Tous droits réservés
 *//*
/*Copyright (c) 2014 DGFiP - Tous droits réservés/* * Copyright 2004 original author or authors.
  * 
  * Licensed under the Apache License, Version 2.0 (the "License"); you may not
  * use this file except in compliance with the License. You may obtain a copy of
  * the License at
  * 
  * http://www.apache.org/licenses/LICENSE-2.0
  * 
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
  * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
  * License for the specific language governing permissions and limitations under
  * the License.
  */
package fr.gouv.finances.lombok.apptags.table.view.html;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import fr.gouv.finances.lombok.apptags.table.bean.Column;
import fr.gouv.finances.lombok.apptags.table.bean.Table;
import fr.gouv.finances.lombok.apptags.table.core.TableModel;
import fr.gouv.finances.lombok.apptags.table.view.CpStatusBar;
import fr.gouv.finances.lombok.apptags.util.HtmlBuilder;

/**
 * Class TableBuilder
 */
public class TableBuilder
{

    /** html. */
    private HtmlBuilder html;

    /** model. */
    private TableModel model;

    /** table. */
    private Table table;

    /**
     * Instanciation de table builder.
     * 
     * @param model
     */
    public TableBuilder(TableModel model)
    {
        this(new HtmlBuilder(), model);
    }

    /**
     * Instanciation de table builder.
     * 
     * @param html
     * @param model
     */
    public TableBuilder(HtmlBuilder html, TableModel model)
    {
        this.html = html;
        this.model = model;
        this.table = model.getTableHandler().getTable();
    }

    /**
     * Accesseur de l attribut html builder.
     * 
     * @return html builder
     */
    public HtmlBuilder getHtmlBuilder()
    {
        return html;
    }

    /**
     * Accesseur de l attribut table model.
     * 
     * @return table model
     */
    protected TableModel getTableModel()
    {
        return model;
    }

    /**
     * Accesseur de l attribut table.
     * 
     * @return table
     */
    protected Table getTable()
    {
        return table;
    }

    /**
     * methode Table start :
     */
    public void tableStart()
    {
        html.tablePres(0);
        id();
        border();
        cellSpacing();
        cellPadding();
        styleClass();
        dataAttributes();
        style();
        if (!table.isShowResponsive())
        {
            width();
        }
        html.close();
    }

    /**
     * methode Table start :
     */
    public void tableStart(boolean data)
    {

        if (data)
        {
            html.table(0);
            idData();
        }
        else
        {
            html.tablePres(0);
            idFilter();
        }
        border();
        cellSpacing();
        cellPadding();
        styleClass();
        dataAttributes();
        style();
        if (!table.isShowResponsive())
        {
            width();
        }
        html.close();
    }

    /**
     * methode Table end :
     */
    public void tableEnd()
    {
        html.tableEnd(0);

    }

    /**
     * methode Id :
     */
    public void id()
    {
        html.id(model.getTableHandler().prefixWithTableId() + BuilderConstants.TABLE);
    }

    /**
     * methode IdDATA :
     */
    public void idData()
    {
        html.id(model.getTableHandler().prefixWithTableId() + BuilderConstants.TABLEDATA);
    }

    /**
     * methode IdFILTER :
     */
    public void idFilter()
    {
        html.id(model.getTableHandler().prefixWithTableId() + BuilderConstants.TABLEFILTER);
    }

    /**
     * methode Border :
     */
    public void border()
    {
        String border = table.getBorder();
        html.border(border);
    }

    /**
     * methode Cell spacing :
     */
    public void cellSpacing()
    {
        String cellSpacing = table.getCellspacing();
        html.cellSpacing(cellSpacing);
    }

    /**
     * methode Cell padding :
     */
    public void cellPadding()
    {
        String cellPadding = table.getCellpadding();
        html.cellPadding(cellPadding);
    }

    /**
     * methode Style class :
     */
    public void styleClass()
    {
        String styleClass = table.getStyleClass();
        html.styleClass(styleClass);
    }

    /**
     * methode data Attributes :
     */
    public void dataAttributes()
    {
        String dataAttributes = table.getDataAttributes();
        html.dataAttributes(dataAttributes);
    }

    /**
     * methode Style :
     */
    public void style()
    {
        String style = table.getStyle();
        html.style(style);
    }

    /**
     * methode Width :
     */
    public void width()
    {
        if (!table.isShowResponsive())
        {
            String width = table.getWidth();
            html.width(width);
        }
    }

    /**
     * methode Title :
     */
    public void title()
    {
        boolean showTitle = BuilderUtils.showTitle(model);
        if (showTitle)
        {
            String title = model.getTableHandler().getTable().getTitle();
            if (StringUtils.isNotBlank(title))
            {
                html.span().styleClass(BuilderConstants.TITLE_CSS).close().append(title).spanEnd();
            }
        }
    }

    /**
     * methode Title row span columns :
     */
    public void titleRowSpanColumns()
    {
        boolean showTitle = BuilderUtils.showTitle(model);
        if (showTitle)
        {
            String title = model.getTableHandler().getTable().getTitle();
            if (StringUtils.isNotBlank(title))
            {
                int columnCount = model.getColumnHandler().columnCount();
                html.tr(1).styleClass(BuilderConstants.TITLE_ROW_CSS).close();
                html.td(2).colSpan("" + columnCount).close();
                html.span().close().append(title).spanEnd();
                html.tdEnd();
                html.trEnd(1);
            }
        }
    }

    /**
     * methode Header row :
     */
    public void headerRow()
    {
        tableStart(true);
        html.thead(0).close();
        html.tr(1).close();

        List<Column> columns = model.getColumnHandler().getHeaderColumns();

        for (Column column : columns)
        {
            html.append(column.getCellDisplay());
        }

        html.trEnd(1);
        html.theadEnd(0);
    }

    /**
     * methode Filter row :
     */
    public void filterRow()
    {
        html.div().styleClass("filterButtons").close();
        html.fieldset().close();
        html.legend().styleClass("sr-only").close();
        html.append("Boutons filtrer et effacer gérant la liste de filtres ci-après");
        html.legendEnd();
        ToolbarBuilder toolbarBuilder = new ToolbarBuilder(html, model);
        toolbarBuilder.filterItemAsButton();
        html.nbsp();
        toolbarBuilder.clearItemAsButton();
        html.fieldsetEnd();
        html.divEnd();

        tableStart(false);
        
        html.tr(1).styleClass(BuilderConstants.FILTER_CSS+" inputfiltres").id("tr_filter").close();
        List<Column> columns = model.getColumnHandler().getFilterColumns();
        for (Column column : columns)
        {
            html.append(column.getCellDisplay());
        }
        html.trEnd(1);
        html.tableEnd(0);       

    }

    /**
     * methode Filter row :
     */
    public void statusRow()
    {
       html.div().styleClass("statusBar").close(); 
       tableStart(); 
       new CpStatusBar(html, model).layout();
       html.tableEnd(0); 
       html.divEnd();
    }

    /**
     * methode Thead start :
     */
    public void theadStart()
    {
        html.thead(1).close();
    }

    /**
     * methode Thead end :
     */
    public void theadEnd()
    {
        html.theadEnd(1);
    }

    /**
     * methode Tbody start :
     */
    public void tbodyStart()
    {
        // amlp: 09/09 : rajout de la clause if pour
        // eviter balise tbody vide si pas d elements
        // dans le tableau

        if (model.getLimit().getTotalRows() > 0)
        {
            html.tbody(1).styleClass(BuilderConstants.TABLE_BODY_CSS).close();
        }
        afficherMessagePasElementTrouveDansPremiereLigneTableau();
    }

    /**
     * methode Tbody end :
     */
    public void tbodyEnd()
    {
        // amlp: 09/09 : rajout de la clause if pour
        // eviter balise tbody vide si pas d elements
        // dans le tableau
        if (model.getLimit().getTotalRows() > 0)
        {
            html.tbodyEnd(1);
        }

    }

    /**
     * methode Theme start :
     */
    public void themeStart()
    {
        html.newline();
        String theme = model.getTableHandler().getTable().getTheme();
        html.div().styleClass(theme);
        html.close();
    }

    /**
     * methode Theme end :
     */
    public void themeEnd()
    {
        html.newline();
        html.divEnd();
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see java.lang.Object#toString()
     */
    public String toString()
    {
        return html.toString();
    }

    /**
     * methode Afficher message pas element trouve dans premiere ligne tableau :
     */
    private void afficherMessagePasElementTrouveDansPremiereLigneTableau()
    {
        if (model.getLimit().getTotalRows() == 0)
        {
            if (model.getTableHandler().getTable().getPositionnoresultsfound() != null
                && model.getTableHandler().getTable().getPositionnoresultsfound().compareToIgnoreCase("body") == 0)
            {
                html.tr(1).styleClass(BuilderConstants.STATUSBARNORESULTSFOUND_CSS).close();

                html.td(1).colSpan(String.valueOf(model.getColumnHandler().columnCount())).styleClass(
                    BuilderConstants.STATUSBAR_NO_RESULTS_FOUND).close();
                html.append(model.getTableHandler().getTable().getLibellenoresultsfound());

                html.tdEnd();

                html.trEnd(1);
            }
        }
    }
}
