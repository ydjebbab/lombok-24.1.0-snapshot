/*
/*Copyright (c) 2014 DGFiP - Tous droits réservés/* * Copyright 2004 original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package fr.gouv.finances.lombok.apptags.table.view.html.toolbar;

import fr.gouv.finances.lombok.apptags.table.core.TableModel;
import fr.gouv.finances.lombok.apptags.util.HtmlBuilder;

/**
 * Class TextItem
 */
public class TextItem extends AbstractItem implements ToolbarItem
{

    /** text. */
    private String text;

    /**
     * Constructeur de la classe TextItem.java
     *
     */
    public TextItem()
    {
        super();
        
    }

    /**
     * Accesseur de l attribut text.
     * 
     * @return text
     */
    public String getText()
    {
        return text;
    }

    /**
     * Modificateur de l attribut text.
     * 
     * @param text le nouveau text
     */
    public void setText(String text)
    {
        this.text = text;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.view.html.toolbar.AbstractItem#disabled(fr.gouv.finances.lombok.apptags.util.HtmlBuilder)
     */
    @Override
    public void disabled(HtmlBuilder html)
    {
        html.span().close().append(getText()).spanEnd();
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.view.html.toolbar.AbstractItem#enabled(fr.gouv.finances.lombok.apptags.util.HtmlBuilder,
     *      fr.gouv.finances.lombok.apptags.table.core.TableModel)
     */
    @Override
    public void enabled(HtmlBuilder html, TableModel model)
    {
        html.a();
        html.quote();
        html.append(getAction());
        html.quote().close();

        boolean showTooltips = model.getTableHandler().getTable().isShowTooltips();
        if (showTooltips)
        {
            html.span().title(getTooltip());
            html.styleClass(getStyleClass()).style(getStyle());
            html.onmouseover(getOnmouseover()).onmouseout(getOnmouseout());
            html.close();
            html.append(getText());
            html.spanEnd();
        }
        else
        {
            html.span();
            html.styleClass(getStyleClass()).style(getStyle());
            html.onmouseover(getOnmouseover()).onmouseout(getOnmouseout());
            html.close();
            html.append(getText());
            html.spanEnd();
        }

        html.aEnd();
    }
}
