/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.apptags.table.callback;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.apache.commons.beanutils.BeanComparator;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.ListUtils;
import org.apache.commons.collections.Predicate;
import org.apache.commons.collections.comparators.ReverseComparator;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import fr.gouv.finances.lombok.apptags.table.core.TableConstants;
import fr.gouv.finances.lombok.apptags.table.core.TableModel;
import fr.gouv.finances.lombok.apptags.table.limit.Sort;
import fr.gouv.finances.lombok.apptags.util.comparators.NullorNotComparableAlphabeticalComparator;
import fr.gouv.finances.lombok.util.comparators.NullSafeBeanComparator;

/**
 * Class CpProcessRowsCallBackWithIgnoreCase
 */
public class CpProcessRowsCallBackWithIgnoreCase implements SortRowsCallback
{

    /** log. */
    private static Log log = LogFactory.getLog(ProcessRowsCallback.class);

    /**
     * Instanciation de cp process rows call back with ignore case.
     */
    public CpProcessRowsCallBackWithIgnoreCase()
    {
        super();
    }

    /**
     * Tri d'un tableau à partir d'une colonne acceptant de trier même les lignes qui ne possèdent pas la propriété
     * affichée dans la colonne de tri.
     * 
     * @param model
     * @param rows
     * @return collection
     */
    @Override
    public Collection<Object> sortRows(TableModel model, Collection<Object> rows)
    {
        Collection<Object> newList;

        boolean sorted = model.getLimit().isSorted();

        if (!sorted)
        {
            newList = rows;
        }
        else
        {
            newList = this.executeSortRows(model, rows);
        }
        return newList;
    }

    /**
     * methode Execute sort rows :
     * 
     * @param model
     * @param rows
     * @return collection
     */
    @SuppressWarnings("unchecked")
    private Collection<Object> executeSortRows(TableModel model, Collection rows)
    {
        Collection<Object> newList;
        Sort sort = model.getLimit().getSort();
        String property = sort.getProperty();
        String sortOrder = sort.getSortOrder();

        Predicate pred = CpContainPropertyPredicate.getInstance(property);

        /*
         * Sous ensemble des beans qui possède la propriété de la colonne
         */
        Collection<Object> rowsWithProperty = CollectionUtils.select(rows, pred);

        /*
         * Sous ensemble des beans qui ne possède pas la propriété de la colonne
         */
        Collection<Object> rowsWithoutProperty = CollectionUtils.subtract(rows, rowsWithProperty);

        newList = rows;

        if (StringUtils.contains(property, "."))
        {
            try
            {
                if (sortOrder.equals(TableConstants.SORT_ASC))
                {
                    Collections.sort((List<Object>) rowsWithProperty,
                        (Comparator<? super Object>) new NullSafeBeanComparator(property,
                            new NullorNotComparableAlphabeticalComparator()));
                    newList = ListUtils.union((List) rowsWithProperty, (List) rowsWithoutProperty);
                }
                else if (sortOrder.equals(TableConstants.SORT_DESC))
                {
                    NullSafeBeanComparator reversedNaturalOrderBeanComparator =
                        new NullSafeBeanComparator(property, new ReverseComparator(new NullorNotComparableAlphabeticalComparator()));
                    Collections.sort((List) rowsWithProperty, reversedNaturalOrderBeanComparator);
                    newList = ListUtils.union((List) rowsWithoutProperty, (List) rowsWithProperty);
                }
            }
            catch (NoClassDefFoundError exception)
            {
                String msg =
                    "LA PROPRIETE DE LA COLONNE[" + property
                        + "] NECESSITE BeanUtils 1.7 POUR QUE LE TRI SOIT CORRECT.";
                log.error(msg, exception);
                throw new NoClassDefFoundError(msg);
            }
        }
        else
        {
            if (sortOrder.equals(TableConstants.SORT_ASC))
            {
                BeanComparator comparator = new BeanComparator(property, new NullorNotComparableAlphabeticalComparator());
                Collections.sort((List) rowsWithProperty, comparator);
                newList = ListUtils.union((List) rowsWithProperty, (List) rowsWithoutProperty);
            }
            else if (sortOrder.equals(TableConstants.SORT_DESC))
            {
                BeanComparator reversedNaturalOrderBeanComparator =
                    new BeanComparator(property, new ReverseComparator(new NullorNotComparableAlphabeticalComparator()));
                Collections.sort((List) rowsWithProperty, reversedNaturalOrderBeanComparator);
                newList = ListUtils.union((List) rowsWithoutProperty, (List) rowsWithProperty);
            }
        }

        return newList;

    }
}
