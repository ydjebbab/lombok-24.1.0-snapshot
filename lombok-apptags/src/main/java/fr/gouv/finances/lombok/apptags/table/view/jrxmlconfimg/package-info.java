/*
 * Copyright (c) 2017 DGFiP - Tous droits réservés
 * 
*/
// Projet : lombok-apptags
/**
 * Documentation du paquet fr.gouv.finances.lombok.apptags.table.view.jrxmlconfimg
 * @author chouard
 * @version 1.0
 */
package fr.gouv.finances.lombok.apptags.table.view.jrxmlconfimg;