/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.apptags.table.tag;

import javax.servlet.jsp.JspException;

import fr.gouv.finances.lombok.apptags.table.bean.Column;
import fr.gouv.finances.lombok.apptags.table.core.TableModel;

/**
 * Class TwoOrThreeRowHeaderColumnTag
 */
public class TwoOrThreeRowHeaderColumnTag extends ColumnTag
{

    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** rowspan. */
    private String rowspan;

    /** group. */
    private String group;

    /** sub group. */
    private String subGroup;

    /** rowid. */
    private String rowid;

    /**
     * Instanciation de two or three row header column tag.
     */
    public TwoOrThreeRowHeaderColumnTag()
    {
        super();
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.tag.ColumnTag#addColumnAttributes(fr.gouv.finances.lombok.apptags.table.core.TableModel,
     *      fr.gouv.finances.lombok.apptags.table.bean.Column)
     */
    @Override
    public void addColumnAttributes(TableModel model, Column column)
    {
        column.addAttribute("rowspan", getRowspan());
        column.addAttribute("group", group);
        column.addAttribute("subGroup", subGroup);
        column.addAttribute("rowid", rowid);
    }

    /**
     * Accesseur de l attribut rowid.
     * 
     * @return rowid
     */
    public String getRowid()
    {
        return rowid;
    }

    /**
     * Modificateur de l attribut rowid.
     * 
     * @param rowid le nouveau rowid
     */
    public void setRowid(String rowid)
    {
        this.rowid = rowid;
    }

    /**
     * Accesseur de l attribut group.
     * 
     * @return group
     */
    public String getGroup()
    {
        return group;
    }

    /**
     * Modificateur de l attribut group.
     * 
     * @param group le nouveau group
     */
    public void setGroup(String group)
    {
        this.group = group;
    }

    /**
     * Accesseur de l attribut sub group.
     * 
     * @return sub group
     */
    public String getSubGroup()
    {
        return subGroup;
    }

    /**
     * Modificateur de l attribut sub group.
     * 
     * @param subGroup le nouveau sub group
     */
    public void setSubGroup(String subGroup)
    {
        this.subGroup = subGroup;
    }

    /**
     * Accesseur de l attribut rowspan.
     * 
     * @return rowspan
     */
    public String getRowspan()
    {
        String result;
        try
        {
            result =
                (String) EcExpressionEvaluatorManager.evaluate("rowspan", rowspan, String.class, this, pageContext);
        }
        catch (JspException e)
        {
            result = rowspan;
        }
        return result;
    }

    /**
     * Modificateur de l attribut rowspan.
     * 
     * @param rowspan le nouveau rowspan
     */
    public void setRowspan(String rowspan)
    {
        this.rowspan = rowspan;
    }
}
