/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.apptags.table.cell;

/**
 * Class PourCentCellNeufDecimales
 */
public class PourCentCellNeufDecimales extends AbstractPourCentCell
{

    /** Constant : PRECISION. */
    private static final int PRECISION = 9;

    /** Constant : FORMAT_PATTERN. */
    private static final String FORMAT_PATTERN = "#,##0.000000000 '%'";

    /**
     * Instanciation de pour cent cell neuf decimales.
     */
    public PourCentCellNeufDecimales()
    {
        super();
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.cell.AbstractPourCentCell#getPrecision()
     */
    @Override
    public int getPrecision()
    {
        return PRECISION;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.cell.PatternCell#getFormatPattern()
     */
    @Override
    public String getFormatPattern()
    {
        return FORMAT_PATTERN;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.cell.PatternCell#getAlignmentCell()
     */
    @Override
    public int getAlignmentCell()
    {
        return PatternCell.HORIZONTAL_ALIGN_RIGHT;
    }
}
