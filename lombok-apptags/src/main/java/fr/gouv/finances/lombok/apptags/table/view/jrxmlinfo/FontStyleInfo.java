/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.apptags.table.view.jrxmlinfo;

import java.io.Serializable;

/**
 * Class FontStyleInfo
 */
public class FontStyleInfo implements Serializable
{

    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** font name. */
    private String fontName;

    /** bold. */
    private boolean bold;

    /** font size. */
    private int fontSize;

    /** pdf font name. */
    private String pdfFontName;

    /** pdf encoding. */
    private String pdfEncoding;

    /** pdf embedded. */
    private boolean pdfEmbedded;

    /**
     * Instanciation de font style info.
     */
    public FontStyleInfo()
    {
        super();
    }

    /**
     * Verifie si bold.
     * 
     * @return true, si c'est bold
     */
    public boolean isBold()
    {
        return bold;
    }

    /**
     * Modificateur de l attribut bold.
     * 
     * @param bold le nouveau bold
     */
    public void setBold(boolean bold)
    {
        this.bold = bold;
    }

    /**
     * Accesseur de l attribut font name.
     * 
     * @return font name
     */
    public String getFontName()
    {
        return fontName;
    }

    /**
     * Modificateur de l attribut font name.
     * 
     * @param fontName le nouveau font name
     */
    public void setFontName(String fontName)
    {
        this.fontName = fontName;
    }

    /**
     * Accesseur de l attribut font size.
     * 
     * @return font size
     */
    public int getFontSize()
    {
        return fontSize;
    }

    /**
     * Modificateur de l attribut font size.
     * 
     * @param fontSize le nouveau font size
     */
    public void setFontSize(int fontSize)
    {
        this.fontSize = fontSize;
    }

    /**
     * Verifie si pdf embedded.
     * 
     * @return true, si c'est pdf embedded
     */
    public boolean isPdfEmbedded()
    {
        return pdfEmbedded;
    }

    /**
     * Modificateur de l attribut pdf embedded.
     * 
     * @param pdfEmbedded le nouveau pdf embedded
     */
    public void setPdfEmbedded(boolean pdfEmbedded)
    {
        this.pdfEmbedded = pdfEmbedded;
    }

    /**
     * Accesseur de l attribut pdf encoding.
     * 
     * @return pdf encoding
     */
    public String getPdfEncoding()
    {
        return pdfEncoding;
    }

    /**
     * Modificateur de l attribut pdf encoding.
     * 
     * @param pdfEncoding le nouveau pdf encoding
     */
    public void setPdfEncoding(String pdfEncoding)
    {
        this.pdfEncoding = pdfEncoding;
    }

    /**
     * Accesseur de l attribut pdf font name.
     * 
     * @return pdf font name
     */
    public String getPdfFontName()
    {
        return pdfFontName;
    }

    /**
     * Modificateur de l attribut pdf font name.
     * 
     * @param pdfFontName le nouveau pdf font name
     */
    public void setPdfFontName(String pdfFontName)
    {
        this.pdfFontName = pdfFontName;
    }

}
