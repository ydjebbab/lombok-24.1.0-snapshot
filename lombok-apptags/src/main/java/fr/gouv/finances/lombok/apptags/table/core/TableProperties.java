/*
 * Copyright (c) 2014 DGFiP - Tous droits réservés
 */
/*
/*Copyright (c) 2014 DGFiP - Tous droits réservés/* * Copyright 2004 original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package fr.gouv.finances.lombok.apptags.table.core;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Optional;
import java.util.Properties;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import fr.gouv.finances.lombok.apptags.table.context.Context;

/**
 * Class TableProperties
 */
public class TableProperties implements Preferences
{

    private static Log logger = LogFactory.getLog(TableProperties.class);

    /** Constant : EXTREMECOMPONENTS_PROPERTIES. */
    public static final String EXTREMECOMPONENTS_PROPERTIES = "/application.properties";

    /** Constant : EXTREMETABLE_PROPERTIES. */
    public static final String EXTREMETABLE_PROPERTIES = "extremetable.properties";

    /** properties. */
    private Properties properties = new Properties();

    public TableProperties()
    {
        super();
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.core.Preferences#init(fr.gouv.finances.lombok.apptags.table.context.Context,
     *      java.lang.String)
     */
    @Override
    public void init(Context context, String preferencesLocation)
    {
        try
        {
            Optional<String> sPringSystemProp = Optional.ofNullable(System.getProperty("spring.config.location"));

            if (sPringSystemProp.isPresent())
            {
                properties.load(new URL(sPringSystemProp.get() + EXTREMECOMPONENTS_PROPERTIES).openStream());
            }
            else
            {
                properties.load(this.getClass().getResourceAsStream(EXTREMECOMPONENTS_PROPERTIES));
            }
            
            properties.load(this.getClass().getResourceAsStream(EXTREMETABLE_PROPERTIES));
            
            if (StringUtils.isNotBlank(preferencesLocation))
            {
                InputStream input = this.getClass().getResourceAsStream(preferencesLocation);
                if (input != null)
                {
                    properties.load(input);
                }
            }
        }
        catch (IOException e)
        {
            if (logger.isErrorEnabled())
            {
                logger.error("Could not load the eXtremeTable preferences.", e);
            }
        }
    }

    /**
     * Get the default property.
     * 
     * @param name
     * @return preference
     */
    @Override
    public String getPreference(String name)
    {
        return (String) properties.get(name);
    }
}
