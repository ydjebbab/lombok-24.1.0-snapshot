/*
 * Copyright (c) 2014 DGFiP - Tous droits réservés
 *//*
/*Copyright (c) 2014 DGFiP - Tous droits réservés/* * Copyright 2004 original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package fr.gouv.finances.lombok.apptags.table.view.html;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;

import fr.gouv.finances.lombok.apptags.table.bean.Column;
import fr.gouv.finances.lombok.apptags.table.core.TableConstants;
import fr.gouv.finances.lombok.apptags.table.core.TableModel;
import fr.gouv.finances.lombok.apptags.table.limit.Sort;
import fr.gouv.finances.lombok.apptags.util.HtmlBuilder;

/**
 * Class FormBuilder
 */
public class FormBuilder
{

    /** html. */
    private HtmlBuilder html;

    /** model. */
    private TableModel model;

    /**
     * Instanciation de form builder.
     * 
     * @param model
     */
    public FormBuilder(TableModel model)
    {
        this(new HtmlBuilder(), model);
    }

    /**
     * Instanciation de form builder.
     * 
     * @param html
     * @param model
     */
    public FormBuilder(HtmlBuilder html, TableModel model)
    {
        this.html = html;
        this.model = model;
    }

    /**
     * Accesseur de l attribut html builder.
     * 
     * @return html builder
     */
    public HtmlBuilder getHtmlBuilder()
    {
        return html;
    }

    /**
     * Accesseur de l attribut table model.
     * 
     * @return table model
     */
    protected TableModel getTableModel()
    {
        return model;
    }

    /**
     * methode Form start :
     */
    public void formStart()
    {
        formAttributes();
        html.newline();
        html.div().close();
        instanceParameter();
        exportTableIdParameter();
        exportParameters();
        rowsDisplayedParameter();
        filterParameter();
        pageParameters(); //   à enlever quand on voudra supprimer javascript
        sortParameters();
        aliasParameters();
        userDefinedParameters();
        html.newline();
        html.divEnd();
    }

    /**
     * methode Form end :
     */
    public void formEnd()
    {
        String form = model.getTableHandler().getTable().getForm();
        if (StringUtils.isBlank(form))
        {
            html.formEnd();
        }
    }

    /**
     * methode Form attributes :
     */
    public void formAttributes()
    {
        String form = model.getTableHandler().getTable().getForm();
        if (StringUtils.isBlank(form))
        {
            html.form();
            html.id(model.getTableHandler().getTable().getTableId());
            html.action(model.getTableHandler().getTable().getAction());
            html.method(model.getTableHandler().getTable().getMethod());
            html.close();
        }
    }

    /**
     * methode Instance parameter :
     */
    public void instanceParameter()
    {
        html.newline();
        html.input("hidden");
        html.name(TableConstants.EXTREME_COMPONENTS_INSTANCE);
        html.value(model.getTableHandler().getTable().getTableId());
        html.xclose();
    }

    /**
     * methode Filter parameter :
     */
    public void filterParameter()
    {
        if (BuilderUtils.filterable(model))
        {
            html.newline();
            html.input("hidden");
            StringBuilder   fieldName = new StringBuilder();
            fieldName.append(model.getTableHandler().prefixWithTableId());
            fieldName.append(TableConstants.FILTER);
            fieldName.append(TableConstants.ACTION);
            html.name(fieldName.toString());
            if (model.getLimit().isFiltered())
            {
                html.value(TableConstants.FILTER_ACTION);
            }

            html.xclose();
        }
    }

    /**
     * methode Rows displayed parameter :
     */
    public void rowsDisplayedParameter()
    {
        html.newline();
        html.input("hidden");
        StringBuilder   fieldName = new StringBuilder();
        fieldName.append(model.getTableHandler().prefixWithTableId());
        fieldName.append(TableConstants.CURRENT_ROWS_DISPLAYED);
        html.name(fieldName.toString());
        int currentRowsDisplayed = model.getLimit().getCurrentRowsDisplayed();
        html.value(String.valueOf(currentRowsDisplayed));
        html.xclose();
    }

    /**
     * methode Page parameters :
     */
    public void pageParameters()
    {
        html.newline();
        html.input("hidden");
        StringBuilder   fieldName = new StringBuilder();
        fieldName.append(model.getTableHandler().prefixWithTableId());
        fieldName.append(TableConstants.PAGE);
        html.name(fieldName.toString());
        int page = model.getLimit().getPage();
        if (page > 0)
        {
            html.value(String.valueOf(page));
        }
        html.xclose();
    }

    /**
     * L'id de la table est utilisé pour identifier la table lors de l'export. Si une page comporte plus d'une table
     * dans le formulaire, les tables doivent posséder des id différents
     */
    public void exportTableIdParameter()
    {
        if (BuilderUtils.showExports(model) || BuilderUtils.showExportsBottom(model))
        {
            String form = BuilderUtils.getForm(model);
            String existingForm = (String) model.getContext().getRequestAttribute(TableConstants.EXPORT_TABLE_ID);
            if (!form.equals(existingForm))
            {

                html.newline();
                html.input("hidden");
                html.name(TableConstants.EXPORT_TABLE_ID);
                html.xclose();

                model.getContext().setRequestAttribute(TableConstants.EXPORT_TABLE_ID, form);
            }
        }
    }

    /**
     * Paramètres nécessaires pour réaliser les exports, comprens le ViewResolver et le nom du fichier.
     */
    public void exportParameters()
    {
        if (BuilderUtils.showExports(model) || BuilderUtils.showExportsBottom(model))
        {
            html.newline();
            html.input("hidden");
            html.name(model.getTableHandler().prefixWithTableId() + TableConstants.EXPORT_VIEW);
            html.xclose();

            html.newline();
            html.input("hidden");
            html.name(model.getTableHandler().prefixWithTableId() + TableConstants.EXPORT_FILE_NAME);
            html.xclose();
        }
    }

    /**
     * methode Sort parameters :
     */
    public void sortParameters()
    {
        List<Column> columns = model.getColumnHandler().getColumns();

        for (Column column : columns)
        {
            if (column.isSortable())
            {
                html.newline();
                html.input("hidden");
                StringBuilder   fieldName = new StringBuilder();
                fieldName.append(model.getTableHandler().prefixWithTableId());
                fieldName.append(TableConstants.SORT);
                fieldName.append(column.getAlias());
                html.name(fieldName.toString());
                Sort sort = model.getLimit().getSort();
                if (sort.isSorted() && sort.getAlias().equals(column.getAlias()))
                {
                    html.value(sort.getSortOrder());
                }
                html.xclose();
            }

        }

    }

    /**
     * methode User defined parameters :
     */
    public void userDefinedParameters()
    {
        Map<String, Object> parameterMap = model.getRegistry().getParameterMap();
        Set<String> keys = parameterMap.keySet();

        for (String name : keys)
        {
            if (name.startsWith(model.getTableHandler().prefixWithTableId()))
            {
                continue;
            }

            String[] values = (String[]) parameterMap.get(name);
            if (values == null || values.length == 0)
            {
                html.newline();
                html.input("hidden").name(name).xclose();
            }
            else
            {
                for (int i = 0; i < values.length; i++)
                {
                    html.newline();
                    html.input("hidden").name(name).value(values[i]).xclose();
                }
            }

        }

    }

    /**
     * Si la colonne possède un alias, elle conserve tout de même la propriété comme paramètre.
     */
    public void aliasParameters()
    {
        List<Column> columns = model.getColumnHandler().getColumns();

        for (Column column : columns)
        {
            if (StringUtils.isNotBlank(column.getProperty()) && !column.getProperty().equals(column.getAlias()))
            {
                html.newline();
                html.input("hidden");
                StringBuilder   fieldName = new StringBuilder();
                fieldName.append(model.getTableHandler().prefixWithTableId());
                fieldName.append(TableConstants.ALIAS);
                fieldName.append(column.getAlias());
                html.name(fieldName.toString());
                html.value(column.getProperty());
                html.xclose();
            }
        }

    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        return html.toString();
    }
}
