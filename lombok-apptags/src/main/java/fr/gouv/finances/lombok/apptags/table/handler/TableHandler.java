/*
/*Copyright (c) 2014 DGFiP - Tous droits réservés/* * Copyright 2004 original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package fr.gouv.finances.lombok.apptags.table.handler;

import fr.gouv.finances.lombok.apptags.table.bean.Table;
import fr.gouv.finances.lombok.apptags.table.core.PreferencesConstants;
import fr.gouv.finances.lombok.apptags.table.core.TableCache;
import fr.gouv.finances.lombok.apptags.table.core.TableConstants;
import fr.gouv.finances.lombok.apptags.table.core.TableModel;
import fr.gouv.finances.lombok.apptags.table.core.TableModelUtils;

/**
 * La première passe sur le tableau charge les propriétés des colonnes.
 */
public class TableHandler
{

    /** model. */
    protected TableModel model;

    /** table. */
    private Table table;

    /** Constant : LAST_VALUE. */
    private static final String LAST_VALUE = "lastValue";

    /** Constant : EGALITE_BEAN. */
    private static final String EGALITE_BEAN = "egalitebean";

    /** Constant : NUM_ROW_BEAN. */
    private static final String NUM_ROW_BEAN = "numrowbean";

    /**
     * Instanciation de table handler.
     * 
     * @param model
     */
    public TableHandler(TableModel model)
    {
        this.model = model;
    }

    /**
     * Accesseur de l attribut table.
     * 
     * @return table
     */
    public Table getTable()
    {
        return table;
    }

    /**
     * methode Adds the table :
     * 
     * @param table
     */
    public void addTable(Table table)
    {
        this.table = table;
        addTableAttributes();
        table.defaults();
        if (model.getTableHandler() != null && model.getTableHandler().getTable() != null)
        { // on réinitialise les attributs de la table
            model.getTableHandler().getTable().addAttribute(EGALITE_BEAN, true);
            model.getTableHandler().getTable().addAttribute(LAST_VALUE, "");
            model.getTableHandler().getTable().addAttribute(NUM_ROW_BEAN, 0);
        }
    }

    /**
     * methode Adds the table attributes :
     */
    public void addTableAttributes()
    {
        String interceptor =
            TableModelUtils.getInterceptPreference(model, table.getInterceptor(),
                PreferencesConstants.TABLE_INTERCEPTOR);
        table.setInterceptor(interceptor);
        TableCache.getInstance().getTableInterceptor(interceptor).addTableAttributes(model, table);
    }

    /**
     * methode Prefix with table id :
     * 
     * @return string
     */
    public String prefixWithTableId()
    {
        StringBuilder   tableIdPrefix = new StringBuilder();
        tableIdPrefix.append(table.getTableId());
        tableIdPrefix.append("_");
        return tableIdPrefix.toString();
    }

    /**
     * Accesseur de l attribut total rows.
     * 
     * @return Le nombre total de lignes qui sera utilisé pour la limite Limit.setAttributes().
     * @see #setTotalRows(Integer)
     */
    public Integer getTotalRows()
    {
        return (Integer) table.getAttribute(TableConstants.TOTAL_ROWS);
    }

    /**
     * Lors de l'utilisation de l'interface Limit pour filtrer et trier les lignes on stocke le nombre total de lignes
     * passées au tableau.
     * 
     * @param totalRows Le nombre total de lignes qui sera utilisé pour la limite Limit.setAttributes().
     */
    public void setTotalRows(Integer totalRows)
    {
        table.addAttribute(TableConstants.TOTAL_ROWS, totalRows);
    }
}
