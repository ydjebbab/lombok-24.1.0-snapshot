/*
 * Copyright (c) 2014 DGFiP - Tous droits réservés
 *//*
/*Copyright (c) 2014 DGFiP - Tous droits réservés/* * Copyright 2004 original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package fr.gouv.finances.lombok.apptags.table.view.html;

import org.apache.commons.lang.StringUtils;

import fr.gouv.finances.lombok.apptags.table.bean.Row;
import fr.gouv.finances.lombok.apptags.table.core.TableModel;
import fr.gouv.finances.lombok.apptags.util.HtmlBuilder;

/**
 * Class RowBuilder
 */
public class RowBuilder
{

    /** html. */
    private HtmlBuilder html;

    /** model. */
    private TableModel model;

    /** row. */
    private Row row;

    /**
     * Instanciation de row builder.
     * 
     * @param model
     */
    public RowBuilder(TableModel model)
    {
        this(new HtmlBuilder(), model);
    }

    /**
     * Instanciation de row builder.
     * 
     * @param html
     * @param model
     */
    public RowBuilder(HtmlBuilder html, TableModel model)
    {
        this.html = html;
        this.model = model;
        this.row = model.getRowHandler().getRow();
    }

    /**
     * Accesseur de l attribut html builder.
     * 
     * @return html builder
     */
    public HtmlBuilder getHtmlBuilder()
    {
        return html;
    }

    /**
     * Accesseur de l attribut table model.
     * 
     * @return table model
     */
    protected TableModel getTableModel()
    {
        return model;
    }

    /**
     * Accesseur de l attribut row.
     * 
     * @return row
     */
    protected Row getRow()
    {
        return row;
    }

    /**
     * methode Row start :
     */
    public void rowStart()
    {
        html.tr(1);
        styleClass();
        style();
        onclick();
        onmouseover();
        onmouseout();
        html.close();
    }

    /**
     * methode Row end :
     */
    public void rowEnd()
    {
        html.trEnd(1);
    }

    /**
     * methode Style :
     */
    public void style()
    {
        String style = row.getStyle();
        html.style(style);
    }

    /**
     * methode Style class :
     */
    public void styleClass()
    {
        String styleClass = getStyleClass();
        html.styleClass(styleClass);
    }

    /**
     * methode Onclick :
     */
    public void onclick()
    {
        String onclick = row.getOnclick();
        html.onclick(onclick);
    }

    /**
     * methode Onmouseover :
     */
    public void onmouseover()
    {
        // boolean highlightRow = row.isHighlightRow();
        // if (highlightRow)
        // {
        // String highlightClass = row.getHighlightClass();
        // if (StringUtils.isNotBlank(row.getOnmouseover()))
        // {
        // html.onmouseover("this.className='" + highlightClass + "';" + row.getOnmouseover());
        // }
        // else
        // {
        // html.onmouseover("this.className='" + highlightClass + "'");
        // }
        // }
        // else
        // {
        html.onmouseover(row.getOnmouseover());
        // }
    }

    /**
     * methode Onmouseout :
     */
    public void onmouseout()
    {
        // boolean highlightRow = row.isHighlightRow();
        // if (highlightRow)
        // {
        // String styleClass = getStyleClass();
        // if (StringUtils.isNotBlank(row.getOnmouseout()))
        // {
        // html.onmouseout("this.className='" + styleClass + "';" + row.getOnmouseout());
        // }
        // else
        // {
        // html.onmouseout("this.className='" + styleClass + "'");
        // }
        // }
        // else
        // {
        html.onmouseout(row.getOnmouseout());
        // }
    }

    /**
     * Accesseur de l attribut style class.
     * 
     * @return style class
     */
    protected String getStyleClass()
    {
        String styleClass = row.getStyleClass();
        if (StringUtils.isNotBlank(styleClass))
        {
            return styleClass + " " + getHightlightClassIfActived();
        }

        if (model.getRowHandler().isRowEven())
        {
            return BuilderConstants.ROW_EVEN_CSS + " " + getHightlightClassIfActived();
        }

        return BuilderConstants.ROW_ODD_CSS + " " + getHightlightClassIfActived();
    }

    /**
     * Accesseur de l attribut hightlight class if actived.
     * 
     * @return hightlight class if actived
     */
    public String getHightlightClassIfActived()
    {
        if (row.isHighlightRow())
        {
            return row.getHighlightClass();
        }
        return "";
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        return html.toString();
    }
}
