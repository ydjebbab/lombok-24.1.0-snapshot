/*
 * Copyright (c) 2015 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.apptags.table.limit;

import java.util.Map;

import fr.gouv.finances.lombok.apptags.table.context.Context;
import fr.gouv.finances.lombok.apptags.table.core.AbstractRegistry;
import fr.gouv.finances.lombok.apptags.table.state.State;



/**
 * Class LimitRegistry.
 */
public final class LimitRegistry extends AbstractRegistry
{

    /**
     * Instanciation de limit registry.
     *
     * @param context the context
     * @param tableId the table id
     * @param prefixWithTableId the prefix with table id
     * @param state the state
     * @param stateAttr the state attr
     */
    public LimitRegistry(Context context, String tableId, String prefixWithTableId, String state, String stateAttr)
    {
        this.context = context;
        this.tableId = tableId;
        this.prefixWithTableId = prefixWithTableId;
        this.state = state;
        this.stateAttr = stateAttr;
        this.autoIncludeParameters = false;
        setParameterMap();
    }

    /**
     * Si l'interface Limit n'est pas utilisée, l'état des paramètres est sauvegardé. A contrario, lors l'utilisation de
     * l'interface Limit il n'est pas nécésaire de sauvegardé l'état des paramètres
     *
     * @param state the state
     * @param tableParameterMap the table parameter map
     */
    @Override
    protected void handleStateInternal(State state, Map tableParameterMap)
    {
        // do nothing
    }
}
