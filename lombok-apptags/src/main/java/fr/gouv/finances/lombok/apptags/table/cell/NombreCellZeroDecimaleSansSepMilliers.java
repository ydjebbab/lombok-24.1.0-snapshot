/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.apptags.table.cell;

import fr.gouv.finances.lombok.apptags.table.bean.Column;
import fr.gouv.finances.lombok.apptags.table.core.TableModel;
import fr.gouv.finances.lombok.apptags.util.format.FormaterNombre;

/**
 * Class NombreCellZeroDecimaleSansSepMilliers
 */
public class NombreCellZeroDecimaleSansSepMilliers extends AbstractNombreCell
{

    /** Constant : PRECISION. */
    private static final int PRECISION = 0;

    /** Constant : FORMAT_PATTERN. */
    private static final String FORMAT_PATTERN = "##0";

    /**
     * Instanciation de nombre cell zero decimale sans sep milliers.
     */
    public NombreCellZeroDecimaleSansSepMilliers()
    {
        super();
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.cell.AbstractNombreCell#getPrecision()
     */
    @Override
    public int getPrecision()
    {
        return PRECISION;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.cell.AbstractNombreCell#getExportDisplay(fr.gouv.finances.lombok.apptags.table.core.TableModel,
     *      fr.gouv.finances.lombok.apptags.table.bean.Column)
     */
    @Override
    public String getExportDisplay(TableModel model, Column column)
    {
        return FormaterNombre.nombreNDecimalesSansSepMilliersPourCsv(column.getPropertyValue(), getPrecision());
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.cell.AbstractNombreCell#getCellValue(fr.gouv.finances.lombok.apptags.table.core.TableModel,
     *      fr.gouv.finances.lombok.apptags.table.bean.Column)
     */
    @Override
    protected String getCellValue(TableModel model, Column column)
    {
        return FormaterNombre.nombreNDecimaleSansSepMilliersPourHtml(column.getPropertyValue(), getPrecision());
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.cell.PatternCell#getFormatPattern()
     */
    @Override
    public String getFormatPattern()
    {
        return FORMAT_PATTERN;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.cell.PatternCell#getAlignmentCell()
     */
    @Override
    public int getAlignmentCell()
    {
        return PatternCell.HORIZONTAL_ALIGN_CENTER;
    }
}
