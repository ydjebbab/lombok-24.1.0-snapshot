/*
/*Copyright (c) 2014 DGFiP - Tous droits réservés/* * Copyright 2004 original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package fr.gouv.finances.lombok.apptags.table.calc;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import fr.gouv.finances.lombok.apptags.table.bean.Column;
import fr.gouv.finances.lombok.apptags.table.core.PreferencesConstants;
import fr.gouv.finances.lombok.apptags.table.core.TableCache;
import fr.gouv.finances.lombok.apptags.table.core.TableModel;
import fr.gouv.finances.lombok.apptags.util.ExtremeUtils;

/**
 * Class CalcUtils
 */
public final class CalcUtils
{

    /** log. */
    private static Log log = LogFactory.getLog(CalcUtils.class);

    /**
     * Instanciation de calc utils.
     */
    private CalcUtils()
    {
    }

    /**
     * Accesseur de l attribut calc results.
     * 
     * @param model
     * @param column
     * @return calc results
     */
    public static List<CalcResult> getCalcResults(TableModel model, Column column)
    {
        List<CalcResult> values = new ArrayList<CalcResult>();

        List<String> calcs = column.getCalc();
        for (int i = 0; i < calcs.size(); i++)
        {
            values.add(getCalcResultsByPosition(model, column, i));
        }

        return values;
    }

    /**
     * Accesseur de l attribut calc results by position.
     * 
     * @param model
     * @param column
     * @param position
     * @return calc results by position
     */
    public static CalcResult getCalcResultsByPosition(TableModel model, Column column, int position)
    {
        CalcResult result;
        String calcClassName = CalcUtils.getCalcClassNameByPosition(model, column, position);

        if (!isCalcClassName(calcClassName))
        {
            result = new CalcResult(calcClassName, null);
        }
        else
        {
            Calc calc = TableCache.getInstance().getCalc(calcClassName);
            result = new CalcResult(calcClassName, calc.getCalcResult(model, column));
        }
        return result;
    }

    /**
     * Accesseur de l attribut first calc column titles.
     * 
     * @param model
     * @return first calc column titles
     */
    public static String[] getFirstCalcColumnTitles(TableModel model)
    {
        List<String> values = new ArrayList<String>();

        Column column = model.getColumnHandler().getFirstCalcColumn();
        // String calcs[] = column.getCalc();
        List<String> calcs = column.getCalc();
        for (int i = 0; i < calcs.size(); i++)
        {
            values.add(getFirstCalcColumnTitleByPosition(model, i));
        }

        return values.toArray(new String[values.size()]);
    }

    /**
     * Accesseur de l attribut first calc column title by position.
     * 
     * @param model
     * @param position
     * @return first calc column title by position
     */
    public static String getFirstCalcColumnTitleByPosition(TableModel model, int position)
    {
        String result = "";
        Column column = model.getColumnHandler().getFirstCalcTitleColumn();
        List<String> calcTitle = column.getCalcTitle();
        if (calcTitle != null && ((calcTitle.size()) - 1 >= position))
        {
            result = calcTitle.get(position);
        }
        return result;
    }

    /**
     * Accesseur de l attribut calc class name by position.
     * 
     * @param model
     * @param column
     * @param position
     * @return calc class name by position
     */
    private static String getCalcClassNameByPosition(TableModel model, Column column, int position)
    {
        List<String> calcs = column.getCalc();

        String calcName = "";

        if (calcs != null && ((calcs.size()) - 1 >= position))
        {
            calcName = calcs.get(position);
        }
        String calcClassName = model.getPreferences().getPreference(PreferencesConstants.COLUMN_CALC + calcName);
        if (StringUtils.isBlank(calcClassName))
        {
            calcClassName = calcName;
        }

        return calcClassName;
    }

    /**
     * Verifie si calc class name.
     * 
     * @param calcClassName
     * @return true, si c'est calc class name
     */
    private static boolean isCalcClassName(String calcClassName)
    {
        boolean result;
        try
        {
            Class.forName(calcClassName);
            result = true;
        }
        catch (ClassNotFoundException exception)
        {
            result = false;
        }
        return result;
    }

    /**
     * methode Each row calc value :
     * 
     * @param handler
     * @param rows
     * @param property
     */
    public static void eachRowCalcValue(CalcHandler handler, Collection<Object> rows, String property)
    {
        if (rows != null)
        {
            for (Object row : rows)
            {
                Object value = null;

                if (ExtremeUtils.isBeanPropertyReadable(row, property))
                {
                    try
                    {
                        value = PropertyUtils.getProperty(row, property);

                        if (value instanceof Number)
                        {
                            handler.processCalcValue((Number) value);
                        }
                        else
                        {
                            handler.processCalcValue(getValue(property, value));
                        }
                    }
                    catch (Exception exception)
                    {
                        StringBuilder   msg = new StringBuilder();
                        msg.append("Erreur lors de la lecture d'une valeur numérique à partir de la propriété  :[");
                        msg.append(property);
                        msg.append("].");
                        log.error(msg.toString());
                    }
                }
            }
        }

    }

    /**
     * Accesseur de l attribut value.
     * 
     * @param property
     * @param value
     * @return value
     */
    private static Number getValue(String property, Object value)
    {
        BigDecimal result = new BigDecimal("0.00");
        String valueAsString = String.valueOf(value);
        if (StringUtils.isNotBlank(valueAsString))
        {
            try
            {
                result = new BigDecimal(valueAsString);
            }
            catch (NumberFormatException nbe)
            {
                StringBuilder   msg = new StringBuilder();
                msg.append("Erreur lors de la lecture d'une valeur numérique à partir de la propriété  :[");
                msg.append(property);
                msg.append("] avec la valeur [ ");
                msg.append(valueAsString);
                msg.append("].");
                log.error(msg.toString());
            }
        }

        return result;
    }
}
