/*
/*Copyright (c) 2014 DGFiP - Tous droits réservés/* * Copyright 2004 original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package fr.gouv.finances.lombok.apptags.table.resource;

import java.text.MessageFormat;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import fr.gouv.finances.lombok.apptags.table.context.Context;
import fr.gouv.finances.lombok.apptags.table.core.Messages;
import fr.gouv.finances.lombok.apptags.table.core.TableModelUtils;

/**
 * Charge le fichiers des messages qui seront utilisés par le Model.
 */
public class TableResourceBundle implements Messages
{

    /** log. */
    private static Log log = LogFactory.getLog(TableResourceBundle.class);

    /** Constant : EXTREMETABLE_RESOURCE_BUNDLE. */
    public static final String EXTREMETABLE_RESOURCE_BUNDLE =
        "fr.gouv.finances.lombok.apptags.table.resource.extremetableResourceBundle";

    /** custom resource bundle. */
    private ResourceBundle customResourceBundle;

    /** default resource bundle. */
    private ResourceBundle defaultResourceBundle;

    /** locale. */
    private Locale locale;

    /**
     * Constructeur de la classe TableResourceBundle.java
     *
     */
    public TableResourceBundle()
    {
        super();
        
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.core.Messages#init(fr.gouv.finances.lombok.apptags.table.context.Context,
     *      java.util.Locale)
     */
    public void init(Context context, Locale locale)
    {
        this.locale = locale;
        defaultResourceBundle = findResourceBundle(EXTREMETABLE_RESOURCE_BUNDLE, locale);
        String messagesLocation = TableModelUtils.getMessagesLocation(context);
        if (StringUtils.isNotBlank(messagesLocation))
        {
            customResourceBundle = findResourceBundle(messagesLocation, locale);
        }
    }

    /**
     * methode Find resource bundle :
     * 
     * @param resourceBundleLocation
     * @param locale
     * @return resource bundle
     */
    private ResourceBundle findResourceBundle(String resourceBundleLocation, Locale locale)
    {
        try
        {
            return ResourceBundle.getBundle(resourceBundleLocation, locale, getClass().getClassLoader());
        }
        catch (MissingResourceException exception)
        {
            if (log.isTraceEnabled())
            {
                StringBuilder   msg = new StringBuilder(20);
                msg.append("Le fichier de messages [");
                msg.append(resourceBundleLocation);
                msg.append("] n'a pas été trouvé");
                log.trace(msg.toString(), exception);
            }
        }

        return null;
    }

    /**
     * Get the resource property.
     * 
     * @param code
     * @return message
     */
    public String getMessage(String code)
    {
        return getMessage(code, null);
    }

    /**
     * Get the resource property.
     * 
     * @param code
     * @param args
     * @return message
     */
    public String getMessage(String code, Object[] args)
    {
        String result = findResource(customResourceBundle, code);

        if (result == null)
        {
            result = findResource(defaultResourceBundle, code);
        }

        if (result != null && args != null)
        {
            MessageFormat formatter = new MessageFormat("");
            formatter.setLocale(locale);
            formatter.applyPattern(result);
            result = formatter.format(args);
        }

        return result;
    }

    /**
     * methode Find resource :
     * 
     * @param resourceBundle
     * @param code
     * @return string
     */
    private String findResource(ResourceBundle resourceBundle, String code)
    {
        String result = null;

        if (resourceBundle == null)
        {
            return result;
        }

        try
        {
            result = resourceBundle.getString(code);
        }
        catch (MissingResourceException exception)
        {
            // On ne fait rien
        }

        return result;
    }
}
