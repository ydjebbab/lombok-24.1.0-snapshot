/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
 *  Copyright 2004 Jenkov Development
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http:/www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : ServletContextUtil.java
 *
 */
package fr.gouv.finances.lombok.apptags.util;

import javax.servlet.jsp.PageContext;

/**
 * Class ServletContextUtil --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.2 $ Date: 11 déc. 2009
 */
public class ServletContextUtil
{

    /** ICO n_ dir. */
    public static String ICON_DIR = "prizetags.icon.dir";

    /** O n_ click. */
    public static String ON_CLICK = "prizetags.icon.onClick";

    /** O n_ db l_ click. */
    public static String ON_DBL_CLICK = "prizetags.icon.onDblClick";

    /** O n_ ke y_ down. */
    public static String ON_KEY_DOWN = "prizetags.icon.onKeyDown";

    /** O n_ ke y_ pressed. */
    public static String ON_KEY_PRESSED = "prizetags.icon.onKeyPressed";

    /** O n_ mous e_ down. */
    public static String ON_MOUSE_DOWN = "prizetags.icon.onMouseDown";

    /** O n_ mous e_ move. */
    public static String ON_MOUSE_MOVE = "prizetags.icon.onMouseMove";

    /** O n_ mous e_ over. */
    public static String ON_MOUSE_OVER = "prizetags.icon.onMouseOver";

    /**
     * Accesseur de l attribut context parameter.
     * 
     * @param pageContext --
     * @param parameterName --
     * @return context parameter
     */
    public static String getContextParameter(PageContext pageContext, String parameterName)
    {
        return pageContext.getServletContext().getInitParameter(parameterName);
    }

    /**
     * Accesseur de l attribut icon dir.
     * 
     * @param pageContext --
     * @return icon dir
     */
    public static String getIconDir(PageContext pageContext)
    {
        return getContextParameter(pageContext, ICON_DIR);
    }

    /**
     * Accesseur de l attribut on click.
     * 
     * @param pageContext --
     * @return on click
     */
    public static String getOnClick(PageContext pageContext)
    {
        return getContextParameter(pageContext, ON_CLICK);
    }

    /**
     * Accesseur de l attribut on dbl click.
     * 
     * @param pageContext --
     * @return on dbl click
     */
    public static String getOnDblClick(PageContext pageContext)
    {
        return getContextParameter(pageContext, ON_DBL_CLICK);
    }

    /**
     * Accesseur de l attribut on key down.
     * 
     * @param pageContext --
     * @return on key down
     */
    public static String getOnKeyDown(PageContext pageContext)
    {
        return getContextParameter(pageContext, ON_KEY_DOWN);
    }

    /**
     * Accesseur de l attribut on key pressed.
     * 
     * @param pageContext --
     * @return on key pressed
     */
    public static String getOnKeyPressed(PageContext pageContext)
    {
        return getContextParameter(pageContext, ON_KEY_PRESSED);
    }

    /**
     * Accesseur de l attribut on mouse down.
     * 
     * @param pageContext --
     * @return on mouse down
     */
    public static String getOnMouseDown(PageContext pageContext)
    {
        return getContextParameter(pageContext, ON_MOUSE_DOWN);
    }

    /**
     * Accesseur de l attribut on mouse move.
     * 
     * @param pageContext --
     * @return on mouse move
     */
    public static String getOnMouseMove(PageContext pageContext)
    {
        return getContextParameter(pageContext, ON_MOUSE_MOVE);
    }

    /**
     * Accesseur de l attribut on mouse over.
     * 
     * @param pageContext --
     * @return on mouse over
     */
    public static String getOnMouseOver(PageContext pageContext)
    {
        return getContextParameter(pageContext, ON_MOUSE_OVER);
    }

}
