/*
/*Copyright (c) 2014 DGFiP - Tous droits réservés/* * Copyright 2004 original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package fr.gouv.finances.lombok.apptags.table.context;

import java.io.StringWriter;
import java.io.Writer;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import fr.gouv.finances.lombok.apptags.table.core.TableModelUtils;

/**
 * Class HttpServletRequestContext
 */
public class HttpServletRequestContext implements Context
{

    /** request. */
    private HttpServletRequest request;

    /** parameter map. */
    private Map<String, String[]> parameterMap;

    /**
     * Instanciation de http servlet request context.
     * 
     * @param request
     */
    public HttpServletRequestContext(HttpServletRequest request)
    {
        this.request = request;
    }

    /**
     * Permet d'utiliser le contexte de servlet normal HttpServletRequest avec une map de paramètres spécifique au
     * tableau.
     * 
     * @param request Une instance de HttpServletRequest.
     * @param parameterMap Une map de mapramètres à utiliser
     */
    public HttpServletRequestContext(HttpServletRequest request, Map<String, String[]> parameterMap)
    {
        this(request);
        this.parameterMap = parameterMap;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.context.Context#getApplicationInitParameter(java.lang.String)
     */
    @Override
    public Object getApplicationInitParameter(String name)
    {
        return request.getSession().getServletContext().getInitParameter(name);
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.context.Context#getApplicationAttribute(java.lang.String)
     */
    @Override
    public Object getApplicationAttribute(String name)
    {
        return request.getSession().getServletContext().getAttribute(name);
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.context.Context#setApplicationAttribute(java.lang.String,
     *      java.lang.Object)
     */
    @Override
    public void setApplicationAttribute(String name, Object value)
    {
        request.getSession().getServletContext().setAttribute(name, value);
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.context.Context#removeApplicationAttribute(java.lang.String)
     */
    @Override
    public void removeApplicationAttribute(String name)
    {
        request.getSession().getServletContext().removeAttribute(name);
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.context.Context#getPageAttribute(java.lang.String)
     */
    @Override
    public Object getPageAttribute(String name)
    {
        return request.getAttribute(name);
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.context.Context#setPageAttribute(java.lang.String, java.lang.Object)
     */
    @Override
    public void setPageAttribute(String name, Object value)
    {
        request.setAttribute(name, value);
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.context.Context#removePageAttribute(java.lang.String)
     */
    @Override
    public void removePageAttribute(String name)
    {
        request.removeAttribute(name);
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.context.Context#getParameter(java.lang.String)
     */
    @Override
    public String getParameter(String name)
    {
        String result = request.getParameter(name);
        if (parameterMap != null)
        {
            String[] values = TableModelUtils.getValueAsArray(parameterMap.get(name));
            if (values != null && values.length > 0)
            {
                result = values[0];
            }
        }

        return result;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.context.Context#getParameterMap()
     */
    @Override
    public Map<String, String[]> getParameterMap()
    {
        Map<String, String[]> result;
        if (parameterMap != null)
        {
            result = parameterMap;
        }
        else
        {
            result = request.getParameterMap();
        }

        return result;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.context.Context#getRequestAttribute(java.lang.String)
     */
    @Override
    public Object getRequestAttribute(String name)
    {
        return request.getAttribute(name);
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.context.Context#setRequestAttribute(java.lang.String,
     *      java.lang.Object)
     */
    @Override
    public void setRequestAttribute(String name, Object value)
    {
        request.setAttribute(name, value);
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.context.Context#removeRequestAttribute(java.lang.String)
     */
    @Override
    public void removeRequestAttribute(String name)
    {
        request.removeAttribute(name);
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.context.Context#getSessionAttribute(java.lang.String)
     */
    @Override
    public Object getSessionAttribute(String name)
    {
        return request.getSession().getAttribute(name);
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.context.Context#setSessionAttribute(java.lang.String,
     *      java.lang.Object)
     */
    @Override
    public void setSessionAttribute(String name, Object value)
    {
        request.getSession().setAttribute(name, value);
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.context.Context#removeSessionAttribute(java.lang.String)
     */
    @Override
    public void removeSessionAttribute(String name)
    {
        request.getSession().removeAttribute(name);
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.context.Context#getWriter()
     */
    @Override
    public Writer getWriter()
    {
        return new StringWriter();
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.context.Context#getLocale()
     */
    @Override
    public Locale getLocale()
    {
        return request.getLocale();
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.context.Context#getContextPath()
     */
    @Override
    public String getContextPath()
    {
        return request.getContextPath();
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.context.Context#getContextObject()
     */
    @Override
    public Object getContextObject()
    {
        return request;
    }
}
