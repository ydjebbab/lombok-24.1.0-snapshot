/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.apptags.table.view.jrxmlinfo;

/**
 * Class LogoInfo
 */
public class LogoInfo
{

    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** xleft corner. */
    private int xleftCorner;

    /** yleft corner. */
    private int yleftCorner;

    /** width. */
    private int width;

    /** height. */
    private int height;

    /** image path. */
    private String imagePath;

    /** image name. */
    private String imageName;

    /**
     * Instanciation de logo info.
     */
    public LogoInfo()
    {
        super();
    }

    /**
     * Accesseur de l attribut image name.
     * 
     * @return image name
     */
    public String getImageName()
    {
        return imageName;
    }

    /**
     * Modificateur de l attribut image name.
     * 
     * @param imageName le nouveau image name
     */
    public void setImageName(String imageName)
    {
        this.imageName = imageName;
    }

    /**
     * Accesseur de l attribut image path.
     * 
     * @return image path
     */
    public String getImagePath()
    {
        return imagePath;
    }

    /**
     * Modificateur de l attribut image path.
     * 
     * @param imagePath le nouveau image path
     */
    public void setImagePath(String imagePath)
    {
        this.imagePath = imagePath;
    }

    /**
     * Accesseur de l attribut height.
     * 
     * @return height
     */
    public int getHeight()
    {
        return height;
    }

    /**
     * Modificateur de l attribut height.
     * 
     * @param height le nouveau height
     */
    public void setHeight(int height)
    {
        this.height = height;
    }

    /**
     * Accesseur de l attribut width.
     * 
     * @return width
     */
    public int getWidth()
    {
        return width;
    }

    /**
     * Modificateur de l attribut width.
     * 
     * @param width le nouveau width
     */
    public void setWidth(int width)
    {
        this.width = width;
    }

    /**
     * Accesseur de l attribut xleft corner.
     * 
     * @return xleft corner
     */
    public int getXleftCorner()
    {
        return xleftCorner;
    }

    /**
     * Modificateur de l attribut xleft corner.
     * 
     * @param xleftCorner le nouveau xleft corner
     */
    public void setXleftCorner(int xleftCorner)
    {
        this.xleftCorner = xleftCorner;
    }

    /**
     * Accesseur de l attribut yleft corner.
     * 
     * @return yleft corner
     */
    public int getYleftCorner()
    {
        return yleftCorner;
    }

    /**
     * Modificateur de l attribut yleft corner.
     * 
     * @param yleftCorner le nouveau yleft corner
     */
    public void setYleftCorner(int yleftCorner)
    {
        this.yleftCorner = yleftCorner;
    }

}
