/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.apptags.table.view;

import java.io.Serializable;

import org.apache.commons.lang.StringUtils;

import fr.gouv.finances.lombok.apptags.table.cell.PreferencesConstants;

/**
 * Class MaxFetchTableAttributes
 */
public class MaxFetchTableAttributes implements Serializable
{

    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** Nombre maximal d'éléments retournés à l'utilisaetru. */
    private int maxFetch;

    /** Id du tableau extremecomponents. */
    private String tableId;

    /** Contexte de stockage de l'attribut de gestion MaxFetchTableAttributes. */
    private int maxFetchAttrStorageScope;

    /** reponse complete. */
    private String reponseComplete;

    /** elements in flow. */
    private String elementsInFlow;

    /**
     * Instanciation de max fetch table attributes.
     * 
     * @param tableId
     */
    public MaxFetchTableAttributes(String tableId)
    {
        this(tableId, Integer.valueOf(PreferencesConstants.MAXFETCH),
            PreferencesConstants.DEFAULT_MAXFETCHATTR_STORAGE_SCOPE);
    }

    /**
     * Instanciation de max fetch table attributes.
     * 
     * @param tableId
     * @param maxFetch
     */
    public MaxFetchTableAttributes(String tableId, Integer maxFetch)
    {
        this(tableId, maxFetch, PreferencesConstants.DEFAULT_MAXFETCHATTR_STORAGE_SCOPE);
    }

    /**
     * Instanciation de max fetch table attributes.
     * 
     * @param tableId
     * @param maxFetch
     * @param storageScope
     */
    public MaxFetchTableAttributes(String tableId, Integer maxFetch, int storageScope)
    {

        // Rowid
        if (maxFetch == null || maxFetch.intValue() == 0)
        {
            this.maxFetch = PreferencesConstants.MAXFETCH;
        }
        else
        {
            this.maxFetch = maxFetch.intValue();
        }

        // tableId
        if (tableId == null || StringUtils.isBlank(tableId))
        {
            this.tableId = PreferencesConstants.TABLE_ID;
        }
        else
        {
            this.tableId = tableId;
        }

        // checkboxattrStorageScope
        if (storageScope != PreferencesConstants.FLOW_SCOPE && storageScope != PreferencesConstants.CONVERSATION_SCOPE)
        {
            this.maxFetchAttrStorageScope = PreferencesConstants.FLOW_SCOPE;
        }
        else
        {
            this.maxFetchAttrStorageScope = storageScope;
        }

        // Nom dans de l'indicateur reponse complète
        this.reponseComplete = this.tableId + "_" + PreferencesConstants.REPONSE_COMPLETE;

    }

    /**
     * Accesseur de l attribut elements in flow.
     * 
     * @return elements in flow
     */
    public String getElementsInFlow()
    {
        return elementsInFlow;
    }

    /**
     * Modificateur de l attribut elements in flow.
     * 
     * @param elementsInFlow le nouveau elements in flow
     */
    public void setElementsInFlow(String elementsInFlow)
    {
        this.elementsInFlow = elementsInFlow;
    }

    /**
     * Gets the nombre maximal d'éléments retournés à l'utilisaetru.
     * 
     * @return the nombre maximal d'éléments retournés à l'utilisaetru
     */
    public long getMaxFetch()
    {
        return maxFetch;
    }

    /**
     * Sets the nombre maximal d'éléments retournés à l'utilisaetru.
     * 
     * @param maxFetch the new nombre maximal d'éléments retournés à l'utilisaetru
     */
    public void setMaxFetch(int maxFetch)
    {
        this.maxFetch = maxFetch;
    }

    /**
     * Gets the contexte de stockage de l'attribut de gestion MaxFetchTableAttributes.
     * 
     * @return the contexte de stockage de l'attribut de gestion MaxFetchTableAttributes
     */
    public int getMaxFetchAttrStorageScope()
    {
        return maxFetchAttrStorageScope;
    }

    /**
     * Sets the contexte de stockage de l'attribut de gestion MaxFetchTableAttributes.
     * 
     * @param maxFetchAttrStorageScope the new contexte de stockage de l'attribut de gestion MaxFetchTableAttributes
     */
    public void setMaxFetchAttrStorageScope(int maxFetchAttrStorageScope)
    {
        this.maxFetchAttrStorageScope = maxFetchAttrStorageScope;
    }

    /**
     * Accesseur de l attribut reponse complete.
     * 
     * @return reponse complete
     */
    public String getReponseComplete()
    {
        return reponseComplete;
    }

    /**
     * Modificateur de l attribut reponse complete.
     * 
     * @param reponseComplete le nouveau reponse complete
     */
    public void setReponseComplete(String reponseComplete)
    {
        this.reponseComplete = reponseComplete;
    }

    /**
     * Gets the id du tableau extremecomponents.
     * 
     * @return the id du tableau extremecomponents
     */
    public String getTableId()
    {
        return tableId;
    }

    /**
     * Sets the id du tableau extremecomponents.
     * 
     * @param tableId the new id du tableau extremecomponents
     */
    public void setTableId(String tableId)
    {
        this.tableId = tableId;
    }

    /**
     * methode Utiliser liste elements :
     * 
     * @param elements
     * @return max fetch table attributes
     */
    public MaxFetchTableAttributes utiliserListeElements(String elements)
    {
        this.elementsInFlow = elements;
        return this;
    }

    /**
     * Définit le nombre maximal d'éléments présentés à l'utilisateur.
     * 
     * @param maxFetch nombre maximal d'éléments retournés
     * @return max fetch table attributes
     */
    public MaxFetchTableAttributes utiliserMaxFetch(final int maxFetch)
    {
        this.maxFetch = maxFetch;
        return this;
    }

    /**
     * Accesseur de l attribut default.
     * 
     * @param tableId
     * @return default
     */
    public static MaxFetchTableAttributes getDefault(final String tableId)
    {
        return new MaxFetchTableAttributes(tableId, null);
    }

    /**
     * Accesseur de l attribut default.
     * 
     * @return default
     */
    public static MaxFetchTableAttributes getDefault()
    {
        return new MaxFetchTableAttributes(null, null);
    }

    /**
     * Retourne le nom utilisé dans le contexte pour stocker l'attribut de type MaxFetchTableAttributes.
     * 
     * @return scope name
     */
    public String getScopeName()
    {
        return this.tableId + PreferencesConstants.MAXFETCH_PARAM_ATTR;
    }

    /**
     * Retourne le nom utilisé dans le contexte pour stocker l'attribut de type MaxFetchTableAttributes.
     * 
     * @param tableId
     * @return scope name
     */
    public static String getScopeName(final String tableId)
    {
        return tableId + PreferencesConstants.MAXFETCH_PARAM_ATTR;
    }
}
