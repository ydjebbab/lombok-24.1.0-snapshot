/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.apptags.table.cell;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import org.apache.commons.beanutils.BeanComparator;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import fr.gouv.finances.lombok.apptags.table.bean.Column;
import fr.gouv.finances.lombok.apptags.table.core.Messages;
import fr.gouv.finances.lombok.apptags.table.core.TableConstants;
import fr.gouv.finances.lombok.apptags.table.core.TableModel;
import fr.gouv.finances.lombok.apptags.table.view.html.TableActions;
import fr.gouv.finances.lombok.apptags.util.EcTableException;
import fr.gouv.finances.lombok.apptags.util.ExtremeUtils;
import fr.gouv.finances.lombok.apptags.util.HtmlBuilder;
import fr.gouv.finances.lombok.apptags.util.comparators.DateComparator;
import fr.gouv.finances.lombok.apptags.util.comparators.NullorNotComparableComparator;
import fr.gouv.finances.lombok.apptags.util.format.FormaterBoolean;
import fr.gouv.finances.lombok.util.comparators.NullSafeBeanComparator;

/**
 * Une cellule de filtre de type liste déroulante.
 */
public class CpFilterDroplistCell implements Cell
{

    
    /**
     * Instanciation de cp filter droplist cell.
     */
    public CpFilterDroplistCell()
    {
        super();
    }

    /** log. */
    private static Log log = LogFactory.getLog(CpFilterDroplistCell.class);

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.cell.Cell#getExportDisplay(fr.gouv.finances.lombok.apptags.table.core.TableModel,
     *      fr.gouv.finances.lombok.apptags.table.bean.Column)
     */
    @Override
    public String getExportDisplay(TableModel model, Column column)
    {
        return null;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.cell.Cell#getHtmlDisplay(fr.gouv.finances.lombok.apptags.table.core.TableModel,
     *      fr.gouv.finances.lombok.apptags.table.bean.Column)
     */
    @Override
    public String getHtmlDisplay(TableModel model, Column column)
    {
        HtmlBuilder html = new HtmlBuilder();
        Messages messages = model.getMessages();
        html.td(2);
        if (!column.isFilterable())
        {                        
            StringBuilder tdName = new StringBuilder();
            tdName.append(model.getTableHandler().prefixWithTableId());
            tdName.append(TableConstants.FILTER);
            tdName.append(column.getAlias());
            tdName.append(TableConstants.TDFILTER);
            html.id(tdName.toString());
            String styleClass = "";
            if (StringUtils.isNotEmpty(column.getFilterClass()))
            {
                styleClass = column.getFilterClass();
            }
            html.ariaHidden("true");
            html.close();

            html.span().ariaHidden("true").close();
            html.append("");
            html.spanEnd();
        }
        else
        {
            Collection<FilterOption> filterOptions = column.getFilterOptions();
            if (filterOptions == null || filterOptions.isEmpty())
            {
                filterOptions = getFilterDropList(model, column);
            }
            html.append(dropListHtml(model, column, filterOptions));
        }
        html.tdEnd();

        return html.toString();
    }

    /**
     * Retourne la liste des options de filtre pour une colonne.
     * 
     * @param model
     * @param column
     * @return filter drop list
     */
    @SuppressWarnings("unchecked")
    protected Collection<FilterOption> getFilterDropList(TableModel model, Column column)
    {
        List<FilterOption> droplist = new ArrayList<FilterOption>();

        Set<Object> options = new HashSet<Object>();

        Collection beans = model.getCollectionOfBeans();
        if (column.getFilteredDroplist()!=null && column.getFilteredDroplist())beans = model.getCollectionOfFilteredBeans();

        for (Iterator iter = beans.iterator(); iter.hasNext();)
        {
            Object bean = iter.next();
            try
            {
                Object obj = getFilterOption(column, bean);

                // on doit formater l'objet quand c'est une date sinon selon le format (si heure minute)
                // ce dera considéré comme 2 dates distinctes (si heures différentes par exemple)
                // on prend par défaut même parse que pattern

                if (column.isDate())
                {
                    obj =
                        ExtremeUtils.formatDate(this.findPattern(column), this.findPattern(column), obj, model.getLocale());

                }
                if ((obj != null) && !options.contains(obj))
                {
                    droplist.add(new Option(obj));
                    options.add(obj);
                }
            }
            catch (EcTableException exception)
            {
                log.debug("ERREUR LORS DE LA CONTRUCTION DE LA LISTE DEROULANTE.", exception);
            }
        }
        BeanComparator comparator = null;
        // comparator specifique à la date
        if (column.isDate())
        {
            comparator = new NullSafeBeanComparator("label", new DateComparator());
        }
        else
        {
            comparator = new NullSafeBeanComparator("label", new NullorNotComparableComparator());
        }
        Collections.sort(droplist, comparator);

        return droplist;
    }

    /**
     * Retourne un la valeur d'une cellule d'une colonne.
     * 
     * @param column
     * @param bean
     * @return filter option
     */
    protected Object getFilterOption(Column column, Object bean)
    {
        try
        {
            return PropertyUtils.getProperty(bean, column.getProperty());
        }
        catch (IllegalAccessException exception)
        {
            throw new EcTableException(exception);
        }
        catch (InvocationTargetException exception)
        {
            throw new EcTableException(exception);
        }
        catch (NoSuchMethodException exception)
        {
            throw new EcTableException(exception);
        }
        catch (IllegalArgumentException exception)
        {
            throw new EcTableException(exception);
        }
    }

    /**
     * Construit une liste déroulante html permettant de filtrer d'une colonne Formate les éléments de la liste si
     * l'attribut "cell" de la colonne prend une des valeurs suivantes :
     * booleanouinon,boolean10,booleanvraifaux,booleanvf,booleanon,boolean10.
     * 
     * @param model
     * @param column
     * @param droplist
     * @return texte HTML représentant une cellule de tableau contenant une liste déroulante
     */
    protected String dropListHtml(TableModel model, Column column, Collection<FilterOption> droplist)
    {
        HtmlBuilder html = new HtmlBuilder();
        Messages messages = model.getMessages();
       
        StringBuilder tdName = new StringBuilder();
        tdName.append(model.getTableHandler().prefixWithTableId());
        tdName.append(TableConstants.FILTER);
        tdName.append(column.getAlias());
        tdName.append(TableConstants.TDFILTER);
        html.id(tdName.toString());
        String styleClass = "";
        if (StringUtils.isNotEmpty(column.getFilterClass()))
        {
            styleClass = column.getFilterClass();
        }        
        StringBuilder   styleValue = new StringBuilder();      

        html.close();

        html.newline();
        html.tabs(2);

        StringBuilder selectName = new StringBuilder();
        selectName.append(model.getTableHandler().prefixWithTableId());
        selectName.append(TableConstants.FILTER);
        selectName.append(column.getAlias());
        String title = column.getTitle();        
        html.select().name(selectName.toString());
        html.ariaLabel("Filtrer " + column.getTitle() + ", " + html.SignToLitteralText("< <= >= > <> = *", true));

        StringBuilder   onkeypress = new StringBuilder();
        onkeypress.append(new TableActions(model).getFilterAction());
        html.onchange(onkeypress.toString());

        html.close();

        html.newline();
        html.tabs(2);
        // aucune sélection de filtre
        html.option().value("").close();
        if (column.getFilterNullOption() != null && column.getFilterNullOption() != "")
        {
            html.append(column.getFilterNullOption());
        }
        else
        {
            html.nbsp();
        }
        html.optionEnd();

        Locale locale = model.getLocale();

        for (FilterOption filterOption : droplist)
        {
            String valueItem = String.valueOf(filterOption.getValue());
            String labelItem = String.valueOf(filterOption.getLabel());

            if (column.isDate())
            {
                valueItem =
                    ExtremeUtils.formatDate(this.findPattern(column), this.findPattern(column), filterOption.getValue(), locale);
                labelItem =
                    ExtremeUtils.formatDate(this.findPattern(column), this.findPattern(column), filterOption.getLabel(), locale);
            }

            Class<? extends Object> clazzCell = findCellClass(column.getCell());

            if (clazzCell != null)
            {
                if (clazzCell.isAssignableFrom(BooleanCellOuiNon.class))
                {
                    labelItem = FormaterBoolean.booleanOuiNon((Boolean) filterOption.getLabel());
                    valueItem = FormaterBoolean.booleanOuiNon((Boolean) filterOption.getValue());
                }
                else if (clazzCell.isAssignableFrom(BooleanCellON.class))
                {
                    labelItem = FormaterBoolean.booleanON((Boolean) filterOption.getLabel());
                    valueItem = FormaterBoolean.booleanON((Boolean) filterOption.getValue());
                }
                else if (clazzCell.isAssignableFrom(BooleanCell10.class))
                {
                    labelItem = FormaterBoolean.boolean10((Boolean) filterOption.getLabel());
                    valueItem = FormaterBoolean.boolean10((Boolean) filterOption.getValue());
                }
                else if (clazzCell.isAssignableFrom(BooleanCellVF.class))
                {
                    labelItem = FormaterBoolean.booleanVF((Boolean) filterOption.getLabel());
                    valueItem = FormaterBoolean.booleanVF((Boolean) filterOption.getValue());
                }
                else if (clazzCell.isAssignableFrom(BooleanCellVraiFaux.class))
                {
                    labelItem = FormaterBoolean.booleanVraiFaux((Boolean) filterOption.getLabel());
                    valueItem = FormaterBoolean.booleanVraiFaux((Boolean) filterOption.getValue());
                }
                else if (clazzCell.isAssignableFrom(BooleanCellVF.class))
                {
                    labelItem = FormaterBoolean.booleanVraiFaux((Boolean) filterOption.getLabel());
                    valueItem = FormaterBoolean.booleanVraiFaux((Boolean) filterOption.getValue());
                }
            }

            html.newline();
            html.tabs(2);
            html.option().value(valueItem);

            if (valueItem.equals(column.getValueAsString()))
            {
                html.selected();
            }

            html.close();
            html.append(labelItem);
            html.optionEnd();

        }

        html.newline();
        html.tabs(2);
        html.selectEnd();

        return html.toString();
    }

    /**
     * Find cell class.
     * 
     * @param className
     * @return the class<? extends object>
     */
    private Class<? extends Object> findCellClass(String className)
    {
        Class<? extends Object> cellClass = null;
        try
        {
            cellClass = Class.forName(className);

        }
        catch (ClassNotFoundException exception)
        {
            throw new EcTableException("Cellule inconnue", exception);

        }
        return cellClass;
    }

    /**
     * Classe représentant un élément HTML Option.
     * 
     * @author wpetit-cp
     */
    protected static class Option implements FilterOption
    {

        /** label. */
        private final Object label;

        /** value. */
        private final Object value;

        /**
         * Instanciation de option.
         * 
         * @param obj
         */
        public Option(Object obj)
        {
            this.label = obj;
            this.value = obj;
        }

        /**
         * Instanciation de option.
         * 
         * @param label
         * @param value
         */
        public Option(Object label, Object value)
        {
            this.label = label;
            this.value = value;
        }

        /**
         * (methode de remplacement) {@inheritDoc}
         * 
         * @see fr.gouv.finances.lombok.apptags.table.cell.FilterOption#getLabel()
         */
        public Object getLabel()
        {
            return label;
        }

        /**
         * (methode de remplacement) {@inheritDoc}
         * 
         * @see fr.gouv.finances.lombok.apptags.table.cell.FilterOption#getValue()
         */
        public Object getValue()
        {
            return value;
        }
    }

    /**
     * Récupère la pattern de formatage associée à un type de cellule.
     * 
     * @param column
     * @return string
     */
    private String findPattern(Column column)
    {
        String pattern = null;
        String cellType = column.getCell();
        Object objectCell = null;

        try
        {
            objectCell = Class.forName(cellType).newInstance();
        }
        catch (ClassNotFoundException exception)
        {
            throw new EcTableException(exception);
        }
        catch (InstantiationException exception)
        {
            throw new EcTableException(exception);
        }
        catch (IllegalAccessException exception)
        {
            throw new EcTableException(exception);
        }

        if (objectCell instanceof PatternCell)
        {
            pattern = ((PatternCell) objectCell).getFormatPattern();
        }

        return pattern;
    }

}
