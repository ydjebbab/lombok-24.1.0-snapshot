/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : NodesTag.java
 *
 */
package fr.gouv.finances.lombok.apptags.tree.tag;

import javax.servlet.jsp.JspException;

import fr.gouv.finances.lombok.apptags.base.TagWriter;

/**
 * Class NodesTag Tag qui parcourt l'itérateur des noeuds de l'arbre et place les noeuds les uns après les autres dans
 * le contexte.
 * 
 * @author wpetit-cp
 * @version $Revision: 1.2 $ Date: 15 déc. 2009
 */
public class NodesTag extends NodeBaseTag
{

    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** tag writer. */
    private TagWriter tagWriter;

    public NodesTag()
    {
        super();
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @return int
     * @throws JspException the jsp exception
     * @see javax.servlet.jsp.tagext.TagSupport#doAfterBody()
     */
    @Override
    public int doAfterBody() throws JspException
    {
        this.writeFermetureLigneEtTableau();

        return EVAL_PAGE;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param tagWriter
     * @return int
     * @throws JspException the jsp exception
     * @see fr.gouv.finances.lombok.apptags.base.BaseAppTags#writeTagContent(fr.gouv.finances.lombok.apptags.base.TagWriter)
     */
    @Override
    protected int writeTagContent(TagWriter tagWriter) throws JspException
    {
        // Map du tag parent
        this.initParentTreeTag();

        this.tagWriter = tagWriter;

        int result = EVAL_PAGE;

        this.writeOuvertureLigneEtTableau();

        return result;
    }

    /**
     * methode Write fermeture ligne et tableau : --.
     * 
     * @throws JspException the jsp exception
     */
    private void writeFermetureLigneEtTableau() throws JspException
    {
        /*
         * </tr> </table></td> </tr>
         */
        // this.tagWriter.endTag();
        this.tagWriter.endTag();
        this.tagWriter.endTag();
        this.tagWriter.endTag();
    }

    /**
     * methode Write ouverture ligne et tableau : --.
     * 
     * @throws JspException the jsp exception
     */
    private void writeOuvertureLigneEtTableau() throws JspException
    {
        // Ouverture du tableau contenant un noeud
        // <tr><td>
        // <table class="tree" cellspacing="0" cellpadding="0" border="0">
        // <tr class="tree">
        // tagWriter.startTag(TR_TAG);
        // tagWriter.startTag(TD_TAG);
        tagWriter.startTag(DIV_TAG);
        tagWriter.writeOptionalAttributeValue(STYLE_ATTRIBUTE, "display: block");

        tagWriter.startTag(TABLE_TAG);
        tagWriter.writeOptionalAttributeValue(CLASS_ATTRIBUTE, this.parentTreeTag.getCssClass());
        tagWriter.writeOptionalAttributeValue(CELLSPACING_ATTRIBUTE, "0");
        tagWriter.writeOptionalAttributeValue(CELLPADDING_ATTRIBUTE, "0");
        tagWriter.writeOptionalAttributeValue(BORDER_ATTRIBUTE, "0");
        tagWriter.startTag(TR_TAG);
        tagWriter.writeOptionalAttributeValue(CLASS_ATTRIBUTE, this.parentTreeTag.getCssClass());
        tagWriter.forceBlock();
    }

}
