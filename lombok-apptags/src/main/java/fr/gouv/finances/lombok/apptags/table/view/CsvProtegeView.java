/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.apptags.table.view;

import org.apache.commons.lang.StringUtils;

import fr.gouv.finances.lombok.apptags.table.bean.Column;
import fr.gouv.finances.lombok.apptags.table.bean.Export;
import fr.gouv.finances.lombok.apptags.table.core.TableCache;
import fr.gouv.finances.lombok.apptags.table.core.TableModel;

/**
 * For producing a delimited datafile view of the table. Default delimiter is comma.
 */
public class CsvProtegeView implements View
{

    /** Constant : DELIMITER. */
    public static final String DELIMITER = "delimiter";

    /** Constant : DEFAULT_DELIMITER. */
    static final String DEFAULT_DELIMITER = ",";
    
    /** Constant : DELIMITER. */
    public static final String ENDS_WITH_DELIMITER = "endsWithDelimiter";

    /** Constant : DELIMITER. */
    public static final boolean DEFAULT_ENDS_WITH_DELIMITER = true;
    
    /** Constant : PROTECTION. */
    public static final String PROTECTION = "protection";

    /** Constant : DEFAULT_PROTECTION. */
    static final String DEFAULT_PROTECTION = "\"";

    /** Constant : INCLUDE_HEADER. */
    public static final String INCLUDE_HEADER = "includeHeader";

    /** Constant : DEFAULT_INCLUDE_HEADER. */
    static final boolean DEFAULT_INCLUDE_HEADER = true;

    /** Constant : COLONNE_PROTECTION_CLASS. */
    public static final String COLONNE_PROTECTION_CLASS = "colonneProtectionClass";

    /** Constant : INCLUDE_TITLE. */
    public static final String INCLUDE_TITLE = "includeTitle";

    /** plain data. */
    private StringBuilder plainData = new StringBuilder();

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.view.View#afterBody(fr.gouv.finances.lombok.apptags.table.core.TableModel)
     */
    @Override
    public Object afterBody(TableModel model)
    {
        return plainData.toString();
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.view.View#beforeBody(fr.gouv.finances.lombok.apptags.table.core.TableModel)
     */
    @Override
    public void beforeBody(TableModel model)
    {
        boolean group = false;
        if (isIncludeTitle(model) != null && !("").equalsIgnoreCase(isIncludeTitle(model)))
        {
            plainData.append(model.getExportHandler().getCurrentExport().getAttributeAsString(INCLUDE_TITLE));
            plainData.append("\r\n");
        }
        if (isIncludeHeader(model))
        {
            for (Column column : model.getColumnHandler().getFilterColumns())
            {
                if (getGroup(column) != null && StringUtils.isNotBlank(getGroup(column)))
                {
                    group = true;
                    String headerValue = getGroup(column);
                    appendToPlainData(model, column, headerValue);

                }

            }
            for (Column column : model.getColumnHandler().getFilterColumns())
            {
                if (getSubGroup(column) != null && StringUtils.isNotBlank(getSubGroup(column)))
                {
                    group = true;
                    String headerValue = getSubGroup(column);
                    appendToPlainData(model, column, headerValue);

                }
            }

            if (group == true)
            {
                // plainData.append("\r\n");
                for (Column column1 : model.getColumnHandler().getHeaderColumns())
                {

                    appendToPlainData(model, column1, column1.getTitle());

                }
            }
            else
            {
                for (Column column : model.getColumnHandler().getFilterColumns())
                {
                    String headerValue = ExportViewUtils.parseCSV(column.getTitle());
                    appendToPlainData(model, column, headerValue);
                }
            }

        }
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.view.View#body(fr.gouv.finances.lombok.apptags.table.core.TableModel,
     *      fr.gouv.finances.lombok.apptags.table.bean.Column)
     */
    @Override
    public void body(TableModel model, Column column)
    {
        String value = ExportViewUtils.parseCSV(column.getCellDisplay());
        appendToPlainData(model, column, value);
    }

    /**
     * Verifie si include header.
     * 
     * @param model
     * @return true, si c'est include header
     */
    public boolean isIncludeHeader(TableModel model)
    {
        Boolean result = DEFAULT_INCLUDE_HEADER;
        Export export = model.getExportHandler().getCurrentExport();
        String includeHeader = export.getAttributeAsString(INCLUDE_HEADER);

        if (includeHeader != null)
        {
            result = Boolean.parseBoolean(includeHeader);
        }
        return result;
    }

    /**
     * Verifie si endsWithDelimiter.
     * 
     * @param model
     * @return true, si la ligne doit être terminée par un séparateur de champs
     */
    public boolean isEndsWithDelimiter(TableModel model)
    {
        Boolean result = DEFAULT_ENDS_WITH_DELIMITER;
        Export export = model.getExportHandler().getCurrentExport();
        String endsWithDelimiter = export.getAttributeAsString(ENDS_WITH_DELIMITER);

        if (endsWithDelimiter != null)
        {
            result = Boolean.parseBoolean(endsWithDelimiter);
        }
        return result;
    }
    
    /**
     * methode Append to plain data :
     * 
     * @param model
     * @param column
     * @param value
     */
    private void appendToPlainData(TableModel model, Column column, String value)
    {
        Export export = model.getExportHandler().getCurrentExport();
        String delimiter = export.getAttributeAsString(DELIMITER);
        if (StringUtils.isBlank(delimiter))
        {
            delimiter = DEFAULT_DELIMITER;
        }

        String protection = export.getAttributeAsString(PROTECTION);
        String colonneProtectionClass = export.getAttributeAsString(COLONNE_PROTECTION_CLASS);

        if (StringUtils.isBlank(protection))
        {
            protection = DEFAULT_PROTECTION;
        }
        if (colonneProtectionClass != null)
        {
            protection = getProtection(colonneProtectionClass, column);
        }

        if (column.getValue() instanceof java.lang.String)
        {
            plainData.append(protection);
            plainData.append(value.trim());
            plainData.append(protection);
        }
        else
        {
            plainData.append(value.trim());
        }

        if (column.isLastColumn())
        {
            if (isEndsWithDelimiter(model)) 
            {                
                plainData.append(delimiter);
            }
            plainData.append("\r\n");
        }
        else 
        {
            plainData.append(delimiter);
        }
    }

    /**
     * Accesseur de l attribut protection.
     * 
     * @param colonneProtectionClass
     * @param column
     * @return protection
     */
    private String getProtection(String colonneProtectionClass, Column column)
    {
        return TableCache.getInstance().getColonneProtection(colonneProtectionClass).getColonneCsvProtection(column);
    }

    /**
     * methode Checks if is include title :
     * 
     * @param model
     * @return string
     */
    public String isIncludeTitle(TableModel model)
    {
        String includeTitle = "";
        Export export = model.getExportHandler().getCurrentExport();
        if (export != null)
        {
            includeTitle = export.getAttributeAsString(INCLUDE_TITLE);
        }
        return includeTitle;
    }

    /**
     * recuperer la valeur de "group"
     * 
     * @param column
     * @return
     */
    private static String getGroup(Column column)
    {
        return column.getAttributeAsString("group");
    }

    /**
     * recuperer la valeur de "subGroup"
     * 
     * @param column
     * @return
     */
    private static String getSubGroup(Column column)
    {
        return column.getAttributeAsString("subGroup");
    }

}
