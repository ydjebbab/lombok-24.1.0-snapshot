/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.apptags.table.view;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;

import fr.gouv.finances.lombok.apptags.table.bean.Column;
import fr.gouv.finances.lombok.apptags.table.cell.Cell;
import fr.gouv.finances.lombok.apptags.table.cell.DisabledSelectedCheckboxCell;
import fr.gouv.finances.lombok.apptags.table.cell.SelectedCheckboxCell;
import fr.gouv.finances.lombok.apptags.table.cell.SelectedCheckboxCellAttributes;
import fr.gouv.finances.lombok.apptags.table.core.TableModel;
import fr.gouv.finances.lombok.apptags.table.core.TableModelUtils;
import fr.gouv.finances.lombok.apptags.table.view.html.BuilderUtils;
import fr.gouv.finances.lombok.apptags.table.view.html.CpCalcBuilder;
import fr.gouv.finances.lombok.apptags.table.view.html.RowBuilder;
import fr.gouv.finances.lombok.apptags.table.view.html.TableBuilder;
import fr.gouv.finances.lombok.apptags.table.view.html.TwoColumnTableLayout;
import fr.gouv.finances.lombok.apptags.util.HtmlBuilder;

/**
 * Class CpResultsHtmlView
 */
public class CpResultsHtmlView extends AbstractHtmlView
{
    /**
     * Instanciation de cp results html view.
     */
    public CpResultsHtmlView()
    {
        super();
    }

    private static class GroupeColonnes
    {
        Map<String, String> titles;
        Map<String, Map<String, String>> subGroups;
    }

    /**
     * methode Header :
     * 
     * @param html
     * @param model
     */
    public void header(HtmlBuilder html, TableModel model)
    {
        getTableBuilder().tableStart(true);
        html.thead(0).close();

        List<Column> columns = model.getColumnHandler().getHeaderColumns();

        int rows = 1;
        Map<String, GroupeColonnes> groups = new HashMap<>();
        for (Column column: columns)
        {
            if ((column.getCell() != null)
                    && (column.getCell().equalsIgnoreCase(SelectedCheckboxCell.class.getCanonicalName())
                        || column.getCell().equalsIgnoreCase(DisabledSelectedCheckboxCell.class.getCanonicalName())))
            {
                throw new IllegalArgumentException("L'utilisation des cases à cocher n'est pas compatible avec les groupes.");
            }

            if (StringUtils.isNotBlank(column.getAttributeAsString("group")))
            {
                if (rows == 1)
                {
                    rows = 2;
                }
                // On récupère le descripteur du groupe (null s'il n'existe pas encore).
                GroupeColonnes group = groups.get(column.getAttributeAsString("group"));
                if (group == null)
                {
                    // On crée le descripteur de groupe et l'ajoute au distionnaire des groupes.
                    group = new GroupeColonnes();
                    group.titles = new HashMap<>();
                    group.subGroups = new HashMap<>();
                    groups.put(column.getAttributeAsString("group"), group);
                }
                if (StringUtils.isNotBlank(column.getAttributeAsString("subGroup")))
                {
                    if (rows == 2)
                    {
                        rows = 3;
                    }
                    Map<String, Map<String, String>> subGroups = group.subGroups;

                    // On récupère le dictionnaire du sous-groupe (null s'il n'existe pas encore).
                    Map<String, String> subGroup = subGroups.get(column.getAttributeAsString("subGroup"));
                    if (subGroup == null)
                    {
                        // On crée le dictionnaire de sous-groupe, et l'ajoute au dictionnaire des sous-groupes.
                        subGroup = new HashMap<>();
                        subGroups.put(column.getAttributeAsString("subGroup"), subGroup);
                    }
                    // Ajoute le titre au dictionnaire des sous-groupes.
                    subGroup.put(column.getTitle(), column.getTitle());
                }
                else
                {
                    Map<String, String> titles = group.titles;
                    titles.put(column.getTitle(), column.getTitle());
                }
            }
        }

        // Première ligne : groupes (ou sous-groupes, ou colonnes)
        Map<String, String> titles = null;
        Map<String, Map<String, String>> subGroups = null;
        String previousGroup = "";

        html.tr(1).close();

        columns = model.getColumnHandler().getColumns();
        for (Column column: columns)
        {
            if (StringUtils.isNotBlank(column.getAttributeAsString("group"))
                && !previousGroup.equals(column.getAttributeAsString("group")))
            {
                previousGroup = column.getAttributeAsString("group");

                titles = groups.get(column.getAttributeAsString("group")).titles;
                subGroups = groups.get(column.getAttributeAsString("group")).subGroups;

                Set<String> subGroupKeys = subGroups.keySet();
                Set<String> subGroupTitles = new HashSet<>();

                for (String sgKey: subGroupKeys)
                {
                    Map<String, String> subGroup = subGroups.get(sgKey);

                    if (subGroup.size() > 0)
                    {
                        subGroupTitles.addAll(subGroup.keySet());
                    }
                }

                html.td(2);

                if (StringUtils.isNotEmpty(column.getHeaderClass()))
                {
                    html.styleClass(column.getHeaderClass());
                }

                if (StringUtils.isNotEmpty(column.getHeaderStyle()))
                {
                    html.style(column.getHeaderStyle() + ";border-bottom-style: solid; border-bottom-width: 1px;");
                }
                else
                {
                    html.style("border-bottom-style: solid; border-bottom-width: 1px;");
                }

                if (StringUtils.isNotEmpty(column.getWidth()))
                {
                    html.width(column.getWidth());
                }

                int colspan = titles.size() + subGroupTitles.size();

                html.colSpan(Integer.toString(colspan));

                html.close();
                html.append(previousGroup);
                html.tdEnd();
            }
            else if (StringUtils.isBlank(column.getAttributeAsString("group")))
            {
                getHeaderColumn(model, column);
                html.append(column.getCellDisplay());
            }
        }

        html.trEnd(1);

        // Deuxième ligne : sous-groupes (ou colonnes)
        Map<String, String> subGroup = null;
        String previousSubGroup = "";

        if (rows > 1)
        {
            html.tr(1).close();

            for (Column column: columns)
            {
                if (StringUtils.isNotBlank(column.getAttributeAsString("subGroup")))
                {
                    if (!previousSubGroup.equals(column.getAttributeAsString("group")
                        + column.getAttributeAsString("subGroup")))
                    {
                        previousSubGroup =
                            column.getAttributeAsString("group") + column.getAttributeAsString("subGroup");

                        if (groups.get(column.getAttributeAsString("group")) != null)
                        {
                            subGroup = groups.get(column.getAttributeAsString("group")).subGroups.get(column.getAttributeAsString("subGroup"));
                        }

                        Set<String> subGroupTitles = new HashSet<>();

                        if (subGroup.size() > 0)
                        {
                            subGroupTitles = subGroup.keySet();
                        }

                        html.td(2);

                        if (StringUtils.isNotEmpty(column.getHeaderClass()))
                        {
                            html.styleClass(column.getHeaderClass());
                        }

                        if (StringUtils.isNotEmpty(column.getHeaderStyle()))
                        {
                            html.style(column.getHeaderStyle()
                                + ";border-bottom-style: solid; border-bottom-width: 1px;");
                        }
                        else
                        {
                            html.style(";border-bottom-style: solid; border-bottom-width: 1px;");
                        }

                        if (StringUtils.isNotEmpty(column.getWidth()))
                        {
                            html.width(column.getWidth());
                        }

                        html.colSpan("" + subGroupTitles.size());

                        html.close();
                        html.append(column.getAttributeAsString("subGroup"));
                        html.tdEnd();
                    }
                }
                else if (StringUtils.isNotBlank(column.getAttributeAsString("group")))
                {
                    if (rows > 2)
                    {
                        column.addAttribute("rowspan", "2");
                    }
                    getHeaderColumn(model, column);
                    html.append(column.getCellDisplay());
                }
            }

            html.trEnd(1);
        }

        // Troisième ligne : colonnes
        if (rows > 2)
        {
            html.tr(1).close();

            for (Column column: columns)
            {
                if (StringUtils.isNotBlank(column.getAttributeAsString("subGroup")))
                {
                    getHeaderColumn(model, column);
                    html.append(column.getCellDisplay());
                }
            }

            html.trEnd(1);
        }

        html.theadEnd(0);
    }

    /**
     * Accesseur de l attribut header column.
     * 
     * @param model
     * @param column
     * @return header column
     */
    private void getHeaderColumn(TableModel model, Column column)
    {
        Cell cell = TableModelUtils.getHeaderCell(column, column.getTitle());

        boolean isExported = model.getLimit().isExported();
        if (!isExported)
        {
            column.setCellDisplay(cell.getHtmlDisplay(model, column));
        }
        else
        {
            column.setCellDisplay(cell.getExportDisplay(model, column));
        }
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.view.HtmlView#afterBodyInternal(fr.gouv.finances.lombok.apptags.table.core.TableModel)
     */
    @Override
    protected void afterBodyInternal(TableModel model)
    {
        getCpCalcBuilder().defaultCalcLayout();

        getTableBuilder().tbodyEnd();

        getTableBuilder().tableEnd();

        boolean showPaginationBottom = BuilderUtils.showPaginationBottom(model);
        boolean showExportsBottom = BuilderUtils.showExportsBottom(model);

        if (showPaginationBottom || showExportsBottom)
        {
            BuilderUtils.setInPaginationBottom(model);
            toolbar(getHtmlBuilder(), getTableModel());
        }
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.view.AbstractHtmlView#init(fr.gouv.finances.lombok.apptags.util.HtmlBuilder,
     *      fr.gouv.finances.lombok.apptags.table.core.TableModel)
     */
    @Override
    protected void init(HtmlBuilder html, TableModel model)
    {
        setTableBuilder(new TableBuilder(html, model));
        setRowBuilder(new RowBuilder(html, model));
        setCpCalcBuilder(new CpCalcBuilder(html, model));
    }

    /**
     * methode Toolbar :
     * 
     * @param model
     */
    protected void toolbar(HtmlBuilder html, final TableModel model)
    {
        final TwoColumnTableLayout toolbar = new CpDefaultToolbar(getHtmlBuilder(), model);
        toolbar.layout();
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.view.HtmlView#beforeBodyInternal(fr.gouv.finances.lombok.apptags.table.core.TableModel)
     */
    @Override
    protected void beforeBodyInternal(final TableModel model)
    {
        boolean showPagination = BuilderUtils.showPagination(model);
        boolean showExports = BuilderUtils.showExports(model);

        if (showPagination || showExports)
        {
            toolbar(getHtmlBuilder(), getTableModel());
        }

        CpHtmlView2.formJavascript(getHtmlBuilder(), model);                
       
        getTableBuilder().statusRow();
        
        if (model.getTableHandler().getTable().isFilterable())
        {
            getTableBuilder().filterRow();
        }
        
        header(getHtmlBuilder(), model);

        getTableBuilder().tbodyStart();

    }

    protected void beforeBodyInternalResponsive(final TableModel model)
    {
        boolean showPagination = BuilderUtils.showPagination(model);
        boolean showExports = BuilderUtils.showExports(model);

        if (showPagination || showExports)
        {
            toolbar(getHtmlBuilder(), getTableModel());
        }

        CpHtmlView2.formJavascript(getHtmlBuilder(), model);

        getTableBuilder().tableStart(false);

        header(getHtmlBuilder(), model);
        getTableBuilder().filterRow();
        statusBar(getHtmlBuilder(), getTableModel());       

        getTableBuilder().tbodyStart();

    }

    protected void statusBar(HtmlBuilder html, TableModel model)
    {
        new CpStatusBar(html, model).layout();
    }

    /**
     * methode Form javascript :
     * 
     * @param html
     * @param model
     */
    public static void formJavascript(final HtmlBuilder html, final TableModel model)
    {

        final String tableId = model.getTableHandler().getTable().getTableId();
        final String form = model.getTableHandler().getTable().getForm();

        if (tableId != null && StringUtils.isNotBlank(tableId))
        {

            final SelectedCheckboxCellAttributes checkboxCellAttr = SelectedCheckboxCellAttributes.getDefault(tableId);

            // Javascript pour la gestion des checkboxes
            final StringBuilder javascript = new StringBuilder();

            javascript.append("<script  type=\"text/javascript\">/*<![CDATA[*/\n ");
            javascript.append("function ");
            javascript.append(checkboxCellAttr.getOnClickJavascript());
            javascript.append(tableId);
            javascript.append("(chkbx)  { ");
            javascript.append("if (chkbx == null)");
            javascript.append('{');
            javascript.append("return;");
            javascript.append('}');
            javascript.append("var hiddenField;");
            javascript.append("hiddenField = document.getElementById('");
            javascript.append(checkboxCellAttr.getPrefixIdHidden());
            javascript.append("' + chkbx.name);");
            javascript.append("if (chkbx.checked) {");
            javascript.append("if (typeof(hiddenField) != 'undefined')");
            javascript.append('{');
            javascript.append("hiddenField.value='SELECTED';");
            javascript.append('}');
            javascript.append('}');
            javascript.append("else {");
            javascript.append("if (typeof(hiddenField) != 'undefined')");
            javascript.append('{');
            javascript.append("hiddenField.value='UNSELECTED';");
            javascript.append('}');
            javascript.append('}');
            javascript.append("}\n");

            // Javascript permettant de sélectionner /déselectionner
            // toutes les cases à cocher d'une page
            javascript.append("function ");
            javascript.append(checkboxCellAttr.getOnClickSelectAll());
            javascript.append(tableId);
            javascript.append("(selector)");
            javascript.append('{');
            javascript.append("leFormulaireActif = document.forms." + form + ";");
            javascript.append("if (leFormulaireActif != null)");
            javascript.append(" {");
            javascript.append("lesElementsDuFormulaire = leFormulaireActif.elements;");
            javascript.append("for (var i = 0; i < lesElementsDuFormulaire.length; ++i)");
            javascript.append('{');
            javascript.append("var tagName = lesElementsDuFormulaire[i].tagName;");
            javascript.append("if (tagName == 'INPUT' || tagName == 'input')");
            javascript.append('{');
            javascript.append("var type = lesElementsDuFormulaire[i].type;");
            javascript.append("if (type == 'hidden' || type == 'checkbox')");
            javascript.append('{');
            javascript.append("var id = lesElementsDuFormulaire[i].id;");
            javascript.append("if (id.search('");
            javascript.append(checkboxCellAttr.getPrefixIdHidden());
            javascript.append("') != -1)");
            javascript.append('{');
            // mise à jour du champ caché
            javascript.append("if (selector.checked == false)");
            javascript.append('{');
            javascript.append("lesElementsDuFormulaire[i].value = '");
            javascript.append(checkboxCellAttr.getPatternUnselected());
            javascript.append("';");
            javascript.append('}');
            javascript.append("if (selector.checked == true)");
            javascript.append('{');
            javascript.append("lesElementsDuFormulaire[i].value = '");
            javascript.append(checkboxCellAttr.getPatternSelected());
            javascript.append("';");
            javascript.append('}');
            javascript.append('}');
            javascript.append("if (id.search('");
            javascript.append(checkboxCellAttr.getPrefixIdCheckbox());
            javascript.append("') != -1)");
            javascript.append('{');
            // mise à jour de la checkbox
            javascript.append("lesElementsDuFormulaire[i].checked = selector.checked;");
            javascript.append('}');
            javascript.append('}');
            javascript.append('}');
            javascript.append('}');
            javascript.append('}');
            javascript.append('}');

            javascript.append('\n');
            javascript.append("/*]]>*/");
            javascript.append("</script>");
            html.append(javascript.toString());
        }
    }
}