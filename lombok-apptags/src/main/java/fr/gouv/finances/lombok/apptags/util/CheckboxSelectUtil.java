/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.apptags.util;

import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.beanutils.PropertyUtilsBean;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.webflow.core.collection.MutableAttributeMap;
import org.springframework.webflow.core.collection.ParameterMap;
import org.springframework.webflow.execution.RequestContext;

import fr.gouv.finances.lombok.apptags.table.cell.PreferencesConstants;
import fr.gouv.finances.lombok.apptags.table.cell.SelectedCheckboxCellAttributes;

/**
 * Class CheckboxSelectUtil
 */
public class CheckboxSelectUtil
{

    /** Constant : log. */
    private static final Log log = LogFactory.getLog(CheckboxSelectUtil.class);

    /** Constant : ACCESSOR_ERROR. */
    private static final String ACCESSOR_ERROR = "Erreur d'exécution de la méthode get";

    /** controlenbelements. */
    private boolean controlenbelements = true;

    /**
     * Constructeur
     */
    private CheckboxSelectUtil()
    {
        super();
    }

    /**
     * Retourne true si la collection est composée d'au moins un élément.
     * 
     * @param elementsSelectionnes
     * @return true, si c'est vrai
     */
    public static boolean auMoinsUnElementSelectionne(Collection elementsSelectionnes)
    {
        return (elementsSelectionnes != null && !elementsSelectionnes.isEmpty()) ? true : false;
    }

    /**
     * Retourne true si la collection est composée d'un et d'un seul élément.
     * 
     * @param elementsSelectionnes
     * @return true, si c'est vrai
     */
    public static boolean unEtUnSeulElementSelectionne(Collection elementsSelectionnes)
    {
        return (elementsSelectionnes != null && elementsSelectionnes.size() == 1) ? true : false;
    }

    /**
     * Accesseur de l attribut elements selectionnes.
     * 
     * @param context context
     * @param tableId tableId
     * @return elements selectionnes
     */
    public static Object getElementsSelectionnes(final RequestContext context, String tableId)
    {
        Object result = null;
        CheckboxSelectUtil.traiterCheckboxesEntrePages(context, tableId);
        result = CheckboxSelectUtil.extraitLesElementsSelectionnes(context, tableId);
        return result;
    }

    /**
     * Place dans le contexte de flux (flowScope) la liste des "rowid" sélectionnés Cette liste est utilisée par la vue
     * extremecomponent pour activer les case à cocher.
     * 
     * @param context context
     */
    public static void traiterCheckboxesEntrePages(final RequestContext context)
    {
        CheckboxSelectUtil.traiterCheckboxesEntrePages(context, PreferencesConstants.TABLE_ID);
    }

    /**
     * Place dans le contexte de flux (flowScope) la liste des "rowid" sélectionnés Cette liste est utilisée par la vue
     * extremecomponent pour activer les case à cocher.
     * 
     * @param context context
     * @param tableId ivaleur du paramètre tableId utilisée dans le tag <ec:table>
     */
    public static void traiterCheckboxesEntrePages(final RequestContext context, final String tableId)
    {
        // Récupère l'objet de paramétrage
        SelectedCheckboxCellAttributes checkboxAttr =
            CheckboxSelectUtil.getSelectedCheckboxAttributesFromScopeOrDefault(context, tableId);

        // Lecture dans le contexte de flux de la liste des éléments
        // déjà sélectionnés
        Collection selectedRowids = (Collection) context.getFlowScope().get(checkboxAttr.getSelectedRowids());

        // Construction de la liste des éléments sélectionnés
        Collection selectedElements = remplaceLesElementsDuTableau(context, selectedRowids, checkboxAttr);

        // Stockage dans le contexte de flux des ID sélectionnés
        PreferencesConstants.getUsedScope(context, checkboxAttr.getSelectedRowidsStorageScope()).put(
            checkboxAttr.getSelectedRowids(), selectedElements);
    }

    /**
     * Enregistre dans le context de flux les éléments préselectionnés du tableau.
     * 
     * @param context context
     * @param preSelectedRowids collection contenant les rowid des éléments préselectionnés du tableau
     */
    public static void enregistreRowidsPreSelectionnesDansFlowScope(final RequestContext context,
        final Collection preSelectedRowids)
    {
        CheckboxSelectUtil.enregistreRowidsPreSelectionnesDansFlowScope(context, PreferencesConstants.TABLE_ID,
            preSelectedRowids);
    }

    /**
     * Enregistre dans le context de flux les éléments préselectionnés du tableau.
     * 
     * @param context context
     * @param tableId valeur du paramètre tableId utilisée dans le tag <ec:table>
     * @param preSelectedRowids collection contenant les rowid des éléments préselectionnés du tableau
     */
    public static void enregistreRowidsPreSelectionnesDansFlowScope(final RequestContext context, final String tableId,
        final Collection preSelectedRowids)
    {
        // Récupère l'objet de paramétrage
        SelectedCheckboxCellAttributes checkboxAttr =
            CheckboxSelectUtil.getSelectedCheckboxAttributesFromScopeOrDefault(context, tableId);

        // Enregiste les éléments pré-sélectionnés dans le contexte de
        // flux
        PreferencesConstants.getUsedScope(context, checkboxAttr.getSelectedRowidsStorageScope()).put(
            checkboxAttr.getSelectedRowids(), preSelectedRowids);

    }

    /**
     * Enregistre dans le contexte de flux l'objet de paramétrage des checkboxes.
     * 
     * @param context context
     * @return l'objet de paramétrage
     */
    public static SelectedCheckboxCellAttributes parametrerCheckboxes(final RequestContext context)
    {
        return CheckboxSelectUtil.parametrerCheckboxes(context, PreferencesConstants.TABLE_ID);
    }

    /**
     * Enregistre dans le contexte de flux l'objet de paramétrage des checkboxes.
     * 
     * @param context context
     * @param tableId valeur du paramètre tableId utilisée dans le tag <ec:table>
     * @return l'objet de paramétrage
     */
    public static SelectedCheckboxCellAttributes parametrerCheckboxes(final RequestContext context, final String tableId)
    {
        SelectedCheckboxCellAttributes checkboxAttr = SelectedCheckboxCellAttributes.getDefault(tableId);
        // Sauvegarde de l'objet de paramétrage dans le FlowScope
        // Si plusieurs liste sont présentes dans la vue contenant le
        // tableau, il
        // faut stocker l'objet de paramétrage dans un attribut du
        // FlowScope qui
        // est nommé <valeur de l'attribut tableId indiqué dans
        // <ec:column> , suffixé par "_parametrage"

        PreferencesConstants.getUsedScope(context, checkboxAttr.getCheckboxattrStorageScope()).put(
            checkboxAttr.getScopeName(), checkboxAttr);

        return checkboxAttr;
    }

    /**
     * Retourne l'objet de paramétrage par défaut des checkboxes.
     * 
     * @param tableId valeur du paramètre tableId utilisée dans le tag <ec:table>
     * @return parametrage checkboxes par defaut
     */
    public static SelectedCheckboxCellAttributes getParametrageCheckboxesParDefaut(final String tableId)
    {
        return SelectedCheckboxCellAttributes.getDefault(tableId);
    }

    /**
     * Retourne l'objet de paramétrage par défaut des checkboxes.
     * 
     * @return parametrage checkboxes par defaut
     */
    public static SelectedCheckboxCellAttributes getParametrageCheckboxesParDefaut()
    {
        return SelectedCheckboxCellAttributes.getDefault();
    }

    /**
     * Copie le message d'erreur retourné par le sous-flux au flux parent du FlowScope vers le RequestScope Puis
     * supprime ce message du FlowScope.
     * 
     * @param context context
     */
    public static void traiterMessageErreurRetourne(final RequestContext context)
    {
        CheckboxSelectUtil.traiterMessageErreurRetourne(context, null);
    }

    /**
     * Copie le message d'erreur retourné par le sous-flux au flux parent du FlowScope vers le RequestScope Puis
     * supprime ce message du FlowScope.
     * 
     * @param context context
     * @param tableId valeur du paramètre tableId utilisée dans le tag <ec:table>
     */
    public static void traiterMessageErreurRetourne(final RequestContext context, final String tableId)
    {
        String attributeName = SelectedCheckboxCellAttributes.getScopeName(tableId);
        // Récupère l'objet de paramétrage
        SelectedCheckboxCellAttributes checkboxAttr =
            CheckboxSelectUtil.getSelectedCheckboxAttributesFromScopeOrDefault(context, attributeName);

        if (context.getFlowScope().contains(checkboxAttr.getMsgErrorInFlow()))
        {
            String messageErreur = (String) context.getFlowScope().get(checkboxAttr.getMsgErrorInFlow());
            context.getRequestScope().put(checkboxAttr.getMsgErrorInFlow(), messageErreur);
            context.getFlowScope().remove(checkboxAttr.getMsgErrorInFlow());
        }

    }

    /**
     * Lit dans le contexte (flowScope) l'objet de paramétrage des checkbox. Le nom sous lequel l'objet est stocké dans
     * le contexte est construit à partir du paramètre tableId. Si l'objet n'est pas dans le contexte,l'objet de
     * paramétrage par défaut est construit, puis stocké dans le contexte de flux
     * 
     * @param context context
     * @param tableId valeur du paramètre tableId utilisée dans le tag <ec:table>
     * @return objet de paramétrage des checkboxes
     */
    private static SelectedCheckboxCellAttributes getSelectedCheckboxAttributesFromScopeOrDefault(
        final RequestContext context, final String tableId)
    {
        return CheckboxSelectUtil.getSelectedCheckboxAttributesFromScopeOrDefault(context, tableId,
            PreferencesConstants.DEFAULT_CHEXBOXATTR_STORAGE_SCOPE);
    }

    /**
     * Lit dans le contexte (flowScope ou conversationScope) l'objet de paramétrage des checkbox. Le nom sous lequel
     * l'objet est stocké dans le contexte est construit à partir du paramètre tableId. Si l'objet n'est pas dans le
     * contexte,l'objet de paramétrage par défaut est construit, puis stocké dans le contexte de flux
     * 
     * @param context context
     * @param tableId valeur du paramètre tableId utilisée dans le tag <ec:table>
     * @param scope scope
     * @return objet de paramétrage des checkboxes
     */
    private static SelectedCheckboxCellAttributes getSelectedCheckboxAttributesFromScopeOrDefault(
        final RequestContext context, final String tableId, final int scope)
    {
        SelectedCheckboxCellAttributes result = null;
        String attributeName = SelectedCheckboxCellAttributes.getScopeName(tableId);

        MutableAttributeMap usedScope = PreferencesConstants.getUsedScope(context, scope);

        // On cherche l'objet de paramétrage dans le contexte de flux
        if (attributeName != null && StringUtils.isNotBlank(attributeName) && usedScope.contains(attributeName))
        {
            Object parametrage = usedScope.get(attributeName);
            if (parametrage == null)
            {
                throw new EcTableException("L'attribut " + attributeName + " est null. L'objet de paramétrage de type "
                    + "SelectedCheckboxAttributes n'est pas dans le contexte");
            }

            if (parametrage instanceof SelectedCheckboxCellAttributes)
            {
                result = (SelectedCheckboxCellAttributes) parametrage;
            }
            else
            {
                throw new EcTableException("L'attribut " + attributeName
                    + " stocké dans le FlowScope ou ConversationScope n'est pas du type SelectedCheckboxCellAttributes");
            }
        }

        // Si on ne le trouve pas ou si on utilise la valeur par défaut
        // de l'objet de paramétrage
        else
        {
            result = SelectedCheckboxCellAttributes.getDefault(tableId);
            usedScope.put(attributeName, result);
        }

        return result;
    }

    /**
     * Extrait d'une requête contenant l'ensemble des paramètres d'une requête dont le nom est préfixé par
     * selectedCheckboxCellAttributesgetPrefixHiddenField() et la valeur est
     * selectedCheckboxCellAttributes.getPatternSelected(). Ajoute à la liste construite le nom du paramètre moins le
     * préfixe getPrefixHiddenField(). La liste ne peut contenir qu'une fois le même paramètre.
     * 
     * @param context context
     * @param rowids rowids
     * @param checkboxAttr checkboxAttr
     * @return List
     */
    @SuppressWarnings("unchecked")
    private static Collection remplaceLesElementsDuTableau(final RequestContext context, final Collection rowids,
        final SelectedCheckboxCellAttributes checkboxAttr)
    {
        ParameterMap requestParameterMap = context.getExternalContext().getRequestParameterMap();

        String prefixHiddenField = checkboxAttr.getPrefixNameHidden();
        String patternSelected = checkboxAttr.getPatternSelected();
        String rowCountSep = checkboxAttr.getRowcountSep();

        Collection newRowids = (rowids == null) ? new ArrayList() : rowids;

        // Enumeration parameterNames = request.getParameterNames();
        // Avant webflow 1.0
        // Iterator parameterNames =
        // requestParameterMap.getMap().keySet().iterator();

        Iterator parameterNames = requestParameterMap.asMap().keySet().iterator();
        // Mises à jour de la collection des éléments sélectionnés
        // ie ajout ou suppression d'éléments en fonction de la
        // valeur du paramètre prefixHiddenField
        while (parameterNames.hasNext())
        {
            String parameterName = (String) parameterNames.next();
            if (parameterName.startsWith(prefixHiddenField))
            {
                String identifiantDeLaligne;
                String parameterNameWithoutPref = StringUtils.substringAfter(parameterName, prefixHiddenField);

                if (checkboxAttr.getRowId().equalsIgnoreCase(PreferencesConstants.ROWNUMBER_SELECTION)
                    || checkboxAttr.getRowId().equalsIgnoreCase(PreferencesConstants.HASHCODE_SELECTION))
                {
                    identifiantDeLaligne = StringUtils.substringBefore(parameterNameWithoutPref, rowCountSep);
                }
                else
                {
                    identifiantDeLaligne = StringUtils.substringAfter(parameterNameWithoutPref, rowCountSep);
                }

                String parameterValue = requestParameterMap.get(parameterName);

                if (parameterValue.equals(patternSelected))
                {
                    if (!newRowids.contains(identifiantDeLaligne))
                    {
                        newRowids.add(identifiantDeLaligne);
                    }
                }
                else
                {
                    if (newRowids.contains(identifiantDeLaligne))
                    {
                        newRowids.remove(identifiantDeLaligne);
                    }
                }
            }
        }
        return newRowids;
    }

    /**
     * Vide la liste contenant les rowids des éléments sélectionnés.
     * 
     * @param context context
     * @param tableId valeur du paramètre tableId utilisée dans le tag <ec:table>
     * @param checkboxAttr checkboxAttr
     */
    private static void razElementsSelectionnes(final RequestContext context, final String tableId,
        final SelectedCheckboxCellAttributes checkboxAttr)
    {
        // Contrôle de la présence des attributs dans le contexte de
        // flux
        List selectedRowids =
            (List) PreferencesConstants.getObjectFromScope(context, checkboxAttr.getSelectedRowids(),
                PreferencesConstants.FLOW_SCOPE);

        if (selectedRowids != null)
        {
            selectedRowids.clear();
        }

    }

    /**
     * Met à jour dans le sous-flux la collection des éléments affichée dans le tableau.
     * 
     * @param context context
     * @param elements elements
     */
    public static void remplaceLesElementsDuTableau(final RequestContext context, final Collection elements)
    {
        CheckboxSelectUtil.remplaceLesElementsDuTableau(context, elements, PreferencesConstants.TABLE_ID);
    }

    /**
     * Met à jour dans le sous-flux la collection des éléments affichée dans le tableau.
     * 
     * @param context context
     * @param elements elements
     * @param tableId valeur du paramètre tableId utilisée dans le tag <ec:table>
     */
    public static void remplaceLesElementsDuTableau(final RequestContext context, final Collection elements,
        final String tableId)
    {
        // Récupère l'objet de paramétrage
        SelectedCheckboxCellAttributes checkboxAttr =
            CheckboxSelectUtil.getSelectedCheckboxAttributesFromScopeOrDefault(context, tableId);

        context.getFlowScope().put(checkboxAttr.getElementsInFlow(), elements);
    }

    /**
     * Ajoute un élément dans la collection qui sert de support au tableau.
     * 
     * @param context context
     * @param element élément à ajouter
     */
    public static void ajouteUnElementDansLeTableau(final RequestContext context, final Object element)
    {
        CheckboxSelectUtil.ajouteUnElementDansLeTableau(context, element, PreferencesConstants.TABLE_ID);
    }

    /**
     * Ajoute un élément dans la collection qui sert de support au tableau.
     * 
     * @param context context
     * @param element élément à ajouter
     * @param tableId tableId valeur du paramètre tableId utilisée dans le tag <ec:table>
     */
    @SuppressWarnings("unchecked")
    public static void ajouteUnElementDansLeTableau(final RequestContext context, final Object element,
        final String tableId)
    {
        // Récupère l'objet de paramétrage
        SelectedCheckboxCellAttributes checkboxAttr =
            CheckboxSelectUtil.getSelectedCheckboxAttributesFromScopeOrDefault(context, tableId);

        // Ajout d'un élément dans la liste des éléments en entrée du
        // flux
        Collection elements =
            (Collection) PreferencesConstants.getObjectFromScope(context, checkboxAttr.getElementsInFlow(),
                PreferencesConstants.FLOW_SCOPE);
        if (elements != null)
        {
            elements.add(element);
        }
    }

    /**
     * Supprime une collection d'éléments dans la collection qui sert de support au tableau.
     * 
     * @param context context
     * @param elementsASupprimer elementsASupprimer
     */
    public static void supprimeDesElementsDansLeTableau(final RequestContext context,
        final Collection elementsASupprimer)
    {
        CheckboxSelectUtil.supprimeDesElementsDansLeTableau(context, elementsASupprimer, PreferencesConstants.TABLE_ID);
    }

    /**
     * Supprime une collection d'éléments dans la collection qui sert de support au tableau.
     * 
     * @param context context
     * @param elementsASupprimer elementsASupprimer
     * @param tableId tableId valeur du paramètre tableId utilisée dans le tag <ec:table>
     */
    @SuppressWarnings("unchecked")
    public static void supprimeDesElementsDansLeTableau(final RequestContext context,
        final Collection elementsASupprimer, final String tableId)
    {
        // Récupère l'objet de paramétrage
        SelectedCheckboxCellAttributes checkboxAttr =
            CheckboxSelectUtil.getSelectedCheckboxAttributesFromScopeOrDefault(context, tableId);

        // Suppression de la collection d'éléments dans la liste des
        // éléments en entrée du flux
        Collection elements =
            (Collection) PreferencesConstants.getObjectFromScope(context, checkboxAttr.getElementsInFlow(),
                PreferencesConstants.FLOW_SCOPE);

        if (elements != null && elements.size() > 0)
        {
            elements.removeAll(elementsASupprimer);
        }
    }

    /**
     * Supprime un élément dans la collection qui sert de support au tableau.
     * 
     * @param context context
     * @param element élément à ajouter
     */
    public static void supprimeUnElementDansLeTableau(final RequestContext context, final Object element)
    {
        CheckboxSelectUtil.supprimeUnElementDansLeTableau(context, element, PreferencesConstants.TABLE_ID);
    }

    /**
     * Supprime un élément dans la collection qui sert de support au tableau.
     * 
     * @param context context
     * @param element élément à ajouter
     * @param tableId tableId valeur du paramètre tableId utilisée dans le tag <ec:table>
     */
    public static void supprimeUnElementDansLeTableau(final RequestContext context, final Object element,
        final String tableId)
    {
        // Récupère l'objet de paramétrage
        SelectedCheckboxCellAttributes checkboxAttr =
            CheckboxSelectUtil.getSelectedCheckboxAttributesFromScopeOrDefault(context, tableId);

        // Supprime un élément dans la liste des éléments en entrée du
        // flux
        Collection elements =
            (Collection) PreferencesConstants.getObjectFromScope(context, checkboxAttr.getElementsInFlow(),
                PreferencesConstants.FLOW_SCOPE);
        if (elements != null && elements.size() > 0)
        {
            elements.remove(element);
        }
    }

    /**
     * Contrôle qu'une collection contient un et un seul élément Retourne un message d'erreur si ce n'est pas le cas.
     * 
     * @param context context
     * @param selectedElements collection à contrôler
     * @param checkboxAttr checkboxAttr
     * @param scope scope
     * @return String
     */
    private static Object valideUnEtUnSeulObjetSelectionne(final RequestContext context,
        final Collection selectedElements, final SelectedCheckboxCellAttributes checkboxAttr, final int scope)
    {
        Object selectedElement = null;
        String messageErreur = null;

        if (selectedElements == null || selectedElements.size() == 0)
        {
            messageErreur = checkboxAttr.getMsgNotSelected();
        }
        else if (selectedElements.size() > 1)
        {
            messageErreur = checkboxAttr.getMsgNotMany();
        }
        else
        {
            Iterator selectedElementsI = selectedElements.iterator();
            if (selectedElementsI.hasNext())
            {
                Object element = selectedElementsI.next();
                if (element != null)
                {
                    selectedElement = element;
                }
            }
        }

        MutableAttributeMap usedScope = PreferencesConstants.getUsedScope(context, scope);
        usedScope.put(checkboxAttr.getMsgErrorInFlow(), messageErreur);

        return selectedElement;
    }

    /**
     * Contrôle qu'une collection contient au moins un élément.
     * 
     * @param context context
     * @param selectedElements collection à contrôler
     * @param checkboxAttr checkboxAttr
     * @param scope scope
     * @return String
     */
    private static Collection valideAuMoinsUnObjetSelectionne(final RequestContext context,
        final Collection selectedElements, final SelectedCheckboxCellAttributes checkboxAttr, final int scope)
    {
        if (selectedElements == null || selectedElements.size() == 0)
        {
            MutableAttributeMap usedScope = PreferencesConstants.getUsedScope(context, scope);
            usedScope.put(checkboxAttr.getMsgErrorInFlow(), checkboxAttr.getMsgNotSelected());
        }
        else
        {
            PreferencesConstants.getUsedScope(context, scope).put(checkboxAttr.getSelectedInFlow(), selectedElements);
        }
        return selectedElements;
    }

    /**
     * Extrait l'élément sélectionné dans le flux parent et l'enregistre dans le flowScope. Si aucun élément n'est
     * sélectionné ou si plus d'un élément est sélectionné enregistre un message d'erreur dans le flowScope et retourne
     * null
     * 
     * @param context context
     * @return l'élément sélectionné dans le tableau
     */
    public static Object extraitUnElementSelectionneDansLeFluxParent(final RequestContext context)
    {
        return CheckboxSelectUtil.extraitUnElementSelectionneDansLeFluxParent(context, PreferencesConstants.TABLE_ID);
    }

    /**
     * Extrait l'élément sélectionné dans le flux parent et l'enregistre dans le flowScope. Si aucun élément n'est
     * sélectionné ou si plus d'un élément est sélectionné enregistre un message d'erreur dans le flowScope et retourne
     * null
     * 
     * @param context context
     * @param tableId valeur du paramètre tableId utilisée dans le tag <ec:table>
     * @return l'élément sélectionné dans le tableau
     */
    public static Object extraitUnElementSelectionneDansLeFluxParent(final RequestContext context, final String tableId)
    {
        // Récupère l'objet de paramétrage
        SelectedCheckboxCellAttributes checkboxAttr =
            CheckboxSelectUtil.getSelectedCheckboxAttributesFromScopeOrDefault(context, tableId);
        Collection selectedElements = CheckboxSelectUtil.extraitLesElementsSelectionnes(context, tableId);
        Object selectedElement =
            CheckboxSelectUtil.valideUnEtUnSeulObjetSelectionne(context, selectedElements, checkboxAttr,
                PreferencesConstants.FLOW_SCOPE);
        if (selectedElement != null)
        {
            CheckboxSelectUtil.razElementsSelectionnes(context, tableId, checkboxAttr);
        }
        return selectedElement;
    }

    /**
     * Extrait l'élément sélectionné dans le flux et l'enregistre dans le requestScope. Si aucun élément n'est
     * sélectionné ou si plus d'un élément est sélectionné enregistre un message d'erreur dans le requestScope et
     * retourne null
     * 
     * @param context context
     * @return l'élément sélectionné dans le tableau
     */
    public static Object extraitUnElementSelectionneDansLeFlux(final RequestContext context)
    {
        return CheckboxSelectUtil.extraitUnElementSelectionneDansLeFlux(context, PreferencesConstants.TABLE_ID);
    }

    /**
     * Extrait l'élément sélectionné dans le flux et l'enregistre dans le requestScope. Si aucun élément n'est
     * sélectionné ou si plus d'un élément est sélectionné enregistre un message d'erreur dans le requestScope et
     * retourne null
     * 
     * @param context context
     * @param tableId valeur du paramètre tableId utilisée dans le tag <ec:table>
     * @return l'élément sélectionné dans le tableau
     */
    public static Object extraitUnElementSelectionneDansLeFlux(final RequestContext context, final String tableId)
    {
        // Récupère l'objet de paramétrage
        SelectedCheckboxCellAttributes checkboxAttr =
            CheckboxSelectUtil.getSelectedCheckboxAttributesFromScopeOrDefault(context, tableId);
        Collection selectedElements = CheckboxSelectUtil.extraitLesElementsSelectionnes(context, tableId);
        Object selectedElement =
            CheckboxSelectUtil.valideUnEtUnSeulObjetSelectionne(context, selectedElements, checkboxAttr,
                PreferencesConstants.REQUEST_SCOPE);
        if (selectedElement != null)
        {
            CheckboxSelectUtil.razElementsSelectionnes(context, tableId, checkboxAttr);
        }
        return selectedElement;
    }

    /**
     * Extrait la liste des éléments sélectionnés dans le flux et l'enregistre dans le requestScope. Si aucun élément
     * n'est sélectionné, enregistre un message d'erreur dans le requestScope et retourne null
     * 
     * @param context context
     * @return la liste des éléments sélectionnés
     */
    public static Collection extraitLesElementsSelectionnesDansLeFlux(final RequestContext context)
    {
        return CheckboxSelectUtil.extraitLesElementsSelectionnes(context, PreferencesConstants.TABLE_ID);
    }

    /**
     * Extrait la liste des éléments sélectionnés dans le flux et l'enregistre dans le contexte Si aucun élément n'est
     * sélectionné, enregistre un message d'erreur dans le scope et retourne null.
     * 
     * @param context context
     * @param tableId valeur du paramètre tableId utilisée dans le tag <ec:table>
     * @return la liste des éléments sélectionnés
     */
    public static Collection extraitLesElementsSelectionnesDansLeFlux(final RequestContext context, final String tableId)
    {
        return extraitLesElementsSelectionnesDansLeFlux(context, tableId, true);
    }

    /**
     * Extrait la liste des éléments sélectionnés dans le flux et l'enregistre dans le contexte Si aucun élément n'est
     * sélectionné, enregistre un message d'erreur dans le scope et retourne null.
     * 
     * @param context context
     * @param tableId valeur du paramètre tableId utilisée dans le tag <ec:table>
     * @param controlenbelements controlenbelements
     * @return la liste des éléments sélectionnés
     */
    public static Collection extraitLesElementsSelectionnesDansLeFlux(final RequestContext context, final String tableId,
        final boolean controlenbelements)
    {
        // Récupère l'objet de paramétrage
        SelectedCheckboxCellAttributes checkboxAttr =
            CheckboxSelectUtil.getSelectedCheckboxAttributesFromScopeOrDefault(context, tableId);

        Collection selectedElements = CheckboxSelectUtil.extraitLesElementsSelectionnes(context, tableId);
        if (controlenbelements == true)
        {
            selectedElements =
                CheckboxSelectUtil.valideAuMoinsUnObjetSelectionne(context, selectedElements, checkboxAttr,
                    PreferencesConstants.REQUEST_SCOPE);
        }
        // une fois les éléments extraits, leur rowid est retiré de la
        // liste des rowids sélectionnés
        if (selectedElements != null)
        {
            CheckboxSelectUtil.razElementsSelectionnes(context, tableId, checkboxAttr);
        }
        return selectedElements;
    }

    /**
     * Extrait la liste des éléments sélectionnés dans le flux et l'enregistre dans le requestScope. Si aucun élément
     * n'est sélectionné, enregistre un message d'erreur dans le requestScope et retourne null
     * 
     * @param context context
     * @return la liste des éléments sélectionnés
     */
    public static Collection extraitLesElementsSelectionnesDansLeFluxParent(final RequestContext context)
    {
        return CheckboxSelectUtil
            .extraitLesElementsSelectionnesDansLeFluxParent(context, PreferencesConstants.TABLE_ID);
    }

    /**
     * Extrait la liste des éléments sélectionnés dans le flux parent et l'enregistre dans le flowScope. Si aucun
     * élément n'est sélectionné, enregistre un message d'erreur dans le requestScope et retourne null
     * 
     * @param context context
     * @param tableId valeur du paramètre tableId utilisée dans le tag <ec:table>
     * @return la liste des éléments sélectionnés
     */
    public static Collection extraitLesElementsSelectionnesDansLeFluxParent(final RequestContext context,
        final String tableId)
    {
        // Récupère l'objet de paramétrage
        SelectedCheckboxCellAttributes checkboxAttr =
            CheckboxSelectUtil.getSelectedCheckboxAttributesFromScopeOrDefault(context, tableId);

        Collection selectedElements = CheckboxSelectUtil.extraitLesElementsSelectionnes(context, tableId);
        selectedElements =
            CheckboxSelectUtil.valideAuMoinsUnObjetSelectionne(context, selectedElements, checkboxAttr,
                PreferencesConstants.FLOW_SCOPE);

        if (selectedElements != null)
        {
            CheckboxSelectUtil.razElementsSelectionnes(context, tableId, checkboxAttr);
        }

        return selectedElements;
    }

    /**
     * Extrait la liste des éléments sélectionnés dans le tableau.
     * 
     * @param context context
     * @param tableId valeur du paramètre tableId utilisée dans le tag <ec:table>
     * @return la liste des éléments sélectionnés
     */
    @SuppressWarnings("unchecked")
    private static Collection extraitLesElementsSelectionnes(final RequestContext context, final String tableId)
    {
        Collection elements = null;
        Collection selectedRowids = null;
        Collection selectedElements = new ArrayList<Object>();

        // Récupère l'objet de paramétrage
        SelectedCheckboxCellAttributes checkboxAttr =
            CheckboxSelectUtil.getSelectedCheckboxAttributesFromScopeOrDefault(context, tableId);

        if (log.isDebugEnabled() && checkboxAttr != null)
        {
            log.debug("Identification des lignes du tableau : " + checkboxAttr.getRowId());
            log.debug("tableId : " + checkboxAttr.getTableId());
        }

        // Recupération des paramètres en entrée du Flux
        elements =
            (Collection) PreferencesConstants.getObjectFromScope(context, checkboxAttr.getElementsInFlow(),
                PreferencesConstants.FLOW_SCOPE);
        selectedRowids =
            (Collection) PreferencesConstants.getObjectFromScope(context, checkboxAttr.getSelectedRowids(),
                PreferencesConstants.FLOW_SCOPE);

        if (elements != null && selectedRowids != null)
        {
            selectedElements =
                CheckboxSelectUtil.extraitDeLaListeLesElementsSelectionnes(new ArrayList(elements), selectedRowids,
                    checkboxAttr);
        }
        else
        {
            if (log.isDebugEnabled())
            {
                if (elements == null)
                {
                    log.debug("La collection contenant les éléments à sélectionner est vide");
                }
                if (selectedRowids == null)
                {
                    log.debug("La collection les rowids sélectionnés est vide");
                }
            }
        }

        return selectedElements;
    }

    /**
     * Extrait de la liste complète des éléments ceux qui ont été sélectionnés.
     * 
     * @param elements elements
     * @param selectedRowids selectedRowids
     * @param checkboxAttr checkboxAttr
     * @return collection
     */
    private static Collection extraitDeLaListeLesElementsSelectionnes(final Collection elements,
        final Collection selectedRowids, final SelectedCheckboxCellAttributes checkboxAttr)
    {
        Collection<Object> result = new ArrayList<Object>();
        if (elements != null && selectedRowids != null)
        {

            if (log.isDebugEnabled())
            {
                StringBuilder msg = new StringBuilder();
                msg.append("NOMBRE D'ELEMENTS DANS LA COLLECTION : ");
                msg.append(elements.size());
                msg.append("NOMBRE DE ROWIDS SELECTIONNES : ");
                msg.append(selectedRowids.size());
                log.debug(msg.toString());
            }

            for (Iterator iter = selectedRowids.iterator(); iter.hasNext();)
            {
                String selectedRowid = (String) iter.next();
                Object selectedElement =
                    extraitUnElementSelectionneDeLaListeDesElements(elements, selectedRowid, checkboxAttr);
                if (selectedElement != null)
                {
                    result.add(selectedElement);
                }
            }
        }
        return result;
    }

    /**
     * Extrait de la liste des éléments, l'élément identifié par son rowid.
     * 
     * @param elements elements
     * @param selectedRowids selectedRowids
     * @param checkboxAttr checkboxAttr
     * @return object
     */
    private static Object extraitUnElementSelectionneDeLaListeDesElementsParSonId(final Collection elements,
        final String selectedRowids, final SelectedCheckboxCellAttributes checkboxAttr)
    {
        Object result = null;

        if (log.isDebugEnabled())
        {
            log.debug("identifiant recherché : " + selectedRowids);
        }

        PropertyUtilsBean propUtilsBean = new PropertyUtilsBean();
        for (Iterator iter = elements.iterator(); iter.hasNext();)
        {
            Object element = iter.next();
            if (element != null)
            {

                try
                {
                    PropertyDescriptor propDescriptor =
                        propUtilsBean.getPropertyDescriptor(element, checkboxAttr.getRowId());
                    if (propDescriptor == null)
                    {
                        throw new EcTableException(ACCESSOR_ERROR + checkboxAttr.getRowId());
                    }

                    Method method = propUtilsBean.getReadMethod(propDescriptor);
                    if (method != null)
                    {
                        Object rowid = method.invoke(element, (Object[]) null);

                        if (log.isDebugEnabled())
                        {
                            log.debug("identifiant en cours : " + rowid);
                        }

                        if (rowid != null && rowid.toString().equals(selectedRowids))
                        {
                            result = element;
                        }
                    }
                    else
                    {
                        throw new EcTableException(ACCESSOR_ERROR + checkboxAttr.getRowId());
                    }

                }
                catch (SecurityException secuExc)
                {
                    throw new EcTableException(ACCESSOR_ERROR + checkboxAttr.getRowId(), secuExc);
                }
                catch (NoSuchMethodException nomethe)
                {
                    throw new EcTableException(ACCESSOR_ERROR + checkboxAttr.getRowId(), nomethe);
                }
                catch (IllegalAccessException ile)
                {
                    throw new EcTableException(ACCESSOR_ERROR + checkboxAttr.getRowId(), ile);
                }
                catch (InvocationTargetException inve)
                {
                    throw new EcTableException(ACCESSOR_ERROR + checkboxAttr.getRowId(), inve);
                }
            }
        }
        return result;
    }

    /**
     * methode Extrait un element selectionne de la liste des elements par position liste :
     * 
     * @param elements elements
     * @param selectedRowids selectedRowids
     * @param checkboxAttr checkboxAttr
     * @return object
     */
    private static Object extraitUnElementSelectionneDeLaListeDesElementsParPositionListe(final Collection elements,
        final String selectedRowids, final SelectedCheckboxCellAttributes checkboxAttr)
    {
        Object result = null;
        if (elements instanceof List)
        {
            int pos = Integer.parseInt(selectedRowids);
            if (pos < elements.size())
            {
                result = ((List) elements).get(pos);
            }
        }
        else
        {
            throw new EcTableException("La collection utilisée doit implémenter l'interface List");
        }

        return result;
    }

    /**
     * methode Extrait un element du set des elements par son hash code :
     * 
     * @param elements elements
     * @param selectedRowids selectedRowids
     * @param checkboxAttr checkboxAttr
     * @return object
     */
    private static Object extraitUnElementDuSetDesElementsParSonHashCode(final Collection elements,
        final String selectedRowids, final SelectedCheckboxCellAttributes checkboxAttr)
    {
        Object result = null;

        if (log.isDebugEnabled())
        {
            log.debug("hashcode recherché : " + selectedRowids);
        }

        int hashCodeRecherche = Integer.parseInt(selectedRowids);

        for (Iterator iter = elements.iterator(); iter.hasNext();)
        {
            Object element = iter.next();

            if (element != null && log.isDebugEnabled())
            {
                log.debug("hashcode de l'élément en cours : " + element.hashCode());
            }

            if (elements != null && element.hashCode() == hashCodeRecherche)
            {
                result = element;
            }
        }
        return result;
    }

    /**
     * methode Extrait un element selectionne de la liste des elements :
     * 
     * @param elements elements
     * @param selectedRowids selectedRowids
     * @param checkboxAttr checkboxAttr
     * @return object
     */
    private static Object extraitUnElementSelectionneDeLaListeDesElements(final Collection elements,
        final String selectedRowids, final SelectedCheckboxCellAttributes checkboxAttr)
    {
        Object result;

        if (checkboxAttr.getRowId().equalsIgnoreCase(PreferencesConstants.ROWNUMBER_SELECTION))
        {
            result =
                CheckboxSelectUtil.extraitUnElementSelectionneDeLaListeDesElementsParPositionListe(elements,
                    selectedRowids, checkboxAttr);
        }
        else if (checkboxAttr.getRowId().equalsIgnoreCase(PreferencesConstants.HASHCODE_SELECTION))
        {
            result =
                CheckboxSelectUtil.extraitUnElementDuSetDesElementsParSonHashCode(elements, selectedRowids,
                    checkboxAttr);
        }
        else
        {
            result =
                CheckboxSelectUtil.extraitUnElementSelectionneDeLaListeDesElementsParSonId(elements, selectedRowids,
                    checkboxAttr);
        }
        return result;
    }

    /**
     * Lit dans le contexte de flux la collection utilisée par le tableau.
     * 
     * @param context context
     * @return la collection support du tableau
     */
    public static Collection litLesElementsDuTableau(final RequestContext context)
    {
        return CheckboxSelectUtil.litLesElementsDuTableau(context, PreferencesConstants.TABLE_ID);
    }

    /**
     * Lit dans le contexte de flux la collection utilisée par le tableau.
     * 
     * @param context context
     * @param tableId valeur du paramètre tableId utilisée dans le tag <ec:table>
     * @return la collection support du tableau
     */
    public static Collection litLesElementsDuTableau(final RequestContext context, final String tableId)
    {
        Collection result;
        // Récupère l'objet de paramétrage
        SelectedCheckboxCellAttributes checkboxAttr =
            CheckboxSelectUtil.getSelectedCheckboxAttributesFromScopeOrDefault(context, tableId);
        result =
            (Collection) PreferencesConstants.getObjectFromScope(context, checkboxAttr.getElementsInFlow(),
                PreferencesConstants.FLOW_SCOPE);
        return result;
    }

}