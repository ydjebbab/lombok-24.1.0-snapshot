/*Copyright (c) 2014 DGFiP - Tous droits réservés/* * Copyright 2004 original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package fr.gouv.finances.lombok.apptags.table.bean;

import java.util.HashMap;
import java.util.Map;

/**
 * Class Attributes
 */
public class Attributes
{

    /** attr. */
    private Map<String, Object> attr = new HashMap<>();

    public Attributes()
    {
        super();

    }

    /**
     * add attribute
     *
     * @param key key
     * @param value value
     */
    public void addAttribute(String key, Object value)
    {
        attr.put(key, value);
    }

    /**
     * Accesseur de l attribut attribute.
     *
     * @param key key
     * @return attribute
     */
    public Object getAttribute(String key)
    {
        return attr.get(key);
    }

    /**
     * Accesseur de l attribut attribute as int.
     *
     * @param key key
     * @return attribute as int
     */
    public int getAttributeAsInt(String key)
    {
        int result = 0;
        Object value = attr.get(key);
        if (value != null)
        {
            result = Integer.parseInt(String.valueOf(value));
        }

        return result;
    }

    /**
     * Accesseur de l attribut attribute as string.
     *
     * @param key key
     * @return attribute as string
     */
    public String getAttributeAsString(String key)
    {
        String result = null;
        Object value = attr.get(key);
        if (value != null)
        {
            result = String.valueOf(value);
        }

        return result;
    }
}
