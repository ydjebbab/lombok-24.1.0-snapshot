/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.apptags.table.view.jrxmlinfo;

import net.sf.jasperreports.engine.JRPen;

import org.apache.commons.lang.StringUtils;

import fr.gouv.finances.lombok.apptags.table.core.TableModel;
import fr.gouv.finances.lombok.apptags.table.view.CpJrxmlView;

/**
 * Class DocumentInfo
 */
public class DocumentInfo
{

    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** page width. */
    private int pageWidth;

    /** page height. */
    private int pageHeight;

    /** left margin. */
    private int leftMargin;

    /** right margin. */
    private int rightMargin;

    /** top margin. */
    private int topMargin;

    /** bottom margin. */
    private int bottomMargin;

    /** use title on new page. */
    private boolean useTitleOnNewPage;

    /** background. */
    private BackgroundInfo background;

    /** title. */
    private TitleInfo title;

    /** page header. */
    private PageHeaderInfo pageHeader;

    /** column header. */
    private ColumnHeaderInfo columnHeader;

    /** detail. */
    private DetailInfo detail;

    /** column footer. */
    private ColumnFooterInfo columnFooter;

    /** page footer. */
    private PageFooterInfo pageFooter;

    /** last page footer. */
    private LastPageFooterInfo lastPageFooter;

    /** summary info. */
    private SummaryInfo summaryInfo;

    /** line width. */
    private Float lineWidth;

    /**
     * Instanciation de document info.
     */
    public DocumentInfo()
    {
        super();
        this.background = new BackgroundInfo();
        this.title = new TitleInfo();
        this.pageHeader = new PageHeaderInfo();
        this.columnHeader = new ColumnHeaderInfo();
        this.detail = new DetailInfo();
        this.columnFooter = new ColumnFooterInfo();
        this.pageFooter = new PageFooterInfo();
        this.lastPageFooter = new LastPageFooterInfo();
        this.summaryInfo = new SummaryInfo();
        this.lineWidth = JRPen.LINE_WIDTH_1;
    }

    /**
     * Verifie si document avec graphiques.
     * 
     * @return true, si c'est document avec graphiques
     */
    public boolean isDocumentAvecGraphiques()
    {
        return (this.getSummaryInfo().getGraphiquesInfos().size() > 0);
    }

    /**
     * Verifie si document avec sous rapports.
     * 
     * @return true, si c'est document avec sous rapports
     */
    public boolean isDocumentAvecSousRapports()
    {
        return (this.getSummaryInfo().getSousRapportsInfos().size() > 0);
    }

    /**
     * methode Cal column width :
     * 
     * @return int
     */
    public int calColumnWidth()
    {
        return this.pageWidth - this.leftMargin - this.rightMargin;
    }

    /**
     * Verifie si use title on new page.
     * 
     * @return true, si c'est use title on new page
     */
    public boolean isUseTitleOnNewPage()
    {
        return useTitleOnNewPage;
    }

    /**
     * Modificateur de l attribut use title on new page.
     * 
     * @param useTitleOnNewPage le nouveau use title on new page
     */
    public void setUseTitleOnNewPage(boolean useTitleOnNewPage)
    {
        this.useTitleOnNewPage = useTitleOnNewPage;
    }

    /**
     * Accesseur de l attribut background.
     * 
     * @return background
     */
    public BackgroundInfo getBackground()
    {
        return background;
    }

    /**
     * Modificateur de l attribut background.
     * 
     * @param background le nouveau background
     */
    public void setBackground(BackgroundInfo background)
    {
        this.background = background;
    }

    /**
     * Accesseur de l attribut bottom margin.
     * 
     * @return bottom margin
     */
    public int getBottomMargin()
    {
        return bottomMargin;
    }

    /**
     * Modificateur de l attribut bottom margin.
     * 
     * @param bottomMargin le nouveau bottom margin
     */
    public void setBottomMargin(int bottomMargin)
    {
        this.bottomMargin = bottomMargin;
    }

    /**
     * Accesseur de l attribut column footer.
     * 
     * @return column footer
     */
    public ColumnFooterInfo getColumnFooter()
    {
        return columnFooter;
    }

    /**
     * Modificateur de l attribut column footer.
     * 
     * @param columnFooter le nouveau column footer
     */
    public void setColumnFooter(ColumnFooterInfo columnFooter)
    {
        this.columnFooter = columnFooter;
    }

    /**
     * Accesseur de l attribut column header.
     * 
     * @return column header
     */
    public ColumnHeaderInfo getColumnHeader()
    {
        return columnHeader;
    }

    /**
     * Modificateur de l attribut column header.
     * 
     * @param columnHeader le nouveau column header
     */
    public void setColumnHeader(ColumnHeaderInfo columnHeader)
    {
        this.columnHeader = columnHeader;
    }

    /**
     * Accesseur de l attribut detail.
     * 
     * @return detail
     */
    public DetailInfo getDetail()
    {
        return detail;
    }

    /**
     * Modificateur de l attribut detail.
     * 
     * @param detail le nouveau detail
     */
    public void setDetail(DetailInfo detail)
    {
        this.detail = detail;
    }

    /**
     * Accesseur de l attribut last page footer.
     * 
     * @return last page footer
     */
    public LastPageFooterInfo getLastPageFooter()
    {
        return lastPageFooter;
    }

    /**
     * Modificateur de l attribut last page footer.
     * 
     * @param lastPageFooter le nouveau last page footer
     */
    public void setLastPageFooter(LastPageFooterInfo lastPageFooter)
    {
        this.lastPageFooter = lastPageFooter;
    }

    /**
     * Accesseur de l attribut left margin.
     * 
     * @return left margin
     */
    public int getLeftMargin()
    {
        return leftMargin;
    }

    /**
     * Modificateur de l attribut left margin.
     * 
     * @param leftMargin le nouveau left margin
     */
    public void setLeftMargin(int leftMargin)
    {
        this.leftMargin = leftMargin;
    }

    /**
     * Accesseur de l attribut page footer.
     * 
     * @return page footer
     */
    public PageFooterInfo getPageFooter()
    {
        return pageFooter;
    }

    /**
     * Modificateur de l attribut page footer.
     * 
     * @param pageFooter le nouveau page footer
     */
    public void setPageFooter(PageFooterInfo pageFooter)
    {
        this.pageFooter = pageFooter;
    }

    /**
     * Accesseur de l attribut page header.
     * 
     * @return page header
     */
    public PageHeaderInfo getPageHeader()
    {
        return pageHeader;
    }

    /**
     * Modificateur de l attribut page header.
     * 
     * @param pageHeader le nouveau page header
     */
    public void setPageHeader(PageHeaderInfo pageHeader)
    {
        this.pageHeader = pageHeader;
    }

    /**
     * Accesseur de l attribut page height.
     * 
     * @return page height
     */
    public int getPageHeight()
    {
        return pageHeight;
    }

    /**
     * Modificateur de l attribut page height.
     * 
     * @param pageHeight le nouveau page height
     */
    public void setPageHeight(int pageHeight)
    {
        this.pageHeight = pageHeight;
    }

    /**
     * Accesseur de l attribut page width.
     * 
     * @return page width
     */
    public int getPageWidth()
    {
        return pageWidth;
    }

    /**
     * Modificateur de l attribut page width.
     * 
     * @param pageWidth le nouveau page width
     */
    public void setPageWidth(int pageWidth)
    {
        this.pageWidth = pageWidth;
    }

    /**
     * Accesseur de l attribut right margin.
     * 
     * @return right margin
     */
    public int getRightMargin()
    {
        return rightMargin;
    }

    /**
     * Modificateur de l attribut right margin.
     * 
     * @param rightMargin le nouveau right margin
     */
    public void setRightMargin(int rightMargin)
    {
        this.rightMargin = rightMargin;
    }

    /**
     * Accesseur de l attribut summary info.
     * 
     * @return summary info
     */
    public SummaryInfo getSummaryInfo()
    {
        return summaryInfo;
    }

    /**
     * Modificateur de l attribut summary info.
     * 
     * @param summaryInfo le nouveau summary info
     */
    public void setSummaryInfo(SummaryInfo summaryInfo)
    {
        this.summaryInfo = summaryInfo;
    }

    /**
     * Accesseur de l attribut title.
     * 
     * @return title
     */
    public TitleInfo getTitle()
    {
        return title;
    }

    /**
     * Modificateur de l attribut title.
     * 
     * @param title le nouveau title
     */
    public void setTitle(TitleInfo title)
    {
        this.title = title;
    }

    /**
     * Accesseur de l attribut top margin.
     * 
     * @return top margin
     */
    public int getTopMargin()
    {
        return topMargin;
    }

    /**
     * Modificateur de l attribut top margin.
     * 
     * @param topMargin le nouveau top margin
     */
    public void setTopMargin(int topMargin)
    {
        this.topMargin = topMargin;
    }

    /**
     * Accesseur de l attribut line width.
     * 
     * @return line width
     */
    public Float getLineWidth()
    {
        return lineWidth;
    }

    /**
     * Modificateur de l attribut line width.
     * 
     * @param lineWidth le nouveau line width
     */
    public void setLineWidth(Float lineWidth)
    {
        this.lineWidth = lineWidth;
    }

    /**
     * methode Ajustement dimensions :
     * 
     * @param model
     */
    public void ajustementDimensions(TableModel model)
    {
        String utilisePageDeGardeAttr =
            (String) model.getExportHandler().getCurrentExport().getAttribute(CpJrxmlView.ATTR_PAGE_DE_GARDE);
        //
        if (useTitleOnNewPage && (utilisePageDeGardeAttr == null || utilisePageDeGardeAttr.compareToIgnoreCase("false") != 0))
        {
            // Calcule la hauteur de la page de garde et positionne
            // le pied de page associé
            this.calculTitleHeightAndPageFooterCoord();
        }
        else
        {
            if (this.getTitle().getHeight() != 0)
            {
                // Calcul de la hauteur de la section title
                this.calculHeightTitle(model);

                // Calcul des coordonnées de la section title
                this.calculCoordTitle();
            }
        }

        if (this.getPageHeader().getHeight() != 0)
        {
            // Calcul de la hauteur de la section pageHeader
            this.calculHeightPageHeader(model);

            // Ajuste les largeurs dans le pageheader
            this.adjustWidhtInPageHeader();

            // Calcul des coordonnées dans le pageheader
            this.calculCoordPageHeader();
        }
    }

    /**
     * methode Calcul title height and page footer coord :
     */
    public void calculTitleHeightAndPageFooterCoord()
    {
        // Affectation de la taille de section title
        this.title.setHeight(this.pageHeight - this.bottomMargin - this.topMargin);

        // Centre le logo mariane en haut de page
        int xleftCornerMariane =
            Math.round((((float) this.calColumnWidth()) / 2) - (((float) this.title.getLogoMariane().getWidth()) / 2));

        this.title.getLogoMariane().setXleftCorner(xleftCornerMariane);
        this.title.getLogoMariane().setYleftCorner(0);

        // Centre le logo du ministère en bas de page
        // Ordonnée du coin gauche du logo
        this.title.getLogoMinistere().setYleftCorner(
            this.title.getHeight() - this.title.getLogoMinistere().getHeight() - 1);

        // Abscisse du coin gauche du logo
        int xLogo =
            Math
                .round((((float) this.calColumnWidth()) / 2) - (((float) this.title.getLogoMinistere().getWidth()) / 2));

        this.title.getLogoMinistere().setXleftCorner(xLogo);

    }

    /**
     * Calcul de la taille de la section pageHeader.
     * 
     * @param model
     */
    public void calculHeightPageHeader(TableModel model)
    {
        int pageHeaderHeight = 0;
        if (this.pageHeader.getRectangle11().isInclude() || this.pageHeader.getRectangle12().isInclude()
            || this.pageHeader.getRectangle13().isInclude())
        {
            pageHeaderHeight =
                Math.max(this.pageHeader.getRectangle11().getHeight(), this.pageHeader.getRectangle12().getHeight());
            pageHeaderHeight = Math.max(pageHeaderHeight, this.pageHeader.getRectangle13().getHeight());
        }

        String texteentete =
            (String) model.getExportHandler().getCurrentExport().getAttribute(CpJrxmlView.ATTR_TEXTE_ENTETE);
        if (this.pageHeader.getRectangle21().isInclude() && texteentete != null && StringUtils.isNotBlank(texteentete))
        {
            pageHeaderHeight = pageHeaderHeight + this.pageHeader.getRectangle21().getHeight();
        }

        // Ajout de 1 pour l'épaisseur de la ligne horizontale
        // inférieure

        pageHeaderHeight = pageHeaderHeight + 1;
        this.pageHeader.setHeight(pageHeaderHeight);
    }

    /**
     * Ajuste les largeurs des éléments du pageheader.
     */
    private void adjustWidhtInPageHeader()
    {
        int somme = this.getPageHeader().sommeTailleLigne1Rectangles();
        int ecart = somme - this.calColumnWidth();

        float tauxext = ((float) this.calColumnWidth()) / ((float) somme);

        if (ecart != 0)
        {
            StringBuilder   msg = new StringBuilder();
            msg.append('\n');
            msg.append(this.getPageHeader().printInfoWidthFirstLineRectangles());
            msg.append('\n');
            msg.append("différente de la largeur de la colonne de l'édition ");
            msg.append(" (");
            msg.append(this.calColumnWidth());
            msg.append(" )");
            msg.append('\n');
            msg.append("La taille des rectangles est ajustée de façon proportionnelle");

            int rect11Width = Math.round((this.getPageHeader().getRectangle11().getWidth()) * tauxext);
            int rect13Width = Math.round((this.getPageHeader().getRectangle13().getWidth()) * tauxext);
            int rect12Width = this.calColumnWidth() - rect11Width - rect13Width;

            this.getPageHeader().getRectangle11().setWidth(rect11Width);
            this.getPageHeader().getRectangle12().setWidth(rect12Width);
            this.getPageHeader().getRectangle13().setWidth(rect13Width);
        }
    }

    /**
     * methode Calcul coord page header :
     */
    private void calculCoordPageHeader()
    {
        // Abscisses
        int xRect1 = 0;
        int xRect2 = xRect1 + this.getPageHeader().getRectangle11().getWidth();
        int xRect3 = xRect2 + this.getPageHeader().getRectangle12().getWidth();

        this.getPageHeader().getRectangle11().setXleftcorner(xRect1);
        this.getPageHeader().getRectangle12().setXleftcorner(xRect2);
        this.getPageHeader().getRectangle13().setXleftcorner(xRect3);

        this.getPageHeader().getRectangle21().setXleftcorner(0);

        // Ordonnées
        this.getPageHeader().getRectangle11().setYleftcorner(0);
        this.getPageHeader().getRectangle12().setYleftcorner(0);
        this.getPageHeader().getRectangle13().setYleftcorner(0);

        int maxRect1Height =
            Math.max(this.pageHeader.getRectangle11().getHeight(), this.pageHeader.getRectangle12().getHeight());
        maxRect1Height = Math.max(maxRect1Height, this.pageHeader.getRectangle13().getHeight());

        this.getPageHeader().getRectangle21().setYleftcorner(maxRect1Height);

    }

    /**
     * methode Calcul coord title :
     */
    private void calculCoordTitle()
    {
        // Abscisses
        int xRect1 = 0;
        int xRect2 = xRect1 + this.getTitle().getRectangle11().getWidth();
        int xRect3 = xRect2 + this.getTitle().getRectangle12().getWidth();

        this.getTitle().getRectangle11().setXleftcorner(xRect1);
        this.getTitle().getRectangle12().setXleftcorner(xRect2);
        this.getTitle().getRectangle13().setXleftcorner(xRect3);

        this.getTitle().getRectangle21().setXleftcorner(0);

        // Ordonnées
        this.getTitle().getRectangle11().setYleftcorner(0);
        this.getTitle().getRectangle12().setYleftcorner(0);
        this.getTitle().getRectangle13().setYleftcorner(0);

        int maxRect1Height =
            Math.max(this.title.getRectangle11().getHeight(), this.title.getRectangle12().getHeight());
        maxRect1Height = Math.max(maxRect1Height, this.title.getRectangle13().getHeight());

        this.getTitle().getRectangle21().setYleftcorner(maxRect1Height);

    }

    /**
     * methode Calcul height title :
     * 
     * @param model
     */
    public void calculHeightTitle(TableModel model)
    {
        int titleHeight = 0;
        if (this.title.getRectangle11().isInclude() || this.title.getRectangle12().isInclude()
            || this.title.getRectangle13().isInclude())
        {
            titleHeight =
                Math.max(this.title.getRectangle11().getHeight(), this.title.getRectangle12().getHeight());
            titleHeight = Math.max(titleHeight, this.title.getRectangle13().getHeight());
        }

        String texteentete =
            (String) model.getExportHandler().getCurrentExport().getAttribute(CpJrxmlView.ATTR_TEXTE_ENTETE);
        if (this.title.getRectangle21().isInclude() && texteentete != null && StringUtils.isNotBlank(texteentete))
        {
            titleHeight = titleHeight + this.title.getRectangle21().getHeight();
        }

        // Ajout de 1 pour l'épaisseur de la ligne horizontale
        // inférieure

        titleHeight = titleHeight + 1;
        this.title.setHeight(titleHeight);
    }

}
