/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.apptags.table.cell;

/**
 * Class MonnaieCellHuitDecimales
 */
public class MonnaieCellHuitDecimales extends AbstractMonnaieCell
{

    /** Constant : PRECISION. */
    private static final int PRECISION = 8;

    /** Constant : FORMAT_PATTERN. */
    private static final String FORMAT_PATTERN = "#,##0.00000000 €";

    /**
     * Instanciation de monnaie cell huit decimales.
     */
    public MonnaieCellHuitDecimales()
    {
        super();
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.cell.AbstractMonnaieCell#getPrecision()
     */
    @Override
    public int getPrecision()
    {
        return PRECISION;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.cell.PatternCell#getFormatPattern()
     */
    @Override
    public String getFormatPattern()
    {
        return FORMAT_PATTERN;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.cell.PatternCell#getAlignmentCell()
     */
    @Override
    public int getAlignmentCell()
    {
        return PatternCell.HORIZONTAL_ALIGN_RIGHT;
    }
}
