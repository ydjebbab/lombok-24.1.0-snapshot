/*
 * Copyright (c) 2013 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.apptags.table.callback;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;
import java.util.Locale;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import fr.gouv.finances.lombok.apptags.util.EcTableException;
import fr.gouv.finances.lombok.apptags.util.format.BooleanFormat;
import fr.gouv.finances.lombok.apptags.util.format.BooleanFormat.TypeFormat;
import fr.gouv.finances.lombok.apptags.util.format.FormaterDate;
import fr.gouv.finances.lombok.apptags.util.propertyeditor.CpBooleanEditor;
import fr.gouv.finances.lombok.apptags.util.propertyeditor.CpCustomNumberEditor;

/**
 * Class FiltreAvecComparateurInferieurSuperieur
 * 
 * @author lcontinsouzas-cp Cette classe permet de filtrer des valeurs avec des opérateurs < <= >= > <> et = Les
 *         comparaisons possibles se font : - Sur des valeurs de types String : Dans ce cas c'est l'ordre alphabétique
 *         qui est utilisé pour les comparaisons - Sur des valeurs de type Date : Dans ce cas le filtre doit être saisi
 *         exactement selon le même format de date que celui de la valeur. Dans le cas contraire la valeur sera
 *         considérée comme non assortie - Sur des valeurs de type Number : Dans ce cas le filtre peut être saisi dans
 *         tout format de nombre autorisé, y compris avec signes monétaires - Sur des valeurs de type Boolean : Le
 *         format de saisie du filtre est celui d'affichage des colonnes (oui/non) dans la plupart des cas. Le filtre
 *         considere toute valeur comme non assortie si on lui donne un comparateur < > >= <=
 */
public class FiltreAvecComparateurInferieurSuperieur
{
    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** Constant : COMPARATEUR_DIFFERENT. */
    public static final String COMPARATEUR_DIFFERENT = "<>";

    /** Constant : COMPARATEUR_EGAL. */
    public static final String COMPARATEUR_EGAL = "=";

    /** Constant : COMPARATEUR_INFERIEUR. */
    public static final String COMPARATEUR_INFERIEUR = "<";

    /** Constant : COMPARATEUR_INFERIEUR_OU_EGAL. */
    public static final String COMPARATEUR_INFERIEUR_OU_EGAL = "<=";

    /** Constant : COMPARATEUR_SUPERIEUR. */
    public static final String COMPARATEUR_SUPERIEUR = ">";

    /** Constant : COMPARATEUR_SUPERIEUR_OU_EGAL. */
    public static final String COMPARATEUR_SUPERIEUR_OU_EGAL = ">=";

    /** Constant : TYPE_FILTRAGE_BOOLEEN. */
    public static final int TYPE_FILTRAGE_BOOLEEN = 5;

    /** Constant : TYPE_FILTRAGE_DATE. */
    public static final int TYPE_FILTRAGE_DATE = 1;

    /** Constant : TYPE_FILTRAGE_MONNAIE. */
    public static final int TYPE_FILTRAGE_MONNAIE = 2;

    /** Constant : TYPE_FILTRAGE_NOMBRE. */
    public static final int TYPE_FILTRAGE_NOMBRE = 3;

    /** Constant : TYPE_FILTRAGE_POURCENTAGE. */
    public static final int TYPE_FILTRAGE_POURCENTAGE = 4;

    /** Constant : TYPE_FILTRAGE_STRING. */
    public static final int TYPE_FILTRAGE_STRING = 0;

    /** log. */
    protected final Log log = LogFactory.getLog(getClass());

    /** filtre strict. */
    private boolean filtreStrict = false;

    /** format pour booleen. */
    private TypeFormat formatPourBooleen = TypeFormat.OUI_NON;

    /** locale. */
    private Locale locale = Locale.FRANCE;

    /** pattern qui permettra de reconstituer le filtre sous forme d'objet. */
    private String patternPourConstruireLeFiltre = null;

    /** type comparaison. */
    private String typeComparaison = null;

    /** type du filtre. */
    private int typeDuFiltre = 0;

    /** filtre sous forme de string. */
    private String valeurFiltreSousFormeString = null;

    /**
     * Constructeur de la classe FiltreAvecComparateurInferieurSuperieur.java
     *
     */
    public FiltreAvecComparateurInferieurSuperieur()
    {
        super();
        
    }

    /**
     * methode Construire un objet filtre :
     * 
     * @param classe
     * @return object
     */
    public Object construireUnObjetFiltre(Class<? extends Object> classe)
    {
        return null;
    }

    /**
     * methode Est un filtre de type booleen :
     * 
     * @return true, si c'est vrai
     */
    public boolean estUnFiltreDeTypeBooleen()
    {
        return (typeDuFiltre == TYPE_FILTRAGE_BOOLEEN ? true : false);
    }

    /**
     * methode Est un filtre de type date :
     * 
     * @return true, si c'est vrai
     */
    public boolean estUnFiltreDeTypeDate()
    {
        return (typeDuFiltre == TYPE_FILTRAGE_DATE ? true : false);
    }

    /**
     * methode Est un filtre de type monnaie :
     * 
     * @return true, si c'est vrai
     */
    public boolean estUnFiltreDeTypeMonnaie()
    {
        return (typeDuFiltre == TYPE_FILTRAGE_MONNAIE ? true : false);
    }

    /**
     * methode Est un filtre de type nombre :
     * 
     * @return true, si c'est vrai
     */
    public boolean estUnFiltreDeTypeNombre()
    {

        return (typeDuFiltre == TYPE_FILTRAGE_NOMBRE ? true : false);

    }

    /**
     * methode Est un filtre de type pourcentage :
     * 
     * @return true, si c'est vrai
     */
    public boolean estUnFiltreDeTypePourcentage()
    {
        return (typeDuFiltre == TYPE_FILTRAGE_POURCENTAGE ? true : false);
    }

    /**
     * methode Est un filtre de type string :
     * 
     * @return true, si c'est vrai
     */
    public boolean estUnFiltreDeTypeString()
    {
        return (typeDuFiltre == TYPE_FILTRAGE_STRING ? true : false);
    }

    /**
     * methode Est valeur assortie au filtre :
     * 
     * @param valeurATester valeur a tester par rapport au filtre
     * @return true si la valeur est assortie au filtre
     */
    public boolean estValeurAssortieAuFiltre(Object valeurATester)
    {
        boolean result = false;

        if (valeurATester == null)
        {
            throw new EcTableException("La valeur à tester ne peut être à null");
        }
        if (this.getValeurFiltreSousFormeString() == null)
        {
            throw new EcTableException("Test de comparaison impossible si Le filtre n'est pas défini (null)");
        }
        if (estUnFiltreDeTypeString())
        {
            result = estValeurAssortieAuFiltreDeTypeString(valeurATester);
        }
        else if (estUnFiltreDeTypeBooleen())
        {
            result = estValeurAssortieAuFiltreDeTypeBooleen(valeurATester);
        }
        else if (estUnFiltreDeTypeDate())
        {
            result = estValeurAssortieAuFiltreDeTypeDate(valeurATester);
        }
        else if (estUnFiltreDeTypeMonnaie() || estUnFiltreDeTypeNombre() || estUnFiltreDeTypePourcentage())
        {
            // pec uniquement de la saisie du filtre sous forme de nombre sans
            // format
            result = estValeurAssortieAuFiltreDeTypeNombre(valeurATester);
        }
        return result;
    }

    /**
     * Accesseur de l attribut format pour booleen.
     * 
     * @return the formatPourBooleen
     */
    public TypeFormat getFormatPourBooleen()
    {
        return formatPourBooleen;
    }

    /**
     * Accesseur de l attribut locale.
     * 
     * @return locale
     */
    public Locale getLocale()
    {
        return locale;
    }

    /**
     * Gets the pattern qui permettra de reconstituer le filtre sous forme d'objet.
     * 
     * @return the pattern qui permettra de reconstituer le filtre sous forme d'objet
     */
    public String getPatternPourConstruireLeFiltre()
    {
        return patternPourConstruireLeFiltre;
    }

    /**
     * Accesseur de l attribut type comparaison.
     * 
     * @return type comparaison
     */
    public String getTypeComparaison()
    {
        return typeComparaison;
    }

    /**
     * Accesseur de l attribut type du filtre.
     * 
     * @return type du filtre
     */
    public int getTypeDuFiltre()
    {
        return typeDuFiltre;
    }

    /**
     * Gets the filtre sous forme de string.
     * 
     * @return the filtre sous forme de string
     */
    public String getValeurFiltreSousFormeString()
    {
        return valeurFiltreSousFormeString;
    }

    /**
     * Verifie si filtre strict.
     * 
     * @return true, si c'est filtre strict
     */
    public boolean isFiltreStrict()
    {
        return filtreStrict;
    }

    /**
     * Modificateur de l attribut filtre strict.
     * 
     * @param filtreStrict le nouveau filtre strict
     */
    public void setFiltreStrict(boolean filtreStrict)
    {
        this.filtreStrict = filtreStrict;
    }

    /**
     * Modificateur de l attribut format pour booleen.
     * 
     * @param formatPourBooleen the formatPourBooleen to set
     */
    public void setFormatPourBooleen(TypeFormat formatPourBooleen)
    {
        this.formatPourBooleen = formatPourBooleen;
    }

    /**
     * Modificateur de l attribut locale.
     * 
     * @param locale le nouveau locale
     */
    public void setLocale(Locale locale)
    {
        this.locale = locale;
    }

    /**
     * Sets the pattern qui permettra de reconstituer le filtre sous forme d'objet.
     * 
     * @param patternPourConstruireLeFiltre the new pattern qui permettra de reconstituer le filtre sous forme d'objet
     */
    public void setPatternPourConstruireLeFiltre(String patternPourConstruireLeFiltre)
    {
        this.patternPourConstruireLeFiltre = patternPourConstruireLeFiltre;
    }

    /**
     * Modificateur de l attribut type comparaison.
     * 
     * @param typeComparaison le nouveau type comparaison
     */
    public void setTypeComparaison(String typeComparaison)
    {
        this.typeComparaison = typeComparaison;
    }

    /**
     * Modificateur de l attribut type du filtre.
     * 
     * @param typeFiltrage le nouveau type du filtre
     */
    public void setTypeDuFiltre(int typeFiltrage)
    {
        this.typeDuFiltre = typeFiltrage;
    }

    /**
     * Sets the filtre sous forme de string.
     * 
     * @param valeurFiltreSousFormeString the new filtre sous forme de string
     */
    public void setValeurFiltreSousFormeString(String valeurFiltreSousFormeString)
    {
//        ancien code
//        if (valeurFiltreSousFormeString != null)
//        {
//            this.valeurFiltreSousFormeString = valeurFiltreSousFormeString.trim();
//        }
//        else
//        {
//            this.valeurFiltreSousFormeString = null;
//        }
        
//        nouveau code
        this.valeurFiltreSousFormeString = valeurFiltreSousFormeString;
        if (valeurFiltreSousFormeString != null)
        {
            this.valeurFiltreSousFormeString = valeurFiltreSousFormeString.trim();
        }
    }

    /**
     * methode Est comparaison verifiee :
     * 
     * @param test
     * @return true, si c'est vrai
     */
    private boolean estComparaisonVerifiee(int test)
    {
        boolean estValeurAssortie = false;
        if (((test == 0) && (this.typeComparaison.equalsIgnoreCase(COMPARATEUR_EGAL)))
            || ((test >= 0) && (this.typeComparaison.equalsIgnoreCase(COMPARATEUR_SUPERIEUR_OU_EGAL)))
            || ((test <= 0) && (this.typeComparaison.equalsIgnoreCase(COMPARATEUR_INFERIEUR_OU_EGAL)))
            || ((test > 0) && (this.typeComparaison.equalsIgnoreCase(COMPARATEUR_SUPERIEUR)))
            || ((test < 0) && (this.typeComparaison.equalsIgnoreCase(COMPARATEUR_INFERIEUR)))
            || ((test != 0) && (this.typeComparaison.equalsIgnoreCase(COMPARATEUR_DIFFERENT))))
        {
            estValeurAssortie = true;
        }

        return estValeurAssortie;
    }

    /**
     * methode Est valeur assortie au filtre de type booleen :
     * 
     * @param valeurATester
     * @return true, si c'est vrai
     */
    private boolean estValeurAssortieAuFiltreDeTypeBooleen(Object valeurATester)
    {
        log.debug("Debut comparaison de type booleen avec filtre : " + this.getValeurFiltreSousFormeString());

        boolean estValeurAssortie = false;

        if ((valeurATester instanceof Boolean))
        {
            // Utilisation du CustomBooleanEditor initialisé avec le numero de
            // format
            // utilise pour les colonnes du tableau
            BooleanFormat format = BooleanFormat.getInstance(this.getFormatPourBooleen());
            CpBooleanEditor propertyeditor = new CpBooleanEditor(format);

            try
            {
                propertyeditor.setAsText(this.getValeurFiltreSousFormeString());

                Boolean valeurFiltreSousFormeBoolean = (Boolean) propertyeditor.getValue();

                Boolean valeurATesterSousFormeBoolean = (Boolean) valeurATester;

                // on ne traite pas les comparaisons < <= > >= pour les filtres
                // booleen
                if (((this.typeComparaison.equalsIgnoreCase(COMPARATEUR_EGAL)) && (valeurATesterSousFormeBoolean
                    .equals(valeurFiltreSousFormeBoolean)))
                    || ((this.typeComparaison.equalsIgnoreCase(COMPARATEUR_DIFFERENT)) && (!valeurATesterSousFormeBoolean
                        .equals(valeurFiltreSousFormeBoolean))))
                {
                    estValeurAssortie = true;
                }
                else
                {
                    estValeurAssortie = false;
                }

            }
            catch (IllegalArgumentException ile)
            {
                log.debug("impossible de convertir le filtre saisi en booleen", ile);
            }

        }
        else
        {
            log.debug("La valeur à tester n'est pas de type Boolean");
        }
        return estValeurAssortie;
    }

    /**
     * methode Est valeur assortie au filtre de type date :
     * 
     * @param valeurATester
     * @return true, si c'est vrai
     */
    private boolean estValeurAssortieAuFiltreDeTypeDate(Object valeurATester)
    {

        log.debug("Debut comparaison de type date avec filtre : " + this.getValeurFiltreSousFormeString());

        boolean estValeurAssortie = false;
        Date valeurFiltreSousFormeDate;
        Date valeurATesterArrondie;

        // Utilisation du format d'affichage pour convertir le filtre en date
        // suppose que le format de saisie dans le filtre est identique au
        // format des valeurs dans la colonne
        DateFormat format = FormaterDate.getFormatDate(this.getPatternPourConstruireLeFiltre());

     

        try
        {
            valeurFiltreSousFormeDate = format.parse(this.getValeurFiltreSousFormeString());

            if ((valeurATester instanceof Date))
            {

                // double conversion de la valeur à tester en texte puis de
                // nouveau en
                // date pour perdre la
                // precision de la date qui ne peut retranscrite dans le filtre
                // avec le
                // format d'affichage retenu
                //  faire la conversion avec le propertyeditor quand il
                // prendra en
                // charge les formats avec date et heure
                try
                {
                    valeurATesterArrondie = format.parse(format.format(valeurATester));

                    int test = (valeurATesterArrondie).compareTo(valeurFiltreSousFormeDate);
                    estValeurAssortie = estComparaisonVerifiee(test);

                }
                catch (ParseException pare)
                {
                    log.debug("Impossible d'arrondir la valeur du tableau en utilisant le format de saisie", pare);
                }

            }
            else
            {
                log
                    .debug("erreur la valeur à tester n'est pas de type date alors que la declaration ec est un type date");
            }

        }
        catch (ParseException e)
        {
            log.debug("Impossible de convertir le filtre saisi en date", e);
        }

        return estValeurAssortie;
    }

    /**
     * methode Est valeur assortie au filtre de type nombre :
     * 
     * @param valeurATester
     * @return true, si c'est vrai
     */
    private boolean estValeurAssortieAuFiltreDeTypeNombre(Object valeurATester)
    {

        log.debug("Debut comparaison numérique avec filtre : " + this.getValeurFiltreSousFormeString());

        boolean estValeurAssortie = false;

        if ((valeurATester instanceof Number))
        {

            // Utilisation du CustomNumberEditor initialisé avec la classe
            CpCustomNumberEditor propertyeditor = new CpCustomNumberEditor(valeurATester.getClass());

            try
            {
                propertyeditor.setAsText(this.getValeurFiltreSousFormeString());

                Number valeurFiltreSousFormeNombre = (Number) propertyeditor.getValue();

                if (valeurFiltreSousFormeNombre != null)
                {

                    // double conversion de la valeur a tester en texte puis de
                    // nouveau
                    // en
                    // nombre pour perdre la
                    // precision du nombre qui ne peut retranscrite dans le
                    // filtre avec
                    // le
                    // format d'affichage retenu
                    propertyeditor.setValue(valeurATester);
                    try
                    {
                        propertyeditor.setAsText(propertyeditor.getAsText());

                        Number valeurATesterArrondie = (Number) propertyeditor.getValue();

                        // en fonction du type de valeur à tester on fait la
                        // comparaison
                        // appropriée

                        if (valeurATesterArrondie instanceof BigDecimal)
                        {
                            BigDecimal valeurATesterSousFormeBigDecimal = (BigDecimal) valeurATesterArrondie;
                            BigDecimal filtreSousFormeBigDecimal = (BigDecimal) valeurFiltreSousFormeNombre;
                            int test = valeurATesterSousFormeBigDecimal.compareTo(filtreSousFormeBigDecimal);
                            estValeurAssortie = estComparaisonVerifiee(test);

                        }
                        else if (valeurATesterArrondie instanceof BigInteger)
                        {
                            BigInteger valeurATesterSousFormeBigInteger = (BigInteger) valeurATesterArrondie;
                            BigInteger filtreSousFormeBigInteger = (BigInteger) valeurFiltreSousFormeNombre;

                            int test = valeurATesterSousFormeBigInteger.compareTo(filtreSousFormeBigInteger);

                            estValeurAssortie = estComparaisonVerifiee(test);

                        }
                        else if ((valeurATesterArrondie instanceof Double) || (valeurATesterArrondie instanceof Float))
                        {
                            Double valeurATesterSousFormeDouble =
                                new Double((valeurATesterArrondie).doubleValue());
                            Double filtreSousFormeDouble = new Double(valeurFiltreSousFormeNombre.doubleValue());

                            int test = valeurATesterSousFormeDouble.compareTo(filtreSousFormeDouble);
                            estValeurAssortie = estComparaisonVerifiee(test);
                        }
                        else if ((valeurATester instanceof Long) || (valeurATester instanceof Integer)
                            || (valeurATester instanceof Short))
                        {
                            Long valeurATesterSousFormeLong = Long.valueOf(((Number) valeurATester).longValue());
                            Long filtreSousFormeLong = Long.valueOf(valeurFiltreSousFormeNombre.longValue());

                            int test = valeurATesterSousFormeLong.compareTo(filtreSousFormeLong);
                            estValeurAssortie = estComparaisonVerifiee(test);
                        }

                    }
                    catch (IllegalArgumentException ile)
                    {
                        log.debug("Impossible d'arrondir la valeur du tableau en utilisant le format de saisie", ile);
                        estValeurAssortie = false;
                    }

                }

            }
            catch (IllegalArgumentException ile)
            {
                log.debug("impossible de convertir le filtre saisi en nombre", ile);
                estValeurAssortie = false;
            }

        }
        else
        {
            log.debug("La valeur à tester n'est pas de type Number");
        }
        return estValeurAssortie;
    }

    /**
     * methode Est valeur assortie au filtre de type string :
     * 
     * @param valeurATester
     * @return true, si c'est vrai
     */
    private boolean estValeurAssortieAuFiltreDeTypeString(Object valeurATester)
    {

        // ici on convertit la valeur en string avant de faire la comparaison

        log.debug("Debut comparaison de type string avec filtre : " + this.getValeurFiltreSousFormeString());

        boolean estValeurAssortie = false;

        String valeurATesterTrim = valeurATester.toString().toLowerCase(Locale.FRANCE).trim();
        String filtreTrim = getValeurFiltreSousFormeString().toLowerCase(Locale.FRANCE).trim();

//        ancien code
//        if (this.typeComparaison != COMPARATEUR_EGAL && this.typeComparaison != COMPARATEUR_DIFFERENT)
//        nouveau code
        if (!(this.typeComparaison.equals(COMPARATEUR_EGAL)) && !(this.typeComparaison.equals(COMPARATEUR_DIFFERENT)))
        {

            int test = valeurATesterTrim.compareTo(filtreTrim);
            estValeurAssortie = estComparaisonVerifiee(test);
        }
//        ancien code
//        else if (this.typeComparaison == COMPARATEUR_EGAL)
//        nouveau code
        else if (this.typeComparaison.equals(COMPARATEUR_EGAL))
        {
            if (filtreStrict)
            {
                estValeurAssortie = StringUtils.equalsIgnoreCase(valeurATesterTrim, filtreTrim);
            }
            
            else if (filtreTrim.length() == 0)
                {
                estValeurAssortie = false;
                }
            
            else if ((filtreTrim.length() > 0) &&  filtreTrim.charAt(0) == '*' && valeurATesterTrim.endsWith(StringUtils.replace(filtreTrim, "*", "")))
            { 
                estValeurAssortie = true;
            }
            else if (filtreTrim.endsWith("*") && valeurATesterTrim.startsWith(StringUtils.replace(filtreTrim, "*", "")))
            {
                estValeurAssortie = true;
            }
            else if (StringUtils.contains(valeurATesterTrim, filtreTrim))
            {
                estValeurAssortie = true;
            }
        }

//        ancien code
//        else if (this.typeComparaison == COMPARATEUR_DIFFERENT)
//        nouveau code
        else if (this.typeComparaison.equals(COMPARATEUR_DIFFERENT))
        {
            if (filtreStrict)
            {
                estValeurAssortie = StringUtils.equalsIgnoreCase(valeurATesterTrim, filtreTrim);
            }
            else if (filtreTrim.length() == 0)
            {
                estValeurAssortie = false;
            }
            else if (  (filtreTrim.length() > 0) && filtreTrim.charAt(0) == '*')
            { 
                if (!valeurATesterTrim.endsWith(StringUtils.replace(filtreTrim, "*", "")))
                {
                    estValeurAssortie = true;
                }
            }
            else if (filtreTrim.endsWith("*"))
            {
                if (!valeurATesterTrim.startsWith(StringUtils.replace(filtreTrim, "*", "")))
                {
                    estValeurAssortie = true;
                }
            }
            else if (!StringUtils.contains(valeurATesterTrim, filtreTrim))
            {
                estValeurAssortie = true;
            }
        }

        return estValeurAssortie;
    }
}
