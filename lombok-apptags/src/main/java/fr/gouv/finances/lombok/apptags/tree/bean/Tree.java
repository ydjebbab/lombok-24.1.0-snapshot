/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : Tree.java
 *
 */
package fr.gouv.finances.lombok.apptags.tree.bean;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Class Tree.
 * 
 * @author chouard-cp
 * @version $Revision: 1.3 $ Date: 15 déc. 2009
 */
public class Tree implements Serializable
{

    /**
     * Class NodePath --.
     * 
     * @author amleplatinec
     * @version $Revision: 1.3 $ Date: 11 déc. 2009
     */
    private class NodePath
    {

        /** current node. */
        public int currentNode = 0;

        /** node count. */
        public int nodeCount = 1;

        /** node path. */
        public TreeNode[] nodePath = null;
        
        /**
         * Constructeur de la classe NodePath.java
         *
         */
        public NodePath()
        {
            
        }
    }

    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** log. */
    protected final Log log = LogFactory.getLog(getClass());

    /** root. */
    protected TreeNode root = null;

    /** single selection mode. */
    protected boolean singleSelectionMode = false;

    /** preserve selection of invisible nodes. */
    protected boolean preserveSelectionOfInvisibleNodes = true;

    /** expanded. */
    protected Set<String> expanded = new TreeSet<String>();

    // protected Set preservedSelected = new TreeSet();

    /** selected. */
    protected Set<String> selected = new TreeSet<String>();

    /** filter. */
    protected ITreeFilter filter = new BaseTreeFilter();

    /** notify on change only. */
    protected boolean notifyOnChangeOnly = true;

    /**
     * Instanciation de tree.
     */
    public Tree()
    {        
         //RAS
    }

    /**
     * Créé un arbre et positionne le noeud passé en paramètre comme la racine de l'arbre.
     * 
     * @param root le noeud racine de l'arbre
     */
    public Tree(TreeNode root)
    {
        this.root = root;
    }

    /**
     * Ferme les noeuds identifiés par la collection d'id en paramètre.
     * 
     * @param nodeIds Les id des noeuds à fermer
     */
    public void collapse(Collection<String> nodeIds)
    {
        for (String nodeId : nodeIds)
        {
            this.collapse(nodeId);
        }
    }

    /**
     * Marque le noeud identifié par l'id en paramètre comme fermé (collapsed).
     * 
     * @param treeNodeId Id d'un noeud
     */
    public void collapse(String treeNodeId)
    {
        if (this.notifyOnChangeOnly && !isExpanded(treeNodeId))
        {
            return;
        }
        this.expanded.remove(treeNodeId);
    }

    /**
     * Ferme les noeuds indentifiés par le tableau d'id en paramètre.
     * 
     * @param nodeIds Les id des noeuds à fermer
     */
    public void collapse(String[] nodeIds)
    {
        for (int i = 0; i < nodeIds.length; i++)
        {
            this.collapse(nodeIds[i]);
        }
    }

    /**
     * Ferme tous les noeuds de l'arbre.
     */
    public void collapseAll()
    {
        if (this.root != null)
        {
            this.collapse(this.root.getId());
            this.collapseDescendants(this.root);
        }
    }

    /**
     * Ferme tous les ancêtre du noeud identifié par l'id en paramètre.
     * 
     * @param nodeId L'id du noeud dont les ancêtres doivent être fermés
     */
    public void collapseAncestors(String nodeId)
    {
        TreeNode node = this.findNode(nodeId);
        if (node != null)
        {
            while (!isRoot(node))
            {
                this.collapse(node.getParent().getId());
                node = node.getParent();
            }
        }
    }

    /**
     * Ferme le noeud identifié par l'id en paramètre, ainsi que ses ancêtres.
     * 
     * @param nodeId L'id du noeud à fermer
     */
    public void collapseAncestorsAndSelf(String nodeId)
    {
        this.collapse(nodeId);
        this.collapseAncestors(nodeId);
    }

    /**
     * Ferme les enfants due noeud identifié par l'id en paramètre.
     * 
     * @param nodeId L'id du noeud à fermer
     */
    public void collapseChildren(String nodeId)
    {
        TreeNode node = this.findNode(nodeId);
        if (node != null)
        {
            for (TreeNode child : node.getChildren())
            {
                this.collapse(child.getId());
            }
        }
    }

    /**
     * Ferme le noeud identifié par l'id en paramètre, ainsi que ses enfants.
     * 
     * @param nodeId L'id du noeud à fermer
     */
    public void collapseChildrenAndSelf(String nodeId)
    {
        this.collapse(nodeId);
        this.collapseChildren(nodeId);
    }

    /**
     * Ferme les descendants du noeud identifié par l'id en paramètre.
     * 
     * @param nodeId L'id du noeud dont les descendants doivent être fermés
     */
    public void collapseDescendants(String nodeId)
    {
        TreeNode node = this.findNode(nodeId);
        this.collapseDescendants(node);
    }

    /**
     * Ferme le noeud identifié par l'id en paramètre, ainsi que ses descendants.
     * 
     * @param nodeId L'id du noeud à fermer
     */
    public void collapseDescendantsAndSelf(String nodeId)
    {
        this.collapse(nodeId);
        this.collapseDescendants(nodeId);
    }

    /**
     * Ferme le noeud parent du noeud identifié par l'id en paramètre.
     * 
     * @param nodeId L'id du noeud dont le parent doit être fermé
     */
    public void collapseParent(String nodeId)
    {
        TreeNode node = findNode(nodeId);
        if (node != null && !isRoot(node))
        {
            this.collapse(node.getParent().getId());
        }
    }

    /**
     * Ferme le noeud identifié par l'id en paramètre, ainsi que son parent s'il existe.
     * 
     * @param nodeId L'id du noeud à fermer
     */
    public void collapseParentAndSelf(String nodeId)
    {
        this.collapse(nodeId);
        this.collapseParent(nodeId);
    }

    /**
     * Ouvre les noeuds identifiés par le tableau d'id en paramètre.
     * 
     * @param nodeIds Les id des noeuds à ouvrir
     */
    public void expand(Collection<String> nodeIds)
    {
        for (String nodeId : nodeIds)
        {
            this.expand(nodeId);
        }
    }

    /**
     * Marque le noeud identifié par l'id en paramètre comme ouvert (expanded).
     * 
     * @param treeNodeId Id d'un noeud
     */
    public void expand(String treeNodeId)
    {
        if (!(this.notifyOnChangeOnly && this.isExpanded(treeNodeId)))
        {
            this.expanded.add(treeNodeId);
        }
    }

    /**
     * Ouvre les noeuds identifiés par la collection d'id en paramètre.
     * 
     * @param nodeIds Les id des noeuds à ouvrir
     */
    public void expand(String[] nodeIds)
    {
        for (int i = 0; i < nodeIds.length; i++)
        {
            expand(nodeIds[i]);
        }
    }

    /**
     * Ouvre tous les noeuds de l'arbre.
     */
    public void expandAll()
    {
        if (this.root != null && this.root.hasChildren())
        {
            this.expand(this.root.getId());
            this.expandDescendants(this.root);
        }
    }

    /**
     * Ouvre tous les ancêtre du noeud identifié par l'id en paramètre.
     * 
     * @param nodeId L'id du noeud dont les ancêtre doivent être ouverts
     */
    public void expandAncestors(String nodeId)
    {
        TreeNode node = findNode(nodeId);
        if (node != null)
        {
            while (!isRoot(node))
            {
                this.expand(node.getParent().getId());
                node = node.getParent();
            }
        }
    }

    /**
     * Ouvre le noeud identifié par l'id en paramètre, ainsi que son parent s'il existe.
     * 
     * @param nodeId L'id du noeud à ouvrir
     */
    public void expandAncestorsAndSelf(String nodeId)
    {
        this.expand(nodeId);
        this.expandAncestors(nodeId);
    }

    /**
     * Ouvre les enfants du noeud identifié par l'id en paramètre.
     * 
     * @param nodeId L'id du noeud à ouvrir
     */
    public void expandChildren(String nodeId)
    {
        TreeNode node = findNode(nodeId);
        if (node != null && node.getChildren().size() > 0)
        {
            for (TreeNode child : node.getChildren())
            {
                if (child != null)
                {
                    this.expand(child.getId());
                }
            }
        }
    }

    /**
     * Ouvre le noeud identifié par l'id en paramètre, ainsi que ses enfants.
     * 
     * @param nodeId L'id du noeud à ouvrir
     */
    public void expandChildrenAndSelf(String nodeId)
    {
        this.expand(nodeId);
        this.expandChildren(nodeId);
    }

    /**
     * Ouvre les descendants du noeud identifié par l'id en paramètre.
     * 
     * @param nodeId L'id du noeud dont les descendants doivent être ouverts
     */
    public void expandDescendants(String nodeId)
    {
        TreeNode node = findNode(nodeId);
        this.expandDescendants(node);
    }

    /**
     * Ouvre le noeud identifié par l'id en paramètre, ainsi que ses descendants.
     * 
     * @param nodeId L'id du noeud à ouvrir
     */
    public void expandDescendantsAndSelf(String nodeId)
    {
        this.expand(nodeId);
        this.expandDescendants(nodeId);
    }

    /**
     * Ouvre le noeud parent du noeud identifié par l'id en paramètre.
     * 
     * @param nodeId L'id du noeud dont le parent doit être ouvert
     */
    public void expandParent(String nodeId)
    {
        TreeNode node = this.findNode(nodeId);
        if (node != null && !isRoot(node))
        {
            this.expand(node.getParent().getId());
        }
    }

    /**
     * methode Expand parent and self : --.
     * 
     * @param nodeId --
     */
    public void expandParentAndSelf(String nodeId)
    {
        this.expand(nodeId);
        this.expandParent(nodeId);
    }

    /**
     * Retourne l'instance <code>TreeNode</code> dont l'id est égal au paramètre treeNodeId. Si aucun noeud ne possède
     * cet id, null est retourné. Si plus d'un noeud possède cet id, le premier trouvé sera retourné. Comme les ids sont
     * supposés unique au sein d'un même arbre, ce cas ne devrait pas se produire
     * 
     * @param treeNodeId L'id du noeud à rechercher
     * @return le noeud qui possède l'id recherché ou null si aucun noeud ne correspond
     */
    public TreeNode findNode(String treeNodeId)
    {
        return findNode(getRoot(), treeNodeId);
    }

    /**
     * Retourne l'ensemble des noeuds dont l'id appartient à l'ensemble d'id passés en paramètre.
     * 
     * @param treeNodeIds Un ensemble d'ids
     * @return Un ensemble d'instances <code>TreeNode</code>.
     */
    public Set<TreeNode> findNodes(Set<String> treeNodeIds)
    {
        Set<TreeNode> treeNodes = new HashSet<TreeNode>();
        this.findNodes(getRoot(), treeNodeIds, treeNodes);
        return treeNodes;
    }

    /**
     * Retourne tous les noeuds marqués ouverts (expanded) de l'arbre.
     * 
     * @return Un <code>Set</code> d'instances <code>TreeNode</code>.
     */
    public Set<TreeNode> getExpandedNodes()
    {
        return this.findNodes(this.expanded);
    }

    /**
     * retourne le filtre à appliquer à cet arbre.
     * 
     * @return Le filtre à appliquer
     */
    public ITreeFilter getFilter()
    {
        return this.filter;
    }

    /**
     * Retourne un tableau d'objet <code>TreeNode</code> contenant tous les noeuds permettant d'accéder au noeud
     * identifié par l'id en paramètre jusqu'au noeud racine. Le noeud racine à l'index 0 dans le tableau
     * 
     * @param nodeId Tid du noeud dont on cherche le chemin à partir du noeud racine
     * @return tableau contenant les noeuds qui permettent le noeud identifié par l'id en paramètre à partir du noeud
     *         racine
     */
    public TreeNode[] getNodePath(String nodeId)
    {
        TreeNode node = findNode(nodeId);
        if (node == null)
        {
            return new TreeNode[0];
        }

        NodePath nodePath = new NodePath();
        getNodePath(node, nodePath);
        return nodePath.nodePath;
    }

    /**
     * Retourne le noeud racine de cet arbre.
     * 
     * @return Le noeud racine
     */
    public TreeNode getRoot()
    {
        return this.root;
    }

    /**
     * Retourne tous les noeuds sélectionnés.
     * 
     * @return Un <code>Set</code> d'instances <code>TreeNode</code>
     */
    public Set<TreeNode> getSelectedNodes()
    {
        return findNodes(this.selected);
    }

    /**
     * Retourne 'true' si le noeud identifié par l'id en paramètre est dans l'état 'expanded'.
     * 
     * @param treeNodeId Id d'un noeud
     * @return True si le noeud est ouvert (expanded), sinon false
     */
    public boolean isExpanded(String treeNodeId)
    {
        return this.expanded.contains(treeNodeId);
    }

    /**
     * Verifie si invisible.
     * 
     * @param nodeId --
     * @return true, si c'est invisible
     */
    public boolean isInvisible(String nodeId)
    {
        boolean result = false;
        TreeNode node = this.findNode(nodeId);

        if (node == null)
        {
            result = true;
        }
        else
        {
            TreeNode parent = node.getParent();
            while (parent != null)
            {
                if (!isExpanded(parent.getId()))
                {
                    result = true;
                }
                parent = parent.getParent();
            }
        }
        return result;
    }

    /**
     * Retourne si l'arbre conserve la sélection des noeuds invisibles. True par défaut. Quand dans une JSP, une case à
     * cocher est associée à chaque noeud de l'arbre, on ne peut pas distinguer une case à cocher visible non cochée
     * d'une case à cocher qui n'appartient plus au formulaire car son noeud parent a été refermé. En effet, la
     * fermeture d'un noeud déselectionne tous ses fils et petits fils. Si preserveSelectionOfInvisibleNodes est à true,
     * la méthode selectOnly utilisée par la case à cocher ne modifiera pas l'état des noeuds invisibles <br/>
     * <br/>
     * Note : ce paramètre affecte uniquement la méthode selecOnly, les autres méthodes de déselection l'ignore.
     * 
     * @return True si la sélection des noeuds invisibles est conservée, sinon false
     */
    public boolean isPreserveSelectionOfInvisibleNodes()
    {
        return preserveSelectionOfInvisibleNodes;
    }

    /**
     * Retourne true si le noeud identifié par l'id en paramètre est sélectionné.
     * 
     * @param treeNodeId L'id du noeud testé
     * @return True si le noeud est sélectionné, sinon false
     */
    public boolean isSelected(String treeNodeId)
    {
        return this.selected.contains(treeNodeId);
    }

    /**
     * Retourne true si l'arbre est en mode sélection unique.
     * 
     * @return True si l'arbre est en mode sélection unqiue, sinon false
     */
    public boolean isSingleSelectionMode()
    {
        return this.singleSelectionMode;
    }

    /**
     * Retourne un itérateur sur les noeuds de cet arbre. Les noeuds sont encapsulés dans des instances de
     * <code>ITreeIteratorElements</code>, qui contient des informations supplémentaires sur les noeuds itérés, par
     * exemple s'ils sont ouverts, fermés, .... La classe TreeNode ne stocke pas ces informations. <br/>
     * <br/>
     * Les noeuds de l'arbres sont présentés dans le même que celui dans lequel ils seront affichés.
     * 
     * @param includeRootNode True pour inclure le noeud racine, sinon false
     * @return Un itérateur sur les noeuds de l'arbre encapsulés dans un objet ITreeIteratorElements
     */
    public Iterator<TreeIteratorElement> iterator(boolean includeRootNode)
    {
        return new TreeIterator(this, includeRootNode);
    }

    /**
     * Selectionne les noeuds dont l'id est passé en paramètre.
     * 
     * @param nodeIds id des noeuds à Sélectionner
     */
    public void select(Collection<String> nodeIds)
    {
        for (String nodeId : nodeIds)
        {
            this.select(nodeId);
        }
    }

    /**
     * Marque le noeud identifié par l'id en paramètre comme sélectionné.
     * 
     * @param treeNodeId L'id du noeud à marquer sélectionné
     */
    public void select(String treeNodeId)
    {
        if (!(notifyOnChangeOnly && isSelected(treeNodeId)))
        {
            if (isSingleSelectionMode())
            {
                unSelectAll();
            }
            this.selected.add(treeNodeId);
        }

    }

    /**
     * Marque comme sélectionné les noeuds identifiés par le tableau d'id passée en paramètre <br/>
     * <br/>
     * Si l'arbre est en mode single-selection et si le tableau contient plus d'un id, cette méthode lève une exception.
     * 
     * @param treeNodeIds Le tableau d'id de noeuds à marquer comme sélectionnés
     */
    public void select(String[] treeNodeIds)
    {
        this.multipleSelectVsSelectionModeValidation(treeNodeIds);
        for (int i = 0; i < treeNodeIds.length; i++)
        {
            this.select(treeNodeIds[i]);
        }
    }

    /**
     * Selectionne tous les noeuds de l'arbre.
     */
    public void selectAll()
    {
        this.select(root.getId());
        this.selectDescendants(root);
    }

    /**
     * Sélectionne tous les ancêtres du noeud identifié par l'id en paramètre.
     * 
     * @param nodeId Id du noeud dont les ancêtres doivent être sélectionnés
     */
    public void selectAncestors(String nodeId)
    {
        TreeNode node = findNode(nodeId);
        if (node != null)
        {
            while (!isRoot(node) && node.getParent() != null)
            {
                this.select(node.getParent().getId());
                node = node.getParent();
            }
        }
    }

    /**
     * Sélectionne le noeud identifié par l'id en paramètre, ainsi que ses ancêtres.
     * 
     * @param nodeId Id du noeud à sélectionner
     */
    public void selectAncestorsAndSelf(String nodeId)
    {
        this.select(nodeId);
        this.selectAncestors(nodeId);
    }

    /**
     * Sélectionne tous les enfants du noeud identifié par l'id en paramètre.
     * 
     * @param nodeId Id du noeud dont les enfants doivent être sélectionnés
     */
    public void selectChildren(String nodeId)
    {
        TreeNode node = findNode(nodeId);
        if (node != null && node.getChildren().size() > 0)
        {
            for (TreeNode child : node.getChildren())
            {
                this.select(child.getId());
            }
        }
    }

    /**
     * Sélectionne le noeud identifié par l'id en paramètre, ainsi que ses enfants.
     * 
     * @param nodeId Id du noeud à sélectionner
     */
    public void selectChildrenAndSelf(String nodeId)
    {
        this.select(nodeId);
        this.selectChildren(nodeId);
    }

    /**
     * Sélectionne tous les descendants du noeud identifié par l'id en paramètre.
     * 
     * @param nodeId Id du noeud dont les descendants doivent être sélectionnés
     */
    public void selectDescendants(String nodeId)
    {
        TreeNode node = this.findNode(nodeId);
        this.selectDescendants(node);
    }

    /**
     * Sélectionne le noeud identifié par l'id en paramètre, ainsi que ses descendants.
     * 
     * @param nodeId Id du noeud à sélectionner
     */
    public void selectDescendantsAndSelf(String nodeId)
    {
        this.select(nodeId);
        this.selectDescendants((nodeId));
    }

    /**
     * Marque comme sélectionné uniquement les noeuds identifiés par le tableau d'id passée en paramètre. Si d'autres
     * noeuds étaient sélectionnés au préalable, ils sont désélectionnés. Cette méthode est utiles pourt utiliser des
     * cases à cocher dans un arbre. <br/>
     * <br/>
     * Exemple:<br/>
     * <br/>
     * tree.selectOnly(request.getParameterValues("select"));
     * 
     * @param treeNodeIds Les ids des noeuds à sélectionner
     */
    public void selectOnly(String[] treeNodeIds)
    {
        this.multipleSelectVsSelectionModeValidation(treeNodeIds);
        Set<String> selectOnlySet = new HashSet<String>();
        for (int i = 0; i < treeNodeIds.length; i++)
        {
            selectOnlySet.add(treeNodeIds[i]);
        }
        Set<String> selected = this.copy(this.selected);
        Set<String> unselect = this.copy(this.selected);

        // désélectionne tous les noeuds qui étaient sélectionnés mais ne le
        // sont plus
        unselect.removeAll(selectOnlySet);

        // Contrôle si les noeuds à désélectionné sont invisibles et si leur
        // état doit être préservé
        if (this.isPreserveSelectionOfInvisibleNodes())
        {
            for (Iterator<String> iterator = unselect.iterator(); iterator.hasNext();)
            {
                String nodeId = iterator.next();
                // Si le noeud est visible on le retire
                if (this.isInvisible(nodeId))
                {
                    // Si le noeud est invisible on nele déselectionne pas
                    iterator.remove();
                }
            }
        }
        this.unSelect(unselect);

        // Sélectionne tous les noeuds qui n'étaient pas sélectionnés
        // précédemment
        selectOnlySet.removeAll(selected);
        this.select(selectOnlySet);
    }

    /**
     * Sélectionne le noeud parent du noeud identifié par l'id en paramètre.
     * 
     * @param nodeId Id du noeud dont le parent doit être sélectionné
     */
    public void selectParent(String nodeId)
    {
        TreeNode node = this.findNode(nodeId);
        if (!isRoot(node))
        {
            this.select(node.getParent().getId());
        }
    }

    /**
     * Sélectionne le noeud identifié par l'id en paramètre, ainsi que son noeud parent.
     * 
     * @param nodeId Id du noeud à sélectionner
     */
    public void selectParentAndSelf(String nodeId)
    {
        this.select(nodeId);
        this.selectParent(nodeId);
    }

    /**
     * Positionne le filtre à appliquer à cet arbre.
     * 
     * @param filter Le filtre à appliquer
     */
    public void setFilter(ITreeFilter filter)
    {
        this.filter = filter;
    }

    /**
     * Défini si l'arbre conserve la sélection des noeuds invisibles. True par défaut. Quand dans une JSP, une case à
     * cocher est associée à chaque noeud de l'arbre, on ne peut pas distinguer une case à cocher visible non cochée
     * d'une case à cocher qui n'appartient plus au formulaire car son noeud parent a été refermé. En effet, la
     * fermeture d'un noeud déselectionne tous ses fils et petits fils. Si preserveSelectionOfInvisibleNodes est à true,
     * la méthode selectOnly utilisée par la case à cocher ne modifiera pas l'état des noeuds invisibles <br/>
     * <br/>
     * Note : ce paramètre affecte uniquement la méthode selecOnly, les autres méthodes de déselection l'ignore.
     * preserveSelectionOfInvisibleNodes True si la sélection des noeuds invisibles doit être conservée, sinon false
     * 
     * @param preserveSelectionOfInvisibleNodes --
     */
    public void setPreserveSelectionOfInvisibleNodes(boolean preserveSelectionOfInvisibleNodes)
    {
        this.preserveSelectionOfInvisibleNodes = preserveSelectionOfInvisibleNodes;
    }

    /**
     * Positionne le noeud racine de cet arbre.
     * 
     * @param node --
     */
    public void setRoot(TreeNode node)
    {
        this.root = node;
    }

    /**
     * Positionne le mode de sélection de l'arbre.
     * 
     * @param mode True pour positionner l'arbre en mode sélection unique, false pour placer l'arbre en mode multi
     *        sélection
     */
    public void setSingleSelectionMode(boolean mode)
    {
        this.singleSelectionMode = mode;
    }

    /**
     * Déselectionne les noeuds dont l'id est passé en paramètre.
     * 
     * @param nodeIds id des noeuds à déselectionner
     */
    public void unSelect(Collection<String> nodeIds)
    {
        for (String nodeId : nodeIds)
        {
            this.unSelect(nodeId);
        }
    }

    /**
     * Déselectionne le noeud identifié par l'id en paramètre et notifie les listener de type unSelect.
     * 
     * @param treeNodeId L'id du noeud à déselectionné
     */
    public void unSelect(String treeNodeId)
    {
        if (notifyOnChangeOnly && !isSelected(treeNodeId))
        {
            return;
        }
        this.selected.remove(treeNodeId);
    }

    /**
     * Déselectionne les noeuds dont l'id est passé en paramètre.
     * 
     * @param nodeIds id des noeuds à déselectionner
     */
    public void unSelect(String[] nodeIds)
    {
        for (int i = 0; i < nodeIds.length; i++)
        {
            this.unSelect(nodeIds[i]);
        }
    }

    /**
     * Déselectionne tous les noeuds de cet arbre et notifie les listener de type unSelect.
     */
    public void unSelectAll()
    {
        Iterator<String> iterator = this.selected.iterator();
        while (iterator.hasNext())
        {
            iterator.next();
            iterator.remove();
        }
    }

    /**
     * Désélectionne tous les ancêtres du noeud identifié par l'id en paramètre.
     * 
     * @param nodeId Id du noeud dont les ancêtres doivent être désélectionnés
     */
    public void unSelectAncestors(String nodeId)
    {
        TreeNode node = findNode(nodeId);
        if (node != null)
        {
            while (!this.isRoot(node) && node.getParent() != null)
            {
                this.unSelect(node.getParent().getId());
                node = node.getParent();
            }
        }
    }

    /**
     * Désélectionne le noeud identifié par l'id en paramètre, ainsi que ses ancêtres.
     * 
     * @param nodeId Id du noeud à désélectionner
     */
    public void unSelectAncestorsAndSelf(String nodeId)
    {
        this.unSelect(nodeId);
        this.unSelectAncestors(nodeId);
    }

    /**
     * Désélectionne tous les enfants du noeud identifié par l'id en paramètre.
     * 
     * @param nodeId Id du noeud dont les enfants doivent être désélectionnés
     */
    public void unSelectChildren(String nodeId)
    {
        TreeNode node = findNode(nodeId);
        if (node != null)
        {
            for (TreeNode child : node.getChildren())
            {
                if (child != null)
                {
                    this.unSelect(child.getId());
                }
                else
                {
                    StringBuilder   msg = new StringBuilder();
                    msg.append("Le noeud ('nodeId = ");
                    msg.append(nodeId);
                    msg.append(") possède une sous-noeud null");
                    log.warn(msg.toString());
                }
            }
        }
    }

    /**
     * Désélectionne le noeud identifié par l'id en paramètre, ainsi que ses enfants.
     * 
     * @param nodeId Id du noeud à désélectionner
     */
    public void unSelectChildrenAndSelf(String nodeId)
    {
        this.unSelect(nodeId);
        this.unSelectChildren(nodeId);
    }

    /**
     * Désélectionne tous les descendants du noeud identifié par l'id en paramètre.
     * 
     * @param nodeId Id du noeud dont les descendants doivent être désélectionnés
     */
    public void unSelectDescendants(String nodeId)
    {
        TreeNode node = this.findNode(nodeId);
        this.unSelectDescendants(node);
    }

    /**
     * Désélectionne le noeud identifié par l'id en paramètre, ainsi que ses descendants.
     * 
     * @param nodeId Id du noeud à désélectionner
     */
    public void unSelectDescendantsAndSelf(String nodeId)
    {
        this.unSelect(nodeId);
        this.unSelectDescendants(nodeId);
    }

    /**
     * Désélectionne le noeud parent du noeud identifié par l'id en paramètre.
     * 
     * @param nodeId Id du noeud dont le parent doit être désélectionné
     */
    public void unSelectParent(String nodeId)
    {
        TreeNode node = this.findNode(nodeId);
        if (node != null && !isRoot(node))
        {
            this.unSelect(node.getParent().getId());
        }
    }

    /**
     * Désélectionne le noeud identifié par l'id en paramètre, ainsi que son noeud parent.
     * 
     * @param nodeId Id du noeud à désélectionner
     */
    public void unSelectParentAndSelf(String nodeId)
    {
        this.unSelect(nodeId);
        this.unSelectParent(nodeId);
    }

    /**
     * methode Find node : --.
     * 
     * @param treeNode --
     * @param treeNodeId --
     * @return tree node
     */
    protected TreeNode findNode(TreeNode treeNode, String treeNodeId)
    {
        TreeNode result = null;

        // Si le noeud lui même possède l'identifiant passé en paramètre
        // c'est le noeud recherché
        if (treeNode.getId().equals(treeNodeId))
        {
            result = treeNode;
        }
        // On cherche parmis ses noeuds fils si un noeud possède l'id recherché
        else
        {
            for (TreeNode child : treeNode.getChildren())
            {
                TreeNode match = findNode(child, treeNodeId);
                if (match != null)
                {
                    result = match;
                }
            }
        }
        return result;
    }

    /**
     * methode Find nodes : --.
     * 
     * @param treeNode --
     * @param treeNodeIds --
     * @param treeNodes --
     */
    protected void findNodes(TreeNode treeNode, Set<String> treeNodeIds, Set<TreeNode> treeNodes)
    {
        if (treeNode != null && treeNodeIds != null)
        {
            if (treeNodeIds.contains(treeNode.getId()))
            {
                treeNodes.add(treeNode);
            }

            for (TreeNode child : treeNode.getChildren())
            {
                findNodes(child, treeNodeIds, treeNodes);
            }
        }
    }

    /**
     * methode Collapse descendants : --.
     * 
     * @param node --
     */
    private void collapseDescendants(TreeNode node)
    {
        if (node != null)
        {
            for (TreeNode child : node.getChildren())
            {
                this.collapse(child.getId());
                this.collapseDescendants(child);
            }
        }
    }

    /**
     * Retourne une Copie d'un Set d'objets.
     * 
     * @param <T>
     * @param setToCopy --
     * @return the set< t>
     */
    private <T> Set<T> copy(Set<T> setToCopy)
    {
        Set<T> copy = new HashSet<T>();
        for (T element : copy)
        {
            copy.add(element);
        }
        return copy;
    }

    /**
     * methode Expand descendants : --.
     * 
     * @param node --
     */
    private void expandDescendants(TreeNode node)
    {
        if (node != null)
        {
            for (TreeNode child : node.getChildren())
            {
                if (child.hasChildren())
                {
                    this.expand(child.getId());
                    this.expandDescendants(child);
                }
            }
        }
    }

    /**
     * Accesseur de l attribut node path.
     * 
     * @param node --
     * @param nodePath --
     * @return node path
     */
    private void getNodePath(TreeNode node, NodePath nodePath)
    {
        if (!isRoot(node))
        {
            nodePath.nodeCount++;
            getNodePath(node.getParent(), nodePath);
            nodePath.nodePath[nodePath.currentNode++] = node;
        }
        else
        {
            nodePath.nodePath = new TreeNode[nodePath.nodeCount];
            nodePath.nodePath[0] = node;
            nodePath.currentNode++;
        }
    }

    /**
     * Verifie si root.
     * 
     * @param node --
     * @return true, si c'est root
     */
    private boolean isRoot(TreeNode node)
    {
        return node == this.root || node.getParent() == null;
    }

    /**
     * methode Multiple select vs selection mode validation : --.
     * 
     * @param treeNodeIds --
     */
    private void multipleSelectVsSelectionModeValidation(String[] treeNodeIds)
    {
        if (isSingleSelectionMode() && treeNodeIds.length > 1)
        {
            throw new IllegalStateException("Il n'est pas possible de sélectionner plusieurs noeuds"
                + " quand l'arbre est en mode 'single-selection");
        }
    }

    /**
     * methode Select descendants : --.
     * 
     * @param node --
     */
    private void selectDescendants(TreeNode node)
    {
        if (node != null)
        {
            for (TreeNode treeNode : node.getChildren())
            {
                this.select(treeNode.getId());
                if (treeNode.hasChildren())
                {
                    selectDescendants(treeNode);
                }
            }
        }
    }

    /**
     * methode Un select descendants : --.
     * 
     * @param node --
     */
    private void unSelectDescendants(TreeNode node)
    {
        if (node != null)
        {
            for (TreeNode treeNode : node.getChildren())
            {
                this.unSelect(treeNode.getId());
                if (treeNode.hasChildren())
                {
                    this.unSelectDescendants(treeNode);
                }
            }
        }
    }
    
    

}
