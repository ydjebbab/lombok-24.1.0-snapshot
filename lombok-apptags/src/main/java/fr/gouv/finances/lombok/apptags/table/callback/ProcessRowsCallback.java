/*
/*Copyright (c) 2014 DGFiP - Tous droits réservés/* * Copyright 2004 original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package fr.gouv.finances.lombok.apptags.table.callback;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.apache.commons.beanutils.BeanComparator;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.comparators.NullComparator;
import org.apache.commons.collections.comparators.ReverseComparator;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import fr.gouv.finances.lombok.apptags.table.bean.Table;
import fr.gouv.finances.lombok.apptags.table.core.RetrievalUtils;
import fr.gouv.finances.lombok.apptags.table.core.TableConstants;
import fr.gouv.finances.lombok.apptags.table.core.TableModel;
import fr.gouv.finances.lombok.apptags.table.limit.Sort;
import fr.gouv.finances.lombok.util.comparators.NullSafeBeanComparator;

/**
 * Implémentation par défaut des callback qui réalisent la récupération, le filtre et le tri des collections passées au
 * tableau ECTABLE.
 */
public class ProcessRowsCallback implements RetrieveRowsCallback, FilterRowsCallback, SortRowsCallback
{

    /** logger. */
    private static Log logger = LogFactory.getLog(ProcessRowsCallback.class);

    /**
     * Constructeur de la classe ProcessRowsCallback.java
     *
     */
    public ProcessRowsCallback()
    {
        super();
        
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.callback.RetrieveRowsCallback#retrieveRows(fr.gouv.finances.lombok.apptags.table.core.TableModel)
     */
    @Override
    public Collection<Object> retrieveRows(TableModel model) throws Exception
    {
        Table table = model.getTableHandler().getTable();
        return RetrievalUtils.retrieveCollection(model.getContext(), table.getItems(), table.getScope());
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.callback.FilterRowsCallback#filterRows(fr.gouv.finances.lombok.apptags.table.core.TableModel,
     *      java.util.Collection)
     */
    @Override
    public Collection<Object> filterRows(TableModel model, Collection<Object> rows) throws Exception
    {

        Collection<Object> result;

        boolean filtered = model.getLimit().isFiltered();
        boolean cleared = model.getLimit().isCleared();

        if (!filtered || cleared)
        {
            result = rows;
        }

        else if (filtered)
        {
            result = new ArrayList<>();
            FilterPredicate filterPredicate = new FilterPredicate(model);
            CollectionUtils.select(rows, filterPredicate, result);
        }

        return rows;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.apptags.table.callback.SortRowsCallback#sortRows(fr.gouv.finances.lombok.apptags.table.core.TableModel,
     *      java.util.Collection)
     */
    @SuppressWarnings("unchecked")
    public Collection<Object> sortRows(TableModel model, Collection<Object> rows) throws Exception
    {

        Collection<Object> result = rows;

        boolean sorted = model.getLimit().isSorted();

        if (sorted)
        {
            Sort sort = model.getLimit().getSort();
            String property = sort.getProperty();
            String sortOrder = sort.getSortOrder();

            if (StringUtils.contains(property, "."))
            {
                try
                {
                    if (sortOrder.equals(TableConstants.SORT_ASC))
                    {
                        Collections.sort((List<Object>) rows, (Comparator<? super Object>) new NullSafeBeanComparator(
                            property, new NullComparator()));
                    }
                    else if (sortOrder.equals(TableConstants.SORT_DESC))
                    {
                        NullSafeBeanComparator reversedNaturalOrderBeanComparator =
                            new NullSafeBeanComparator(property, new ReverseComparator(new NullComparator()));
                        Collections.sort((List<Object>) rows, reversedNaturalOrderBeanComparator);
                    }
                }
                catch (NoClassDefFoundError exception)
                {
                    String msg =
                        "La propriété de la colonne [" + property
                            + "] est imbriquée et nécessite BeanUtils 1.7 ou plus pour effectuer un tri correct.";
                    logger.error(msg);
                    throw new NoClassDefFoundError(msg);
                }
            }
            else
            {
                if (sortOrder.equals(TableConstants.SORT_ASC))
                {
                    Comparator<Object> comparator =
                        new BeanComparator(property, new NullComparator());
                    Collections.sort((List<Object>) rows, comparator);
                }
                else if (sortOrder.equals(TableConstants.SORT_DESC))
                {
                    BeanComparator reversedNaturalOrderBeanComparator =
                        new BeanComparator(property, new ReverseComparator(new NullComparator()));
                    Collections.sort((List<Object>) rows, reversedNaturalOrderBeanComparator);
                }
            }
        }
        return result;
    }
}
