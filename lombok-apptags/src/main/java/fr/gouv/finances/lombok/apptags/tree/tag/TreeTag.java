/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : TreeTag.java
 *
 */

package fr.gouv.finances.lombok.apptags.tree.tag;

import java.util.Iterator;

import javax.servlet.jsp.JspException;

import fr.gouv.finances.lombok.apptags.base.NamePropertyTag;
import fr.gouv.finances.lombok.apptags.base.TagWriter;
import fr.gouv.finances.lombok.apptags.tree.bean.Tree;
import fr.gouv.finances.lombok.apptags.tree.bean.TreeIteratorElement;
import fr.gouv.finances.lombok.apptags.util.Constantes;

/**
 * Class TreeTag --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.4 $ Date: 11 déc. 2009
 */
public class TreeTag extends NamePropertyTag
{

    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** Itérateur sur l'arbre utilisé par le tag. */
    protected Iterator<TreeIteratorElement> treeIterator = null;

    /** Element en cours de traitement. */
    protected TreeIteratorElement element;

    /*
     * --------------------------------------------------------------------------------------------- Paramètres du tag
     * ---------------------------------------------------------------------------------------------
     */

    /** Nom du bean stocké dans le contexte qui contient l'objet de type Tree à afficher sous forme d'arbre. */
    private String tree;

    /** URI qui pointe vers le contrôleur de flux utilisé. */
    private String action;

    /** level. */
    //   vérifier si utile
    private String level;

    /** 'true' pour indiquer que le noeud racine doit être affiché, sinon 'false'. */
    private String includeRootNode;

    /**
     * Contexte dans lequel le bean 'tree' est stocké. (request, session, page, application). Par défaut : le bean est
     * recherché dans tous les scopes
     */
    private String scope;

    /** Classe css utilisée par le tag. Par défaut : 'tree */
    private String cssClass;

    /** Répertoire dans lequel les images utilisées par le tag sont stockées. Par défaut : /composants/tree/images */
    private String ImagesDir;

    /**
     * Paramètre utilisé pour transférer au serveur le noeud qui fait l'objet d'une action d'ouverture, de fermeture, de
     * sélection, ... Par défaut : nodeID
     */
    private String nodeIdParameter;

    /** Paramètre utilisé pour transférer au serveur le nom de la transition du webflow. */
    private String eventIdFieldName;

    /** Paramètre utilisé pour transférer au serveur la clé flowExecutionkey utilisée par le webflow. */
    private String flowExecutionKeyFieldName;

    /** Nom du bean utilisé pour stocker dans le contexte la clé flowExecutionKey. */
    private String flowExecutionKeyBeanName;

    /*
     * --------------------------------------------------------------------------------------------- Etats du tag
     * ---------------------------------------------------------------------------------------------
     */

    /** tag writer. */
    private TagWriter tagWriter;

    /** Arbre à afficher. */
    private Tree itree;

    /**
     * Constructeur de la classe TreeTag.java
     *
     */
    public TreeTag()
    {
        super();
        
    }

    /**
     * Code exécuté lorsque le tag 'tree' est refermé. Exécute le contenu du tag pour chaque élément de l'itérateur sur
     * l'arbre
     * 
     * @return the int
     * @throws JspException the jsp exception
     */
    @Override
    public int doAfterBody() throws JspException
    {
        int result = SKIP_BODY;

        if (this.treeIterator.hasNext())
        {
            TreeIteratorElement iTreeElement = this.treeIterator.next();
            if (this.itree.getFilter() != null)
            {
                while (!this.itree.getFilter().accept(this.itree, iTreeElement) && this.treeIterator.hasNext())
                {
                    iTreeElement = this.treeIterator.next();
                }
            }
            // Tant que l'ensemble de l'arbre n'a pas été balayé, on réexécute le corps du tag
            if (this.itree.getFilter() == null || this.itree.getFilter().accept(this.itree, iTreeElement))
            {
                // Maj de l'élément courrant
                this.element = iTreeElement;
                result = EVAL_BODY_AGAIN;
            }
        }
        else
        {
            // Fermeture de la table qui contient l'arbre
            // tagWriter.endTag();
        }
        return result;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @see org.springframework.web.servlet.tags.RequestContextAwareTag#doFinally()
     */
    @Override
    public void doFinally()
    {
        super.doFinally();
        this.tagWriter = null;
    }

    /**
     * Accesseur de l attribut action.
     * 
     * @return action
     */
    public String getAction()
    {
        return action;
    }

    /**
     * Accesseur de l attribut css class.
     * 
     * @return css class
     */
    public String getCssClass()
    {
        return cssClass;
    }

    /**
     * Accesseur de l attribut element.
     * 
     * @return element
     */
    public TreeIteratorElement getElement()
    {
        return element;
    }

    /**
     * Accesseur de l attribut event id field name.
     * 
     * @return event id field name
     */
    public String getEventIdFieldName()
    {
        return eventIdFieldName;
    }

    /**
     * Accesseur de l attribut flow execution key bean name.
     * 
     * @return flow execution key bean name
     */
    public String getFlowExecutionKeyBeanName()
    {
        return flowExecutionKeyBeanName;
    }

    /**
     * Accesseur de l attribut flow execution key field name.
     * 
     * @return flow execution key field name
     */
    public String getFlowExecutionKeyFieldName()
    {
        return flowExecutionKeyFieldName;
    }

    /**
     * Accesseur de l attribut images dir.
     * 
     * @return images dir
     */
    public String getImagesDir()
    {
        return ImagesDir;
    }

    /**
     * Accesseur de l attribut include root node.
     * 
     * @return include root node
     */
    public String getIncludeRootNode()
    {
        return includeRootNode;
    }

    /**
     * Accesseur de l attribut itree.
     * 
     * @return itree
     */
    public Tree getItree()
    {
        return itree;
    }

    /**
     * Accesseur de l attribut level.
     * 
     * @return level
     */
    public String getLevel()
    {
        return level;
    }

    /**
     * Accesseur de l attribut node id parameter.
     * 
     * @return node id parameter
     */
    public String getNodeIdParameter()
    {
        return nodeIdParameter;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @return the contexte dans lequel le bean 'tree' est stocké
     * @see fr.gouv.finances.lombok.apptags.base.NamePropertyTag#getScope()
     */
    @Override
    public String getScope()
    {
        return scope;
    }

    /**
     * Accesseur de l attribut tree.
     * 
     * @return tree
     */
    public String getTree()
    {
        return tree;
    }

    /**
     * Modificateur de l attribut action.
     * 
     * @param action le nouveau action
     */
    public void setAction(String action)
    {
        this.action = action;
    }

    /**
     * Modificateur de l attribut css class.
     * 
     * @param cssClass le nouveau css class
     */
    public void setCssClass(String cssClass)
    {
        this.cssClass = cssClass;
    }

    /**
     * Modificateur de l attribut event id field name.
     * 
     * @param eventIdFieldName le nouveau event id field name
     */
    public void setEventIdFieldName(String eventIdFieldName)
    {
        this.eventIdFieldName = eventIdFieldName;
    }

    /**
     * Modificateur de l attribut flow execution key bean name.
     * 
     * @param flowExecutionKeyBeanName le nouveau flow execution key bean name
     */
    public void setFlowExecutionKeyBeanName(String flowExecutionKeyBeanName)
    {
        this.flowExecutionKeyBeanName = flowExecutionKeyBeanName;
    }

    /**
     * Modificateur de l attribut flow execution key field name.
     * 
     * @param flowExecutionKeyFieldName le nouveau flow execution key field name
     */
    public void setFlowExecutionKeyFieldName(String flowExecutionKeyFieldName)
    {
        this.flowExecutionKeyFieldName = flowExecutionKeyFieldName;
    }

    /**
     * Modificateur de l attribut images dir.
     * 
     * @param imagesDir le nouveau images dir
     */
    public void setImagesDir(String imagesDir)
    {
        ImagesDir = imagesDir;
    }

    /**
     * Modificateur de l attribut include root node.
     * 
     * @param includeRootNode le nouveau include root node
     */
    public void setIncludeRootNode(String includeRootNode)
    {
        this.includeRootNode = includeRootNode;
    }

    /**
     * Modificateur de l attribut itree.
     * 
     * @param itree le nouveau itree
     */
    public void setItree(Tree itree)
    {
        this.itree = itree;
    }

    /**
     * Modificateur de l attribut level.
     * 
     * @param level le nouveau level
     */
    public void setLevel(String level)
    {
        this.level = level;
    }

    /**
     * Modificateur de l attribut node id parameter.
     * 
     * @param nodeIdParameter le nouveau node id parameter
     */
    public void setNodeIdParameter(String nodeIdParameter)
    {
        this.nodeIdParameter = nodeIdParameter;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param scope the new contexte dans lequel le bean 'tree' est stocké
     * @see fr.gouv.finances.lombok.apptags.base.NamePropertyTag#setScope(java.lang.String)
     */
    @Override
    public void setScope(String scope)
    {
        this.scope = scope;
    }

    /**
     * Modificateur de l attribut tree.
     * 
     * @param tree le nouveau tree
     */
    public void setTree(String tree)
    {
        this.tree = tree;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param tagWriter
     * @return int
     * @throws JspException the jsp exception
     * @see fr.gouv.finances.lombok.apptags.base.BaseAppTags#writeTagContent(fr.gouv.finances.lombok.apptags.base.TagWriter)
     */
    @Override
    protected int writeTagContent(TagWriter tagWriter) throws JspException
    {
        int result = EVAL_BODY_INCLUDE;

        this.tagWriter = tagWriter;

        // Contrôle que le paramètre tree est positionné
        this.validateAttributes();

        // Initialisation des attributs
        this.initAttributes();

        // Lecture du modèle utilisé par le tag
        this.isTreeAvailable(this.tree, this.scope);
        this.itree = findTree(this.tree, this.scope);

        // Insertion des champs cachés qui maintiennent les coordonnées
        // nécessaires au scrollback
        this.writeHiddenFieldScrollback();

        // javascript qui enregistre les coordonnées
        this.writeJavascriptScrollback();

        // Ouverture du tableau qui contiendra l'arbre

        /*
         * tagWriter.startTag(TABLE_TAG); tagWriter.writeOptionalAttributeValue(CLASS_ATTRIBUTE, this.cssClass);
         * tagWriter.writeOptionalAttributeValue(CELLSPACING_ATTRIBUTE, "0");
         * tagWriter.writeOptionalAttributeValue(CELLPADDING_ATTRIBUTE, "0");
         * tagWriter.writeOptionalAttributeValue(BORDER_ATTRIBUTE, "0"); tagWriter.forceBlock();
         */

        // Constrution de l'itérateur sur l'arbre
        this.treeIterator = itree.iterator(this.isTRUE(this.includeRootNode));

        // Traitement du premier élément de l'arbre
        if (this.treeIterator.hasNext())
        {
            TreeIteratorElement iTreeElement = this.treeIterator.next();
            if (itree.getFilter() != null)
            {
                while (!itree.getFilter().accept(itree, iTreeElement) && this.treeIterator.hasNext())
                {
                    iTreeElement = this.treeIterator.next();
                }
            }
            if (itree.getFilter() == null || itree.getFilter().accept(itree, iTreeElement))
            {
                this.element = iTreeElement;
                result = EVAL_BODY_INCLUDE;
            }
        }

        return result;
    }

    /**
     * Lit l'objet Tree stocké dans le contexte sout la clé 'tree'.
     * 
     * @param tree --
     * @param scope --
     * @return the tree
     * @throws JspException the jsp exception
     */
    private Tree findTree(String tree, String scope) throws JspException
    {
        return (Tree) getBean(tree, null, scope);
    }

    /**
     * Initialise les paramètres du tag en fonction de la valeur des attributs passés en paramètre au tag et des
     * paramètres par défaut.
     * 
     * @throws JspException the jsp exception
     */
    private void initAttributes() throws JspException
    {
        this.includeRootNode =
            this.initvar(this.includeRootNode, Constantes.INCLUDE_NODE_ROOT_KEY, Constantes.INCLUDE_NODE_ROOT_DEFAULT);

        this.cssClass = this.initvar(this.cssClass, Constantes.CSS_CLASS_KEY, Constantes.CSS_CLASS_DEFAULT);

        this.ImagesDir = this.initvar(this.ImagesDir, Constantes.IMAGES_DIR_KEY, Constantes.IMAGES_DIR_DEFAULT);

        this.scope = this.initvar(this.scope, "app.tree.scope", null);

        this.nodeIdParameter = this.initvar(this.nodeIdParameter, Constantes.NODE_ID_KEY, Constantes.NODE_ID_DEFAULT);

        this.flowExecutionKeyBeanName =
            this.initvar(this.flowExecutionKeyBeanName, Constantes.FLOW_EXECUTION_KEY_BEAN_NAME_KEY,
                Constantes.FLOW_EXECUTION_KEY_BEAN_NAME_DEFAULT);

        this.flowExecutionKeyFieldName =
            this.initvar(this.flowExecutionKeyFieldName, Constantes.FLOW_EXECUTION_KEY_FIELD_NAME_KEY,
                Constantes.FLOW_EXECUTION_KEY_FIELD_NAME_DEFAULT);

        this.eventIdFieldName =
            this.initvar(this.eventIdFieldName, Constantes.EVENT_ID_FIELD_NAME_KEY,
                Constantes.EVENT_ID_FIELD_NAME_DEFAULT);
    }

    /**
     * Teste si un objet de type Tree est stocké dans le contexte 'scope' sous la clé 'tree'.
     * 
     * @param tree --
     * @param scope --
     * @throws JspException the jsp exception
     */
    private void isTreeAvailable(String tree, String scope) throws JspException
    {
        if (findTree(tree, scope) == null)
        {
            StringBuilder   msg = new StringBuilder();
            msg.append("Aucun bean de type Tree trouvé avec la clé : ");
            msg.append(tree);
            msg.append(" Le Scope de recherche est : ");
            msg.append(scope);
            msg.append("Il faut fournir au tag 'tree' un bean de type Tree valide.");
            throw new JspException(msg.toString());
        }
    }

    /**
     * methode Validate attributes : --.
     * 
     * @throws JspException the jsp exception
     */
    private void validateAttributes() throws JspException
    {
        if (getTree() == null)
        {
            throw new JspException("L'attribut 'tree' du tag <tree> ne doit pas être null");
        }
    }

    /**
     * Ecriture des champs cachés utilisé pour le repositionnement dans la fenêtre windows.
     * 
     * @throws JspException the jsp exception
     */
    private void writeHiddenFieldScrollback() throws JspException
    {
        tagWriter.startTag(DIV_TAG);
        tagWriter.startTag(INPUT_TAG);
        tagWriter.writeOptionalAttributeValue(TYPE_ATTRIBUTE, "hidden");
        tagWriter.writeOptionalAttributeValue(ID_ATTRIBUTE, Constantes.SCROLLX_HIDDEN_ID);
        tagWriter.writeAttribute(NAME_ATTRIBUTE, Constantes.SCROLLX_PARAMETER);
        String scrollx = (String) this.getBean(Constantes.SCROLLX_CONTEXT_PARAMETER, null, null);
        if (scrollx != null)
        {
            tagWriter.writeAttribute(VALUE_ATTRIBUTE, scrollx);
        }
        tagWriter.endTag();
        tagWriter.startTag(INPUT_TAG);
        tagWriter.writeOptionalAttributeValue(TYPE_ATTRIBUTE, "hidden");
        tagWriter.writeOptionalAttributeValue(ID_ATTRIBUTE, Constantes.SCROLLY_HIDDEN_ID);
        tagWriter.writeAttribute(NAME_ATTRIBUTE, Constantes.SCROLLY_PARAMETER);
        String scrolly = (String) this.getBean(Constantes.SCROLLY_CONTEXT_PARAMETER, null, null);
        if (scrolly != null)
        {
            tagWriter.writeAttribute(VALUE_ATTRIBUTE, scrolly);
        }
        tagWriter.endTag();

        tagWriter.startTag(INPUT_TAG);
        tagWriter.writeOptionalAttributeValue(TYPE_ATTRIBUTE, "hidden");
        tagWriter.writeOptionalAttributeValue(ID_ATTRIBUTE, Constantes.SCROLLYY_HIDDEN_ID);
        tagWriter.writeAttribute(NAME_ATTRIBUTE, Constantes.SCROLLYY_PARAMETER);
        String scrollYY = (String) this.getBean(Constantes.SCROLLYY_HIDDEN_ID, null, null);

        if (scrollYY != null)
        {
            tagWriter.writeAttribute(VALUE_ATTRIBUTE, scrollYY);
        }
        tagWriter.endTag();
        tagWriter.endTag();
    }

    /**
     * Insertion du javascript utilisé pour le repositionnement dans la fenêtre windows.
     * 
     * @throws JspException the jsp exception
     */
    private void writeJavascriptScrollback() throws JspException
    {
        tagWriter.startTag(SCRIPT_TAG);
        tagWriter.writeOptionalAttributeValue(TYPE_ATTRIBUTE, "text/javascript");
        tagWriter.appendValue("/*<![CDATA[*/");
        tagWriter.appendValue("function ");
        tagWriter.appendValue(Constantes.SCROLL_JAVASCRIPT_SCROLLBACK);
        tagWriter.appendValue("{");
        tagWriter.appendValue("var inputScrollY =  document.getElementById('");
        tagWriter.appendValue(Constantes.SCROLLY_HIDDEN_ID);
        tagWriter.appendValue("');");
        tagWriter.appendValue("var inputScrollYY =  document.getElementById('");
        tagWriter.appendValue(Constantes.SCROLLYY_HIDDEN_ID);
        tagWriter.appendValue("');");
        tagWriter.appendValue("var inputScrollX =  document.getElementById('");
        tagWriter.appendValue(Constantes.SCROLLX_HIDDEN_ID);
        tagWriter.appendValue("');");

        tagWriter.appendValue("if (inputScrollX != null && inputScrollY != null)");
        tagWriter.appendValue("{");
        tagWriter.appendValue("var x = inputScrollX.value;");
        tagWriter.appendValue("var y = inputScrollY.value;");
        tagWriter.appendValue("var yy = inputScrollYY.value;");
        // ici on doit récupérer la valeur de l'ascenceur yy
        tagWriter.appendValue("if(yy > 0)");
        tagWriter.appendValue("{");
        tagWriter.appendValue("window.document.getElementById('");
        tagWriter.appendValue(Constantes.SCROLLYY_CONTEXT_PARAMETER);
        tagWriter.appendValue("').scrollTop = yy;");
        tagWriter.appendValue("}");
        tagWriter.appendValue("window.scrollTo(x, y);");
        // rajout amlp 25/03 pour gérer compatibilité ie //sinon tjrs retour sur 0,0
        tagWriter.appendValue("setTimeout('window.scrollTo(' + x + ',' + y + ');',10);");
        tagWriter.appendValue("}");
        tagWriter.appendValue("}");
        tagWriter.appendValue("\n");
        tagWriter.appendValue("function ");
        tagWriter.appendValue(Constantes.SCROLL_JAVASCRIPT_GETCOORD);
        tagWriter.appendValue("{");
        tagWriter.appendValue("var scrollX, scrollY,scrollYY;");
        tagWriter.appendValue("if (document.getElementById('");
        tagWriter.appendValue(Constantes.SCROLLYY_HIDDEN_ID);
        tagWriter.appendValue("')== null )");
        tagWriter.appendValue("{");
        tagWriter.appendValue("scrollYY = 0 ; ");
        tagWriter.appendValue("}");
        tagWriter.appendValue("else");
        tagWriter.appendValue("{");
        tagWriter.appendValue("scrollYY =document.getElementById('");
        tagWriter.appendValue(Constantes.SCROLLYY_HIDDEN_ID);
        tagWriter.appendValue("').value ;");
        tagWriter.appendValue("}");
        tagWriter.appendValue("if (document.all)");
        tagWriter.appendValue("{");
        tagWriter.appendValue("if (! document.documentElement.scrollLeft)");
        tagWriter.appendValue("{");
        tagWriter.appendValue("scrollX = document.body.scrollLeft;");
        tagWriter.appendValue("}");
        tagWriter.appendValue("else");
        tagWriter.appendValue("{");
        tagWriter.appendValue("scrollX = document.documentElement.scrollLeft;");
        tagWriter.appendValue("}");
        tagWriter.appendValue("if(!document.documentElement.scrollTop)");
        tagWriter.appendValue("{");
        tagWriter.appendValue("scrollY = document.body.scrollTop;");
        tagWriter.appendValue("}");
        tagWriter.appendValue("else");
        tagWriter.appendValue("{");
        tagWriter.appendValue("scrollY = document.documentElement.scrollTop;");
        tagWriter.appendValue("}");
        tagWriter.appendValue("}");
        tagWriter.appendValue("else");
        tagWriter.appendValue("{");
        tagWriter.appendValue("scrollX = window.pageXOffset;");
        tagWriter.appendValue("scrollY = window.pageYOffset;");
        tagWriter.appendValue("}");
        tagWriter.appendValue("document.getElementById('");
        tagWriter.appendValue(Constantes.SCROLLX_HIDDEN_ID);
        tagWriter.appendValue("').value = scrollX;");
        tagWriter.appendValue("document.getElementById('");
        tagWriter.appendValue(Constantes.SCROLLY_HIDDEN_ID);
        tagWriter.appendValue("').value = scrollY;");
        tagWriter.appendValue("if(scrollYY > 0)");
        tagWriter.appendValue("{");
        tagWriter.appendValue("return '");
        tagWriter.appendValue(Constantes.SCROLLX_PARAMETER);
        tagWriter.appendValue("='+scrollX");
        tagWriter.appendValue("+ '&");
        tagWriter.appendValue(Constantes.SCROLLY_PARAMETER);
        tagWriter.appendValue("='+scrollY");
        tagWriter.appendValue("+ '&");
        tagWriter.appendValue(Constantes.SCROLLYY_PARAMETER);
        tagWriter.appendValue("='+scrollYY;");
        tagWriter.appendValue("}");
        tagWriter.appendValue("else");
        tagWriter.appendValue("{");
        tagWriter.appendValue("return '");
        tagWriter.appendValue(Constantes.SCROLLX_PARAMETER);
        tagWriter.appendValue("='+scrollX");
        tagWriter.appendValue("+ '&");
        tagWriter.appendValue(Constantes.SCROLLY_PARAMETER);
        tagWriter.appendValue("='+scrollY;");
        tagWriter.appendValue("}");
        tagWriter.appendValue("}");
        tagWriter.appendValue("\n");
        tagWriter.appendValue("function treetagsappAddEvent(obj, evType, fn)");
        tagWriter.appendValue("{");
        tagWriter.appendValue("if (obj.addEventListener)");
        tagWriter.appendValue("{");
        tagWriter.appendValue("obj.addEventListener(evType, fn, false);");
        tagWriter.appendValue("return true;");
        tagWriter.appendValue("}");
        tagWriter.appendValue("else if (obj.attachEvent)");
        tagWriter.appendValue("{");
        tagWriter.appendValue("var r = obj.attachEvent('on' + evType, fn);");
        tagWriter.appendValue("return r;");
        tagWriter.appendValue("}");
        tagWriter.appendValue("else");
        tagWriter.appendValue("{");
        tagWriter.appendValue("return false;");
        tagWriter.appendValue("}");
        tagWriter.appendValue("}");
        tagWriter.appendValue("\n");
        tagWriter.appendValue("treetagsappAddEvent(window,'load',scrollback)");
        tagWriter.appendValue("\n");
        tagWriter.appendValue("/*]]>*/");

        tagWriter.endTag();
    }
}
