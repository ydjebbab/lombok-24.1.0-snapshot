/*
 * Copyright (c) 2014 DGFiP - Tous droits réservés
 */
/*
/*Copyright (c) 2014 DGFiP - Tous droits réservés/* * Copyright 2004 original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package fr.gouv.finances.lombok.apptags.table.core;

/**
 * Class TableConstants
 */
public class TableConstants
{

    /** Constant : ACTION. */
    public static final String ACTION = "a";

    // Utilisé pour préfixer l'alias
    /** Constant : ALIAS. */
    public static final String ALIAS = "a_";

    /** application scope. */
    public static String APPLICATION_SCOPE = "application";

    // attributs calc
    /** Constant : CALC_TOTAL. */
    public static final String CALC_TOTAL = "total";

    // attributs callback
    /** Constant : CALLBACK_DEFAULT. */
    public static final String CALLBACK_DEFAULT = "default";

    // attributs cell
    /** Constant : CELL_DISPLAY. */
    public static final String CELL_DISPLAY = "display";

    /** Constant : CELL_FILTER. */
    public static final String CELL_FILTER = "filter";

    /** Constant : CELL_HEADER. */
    public static final String CELL_HEADER = "header";

    /** Constant : CLEAR_ACTION. */
    public static final String CLEAR_ACTION = "ca";

    /** Constant : COROWCOUNT. */
    public static final String COROWCOUNT = "COROWCOUNT";

    /** Constant : CURRENCY. */
    public static final String CURRENCY = "currency";

    /** Constant : CURRENT_ROWS_DISPLAYED. */
    public static final String CURRENT_ROWS_DISPLAYED = "crd";

    /** Constant : DATE. */
    public static final String DATE = "date";

    // service edition
    /** Constant : DEFAULT_EDITION_SERVICE. */
    public static final String DEFAULT_EDITION_SERVICE = "editionProducerService";

    // interceptors
    /** Constant : DEFAULT_INTERCEPT. */
    public static final String DEFAULT_INTERCEPT = "default";

    /** Constant : EXPORT_FILE_NAME. */
    public static final String EXPORT_FILE_NAME = "efn";

    // attributs d'export
    /** Constant : EXPORT_TABLE_ID. */
    public static final String EXPORT_TABLE_ID = "ec_eti";

    /** Constant : EXPORT_VIEW. */
    public static final String EXPORT_VIEW = "ev";

    // attributs ec
    /** Constant : EXTREME_COMPONENTS. */
    public static final String EXTREME_COMPONENTS = "ec";

    /** Constant : EXTREME_COMPONENTS_INSTANCE. */
    public static final String EXTREME_COMPONENTS_INSTANCE = "ec_i";
    
    /** Constant : COLUMN. */
    public static final String COLUMN = "c_";

    // attributs limit
    /** Constant : FILTER. */
    public static final String FILTER = "f_";

    /** Constant : FILTER_ACTION. */
    public static final String FILTER_ACTION = "fa";

    /** Constant : FILTER. */
    public static final String TDFILTER = "_tdfilter";
    
    // attributs column
    /** Constant : IS_AUTO_GENERATE_COLUMN. */
    public static final String IS_AUTO_GENERATE_COLUMN = "isAutoGenerateColumn";

    /** Constant : ISFIRSTPAGE. */
    public static final String ISFIRSTPAGE = "ISFIRSTPAGE";

    /** Constant : ISLASTPAGE. */
    public static final String ISLASTPAGE = "ISLASTPAGE";

    // attributs web.xml
    /** Constant : MESSAGES_LOCATION. */
    public static final String MESSAGES_LOCATION = "extremecomponentsMessagesLocation";

    /** Constant : PAGE. */
    public static final String PAGE = "p";

    // attributs scope
    /** page scope. */
    public static String PAGE_SCOPE = "page";

    /** Constant : PAGECOUNT. */
    public static final String PAGECOUNT = "PAGECOUNT";

    /** Constant : PREFERENCES_LOCATION. */
    public static final String PREFERENCES_LOCATION = "extremecomponentsPreferencesLocation";

    /** request scope. */
    public static String REQUEST_SCOPE = "request";

    // utilisé dans le contrôleur et le tag
    /** Constant : ROWCOUNT. */
    public static final String ROWCOUNT = "ROWCOUNT";

    /** Constant : ROWS_DISPLAYED. */
    public static final String ROWS_DISPLAYED = "rd";

    /** session scope. */
    public static String SESSION_SCOPE = "session";

    /** Constant : SORT. */
    public static final String SORT = "s_";

    /** Constant : SORT_ASC. */
    public static final String SORT_ASC = "asc";

    /** Constant : SORT_DEFAULT. */
    public static final String SORT_DEFAULT = "default";

    /** Constant : SORT_DESC. */
    public static final String SORT_DESC = "desc";

    // attributs state
    /** Constant : STATE. */
    public static final String STATE = "s_";

    /** Constant : STATE_DEFAULT. */
    public static final String STATE_DEFAULT = "default";

    /** Constant : STATE_NOTIFY_TO_DEFAULT. */
    public static final String STATE_NOTIFY_TO_DEFAULT = "notifyToDefault";

    /** Constant : STATE_NOTIFY_TO_PERSIST. */
    public static final String STATE_NOTIFY_TO_PERSIST = "notifyToPersist";

    /** Constant : STATE_PERSIST. */
    public static final String STATE_PERSIST = "persist";

    // attributs total
    /** Constant : TOTAL_ROWS. */
    public static final String TOTAL_ROWS = "totalRows";

    /** Constant : TOTALPAGECOUNT. */
    public static final String TOTALPAGECOUNT = "TOTALPAGECOUNT";

    /** Constant : TOTALROWCOUNT. */
    public static final String TOTALROWCOUNT = "TOTALROWCOUNT";

    /** Constant : VIEW_CPJRXML. */
    public static final String VIEW_CPJRXML = "cpjrxml";

    /** Constant : VIEW_CSV. */
    public static final String VIEW_CSV = "csv";

    /** Constant : VIEW_DATA. */
    public static final String VIEW_DATA = "viewData";

    // attributs view
    /** Constant : VIEW_HTML. */
    public static final String VIEW_HTML = "html";

    /** Constant : VIEW_ODS. */
    public static final String VIEW_ODS = "ods";

    /** Constant : VIEW_PDF. */
    public static final String VIEW_PDF = "pdf";

    /** Constant : VIEW_RESOLVER. */
    public static final String VIEW_RESOLVER = "viewResolver";

    /** Constant : VIEW_XLS. */
    public static final String VIEW_XLS = "xls";

    /**
     * Instanciation de table constants.
     */
    private TableConstants()
    {
    }
}
