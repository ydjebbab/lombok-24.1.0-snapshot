/*
/*Copyright (c) 2014 DGFiP - Tous droits réservés/* * Copyright 2004 original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package fr.gouv.finances.lombok.apptags.table.bean;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import fr.gouv.finances.lombok.apptags.table.core.MessagesConstants;
import fr.gouv.finances.lombok.apptags.table.core.PreferencesConstants;
import fr.gouv.finances.lombok.apptags.table.core.RetrievalUtils;
import fr.gouv.finances.lombok.apptags.table.core.TableConstants;
import fr.gouv.finances.lombok.apptags.table.core.TableModel;
import fr.gouv.finances.lombok.apptags.table.core.TableModelUtils;
import fr.gouv.finances.lombok.apptags.util.ExtremeUtils;

/**
 * Positionne toutes les valeurs par défaut du tag ColumnTag. Les valeurs par défaut sont issues de fichier properties
 * ou d'un classe contenant des constantes
 */
public final class ColumnDefaults
{

    /** log. */
    private static Log log = LogFactory.getLog(ColumnDefaults.class);

    /**
     * Accesseur de l attribut alias.
     *
     * @param alias alias
     * @param property property
     * @return alias
     */
    public static String getAlias(String alias, String property)
    {
        String result = alias;
        if (StringUtils.isBlank(alias) && StringUtils.isNotBlank(property))
        {
            result = property;
        }

        return result;
    }

    /**
     * Accesseur de l attribut calc class.
     *
     * @param model model
     * @param calcClass calcClass
     * @return calc class
     */
    public static String getCalcClass(TableModel model, String calcClass)
    {
        String result = calcClass;
        if (StringUtils.isBlank(calcClass))
        {
            result = model.getPreferences().getPreference(PreferencesConstants.COLUMN_CALC_CLASS);
        }

        return result;
    }

    /**
     * Retourne la liste des titres des cellules calculées.
     *
     * @param model model
     * @param calcTitle calcTitle
     * @return calc title
     */
    public static List<String> getCalcTitle(TableModel model, List<String> calcTitle)
    {
        List<String> results = new ArrayList<String>();

        for (String title : calcTitle)
        {
            if (TableModelUtils.isResourceBundleProperty(title))
            {
                String resourceValue = model.getMessages().getMessage(title);
                if (resourceValue == null)
                {
                    resourceValue = title;
                }

                if (StringUtils.isNotBlank(resourceValue))
                {
                    results.add(resourceValue);
                }
            }
            else
            {
                results.add(title);
            }
        }

        return results;
    }

    /**
     * Accesseur de l attribut calc title class.
     *
     * @param model model
     * @param calcTitleClass calcTitleClass
     * @return calc title class
     */
    public static String getCalcTitleClass(TableModel model, String calcTitleClass)
    {
        String result = calcTitleClass;

        if (StringUtils.isBlank(calcTitleClass))
        {
            result = model.getPreferences().getPreference(PreferencesConstants.COLUMN_CALCTITLE_CLASS);
        }
        return result;
    }

    /**
     * Accesseur de l attribut cell.
     *
     * @param model model
     * @param cell cell
     * @return cell
     */
    public static String getCell(TableModel model, String cell)
    {
        return getReadPropValue(model, PreferencesConstants.COLUMN_CELL, cell, TableConstants.CELL_DISPLAY);
    }

    /**
     * Accesseur de l attribut filter cell.
     *
     * @param model model
     * @param filterCell filterCell
     * @return filter cell
     */
    public static String getFilterCell(TableModel model, String filterCell)
    {
        return getReadPropValue(model, PreferencesConstants.COLUMN_FILTER_CELL, filterCell, TableConstants.CELL_FILTER);
    }

    /**
     * Accesseur de l attribut filter null option.
     *
     * @param model model
     * @param filterNullOption filterNullOption
     * @return filter null option
     */
    public static String getFilterNullOption(TableModel model, String filterNullOption)
    {
        if (filterNullOption == null)
        {
            return model.getPreferences().getPreference(PreferencesConstants.COLUMN_FILTER_NULL_OPTION);
        }

        return filterNullOption;
    }

    /**
     * Accesseur de l attribut filter options.
     *
     * @param model model
     * @param filterOptions filterOptions
     * @return filter options
     */
    public static Collection<Object> getFilterOptions(TableModel model, Object filterOptions)
    {

        Collection<Object> result = null;
        try
        {
            if (filterOptions != null)
            {
                result = RetrievalUtils.retrieveCollection(model.getContext(), filterOptions);
            }
        }
        catch (Exception exception)
        {
            // On ne fait rien
            log.warn("Erreur lors de la lecture de la collection.");
        }

        return result;
    }

    /**
     * If this is a named format then it should be in the resource bundle. For backwards compatibility check the
     * properties file also.
     *
     * @param model model
     * @param column column
     * @param format format
     * @return format
     */
    public static String getFormat(TableModel model, Column column, String format)
    {
        String result = getFormatInResourceBundle(model, column, format);
        if (StringUtils.isBlank(result))
        {
            result = getFormatInProperties(model, column, format);
        }

        if (StringUtils.isNotBlank(result))
        {
            return result;
        }

        return format;
    }

    /**
     * Accesseur de l attribut format in properties.
     *
     * @param model model
     * @param column column
     * @param format format
     * @return format in properties
     */
    public static String getFormatInProperties(TableModel model, Column column, String format)
    {
        String result = null;
        if (StringUtils.isNotBlank(format) && isNamedFormat(format))
        {
            result = model.getPreferences().getPreference(PreferencesConstants.COLUMN_FORMAT + format);
        }

        else if (StringUtils.isBlank(format))
        {
            if (column.isCurrency())
            {
                result =
                    model.getPreferences().getPreference(PreferencesConstants.COLUMN_FORMAT + TableConstants.CURRENCY);
            }
            else if (column.isDate())
            {
                result = model.getPreferences().getPreference(PreferencesConstants.COLUMN_FORMAT + TableConstants.DATE);
            }
        }

        return result;
    }

    /**
     * Accesseur de l attribut format in resource bundle.
     *
     * @param model model
     * @param column column
     * @param format format
     * @return format in resource bundle
     */
    public static String getFormatInResourceBundle(TableModel model, Column column, String format)
    {
        String result = null;

        if (StringUtils.isNotBlank(format) && isNamedFormat(format))
        {
            result = model.getMessages().getMessage(MessagesConstants.COLUMN_FORMAT + format);
        }

        else if (StringUtils.isBlank(format))
        {
            if (column.isCurrency())
            {
                result = model.getMessages().getMessage(MessagesConstants.COLUMN_FORMAT + TableConstants.CURRENCY);
            }
            else if (column.isDate())
            {
                result = model.getMessages().getMessage(MessagesConstants.COLUMN_FORMAT + TableConstants.DATE);
            }
        }

        return result;
    }

    /**
     * Accesseur de l attribut header cell.
     *
     * @param model model
     * @param headerCell headerCell
     * @return header cell
     */
    public static String getHeaderCell(TableModel model, String headerCell)
    {
        return getReadPropValue(model, PreferencesConstants.COLUMN_HEADER_CELL, headerCell, TableConstants.CELL_HEADER);
    }

    /**
     * Accesseur de l attribut header class.
     *
     * @param model model
     * @param headerClass headerClass
     * @return header class
     */
    public static String getHeaderClass(TableModel model, String headerClass)
    {
        String result = headerClass;

        if (StringUtils.isBlank(headerClass))
        {
            result = model.getPreferences().getPreference(PreferencesConstants.TABLE_HEADER_CLASS);
        }

        return result;
    }

    /**
     * Accesseur de l attribut parses the.
     *
     * @param model model
     * @param column column
     * @param parse parse
     * @return parses the
     */
    public static String getParse(TableModel model, Column column, String parse)
    {
        String result = parse;
        if (StringUtils.isNotBlank(parse))
        {
            result = parse;
        }
        else if (column.isDate())
        {
            result = model.getPreferences().getPreference(PreferencesConstants.COLUMN_PARSE + TableConstants.DATE);
        }

        return result;
    }

    /**
     * Lecture dans les préférences du modèle de la valeur identifiée par les propriétés en paramètre.
     *
     * @param model model
     * @param propertyRacine propertyRacine
     * @param propertyName propertyName
     * @param propertyNameDefault propertyNameDefault
     * @return read prop value
     */
    public static String getReadPropValue(TableModel model, String propertyRacine, String propertyName,
        String propertyNameDefault)
    {
        String result;
        StringBuilder preferenceProperty = new StringBuilder();
        preferenceProperty.append(propertyRacine);
        if (StringUtils.isNotBlank(propertyName))
        {
            preferenceProperty.append(propertyName);
            result = model.getPreferences().getPreference(preferenceProperty.toString());
            if (StringUtils.isBlank(result))
            {
                result = propertyName;
            }
        }
        else
        {
            preferenceProperty.append(propertyNameDefault);
            result = model.getPreferences().getPreference(preferenceProperty.toString());
        }

        return result;

    }

    /**
     * Retourne le titre d'une colonne soit en utilisant la valeur de l'attribut 'title', soit la propriété associée à
     * la colonne, soit en utilisant l'attribut 'title' comme une clé d'un fichier *.properties
     *
     * @param model model
     * @param title title
     * @param property property
     * @return titre de la colonne
     */
    public static String getTitle(TableModel model, String title, String property)
    {
        String result = title;
        if (StringUtils.isEmpty(title))
        {
            result = ExtremeUtils.camelCaseToWord(property);
        }

        else if (TableModelUtils.isResourceBundleProperty(title))
        {
            String resourceValue = model.getMessages().getMessage(title);
            if (resourceValue != null)
            {
                result = resourceValue;
            }
        }

        return result;
    }

    /**
     * Accesseur de l attribut title pdf.
     *
     * @param model model
     * @param titlePdf titlePdf
     * @return title pdf
     */
    public static String getTitlePdf(TableModel model, String titlePdf)
    {
        return titlePdf;
    }

    /**
     * Teste si la table est "compacte".
     *
     * @param model model
     * @param compact compact
     * @return boolean
     */
    public static Boolean isCompact(TableModel model, Boolean compact)
    {
        Boolean result = compact;
        if (compact == null)
        {
            result = Boolean.valueOf(model.getTableHandler().getTable().isCompact());
        }

        return result;
    }

    /**
     * methode Checks if is escape auto format :
     *
     * @param model model
     * @param escapeAutoFormat escapeAutoFormat
     * @return boolean
     */
    public static Boolean isEscapeAutoFormat(TableModel model, Boolean escapeAutoFormat)
    {
        Boolean result = escapeAutoFormat;
        if (escapeAutoFormat == null)
        {
            result =
                Boolean.valueOf(model.getPreferences().getPreference(PreferencesConstants.COLUMN_ESCAPE_AUTO_FORMAT));
        }

        return result;
    }

    /**
     * Teste si la table est "filtrable".
     *
     * @param model model
     * @param filterable filterable
     * @return boolean
     */
    public static Boolean isFilterable(TableModel model, Boolean filterable)
    {
        Boolean result = filterable;
        if (filterable == null)
        {
            result = Boolean.valueOf(model.getTableHandler().getTable().isFilterable());
        }

        return result;
    }

    /**
     * methode Checks if is filter strict :
     *
     * @param model model
     * @param filterStrict filterStrict
     * @return boolean
     */
    public static Boolean isFilterStrict(TableModel model, Boolean filterStrict)
    {
        if (filterStrict == null)
        {
            return Boolean.valueOf(model.getPreferences().getPreference(PreferencesConstants.COLUMN_FILTER_STRICT));
        }

        return filterStrict;
    }

    /**
     * If the format contains any of these formats then it is custom format doing inline.
     *
     * @param format format
     * @return true, si c'est named format
     */
    public static boolean isNamedFormat(String format)
    {
        boolean result = false;
        char[] args = {'#', '/', '-'};
        if (StringUtils.containsNone(format, args))
        {
            result = true;
        }

        return result;
    }

    /**
     * Teste si la table est "triable".
     *
     * @param model model
     * @param sortable sortable
     * @return boolean
     */
    public static Boolean isSortable(TableModel model, Boolean sortable)
    {
        Boolean result = sortable;
        if (sortable == null)
        {
            result = Boolean.valueOf(model.getTableHandler().getTable().isSortable());
        }

        return result;
    }

    /**
     * methode Checks if is write or blank :
     *
     * @param model model
     * @param writeOrBlank writeOrBlank
     * @return boolean
     */
    public static Boolean isWriteOrBlank(TableModel model, Boolean writeOrBlank)
    {
        return writeOrBlank;
    }

    /**
     * Accesseur de l attribut dataAttributes.
     *
     * @return dataAttributes
     */
    static String getDataAttributes(TableModel model, String _dataAttributes)
    {
        if (_dataAttributes == null)
        {
            return model.getPreferences().getPreference(PreferencesConstants.TABLE_DATA_ATTRIBUTES);
        }
        return _dataAttributes;
    }

    /**
     * Accesseur de l attribut responsive.
     *
     * @return responsive (true or false)
     */
    static Boolean isShowResponsive(TableModel model, Boolean _showResponsive)
    {
        if (_showResponsive == null)
        {
            return Boolean.valueOf(model.getPreferences().getPreference(PreferencesConstants.TABLE_SHOW_RESPONSIVE));
        }
        return _showResponsive;
    }
    
    /**
     * Accesseur de l attribut escapeXml.
     *
     * @return escapeXml (true or false)
     */
    static Boolean getEscapeXml(TableModel model, Boolean _escapeXml)
    {
        if (_escapeXml == null)
        {
            return Boolean.valueOf(model.getPreferences().getPreference(PreferencesConstants.COLUMN_ESCAPE_XML));
        }
        return _escapeXml;
    }

}
