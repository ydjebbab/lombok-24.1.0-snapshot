/*
/*Copyright (c) 2014 DGFiP - Tous droits réservés/* * Copyright 2004 original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package fr.gouv.finances.lombok.apptags.table.bean;

import org.apache.commons.lang.StringUtils;

import fr.gouv.finances.lombok.apptags.table.core.PreferencesConstants;
import fr.gouv.finances.lombok.apptags.table.core.TableModel;
import fr.gouv.finances.lombok.apptags.table.core.TableModelUtils;

/**
 * Pull all the default values for the ExportTag. Because the default values could be coming from the properties or
 * resource bundle this class will abstract that out.
 */
public final class ExportDefaults
{

    /**
     * Accesseur de l attribut edition service.
     *
     * @param model
     * @param editionService
     * @return edition service
     */
    public static String geteditionService(TableModel model, String editionService)
    {
        String result = null;

        if (StringUtils.isNotBlank(editionService))
        {
            result = model.getPreferences().getPreference(PreferencesConstants.EXPORT_EDITION_SERVICE + editionService);
            if (StringUtils.isBlank(result))
            {
                result = editionService;
            }
        }

        return result;
    }

    /**
     * Accesseur de l attribut text.
     *
     * @param model
     * @param text
     * @return text
     */
    public static String getText(TableModel model, String text)
    {
        if (TableModelUtils.isResourceBundleProperty(text))
        {
            String resourceValue = model.getMessages().getMessage(text);
            if (resourceValue != null)
            {
                return resourceValue;
            }
        }

        return text;
    }

    /**
     * Accesseur de l attribut tooltip.
     *
     * @param model
     * @param tooltip
     * @return tooltip
     */
    public static String getTooltip(TableModel model, String tooltip)
    {
        if (TableModelUtils.isResourceBundleProperty(tooltip))
        {
            String resourceValue = model.getMessages().getMessage(tooltip);
            if (resourceValue != null)
            {
                return resourceValue;
            }
        }

        return tooltip;
    }

    /**
     * Accesseur de l attribut view resolver.
     *
     * @param model
     * @param viewResolver
     * @return view resolver
     */
    public static String getviewResolver(TableModel model, String viewResolver)
    {
        String result = null;

        if (StringUtils.isNotBlank(viewResolver))
        {
            result = model.getPreferences().getPreference(PreferencesConstants.EXPORT_VIEW_RESOLVER + viewResolver);
            if (StringUtils.isBlank(result))
            {
                result = viewResolver;
            }
        }

        return result;
    }

    /**
     * Accesseur de l attribut encoding.
     *
     * @param model
     * @param encoding
     * @return encoding
     */
    static String getEncoding(TableModel model, String encoding)
    {
        if (StringUtils.isBlank(encoding))
        {
            return model.getPreferences().getPreference(PreferencesConstants.EXPORT_ENCODING);
        }

        return encoding;
    }
}
