<%@ tag dynamic-attributes="attributes" isELIgnored="false" pageEncoding="UTF-8"
    description="Insère un élément 'textarea' et l'associe (bind) à un bean"%>
    <%@ include file="/META-INF/resources/WEB-INF/tags/html/includes.tagf" %>
    
<%@ attribute name="path" required="true" description="Chemin complet du bean à associer (binder)"%>

<%@ attribute name="ignorenullnestedpath"  %>	

<%@ attribute name="readOnly" description="'true' indique que le contrôle est en lecture seule" %>

<%@ attribute name="disabled" description="'true' indique que le contrôle est désactivé" %>
	
<%@ attribute name="disabledwhenreadonly" 
	description="'true' indique que si le contrôle est en lecture seule alors il est aussi désactivé" 
%>	

<c:set var="_ignorenullnestedpath" value="${ignorenullnestedpath}" />


<%@ include file="/META-INF/resources/WEB-INF/tags/html/fillstatus_value.tagf" %>

<html:attributes var="attrString" attributeMap="${attributes}" name="${status_expression}">
	<c:if test="${readOnly == 'true'}">
		<c:set var="attrString" value="${attrString} readonly='readonly'"/>
	</c:if>

	<c:if test="${disabled == 'true'}">
		<c:set var="attrString" value="${attrString} disabled='disabled'"/>
	</c:if>

	<c:if test="${readOnly == 'true'}">
		<c:if test="${disabledwhenreadonly == 'true' && ( ! (disabled == 'true') )  && ( ! (disabled == 'false') )}">
			<c:set var="attrString" value="${attrString} disabled='disabled'"/>
		</c:if>
	</c:if>

	 <textarea ${attrString}>${fn:escapeXml(status_value)}</textarea> 
    
</html:attributes>