<%@ tag body-content="empty" isELIgnored="false" pageEncoding="UTF-8"
    description="Insère une liste d'éléments html 'option' en utilisant la collection passée en 
                 paramètres."%>
<%@ attribute name="items" type="java.lang.Object" rtexprvalue="true" 
	description="Collection d'objets à utiliser pour construire une liste d'options."%>
<%@ attribute name="label" 
	description="Propriété de chaque objet à utiliser comme label de l'option.
	Si cette option n'est pas spécifiée, la propriété utilisée pour le label est la même que celle 
	utilisée pout la valeur de l'option"%>
<%@ attribute name="value" description="Propriété de chaque objet à utiliser pour affecter la valeur de l'option."%>
<%@ attribute name="selected" type="java.lang.Object" description="Valeur sélectionnée initialement."%>
<%@ attribute name="selectedValues" type="java.lang.Object" description="Valeurs sélectionnées initialement."%>
<%@ attribute name="readOnly"  description="Contrôle en lecture seule."%>

<%@ include file="/META-INF/resources/WEB-INF/tags/html/includes.tagf" %>
<c:catch var="excep">
<html:attributes var="attrString" attributeMap="${attributes}">
	<c:forEach var="item" items="${items}">
		<c:set var="todisplay" value="true" />		
		<c:choose>
			<c:when test="${!empty label}">				
				<c:set var="lbl" value="${item[label]}" />
			</c:when>
			<c:otherwise>
				<c:set var="lbl" value="${item}" />
			</c:otherwise>
		</c:choose>
		<c:choose>
			<c:when test="${!empty value}">
				<c:set var="val" value="${item[value]}" />
				<c:set var="val"><c:out value="${val}" escapeXml="false"/></c:set>
			</c:when>
			<c:otherwise>
				<c:set var="val" value="${lbl}" />
				<%-- Pour éviter un accès par EL aux enum--%>
				<c:set var="val"><c:out value="${val}" escapeXml="false"/></c:set>
			</c:otherwise>
		</c:choose>
		<c:set var="newAttrString">
	        <c:out escapeXml="false" value="value=\""/><c:out value="${val}"/><c:out escapeXml="false"
	    	       value="\" ${attrString}"/>
	    </c:set>
		<c:choose>
			<c:when test="${!empty selected }">
				<c:choose>
					<c:when test="${selected == val}">
						<c:if test="${readOnly != 'true'}">
							<c:set var="newAttrString" value="selected='selected' ${newAttrString}" />
						</c:if>
					</c:when>
					<c:otherwise>
						<c:if test="${readOnly == 'true'}">
						 	<c:set var="todisplay" value="false" /> 
						</c:if>						
					</c:otherwise>
				</c:choose>
			</c:when>
			<c:when test="${!empty selectedValues}">
				<c:set var="onefound" value='false'/>
				<c:forEach var="sel" items="${selectedValues}">
					<c:choose>
						<c:when test="${sel == val}">
							<c:if test="${readOnly != 'true'}">
								<c:set var="newAttrString" value="selected='selected' ${newAttrString}" />
							</c:if>
							<c:set var="onefound" value='true'/>
							<c:set var="todisplay" value="true" />
						</c:when>
						<c:otherwise>
							<c:if test="${readOnly == 'true'}">
								<c:if test="${onefound == 'false'}">
							 		<c:set var="todisplay" value="false" /> 
							 	</c:if>
							</c:if>						
						</c:otherwise>
					</c:choose>				
				</c:forEach>
			</c:when>
		</c:choose>
		<c:if test="${todisplay == 'true'}">
		<option ${newAttrString}><c:out value="${lbl}" /></option>
		</c:if>
	</c:forEach>
</html:attributes>
</c:catch>
<c:if test="${!empty excep}">
	<c:set var="tempo" value="<li>${excep.message}</li>"/>
	<c:set var="exceptionCatchedInTag" value="${exceptionCatchedInTag}${tempo}" scope="request"/>
	<option>Erreur de programmation</option>
</c:if>