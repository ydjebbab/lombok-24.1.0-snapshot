<%@ tag dynamic-attributes="attributes" 
		isELIgnored="false" 
		pageEncoding="UTF-8"
    	description="Insère un élément html de type 'button'" display-name="button"%>
    	
<%@ attribute name="disabled" description="true: le bouton possède l'attribut 'disabled'" %>	
<%@ include file="/META-INF/resources/WEB-INF/tags/html/includes.tagf" %>

	<html:attributes var="attrString" attributeMap="${attributes}" type="button">
	<c:choose>
		<c:when test="${disabled == 'true'}">
			<button ${attrString} disabled="disabled"><jsp:doBody/></button>
		</c:when>
		<c:otherwise>
			<button ${attrString}><jsp:doBody/></button>
		</c:otherwise>
	</c:choose>
	</html:attributes>
