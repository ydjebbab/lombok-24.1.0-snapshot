<%@ tag body-content="empty" %>
<%@ tag dynamic-attributes="attributes" isELIgnored="false" pageEncoding="UTF-8"
	description="Insére une case à cocher ou un bouton radio"
%>
<%@ include file="/META-INF/resources/WEB-INF/tags/html/includes.tagf" %>
<%@ attribute name="value" required="true" type="java.lang.Object" description="Valeur de la case à cocher"%>

<%@ attribute name="path" required="true" description="Chemin du champ à binder"  %>

<%@ attribute name="type" 
description="multibox: insertion d'une case à cocher. radio : insertion d'un bouton radio."%>

<%@ attribute name="readOnly"  description="true: le champ est en lecture seule, sinon false"%>
	
<%@ attribute name="ignorenullnestedpath"  %>	

<%@ attribute name="elementPath" %>

<c:set var="_ignorenullnestedpath" value="${ignorenullnestedpath}" />

<%@ include file="/META-INF/resources/WEB-INF/tags/html/fillstatus_value.tagf" %>

	<c:set var="inputType" value="${type}"/>
	<c:if test="${! readOnly == 'true'}">
		<input type="hidden" name="_<c:out value="${status_expression}"/>" />
	</c:if>

	<c:if test="${inputType != \"radio\"}"><c:set var="inputType" value="checkbox"/></c:if>
	<html:attributes var="attrString" attributeMap="${attributes}" type="${inputType}" name="${status_expression}" value="${value}">
		<c:choose>
			<c:when test="${type == 'multibox'}">
				<c:choose>
					<%-- La propriété bindée est de type Boolean ou boolean --%>
					<c:when test="${! empty status_value && 
					                (status_valueType.name == 'java.lang.Boolean' || status_valueType.name == 'boolean')}">
						<c:if test="${status_value == value}">
							<c:set var="isChecked" value="checked='checked' "/>
						</c:if>
					</c:when>
					<c:otherwise>
						<c:forEach var="val" items="${status_value}">
							<c:choose>
								<c:when test="${!empty elementPath}">
								    <c:set var="value" ><c:out value="${value}" escapeXml="false" /></c:set>
								    <c:set var="valueelt" ><c:out value="${val[elementPath]}" escapeXml="false" /></c:set>
									<c:if test="${valueelt == value}">
										<c:set var="isChecked" value="checked='checked' "/>
									</c:if>
								</c:when>
								<c:otherwise>
								    <c:set var="value" ><c:out value="${value}" escapeXml="false" /></c:set>
					                    <c:if test="${fn:toLowerCase(val) == fn:toLowerCase(value)}">
										<c:set var="isChecked" value="checked='checked' "/>
									</c:if>
								</c:otherwise>
							</c:choose>
						</c:forEach>
					</c:otherwise>
				</c:choose>
			</c:when>
			<c:otherwise>
			    <c:set var="value" ><c:out value="${value}" escapeXml="false" /></c:set>
				<c:if test="${fn:toLowerCase(status_value) == fn:toLowerCase(value)}">
					<c:set var="isChecked" value="checked='checked' "/>
				</c:if>
			</c:otherwise>
		</c:choose>
		
		<c:set var="parametres" value=" "/>
		<c:if test="${! empty isChecked}">
		  <c:set var="parametres" ><c:out value="${isChecked} " escapeXml="false" /></c:set>
		</c:if>
		
		<c:if test="${! empty attrString}">
          <c:set var="parametres" ><c:out value="${parametres}" escapeXml="false" /><c:out value="${attrString}" escapeXml="false" /></c:set>
        </c:if>
		
		<c:choose>
		<c:when test="${readOnly == 'true'}">
			<input ${parametres} readonly="readonly" disabled="disabled" />
		</c:when>
		<c:otherwise>
			<input ${parametres} />
		</c:otherwise>
	</c:choose>
	</html:attributes>