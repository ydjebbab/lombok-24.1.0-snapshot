<%@ tag dynamic-attributes="attributes" isELIgnored="false" pageEncoding="UTF-8"
	description="Itère sur la liste d'erreur associée au bean spécifié par l'arribut 'path'.
	Le corps du tag est exécuté pour chaque erreur."%>
<%@ include file="/META-INF/resources/WEB-INF/tags/html/includes.tagf" %>

<%@ attribute name="path" required="true" description="Chemin vers le bean à utiliser"%>

<%@ attribute name="var" required="true" rtexprvalue="false" 
	description="variable dans laquelle le message d'erreur est exposé."%>
	
<%@ attribute name="listeexceptions" required="false"
	description="Liste des exceptions qui doivent être remplacées par un message standard."%>

<app:initvar attrname="_listeexceptions" attrvalue="${listeexceptions}"
	bundlekey="html.foreacherror.listeexceptions" defaultvalue="" />

<%@ variable name-from-attribute="var" alias="errorMessage" %>

<spring:bind path="${path}">
    <c:forEach var="error" items="${status.errorMessages}">
        <c:set var="errorMessage" value="${error}" />
        <c:if test="${!empty  errorMessage}">
            <c:forTokens var="exception" delims="," items="${_listeexceptions}">
                <c:if test="${fn:contains(error, exception)}">
                    <c:set var="errorMessage" value="Erreur de saisie" />
                </c:if>
            </c:forTokens>
            <jsp:doBody />
        </c:if>
    </c:forEach>
</spring:bind>
