<%--
	- options
	- 
	- Display a list of html option tags using the given items collection.
	- Accepts dynamic attributes.
	-
	- @param items a collection of items used to generate option tags (required)
	-     If labelProperty is not specified, the value of each item will be used
	-     as both the label and the value.
	- @param label the property on each item to use as the option label
	-     Required if valueProperty is specified.
	- @param value the property on each item to use as the option value
	- @param selected the value that should be selected initially. (this
	-     attribute is mutually exclusive with "selectedValues").
	- @param selectedValues the values that should be selected initially (this
	-     attribute is mutually exclusive with "selected").
	--%>

<%@ tag dynamic-attributes="attributes" isELIgnored="false" pageEncoding="UTF-8"
	description="Insère une liste d'élément html 'option' "%>
<%@ attribute name="label" description=""%>
<%@ attribute name="value" required="true" description=""%>
<%@ attribute name="selected" type="java.lang.Object" description=""%>
<%@ attribute name="selectedValues" type="java.lang.Object" description=""%>
<%@ attribute name="readOnly"  description=""%>

<%@ include file="/META-INF/resources/WEB-INF/tags/html/includes.tagf" %>

<html:attributes var="attrString" attributeMap="${attributes}">
 	<c:set var="todisplay" value="true" /> 
	<c:choose>
		<c:when test="${!empty label}">
			<c:set var="lbl" value="${label}"/>
		</c:when>
		<c:otherwise>
			<c:set var="lbl"><jsp:doBody /></c:set>
		</c:otherwise>
	</c:choose>
	
	<c:set var="val" value="${value}"/>
	<c:set var="newAttrString">
        <c:out escapeXml="false" value="value=\""/><c:out value="${val}"/><c:out escapeXml="false"
    	       value="\" ${attrString}"/>
    </c:set>
	<c:choose>
		<c:when test="${!empty selected}">
			<c:choose>
				<c:when test="${selected == val}">
					<c:if test="${readOnly != 'true'}">
						<c:set var="newAttrString" value="selected=\"selected\" ${newAttrString}" />
					</c:if>
				</c:when>
				<c:otherwise>
					<c:if test="${readOnly == 'true'}">
					 	<c:set var="todisplay" value="false" /> 
					</c:if>						
				</c:otherwise>
			</c:choose>
		</c:when>
		<c:when test="${!empty selectedValues}">
			<c:set var="onefound" value='false'/>
				<c:forEach var="sel" items="${selectedValues}">
					<c:choose>
						<c:when test="${sel == val}">
							<c:if test="${readOnly != 'true'}">
								<c:set var="newAttrString" value="selected=\"selected\" ${newAttrString}" />
							 </c:if>	
							<c:set var="onefound" value='true'/>
							<c:set var="todisplay" value="true" />
						</c:when>
						<c:otherwise>
							<c:if test="${readOnly == 'true' && onefound == 'false'}">
						 		<c:set var="todisplay" value="false" /> 
							</c:if>						
						</c:otherwise>
					</c:choose>				
				</c:forEach>
			</c:when>
	</c:choose>
	
	<c:if test="${todisplay == 'true'}">
	<option ${newAttrString}><c:out value="${lbl}" /></option>
	</c:if>
</html:attributes>
