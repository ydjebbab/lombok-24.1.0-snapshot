<%@ tag dynamic-attributes="attributes" isELIgnored="false" pageEncoding="UTF-8"
    description="Insère un élément 'input' et le bind avec la propriété d'un bean"%>
<%@ include file="/META-INF/resources/WEB-INF/tags/html/includes.tagf" %>
<%@ attribute name="readOnly" description="'true' indique que le contrôle est en lecture seule" %>	
<%@ attribute name="disabled" description="'true' indique que le contrôle est désactivé" %>	
<%@ attribute name="type" description="Type de l'élément input" %>
<%@ attribute name="disabledwhenreadonly" description="'true' indique que si le contrôle est en lecture seule alors il est aussi désactivé" %>	
<%@ attribute name="ignorenullnestedpath"  %>	

<%@ attribute name="path" required="true" description="Chemin complet de la propriété associée (bindée) au contrôle (obligatoire)."%>

<c:set var="_ignorenullnestedpath" value="${ignorenullnestedpath}" />
<%@ include file="/META-INF/resources/WEB-INF/tags/html/fillstatus_value.tagf" %>

<html:attributes var="attrString" attributeMap="${attributes}" type="${type}" name="${status_expression}" value="${status_value}">


<c:if test="${readOnly == 'true'}">
   <c:set var="attrString" value="${attrString} readonly='readonly'" />
</c:if>
<c:if test="${(disabled == 'true') || ( (readOnly == 'true') && (disabledwhenreadonly == 'true') )}">
   <c:set var="attrString" value="${attrString} disabled='disabled'" />
</c:if>


<input ${attrString}/>

</html:attributes>
