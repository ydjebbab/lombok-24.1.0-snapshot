<%--
	- Expose a string of HTML attributes from the given map of attributes.
	- Dynamic attributes specified will be added to the string if they do
	- not already exist in the map. This tag exposes a variable with the
	- name specified in the 'var' attribute.
	-
	- @param var the variable in which the string attributes will be exposed.
	-     (required)
	- @param attributeMap a map of attributes to convert to a string of
	-     name/value pairs.
	--%>

<%@ include file="/META-INF/resources/WEB-INF/tags/html/includes.tagf" %>
<%@ tag dynamic-attributes="attributes" isELIgnored="false" pageEncoding="UTF-8"
	description="Constuit une chaîne contenant des attributs HTML sous forme d'une chaîne de type
	'nom attribut / valeur' à partir de la Map passée en paramètres. Les attributs dynamiques sont
	ajouté à la chaîne s'ils ne sont pas déjà présents dans la Map.
	Le tag expose une variable dont le nom est spécifié par le paramètre 'var'"
	%>

<%@ attribute name="var" required="true" rtexprvalue="false" 
	description="Variable dans laquelle la chaîne d'attribut est stockée"%>
	
<%@ attribute name="attributeMap" type="java.util.Map" 
	description="Map d'attributs à convertir en chaîne de type nom attribut / valeur"%>
	
<%@ variable name-from-attribute="var" alias="attrString" declare="false" %>

<c:set var="attrString" value=" "/>
<c:forEach var="attr" items="${attributeMap}">
    <c:if test="${! empty attr.value}">
    <c:set var="attrString">
    <c:if test="${! empty attrString}">
    <c:out escapeXml="false" value="${attrString} "/>
    </c:if>
    <c:if test="${! empty attr.key}">
    <c:out escapeXml="false" value="${attr.key}="/>
    </c:if>
    <c:out escapeXml="false" value="\""/>   
    <c:out value="${attr.value}"/><c:out escapeXml="false" value="\""/>
    </c:set>
    </c:if>
</c:forEach>

<c:forEach var="attr" items="${attributes}">
    <c:if test="${empty attributeMap[attr.key] && ! empty attr.value}">
        <c:set var="attrString">
        <c:out escapeXml="false" value="${attrString} "/>
        <c:if test="${! empty attr.key}">
         <c:out escapeXml="false" value="${attr.key}="/>
        </c:if>
        <c:out escapeXml="false" value="\""/>
        <c:out value="${attr.value}"/><c:out escapeXml="false"
            value="\""/>
        </c:set>
    </c:if>
</c:forEach>
<jsp:doBody />