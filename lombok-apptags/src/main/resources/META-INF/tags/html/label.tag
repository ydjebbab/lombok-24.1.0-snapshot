<%@ tag dynamic-attributes="attributes" isELIgnored="false" pageEncoding="UTF-8"
description="Insère un élément 'label' bindé avec un bean "%>
<%@ include file="/META-INF/resources/WEB-INF/tags/html/includes.tagf" %>

<%@ attribute name="ignorenullnestedpath"  %>	

<%@ attribute name="path" required="true" description="Chemin complet du bean bindé"%>

<c:set var="_ignorenullnestedpath" value="${ignorenullnestedpath}" />



<%@ include file="/META-INF/resources/WEB-INF/tags/html/fillstatus_value.tagf" %>

<html:attributes var="attrString" attributeMap="${attributes}" >
 	<label ${attrString}>${fn:escapeXml(status_value)}</label>	    
</html:attributes>