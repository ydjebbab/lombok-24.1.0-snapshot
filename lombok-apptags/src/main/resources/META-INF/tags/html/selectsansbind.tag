<%@ include file="/META-INF/resources/WEB-INF/tags/html/includes.tagf" %>
<%@ tag dynamic-attributes="attributes" isELIgnored="false" pageEncoding="UTF-8"
	description="Insère un élément 'select' sans le binder à un bean"
%>

<%@ attribute name="readOnly" description="'true' indique que le contrôle est en lecture seule" %>

<%@ attribute name="disabled" description="'true' indique que le contrôle est désactivé" %>
	
<%@ attribute name="disabledwhenreadonly" 
	description="'true' indique que si le contrôle est en lecture seule alors il est aussi désactivé" 
%>

<html:attributes var="attrString" attributeMap="${attributes}">

<c:if test="${disabled == 'true'}">
	<c:set var="attrString" value="${attrString} disabled='disabled'"/>
</c:if>

<c:if test="${readOnly == 'true'}">
	<c:if test="${disabledwhenreadonly == 'true' && ( ! (disabled == 'true') && ( ! (disabled == 'false') ))}">
		<c:set var="attrString" value="${attrString} disabled='disabled'"/>
	</c:if>
</c:if>
<select ${attrString}>
	<jsp:doBody/>
</select>

</html:attributes>