<%@ include file="/META-INF/resources/WEB-INF/tags/html/includes.tagf" %>

<%@ tag dynamic-attributes="attributes" isELIgnored="false" pageEncoding="UTF-8"
	description="Insère un élément 'select' et l'associe à la propriété d'un bean."
	%>

<%@ include file="/META-INF/resources/WEB-INF/tags/html/fillstatus_value.tagf" %>	
	
<%@ attribute name="path" required="true" 
    description="Chemin complet de la propriété associée (bindée) au contrôle (obligatoire)."%>
    
<%@ attribute name="readOnly" description="'true' indique que le contrôle est en lecture seule" %>

<%@ attribute name="disabled" description="'true' indique que le contrôle est désactivé" %>
	
<%@ attribute name="disabledwhenreadonly" 
	description="'true' indique que si le contrôle est en lecture seule alors il est aussi désactivé" 
%>
	
<%@ attribute name="ignorenullnestedpath"  %>
	
<%@ variable name-given="selected" declare="false" %>

<c:set var="_ignorenullnestedpath" value="${ignorenullnestedpath}" />

<html:attributes var="attrString" attributeMap="${attributes}" name="${status_expression}">

<c:if test="${disabled == 'true'}">
	<c:set var="attrString" value="${attrString} disabled='disabled'"/>
</c:if>

<c:if test="${readOnly == 'true'}">
	<c:if test="${disabledwhenreadonly == 'true' && ( ! (disabled == 'true') && ( ! (disabled == 'false') ))}">
		<c:set var="attrString" value="${attrString} disabled='disabled'"/>
	</c:if>
</c:if>

<%-- Insertion de l'élément select --%>	
<select ${attrString} >
<c:set var="selected" value="${status_value}" />
<c:set var="selected"><c:out value="${selected}" escapeXml="false"/></c:set>
<jsp:doBody />
</select>
</html:attributes>