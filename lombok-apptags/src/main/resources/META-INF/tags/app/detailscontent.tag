<%@ tag dynamic-attributes="attributes" isELIgnored="false"
	pageEncoding="UTF-8"
	description="Balise html5 DETAILs, permettant de caché un texte."%>

<%@ include file="/META-INF/resources/WEB-INF/tags/html/includes.tagf" %>

<jsp:doBody/>
