<%@ tag dynamic-attributes="attributes" 
        isELIgnored="false" 
        pageEncoding="UTF-8"
        description="Insère un bouton de ré-initialisation du formulaire"%>
        
<%@ include file="/META-INF/resources/WEB-INF/tags/html/includes.tagf" %>
<%--Attributs principaux --%>
<%@ attribute name="label"
              description="Libellé du bouton"%>
<%@ attribute name="resetclass"
              description="Classe css de l'él&eacute,ment input. Par défaut = submit"%>
<%@ attribute name="displayed"
              description="true: le tag est évalué. Par défaut : true"%>

<%-- Affectation de valeurs par défaut aux attributs --%>
<app:initvar attrname="_displayed" attrvalue="${displayed}" bundlekey="app.reset.displayed" defaultvalue="true"/>
<app:initvar attrname="_label" attrvalue="${label}" bundlekey="app.reset.label" defaultvalue="Réinitialiser"/>
<app:initvar attrname="_resetclass" attrvalue="${resetclass}" bundlekey="app.reset.resetclass" defaultvalue="submit"/>

<%-- début du test d'évaluation du tag--%>
<c:if test="${_displayed == 'true' }">

<html:attributes var="attrString" attributeMap="${attributes}" 
                      type="reset" value="${_label}" class="${_resetclass}">
<input ${attrString}/>		
</html:attributes>

<%-- fin du test d'évaluation du tag--%>
</c:if>