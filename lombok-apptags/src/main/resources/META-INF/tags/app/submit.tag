<%@ tag dynamic-attributes="attributes" 
        isELIgnored="false" 
        pageEncoding="UTF-8"
        description="Insère un champ de saisie de type 'input'. Accepte comme paramètres dynamiques tous les attributs de l'élément html input."%>
        
<%@ include file="/META-INF/resources/WEB-INF/tags/html/includes.tagf" %>

<%--Attributs principaux --%>
<%@ attribute name="label"
              description="Libellé du bouton submit : attribut 'value' du champ input de type 'submit'"%>
<%@ attribute name="transition"
              description="Correspond au nom de la transition définie dans le webflow. Attribut 'name' du champ input de type 'submit'. "%>
<%@ attribute name="disabled"
              description="true : Bouton inactif"%>              
<%@ attribute name="inputclass"
              description="Classe css du bouton input"%>              
<%@ attribute name="enctype"
              description="Encodage du formulaire."%>
<%@ attribute name="displayed"
              description="true : le tag est évalué, false : il est ignoré. Par défaut : true."%>
<%@ attribute name="title"
              description="Info-bulle affichée lors du passage de la souris sur le bouton submit"%>
<%@ attribute name="responsive" description="gestion du parametre responsive."%> 
<%-- Affectation de valeurs par défaut aux attributs --%>                            
<app:initvar attrname="_disabled" attrvalue="${disabled}" bundlekey="app.submit.disabled" defaultvalue="false"/>
<app:initvar attrname="_displayed" attrvalue="${displayed}" bundlekey="app.submit.displayed" defaultvalue="true"/>
<app:initvar attrname="_inputclass" attrvalue="${inputclass}" bundlekey="app.submit.inputclass" defaultvalue="submit"/>

<%@ include file="/META-INF/resources/WEB-INF/tags/app/responsive_test.tagf" %>

<%-- début du test d'évaluation du tag--%>
<c:if test="${_displayed == 'true' }">

<c:if test="${! empty transition}">
	<%-- Complete la transition avec le préfixe ${EVENTID_FIELD_PREFIX} si nécessaire --%>
  	<str:countMatches var="nb" substring="${EVENTID_FIELD_PREFIX}">${transition}</str:countMatches>
  	<c:if test="${nb ne '1' }">
  		<c:set var="transition" value="${EVENTID_FIELD_PREFIX}${transition}"/>
  	</c:if>

	<c:set var="onmousedown">javascript:stocketransition('${transition}');removeInputEventId();initecevefn();</c:set>
	<c:if test="${! empty _formId && ! empty enctype}">
		<c:set var="onmousedown">${onmousedown};document.forms.${_formId}.encoding='${enctype}';</c:set>
	</c:if>

	<%-- Insère le champs de type submit --%>  
	<html:attributes var="attrString" attributeMap="${attributes}" 
                      type="submit" name="${transition}" title="${title}"
                      value="${label}" class="${_inputclass}"
                      onmousedown="${onmousedown}" onkeypress="${onmousedown}" onclick="${onmousedown}">                    
                      
		<c:choose>
                <c:when test="${_responsive eq 'false'}">
                    <c:choose>
			<c:when test="${_disabled == 'true'}">
				<input ${attrString} disabled="disabled" />
			</c:when>
			<c:otherwise>
				<input ${attrString} />
			</c:otherwise>
		</c:choose>		
                </c:when>
                <c:otherwise>
                    <c:choose>
                        <c:when test="${_disabled == 'true'}">
                            <button ${attrString} disabled="disabled">${label}</button>
                        </c:when>
                        <c:otherwise>
                            <button ${attrString}>${label}</button>
                        </c:otherwise>
                    </c:choose>
                </c:otherwise>
            </c:choose>

	</html:attributes>
</c:if>

<%-- fin du test d'évaluation du tag--%>
</c:if>