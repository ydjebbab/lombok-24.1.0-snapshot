<%@ tag dynamic-attributes="attributes" isELIgnored="false"
	pageEncoding="UTF-8" description="Insertion d'un formulaire"
	display-name="form"%>
<%@ include file="/META-INF/resources/WEB-INF/tags/html/includes.tagf" %>

<%--Attributs optionnels --%>
<%@ attribute name="action" required="false"
	description="Action utilisée lors de lors de la soumission du formulaire. Exemple action='zf2/flux.ex'"%>
<%@ attribute name="formobjectname" required="false"
	description="Valeur de la propriété 'formObjectName' déclarée dans le bean de type 'FormAction' utilisée pour binder ce formulaire "%>
<%@ attribute name="formid"
	description="Valeur de l'attribut id de l'élément html 'form'.Par défaut = formulaire"%>
<%@ attribute name="formrole"
    description="Valeur de l'attribut role de l'élément html 'form'.Par défaut = "%>
<%@ attribute name="method"
	description="Méthode HTTP utilisée pour soumettre le formulaire (POST | GET). Par défaut = POST"%>
<%@ attribute name="enctype"
	description="Mode d'encodage du formulaire. Par défaut = application/x-www-form-urlencoded"%>
<%@ attribute name="formreadonly"
	description="true : place les champs d'entrée en lecture seule. Par défaut = false"%>
<%@ attribute name="formboxid"
	description="Id du bloc div contenant le formulaire (qui contient les champs du formulaire). Par défaut = donnees"%>
<%@ attribute name="target"
	description="Nom de la frame mise à jour après soumission du formulaire"%>

<%@ attribute name="formboxclass"
	description="Classe(s) du bloc div qui contient le formulaire. Par défaut = donnees"%>
<%@ attribute name="onsubmit"
	description="Script exécuté après soumission du formulaire. Par défaut = return submit_form(500)"%>
<%@ attribute name="onreset"
	description="Script exécuté après initialisation (reset) du formulaire. Par défaut = "%>
<%@ attribute name="stylewait"
	description="Permet de positionner l'affichage du message d'attente.Par défaut ='top:40%'. Valeur de l'attribut 'style' positionné sur l'élément div contenant les messages d'attente."%>
<%@ attribute name="msgwait"
	description="Permet de personnaliser le message d'attente.Par défaut =Requête en cours de traitement. Patientez ..."%>
<%@ attribute name="formclass"
	description="Classe css appliquée à l'élément html 'form'. Par défaut = appForm"%>

<%@ attribute name="autocomplete"
	description="Fonctionnalité HTML5 qui permet d'indiquer au navigateur s'il doit préremplir les champs du formulaire. Par défaut en 'on' "%>


<%-- Variables exposées --%>
<%@ variable name-given="_formId" variable-class="java.lang.String"
	declare="true" scope="NESTED"
	description="Expose l'ID du formulaire qui sera utilisé par l'ensemble des tags inclus dans la balise form."%>

<%@ variable name-given="_formObjectName"
	variable-class="java.lang.String" declare="true" scope="NESTED"
	description="Expose le nom de l'objet support du formulaire qui sera utilisé par l'ensemble des tags inclus dans la balise form."%>

<%@ variable name-given="_formReadOnly"
	variable-class="java.lang.String" declare="true" scope="NESTED"
	description="Indique à l'ensemble des tags inclus dans la balise form si le formulaire est en lecture seule ou non."%>

<%@ attribute name="responsive" 
              required="false"
              description="indique si les elements de la page doivent etre responsive" %> 

<%@ attribute name="rwdformclass" 
              required="false"
              description="classes bootstrap du formulaire" %>                 
              
 <%@ attribute name="bootstrapmodel" 
              required="false"
              description="indique le model simplifié bootstrap" %>                  


<%-- Place dans le contexte 'request' la variable _formObjectName --%>
<c:if test="${! empty formobjectname}">
	<c:set var="_formObjectName" value="${formobjectname}" scope="request" />
</c:if>
<%-- Place dans le contexte 'request' la variable _formReadOnly --%>
<c:if test="${! empty formreadonly}">
	<c:set var="_formReadOnly" value="${formreadonly}" scope="request" />
</c:if>

<%--Affectation de valeurs par défaut aux attributs--%>
<app:initvar attrname="_action" attrvalue="${action}"
	bundlekey="app.form.action" defaultvalue="" />
<app:initvar attrname="_method" attrvalue="${method}"
	bundlekey="app.form.method" defaultvalue="POST" />
<app:initvar attrname="_enctype" attrvalue="${enctype}"
	bundlekey="app.form.enctype"
	defaultvalue="application/x-www-form-urlencoded" />
<app:initvar attrname="_formboxid" attrvalue="${formboxid}"
	bundlekey="app.form.formboxid" defaultvalue="donnees" />
<app:initvar attrname="_formrole" attrvalue="${formrole}"
    bundlekey="app.page.rwd.form.role" defaultvalue="" />    
<app:initvar attrname="_formboxclass" attrvalue="${formboxclass}"
	bundlekey="app.form.formboxclass" defaultvalue="donnees" />
<app:initvar attrname="_onsubmit" attrvalue="${onsubmit}"
	bundlekey="app.form.onsubmit" defaultvalue="return submit_form(500)" />
<app:initvar attrname="_onreset" attrvalue="${onreset}"
	bundlekey="app.form.onreset" defaultvalue="" />
<app:initvar attrname="_formid" attrvalue="${formid}"
	bundlekey="app.form.formid" defaultvalue="formulaire" />
<app:initvar attrname="_stylewait" attrvalue="${stylewait}"
	bundlekey="app.form.stylewait" defaultvalue="top:40%" />
<app:initvar attrname="_msgwait" attrvalue="${msgwait}"
	bundlekey="app.form.msgwait"
	defaultvalue="Requête en cours de traitement. Patientez ..." />
    
<app:initvar attrname="_formclass" attrvalue="${formclass}"
	bundlekey="app.form.formclass" defaultvalue="appForm" />
<app:initvar attrname="rwdformclass " attrvalue="${rwdformclass}"
    bundlekey="app.page.rwd.formclass" defaultvalue="form-horizontal" />      
<app:initvar attrname="_errortop" attrvalue="${errortop}"
	bundlekey="app.page.erreur.top" defaultvalue="true" />
<app:initvar attrname="_autocomplete" attrvalue="${autocomplete}"
    bundlekey="app.form.autocomplete" defaultvalue="on" />

<%@ include file="/META-INF/resources/WEB-INF/tags/app/responsive_test.tagf" %>
    

<%-- Appel du tag gérant l'affichage des erreurs --%>
<c:if test="${_errortop ne 'false'}">
	<c:if test="${! empty _formObjectName}">
		<app:disperrortop path="${_formObjectName}.*" />
	</c:if>
</c:if>
<%-- Place dans le contexte 'request' la variable _formID --%>
<c:if test="${! empty _formid}">
	<c:set var="_formId" value="${_formid}" scope="request" />
</c:if>

<%-- Place dans l'attribut 'attributes' les attributs dynamiques passés au tag --%>
<html:attributes var="attrString" attributeMap="${attributes}">
	<%-- Insère un bloc div qui contiendra le formulaire --%>
	<c:if test="${! empty _formboxid}">
		<div id="${_formboxid}" class="${_formboxclass}" ${attrString}>
	</c:if>

	<c:if test="${! empty target}">
		<c:set var="_target" value="target='${target}'" />
	</c:if>

  
    <c:choose>
       <c:when test="${_responsive eq 'false'}">
	<form class="${_formclass}" method="${_method}" enctype="${_enctype}"
		action="${pageContext.request.contextPath}/${_action}" id="${_formid}"
		onsubmit="${_onsubmit}" autocomplete="${_autocomplete}" ${_target}>
        </c:when>
        <c:otherwise>  
            <form role="${_formrole}" class="${rwdformclass}" method="${_method}" enctype="${_enctype}"
            action="${pageContext.request.contextPath}/${_action}" id="${_formid}"
            onsubmit="${_onsubmit}" autocomplete="${_autocomplete}" ${_target}>
        </c:otherwise>
    </c:choose>    
        
		<div>
			<%-- Positionne les champs cachés utilisés par le Spring WebFlow --%>
               <input type="hidden" name="${FLOWEXECUTIONID_FIELD_NAME}"
                value="${FLOWEXECUTIONID_VALUE}"></input>
			<%-- Champ caché utilisé pour faciliter l'exploitation des traces http--%>
			<c:choose>
				<c:when test="${! empty flowExecutionContext.activeSession.flow.id}">
					<c:set var="nomdufluxencours"
						value="${flowExecutionContext.activeSession.flow.id}" />
				</c:when>
				<c:otherwise>
					<c:set var="nomdufluxencours" value="aucun" />
				</c:otherwise>
			</c:choose>
			<input type="hidden" name="nomdufluxencours"
				value="${nomdufluxencours}"></input> <input type="hidden"
				name="transitiondemandee" value="aucun"></input>
		</div>
		<%-- Traite le contenu du Tag --%>
		<jsp:doBody />
           <security:csrfInput />
	</form>

	<%-- Fin du bloc div contenant le formulaire --%>
	<c:if test="${! empty _formboxid}">
		</div>
	</c:if>
  
	<%-- Appel du tag qui affiche un message d'attente--%>
	<app:dispwait var="${_msgwait}" style="${_stylewait}" />

	<%-- Appel du tag gérant l'affichage des erreurs --%>
	<c:if test="${_errortop ne 'true'}">
		<c:if test="${! empty _formObjectName}">
			<app:disperror path="${_formObjectName}.*" />
		</c:if>
	</c:if>

	<%-- Supprime du contexte 'request' la variable _formObjectName --%>
	<c:if test="${! empty formobjectname}">
		<c:remove var="_formObjectName" scope="request" />
	</c:if>

	<%-- Supprime du contexte 'request' la variable _formReadOnly --%>
	<c:if test="${! empty formreadonly}">
		<c:remove var="_formReadOnly" scope="request" />
	</c:if>

	<%-- Supprime du contexte 'request' la variable _formId --%>
	<c:if test="${! empty _formId}">
		<c:remove var="_formId" scope="request" />
	</c:if>
</html:attributes>



