<%@ tag dynamic-attributes="attributes"
        isELIgnored="false"
        pageEncoding="UTF-8"
        description="Insère une image stockee dans le contexte de flux"%>

<%@ include file="/META-INF/resources/WEB-INF/tags/html/includes.tagf" %>
<%--Attributs principaux --%>
<%@ attribute name="fichierjoint"
              description="Nom sous lequel l'objet de type FichierJoint (qui contient l'image) est stocké dans le contexte de flux"%>

<%@ attribute name="nomfichier"
              description="Nom sous lequel l'image est presentée a l'utilisateur lorsqu'il veut l'enregistrer"%>
              
<app:initvar attrname="_urlpattern" attrvalue="${urlpattern}" bundlekey="app.image.urlpattern" defaultvalue="fichierjoint"/>
<app:initvar attrname="_urlparametre" attrvalue="${urlparametre}" bundlekey="app.image.urlparametre" defaultvalue="fichierjoint"/>

<c:if test="${! empty fichierjoint}">
	<c:choose>
	<c:when test="${! empty nomfichier}">
		<c:set var="_nomFichierImage" value="${nomfichier}"/>
	</c:when>
	<c:otherwise>
		<c:set var="_nomFichierImage" value="image"/>
	</c:otherwise>
	</c:choose>
	<c:set var="src">
		<c:url value="/${_urlpattern}/${_nomFichierImage}?${FLOWEXECUTIONID_FIELD_NAME}=${FLOWEXECUTIONID_VALUE}&${_urlparametre}=${fichierjoint}"/>
	</c:set>
</c:if>

<html:attributes var="attrString" attributeMap="${attributes}" src='${src}'>
 	<img ${attrString}/>
</html:attributes>