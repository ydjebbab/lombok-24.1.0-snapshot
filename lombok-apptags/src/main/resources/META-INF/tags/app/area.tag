<%@ tag dynamic-attributes="attributes" 
        isELIgnored="false" 
        pageEncoding="UTF-8"
        description="Insère un lien contenant une image ou un label."%>
        
<%@ include file="/META-INF/resources/WEB-INF/tags/html/includes.tagf" %>
<%--Attributs principaux --%>
<%@ attribute name="action"
              description="Nom de l'action - URL qui permet d'acc&eacute;der au contr&ocirc;leur."%>
<%@ attribute name="transition"
              description="Nom de la transition d&eacute;finie dans le webflow.;"%>
<%@ attribute name="uri"
              description="Uri vers laquelle pointe le lien. Utilis&eacute; si les param&egrave;tres action et transition ne sont pas utilis&eacute;s."%>
<%@ attribute name="active"
              description="Attribut qui indique si le lien est actif."%>              
<%@ attribute name="attribute"
              description="Nom de l'attribut passé en paramètre"%>              
<%@ attribute name="value"
              description="Valeur de l'attribut passé en paramètre"%>                            
<%@ attribute name="aclass"
              description="Classe css de l'&eacute;l&eacute;ment"%>
<%@ attribute name="image"
              description="Nom de l'image qui sert pour mat&eacute;rialiser le lien."%>
<%@ attribute name="title"
              description="Titre de l'&eacute;l&eacute;ment href et de l'image"%>
<%@ attribute name="alt"
              description="Texte affich&eacute; si le navigateur n'affiche pas les images."%>              
<%@ attribute name="label"
              description="Libell&eacute; du lien "%>                            
<%@ attribute name="openwindow"
              description="true : ouvre une nouvelle fen&ecirc;tre du navigateur. Par d&eacute;faut : false "%>
<%@ attribute name="waitingmsg"
              description="true : affiche un message d'attente. Par d&eacute;faut : false "%>
<%@ attribute name="displayed"
              description="true: le tag est &eacute;valu&eacute;. Par d&eacute;faut : true"%>
<%@ attribute name="shape"
              description="forme de la zone : rect pour un rectangle, circle pour un disque, poly pour un polygone;. Par d&eacute;faut : poly"%>
<%@ attribute name="coords"
              description="coordonnees de la zone"%>



<%-- Affectation de valeurs par défaut aux attributs --%>
<app:initvar attrname="_displayed" attrvalue="${displayed}" bundlekey="app.area.displayed" defaultvalue="true"/>              
<app:initvar attrname="_imagesrepertoire" attrvalue="${imagesrepertoire}" bundlekey="app.page.imagesrepertoire" defaultvalue="/composants/tagsapp/images/"/>              
<app:initvar attrname="_aclass" attrvalue="${aclass}" bundlekey="app.area.aclass" defaultvalue="lien"/>
<app:initvar attrname="_openwindow" attrvalue="${openwindow}" bundlekey="app.area.openwindow" defaultvalue="false"/>
<app:initvar attrname="_active" attrvalue="${active}" bundlekey="app.area.active" defaultvalue="true"/>
<app:initvar attrname="_waitingmsg" attrvalue="${waitingmsg}" bundlekey="app.area.waitingmsg" defaultvalue="false"/>
<app:initvar attrname="_shape" attrvalue="${shape}" bundlekey="app.area.shape" defaultvalue="poly"/>
<app:initvar attrname="_coords" attrvalue="${coords}" bundlekey="app.area.coords" defaultvalue="0"/>

<%-- début du test d'évaluation du tag--%>
<c:if test="${_displayed == 'true' }">

<%-- correction pour éviter l'affichage de la valeur "null" sur Tomcat 6.0.20 et + --%>
<c:set var="_content" ><c:out value="${_content}" escapeXml="false" /></c:set>

<%-- Contenu du lien --%>
<c:if test="${!empty image}">
		<c:set var="_content"><img title="${title}" alt="${title}" src="<c:url value="${_imagesrepertoire}/${image}"/>" ${attrString}   alt="${tooltip}" style="border:0"/></c:set>
</c:if>

<c:if test="${!empty label}">
	<c:set var="_content" value="${_content}${label}"/>
</c:if>

<%-- Insère le lien --%>
<c:choose>
	<%-- le lien est actif --%>
	<c:when test="${_active == true}">
		<%-- L'indication du lien est transmise sous la forme action / transition du webflow--%>
		<c:choose>
		<c:when test="${! empty action && ! empty transition}">
			  	<c:set var="_href" value="${pageContext.request.contextPath}/${action}?${FLOWEXECUTIONID_FIELD_NAME}=${FLOWEXECUTIONID_VALUE}&amp;${EVENTID_FIELDURL_PREFIX}=${transition}"/>
			  	<%-- Un couple attribut / valeur complète l'URL--%>
			  	<c:if test="${! empty attribute}">
		  			<c:set var="_href" value="${_href}&amp;${attribute}=${value}"/>
		  		</c:if>
		</c:when>
		<%-- L'indication du lien est transmise sous la forme d'une action --%>
		<c:when test="${! empty action &&  empty transition}">
	  	<c:set var="_href" value="${pageContext.request.contextPath}/${action}"/>

	  	<%-- Un couple attribut / valeur complète l'URL--%>
		  	<c:if test="${! empty attribute}">
		  			<c:set var="_href" value="${_href}&amp;${attribute}=${value}"/>
	  		</c:if>
		</c:when>
		<%-- L'indication du lien est sous la forme d'une URL--%>
		<c:when test="${! empty uri}">
			<c:set var="_href" value="${uri}"/>
		</c:when>
		</c:choose>
		
        <!-- si le token csrf est disponible dans la page il sera ajouter à l'url construit car le filtre csrf vérifie désormais également
        les requetes en GET -->
        <c:if test="${! empty _csrf.parameterName}"><c:set var="_href" value="${_href}&${_csrf.parameterName}=${_csrf.token}"/></c:if>
               
   		<c:choose>
		    <c:when test="${_openwindow == true}">
			   <area shape="${shape}" coords="${coords}" title="${title}" href="javascript:send_url_openwindow('${_href}')" class="${_aclass}" alt="${title}">${_content}</area>
			</c:when>
			<c:when test="${_waitingmsg == true}">
				<area shape="${shape}" coords="${coords}" title="${title}" href="javascript:send_url_waiting('${_href}')" class="${_aclass}" alt="${title}">${_content}</area>
			</c:when>
			<c:otherwise>
				<area shape="${shape}" coords="${coords}" title="${title}" href="javascript:stocketransition('${transition}');send_url('${_href}')" class="${_aclass}" alt="${title}">${_content}</area>
			</c:otherwise>
		</c:choose>
	</c:when>	
	<%-- le lien est inactif --%>
	<c:otherwise>
		${_content}
	</c:otherwise>	
</c:choose>

<%-- fin du test d'évaluation du tag--%>
</c:if>