<%@ tag isELIgnored="false" pageEncoding="UTF-8"
         description="Insère la valeur de l'attribut."%>
         
<%@ include file="/META-INF/resources/WEB-INF/tags/html/includes.tagf" %>

<%@ attribute name="attribut" required="true" 
			description="Nom de la propriété à afficher. Si le tag est utilisé dans un tag 'form',
			la propriété est recherchée dans le formulaire, sinon la valeur de ce paramètre est utilisée
			comme 'path' pour effectuer le bind."%>
<%@ attribute name="ignorenullnestedpath"%>	
<%@ attribute name="displayed"
              description="true : le tag est évalué, false : il est ignoré. Par défaut : true."%>
              
<%--Affectation de valeurs par défaut aux attributs--%>
<app:initvar attrname="_displayed" attrvalue="${displayed}" bundlekey="app.text.displayed" defaultvalue="true"/>
<app:initvar attrname="_ignorenullnestedpath" attrvalue="${ignorenullnestedpath}" bundlekey="app.text.ignorenullnestedpath" defaultvalue="true"/>

<%-- début du test d'évaluation du tag--%>
<c:if test="${_displayed == 'true' }">

<%-- Utilisation de la variable _formObjectName exposée par le tag app:form --%>
<c:choose>
<c:when test="${!empty  _formObjectName}">
<c:set var="path" value="${_formObjectName}.${attribut}"/>
</c:when>
<c:otherwise>
<c:set var="path" value="${attribut}"/>
</c:otherwise>
</c:choose>

<%-- Insère la valeur du champ ou un blanc si le champ est vide --%>
<%@ include file="/META-INF/resources/WEB-INF/tags/html/fillstatus_value.tagf" %>
<c:choose>
	<c:when test="${! empty status_value}">
		<c:out value="${status_value}"/>
	</c:when>
	<c:otherwise>&nbsp;</c:otherwise>
</c:choose>

<%-- fin du test d'évaluation du tag--%>
</c:if>