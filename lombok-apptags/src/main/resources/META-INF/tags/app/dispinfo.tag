<%@ tag dynamic-attributes="attributes" 
        isELIgnored="false" 
        pageEncoding="UTF-8"
        description="Affiche une popup contenant un message d'information"%>
        
<%@ include file="/META-INF/resources/WEB-INF/tags/html/includes.tagf" %>

<%@ attribute name="path" 
              required="false"
              type="java.lang.Object"
              description="Chemin d'accès au bean ou à la propriété du bean dont la variable status sera exposée"%>
<%@ attribute name="var" 
              required="false"
              description="Nom d'un bean, contenant un message d'erreur à afficher, enregistré dans un des scopes accessibles à la JSP."%>
<%@ attribute name="transition" 
              required="false"
              description="Nom de la transition utilisée pour continuer en ignorant le message d'avertissement. Par défaut : continuer"%>
<%@ attribute name="libelletransition" 
              required="false"
              description="Libelle du bouton de la transition utilisée pour continuer en ignorant le message d'avertissement. Par défaut : Continuer"%>
<%@ attribute name="id" 
              required="false"
              description="id du popup. Par défaut = boite"%>
<%@ attribute name="idmsg" 
              required="false"
              description="id du bloc div qui contient les messages à afficher. Par défaut = popmsgid"%>

<%@ attribute name="imagesrepertoire" 
              description="Répertoire placé sous 'webapp' contenant les images utilisées par le tag. Par défaut = /composants/dhtmlpopup/img" %>
<%@ attribute name="titre" 
              description="Titre du popup. Par défaut = Avertissement" %>
<%@ attribute name="backgroundcolor" 
              description="couleur arrière-plan du popup. Par défaut = white" %>
<%@ attribute name="textcolor" 
              description="couleur du texte. Par défaut = black" %>
<%@ attribute name="textfont" 
              description="Police du texte. Par défaut = bold 10pt verdana" %>
<%@ attribute name="titrebackgroundcolor" 
              description="Police du texte. Par défaut = navy" %>              
<%@ attribute name="titrecolor" 
              description="Couleur du texte du titre. Par défaut = white" %>              
<%@ attribute name="bordercolor" 
              description="Couleur de la bordure. Par défaut = #dddddd" %>              
<%@ attribute name="scrollbarcolor" 
              description="Couleur de la barre de scroll. Par défaut = gray" %>              
<%@ attribute name="inputclass"
              description="Classe css des boutons input"%>              

<%@ attribute name="maxwidth" 
              description="Largeur maximale en pixel du popup. Par défaut = 500" %>              
<%@ attribute name="maxheight" 
              description="Hauteur maximale en pixel du popup. Par défaut = 300" %>  

<%@ attribute name="afficheboutons" 
              description="Affiche ou non les boutons permettant de fermer, minimiser et maximiser le popup. Par défaut = true" %>              
                            
<%-- Initialisation des paramètres par défaut --%>
<app:initvar attrname="_id" attrvalue="${id}" bundlekey="app.dispinfo.id" defaultvalue="boite"/>
<app:initvar attrname="_transition" attrvalue="${transition}" bundlekey="app.dispinfo.transition" defaultvalue="continuer"/>
<app:initvar attrname="_libelletransition" attrvalue="${libelletransition}" bundlekey="app.dispinfo.libelletransition" defaultvalue="Continuer"/>
<app:initvar attrname="_idmsg" attrvalue="${idmsg}" bundlekey="app.dispinfo.idmsg" defaultvalue="popmsgid"/>
<app:initvar attrname="_imagesrepertoire" attrvalue="${imagesrepertoire}" bundlekey="app.dispinfo.imagesrepertoire" defaultvalue="/composants/dhtmlpopup/img"/>
<app:initvar attrname="_titre" attrvalue="${titre}" bundlekey="app.dispinfo.titre" defaultvalue="Avertissement !"/>
<app:initvar attrname="_backgroundcolor" attrvalue="${backgroundcolor}" bundlekey="app.dispinfo.backgroundcolor" defaultvalue="white"/>
<app:initvar attrname="_textcolor" attrvalue="${textcolor}" bundlekey="app.dispinfo.textcolor" defaultvalue="black"/>
<app:initvar attrname="_textfont" attrvalue="${textfont}" bundlekey="app.dispinfo.textfont" defaultvalue="bold 10pt verdana"/>
<app:initvar attrname="_titrebackgroundcolor" attrvalue="${titrebackgroundcolor}" bundlekey="app.dispinfo.titrebackgroundcolor" defaultvalue="navy"/>
<app:initvar attrname="_titrecolor" attrvalue="${titrecolor}" bundlekey="app.dispinfo.titrecolor" defaultvalue="white"/>
<app:initvar attrname="_bordercolor" attrvalue="${bordercolor}" bundlekey="app.dispinfo.bordercolor" defaultvalue="#dddddd"/>
<app:initvar attrname="_scrollbarcolor" attrvalue="${scrollbarcolor}" bundlekey="app.dispinfo.scrollbarcolor" defaultvalue="gray"/>
<app:initvar attrname="_inputclass" attrvalue="${inputclass}" bundlekey="app.dispinfo.inputclass" defaultvalue="submit"/>
<app:initvar attrname="_maxwidth" attrvalue="${maxwidth}" bundlekey="app.dispinfo.maxwidth" defaultvalue="500"/>
<app:initvar attrname="_maxheight" attrvalue="${maxheight}" bundlekey="app.dispinfo.maxheight" defaultvalue="300"/>
<app:initvar attrname="_afficheboutons" attrvalue="${afficheboutons}" bundlekey="app.disperror.afficheboutons" defaultvalue="true"/>

<%-- Complete la transition avec le préfixe ${EVENTID_FIELD_PREFIX} si nécessaire --%>
<str:countMatches var="nb" substring="${EVENTID_FIELD_PREFIX}">${_transition}</str:countMatches>
<c:if test="${nb ne '1' }">
    <c:set var="_transition" value="${EVENTID_FIELD_PREFIX}${_transition}"/>
</c:if>

<%--Construit une variable contenant un fragment HTML à placer dans le message d'erreur. --%>
<c:if test="${! empty path}">
    <%-- Traitement des erreurs associées à des champs du formulaire --%>
    <c:if test="${path.errorCount != 0}">
        <c:forEach var="item" items="${path.allErrors}">
            <c:choose>
                <c:when test="${empty item.code}">
                    <c:set var="avertissement" value="${item.defaultMessage}"/>
                </c:when>
                <c:when test="${item.code eq 'null'}">
                    <c:set var="avertissement" value="${item.defaultMessage}"/>
                </c:when>               
                <c:otherwise>
                        <fmt:message key="${item.code}" var="avertissement">
                            <c:forEach var="argument" items="${item.arguments}">
                                <fmt:param value="${argument}"/>
                            </c:forEach>
                        </fmt:message>
                        <str:countMatches substring="???${item.code}???" var="exitInMessagesProperties">${avertissement}</str:countMatches>
                        <c:if test="${exitInMessagesProperties == 1}"><c:set var="avertissement" value="${item.defaultMessage}"/></c:if>    
                </c:otherwise>          
            </c:choose>
            <c:set var="listewarnings">
                <c:if test="${! empty listewarnings}">
                    <c:out value="${listewarnings}" escapeXml="false" />
                </c:if>
                <li><c:out value="${avertissement}" escapeXml="true" /></li>
            </c:set>
        </c:forEach>
    </c:if>
</c:if> 

<jsp:doBody var="contentTag"/>

<%--Affiche un message d'erreur si nécessaire --%>
<c:if test="${! empty listewarnings || ! empty var || ! empty contentTag || ! empty exceptionCatchedInTag}">

<div id="${_idmsg}" style="display:none">
    <ul>
    <c:if test="${! empty exceptionCatchedInTag}">${exceptionCatchedInTag}</c:if>
    <c:if test="${! empty listewarnings}">${listewarnings}</c:if>
    <c:if test="${! empty var}"><li>${var}</li></c:if>
    </ul>
    ${contentTag}
    <div id="boutonswarning" class="boxCompText" style="text-align: center"/>
        <button  id="boutonswarning_btn" value="Continuer" onclick="javascript:submitfromonglet('${_transition}')" class="${_inputclass}">${_libelletransition}</button>
    </div>
</div>

<script type="text/javascript">
/*<![CDATA[*/

<c:choose>
    <c:when test="${! empty _firsterrorid}">
        <%--Focus sur le premier champ en erreur --%>
        pumFocusErrorId = '${_firsterrorid}';
        <c:set var="_nearid" value="${_firsterrorid}"/>
    </c:when>
    <c:otherwise>
        <c:set var="_nearid" value="${_idmsg}"/>
    </c:otherwise>
</c:choose>
var _dynarch_PumPopup = new PumPopUp('${_nearid}', <%-- id de l'objet qui sert de reference pour positionner le popup --%>
                        '${_id}',                   <%-- id_de_la_popup --%>
                        '${_idmsg}',                 <%-- id de l'objet qui contient les messages d'erreur Ã  afficher --%>
                        '${_backgroundcolor}',      <%-- couleur arriere-plan popup --%>
                        '${_textcolor}',            <%-- couleur texte --%>
                        '${_textfont}',             <%-- police --%>
                        '${_titre}',                <%-- titre --%>
                        '${_titrebackgroundcolor}', <%-- couleur arriere-plan du titre --%>
                        '${_titrecolor}',           <%-- couleur titre --%>
                        '${_bordercolor}',          <%-- couleur bordure --%>
                        '${_scrollbarcolor}',       <%-- couleur barre de scroll --%> 
                        '<c:url value="${_imagesrepertoire}"/>', <%--  Repertoire de stockage des images --%>
                        30, <%-- Delta utilise pour augmenter la taille du popup--%>
                        '${_maxwidth}',  <%-- Largeur maximale du popup en pixel--%>
                        '${_maxheight}',  <%-- hauteur maximale du popup en pixel--%>      
                        ${_afficheboutons}  <%-- booléen permettant d'afficher ou non les boutons permettant de fermer, minimiser et maximiser le popup --%>);
                        
PumPopUp.addEvent(window,'load',PumPopUp.objectpopup);                          

/*]]>*/
</script>
</c:if>
