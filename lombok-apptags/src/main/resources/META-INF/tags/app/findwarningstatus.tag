<%@ tag dynamic-attributes="attributes" 
        isELIgnored="false" 
        pageEncoding="UTF-8"
        description="Retourne le status d'un champ"
       %>
<%@ include file="/META-INF/resources/WEB-INF/tags/html/includes.tagf" %>

<%@ attribute name="varname" 
              required="true" rtexprvalue="false" type="java.lang.String"
              description="Nom de la variable à créer." %>

<%@ attribute name="attributname" 
              required="false"
              description="Valeur à affecter à l'attribut"%>

<%@ attribute name="id" 
              required="false" rtexprvalue="true" 
              description="Id du champ de saisie"%>

<%-- Variables exposées --%>
<%@ variable name-from-attribute="varname" 
             alias="result"
             scope="AT_BEGIN" 
             description="Expose une variable à partir de l'attribut attrname" %>
             
<%-- Si le champ n'est associé à aucun attribut, on ne fait rien--%>
<c:if test="${! empty attributname}">

<%-- Utilisation de la variable _formObjectName exposée par le tag app:form --%>
<c:if test="${!empty  _formObjectName}">
<c:set var="_path" value="${_formObjectName}.${attributname}"/>
</c:if>

<%-- Remplit la variable status_warning si nécessaire --%>
<%--Construit une variable contenant un fragment HTML à placer dans le message d'erreur. --%>
<c:if test="${! empty warning}">
	<%-- Traitement des erreurs associées à des champs du formulaire --%>
	<c:if test="${warning.errorCount != 0}">
		<c:forEach var="item" items="${warning.allErrors}">
			<c:if test="${item.getClass().name == 'org.springframework.validation.FieldError'}">
			<c:choose>
				<c:when test="${item.field == attributname}">
					<c:set var="result" value="true"/>
				</c:when>
				<c:otherwise>
					<c:set var="result" value="false"/>
				</c:otherwise>
			</c:choose>
			</c:if>
		</c:forEach>
	</c:if>
</c:if>

<c:if test="${result}">
	<c:if test="${empty _firsterrorid}">
			<c:set var="_firsterrorid" value="${id}" scope="request"/> 
	</c:if>
	<c:set var="status_warning" value="${result}" scope="request"/>
</c:if>

</c:if>