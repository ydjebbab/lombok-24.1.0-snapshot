<%@ tag dynamic-attributes="attributes" 
        isELIgnored="false" 
        pageEncoding="UTF-8"
        description="Crée une variable valorisée (class) avec la valeur d'un model pour bootstrap ."
       %>
<%@ include file="/META-INF/resources/WEB-INF/tags/html/includes.tagf" %>

<%@ attribute name="bootstrapclass" 
              required="true" rtexprvalue="false" type="java.lang.String"
              description="Nom de la variable à créer." %>

<%@ attribute name="bootstrapmodel" 
              required="true"
              description="Valeur à affecter à l'attribut"%>
                         
<%-- Variables exposées --%>
<%@ variable name-from-attribute="bootstrapclass" 
             alias="result"
             scope="AT_BEGIN" 
             description="Expose une variable à partir de l'attribut attrname" %>              
 
              
  <%@ attribute name="bootstraptest" 
              required="false"
              description="test pour afficher les blocs bootstrap"%>   
              

<fmt:bundle basename="apptags">      
    <fmt:message key="app.page.rwd.test.redborder" var="_redborder"  />
</fmt:bundle>

<c:choose>
    <c:when test="${_redborder eq 'true'}" >
        <c:set var="bootstraptest" value=" bootstrap_test" />
    </c:when>
    <c:otherwise>
        <c:set var="bootstraptest" value="" />
    </c:otherwise>
</c:choose>
            
 <c:choose>
        <c:when test="${fn:contains(bootstrapmodel,'btmodel_1')}">   
            <c:set var="result" value=" ${bootstrapmodel}${bootstraptest} col-md-1 col-sm-2 col-xs-4" /> 
        </c:when>
        <c:when test="${fn:contains(bootstrapmodel,'btmodel_2')}">   
            <c:set var="result" value=" ${bootstrapmodel}${bootstraptest} col-md-2 col-sm-3 col-xs-4" /> 
        </c:when>
        <c:when test="${fn:contains(bootstrapmodel,'btmodel_inv_third')}">   
            <c:set var="result" value=" ${bootstrapmodel}${bootstraptest} col-md-4 col-sm-4 col-xs-4" /> 
        </c:when>
         <c:when test="${fn:contains(bootstrapmodel,'btmodel_inv_half')}">   
            <c:set var="result" value=" ${bootstrapmodel}${bootstraptest} col-md-6 col-sm-6 col-xs-6" /> 
        </c:when>
        <c:when test="${fn:contains(bootstrapmodel,'btmodel_inv_quarter')}">   
            <c:set var="result" value=" ${bootstrapmodel}${bootstraptest} col-md-3 col-sm-3 col-xs-3" /> 
        </c:when>
         <c:when test="${fn:contains(bootstrapmodel, 'btmodel_sixth')}">
            <c:set var="result" value=" ${bootstrapmodel}${bootstraptest} col-md-2 col-sm-3 col-xs-6" /> 
        </c:when> 
        
         <c:when test="${fn:contains(bootstrapmodel,'btmodel_quarter_to_half')}">
            <c:set var="result" value=" ${bootstrapmodel}${bootstraptest} col-md-3 col-sm-5 col-xs-6" /> 
        </c:when>
         <c:when test="${fn:contains(bootstrapmodel, 'btmodel_quarter')}">
            <c:set var="result" value=" ${bootstrapmodel}${bootstraptest} col-md-3 col-sm-5 col-xs-8" /> 
        </c:when> 
         <c:when test="${fn:contains(bootstrapmodel,'btmodel_third_sm_right')}">
            <c:set var="result" value=" ${bootstrapmodel}${bootstraptest} col-md-offset-0 col-md-4 col-sm-offset-6 col-sm-6 col-xs-12" /> 
        </c:when> 
         <c:when test="${fn:contains(bootstrapmodel,'btmodel_third')}">
            <c:set var="result" value=" ${bootstrapmodel}${bootstraptest} col-md-4 col-sm-6 col-xs-12" /> 
        </c:when> 
         <c:when test="${fn:contains(bootstrapmodel,'btmodel_half-center')}">
            <c:set var="result" value=" ${bootstrapmodel}${bootstraptest} col-md-offset-3 col-md-6 col-sm-offset-2 col-sm-8 col-xs-offset-0 col-xs-12" /> 
        </c:when>
         <c:when test="${fn:contains(bootstrapmodel, 'btmodel_half')}">
            <c:set var="result" value=" ${bootstrapmodel}${bootstraptest} col-md-6 col-sm-6 col-xs-12" /> 
        </c:when>   
        <c:otherwise>
             <c:set var="result" value=" ${bootstrapmodel}${bootstraptest}" /> 
        </c:otherwise>
 </c:choose>
