<%@ tag dynamic-attributes="attributes" isELIgnored="false"
	pageEncoding="UTF-8"
	description="Insère un élément de chemin dans ensemble de chemins"%>

<%@ include file="/META-INF/resources/WEB-INF/tags/html/includes.tagf" %>

<%--Attributs principaux --%>
<%@ attribute name="action"
	description="Action à utiliser si l'action pointée est différente de celle définie dans le tag app:chemins"%>
<%@ attribute name="label" description="Texte du chemin"%>
<%@ attribute name="uri"
	description="Uri vers laquelle le lien doit pointer si les paramètres actions et transitions ne sont pas utilisés."%>
<%@ attribute name="transition"
	description="Nom de la transition à utiliser par le lien"%>
<%@ attribute name="active"
	description="true: le lien est est actif. Par défaut : true"%>
<%@ attribute name="title"
	description="Titre de l'élément href et de l'image"%>
<%@ attribute name="image"
	description="Nom de l'image qui sert pour matérialiser le lien."%>
<%@ attribute name="imagesrepertoire"
	description="Répertoire de stockage de l'image. Par défaut : /composants/tagsapp/images/."%>
<%@ attribute name="attribute"
	description="Nom de l'attribut à passer en paramètre"%>
<%@ attribute name="value"
	description="Valeur de l'attribut à passer en paramètre"%>
<%@ attribute name="displayed"
	description="true : le tag est évalué, false : il est ignoré. Par défaut : true."%>
<%@ attribute name="responsive" description="gestion du parametre responsive."%>

<%-- Affectation de valeurs par défaut aux attributs --%>
<app:initvar attrname="_displayed" attrvalue="${displayed}"
	bundlekey="app.chemin.displayed" defaultvalue="true" />
<app:initvar attrname="_imagesrepertoire"
	attrvalue="${imagesrepertoire}" bundlekey="app.page.imagesrepertoire"
	defaultvalue="/composants/tagsapp/images/" />
<app:initvar attrname="_active" attrvalue="${active}"
	bundlekey="app.chemin.active" defaultvalue="true" />


<%@ include file="/META-INF/resources/WEB-INF/tags/app/responsive_test.tagf" %>

<%-- début du test d'évaluation du tag--%>
<c:if test="${_displayed == 'true' }">

	<%-- correction pour éviter l'affichage de la valeur "null" sur Tomcat 6.0.20 et + --%>
	<c:set var="_content">
		<c:out value="${_content}" escapeXml="false" />
	</c:set>

	<c:choose>
		<%-- 1er passe : on compte le nombre de chemins--%>
		<c:when test="${_actionDeComptage == 'true'}">
			<c:set var="_nbElementsDansChemin"
				value="${_nbElementsDansChemin +1}" scope="request" />
		</c:when>

		<%-- 2eme passe : on insère le chemin--%>
		<c:otherwise>
			<c:set var="_posElementDansChemin"
				value="${_posElementDansChemin +1}" scope="request" />

			<%-- Contenu du lien --%>
			<c:if test="${!empty image}">
				<c:set var="_content">
					<img title="${title}"
						src="<c:url value="${_imagesrepertoire}/${image}"/>" ${attrString}
						alt="${tooltip}" style="border: 0" />
				</c:set>
			</c:if>

			<c:if test="${!empty label}">
				<c:set var="_content" value="${_content}${label}" />
			</c:if>

			<jsp:doBody var="contenuTag" />

			<c:if test="${!empty contenuTag}">
				<c:set var="_content" value="${_content}${contenuTag}" />
			</c:if>

			<%-- Récupère dans le contexte 'request' la variable _cheminaction 
		 placée par le tag app:chemins
	--%>
			<c:choose>
				<c:when test="${! empty _cheminAction && empty action}">
					<c:set var="_action" value="${_cheminAction}" />
				</c:when>
				<c:when test="${! empty action}">
					<c:set var="_action" value="${action}" />
				</c:when>
			</c:choose>

			<c:if test="${! empty transition}">
				<c:set var="_transition" value="${transition}" />
			</c:if>


			<%-- affichage de l'icon "home" au lieu du texte "accueil" dans la barre de navigation --%>


            <c:if test="${_transition == 'annuler' || fn:startsWith(_action, 'accueil.ex') || _transition == 'fin' || _transition == 'back'}">
                <c:choose>
                    <c:when test="${(_responsive == 'true')}">
                        <c:set var="home_icon" value="" />
                        <c:set var="_content" value="${_content}" />
                    </c:when>
                    <c:otherwise>
				<c:set var="home_icon" value="<span class='home' aria-labelledby='homeIconLabel'></span>" />
				<c:set var="_content" value="<span class='homeOld' id='homeIconLabel' aria-label='Accueil'>${_content}</span>" />
                    </c:otherwise>
                </c:choose>
			</c:if>

			<%-- Si la transition n'est pas préfixée par ${EVENTID_FIELD_PREFIX}, on ajoute ce préfixe au début de la chaîne --%>
			<c:if test="${! empty _transition }">
				<str:countMatches var="nb" substring="${EVENTID_FIELD_PREFIX}">${_transition}</str:countMatches>
				<c:if test="${nb ne '1' }">
					<c:set var="_transition"
						value="${EVENTID_FIELD_PREFIX}${_transition}" />
				</c:if>


			</c:if>

			<%-- Insère un chemin --%>
             <c:if test="${(_responsive == 'true')}">
                <li>
             </c:if>
            
			<c:choose>
				<%-- Le lien est actif --%>
				<c:when
					test="${_active == true && (! empty action  || ! empty uri || ! empty _transition) && (_posElementDansChemin != _nbElementsDansChemin )}">
					<%-- L'indication du lien est transmise sous la forme action / transition du webflow--%>
					<c:choose>
						<c:when test="${! empty _action && ! empty transition}">
							<c:set var="_href"
								value="${pageContext.request.contextPath}/${_transition}/${_action}?${FLOWEXECUTIONID_FIELD_NAME}=${FLOWEXECUTIONID_VALUE}&amp;${EVENTID_FIELDURL_PREFIX}=${transition}" />
							<%-- Un couple attribut / valeur complète l'URL--%>
							<c:if test="${! empty attribute}">
								<c:set var="_href" value="${_href}&amp;${attribute}=${value}" />
							</c:if>
						</c:when>
						<%-- L'indication du lien est transmise sous la forme d'une action --%>
						<c:when test="${! empty action &&  empty transition}">
							<c:set var="_href"
								value="${pageContext.request.contextPath}/${_transition}/${action}" />
							<%-- Un couple attribut / valeur complète l'URL--%>
							<c:if test="${! empty attribute}">
								<c:set var="_href" value="${_href}&amp;${attribute}=${value}" />
							</c:if>
						</c:when>
						<%-- L'indication du lien est sous la forme d'une URL--%>
						<c:when test="${! empty uri}">
							<c:set var="_href" value="${uri}" />
						</c:when>
					</c:choose>
					<a title="${title}"
						href="javascript:doit('${pageContext.request.contextPath}/${_action}','${_transition}')">${home_icon}${_content}</a>
						
					<c:if test="${(_posElementDansChemin != 0) }">
						<span class="chevron">></span>
					</c:if>
				</c:when>
				<%-- Le lien n'est pas actif--%>
				<c:otherwise>
					<c:choose>
						<c:when test="${_posElementDansChemin == _nbElementsDansChemin }">
							<span class="last">${_content}</span>
						</c:when>
						<c:otherwise>
							<span>${_content}</span>
							<c:if test="${(_posElementDansChemin != 0) }">
								<span class="chevron">></span>
							</c:if>
						</c:otherwise>
					</c:choose>
				</c:otherwise>
			</c:choose>

                <c:if test="${(_responsive == 'true')}">
                    </li>
                </c:if> 

		</c:otherwise>
	</c:choose>

	<%-- fin du test d'évaluation du tag--%>
</c:if>
