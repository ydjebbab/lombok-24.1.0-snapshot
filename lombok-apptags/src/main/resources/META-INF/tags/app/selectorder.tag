<%@ tag dynamic-attributes="attributes" 
        isELIgnored="false" 
        pageEncoding="UTF-8"
        description="Insère un élément select"
        %>
<%@ include file="/META-INF/resources/WEB-INF/tags/html/includes.tagf" %>

<%--Attributs principaux --%>
<%@ attribute name="attribut" 
              required="true" 
              description="Nom de l'attribut associé au champ de saisie  dans l'objet support du formulaire. Utilisé dans le spring bind" %>
<%@ attribute name="readonly"
              description="true : champs en lecture seule"%>
<%@ attribute name="disabled"
              description="true : champs inactif"%>       
<%@ attribute name="disabledwhenreadonly"
              description="true : quand un champ est en readonly, il est aussi disabled"%>                            
<%@ attribute name="libelle" 
              required="false" 
              description="Label associé à la liste déroulante (select)"%>
<%@ attribute name="itemslist" 
              type="java.lang.Object" 
              description="Bean de type List contenant les éléments de la liste déroulante."%>
<%@ attribute name="label"
              description="Nom de la propriété d'un élément de la collection (items...) affichée dans la liste déroulante."%>
<%@ attribute name="value"
              description="Nom de la propriété d'un élément de la collection (items...) utilisé comme valeur de l'option."%>
<%@ attribute name="ignorenullnestedpath" 
              description="true : Ignore les références nulles lors de l'accès à un attribut.  Valeur par défaut = true "%>
<%@ attribute name="displayed"
              description="true: le tag est évalué. Par défaut : true"%>
<%@ attribute name="affichelibelle"
              description="Affichage du libellé associé à la liste déroulante. Par défaut : true"%>
              
<%--Attributs optionnels --%>
<%@ attribute name="requis" 
              description="true : champs obligatoire.  Valeur par défaut = true "%>
<%@ attribute name="consigne" 
              description="Correspond à l'attribut 'title' de l'élément label. Peut contenir, par exemple, une consigne de saisie du champs. "%>
<%@ attribute name="size" 
              description="Nombre d'éléments de la liste à afficher simultanément. "%>

<%--Attributs de présentation --%>
<%@ attribute name="theme"
              description="Thème de présentation. Par défaut = V"%>
<%@ attribute name="boxheight"
              description="Hauteur du bloc div contenant l'ensemble des éléments relatifs à un champ de saisie."%>
<%@ attribute name="boxwidth"
              description="Largeur du bloc div contenant l'ensemble des éléments relatifs à un champ de saisie."%>
<%@ attribute name="labelboxwidth"
              description="Largeur du bloc div contenant le libelle (en % de la largeur totale du bloc parent)"%>
<%@ attribute name="inputboxwidth"
              description="Largeur du bloc div contenant le champ de saisie (en % de la largeur totale du bloc parent)"%>
<%@ attribute name="compboxwidth"
              description="Largeur du bloc div contenant la zone complémentaire (en % de la largeur totale du bloc parent)"%>
              
<%@ attribute name="normalboxclass"
              description="Classe css du bloc div contenant l'ensemble des éléments relatifs à la liste déroulante. Valeur par défaut = normal "%>
<%@ attribute name="lectureboxclass"
              description="Classe css du bloc div contenant l'ensemble des éléments relatifs à la liste déroulante en lecture seule. Valeur par défaut = lecture."%>
<%@ attribute name="erreurboxclass"
              description="Classe css du bloc div contenant l'ensemble des éléments relatifs à la liste déroulante, lorsque le champs est en erreur. Valeur par défaut = erreur."%>
<%@ attribute name="requisboxclass"
              description="Classe css du bloc div contenant l'ensemble des éléments relatifs à la liste déroulante, lorsque le champs de saisie est obligatoire. Valeur par défaut = requis."%>


<%@ attribute name="labelclass"
              description="Classe css de l'élément label. Valeur par défaut = labelText"%>            
<%@ attribute name="labelboxclass"
              description="Classe css du bloc div contenant le libellé.Valeur par défaut = boxlblText."%>
<%@ attribute name="inputboxclass"
              description="Classe css du bloc div contenant le champs de saisie.Valeur par défaut = boxText."%>
<%@ attribute name="compboxclass"
              description="Classe css du bloc div contenant des données complémentaires sur le champs de saisie.Valeur par défaut = boxCompText."%>

<%-- Attributs  --%>
<%@ attribute name="tabindex"
              description="Numéro indiquant l'ordre d'accès par tabulation."%>
<%@ attribute name="accesskey"
              description="Touche de raccourci pour accéder à un champs."%>
<%@ attribute name="onfocus"
              description="Script exécuté lors que l'élément est accédé."%>
<%@ attribute name="onblur"
              description="Script exécuté lors que l'élément est quitté."%>
<%@ attribute name="onchange"
              description="Script exécuté lors que l'élément est quitté et que son contenu a été modifié."%>
              
<%-- Attributs événements intrinsèques HTML --%>
<%@ attribute name="onclick"
              description="Script exécuté quand le bouton du dispositif de pointage est cliqué au-dessus d'un élément."%>
<%@ attribute name="ondblclick"
              description="Script exécuté quand le bouton du dispositif de pointage est double-cliqué au-dessus d'un élément."%>              
<%@ attribute name="onmousedown"
              description="Script exécuté quand le bouton du dispositif de pointage est appuyé au-dessus d'un élément."%>              
<%@ attribute name="onmouseup"
              description="Script exécuté quand le bouton du dispositif de pointage est relaché au-dessus d'un élément."%>                            
<%@ attribute name="onmouseover"
              description="Script exécuté quand le bouton du dispositif de pointage est déplacé sur un élément."%>                                          
<%@ attribute name="onmousemove"
              description="Script exécuté quand le bouton du dispositif de pointage est déplacé alors qu'il est au-dessus d'un élément."%>
<%@ attribute name="onmouseout"
              description="Script exécuté quand le bouton du dispositif de pointage est déplacé en dehors d'un élément."%>                                                                      
<%@ attribute name="onkeypress"
              description="Script exécuté quand une touche est pressée puis relachée au-dessus d'un élément."%>
<%@ attribute name="onkeydown"
              description="Script exécuté quand une touche est gardée appuyée au-dessus d'un élément."%>              
<%@ attribute name="onkeyup"
              description="Script exécuté quand une touche est relachée au-dessus d'un élément."%>                            
              
<%@ attribute name="id"
              description="id de l'élément input. Par défaut : attribué automatiquement."%>
              
<%--Affectation de valeurs par défaut aux attributs--%>
<app:initvar attrname="_imagesrepertoire" attrvalue="${imagesrepertoire}" bundlekey="app.page.imagesrepertoire" defaultvalue="/composants/tagsapp/images"/>
<app:initvar attrname="_imagemonter" attrvalue="${imagemonter}" bundlekey="app.selectorder.imagemonter" defaultvalue="moveup.gif"/>
<app:initvar attrname="_imagedescendre" attrvalue="${imagedescendre}" bundlekey="app.selectorder.imagedescendre" defaultvalue="movedown.gif"/>
<app:initvar attrname="_imagetop" attrvalue="${imagetop}" bundlekey="app.selectorder.imagetop" defaultvalue="top.gif"/>
<app:initvar attrname="_imagebottom" attrvalue="${imagebottom}" bundlekey="app.selectorder.imagebottom" defaultvalue="bottom.gif"/>
<app:initvar attrname="_imagemonterinactive" attrvalue="${imagemonterinactive}" bundlekey="app.selectorder.imagemonterinactive" defaultvalue="moveupdisabled.gif"/>
<app:initvar attrname="_imagedescendreinactive" attrvalue="${imagedescendreinactive}" bundlekey="app.selectorder.imagedescendreinactive" defaultvalue="movedowndisabled.gif"/>
<app:initvar attrname="_imagetopinactive" attrvalue="${imagetopinactive}" bundlekey="app.selectorder.imagetopinactive" defaultvalue="topdisabled.gif"/>
<app:initvar attrname="_imagebottominactive" attrvalue="${imagebottominactive}" bundlekey="app.selectorder.imagebottominactive" defaultvalue="bottomdisabled.gif"/>
<app:initvar attrname="_imageclass" attrvalue="${imageclass}" bundlekey="app.selectorer.imageclass" defaultvalue="aide"/>

<app:initvar attrname="_displayed" attrvalue="${displayed}" bundlekey="app.selectorder.displayed" defaultvalue="true"/>
<app:initvar attrname="_ignorenullnestedpath" attrvalue="${ignorenullnestedpath}" bundlekey="app.selectorder.ignorenullnestedpath" defaultvalue="true"/>
<app:initvar attrname="_readonly" attrvalue="${readonly}" bundlekey="app.selectorder.readonly" defaultvalue="false"/>
<app:initvar attrname="_disabled" attrvalue="${disabled}" bundlekey="app.selectorder.disabled" defaultvalue=""/>
<app:initvar attrname="_disabledwhenreadonly" attrvalue="${disabledwhenreadonly}" bundlekey="app.selectorder.disabledwhenreadonly" defaultvalue="true"/>
<app:initvar attrname="_labelclass" attrvalue="${labelclass}" bundlekey="app.selectorder.labelclass" defaultvalue="labelText"/>
<app:initvar attrname="_requis" attrvalue="${requis}" bundlekey="app.selectorder.requis" defaultvalue="false"/>
<app:initvar attrname="_onkeypress" attrvalue="${onkeypress}" bundlekey="app.selectorder.onkeypress" defaultvalue=""/>
<app:initvar attrname="_affichelibelle" attrvalue="${affichelibelle}" bundlekey="app.selectorder.affichelibelle" defaultvalue="true"/>

<app:initvar attrname="_requisindicateur" attrvalue="${requisindicateur}" bundlekey="app.selectorder.requisindicateur" defaultvalue=" *"/>
<app:initvar attrname="_normalindicateur" attrvalue="${normalindicateur}" bundlekey="app.selectorder.normalindicateur" defaultvalue=""/>

<app:initvar attrname="_theme" attrvalue="${theme}" bundlekey="app.selectorder.theme" defaultvalue="V"/>
<app:initvar attrname="_boxheight" attrvalue="${boxheight}" bundlekey="app.selectorder.boxheight" defaultvalue="20px"/>
<app:initvar attrname="_boxwidth" attrvalue="${boxwidth}" bundlekey="app.selectorder.boxwidth" defaultvalue="100%"/>
<app:initvar attrname="_labelboxwidth" attrvalue="${labelboxwidth}" bundlekey="app.selectorder.labelboxwidth" defaultvalue="35%"/>
<app:initvar attrname="_inputboxwidth" attrvalue="${inputboxwidth}" bundlekey="app.selectorder.inputboxwidth" defaultvalue="60%"/>
<app:initvar attrname="_compboxwidth" attrvalue="${compboxwidth}" bundlekey="app.selectorder.compboxwidth" defaultvalue="5%"/>

<app:initvar attrname="_normalboxclass" attrvalue="${normalboxclass}" bundlekey="app.selectorder.normalboxclass" defaultvalue="normal"/>
<app:initvar attrname="_lectureboxclass" attrvalue="${lectureboxclass}" bundlekey="app.selectorder.lectureboxclass" defaultvalue="lecture"/>
<app:initvar attrname="_erreurboxclass" attrvalue="${erreurboxclass}" bundlekey="app.selectorder.erreurboxclass" defaultvalue="erreur"/>
<app:initvar attrname="_requisboxclass" attrvalue="${requisboxclass}" bundlekey="app.selectorder.requisboxclass" defaultvalue="requis"/>
<app:initvar attrname="_labelboxclass" attrvalue="${labelboxclass}" bundlekey="app.selectorder.labelboxclass" defaultvalue="boxlblText"/>
<app:initvar attrname="_inputboxclass" attrvalue="${inputboxclass}" bundlekey="app.selectorder.inputboxclass" defaultvalue="boxText"/>
<app:initvar attrname="_compboxclass" attrvalue="${compboxclass}" bundlekey="app.selectorder.compboxclass" defaultvalue="boxCompText"/>


<%-- début du test d'évaluation du tag--%>
<c:if test="${_displayed == 'true' }">

<%-- Utilisation de la variable _formObjectName exposée par le tag app:form --%>
<c:if test="${!empty  _formObjectName}">
<c:set var="_path" value="${_formObjectName}.${attribut}"/>
</c:if>

<%-- Utilisation de la variable _formReadOnly exposée par le tag app:form --%>
<c:if test="${!empty  _formReadOnly}">
<c:set var="_readonly" value="true"/>
</c:if>

<app:id attrname="_idselect"></app:id>

<c:if test="${empty libelle}">
	<fmt:message key="${attribut}" var="libelle" />
	<str:countMatches substring="???${attribut}???" var="exitInMessagesProperties">${libelle}</str:countMatches>
	<c:if test="${exitInMessagesProperties == 1}"><c:set var="libelle" value=""/></c:if>
</c:if>

<%-- Sélection de la classe du bloc div --%>
<c:choose>
	<c:when test="${status_error || status_warning}">
		<c:set var="_divclass" value="${_theme}${_erreurboxclass}"/>
		<c:if test="${empty _firsterrorid}">
			<c:set var="_firsterrorid" value="${_id}" scope="request"/> 
		</c:if>
	</c:when>
	<c:when test="${_readonly eq 'true'}">
		<c:set var="_divclass" value="${_theme}${_lectureboxclass}"/>
	</c:when>
	<c:when test="${_requis eq 'true'}">
		<c:set var="_divclass" value="${_theme}${_requisboxclass}"/>
	</c:when>
	<c:otherwise>
		<c:set var="_divclass" value="${_theme}${_normalboxclass}"/>
	</c:otherwise>
</c:choose>

<%-- Ajout d'un indicateur pour les champs obligatoires --%>
<c:choose>
	<c:when test="${_requis eq 'true'}">
		<c:set var="libelle" value="${libelle}${_requisindicateur}"/>
	</c:when>
	<c:otherwise>
		<c:set var="libelle" value="${libelle}${_normalindicateur}"/>
	</c:otherwise>
</c:choose>

<%-- Utilisation du hack "_" pour contourner l'absence de propriété min-height sous IE --%>
<div class="${_divclass}" style="width: ${_boxwidth};height: auto;_height: ${_boxheight};min-height: ${_boxheight};">
    <%-- Insère le label associé à  la liste déroulante --%>
    <c:if test="${_affichelibelle eq 'true'}">
	    <div class="${_labelboxclass}" style="width: ${_labelboxwidth};">
		    <c:if test="${! empty accesskey}">
			    <c:set var="_complabel" value="accesskey=\"${accesskey}\""/>
		    </c:if>
		    <c:if test="${! empty consigne}">
			    <c:set var="_complabel" value="${_complabel} title=\"${consigne}\""/>
		    </c:if>
		    <label class="${_labelclass}" for="${_idselect}" ${_complabel}>${libelle}</label>
	    </div>
    </c:if>
    
    <%-- Insère la liste déroulante--%>
    <div class="${_inputboxclass}" style="width: ${_inputboxwidth};">
    
    <%-- Insertion du champ caché qui contient la liste des éléments "values" séparées par un ';'--%>
    <%-- Détermination de l'ID du champ --%>
	<app:id attrname="_id" id="${id}"></app:id>
    
    <html:input path="${_path}" type="hidden" readOnly="${_readonly}" disabledwhenreadonly="${_disabledwhenreadonly}">
    	<jsp:attribute name="id">${_id}</jsp:attribute>
    </html:input>
    
    <table style="margin:0;padding:0;">
    <tr>
    <td  style="vertical-align:top;">

     <html:selectsansbind>
    	<jsp:attribute name="id">${_idselect}</jsp:attribute>
    	<jsp:attribute name="readOnly">${_readonly}</jsp:attribute>
    	<jsp:attribute name="disabled">${_disabled}</jsp:attribute>
    	<jsp:attribute name="disabledwhenreadonly">${_disabledwhenreadonly}</jsp:attribute>    	  	    	
    	<jsp:attribute name="multiple">multiple</jsp:attribute>
    	<jsp:attribute name="size">${size}</jsp:attribute>
    	<jsp:attribute name="tabindex">${tabindex}</jsp:attribute>
    	<jsp:attribute name="onfocus">${onfocus}</jsp:attribute>
    	<jsp:attribute name="onblur">${onblur}</jsp:attribute>
    	<jsp:attribute name="onchange">${onchange}</jsp:attribute>
    	<jsp:attribute name="onkeypress">${_onkeypress}</jsp:attribute> 
		<jsp:attribute name="onclick">${onclick}</jsp:attribute> 
		<jsp:attribute name="ondblclick">${ondblclick}</jsp:attribute> 	   			
		<jsp:attribute name="onmousedown">${onmousedown}</jsp:attribute>
		<jsp:attribute name="onmouseup">${onmouseup}</jsp:attribute>
		<jsp:attribute name="onmouseover">${onmouseover}</jsp:attribute>
		<jsp:attribute name="onmousemove">${onmousemove}</jsp:attribute>
		<jsp:attribute name="onmouseout">${onmouseout}</jsp:attribute>
		<jsp:attribute name="onkeydown">${onkeydown}</jsp:attribute>
		<jsp:attribute name="onkeyup">${onkeyup}</jsp:attribute>     	
    	<jsp:body>

		<%-- Les items de la liste déroulante sont stockés dans une collection de type List --%>
		<c:if test="${!empty itemslist}">
			<html:options items="${itemslist}">
 		    	<jsp:attribute name="label">${label}</jsp:attribute>
  		    	<jsp:attribute name="value">${value}</jsp:attribute>
			</html:options>
		</c:if>		
		</jsp:body>
    </html:selectsansbind>
    
   	</td>
   	<c:choose>
	<c:when test="${(_readonly != 'true') and (_disabled != 'true')}">
		<c:set var="_movetop">onclick="movetop('${_idselect}','${_id}');return false;"</c:set>
		<c:set var="_moveup">onclick="moveup('${_idselect}','${_id}');return false;"</c:set>
		<c:set var="_movedown">onclick="movedown('${_idselect}','${_id}');return false;"</c:set>
		<c:set var="_movebottom">onclick="movebottom('${_idselect}','${_id}');return false;"</c:set>
		
		<c:set var="_srcimagetop">"<c:url value="${_imagesrepertoire}/${_imagetop}"/>"</c:set>
		<c:set var="_srcimagebottom">"<c:url value="${_imagesrepertoire}/${_imagebottom}"/>"</c:set>
		<c:set var="_srcimagedescendre">"<c:url value="${_imagesrepertoire}/${_imagedescendre}"/>"</c:set>		
		<c:set var="_srcimagemonter">"<c:url value="${_imagesrepertoire}/${_imagemonter}"/>"</c:set>				
	</c:when>
	<c:otherwise>
		<c:set var="_srcimagetop">"<c:url value="${_imagesrepertoire}/${_imagetopinactive}"/>"</c:set>
		<c:set var="_srcimagebottom">"<c:url value="${_imagesrepertoire}/${_imagebottominactive}"/>"</c:set>
		<c:set var="_srcimagedescendre">"<c:url value="${_imagesrepertoire}/${_imagedescendreinactive}"/>"</c:set>		
		<c:set var="_srcimagemonter">"<c:url value="${_imagesrepertoire}/${_imagemonterinactive}"/>"</c:set>				
	</c:otherwise>
	</c:choose>
	<td>
	<table>
	<tr>
	<td style="margin:0;padding:0;border-top:solid;border-top-width:1px;border-top-style:dotted;border-left:solid;border-left-width:1px;border-left-style:dotted;border-right:solid;border-right-width:1px;border-right-style:dotted;text-align:center;">	
     		<a href="#" ${_movetop}>
				<img style="border:none;padding:0;margin:0;" src=${_srcimagetop} alt="Placer au début de la liste" title="Placer au début de la liste" />    
			</a>
		 </td>
	</tr>	     
     <tr>
     	<td style="margin:0;padding:0;border-left:solid;border-left-width:1px;border-left-style:dotted;border-right:solid;border-right-width:1px;border-right-style:dotted;text-align:center;">	
          <a href="#" ${_moveup}>
   			<img style="border:none;padding:0;marging:0;" src=${_srcimagemonter} alt="Monter" title="Monter"/>
		  </a>
		 </td>
	</tr>
     <tr>
     	<td style="margin:0;padding:0;border-left:solid;border-left-width:1px;border-left-style:dotted;border-right:solid;border-right-width:1px;border-right-style:dotted;text-align:center;">	
     <a href="#" ${_movedown}>
			<img style="border:none;padding:0;margin:0;" src=${_srcimagedescendre} alt="Descendre" title="Descendre" />    
	</a> 
		 </td>
	</tr>	
     <tr>
	<td style="margin:0;padding:0;border-bottom:solid;border-bottom-width:1px;border-bottom-style:dotted;border-left:solid;border-left-width:1px;border-left-style:dotted;border-right:solid;border-right-width:1px;border-right-style:dotted;text-align:center;">	
     <a href="#" ${_movebottom}>
			<img style="border:none;padding:0;margin:0;" src=${_srcimagebottom} alt="Placer à la fin de la liste" title="Placer à la fin de la liste" />    
	</a>	
	</td>
	</tr>
	</table>
	</td>
	</tr>
	</table>
    
    </div>
    <%-- Insère les éléments présents dans le corps du tag --%>
    <%-- Lecture du corps du Tag--%>
 	<jsp:doBody var="resbody"/>
	<c:if test="${! empty resbody}">
			        <div class="${_compboxclass}" style="width: ${_compboxwidth};height: auto;_height: ${_boxheight};min-height: ${_boxheight};">${resbody}</div>
	</c:if>			        
		
</div>

<%-- fin du test d'évaluation du tag--%>
</c:if>
