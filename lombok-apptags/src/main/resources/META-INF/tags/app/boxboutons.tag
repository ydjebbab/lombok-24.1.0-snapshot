<%@ tag dynamic-attributes="attributes" 
        isELIgnored="false" 
        pageEncoding="UTF-8"
        description="Insère un bloc div contenant une barre de boutons. Accepte comme paramètres dynamiques tous les attributs de l'élément html div."%>
        
<%@ include file="/META-INF/resources/WEB-INF/tags/html/includes.tagf" %>

<%--Attributs principaux --%>
<%@ attribute name="divboxboutonsclass"
              description="Classe css du bloc div. Valeur par défaut : boxboutons."%>
<%@ attribute name="displayed"
              description="true : le tag est évalué, false : il est ignoré. Par défaut : true."%>              

<%-- Affectation de valeurs par défaut aux attributs --%>
<app:initvar attrname="_displayed" attrvalue="${displayed}" bundlekey="app.boxboutons.displayed" defaultvalue="true"/>            
<app:initvar attrname="_divboxboutonsclass" attrvalue="${divboxboutonsclass}" bundlekey="app.boxboutons.divboxboutonsclass" defaultvalue="boxboutons"/>

<%-- début du test d'évaluation du tag--%>
<c:if test="${_displayed == 'true' }">

<html:attributes var="attrString" attributeMap="${attributes}">
	<div class="${_divboxboutonsclass}" ${attrString}>
		<jsp:doBody/>
	</div>
</html:attributes>

<%-- fin du test d'évaluation du tag--%>
</c:if>