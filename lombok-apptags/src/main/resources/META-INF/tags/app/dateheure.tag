<%@ tag dynamic-attributes="attributes" 
        isELIgnored="false" 
        pageEncoding="utf-8"
        description="Saisie de la date et de l'heure à partir du calendrier."%>
        
<%@ include file="/META-INF/resources/WEB-INF/tags/html/includes.tagf" %>

<%--Attributs principaux --%>
<%@ attribute name="attribut" 
              required="true" 
              description="Nom de l'attribut associé au champs de saisie (input) dans l'objet support du formulaire. Utilisé dans le spring bind" %>
<%@ attribute name="libelle" 
              required="false" 
              description="Label associé au champs de saisie (input)"%>
<%@ attribute name="ignorenullnestedpath" 
              description="true : Ignore les références nulles lors de l'accès à un attribut. Valeur par défaut = true "%>
<%@ attribute name="displayed"
              description="true: le tag est évalué. Par défaut : true"%>
              
<%--Attributs de présentation --%>
<%@ attribute name="theme"
              description="Th&egrave;me de présentation. Par défaut = V"%>
<%@ attribute name="boxheight"
              description="Hauteur du bloc div contenant l'ensemble des éléments relatifs à un champs de saisie."%>
<%@ attribute name="boxwidth"
              description="Largeur du bloc div contenant l'ensemble des éléments relatifs à un champs de saisie."%>
<%@ attribute name="labelboxwidth"
              description="Largeur du bloc div contenant le libelle (en % de la largeur totale du bloc parent)"%>
<%@ attribute name="inputboxwidth"
              description="Largeur du bloc div contenant le champ de saisie (en % de la largeur totale du bloc parent)"%>
<%@ attribute name="compboxwidth"
              description="Largeur du bloc div contenant la zone complémentaire (en % de la largeur totale du bloc parent)"%>

<%@ attribute name="normalboxclass"
              description="Classe css du bloc div contenant l'ensemble des éléments relatifs à un champs de saisie. Valeur par défaut = normal "%>
<%@ attribute name="lectureboxclass"
              description="Classe css du bloc div contenant l'ensemble des éléments relatifs à un champs de saisie, en lecture seule. Valeur par défaut = lectureseule."%>
<%@ attribute name="erreurboxclass"
              description="Classe css du bloc div contenant l'ensemble des éléments relatifs à un champs de saisie en erreur. Valeur par défaut = erreur."%>
<%@ attribute name="labelboxclass"
              description="Classe css du bloc div contenant le libellé.Valeur par défaut = boxlblText."%>
<%@ attribute name="inputboxclass"
              description="Classe css du bloc div contenant le champs de saisie.Valeur par défaut = boxText."%>
<%@ attribute name="compboxclass"
              description="Classe css du bloc div contenant des données complémentaires sur le champs de saisie.Valeur par défaut = boxCompText."%>
<%@ attribute name="inputclass"
              description="Classe css du champs input. Valeur par défaut = inputText"%>            
<%@ attribute name="labelclass"
              description="Classe css de l'élément label. Valeur par défaut = labelText"%>            
<%@ attribute name="normalindicateur"
              description="Chaîne de caractés indiquant qu'un message est optionnel. Valeur par défaut = Chaîne vide"%>                          
         
<%--Attributs optionnels --%>
<%@ attribute name="consigne" 
              description="Correspond à l'attribut 'title' de l'élément label. Peut contenir, par exemple, une consigne de saisie du champs. "%>
<%@ attribute name="onkeypress"
              description="Code javascript à exécuter lorsqu'on appuie sur la touche 'ENTER' dans ce champs.Valeur par défaut = 'return submitEnterButton(event)'"%>

<%@ attribute name="dateouverture"
              description="Date affichée à l'ouverture du calendrier."%>              

<%-- Attributs  --%>
<%@ attribute name="tabindex"
              description="Numéro indiquant l'ordre d'accès par tabulation."%>
<%@ attribute name="accesskey"
              description="Touche de raccourci pour accéder à un champs."%>
<%@ attribute name="onfocus"
              description="Script exécuté lors que l'élément est accédé."%>
<%@ attribute name="onblur"
              description="Script exécuté lors que l'élément est quitté."%>
<%@ attribute name="select"
              description="Script exécuté lorsqu'une portion de texte est sélectionnée."%>
<%@ attribute name="onchange"
              description="Script exécuté lors que l'élément est quitté et que son contenu a été modifié."%>
<%@ attribute name="id"
              description="id de l'élément input. Par défaut : attribué automatiquement."%>

<%--Affectation de valeurs par défaut aux attributs--%>
<app:initvar attrname="_displayed" attrvalue="${displayed}" bundlekey="app.dateheure.displayed" defaultvalue="true"/>
<app:initvar attrname="_ignorenullnestedpath" attrvalue="${ignorenullnestedpath}" bundlekey="app.dateheure.ignorenullnestedpath" defaultvalue="true"/>
<app:initvar attrname="_theme" attrvalue="${theme}" bundlekey="app.dateheure.theme" defaultvalue="V"/>
<app:initvar attrname="_boxheight" attrvalue="${boxheight}" bundlekey="app.dateheure.boxheight" defaultvalue="auto"/>
<app:initvar attrname="_boxwidth" attrvalue="${boxwidth}" bundlekey="app.dateheure.boxwidth" defaultvalue="100%"/>
<app:initvar attrname="_labelboxwidth" attrvalue="${labelboxwidth}" bundlekey="app.dateheure.labelboxwidth" defaultvalue="35%"/>
<app:initvar attrname="_inputboxwidth" attrvalue="${inputboxwidth}" bundlekey="app.dateheure.inputboxwidth" defaultvalue="50%"/>
<app:initvar attrname="_compboxwidth" attrvalue="${compboxwidth}" bundlekey="app.dateheure.compboxwidth" defaultvalue="15%"/>

<app:initvar attrname="_inputclass" attrvalue="${inputclass}" bundlekey="app.dateheure.inputclass" defaultvalue="inputText"/>
<app:initvar attrname="_labelclass" attrvalue="${labelclass}" bundlekey="app.dateheure.labelclass" defaultvalue="labelText"/>
<app:initvar attrname="_normalboxclass" attrvalue="${normalboxclass}" bundlekey="app.dateheure.normalboxclass" defaultvalue="normal"/>
<app:initvar attrname="_lectureboxclass" attrvalue="${lectureboxclass}" bundlekey="app.dateheure.lectureboxclass" defaultvalue="lecture"/>
<app:initvar attrname="_erreurboxclass" attrvalue="${erreurboxclass}" bundlekey="app.dateheure.erreurboxclass" defaultvalue="erreur"/>
<app:initvar attrname="_labelboxclass" attrvalue="${labelboxclass}" bundlekey="app.dateheure.labelboxclass" defaultvalue="boxlblText"/>
<app:initvar attrname="_inputboxclass" attrvalue="${inputboxclass}" bundlekey="app.dateheure.inputboxclass" defaultvalue="boxText"/>
<app:initvar attrname="_compboxclass" attrvalue="${compboxclass}" bundlekey="app.dateheure.compboxclass" defaultvalue="boxCompText"/>
<app:initvar attrname="_normalindicateur" attrvalue="${normalindicateur}" bundlekey="app.dateheure.normalindicateur" defaultvalue=""/>

<app:initvar attrname="_dateformat" attrvalue="${dateformat}" bundlekey="app.dateheure.dateformat" defaultvalue="%d/%m/%Y %H:%M"/>
<app:initvar attrname="_calendarrepertoire" attrvalue="${calendarrepertoire}" bundlekey="app.dateheure.calendarrepertoire" defaultvalue="/composants/jscalendar"/>
<app:initvar attrname="_consigne" attrvalue="${consigne}" bundlekey="app.dateheure.consigne" defaultvalue="Choisir une date et une heure."/>
<app:initvar attrname="_appnouvelcharte" attrvalue="${appnouvelcharte}" bundlekey="app.nouvel.charte" defaultvalue="false"/>
<c:if test="${_appnouvelcharte eq 'true' }">
<app:initvar attrname="_calendarimage" attrvalue="${calendarimage}" bundlekey="app.dateheure.calendarimage" defaultvalue="calendar.gif"/>
</c:if>
<c:if test="${_appnouvelcharte ne 'true' }">
<app:initvar attrname="_calendarimage" attrvalue="${calendarimage}" bundlekey="app.dateheure.calendarimage" defaultvalue="calendrier.gif"/>
</c:if>

<%@ variable name-given="_inputId"  
             variable-class="java.lang.String" declare="true" 
             scope="NESTED" 
             description="Indique &egrave; l'ensemble des tags inclus l'ID du champ input." %> 

<%-- début du test d'évaluation du tag--%>
<c:if test="${_displayed == 'true' }">

<%-- Utilisation de la variable _formObjectName exposée par le tag app:form --%>
<c:if test="${!empty  _formObjectName}">
<c:set var="_path" value="${_formObjectName}.${attribut}"/>
</c:if>

<%-- Utilisation de la variable _formReadOnly exposée par le tag app:form --%>
<c:if test="${!empty  _formReadOnly ||  _readonly eq 'true'}">
<c:set var="_readonly" value="true" scope="request"/>
</c:if>

<%-- Détermination de l'ID du champ --%>
<app:id attrname="_id" id="${id}"></app:id>

<%-- Place dans le contexte 'request' la variable _inputId --%>
<c:if test="${! empty _id}">  
  <c:set var="_inputId" value="${_id}" scope="request"/>
</c:if>

<%@ include file="/META-INF/resources/WEB-INF/tags/app/fillstatus_error.tagf" %>

<c:if test="${empty libelle}">
	<fmt:message key="${attribut}" var="libelle" />
	<str:countMatches substring="???${attribut}???" var="exitInMessagesProperties">${libelle}</str:countMatches>
	<c:if test="${exitInMessagesProperties == 1}"><c:set var="libelle" value=""/></c:if>
</c:if>

<%-- Sélection de la classe du bloc div --%>
<c:choose>
	<c:when test="${status_error}">
		<c:set var="_divclass" value="${_theme}${_erreurboxclass}"/>
		<c:if test="${empty _firsterrorid}">
			<c:set var="_firsterrorid" value="${_inputId}" scope="request"/> 
		</c:if>
	</c:when>
	<c:when test="${_readonly eq 'true'}">
		<c:set var="_divclass" value="${_theme}${_lectureboxclass}"/>
	</c:when>
	<c:otherwise>
		<c:set var="_divclass" value="${_theme}${_normalboxclass}"/>
		<c:set var="libelle" value="${libelle}${_normalindicateur}"/>
	</c:otherwise>
</c:choose>

<%-- Utilisation du hack "_" pour contourner l'absence de propriété min-height sous IE --%>
<div class="${_divclass}" style="width: ${_boxwidth};height: auto;_height: ${_boxheight};min-height: ${_boxheight};">
    <%-- Insère le label associé aux champs de saisie--%>
    <div class="${_labelboxclass}" style="width: ${_labelboxwidth};">
	    <c:if test="${! empty accesskey}">
		    <c:set var="_complabel" value="accesskey=\"${accesskey}\""/>
	    </c:if>
	    <c:if test="${! empty consigne}">
		    <c:set var="_complabel" value="${_complabel} title=\"${consigne}\""/>
	    </c:if>
        <label class="${_labelclass}" for="${_id}" ${_complabel}>${libelle}</label>
        
        <%-- Insère le champ caché associé --%>
        <html:input path="${_path}" type="hidden">
	      	<jsp:attribute name="ignorenullnestedpath">${_ignorenullnestedpath}</jsp:attribute>
	    	<jsp:attribute name="id">${_id}</jsp:attribute>
	    	<jsp:attribute name="readOnly">${_readonly}</jsp:attribute>
	    </html:input>    
	    
    </div>
	
	<%-- Insère le label --%>
	<app:id attrname="_labelid" id="${labelid}"></app:id>
    <div class="${_inputboxclass}" style="width: ${_inputboxwidth};">
	    <html:label path="${_path}" id="${_labelid}" class="${_inputclass}" style="width:140px;" ignorenullnestedpath="${_ignorenullnestedpath}"/>
  	     <app:id attrname="_buttonid" id="${buttonid}"></app:id> 
  	     
		<c:if test="${! empty dateouverture }">
			<c:catch var="parseexcep" >
				<fmt:parseDate value="${dateouverture}" pattern="dd/MM/yyyy" var="daouvcal"/>
			</c:catch>
		</c:if>
		<c:if test="${empty daouvcal}">
			<jsp:useBean  id="daouvcal" class="java.util.Date"/>
		</c:if>
		
		<fmt:formatDate value="${daouvcal}"  pattern="yyyy" var="anneeouv"/>
		<fmt:formatDate value="${daouvcal}"  pattern="M" var="moisouv"/>
		<fmt:formatDate value="${daouvcal}"  pattern="d" var="jourouv"/>
		<c:set var="moisouv" value="${moisouv} - 1"/>
		<c:set var="dateouvformatepourjs" value="${anneeouv},${moisouv},${jourouv}"/>
	  	         	       
		<%-- Insère l'image qui permet de pointer vers le calendrier --%>
			 <html:attributes var="attrString" id="${_buttonid}" title="${_consigne}">
			  		 <c:if test="${! _readonly eq 'true'}">
							<img ${attrString} src="<c:url value="${_calendarrepertoire}/img/${_calendarimage}"/>"
								 onmouseover="this.style.background='red';"  onmouseout="this.style.background=''" alt="Calendrier"/>
							<script type="text/javascript">
							/*<![CDATA[*/
							datedemarrage = new Date(${dateouvformatepourjs}); 
							Calendar.setup({inputField:"${_inputId}",ifFormat:"${_dateformat}",displayArea:"${_labelid}",
						        daFormat:"${_dateformat}",showsTime:true,singleClick:false,button:"${_buttonid}",date:datedemarrage });
							/*]]>*/
							</script>
					</c:if>
			</html:attributes>
	</div>
	
    <%-- Insère les éléments présents dans le corps du tag --%>
    <%-- Lecture du corps du Tag--%>
	<jsp:doBody var="resbody"/>
		<c:if test="${! empty resbody}">
        <div class="${_compboxclass}" style="width: ${_compboxwidth};">
        ${resbody}
        </div>
        </c:if>
</div>

<%-- Supprime du contexte 'request' la variable _inputId --%>
<c:if test="${! empty _id}">  
  <c:remove var="_inputId" scope="request"/>
</c:if>

<%-- fin du test d'évaluation du tag--%>
</c:if>