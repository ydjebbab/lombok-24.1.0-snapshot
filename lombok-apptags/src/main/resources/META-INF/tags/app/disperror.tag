<%@ tag dynamic-attributes="attributes" 
        isELIgnored="false" 
        pageEncoding="UTF-8"
        description="Affiche une boite de dialogue contenant des messages d'erreur."%>
        
<%@ include file="/META-INF/resources/WEB-INF/tags/html/includes.tagf" %>
<%@ attribute name="path" 
              required="false"
              description="Chemin d'accès au bean ou à la propriété du bean dont la variable status sera exposée"%>
<%@ attribute name="var" 
              required="false"
              description="Nom d'un bean, contenant un message d'erreur à afficher, enregistré dans un des scopes accessibles à la JSP."%>
<%@ attribute name="id" 
              required="false"
              description="id du popup. Par défaut = boite"%>
<%@ attribute name="idmsg" 
              required="false"
              description="id du bloc div qui contient les messages à afficher. Par défaut = popmsgid"%>

<%@ attribute name="imagesrepertoire" 
              description="Répertoire placé sous 'webapp' contenant les images utilisées par le tag. Par défaut = /composants/dhtmlpopup/img" %>
<%@ attribute name="titre" 
              description="Titre du popup. Par défaut = Attention" %>
<%@ attribute name="backgroundcolor" 
              description="couleur arrière-plan du popup. Par défaut = white" %>
<%@ attribute name="textcolor" 
              description="couleur du texte. Par défaut = black" %>
<%@ attribute name="textfont" 
              description="Police du texte. Par défaut = bold 10pt verdana" %>
<%@ attribute name="titrebackgroundcolor" 
              description="Police du texte. Par défaut = navy" %>              
<%@ attribute name="titrecolor" 
              description="Couleur du texte du titre. Par défaut = white" %>              
<%@ attribute name="bordercolor" 
              description="Couleur de la bordure. Par défaut = #dddddd" %>              
<%@ attribute name="scrollbarcolor" 
              description="Couleur de la barre de scroll. Par défaut = gray" %>              

<%@ attribute name="maxwidth" 
              description="Largeur maximale en pixel du popup. Par défaut = 500" %>              
<%@ attribute name="maxheight" 
              description="Hauteur maximale en pixel du popup. Par défaut = 300" %>              

<%@ attribute name="afficheboutons" 
              description="Affiche ou non les boutons permettant de fermer, minimiser et maximiser le popup. Par défaut = true" %>              


<%-- Initialisation des paramètres par défaut --%>
<app:initvar attrname="_id" attrvalue="${id}" bundlekey="app.disperror.id" defaultvalue="boite"/>
<app:initvar attrname="_idmsg" attrvalue="${idmsg}" bundlekey="app.disperror.idmsg" defaultvalue="popmsgid"/>
<app:initvar attrname="_imagesrepertoire" attrvalue="${imagesrepertoire}" bundlekey="app.disperror.imagesrepertoire" defaultvalue="/composants/dhtmlpopup/img"/>
<app:initvar attrname="_titre" attrvalue="${titre}" bundlekey="app.disperror.titre" defaultvalue="Attention !"/>
<app:initvar attrname="_backgroundcolor" attrvalue="${backgroundcolor}" bundlekey="app.disperror.backgroundcolor" defaultvalue="white"/>
<app:initvar attrname="_textcolor" attrvalue="${textcolor}" bundlekey="app.disperror.textcolor" defaultvalue="black"/>
<app:initvar attrname="_textfont" attrvalue="${textfont}" bundlekey="app.disperror.textfont" defaultvalue="bold 10pt verdana"/>
<app:initvar attrname="_titrebackgroundcolor" attrvalue="${titrebackgroundcolor}" bundlekey="app.disperror.titrebackgroundcolor" defaultvalue="navy"/>
<app:initvar attrname="_titrecolor" attrvalue="${titrecolor}" bundlekey="app.disperror.titrecolor" defaultvalue="white"/>
<app:initvar attrname="_bordercolor" attrvalue="${bordercolor}" bundlekey="app.disperror.bordercolor" defaultvalue="#dddddd"/>
<app:initvar attrname="_scrollbarcolor" attrvalue="${scrollbarcolor}" bundlekey="app.disperror.scrollbarcolor" defaultvalue="gray"/>
<app:initvar attrname="_maxwidth" attrvalue="${maxwidth}" bundlekey="app.disperror.maxwidth" defaultvalue="500"/>
<app:initvar attrname="_maxheight" attrvalue="${maxheight}" bundlekey="app.disperror.maxheight" defaultvalue="300"/>
<app:initvar attrname="_afficheboutons" attrvalue="${afficheboutons}" bundlekey="app.disperror.afficheboutons" defaultvalue="true"/>

<%--Construit une variable contenant un fragment HTML à placer dans le message d'erreur. --%>
<c:if test="${! empty path}">
	<spring:bind path="${path}">
	<c:set var="listeerreurs" value=""/>
	<c:if test="${status.error}">
		<html:forEachError path="${path}" var="errorMessage">
			<c:set var="listeerreurs" value="${listeerreurs}<li>${errorMessage}</li>"/>
		</html:forEachError>
	</c:if>
	</spring:bind>
</c:if>

<jsp:doBody var="contentTag"/>

<%--Affiche un message d'erreur si nécessaire --%>
<c:if test="${! empty listeerreurs || ! empty var || ! empty contentTag || ! empty exceptionCatchedInTag}">

<div id="${_idmsg}" style="display:none">
	<ul>
	<c:if test="${! empty exceptionCatchedInTag}">${exceptionCatchedInTag}</c:if>
	<c:if test="${! empty listeerreurs}">${listeerreurs}</c:if>
	<c:if test="${! empty var}"><li>${var}</li></c:if>
	</ul>
	${contentTag}
</div>

<script type="text/javascript">
/*<![CDATA[*/

<c:choose>
	<c:when test="${! empty _firsterrorid}">
		<%--Focus sur le premier champ en erreur --%>
		pumFocusErrorId = '${_firsterrorid}';
		<c:set var="_nearid" value="${_firsterrorid}"/>
	</c:when>
	<c:otherwise>
		<c:set var="_nearid" value="${_idmsg}"/>
	</c:otherwise>
</c:choose>
var _dynarch_PumPopup = new PumPopUp('${_nearid}', <%-- id de l'objet qui sert de reference pour positionner le popup --%>
                        '${_id}',      				<%-- id_de_la_popup --%>
                        '${_idmsg}', 				 <%-- id de l'objet qui contient les messages d'erreur Ã  afficher --%>
                        '${_backgroundcolor}',      <%-- couleur arriere-plan popup --%>
                        '${_textcolor}',      		<%-- couleur texte --%>
                        '${_textfont}', 			<%-- police --%>
                        '${_titre}',				<%-- titre --%>
                        '${_titrebackgroundcolor}', <%-- couleur arriere-plan du titre --%>
                        '${_titrecolor}',			<%-- couleur titre --%>
                        '${_bordercolor}',    		<%-- couleur bordure --%>
                        '${_scrollbarcolor}',		<%-- couleur barre de scroll --%> 
                        '<c:url value="${_imagesrepertoire}"/>', <%--  Repertoire de stockage des images --%>
                        0, <%-- Delta utilise pour augmenter la taille du popup--%>
                        '${_maxwidth}',  <%-- Largeur maximale du popup en pixel--%>
                        '${_maxheight}',  <%-- hauteur maximale du popup en pixel--%>
                        ${_afficheboutons}  <%-- booléen permettant d'afficher ou non les boutons permettant de fermer, minimiser et maximiser le popup --%>);
                   
PumPopUp.addEvent(window,'load',PumPopUp.objectpopup);   						
/*]]>*/
</script>
</c:if>
