<%@ tag dynamic-attributes="attributes" 
        isELIgnored="false" 
        pageEncoding="utf-8"
        description="Champs de saisie de type 'text' associé à une liste déroulante qui propose une liste
        de valeurs possibles. Le champ de saisie est bindé à un objet du formulaire."%>
        
<%@ include file="/META-INF/resources/WEB-INF/tags/html/includes.tagf" %>

<%--Attributs principaux --%>
<%@ attribute name="attribut" 
              required="true" 
              description="Nom de l'attribut associé au champs de saisie (input) dans l'objet support du formulaire. Utilisé dans le spring bind" %>
<%@ attribute name="maxlength" 
              required="true"
              description="Longueur maximale qu'il est possible de saisir : propriété 'maxlength' de l'élément input"%>
<%@ attribute name="readonly"
              description="true : champs en lecture seule"%>
<%@ attribute name="libelleinput" 
              required="false" 
              description="Label associé au champs de saisie (input)"%>
<%@ attribute name="libelleliste" 
              required="false" 
              description="Label associé à la liste déroulante (select)"%>
<%@ attribute name="size" 
              required="true" 
              description="Taille du champs de saisie. Position la propriété size dans l'élément input."%>
<%@ attribute name="requis" 
              description="true : champs de saisie obligatoire.  Valeur par défaut = true "%>
<%@ attribute name="itemslist" 
              type="java.lang.Object" 
              description="Bean de type List contenant les éléments de la liste déroulante."%>
<%@ attribute name="itemsset" 
              type="java.lang.Object" 
              description="Bean de type set contenant les éléments de la liste déroulante."%>
<%@ attribute name="itemsmap" 
              type="java.lang.Object"
              description="Bean de type Map contenant les éléments de la liste déroulante." %>
<%@ attribute name="label"
              description="Nom de la propriété d'un élément de la collection (items...) affichée dans la liste déroulante."%>
<%@ attribute name="value"
              description="Nom de la propriété d'un élément de la collection (items...) utilisé comme valeur de l'option."%>

<%@ attribute name="defautitemlabel"
			description="Libellé de l'item par défaut. Valeur par défaut : -- Aucune --"%>
<%@ attribute name="defautitemvalue"
			description="Libellé de l'item par défaut. Valeur par défaut : 999999"%>
<%@ attribute name="defautitem"
			description="true : affiche un item par défaut. Valeur par défaut : true"%>
<%@ attribute name="ignorenullnestedpath" 
              description="true : Ignore les références nulles lors de l'accès à un attribut. Valeur par défaut = true "%>
<%@ attribute name="displayed"
              description="true: le tag est évalué. Par défaut : true"%>

<%--Attributs de présentation --%>
<%@ attribute name="theme"
              description="Thème de présentation. Par défaut = V"%>
<%@ attribute name="boxheight"
              description="Hauteur du bloc div contenant l'ensemble des éléments relatifs à un champs de saisie."%>
<%@ attribute name="boxwidth"
              description="Largeur du bloc div contenant l'ensemble des éléments relatifs à un champs de saisie."%>
<%@ attribute name="labelboxwidth"
              description="Largeur du bloc div contenant le libelle (en % de la largeur totale du bloc parent)"%>
<%@ attribute name="inputboxwidth"
              description="Largeur du bloc div contenant le champ de saisie (en % de la largeur totale du bloc parent)"%>
<%@ attribute name="compboxwidth"
              description="Largeur du bloc div contenant la zone complémentaire (en % de la largeur totale du bloc parent)"%>

<%--Attributs de présentation --%>
<%@ attribute name="normalboxclass"
              description="Classe css du bloc div contenant l'ensemble des éléments relatifs à un champs de saisie. Valeur par défaut = normal "%>
<%@ attribute name="lectureboxclass"
              description="Classe css du bloc div contenant l'ensemble des éléments relatifs à un champs de saisie, en lecture seule. Valeur par défaut = lectureseule."%>
<%@ attribute name="erreurboxclass"
              description="Classe css du bloc div contenant l'ensemble des éléments relatifs à un champs de saisie en erreur. Valeur par défaut = erreur."%>
<%@ attribute name="requisboxclass"
              description="Classe css du bloc div contenant l'ensemble des éléments relatifs à un champs de saisie obligatoire. Valeur par défaut = requis."%>
<%@ attribute name="labelboxclass"
              description="Classe css du bloc div contenant le libellé.Valeur par défaut = boxlblText."%>
<%@ attribute name="inputboxclass"
              description="Classe css du bloc div contenant le champs de saisie.Valeur par défaut = boxText."%>
<%@ attribute name="compboxclass"
              description="Classe css du bloc div contenant des données complémentaires sur le champs de saisie.Valeur par défaut = boxCompText."%>
<%@ attribute name="inputclass"
              description="Classe css du champs input. Valeur par défaut = inputText"%>            
<%@ attribute name="labelclass"
              description="Classe css de l'élément label. Valeur par défaut = labelText"%>            
              
<%--Attributs optionnels --%>
<%@ attribute name="consigne" 
              description="Correspond à l'attribut 'title' de l'élément label. Peut contenir, par exemple, une consigne de saisie du champs. "%>
<%@ attribute name="onkeypress"
              description="Code javascript à exécuter lorsqu'on appuie sur la touche 'ENTER' dans ce champs.Valeur par défaut = 'return submitEnterButton(event)'"%>
<%@ attribute name="pctlibelle"
			description="Largeur occupé par le label, exprimé en pourcentage de la taille du bloc.Valeur par défaut = 35%"%>


<%-- Attributs  --%>
<%@ attribute name="tabindex"
              description="Numéro indiquant l'ordre d'accès par tabulation."%>
<%@ attribute name="accesskey"
              description="Touche de raccourci pour accéder à un champs."%>
<%@ attribute name="onfocus"
              description="Script exécuté lors que l'élément est accédé."%>
<%@ attribute name="onblur"
              description="Script exécuté lors que l'élément est quitté."%>
<%@ attribute name="select"
              description="Script exécuté lorsqu'une portion de texte est sélectionnée."%>
<%@ attribute name="onchange"
              description="Script exécuté lors que l'élément est quitté et que son contenu a été modifié."%>
<%@ attribute name="id"
              description="id de l'élément input. Par défaut : attribué automatiquement."%>
              
<%--Affectation de valeurs par défaut aux attributs--%>
<app:initvar attrname="_displayed" attrvalue="${displayed}" bundlekey="app.inputcombo.displayed" defaultvalue="true"/>
<app:initvar attrname="_ignorenullnestedpath" attrvalue="${ignorenullnestedpath}" bundlekey="app.inputcombo.ignorenullnestedpath" defaultvalue="true"/>
<app:initvar attrname="_requis" attrvalue="${requis}" bundlekey="app.inputcombo.requis" defaultvalue="true"/>
<app:initvar attrname="_readonly" attrvalue="${readonly}" bundlekey="app.inputcombo.readonly" defaultvalue="false"/>

<app:initvar attrname="_requisindicateur" attrvalue="${requisindicateur}" bundlekey="app.inputcombo.requisindicateur" defaultvalue=" *"/>
<app:initvar attrname="_normalindicateur" attrvalue="${normalindicateur}" bundlekey="app.inputcombo.normalindicateur" defaultvalue=""/>

<app:initvar attrname="_theme" attrvalue="${theme}" bundlekey="app.inputcombo.theme" defaultvalue="V"/>
<app:initvar attrname="_boxheight" attrvalue="${boxheight}" bundlekey="app.inputcombo.boxheight" defaultvalue="auto"/>
<app:initvar attrname="_boxwidth" attrvalue="${boxwidth}" bundlekey="app.inputcombo.boxwidth" defaultvalue="100%"/>
<app:initvar attrname="_labelboxwidth" attrvalue="${labelboxwidth}" bundlekey="app.inputcombo.labelboxwidth" defaultvalue="0%"/>
<app:initvar attrname="_inputboxwidth" attrvalue="${inputboxwidth}" bundlekey="app.inputcombo.inputboxwidth" defaultvalue="0%"/>
<app:initvar attrname="_compboxwidth" attrvalue="${compboxwidth}" bundlekey="app.inputcombo.compboxwidth" defaultvalue="30%"/>

<app:initvar attrname="_inputclass" attrvalue="${inputclass}" bundlekey="app.inputcombo.inputclass" defaultvalue="inputText"/>
<app:initvar attrname="_labelclass" attrvalue="${labelclass}" bundlekey="app.inputcombo.labelclass" defaultvalue="labelText"/>
<app:initvar attrname="_onkeypress" attrvalue="${onkeypress}" bundlekey="app.inputcombo.onkeypress" defaultvalue="return submitEnterButton(event)"/>
<app:initvar attrname="_normalboxclass" attrvalue="${normalboxclass}" bundlekey="app.inputcombo.normalboxclass" defaultvalue="normal"/>
<app:initvar attrname="_lectureboxclass" attrvalue="${lectureboxclass}" bundlekey="app.inputcombo.lectureboxclass" defaultvalue="lecture"/>
<app:initvar attrname="_erreurboxclass" attrvalue="${erreurboxclass}" bundlekey="app.inputcombo.erreurboxclass" defaultvalue="erreur"/>
<app:initvar attrname="_requisboxclass" attrvalue="${requisboxclass}" bundlekey="app.inputcombo.requisboxclass" defaultvalue="requis"/>
<app:initvar attrname="_labelboxclass" attrvalue="${labelboxclass}" bundlekey="app.inputcombo.labelboxclass" defaultvalue="boxlblText"/>
<app:initvar attrname="_inputboxclass" attrvalue="${inputboxclass}" bundlekey="app.inputcombo.inputboxclass" defaultvalue="boxText"/>
<app:initvar attrname="_compboxclass" attrvalue="${compboxclass}" bundlekey="app.inputcombo.compboxclass" defaultvalue="boxCompText"/>

<app:initvar attrname="_defautitemlabel" attrvalue="${defautitemlabel}" bundlekey="app.inputcombo.defautitemlabel" defaultvalue="--      ---"/>			
<app:initvar attrname="_defautitemvalue" attrvalue="${defautitemvalue}" bundlekey="app.inputcombo.defautitemvalue" defaultvalue="-999999"/>			
<app:initvar attrname="_defautitem" attrvalue="${defautitem}" bundlekey="app.inputcombo.defautitem" defaultvalue="true"/>			

<%-- début du test d'évaluation du tag--%>
<c:if test="${_displayed == 'true' }">

<%-- Utilisation de la variable _formObjectName exposée par le tag app:form --%>
<c:if test="${!empty  _formObjectName}">
<c:set var="_path" value="${_formObjectName}.${attribut}"/>
</c:if>

<%-- Utilisation de la variable _formReadOnly exposée par le tag app:form --%>
<c:if test="${!empty  _formReadOnly}">
<c:set var="_readonly" value="true"/>
</c:if>

<%-- Détermination de l'ID du champ input--%>
<app:id attrname="_id" id="${id}"></app:id>

<%-- Détermination de l'ID du champ select --%>
<app:id attrname="_idselect" id="${attribut}inactif"></app:id>

<%@ include file="/META-INF/resources/WEB-INF/tags/app/fillstatus_error.tagf" %>

<%-- Remplit la variable status_warning si nécessaire --%>
<%@ include file="/META-INF/resources/WEB-INF/tags/app/fillstatus_warning.tagf" %>

<c:if test="${empty libelle}">
	<fmt:message key="${attribut}" var="libelle" />
	<str:countMatches substring="???${attribut}???" var="exitInMessagesProperties">${libelle}</str:countMatches>
	<c:if test="${exitInMessagesProperties == 1}"><c:set var="libelle" value=""/></c:if>
</c:if>

<%-- Sélection de la classe du bloc div --%>
<c:choose>
	<c:when test="${status_error || status_warning}">
		<c:set var="_divclass" value="${_theme}${_erreurboxclass}"/>
		<c:if test="${empty _firsterrorid}">
			<c:set var="_firsterrorid" value="${_id}" scope="request"/> 
		</c:if>
	</c:when>
	<c:when test="${_readonly eq 'true'}">
		<c:set var="_divclass" value="${_theme}${_lectureboxclass}"/>
	</c:when>
	<c:when test="${_requis eq 'true'}">
		<c:set var="_divclass" value="${_theme}${_requisboxclass}"/>
	</c:when>
	<c:otherwise>
		<c:set var="_divclass" value="${_theme}${_normalboxclass}"/>
	</c:otherwise>
</c:choose>

<%-- Ajout d'un indicateur pour les champs obligatoires --%>
<c:choose>
	<c:when test="${_requis eq 'true'}">
		<c:set var="libelle" value="${libelle}${_requisindicateur}"/>
	</c:when>
	<c:otherwise>
		<c:set var="libelle" value="${libelle}${_normalindicateur}"/>
	</c:otherwise>
</c:choose>

<div class="${_divclass}" style="width: ${_boxwidth};height: auto;_height: ${_boxheight};min-height: ${_boxheight};">
    <%-- Insère le label associé aux champs de saisie--%>
    <div class="${_labelboxclass}" style="width: auto;_width: ${_labelboxwidth};min-width: ${_labelboxwidth};">
	    <c:if test="${! empty accesskey}">
		    <c:set var="_complabel" value="accesskey='${accesskey}'"/>
	    </c:if>
	    <c:if test="${! empty consigne}">
		    <c:set var="_complabel" value="${_complabel} title='${consigne}'"/>
	    </c:if>
        <label class="${_labelclass}" for="${_id}" ${_complabel}>${libelleinput}</label>
    </div>
    <%-- Insère le champs de saisie --%>
    <div class="${_inputboxclass}" style="width: auto;_width: ${_inputboxwidth};min-width: ${_inputboxwidth};">
    <html:input path="${_path}">
       	<jsp:attribute name="ignorenullnestedpath">${_ignorenullnestedpath}</jsp:attribute>
    	<jsp:attribute name="maxlength">${maxlength}</jsp:attribute>
    	<jsp:attribute name="size">${size}</jsp:attribute>
    	<jsp:attribute name="id">${_id}</jsp:attribute>
    	<jsp:attribute name="class">${_inputclass}</jsp:attribute>
    	<jsp:attribute name="readOnly">${_readonly}</jsp:attribute>
    	<jsp:attribute name="onkeypress">${_onkeypress}</jsp:attribute>
    	<jsp:attribute name="tabindex">${tabindex}</jsp:attribute>
    	<jsp:attribute name="onfocus">${onfocus}</jsp:attribute>
    	<jsp:attribute name="onblur">javascript:return refreshCombo(this,'${_idselect}');</jsp:attribute>
    	<jsp:attribute name="onselect">${onselect}</jsp:attribute>
    	<jsp:attribute name="onchange">${onchange}</jsp:attribute>
    </html:input>
	</div>
	
    <%-- Insère le label associé à  la liste déroulante --%>
    <div class="${_labelboxclass}" style="width:auto;_width: ${_labelboxwidth};min-width: ${_labelboxwidth};">
    <c:if test="${!empty  libelleliste}">
	    <label class="${_labelclass}" for="${_idselect}">${libelleliste}</label>
    </c:if>
    </div>
    <%-- Insère la liste déroulante--%>
    <div class="${_compboxclass}" style="width: auto;_width: ${_inputboxwidth};min-width: ${_inputboxwidth};">
    <html:selectsansbind>
    	<jsp:attribute name="id">${_idselect}</jsp:attribute>
    	<jsp:attribute name="readOnly">${_readonly}</jsp:attribute>
    	<jsp:attribute name="onchange">javascript:populateInput('${_id}',this);</jsp:attribute>
    	<jsp:body>
		
		<%-- Insertion d'un item par défaut--%>
		<c:if test="${_defautitem eq 'true'}">
			<html:option value="${_defautitemvalue}" selected="${selected}">${_defautitemlabel}</html:option>
		</c:if>
		
		<%-- Les items de la liste déroulante sont stockés dans une MAP --%>
		<c:if test="${!empty itemsmap}">  
     		<html:optionsmap items="${itemsmap}" selected="${selected}">
     			<jsp:attribute name="label">${label}</jsp:attribute>
     			<jsp:attribute name="value">key</jsp:attribute>
     		</html:optionsmap>
		</c:if>

		<%-- Les items de la liste déroulante sont stockés dans une collection de type List --%>
		<c:if test="${!empty itemslist}">
			<html:options items="${itemslist}" selected="${selected}">
 		    	<jsp:attribute name="label">${label}</jsp:attribute>
  		    	<jsp:attribute name="value">${value}</jsp:attribute>
			</html:options>
		</c:if>
		
		<%-- Les items de la liste déroulante sont stockés dans une collection de type Set --%>
		<c:if test="${!empty itemsset}">
			<html:options items="${itemsset}" selected="${selected}">
 		    	<jsp:attribute name="label">${label}</jsp:attribute>
  		    	<jsp:attribute name="value">${value}</jsp:attribute>
			</html:options>
		</c:if>
		</jsp:body>
    </html:selectsansbind>
    </div>
    <%-- Insère les éléments présents dans le corps du tag --%>
    <%-- Lecture du corps du Tag--%>
	<jsp:doBody var="resbody"/>
		<c:if test="${! empty resbody}">
	        <div class="${_compboxclass}">${resbody}</div>
		</c:if>
</div>

<%-- fin du test d'évaluation du tag--%>
</c:if>