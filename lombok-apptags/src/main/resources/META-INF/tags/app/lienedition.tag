<%@ tag dynamic-attributes="attributes"
        isELIgnored="false"
        pageEncoding="UTF-8"
        description="Insère un lien pointant vers une edition"%>

<%@ include file="/META-INF/resources/WEB-INF/tags/html/includes.tagf" %>
<%--Attributs principaux --%>
<%@ attribute name="jobhistory"
			 type="java.lang.Object"
              description="Objet JobHistory qui représente l'édition."
              %>
<%@ attribute name="active"
              description="Attribut qui indique si le lien est actif."%>
<%@ attribute name="aclass"
              description="Classe css de l'élément 'a'"%>
<%@ attribute name="title"
              description="Titre de l'élément href et de l'image"%>
<%@ attribute name="alt"
              description="Texte affiché si le navigateur n'affiche pas les images."%>
<%@ attribute name="openwindow"
              description="true : ouvre une nouvelle fenêtre du navigateur. Par défaut : false "%>
<%@ attribute name="openwindowoptions"
              description="Options d'ouverture d'une nouvelle fenêtre du navigateur."%>
<%@ attribute name="openwindowname"
              description="Nom de la nouvelle fenêtre."%>                            
<%@ attribute name="waitingmsg"
              description="true : affiche un message d'attente. Par défaut : false "%>
<%@ attribute name="displayed"
              description="true: le tag est évalué. Par défaut : true"%>


<%@ attribute name="onclick"
              description="Script exécuté lors du clic"%>

<%-- Affectation de valeurs par défaut aux attributs --%>
<app:initvar attrname="_displayed" attrvalue="${displayed}" bundlekey="app.lienedition.displayed" defaultvalue="true"/>
<app:initvar attrname="_imagesrepertoire" attrvalue="${imagesrepertoire}" bundlekey="app.lienedition.imagesrepertoire" defaultvalue="/composants/tagsapp/images/"/>
<app:initvar attrname="_aclass" attrvalue="${aclass}" bundlekey="app.lienedition.aclass" defaultvalue="lien"/>
<app:initvar attrname="_openwindow" attrvalue="${openwindow}" bundlekey="app.lienedition.openwindow" defaultvalue="false"/>
<app:initvar attrname="_openwindowname" attrvalue="${openwindowname}" bundlekey="app.lienedition.openwindowname" defaultvalue=""/>
<app:initvar attrname="_openwindowoptions" attrvalue="${openwindowoptions}" bundlekey="app.lienedition.openwindowoptions" 
	defaultvalue="toolbar=yes, directories=no, location=no, status=yes, menubar=no, resizable=yes, scrollbars=yes, width=500, height=400"/>
<app:initvar attrname="_active" attrvalue="${active}" bundlekey="app.lienedition.active" defaultvalue="true"/>
<app:initvar attrname="_waitingmsg" attrvalue="${waitingmsg}" bundlekey="app.lienedition.waitingmsg" defaultvalue="false"/>

<%-- début du test d'évaluation du tag--%>
<c:if test="${_displayed == 'true' }">



<%-- Contenu du lien --%>
<c:if test="${!empty image}">
		<c:set var="_content"><img title="${title}" src="<c:url value="${_imagesrepertoire}/${image}"/>" ${attrString}   alt="${tooltip}" style="border:0"/></c:set>
</c:if>


<%-- Insère le lien --%>
<c:choose>
	<%-- l'objet jobhistory n'est pas null --%>
	<c:when test="${! empty jobhistory}">
	  	<c:set var="_href" value="${pageContext.request.contextPath}/composantedition/banette/flux.ex?_flowId=consultationbanette-flow&amp;editionUuid=${jobhistory.editionUuid}&amp;uid=${jobhistory.destinationEdition.uidProprietaire}"/>
		<c:set var="_content" value="${jobhistory.presentationEdition.description}"/>
	</c:when>

	<%-- auucun objet dispo => accès à la banette des éditions de l'utilisateur --%>
	<c:otherwise>
		<c:set var="_content" value="Editions disponibles"/>
	  	<c:set var="_href" value="${pageContext.request.contextPath}/composantedition/banette/flux.ex?_flowId=consultationbanette-flow"/>
	</c:otherwise>
</c:choose>

<c:choose>
   <c:when test="${_openwindow == true}">
	   <a title="${title}" href="javascript:send_url_openwindow('${_href}','${_openwindowname}','${_openwindowoptions}')" class="${linkclass}" onclick="${onclick}" }>${_content}</a>
	</c:when>
	<c:when test="${_waitingmsg == true}">
		<a title="${title}" href="javascript:send_url_waiting('${_href}')" class="${linkclass}"  onclick="${onclick}">${_content}</a>
	</c:when>
	<c:otherwise>
		<a title="${title}" href="javascript:send_url('${_href}')" class="${linkclass}"  onclick="${onclick}" >${_content}</a>
	</c:otherwise>
</c:choose>

<%-- fin du test d'évaluation du tag--%>
</c:if>