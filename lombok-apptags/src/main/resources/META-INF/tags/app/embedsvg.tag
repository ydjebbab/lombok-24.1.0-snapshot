<%@ tag dynamic-attributes="attributes"
        isELIgnored="false"
        pageEncoding="UTF-8"
        description="Insè une image de type svgstockee dans le contexte de flux"%>

<%@ include file="/META-INF/resources/WEB-INF/tags/html/includes.tagf" %>
<%--Attributs principaux --%>
<%@ attribute name="fichierjoint"
              description="Nom sous lequel l'objet de type FichierJoint est stockee dans le contexte de flux"%>

<app:initvar attrname="_urlpattern" attrvalue="${urlpattern}" bundlekey="app.embedsvg.urlpattern" defaultvalue="fichierjoint"/>
<app:initvar attrname="_urlparametre" attrvalue="${urlparametre}" bundlekey="app.embedsvg.urlparametre" defaultvalue="fichierjoint"/>

<c:if test="${! empty fichierjoint}">
	<c:set var="src">
		<c:url value="/${_urlpattern}?${FLOWEXECUTIONID_FIELD_NAME}=${FLOWEXECUTIONID_VALUE}&${_urlparametre}=${fichierjoint}"/>
	</c:set>
</c:if>

<html:attributes var="attrString" attributeMap="${attributes}" src='${src}'type="image/svg+xml">
 	<embed ${attrString}/>
</html:attributes>

