<%@ tag dynamic-attributes="attributes" 
        isELIgnored="false" 
        pageEncoding="UTF-8"
        description="Ins&egrave;re un onglet dans un ensemble d'onglets"%>
        
<%@ include file="/META-INF/resources/WEB-INF/tags/html/includes.tagf" %>

<%--Attributs principaux --%>
<%@ attribute name="cle"
              required="true"
              description="Clé identifiant l'onglet"%>
<%@ attribute name="libelle"
              description="Texte de l'onglet"%>
<%@ attribute name="uri"
              description="Uri vers laquelle l'onglet doit pointer."%>
<%@ attribute name="bouton"
              description="Nom de la transition ou du bouton vers lequel l'onglet doit soumettre la requête."%>
<%@ attribute name="transition"
              description="Nom de la transition utilisée"%>
<%@ attribute name="active"
              description="true: le lien de l'onglet est actif. Par défaut : true"%>
<%@ attribute name="title"
              description="Titre de l'élément href et de l'image"%>
<%@ attribute name="displayed"
              description="true : le tag est évalué, false : il est ignoré. Par défaut : true."%>
<%@ attribute name="exposecle"
              required="false"
              description="true : Expose la clé identifiant l'onglet comme attribut de la requête http. Par défaut : false."%>
<%@ attribute name="responsive"
              description="gestion du parametre responsive."%>   
<%@ attribute name="ongletclass"
              description="classe associée a l'onglet."%>                  
<%@ attribute name="parametrecle"
              required="false"
              description="Nom de l'attribut sous lequel la clé identifiant l'onglet est exposée dans la requête http. Par défaut : cleonglet"%>              

<%-- Variables exposées --%>
<%@ variable name-given="_cleActive"  
             variable-class="java.lang.String" declare="true" 
             scope="NESTED" 
             description="Expose la cl&eacute; de l'onglet actif." %>

<%-- Affectation de valeurs par défaut aux attributs --%>
<app:initvar attrname="_active" attrvalue="${active}" bundlekey="app.onglet.active" defaultvalue="true"/>
<app:initvar attrname="_displayed" attrvalue="${displayed}" bundlekey="app.onglet.displayed" defaultvalue="true"/>
<app:initvar attrname="_exposecle" attrvalue="${exposecle}" bundlekey="app.onglet.exposecle" defaultvalue="false"/>
<app:initvar attrname="_parametrecle" attrvalue="${parametrecle}" bundlekey="app.onglet.parametrecle" defaultvalue="cleonglet"/>

<%-- gestion responsive avec properties --%>
<app:initvar attrname="_responsive" attrvalue="" bundlekey="app.page.rwd" defaultvalue="false"/>
<app:initvar attrname="_ongletclass" attrvalue="${ongletclass}" bundlekey="app.page.rwd.ongletsclass" defaultvalue="btmodel_2"/>

<%@ include file="/META-INF/resources/WEB-INF/tags/app/responsive_test.tagf" %>

<%-- début du test d'évaluation du tag--%>
<c:if test="${_displayed == 'true' }">

<%-- Les attributs bouton et transitions sont traités de la même façon --%>
<c:choose>
	<c:when test="${! empty bouton}">
		<c:set var="_transition" value="${bouton}"/>
	</c:when>
	<c:when test="${! empty transition}">
		<c:set var="_transition" value="${transition}"/>
	</c:when>
</c:choose>

<%-- Si la transition n'est pas préfixée par ${EVENTID_FIELD_PREFIX}, on ajoute ce préfixe au début de la chaîne --%>
<c:if test="${! empty _transition }">
  	<str:countMatches var="nb" substring="${EVENTID_FIELD_PREFIX}">${_transition}</str:countMatches>
  	<c:if test="${nb ne '1' }">
  		<c:set var="_transition" value="${EVENTID_FIELD_PREFIX}${_transition}"/>
  	</c:if>
</c:if>


<html:attributes var="attrString" attributeMap="${attributes}">

<c:if test="${ _responsive eq 'false'}">
<%-- Insère un onglet --%>
<c:choose>
	<%-- L'onglet en cours correspond à la clé active--%>
	<c:when test="${_cleActive eq cle}">
		<li class="active">${libelle}</li>
	</c:when>
	<%-- L'onglet ne correspond pas à la clé active--%>
	<c:otherwise>
		<c:choose>
			<%-- Si l'onglet en cours n'est pas actif, ne possède pas de transition ou d'url --%>
			<c:when test="${_active != true || (empty _transition && empty uri) }">
				<li class="onlytext">${libelle}</li>		
			</c:when>
			<c:when test="${_active == true && (! empty uri) }">
				<li><a title="${title}" href="javascript:send_url('<c:url value="${uri}"/>')">${libelle}</a></li>
			</c:when>
			<c:when test="${_active == true && (! empty _transition) && (_exposecle == false) }">
				<li><a title="${title}" href="javascript:submitfromonglet('${_transition}')">${libelle}</a></li>
			</c:when>
			<c:when test="${_active == true && (! empty _transition) && (_exposecle == true) }">
				<li><a title="${title}" href="javascript:submitfromonglet_parameter('${_transition}','${_parametrecle}','${cle}')">${libelle}</a></li>
			</c:when>
		</c:choose>
	</c:otherwise>
</c:choose>
</c:if>


<c:if test="${ _responsive eq 'true'}">

<%-- Insère un onglet --%>
<c:choose>
    <%-- L'onglet en cours correspond à la clé active--%>
    <c:when test="${_cleActive eq cle}">
      <div class="height_eq_item ${_ongletclass}" >
        <div class="active action_touch item">${libelle}</div>
      </div>
    </c:when>
    <%-- L'onglet ne correspond pas à la clé active--%>
    <c:otherwise>
        <c:choose>
            <%-- Si l'onglet en cours n'est pas actif, ne possède pas de transition ou d'url --%>
            <c:when test="${_active != true || (empty _transition && empty uri) }">
                 <div class="height_eq_item  ${_ongletclass}" >
                    <div class="onlytext action_touch item">${libelle}</div> 
                </div>       
            </c:when>
            <c:when test="${_active == true && (! empty uri) }">
                <div class="height_eq_item ${_ongletclass}">
                    <div class="action_touch item"><a title="${title}" href="javascript:send_url('<c:url value="${uri}"/>')">${libelle}</a></div>
                </div>       
            </c:when>
            <c:when test="${_active == true && (! empty _transition) && (_exposecle == false) }">
                 <div class="height_eq_item ${_ongletclass}"}>
                    <div class="action_touch item"><a title="${title}" href="javascript:submitfromonglet('${_transition}')">${libelle}</a></div>
                 </div>
            </c:when>
            <c:when test="${_active == true && (! empty _transition) && (_exposecle == true) }">
                <div class="height_eq_item ${_ongletclass}">
                    <div class="action_touch item"><a title="${title}" href="javascript:submitfromonglet_parameter('${_transition}','${_parametrecle}','${cle}')">${libelle}</a></div>
                </div>
            </c:when>
        </c:choose>
    </c:otherwise>
</c:choose>
</c:if>

</html:attributes>


<%-- fin du test d'évaluation du tag--%>
</c:if>
