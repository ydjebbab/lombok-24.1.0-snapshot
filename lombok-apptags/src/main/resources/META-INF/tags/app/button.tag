<%@ tag dynamic-attributes="attributes" 
        isELIgnored="false" 
        pageEncoding="UTF-8"
        description="Insère un élément 'button' associé à une transition ou à une URL"%>
        
<%@ include file="/META-INF/resources/WEB-INF/tags/html/includes.tagf" %>
<%--Attributs principaux --%>
<%@ attribute name="action"
              description="Nom de l'action - URL qui permet d'accéder au contrôleur."%>
<%@ attribute name="transition"
              description="Nom de la transition définie dans le webflow.;"%>
<%@ attribute name="uri"
              description="Uri vers laquelle pointe le lien. Utilisé si les paramètres action et transition ne sont pas utilisés."%>
<%@ attribute name="active"
              description="Attribut qui indique si le lien est actif."%>              
<%@ attribute name="attribute"
              description="Nom de l'attribut passé en paramètre"%>              
<%@ attribute name="value"
              description="Valeur de l'attribut passé en paramètre"%>                            
<%@ attribute name="buttonclass"
              description="Classe css de l'élément 'a'"%>
<%@ attribute name="openwindow"
              description="true : ouvre une nouvelle fenêtre du navigateur. Par défaut : false "%>
<%@ attribute name="modal"
              description="true : ouvre une fenêtre modale dans le cas de l'ouverture d'une nouvelle fenêtre du navigateur. Par défaut : false "%>
<%@ attribute name="waitingmsg"
              description="true : affiche un message d'attente. Par défaut : false "%>
<%@ attribute name="displayed"
              description="ttrue : le tag est évalué, false : il est ignoré. Par défaut : true."%>
<%@ attribute name="disabled"
              description="true : bouton désactivé."%>              
<%@ attribute name="responsive"
              description="gestion du parametre responsive."%>                            

<%-- Attributs  js--%>
<%@ attribute name="tabindex"
              description="Numéro indiquant l'ordre d'accès par tabulation."%>
<%@ attribute name="accesskey"
              description="Touche de raccourci pour accéder &agrave; un champs."%>
<%@ attribute name="onfocus"
              description="Script exécuté lors que l'élément est accédé."%>
<%@ attribute name="onblur"
              description="Script exécuté lors que l'élément est quitté."%>
<%@ attribute name="onselect"
              description="Script exécuté lorsqu'une portion de texte est sélectionnée."%>
<%@ attribute name="onchange"
              description="Script exécuté lors que l'élément est quitté et que son contenu &agrave; été modifié."%>

<%-- Attributs événements intrinsèques HTML --%>
<%@ attribute name="onclick"
              description="Script exécuté quand le bouton du dispositif de pointage est cliqué au-dessus d'un élément."%>
<%@ attribute name="ondblclick"
              description="Script exécuté quand le bouton du dispositif de pointage est double-cliqué au-dessus d'un élément."%>              
<%@ attribute name="onmousedown"
              description="Script exécuté quand le bouton du dispositif de pointage est appuyé au-dessus d'un élément."%>              
<%@ attribute name="onmouseup"
              description="Script exécuté quand le bouton du dispositif de pointage est relaché au-dessus d'un élément."%>                            
<%@ attribute name="onmouseover"
              description="Script exécuté quand le bouton du dispositif de pointage est déplacé sur un élément."%>                                          
<%@ attribute name="onmousemove"
              description="Script exécuté quand le bouton du dispositif de pointage est déplacé alors qu'il est au-dessus d'un élément."%>
<%@ attribute name="onmouseout"
              description="Script exécuté quand le bouton du dispositif de pointage est déplacé en dehors d'un élément."%>                                                                      
<%@ attribute name="onkeypress"
              description="Script exécuté quand une touche est pressée puis relachée au-dessus d'un élément."%>
<%@ attribute name="onkeydown"
              description="Script exécuté quand une touche est gardée appuyée au-dessus d'un élément."%>              
<%@ attribute name="onkeyup"
              description="Script exécuté quand une touche est relachée au-dessus d'un élément."%>                            


<%-- Paramètres de la nouvelle fenêtre ouverte --%>
<%@ attribute name="w_name"
              description="Nom de la nouvelle fenêtre. Par défaut : "%>
<%@ attribute name="w_width"
              description="Largeur de la nouvelle fenêtre en pixels . Par défaut : "%>
<%@ attribute name="w_height"
              description="Hauteur de la nouvelle fenêtre en pixels . Par défaut : "%>              
<%@ attribute name="w_resizable"
              description="yes or no .Active ou désactive la possibilité pour l'utilisateur de redimensionné la fenêtre. Par défaut : yes"%>         
<%@ attribute name="w_scrollbars"
              description="yes or no .Active ou désactive les barres de scroll. Par défaut : yes"%>                       
<%@ attribute name="w_toolbars"
              description="yes or no .Active ou désactive les boutons de navigations. Par défaut : no"%>                                     
<%@ attribute name="w_location"
              description="yes or no .Affiche ou non la barre d'adresse. Par défaut : no"%>  
<%@ attribute name="w_directories"
              description="yes or no .Affiche ou non les boutons supplémentaires. Par défaut : no"%>                
<%@ attribute name="w_status"
              description="yes or no .Affiche ou non la barre de statut. Par défaut : no"%>  
<%@ attribute name="w_menubar"
              description="yes or no .Affiche ou non les boutons (Fichier, Edition, ...). Par défaut : no"%>  
<%@ attribute name="w_copyhistory"
              description="yes or no .Permet ou non d'accéder à l'historique. Par défaut : no"%>  
                            

<%-- Affectation de valeurs par défaut aux attributs --%>
<app:initvar attrname="_imagesrepertoire" attrvalue="${imagesrepertoire}" bundlekey="app.page.imagesrepertoire" defaultvalue="/composants/tagsapp/images/"/>              
<app:initvar attrname="_displayed" attrvalue="${displayed}" bundlekey="app.button.displayed" defaultvalue="true"/>              
<app:initvar attrname="_disabled" attrvalue="${disabled}" bundlekey="app.button.disabled" defaultvalue="false"/>              
<app:initvar attrname="_buttonclass" attrvalue="${buttonclass}" bundlekey="app.button.buttonclass" defaultvalue="submit"/>
<app:initvar attrname="_openwindow" attrvalue="${openwindow}" bundlekey="app.button.openwindow" defaultvalue="false"/>
<app:initvar attrname="_modal" attrvalue="${modal}" bundlekey="app.button.modal" defaultvalue="false"/>
<app:initvar attrname="_active" attrvalue="${active}" bundlekey="app.button.active" defaultvalue="true"/>
<app:initvar attrname="_waitingmsg" attrvalue="${waitingmsg}" bundlekey="app.button.waitingmsg" defaultvalue="false"/>

<app:initvar attrname="_w_name" attrvalue="${w_name}" bundlekey="app.button.w_name" defaultvalue=""/>
<app:initvar attrname="_w_width" attrvalue="${w_width}" bundlekey="app.button.w_width" defaultvalue=""/>
<app:initvar attrname="_w_height" attrvalue="${w_height}" bundlekey="app.button.w_height" defaultvalue=""/>
<app:initvar attrname="_w_resizable" attrvalue="${w_resizable}" bundlekey="app.button.w_resizable" defaultvalue="yes"/>
<app:initvar attrname="_w_scrollbars" attrvalue="${w_scrollbars}" bundlekey="app.button.w_scrollbars" defaultvalue="yes"/>
<app:initvar attrname="_w_toolbars" attrvalue="${w_toolbars}" bundlekey="app.button.w_toolbars" defaultvalue="no"/>
<app:initvar attrname="_w_location" attrvalue="${w_location}" bundlekey="app.button.w_location" defaultvalue="no"/>
<app:initvar attrname="_w_directories" attrvalue="${w_directories}" bundlekey="app.button.w_directories" defaultvalue="no"/>
<app:initvar attrname="_w_status" attrvalue="${w_status}" bundlekey="app.button.w_status" defaultvalue="yes"/>
<app:initvar attrname="_w_menubar" attrvalue="${w_menubar}" bundlekey="app.button.w_menubar" defaultvalue="no"/>
<app:initvar attrname="_w_copyhistory" attrvalue="${w_copyhistory}" bundlekey="app.button.w_copyhistory" defaultvalue="no"/>

<%@ include file="/META-INF/resources/WEB-INF/tags/app/responsive_test.tagf" %>


<%-- début du test d'évaluation du tag--%>
<c:if test="${_displayed == 'true' }">

<%-- La destination du bouton est transmise sous la forme action / transition du webflow--%>
<c:choose>
	<c:when test="${! empty action && ! empty transition}">
	  	<c:set var="_href" value="${pageContext.request.contextPath}/${action}?${FLOWEXECUTIONID_FIELD_NAME}=${FLOWEXECUTIONID_VALUE}&${EVENTID_FIELDURL_PREFIX}=${transition}"/>
	  	<%-- Un couple attribut / valeur complète l'URL--%>
	  	<c:if test="${! empty attribute}">
  			<c:set var="_href" value="${_href}&${attribute}=${value}"/>
  		</c:if>
	</c:when>
	<%-- L'indication du lien est transmise sous la forme d'une action --%>
	<c:when test="${! empty action &&  empty transition}">
     	<c:set var="_href" value="${pageContext.request.contextPath}/${action}"/>

  	    <%-- Un couple attribut / valeur complète l'URL--%>
  	    <c:if test="${! empty attribute}">
 			<c:set var="_href" value="${_href}&${attribute}=${value}"/>
	  	</c:if>
		</c:when>
	<%-- L'indication du lien est sous la forme d'une URL--%>
	<c:when test="${! empty uri}">
			<c:set var="_href" value="${uri}"/>
	</c:when>
</c:choose>

<!-- si le token csrf est disponible dans la page il sera ajouter à l'url construit car le filtre csrf vérifie désormais également
les requetes en GET -->
<c:if test="${! empty _csrf.parameterName}"><c:set var="_href" value="${_href}&${_csrf.parameterName}=${_csrf.token}"/></c:if>


<c:choose>
    <c:when test="${_openwindow == true && ! empty _href}">	  
	    <c:if test="${!empty _w_width}">
	    	<c:set var="_w_options" value="width=${_w_width}"/>
	    </c:if>
		    
	    <c:if test="${!empty _w_height}">
	    	<c:set var="_w_options" value="${_w_options},height=${_w_height}"/>
	    </c:if>
        		    
	    <c:set var="_w_options" value="${_w_options},resizable=${_w_resizable},scrollbars=${_w_scrollbars},toolbars=${_w_toolbars},location=${_w_location}"/>
		<c:set var="_w_options" value="${_w_options},directories=${_w_directories},status=${_w_status},menubar=${_w_menubar},copyhistory=${_w_copyhistory}"/>		    
        <c:choose>
            <c:when test="${_modal}">
                <c:if test="${!empty _w_width}">
		            <c:set var="_w_modal_options" value="dialogWidth=${_w_width}"/>
		        </c:if>            
		        <c:if test="${!empty _w_height}">
		            <c:set var="_w_modal_options" value="${_w_modal_options},dialogHeight=${_w_height}"/>
		        </c:if>
		        <c:set var="_w_options" value="${_w_options},modal=yes"/>
                <c:set var="_w_modal_options" value="${_w_modal_options},resizable=${_w_resizable},scroll=${_w_scrollbars},status=${_w_status}"/>
                <c:set var="_execute" value="send_url_openwindow_modal('${_href}','${_w_name}','${_w_options}','${_w_modal_options}')"/>
            </c:when>
            <c:otherwise>
	           <c:set var="_execute" value="send_url_openwindow('${_href}','${_w_name}','${_w_options}')"/>
           </c:otherwise>
        </c:choose>
	</c:when>
	<c:otherwise>
		    <c:set var="_execute" value="send_url('${_href}')"/>
	</c:otherwise>
</c:choose>

<html:button>
    	<jsp:attribute name="class">${_buttonclass}</jsp:attribute>
    	<jsp:attribute name="disabled">${_disabled}</jsp:attribute>
    	<jsp:attribute name="tabindex">${tabindex}</jsp:attribute>
    	<jsp:attribute name="onfocus">${onfocus}</jsp:attribute>
    	<jsp:attribute name="onblur">${onblur}</jsp:attribute>
    	<jsp:attribute name="onselect">${onselect}</jsp:attribute>
    	<jsp:attribute name="onchange">${onchange}</jsp:attribute>
		<jsp:attribute name="onkeypress">${_onkeypress};${_execute};</jsp:attribute> 
		<jsp:attribute name="onclick">${onclick};${_execute};</jsp:attribute> 
		<jsp:attribute name="ondblclick">${ondblclick};${_execute};</jsp:attribute> 	   			
		<jsp:attribute name="onmousedown">${onmousedown}</jsp:attribute>
		<jsp:attribute name="onmouseup">${onmouseup}</jsp:attribute>
		<jsp:attribute name="onmouseover">${onmouseover}</jsp:attribute>
		<jsp:attribute name="onmousemove">${onmousemove}</jsp:attribute>
		<jsp:attribute name="onmouseout">${onmouseout}</jsp:attribute>
		<jsp:attribute name="onkeydown">${onkeydown}</jsp:attribute>
		<jsp:attribute name="onkeyup">${onkeyup}</jsp:attribute> 
		<jsp:body>
			<jsp:doBody/>
		</jsp:body>
    </html:button>
	
<%-- fin du test d'évaluation du tag--%>
</c:if>