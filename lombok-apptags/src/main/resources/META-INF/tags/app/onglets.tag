<%@ tag dynamic-attributes="attributes" 
        isELIgnored="false" 
        pageEncoding="UTF-8"
        description="Insère un ensemble d'onglets"%>
        
<%@ include file="/META-INF/resources/WEB-INF/tags/html/includes.tagf" %>

<%--Attributs principaux --%>
<%@ attribute name="cleactive"
              description="Clé identifiant l'onglet actif."%>
<%@ attribute name="boxongletsclass"
              description="Classe du bloc div contenant les onglets.Par défaut : ongletcontainer."%>
<%@ attribute name="responsive"
              description="gestion du parametre responsive."%>              
<%@ attribute name="ongletclass"
              description="Classe css de la liste qui contient les onglets.Valeur par défaut = onglet."%>
<%@ attribute name="displayed"
              description="true : le tag est évalué, false : il est ignoré. Par défaut : true."%>              
<%-- Variables exposées --%>
<%@ variable name-given="_cleActive"  
             variable-class="java.lang.String" declare="true" 
             scope="NESTED" 
             description="Expose la clé de l'onglet actif." %>

<%-- Affectation de valeurs par défaut aux attributs --%>
<app:initvar attrname="_displayed" attrvalue="${displayed}" bundlekey="app.onglets.displayed" defaultvalue="true"/>            
<app:initvar attrname="_boxongletsclass" attrvalue="${boxongletsclass}" bundlekey="app.onglets.boxongletsclass" defaultvalue="ongletcontainer"/>
<app:initvar attrname="_ongletclass" attrvalue="${ongletclass}" bundlekey="app.onglets.ongletclass" defaultvalue="onglet"/>
<app:initvar attrname="_cleactive" attrvalue="${cleactive}" bundlekey="app.onglets.cleactive" defaultvalue="1"/>

<%@ include file="/META-INF/resources/WEB-INF/tags/app/responsive_test.tagf" %>

<%-- début du test d'évaluation du tag--%>
<c:if test="${_displayed == 'true' }">

<%-- Place dans le contexte 'request' la variable _formObjectName --%>
<c:if test="${! empty _cleactive}">  
  <c:set var="_cleActive" value="${_cleactive}" scope="request"/>
</c:if> 

<html:attributes var="attrString" attributeMap="${attributes}">


<c:choose>
        <c:when test="${ _responsive eq 'false'}">
	<div class="${_boxongletsclass}" ${attrString}>
		<ul class="${_ongletclass}" >
			<jsp:doBody/>
		</ul>
	</div>
       </c:when>
       <c:otherwise>
             <div class="${_boxongletsclass}" ${attrString}>
                    <jsp:doBody/>
            </div> 
       </c:otherwise>
</c:choose>
	
</html:attributes>

<%-- Supprime du contexte 'request' la variable _formObjectName --%>
<c:if test="${! empty _cleActive}">  
  <c:remove var="_cleActive" scope="request"/>
</c:if>

<%-- fin du test d'évaluation du tag--%>
</c:if>