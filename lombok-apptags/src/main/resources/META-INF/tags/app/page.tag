<%@ tag dynamic-attributes="attributes" 
        isELIgnored="false" 
        pageEncoding="UTF-8"
        description="Insère un squelette de page HTML composé de quatre blocs (div) principaux :
         un conteneur principal, un bandeau, un menu déroulant horizontal,un conteneur de données."%>
<%@ include file="/META-INF/resources/WEB-INF/tags/html/includes.tagf" %>

<%-- Attributs principaux --%>
<%@ attribute name="titreecran" 
              required="true"
              description="Titre de la fenêtre windows. Elément head->title" %>
<%@ attribute name="titrecontainer" 
              required="false" 
              description="Titre des données contenues dans la page."%>
<%@ attribute name="donneescontainer"
              description="false : n'insère pas de conteneur de données. Sinon, l'insère. Par défaut = true "%>
<%@ attribute name="responsive" 
              required="false" 
              description="indique si les elements de la page doivent etre responsive"%>

<%-- Paramétrage du bandeau --%>
<%@ attribute name="bandeau"
              description="Indique si le bandeau doit être inclus. Par défaut = true "%>
<%@ attribute name="bandeaujsp"
              description="Fichier JSP bandeau. Par défaut = /WEB-INF/tags/app/bandeau.jsp "%>
<%@ attribute name="titrebnd" 
              description="Texte de la partie centrale du bandeau. Par défaut = vide" %>
<%@ attribute name="titrebnd2" 
              description="Seconde ligne de texte de la partie centrale du bandeau. Par défaut = vide" %>

<%-- Paramétrage du pied de page --%>
<%@ attribute name="piedpage"
              description="Indique si le pied de page doit être inclus. Par défaut = true "%>
<%@ attribute name="piedpagejsp"
              description="Fichier JSP pied de page. Par défaut = /WEB-INF/tags/app/piedpage.jsp "%>
<%@ attribute name="titrepied" 
              description="Texte de la partie centrale du pied de page. Par défaut = vide" %>

<%-- Paramétrage des éléments externes --%>
<%@ attribute name="tagjavascript" 
              description="Fichier javascript utilisé par les tags de la librairie app. Par défaut = /composants/tagsapp/js/tagsapp.js" %>
<%@ attribute name="tagcss" 
              description="Feuille de style par les tags de la librairie app. Par défaut = /composants/tagsapp/css/communcp.css" %>
              <%@ attribute name="new_tagcss" 
              description="Feuille de style par les tags de la librairie app. Par défaut = /composants/tagsapp/css/new_communcp.css" %>
<%@ attribute name="imagesrepertoire" 
              description="Répertoire contenant les images utilisées par le tag. Par défaut = /composants/tagsapp/images/" %>
<%@ attribute name="includecssjs" 
              description="Jsp contenant l'import de fichiers javascripts et de feuilles de styles css. Par défaut = includecssjs.jsp" %>

<%-- Paramétrage du thème de présentation css --%>
<%@ attribute name="theme"
              description="Thème de présentation. Pardéfaut = container "%>

<%-- Paramétrage de l'élément body --%>
<%@ attribute name="donneescontainerboxclass" 
              description="Classe du bloc div placé dans l'élément body et contenant l'ensemble des autres composants de la page. Par défaut = donneescontainer" %>
<%@ attribute name="onloadbody"
              description="Script exécuté lors du chargement de l'élément body. Par défaut = initbody()." %>

<%-- Paramétrage du blocage du clic droit --%>
<%@ attribute name="sansclicdroit"
              description="Indique si le clic droit est bloqué. Par défaut = false "%>

<%-- Paramétrage du theme --%>
<app:initvar attrname="_theme" attrvalue="${theme}" bundlekey="app.page.theme" defaultvalue="container" />
<app:initvar attrname="_theme_rwd" attrvalue="${theme_rwd}" bundlekey="app.page.rwd.theme" defaultvalue=" container-fluid" />

<%-- Paramétrage du titre des données --%>
<%@ attribute name="titredonneesboxclass" 
              description="id du bloc div contenant le titre associé au container. Par défaut = titredonneescontainer" %>
<%@ attribute name="titredonneesclass" 
              description="Classe appliquée au paragraphe du titre du container. Par défaut = titrepage" %>

<%-- Paramétrage du menu --%>              
<%@ attribute name="menu"
              description="false : n'affiche pas le menu. Sinon, l'affiche. Par défaut = true "%>
<%@ attribute name="menunew"
              description="false : n'affiche pas le nouveau menu. Sinon, l'affiche. Par défaut = true "%>
              
<%@ attribute name="menunewvertical"
              description="false : laisse le menu à l'horizontal. Sinon, affiche le menu verticalement. Par défaut = false "%>

<%@ attribute name="menurepertoire"
              description="Répertoire placé sous webapp qui contient les éléments statiques du menu (js, css, images). Par défaut =/jscookmenu" %> 
<%@ attribute name="menutheme"
              description="Thème utilisé par JScookmenu. Par défaut = ThemeOffice" %>
<%@ attribute name="menudiv"
              description="id du bloc div qui sert à déclencher l'affichage du menu.  Par défaut = menu." %>                
<%@ attribute name="utiliserfavicon" description="true: affiche l'icone application/img/favicon.ico Par défaut = false" %>

<%--Decoration spring js --%>
<%@ attribute name="decorationnew"
              description="false : n'affiche pas la decoration springjs. Sinon, l'affiche. Par défaut = false "%>
              
<%-- Gestion de la transparence des images PNG sous IE --%>
<%@ attribute name="transparencepngie"
              description="true : gère la transparence des images PNG sous IE. Sinon, gestion désactivée. Par défaut = false "%>

<%-- Variables exposées --%>
<%@ variable name-given="_elementId"  
             variable-class="java.lang.Long" declare="true" 
             scope="NESTED" 
             description="Expose l'index du dernier ID utilisé." %>

<%-- Place dans le contexte 'request' la variable _elementId --%>
<c:set var="_elementId" value="1" scope="request"/>

<%-- Initialisation des paramètres par défaut --%>

<app:initvar attrname="_donneescontainer" attrvalue="${donneescontainer}" bundlekey="app.page.donneescontainer" defaultvalue="true"/>
<app:initvar attrname="_donneescontainerboxclass" attrvalue="${donneescontainerboxclass}" bundlekey="app.page.donneescontainerboxclass" defaultvalue="donneescontainer"/>
<app:initvar attrname="_imagesrepertoire" attrvalue="${imagesrepertoire}" bundlekey="app.page.imagesrepertoire" defaultvalue="/composants/tagsapp/images"/>
<app:initvar attrname="_includecssjs" attrvalue="${includecssjs}" bundlekey="app.page.includecssjs" defaultvalue="/composants/tagsapp/images"/>
<app:initvar attrname="_imagesapplicationrepertoire" attrvalue="${imagesapplicationrepertoire}" bundlekey="app.page.imagesapplicationrepertoire" defaultvalue="/application/img"/>
<app:initvar attrname="_transparencepngie" attrvalue="${transparencepngie}" bundlekey="app.page.transparencepngie" defaultvalue="false"/>
<app:initvar attrname="_composantsrepertoire" attrvalue="${composantsrepertoire}" bundlekey="app.page.composantsrepertoire" defaultvalue="composants" />
<app:initvar attrname="_appnouvelcharte" attrvalue="${appnouvelcharte}" bundlekey="app.nouvel.charte" defaultvalue="false"/>


<%-- Body --%>
<app:initvar attrname="_onloadbody" attrvalue="${onloadbody}" bundlekey="app.page.onloadbody" defaultvalue="initbody()"/>
<app:initvar attrname="_titredonneesboxclass" attrvalue="${titredonneesboxclass}" bundlekey="app.page.titredonneesboxclass" defaultvalue="titredonneescontainer"/>
<app:initvar attrname="_titredonneesclass" attrvalue="${titredonneesclass}" bundlekey="app.page.titredonneesclass" defaultvalue="titrepage"/>

<%-- Bandeau --%>
<app:initvar attrname="_bandeaujsp" attrvalue="${bandeaujsp}" bundlekey="app.page.bandeaujsp" defaultvalue="/WEB-INF/tags/app/bandeau.jsp"/>
<app:initvar attrname="_bandeau" attrvalue="${bandeau}" bundlekey="app.page.bandeau" defaultvalue="true"/>

<%-- Pied de page --%>
<app:initvar attrname="_piedpagejsp" attrvalue="${piedpagejsp}" bundlekey="app.page.piedpagejsp" defaultvalue="/WEB-INF/tags/app/piedpage.jsp"/>
<app:initvar attrname="_piedpage" attrvalue="${piedpage}" bundlekey="app.page.piedpage" defaultvalue="false"/>
<app:initvar attrname="_titrepied" attrvalue="${titrepied}" bundlekey="app.page.titrepied" defaultvalue="" />

<%-- Menu --%>
<app:initvar attrname="_menu" attrvalue="${menu}" bundlekey="app.page.menu" defaultvalue="true"/>
<app:initvar attrname="_menunew" attrvalue="${menunew}" bundlekey="app.page.menu.new" defaultvalue="true"/>
<app:initvar attrname="_menunew_vertical" attrvalue="${menunew_vertical}" bundlekey="app.page.menu.new.vertical" defaultvalue="true"/>



<app:initvar attrname="_menurepertoire" attrvalue="${menurepertoire}" bundlekey="app.page.menurepertoire" defaultvalue="/composants/jscookmenu"/>
<app:initvar attrname="_menutheme" attrvalue="${menutheme}" bundlekey="app.page.menutheme" defaultvalue="ThemeOffice"/>
<app:initvar attrname="_menudiv" attrvalue="${menudiv}" bundlekey="app.page.menudiv" defaultvalue="menu"/>

<%-- Decoration --%>
<app:initvar attrname="_decorationnew" attrvalue="${decorationnew}" bundlekey="app.page.decoration.new" defaultvalue="true"/>
<%-- Favicon --%>
<app:initvar attrname="_favicon" attrvalue="${utiliserfavicon}" bundlekey="app.page.favicon" defaultvalue="false"/>

<%-- Blocage du clic droit --%>
<app:initvar attrname="_sansclicdroit" attrvalue="${sansclicdroit}" bundlekey="app.page.sansclicdroit" defaultvalue="false" />

<%@ include file="/META-INF/resources/WEB-INF/tags/app/responsive_test.tagf" %>

<jsp:useBean id="utilisateurSecurityLombokContainer" scope="request"  class="fr.gouv.finances.lombok.securite.springsecurity.UtilisateurSecurityLombokContainer" > </jsp:useBean>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr-FR" lang="fr-FR">

<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <c:if test="${_favicon eq 'true'}">
        <link rel="shortcut icon" type="image/png" href="<c:url value='${_imagesapplicationrepertoire}/favicon.ico'/>" />
    </c:if> 
    <script type="text/javascript">
            /*<![CDATA[*/
            history.forward();
            <c:if test="${_menu ne 'false'}">
                var my${_menutheme}Base='<c:url value="${_menurepertoire}/${_menutheme}/" />';
            </c:if>
            /*]]>*/
            
            var composantsrepertoire =  "<c:url value="/${_composantsrepertoire}/"/>";
    </script>  
    
<%-- Import de fichiers javascripts et css --%>
<c:if test="${! empty _includecssjs}">
    <jsp:include page="${_includecssjs}">
            <jsp:param name="menu" value="${_menu}"/>
        <jsp:param name="responsive" value="${_responsive}" /> 
    </jsp:include>
</c:if>

<c:if test="${_menunew ne 'false'}">
    <c:if test="${not empty utilisateurSecurityLombokContainer.utilisateurSecurityLombok.generationmenuhtml}">
        <c:choose>
            <c:when test="${_responsive eq 'false'}">
                <link rel="stylesheet" type="text/css" href="<c:url value="/${_composantsrepertoire}/tagsapp/css/new/menu.css"/>" />      
            </c:when>
            <c:otherwise>
                <link rel="stylesheet" type="text/css" href="<c:url value="/${_composantsrepertoire}/tagsapp/css/rwd/menu_rwd.css"/>" />
            </c:otherwise>
       </c:choose>  
    </c:if>
</c:if>

<%-- Import du fichier css pour compatibilité bootstrap absolument apres tous les css --%>
<c:if test="${_responsive eq 'true'}">
    <link rel="stylesheet" type="text/css" href="<c:url value="/${_composantsrepertoire}/tagsapp/css/rwd/bootstrap_add.css"/>" />
</c:if>

<c:if test="${_decorationnew ne 'false'}">
    <script type="text/javascript" src="<c:url value="/resources/dojo/dojo.js" />"></script>
 <script type="text/javascript" src="<c:url value="/resources/spring/Spring.js" />"></script> 
<script type="text/javascript" src="<c:url value="/resources/spring/Spring-Dojo.js" />"></script> 
<link type="text/css" rel="stylesheet" href="<c:url value="/resources/dijit/themes/claro/claro.css" />"/> 
<link type="text/css" rel="stylesheet" href="<c:url value="/resources/dojo/resources/dojo.css"/>"/> 
        
    
</c:if>

<c:if test="${_appnouvelcharte ne 'true' }">
    <link rel="stylesheet" href="<c:url value="/${_composantsrepertoire}/"/>tagsapp/css/new/pagination.css">
    <link rel="stylesheet" href="<c:url value="/${_composantsrepertoire}/"/>tagsapp/css/new/icon.css">
</c:if>
 

<c:choose>
            <c:when test="${_responsive eq 'false'}">   
                <!--[if lt IE 8]>
                <link rel="stylesheet" href="<c:url value="/${_composantsrepertoire}/"/>tagsapp/css/new/iefix.css?v2">
                <![endif]-->
            </c:when> 
            <c:otherwise>
               <!--[if lt IE 8]>
                <link rel="stylesheet" href="<c:url value="/${_composantsrepertoire}/"/>tagsapp/css/new/iefix.css?v2">
                <script type="text/javascript" src="<c:url value="/${_composantsrepertoire}"/>/tagsapp/js/rwd/respond.min.js"></script>
                <![endif]-->
            </c:otherwise>
</c:choose>  


<%--<link href="<c:url value="/${_composantsrepertoire}/"/>tagsapp/css/themes/copernic-jaune.css" type="text/css" rel="Stylesheet"/> --%>

    <title>${titreecran}</title>
    
      <security:csrfMetaTags/>
    
</head>

<!-- Conditionnal comments pour gérer le CSS. séparation oncontext et onload --> 
<c:choose>
<c:when test="${_sansclicdroit eq 'true'}">
<!--[if lt IE 9]>  <body class="ltie9" oncontextmenu="return false;">  <![endif]-->
<!--[if IE 9]>     <body class="ie9 gteie9" oncontextmenu="return false;">  <![endif]-->
<!--[if gt IE 9]>  <body class="gteie9 gtie9" oncontextmenu="return false;"> <![endif]-->
<!--[if !IE]><!--> <body class="gteie9 noie" oncontextmenu="return false;">  <!--<![endif]-->
</c:when>
<c:otherwise>
<!--[if lt IE 9]>  <body class="ltie9" onLoad="${_onloadbody}">  <![endif]-->
<!--[if IE 9]>     <body class="ie9 gteie9" onLoad="${_onloadbody}">  <![endif]-->
<!--[if gt IE 9]>  <body class="gteie9 gtie9" onLoad="${_onloadbody}"> <![endif]-->
<!--[if !IE]><!--> <body class="gteie9 noie" onLoad="${_onloadbody}">  <!--<![endif]-->
  
</c:otherwise>
</c:choose>
<noscript><p>Javascript désactivé. Application inutilisable</p></noscript>

<!-- commentaires conditionnels pour connaitre la version d'internet explorer  -->
    <!--[if lt IE 7]>      <span class="ie-version no-js lt-ie9 lt-ie8 lt-ie7"></span> <![endif]-->
    <!--[if IE 7]>         <span class="ie-version no-js lt-ie9 lt-ie8"></span> <![endif]-->
    <!--[if IE 8]>         <span class="ie-version no-js lt-ie9"></span> <![endif]-->
    <!--[if gt IE 8]><!--> <div class="ie-version no-js"></div> <!--<![endif]-->  
<div style="display:none;">
    <p id="appli-version"><fmt:message key="appli.version" /></p>
    <p id="appli-libellelong"><fmt:message key="appli.libelle-long"/></p>                
    <p id="personneannuaire-cn">Nom : ${utilisateurSecurityLombokContainer.utilisateurSecurityLombok.personneAnnuaire.cn}</p>   
    <p id="personneannuaire-fonction">Fonction : ${utilisateurSecurityLombokContainer.utilisateurSecurityLombok.personneAnnuaire.fonction}</p>  
    <p id="expirationhabilitation">Expiration habilitation : <fmt:formatDate pattern="dd/MM/yyyy" value="${utilisateurSecurityLombokContainer.utilisateurSecurityLombok.personneAnnuaire.dateExpirationDerniereHabilitation}"/> </p>    
</div>


<c:if test="${_responsive eq 'false'}">
        <div id="container" class="${_theme} container">
</c:if>
    

<c:if test="${_responsive eq 'true'}">
    <header>
    <div class="${_theme_rwd} headercontainer">
</c:if>

<%-- Affichage du bandeau --%>
<c:if test="${_bandeau eq 'true'}">
    <jsp:include page="${_bandeaujsp}">
    <jsp:param name="titrebandeau" value="${titrebnd}"/>
    <jsp:param name="titrebandeau2" value="${titrebnd2}"/>
    <jsp:param name="titreecran" value="${titreecran}"/>
    <jsp:param name="titrecontainer" value="${titrecontainer}"/>
        <jsp:param name="responsive" value="${_responsive}" />
    </jsp:include>
</c:if>
    
    <%-- Affiche le menu si nécessaire --%>
    <c:choose>
        <c:when test="${_menu ne 'false'}">
        <div  class="${_menudiv} <c:if test="${_menunew eq 'true'}"> hidden </c:if>">   <!--  si le nouveau menu est affiché : on cache les div de l'ancien menu -->     
           <div id="${_menudiv}" style="position: float;"></div>
        </div>
           
          
                <script type="text/javascript">                        
                 ${utilisateurSecurityLombokContainer.utilisateurSecurityLombok.generationmenu} 
                
                 var _menudiv = '${_menudiv}';
                 var cm_menutheme = cm${_menutheme};
                 var _menutheme = '${_menutheme}';
                 
                 <c:if test="${_menunew ne 'true'}">
                     if (typeof barremenu != 'undefined'){
                        cmDraw (_menudiv, barremenu , 'hbr', cm_menutheme, _menutheme);
                    }
                 </c:if>
                
                 var cookieDoShowMessagePrompt = 'showMessagePrompt';
                 var cookieSessionEndName = 'sessionEndTimeStamp';
                 var cookieSessionTimeForWarningName = 'sessionTimeWarning';

                 function isCookieHere(name) {
                	 return document.cookie.indexOf(name) !== -1;
                 }
                 
                 function getCookieValue(name)
                 {
                     var name = name + "=";
                     var ca = document.cookie.split(';');
                     for(var i=0; i<ca.length; i++)
                     {
                         var c = ca[i].trim();
                         if (c.indexOf(name)==0) return c.substring(name.length,c.length);
                     }
                     return "";
                 }
                 
                 function rewriteCookie(name, newValue) {
                	 
                	function createCookie(name,value,days) {
            		    if (days) {
            		        var date = new Date();
            		        date.setTime(date.getTime()+(days*24*60*60*1000));
            		        var expires = "; expires="+date.toGMTString();
            		    }
            		    else var expires = "";
            		    document.cookie = name+"="+value+expires+"; path=/";
            		}

            		function eraseCookie(name) {
            		    createCookie(name,"",-1);
            		}
            		
            		eraseCookie(name);
            		createCookie(name, newValue, 10);
            		
                 }
                 
                 function setSessionPrompt() {
                	 if(getCookieValue(cookieDoShowMessagePrompt)) {
                		 if(isCookieHere(cookieSessionEndName) && isCookieHere(cookieSessionTimeForWarningName)
                				 && new Date(parseInt(getCookieValue(cookieSessionEndName))).getTime() > new Date().getTime()) {
                             var remainingTime = new Date(parseInt(getCookieValue(cookieSessionEndName))).getTime() - (parseInt(getCookieValue(cookieSessionTimeForWarningName))*60*1000) - new Date().getTime();
                             window.setTimeout(function() {
                                 if(confirm("Votre session se termine dans " + getCookieValue(cookieSessionTimeForWarningName) + " minutes. Voulez-vous continuer sur cette session (annuler), ou vous reconnecter pour renouveler votre délai (valider) ?")) {
                                	 window.location="${pageContext.request.contextPath}/j_spring_security_logout";
                                 } else {
                                     rewriteCookie(cookieDoShowMessagePrompt, false);
                                 }
                             }, remainingTime);
                         } 
                	 }
                 }

                 setSessionPrompt();
               
                </script>
            
        </c:when>
    </c:choose>    

    
    <!-- menu new = ${_menunew} -->
	<c:if test="${_menu ne 'false'}">
		<c:if test="${_menunew ne 'false'}">
        <c:if test="${not empty utilisateurSecurityLombokContainer.utilisateurSecurityLombok.generationmenuhtml}">

            <c:choose>
                <c:when test="${_responsive eq 'true'}">

                    <div id="header_menu">

                        <div class="toggle_header_menu">
                            <p class="navbar-menu-left">Menu</p>
                            <button type="button" class="button_menu-toggle" aria-expanded="false" aria-controls="navbar">
                                <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>

                        <div class="navbar_clear"></div>

                        <c:if test="${_menunew_vertical eq 'true'}"> class='vertical' </c:if>
                        <div class='nav menu header_nav_menu navbar-nav'>
                            ${utilisateurSecurityLombokContainer.utilisateurSecurityLombok.generationmenuhtml}</div>
                        </div>

                </c:when>
                <c:otherwise>

                    <c:if test="${_menunew_vertical eq 'true'}"> class='vertical' </c:if>
                        <div id="navigationMenu" ></div>
                        <nav role="navigation" tabindex="0">
					<div class='nav menu'>
						${utilisateurSecurityLombokContainer.utilisateurSecurityLombok.generationmenuhtml}

					</div>
				</nav>

                </c:otherwise>
            </c:choose>

				<c:if test="${_appnouvelcharte ne 'true' }">
					<br />
					<br />
				</c:if>

			</c:if>
		</c:if>
	</c:if>

<c:if test="${_responsive eq 'true'}">
    </div>
    </header>
</c:if>

	<%-- Insère le conteneur de données --%>
    <c:if test="${_donneescontainer ne 'false'}">
        <c:choose>
            <c:when test="${_responsive eq 'false'}">
                <main role="main" id="mainContent" tabindex="0">
                    <div id="donneesbox" class="${_donneescontainerboxclass}">
            </c:when>
            <c:otherwise>
                <%-- <main> semantique HTML5 specification W3C , et au moins h1 est obligatoire --%>
                <main role="main" id="mainContent" tabindex="0">
                    <c:choose>
                        <c:when test="${! empty titrecontainer}">
                            <h1>${titrecontainer}</h1>
                        </c:when>
                        <c:otherwise>
                            <h1>${titreecran}</h1>
                        </c:otherwise>
                    </c:choose>
                    <div class="${_theme_rwd} donneescontainer">
            </c:otherwise>
        </c:choose>
    </c:if>    
    
    <%-- Affiche le titre du bloc de données si non vide--%>
    <c:if test="${! empty titrecontainer}">
        <div class="${_titredonneesboxclass}">
            <h1 class="${_titredonneesclass}">${titrecontainer}</h1>
        </div>
    </c:if>      
    
    <%-- Exécution du corps du tag --%>
    <jsp:doBody/>
    <%-- Ferme le conteneur de données --%>
    <c:if test="${_donneescontainer ne 'false'}">
    </div>
    </c:if>

<c:if test="${_responsive eq 'true'}">
</div>
    </main>
     <footer role="contentinfo"  id="footerContent"  tabindex="0"> 
     <div class="${_theme_rwd} footercontainer">
</c:if>

<%-- Affichage du pied de page --%>
<c:if test="${_piedpage eq 'true'}">
    <jsp:include page="${_piedpagejsp}">
    <jsp:param name="titrepiedpage" value="${_titrepied}"/>
        <jsp:param name="responsive" value="${_responsive}" />
    </jsp:include>
</c:if> 

<c:if test="${_responsive eq 'true'}">
     </div>
     </footer>
</c:if>

</body>
</html>