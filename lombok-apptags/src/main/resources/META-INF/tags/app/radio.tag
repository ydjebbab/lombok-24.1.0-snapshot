<%@ tag dynamic-attributes="attributes" isELIgnored="false"
	pageEncoding="UTF-8" description="Insère un groupe de boutons radios"%>

<%@ include file="/META-INF/resources/WEB-INF/tags/html/includes.tagf" %>

<%--Attributs principaux --%>
<%@ attribute name="attribut" required="true"
	description="Nom de l'attribut associé au champ de saisie dans l'objet support du formulaire. Utilisé dans le spring bind"%>
<%@ attribute name="readonly"
	description="true : champs en lecture seule"%>
<%@ attribute name="libelle" required="false"
	description="Label associé à l'ensemble des boutons radios (élément legend)"%>
<%@ attribute name="itemslist" type="java.lang.Object"
	description="Bean de type List contenant les éléments du groupe de boutons radios."%>
<%@ attribute name="itemsset" type="java.lang.Object"
	description="Bean de type set contenant les éléments du groupe de boutons radios."%>
<%@ attribute name="itemsmap" type="java.lang.Object"
	description="Bean de type Map contenant les éléments du groupe de boutons radios."%>
<%@ attribute name="label"
	description="Nom de la propriété d'un élément de la collection (items...) affichée dans le bouton radio."%>
<%@ attribute name="value"
	description="Nom de la propriété d'un élément de la collection (items...) utilisé comme valeur du bouton radio."%>
<%@ attribute name="ignorenullnestedpath"
	description="true : Ignore les références nulles lors de l'accès à un attribut.  Valeur par défaut = true "%>
<%@ attribute name="displayed"
	description="true: le tag est évalué. Par défaut : true"%>
<%@ attribute name="affichelibelle"
	description="Affichage du libellé associé au champ de saisie. Par défaut : true"%>

<%--Attributs optionnels --%>
<%@ attribute name="requis"
	description="true : champs obligatoire.  Valeur par défaut = true "%>
<%@ attribute name="consigne"
	description="Correspond à l'attribut 'title' de l'élément label. Peut contenir, par exemple, une consigne de saisie du champs. "%>

<%--Attributs de présentation --%>
<%@ attribute name="theme"
	description="Thème de présentation. Par défaut = V"%>
<%@ attribute name="boxheight"
	description="Hauteur du bloc div contenant l'ensemble des éléments relatifs à un champs de saisie."%>
<%@ attribute name="boxwidth"
	description="Largeur du bloc div contenant l'ensemble des éléments relatifs à un champs de saisie."%>
<%@ attribute name="labelboxwidth"
	description="Largeur du bloc div contenant le libelle (en % de la largeur totale du bloc parent)"%>
<%@ attribute name="inputboxwidth"
	description="Largeur du bloc div contenant le champ de saisie (en % de la largeur totale du bloc parent)"%>
<%@ attribute name="compboxwidth"
	description="Largeur du bloc div contenant la zone complémentaire (en % de la largeur totale du bloc parent)"%>

<%@ attribute name="normalboxclass"
	description="Classe css du bloc div contenant l'ensemble des éléments relatifs aux boutons radios. Valeur par défaut = normal "%>
<%@ attribute name="lectureboxclass"
	description="Classe css du bloc div contenant l'ensemble des éléments relatifs aux boutons radios en lecture seule. Valeur par défaut = lectureseule."%>
<%@ attribute name="erreurboxclass"
	description="Classe css du bloc div contenant l'ensemble des éléments relatifs aux boutons radios, lorsque le champs est en erreur. Valeur par défaut = erreur."%>
<%@ attribute name="requisboxclass"
	description="Classe css du bloc div contenant l'ensemble des éléments relatifs aux boutons radios, lorsque le champs de saisie est obligatoire. Valeur par défaut = requis."%>


<%@ attribute name="labelclass"
	description="Classe css de l'élément label. Valeur par défaut = lblGrpRadioChk"%>
<%@ attribute name="labelboxclass"
	description="Classe css du bloc div contenant le libellé.Valeur par défaut = boxlblGrpRadioChk."%>
<%@ attribute name="inputboxclass"
	description="Classe css du bloc div contenant le champs de saisie.Valeur par défaut = boxGrpRadioChk."%>
<%@ attribute name="compboxclass"
	description="Classe css du bloc div contenant des données complémentaires sur le champs de saisie.Valeur par défaut = boxCompRadioChk."%>
<%@ attribute name="radioclass"
	description="Classe css du champs input de type radio. Valeur par défaut = radio."%>
<%@ attribute name="labelradioclass"
	description="Classe css du label associé à chaque champs input de type 'radio'. Valeur par défaut = lblRadioChk"%>
<%@ attribute name="responsive" description="gestion du parametre responsive."%>
<%@ attribute name="bootstrapclass" description="classes bootstrap de radio"%>
<%@ attribute name="bootstraplblclass" description="classes bootstrap du label"%>
<%@ attribute name="bootstrapgrpclass" description="classes bootstrap du groupe"%>

<%-- Attributs  --%>
<%@ attribute name="tabindex"
	description="Numéro indiquant l'ordre d'accès par tabulation."%>
<%@ attribute name="accesskey"
	description="Touche de raccourci pour accéder à un champs."%>
<%@ attribute name="onfocus"
	description="Script exécuté lors que l'élément est accédé."%>
<%@ attribute name="onblur"
	description="Script exécuté lors que l'élément est quitté."%>
<%@ attribute name="onselect"
	description="Script exécuté lorsqu'une portion de texte est sélectionnée."%>
<%@ attribute name="onchange"
	description="Script exécuté lors que l'élément est quitté et que son contenu a été modifié."%>

<%-- Attributs événements intrinsèques HTML --%>
<%@ attribute name="onclick"
	description="Script exécuté quand le bouton du dispositif de pointage est cliqué au-dessus d'un élément."%>
<%@ attribute name="ondblclick"
	description="Script exécuté quand le bouton du dispositif de pointage est double-cliqué au-dessus d'un élément."%>
<%@ attribute name="onmousedown"
	description="Script exécuté quand le bouton du dispositif de pointage est appuyé au-dessus d'un élément."%>
<%@ attribute name="onmouseup"
	description="Script exécuté quand le bouton du dispositif de pointage est relaché au-dessus d'un élément."%>
<%@ attribute name="onmouseover"
	description="Script exécuté quand le bouton du dispositif de pointage est déplacé sur un élément."%>
<%@ attribute name="onmousemove"
	description="Script exécuté quand le bouton du dispositif de pointage est déplacé alors qu'il est au-dessus d'un élément."%>
<%@ attribute name="onmouseout"
	description="Script exécuté quand le bouton du dispositif de pointage est déplacé en dehors d'un élément."%>
<%@ attribute name="onkeypress"
	description="Script exécuté quand une touche est pressée puis relachée au-dessus d'un élément."%>
<%@ attribute name="onkeydown"
	description="Script exécuté quand une touche est gardée appuyée au-dessus d'un élément."%>
<%@ attribute name="onkeyup"
	description="Script exécuté quand une touche est relachée au-dessus d'un élément."%>

<%@ attribute name="id"
	description="id de l'élément input. Par défaut : attribué automatiquement."%>

<%--Affectation de valeurs par défaut aux attributs--%>
<app:initvar attrname="_displayed" attrvalue="${displayed}"
	bundlekey="app.radio.displayed" defaultvalue="true" />
<app:initvar attrname="_ignorenullnestedpath"
	attrvalue="${ignorenullnestedpath}"
	bundlekey="app.radio.ignorenullnestedpath" defaultvalue="true" />
<app:initvar attrname="_readonly" attrvalue="${readonly}"
	bundlekey="app.radio.readonly" defaultvalue="false" />
<app:initvar attrname="_requis" attrvalue="${requis}"
	bundlekey="app.radio.requis" defaultvalue="false" />
<app:initvar attrname="_onkeypress" attrvalue="${onkeypress}"
	bundlekey="app.radio.onkeypress"
	defaultvalue="return submitEnterButton(event)" />
<app:initvar attrname="_requisindicateur"
	attrvalue="${requisindicateur}" bundlekey="app.radio.requisindicateur"
	defaultvalue=" *" />
<app:initvar attrname="_normalindicateur"
	attrvalue="${normalindicateur}" bundlekey="app.radio.normalindicateur"
	defaultvalue="" />
<app:initvar attrname="_affichelibelle" attrvalue="${affichelibelle}"
	bundlekey="app.radio.affichelibelle" defaultvalue="true" />

<app:initvar attrname="_theme" attrvalue="${theme}"
	bundlekey="app.radio.theme" defaultvalue="V" />
<app:initvar attrname="_boxheight" attrvalue="${boxheight}"
	bundlekey="app.radio.boxheight" defaultvalue="20px" />
<app:initvar attrname="_boxwidth" attrvalue="${boxwidth}"
	bundlekey="app.radio.boxwidth" defaultvalue="100%" />
<app:initvar attrname="_labelboxwidth" attrvalue="${labelboxwidth}"
	bundlekey="app.radio.labelboxwidth" defaultvalue="35%" />
<app:initvar attrname="_inputboxwidth" attrvalue="${inputboxwidth}"
	bundlekey="app.radio.inputboxwidth" defaultvalue="50%" />
<app:initvar attrname="_compboxwidth" attrvalue="${compboxwidth}"
	bundlekey="app.radio.compboxwidth" defaultvalue="50%" />

<app:initvar attrname="_normalboxclass" attrvalue="${normalboxclass}"
	bundlekey="app.radio.normalboxclass" defaultvalue="normal" />
<app:initvar attrname="_lectureboxclass" attrvalue="${lectureboxclass}"
	bundlekey="app.radio.lectureboxclass" defaultvalue="lecture" />
<app:initvar attrname="_erreurboxclass" attrvalue="${erreurboxclass}"
	bundlekey="app.radio.erreurboxclass" defaultvalue="erreur" />
<app:initvar attrname="_requisboxclass" attrvalue="${requisboxclass}"
	bundlekey="app.radio.requisboxclass" defaultvalue="requis" />

<app:initvar attrname="_labelboxclass" attrvalue="${labelboxclass}"
	bundlekey="app.radio.labelboxclass" defaultvalue="boxlblGrpRadioChk" />
<app:initvar attrname="_inputboxclass" attrvalue="${inputboxclass}"
	bundlekey="app.radio.inputboxclass" defaultvalue="boxGrpRadioChk" />
<app:initvar attrname="_compboxclass" attrvalue="${compboxclass}"
	bundlekey="app.radio.compboxclass" defaultvalue="boxCompRadioChk" />
<app:initvar attrname="_labelradioclass" attrvalue="${labelradioclass}"
	bundlekey="app.radio.labelradioclass" defaultvalue="lblRadioChk" />
<app:initvar attrname="_radioclass" attrvalue="${radioclass}"
	bundlekey="app.radio.radioclass" defaultvalue="radioclass" />
<app:initvar attrname="_labelclass" attrvalue="${labelclass}"
	bundlekey="app.radio.labelclass" defaultvalue="lblGrpRadioChk" />
    
<app:initvar attrname="_bootstrapclass" attrvalue="${bootstrapclass}" 
    bundlekey="app.page.rwd.bootstrapclass"  defaultvalue="fullwidth" />
<app:initvar attrname="_bootstraplblclass" attrvalue="${bootstraplblclass}" 
    bundlekey="app.page.rwd.bootstraplblclass" defaultvalue="btmodel_quarter_to_half" />
<app:initvar attrname="_bootstrapgrpclass" attrvalue="${bootstrapgrpclass}" 
    bundlekey="app.page.rwd.bootstrapgrpclass" defaultvalue="btmodel_quarter_to_half" />

<app:initvar attrname="_imagesrepertoire"
	attrvalue="${imagesrepertoire}" bundlekey="app.page.imagesrepertoire"
	defaultvalue="/composants/tagsapp/images" />

<%@ include file="/META-INF/resources/WEB-INF/tags/app/responsive_test.tagf" %>

<%-- début du test d'évaluation du tag--%>
<c:if test="${_displayed == 'true' }">

	<%-- Utilisation de la variable _formObjectName exposée par le tag app:form --%>
	<c:if test="${!empty  _formObjectName}">
		<c:set var="_path" value="${_formObjectName}.${attribut}" />
	</c:if>

	<%-- Utilisation de la variable _formReadOnly exposée par le tag app:form --%>
	<c:if test="${!empty  _formReadOnly}">
		<c:set var="_readonly" value="true" />
	</c:if>

	<%-- Détermination de la racine de l'ID des boutons radios --%>
	<app:id attrname="_id" id="${id}"></app:id>

	<%-- cptid : compteur pour créer un attribut id unique pour chaque bouton radio--%>
	<c:set var="cptid" value="0" />

    <%@ include file="/META-INF/resources/WEB-INF/tags/app/fillstatus_error.tagf" %>

	<%-- Remplit la variable status_warning si nécessaire --%>
	<%@ include file="/META-INF/resources/WEB-INF/tags/app/fillstatus_warning.tagf" %>

	<c:if test="${empty libelle}">
		<fmt:message key="${attribut}" var="libelle" />
		<str:countMatches substring="???${attribut}???"
			var="existInMessagesProperties">${libelle}</str:countMatches>
		<c:if test="${existInMessagesProperties == 1}">
			<c:set var="libelle" value="" />
		</c:if>
	</c:if>

	<%-- Sélection de la classe du bloc div --%>
	<c:choose>
		<c:when test="${status_error || status_warning}">
			<c:set var="_divclass" value="${_theme}${_erreurboxclass}" />
			<c:if test="${empty _firsterrorid}">
				<c:set var="_firsterrorid" value="${_id}" scope="request" />
			</c:if>
		</c:when>
		<c:when test="${_readonly eq 'true'}">
			<c:set var="_divclass" value="${_theme}${_lectureboxclass}" />
		</c:when>
		<c:when test="${_requis eq 'true'}">
			<c:set var="_divclass" value="${_theme}${_requisboxclass}" />
		</c:when>
		<c:otherwise>
			<c:set var="_divclass" value="${_theme}${_normalboxclass}" />
		</c:otherwise>
	</c:choose>

	<%-- Ajout d'un indicateur pour les champs obligatoires --%>
	<c:choose>
		<c:when test="${_requis eq 'true'}">
			<c:set var="libelle" value="${libelle}${_requisindicateur}" />
		</c:when>
		<c:otherwise>
			<c:set var="libelle" value="${libelle}${_normalindicateur}" />
		</c:otherwise>
	</c:choose>

	<%-- Utilisation du hack "_" pour contourner l'absence de propriété min-height sous IE --%>

    <c:choose>
        <c:when test="${ _responsive eq 'false'}">
            <div class="${_divclass}" style="width: ${_boxwidth};height: auto;_height: ${_boxheight};min-height: ${_boxheight};">
        </c:when>
        <c:otherwise>
            <div class="${_divclass} form-group ${_bootstrapclass}">
        </c:otherwise>
    </c:choose>

		<%-- Insère le label associé au groupe de boutons radio --%>
		<%--
        <c:if test="${_affichelibelle eq 'true'}">

            <c:if test="${ _responsive eq 'false'}">
    			<div class="${_labelboxclass}" style="width: ${_labelboxwidth};">
            </c:if>
    
    				<c:if test="${! empty accesskey}">
    					<c:set var="_complabel" value="accesskey=\" ${accesskey}\""/>
    				</c:if>
    				<c:if test="${! empty consigne}">
    					<c:set var="_complabel" value="${_complabel} title=\" ${consigne}\""/>
    				</c:if>
            <label class="${_labelclass} ${_bootstraplblclass}" for="${_id}" ${_complabel}> <c:if
    						test="${status_error || status_warning}">
    						<fmt:message key="required" var="errorMsg">
    							<fmt:param value="${libelle}" />
    						</fmt:message>
    						<img src="<c:url value='${_imagesrepertoire}/errInput.gif'/>"
    							alt="${fn:escapeXml(errorMsg)}" title="${fn:escapeXml(errorMsg)}" />
    					</c:if> ${libelle}
    				</label>
    				<c:if test="${_affichelibelle eq 'false'}">
    					<c:if test="${status_error || status_warning}">
    						<fmt:message key="required" var="errorMsg">
    							<fmt:param value="${libelle}" />
    						</fmt:message>
    						<img src="<c:url value='${_imagesrepertoire}/errInput.gif'/>"
    							alt="${fn:escapeXml(errorMsg)}" title="${fn:escapeXml(errorMsg)}" />
    					</c:if>
    				</c:if>
            <c:if test="${ _responsive eq 'false'}">
    			</div>
    		</c:if>
        </c:if>
    --%>
		<%-- Insère un bloc div qui contiendra l'ensemble des boutons radios --%>
    <c:choose>
        <c:when test="${ _responsive eq 'false'}">
            <fieldset class="${_inputboxclass}" style="width: ${_inputboxwidth}" id="${_id}">
        </c:when>
        <c:otherwise>
            <fieldset class="${_inputboxclass} ${_bootstrapgrpclass} radio" id="${_id}">
        </c:otherwise>
    </c:choose>
    
            <legend>${libelle}</legend>

			<%-- Choix du type de collection Map, List ou Set--%>
			<c:choose>
				<c:when test="${! empty itemsmap}">
					<c:set var="items" value="${itemsmap}" />
				</c:when>
				<c:when test="${! empty itemslist}">
					<c:set var="items" value="${itemslist}" />
				</c:when>
				<c:when test="${! empty itemsset}">
					<c:set var="items" value="${itemsset}" />
				</c:when>
			</c:choose>

			<%--Boucle sur les items de la collection contenant les choix possibles--%>
			<c:forEach var="item" items="${items}">
				<c:set var="cptid" value="${cptid + 1 }" />
				<%-- En fonction du type de collection et des paramètres label et value--%>
				<%-- détermination des valeurs utilisées pour chaque bouton radio--%>
				<c:choose>
					<%-- valeur du bouton radio =  clé de la Map
				     label du bouton radio = propriété ${label} de l'objet de la Map --%>
					<c:when test="${! empty itemsmap && ! empty label}">
						<c:set var="lbl" value="${item['value'].label}" />
						<c:set var="val" value="${item['key']}" />
					</c:when>
					<%-- valeur du bouton radio =  clé de la Map
				     label du bouton radio = valeur de la méthode toString() de l'objet de la Map--%>
					<c:when test="${! empty itemsmap && empty label}">
						<c:set var="lbl" value="${item['value']}" />
						<c:set var="val" value="${item['key']}" />
					</c:when>
					<%-- valeur du bouton radio = propriété ${value} d'un item de la collection
				     label du bouton radio = propriété ${label} d'un item de la collection --%>
            <c:when test="${! empty itemsset && ! empty label && ! empty value}">
						<c:set var="lbl" value="${item[label]}" />
						<c:set var="val" value="${item[value]}" />
					</c:when>
					<%-- valeur du bouton radio = propriété ${value} d'un item de la collection
				     label du bouton radio = propriété ${value} d'un item de la collection --%>
					<c:when test="${! empty itemsset && empty label && ! empty value}">
						<c:set var="lbl" value="${item[value]}" />
						<c:set var="val" value="${item[value]}" />
					</c:when>
					<%-- valeur du bouton radio = propriété ${value} d'un item de la collection
				     label du bouton radio = propriété ${label} d'un item de la collection --%>
            <c:when test="${! empty itemslist && ! empty label && ! empty value}">
						<c:set var="lbl" value="${item[label]}" />
						<c:set var="val" value="${item[value]}" />
					</c:when>
					<%-- valeur du bouton radio = propriété ${value} d'un item de la collection
				     label du bouton radio = propriété ${value} d'un item de la collection --%>
					<c:when test="${! empty itemslist && empty label && ! empty value}">
						<c:set var="lbl" value="${item[value]}" />
						<c:set var="val" value="${item[value]}" />
					</c:when>
					<%-- valeur du bouton radio = valeur de la méthode toString () d'un item de la collection
				     label du bouton radio = valeur de la méthode toString () d'un item de la collection --%>
					<c:otherwise>
						<c:set var="lbl" value="${item}" />
						<c:set var="val" value="${item}" />
					</c:otherwise>
				</c:choose>

				<%-- Insére le label associé au bouton radio --%>
				<label for="${_id}_${cptid}" class="${_labelradioclass}"> <%-- Insère le bouton radio --%>
					<html:checkbox path="${_path}" value="${val}">
						<jsp:attribute name="ignorenullnestedpath">${_ignorenullnestedpath}</jsp:attribute>
						<jsp:attribute name="type">radio</jsp:attribute>
						<jsp:attribute name="id">${_id}_${cptid}</jsp:attribute>
						<jsp:attribute name="class">${_radioclass}</jsp:attribute>
						<jsp:attribute name="readOnly">${_readonly}</jsp:attribute>
						<jsp:attribute name="tabindex">${tabindex}</jsp:attribute>
						<jsp:attribute name="onfocus">${onfocus}</jsp:attribute>
						<jsp:attribute name="onblur">${onblur}</jsp:attribute>
						<jsp:attribute name="onselect">${onselect}</jsp:attribute>
						<jsp:attribute name="onchange">${onchange}</jsp:attribute>
						<jsp:attribute name="onkeypress">${_onkeypress}</jsp:attribute>
						<jsp:attribute name="onclick">${onclick}</jsp:attribute>
						<jsp:attribute name="ondblclick">${ondblclick}</jsp:attribute>
						<jsp:attribute name="onmousedown">${onmousedown}</jsp:attribute>
						<jsp:attribute name="onmouseup">${onmouseup}</jsp:attribute>
						<jsp:attribute name="onmouseover">${onmouseover}</jsp:attribute>
						<jsp:attribute name="onmousemove">${onmousemove}</jsp:attribute>
						<jsp:attribute name="onmouseout">${onmouseout}</jsp:attribute>
						<jsp:attribute name="onkeydown">${onkeydown}</jsp:attribute>
						<jsp:attribute name="onkeyup">${onkeyup}</jsp:attribute>
					</html:checkbox>${lbl}
				</label>
			</c:forEach>
		</fieldset>
		<%-- Insère les éléments présents dans le corps du tag --%>
		<%-- Lecture du corps du Tag--%>
		<jsp:doBody var="resbody" />
		<c:if test="${! empty resbody}">
        <c:choose>
            <c:when test="${ _responsive eq 'false'}">
			<div class="${_compboxclass}" style="width: ${_compboxwidth};">${resbody}</div>
            </c:when>
            <c:otherwise>
                <div class="${_compboxclass}">${resbody}</div>
            </c:otherwise>
        </c:choose>
		</c:if>
	</div>

	<%-- fin du test d'évaluation du tag--%>
</c:if>