<%@ tag dynamic-attributes="attributes" 
        isELIgnored="false" 
        pageEncoding="UTF-8"%>
<%@ include file="/META-INF/resources/WEB-INF/tags/html/includes.tagf" %>
<%@ attribute name="var"
              description="Message d'attente."%>
<%@ attribute name="boxwaitclass" 
              description="Classe css du bloc 'div' contenant le message d'attente. Par défaut = pleaseWait" %>
<%@ attribute name="style" 
              description="Style css appliqué au bloc 'div' contenant le message d'attente. Par défaut = " %>              
<%@ attribute name="imgwait" 
              description="Image du message d'attente. Par défaut = wait.gif" %>

<%-- Initialisation des paramètres par défaut --%>
<app:initvar attrname="_imagesrepertoire" attrvalue="${imagesrepertoire}" bundlekey="app.page.imagesrepertoire" defaultvalue="/images/app"/>
<app:initvar attrname="_boxwaitclass" attrvalue="${boxwaitclass}" bundlekey="app.dispwait.boxwaitclass" defaultvalue="pleaseWait"/>

<app:initvar attrname="_appnouvelcharte" attrvalue="${appnouvelcharte}" bundlekey="app.nouvel.charte" defaultvalue="false"/>
<c:if test="${_appnouvelcharte ne 'true' }">
	<app:initvar attrname="_imgwait" attrvalue="${imgwait}" bundlekey="app.dispwait.imgwait" defaultvalue="wait.gif"/>
</c:if>
<c:if test="${_appnouvelcharte eq 'true' }">
	<app:initvar attrname="_imgwait" attrvalue="${imgwait}" bundlekey="app.dispwait.imgwait" defaultvalue="loading_128.gif"/>
</c:if>
<c:if test="${empty var}">
	<c:set var="var" value="Requête en cours de traitement.  Patientez..."/>
</c:if>

<html:attributes var="attrString" attributeMap="${attributes}">
<div id="pleaseWait" class="${_boxwaitclass}" style="${style}" ${attrString} tabIndex="1">
<h1>
<a href="#" id="waitMsg" >    ${var} </a>
</h1>
<img id="waitimg"  src="<c:url value="${_imagesrepertoire}/${_imgwait}"/>" alt="Chargement en cours, veuillez patienter" />
</div>

<div id="overlay">&nbsp;</div>
</html:attributes>




