<%@ tag dynamic-attributes="attributes" 
        isELIgnored="false" 
        pageEncoding="UTF-8"
        description="Insère un bloc div contenant des messages d'erreurs"
        %>
<%@ include file="/META-INF/resources/WEB-INF/tags/html/includes.tagf" %>
<%@ attribute name="path" description="Chemin d'accès à la propriété d'un bean bindé"%>
<%@ attribute name="var" description="Variable stockée dans le contexte contenant un message d'erreur à affiché"%>
<%@ attribute name="divid" description="id du bloc div contenant les messages d'erreur"%>
<%@ attribute name="imagesrepertoire" 
              description="Répertoire contenant les images utilisées par le tag. Par défaut = /composants/tagsapp/images/" %>
<%@ attribute name="imghautgauche" 
              description="Image du coin haut gauche du message d'erreur. Par défaut = error.gif" %>

<%-- Initialisation des paramètres par défaut --%>
<app:initvar attrname="_imagesrepertoire" attrvalue="${imagesrepertoire}" bundlekey="app.page.imagesrepertoire" defaultvalue="/composants/images/app"/>
<app:initvar attrname="_imghautgauche" attrvalue="${imghautgauche}" bundlekey="app.disperrorfix.imghautgauche" defaultvalue="error.gif"/>

<c:if test="${! empty path }">
	<spring:bind path="${path}">
	<c:set var="listeerreurs" value=""/>
	<c:if test="${status.error}">
		<html:forEachError path="${path}" var="errorMessage">
			<c:set var="listeerreurs" value="${listeerreurs}<li>${errorMessage}</li>"/>
		</html:forEachError>
	</c:if>
	</spring:bind>
</c:if>

<jsp:doBody var="contentTag"/>

<c:if test="${! empty listeerreurs || ! empty var || ! empty contentTag}">
<html:attributes var="attrString" attributeMap="${attributes}">
<div id="${divid}" class="popboxerreurclass" ${attrString}>
   <div id="popbt" class="popbt">
		<h1> Attention </h1>
	</div>
	<div id="popimg" class="popimg">
			<img src="<c:url value="${_imagesrepertoire}${_imghautgauche}"/>" alt="Attention" style="border: none"/>
	</div>
	<div id="popmsg" class="popmsg">
		<c:if test="${! empty listeerreurs}"><ul>${listeerreurs}</ul></c:if>
		<c:if test="${! empty var}"><ul><li>${var}</li></ul></c:if>
		${contentTag}
		<br/>
	</div>
</div>
</html:attributes>
</c:if>



