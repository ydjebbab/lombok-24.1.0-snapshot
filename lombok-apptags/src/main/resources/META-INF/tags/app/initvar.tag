<%@ tag dynamic-attributes="attributes" 
        isELIgnored="false" 
        pageEncoding="UTF-8"
        description="Crée une variable valorisée avec la valeur d'un attribut ou une valeur par défaut."
       %>
<%@ include file="/META-INF/resources/WEB-INF/tags/html/includes.tagf" %>

<%@ attribute name="attrname" 
              required="true" rtexprvalue="false" type="java.lang.String"
              description="Nom de la variable à créer." %>

<%@ attribute name="attrvalue" 
              required="true"
              description="Valeur à affecter à l'attribut"%>

<%@ attribute name="bundlekey" 
              required="true"
              description="Clé sous laquelle chercher une valeur par défaut dans le fichier application.properties"%>
                            
<%@ attribute name="defaultvalue" 
              required="false"
              description="Valeur à affecter par défaut."%>              

<%@ attribute name="bootstrapmodel"
 				required="false"
				description="modèle intermédiaire bootstrap."%>

<%-- Variables exposées --%>
<%@ variable name-from-attribute="attrname" 
             alias="result"
             scope="AT_BEGIN" 
             description="Expose une variable à partir de l'attribut attrname" %>



<fmt:bundle basename="apptags">
    <c:choose>
        <c:when test="${! empty attrvalue}">
            <%-- si la valeur d'une variable contient btmodel_ :
         c'est une variable responsive a transformer en classe bootstrap --%>
            <c:choose>
                <c:when test="${fn:contains(attrvalue,'btmodel_')}">
                    <app:responsive_models bootstrapclass="responsive_bootstrap_result" bootstrapmodel="${attrvalue}" />
                    <c:set var="result" value="${responsive_bootstrap_result}" />
                </c:when>
                <c:otherwise>
		<c:set var="result" value="${attrvalue}"/>
                </c:otherwise>
            </c:choose>
	</c:when>
	<c:otherwise>
		<c:if test="${! empty bundlekey}">
			<fmt:message key='${bundlekey}' var="bundlekeyvalue"/>
		</c:if>	
		<c:set var="keynotfound" value="???${bundlekey}???"/>
		<c:choose>	
			<c:when  test="${! empty bundlekeyvalue && keynotfound ne bundlekeyvalue}">
                    <%-- si la valeur d'une variable contient btmodel_ :
                    c'est une variable responsive a transformer en classe bootstrap --%>
                    <c:choose>
                        <c:when test="${fn:contains(bundlekeyvalue,'btmodel_')}">
                            <app:responsive_models bootstrapclass="responsive_bootstrap_result" bootstrapmodel="${bundlekeyvalue}" />
                            <c:set var="result" value="${responsive_bootstrap_result}" />
                        </c:when>
                        <c:otherwise>
				<c:set var="result" value="${bundlekeyvalue}"/>
                        </c:otherwise>
                    </c:choose>

                </c:when>
                <c:otherwise>
                    <%-- si la valeur d'une variable contient btmodel_ :
                    c'est une variable responsive a transformer en classe bootstrap --%>
                    <c:choose>
                        <c:when test="${fn:contains(defaultvalue,'btmodel_')}">
                            <app:responsive_models bootstrapclass="responsive_bootstrap_result" bootstrapmodel="${defaultvalue}" />
                            <c:set var="result" value="${responsive_bootstrap_result}" />
			</c:when>
			<c:otherwise>
				<c:set var="result" value="${defaultvalue}"/>
			</c:otherwise>
		</c:choose>

                </c:otherwise>
            </c:choose>
	</c:otherwise>
</c:choose>
</fmt:bundle>
