<%@ tag dynamic-attributes="attributes" isELIgnored="false"
	pageEncoding="UTF-8"
	description="Insère un bouton qui ouvre un popup contenant un calendrier."%>

<%@ include file="/META-INF/resources/WEB-INF/tags/html/includes.tagf" %>

<%--Attributs principaux --%>
<%@ attribute name="buttonid" description="Id du bouton"%>
<%@ attribute name="fieldid"
	description="Id (html) qui désigne le champ à remplir."%>
<%@ attribute name="consigne"
	description="Libellé qui s'affiche au survol de l'image.Par défaut = Choisir une date"%>
<%@ attribute name="dateformat"
	description="Format du champs date. Par défaut : %d/%m/%Y "%>
<%@ attribute name="calendarrepertoire"
	description="Répertoire contenant les éléments du calendrier. Par défaut = /jscalendar"%>
<%@ attribute name="calendarimage"
	description="Image qui représente le calendrier sur la page. Par défaut = calendar.gif"%>
<%@ attribute name="dateouverture"
	description="Date affichée à l'ouverture du calendrier."%>
<%@ attribute name="displayed"
	description="true : le tag est évalué, false : il est ignoré. Par défaut : true."%>

<%-- Affectation de valeurs par défaut aux attributs --%>
<app:initvar attrname="_displayed" attrvalue="${displayed}"
	bundlekey="app.calendar.displayed" defaultvalue="true" />
<app:initvar attrname="_dateformat" attrvalue="${dateformat}"
	bundlekey="app.calendar.dateformat" defaultvalue="%d/%m/%Y" />
<app:initvar attrname="_calendarrepertoire"
	attrvalue="${calendarrepertoire}"
	bundlekey="app.calendar.calendarrepertoire"
	defaultvalue="/composants/jscalendar" />
<app:initvar attrname="_consigne" attrvalue="${consigne}"
	bundlekey="app.calendar.consigne" defaultvalue="Choisir une date." />
<app:initvar attrname="_appnouvelcharte" attrvalue="${appnouvelcharte}"
	bundlekey="app.nouvel.charte" defaultvalue="false" />
<c:if test="${_appnouvelcharte eq 'true' }">
	<app:initvar attrname="_calendarimage" attrvalue="${calendarimage}"
		bundlekey="app.calendar.calendarimage" defaultvalue="calendar.gif" />
</c:if>
<c:if test="${_appnouvelcharte ne 'true' }">
	<app:initvar attrname="_calendarimage" attrvalue="${calendarimage}"
		bundlekey="app.calendar.calendarimage" defaultvalue="calendrier.gif" />
</c:if>


<%-- début du test d'évaluation du tag--%>
<c:if test="${_displayed == 'true' }">

	<%-- Utilise l'ID du champ de saisie parent si l'attribut fieldid n'est pas précisé --%>
	<c:choose>
		<c:when test="${empty fieldid}">
			<c:set var="_fieldid" value="${_inputId}" />
		</c:when>
		<c:otherwise>
			<c:set var="_fieldid" value="${fieldid}" />
		</c:otherwise>
	</c:choose>

	<app:id attrname="_buttonid" id="${buttonid}"></app:id>


	<c:if test="${! empty dateouverture }">
		<c:catch var="parseexcep">
			<fmt:parseDate value="${dateouverture}" pattern="dd/MM/yyyy"
				var="daouvcal" />
		</c:catch>
	</c:if>
	<c:if test="${empty daouvcal}">
		<jsp:useBean id="daouvcal" class="java.util.Date" />
	</c:if>

	<fmt:formatDate value="${daouvcal}" pattern="yyyy" var="anneeouv" />
	<fmt:formatDate value="${daouvcal}" pattern="M" var="moisouv" />
	<fmt:formatDate value="${daouvcal}" pattern="d" var="jourouv" />
	<c:set var="moisouv" value="${moisouv} - 1" />
	<c:set var="dateouvformatepourjs"
		value="${anneeouv},${moisouv},${jourouv}" />

	<%-- Insère l'image qui permet de pointer vers le calendrier --%>
	<html:attributes var="attrString" attributeMap="${attributes}"
		id="${_buttonid}" title="${_consigne}">
		<c:if test="${! _readonly eq 'true'}">
			<c:choose>
				<c:when test="${_appnouvelcharte ne 'true' }">
					<img ${attrString}
						src="<c:url value="${_calendarrepertoire}/img/${_calendarimage}"/>"
						onmouseover="this.style.background='red';"
						onmouseout="this.style.background=''"
						style="cursor: pointer; border: 1px solid red;" alt="calendrier" />
				</c:when>
				<c:otherwise>
					<img ${attrString}
					  src="<c:url value="${_calendarrepertoire}/img/${_calendarimage}" />"
                      style="cursor: pointer; alt="calendrier"/>
				</c:otherwise>
			</c:choose>
			<script type="text/javascript">
					/*<![CDATA[*/
					datedemarrage = new Date(${dateouvformatepourjs}); 
				    Calendar.setup({
			        inputField     :    "${_fieldid}",   
			        ifFormat       :    "${_dateformat}",
			        button         :    "${_buttonid}",
			        cache          : false,
			        step           :    1,
			        date           :   datedemarrage 
				    });
					/*]]>*/
					</script>
		</c:if>
	</html:attributes>

	<%-- fin du test d'évaluation du tag--%>
</c:if>