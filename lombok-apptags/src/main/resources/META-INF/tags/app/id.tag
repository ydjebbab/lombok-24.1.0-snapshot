<%@ tag dynamic-attributes="attributes" 
        isELIgnored="false" 
        pageEncoding="UTF-8"
        description="Normalise l'ID d'un champs de saisie"
       %>
<%@ include file="/META-INF/resources/WEB-INF/tags/html/includes.tagf" %>

<%@ attribute name="attrname" 
              required="true" rtexprvalue="false" type="java.lang.String"
              description="Nom de la variable à créer." %>

<%@ attribute name="id" 
              required="false"
              description="Valeur à affecter à l'attribut"%>

<%-- Variables exposées --%>
<%@ variable name-from-attribute="attrname" 
             alias="result"
             scope="AT_BEGIN" 
             description="Expose une variable à partir de l'attribut attrname" %>

<c:choose>
	<c:when test="${empty id }">
		<c:set var="_elementId" value="${_elementId + 1}" scope="request"/>
		<c:set var="result" value="id${_elementId}"/>
	</c:when>
	<c:otherwise>
		<c:set var="result" value="${id}"/>
	</c:otherwise>
</c:choose>


 