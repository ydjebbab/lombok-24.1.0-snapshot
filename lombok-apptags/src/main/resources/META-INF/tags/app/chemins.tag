<%@ tag dynamic-attributes="attributes" 
        isELIgnored="false" 
        pageEncoding="UTF-8"
        description="Insére un chemin de navigation."%>
        
<%@ include file="/META-INF/resources/WEB-INF/tags/html/includes.tagf" %>

<%--Attributs principaux --%>
<%@ attribute name="action"
              description="Action utilisée par les sous tags app:chemin s'ils ne portent pas d'attribut action.Exemple action='zf2/flux.ex'"%>
<%@ attribute name="boxcheminclass"
              description="Classe du bloc div contenant les chemins. Par défaut : chemincontainer."%>
<%@ attribute name="displayed"
              description="true : le tag est évalué, false : il est ignoré. Par défaut : true."%>
<%@ attribute name="responsive"
              description="gestion du parametre responsive."%>                 
              
<%-- Variables exposées --%>
<%@ variable name-given="_cheminAction"  
             variable-class="java.lang.String" declare="true" 
             scope="NESTED" 
             description="Expose le chemin par défaut à utiliser." %>

<%-- Affectation de valeurs par défaut aux attributs --%>
<app:initvar attrname="_displayed" attrvalue="${displayed}" bundlekey="app.chemins.displayed" defaultvalue="true"/>
<app:initvar attrname="_boxcheminsclass" attrvalue="${boxcheminclass}" bundlekey="app.chemins.boxcheminsclass" defaultvalue="chemincontainer"/>


 <%@ include file="/META-INF/resources/WEB-INF/tags/app/responsive_test.tagf" %> 


<%-- début du test d'évaluation du tag--%>
<c:if test="${_displayed == 'true' }">

<%-- Place dans le contexte 'request' la variable _cheminaction --%>
<c:if test="${! empty action}">  
  <c:set var="_cheminAction" value="${action}" scope="request"/>
</c:if> 

<%-- Compte le nombre d'éléments dans le chemin --%>
<c:set var="_nbElementsDansChemin" value="0" scope="request"/>
<c:set var="_actionDeComptage" value="true" scope="request"/>

<jsp:doBody var="contenu"/>

<c:set var="_actionDeComptage" value="false" scope="request"/>
<c:set var="_posElementDansChemin" value="0" scope="request"/>

<html:attributes var="attrString" attributeMap="${attributes}">


<c:choose>
        <c:when test="${_responsive eq 'false'}">
	<div class="${_boxcheminsclass}">
	 	<jsp:doBody/> 
	</div>
       </c:when>
       <c:otherwise>
              <div id="fil_ariane" class="${_boxcheminsclass} rcrumbs" > 
             <ul>
                <jsp:doBody/>
             </ul>    
            </div>
       </c:otherwise>
</c:choose>
    
   
</html:attributes>
    

<%-- Supprime du contexte 'request' les variables --%>
<c:if test="${! empty _cheminAction}">  
  <c:remove var="_cheminAction" scope="request"/>
  <c:remove var="_actionDeComptage" scope="request"/>  
  <c:remove var="_posElementDansChemin" scope="request"/>
  <c:remove var="_nbElementsDansChemin" scope="request"/>
</c:if>

<%-- fin du test d'évaluation du tag--%>
</c:if>