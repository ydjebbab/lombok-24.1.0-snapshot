<%@ tag dynamic-attributes="attributes" 
        isELIgnored="false" 
        pageEncoding="utf-8"
        description="Champs de saisie de type 'file' associé via un bind à l'objet contenant les données du formulaire."%>
        
<%@ include file="/META-INF/resources/WEB-INF/tags/html/includes.tagf" %>

<%--Attributs principaux --%>
<%@ attribute name="attribut" 
              required="true" 
              description="Nom de l'attribut associé au champs de saisie (input)dans l'objet support du formulaire. Utilisé dans le spring bind" %>
<%@ attribute name="readonly"
              description="true : champs en lecture seule"%>
<%@ attribute name="libelle" 
              required="false" 
              description="Label associé au champs de saisie (input)"%>
<%@ attribute name="size" 
              required="true" 
              description="Taille du champs de saisie. Position la propriété size dans l'élé input."%>
<%@ attribute name="requis" 
              description="true : champs de saisie obligatoire.  Valeur par défaut = true "%>
<%@ attribute name="displayed"
              description="true : le tag est évalué, false : il est ignoré. Par défaut : true."%>

<%--Attributs de présentation --%>
<%@ attribute name="theme"
              description="Thème de présentation. Par défaut = V"%>
<%@ attribute name="boxheight"
              description="Hauteur du bloc div contenant l'ensemble des éléments relatifs à un champs de saisie."%>
<%@ attribute name="boxwidth"
              description="Largeur du bloc div contenant l'ensemble des éléments relatifs à un champs de saisie."%>
<%@ attribute name="labelboxwidth"
              description="Largeur du bloc div contenant le libelle (en % de la largeur totale du bloc parent)"%>
<%@ attribute name="inputboxwidth"
              description="Largeur du bloc div contenant le champ de saisie (en % de la largeur totale du bloc parent)"%>
<%@ attribute name="compboxwidth"
              description="Largeur du bloc div contenant la zone complémentaire (en % de la largeur totale du bloc parent)"%>

<%@ attribute name="normalboxclass"
              description="Classe css du bloc div contenant l'ensemble des éléments relatifs à un champs de saisie. Valeur par défaut = normal "%>
<%@ attribute name="lectureboxclass"
              description="Classe css du bloc div contenant l'ensemble des éléments relatifs à un champs de saisie, en lecture seule. Valeur par défaut = lectureseule."%>
<%@ attribute name="erreurboxclass"
              description="Classe css du bloc div contenant l'ensemble des éléments relatifs à un champs de saisie en erreur. Valeur par défaut = erreur."%>
<%@ attribute name="requisboxclass"
              description="Classe css du bloc div contenant l'ensemble des élément relatifs à un champs de saisie obligatoire. Valeur par défaut = requis."%>
<%@ attribute name="labelboxclass"
              description="Classe css du bloc div contenant le libellé.Valeur par défaut = boxlblText."%>
<%@ attribute name="inputboxclass"
              description="Classe css du bloc div contenant le champs de saisie.Valeur par défaut = boxText."%>
<%@ attribute name="compboxclass"
              description="Classe css du bloc div contenant des données complémentaires sur le champs de saisie.Valeur par défaut = boxCompText."%>
<%@ attribute name="inputclass"
              description="Classe css du champs input. Valeur par défaut = inputText"%>            
<%@ attribute name="labelclass"
              description="Classe css de l'élément label. Valeur par défaut = labelText"%>            
              
<%--Attributs optionnels --%>
<%@ attribute name="consigne" 
              description="Correspond à l'attribut 'title' de l'élément label. Peut contenir, par exemple, une consigne de saisie du champs. "%>

<%-- Attributs  --%>
<%@ attribute name="accept" 
              description="Liste des types de fichiers acceptés (type MIME). Non géré par les navigateurs."%>
<%@ attribute name="tabindex"
              description="Numéro indiquant l'ordre d'accès par tabulation."%>
<%@ attribute name="accesskey"
              description="Touche de raccourci pour accéder à un champs."%>
<%@ attribute name="onfocus"
              description="Script exécuté lors que l'élément est accédé."%>
<%@ attribute name="onblur"
              description="Script exécuté lors que l'élément est quitté."%>
<%@ attribute name="select"
              description="Script exécuté lorsqu'une portion de texte est sélectionnée."%>
<%@ attribute name="onchange"
              description="Script exécuté lors que l'élément est quitté et que son contenu a été modifié."%>
              
<%-- Attributs événements intrinsèques HTML --%>
<%@ attribute name="onclick"
              description="Script exécuté quand le bouton du dispositif de pointage est cliqué au-dessus d'un élément."%>
<%@ attribute name="ondblclick"
              description="Script exécuté quand le bouton du dispositif de pointage est double-cliqué au-dessus d'un élément."%>              
<%@ attribute name="onmousedown"
              description="Script exécuté quand le bouton du dispositif de pointage est appuyé au-dessus d'un élément."%>              
<%@ attribute name="onmouseup"
              description="Script exécuté quand le bouton du dispositif de pointage est relaché au-dessus d'un élément."%>                            
<%@ attribute name="onmouseover"
              description="Script exécuté quand le bouton du dispositif de pointage est déplacé sur un élément."%>                                          
<%@ attribute name="onmousemove"
              description="Script exécuté quand le bouton du dispositif de pointage est déplacé alors qu'il est au-dessus d'un élément."%>
<%@ attribute name="onmouseout"
              description="Script exécuté quand le bouton du dispositif de pointage est déplacé en dehors d'un élément."%>                                                                      
<%@ attribute name="onkeypress"
              description="Script exécuté quand une touche est pressée puis relachée au-dessus d'un élément."%>
<%@ attribute name="onkeydown"
              description="Script exécuté quand une touche est gardée appuyée au-dessus d'un élément."%>              
<%@ attribute name="onkeyup"
              description="Script exécuté quand une touche est relachée au-dessus d'un élément."%>                            
              
<%@ attribute name="id"
              description="id de l'élément input. Par défaut : attribué automatiquement."%>

<%--Affectation de valeurs par défaut aux attributs--%>
<app:initvar attrname="_displayed" attrvalue="${displayed}" bundlekey="app.file.displayed" defaultvalue="true"/>
<app:initvar attrname="_requis" attrvalue="${requis}" bundlekey="app.input.requis" defaultvalue="true"/>
<app:initvar attrname="_readonly" attrvalue="${readonly}" bundlekey="app.input.readonly" defaultvalue="false"/>

<app:initvar attrname="_requisindicateur" attrvalue="${requisindicateur}" bundlekey="app.file.requisindicateur" defaultvalue=" *"/>
<app:initvar attrname="_normalindicateur" attrvalue="${normalindicateur}" bundlekey="app.file.normalindicateur" defaultvalue=""/>

<app:initvar attrname="_theme" attrvalue="${theme}" bundlekey="app.input.theme" defaultvalue="V"/>
<app:initvar attrname="_boxheight" attrvalue="${boxheight}" bundlekey="app.input.boxheight" defaultvalue="auto"/>
<app:initvar attrname="_boxwidth" attrvalue="${boxwidth}" bundlekey="app.input.boxwidth" defaultvalue="100%"/>
<app:initvar attrname="_labelboxwidth" attrvalue="${labelboxwidth}" bundlekey="app.input.labelboxwidth" defaultvalue="35%"/>
<app:initvar attrname="_inputboxwidth" attrvalue="${inputboxwidth}" bundlekey="app.input.inputboxwidth" defaultvalue="50%"/>
<app:initvar attrname="_compboxwidth" attrvalue="${compboxwidth}" bundlekey="app.input.compboxwidth" defaultvalue="50%"/>

<app:initvar attrname="_inputclass" attrvalue="${inputclass}" bundlekey="app.input.inputclass" defaultvalue="inputText"/>
<app:initvar attrname="_labelclass" attrvalue="${labelclass}" bundlekey="app.input.labelclass" defaultvalue="labelText"/>
<app:initvar attrname="_onkeypress" attrvalue="${onkeypress}" bundlekey="app.input.onkeypress" defaultvalue="return submitEnterButton(event)"/>
<app:initvar attrname="_normalboxclass" attrvalue="${normalboxclass}" bundlekey="app.input.normalboxclass" defaultvalue="normal"/>
<app:initvar attrname="_lectureboxclass" attrvalue="${lectureboxclass}" bundlekey="app.input.lectureboxclass" defaultvalue="lecture"/>
<app:initvar attrname="_erreurboxclass" attrvalue="${erreurboxclass}" bundlekey="app.input.erreurboxclass" defaultvalue="erreur"/>
<app:initvar attrname="_requisboxclass" attrvalue="${requisboxclass}" bundlekey="app.input.requisboxclass" defaultvalue="requis"/>
<app:initvar attrname="_labelboxclass" attrvalue="${labelboxclass}" bundlekey="app.input.labelboxclass" defaultvalue="boxlblText"/>
<app:initvar attrname="_inputboxclass" attrvalue="${inputboxclass}" bundlekey="app.input.inputboxclass" defaultvalue="boxText"/>
<app:initvar attrname="_compboxclass" attrvalue="${compboxclass}" bundlekey="app.input.compboxclass" defaultvalue="boxCompText"/>

<%@ variable name-given="_inputId"  
             variable-class="java.lang.String" declare="true" 
             scope="NESTED" 
             description="Indique à l'ensemble des tags inclus l'ID du champ input." %> 

<%-- début du test d'évaluation du tag--%>
<c:if test="${_displayed == 'true' }">

<%-- Utilisation de la variable _formObjectName exposée par le tag app:form --%>
<c:if test="${!empty  _formObjectName}">
<c:set var="_path" value="${_formObjectName}.${attribut}"/>
</c:if>

<%-- Utilisation de la variable _formReadOnly exposée par le tag app:form --%>
<c:if test="${!empty  _formReadOnly ||  _readonly eq 'true'}">
<c:set var="_readonly" value="true" scope="request"/>
</c:if>

<%-- Détermination de l'ID du champ --%>
<app:id attrname="_id" id="${id}"></app:id>

<%-- Remplit la variable status_error si nécessaire --%>
<%@ include file="/META-INF/resources/WEB-INF/tags/app/fillstatus_error.tagf" %>

<%-- Remplit la variable status_warning si nécessaire --%>
<%@ include file="/META-INF/resources/WEB-INF/tags/app/fillstatus_warning.tagf" %>

<%-- Place dans le contexte 'request' la variable _inputId --%>
<c:if test="${! empty _id}">  
  <c:set var="_inputId" value="${_id}" scope="request"/>
</c:if>

<c:if test="${empty libelle}">
	<fmt:message key="${attribut}" var="libelle" />
	<str:countMatches substring="???${attribut}???" var="exitInMessagesProperties">${libelle}</str:countMatches>
	<c:if test="${exitInMessagesProperties == 1}"><c:set var="libelle" value=""/></c:if>
</c:if>

<%-- Sélection de la classe du bloc div --%>
<c:choose>
	<c:when test="${status_error || status_warning}">
		<c:set var="_divclass" value="${_theme}${_erreurboxclass}"/>
		<c:if test="${empty _firsterrorid}">
			<c:set var="_firsterrorid" value="${_id}" scope="request"/> 
		</c:if>
	</c:when>
	<c:when test="${_readonly eq 'true'}">
		<c:set var="_divclass" value="${_theme}${_lectureboxclass}"/>
	</c:when>
	<c:when test="${_requis eq 'true'}">
		<c:set var="_divclass" value="${_theme}${_requisboxclass}"/>
	</c:when>
	<c:otherwise>
		<c:set var="_divclass" value="${_theme}${_normalboxclass}"/>
	</c:otherwise>
</c:choose>

<%-- Ajout d'un indicateur pour les champs obligatoires --%>
<c:choose>
	<c:when test="${_requis eq 'true'}">
		<c:set var="libelle" value="${libelle}${_requisindicateur}"/>
	</c:when>
	<c:otherwise>
		<c:set var="libelle" value="${libelle}${_normalindicateur}"/>
	</c:otherwise>
</c:choose>

<%-- Utilisation du hack "_" pour contourner l'absence de propriété min-height sous IE --%>
<div class="${_divclass}" style="width: ${_boxwidth};height: auto;_height: ${_boxheight};min-height: ${_boxheight};">
    <%-- Insère le label associé aux champs de saisie--%>
    <div class="${_labelboxclass}" style="width: ${_labelboxwidth};">
	    <c:if test="${! empty accesskey}">
		    <c:set var="_complabel" value="accesskey=\"${accesskey}\""/>
	    </c:if>
	    <c:if test="${! empty consigne}">
		    <c:set var="_complabel" value="${_complabel} title=\"${consigne}\""/>
	    </c:if>
        <label class="${_labelclass}" for="${_id}" ${_complabel}>${libelle}</label>
    </div>
    <%-- Insère le champs de saisie --%>
    <div class="${_inputboxclass}" style="width: ${_inputboxwidth};">
    <html:input type="file" path="${_path}">
    	<jsp:attribute name="size">${size}</jsp:attribute>
    	<jsp:attribute name="id">${_id}</jsp:attribute>
    	<jsp:attribute name="class">${_inputclass}</jsp:attribute>
    	<jsp:attribute name="readOnly">${_readonly}</jsp:attribute>
    	<jsp:attribute name="tabindex">${tabindex}</jsp:attribute>
    	<jsp:attribute name="accept">${accept}</jsp:attribute>
		<jsp:attribute name="onfocus">${onfocus}</jsp:attribute>
    	<jsp:attribute name="onblur">${onblur}</jsp:attribute>
   		<jsp:attribute name="onselect">${onselect}</jsp:attribute>
    	<jsp:attribute name="onchange">${onchange}</jsp:attribute> 
		<jsp:attribute name="onkeypress">${_onkeypress}</jsp:attribute> 
		<jsp:attribute name="onclick">${onclick}</jsp:attribute> 
		<jsp:attribute name="ondblclick">${ondblclick}</jsp:attribute> 	   			
		<jsp:attribute name="onmousedown">${onmousedown}</jsp:attribute>
	   	<jsp:attribute name="onmouseup">${onmouseup}</jsp:attribute>
	   	<jsp:attribute name="onmouseover">${onmouseover}</jsp:attribute>
	    <jsp:attribute name="onmousemove">${onmousemove}</jsp:attribute>
	   	<jsp:attribute name="onmouseout">${onmouseout}</jsp:attribute>
	   	<jsp:attribute name="onkeydown">${onkeydown}</jsp:attribute>
	   	<jsp:attribute name="onkeyup">${onkeyup}</jsp:attribute>    	
    </html:input>
    </div>
    <%-- Insère les éléments présents dans le corps du tag --%>
    <%-- Lecture du corps du Tag--%>
	<jsp:doBody var="resbody"/>
		<c:if test="${! empty resbody}">
	        <div class="${_compboxclass}" style="width: ${_compboxwidth};">${resbody}</div>
		</c:if>
</div>

<%-- Supprime du contexte 'request' la variable _readonly --%>
<c:if test="${! empty readonly}">  
  <c:remove var="_readonly" scope="request"/>
</c:if>

<%-- Supprime du contexte 'request' la variable _inputId --%>
<c:if test="${! empty _id}">  
  <c:remove var="_inputId" scope="request"/>
</c:if>

<%-- fin du test d'évaluation du tag--%>
</c:if>
