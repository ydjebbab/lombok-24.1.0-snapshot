<%@ tag dynamic-attributes="attributes"
        isELIgnored="false" 
        pageEncoding="UTF-8"
        description="Insère un groupe de cases à cocher"
        display-name="checkbox"
        %>

<%@ include file="/META-INF/resources/WEB-INF/tags/html/includes.tagf" %>        

<%--Attributs principaux --%>
<%@ attribute name="attribut" 
              required="true" 
              description="Nom de l'attribut associé au champ de saisie dans l'objet support du formulaire. Utilisé dans le spring bind" %>
<%@ attribute name="checkAllAttribut" required="false"
    description="Nom de l'attribut associé à la case Tout sélectionner dans l'objet support du formulaire."%>
<%@ attribute name="checkAllStyle" required="false" description="Style du libellé de la case Tout sélectionner."%>
<%@ attribute name="checkAllLibelle" required="false" description="Libellé de la case Tout sélectionner."%>
<%@ attribute name="readonly"
              description="true : champs en lecture seule"%>
<%@ attribute name="libelle" 
              required="false" 
              description="Label associé à l'ensemble des cases à cocher (élément legend)"%>
<%@ attribute name="items" 
              type="java.lang.Object" 
              description="Bean de type Collection ou Map contenant les éléments du groupe de cases à cocher."%>
<%@ attribute name="itemslist" 
              type="java.lang.Object" 
              description="Bean de type List contenant les éléments du groupe de cases à cocher."%>
<%@ attribute name="itemsset" 
              type="java.lang.Object" 
              description="Bean de type set contenant les éléments du groupe de cases à cocher."%>
<%@ attribute name="itemsmap" 
              type="java.lang.Object"
              description="Bean de type Map contenant les éléments du groupe de cases à cocher" %>
<%@ attribute name="label"
              description="Nom de la propriété d'un élément de la collection (items...) affichée comme label de la case à cocher."%>
<%@ attribute name="consignelabel"
              description="Nom de la propriété d'un élément de la collection (items...) affichée comme consigne sur le label de la case à cocher."%>
<%@ attribute name="value"
              description="Nom de la propriété d'un élément de la collection (items...) utilis&eacute comme valeur de la case à cocher."%>
<%@ attribute name="requis" 
              description="true : champs obligatoire.  Valeur par défaut = true "%>
<%@ attribute name="consigne" 
              description="Définit l'attribut 'title' de l'élément 'label'. Peut contenir, par exemple, une consigne de saisie du champs. "%>
<%@ attribute name="checkall" 
              description="Insère un bouton qui sélectionne toutes les cases à cocher"%>
<%@ attribute name="ignorenullnestedpath" 
              description="true : Ignore les références nulles lors de l'accès à un attribut.  Valeur par défaut = true "%>              
<%@ attribute name="displayed"
              description="true: le tag est évalué. Par défaut : true"%>
<%@ attribute name="affichelibelle"
              description="Affichage du libellé associé à l'ensemble des cases à cocher. Par défaut : true"%>              
<%@ attribute name="checkallenhaut"
              description="Affichage du bouton qui sélectionne toutes les cases à cocher en haut de la liste. Par défaut : false"%>              
              
<%--Attributs de présentation --%>
<%@ attribute name="theme"
              description="Thème de présentation. Par défaut = V"%>
<%@ attribute name="boxheight"
              description="Hauteur du bloc div contenant l'ensemble des élément relatifs à un champs de saisie."%>
<%@ attribute name="boxwidth"
              description="Largeur du bloc div contenant l'ensemble des élément relatifs à un champs de saisie."%>
<%@ attribute name="labelboxwidth"
              description="Largeur du bloc div contenant le libelle (en % de la largeur totale du bloc parent)"%>
<%@ attribute name="inputboxwidth"
              description="Largeur du bloc div contenant le champ de saisie (en % de la largeur totale du bloc parent)"%>
<%@ attribute name="compboxwidth"
              description="Largeur du bloc div contenant la zone complémentaire (en % de la largeur totale du bloc parent)"%>
<%@ attribute name="responsive" description="gestion du parametre responsive."%>
<%@ attribute name="fieldsetMarginLeft" description="Marge à gauche à appliquer sur le fieldset" %>

<%--Attributs de paramétrage des classes css --%>              
<%@ attribute name="normalboxclass"
              description="Classe css du bloc div contenant l'ensemble des éléments relatifs aux cases à cocher. Valeur par défaut = normal "%>
<%@ attribute name="lectureboxclass"
              description="Classe css du bloc div contenant l'ensemble des éléments relatifs aux cases à cocher en lecture seule. Valeur par défaut = lectureseule."%>
<%@ attribute name="erreurboxclass"
              description="Classe css du bloc div contenant l'ensemble des éléments relatifs aux cases à cocher, lorsque le champs est en erreur. Valeur par défaut = erreur."%>
<%@ attribute name="requisboxclass"
              description="Classe css du bloc div contenant l'ensemble des éléments relatifs aux cases à cocher, lorsque le champs de saisie est obligatoire. Valeur par défaut = requis."%>
<%@ attribute name="labelclass"
              description="Classe css de l'élément label. Valeur par défaut = lblGrpRadioChk"%>            
<%@ attribute name="labelboxclass"
              description="Classe css du bloc div contenant le libellé.Valeur par défaut = boxlblGrpRadioChk."%>
<%@ attribute name="inputboxclass"
              description="Classe css du bloc div contenant le champs de saisie.Valeur par défaut = boxGrpRadioChk."%>
<%@ attribute name="compboxclass"
              description="Classe css du bloc div contenant des données complémentaires sur le champs de saisie.Valeur par défaut = boxCompRadioChk."%>
<%@ attribute name="checkboxclass"
              description="Classe css du champs input de type multibox. Valeur par défaut = 'checkbox'"%>            
<%@ attribute name="labelcheckboxclass"
              description="Classe css du label associé aux champs input de type 'multibox'. Valeur par défaut = lblRadioChk"%>                      
<%@ attribute name="checkallclass"
              description="Classe css du bouton qui sélectionne toutes les cases à cocher. Valeur par défaut = 'checkbox'"%>            
<%@ attribute name="labelcheckallclass"
              description="Classe css du label associé au bouton qui sélectionne toutes les cases à cocher. Valeur par défaut = lblRadioChk"%>                      

<%@ attribute name="bootstrapclass" description="classes bootstrap de la checkbox"%>
<%@ attribute name="bootstraplblclass" description="classes bootstrap du label"%>
<%@ attribute name="bootstrapgrpclass" description="classes bootstrap du groupe"%>

              
<%-- Attributs  --%>
<%@ attribute name="tabindex"
              description="Numéro indiquant l'ordre d'accès par tabulation."%>
<%@ attribute name="accesskey"
              description="Touche de raccourci pour accéder à un champs."%>
<%@ attribute name="onfocus"
              description="Script exécuté lors que l'élément est accédé."%>
<%@ attribute name="onblur"
              description="Script exécuté lors que l'élément est quitté."%>
<%@ attribute name="onselect"
              description="Script exécuté lorsqu'une portion de texte est sélectionnée."%>
<%@ attribute name="onchange"
              description="Script exécuté lors que l'élément est quitté et que son contenu à été modifié."%>

<%-- Attributs événements intrinsèques HTML --%>
<%@ attribute name="onclick"
              description="Script exécuté quand le bouton du dispositif de pointage est cliqué au-dessus d'un élément."%>
<%@ attribute name="ondblclick"
              description="Script exécuté quand le bouton du dispositif de pointage est double-cliqué au-dessus d'un élément."%>              
<%@ attribute name="onmousedown"
              description="Script exécuté quand le bouton du dispositif de pointage est appuyé au-dessus d'un élément."%>              
<%@ attribute name="onmouseup"
              description="Script exécuté quand le bouton du dispositif de pointage est relaché au-dessus d'un élément."%>                            
<%@ attribute name="onmouseover"
              description="Script exécuté quand le bouton du dispositif de pointage est déplacé sur un élément."%>                                          
<%@ attribute name="onmousemove"
              description="Script exécuté quand le bouton du dispositif de pointage est déplacé alors qu'il est au-dessus d'un élément."%>
<%@ attribute name="onmouseout"
              description="Script exécuté quand le bouton du dispositif de pointage est déplacé en dehors d'un élément."%>                                                                      
<%@ attribute name="onkeypress"
              description="Script exécuté quand une touche est pressée puis relachée au-dessus d'un élément."%>
<%@ attribute name="onkeydown"
              description="Script exécuté quand une touche est gardée appuyée au-dessus d'un élément."%>              
<%@ attribute name="onkeyup"
              description="Script exécuté quand une touche est relachée au-dessus d'un élément."%>                            
              
<%@ attribute name="id"
              description="id de l'élément input. Par défaut : attribué automatiquement."%>
              
<%--Affectation de valeurs par défaut aux attributs--%>
<app:initvar attrname="_displayed" attrvalue="${displayed}" bundlekey="app.checkbox.displayed" defaultvalue="true"/>
<app:initvar attrname="_ignorenullnestedpath" attrvalue="${ignorenullnestedpath}" bundlekey="app.checkbox.ignorenullnestedpath" defaultvalue="true"/>
<app:initvar attrname="_readonly" attrvalue="${readonly}" bundlekey="app.checkbox.readonly" defaultvalue="false"/>
<app:initvar attrname="_requis" attrvalue="${requis}" bundlekey="app.checkbox.requis" defaultvalue="false"/>
<app:initvar attrname="_affichelibelle" attrvalue="${affichelibelle}" bundlekey="app.checkbox.affichelibelle" defaultvalue="true"/>
<app:initvar attrname="_checkallenhaut" attrvalue="${checkallenhaut}" bundlekey="app.checkbox.checkallenhaut" defaultvalue="false"/>

<app:initvar attrname="_requisindicateur" attrvalue="${requisindicateur}" bundlekey="app.checkbox.requisindicateur" defaultvalue=" *"/>
<app:initvar attrname="_normalindicateur" attrvalue="${normalindicateur}" bundlekey="app.checkbox.normalindicateur" defaultvalue=""/>

<app:initvar attrname="_theme" attrvalue="${theme}" bundlekey="app.checkbox.theme" defaultvalue="V"/>
<app:initvar attrname="_boxheight" attrvalue="${boxheight}" bundlekey="app.checkbox.boxheight" defaultvalue="auto"/>
<app:initvar attrname="_boxwidth" attrvalue="${boxwidth}" bundlekey="app.checkbox.boxwidth" defaultvalue="100%"/>
<app:initvar attrname="_labelboxwidth" attrvalue="${labelboxwidth}" bundlekey="app.checkbox.labelboxwidth" defaultvalue="35%"/>
<app:initvar attrname="_inputboxwidth" attrvalue="${inputboxwidth}" bundlekey="app.checkbox.inputboxwidth" defaultvalue="50%"/>
<app:initvar attrname="_compboxwidth" attrvalue="${compboxwidth}" bundlekey="app.checkbox.compboxwidth" defaultvalue="50%"/>

<%--Paramétrage des classes css par défaut --%>
<app:initvar attrname="_normalboxclass" attrvalue="${normalboxclass}" bundlekey="app.checkbox.normalboxclass" defaultvalue="normal"/>
<app:initvar attrname="_lectureboxclass" attrvalue="${lectureboxclass}" bundlekey="app.checkbox.lectureboxclass" defaultvalue="lecture"/>
<app:initvar attrname="_erreurboxclass" attrvalue="${erreurboxclass}" bundlekey="app.checkbox.erreurboxclass" defaultvalue="erreur"/>
<app:initvar attrname="_requisboxclass" attrvalue="${requisboxclass}" bundlekey="app.checkbox.requisboxclass" defaultvalue="requis"/>
<app:initvar attrname="_labelboxclass" attrvalue="${labelboxclass}" bundlekey="app.checkbox.labelboxclass" defaultvalue="boxlblGrpRadioChk"/>
<app:initvar attrname="_inputboxclass" attrvalue="${inputboxclass}" bundlekey="app.checkbox.inputboxclass" defaultvalue="boxGrpRadioChk"/>
<app:initvar attrname="_compboxclass" attrvalue="${compboxclass}" bundlekey="app.checkbox.compboxclass" defaultvalue="boxCompRadioChk"/>
<app:initvar attrname="_labelcheckboxclass" attrvalue="${labelcheckboxclass}" bundlekey="app.checkbox.labelcheckboxclass" defaultvalue="lblRadioChk"/>          
<app:initvar attrname="_checkboxclass" attrvalue="${checkclass}" bundlekey="app.checkbox.radioclass" defaultvalue="checkbox"/>          
<app:initvar attrname="_labelcheckallclass" attrvalue="${labelcheckallclass}" bundlekey="app.checkbox.labelcheckallclass" defaultvalue="lblRadioChk"/>          
<app:initvar attrname="_checkallclass" attrvalue="${checkallclass}" bundlekey="app.checkbox.radioallclass" defaultvalue="checkbox"/>            
<app:initvar attrname="_labelclass" attrvalue="${labelclass}" bundlekey="app.checkbox.labelclass" defaultvalue="lblGrpRadioChk"/>



<app:initvar attrname="_bootstrapclass" attrvalue="${bootstrapclass}" bundlekey="app.page.rwd.bootstrapclass"  defaultvalue="fullwidth" />
<app:initvar attrname="_bootstraplblclass" attrvalue="${bootstraplblclass}" bundlekey="app.page.rwd.bootstraplblclass" defaultvalue="btmodel_quarter_to_half" />
<app:initvar attrname="_bootstrapgrpclass" attrvalue="${bootstrapgrpclass}" bundlekey="app.page.rwd.bootstrapgrpclass" defaultvalue="btmodel_quarter_to_half" />

<%-- Paramétrage des évènements --%>
<app:initvar attrname="_onkeypress" attrvalue="${onkeypress}" bundlekey="app.checkbox.onkeypress" defaultvalue="return submitEnterButton(event)"/>

<app:initvar attrname="_imagesrepertoire" attrvalue="${imagesrepertoire}" bundlekey="app.page.imagesrepertoire" defaultvalue="/composants/tagsapp/images"/>

<%@ include file="/META-INF/resources/WEB-INF/tags/app/responsive_test.tagf" %>

<%-- début du test d'évaluation du tag--%>
<c:if test="${_displayed == 'true' }">

<%-- Utilisation de la variable _formObjectName exposée par le tag app:form --%>
<c:if test="${!empty  _formObjectName}">
<c:set var="_path" value="${_formObjectName}.${attribut}"/>
</c:if>

<%-- Utilisation de la variable _formReadOnly exposée par le tag app:form --%>
<c:if test="${!empty  _formReadOnly}">
<c:set var="_readonly" value="true"/>
</c:if>

<%-- Détermination de l'ID du champ --%>
<app:id attrname="_id" id="${id}"></app:id>

<%-- cptid : compteur pour créer un attribut id unique pour chaque bouton radio--%>
<c:set var="cptid" value="0"/> 

<%@ include file="/META-INF/resources/WEB-INF/tags/app/fillstatus_error.tagf" %>

<%-- Remplit la variable status_warning si nécessaire --%>
<%@ include file="/META-INF/resources/WEB-INF/tags/app/fillstatus_warning.tagf" %>

<%-- Si l'attribut libellé est vide, utilise le nom de la propriété bindée comme
     clé de recherche dans les fichiers *.properties --%>
<c:if test="${empty libelle}">
    <fmt:message key="${attribut}" var="libelle" />
    <str:countMatches substring="???${attribut}???" var="exitInMessagesProperties">${libelle}</str:countMatches>
    <c:if test="${exitInMessagesProperties == 1}"><c:set var="libelle" value=""/></c:if>
</c:if>

    <%-- Si l'attribut checkAllAttribut est défini, on récupère la valeur correspondante dans le form --%>
    <c:if test="${(!empty  _formObjectName ) && (!empty  checkAllAttribut )}">
        <spring:bind path="${_formObjectName}.${checkAllAttribut}">
            <c:set var="checkedValue" value="${status.value}" />
        </spring:bind>
    </c:if>

<%-- Sélection de la classe du bloc div --%>
<c:choose>
    <c:when test="${status_error || status_warning}">
        <c:set var="_divclass" value="${_theme}${_erreurboxclass}"/>
        <c:if test="${empty _firsterrorid}">
            <c:set var="_firsterrorid" value="${_id}" scope="request"/> 
        </c:if>
    </c:when>
    <c:when test="${_readonly eq 'true'}">
        <c:set var="_divclass" value="${_theme}${_lectureboxclass}"/>
    </c:when>
    <c:when test="${_requis eq 'true'}">
        <c:set var="_divclass" value="${_theme}${_requisboxclass}"/>
    </c:when>
    <c:otherwise>
        <c:set var="_divclass" value="${_theme}${_normalboxclass}"/>
    </c:otherwise>
</c:choose>

<%-- Ajout d'un indicateur pour les champs obligatoires --%>
<c:choose>
    <c:when test="${_requis eq 'true'}">
        <c:set var="libelle" value="${libelle}${_requisindicateur}"/>
    </c:when>
    <c:otherwise>
        <c:set var="libelle" value="${libelle}${_normalindicateur}"/>
    </c:otherwise>
</c:choose>

<%-- Utilisation du hack "_" pour contourner l'absence de propriété min-height sous IE --%>

    <c:choose>
        <c:when test="${ _responsive eq 'false'}">
<div class="${_divclass}" style="width: ${_boxwidth};height: auto;_height: ${_boxheight};min-height: ${_boxheight};">
        </c:when>
        <c:otherwise>
            <div class="${_divclass}  form-group ${_bootstrapclass}">
        </c:otherwise>
    </c:choose>


    <%-- Insère le label associé au groupe de boutons radio --%>
    <%-- 
    <c:if test="${_affichelibelle eq 'true'}">


        <c:if test="${ _responsive eq 'false'}">
        <div class="${_labelboxclass}" style="width: ${_labelboxwidth};">
        </c:if>


            <c:if test="${! empty accesskey}">
                <c:set var="_complabel" value="accesskey='${accesskey}'"/>
            </c:if>
            <c:if test="${! empty consigne}">
                <c:set var="_complabel" value="${_complabel} title='${consigne}'"/>
            </c:if>
            <label class="${_labelclass} ${_bootstraplblclass}" for="${_id}" ${_complabel}>
            <c:if test="${status_error || status_warning}">
                    <fmt:message key="required" var="errorMsg">
                        <fmt:param value="${libelle}"/>
                    </fmt:message>
                    <img src="<c:url value='${_imagesrepertoire}/errInput.gif'/>"   alt="${fn:escapeXml(errorMsg)}" 
                                                                                  title="${fn:escapeXml(errorMsg)}"/>
            </c:if> 
        </label>
        <c:if test="${ _responsive eq 'false'}">
        </div>
    </c:if>
    </c:if>--%>
    <c:if test="${_affichelibelle eq 'false'}">
        <c:if test="${status_error || status_warning}">
             <fmt:message key="required" var="errorMsg">
                 <fmt:param value="${libelle}"/>
             </fmt:message>
             <img src="<c:url value='${_imagesrepertoire}/errInput.gif'/>"   alt="${fn:escapeXml(errorMsg)}" 
                                                                           title="${fn:escapeXml(errorMsg)}"/>
         </c:if>
    </c:if>
    
    <%-- Insère un bloc div qui contiendra l'ensemble des cases à  cocher --%>  

    <c:choose>
        <c:when test="${ _responsive eq 'false'}">
            <fieldset class="${_inputboxclass}" style="width: ${_inputboxwidth}; margin-left:${fieldsetMarginLeft}" id="${_id}">
        </c:when>
        <c:otherwise>
            <fieldset class="${_inputboxclass} ${_bootstrapgrpclass}" id="${_id}">
        </c:otherwise>
    </c:choose>
    <legend>${libelle}</legend>
        
    <%-- Traitement des cas où aucune collection d'items ou Map d'items ne sont utilisées --%>  
         <c:if test="${empty items && empty itemsmap && empty itemsset && empty itemslist}">
            <c:choose>  
                <c:when test="${! empty label}">
                    <c:set var="key" value="${label}"/>
                </c:when>
                <c:otherwise>
                    <c:set var="key" value=" "/>
                </c:otherwise>
            </c:choose>

            <c:choose>
                <%-- Si aucune collection ou map d'items n'est passée en paramètres et si 
                 l'attribut value n'est pas vide on suppose que la case à cocher renvoie 
                 la valeur de l'attribut value si elle est cochée. --%> 
                <c:when test="${! empty value}">
                    <jsp:useBean id="itemsunique" class="java.util.HashMap"/>
                    <c:set target="${itemsunique}" property="${value}"  value="${key}"/>
                </c:when>
                <%-- Si aucune collection ou map d'items n'est passée en paramètres et si 
                     l'attribut value est vide on suppose que la case à cocher renvoie TRUE si elle est cochée. --%> 
                <c:otherwise>
                    <jsp:useBean id="itemsbool" class="java.util.HashMap"/>
                    <c:set target="${itemsbool}" property="TRUE"  value="${key}"/>
                </c:otherwise>
            </c:choose>
        </c:if>
        
         <%-- Choix du type de collection Map, List ou Set--%>
        <c:choose>
            <c:when test="${! empty itemsmap}">
                <c:set var="items" value="${itemsmap}"/>
            </c:when>
            <c:when test="${! empty itemslist}">
              <c:set var="items" value="${itemslist}"/>
            </c:when>
            <c:when test="${! empty itemsset}">
              <c:set var="items" value="${itemsset}"/>
            </c:when>
            <c:when test="${! empty itemsbool}">
                  <c:set var="items" value="${itemsbool}"/>
            </c:when>
            <c:when test="${! empty itemsunique}">
                  <c:set var="items" value="${itemsunique}"/>
            </c:when>
        </c:choose>
        

        
        <c:if test="${_readonly ne 'true' && checkall eq 'true' && _checkallenhaut eq 'true'}">
            <c:forEach var="item" items="${items}">         
                <c:set var="cpt" value="${cpt + 1 }"/>
            </c:forEach>


                <label for="${_id}_checker" class="${_labelcheckallclass}" style="${checkAllStyle}"> <input type="checkbox"
                    id="${_id}_checker" name="${_id}_checker" checked="checked"
                    onclick="javascript:checkall('${_id}','${cpt}');${onclick};" class="${_checkallclass}" /> <c:set var="_libelleCase"
                        value="Tout sélectionner / désélectionner" /> <c:if test="${(!empty  checkAllLibelle)}">
                        <c:set var="_libelleCase" value="${checkAllLibelle}" />
                    </c:if>${_libelleCase}
                </label>


        </c:if>
        
    <%--Boucle sur les items de la collection contenant les choix possibles--%>

        <c:forEach var="item" items="${items}">         
                <c:set var="cptid" value="${cptid + 1 }"/>
                <%-- En fonction du type de collection et des paramètres label et value--%>
                <%-- détermination des valeurs utilisées pour chaque bouton radio--%>
            <c:choose>
                <%-- valeur de la case à cocher =  clé de la Map
                     label de la case à cocher = propriété ${label} de l'objet de la Map --%>
                <c:when test="${! empty itemsmap && ! empty label && empty consignelabel}">
                    <c:set var="lbl" value="${item['value'].label}" />
                    <c:set var="val" value="${item['key']}" />
                </c:when>
                 <%-- valeur de la case à cocher =  clé de la Map
                     label de la case à cocher = propriété ${label} de l'objet de la Map --%>
                <c:when test="${! empty itemsmap && ! empty label && ! empty consignelabel}">
                    <c:set var="lbl" value="${item['value'].label}" />
                    <c:set var="consignelbl" value="${item['value'].consignelabel}" />
                    <c:set var="val" value="${item['key']}" />
                </c:when>
                <%-- valeur de la case à cocher =  clé de la Map
                     label de la case à cocher = valeur de la méthode toString() de l'objet de la Map--%>
                <c:when test="${! empty itemsmap && empty label}">
                    <c:set var="lbl" value="${item['value']}" />
                    <c:set var="val" value="${item['key']}" />
                </c:when>
                <%-- valeur de la case à cocher = propriété ${value} d'un item de la collection
                     label de la case à cocher = propriété ${label} d'un item de la collection --%>
                <c:when test="${! empty itemsset && ! empty label && ! empty value && empty consignelabel}">
                    <c:set var="lbl" value="${item[label]}" />
                    <c:set var="val" value="${item[value]}" />
                </c:when>
                <%-- valeur de la case à cocher = propriété ${value} d'un item de la collection
                     label de la case à cocher = propriété ${label} d'un item de la collection --%>
                <c:when test="${! empty itemsset && ! empty label && ! empty value &&! empty consignelabel}">
                    <c:set var="lbl" value="${item[label]}" />
                     <c:set var="consignelbl" value="${item[consignelabel]}" />
                    <c:set var="val" value="${item[value]}" />
                </c:when>
                <%-- valeur de la case à cocher = propriété ${value} d'un item de la collection
                     label de la case à cocher = propriété ${value} d'un item de la collection --%>
                <c:when test="${! empty itemsset && empty label && ! empty value }">
                    <c:set var="lbl" value="${item[value]}" />                   
                    <c:set var="val" value="${item[value]}" />
                </c:when>
                <%-- valeur de la case à cocher = propriété ${value} d'un item de la collection
                     label de la case à cocher = propriété ${label} d'un item de la collection --%>
                <c:when test="${! empty itemslist && ! empty label && ! empty value && empty consignelabel}">
                    <c:set var="lbl" value="${item[label]}" />
                    <c:set var="val" value="${item[value]}" />
                </c:when>
                <%-- valeur de la case à cocher = propriété ${value} d'un item de la collection
                     label de la case à cocher = propriété ${label} d'un item de la collection --%>
                <c:when test="${! empty itemslist && ! empty label && ! empty value && ! empty consignelabel}">
                    <c:set var="lbl" value="${item[label]}" />
                    <c:set var="consignelbl" value="${item[consignelabel]}" />
                    <c:set var="val" value="${item[value]}" />
                </c:when>
                <%-- valeur de la case à cocher = propriété ${value} d'un item de la collection
                     label de la case à cocher = propriété ${value} d'un item de la collection --%>
                <c:when test="${! empty itemslist && empty label && ! empty value}">
                    <c:set var="lbl" value="${item[value]}" />
                    <c:set var="val" value="${item[value]}" />
                </c:when>
                <c:when test="${! empty itemsbool}">
                    <c:set var="lbl" value="${item['value']}" />
                    <c:set var="val" value="${item['key']}" />
                </c:when>
                <c:when test="${! empty itemsunique}">
                    <c:set var="lbl" value="${item['value']}" />
                    <c:set var="val" value="${item['key']}" />
                </c:when>
                <%-- valeur de la case à cocher = valeur de la méthode toString () d'un item de la collection
                     label de la case à cocher = valeur de la méthode toString () d'un item de la collection --%>
                <c:otherwise>
                    <c:set var="lbl" value="${item}" />
                    <c:set var="val" value="${item}" />
                </c:otherwise>
            </c:choose>

        <c:if test="${ _responsive eq 'true' && _checkboxclass eq 'checkbox' }">
            <c:set var="_checkboxclass" value="" />
        </c:if>
        <c:if test="${ _responsive eq 'true' && _checkallclass eq 'checkbox' }">
            <c:set var="_checkallclass" value="" />
        </c:if>

            <%-- Insère le label associé à  chaque case à  cocher --%>  
            <label for="${_id}_${cptid}" class="${_labelcheckboxclass}" title="${consignelbl}">
            <%-- Insère la case à  cocher --%>
            <html:checkbox path="${_path}"  value="${val}" class="${_checkboxclass}">
                <jsp:attribute name="ignorenullnestedpath">${_ignorenullnestedpath}</jsp:attribute>
                <jsp:attribute name="type">multibox</jsp:attribute>
                <jsp:attribute name="id">${_id}_${cptid}</jsp:attribute>
                <jsp:attribute name="class">${_checkboxclass}</jsp:attribute>
                <jsp:attribute name="readOnly">${_readonly}</jsp:attribute>
                <jsp:attribute name="tabindex">${tabindex}</jsp:attribute>
                <jsp:attribute name="onfocus">${onfocus}</jsp:attribute>
                <jsp:attribute name="onblur">${onblur}</jsp:attribute>
                <jsp:attribute name="onselect">${onselect}</jsp:attribute>
                <jsp:attribute name="onchange">${onchange}</jsp:attribute> 
                <jsp:attribute name="onkeypress">${_onkeypress}</jsp:attribute> 
                <jsp:attribute name="onclick">${onclick}</jsp:attribute> 
                <jsp:attribute name="ondblclick">${ondblclick}</jsp:attribute>              
                <jsp:attribute name="onmousedown">${onmousedown}</jsp:attribute>
                <jsp:attribute name="onmouseup">${onmouseup}</jsp:attribute>
                <jsp:attribute name="onmouseover">${onmouseover}</jsp:attribute>
                <jsp:attribute name="onmousemove">${onmousemove}</jsp:attribute>
                <jsp:attribute name="onmouseout">${onmouseout}</jsp:attribute>
                <jsp:attribute name="onkeydown">${onkeydown}</jsp:attribute>
                <jsp:attribute name="onkeyup">${onkeyup}</jsp:attribute> 
            </html:checkbox>${lbl}</label>
        </c:forEach>
        
        <c:if test="${_readonly ne 'true' && checkall eq 'true' && _checkallenhaut eq 'false'}">

                <label for="${_id}_checker" class="${_labelcheckallclass}" style="${checkAllStyle}"> <input type="checkbox"
                    id="${_id}_checker" name="${_id}_checker" checked="checked" 
                    onclick="javascript:checkall('${_id}','${cptid}');${onclick};" class="${_checkallclass}" /> <c:set var="_libelleCase"
                        value="Tout sélectionner / désélectionner" /> <c:if test="${(!empty  checkAllLibelle)}">
                        <c:set var="_libelleCase" value="${checkAllLibelle}" />
                    </c:if>${_libelleCase}
                </label>

        </c:if>
    </fieldset>
    <%-- Insère les éléments présents dans le corps du tag --%>
    <%-- Lecture du corps du Tag--%>
    <jsp:doBody var="resbody"/>
    <c:if test="${! empty resbody}">
        <c:choose>
            <c:when test="${ _responsive eq 'false'}">
        <div class="${_compboxclass}"  style="width: ${_compboxwidth};">${resbody}</div>
            </c:when>
            <c:otherwise>
                <div class="${_compboxclass}">${resbody}</div>
            </c:otherwise>
        </c:choose>
    </c:if>
</div>          

<%-- fin du test d'évaluation du tag--%>
</c:if>