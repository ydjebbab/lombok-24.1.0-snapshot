<%@ tag dynamic-attributes="attributes" 
        isELIgnored="false" 
        pageEncoding="UTF-8"
        description="Affiche un popup permettant d'afficher des fichiers pdf"%>
        
<%@ include file="/META-INF/resources/WEB-INF/tags/html/includes.tagf" %>

<%@ attribute name="prefixurlpdf" 
              required="false"
              description="Prefix de l'Url permettant d'accéder au pdf à afficher"%>

<%@ attribute name="pdfs" 
              required="false"
              description="Complément de l'url permettant d'accéder aux pdf à afficher, sous forme de la liste des noms de fichiers, séparés par le caractère '#'. Exemple :'dic02.pdf#dic03.pdf'"%>
              
<%@ attribute name="displayonload" 
              required="false"
              description="Affiche le popup lors du chargement de la page. Par defaut = true"%>              

<%@ attribute name="id" 
              required="false"
              description="id du popup. Par défaut = boitepdf"%>
              
<%@ attribute name="idmsg" 
              required="false"
              description="id du bloc div qui contient les messages à afficher. Par défaut = poppdfid"%>

<%@ attribute name="imagesrepertoire" 
              description="Répertoire contenant les images utilisées par le tag. Par défaut = /composants/dhtmlpopup/img" %>
<%@ attribute name="libelle" 
              description="Libelle du lien qui permet de reafficher le popup. Par defaut : Consulter les PDFs" %>              
<%@ attribute name="titre" 
              description="Titre du popup. Par défaut = Document pdf" %>
<%@ attribute name="backgroundcolor" 
              description="couleur arrière-plan du popup. Par défaut = white" %>
<%@ attribute name="textcolor" 
              description="couleur du texte. Par défaut = black" %>
<%@ attribute name="textfont" 
              description="Police du texte. Par défaut = bold 10pt verdana" %>
<%@ attribute name="titrebackgroundcolor" 
              description="Police du texte. Par défaut = navy" %>              
<%@ attribute name="titrecolor" 
              description="Couleur du texte du titre. Par défaut = white" %>              
<%@ attribute name="bordercolor" 
              description="Couleur de la bordure. Par défaut = #dddddd" %>              
<%@ attribute name="scrollbarcolor" 
              description="Couleur de la barre de scroll. Par défaut = gray" %>              
<%@ attribute name="inputclass"
              description="Classe css des boutons input"%>              

<%@ attribute name="maxwidth" 
              description="Largeur maximale en pixel du popup. Par défaut = 500" %>              
<%@ attribute name="maxheight" 
              description="Hauteur maximale en pixel du popup. Par défaut = 300" %>  
                            
<%-- Initialisation des paramètres par défaut --%>
<app:initvar attrname="_id" attrvalue="${id}" bundlekey="app.disppdf.id" defaultvalue="boitepdf"/>
<app:initvar attrname="_idmsg" attrvalue="${idmsg}" bundlekey="app.disppdf.idmsg" defaultvalue="poppdfid"/>
<app:initvar attrname="_imagesrepertoire" attrvalue="${imagesrepertoire}" bundlekey="app.disppdf.imagesrepertoire" defaultvalue="/composants/dhtmlpopup/img"/>
<app:initvar attrname="_titre" attrvalue="${titre}" bundlekey="app.disppdf.titre" defaultvalue="Document pdf"/>
<app:initvar attrname="_backgroundcolor" attrvalue="${backgroundcolor}" bundlekey="app.disppdf.backgroundcolor" defaultvalue="white"/>
<app:initvar attrname="_textcolor" attrvalue="${textcolor}" bundlekey="app.disppdf.textcolor" defaultvalue="black"/>
<app:initvar attrname="_textfont" attrvalue="${textfont}" bundlekey="app.disppdf.textfont" defaultvalue="bold 10pt verdana"/>
<app:initvar attrname="_titrebackgroundcolor" attrvalue="${titrebackgroundcolor}" bundlekey="app.disppdf.titrebackgroundcolor" defaultvalue="navy"/>
<app:initvar attrname="_titrecolor" attrvalue="${titrecolor}" bundlekey="app.disppdf.titrecolor" defaultvalue="white"/>
<app:initvar attrname="_bordercolor" attrvalue="${bordercolor}" bundlekey="app.disppdf.bordercolor" defaultvalue="#dddddd"/>
<app:initvar attrname="_scrollbarcolor" attrvalue="${scrollbarcolor}" bundlekey="app.disppdf.scrollbarcolor" defaultvalue="gray"/>
<app:initvar attrname="_maxwidth" attrvalue="${maxwidth}" bundlekey="app.disppdf.maxwidth" defaultvalue="500"/>
<app:initvar attrname="_maxheight" attrvalue="${maxheight}" bundlekey="app.disppdf.maxheight" defaultvalue="300"/>
<app:initvar attrname="_displayonload" attrvalue="${displayonload}" bundlekey="app.disppdf.displayonload" defaultvalue="true"/>

<jsp:doBody var="contentTag"/>

<%--Affiche une liste de boutons permettant de choisir le document à afficher --%>

<c:if test="${! empty prefixurlpdf && ! empty pdfs}">

<a href="javascript:PumPdfPopup.pumpdfShowbox();"><jsp:doBody/></a>

<c:set var="_nearid" value="${_idmsg}"/>
<script type="text/javascript">
/*<![CDATA[*/
var _dynarch_PumPdfPopup = new PumPdfPopup('${_nearid}', <%-- id de l'objet qui sert de référence pour positionner le popup --%>
                        '${_id}',      				<%-- id_de_la_popup --%>
                        '${_backgroundcolor}',      <%-- couleur arrière-plan popup --%>
                        '${_textcolor}',      		<%-- couleur texte --%>
                        '${_textfont}', 			<%-- police --%>
                        '${_titre}',				<%-- titre --%>
                        '${_titrebackgroundcolor}', <%-- couleur arrière-plan du titre --%>
                        '${_titrecolor}',			<%-- couleur titre --%>
                        '${_bordercolor}',    		<%-- couleur bordure --%>
                        '${_scrollbarcolor}',		<%-- couleur barre de scroll --%> 
                        '<c:url value="${_imagesrepertoire}"/>',<%--  Répertoire de stockage des images --%>
                        '${_maxwidth}',  <%-- Largeur maximale du popup en pixel--%>
                        '${_maxheight}',  <%-- hauteur maximale du popup en pixel--%>
                        '${prefixurlpdf}', <%-- URL de base à partir de laquelle les PDF sont chargés --%>
                        '${pdfs}' <%-- Liste des pdfs, séparés par un #--%>
                        ); 	     

<c:if test="${_displayonload eq true}">
PumPdfPopup.addEvent(window,'load',PumPdfPopup.objectpopuppdf);
</c:if>
                        
/*]]>*/
</script>

</c:if>

