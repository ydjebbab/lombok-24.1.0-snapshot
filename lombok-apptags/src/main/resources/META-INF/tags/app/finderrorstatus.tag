<%@ tag dynamic-attributes="attributes" 
        isELIgnored="false" 
        pageEncoding="UTF-8"
        description="Retourne le status d'un champ"
       %>
<%@ include file="/META-INF/resources/WEB-INF/tags/html/includes.tagf" %>

<%@ attribute name="varname" 
              required="true" rtexprvalue="false" type="java.lang.String"
              description="Nom de la variable à créer." %>

<%@ attribute name="attributname" 
              required="false"
              description="Nom de l'attribut dont on recherche le code erreur"%>

<%@ attribute name="id" 
              required="false" rtexprvalue="true" 
              description="Id du champ de saisie"%>

<%-- Variables exposées --%>
<%@ variable name-from-attribute="varname" 
             alias="result"
             scope="AT_BEGIN" 
             description="Expose une variable à partir de l'attribut attrname" %>
             
<%-- Si le champ n'est associé à aucun attribut, on ne fait rien--%>
<c:if test="${! (empty attributname)}">

<%-- Utilisation de la variable _formObjectName exposée par le tag app:form --%>
<c:if test="${!empty  _formObjectName}">
<c:set var="_path" value="${_formObjectName}.${attributname}"/>
</c:if>

<%-- Remplit la variable status_error si nécessaire --%>
<%-- Traitement de l'exception NullValueInNestedPathException 
     A remplacer par le paramètre ignoreNestedPath dans le spring:bind 
     à partir de la version 2.1 de spring
--%>
<c:choose>
	<c:when test="${_ignorenullnestedpath == 'true'}">
		<c:catch var="bindexcep" >
			<spring:bind path="${_path}">
				<c:set var="result" value="${status.error}"/>
			</spring:bind>
		</c:catch>

		<c:if test="${bindexcep.getClass().name != 'org.springframework.beans.NullValueInNestedPathException' }">
			${bindexcep.message}
		</c:if>
	</c:when>
	<c:otherwise>
		<spring:bind path="${_path}">
			<c:set var="result" value="${status.error}"/>
		</spring:bind>
	</c:otherwise>	
</c:choose>

<c:if test="${result}">
	<c:if test="${empty _firsterrorid}">
			<c:set var="_firsterrorid" value="${id}" scope="request"/> 
	</c:if>
	<c:set var="status_error" value="${result}" scope="request"/>
</c:if>

</c:if>