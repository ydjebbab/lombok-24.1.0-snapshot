<%@ tag dynamic-attributes="attributes" 
        isELIgnored="false" 
        pageEncoding="UTF-8"
        description="Insère un lien contenant une image ou un label."%>
        
<%@ include file="/META-INF/resources/WEB-INF/tags/html/includes.tagf" %>        
<%--Attributs principaux --%>
<%@ attribute name="action"
              description="Nom de l'action - URL qui permet d'accéder au contr&ocirc;leur."%>
<%@ attribute name="transition"
              description="Nom de la transition définie dans le webflow.;"%>
<%@ attribute name="uri"
              description="Uri vers laquelle pointe le lien. Utilisé si les paramètres action et transition ne sont pas utilisés."%>
<%@ attribute name="active"
              description="Attribut qui indique si le lien est actif."%>              
<%@ attribute name="attribute"
              description="Nom de l'attribut passé en paramètre"%>              
<%@ attribute name="value"
              description="Valeur de l'attribut passé en paramètre"%> 
<%@ attribute name="attributesupp"
              description="Nom de l'attribut supplémentaire passé en paramètre"%>              
<%@ attribute name="valuesupp"
              description="Valeur de l'attribut supplémentaire passé en paramètre"%>                           
<%@ attribute name="aclass"
              description="Classe css de l'élément 'a'"%>              
<%@ attribute name="image"
              description="Nom de l'image qui sert pour matérialiser le lien."%>
<%@ attribute name="imagesrepertoire"
              description="Répertoire de stockage des images"%>              
<%@ attribute name="title"
              description="Titre de l'élément href et de l'image"%>
<%@ attribute name="alt"
              description="Texte affiché si le navigateur n'affiche pas les images."%>              
<%@ attribute name="label"
              description="Libellé du lien "%>                            
<%@ attribute name="openwindow"
              description="true : ouvre une nouvelle fenêtre du navigateur. Par défaut : false "%>
<%@ attribute name="openwindowoptions"
              description="Options d'ouverture de la fenêtre du navigateur."%>
<%@ attribute name="openwindowname"
              description="Nom de la nouvelle fenêtre"%>              
<%@ attribute name="waitingmsg"
              description="true : affiche un message d'attente. Par défaut : false "%>
<%@ attribute name="displayed"
              description="true: le tag est évalué. Par défaut : true"%>

<%-- Affectation de valeurs par défaut aux attributs --%>
<app:initvar attrname="_displayed" attrvalue="${displayed}" bundlekey="app.link.displayed" defaultvalue="true"/>              
<app:initvar attrname="_imagesrepertoire" attrvalue="${imagesrepertoire}" bundlekey="app.page.imagesrepertoire" defaultvalue="/composants/tagsapp/images/"/>              
<app:initvar attrname="_aclass" attrvalue="${aclass}" bundlekey="app.link.aclass" defaultvalue="lien"/>
<app:initvar attrname="_openwindow" attrvalue="${openwindow}" bundlekey="app.link.openwindow" defaultvalue="false"/>
<app:initvar attrname="_openwindowname" attrvalue="${openwindowname}" bundlekey="app.link.openwindowname" defaultvalue=""/>
<app:initvar attrname="_openwindowoptions" attrvalue="${openwindowoptions}" bundlekey="app.link.openwindownoptions"
 defaultvalue="toolbar=yes, directories=no, location=no, status=yes, menubar=no, resizable=yes, scrollbars=yes, width=500, height=400"/>
<app:initvar attrname="_active" attrvalue="${active}" bundlekey="app.link.active" defaultvalue="true"/>
<app:initvar attrname="_waitingmsg" attrvalue="${waitingmsg}" bundlekey="app.link.waitingmsg" defaultvalue="false"/>

<%-- début du test d'évaluation du tag--%>
<c:if test="${_displayed == 'true' }">

<%-- correction pour éviter l'affichage de la valeur "null" sur Tomcat 6.0.20 et + --%>
<c:set var="_content" ><c:out value="${_content}" escapeXml="false" /></c:set>

<%-- Contenu du lien --%>
<c:if test="${!empty image}">
	<c:set var="_content"><img  src="<c:url value="${_imagesrepertoire}/${image}"/>" ${attrString}   alt="${alt}" style="border:0"/></c:set>
</c:if>

<c:if test="${!empty label}">
    <c:set var="_content"><c:out value="${_content}${label}" escapeXml="true"/></c:set> 
</c:if>

<%-- Insère le lien --%>
<c:choose>
	<%-- le lien est actif --%>
	<c:when test="${_active == true}">
		<%-- L'indication du lien est transmise sous la forme action / transition du webflow--%>
		<c:choose>
		<c:when test="${! empty action && ! empty transition}">
			  	<c:set var="_href" value="${pageContext.request.contextPath}/${action}?${FLOWEXECUTIONID_FIELD_NAME}=${FLOWEXECUTIONID_VALUE}&amp;${EVENTID_FIELDURL_PREFIX}=${transition}"/>
			  	<%-- Un couple attribut / valeur complète l'URL--%>
			  	<c:if test="${! empty attribute}">
		  			<c:set var="_href" value="${_href}&amp;${attribute}=${value}"/>
		  		</c:if>
		  		<c:if test="${! empty attributesupp}">
                    <c:set var="_href" value="${_href}&amp;${attributesupp}=${valuesupp}"/>
                </c:if>
		</c:when>
		<%-- L'indication du lien est transmise sous la forme d'une action --%>
		<c:when test="${! empty action &&  empty transition}">
	  	<c:set var="_href" value="${pageContext.request.contextPath}/${action}"/>

	  	<%-- Un couple attribut / valeur complète l'URL--%>
		  	<c:if test="${! empty attribute}">
		  			<c:set var="_href" value="${_href}&amp;${attribute}=${value}"/>
		  			<c:set var="_href" value="${_href}&amp;${attributesupp}=${valuesupp}"/>
	  		</c:if>
		</c:when>
		<%-- L'indication du lien est sous la forme d'une URL--%>
		<c:when test="${! empty uri}">
			<c:set var="_href" value="${uri}"/>
		</c:when>
		</c:choose>
		
   		<c:choose>
		    <c:when test="${_openwindow == true}">
			   <a title="${title}" href="javascript:send_url_openwindow('${_href}','${_openwindowname}','${_openwindowoptions}')" class="${aclass}">${_content}</a>
			</c:when>
			<c:when test="${_waitingmsg == true}">
				<a title="${title}" href="javascript:send_url_waiting('${_href}')" class="${aclass}">${_content}</a>
			</c:when>
			<c:otherwise>
				<a title="${title}" href="javascript:stocketransition('${transition}');send_url('${_href}')" class="${aclass}">${_content}</a>
			</c:otherwise>
		</c:choose>
	</c:when>
	
	<%-- le lien est inactif --%>
	<c:otherwise>
		${_content}
	</c:otherwise>	
</c:choose>

<%-- fin du test d'évaluation du tag--%>
</c:if>