<%@ tag dynamic-attributes="attributes" 
        isELIgnored="false" 
        pageEncoding="utf-8"
        description="Champs de saisie de type 'text' associé via un bind à l'objet contenant les données du formulaire."%>
        
<%@ include file="/META-INF/resources/WEB-INF/tags/html/includes.tagf" %>

<%--Attributs principaux --%>

<%@ attribute name="f1attribut" 
              required="false" 
              description="Nom de l'attribut associé au champs de saisie (input) dans l'objet support du formulaire. Utilisé dans le spring bind" %>              
<%@ attribute name="f1maxlength" 
              required="false"
              description="Champ 1 : longueur maximale qu'il est possible de saisir : propriété 'maxlength' de l'élément input"%>
<%@ attribute name="f1size" 
              required="false" 
              description="Champ 1 : Taille du champs de saisie. Position la propriété size dans l'élément input."%>              
<%@ attribute name="f1libelle" 
              required="false" 
              description="Champ 1 : libellé placé au dessous du champ"%>              
<%@ attribute name="f1consigne" 
              required="false" 
              description="Champ 1 : consigne de saisie"%>      


<%@ attribute name="f2attribut" 
              required="false" 
              description="Nom de l'attribut associé au champs de saisie (input) dans l'objet support du formulaire. Utilisé dans le spring bind" %>              
<%@ attribute name="f2maxlength" 
              required="false"
              description="Champ 2 : longueur maximale qu'il est possible de saisir : propriété 'maxlength' de l'élément input"%>
<%@ attribute name="f2size" 
              required="false" 
              description="Champ 2 : Taille du champs de saisie. Position la propriété size dans l'élément input."%>              
<%@ attribute name="f2libelle" 
              required="false" 
              description="Champ 2 : libellé placé au dessous du champ"%>              
<%@ attribute name="f2consigne" 
              required="false" 
              description="Champ 2 : consigne de saisie"%>

<%@ attribute name="f3attribut" 
              required="false" 
              description="Nom de l'attribut associé au champs de saisie (input) dans l'objet support du formulaire. Utilisé dans le spring bind" %>              
<%@ attribute name="f3maxlength" 
              required="false"
              description="Champ 3 : longueur maximale qu'il est possible de saisir : propriété 'maxlength' de l'élément input"%>
<%@ attribute name="f3size" 
              required="false" 
              description="Champ 3 : Taille du champs de saisie. Position la propriété size dans l'élément input."%>              
<%@ attribute name="f3libelle" 
              required="false" 
              description="Champ 3 : libellé placé au desspus du champ"%>              
<%@ attribute name="f3consigne" 
              required="false" 
              description="Champ 2 : consigne de saisie"%>


<%@ attribute name="f4attribut" 
              required="false" 
              description="Nom de l'attribut associé au champs de saisie (input) dans l'objet support du formulaire. Utilisé dans le spring bind" %>              
<%@ attribute name="f4maxlength" 
              required="false"
              description="Champ 4 : longueur maximale qu'il est possible de saisir : propriété 'maxlength' de l'élément input"%>
<%@ attribute name="f4size" 
              required="false" 
              description="Champ 4 : Taille du champs de saisie. Position la propriété size dans l'élément input."%>              
<%@ attribute name="f4libelle" 
              required="false" 
              description="Champ 4 : libellé placé au dessous du champ"%>              
<%@ attribute name="f4consigne" 
              required="false" 
              description="Champ 4 : consigne de saisie"%>


<%@ attribute name="f5attribut" 
              required="false" 
              description="Nom de l'attribut associé au champs de saisie (input) dans l'objet support du formulaire. Utilisé dans le spring bind" %>              
<%@ attribute name="f5maxlength" 
              required="false"
              description="Champ 5 : longueur maximale qu'il est possible de saisir : propriété 'maxlength' de l'élément input"%>
<%@ attribute name="f5size" 
              required="false" 
              description="Champ 5 : Taille du champs de saisie. Position la propriété size dans l'élément input."%>              
<%@ attribute name="f5libelle" 
              required="false" 
              description="Champ 5 : libellé placé au dessous du champ"%>              
<%@ attribute name="f5consigne" 
              required="false" 
              description="Champ 5 : consigne de saisie"%>
              
<%@ attribute name="f6attribut" 
              required="false" 
              description="Nom de l'attribut associé au champs de saisie (input) dans l'objet support du formulaire. Utilisé dans le spring bind" %>              
<%@ attribute name="f6maxlength" 
              required="false"
              description="Champ 6 : longueur maximale qu'il est possible de saisir : propriété 'maxlength' de l'élément input"%>
<%@ attribute name="f6size" 
              required="false" 
              description="Champ 6 : Taille du champs de saisie. Position la propriété size dans l'élément input."%>              
<%@ attribute name="f6libelle" 
              required="false" 
              description="Champ 6 : libellé placé au dessous du champ"%>                                          
<%@ attribute name="f6consigne" 
              required="false" 
              description="Champ 6 : consigne de saisie"%>

<%@ attribute name="f7attribut" 
              required="false" 
              description="Nom de l'attribut associé au champs de saisie (input)dans l'objet support du formulaire. Utilisé dans le spring bind" %>              
<%@ attribute name="f7maxlength" 
              required="false"
              description="Champ 7 : longueur maximale qu'il est possible de saisir : propriété 'maxlength' de l'élément input"%>
<%@ attribute name="f7size" 
              required="false" 
              description="Champ 7 : Taille du champs de saisie. Position la propriété size dans l'élément input."%>              
<%@ attribute name="f7libelle" 
              required="false" 
              description="Champ 7 : libellé placé au dessous du champ"%>                                          
<%@ attribute name="f7consigne" 
              required="false" 
              description="Champ 7 : consigne de saisie"%>
              
<%@ attribute name="f8attribut" 
              required="false" 
              description="Nom de l'attribut associé au champs de saisie (input) dans l'objet support du formulaire. Utilisé dans le spring bind" %>              
<%@ attribute name="f8maxlength" 
              required="false"
              description="Champ 8 : longueur maximale qu'il est possible de saisir : propriété 'maxlength' de l'élément input"%>
<%@ attribute name="f8size" 
              required="false" 
              description="Champ 8 : Taille du champs de saisie. Position la propriété size dans l'élément input."%>              
<%@ attribute name="f8libelle" 
              required="false" 
              description="Champ 8 : libellé placé au dessous du champ"%>                                          
<%@ attribute name="f8consigne" 
              required="false" 
              description="Champ 8 : consigne de saisie"%>
              
<%@ attribute name="f9attribut" 
              required="false" 
              description="Nom de l'attribut associé au champs de saisie (input) dans l'objet support du formulaire. Utilisé dans le spring bind" %>              
<%@ attribute name="f9maxlength" 
              required="false"
              description="Champ 9 : longueur maximale qu'il est possible de saisir : propriété 'maxlength' de l'élément input"%>
<%@ attribute name="f9size" 
              required="false" 
              description="Champ 9 : Taille du champs de saisie. Position la propriété size dans l'élément input."%>              
<%@ attribute name="f9libelle" 
              required="false" 
              description="Champ 9 : libellé placé au dessous du champ"%>                                          
<%@ attribute name="f9consigne" 
              required="false" 
              description="Champ 9 : consigne de saisie"%>
              
<%@ attribute name="f10attribut" 
              required="false" 
              description="Nom de l'attribut associé au champs de saisie (input) dans l'objet support du formulaire. Utilisé dans le spring bind" %>              
<%@ attribute name="f10maxlength" 
              required="false"
              description="Champ 10 : longueur maximale qu'il est possible de saisir : propriété 'maxlength' de l'élément input"%>
<%@ attribute name="f10size" 
              required="false" 
              description="Champ 10 : Taille du champs de saisie. Position la propriété size dans l'élément input."%>              
<%@ attribute name="f10libelle" 
              required="false" 
              description="Champ 10 : libellé placé au dessous du champ"%>                                          
<%@ attribute name="f10consigne" 
              required="false" 
              description="Champ 10 : consigne de saisie"%>                                          

              
<%@ attribute name="readonly"
              description="true : champs en lecture seule"%>
<%@ attribute name="disabled"
              description="true : champs inactif"%>       
<%@ attribute name="disabledwhenreadonly"
              description="true : quand un champ est en readonly, il est aussi disabled"%>                            
<%@ attribute name="libelle" 
              required="false" 
              description="Label associé au champs de saisie (input)"%>
<%@ attribute name="requis" 
              description="true : champs de saisie obligatoire.  Valeur par défaut = true "%>
<%@ attribute name="ignorenullnestedpath" 
              description="true : Ignore les références nulles lors de l'acc&egrave;s &egrave; un attribut  .  Valeur par défaut = true "%>
<%@ attribute name="displayed"
              description="true: le tag est évalué. Par défaut : false"%>

<%--Attributs de présentation --%>
<%@ attribute name="theme"
              description="Thème de présentation. Par défaut = V"%>
<%@ attribute name="boxheight"
              description="Hauteur du bloc div contenant l'ensemble des éléments relatifs à un champs de saisie."%>
<%@ attribute name="boxwidth"
              description="Largeur du bloc div contenant l'ensemble des éléments relatifs à un champs de saisie."%>
<%@ attribute name="labelboxwidth"
              description="Largeur du bloc div contenant le libelle (en % de la largeur totale du bloc parent)"%>
<%@ attribute name="inputboxwidth"
              description="Largeur du bloc div contenant le champ de saisie (en % de la largeur totale du bloc parent)"%>
<%@ attribute name="compboxwidth"
              description="Largeur du bloc div contenant la zone complémentaire (en % de la largeur totale du bloc parent)"%>

<%@ attribute name="normalboxclass"
              description="Classe css du bloc div contenant l'ensemble des éléments relatifs à un champs de saisie. Valeur par défaut = normal "%>
<%@ attribute name="lectureboxclass"
              description="Classe css du bloc div contenant l'ensemble des éléments relatifs à un champs de saisie, en lecture seule. Valeur par défaut = lectureseule."%>
<%@ attribute name="erreurboxclass"
              description="Classe css du bloc div contenant l'ensemble des éléments relatifs à un champs de saisie en erreur. Valeur par défaut = erreur."%>
<%@ attribute name="requisboxclass"
              description="Classe css du bloc div contenant l'ensemble des éléments relatifs à un champs de saisie obligatoire. Valeur par défaut = requis."%>
<%@ attribute name="labelboxclass"
              description="Classe css du bloc div contenant le libellé.Valeur par défaut = boxlblText."%>
<%@ attribute name="inputboxclass"
              description="Classe css du bloc div contenant le champs de saisie.Valeur par défaut = boxText."%>
<%@ attribute name="compboxclass"
              description="Classe css du bloc div contenant des données complémentaires sur le champs de saisie.Valeur par défaut = boxCompText."%>
<%@ attribute name="inputclass"
              description="Classe css du champs input. Valeur par défaut = inputText"%>            
<%@ attribute name="labelclass"
              description="Classe css de l'élément label. Valeur par défaut = labelText"%>            

<%@ attribute name="requisindicateur"
              description="Chaîne de caractéres indiquant qu'un message est requis. Valeur par défaut = *"%>            
<%@ attribute name="normalindicateur"
              description="Chaîne de caractéres indiquant qu'un message est optionnel. Valeur par défaut = Chaîne vide"%>                          
              
<%--Attributs optionnels --%>
<%@ attribute name="consigne" 
              description="Correspond à l'attribut 'title' de l'élément label. Peut contenir, par exemple, une consigne de saisie du champs. "%>

<%-- Attributs  --%>
<%@ attribute name="tabindex"
              description="Numéro indiquant l'ordre d'accès par tabulation."%>
<%@ attribute name="accesskey"
              description="Touche de raccourci pour accéder à un champs."%>
<%@ attribute name="onfocus"
              description="Script exécuté lors que l'élément est accédé."%>
<%@ attribute name="onblur"
              description="Script exécuté lors que l'élément est quitté."%>
<%@ attribute name="onselect"
              description="Script exécuté lorsqu'une portion de texte est sélectionnée."%>
<%@ attribute name="onchange"
              description="Script exécuté lors que l'élément est quitté et que son contenu a été modifié."%>

<%-- Attributs événements intrinsèques HTML --%>
<%@ attribute name="onclick"
              description="Script exécuté quand le bouton du dispositif de pointage est cliqué au-dessus d'un élément."%>
<%@ attribute name="ondblclick"
              description="Script exécuté quand le bouton du dispositif de pointage est double-cliqué au-dessus d'un élément."%>              
<%@ attribute name="onmousedown"
              description="Script exécuté quand le bouton du dispositif de pointage est appuyé au-dessus d'un élément."%>              
<%@ attribute name="onmouseup"
              description="Script exécuté quand le bouton du dispositif de pointage est relaché au-dessus d'un élément."%>                            
<%@ attribute name="onmouseover"
              description="Script exécuté quand le bouton du dispositif de pointage est déplacé sur un élément."%>                                          
<%@ attribute name="onmousemove"
              description="Script exécuté quand le bouton du dispositif de pointage est déplacé alors qu'il est au-dessus d'un élément."%>
<%@ attribute name="onmouseout"
              description="Script exécuté quand le bouton du dispositif de pointage est déplacé en dehors d'un élément."%>                                                                      
<%@ attribute name="onkeypress"
              description="Script exécuté quand une touche est pressée puis relachée au-dessus d'un élément."%>
<%@ attribute name="onkeydown"
              description="Script exécuté quand une touche est gardée appuyée au-dessus d'un élément."%>              
<%@ attribute name="onkeyup"
              description="Script exécuté quand une touche est relachée au-dessus d'un élément."%>                            

<%@ attribute name="id"
              description="id de l'élément input. Par défaut : attribué automatiquement."%>

<%-- Affectation de valeurs par défaut aux attributs --%>
<app:initvar attrname="_displayed" attrvalue="${displayed}" bundlekey="app.inputmultiple.displayed" defaultvalue="true"/>
<app:initvar attrname="_ignorenullnestedpath" attrvalue="${ignorenullnestedpath}" bundlekey="app.inputmultiple.ignorenullnestedpath" defaultvalue="true"/>
<app:initvar attrname="_requis" attrvalue="${requis}" bundlekey="app.inputmultiple.requis" defaultvalue="true"/>
<app:initvar attrname="_readonly" attrvalue="${readonly}" bundlekey="app.inputmultiple.readonly" defaultvalue="false"/>
<app:initvar attrname="_disabled" attrvalue="${disabled}" bundlekey="app.inputmultiple.disabled" defaultvalue=""/>
<app:initvar attrname="_disabledwhenreadonly" attrvalue="${disabledwhenreadonly}" bundlekey="app.inputmultiple.disabledwhenreadonly" defaultvalue="true"/>

<app:initvar attrname="_theme" attrvalue="${theme}" bundlekey="app.inputmultiple.theme" defaultvalue="V"/>
<app:initvar attrname="_boxheight" attrvalue="${boxheight}" bundlekey="app.inputmultiple.boxheight" defaultvalue="auto"/>
<app:initvar attrname="_boxwidth" attrvalue="${boxwidth}" bundlekey="app.inputmultiple.boxwidth" defaultvalue="100%"/>
<app:initvar attrname="_labelboxwidth" attrvalue="${labelboxwidth}" bundlekey="app.inputmultiple.labelboxwidth" defaultvalue="35%"/>
<app:initvar attrname="_inputboxwidth" attrvalue="${inputboxwidth}" bundlekey="app.inputmultiple.inputboxwidth" defaultvalue="50%"/>
<app:initvar attrname="_compboxwidth" attrvalue="${compboxwidth}" bundlekey="app.inputmultiple.compboxwidth" defaultvalue="15%"/>

<app:initvar attrname="_inputclass" attrvalue="${inputclass}" bundlekey="app.inputmultiple.inputclass" defaultvalue="inputText"/>
<app:initvar attrname="_labelclass" attrvalue="${labelclass}" bundlekey="app.inputmultiple.labelclass" defaultvalue="labelText"/>
<app:initvar attrname="_onkeypress" attrvalue="${onkeypress}" bundlekey="app.inputmultiple.onkeypress" defaultvalue="return submitEnterButton(event)"/>
<app:initvar attrname="_normalboxclass" attrvalue="${normalboxclass}" bundlekey="app.inputmultiple.normalboxclass" defaultvalue="normal"/>
<app:initvar attrname="_lectureboxclass" attrvalue="${lectureboxclass}" bundlekey="app.inputmultiple.lectureboxclass" defaultvalue="lecture"/>
<app:initvar attrname="_erreurboxclass" attrvalue="${erreurboxclass}" bundlekey="app.inputmultiple.erreurboxclass" defaultvalue="erreur"/>
<app:initvar attrname="_requisboxclass" attrvalue="${requisboxclass}" bundlekey="app.inputmultiple.requisboxclass" defaultvalue="requis"/>
<app:initvar attrname="_labelboxclass" attrvalue="${labelboxclass}" bundlekey="app.inputmultiple.labelboxclass" defaultvalue="boxlblText"/>
<app:initvar attrname="_inputboxclass" attrvalue="${inputboxclass}" bundlekey="app.inputmultiple.inputboxclass" defaultvalue="boxText"/>
<app:initvar attrname="_compboxclass" attrvalue="${compboxclass}" bundlekey="app.inputmultiple.compboxclass" defaultvalue="boxCompText"/>
<app:initvar attrname="_requisindicateur" attrvalue="${requisindicateur}" bundlekey="app.inputmultiple.requisindicateur" defaultvalue=" *"/>
<app:initvar attrname="_normalindicateur" attrvalue="${normalindicateur}" bundlekey="app.inputmultiple.normalindicateur" defaultvalue=""/>

<%@ variable name-given="_inputId"  
             variable-class="java.lang.String" declare="true" 
             scope="NESTED" 
             description="Indique &egrave; l'ensemble des tags inclus l'ID du champ input." %> 

<%-- début du test d'évaluation du tag--%>
<c:if test="${_displayed == 'true' }">

<%-- Détermination de l'ID du premier champ --%>

<app:id attrname="_f1id"/>
<app:id attrname="_f2id"/>
<app:id attrname="_f3id"/>
<app:id attrname="_f4id"/>
<app:id attrname="_f5id"/>
<app:id attrname="_f6id"/>
<app:id attrname="_f7id"/>
<app:id attrname="_f8id"/>
<app:id attrname="_f9id"/>
<app:id attrname="_f10id"/>

<app:finderrorstatus varname="_f1err" attributname="${f1attribut}" id="${_f1id}"/>
<app:finderrorstatus varname="_f2err" attributname="${f2attribut}" id="${_f2id}"/>
<app:finderrorstatus varname="_f3err" attributname="${f3attribut}" id="${_f3id}"/>
<app:finderrorstatus varname="_f4err" attributname="${f4attribut}" id="${_f4id}"/>
<app:finderrorstatus varname="_f5err" attributname="${f5attribut}" id="${_f5id}"/>
<app:finderrorstatus varname="_f6err" attributname="${f6attribut}" id="${_f6id}"/>
<app:finderrorstatus varname="_f7err" attributname="${f7attribut}" id="${_f7id}"/>
<app:finderrorstatus varname="_f8err" attributname="${f8attribut}" id="${_f8id}"/>
<app:finderrorstatus varname="_f9err" attributname="${f9attribut}" id="${_f9id}"/>
<app:finderrorstatus varname="_f10err" attributname="${f10attribut}" id="${_f10id}"/>

<app:findwarningstatus varname="_f1warn" attributname="${f1attribut}" id="${_f1id}"/>
<app:findwarningstatus varname="_f2warn" attributname="${f2attribut}" id="${_f2id}"/>
<app:findwarningstatus varname="_f3warn" attributname="${f3attribut}" id="${_f3id}"/>
<app:findwarningstatus varname="_f4warn" attributname="${f4attribut}" id="${_f4id}"/>
<app:findwarningstatus varname="_f5warn" attributname="${f5attribut}" id="${_f5id}"/>
<app:findwarningstatus varname="_f6warn" attributname="${f6attribut}" id="${_f6id}"/>
<app:findwarningstatus varname="_f7warn" attributname="${f7attribut}" id="${_f7id}"/>
<app:findwarningstatus varname="_f8warn" attributname="${f8attribut}" id="${_f8id}"/>
<app:findwarningstatus varname="_f9warn" attributname="${f9attribut}" id="${_f9id}"/>
<app:findwarningstatus varname="_f10warn" attributname="${f10attribut}" id="${_f10id}"/>

<%-- Création de la liste qui décrit les différents champs--%>
<c:set var="tabinp" value="${lb:buildInputMultipleArray(
								  f1attribut,f1maxlength,f1size,f1libelle,f1consigne,f1width,_f1id,_f1err,_f1warn,
								  f2attribut,f2maxlength,f2size,f2libelle,f2consigne,f2width,_f2id,_f2err,_f2warn,
								  f3attribut,f3maxlength,f3size,f3libelle,f3consigne,f3width,_f3id,_f3err,_f3warn,
								  f4attribut,f4maxlength,f4size,f4libelle,f4consigne,f4width,_f4id,_f4err,_f4warn,
								  f5attribut,f5maxlength,f5size,f5libelle,f5consigne,f5width,_f5id,_f5err,_f5warn,
								  f6attribut,f6maxlength,f6size,f6libelle,f6consigne,f6width,_f6id,_f6err,_f6warn,
								  f7attribut,f7maxlength,f7size,f7libelle,f7consigne,f7width,_f7id,_f7err,_f7warn,
								  f8attribut,f8maxlength,f8size,f8libelle,f8consigne,f8width,_f8id,_f8err,_f8warn,
								  f9attribut,f9maxlength,f9size,f9libelle,f9consigne,f9width,_f9id,_f9err,_f9warn,								  
								  f10attribut,f10maxlength,f10size,f10libelle,f10consigne,f10width,_f10id,_f10err,_f10warn
								)}"/>

<%-- Utilisation de la variable _formReadOnly exposée par le tag app:form --%>
<c:if test="${!empty  _formReadOnly ||  _readonly eq 'true'}">
<c:set var="_readonly" value="true" scope="request"/>
</c:if>    
    
<%-- Lecture dans un fichier *.properties du libellé associé au champ du formulaire--%>
<c:if test="${empty libelle}">
	<fmt:message key="${attribut}" var="libelle" />
	<str:countMatches substring="???${attribut}???" var="exitInMessagesProperties">${libelle}</str:countMatches>
	<c:if test="${exitInMessagesProperties == 1}"><c:set var="libelle" value=""/></c:if>
</c:if>

<%-- Sélection de la classe du bloc div --%>
<c:choose>
	<c:when test="${status_error || status_warning}">
		<c:set var="_divclass" value="${_theme}${_erreurboxclass}"/>
		<c:if test="${empty _firsterrorid}">
			<c:set var="_firsterrorid" value="${_id}" scope="request"/> 
		</c:if>
	</c:when>
	<c:when test="${_readonly eq 'true'}">
		<c:set var="_divclass" value="${_theme}${_lectureboxclass}"/>
	</c:when>
	<c:when test="${_requis eq 'true'}">
		<c:set var="_divclass" value="${_theme}${_requisboxclass}"/>
	</c:when>
	<c:otherwise>
		<c:set var="_divclass" value="${_theme}${_normalboxclass}"/>
	</c:otherwise>
</c:choose>

<%-- Ajout d'un indicateur pour les champs obligatoires --%>
<c:choose>
	<c:when test="${_requis eq 'true'}">
		<c:set var="libelle" value="${libelle}${_requisindicateur}"/>
	</c:when>
	<c:otherwise>
		<c:set var="libelle" value="${libelle}${_normalindicateur}"/>
	</c:otherwise>
</c:choose>


<script type="text/javascript">
/*<![CDATA[*/
function splitOnPaste(pNumField)
{
	var numField = pNumField;
	var sizeArray = new Array();
	var idArray = new Array();
	var toCopyArray = new Array();
	<c:forEach var="index" begin="0" end="${fn:length(tabinp) - 1}"> 
		sizeArray[${index}] = ${tabinp[index][1]};
		idArray[${index}] = '${tabinp[index][4]}';
	</c:forEach>

	var maxTotalSize = 0;
	for (var i = numField; i < sizeArray.length ; i++)
		{
		maxTotalSize = maxTotalSize + sizeArray[i];
		}
		
	if (window.event)
		{
		window.event.returnValue = false;
		}

	var sNewString;

	if (window.clipboardData)
		{
		sNewString = window.clipboardData.getData("Text");
		}
	var sPasteString = sNewString;
	for (var k = 0 ; k < sizeArray.length ; k++)
		{
		if (k >= numField && sPasteString.length > 0)
			{
			toCopyArray[k] = true;
			if (sPasteString.length >= sizeArray[k])
				{
				sPasteString = sPasteString.substring(sizeArray[k],sPasteString.length); 
				}
			else
				{
				sPasteString = "";
				}
			}
		else
			{
			toCopyArray[k] = false;
			}
		}

	var debut = 0;
	var fin = 0;

	for (var j = 0 ; j < sizeArray.length - numField ; j++)
	{
		fin = fin + sizeArray[j + numField];
		var id = idArray[j + numField];
		
		if (toCopyArray[j + numField])
			{
			document.getElementById(id).value = sNewString.substring(debut,fin);
			}
		debut = debut + sizeArray[j];
	}

if (maxTotalSize < sNewString.length)
   	{
   	alert('Le texte collé a été tronqué.');
	}
}

/*]]>*/
</script>

<%-- Utilisation du hack "_" pour contourner l'absence de propriété min-height sous IE --%>
<div class="${_divclass}" style="width: ${_boxwidth};height: auto;_height: ${_boxheight};min-height: ${_boxheight};">
    <%-- Insère le label associé aux champs de saisie--%>
    <div class="${_labelboxclass}" style="width: ${_labelboxwidth};">
	    <c:if test="${! empty accesskey}">
		    <c:set var="_complabel" value="accesskey='${accesskey}'"/>
	    </c:if>
	    <c:if test="${! empty consigne}">
		    <c:set var="_complabel" value="${_complabel} title='${consigne}'"/>
	    </c:if>
         <label class="${_labelclass}" ${_complabel}>${libelle}</label>
       </div>
	<table>
	<colgroup>
	 <c:forEach var="index" begin="0" end="${fn:length(tabinp) - 1}"> 
		<col width="5"/>
   	</c:forEach>
	</colgroup>
	<thead>  
	<tr>
    <%-- Insertion des champs de saisie--%>
    <c:forEach var="index" begin="0" end="${fn:length(tabinp) - 1}"> 
        <c:set var="_attribut" value="${tabinp[index][0]}"/> 
		<c:set var="_maxlength" value="${tabinp[index][1]}"/>
		<c:set var="_size" value="${tabinp[index][2]}"/>
		<c:set var="_id" value="${tabinp[index][4]}"/>
		<c:choose>
				<c:when test="${tabinp[index][5] || tabinp[index][6]}">
					<c:set var="tdclass" value="cellErreur"/>
				</c:when>
				<c:when test="${_readonly eq 'true'}">
					<c:set var="tdclass" value="cellLecture"/>
				</c:when>
				<c:otherwise>
					<c:set var="tdclass" value="cellNormal"/>
				</c:otherwise>
		</c:choose>
		
		<c:choose>
			<c:when test="${index < (fn:length(tabinp) - 1) }">
			<%-- 
				<c:set var="onkeyup" value="gotofieldaftermax(this.value,this,'${tabinp[index + 1][4]}')"/>--%>
				<c:set var="onkeyup" value="Autotab('${tabinp[index + 1][4]}', this.size-1, this.value)"/>
			</c:when>
			<c:otherwise>
				<c:set var="onkeyup" value=""/>
			</c:otherwise>
		</c:choose>
		
		<td class="${tdclass}">
			<%-- Utilisation de la variable _formObjectName exposée par le tag app:form --%>
			<c:if test="${!empty  _formObjectName}">
			<c:set var="_path" value="${_formObjectName}.${_attribut}"/>
			</c:if>
			
			<html:input path="${_path}" onpaste="splitOnPaste(${index})" >
			      	<jsp:attribute name="ignorenullnestedpath">${_ignorenullnestedpath}</jsp:attribute>
			    	<jsp:attribute name="maxlength">${_maxlength}</jsp:attribute>
			    	<jsp:attribute name="size">${_size+1}</jsp:attribute>
			    	<jsp:attribute name="id">${_id}</jsp:attribute>
			    	<jsp:attribute name="class">${_inputclass}</jsp:attribute>
			    	<jsp:attribute name="readOnly">${_readonly}</jsp:attribute>
			    	<jsp:attribute name="disabled">${_disabled}</jsp:attribute>    
			    	<jsp:attribute name="disabledwhenreadonly">${_disabledwhenreadonly}</jsp:attribute>    	
			    	<jsp:attribute name="tabindex">${tabindex}</jsp:attribute>
			    	<jsp:attribute name="onfocus">${onfocus}</jsp:attribute>
			    	<jsp:attribute name="onblur">${onblur}</jsp:attribute>
			    	<jsp:attribute name="onselect">${onselect}</jsp:attribute>
			    	<jsp:attribute name="onchange">${onchange}</jsp:attribute>
					<jsp:attribute name="onkeypress">${_onkeypress}</jsp:attribute> 
					<jsp:attribute name="onclick">${onclick}</jsp:attribute> 
					<jsp:attribute name="ondblclick">${ondblclick}</jsp:attribute> 	   			
					<jsp:attribute name="onmousedown">${onmousedown}</jsp:attribute>
					<jsp:attribute name="onmouseup">${onmouseup}</jsp:attribute>
					<jsp:attribute name="onmouseover">${onmouseover}</jsp:attribute>
					<jsp:attribute name="onmousemove">${onmousemove}</jsp:attribute>
					<jsp:attribute name="onmouseout">${onmouseout}</jsp:attribute>
					<jsp:attribute name="onkeydown">${onkeydown}</jsp:attribute>
					<jsp:attribute name="onkeyup">${onkeyup}</jsp:attribute> 
			</html:input>
			     
			<%-- Place dans le contexte 'request' la variable _inputId --%>
			<c:if test="${! empty _id}">  
				<c:set var="_inputId" value="${_id}" scope="request"/>
			</c:if>
		
		</td>
   	</c:forEach>
	</tr>
	</thead>
	<tbody>
	<tr>		
       <c:forEach var="index" begin="0" end="${fn:length(tabinp) - 1}"> 
			<c:set var="_flibelle" value="${tabinp[index][3]}"/>
			<c:set var="_id" value="${tabinp[index][4]}"/>
			<c:choose>
				<c:when test="${tabinp[index][5]}">
					<c:set var="tdclass" value="cellErreur"/>
				</c:when>
				<c:when test="${_readonly eq 'true'}">
					<c:set var="tdclass" value="cellLecture"/>
				</c:when>
				<c:otherwise>
					<c:set var="tdclass" value="cellNormal"/>
				</c:otherwise>
			</c:choose>
			<c:choose>
		   <c:when test="${! empty tabinp[index][7]}">
			    <c:set var="_fcomplabel" value="title=\"${tabinp[index][7]}\""/>
	    	</c:when>
	    	<c:otherwise><c:set var="_fcomplabel" value=""/></c:otherwise>
	    	</c:choose>
			<td class="${tdclass}"><label class="${_labelclass}" ${_fcomplabel} for="${_id}">${_flibelle}</label></td>
   		</c:forEach>
	</tr>
	</tbody>
	</table>
    
    <%-- Insère les éléments présents dans le corps du tag --%>
    <%-- Lecture du corps du Tag--%>
	<jsp:doBody var="resbody"/>
		<c:if test="${! empty resbody}">
	        <div class="${_compboxclass}" style="width: ${_compboxwidth};">${resbody}</div>
		</c:if>
</div>

<%-- Supprime du contexte 'request' la variable _readonly --%>
<c:if test="${! empty readonly}">  
  <c:remove var="_readonly" scope="request"/>
</c:if>

<%-- Supprime du contexte 'request' la variable _inputId --%>
<c:if test="${! empty _id}">  
  <c:remove var="_id" scope="request"/>
   <c:remove var="status_error" scope="request"/>
</c:if>

<%-- fin du test d'évaluation du tag--%>
</c:if>
