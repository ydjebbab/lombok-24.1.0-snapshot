<%@ tag dynamic-attributes="attributes" 
        isELIgnored="false" 
        pageEncoding="UTF-8"
        description="Insère une image permettant d'afficher une consigne d'aide"%>
        
<%@ include file="/META-INF/resources/WEB-INF/tags/html/includes.tagf" %>
<%--Attributs principaux --%>
<%@ attribute name="label"
              description="Texte associé à l'image d'aide."%>
<%@ attribute name="aideclass"
              description="Classe css de l'élément 'a'. Par défaut = aide"%>

<app:initvar attrname="_imagesrepertoire" attrvalue="${imagesrepertoire}" bundlekey="app.page.imagesrepertoire" defaultvalue="/composants/tagsapp/images"/>
<app:initvar attrname="_imageaide" attrvalue="${imageaide}" bundlekey="app.aide.imageaide" defaultvalue="help.gif"/>
<app:initvar attrname="_aideclass" attrvalue="${aideclass}" bundlekey="app.aide.aideclass" defaultvalue="aide"/>
	
<img src="<c:url value="${_imagesrepertoire}/${_imageaide}"/>" alt="${label}" title="${label}"/>