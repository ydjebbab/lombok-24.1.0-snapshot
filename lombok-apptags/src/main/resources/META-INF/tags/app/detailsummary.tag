<%@ tag dynamic-attributes="attributes" isELIgnored="false"
	pageEncoding="UTF-8"
	description="Balise html5 DETAILs, permettant de caché un texte."%>

<%@ include file="/META-INF/resources/WEB-INF/tags/html/includes.tagf" %>

<%--Attributs principaux --%>
<%@ attribute name="title" description="texte affiché en info-bulle"%>

<%-- Affectation de valeurs par défaut aux attributs --%>
<app:initvar attrname="_title" attrvalue="${title}" bundlekey="app.details.summary.title" defaultvalue="Cliquer pour afficher ou cacher les détails" />
<summary title="${_title}">
    <jsp:doBody/>
</summary>	