<%@ tag dynamic-attributes="attributes" isELIgnored="false" 
 pageEncoding="UTF-8"
 description="Insertion d'une entité formulaire"
 display-name="formgroup"%>

<%@ include file="/META-INF/resources/WEB-INF/tags/html/includes.tagf" %>

<%@ attribute name="responsive"
    description="gestion responsive"%>
    
<%@ attribute name="formgroupclass"
    description="class of form group"%>

<%@ attribute name="labelname"
    description="class of form group"%>
    
<%@ attribute name="labelfor"
    description="labelbox style if no responsive"%>      
    
<%@ attribute name="labelclass"
    description="labelbox style if no responsive"%>
     
<%@ attribute name="labelboxstyle"
    description="labelbox style if no responsive"%>      
    
<%@ attribute name="boxclass"
    description="labelbox style if no responsive"%>
    
<%@ attribute name="inputname"
    description="labelbox style if no responsive"%>      
    
<%@ attribute name="maxlength"
    description="labelbox style if no responsive"%>
    
<%@ attribute name="size"
    description="labelbox style if no responsive"%>
    
<%@ attribute name="id"
    description="labelbox style if no responsive"%>          
    
<%@ attribute name="typename"
    description="labelbox style if no responsive"%>
    
<%@ attribute name="classname"
    description="labelbox style if no responsive"%>
    
<%@ attribute name="required"
    description="labelbox style if no responsive"%>    
    
<%@ attribute name="autocomplete"
    description="labelbox style if no responsive"%>
    
<%@ attribute name="placeholder"
    description="labelbox style if no responsive"%>       
        
  
<%@ attribute name="_responsive"
    description="gestion responsive"%>
    
<%@ attribute name="_formgroupclass"
    description="class of form group"%>

<%@ attribute name="_labelname"
    description="class of form group"%>
    
<%@ attribute name="_labelfor"
    description="labelbox style if no responsive"%>      
    
<%@ attribute name="_labelclass"
    description="labelbox style if no responsive"%>
     
<%@ attribute name="_labelboxstyle"
    description="labelbox style if no responsive"%>      
    
<%@ attribute name="_boxclass"
    description="labelbox style if no responsive"%>
    
<%@ attribute name="_inputname"
    description="labelbox style if no responsive"%>      
    
<%@ attribute name="_maxlength"
    description="labelbox style if no responsive"%>
    
<%@ attribute name="_size"
    description="labelbox style if no responsive"%>
    
<%@ attribute name="_id"
    description="labelbox style if no responsive"%>          
    
<%@ attribute name="_typename"
    description="labelbox style if no responsive"%>
    
<%@ attribute name="_classname"
    description="labelbox style if no responsive"%>
    
<%@ attribute name="_required"
    description="labelbox style if no responsive"%>    
    
<%@ attribute name="_autocomplete"
    description="labelbox style if no responsive"%>
    
<%@ attribute name="_placeholder"
    description="labelbox style if no responsive"%>       
        
  
<%@ include file="/META-INF/resources/WEB-INF/tags/app/responsive_test.tagf" %>

<c:if test="${! empty formgroupclass}">
    <c:choose>
    <c:when test="${_responsive eq 'false'}">
        <c:set var="_formgroupclass" value="class='${formgroupclass}'" />
    </c:when>
    <c:otherwise>
        <c:set var="_formgroupclass" value="class='${formgroupclass} form-group'" />
    </c:otherwise>
    </c:choose>
</c:if>


<c:if test="${(_responsive eq 'false') and  (! empty labelboxstyle)}">
    <c:set var="_labelboxstyle" value="style='${labelboxstyle}'" />
</c:if>

<c:if test="${! empty labelclass}">
    <c:set var="_labelclass" value="${labelclass}" />
</c:if>

<c:if test="${! empty boxclass}">
    <c:set var="_boxclass" value="${boxclass}" />
</c:if>

<c:if test="${! empty labelfor}">
    <c:set var="_labelfor" value="${labelfor}" />
</c:if>

<c:if test="${! empty maxlength}">
    <c:set var="_maxlength" value="maxlength='${maxlength}'" />
</c:if>

<c:if test="${! empty size}">
    <c:set var="_size" value="size='${size}'" />
</c:if>

<c:if test="${! empty typename}">
    <c:set var="_typename" value="type='${typename}'" />
</c:if>

<c:if test="${! empty classname}">
    <c:set var="_classname" value="class='${classname}'" />
</c:if>


<c:if test="${! empty id}">
    <c:set var="_id" value="id='${id}'" />
</c:if>

<c:if test="${! empty autocomplete}">
    <c:set var="_autocomplete" value="autocomplete='${autocomplete}'" />
</c:if>

<c:if test="${! empty placeholder}">
    <c:set var="_placeholder" value="placeholder='${placeholder}'" />
</c:if>

<c:if test="${! empty required}">
    <c:set var="_required" value="required='${required}'" />
</c:if>


<c:choose>
<c:when test="${_responsive eq 'false'}">
    <div ${_formgroupclass}>
        <div class="boxlblText" ${_labelboxstyle}>
            <label class="labelText" for="${labelfor}">${labelname}</label>
        </div>
        <div class="boxText">
            <input name="${inputname}" ${_classname} ${_maxlength} ${_size} ${_typename} ${_id} ${_autocomplete} ${_placeholder} ${_required} />
        </div>
    </div>
</c:when>
<c:otherwise>
    <div ${_formgroupclass}>
        <label class="control-label ${_labelclass}" for="${labelfor}">${labelname}</label>
        <div class="boxText ${_boxclass} ">
            <input name="${inputname}" ${_maxlength} ${_size} ${_typename} ${_id} ${_autocomplete} ${_placeholder} ${_required}
                class="form-control ${classname}" />
        </div>
    </div>
</c:otherwise>
</c:choose>





