<%@ tag dynamic-attributes="attributes" isELIgnored="false"
	pageEncoding="UTF-8"
	description="Balise html5 DETAILs, permettant de caché un texte."%>

<%@ include file="/META-INF/resources/WEB-INF/tags/html/includes.tagf" %>

<%--Attributs principaux --%>
<%@ attribute name="summary" description="texte affiché en en-tete"%>
<%@ attribute name="open"    description="true : les détails sont affichés, false: les détails sont cachés"%>

<%-- Affectation de valeurs par défaut aux attributs --%>
<app:initvar attrname="_open" attrvalue="${open}" bundlekey="app.details.open" defaultvalue="false" />

<details <c:if test="${_open == 'true' }"> open </c:if>>
	<jsp:doBody />
</details>