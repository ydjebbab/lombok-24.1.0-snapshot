/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 */
package fr.gouv.finances.lombok.atlas.jpa.dao.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaBuilder.In;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Fetch;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import fr.gouv.finances.lombok.atlas.bean.DocumentAtlas;
import fr.gouv.finances.lombok.atlas.bean.EtatDocumentAtlas;
import fr.gouv.finances.lombok.atlas.bean.TypeDocumentAtlas;
import fr.gouv.finances.lombok.atlas.dao.DocumentAtlasDao;
import fr.gouv.finances.lombok.atlas.dao.ProprieteDocumentAtlas;
import fr.gouv.finances.lombok.jpa.dao.BaseDaoJpaImpl;
import fr.gouv.finances.lombok.upload.bean.FichierJoint;
import fr.gouv.finances.lombok.util.persistance.ModeCritereRecherche;
import fr.gouv.finances.lombok.util.persistance.ScrollIterator;

/**
 * Implémentation JPA de la gestion des données Atlas.
 *
 * @author Christophe Breheret-Girardin
 */
@ConditionalOnProperty(name = "lombok.orm.jpa", havingValue="true")
@Repository("documentatlasdaoimpl")
@Transactional(transactionManager="transactionManager")
public class DocumentAtlasDaoImpl extends BaseDaoJpaImpl implements DocumentAtlasDao
{
    protected static final Logger LOGGER = LoggerFactory.getLogger(DocumentAtlasDaoImpl.class);

    public DocumentAtlasDaoImpl()
    {
        super();
    }

    /**
     * {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.atlas.dao.DocumentAtlasDao#findDocumentAtlasParIdentifiantAtlas( java.lang.String).
     */
    @Override
    public DocumentAtlas findDocumentAtlasParIdentifiantAtlas(String identifiantAtlas)
    {
        LOGGER.debug("Recherche du document Atlas d'identifiant '{}'", identifiantAtlas);

        // Si aucun paramètre, la recherche n'est pas exécutée
        if (StringUtils.isEmpty(identifiantAtlas))
        {
            return null;
        }

        // Préparation des critères
        Map<String, String> criteres = new HashMap<>();
        criteres.put(ProprieteDocumentAtlas.IDENTIFIANT.getNom(), identifiantAtlas);

        // Préparation des jointures
        Map<String, JoinType> jointures = new HashMap<>();
        jointures.put(ProprieteDocumentAtlas.PROPRIETE_TYPE_DOCUMENT.getNom(), JoinType.INNER);

        // Exécution de la requête
        return findByCriteriasWithJoins(DocumentAtlas.class, criteres, ModeCritereRecherche.EQUAL, jointures);
    }

    /**
     * {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.atlas.dao.DocumentAtlasDao#findListeDocumentAtlasParIdentifiantAtlas(
     *      java.util.Collection).
     */
    @Override
    public List<DocumentAtlas> findListeDocumentAtlasParIdentifiantAtlas(Collection<String> colIdentifiantAtlas)
    {
        LOGGER.debug("Recherche des documents Atlas d'identifiant '{}'", colIdentifiantAtlas);

        // Si aucun paramètre, la recherche n'est pas exécutée
        if (colIdentifiantAtlas == null || colIdentifiantAtlas.isEmpty())
        {
            return new ArrayList<>();
        }

        // Paramètrage de la recherche
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<DocumentAtlas> query = criteriaBuilder.createQuery(DocumentAtlas.class);
        Root<DocumentAtlas> root = query.from(DocumentAtlas.class);

        // Paramètrage des critères
        Path<Long> path = root.<Long> get(ProprieteDocumentAtlas.IDENTIFIANT.getNom());
        In<Object> criteresIdentifiant = criteriaBuilder.in(path);
        criteresIdentifiant.value(colIdentifiantAtlas);
        query.where(criteresIdentifiant);

        // Paramètrage des jointures
        //root.fetch(ProprieteDocumentAtlas.PROPRIETE_TYPE_DOCUMENT.getNom(), JoinType.INNER);

        // Exécution de la recherche
        return findAll(query);
    }

    /**
     * {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.atlas.dao.DocumentAtlasDao#findListeDocumentAtlasParType(
     *      fr.gouv.finances.lombok.atlas.bean.TypeDocumentAtlas).
     */
    @Override
    public List<DocumentAtlas> findListeDocumentAtlasParType(TypeDocumentAtlas typeDocument)
    {
        LOGGER.debug("Recherche des documents Atlas de type '{}'", typeDocument);

        // Si aucun paramètre, la recherche n'est pas exécutée
        if (typeDocument == null)
        {
            return new ArrayList<>();
        }

        // Paramètrage de la recherche
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<DocumentAtlas> query = criteriaBuilder.createQuery(DocumentAtlas.class);
        Root<DocumentAtlas> root = query.from(DocumentAtlas.class);

        // Paramètrage des critères
        Path<TypeDocumentAtlas> path = root.<TypeDocumentAtlas> get(
            ProprieteDocumentAtlas.PROPRIETE_TYPE_DOCUMENT.getNom());
        Predicate predicateTypeDocument = criteriaBuilder.equal(path, typeDocument);
        query.where(predicateTypeDocument);

        // Paramètrage des jointures
        root.fetch(ProprieteDocumentAtlas.PROPRIETE_TYPE_DOCUMENT.getNom(), JoinType.INNER);

        // Exécution de la recherche
        return findAll(query);
    }

    /**
     * {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.atlas.dao.DocumentAtlasDao#findListeDocumentAtlasParTypeEtDates(
     *      fr.gouv.finances.lombok.atlas.bean.TypeDocumentAtlas, java.util.Date, java.util.Date).
     */
    @Override
    public List<DocumentAtlas> findListeDocumentAtlasParTypeEtDates(
        TypeDocumentAtlas typeDocumentAtlas, Date dateDebut, Date dateFin)
    {
        LOGGER.debug("Recherche des documents Atlas de type '{}', de dates comprises entre '{}' et '{}'", typeDocumentAtlas, dateDebut, dateFin);

        // Si une borne n'est pas précisée, la recherche n'est pas exécutée
        if (dateDebut == null || dateFin == null)
        {
            return new ArrayList<>();
        }

        // Paramètrage de la recherche
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<DocumentAtlas> query = criteriaBuilder.createQuery(DocumentAtlas.class);
        Root<DocumentAtlas> root = query.from(DocumentAtlas.class);

        // Paramètrage des critères
        Predicate predicateTypeDocument = criteriaBuilder.equal(
            root.<TypeDocumentAtlas> get(
                ProprieteDocumentAtlas.PROPRIETE_TYPE_DOCUMENT.getNom()),
            typeDocumentAtlas);
        Predicate predicateDate = criteriaBuilder.between(
            root.<Date> get(ProprieteDocumentAtlas.PROPRIETE_DATE_PEREMPTION.getNom()), dateDebut, dateFin);
        query.where(predicateTypeDocument, predicateDate);

        // Paramètrage des jointures
        root.fetch(ProprieteDocumentAtlas.PROPRIETE_TYPE_DOCUMENT.getNom(), JoinType.INNER);

        // Exécution de la recherche
        return findAll(query);
    }

    /**
     * {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.atlas.dao.DocumentAtlasDao#findTousLesTypesDocument().
     */
    @Override
    public List<TypeDocumentAtlas> findTousLesTypesDocument()
    {
        LOGGER.trace("Recherche de tous les types de document Atlas");

        // Récupération de tous les documents sans critère
        return loadAllObjects(TypeDocumentAtlas.class);
    }

    /**
     * {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.atlas.dao.DocumentAtlasDao#findTypeDocumentParCode(java.lang.String).
     */
    @Override
    public TypeDocumentAtlas findTypeDocumentParCode(String code)
    {
        LOGGER.debug("Recherche du document Atlas de code '{}'", code);

        return findbyUniqueCriteria(TypeDocumentAtlas.class, ProprieteDocumentAtlas.PROPRIETE_CODE_TYPE_DOCUMENT.getNom(), code);
    }

    /**
     * {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.atlas.dao.DocumentAtlasDao#findDocumentsAtlasEtContenusAArchiverParIterateur(
     *      java.lang.Integer).
     */
    @Override
    public ScrollIterator findDocumentsAtlasEtContenusAArchiverParIterateur(Integer nombreOccurences)
    {
        LOGGER.trace("Recherche des documents Atlas à archiver");

        // Paramètrage de la recherche
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<DocumentAtlas> query = criteriaBuilder.createQuery(DocumentAtlas.class);
        Root<DocumentAtlas> root = query.from(DocumentAtlas.class);

        // Paramètrage des critères
        Path<TypeDocumentAtlas> pathEtat = root.<TypeDocumentAtlas> get(
            ProprieteDocumentAtlas.PROPRIETE_ETAT.getNom());
        Predicate predicateEtat = criteriaBuilder.equal(pathEtat, EtatDocumentAtlas.AARCHIVER);
        query.where(predicateEtat);

        // Paramètrage des jointures
        Fetch<DocumentAtlas, FichierJoint> fetchFichier = root.fetch(ProprieteDocumentAtlas.PROPRIETE_FICHIER.getNom(), JoinType.LEFT);
        fetchFichier.fetch(ProprieteDocumentAtlas.PROPRIETE_CONTENU_FICHIER.getNom(), JoinType.LEFT);

        // Exécution de la recherche
        return getScrollIterator(query, nombreOccurences);
    }

    /**
     * {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.atlas.dao.DocumentAtlasDao#findDocumentsAtlasEtContenusAPurgerParIterateur(
     *      java.lang.Integer).
     */
    @Override
    public ScrollIterator findDocumentsAtlasEtContenusAPurgerParIterateur(Integer nombreOccurences)
    {
        LOGGER.trace("Recherche des documents Atlas à purger");

        // Paramètrage de la recherche
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<DocumentAtlas> query = criteriaBuilder.createQuery(DocumentAtlas.class);
        Root<DocumentAtlas> root = query.from(DocumentAtlas.class);

        // Paramètrage des critères
        Path<TypeDocumentAtlas> pathEtat = root.<TypeDocumentAtlas> get(
            ProprieteDocumentAtlas.PROPRIETE_ETAT.getNom());
        Predicate predicateEtat = criteriaBuilder.equal(pathEtat, EtatDocumentAtlas.ARCHIVE);
        query.where(predicateEtat);

        // Paramètrage des jointures
        Fetch<DocumentAtlas, FichierJoint> fetchFichier = root.fetch(ProprieteDocumentAtlas.PROPRIETE_FICHIER.getNom(), JoinType.INNER);
        fetchFichier.fetch(ProprieteDocumentAtlas.PROPRIETE_CONTENU_FICHIER.getNom(), JoinType.INNER);

        // Exécution de la recherche
        return getScrollIterator(query, nombreOccurences);
    }

    /**
     * {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.atlas.dao.DocumentAtlasDao#findDocumentsAtlasPerimesParIterateur(
     *      java.lang.Integer).
     */
    @Override
    public ScrollIterator findDocumentsAtlasPerimesParIterateur(Integer nombreOccurences)
    {
        LOGGER.trace("Recherche des documents Atlas périmés");

        // Paramètrage de la recherche
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<DocumentAtlas> query = criteriaBuilder.createQuery(DocumentAtlas.class);
        Root<DocumentAtlas> root = query.from(DocumentAtlas.class);

        // Paramètrage des critères
        Path<Date> path = root.<Date> get(
            ProprieteDocumentAtlas.PROPRIETE_DATE_PEREMPTION.getNom());
        Predicate predicateDate = criteriaBuilder.lessThan(path, Calendar.getInstance().getTime());
        query.where(predicateDate);

        // Exécution de la recherche
        return getScrollIterator(query, nombreOccurences);
    }

    /**
     * {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.atlas.dao.DocumentAtlasDao#findDocumentsEnvoyesAAtlasParIterateur(
     *      java.lang.Integer, java.sql.Timestamp).
     */
    @Override
    public ScrollIterator findDocumentsEnvoyesAAtlasParIterateur(Integer nombreOccurences,
        final Timestamp dateDEnvoiAtlas)
    {
        LOGGER.trace("Recherche des documents Atlas à archiver à la date '{}'", dateDEnvoiAtlas);

        // Paramètrage de la recherche
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<DocumentAtlas> query = criteriaBuilder.createQuery(DocumentAtlas.class);
        Root<DocumentAtlas> root = query.from(DocumentAtlas.class);

        // Paramètrage des critères
        Path<TypeDocumentAtlas> pathEtat = 
            root.<TypeDocumentAtlas> get(ProprieteDocumentAtlas.PROPRIETE_ETAT.getNom());
        Predicate predicateEtat = criteriaBuilder.equal(pathEtat, EtatDocumentAtlas.ENVOYEPOURARCHIVAGE);
        Path<Date> pathDate = root.<Date> get(ProprieteDocumentAtlas.PROPRIETE_DATE_ENVOI.getNom());
        Predicate predicateDate = criteriaBuilder.equal(pathDate, dateDEnvoiAtlas);
        query.where(predicateEtat, predicateDate);

        // Exécution de la recherche
        return getScrollIterator(query, nombreOccurences);
    }
}
