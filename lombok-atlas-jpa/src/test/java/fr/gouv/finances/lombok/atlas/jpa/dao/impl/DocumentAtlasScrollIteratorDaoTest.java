package fr.gouv.finances.lombok.atlas.jpa.dao.impl;

import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.data.jpa.JpaRepositoriesAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionTemplate;

import fr.gouv.finances.lombok.atlas.bean.DocumentAtlas;
import fr.gouv.finances.lombok.atlas.bean.TypeDocumentAtlas;
import fr.gouv.finances.lombok.atlas.dao.DocumentAtlasDao;
import fr.gouv.finances.lombok.atlas.dao.impl.AtlasDaoCore;
import fr.gouv.finances.lombok.atlas.jpa.config.PersistenceJPAConfigTest;
import fr.gouv.finances.lombok.test.controle.ControleDonnees;
import fr.gouv.finances.lombok.upload.bean.FichierJoint;
import fr.gouv.finances.lombok.util.base.PurgeTablesDao;
import fr.gouv.finances.lombok.util.date.TemporaliteUtils;
import fr.gouv.finances.lombok.util.persistance.ScrollIterator;

/**
 * Tests unitaires automatisés du DAO JPA gérant les données des documents Atlas.
 *
 * @author Christophe Breheret-Girardin
 * @author CF
 */
@ComponentScan(basePackages = {"fr.gouv.finances.lombok.jpa.dao", "fr.gouv.finances.lombok.atlas.jpa.dao.impl"})
@EnableAutoConfiguration(exclude = JpaRepositoriesAutoConfiguration.class)
@SpringBootTest(classes = {PersistenceJPAConfigTest.class}, properties = {"lombok.composant.atlas.inclus=true", "lombok.orm.jpa=true"})
@ActiveProfiles(profiles = {"embedded"})
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class DocumentAtlasScrollIteratorDaoTest extends AtlasDaoCore
{
    public DocumentAtlasScrollIteratorDaoTest()
    {
        super();
    }
    
    protected static final Logger LOGGER = LoggerFactory.getLogger(DocumentAtlasScrollIteratorDaoTest.class);

    @Autowired
    private DocumentAtlasDao documentatlasdao;

    @Autowired
    private PurgeTablesDao purgetablesdao;

    @Autowired
    private TransactionTemplate transactionTemplate;
    
    /**
     * Initialisation des données en base avant chaque test.
     */
    @Before
    public void creerDonnees()
    {
        // Purge des données de la base
        purgetablesdao.purgerTables("ZDOCUMENTATLAS_ZDOA", "ZTYPEDOCUMENTATLAS_ZTDA", "ZFICHIERJOINT_ZFIJ");

        // Purge des données de la base
        purgetablesdao.purgerTables("ZDOCUMENTATLAS_ZDOA", "ZTYPEDOCUMENTATLAS_ZTDA", "ZFICHIERJOINT_ZFIJ");

        // Vérification de l'absence de données
        ControleDonnees.verifierElements(documentatlasdao.loadAllObjects(DocumentAtlas.class));
        ControleDonnees.verifierElements(documentatlasdao.loadAllObjects(TypeDocumentAtlas.class));
        ControleDonnees.verifierElements(documentatlasdao.loadAllObjects(FichierJoint.class));

        // Génération des types de document Atlas, insertion en base de données et sauvegarde dans une Map
        mapTypeDocumentAtlas = IntStream.rangeClosed(1, 2).mapToObj(this::getTypeDocument)
            .peek(documentatlasdao::saveObject)
            .collect(Collectors.toMap(t -> Integer.parseInt(t.getCode()), t -> t));

        // Génération des fichiers joints
        mapFichierJoint = IntStream.rangeClosed(1, 2).mapToObj(this::getFichierJoint)
            .peek(documentatlasdao::saveObject)
            .collect(Collectors.toMap(f -> Integer.parseInt(f.getNomFichierOriginal()), f -> f));

        // Génération des documents Atlas, insertion en base de données et sauvegarde dans une Map
        mapDocumentAtlas = IntStream.rangeClosed(1, 7)
            .mapToObj(this::getDocument).peek(documentatlasdao::saveObject)
            .collect(Collectors.toMap(d -> Integer.parseInt(d.getIdentifiantAtlas()), d -> d));

        // Vérification des données à partir de l'image de la base
        ControleDonnees.verifierElements(documentatlasdao.loadAllObjects(TypeDocumentAtlas.class)
            , mapTypeDocumentAtlas.get(1), mapTypeDocumentAtlas.get(2));
        ControleDonnees.verifierElements(documentatlasdao.loadAllObjects(FichierJoint.class)
            , mapFichierJoint.get(1), mapFichierJoint.get(2));
        ControleDonnees.verifierElements(documentatlasdao.loadAllObjects(DocumentAtlas.class)
            , mapDocumentAtlas.get(1), mapDocumentAtlas.get(2), mapDocumentAtlas.get(3)
            , mapDocumentAtlas.get(4), mapDocumentAtlas.get(5), mapDocumentAtlas.get(6)
            , mapDocumentAtlas.get(7));

        // Suppression du cache pour forcer les requêtes des tests
        documentatlasdao.clearPersistenceContext();
    }

    /**
     * Méthode permettant de tester l'injection du DAO.
     */
    @Test
    public final void testInjectionDao()
    {
        assertNotNull("DAO non injecté dans le test", documentatlasdao);
    }

    /**
     * Test complet de la méthode
     * {@link fr.gouv.finances.lombok.atlas.dao.DocumentAtlasDao
     * #findDocumentsAtlasEtContenusAArchiverParIterateur(java.util.Integer)}.
     */
    @Test
    public void findDocumentsAtlasEtContenusAArchiverParIterateur()
    {
        // Exécutions de la méthode à tester, via une transaction manuelle
        List<DocumentAtlas> documents
            = transactionTemplate.execute(t
                -> getDocumentsAtlas(documentatlasdao.findDocumentsAtlasEtContenusAArchiverParIterateur(1)));

        // Vérification des données + les associations pré-chargées
        verifierDocuments(documents, false, true
            , mapDocumentAtlas.get(1), mapDocumentAtlas.get(2), mapDocumentAtlas.get(3));
    }

    /**
     * Test complet de la méthode
     * {@link fr.gouv.finances.lombok.atlas.dao.DocumentAtlasDao
     * #findDocumentsAtlasEtContenusAPurgerParIterateur(java.util.Integer)}.
     */
    @Test
    public void findDocumentsAtlasEtContenusAPurgerParIterateur()
    {
        // Exécutions de la méthode à tester, via une transaction manuelle
        List<DocumentAtlas> documents
            = transactionTemplate.execute(t
                -> getDocumentsAtlas(documentatlasdao.findDocumentsAtlasEtContenusAPurgerParIterateur(1)));

        // Vérification des données + les associations pré-chargées
        verifierDocuments(documents, false, true, mapDocumentAtlas.get(5), mapDocumentAtlas.get(6));
    }

    /**
     * Test complet de la méthode
     * {@link fr.gouv.finances.lombok.atlas.dao.DocumentAtlasDao
     * #findDocumentsAtlasPerimesParIterateur(java.util.Integer)}.
     */
    @Test
    public void findDocumentsAtlasPerimesParIterateur()
    {
        // Exécutions de la méthode à tester, via une transaction manuelle
        List<DocumentAtlas> documents
            = transactionTemplate.execute(t
                -> getDocumentsAtlas(documentatlasdao.findDocumentsAtlasPerimesParIterateur(1)));

        // Vérification des données (aucune association ne doit être pré-chargée)
        verifierDocuments(documents, false, false, mapDocumentAtlas.get(1), mapDocumentAtlas.get(2));
    }

    /**
     * Test complet de la méthode
     * {@link fr.gouv.finances.lombok.atlas.dao.DocumentAtlasDao#findDocumentsEnvoyesAAtlasParIterateur(
     * java.util.Integer)}.
     */
    @Test
    public void findDocumentsEnvoyesAAtlasParIterateur()
    {
        // Exécutions de la méthode à tester, via une transaction manuelle
        List<DocumentAtlas> documents
            = transactionTemplate.execute(t ->
                getDocumentsAtlas(documentatlasdao
                    .findDocumentsEnvoyesAAtlasParIterateur(1, TemporaliteUtils.getTimestamp(2018, 1, 9))));

        // Vérification des données + les associations pré-chargées
        verifierDocuments(documents, false, false, mapDocumentAtlas.get(4));
    }

    /**
     * Méthode permettant de récupérer les documents à partir d'un curseur.
     *
     * @param scrollIterator utilitaire de recherche par curseur
     * @return la liste des documents recherchés
     */
    private List<DocumentAtlas> getDocumentsAtlas(ScrollIterator scrollIterator)
    {
        // Récupération des documents en mode curseur (possible par l'encapsulation dans une transaction)
        List<DocumentAtlas> documents = new ArrayList<>();
        while (scrollIterator.hasNext())
        {
            documents.add((DocumentAtlas) scrollIterator.nextObjetMetier());
        }
        // Fermeture du curseur après parcours de tous les éléments
        scrollIterator.close();

        // Retour de tous les documents trouvés
        return documents;
    }
}
