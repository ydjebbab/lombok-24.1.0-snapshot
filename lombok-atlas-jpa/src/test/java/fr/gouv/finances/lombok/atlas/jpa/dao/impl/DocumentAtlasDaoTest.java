/*
* Copyright (c) 2020 DGFiP - Tous droits réservés
* 
*/
package fr.gouv.finances.lombok.atlas.jpa.dao.impl;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.data.jpa.JpaRepositoriesAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import fr.gouv.finances.lombok.atlas.bean.DocumentAtlas;
import fr.gouv.finances.lombok.atlas.bean.TypeDocumentAtlas;
import fr.gouv.finances.lombok.atlas.dao.impl.AtlasDaoCore;
import fr.gouv.finances.lombok.atlas.jpa.config.PersistenceJPAConfigTest;
import fr.gouv.finances.lombok.test.controle.ControleDonnees;
import fr.gouv.finances.lombok.upload.bean.FichierJoint;
import fr.gouv.finances.lombok.util.date.TemporaliteUtils;

/**
 * Tests unitaires automatisés du DAO JPA gérant les données des documents Atlas.
 *
 * @author Christophe Breheret-Girardin
 * @author CF
 */
@ComponentScan(basePackages = {"fr.gouv.finances.lombok.jpa.dao", "fr.gouv.finances.lombok.atlas.jpa.dao.impl"})
@EnableAutoConfiguration(exclude = JpaRepositoriesAutoConfiguration.class)
@SpringBootTest(classes = {PersistenceJPAConfigTest.class}, properties = {"lombok.composant.atlas.inclus=true", "lombok.orm.jpa=true"})
@ActiveProfiles(profiles = {"embedded"})
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class DocumentAtlasDaoTest extends AtlasDaoCore
{
    public DocumentAtlasDaoTest()
    {
        super();
    }

    protected static final Logger LOGGER = LoggerFactory.getLogger(DocumentAtlasDaoTest.class);

    /**
     * Initialisation des données en base avant chaque test.
     */
    @Before
    public void creerDonnees()
    {
        // Purge des données de la base
        purgetablesdao.purgerTables("ZDOCUMENTATLAS_ZDOA", "ZTYPEDOCUMENTATLAS_ZTDA", "ZFICHIERJOINT_ZFIJ");

        // Vérification de l'absence de données
        ControleDonnees.verifierElements(documentatlasdao.loadAllObjects(DocumentAtlas.class));
        ControleDonnees.verifierElements(documentatlasdao.loadAllObjects(TypeDocumentAtlas.class));
        ControleDonnees.verifierElements(documentatlasdao.loadAllObjects(FichierJoint.class));

        // Génération des types de document Atlas, insertion en base de données et sauvegarde dans une Map
        mapTypeDocumentAtlas = IntStream.rangeClosed(1, 2).mapToObj(this::getTypeDocument)
            .peek(documentatlasdao::saveObject)
            .collect(Collectors.toMap(t -> Integer.parseInt(t.getCode()), t -> t));

        // Génération des fichiers joints
        mapFichierJoint = IntStream.rangeClosed(1, 1).mapToObj(this::getFichierJoint)
            .peek(documentatlasdao::saveObject)
            .collect(Collectors.toMap(f -> Integer.parseInt(f.getNomFichierOriginal()), f -> f));

        // Génération des documents Atlas, insertion en base de données et sauvegarde dans une Map
        mapDocumentAtlas = IntStream.rangeClosed(1, 3)
            .mapToObj(this::getDocument).peek(documentatlasdao::saveObject)
            .collect(Collectors.toMap(d -> Integer.parseInt(d.getIdentifiantAtlas()), d -> d));

        // Vérification des données à partir de l'image de la base
        ControleDonnees.verifierElements(documentatlasdao.loadAllObjects(TypeDocumentAtlas.class), mapTypeDocumentAtlas.get(1),
            mapTypeDocumentAtlas.get(2));
        ControleDonnees.verifierElements(documentatlasdao.loadAllObjects(FichierJoint.class), mapFichierJoint.get(1));
        ControleDonnees.verifierElements(documentatlasdao.loadAllObjects(DocumentAtlas.class), mapDocumentAtlas.get(1),
            mapDocumentAtlas.get(2), mapDocumentAtlas.get(3));

        // Suppression du cache pour forcer les requêtes des tests
        documentatlasdao.clearPersistenceContext();
    }

    /**
     * Méthode permettant de tester l'injection du DAO.
     */
    @Test
    public final void testInjectionDao()
    {
        assertNotNull("DAO non injecté dans le test", documentatlasdao);
    }

    /**
     * Test de la méthode
     * {@link fr.gouv.finances.lombok.atlas.dao.DocumentAtlasDao#findDocumentAtlasParIdentifiantAtlas()}.
     */
    @Test
    public void findDocumentAtlasParIdentifiantAtlas()
    {
        // Exécution de la méthode à tester sur des paramètres non renseignés ou vides
        assertNull(documentatlasdao.findDocumentAtlasParIdentifiantAtlas(null));
        assertNull(documentatlasdao.findDocumentAtlasParIdentifiantAtlas(""));

        // Exécution de la méthode à tester sans résultat et vérification
        assertNull(documentatlasdao
            .findDocumentAtlasParIdentifiantAtlas(VALEUR_INCONNUE));

        // Exécution de la méthode à tester sur toutes les données et vérifications
        mapDocumentAtlas.entrySet().forEach(e -> verifierDocument(
            documentatlasdao.findDocumentAtlasParIdentifiantAtlas(e.getValue().getIdentifiantAtlas()), true, false, e.getValue()));
    }

    /**
     * Test de la méthode
     * {@link fr.gouv.finances.lombok.atlas.dao.DocumentAtlasDao#findListeDocumentAtlasParIdentifiantAtlas( java.util.Collection)}.
     */
    @Test
    public void findListeDocumentAtlasParIdentifiantAtlas()
    {
        // Exécution de la méthode à tester sur des paramètres non renseignés ou vides
        assertTrue(documentatlasdao.findListeDocumentAtlasParIdentifiantAtlas(null).isEmpty());
        assertTrue(documentatlasdao
            .findListeDocumentAtlasParIdentifiantAtlas(new ArrayList<String>()).isEmpty());

        // Exécution de la méthode à tester sans résultat et vérification
        assertTrue(documentatlasdao
            .findListeDocumentAtlasParIdentifiantAtlas(Arrays.asList(VALEUR_INCONNUE)).isEmpty());

        // Exécution de la méthode à tester sur toutes les données unitairement et vérifications
        mapDocumentAtlas.entrySet().forEach(e -> verifierDocuments(
            documentatlasdao.findListeDocumentAtlasParIdentifiantAtlas(
                Arrays.asList(e.getValue().getIdentifiantAtlas())),
            true, false, e.getValue()));

        // Exécution de la méthode à tester sur les données multiples et vérifications
        verifierDocuments(documentatlasdao.findListeDocumentAtlasParIdentifiantAtlas(
            Arrays.asList(mapDocumentAtlas.get(1).getIdentifiantAtlas(), mapDocumentAtlas.get(2).getIdentifiantAtlas())), true, false,
            mapDocumentAtlas.get(1), mapDocumentAtlas.get(2));
        verifierDocuments(documentatlasdao.findListeDocumentAtlasParIdentifiantAtlas(
            Arrays.asList(mapDocumentAtlas.get(1).getIdentifiantAtlas(), mapDocumentAtlas.get(2).getIdentifiantAtlas(),
                mapDocumentAtlas.get(3).getIdentifiantAtlas())),
            true, false, mapDocumentAtlas.get(1), mapDocumentAtlas.get(2), mapDocumentAtlas.get(3));
    }

    /**
     * Test de la méthode
     * {@link fr.gouv.finances.lombok.atlas.dao.DocumentAtlasDao#findListeDocumentAtlasParType( fr.gouv.finances.lombok.atlas.bean.TypeDocumentAtlas)}.
     */
    @Test
    public void findListeDocumentAtlasParType()
    {
        LOGGER.debug("Methode findListeDocumentAtlasParType");
        // Exécution de la méthode à tester sur des paramètres non renseignés
        assertTrue(documentatlasdao.findListeDocumentAtlasParType(null).isEmpty());

        // Exécution de la méthode à tester sans résultat et vérification
        TypeDocumentAtlas typeDocument3 = getTypeDocument(3);
        documentatlasdao.saveObject(typeDocument3);
        assertTrue(documentatlasdao.findListeDocumentAtlasParType(typeDocument3).isEmpty());

        // Exécution de la méthode à tester sur toutes les données et vérifications
        verifierDocuments(documentatlasdao.findListeDocumentAtlasParType(mapTypeDocumentAtlas.get(1)), true, false, mapDocumentAtlas.get(1),
            mapDocumentAtlas.get(2));
        verifierDocuments(documentatlasdao.findListeDocumentAtlasParType(mapTypeDocumentAtlas.get(2)), true, false,
            mapDocumentAtlas.get(3));
    }

    /**
     * Test de la méthode
     * {@link fr.gouv.finances.lombok.atlas.dao.DocumentAtlasDao#findListeDocumentAtlasParTypeEtDates( fr.gouv.finances.lombok.atlas.bean.TypeDocumentAtlas, java.util.Date, java.util.Date)}.
     */
    @Test
    public void findListeDocumentAtlasParTypeEtDates()
    {
        // Exécution de la méthode à tester sur des paramètres non renseignés
        assertTrue(documentatlasdao.findListeDocumentAtlasParTypeEtDates(null, null, null).isEmpty());
        assertTrue(documentatlasdao.findListeDocumentAtlasParTypeEtDates(
            mapTypeDocumentAtlas.get(1), null, null).isEmpty());
        assertTrue(documentatlasdao.findListeDocumentAtlasParTypeEtDates(
            mapTypeDocumentAtlas.get(1), TemporaliteUtils.getTimestamp(2018, 1, 7), null).isEmpty());
        assertTrue(documentatlasdao.findListeDocumentAtlasParTypeEtDates(
            mapTypeDocumentAtlas.get(1), null, TemporaliteUtils.getTimestamp(2018, 1, 7)).isEmpty());

        // Exécution de la méthode à tester sans résultat et vérification
        assertTrue(documentatlasdao.findListeDocumentAtlasParTypeEtDates(
            mapTypeDocumentAtlas.get(1), TemporaliteUtils.getTimestamp(2012, 1, 7), TemporaliteUtils.getTimestamp(2012, 1, 9))
            .isEmpty());
        assertTrue(documentatlasdao.findListeDocumentAtlasParTypeEtDates(
            mapTypeDocumentAtlas.get(2), TemporaliteUtils.getTimestamp(2018, 1, 7), TemporaliteUtils.getTimestamp(2018, 1, 9))
            .isEmpty());

        // Exécution de la méthode à tester avec résultat et vérification
        verifierDocuments(documentatlasdao.findListeDocumentAtlasParTypeEtDates(
            mapTypeDocumentAtlas.get(1), TemporaliteUtils.getTimestamp(2018, 1, 7), TemporaliteUtils.getTimestamp(2018, 1, 9)), true, false,
            mapDocumentAtlas.get(1));
        verifierDocuments(documentatlasdao.findListeDocumentAtlasParTypeEtDates(
            mapTypeDocumentAtlas.get(1), TemporaliteUtils.getTimestamp(2010, 1, 7), TemporaliteUtils.getTimestamp(2010, 1, 9)), true, false,
            mapDocumentAtlas.get(2));
        verifierDocuments(documentatlasdao.findListeDocumentAtlasParTypeEtDates(
            mapTypeDocumentAtlas.get(2), TemporaliteUtils.getDemain(), TemporaliteUtils.getDemain()), true, false, mapDocumentAtlas.get(3));
    }

}
