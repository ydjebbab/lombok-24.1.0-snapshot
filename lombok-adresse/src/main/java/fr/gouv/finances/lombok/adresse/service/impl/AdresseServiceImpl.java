/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 *
 */
package fr.gouv.finances.lombok.adresse.service.impl;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.gouv.finances.lombok.adresse.bean.CodePostal;
import fr.gouv.finances.lombok.adresse.bean.Commune;
import fr.gouv.finances.lombok.adresse.bean.Departement;
import fr.gouv.finances.lombok.adresse.bean.PaysInsee;
import fr.gouv.finances.lombok.adresse.bean.PaysIso;
import fr.gouv.finances.lombok.adresse.bean.Region;
import fr.gouv.finances.lombok.adresse.dao.AdresseDao;
import fr.gouv.finances.lombok.adresse.service.AdresseService;
import fr.gouv.finances.lombok.util.base.BaseServiceImpl;
import fr.gouv.finances.lombok.util.exception.RegleGestionException;
import fr.gouv.finances.lombok.util.messages.Messages;

/**
 * Implémentation du service métier de gestion des adresses.
 * 
 * @author amleplatinec
 * @author Christophe Breheret-Girardin
 * @author CF
 */
public class AdresseServiceImpl extends BaseServiceImpl implements AdresseService
{

    protected static final Logger LOGGER = LoggerFactory.getLogger(AdresseServiceImpl.class);
    
    private AdresseDao adressedao;

    public AdresseServiceImpl()
    {
        super();
    }

    /**
     * {@inheritDoc}
     * 
     * 
     */
    @Override
    public void ajouterUnCodePostal(CodePostal uncodepostal)
    {
        LOGGER.debug(">>> Debut methode ajouterUnCodePostal");
        adressedao.saveObject(uncodepostal);
    }

    /**
     * {@inheritDoc}
     * 
     * 
     */
    @Override
    public void ajouterUnDepartement(Departement unDepartement)
    {
        LOGGER.debug(">>> Debut methode ajouterUnDepartement");
        adressedao.saveObject(unDepartement);

    }

    /**
     * {@inheritDoc}
     * 
     * 
     */
    @Override
    public void ajouterUneCommune(Commune uneCommune)
    {
        LOGGER.debug(">>> Debut methode ajouterUneCommune");
        adressedao.saveObject(uneCommune);
    }

    /**
     * {@inheritDoc}
     * 
     *
     */
    @Override
    public void ajouterUneRegion(Region uneRegion)
    {
        LOGGER.debug(">>> Debut methode ajouterUneRegion");
        adressedao.saveObject(uneRegion);
    }

    /**
     * {@inheritDoc}
     * 
     *
     */
    @Override
    public void ajouterUnPaysInsee(PaysInsee unPays)
    {
        LOGGER.debug(">>> Debut methode ajouterUnPaysInsee");
        adressedao.saveObject(unPays);
    }

    /**
     * {@inheritDoc}
     * 
     * 
     */
    @Override
    public void ajouterUnPaysIso(PaysIso unPaysIso)
    {
        LOGGER.debug(">>> Debut methode ajouterUnPaysIso");
        adressedao.saveObject(unPaysIso);
    }

    /**
     * {@inheritDoc}
     * 
     * 
     */
    @Override
    public Map<String, String> chargerMapPaysInsee()
    {
        LOGGER.debug(">>> Debut methode chargerMapPaysInsee");
        // Chargement de tous les pays INSEE
        List<PaysInsee> unelistepays = adressedao.loadAllObjects(PaysInsee.class);

        // Conversion de la liste en Map
        Map<String, String> mapPaysInsee = new TreeMap<>();
        for (Iterator<PaysInsee> iter = unelistepays.iterator(); iter.hasNext();)
        {
            PaysInsee element = iter.next();
            mapPaysInsee.put(element.getNom(), element.getNom());
        }

        return mapPaysInsee;
    }

    /**
     * {@inheritDoc}
     * 
     * 
     */
    @Override
    public Map<String, String> chargerMapPaysIso()
    {
        LOGGER.debug(">>> Debut methode chargerMapPaysIso");
        // Chargement de tous les pays ISO
        List<PaysIso> unelistepays = adressedao.loadAllObjects(PaysIso.class);

        // Conversion de la liste en Map
        Map<String, String> mapPaysIso = new TreeMap<>();
        for (Iterator<PaysIso> iter = unelistepays.iterator(); iter.hasNext();)
        {
            PaysIso element = iter.next();
            mapPaysIso.put(element.getNom(), element.getNom());
        }

        return mapPaysIso;
    }

    /**
     * {@inheritDoc}
     * 
     * 
     */
    @Override
    public void modifierUneCommune(Commune unecommune)
    {
        LOGGER.debug(">>> Debut methode modifierUneCommune");
        adressedao.saveObject(unecommune);
    }

    /**
     * {@inheritDoc}
     * 
     * 
     */
    @Override
    public void modifierUnPaysInsee(PaysInsee unPaysInsee)
    {
        LOGGER.debug(">>> Debut methode modifierUnPaysInsee");
        adressedao.saveObject(unPaysInsee);
    }

    /**
     * {@inheritDoc}
     * 
     * 
     */
    @Override
    public void modifierUnPaysIso(PaysIso unPaysIso)
    {
        LOGGER.debug(">>> Debut methode modifierUnPaysIso");
        adressedao.saveObject(unPaysIso);
    }

    /**
     * {@inheritDoc}
     * 
     * 
     */
    @Override
    public List<CodePostal> rechercherCodePostalParCodeVille(String codepostal, String ville)
    {
        LOGGER.debug(">>> Debut methode rechercherCodePostalParCodeVille");
        return adressedao.findCodesPostauxParCodeEtOuVille(codepostal, ville);
    }

    /**
     * {@inheritDoc}
     * 
     * 
     */
    @Override
    public List<Departement> rechercherTousLesDepartementsAvecRegion()
    {
        LOGGER.debug(">>> Debut methode rechercherTousLesDepartementsAvecRegion");
        return this.adressedao.findTousLesDepartementsAvecRegion();
    }

    /**
     * {@inheritDoc}
     * 
     * 
     */
    @Override
    public List<PaysInsee> rechercherTousLesPaysInsee()
    {
        LOGGER.debug(">>> Debut methode rechercherTousLesPaysInsee");
        return this.adressedao.findTousLesPaysInsee();
    }

    /**
     * {@inheritDoc}
     * 
     * 
     */
    @Override
    public List<Region> rechercherToutesLesRegionsAvecDepartement()
    {
        LOGGER.debug(">>> Debut methode rechercherToutesLesRegionsAvecDepartement");
        return this.adressedao.findToutesLesRegionsAvecDepartements();
    }

    /**
     * {@inheritDoc}
     * 
     * 
     */
    @Override
    public Departement rechercherUnDepartementParCodeDepartementINSEE(String uncodedepartementINSEE)
    {
        LOGGER.debug(">>> Debut methode rechercherUnDepartementParCodeDepartementINSEE");
        return this.adressedao.findUnDepartementParCodeDepartementINSEE(uncodedepartementINSEE);
    }

    /**
     * {@inheritDoc}
     * 
     * 
     */
    @Override
    public Commune rechercherUneCommuneParCodeCommuneINSEE(String uncodecommuneINSEE)
    {
        LOGGER.debug(">>> Debut methode rechercherUneCommuneParCodeCommuneINSEE");
        return this.adressedao.findUneCommuneParCodeCommuneINSEE(uncodecommuneINSEE);
    }

    /**
     * {@inheritDoc}
     * 
     * 
     */
    @Override
    public Region rechercherUneRegionParCodeRegionINSEE(String uncoderegionINSEE)
    {
        LOGGER.debug(">>> Debut methode rechercherUneRegionParCodeRegionINSEE");
        return this.adressedao.findUneRegionParCodeRegionINSEE(uncoderegionINSEE);
    }

    /**
     * {@inheritDoc}
     * 
     * 
     */
    @Override
    public PaysInsee rechercherUnPaysParCodeInsee(String uncodepaysinsee)
    {
        LOGGER.debug(">>> Debut methode rechercherUnPaysParCodeInsee");
        return this.adressedao.findUnPaysInseeParCodePaysInsee(uncodepaysinsee);
    }

    /**
     * {@inheritDoc}
     * 
     * 
     */
    @Override
    public PaysIso rechercherUnPaysParCodeIso(String uncodepaysiso)
    {
        LOGGER.debug(">>> Debut methode rechercherUnPaysParCodeIso");
        return adressedao.findUnPaysIsoParCodePaysIso(uncodepaysiso);
    }

    /**
     * {@inheritDoc}
     * 
     * 
     */
    @Override
    public List<CodePostal> rechercherVillesPourUnCodePostal(String codepostal)
    {
        LOGGER.debug(">>> Debut methode rechercherVillesPourUnCodePostal");
        // Chargement de tous les codes postaux
        List<CodePostal> codesPostaux = adressedao.findCodesPostauxParCode(codepostal);

        // La liste n'est pas censée être vide
        if (codesPostaux.isEmpty())
        {
            throw new RegleGestionException(
                Messages.getString("exception.codepostal.nexistepas") + codepostal);
        }
        return codesPostaux;
    }

    /**
     * {@inheritDoc}
     * 
     * 
     */
    @Override
    public void supprimerTousLesCodesPostaux()
    {
        LOGGER.debug(">>> Debut methode supprimerTousLesCodesPostaux");
        adressedao.deleteTousLesCodesPostaux();
    }

    /**
     * {@inheritDoc}
     * 
     * 
     */
    @Override
    public void verifierCodePostalCorrespondAUnLibelleCommuneRG23(String codepostal, String ville)
    {
        LOGGER.debug(">>> Debut methode verifierCodePostalCorrespondAUnLibelleCommuneRG23");
        if (ville.length() == 0 || codepostal.length() == 0
            || (!adressedao.checkExistCodePostalXVille(codepostal, ville)))
        {
            throw new RegleGestionException(Messages.getString("exception.codepostal.necorrespondpasalacommune.part1")
                + " " + codepostal + Messages.getString("exception.codepostal.necorrespondpasalacommune.part2")
                + " : " + ville);
        }
    }

    /**
     * {@inheritDoc}
     * 
     * 
     */
    @Override
    public void verifierExistenceCodePaysISO(String codepaysiso)
    {
        LOGGER.debug(">>> Debut methode verifierExistenceCodePaysISO");
        if (codepaysiso.length() == 0 || (adressedao.findUnPaysIsoParCodePaysIso(codepaysiso) == null))
        {
            throw new RegleGestionException(Messages.getString("exception.codepaysiso.nexistepas") + " " + codepaysiso);
        }
    }

    /**
     * {@inheritDoc}
     * 
     * 
     */
    @Override
    public void verifierExistenceCodePostalRG22(String codepostal)
    {
        LOGGER.debug(">>> Debut methode verifierExistenceCodePostalRG22");
        if (codepostal.length() == 0 || (!adressedao.checkExistCodePostal(codepostal)))
        {
            throw new RegleGestionException(Messages.getString("exception.codepostal.nexistepas")
                + codepostal);
        }
    }

    /**
     * {@inheritDoc}
     * 
     * 
     */
    @Override
    public List<CodePostal> rechercherTousLesCodesPostaux()
    {
        LOGGER.debug(">>> Debut methode rechercherTousLesCodesPostaux");
        return adressedao.findCodePostaux();
    }

    /**
     * Modificateur de l attribut adressedao.
     * 
     * @param adressedao le nouveau adressedao
     */
    @Override
    public void setAdressedao(AdresseDao adressedao)
    {
        this.adressedao = adressedao;
    }

}
