/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 */
package fr.gouv.finances.lombok.adresse.dao;

/**
 * Propriétés liées aux données des adresses.
 * @see fr.gouv.finances.lombok.adresse.bean.CodePostal
 * @see fr.gouv.finances.lombok.adresse.bean.Commune
 * @see fr.gouv.finances.lombok.adresse.bean.Departement
 * @see fr.gouv.finances.lombok.adresse.bean.PaysInsee
 * @see fr.gouv.finances.lombok.adresse.bean.PaysIso
 * @see fr.gouv.finances.lombok.adresse.bean.Region
 *
 * @author Christophe Breheret-Girardin
 */
public enum ProprieteAdresse
{
    /** Propriété code département INSEE. */
    CODE_DEPARTEMENT("codedepartementINSEE"),

    /** Propriété région d'un département. */
    REGION("laRegion"),

    /** Propriété ville d'un code postal. */
    VILLE("ville"),

    /** Propriété du code postal. */
    CODE_POSTAL("code"),

    /** Propriété des départements d'une région. */
    DEPARTEMENTS("lesDepartements"),

    /** Propriété du département de la commune. */
    DEPARTEMENT("leDepartement"),

    /** Propriété du nom d'un pays. */
    NOM_PAYS("nom"),

    /** Propriété du code INSEE d'un pays. */
    CODE_PAYS_INSEE("codePaysInsee"),

    /** Propriété du code ISO d'un pays. */
    CODE_PAYS_ISO("codeIsoAlpha2"),
    
    /** Propriété du libellé d'une région. */
    LIBELLE_REGION("libelleregion"),

    /** Propriété des communes d'une région. */
    COMMUNES("lesCommunes"),

    /** Propriété du code INSEE d'une commune. */
    CODE_COMMUNE("codecommuneINSEE"),

    /** Propriété du code INSEE d'une région. */
    CODE_REGION("coderegionINSEE");

    /** Nom de la propriété */
    private String nom;

    /**
     * Constructeur.
     *
     * @param libelleNom libellé du nom de la propriété
     */
    private ProprieteAdresse(String libelleNom)
    {
        nom = libelleNom;
    }

    /**
     * Accesseur de nom.
     *
     * @return nom
     */
    public String getNom()
    {
        return nom;
    }
    
}
