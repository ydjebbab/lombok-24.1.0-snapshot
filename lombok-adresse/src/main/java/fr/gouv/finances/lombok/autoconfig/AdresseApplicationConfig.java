package fr.gouv.finances.lombok.autoconfig;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

/**
 * Classe de configuration qui permet de conditionner les ressources à importer
 * 
 * @author celfer Date: 7 févr. 2020
 */
@Configuration
@ConditionalOnExpression("'${lombok.composant.adresse.inclus}' == 'true' or '${lombok.composant.structure.inclus}' == 'true'")
@ImportResource({"classpath*:conf/applicationContext-commun-dao.xml",
        "classpath*:conf/applicationContext-adresse-dao.xml",
        "classpath*:conf/applicationContext-commun-service.xml",
        "classpath:conf/applicationContext-adresse-service.xml"})
@ComponentScan(basePackages = {"fr.gouv.finances.lombok.adresse.jpa.dao.impl"})
@EnableConfigurationProperties(AdresseProperties.class)
public class AdresseApplicationConfig
{

    private static final Logger log = LoggerFactory.getLogger(AdresseApplicationConfig.class);

    @Autowired
    private AdresseProperties adresseProperties;

    @PostConstruct
    private void afficherProprietes()
    {
        if (log.isInfoEnabled())
        {
            log.info("Valeurs des propriétés du composant adresse : {}  ", adresseProperties);
        }
    }

}
