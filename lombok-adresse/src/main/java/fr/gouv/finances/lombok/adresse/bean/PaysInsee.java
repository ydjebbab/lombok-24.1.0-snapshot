/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 */
package fr.gouv.finances.lombok.adresse.bean;

import java.util.Objects;

import fr.gouv.finances.lombok.util.base.BaseBean;

/**
 * Pays selon la codification INSEE.
 * 
 * @author lcontinsouzas-cp
 * @author Christophe Breheret-Girardin
 */
public class PaysInsee extends BaseBean
{

    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = -6678671232273121550L;

    /** id. */
    private long id;

    /** code pays insee. */
    private String codePaysInsee;

    /** nom. */
    private String nom;

    /**
     * default constructor.
     */
    public PaysInsee()
    {
        super();
    }

    /**
     * Accesseur de l'attribut code pays insee.
     * 
     * @return code pays insee
     */
    public String getCodePaysInsee()
    {
        return this.codePaysInsee;
    }

    /**
     * Accesseur de l'attribut id.
     * 
     * @return id
     */
    public long getId()
    {
        return id;
    }

    /**
     * Accesseur de l'attribut nom.
     * 
     * @return nom
     */
    public String getNom()
    {
        return this.nom;
    }

    /**
     * Modificateur de l'attribut code pays insee.
     * 
     * @param codePaysInsee valeur de code pays insee
     */
    public void setCodePaysInsee(String codePaysInsee)
    {
        this.codePaysInsee = codePaysInsee;
    }

    /**
     * Modificateur de l'attribut id.
     * 
     * @param id valeur de id
     */
    public void setId(long id)
    {
        this.id = id;
    }

    /**
     * Modificateur de l'attribut nom.
     * 
     * @param nom valeur de nom
     */
    public void setNom(String nom)
    {
        this.nom = nom;
    }

    /**
     * {@inheritDoc}
     * 
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode()
    {
        return Objects.hash(codePaysInsee);
    }

    /**
     * {@inheritDoc}
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object object)
    {
        if (object == null)
        {
            return false;
        }

        if (this.getClass() != object.getClass())
        {
            return false;
        }

        PaysInsee paysInsee = (PaysInsee) object;
        return Objects.equals(codePaysInsee, paysInsee.codePaysInsee);
    }

}