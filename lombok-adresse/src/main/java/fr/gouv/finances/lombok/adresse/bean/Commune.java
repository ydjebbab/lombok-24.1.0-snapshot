/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 *
 */
package fr.gouv.finances.lombok.adresse.bean;

import java.util.Objects;

import fr.gouv.finances.lombok.util.base.BaseBean;
import fr.gouv.finances.lombok.util.exception.ProgrammationException;

/**
 * Commune à la norme INSEE.
 *
 * @author Christophe Breheret-Girardin
 */
public class Commune extends BaseBean
{

    /** Initialisation de l'UID. */
    private static final long serialVersionUID = -3279589436582018673L;

    /** Identifiant. */
    private long id;

    /** Code commune insee. */
    private String codecommuneINSEE;

    /** Libellé commune. */
    private String libellecommune;

    /** Département. */
    private Departement leDepartement;

    /**
     * Constructeur par défaut.
     */
    public Commune()
    {
        super();
    }

    /**
     * Accesseur de l'attribut codecommune insee.
     * 
     * @return codecommune insee
     */
    public String getCodecommuneINSEE()
    {
        return this.codecommuneINSEE;
    }

    /**
     * Accesseur de l'attribut codecommune nominoe.
     * 
     * @return codecommune nominoe
     */
    public String getCodecommuneNOMINOE()
    {
        String codenominoe = null;
        if (this.codecommuneINSEE != null)
        {
            if (this.codecommuneINSEE.startsWith("97") || this.codecommuneINSEE.startsWith("98")
                || this.codecommuneINSEE.startsWith("99"))
            {
                // dom-tom : code commune sur 2
                codenominoe = "0" + this.codecommuneINSEE.substring(3, 5);
            }
            else
            {
                // metropole : code commune sur 3
                codenominoe = this.codecommuneINSEE.substring(2, 5);
            }
        }
        return codenominoe;
    }

    /**
     * Accesseur de l'attribut id.
     * 
     * @return id
     */
    public long getId()
    {
        return id;
    }

    /**
     * Accesseur de l'attribut le departement.
     * 
     * @return le departement
     */
    public Departement getLeDepartement()
    {
        return this.leDepartement;
    }

    /**
     * Accesseur de l'attribut libellecommune.
     * 
     * @return libellecommune
     */
    public String getLibellecommune()
    {
        return this.libellecommune;
    }

    /**
     * Modificateur de l'attribut codecommune insee.
     * 
     * @param codecommuneINSEE valeur de codecommune insee
     */
    public void setCodecommuneINSEE(String codecommuneINSEE)
    {
        this.codecommuneINSEE = codecommuneINSEE;
    }

    /**
     * methode Sets the codecommune nominoe : --.
     * 
     * @param codecommuneNOMINOE --
     * @param codedepartementINSEE --
     */
    public void setCodecommuneNOMINOE(String codecommuneNOMINOE, String codedepartementINSEE)
    {
        // Le code Nominoe ne comprend pas le département . On le rajoute
        if (codecommuneNOMINOE != null && codedepartementINSEE != null)
        {
            if (codedepartementINSEE.startsWith("97") || codedepartementINSEE.startsWith("98")
                || codedepartementINSEE.startsWith("99"))
            {
                // dom-tom : code dep sur 3 code commune sur 2
                this.setCodecommuneINSEE(codedepartementINSEE + codecommuneNOMINOE.substring(1, 3));
            }
            else
            {
                // metropole : code dep sur 2 code commune sur 3
                this.setCodecommuneINSEE(codedepartementINSEE + codecommuneNOMINOE);
            }
        }
        else
        {
            throw new ProgrammationException("Passage d'un argument null dans le setter setCodecommuneNOMINOE");
        }
    }

    /**
     * Modificateur de l'attribut id.
     * 
     * @param id valeur de id
     */
    public void setId(long id)
    {
        this.id = id;
    }

    /**
     * Modificateur de l'attribut le departement.
     * 
     * @param leDepartement valeur de le departement
     */
    public void setLeDepartement(Departement leDepartement)
    {
        this.leDepartement = leDepartement;
    }

    /**
     * Modificateur de l'attribut libellecommune.
     * 
     * @param libellecommune valeur de libellecommune
     */
    public void setLibellecommune(String libellecommune)
    {
        this.libellecommune = libellecommune;
    }

    /**
     * {@inheritDoc}
     * 
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode()
    {
        return Objects.hash(codecommuneINSEE);
    }

    /**
     * {@inheritDoc}
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object object)
    {
        if (object == null)
        {
            return false;
        }

        if (this.getClass() != object.getClass())
        {
            return false;
        }

        Commune commune = (Commune) object;
        return Objects.equals(codecommuneINSEE, commune.codecommuneINSEE);
    }
}