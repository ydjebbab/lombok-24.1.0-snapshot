/*
 * Copyright (c) 2017 DGFiP - Tous droits réservés
 * 
*/
/**
 * Entités du composant lombok-adresse
 * @author chouard
 * @version 1.0
 */
package fr.gouv.finances.lombok.adresse.bean;