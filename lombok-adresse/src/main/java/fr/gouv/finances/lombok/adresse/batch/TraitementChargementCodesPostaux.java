/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 *
 */
package fr.gouv.finances.lombok.adresse.batch;

import org.springframework.transaction.support.TransactionTemplate;

import fr.gouv.finances.lombok.adresse.service.AdresseService;
import fr.gouv.finances.lombok.batch.ServiceBatchCommun;
import fr.gouv.finances.lombok.parsertexte.integration.GestionnaireObjetExtrait;

/**
 * Interface du batch de chargement des codes postaux.
 * 
 * @author Christophe Breheret-Girardin
 */
public interface TraitementChargementCodesPostaux extends GestionnaireObjetExtrait, ServiceBatchCommun
{

    /**
     * Méthode permettant d'effectuer le traitement batch.
     */
    void traiterBatch();

    /**
     * Modificateur de l'attribut adresseserviceso.
     * 
     * @param adresseserviceso le nouveau adresseserviceso
     */
    void setAdresseserviceso(final AdresseService adresseserviceso);

    /**
     * Modificateur de l'attribut fichier codes postaux.
     * 
     * @param fichierCodesPostaux le nouveau fichier codes postaux
     */
    void setFichierCodesPostaux(final String fichierCodesPostaux);

    /**
     * Modificateur de l'attribut transaction template.
     * 
     * @param transactionTemplate le nouveau transaction template
     */
    void setTransactionTemplate(final TransactionTemplate transactionTemplate);
}
