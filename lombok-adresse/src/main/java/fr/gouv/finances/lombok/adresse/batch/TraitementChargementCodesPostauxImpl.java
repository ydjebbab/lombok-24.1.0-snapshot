/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 *
 */
package fr.gouv.finances.lombok.adresse.batch;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.support.TransactionTemplate;
import fr.gouv.finances.lombok.adresse.bean.CodePostal;
import fr.gouv.finances.lombok.adresse.service.AdresseService;
import fr.gouv.finances.lombok.batch.ServiceBatchCommunParserTexteImpl;
import fr.gouv.finances.lombok.parsertexte.integration.IGestionnaireImportationFichiers;
import fr.gouv.finances.lombok.parsertexte.integration.IntegrationException;
import fr.gouv.finances.lombok.util.exception.ApplicationExceptionTransformateur;
import fr.gouv.finances.lombok.util.exception.ProgrammationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

/**
 * Implémentation du batch de chargement des codes postaux.
 *
 * @author amleplatinec
 * @author Christophe Breheret-Girardin
 */
@Service("traitementchargementcodespostaux")
@Profile("batch")
@Lazy(true)
public class TraitementChargementCodesPostauxImpl extends ServiceBatchCommunParserTexteImpl implements TraitementChargementCodesPostaux {

    /**
     * Initialisation de la journalisation.
     */
    protected static final Logger LOGGER = LoggerFactory.getLogger(TraitementChargementCodesPostauxImpl.class);

    /**
     * Service de gestion des adresses.
     */
    @Autowired()
    @Qualifier("adresseserviceso")
    private AdresseService adresseserviceso;

    /**
     * Fichier des codes postaux.
     */
    private String fichierCodesPostaux;

    /**
     * Template de gestion des transactions.
     */
    @Autowired()
    @Qualifier("transactionTemplate")
    private TransactionTemplate transactionTemplate;

    /**
     * Constructeur.
     */
    public TraitementChargementCodesPostauxImpl() {
        super();
    }

    /**
     * {@inheritDoc}
     *
     * @see fr.gouv.finances.lombok.parsertexte.integration.GestionnaireObjetExtrait#extractionObjet(java.util.Map,
     *      java.lang.String, int, boolean, java.lang.String)
     * 
     */
    @Override
    public int extractionObjet(@SuppressWarnings("rawtypes") final Map pObjetsExtraits, final String pIdLigne, final int pintNumLigne, final boolean pAnomalieDetecte, String pContenuLigne) {
        // Récupération du code postal
        Object unobjet = pObjetsExtraits.get("CodePostal");
        CodePostal uncodepostal;
        // Est-ce vraiment un code postal ?
        if (unobjet instanceof CodePostal) {
            uncodepostal = (CodePostal) unobjet;
            // Journalisation des informations du code postal
            LOGGER.debug("Ajout du code postal : code = '{}' - ville = '{}'", uncodepostal.getCode(), uncodepostal.getVille());
            // Ajout du code postal en base de données
            adresseserviceso.ajouterUnCodePostal(uncodepostal);
        } else {
            // Gestion de l'erreur
            String nom = "";
            if (unobjet != null) {
                // Récupération du type d'objet trouvé à la place de CodePostal
                nom = unobjet.getClass().getName();
            }
            throw new ProgrammationException("Erreur : le parseur de texte ne renvoie pas un objet de type CodePostal mais de type " + nom);
        }
        // Retourne 0 pour indiquer que tout s'est bien déroulé
        return 0;
    }

    /**
     * {@inheritDoc}
     *
     * @see fr.gouv.finances.lombok.batch.ServiceBatchCommunImpl#traiterBatch()
     * 
     */
    @Override
    public void traiterBatch() {
        // Récupération du gestionnaire d'importation des fichiers
        final IGestionnaireImportationFichiers gestionnaireImportationFichiers = this.construireUnGestionnaireImportationFichiers();
        // Astuce pour pouvoir référencer l'instance du batch dans le callback
        final TraitementChargementCodesPostauxImpl batchSelfRef = this;
        final File fichierPostal;
        try {
            // Récupération du fichier des codes postaux
            fichierPostal = new File(new URL(this.fichierCodesPostaux).getFile());
        } catch (MalformedURLException mue) {
            // Gestion des erreurs liés aux chargement des codes postaux
            throw ApplicationExceptionTransformateur.transformer("l'url spécifiée est erronée", mue);
        }
        // Exécution du traitement
        transactionTemplate.execute(transactionStatus -> traitement(gestionnaireImportationFichiers, fichierPostal, batchSelfRef));
    }

    /**
     * Méthode permettant d'exécuter le traitement d'importation des fichiers
     *
     * @param gestionnaireImportationFichiers gestionnaire d'importation des fichiers
     * @param fichierPostal fichier contenant les codes postaux
     * @param traitementChargement batch de traitement du chargement des codes postaux
     * @return null
     */
    private Object traitement(IGestionnaireImportationFichiers gestionnaireImportationFichiers, final File fichierPostal, final TraitementChargementCodesPostauxImpl traitementChargement) {
        // Suppression des anciens codes postaux
        adresseserviceso.supprimerTousLesCodesPostaux();
        try {
            // Chargement des nouveaux codes postaux
            gestionnaireImportationFichiers.lectureEtDecodageFichier(fichierPostal, traitementChargement);
            return null;
        } catch (IntegrationException e) {
            // Gestion des erreurs
            throw ApplicationExceptionTransformateur.transformer(e);
        }
    }

    /**
     * Modificateur de l'attribut adresseserviceso.
     *
     * @param adresseserviceso le nouveau adresseserviceso
     */
    @Override
    public final void setAdresseserviceso(final AdresseService adresseserviceso) {
        this.adresseserviceso = adresseserviceso;
    }

    /**
     * Modificateur de l'attribut fichier codes postaux.
     *
     * @param fichierCodesPostaux le nouveau fichier codes postaux
     */
    @Override
    @Value("${traitementchargementcodespostaux.fichiercodespostaux}")
    public final void setFichierCodesPostaux(final String fichierCodesPostaux) {
        this.fichierCodesPostaux = fichierCodesPostaux;
    }

    /**
     * Modificateur de l'attribut transaction template.
     *
     * @param transactionTemplate le nouveau transaction template
     */
    @Override
    public final void setTransactionTemplate(final TransactionTemplate transactionTemplate) {
        this.transactionTemplate = transactionTemplate;
    }

    @Value("${traitementchargementcodespostaux.versionbatch}")
    @Override()
    public void setVersionBatch(String versionBatch) {
        super.setVersionBatch(versionBatch);
    }

    @Value("${traitementchargementcodespostaux.nombatch}")
    @Override()
    public void setNomBatch(String nomBatch) {
        super.setNomBatch(nomBatch);
    }
}
