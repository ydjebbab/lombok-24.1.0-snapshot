/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 */
package fr.gouv.finances.lombok.adresse.bean;

import java.util.Objects;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import fr.gouv.finances.lombok.util.base.BaseBean;

/**
 * Pays selon la codification ISO.
 *
 * @author lcontinsouzas-cp
 * @author Christophe Breheret-Girardin
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class PaysIso extends BaseBean
{

    /** Initialisation de l'UID. */
    private static final long serialVersionUID = -1846785869119341949L;

    /** Identifiant. */
    @XmlElement(name = "ISO_3166-1_Alpha-2_code")
    private String id;

    /** Code iso alpha2. */
    @XmlElement(name = "ISO_3166-1_Alpha-2_code")
    private String codeIsoAlpha2;

    /** Nom. */
    @XmlElement(name = "ISO_3166-1_Country_name")
    private String nom;

    /**
     * Constructeur par défaut.
     */
    public PaysIso()
    {
        super();
    }

    /**
     * Constructeur avec identifiant.
     *
     * @param id identifiant
     */
    public PaysIso(String id)
    {
        super();
        this.id = id;
    }

    /**
     * Accesseur de l'attribut code iso alpha2.
     * 
     * @return code iso alpha2
     */
    public String getCodeIsoAlpha2()
    {
        return this.codeIsoAlpha2;
    }

    /**
     * Accesseur de l'attribut id.
     * 
     * @return id
     */
    public String getId()
    {
        return this.id;
    }

    /**
     * Accesseur de l'attribut nom.
     * 
     * @return nom
     */
    public String getNom()
    {
        return this.nom;
    }

    /**
     * Modificateur de l'attribut code iso alpha2.
     * 
     * @param codeIsoAlpha2 valeur de code iso alpha2
     */
    public void setCodeIsoAlpha2(String codeIsoAlpha2)
    {
        this.codeIsoAlpha2 = codeIsoAlpha2;
    }

    /**
     * Modificateur de l'attribut id.
     * 
     * @param id valeur de id
     */
    public void setId(String id)
    {
        this.id = id;
    }

    /**
     * Modificateur de l'attribut nom.
     * 
     * @param nom valeur de nom
     */
    public void setNom(String nom)
    {
        this.nom = nom;
    }

    /**
     * {@inheritDoc}
     * 
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode()
    {
        return Objects.hash(codeIsoAlpha2);
    }

    /**
     * {@inheritDoc}
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object object)
    {
        if (object == null)
        {
            return false;
        }

        if (this.getClass() != object.getClass())
        {
            return false;
        }

        PaysIso paysIso = (PaysIso) object;
        return Objects.equals(codeIsoAlpha2, paysIso.codeIsoAlpha2);
    }

}