/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 *
 */
package fr.gouv.finances.lombok.adresse.bean;

import java.util.Objects;

import fr.gouv.finances.lombok.util.base.BaseBean;

/**
 * Code postal à la norme postale.
 * 
 * @author lcontinsouzas-cp
 * @author Christophe Breheret-Girardin
 */
public class CodePostal extends BaseBean
{

    /** Initialisation de l'UID. */
    private static final long serialVersionUID = 6598902158709432809L;

    /** Identifiant. */
    private long id;

    /** version. */
    private Integer version;

    /** code. */
    private String code;

    /** ville. */
    private String ville;

    /**
     * Constructeur par défaut.
     */
    public CodePostal()
    {
        super();
    }

    /**
     * Constructeur.
     * 
     * @param id identifiant du code postal
     */
    public CodePostal(long id)
    {
        super();
        this.id = id;
    }

    /**
     * Accesseur de l'attribut code.
     * 
     * @return code
     */
    public String getCode()
    {
        return this.code;
    }

    /**
     * Accesseur de l'attribut id.
     * 
     * @return id
     */
    public long getId()
    {
        return this.id;
    }

    /**
     * Accesseur de l'attribut version.
     * 
     * @return version
     */
    public Integer getVersion()
    {
        return this.version;
    }

    /**
     * Accesseur de l'attribut ville.
     * 
     * @return ville
     */
    public String getVille()
    {
        return this.ville;
    }

    /**
     * Modificateur de l'attribut code.
     * 
     * @param code valeur de code
     */
    public void setCode(String code)
    {
        this.code = code;
    }

    /**
     * Modificateur de l'attribut id.
     * 
     * @param id valeur de id
     */
    public void setId(long id)
    {
        this.id = id;
    }

    /**
     * Modificateur de l'attribut version.
     * 
     * @param version valeur de version
     */
    public void setVersion(Integer version)
    {
        this.version = version;
    }

    /**
     * Modificateur de l'attribut ville.
     * 
     * @param ville valeur de ville
     */
    public void setVille(String ville)
    {
        this.ville = ville;
    }

    /**
     * {@inheritDoc}
     * 
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode()
    {
        return Objects.hash(code, ville);
    }

    /**
     * {@inheritDoc}
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object object)
    {
        if (object == null)
        {
            return false;
        }

        if (this.getClass() != object.getClass())
        {
            return false;
        }

        CodePostal codePostal = (CodePostal) object;
        return Objects.equals(code, codePostal.code)
            && Objects.equals(ville, codePostal.ville);
    }
}