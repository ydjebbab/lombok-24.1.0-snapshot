package fr.gouv.finances.lombok.autoconfig;

import org.springframework.boot.context.properties.ConfigurationProperties;

import fr.gouv.finances.lombok.autoconfig.LombokComposantProperties;

/**
 * @author celinio fernandes Date: Feb 19, 2020
 */
@ConfigurationProperties(prefix = "lombok.composant.adresse")
public class AdresseProperties extends LombokComposantProperties
{

    @Override
    public String toString()
    {
        return "AdresseProperties [inclus=" + inclus + "]";
    }

}
