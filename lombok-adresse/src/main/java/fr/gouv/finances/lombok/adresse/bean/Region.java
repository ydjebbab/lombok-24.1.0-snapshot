/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 *
 */
package fr.gouv.finances.lombok.adresse.bean;

import java.util.Objects;
import java.util.Set;

/**
 * Région selon la codification INSEE.
 * 
 * @author lcontinsouzas-cp
 * @author Christophe Breheret-Girardin
 */
public class Region implements java.io.Serializable
{
    /** Initialisation de l'UID. */
    private static final long serialVersionUID = -1118721444854684363L;

    /** Identifiant. */
    private long id;

    /** coderegion insee. */
    private String coderegionINSEE;

    /** libelleregion. */
    private String libelleregion;

    /** les departements. */
    private Set<Departement> lesDepartements;

    /**
     * Constructeur de la classe Region.java
     */
    public Region()
    {
        super();
    }

    /**
     * Accesseur de l'attribut coderegion insee.
     * 
     * @return coderegion insee
     */
    public String getCoderegionINSEE()
    {
        return this.coderegionINSEE;
    }

    /**
     * Accesseur de l'attribut id.
     * 
     * @return id
     */
    public long getId()
    {
        return id;
    }

    /**
     * Modificateur de l'attribut les departements.
     * 
     * @return les departements
     */
    public Set<Departement> getLesDepartements()
    {
        return this.lesDepartements;
    }

    /**
     * Accesseur de l'attribut libelleregion.
     * 
     * @return libelleregion
     */
    public String getLibelleregion()
    {
        return this.libelleregion;
    }

    /**
     * Modificateur de l'attribut coderegion insee.
     * 
     * @param coderegionINSEE valeur de coderegion insee
     */
    public void setCoderegionINSEE(String coderegionINSEE)
    {
        this.coderegionINSEE = coderegionINSEE;
    }

    /**
     * Modificateur de l'attribut identifiant.
     * 
     * @param identifiant valeur de id
     */
    public void setId(long identifiant)
    {
        this.id = identifiant;
    }

    /**
     * Modificateur de l'attribut les departements.
     * 
     * @param lesDepartements valeur de les departements
     */
    public void setLesDepartements(Set<Departement> lesDepartements)
    {
        this.lesDepartements = lesDepartements;
    }

    /**
     * Modificateur de l'attribut libelleregion.
     * 
     * @param libelleregion valeur de libelleregion
     */
    public void setLibelleregion(String libelleregion)
    {
        this.libelleregion = libelleregion;
    }

    /**
     * {@inheritDoc}
     * 
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode()
    {
        return Objects.hash(coderegionINSEE);
    }

    /**
     * {@inheritDoc}
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object object)
    {
        if (object == null)
        {
            return false;
        }

        if (this.getClass() != object.getClass())
        {
            return false;
        }

        Region region = (Region) object;
        return Objects.equals(coderegionINSEE, region.coderegionINSEE);
    }

}