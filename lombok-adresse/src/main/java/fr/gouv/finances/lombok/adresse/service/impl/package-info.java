/*
 * Copyright (c) 2017 DGFiP - Tous droits réservés
 * 
*/
/**
 * Paquet contenant l'implémentation des services métier des adresses.
 *
 * @author Christophe Breheret-Girardin
 */
package fr.gouv.finances.lombok.adresse.service.impl;