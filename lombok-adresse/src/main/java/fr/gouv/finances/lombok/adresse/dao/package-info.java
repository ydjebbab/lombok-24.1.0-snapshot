/*
 * Copyright (c) 2017 DGFiP - Tous droits réservés
 * 
*/
// Projet : lombok-adresse
/**
 * Documentation du paquet fr.gouv.finances.lombok.adresse.dao
 * @author chouard
 * @version 1.0
 */
package fr.gouv.finances.lombok.adresse.dao;