/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 *
 */
package fr.gouv.finances.lombok.adresse.service;

import java.util.List;
import java.util.Map;

import fr.gouv.finances.lombok.adresse.bean.CodePostal;
import fr.gouv.finances.lombok.adresse.bean.Commune;
import fr.gouv.finances.lombok.adresse.bean.Departement;
import fr.gouv.finances.lombok.adresse.bean.PaysInsee;
import fr.gouv.finances.lombok.adresse.bean.PaysIso;
import fr.gouv.finances.lombok.adresse.bean.Region;
import fr.gouv.finances.lombok.adresse.dao.AdresseDao;

/**
 * Interface du service métier de gestion des adresses.
 *
 * @author Christophe Breheret-Girardin
 */
public interface AdresseService
{

    /**
     * Méthode permettant d'associer le DAO permettant la gestion des données liées aux adresses.
     *
     * @param adresseDao DAO de gestion des données liées aux adresses
     */
    void setAdressedao(AdresseDao adresseDao);
    
    /**
     * Permet d'ajouter une objet CodePostal. A priori uniquement utilisé par un batch de chargement.
     * 
     * @param uncodepostal code postal à ajouter
     */
    void ajouterUnCodePostal(CodePostal uncodepostal);

    /**
     * Permet d'ajouter un objet Departement. A priori uniquement utilisé par un batch de chargement.
     * 
     * @param unDepartement département à ajouter
     */
    void ajouterUnDepartement(Departement unDepartement);

    /**
     * Permet d'ajouter un objet Commune. A priori uniquement utilisé par un batch de chargement.
     * 
     * @param uneCommune commune à ajouter
     */
    void ajouterUneCommune(Commune uneCommune);

    /**
     * Permet d'ajouter un objet Region. A priori uniquement utilisé par un batch de chargement.
     * 
     * @param uneRegion région à ajouter
     */
    void ajouterUneRegion(Region uneRegion);

    /**
     * Permet d'ajouter un objet PaysInsee. A priori uniquement utilisé par un batch de chargement.
     * 
     * @param unPaysInsee pays INSEE à ajouter
     */
    void ajouterUnPaysInsee(PaysInsee unPaysInsee);

    /**
     * Permet d'ajouter un objet PaysIso. A priori uniquement utilisé par un batch de chargement.
     * 
     * @param unPaysIso pays ISO à ajouter
     */
    void ajouterUnPaysIso(PaysIso unPaysIso);

    /**
     * Renvoie tous les pays recenses par l'insee sous forme d'une map cle=nompays (String) , valeur=nompays (String).
     * 
     * @return Map des noms de pays chargée
     */
    Map<String, String> chargerMapPaysInsee();

    /**
     * Renvoie tous les pays recenses par l'iso sous forme d'une map cle=nompays (String) , valeur=nompays (String).
     * 
     * @return Map des noms de pays chargée
     */
    Map<String, String> chargerMapPaysIso();

    /**
     * Permet de modifier un objet Commune. A priori uniquement utilisé par un batch de chargement.
     * 
     * @param unecommune commune à modifier
     */
    void modifierUneCommune(Commune unecommune);

    /**
     * Permet de modfier un objet PaysInsee. A priori uniquement utilisé par un batch de chargement.
     * 
     * @param unPaysInsee pays INSEE à modifier
     */
    void modifierUnPaysInsee(PaysInsee unPaysInsee);

    /**
     * Permet de modfierr un objet PaysIso. A priori uniquement utilisé par un batch de chargement.
     * 
     * @param unPaysIso pays ISO à modifier
     */
    void modifierUnPaysIso(PaysIso unPaysIso);

    /**
     * Retourne une liste d'objets CodePostal correspondant soit au codepostal soit à la ville.  <br>
     * En ignorant la casse et les espaces avant après soit aux deux si les deux paramètres sont transmis.
     * 
     * @param codepostal Valeur du code postal à rechercher
     * @param ville Ville du code postal à recherche
     * @return la liste des codes postaux correspondants
     */
    List<CodePostal> rechercherCodePostalParCodeVille(String codepostal, String ville);

    /**
     * Retourne tous les départements et leur région associée.
     * 
     * @return la liste des départements
     */
    List<Departement> rechercherTousLesDepartementsAvecRegion();

    /**
     * Retourne tous les Pays.
     * 
     * @return la liste des pays INSEE
     */
    List<PaysInsee> rechercherTousLesPaysInsee();

    /**
     * Retourne toutes les régions et leurs départements associés.
     * 
     * @return la liste des régions
     */
    List<Region> rechercherToutesLesRegionsAvecDepartement();

    /**
     * Permet de rechercher un département par son code.
     * 
     * @param uncodedepartementINSEE code du département
     * @return le département correspondant
     */
    Departement rechercherUnDepartementParCodeDepartementINSEE(String uncodedepartementINSEE);

    /**
     * Permet de rechercher une commune par code commune INSEE.
     * 
     * @param uncodecommuneINSEE code de la commune
     * @return la commune correspondante
     */
    Commune rechercherUneCommuneParCodeCommuneINSEE(String uncodecommuneINSEE);

    /**
     * Permet de rechercher une région par code région INSEE.
     * 
     * @param uncoderegionINSEE code région
     * @return la région correspondante
     */
    Region rechercherUneRegionParCodeRegionINSEE(String uncoderegionINSEE);

    /**
     * Permet de rechercher un pays par code pays insee.
     * 
     * @param uncodepaysinsee code pays
     * @return le pays correspondant
     */
    PaysInsee rechercherUnPaysParCodeInsee(String uncodepaysinsee);

    /**
     * Permet de rechercher un pays par code pays iso.
     * 
     * @param uncodepaysiso code pays
     * @return le pays correspondant
     */
    PaysIso rechercherUnPaysParCodeIso(String uncodepaysiso);

    /**
     * Retourne une liste de villes correspondant à un code postal (plusieurs communes possibles).
     * 
     * @param codepostal valeur du code postal
     * @return la  liste des codes postaux correspondants
     */
    List<CodePostal> rechercherVillesPourUnCodePostal(String codepostal);

    /**
     * Supprime tous les codes postaux.
     */
    void supprimerTousLesCodesPostaux();

    /**
     * Verifie qu'il existe au moins une correspondance entre le codepostal et la ville en ignorant les espaces avant
     * après, ainsi que la casse.
     * 
     * @param codepostal code postal recherché
     * @param ville ville du code postal recherchée
     */
    void verifierCodePostalCorrespondAUnLibelleCommuneRG23(String codepostal, String ville);

    /**
     * verifie l'existence du code pays iso.
     * 
     * @param codepaysiso code pays
     */
    void verifierExistenceCodePaysISO(String codepaysiso);

    /**
     * Verifie l'existence du code postal.
     * 
     * @param codepostal code postal
     */
    void verifierExistenceCodePostalRG22(String codepostal);

    /**
     * Retourne tous les codes postaux.
     * 
     * @return la liste des codes postaux
     */
    List<CodePostal> rechercherTousLesCodesPostaux();
}