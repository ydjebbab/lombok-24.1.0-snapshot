/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 *
 */
package fr.gouv.finances.lombok.adresse.bean;

import java.util.Objects;
import java.util.Set;

import fr.gouv.finances.lombok.util.base.BaseBean;
import fr.gouv.finances.lombok.util.exception.ProgrammationException;

/**
 * Département selon la codification INSEE.
 *
 * @author lcontinsouzas-cp
 * @author Christophe Breheret-Girardin
 */
public class Departement extends BaseBean
{
    /** Initialisation de l'UID. */
    private static final long serialVersionUID = -8961423448410003570L;

    /** Identifiant. */
    private long id;

    /** codedepartement insee. */
    private String codedepartementINSEE;

    /** libelledepartement. */
    private String libelledepartement;

    /** les communes. */
    private Set<Commune> lesCommunes;

    /** la region. */
    private Region laRegion;

    /**
     * Constructeur par défaut.
     */
    public Departement()
    {
        super();
    }

    /**
     * Accesseur de l'attribut codedepartement insee.
     * 
     * @return codedepartement insee
     */
    public String getCodedepartementINSEE()
    {
        return this.codedepartementINSEE;
    }

    /**
     * Permet de récupérer le code département au format géré par Nominoe c'est à dire précédé d'un 0 lorsqu'il est sur
     * deux chiffres.
     * 
     * @return code département au format NOMINOE
     */
    public String getCodedepartementNOMINOE()
    {
        String codenominoe = null;
        if (this.getCodedepartementINSEE() != null)
        {
            if (this.getCodedepartementINSEE().length() == 2)
            {
                codenominoe = "0" + this.getCodedepartementINSEE();
            }
            else
            {
                codenominoe = this.getCodedepartementINSEE();
            }
        }
        return codenominoe;
    }

    /**
     * Retourne une chaîne de texte composée du code INSEE du département, un blanc et le libellé du département.
     * 
     * @return the code plus libelle departement
     */
    public String getCodePlusLibelleDepartement()
    {
        StringBuilder res = new StringBuilder();
        res.append(this.codedepartementINSEE);
        res.append(' ');
        res.append(this.libelledepartement);
        return res.toString();
    }

    /**
     * Accesseur de l'attribut id.
     * 
     * @return id
     */
    public long getId()
    {
        return id;
    }

    /**
     * Accesseur de l'attribut la region.
     * 
     * @return la region
     */
    public Region getLaRegion()
    {
        return this.laRegion;
    }

    /**
     * Accesseur de l'attribut les communes.
     * 
     * @return les communes
     */
    public Set<Commune> getLesCommunes()
    {
        return this.lesCommunes;
    }

    /**
     * Accesseur de l'attribut libelledepartement.
     * 
     * @return libelledepartement
     */
    public String getLibelledepartement()
    {
        return this.libelledepartement;
    }

    /**
     * Modificateur de l'attribut codedepartement insee.
     * 
     * @param codedepartementINSEE valeur de codedepartement insee
     */
    public void setCodedepartementINSEE(String codedepartementINSEE)
    {
        this.codedepartementINSEE = codedepartementINSEE;
    }

    /**
     * Permet de spécifier le code département avec le format géré par Nominoe c'est à dire précédé d'un 0 lorsqu'il est
     * sur deux chiffres.
     * 
     * @param codedepartementNOMINOE code du département précédé dun 0 si sur deux chiffres
     */

    public void setCodedepartementNOMINOE(String codedepartementNOMINOE)
    {
        // enlever le 0 de tête si présent
        if (codedepartementNOMINOE != null)
        {
            if (codedepartementNOMINOE.startsWith("0"))
            {
                this.setCodedepartementINSEE(codedepartementNOMINOE.substring(1, 3));
            }
            else
            {
                this.setCodedepartementINSEE(codedepartementNOMINOE);
            }
        }
        else
        {
            throw new ProgrammationException("Passage d'un argument null dans le setter setCodedepartementNOMINOE");
        }
    }

    /**
     * Modificateur de l'attribut id.
     * 
     * @param id valeur de id
     */
    public void setId(long id)
    {
        this.id = id;
    }

    /**
     * Modificateur de l'attribut la region.
     * 
     * @param laRegion valeur de la region
     */
    public void setLaRegion(Region laRegion)
    {
        this.laRegion = laRegion;
    }

    /**
     * Modificateur de l'attribut les communes.
     * 
     * @param lesCommunes valeur de les communes
     */
    public void setLesCommunes(Set<Commune> lesCommunes)
    {
        this.lesCommunes = lesCommunes;
    }

    /**
     * Modificateur de l'attribut libelledepartement.
     * 
     * @param libelledepartement valeur de libelledepartement
     */
    public void setLibelledepartement(String libelledepartement)
    {
        this.libelledepartement = libelledepartement;
    }

    /**
     * {@inheritDoc}
     * 
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode()
    {
        return Objects.hash(codedepartementINSEE);
    }

    /**
     * {@inheritDoc}
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object object)
    {
        if (object == null)
        {
            return false;
        }

        if (this.getClass() != object.getClass())
        {
            return false;
        }

        Departement departement = (Departement) object;
        return Objects.equals(codedepartementINSEE, departement.codedepartementINSEE);
    }

}