/*
 * Copyright (c) 2017 DGFiP - Tous droits réservés
 * 
*/
/**
 * Paquet de gestion du chargement des codes postaux
 * @author Christophe Breheret-Girardin
 */
package fr.gouv.finances.lombok.adresse.batch;