/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 *
 */
package fr.gouv.finances.lombok.adresse.bean;

import java.util.Objects;

import fr.gouv.finances.lombok.util.base.BaseBean;

/**
 * Adresse au format de la norme postale.
 * 
 * @author lcontinsouzas-cp
 * @author Christophe Breheret-Girardin
 */
public class Adresse extends BaseBean
{

    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = 2147186082600377186L;

    /** Identifiant. */
    private long id;

    /** code postal et cedex. */
    private String codePostalEtCedex;

    /** ligne2 service appart etg esc. */
    private String ligne2ServiceAppartEtgEsc;

    /** ligne3 batiment immeuble resid. */
    private String ligne3BatimentImmeubleResid;

    /** ligne4 numero libelle voie. */
    private String ligne4NumeroLibelleVoie;

    /** ligne5 lieu dit mention speciale. */
    private String ligne5LieuDitMentionSpeciale;

    /** pays. */
    private String pays;

    /** ville. */
    private String ville;

    /** version. */
    private int version;

    /**
     * Constructeur.
     */
    public Adresse()
    {
        super();
    }

    /**
     * Constructeur.
     *
     * @param id identifiant de l'adresse
     */
    public Adresse(long id)
    {
        super();
        this.id = id;
    }

    /**
     * Accesseur de l'attribut code postal et cedex.
     * 
     * @return code postal et cedex
     */
    public String getCodePostalEtCedex()
    {
        return this.codePostalEtCedex;
    }

    /**
     * Accesseur de l'attribut id.
     * 
     * @return id
     */
    public long getId()
    {
        return this.id;
    }

    /**
     * Accesseur de l'attribut ligne2 service appart etg esc.
     * 
     * @return ligne2 service appart etg esc
     */
    public String getLigne2ServiceAppartEtgEsc()
    {
        return this.ligne2ServiceAppartEtgEsc;
    }

    /**
     * Accesseur de l'attribut ligne3 batiment immeuble resid.
     * 
     * @return ligne3 batiment immeuble resid
     */
    public String getLigne3BatimentImmeubleResid()
    {
        return this.ligne3BatimentImmeubleResid;
    }

    /**
     * Accesseur de l'attribut ligne4 numero libelle voie.
     * 
     * @return ligne4 numero libelle voie
     */
    public String getLigne4NumeroLibelleVoie()
    {
        return this.ligne4NumeroLibelleVoie;
    }

    /**
     * Accesseur de l'attribut ligne5 lieu dit mention speciale.
     * 
     * @return ligne5 lieu dit mention speciale
     */
    public String getLigne5LieuDitMentionSpeciale()
    {
        return this.ligne5LieuDitMentionSpeciale;
    }

    /**
     * Accesseur de l'attribut pays.
     * 
     * @return pays
     */
    public String getPays()
    {
        return this.pays;
    }

    /**
     * Accesseur de l'attribut version.
     * 
     * @return version
     */
    public int getVersion()
    {
        return version;
    }

    /**
     * Accesseur de l'attribut ville.
     * 
     * @return ville
     */
    public String getVille()
    {
        return this.ville;
    }

    /**
     * Modificateur de l'attribut code postal et cedex.
     * 
     * @param codePostalEtCedex valeur de code postal et cedex
     */
    public void setCodePostalEtCedex(String codePostalEtCedex)
    {
        this.codePostalEtCedex = codePostalEtCedex;
    }

    /**
     * Modificateur de l'attribut id.
     * 
     * @param id valeur de id
     */
    public void setId(long id)
    {
        this.id = id;
    }

    /**
     * Modificateur de l'attribut ligne2 service appart etg esc.
     * 
     * @param ligne2ServiceAppartEtageCouloirEsc valeur de ligne2 service appart etg esc
     */
    public void setLigne2ServiceAppartEtgEsc(String ligne2ServiceAppartEtageCouloirEsc)
    {
        this.ligne2ServiceAppartEtgEsc = ligne2ServiceAppartEtageCouloirEsc;
    }

    /**
     * Modificateur de l'attribut ligne3 batiment immeuble resid.
     * 
     * @param ligne3BatimentImmeubleResidence valeur de ligne3 batiment immeuble resid
     */
    public void setLigne3BatimentImmeubleResid(String ligne3BatimentImmeubleResidence)
    {
        this.ligne3BatimentImmeubleResid = ligne3BatimentImmeubleResidence;
    }

    /**
     * Modificateur de l'attribut ligne4 numero libelle voie.
     * 
     * @param ligne4NumeroLibelleVoie valeur de ligne4 numero libelle voie
     */
    public void setLigne4NumeroLibelleVoie(String ligne4NumeroLibelleVoie)
    {
        this.ligne4NumeroLibelleVoie = ligne4NumeroLibelleVoie;
    }

    /**
     * Modificateur de l'attribut ligne5 lieu dit mention speciale.
     * 
     * @param ligne5LieuDitMentionSpeciale valeur de ligne5 lieu dit mention speciale
     */
    public void setLigne5LieuDitMentionSpeciale(String ligne5LieuDitMentionSpeciale)
    {
        this.ligne5LieuDitMentionSpeciale = ligne5LieuDitMentionSpeciale;
    }

    /**
     * Modificateur de l'attribut pays.
     * 
     * @param pays valeur de pays
     */
    public void setPays(String pays)
    {
        this.pays = pays;
    }

    /**
     * Modificateur de l'attribut version.
     * 
     * @param version valeur de version
     */
    public void setVersion(int version)
    {
        this.version = version;
    }

    /**
     * Modificateur de l'attribut ville.
     * 
     * @param ville valeur de ville
     */
    public void setVille(String ville)
    {
        this.ville = ville;
    }

    /**
     * {@inheritDoc}
     * 
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode()
    {
        return Objects.hash(codePostalEtCedex, ligne2ServiceAppartEtgEsc, ligne3BatimentImmeubleResid, ligne4NumeroLibelleVoie,
            ligne5LieuDitMentionSpeciale, pays, ville);
    }

    /**
     * {@inheritDoc}
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object object)
    {
        if (object == null)
        {
            return false;
        }

        if (this.getClass() != object.getClass())
        {
            return false;
        }

        Adresse adresse = (Adresse) object;
        return Objects.equals(codePostalEtCedex, adresse.codePostalEtCedex)
            && Objects.equals(ligne2ServiceAppartEtgEsc, adresse.ligne2ServiceAppartEtgEsc)
            && Objects.equals(ligne3BatimentImmeubleResid, adresse.ligne3BatimentImmeubleResid)
            && Objects.equals(ligne4NumeroLibelleVoie, adresse.ligne4NumeroLibelleVoie)
            && Objects.equals(ligne5LieuDitMentionSpeciale, adresse.ligne5LieuDitMentionSpeciale)
            && Objects.equals(pays, adresse.pays)
            && Objects.equals(ville, adresse.ville);
    }

}