/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 *
 */
package fr.gouv.finances.lombok.adresse.dao;

import java.util.List;

import fr.gouv.finances.lombok.adresse.bean.CodePostal;
import fr.gouv.finances.lombok.adresse.bean.Commune;
import fr.gouv.finances.lombok.adresse.bean.Departement;
import fr.gouv.finances.lombok.adresse.bean.PaysInsee;
import fr.gouv.finances.lombok.adresse.bean.PaysIso;
import fr.gouv.finances.lombok.adresse.bean.Region;
import fr.gouv.finances.lombok.util.base.CoreBaseDao;

/**
 * Interface AdresseDao <br>
 * Fournit des services de persistance pour les entités CodePostaux, Pays, Commune, Département, Region.<br/>
 * Utilisez de préférence la classe de service métier AdresseService.
 * 
 * @see fr.gouv.finances.lombok.adresse.service.AdresseService
 * @author lcontinsouzas-cp
 * @author CBRE
 * @author CF
 */
public interface AdresseDao extends CoreBaseDao
{
    /**
     * Test s'il existe un objet CodePostal avec pour code la chaîne passée en paramètre. <br>
     * Le test ignore la casse des chaînes de caractères et les blancs aux extrêmités de la chaîne en paramètre.
     * 
     * @param code valeur du code postal
     * @return true si le code postal existe, false sinon
     */
    boolean checkExistCodePostal(String code);

    /**
     * Teste si un objet CodePostal existe avec pour 'code' et pour 'ville' les valeurs passées en paramètre.<br/>
     * Le test ignore la casse des chaînes de caractères et les espaces aux extrêmités des chaînes en paramètre
     * 
     * @param code valeur du code postal
     * @param ville ville
     * @return true si la combinaison code postal + ville existe, false sinon
     */
    boolean checkExistCodePostalXVille(String code, String ville);

    /**
     * Supprime tous les codes postaux.
     */
    void deleteTousLesCodesPostaux();

    /**
     * Retourne la liste des codes postaux, triée par ordre ascendant des codes.
     * 
     * @return la liste de code postal
     */
    List<CodePostal> findCodePostaux();

    /**
     * Retourne une liste d'objets CodePostal dont la propriété code est égale au code passé en paramètre <br>
     * en ignorant la casse et les caractères blancs aux extrêmités du paramètre 'code'. <br>
     * Les objets CodePostal sont triés par nom de ville ascendant.
     * 
     * @param code valeur du code postal
     * @return la liste de code postal
     */
    List<CodePostal> findCodesPostauxParCode(String code);

    /**
     * Retourne une liste d'objets CodePostal pour lesquels la propriété 'code' commence par la valeur du paramètre
     * 'code'<br/>
     * ou pour lesquels la propriété 'ville' commence par la valeur du paramètre 'ville'.
     *
     * @param code valeur du code postal
     * @param ville ville
     * @return la liste de code postal correspondante
     */
    List<CodePostal> findCodesPostauxParCodeEtOuVille(String code, String ville);

    /**
     * Retourne une liste d'objets CodePostal dont la propriété ville est égale à la 'ville' passée en paramètre. <br>
     * En ignorant la casse et les caractères blancs aux extrêmités du paramètre 'ville'. <br>
     * Les objets CodePostal sont triés par nom de ville ascendant.
     * 
     * @param ville ville
     * @return la liste de code postal correspondante
     */
    List<CodePostal> findCodesPostauxParVille(String ville);

    /**
     * Retourne tous les départements. La région est renseignée
     * 
     * @return Liste des départements
     */
    List<Departement> findTousLesDepartementsAvecRegion();

    /**
     * Retourne tous les pays (INSEE) triés par ordre ascendant nom.
     * 
     * @return Liste des pays
     */
    List<PaysInsee> findTousLesPaysInsee();

    /**
     * Retourne tous les pays (ISO) triés par ordre ascendant nom.
     * 
     * @return Liste des pays
     */
    List<PaysIso> findTousLesPaysIso();

    /**
     * Retourne toutes les régions triées par ordre ascendant de nom de région. <br>
     * La propriété 'lesDepartements' est renseignée.
     * 
     * @return Liste des régions
     */
    List<Region> findToutesLesRegionsAvecDepartements();

    /**
     * Recherche un département par code département INSEE.
     * 
     * @param unCodeDepartementINSEE code du département INSEE
     * @return le departement correspondant
     */
    Departement findUnDepartementParCodeDepartementINSEE(String unCodeDepartementINSEE);

    /**
     * Recherche un département par code département INSEE. <br>
     * La propriété laRegion est renseignée ainsi que les départements qui constituent la région.
     * 
     * @param codedepartementINSEE code du département INSEE
     * @return le departement correspondant
     */
    Departement findUnDepartementParCodeDepartementINSEEAvecRegion(String codedepartementINSEE);

    /**
     * Recherche un département par code département INSEE. <br>
     * La propriété laRegion est renseignée ainsi que les départements qui constituent la région. <br>
     * La propriété lesCommunes est renseignée.
     * 
     * @param codedepartementINSEE code du département INSEE
     * @return le departement correspondant
     */
    Departement findUnDepartementParCodeDepartementINSEEAvecRegionEtCommunes(String codedepartementINSEE);

    /**
     * Retourne la commune correspondant au code commune INSEE passé en paramètre si elle existe. <br>
     * Sinon, retourne null.
     * 
     * @param codecommuneINSEE code de la commune INSEE
     * @return la commune correspondants
     */
    Commune findUneCommuneParCodeCommuneINSEE(String codecommuneINSEE);

    /**
     * Retourne la région correspondant au code région INSEE passé en paramètre s'il existe. <br>
     * Sinon retourne null.
     * 
     * @param coderegionINSEE code de la région INSEE
     * @return true si la région existe, false sinon
     */
    Region findUneRegionParCodeRegionINSEE(String coderegionINSEE);

    /**
     * Retourne l'objet PaysInsee correspondant au code pays INSEE passé en paramètre s'il existe, sinon retourne null.
     * 
     * @param codePaysInsee code pays INSEE
     * @return true si le pays existe, false sinon
     */
    PaysInsee findUnPaysInseeParCodePaysInsee(String codePaysInsee);

    /**
     * Retourne l'objet PaysIso correspondant au code pays INSEE passé en paramètre s'il existe. <br>
     * Qinon retourne null.
     * 
     * @param codeIsoAlpha2 code pays ISO
     * @return true si le pays existe, false sinon
     */
    PaysIso findUnPaysIsoParCodePaysIso(String codeIsoAlpha2);

    /**
     * Test s'il existe une commune correspondant au code commune INSEE passé en paramètre.
     * 
     * @param codeCommuneINSEE code commune INSEE
     * @return true si la commune existe, false sinon
     */
    boolean testExistenceCommune(String codeCommuneINSEE);

    /**
     * Teste s'il existe un département correspondant au code département passé en paramètre.
     * 
     * @param codeDepartement code département
     * @return true si le département existe, false sinon
     */
    boolean testExistenceDepartement(String codeDepartement);

    /**
     * Teste s'il existe une région correspondant au code région passé en paramètre.
     * 
     * @param codeRegion code région
     * @return true si la région existe, false sinon
     */
    boolean testExistenceRegion(String codeRegion);
    
    
    /**
     * pour autocompletion communes findLesCommunesParDebutLibelleCommune
     *
     * @param nomcommune
     * @return Liste des communes
     */
    public List<String> findLesCommunesParDebutLibelleCommune(String nomcommune);

    /**
     * pour autocompletion pays
     *
     * @param pays
     * @return Liste des pays
     */
    public List<String> findLesPaysParDebutLibellePays(String pays);

}
