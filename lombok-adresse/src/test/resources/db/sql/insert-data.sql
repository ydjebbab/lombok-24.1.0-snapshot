    insert into 
    ZADRESSE_ZADR (ZADR_ID, version,codePostalEtCedex,ligne2ServiceAppartEtgEsc,ligne3BatimentImmeubleResid,ligne4NumeroLibelleVoie,ligne5LieuDitMentionSpeciale,pays,ville)
    values 
    (NEXT VALUE FOR ZADR_ID_SEQUENCE, 1,'75012','ligne2','ligne3','ligne4','ligne5','France','Paris');
    
    insert into
    ZCODEPOSTAL_ZCPO (ZCPO_ID, version, code, ville)
    values
    (NEXT VALUE FOR ZCPO_ID_SEQUENCE, 1,'75012','Paris');
    
    insert into
    ZCODEPOSTAL_ZCPO (ZCPO_ID, version,code,ville)
    values
    (NEXT VALUE FOR ZCPO_ID_SEQUENCE, 2,'75012','A');
    
    insert into
    ZCODEPOSTAL_ZCPO (ZCPO_ID, version,code,ville)
    values
    (NEXT VALUE FOR ZCPO_ID_SEQUENCE, 1,'75011','Paris');
    
    insert into
    ZREGION_ZREG (ZREG_ID, coderegionINSEE, libelleregion)
    values
    (NEXT VALUE FOR ZPAY_ID_SEQUENCE, '11', 'IDF');
    
    insert into
    ZREGION_ZREG (ZREG_ID, coderegionINSEE, libelleregion)
    values
    (NEXT VALUE FOR ZPAY_ID_SEQUENCE, '12', 'Haut de France');
    
    insert into
    ZDEPARTEMENT_ZDEP (ZDEP_ID, codedepartementINSEE, libelledepartement, ZREG_ID_LESDEPARTEMENTS)
    values
    (NEXT VALUE FOR ZDEP_ID_SEQUENCE, '75', 'Paris', 1);
    
    insert into
    ZDEPARTEMENT_ZDEP (ZDEP_ID, codedepartementINSEE, libelledepartement)
    values
    (NEXT VALUE FOR ZDEP_ID_SEQUENCE, '27', 'Eure');
    
    insert into
    ZCOMMUNE_ZCOM (ZCOM_ID, codecommuneINSEE, libellecommune, ZDEP_ID_LESCOMMUNES)
    values
    (NEXT VALUE FOR ZCOM_ID_SEQUENCE, '92073', 'Nanterre', 1);
    
    insert into
    ZPAYSINSEE_ZPIN (ZPIN_ID, codePaysInsee, nom)
    values
    (NEXT VALUE FOR ZPIN_ID_SEQUENCE, '99101','FEROE (ILES)');
    
    insert into
    ZPAYSINSEE_ZPIN (ZPIN_ID, codePaysInsee, nom)
    values
    (NEXT VALUE FOR ZPIN_ID_SEQUENCE, '99109','ALLEMAGNE');
    
    insert into
    ZPAYSISO_ZPIS (ZPIS_ID,CODEISO, nom)
    values
    ('1', 'FR','FRANCE');
    insert into
    
    ZPAYSISO_ZPIS (ZPIS_ID,CODEISO, nom)
    values
    ('2', 'DEU','ALLEMAGNE');