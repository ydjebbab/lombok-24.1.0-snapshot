/*
 * Copyright (c) 2016 DGFiP - Tous droits réservés
 * 
 */
package fr.gouv.finances.lombok.adresse.bean;

import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;

import org.junit.Assert;
import org.junit.Test;

import fr.gouv.finances.lombok.test.pojo.AbstractCorePojoTest;
import fr.gouv.finances.lombok.util.exception.IngerableException;
import fr.gouv.finances.lombok.util.exception.ProgrammationException;

/**
 * Tests unitaires de l'entité Commune.
 *
 * @author Christophe Breheret-Girardin
 */
public class CommuneTest extends AbstractCorePojoTest<Commune>
{
    /**
     * Constructeur.
     */
    public CommuneTest()
    {
        super();
    }

    /**
     * Méthode permettant de tester l'égalité du POJO.
     */
    @Test
    public final void testEqualsHashCode() throws Exception
    {
        EqualsVerifier.forClass(getParameterizedType())
            .suppress(Warning.NONFINAL_FIELDS, Warning.STRICT_INHERITANCE)
            .withPrefabValues(Departement.class, getDepartement(1), getDepartement(2))
            .withOnlyTheseFields("codecommuneINSEE")
            .usingGetClass()
            .verify();
    }

    /**
     * Méthode permettant de tester les communes Nominoe.
     */
    @Test
    public final void testCommuneNominoe() throws Exception
    {
        Commune commune = new Commune();
        Assert.assertNull(commune.getCodecommuneNOMINOE());
        Assert.assertNull(commune.getCodecommuneINSEE());
        
        verifierErreurCommune(null, null, ProgrammationException.class);
        verifierErreurCommune("1", null, ProgrammationException.class);
        verifierErreurCommune(null, "1", ProgrammationException.class);
        
        commune = new Commune();
        commune.setCodecommuneNOMINOE("1", "2");
        Assert.assertEquals("21", commune.getCodecommuneINSEE());
        Assert.assertEquals("21", commune.getCodecommuneINSEE());
        
        commune = new Commune();
        commune.setCodecommuneNOMINOE("1234", "96123");
        Assert.assertEquals("961231234", commune.getCodecommuneINSEE());
        Assert.assertEquals("123", commune.getCodecommuneNOMINOE());
        
        commune = new Commune();
        commune.setCodecommuneNOMINOE("1234", "97123");
        Assert.assertEquals("9712323", commune.getCodecommuneINSEE());
        Assert.assertEquals("023", commune.getCodecommuneNOMINOE());
        
        commune = new Commune();
        commune.setCodecommuneNOMINOE("1234", "98123");
        Assert.assertEquals("9812323", commune.getCodecommuneINSEE());
        Assert.assertEquals("023", commune.getCodecommuneNOMINOE());
        
        commune = new Commune();
        commune.setCodecommuneNOMINOE("1234", "99123");
        Assert.assertEquals("9912323", commune.getCodecommuneINSEE());
        Assert.assertEquals("023", commune.getCodecommuneNOMINOE());
    }
    
    /**
     * Méthode permettant de vérifier qu'une exception est levée lors de l'utilisation
     * du mutateur du code commune nominoe
     *
     * @param codecommuneNOMINOE code commune nominoe
     * @param codedepartementINSEE code commune INSEE
     * @param exceptionAttendue exception attendue
     */
    private final void verifierErreurCommune(String codecommuneNOMINOE, String codedepartementINSEE
        , Class<? extends Exception> exceptionAttendue)
    {
        try
        {
            Commune commune = new Commune();
            commune.setCodecommuneNOMINOE(codecommuneNOMINOE, codedepartementINSEE);
            Assert.fail("Une exception " + exceptionAttendue.getSimpleName() + " aurait due être levée");
        }
        catch (Exception exception)
        {
            if (exception.getClass().equals(exceptionAttendue))
            {
                LOGGER.debug("Une exception attendue a bien été levée" + exception);
            }
            else
            {
                throw new IngerableException("Une exception " + exception.getClass() + " a été levée "
                    + "à la place de "+ exceptionAttendue);
            }
        } 
    }
    
    /**
     * Méthode permettant de fournir un département
     *
     * @param indice indice du département à créer
     * @return un département
     */
    private Departement getDepartement(int indice)
    {
        Departement departement = new Departement();
        departement.setId(indice);
        departement.setLibelledepartement("lib" + indice);
        departement.setCodedepartementINSEE("dep" +  indice);
        return departement;
    }
}
