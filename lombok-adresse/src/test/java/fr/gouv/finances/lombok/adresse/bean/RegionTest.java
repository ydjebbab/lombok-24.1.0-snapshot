/*
 * Copyright (c) 2016 DGFiP - Tous droits réservés
 * 
 */
package fr.gouv.finances.lombok.adresse.bean;

import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;

import org.junit.Test;

import fr.gouv.finances.lombok.test.pojo.AbstractCorePojoTest;

/**
 * Tests unitaires de l'entité Region
 *
 * @author Christophe Breheret-Girardin
 */
public class RegionTest extends AbstractCorePojoTest<Region>
{
    /**
     * Constructeur.
     */
    public RegionTest()
    {
        super();
    }

    /**
     * Méthode permettant de tester l'égalité du POJO.
     */
    @Test
    public final void testEqualsHashCode()
    {
        EqualsVerifier.forClass(getParameterizedType())
            .suppress(Warning.NONFINAL_FIELDS, Warning.STRICT_INHERITANCE)
            .withPrefabValues(Departement.class, getDepartement(1), getDepartement(2))
            .withOnlyTheseFields("coderegionINSEE")
            .usingGetClass()
            .verify();
    }

    /**
     * Méthode permettant de fournir un département
     *
     * @param indice indice du département à créer
     * @return un département
     */
    private Departement getDepartement(int indice)
    {
        Departement departement = new Departement();
        departement.setId(indice);
        departement.setLibelledepartement("lib" + indice);
        departement.setCodedepartementINSEE("dep" +  indice);
        return departement;
    }
}