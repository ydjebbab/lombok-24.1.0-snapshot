package fr.gouv.finances.lombok.adresse.batch;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.gouv.finances.lombok.adresse.bean.CodePostal;
import fr.gouv.finances.lombok.adresse.service.AdresseService;
import fr.gouv.finances.lombok.parsertexte.integration.GestionnaireImportationFichiers;
import fr.gouv.finances.lombok.parsertexte.integration.GestionnaireObjetExtrait;
import fr.gouv.finances.lombok.parsertexte.integration.IntegrationException;
import fr.gouv.finances.lombok.test.controle.VerificationExecution;
import fr.gouv.finances.lombok.util.exception.ProgrammationException;

/**
 * Tests unitaires automatisés du traitement batch de chargement des codes postaux.
 * @author Christophe Breheret-Girardin
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:/conf/traitementchargementcodespostauxContext-service.xml"
    , "classpath*:/conf/applicationContext-core-transaction-test.xml"
    , "classpath*:/conf/applicationContext-commun-transaction.xml"})
public class TraitementChargementCodesPostauxTest
{
    /** Initialisation de la journalisation. */
    protected static final Logger LOGGER = LoggerFactory.getLogger(TraitementChargementCodesPostauxTest.class);

    /** Batch de chargement des codes postaux. */
    @Autowired
    private TraitementChargementCodesPostaux traitementchargementcodespostauxperf;

    /** Bouchon du service des adresses. */
    @Mock
    private AdresseService adresseserviceso;

    /** Bouchon du gestionnaire d'importation de fichiers. */
    @Mock
    private GestionnaireImportationFichiers gestionnaireImportationFichiers;

    /**
     * Initialisation des tests.
     */
    @Before
    public void setupMock()
    {
        MockitoAnnotations.initMocks(this);
        traitementchargementcodespostauxperf.setAdresseserviceso(adresseserviceso);
        traitementchargementcodespostauxperf
            .setGestionnaireImportationFichiers(gestionnaireImportationFichiers);
    }

    /**
     * Tests unitaires de la méthode
     * {@link fr.gouv.finances.lombok.adresse.batch#extractionObjet(java.util.Map, java.lang.String
     * , int, boolean, java.lang.String)}.
     */
    @Test
    public void testExtractionObjet()
    {
        Map<Object, Object> map = new HashMap<>();

        // Test sur Map vide
        VerificationExecution.verifierException(ProgrammationException.class, () -> extractionObjet(map));

        // Test sur code postal manquant
        map.put("a", "a");
        VerificationExecution.verifierException(ProgrammationException.class, () -> extractionObjet(map));

        // Test sur code postal non initialisé
        map.put("CodePostal", null);
        VerificationExecution.verifierException(ProgrammationException.class, () -> extractionObjet(map));

        // Test sur code postal qui n'est pas une instance de CodePostal
        map.put("CodePostal", "b");
        VerificationExecution.verifierException(ProgrammationException.class, () -> extractionObjet(map));

        // Test sur un code postal adéquat
        CodePostal codePostal = new CodePostal();
        codePostal.setCode("9999");
        codePostal.setVille("ville");
        map.put("CodePostal", codePostal);
        // Capture de l'argument CodePostal
        ArgumentCaptor<CodePostal> captureCodePostal = ArgumentCaptor.forClass(CodePostal.class);
        // Exécution de la méthode à tester
        int codeRetour = extractionObjet(map);
        // Vérification du résultat de la méthode
        Assert.assertEquals("Code retour de la méthode", 0, codeRetour);
        // Vérification que celle-ci fait bien appel à la méthode adresseserviceso.ajouterUnCodePostal
        Mockito.verify(adresseserviceso, Mockito.times(1)).ajouterUnCodePostal(captureCodePostal.capture());
        // Vérification de la valeur du paramètre lors de l'appel à la méthode vérifiée ci-dessuss
        assertEquals(captureCodePostal.getValue(), codePostal);
    }
    
    private final int extractionObjet(Map<Object, Object> map)
    {
        return traitementchargementcodespostauxperf.extractionObjet(map, "", 0, false, "");
    }
    
    /**
     * Tests unitaires de la méthode
     * {@link fr.gouv.finances.lombok.adresse.batch#traiterBatch()}.
     * @throws IntegrationException 
     */
    @Test
    public void testTraiterBatch() throws IntegrationException
    {
        // Test sans paramétrage de fichier
        traitementchargementcodespostauxperf.setFichierCodesPostaux("");
        System.setProperty("traitementchargementcodespostaux.fichierCodesPostaux", "");
        VerificationExecution.verifierException(ProgrammationException.class
            , () -> traitementchargementcodespostauxperf.traiterBatch());

        // Test avec un fichier inexistant
        traitementchargementcodespostauxperf.setFichierCodesPostaux("no_one.txt");
        VerificationExecution.verifierException(ProgrammationException.class
            , () -> traitementchargementcodespostauxperf.traiterBatch());

        // Test fonctionnel
        URL url = this.getClass().getResource("/jdd/psa_cficpt.txt");
        url = this.getClass().getResource("/jdd/psa_cficpt.txt");
        traitementchargementcodespostauxperf.setFichierCodesPostaux("file:" + url.getFile());
        // Exécution de la méthode
        traitementchargementcodespostauxperf.traiterBatch();
        // Vérification que celle-ci fait bien appel à la méthode adresseserviceso.supprimerTousLesCodesPostaux
        Mockito.verify(adresseserviceso, Mockito.times(1)).supprimerTousLesCodesPostaux();

        // Test avec un fichier en erreur
        Mockito.doThrow(new IntegrationException())
            .when(gestionnaireImportationFichiers).lectureEtDecodageFichier(
                Mockito.any(File.class), Mockito.any(GestionnaireObjetExtrait.class));
        VerificationExecution.verifierException(ProgrammationException.class
            , () -> traitementchargementcodespostauxperf.traiterBatch());

    }

}