/*
 * Copyright (c) 2016 DGFiP - Tous droits réservés
 * 
 */
package fr.gouv.finances.lombok.adresse.bean;

import static org.junit.Assert.assertEquals;

import org.junit.Assert;
import org.junit.Test;

import fr.gouv.finances.lombok.test.controle.VerificationExecution;
import fr.gouv.finances.lombok.test.pojo.AbstractCorePojoTest;
import fr.gouv.finances.lombok.util.exception.ProgrammationException;
import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;

/**
 * Tests unitaires de l'entité Departement.
 *
 * @author Christophe Breheret-Girardin
 */
public class DepartementTest extends AbstractCorePojoTest<Departement>
{
    /**
     * Constructeur.
     */
    public DepartementTest()
    {
        super();
    }

    /**
     * Méthode permettant de tester l'égalité du POJO.
     */
    @Test
    public final void testEqualsHashCode()
    {
        EqualsVerifier.forClass(getParameterizedType())
            .suppress(Warning.NONFINAL_FIELDS, Warning.STRICT_INHERITANCE)
            .withPrefabValues(Region.class, getRegion(1), getRegion(2))
            .withPrefabValues(Commune.class, getCommune(1), getCommune(2))
            .withOnlyTheseFields("codedepartementINSEE")
            .usingGetClass()
            .verify();
    }
    
    /**
     * Méthode permettant de tester les communes Nominoe.
     */
    @Test
    public final void testCommuneNominoe() throws Exception
    {
        Departement departement = new Departement();
        Assert.assertNull(departement.getCodedepartementNOMINOE());
        Assert.assertNull(departement.getCodedepartementINSEE());

        VerificationExecution.verifierException(ProgrammationException.class, () -> 
        {
            final Departement dep = new Departement();
            dep.setCodedepartementNOMINOE(null);    
        });

        departement = new Departement();
        departement.setCodedepartementNOMINOE("12345");
        Assert.assertEquals("12345", departement.getCodedepartementINSEE());
        Assert.assertEquals("12345", departement.getCodedepartementNOMINOE());
        
        departement = new Departement();
        departement.setCodedepartementNOMINOE("012345");
        Assert.assertEquals("12", departement.getCodedepartementINSEE());
        Assert.assertEquals("012", departement.getCodedepartementNOMINOE());
        
        departement = new Departement();
        departement.setCodedepartementNOMINOE("12");
        Assert.assertEquals("12", departement.getCodedepartementINSEE());
        Assert.assertEquals("012", departement.getCodedepartementNOMINOE());
        
        departement = new Departement();
        departement.setCodedepartementNOMINOE("1");
        Assert.assertEquals("1", departement.getCodedepartementINSEE());
        Assert.assertEquals("1", departement.getCodedepartementNOMINOE());
    }

    /**
     * Méthode permettant de tester la méthode "getCodePlusLibelleDepartement".
     */
    @Test
    public final void getCodePlusLibelleDepartement() throws Exception
    {
        Departement departement = new Departement();
        assertEquals("null null", departement.getCodePlusLibelleDepartement());

        departement.setCodedepartementINSEE("A");
        departement.setLibelledepartement("B");
        assertEquals("A B", departement.getCodePlusLibelleDepartement());
    }

    /**
     * Méthode permettant de fournir une région
     *
     * @param indice indice de la région à créer
     * @return une région
     */
    private Region getRegion(int indice)
    {
        Region region = new Region();
        region.setId(indice);
        region.setCoderegionINSEE("code" + indice);
        region.setLibelleregion("lib" + indice);
        return region;
    }

    /**
     * Méthode permettant de fournir une commune
     *
     * @param indice indice de la commune à créer
     * @return une commune
     */
    private Commune getCommune(int indice)
    {
        Commune commune = new Commune();
        commune.setId(indice);
        commune.setLibellecommune("lib" + indice);
        commune.setCodecommuneINSEE("code" + indice);
        return commune;
    }
}
