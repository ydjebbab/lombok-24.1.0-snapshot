/*
 * Copyright (c) 2016 DGFiP - Tous droits réservés
 * 
 */
package fr.gouv.finances.lombok.adresse.bean;

import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;

import org.junit.Test;

import fr.gouv.finances.lombok.test.pojo.AbstractCorePojoTest;

/**
 * Tests unitaires de l'entité PaysIso.
 *
 * @author Christophe Breheret-Girardin
 */
public class PaysIsoTest extends AbstractCorePojoTest<PaysIso>
{
    /**
     * Constructeur.
     */
    public PaysIsoTest()
    {
        super();
    }

    /**
     * Méthode permettant de tester l'égalité du POJO.
     */
    @Test
    public final void testEqualsHashCode()
    {
        EqualsVerifier.forClass(getParameterizedType())
            .suppress(Warning.NONFINAL_FIELDS, Warning.STRICT_INHERITANCE)
            .withOnlyTheseFields("codeIsoAlpha2")
            .usingGetClass()
            .verify();
    }

}
