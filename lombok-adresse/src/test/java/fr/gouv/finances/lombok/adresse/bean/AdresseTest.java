/*
 * Copyright (c) 2016 DGFiP - Tous droits réservés
 * 
 */
package fr.gouv.finances.lombok.adresse.bean;

import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;

import org.junit.Test;

import fr.gouv.finances.lombok.test.pojo.AbstractCorePojoTest;

/**
 * Tests unitaires de l'entité Adresse.
 *
 * @author Christophe Breheret-Girardin
 */
public class AdresseTest extends AbstractCorePojoTest<Adresse>
{
    /**
     * Constructeur.
     */
    public AdresseTest()
    {
        super();
    }

    /**
     * Méthode permettant de tester l'égalité du POJO.
     */
    @Test
    public final void testEqualsHashCode() throws Exception
    {
        EqualsVerifier.forClass(getParameterizedType())
            .suppress(Warning.NONFINAL_FIELDS, Warning.STRICT_INHERITANCE)
            .withOnlyTheseFields("codePostalEtCedex", "ligne2ServiceAppartEtgEsc"
                , "ligne3BatimentImmeubleResid", "ligne4NumeroLibelleVoie"
                , "ligne5LieuDitMentionSpeciale", "pays", "ville")
            .usingGetClass()
            .verify();
    }
}
