package fr.gouv.finances.lombok.adresse.dao;

import fr.gouv.finances.lombok.test.pojo.AbstractCorePojoTest;

/**
 * Tests unitaires de l'énumération ProprieteAdresse.
 * @see fr.gouv.finances.lombok.adresse.dao.ProprieteAdresse
 *
 * @author Christophe Breheret-Girardin
 */
public class ProprieteAdresseTest extends AbstractCorePojoTest<ProprieteAdresse>
{
    /**
     * Constructeur.
     */
    public ProprieteAdresseTest()
    {
        super();
    }
}