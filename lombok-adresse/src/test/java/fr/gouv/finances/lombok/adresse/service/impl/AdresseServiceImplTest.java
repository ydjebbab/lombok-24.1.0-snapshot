/*
 * Copyright (c) 2016 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.adresse.service.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.gouv.finances.lombok.adresse.bean.CodePostal;
import fr.gouv.finances.lombok.adresse.bean.Commune;
import fr.gouv.finances.lombok.adresse.bean.Departement;
import fr.gouv.finances.lombok.adresse.bean.PaysInsee;
import fr.gouv.finances.lombok.adresse.bean.PaysIso;
import fr.gouv.finances.lombok.adresse.bean.Region;
import fr.gouv.finances.lombok.adresse.dao.AdresseDao;
import fr.gouv.finances.lombok.adresse.service.AdresseService;
import fr.gouv.finances.lombok.util.exception.RegleGestionException;

/**
 * Tests unitaires automatisés des services métier.
 * @author Christophe Breheret-Girardin
 */
@ContextConfiguration({"classpath:/conf/applicationContext-adresse-service-test.xml"})
@ActiveProfiles(profiles = {"adresse"})
@RunWith(SpringJUnit4ClassRunner.class)
public class AdresseServiceImplTest
{
    /** Initialisation de la journalisation. */
    protected static final Logger LOGGER = LoggerFactory.getLogger(AdresseServiceImplTest.class);
    
    /** Service métier des adresses à tester */
    @Autowired
    private AdresseService adresseService;

    /** Bouchon du DAO des adresses */
    @Mock
    private AdresseDao adresseDao;

    /** Règle sur une exception */
    @Rule
    public ExpectedException thrown = ExpectedException.none();
    
    /** Initialisation d'une constante de la valeur de l'arrondissement de Paris 1 */
    private static final String VALEUR_CODE_POSTAL_PARIS1 = "75001";
    /** Initialisation d'une constante du libellé de la ville de Paris */
    private static final String VILLE_PARIS = "PARIS";

    /** Initialisation d'une constante du code postal de Paris dans le 1er arrondissement */
    private static final CodePostal PARIS1_1
        = getCodePostal(1, VALEUR_CODE_POSTAL_PARIS1, VILLE_PARIS);
    /** Initialisation d'une autre constante du code postal de Paris dans le 1er arrondissement */
    private static final CodePostal PARIS1_2
        = getCodePostal(2, VALEUR_CODE_POSTAL_PARIS1, VILLE_PARIS);
    /** Initialisation d'une constante du code postal de Paris dans le 2er arrondissement */
    private static final CodePostal PARIS2 = getCodePostal(3, "75002", VILLE_PARIS);

    /** Initialisation d'une constante du pays ISO représentant la France */
    private static final PaysIso FRANCE_ISO = getPaysIso("1", "France");
    /** Initialisation d'une constante du pays ISO représentant l'Allemagne */
    private static final PaysIso ALLEMAGNE_ISO = getPaysIso("2", "Allemagne");

    /** Initialisation d'une constante du pays INSEE représentant la France */
    private static final PaysInsee FRANCE_INSEE = getPaysInsee(1, "France");
    /** Initialisation d'une constante du pays INSEE représentant l'Allemagne */
    private static final PaysInsee ALLEMAGNE_INSEE = getPaysInsee(2, "Allemagne");

    /** Initialisation d'une constante de la région Ile-De-France */
    private static final Region IDF = getRegion("1", "Ile-De-France");
    /** Initialisation d'une constante de la région Normandie */
    private static final Region NORMANDIE = getRegion("2", "Normandie");

    /** Initialisation d'une constante de la commune de Noisy-Le-Grand */
    private static final Commune NOISY = getCommune(1, "Noisy-Le-Grand");

    /** Initialisation d'une constante du département Saint-Denis*/
    private static final Departement SAINT_DENIS = getDepartement(1, "Saint-Denis");
    /** Initialisation d'une constante du département Paris*/
    private static final Departement PARIS = getDepartement(2, "Paris");

    /**
     * Constructeur.
     */
    public AdresseServiceImplTest()
    {
        super();
    }

    /**
     * Initialisation des tests.
     */
    @Before
    public void setupMock()
    {
        MockitoAnnotations.initMocks(this);
        adresseService.setAdressedao(adresseDao);
    }

    /**
     * Test de l'injection du service.
     */
    @Test
    public final void testAdresseServiceInjection()
    {
        assertNotNull("Service métier non initialisé", adresseService);
    }

    /**
     * Tests unitaires de la méthode
     * {@link fr.gouv.finances.lombok.adresse.service.impl#ajouterUnCodePostal(fr.gouv.finances.lombok.adresse.bean.CodePostal)}.
     */
    @Test
    public final void testAjouterUnCodePostal()
    {
        /** Inspecteur de l'argument passé à une méthode du DAO */
        ArgumentCaptor<CodePostal> captureCodePostal = ArgumentCaptor.forClass(CodePostal.class);
        // Exécution du service
        adresseService.ajouterUnCodePostal(PARIS1_1);
        // Vérification que la méthode adresseDao.saveObject est appelée une fois
        Mockito.verify(adresseDao, Mockito.times(1)).saveObject(captureCodePostal.capture());
        // Vérification du paramètre de la méthode ci-dessous
        assertEquals(PARIS1_1, captureCodePostal.getValue());
    }

    /**
     * Tests unitaires de la méthode
     * {@link fr.gouv.finances.lombok.adresse.service.impl#ajouterUnDepartement(fr.gouv.finances.lombok.adresse.bean.Departement)}.
     */
    @Test
    public final void testAjouterUnDepartement()
    {
        /** Inspecteur de l'argument passé à une méthode du DAO */
        ArgumentCaptor<Departement> captureDepartement = ArgumentCaptor.forClass(Departement.class);
        // Exécution du service
        adresseService.ajouterUnDepartement(SAINT_DENIS);
        // Vérification que la méthode adresseDao.saveObject est appelée une fois
        Mockito.verify(adresseDao, Mockito.times(1)).saveObject(captureDepartement.capture());
        // Vérification du paramètre de la méthode ci-dessous
        assertEquals(SAINT_DENIS, captureDepartement.getValue());
    }

    /**
     * Tests unitaires de la méthode
     * {@link fr.gouv.finances.lombok.adresse.service.impl#ajouterUneCommune(fr.gouv.finances.lombok.adresse.bean.Commune)}.
     */
    @Test
    public final void testAjouterUneCommune()
    {
        /** Inspecteur de l'argument passé à une méthode du DAO */
        ArgumentCaptor<Commune> captureCommune = ArgumentCaptor.forClass(Commune.class);
        // Exécution du service
        adresseService.ajouterUneCommune(NOISY);
        // Vérification que la méthode adresseDao.saveObject est appelée une fois
        Mockito.verify(adresseDao, Mockito.times(1)).saveObject(captureCommune.capture());
        // Vérification du paramètre de la méthode ci-dessous
        assertEquals(NOISY, captureCommune.getValue());
    }

    /**
     * Tests unitaires de la méthode
     * {@link fr.gouv.finances.lombok.adresse.service.impl#ajouterUneRegion(fr.gouv.finances.lombok.adresse.bean.Region)}.
     */
    @Test
    public final void testAjouterUneRegion()
    {
        /** Inspecteur de l'argument passé à une méthode du DAO */
        ArgumentCaptor<Region> captureRegion = ArgumentCaptor.forClass(Region.class);
        // Exécution du service
        adresseService.ajouterUneRegion(IDF);
        // Vérification que la méthode adresseDao.saveObject est appelée une fois
        Mockito.verify(adresseDao, Mockito.times(1)).saveObject(captureRegion.capture());
        // Vérification du paramètre de la méthode ci-dessous
        assertEquals(IDF, captureRegion.getValue());
    }

    /**
     * Tests unitaires de la méthode
     * {@link fr.gouv.finances.lombok.adresse.service.impl#ajouterUnPaysInsee(fr.gouv.finances.lombok.adresse.bean.PaysInsee)}.
     */
    @Test
    public final void testAjouterUnPaysInsee()
    {
        /** Inspecteur de l'argument passé à une méthode du service métier. */
        ArgumentCaptor<PaysInsee> capturePaysInsee = ArgumentCaptor.forClass(PaysInsee.class);
        // Exécution du service
        adresseService.ajouterUnPaysInsee(FRANCE_INSEE);
        // Vérification que la méthode adresseDao.saveObject est appelée une fois
        Mockito.verify(adresseDao, Mockito.times(1)).saveObject(capturePaysInsee.capture());
        // Vérification du paramètre de la méthode ci-dessous
        assertEquals(FRANCE_INSEE, capturePaysInsee.getValue());
    }

    /**
     * Tests unitaires de la méthode
     * {@link fr.gouv.finances.lombok.adresse.service.impl#ajouterUnPaysIso(fr.gouv.finances.lombok.adresse.bean.PaysIso)}.
     */
    @Test
    public final void testAjouterUnPaysIso()
    {
        /** Inspecteur de l'argument passé à une méthode du service métier. */
        ArgumentCaptor<PaysIso> capturePaysIso = ArgumentCaptor.forClass(PaysIso.class);
        // Exécution du service
        adresseService.ajouterUnPaysIso(FRANCE_ISO);
        // Vérification que la méthode adresseDao.saveObject est appelée une fois
        Mockito.verify(adresseDao, Mockito.times(1)).saveObject(capturePaysIso.capture());
        // Vérification du paramètre de la méthode ci-dessous
        assertEquals(FRANCE_ISO, capturePaysIso.getValue());
    }

    /**
     * Tests unitaires de la méthode
     * {@link fr.gouv.finances.lombok.adresse.service#chargerMapPaysInsee(fr.gouv.finances.lombok.adresse.bean.PaysInsee)}.
     */
    @Test
    public final void testChargerMapPaysInsee()
    {
        // Vérification de l'absence des données avant le test
        assertTrue("Map de pays INSEE non vide", adresseService.chargerMapPaysInsee().isEmpty());

        // Paramétrage du comportement du bouchon
        List<PaysInsee> liste = new ArrayList<>();
        liste.add(FRANCE_INSEE);
        liste.add(ALLEMAGNE_INSEE);
        when(adresseDao.loadAllObjects(PaysInsee.class)).thenReturn(liste);

        // Vérification du service par rapport au comportement défini
        Map<String, String> mapPays = adresseService.chargerMapPaysInsee();
        assertFalse("Map de Pays INSEE vide", mapPays.isEmpty());
        Assert.assertEquals("Nombre de pays INSEE", 2, mapPays.size());
        Assert.assertEquals("Nom du pays INSEE", FRANCE_INSEE.getNom(), mapPays.get(FRANCE_INSEE.getNom()));
        Assert.assertEquals("Nom du pays INSEE",ALLEMAGNE_INSEE.getNom(), mapPays.get(ALLEMAGNE_INSEE.getNom()));
    }

    /**
     * Tests unitaires de la méthode
     * {@link fr.gouv.finances.lombok.adresse.service.impl#chargerMapPaysIso(fr.gouv.finances.lombok.adresse.bean.PaysIso)}.
     */
    @Test
    public final void testChargerMapPaysIso()
    {
        // Vérification de l'absence des données avant le test
        assertTrue("Map de pays ISO non vide", adresseService.chargerMapPaysInsee().isEmpty());

        // Paramétrage du comportement du bouchon
        List<PaysIso> liste = new ArrayList<>();
        liste.add(FRANCE_ISO);
        liste.add(ALLEMAGNE_ISO);
        when(adresseDao.loadAllObjects(PaysIso.class)).thenReturn(liste);

        // Vérification du service par rapport au comportement défini
        Map<String, String> mapPays = adresseService.chargerMapPaysIso();
        assertFalse("Map de pays ISO vide", mapPays.isEmpty());
        Assert.assertEquals("Nombre de pays ISO", 2, mapPays.size());
        Assert.assertEquals("Nom du pays ISO", FRANCE_ISO.getNom(), mapPays.get(FRANCE_ISO.getNom()));
        Assert.assertEquals("Nom du pays ISO", ALLEMAGNE_ISO.getNom(), mapPays.get(ALLEMAGNE_ISO.getNom()));
    }

    /**
     * Tests unitaires de la méthode
     * {@link fr.gouv.finances.lombok.adresse.service.impl#modifierUneCommune(fr.gouv.finances.lombok.adresse.bean.Commune)}.
     */
    @Test
    public final void testModifierUneCommune()
    {
        /** Inspecteur de l'argument passé à une méthode du DAO */
        ArgumentCaptor<Commune> captureCommune = ArgumentCaptor.forClass(Commune.class);
        // Exécution du service
        adresseService.modifierUneCommune(NOISY);
        // Vérification que la méthode adresseDao.saveObject est appelée une fois
        Mockito.verify(adresseDao, Mockito.times(1)).saveObject(captureCommune.capture());
        // Vérification du paramètre de la méthode ci-dessous
        assertEquals(NOISY, captureCommune.getValue());
    }

    /**
     * Tests unitaires de la méthode
     * {@link fr.gouv.finances.lombok.adresse.service.impl#modifierUnPaysInsee(fr.gouv.finances.lombok.adresse.bean.PaysInsee)}.
     */
    @Test
    public final void testModifierUnPaysInsee()
    {
        /** Inspecteur de l'argument passé à une méthode du service métier. */
        ArgumentCaptor<PaysInsee> capturePaysInsee = ArgumentCaptor.forClass(PaysInsee.class);
        // Exécution du service
        adresseService.modifierUnPaysInsee(FRANCE_INSEE);
        // Vérification que la méthode adresseDao.saveObject est appelée une fois
        Mockito.verify(adresseDao, Mockito.times(1)).saveObject(capturePaysInsee.capture());
        // Vérification du paramètre de la méthode ci-dessous
        assertEquals(FRANCE_INSEE, capturePaysInsee.getValue());
    }

    /**
     * Tests unitaires de la méthode
     * {@link fr.gouv.finances.lombok.adresse.service.impl#modifierUnPaysIso(fr.gouv.finances.lombok.adresse.bean.PaysIso)}.
     */
    @Test
    public final void testModifierUnPaysIso()
    {
        /** Inspecteur de l'argument passé à une méthode du service métier. */
        ArgumentCaptor<PaysIso> capturePaysIso = ArgumentCaptor.forClass(PaysIso.class);
        // Exécution du service
        adresseService.modifierUnPaysIso(FRANCE_ISO);
        // Vérification que la méthode adresseDao.saveObject est appelée une fois
        Mockito.verify(adresseDao, Mockito.times(1)).saveObject(capturePaysIso.capture());
        // Vérification du paramètre de la méthode ci-dessous
        assertEquals(FRANCE_ISO, capturePaysIso.getValue());
    }

    /**
     * Tests unitaires de la méthode
     * {@link fr.gouv.finances.lombok.adresse.service.impl#rechercherCodePostalParCodeVille(java.lang.String, java.lang.String)}.
     */
    @Test
    public final void testRechercherCodePostalParCodeVille()
    {
        // Vérification de l'absence des données avant le test
        assertTrue("Liste de code postal non vide", adresseService.rechercherCodePostalParCodeVille(
                PARIS1_1.getCode(), PARIS1_1.getVille()).isEmpty());

        // Paramétrage du comportement du bouchon
        List<CodePostal> liste1 = new ArrayList<CodePostal>();
        List<CodePostal> liste2 = new ArrayList<CodePostal>();
        liste1.add(PARIS1_1);
        liste1.add(PARIS1_2);
        liste2.add(PARIS1_1);
        when(adresseDao.findCodesPostauxParCodeEtOuVille(PARIS1_1.getCode(), PARIS1_1.getVille()))
            .thenReturn(liste2);
        when(adresseDao.findCodesPostauxParCodeEtOuVille(PARIS1_1.getCode(), "")).thenReturn(liste1);

        // Vérification du service par rapport au comportement défini
        Assert.assertEquals(new ArrayList<CodePostal>()
            , adresseService.rechercherCodePostalParCodeVille("", ""));
        List<CodePostal> codesPostaux = adresseService.rechercherCodePostalParCodeVille(PARIS1_1.getCode(), "");
        assertNotNull("Code postal non initialisé", codesPostaux);
        Assert.assertEquals("Nombre de code postal", 2, codesPostaux.size());
        Assert.assertSame("Code postal non similaire à celui attendu" ,PARIS1_1, codesPostaux.get(0));
        Assert.assertSame("Code postal non similaire à celui attendu" ,PARIS1_2, codesPostaux.get(1));
        assertTrue("Liste de code postal non vide"
            , adresseService.rechercherCodePostalParCodeVille("", PARIS1_1.getVille()).isEmpty());
        assertTrue("Liste de code postal non vide"
            , adresseService.rechercherCodePostalParCodeVille(PARIS2.getCode(), PARIS2.getVille()).isEmpty());
        codesPostaux = adresseService.rechercherCodePostalParCodeVille(PARIS1_1.getCode(), PARIS1_1.getVille());
        assertNotNull("Code postal non initialisé", codesPostaux);
        Assert.assertEquals("Nombre de code postal", 1, codesPostaux.size());
        Assert.assertSame("Code postal non similaire à celui attendu" ,PARIS1_1, codesPostaux.get(0));
    }

    /**
     * Tests unitaires de la méthode
     * {@link fr.gouv.finances.lombok.adresse.service.impl#rechercherTousLesDepartementsAvecRegion()}.
     */
    @Test
    public final void testRechercherTousLesDepartementsAvecRegion()
    {
        // Vérification de l'absence des données avant le test
        assertTrue("Liste de département non vide"
            , adresseService.rechercherTousLesDepartementsAvecRegion().isEmpty());

        // Paramétrage du comportement du bouchon
        List<Departement> departements = new ArrayList<>();
        departements.add(SAINT_DENIS);
        departements.add(PARIS);
        Mockito.when(adresseDao.findTousLesDepartementsAvecRegion()).thenReturn(departements);

        // Vérification du service par rapport au comportement défini
        List<Departement> departementsTrouves = adresseService.rechercherTousLesDepartementsAvecRegion();
        assertFalse("Liste de département vide", departementsTrouves.isEmpty());
        Assert.assertEquals("Nombre de département", 2, departementsTrouves.size());
        Assert.assertSame("Département non similaire à celui attendu" ,SAINT_DENIS, departementsTrouves.get(0));
        Assert.assertSame("Département non similaire à celui attendu" ,PARIS, departementsTrouves.get(1));
    }

    /**
     * Tests unitaires de la méthode
     * {@link fr.gouv.finances.lombok.adresse.service.impl#rechercherTousLesPaysInsee()}.
     */
    @Test
    public final void testRechercherTousLesPaysInsee()
    {
        // Vérification de l'absence des données avant le test
        assertTrue("Liste de pays INSEE non vide", adresseService.rechercherTousLesPaysInsee().isEmpty());

        // Paramétrage du comportement du bouchon
        List<PaysInsee> pays = new ArrayList<>();
        pays.add(FRANCE_INSEE);
        pays.add(ALLEMAGNE_INSEE);
        Mockito.when(adresseDao.findTousLesPaysInsee()).thenReturn(pays);

        // Vérification du service par rapport au comportement défini
        List<PaysInsee> paysTrouves = adresseService.rechercherTousLesPaysInsee();
        assertFalse("Liste de pays INSEE vide", paysTrouves.isEmpty());
        Assert.assertEquals("Nombre de pays INSEE", 2, paysTrouves.size());
        Assert.assertSame("Pays INSEE non similaire à celui attendu" ,FRANCE_INSEE, paysTrouves.get(0));
        Assert.assertSame("Pays INSEE non similaire à celui attendu" ,ALLEMAGNE_INSEE, paysTrouves.get(1));
    }

    /**
     * Tests unitaires de la méthode
     * {@link fr.gouv.finances.lombok.adresse.service.impl#rechercherToutesLesRegionsAvecDepartement()}.
     */
    @Test
    public final void testRechercherToutesLesRegionsAvecDepartement()
    {
        // Vérification de l'absence des données avant le test
        assertTrue("Liste de région non vide", adresseService.rechercherToutesLesRegionsAvecDepartement().isEmpty());

        // Paramétrage du comportement du bouchon
        List<Region> regions = new ArrayList<>();
        regions.add(IDF);
        regions.add(NORMANDIE);
        Mockito.when(adresseDao.findToutesLesRegionsAvecDepartements()).thenReturn(regions);

        // Vérification du service par rapport au comportement défini
        List<Region> regionsTrouvees = adresseService.rechercherToutesLesRegionsAvecDepartement();
        assertFalse("Liste de région vide", regionsTrouvees.isEmpty());
        Assert.assertEquals("Nombre de région", 2, regionsTrouvees.size());
        Assert.assertSame("Région non similaire à celle attendue" ,IDF, regionsTrouvees.get(0));
        Assert.assertSame("Région non similaire à celle attendue" ,NORMANDIE, regionsTrouvees.get(1));
    }

    /**
     * Tests unitaires de la méthode
     * {@link fr.gouv.finances.lombok.adresse.service.impl#rechercherUnDepartementParCodeDepartementINSEE(java.lang.String)}.
     */
    @Test
    public final void testRechercherUnDepartementParCodeDepartementINSEE()
    {
        // Vérification de l'absence des données avant le test
        assertNull("Département initialisé", adresseService.rechercherUnDepartementParCodeDepartementINSEE(PARIS.getCodedepartementINSEE()));
        
        // Paramétrage du comportement du bouchon
        when(adresseDao.findUnDepartementParCodeDepartementINSEE(PARIS.getCodedepartementINSEE())).thenReturn(PARIS);

        // Vérification du service par rapport au comportement défini
        Departement departement
            = adresseService.rechercherUnDepartementParCodeDepartementINSEE(PARIS.getCodedepartementINSEE());
        assertNotNull("Département non initialisé", departement);
        Assert.assertSame("Département non similaire à celui attendu" ,PARIS, departement);
    }

    /**
     * Tests unitaires de la méthode
     * {@link fr.gouv.finances.lombok.adresse.service.impl#rechercherUneCommuneParCodeCommuneINSEE(java.lang.String)}.
     */
    @Test
    public final void testRechercherUneCommuneParCodeCommuneINSEE()
    {
     // Vérification de l'absence des données avant le test
        assertNull("Commune initialisée"
            , adresseService.rechercherUneCommuneParCodeCommuneINSEE(NOISY.getCodecommuneINSEE()));
        
        // Paramétrage du comportement du bouchon
        when(adresseDao.findUneCommuneParCodeCommuneINSEE(NOISY.getCodecommuneINSEE())).thenReturn(NOISY);

        // Vérification du service par rapport au comportement défini
        Commune commune = adresseService.rechercherUneCommuneParCodeCommuneINSEE(NOISY.getCodecommuneINSEE());
        assertNotNull("Commune non initialisée", commune);
        Assert.assertSame("Commune non similaire à celle attendue" ,NOISY, commune);
    }

    /**
     * Tests unitaires de la méthode
     * {@link fr.gouv.finances.lombok.adresse.service.impl#rechercherUneRegionParCodeRegionINSEE(java.lang.String)}.
     */
    @Test
    public final void testRechercherUneRegionParCodeRegionINSEE()
    {
        // Vérification de l'absence des données avant le test
        assertNull("Région initialisée", adresseService.rechercherUneRegionParCodeRegionINSEE(IDF.getCoderegionINSEE()));
        
        // Paramétrage du comportement du bouchon
        when(adresseDao.findUneRegionParCodeRegionINSEE(IDF.getCoderegionINSEE())).thenReturn(IDF);

        // Vérification du service par rapport au comportement défini
        Region region = adresseService.rechercherUneRegionParCodeRegionINSEE(IDF.getCoderegionINSEE());
        assertNotNull("Région non initialisée", region);
        Assert.assertSame("Région non similaire à celle attendue" ,IDF, region);
    }

    /**
     * Tests unitaires de la méthode
     * {@link fr.gouv.finances.lombok.adresse.service.impl#rechercherUnPaysParCodeInsee(java.lang.String)}.
     */
    @Test
    public final void testRechercherUnPaysParCodeInsee()
    {
        // Vérification de l'absence des données avant le test
        assertNull("Pays INSEE initialisé", adresseService.rechercherUnPaysParCodeInsee(FRANCE_INSEE.getCodePaysInsee()));
        
        // Paramétrage du comportement du bouchon
        when(adresseDao.findUnPaysInseeParCodePaysInsee(FRANCE_INSEE.getCodePaysInsee())).thenReturn(FRANCE_INSEE);

        // Vérification du service par rapport au comportement défini
        PaysInsee pays = adresseService.rechercherUnPaysParCodeInsee(FRANCE_INSEE.getCodePaysInsee());
        assertNotNull("Pays INSEE non initialisé", pays);
        Assert.assertSame("Pays INSEE non similaire à celui attendu" ,FRANCE_INSEE, pays);
    }

    /**
     * Tests unitaires de la méthode
     * {@link fr.gouv.finances.lombok.adresse.service.impl#rechercherUnPaysParCodeIso(java.lang.String)}.
     */
    @Test
    public final void testRechercherUnPaysParCodeIso()
    {
        // Vérification de l'absence des données avant le test
        assertNull("Pays ISO initialisé", adresseService.rechercherUnPaysParCodeIso(FRANCE_ISO.getCodeIsoAlpha2()));

        // Paramétrage du comportement du bouchon
        when(adresseDao.findUnPaysIsoParCodePaysIso(FRANCE_ISO.getCodeIsoAlpha2())).thenReturn(FRANCE_ISO);

        // Vérification du service par rapport au comportement défini
        PaysIso pays = adresseService.rechercherUnPaysParCodeIso(FRANCE_ISO.getCodeIsoAlpha2());
        assertNotNull("Pays ISO non initialisé", pays);
        Assert.assertSame("Pays ISO non similaire à celui attendu" ,FRANCE_ISO, pays);
    }

    /**
     * Tests unitaires de la méthode
     * {@link fr.gouv.finances.lombok.adresse.service.impl#rechercherVillesPourUnCodePostal(java.lang.String)}.
     */
    @Test
    public final void testRechercherVillesPourUnCodePostal()
    {
        // Vérification de l'absence des données avant le test
        verifieExceptionRechercherVillesPourUnCodePostal(PARIS1_1.getCode());

        // Paramétrage du comportement du bouchon
        List<CodePostal> liste = new ArrayList<CodePostal>();
        liste.add(PARIS1_1);
        liste.add(PARIS1_2);
        when(adresseDao.findCodesPostauxParCode(PARIS1_1.getCode())).thenReturn(liste);
 
        // Vérification du service par rapport au comportement défini
        List<CodePostal> listeResultat = adresseService.rechercherVillesPourUnCodePostal(PARIS1_1.getCode());
        assertFalse("Liste de code postal vide", listeResultat.isEmpty());
        Assert.assertEquals("Nombre de code postal", 2, listeResultat.size());
        Assert.assertSame("Code postal non similaire à celui attendu" ,PARIS1_1, listeResultat.get(0));
        Assert.assertSame("Code postal non similaire à celui attendu" ,PARIS1_2, listeResultat.get(1));
        verifieExceptionRechercherVillesPourUnCodePostal(PARIS2.getCode());
    }

    /**
     * Méthode permettant de vérifier la levée une RegleGestionException sur un code postal non trouvé 
     *
     * @param code code postal rercherché
     */
    private void verifieExceptionRechercherVillesPourUnCodePostal(String code)
    {
        try
        {
            adresseService.rechercherVillesPourUnCodePostal(code);
            Assert.fail("Une exception RegleGestionException aurait due être levée");
        }
        catch (RegleGestionException rge)
        {
            LOGGER.debug("Une RegleGestionException a bien été levée", rge);
        } 
    }

    /**
     * Tests unitaires de la méthode
     * {@link fr.gouv.finances.lombok.adresse.service.impl#verifierCodePostalCorrespondAUnLibelleCommuneRG23(java.lang.String, java.lang.String)}.
     */
    @Test
    public final void testVerifierCodePostalCorrespondAUnLibelleCommuneRG23()
    {
        // Vérification de l'absence des données avant le test
        verifierExceptionCodePostalCorrespondAUnLibelleCommuneRG23(
            PARIS1_1.getCode(), PARIS1_1.getVille());

        // Paramétrage du comportement du bouchon
        when(adresseDao.checkExistCodePostalXVille(PARIS1_1.getCode(), PARIS1_1.getVille())).thenReturn(true);

        // Vérification du service par rapport au comportement défini
        verifierExceptionCodePostalCorrespondAUnLibelleCommuneRG23("", "");
        verifierExceptionCodePostalCorrespondAUnLibelleCommuneRG23(PARIS1_1.getCode(), "");
        verifierExceptionCodePostalCorrespondAUnLibelleCommuneRG23("", PARIS1_1.getVille());
        verifierExceptionCodePostalCorrespondAUnLibelleCommuneRG23(PARIS2.getCode(), PARIS2.getVille());
        adresseService.verifierCodePostalCorrespondAUnLibelleCommuneRG23(
            PARIS1_1.getCode(), PARIS1_1.getVille());
    }

    /**
     * Méthode permettant de vérifier qu'une exception RegleGestionException est levée
     * sur un appel à la méthode verifierCodePostalCorrespondAUnLibelleCommuneRG23.
     *
     * @param codePostal valeur du code postal à rechercher
     * @param ville ville lié au code postal à rechercher
     */
    private final void verifierExceptionCodePostalCorrespondAUnLibelleCommuneRG23(
        String codePostal, String ville)
    {
        try
        {
            adresseService.verifierCodePostalCorrespondAUnLibelleCommuneRG23(codePostal, ville);
            Assert.fail("Une exception RegleGestionException aurait due être levée");
        }
        catch (RegleGestionException rge)
        {
            LOGGER.debug("Une RegleGestionException a bien été levée", rge);
        } 
    }
    
    /**
     * Tests unitaires de la méthode
     * {@link fr.gouv.finances.lombok.adresse.service.impl#verifierExistenceCodePaysISO(java.lang.String)}.
     */
    @Test
    public final void testVerifierExistenceCodePaysISO()
    {
        // Vérification de l'absence des données avant le test
        verifierExceptionfindUnPaysIsoParCodePaysIso(FRANCE_ISO.getCodeIsoAlpha2());

        // Paramétrage du comportement du bouchon
        when(adresseDao.findUnPaysIsoParCodePaysIso(FRANCE_ISO.getCodeIsoAlpha2())).thenReturn(FRANCE_ISO);

        // Vérification du service par rapport au comportement défini
        verifierExceptionfindUnPaysIsoParCodePaysIso("");
        verifierExceptionfindUnPaysIsoParCodePaysIso(ALLEMAGNE_ISO.getCodeIsoAlpha2());
        adresseService.verifierExistenceCodePaysISO(FRANCE_ISO.getCodeIsoAlpha2());
    }
    
    /**
     * Méthode permettant de vérifier qu'une exception RegleGestionException est levée
     * sur un appel à la méthode verifierExistenceCodePaysISO.
     *
     * @param codePostal code postal du pays ISO
     */
    private final void verifierExceptionfindUnPaysIsoParCodePaysIso(String codePostal)
    {
        try
        {
            adresseService.verifierExistenceCodePaysISO(codePostal);
            Assert.fail("Une exception RegleGestionException aurait due être levée");
        }
        catch (RegleGestionException rge)
        {
            LOGGER.debug("Une RegleGestionException a bien été levée", rge);
        } 
    }
    
    /**
     * Tests unitaires de la méthode
     * {@link fr.gouv.finances.lombok.adresse.service.impl#verifierExistenceCodePostalRG22(java.lang.String)}.
     */
    @Test
    public final void testVerifierExistenceCodePostalRG22()
    {
        // Vérification de l'absence des données avant le test
        verifierExceptionExistenceCodePostalRG22(PARIS1_1.getCode());
        
        // Paramétrage du comportement du bouchon
        when(adresseDao.checkExistCodePostal(PARIS1_1.getCode())).thenReturn(true);

        // Vérification du service par rapport au comportement défini
        verifierExceptionExistenceCodePostalRG22("");
        verifierExceptionExistenceCodePostalRG22(PARIS2.getCode());
        adresseService.verifierExistenceCodePostalRG22(PARIS1_1.getCode());
    }

    /**
     * Méthode permettant de vérifier qu'une exception RegleGestionException est levée
     * sur un appel à la méthode verifierExistenceCodePostalRG22.
     *
     * @param codePostal valeur du code postal
     */
    private final void verifierExceptionExistenceCodePostalRG22(String codePostal)
    {
        try
        {
            adresseService.verifierExistenceCodePostalRG22(codePostal);
            Assert.fail("Une exception RegleGestionException aurait due être levée");
        }
        catch (RegleGestionException rge)
        {
            LOGGER.debug("Une RegleGestionException a bien été levée", rge);
        } 
    }
    
    /**
     * Tests unitaires de la méthode
     * {@link fr.gouv.finances.lombok.adresse.service.impl#rechercherTousLesCodesPostaux()}.
     */
    @Test
    public final void testRechercherTousLesCodesPostaux()
    {
        // Vérification de l'absence des données avant le test
        assertTrue("Liste de code postal non vide", adresseService.rechercherTousLesCodesPostaux().isEmpty());

        // Paramétrage du comportement du bouchon
        List<CodePostal> codesPostaux = new ArrayList<>();
        codesPostaux.add(PARIS1_1);
        codesPostaux.add(PARIS2);
        Mockito.when(adresseDao.findCodePostaux()).thenReturn(codesPostaux);

        // Vérification du service par rapport au comportement défini
        List<CodePostal> codesPostauxTrouves = adresseService.rechercherTousLesCodesPostaux();
        Assert.assertFalse("Liste de code postal vide", codesPostauxTrouves.isEmpty());
        Assert.assertEquals("Nombre de code postal", 2, codesPostauxTrouves.size());
        Assert.assertSame("Code postal non similaire à celui attendu" ,PARIS1_1, codesPostauxTrouves.get(0));
        Assert.assertSame("Code postal non similaire à celui attendu" ,PARIS2, codesPostauxTrouves.get(1));
    }

    /**
     * Tests unitaires de la méthode
     * {@link fr.gouv.finances.lombok.adresse.service.impl#supprimerTousLesCodesPostaux()}.
     */
    @Test
    public final void testSupprimerTousLesCodesPostaux()
    {
        // Exécution du service
        adresseService.supprimerTousLesCodesPostaux();
        // Vérification que la méthode adresseDao.deleteTousLesCodesPostaux() est appelée une fois
        Mockito.verify(adresseDao, Mockito.times(1)).deleteTousLesCodesPostaux();
    }
    
    /**
     * Méthode permettant de générer un code postal
     *
     * @param identifiant identifiant du code postal
     * @parma code valeur du code postal
     * @return le code postal demandé
     */
    private static CodePostal getCodePostal(int identifiant, String code, String ville)
    {
        CodePostal codePostal = new CodePostal();
        codePostal.setId(identifiant);
        codePostal.setCode(code);
        codePostal.setVille(ville);
        return codePostal;
    }

    /**
     * Méthode permettant de générer un pays INSEE
     *
     * @param identifiant identifiant du pays
     * @param nom nom du pays
     * @return le pays demandé
     */
    private static PaysInsee getPaysInsee(int identifiant, String nom)
    {
        PaysInsee paysInsee = new PaysInsee();
        paysInsee.setId(identifiant);
        paysInsee.setNom(nom);
        return paysInsee;
    }
    
    /**
     * Méthode permettant de générer un pays ISO
     *
     * @param identifiant identifiant du pays
     * @param nom nom du pays
     * @return le pays demandé
     */
    private static PaysIso getPaysIso(String codePays, String nom)
    {
        PaysIso pays = new PaysIso();
        pays.setCodeIsoAlpha2(codePays);
        pays.setNom(nom);
        return pays;
    }

    /**
     * Méthode permettant de générer une région
     *
     * @param identifiant identifiant de la région
     * @parma libelle libellé de la région
     * @return la région demandée
     */
    private static Region getRegion(String code, String libelle)
    {
        Region region = new Region();
        region.setCoderegionINSEE(code);
        region.setLibelleregion(libelle);
        return region;
    }
    
    /**
     * Méthode permettant de générer une commune
     *
     * @param identifiant identifiant de la commune
     * @parma libelle libellé de la commune
     * @return la commune demandée
     */
    private static Commune getCommune(int id, String libelle)
    {
        Commune commune = new Commune();
        commune.setId(id);
        commune.setLibellecommune(libelle);
        return commune;
    }

    /**
     * Méthode permettant de générer un département
     *
     * @param identifiant identifiant du département
     * @parma libelle libellé du département
     * @return le département demandé
     */
    private static Departement getDepartement(int id, String libelle)
    {
        Departement departement = new Departement();
        departement.setId(id);
        departement.setLibelledepartement(libelle);
        return departement;
    }
}
