/*
 * Copyright (c) 2014 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.journal.jpa.dao.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import fr.gouv.finances.lombok.journal.bean.ParametreOperation;
import fr.gouv.finances.lombok.journal.dao.ParametreOperationDao;
import fr.gouv.finances.lombok.jpa.dao.BaseDaoJpaImpl;
import fr.gouv.finances.lombok.util.persistance.ModeCritereRecherche;

/**
 * Implémentation JPA du DAO permettant de gérer les données des paramètres des opérations de journalisation.
 *
 * @author Christophe Breheret-Girardin
 */
@ConditionalOnProperty(name = "lombok.orm.jpa", havingValue="true")
@Repository("paramoperdaoimpl")
@Transactional(transactionManager="transactionManager")
public class ParametreOperationDaoImpl extends BaseDaoJpaImpl implements ParametreOperationDao
{
    /** Initialisation de la journalisation. */
    protected static final Logger LOGGER = LoggerFactory.getLogger(ParametreOperationDaoImpl.class);

    /**
     * Constructeur.
     */
    public ParametreOperationDaoImpl()
    {
        super();
    }

    @Override
    public void deleteParametreOperation(ParametreOperation unParametreOperation)
    {
        LOGGER.debug(">>> Debut methode deleteUnParamOper");
        deleteObject(unParametreOperation);
    }

    @Override
    public ParametreOperation findParametreOperationParNom(String nom)
    {
        LOGGER.debug(">>> Debut methode findUnParamOperParNom");
        List<ParametreOperation> parametresOperation = new ArrayList<>();

        if (StringUtils.isEmpty(nom))
        {
            Map<String, String> criteres = new HashMap<>();
            criteres.put("nom", nom);
            parametresOperation = findAllByCriterias(ParametreOperation.class, criteres, ModeCritereRecherche.IS_NULL);
        }
        else
        {
            parametresOperation = findAllByUniqueCriteria(ParametreOperation.class, "nom", nom);
        }

        if (parametresOperation.size() >= 1)
        {
            return parametresOperation.get(0);
        }
        return null;
    }

    @Override
    public ParametreOperation findParametreOperationParNomEtValeur(String nom, String valeur)
    {
        LOGGER.debug(">>> Debut methode findUnParamOperParNomEtValeur");
        // Paramètrage de la recherche
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<ParametreOperation> query = criteriaBuilder.createQuery(ParametreOperation.class);
        Root<ParametreOperation> root = query.from(ParametreOperation.class);

        List<Predicate> predicates = new ArrayList<>();

        Path<String> pathNom = root.<String> get("nom");
        if (StringUtils.isEmpty(nom))
        {
            predicates.add(criteriaBuilder.isNull(pathNom));
        }
        else
        {
            predicates.add(criteriaBuilder.equal(pathNom, nom));
        }

        Path<String> pathValeur = root.<String> get("valeur");
        if (StringUtils.isEmpty(valeur))
        {
            predicates.add(criteriaBuilder.isNull(pathValeur));
        }
        else
        {
            predicates.add(criteriaBuilder.equal(pathValeur, valeur));
        }

        // Ajout des critères à la requête
        query.where(predicates.toArray(new Predicate[] {}));
        return find(query);
    }

    @Override
    public void saveParametreOperation(ParametreOperation unParametreOperation)
    {
        LOGGER.debug(">>> Debut methode saveUnParamOper");
        saveObject(unParametreOperation);
    }

}
