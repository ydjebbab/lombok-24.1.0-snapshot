/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 */
package fr.gouv.finances.lombok.journal.jpa.dao.impl;

import java.util.List;

import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.transaction.annotation.Transactional;

import fr.gouv.finances.lombok.journal.dao.JournalDao;
import fr.gouv.finances.lombok.jpa.dao.BaseDaoJpaImpl;
import fr.gouv.finances.lombok.util.exception.ProgrammationException;

/**
 * Implémentation JPA de la gestion des données des opérations de journalisation propres à Oracle.
 *
 * @author chouard-cp
 * @author Christophe Breheret-Girardin
 */
abstract class AbstractJournalOracleDaoImpl extends BaseDaoJpaImpl implements JournalDao
{
    /** Initialisation de la journalisation. */
    protected static final Logger LOGGER = LoggerFactory.getLogger(AbstractJournalOracleDaoImpl.class);

    @Override
    public void sauvegardeTablesOperationJournalDansTablesTemporaires()
    {
        // Sauvegarde dans une table temporaire
        sauvegarderUneTableDansTableTemporaire("ZOPERATIONJOURNAL_ZOPE");
        sauvegarderUneTableDansTableTemporaire("ZPARAMETREOPERATION_ZPAO");
        sauvegarderUneTableDansTableTemporaire("TJ_ZOPE_ZPAO_TJ");
    }

    @Override
    @Transactional
    public void ajusterSequencesOracle()
    {
        try
        {
            // Création des séquences en ajustant les valeurs initiales
            // aux tables cibles
            ajusterSequenceToTableMax("ZOPE_ID_SEQUENCE", "ZOPERATIONJOURNAL_ZOPE", "ZOPE_ID", 1);
            ajusterSequenceToTableMax("ZPAO_ID_SEQUENCE", "ZPARAMETREOPERATION_ZPAO", "ZPAO_ID", 1);
        }
        catch (ProgrammationException exception)
        {
            LOGGER.warn("ERREUR LORS DE LA MIGRATION DES DONNEES", exception);
        }

        // en cas d'exception, rollback et restauration des données
        // sauvegardées au début de la transation
    }

    @Override
    public void suppressionCopieTablesOperationJournal()
    {
        throw new UnsupportedOperationException("Pas encore implémentée");
    }

    @Override
    public boolean testerExistenceContrainteUniquePAOP()
    {
        boolean result = false;
        String sqlTestExistenceObjet =
            "SELECT COUNT(*) AS NBCONSTRAINT FROM USER_CONSTRAINTS WHERE TABLE_NAME=:TABLE_NAME AND CONSTRAINT_TYPE=:CONSTRAINT_TYPE";

        Query sqlTestExistenceObjetQuery = entityManager.createNativeQuery(sqlTestExistenceObjet);
        sqlTestExistenceObjetQuery.setParameter("TABLE_NAME", "ZPARAMETREOPERATION_ZPAO");
        sqlTestExistenceObjetQuery.setParameter("CONSTRAINT_TYPE", "U");
        List resultList = sqlTestExistenceObjetQuery.getResultList();

        long nbobject = 0;
        if (resultList.size() == 1)
        {
            nbobject = ((Long) resultList.get(0)).longValue();
        }

        LOGGER.debug("NOMBRE DE CONTRAINTES UNIQUES SUR LA TABLE ZPARAMETREOPERATION_ZPAO = {}", nbobject);

        result = (nbobject >= 1 ? true : false);

        LOGGER.debug("TEST EXISTENCE CONTRAINTE UNIQUE SUR LA TABLE ZPARAMETREOPERATION_ZPAO = {}", result);
        // Si une contrainte existe, on teste si elle est active ou non
        if (result)
        {
            String sqlStatutContrainte =
                "SELECT STATUS AS STATUS FROM USER_CONSTRAINTS WHERE TABLE_NAME=:TABLE_NAME AND CONSTRAINT_TYPE=:CONSTRAINT_TYPE";
            Query sqlTestStatutContrainteQuery = entityManager.createNativeQuery(sqlStatutContrainte);
            sqlTestStatutContrainteQuery.setParameter("TABLE_NAME", "ZPARAMETREOPERATION_ZPAO");
            sqlTestStatutContrainteQuery.setParameter("CONSTRAINT_TYPE", "U");
            List resultConstraintList = sqlTestStatutContrainteQuery.getResultList();

            String statutContrainte = "";
            if (resultConstraintList != null && resultConstraintList.size() == 1)
            {
                statutContrainte = ((String) resultConstraintList.get(0));
            }

            LOGGER.warn("STATUT DE LA CONTRAINTE : {}", statutContrainte);

            result = ("ENABLED".equals(statutContrainte) ? true : false);

        }

        // on vérifie si un index unique est présent sur les colonnes valeur et nom de
        // la table parametreoperation_paop - si c'est le cas, on le supprime

        String sqlIndexUnique =
            "SELECT INDEX_NAME AS INDEX_NAME FROM user_IND_COLUMNS WHERE table_name='ZPARAMETREOPERATION_ZPAO' AND column_name='VALEUR'";

        Query sqlIndexUniqueQuery = entityManager.createNativeQuery(sqlIndexUnique);
        List resultIndexUniqueList = sqlIndexUniqueQuery.getResultList();

        String indexUniqueName = "";
        if (resultIndexUniqueList != null && resultIndexUniqueList.size() == 1)
        {
            indexUniqueName = ((String) resultIndexUniqueList.get(0));
            LOGGER.warn("NOM DE l'INDEX UNIQUE SUR LES COLONNES NOM ET VALEUR DE LA TABLE ZPARAMETREOPERATION_ZPAO {}", indexUniqueName);

            // On supprime l'index associé à la clé unique si il y en a un
            LOGGER.warn("SUPPRESSION DE l'INDEX {}", indexUniqueName);
            String sqlDropIndex = "DROP INDEX :indexName";
            Query sqlDropIndexQuery = entityManager.createNativeQuery(sqlDropIndex);
            sqlDropIndexQuery.setParameter("indexName", indexUniqueName);
            sqlDropIndexQuery.executeUpdate();
        }

        LOGGER.debug("TEST EXISTENCE D'UNE CONTRAINTE UNIQUE SUR LA TABLE ZPARAMETREOPERATION_ZPAO  : {}", result);

        return result;
    }

    private void ajusterSequenceToTableMax(String sequenceName, String tableName, String columnIdName,
        long sequenceIncrement)
    {
        // Lecture de la valeur maximale de l'id dans la table
        long maxIdTableValue = findTableMaxIdValue(tableName, columnIdName);

        long valueForNewSequence = maxIdTableValue + 50;

        // Suppression de la séquence en cours
        supprimerSequenceOracle(sequenceName);

        // Recréation de la séquence
        creerSequenceOracle(sequenceName, valueForNewSequence, sequenceIncrement);
    }

    /**
     * Crée une séquence Oracle.
     * 
     * @param sequenceName --
     * @param sequenceFirstValue --
     * @param sequenceIncrement --
     */
    private void creerSequenceOracle(String sequenceName, long sequenceFirstValue, long sequenceIncrement)
    {
        // On contrôle que la séquence n'existe pas encore
        if (isObjetOracleExiste("SEQUENCE", sequenceName))
        {
            StringBuilder msg = new StringBuilder();
            msg.append("LA SEQUENCE ");
            msg.append(sequenceName);
            msg.append("EXISTE DEJA");

            throw new ProgrammationException(msg.toString());
        }

        StringBuilder sqlCreationSequence = new StringBuilder(43);
        sqlCreationSequence.append(" CREATE SEQUENCE ");
        sqlCreationSequence.append(sequenceName);
        sqlCreationSequence.append(" START WITH ");
        sqlCreationSequence.append(sequenceFirstValue);
        sqlCreationSequence.append(" INCREMENT BY ");
        sqlCreationSequence.append(sequenceIncrement);

        LOGGER.debug(sqlCreationSequence.toString());

        executerSqlNatif(sqlCreationSequence.toString());

        // Contrôle de l'existence de la séquence créee
        if (!isObjetOracleExiste("SEQUENCE", sequenceName))
        {
            StringBuilder msg = new StringBuilder();
            msg.append("LA CREATION DE LA SEQUENCE ");
            msg.append(sequenceName);
            msg.append(" A ECHOUEE");
            LOGGER.warn(msg.toString());
            throw new ProgrammationException(msg.toString());
        }
        else
        {
            StringBuilder msg = new StringBuilder();
            msg.append("LA SEQUENCE ");
            msg.append(sequenceName);
            msg.append(" (START VALUE = ");
            msg.append(sequenceFirstValue);
            msg.append("  INCREMENT = ");
            msg.append(sequenceIncrement);
            msg.append(" )");
            msg.append(" A ETE CREEE");
            LOGGER.warn(msg.toString());
        }
    }

    /**
     * Supprime une séquence d'un schéma Oracle.
     * 
     * @param sequenceName --
     */
    private void supprimerSequenceOracle(String sequenceName)
    {
        // Si la séquence existe, on la supprime sinon on efait rien
        if (isObjetOracleExiste("SEQUENCE", sequenceName))
        {
            StringBuilder sqlSuppressionSequence = new StringBuilder();
            sqlSuppressionSequence.append("DROP SEQUENCE ");
            sqlSuppressionSequence.append(sequenceName);

            executerSqlNatif(sqlSuppressionSequence.toString());

            LOGGER.debug(sqlSuppressionSequence.toString());

            // Contrôle de la suppression de la séquence
            if (isObjetOracleExiste("SEQUENCE", sequenceName))
            {
                StringBuilder msg = new StringBuilder();
                msg.append("LA SUPPRESSION DE LA SEQUENCE ");
                msg.append(sequenceName);
                msg.append("A ECHOUE");
                LOGGER.warn(msg.toString());
                throw new ProgrammationException(msg.toString());
            }
            else
            {
                StringBuilder msg = new StringBuilder();
                msg.append("LA SEQUENCE ");
                msg.append(sequenceName);
                msg.append(" A ETE SUPPRIMEE");
                LOGGER.warn(msg.toString());
            }
        }
        else
        {
            StringBuilder msg = new StringBuilder();
            msg.append("LA SEQUENCE ");
            msg.append(sequenceName);
            msg.append(" N'EXISTE PAS");
            LOGGER.warn(msg.toString());
        }
    }

    /**
     * Méthode permettant de récupérer l'identifiant maximum, en cours, dans une colonne.
     *
     * @param tableName nom de la table
     * @param columnId identifiant de la colonne
     * @return l'identifiant maximum dans une colonne
     */
    private long findTableMaxIdValue(String tableName, String columnId)
    {
        StringBuilder sqlFindMaxId = new StringBuilder(27);
        sqlFindMaxId.append("SELECT MAX(");
        sqlFindMaxId.append(columnId);
        sqlFindMaxId.append(") AS MAXID FROM ");
        sqlFindMaxId.append(tableName);

        Query sqlFindMaxIdQuery = executerSqlNatif(sqlFindMaxId.toString());

        long maxId = 0;
        List maxIdList = sqlFindMaxIdQuery.getResultList();

        if (maxIdList.size() > 0)
        {
            maxId = ((Long) maxIdList.get(0)).longValue();
        }
        else
        {
            maxId = 0;
        }

        StringBuilder msg = new StringBuilder(51);
        msg.append("LA VALEUR MAXIMALE DE LA COLONNE ");
        msg.append(columnId);
        msg.append(" DE LA TABLE ");
        msg.append(tableName);
        msg.append(" EST ");
        msg.append(maxId);

        LOGGER.warn(msg.toString());

        return maxId;
    }

    /**
     * suppression dune table temporaire.
     * 
     * @param tableName --
     */
    private void supprimerUneTableTemporaire(String tableName)
    {
        String nomTableTemporaire = creerNomTableTemporaire(tableName);

        // On contrôle que la table temporaire est présente dans le schéma
        if (isObjetOracleExiste("TABLE", nomTableTemporaire))
        {
            StringBuilder sqlDroptemporaryTable = new StringBuilder();
            sqlDroptemporaryTable.append("DROP TABLE ");
            sqlDroptemporaryTable.append(nomTableTemporaire);

            executerSqlNatif(sqlDroptemporaryTable.toString());

            // On controle que la table a bien été supprimée
            if (!isObjetOracleExiste("TABLE", nomTableTemporaire))
            {
                StringBuilder msg = new StringBuilder(17);
                msg.append("LA TABLE ");
                msg.append(nomTableTemporaire);
                msg.append(" A ETE SUPPRIMEE ");
                LOGGER.warn(msg.toString());
            }
            else
            {
                StringBuilder msg = new StringBuilder(23);
                msg.append("ERREUR : LA TABLE ");
                msg.append(nomTableTemporaire);
                msg.append(" N'A PAS ETE SUPPRIMEE ");
                LOGGER.warn(msg.toString());
                throw new ProgrammationException(msg.toString());
            }
        }
        else
        {
            StringBuilder msg = new StringBuilder(30);
            msg.append("LA TABLE ");
            msg.append(nomTableTemporaire);
            msg.append(" N'EXISTE PAS DANS LE SCHEMA. ");
            LOGGER.warn(msg.toString());
        }
    }

    /**
     * teste si un objet est présent dans un schéma Oracle.
     * 
     * @param objectType --
     * @param objectName --
     * @return true, if test existence objet oracle
     */
    private boolean isObjetOracleExiste(String objectType, String objectName)
    {
        boolean result = false;
        String sqlTestExistenceObjet =
            "SELECT COUNT(*) AS NBOBJECT FROM USER_OBJECTS WHERE OBJECT_TYPE=:OBJECT_TYPE AND OBJECT_NAME=:OBJECT_NAME";

        Query sqlTestExistenceObjetQuery = entityManager.createNativeQuery(sqlTestExistenceObjet);
        sqlTestExistenceObjetQuery.setParameter("OBJECT_TYPE", objectType);
        sqlTestExistenceObjetQuery.setParameter("OBJECT_NAME", objectName);
        List resultList = sqlTestExistenceObjetQuery.getResultList();

        long nbobject = 0;
        if (resultList.size() == 1)
        {
            nbobject = ((Long) resultList.get(0)).longValue();
        }

        result = (nbobject == 1 ? true : false);
        LOGGER.debug("TEST EXISTENCE DE L'OBJET '{} : {}", objectName, result);

        return result;
    }

    /**
     * Création d'une copie temporaire d'une table.
     * 
     * @param tableName --
     */
    private void sauvegarderUneTableDansTableTemporaire(String tableName)
    {
        String nomTableTemporaire = creerNomTableTemporaire(tableName);

        // On contrôle que la table temporaire n'est pas déjà présente dans
        // le
        // schéma
        if (isObjetOracleExiste("TABLE", nomTableTemporaire))
        {
            StringBuilder msg = new StringBuilder(27);
            msg.append("LA TABLE ");
            msg.append(nomTableTemporaire);
            msg.append(" EXISTE DEJA DANS LE SCHEMA");
            throw new ProgrammationException(msg.toString());
        }

        // Compte le nombre de lignes de la table source
        long nbLignesTableSrc = compterNbLignesDansUneTable(tableName);

        StringBuilder sqlCreatetemporaryTable = new StringBuilder(32);
        sqlCreatetemporaryTable.append("CREATE TABLE  ");
        sqlCreatetemporaryTable.append(nomTableTemporaire);
        sqlCreatetemporaryTable.append(" AS SELECT * FROM ");
        sqlCreatetemporaryTable.append(tableName);

        executerSqlNatif(sqlCreatetemporaryTable.toString());

        // Compte le nombre de ligne de la table de sauvegarde
        long nbLignesTableSav = compterNbLignesDansUneTable(nomTableTemporaire);

        if (nbLignesTableSrc != nbLignesTableSav)
        {
            StringBuilder msg = new StringBuilder();
            msg.append("LA TABLE ");
            msg.append(nomTableTemporaire);
            msg.append(" CONTIENT ");
            msg.append(nbLignesTableSav);
            msg.append(" ALORS QUE LA TABLE ");
            msg.append(tableName);
            msg.append(nbLignesTableSav);
            msg.append(" CONTIENT ");
            msg.append(nbLignesTableSrc);
            msg.append(" LIGNES");
            throw new ProgrammationException(msg.toString());
        }

        StringBuilder msg = new StringBuilder(64);
        msg.append("LA TABLE ");
        msg.append(tableName);
        msg.append(" (");
        msg.append(nbLignesTableSrc);
        msg.append(" lignes) A ETE SAUVEGARDEE DANS LA TABLE ");
        msg.append(nomTableTemporaire);
        msg.append(" (");
        msg.append(nbLignesTableSav);
        msg.append(" lignes) ");
        LOGGER.warn(msg.toString());
    }

    /**
     * Définition du nom de la table temporaire.
     * 
     * @param tableName --
     * @return the string
     */
    private String creerNomTableTemporaire(String tableName)
    {
        StringBuilder nomTableTemporaire = new StringBuilder();
        nomTableTemporaire.append(tableName);
        nomTableTemporaire.append("_SAV");
        return nomTableTemporaire.toString();
    }

    /**
     * Compte le nombre de ligne d'une table.
     * 
     * @param tableName --
     * @return nombre de lignes de la table
     */
    private long compterNbLignesDansUneTable(String tableName)
    {
        long nbLignes = 0;

        StringBuilder sqlCompteLigne = new StringBuilder(33);
        sqlCompteLigne.append("SELECT COUNT(*) AS NBLIGNES FROM ");
        sqlCompteLigne.append(tableName);

        Query sqlCompteLigneQuery = executerSqlNatif(sqlCompteLigne.toString());

        List resultList = sqlCompteLigneQuery.getResultList();

        if (resultList.size() == 1)
        {
            nbLignes = ((Long) resultList.get(0)).longValue();

            StringBuilder msg = new StringBuilder();
            msg.append("LA TABLE ");
            msg.append(tableName);
            msg.append(" contient ");
            msg.append(nbLignes);
            msg.append(" LIGNES");
            LOGGER.debug(msg.toString());
        }
        else
        {
            nbLignes = 0;
        }

        return nbLignes;
    }

    private Query executerSqlNatif(String sql)
    {
        Query query = entityManager.createNativeQuery(sql);
        if (1 != 1)
        {
            query.setParameter("1", 1);
        }
        query.executeUpdate();
        return query;
    }
}