/*
 * Copyright (c) 2014 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.journal.jpa.dao.impl;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaBuilder.In;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.SetJoin;
import javax.persistence.criteria.Subquery;

import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.Period;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import fr.gouv.finances.lombok.journal.bean.CriteresRecherchesJournal;
import fr.gouv.finances.lombok.journal.bean.IdentifiantsMetierObjOperation;
import fr.gouv.finances.lombok.journal.bean.OperationJournal;
import fr.gouv.finances.lombok.journal.bean.ParametreOperation;
import fr.gouv.finances.lombok.journal.dao.JournalDao;
import fr.gouv.finances.lombok.journal.dao.ProprieteJournal;
import fr.gouv.finances.lombok.util.date.TemporaliteUtils;
import fr.gouv.finances.lombok.util.exception.ProgrammationException;
import fr.gouv.finances.lombok.util.persistance.ScrollIterator;

/**
 * Implémentation JPA du DAO permettant de gérer les données de journalisation.
 *
 * @author Christophe Breheret-Girardin
 */
@ConditionalOnProperty(name = "lombok.orm.jpa", havingValue="true")
@Repository("journaldaoimpl")
@Transactional(transactionManager="transactionManager")
public class JournalDaoImpl extends AbstractJournalOracleDaoImpl implements JournalDao
{
    /** Initialisation de la journalisation. */
    protected static final Logger LOGGER = LoggerFactory.getLogger(JournalDaoImpl.class);

    /**
     * Constructeur.
     */
    public JournalDaoImpl()
    {
        super();
    }

    /**
     * {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.journal.dao.JournalDao#countOperationJournalAssocieAUnParametreOperation(fr.gouv.finances.lombok.journal.bean.ParametreOperation).
     */
    @Override
    public int countOperationJournalAssocieAUnParametreOperation(ParametreOperation paramOper)
    {
        LOGGER.debug(">>> Debut methode countOperationJournalAssocieAUnParametreOperation");
        // Contrôle du paramètre
        if (paramOper == null)
        {
            throw new ProgrammationException("Aucun paramètre d'opération de journalisation fourni");
        }
        if (paramOper.getId() == null)
        {
            throw new ProgrammationException("Le paramètre d'opération de journalisation n'a pas d'identifiant");
        }

        // Paramétrage de la recherche sur le nombre d'opération de journalisation
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Long> queryNbreOperation = criteriaBuilder.createQuery(Long.class);
        Root<OperationJournal> from = queryNbreOperation.from(OperationJournal.class);
        CriteriaQuery<Long> selectNbreOperation = queryNbreOperation.select(criteriaBuilder.count(from));

        // Restriction sur les opérations liées au paramètre fourni
        SetJoin<OperationJournal, ParametreOperation> join = from.joinSet(ProprieteJournal.PARAMETRES.getNom());
        selectNbreOperation.where(criteriaBuilder.equal(join.<Long> get("id"), paramOper.getId()));

        // Exécution de la recherche
        return entityManager.createQuery(selectNbreOperation).getSingleResult().intValue();
    }

    @Override
    public void deleteOperationJournal(OperationJournal operationjournal)
    {
        LOGGER.debug(">>> Debut methode deleteOperationJournal");
        if (operationjournal != null)
        {
            deleteObject(operationjournal);
        }
    }

    /**
     * {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.journal.dao.JournalDao#findOperationsJournalParCriteresRecherche(fr.gouv.finances.lombok.journal.bean.CriteresRecherchesJournal).
     */
    @Override
    public List<OperationJournal> findOperationsJournalParCriteresRecherche(CriteresRecherchesJournal crit)
    {
        LOGGER.debug(">>> Debut methode findOperationsJournalParCriteresRecherche");
        // Contrôle du paramètre
        if (crit == null)
        {
            throw new ProgrammationException("Aucun critère de recherche fourni");
        }

        // Paramétrage de la recherche
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<OperationJournal> query = criteriaBuilder.createQuery(OperationJournal.class);
        Root<OperationJournal> root = query.from(OperationJournal.class);

        List<Predicate> predicates = new ArrayList<>();

        // Paramétrage des critères
        if ((StringUtils.isNotBlank(crit.getIdentifiantStructure())))
        {
            predicates.add(criteriaBuilder
                .equal(root.<Long> get(ProprieteJournal.IDENTIFIANT_STRUCTURE.getNom()), crit.getIdentifiantStructure()));
        }

        // Recherche avec nature operation
        if (crit.getNaturesOperations() != null && !crit.getNaturesOperations().isEmpty())
        {
            if (crit.getNaturesOperations().size() > 1)
            {
                // Si le critère de recherche contient plus d'une nature opération
                In<Object> criteresNatureOperation = criteriaBuilder
                    .in(root.<String> get(ProprieteJournal.NATURE.getNom()));
                criteresNatureOperation.value(crit.getNaturesOperations());
                predicates.add(criteresNatureOperation);
            }
            else
            {
                if (StringUtils.isNotBlank(crit.getNaturesOperations().get(0)))
                {
                    predicates
                        .add(criteriaBuilder.equal(root.<Long> get(ProprieteJournal.NATURE.getNom()), crit.getNaturesOperations().get(0)));
                }
            }
        }

        // Recherche avec code opération
        String codeoperation = null;
        if (crit.getCodesOperation() != null && !crit.getCodesOperation().isEmpty())
        {
            if (crit.getCodesOperation().size() > 1)
            {
                // Si le critère de recherche contient plus d'un code opération
                In<Object> criteresCodeOperation = criteriaBuilder.in(
                    root.<String> get(ProprieteJournal.CODE.getNom()));
                criteresCodeOperation.value(crit.getCodesOperation());
                predicates.add(criteresCodeOperation);
            }
            else
            {
                // Si le critère de recherche contient une liste d'un seul code opération
                codeoperation = crit.getCodesOperation().get(0);
            }
        }
        else
        {
            // Si le critère de recherche ne contient qu'un seul code opération
            codeoperation = crit.getCodeOperation();
        }
        if (StringUtils.isNotBlank(codeoperation))
        {
            predicates.add(criteriaBuilder.equal(root.<Long> get(ProprieteJournal.CODE.getNom()), codeoperation));
        }

        // Recherche avec identifiant utilisateur ou batch
        if (StringUtils.isNotBlank(crit.getIdentifiantUtilisateurOuBatch()))
        {
            predicates.add(
                criteriaBuilder.equal(root.<Long> get(ProprieteJournal.IDENTIFIANT.getNom()), crit.getIdentifiantUtilisateurOuBatch()));
        }

        // Recherche avec dateHeureOperation
        if (crit.getDateHeureOperation() != null)
        {
            predicates.add(criteriaBuilder.equal(root.<Date> get(ProprieteJournal.TEMPORALITE.getNom()), crit.getDateHeureOperation()));
        }

        // Recherche avec la date maximale
        if (crit.getDateMax() != null)
        {
            DateTime dMax = new DateTime(crit.getDateMax()).plus(Period.hours(24));
            predicates.add(criteriaBuilder.lessThanOrEqualTo(root.<Date> get(ProprieteJournal.TEMPORALITE.getNom()), dMax.toDate()));
        }

        // Recherche avec la date inférieure
        if (crit.getDateMin() != null)
        {
            predicates.add(criteriaBuilder.greaterThanOrEqualTo(root.<Date> get(ProprieteJournal.TEMPORALITE.getNom()), crit.getDateMin()));
        }

        // Recherche à partir de la date avec heures et secondes à 0
        Date dateJJMMAAAA = crit.getDateJJMMAAAA();
        if (dateJJMMAAAA != null)
        {
            DateTime dtt = new DateTime(dateJJMMAAAA).plus(Period.hours(24));
            predicates.add(criteriaBuilder.between(root.<Date> get(ProprieteJournal.TEMPORALITE.getNom()), dateJJMMAAAA, dtt.toDate()));
        }

        // Recherche apurement
        Boolean apurementPossible = crit.getApurementPossible();
        if (apurementPossible != null)
        {
            predicates.add(criteriaBuilder.equal(root.<Boolean> get(ProprieteJournal.APUREMENT.getNom()), apurementPossible));
        }

        // Recherche sur les paramètres
        String valeurParamOperMax = crit.getValeurParamOperMax();
        String nomParamOper = crit.getNomParamOper();
        String valeurParamOperMin = crit.getValeurParamOperMin();
        String valeurParamOper = crit.getValeurParamOper();

        // recherche avec le nom tout simplement
        if (StringUtils.isBlank(valeurParamOperMax) && StringUtils.isNotBlank(nomParamOper)
            && StringUtils.isBlank(valeurParamOperMin)
            && StringUtils.isBlank(valeurParamOper))
        {
            SetJoin<OperationJournal, ParametreOperation> join = root.joinSet(ProprieteJournal.PARAMETRES.getNom());
            predicates.add(criteriaBuilder.equal(join.<String> get(ProprieteJournal.NOM_PARAM.getNom()), nomParamOper));
        }

        try
        {
            // recherche avec le nom et la valeur max
            if (StringUtils.isNotBlank(valeurParamOperMax) && StringUtils.isNotBlank(nomParamOper))
            {
                SetJoin<OperationJournal, ParametreOperation> join = root.joinSet(ProprieteJournal.PARAMETRES.getNom());
                predicates.add(criteriaBuilder.equal(join.<String> get(ProprieteJournal.NOM_PARAM.getNom()), nomParamOper));
                predicates.add(criteriaBuilder.le(criteriaBuilder.toInteger(join.<Number> get(ProprieteJournal.VALEUR_PARAM.getNom())),
                    toValeur(valeurParamOperMax)));
            }

            // Recherche avec le nom et la valeur min
            if (StringUtils.isNotBlank(valeurParamOperMin) && StringUtils.isNotBlank(nomParamOper))
            {
                SetJoin<OperationJournal, ParametreOperation> join = root.joinSet(ProprieteJournal.PARAMETRES.getNom());
                predicates.add(criteriaBuilder.equal(join.<String> get(ProprieteJournal.NOM_PARAM.getNom()), nomParamOper));
                predicates.add(criteriaBuilder.ge(criteriaBuilder.toInteger(join.<Number> get(ProprieteJournal.VALEUR_PARAM.getNom())),
                    toValeur(valeurParamOperMin)));
            }
        }
        catch (ParseException pe)
        {
            return new ArrayList<>();
        }

        // Recherche avec le nom et la valeur exacte
        if (StringUtils.isNotBlank(valeurParamOper) && StringUtils.isNotBlank(nomParamOper))
        {
            SetJoin<OperationJournal, ParametreOperation> join = root.joinSet(ProprieteJournal.PARAMETRES.getNom());
            predicates.add(criteriaBuilder.equal(join.<String> get(ProprieteJournal.NOM_PARAM.getNom()), nomParamOper));
            predicates.add(criteriaBuilder.equal(join.get(ProprieteJournal.VALEUR_PARAM.getNom()), valeurParamOper));
        }

        // Ajout des critères à la requête
        query.where(predicates.toArray(new Predicate[] {}));

        // Paramétrage des jointures
        root.fetch(ProprieteJournal.PARAMETRES.getNom(), JoinType.LEFT);

        // Exécution de la recherche
        return findAll(query.distinct(true));
    }

    /**
     * {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.journal.dao.JournalDao#findOperationsJournalParIdentifiantMetierObjOperation(fr.gouv.finances.lombok.journal.bean.IdentifiantsMetierObjOperation).
     */
    @Override
    public List<OperationJournal> findOperationsJournalParIdentifiantMetierObjOperation(
        IdentifiantsMetierObjOperation unIdentifiantMetierObjOperation)
    {
        LOGGER.debug(">>> Debut methode findOperationsJournalParIdentifiantMetierObjOperation");
        // Cas du paramètre non fourni
        if (unIdentifiantMetierObjOperation == null)
        {
            throw new ProgrammationException("identifiant métier obj opération non fourni");
        }

        // Paramétrage de la recherche
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<OperationJournal> query = criteriaBuilder.createQuery(OperationJournal.class);
        Root<OperationJournal> root = query.from(OperationJournal.class);

        List<Predicate> predicates = new ArrayList<>();

        String valeurIdentifiantMetier1 = unIdentifiantMetierObjOperation.getValeurIdentifiantMetier1();
        if (StringUtils.isNotBlank(valeurIdentifiantMetier1))
        {
            predicates.add(criteriaBuilder.equal(
                root.<IdentifiantsMetierObjOperation> get(ProprieteJournal.IDENTIFIANT_METIER.getNom())
                    .<String> get("valeurIdentifiantMetier1"),
                valeurIdentifiantMetier1));
        }
        String valeurIdentifiantMetier2 = unIdentifiantMetierObjOperation.getValeurIdentifiantMetier2();
        if (StringUtils.isNotBlank(valeurIdentifiantMetier2))
        {
            predicates.add(criteriaBuilder.equal(
                root.<IdentifiantsMetierObjOperation> get(ProprieteJournal.IDENTIFIANT_METIER.getNom())
                    .<String> get("valeurIdentifiantMetier2"),
                valeurIdentifiantMetier2));
        }
        String valeurIdentifiantMetier3 = unIdentifiantMetierObjOperation.getValeurIdentifiantMetier3();
        if (StringUtils.isNotBlank(valeurIdentifiantMetier3))
        {
            predicates.add(criteriaBuilder.equal(
                root.<IdentifiantsMetierObjOperation> get(ProprieteJournal.IDENTIFIANT_METIER.getNom())
                    .<String> get("valeurIdentifiantMetier3"),
                valeurIdentifiantMetier3));
        }
        String valeurIdentifiantMetier4 = unIdentifiantMetierObjOperation.getValeurIdentifiantMetier4();
        if (StringUtils.isNotBlank(valeurIdentifiantMetier4))
        {
            predicates.add(criteriaBuilder.equal(
                root.<IdentifiantsMetierObjOperation> get(ProprieteJournal.IDENTIFIANT_METIER.getNom())
                    .<String> get("valeurIdentifiantMetier4"),
                valeurIdentifiantMetier4));
        }
        String valeurIdentifiantMetier5 = unIdentifiantMetierObjOperation.getValeurIdentifiantMetier5();
        if (StringUtils.isNotBlank(valeurIdentifiantMetier5))
        {
            predicates.add(criteriaBuilder.equal(
                root.<IdentifiantsMetierObjOperation> get(ProprieteJournal.IDENTIFIANT_METIER.getNom())
                    .<String> get("valeurIdentifiantMetier5"),
                valeurIdentifiantMetier5));
        }

        String libelleIdentifiantMetier1 = unIdentifiantMetierObjOperation.getLibelleIdentifiantMetier1();
        if (StringUtils.isNotBlank(libelleIdentifiantMetier1))
        {
            predicates.add(criteriaBuilder.equal(
                root.<IdentifiantsMetierObjOperation> get(ProprieteJournal.IDENTIFIANT_METIER.getNom())
                    .<String> get("libelleIdentifiantMetier1"),
                libelleIdentifiantMetier1));
        }
        String libelleIdentifiantMetier2 = unIdentifiantMetierObjOperation.getLibelleIdentifiantMetier2();
        if (StringUtils.isNotBlank(libelleIdentifiantMetier2))
        {
            predicates.add(criteriaBuilder.equal(
                root.<IdentifiantsMetierObjOperation> get(ProprieteJournal.IDENTIFIANT_METIER.getNom())
                    .<String> get("libelleIdentifiantMetier2"),
                libelleIdentifiantMetier2));
        }
        String libelleIdentifiantMetier3 = unIdentifiantMetierObjOperation.getLibelleIdentifiantMetier3();
        if (StringUtils.isNotBlank(libelleIdentifiantMetier3))
        {
            predicates.add(criteriaBuilder.equal(
                root.<IdentifiantsMetierObjOperation> get(ProprieteJournal.IDENTIFIANT_METIER.getNom())
                    .<String> get("libelleIdentifiantMetier3"),
                libelleIdentifiantMetier3));
        }
        String libelleIdentifiantMetier4 = unIdentifiantMetierObjOperation.getLibelleIdentifiantMetier4();
        if (StringUtils.isNotBlank(libelleIdentifiantMetier4))
        {
            predicates.add(criteriaBuilder.equal(
                root.<IdentifiantsMetierObjOperation> get(ProprieteJournal.IDENTIFIANT_METIER.getNom())
                    .<String> get("libelleIdentifiantMetier4"),
                libelleIdentifiantMetier4));
        }
        String libelleIdentifiantMetier5 = unIdentifiantMetierObjOperation.getLibelleIdentifiantMetier5();
        if (StringUtils.isNotBlank(libelleIdentifiantMetier5))
        {
            predicates.add(criteriaBuilder.equal(
                root.<IdentifiantsMetierObjOperation> get(ProprieteJournal.IDENTIFIANT_METIER.getNom())
                    .<String> get("libelleIdentifiantMetier5"),
                libelleIdentifiantMetier5));
        }

        // Ajout des critères à la requête
        query.where(predicates.toArray(new Predicate[] {}));

        // Paramétrage des jointures
        root.fetch(ProprieteJournal.PARAMETRES.getNom(), JoinType.LEFT);

        // Exécution de la recherche
        return findAll(query.distinct(true));
    }

    /**
     * {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.journal.dao.JournalDao#findOperationsPerimees(java.lang.String).
     */
    @Override
    public List<OperationJournal> findOperationsPerimees(String datelimite)
    {
        LOGGER.debug(">>> Debut methode findOperationsPerimees");
        if (StringUtils.isEmpty(datelimite))
        {
            throw new ProgrammationException("La date limite n'a pas été fournie");
        }

        // Paramétrage de la recherche
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<OperationJournal> query = criteriaBuilder.createQuery(OperationJournal.class);
        Root<OperationJournal> root = query.from(OperationJournal.class);

        query.where(
            criteriaBuilder.lessThan(root.<Date> get(ProprieteJournal.TEMPORALITE.getNom()),
                TemporaliteUtils.getDate(datelimite, "MM/dd/yyyy")),
            criteriaBuilder.equal(root.<Boolean> get(ProprieteJournal.APUREMENT.getNom()), true));

        return findAll(query);
    }

    /**
     * {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.journal.dao.JournalDao#findParametresOperations(fr.gouv.finances.lombok.journal.bean.OperationJournal).
     */
    @Override
    public Set<ParametreOperation> findParametresOperations(OperationJournal operationJournal)
    {
        LOGGER.debug(">>> Debut methode findParametresOperations");
        // Contrôle du paramètre
        if (operationJournal == null)
        {
            throw new ProgrammationException("Aucune opération de journalisation fournie");
        }
        if (operationJournal.getId() == null)
        {
            throw new ProgrammationException("L'opération de journalisation n'a pas d'identifiant");
        }

        entityManager.merge(operationJournal);
        return operationJournal.getLesParamOperation();
    }

    /**
     * {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.journal.dao.JournalDao#findUneOperationJournalParIdUtilEtDateHeure(java.lang.String,
     *      java.util.Date).
     */
    @Override
    public OperationJournal findUneOperationJournalParIdUtilEtDateHeure(String idUtilOuBatch, Date dateHeureOperation)
    {
        LOGGER.debug(">>> Debut methode findUneOperationJournalParIdUtilEtDateHeure");
        // Paramétrage de la recherche
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<OperationJournal> query = criteriaBuilder.createQuery(OperationJournal.class);
        Root<OperationJournal> root = query.from(OperationJournal.class);

        // Paramétrage des critères
        query.where(criteriaBuilder.equal(root.<Long> get(ProprieteJournal.IDENTIFIANT.getNom()), idUtilOuBatch),
            criteriaBuilder.equal(root.<Date> get(ProprieteJournal.TEMPORALITE.getNom()), dateHeureOperation));

        // Paramétrage des jointures
        root.fetch(ProprieteJournal.PARAMETRES.getNom(), JoinType.LEFT);

        // Exécution de la recherche
        return find(query);
    }

    /**
     * {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.journal.dao.JournalDao#findUneOperationParId(java.lang.Long).
     */
    @Override
    public OperationJournal findUneOperationParId(Long identifiant)
    {
        LOGGER.debug(">>> Debut methode findUneOperationParId");
        // Paramétrage de la recherche
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<OperationJournal> query = criteriaBuilder.createQuery(OperationJournal.class);
        Root<OperationJournal> root = query.from(OperationJournal.class);

        // Paramétrage des critères
        query.where(criteriaBuilder.equal(root.<Long> get("id"), identifiant));

        // Paramétrage des jointures
        root.fetch(ProprieteJournal.PARAMETRES.getNom(), JoinType.LEFT);

        // Exécution de la recherche
        return find(query);
    }

    /**
     * {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.journal.dao.JournalDao#countNombreParametresOrphelins().
     */
    @Override
    public Long countNombreParametresOrphelins()
    {
        LOGGER.debug(">>> Debut methode comptageDuNombreDeParametresOrphelins");
        // Paramétrage de la recherche sur le nombre de paramètres d'opération de journalisation
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Long> queryNbreParametre = criteriaBuilder.createQuery(Long.class);
        Root<ParametreOperation> from = queryNbreParametre.from(ParametreOperation.class);
        CriteriaQuery<Long> selectNbreParametre = queryNbreParametre.select(criteriaBuilder.count(from));

        // Restriction sur les paramètres non rattachés à une opération de journalisation
        Subquery<Long> subQuery = queryNbreParametre.subquery(Long.class);
        Root<OperationJournal> rootJournal = subQuery.from(OperationJournal.class);
        Join<OperationJournal, ParametreOperation> join = rootJournal.join(ProprieteJournal.PARAMETRES.getNom());
        subQuery.select(join.get("id"));
        queryNbreParametre.select(criteriaBuilder.count(from)).where(
            criteriaBuilder.not(criteriaBuilder.in(from.get("id")).value(subQuery)));

        // Exécution de la recherche
        return entityManager.createQuery(selectNbreParametre).getSingleResult();
    }

    /**
     * {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.journal.dao.JournalDao#initialiseParametresOperation(fr.gouv.finances.lombok.journal.bean.OperationJournal).
     */
    @Override
    public void initialiseParametresOperation(OperationJournal operationJournal)
    {
        LOGGER.debug(">>> Debut methode initialiseParametresOperation");
        if (operationJournal != null)
        {
            operationJournal.getLesParamOperation().size();
        }
    }

    /**
     * {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.journal.dao.JournalDao#saveNouvelleOperationAuJournal(fr.gouv.finances.lombok.journal.bean.OperationJournal).
     */
    @Override
    public void saveNouvelleOperationAuJournal(OperationJournal uneOperationJournal)
    {
        LOGGER.debug(">>> Debut methode saveUneNouvelleOperationAuJournal");
        saveObject(uneOperationJournal);
    }

    /**
     * {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.journal.dao.JournalDao#findOperationsPerimeesAUneDatePourUneDureeDeRetention(java.util.Date,
     *      java.lang.String, int).
     */
    @Override
    public ScrollIterator findOperationsPerimeesAUneDatePourUneDureeDeRetention(
        Date dateCourante, String typeDureeDeConservation, int joursDeRetention)
    {
        LOGGER.debug(">>> Debut methode findOperationsPerimeesAUneDatePourUneDureeDeRetention");
        // Paramétrage de la recherche
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<OperationJournal> query = criteriaBuilder.createQuery(OperationJournal.class);
        Root<OperationJournal> from = query.from(OperationJournal.class);

        // Paramétrage des critères
        query.where(
            criteriaBuilder.equal(from.<String> get(ProprieteJournal.TYPE_DUREE_CONSERVATION.getNom()), typeDureeDeConservation),
            criteriaBuilder.lessThan(
                from.<Date> get(ProprieteJournal.TEMPORALITE.getNom()), TemporaliteUtils.ajouterJour(dateCourante, -joursDeRetention)),
            criteriaBuilder.or(
                criteriaBuilder.isNull(from.<Boolean> get(ProprieteJournal.APUREMENT.getNom())),
                criteriaBuilder.equal(from.<Boolean> get(ProprieteJournal.APUREMENT.getNom()), true)));

        // Récupération d'un itérateur en mode curseur
        return getScrollIterator(query, 0);
    }

    /**
     * {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.journal.dao.JournalDao#findAllOperationsJournal().
     */
    @Override
    public ScrollIterator findAllOperationsJournal()
    {
        LOGGER.debug(">>> Debut methode findToutesLesOperationsJournal");
        return getScrollIterator(createQuery(OperationJournal.class), 0);
    }

    /**
     * {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.journal.dao.JournalDao#supprimerParametresOperationsOrphelins().
     */
    @Override
    public void supprimerParametresOperationsOrphelins()
    {
        LOGGER.debug(">>> Debut methode supprimerLesParametresOperationsOrphelins");
        // Paramétrage de la recherche
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<ParametreOperation> queryParam = criteriaBuilder.createQuery(ParametreOperation.class);
        Root<ParametreOperation> rootParam = queryParam.from(ParametreOperation.class);

        Subquery<Long> subQuery = queryParam.subquery(Long.class);
        Root<OperationJournal> rootJournal = subQuery.from(OperationJournal.class);
        Join<OperationJournal, ParametreOperation> join = rootJournal.join(ProprieteJournal.PARAMETRES.getNom());

        subQuery.select(join.get("id"));

        queryParam.select(rootParam).where(
            criteriaBuilder.not(criteriaBuilder.in(rootParam.get("id")).value(subQuery)));

        TypedQuery<ParametreOperation> paramTypesQuery = entityManager.createQuery(queryParam);
        List<ParametreOperation> parametresOperation = paramTypesQuery.getResultList();

        parametresOperation.stream().forEach(this::deleteObject);
    }
    
    private Number toValeur(String valeur) throws ParseException
    {
        try
        {
            return NumberFormat.getInstance().parse(valeur);
        }
        catch (ParseException pe)
        {
            LOGGER.info("Impossible de convertir '{}' en numérique", valeur);
            throw pe;
        }
    }
}