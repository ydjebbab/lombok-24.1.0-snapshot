/*
 * Copyright (c) 2020 DGFiP - Tous droits réservés
 * 
 */
package fr.gouv.finances.lombok.journal.dao.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.data.jpa.JpaRepositoriesAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionTemplate;

import fr.gouv.finances.lombok.journal.bean.CriteresRecherchesJournal;
import fr.gouv.finances.lombok.journal.bean.IdentifiantsMetierObjOperation;
import fr.gouv.finances.lombok.journal.bean.OperationJournal;
import fr.gouv.finances.lombok.journal.bean.ParametreOperation;
import fr.gouv.finances.lombok.journal.config.PersistenceJPAConfigTest;
import fr.gouv.finances.lombok.journal.dao.JournalDao;
import fr.gouv.finances.lombok.test.controle.ControleDonnees;
import fr.gouv.finances.lombok.test.controle.VerificationExecution;
import fr.gouv.finances.lombok.util.base.PurgeTablesDao;
import fr.gouv.finances.lombok.util.date.TemporaliteUtils;
import fr.gouv.finances.lombok.util.exception.ProgrammationException;
import fr.gouv.finances.lombok.util.persistance.ScrollIterator;

/**
 * Tests unitaires automatisés du DAO JPA gérant les données des opérations de journalisation.
 *
 * @author Christophe Breheret-Girardin
 * @author CF
 */
@ComponentScan(basePackages = {"fr.gouv.finances.lombok.jpa.dao", "fr.gouv.finances.lombok.journal.jpa.dao.impl"})
@EnableAutoConfiguration(exclude = JpaRepositoriesAutoConfiguration.class)
@SpringBootTest(classes = {PersistenceJPAConfigTest.class}, properties = {"lombok.composant.journal.inclus=true", "lombok.orm.jpa=true"})
@ActiveProfiles(profiles = {"embedded"})
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class JournalDaoTest
{
    public JournalDaoTest()
    {
        super();
    }

    protected static final Logger LOGGER = LoggerFactory.getLogger(JournalDaoTest.class);

    /** Déclaration du DAO de gestion des données de journalisation. */
    @Autowired
    private JournalDao journaldao;

    /** Déclaration du DAO de gestion des purges de la base. */
    @Autowired
    private PurgeTablesDao purgetablesdao;

    /** Déclaration du template de transaction. */
    @Autowired
    private TransactionTemplate transactionTemplate;

    /** Map contenant les paramètres de journalisation présents en base */
    private Map<Integer, OperationJournal> mapJournal = new HashMap<>();

    /** Map contenant les paramètres d'opération de journalisation présents en base */
    private Map<Integer, ParametreOperation> mapParam = new HashMap<>();

    /** Valeur inexistante en base */
    private static final String VALEUR_INEXISTANTE = "kamoulox";

    /** Valeur de référence utilisée dans les tests */
    private static final String PREFIX_VALEUR_REFERENCE = "42-";

    /** Préfix de valeur utilisée dans les tests pour les identifiants métier */
    private static final String PREFIX_VALEUR_ID_METIER = "Valeur id metier ";

    /** Préfix de libellé utilisé dans les tests pour les identifiants métier */
    private static final String PREFIX_LIBELLE_ID_METIER = "Libelle id metier ";

    /** Valeur d'un 1er type de conservation utilisé dans les tests */
    private static final String TYPE_CONSERVATION_1 = "Type conserve 1";

    /** Valeur d'un 2e type de conservation utilisé dans les tests */
    private static final String TYPE_CONSERVATION_2 = "Type conserve 2";

    /**
     * Initialisation des données en base avant chaque test.
     */
    @Before
    public void creerDonnees()
    {
        // Purge des données de la base
        purgetablesdao.purgerTables("TJ_ZOPE_ZPAO_TJ", "ZOPERATIONJOURNAL_ZOPE", "ZPARAMETREOPERATION_ZPAO");

        // Vérification de l'absence de données
        ControleDonnees.verifierElements(journaldao.loadAllObjects(OperationJournal.class));

        // Génération des paramètres de journalisation, insertion en base de données et sauvegarde dans une Map
        mapParam = IntStream.rangeClosed(1, 7).mapToObj(ParametreOperationUtils::getParametreOperation)
            .peek(journaldao::saveObject)
            .collect(Collectors.toMap(param -> param.getNumIncrement().intValue(), param -> param));

        // Génération des opération de journalisation, insertion en base de données et sauvegarde dans une Map
        mapJournal = IntStream.rangeClosed(1, 5).mapToObj(this::getJournal).peek(journaldao::saveObject)
            .collect(Collectors.toMap(j -> Integer.parseInt(j.getIdentifiantUtilisateurOuBatch()), j -> j));

        // Vérification des données à partir de l'image de la base
        ControleDonnees.verifierElements(journaldao.loadAllObjects(OperationJournal.class), mapJournal.get(1), mapJournal.get(2),
            mapJournal.get(3), mapJournal.get(4), mapJournal.get(5));
        ControleDonnees.verifierElements(journaldao.loadAllObjects(ParametreOperation.class), mapParam.get(1), mapParam.get(2),
            mapParam.get(3), mapParam.get(4), mapParam.get(5), mapParam.get(6), mapParam.get(7));

        // Suppression du cache pour forcer les requêtes des tests
        journaldao.clearPersistenceContext();
    }

    /**
     * Méthode permettant de tester l'injection du DAO.
     */
    @Test
    public void injectionDao()
    {
        assertNotNull(journaldao);
    }

    /**
     * Méthode permettant de tester les contraintes de l'entité opération de journalisation.
     */
    @Test
    public void contraintes()
    {
        // Une contrainte existe sur les propriétés "dateHeureOperation" et "identifiantUtilisateurOuBatch"
        VerificationExecution.verifierException(DataIntegrityViolationException.class, () -> journaldao.saveObject(getJournal(1)));

        // Pas d'erreur sur un même identifiant
        OperationJournal operationJournal = new OperationJournal();
        operationJournal.setIdentifiantUtilisateurOuBatch(mapJournal.get(2).getIdentifiantUtilisateurOuBatch());
        operationJournal.setDateHeureOperation(TemporaliteUtils.getDate(2010, 3, 1));
        journaldao.saveObject(operationJournal);

        // Pas d'erreur sur une même date
        operationJournal = new OperationJournal();
        operationJournal.setIdentifiantUtilisateurOuBatch(VALEUR_INEXISTANTE);
        operationJournal.setDateHeureOperation(mapJournal.get(2).getDateHeureOperation());
        journaldao.saveObject(operationJournal);
    }

    /**
     * Méthode permettant de tester le non chargement par défaut des paramètres des opérations de journalisation.
     */
    @Test
    public void lazy()
    {
        // Les paramètres d'opération de journalisation doivent être en chargement "lazy"
        verifierJournaux(journaldao.loadAllObjects(OperationJournal.class), false, mapJournal.get(1), mapJournal.get(2), mapJournal.get(3),
            mapJournal.get(4), mapJournal.get(5));
    }

    // @Test
    public void valeurParDefaut()
    {
        // L'apurement possible doit avoir été positionné à false par défaut
        assertNotNull("L'apurement possible doit avoir été initialisé", mapJournal.get(1).getApurementPossible());
        assertFalse("L'apurement possible doit avoir été initialisé à false", mapJournal.get(1).getApurementPossible());
    }

    /**
     * Test de la méthode
     * fr.gouv.finances.lombok.journal.dao.JournalDao#saveUneNouvelleOperationAuJournal(fr.gouv.finances.lombok.journal.bean.OperationJournal).
     */
    @Test
    public void saveUneNouvelleOperationAuJournal()
    {
        LOGGER.debug("Methode saveUneNouvelleOperationAuJournal");
        // Génération et insertion d'un nouveau journal
        OperationJournal operationJournal6 = getJournal(6);
        journaldao.saveNouvelleOperationAuJournal(operationJournal6);

        // Vérification de l'insertion
        ControleDonnees.verifierElements(journaldao.loadAllObjects(OperationJournal.class), mapJournal.get(1), mapJournal.get(2),
            mapJournal.get(3), mapJournal.get(4), mapJournal.get(5), operationJournal6);
    }

    /**
     * Test de la méthode
     * fr.gouv.finances.lombok.journal.dao.JournalDao#deleteOperationJournal(fr.gouv.finances.lombok.journal.bean.OperationJournal).
     */
    @Test
    public void deleteOperationJournal()
    {
        // Exécution de la méthode à tester sur un paramètre non initialisé
        journaldao.deleteOperationJournal(null);

        // Exécution de la méthode à tester et vérifiation
        journaldao.deleteOperationJournal(mapJournal.get(2));
        ControleDonnees.verifierElements(journaldao.loadAllObjects(OperationJournal.class), mapJournal.get(1), mapJournal.get(3),
            mapJournal.get(4), mapJournal.get(5));
    }

    /**
     * Test de la méthode
     * fr.gouv.finances.lombok.journal.dao.JournalDao#countOperationJournalAssocieAUnParametreOperation(fr.gouv.finances.lombok.journal.bean.ParametreOperation).
     */
    @Test
    public void countOperationJournalAssocieAUnParametreOperation()
    {
        // Exécution de la méthode à tester sur un paramètre non initialisé
        VerificationExecution.verifierException(ProgrammationException.class,
            () -> journaldao.countOperationJournalAssocieAUnParametreOperation(null));

        // Exécution de la méthode à tester sur des paramètres non trouvés
        VerificationExecution.verifierException(ProgrammationException.class,
            () -> journaldao.countOperationJournalAssocieAUnParametreOperation(
                ParametreOperationUtils.getParametreOperation(10)));

        // Exécution de la méthode à tester sur tous les paramètres d'opération de journalisation
        assertEquals(2, journaldao.countOperationJournalAssocieAUnParametreOperation(mapParam.get(1)));
        assertEquals(2, journaldao.countOperationJournalAssocieAUnParametreOperation(mapParam.get(2)));
        assertEquals(1, journaldao.countOperationJournalAssocieAUnParametreOperation(mapParam.get(3)));
        assertEquals(0, journaldao.countOperationJournalAssocieAUnParametreOperation(mapParam.get(4)));
        assertEquals(0, journaldao.countOperationJournalAssocieAUnParametreOperation(mapParam.get(5)));
        assertEquals(0, journaldao.countOperationJournalAssocieAUnParametreOperation(mapParam.get(6)));
        assertEquals(0, journaldao.countOperationJournalAssocieAUnParametreOperation(mapParam.get(7)));
    }

    /**
     * Test de la méthode fr.gouv.finances.lombok.journal.dao.JournalDao#findOperationsPerimees(java.lang.String).
     */
    @Test
    public void findOperationsPerimees()
    {
        // Exécution de la méthode à tester sur des paramètres non renseignés, vides ou erronés
        VerificationExecution.verifierException(ProgrammationException.class, () -> journaldao.findOperationsPerimees(null));
        VerificationExecution.verifierException(ProgrammationException.class, () -> journaldao.findOperationsPerimees(""));
        VerificationExecution.verifierException(DateTimeParseException.class, () -> journaldao.findOperationsPerimees(VALEUR_INEXISTANTE));

        // Exécution de la méthode à tester sans résultat et vérification
        ControleDonnees.verifierElements(journaldao.findOperationsPerimees("03/05/2000"));

        // Exécution de la méthode à tester
        verifierJournaux(journaldao.findOperationsPerimees("03/05/2018"), false, mapJournal.get(1), mapJournal.get(4));
    }

    /**
     * Test de la méthode fr.gouv.finances.lombok.journal.dao.JournalDao#comptageDuNombreDeParametresOrphelins().
     */
    @Test
    public void comptageDuNombreDeParametresOrphelins()
    {
        // Exécution de la méthode à tester
        assertEquals(4l, journaldao.countNombreParametresOrphelins().longValue());

        // Purge des données de la base
        purgetablesdao.purgerTables("TJ_ZOPE_ZPAO_TJ", "ZOPERATIONJOURNAL_ZOPE", "ZPARAMETREOPERATION_ZPAO");
        // Exécution de la méthode à tester
        assertEquals(0l, journaldao.countNombreParametresOrphelins().longValue());
    }

    /**
     * Test de la méthode
     * 
     * @see fr.gouv.finances.lombok.journal.dao.JournalDao#supprimerLesParametresOperationsOrphelins().
     */
    @Test
    public void supprimerLesParametresOperationsOrphelins()
    {
        // Exécution de la méthode à tester
        journaldao.supprimerParametresOperationsOrphelins();
        ControleDonnees.verifierElements(journaldao.loadAllObjects(ParametreOperation.class), mapParam.get(1), mapParam.get(2),
            mapParam.get(3));
    }

    /**
     * Test de la méthode
     * fr.gouv.finances.lombok.journal.dao.JournalDao#findParametresOperations(fr.gouv.finances.lombok.journal.bean.OperationJournal).
     */
    @Test
    public void findParametresOperations()
    {
        // Exécution de la méthode à tester sur des paramètres non renseignés ou vides
        VerificationExecution.verifierException(ProgrammationException.class, () -> journaldao.findParametresOperations(null));
        VerificationExecution.verifierException(ProgrammationException.class, () -> journaldao.findParametresOperations(getJournal(10)));

        // Exécution de la méthode à tester sur des données inexistantes
        OperationJournal journal1000 = getJournal(1000);
        journal1000.setId(1000l);
        ControleDonnees.verifierElements(journaldao.findParametresOperations(journal1000));

        // Exécution de la méthode à tester sur toutes les données et vérification
        ControleDonnees.verifierElements(journaldao.findParametresOperations(mapJournal.get(1)), mapParam.get(1));
        ControleDonnees.verifierElements(journaldao.findParametresOperations(mapJournal.get(2)), mapParam.get(2));
        ControleDonnees.verifierElements(journaldao.findParametresOperations(mapJournal.get(3)), mapParam.get(3));
        ControleDonnees.verifierElements(journaldao.findParametresOperations(mapJournal.get(4)), mapParam.get(1), mapParam.get(2));
        ControleDonnees.verifierElements(journaldao.findParametresOperations(mapJournal.get(5)));
    }

    /**
     * Test de la méthode
     * fr.gouv.finances.lombok.journal.dao.JournalDao#findOperationsJournalParCriteresRecherche(fr.gouv.finances.lombok.journal.bean.CriteresRecherchesJournal).
     */
    @Test
    public void findOperationsJournalParCriteresRechercheVide()
    {
        // Critère non initialisé
        VerificationExecution.verifierException(ProgrammationException.class,
            () -> journaldao.findOperationsJournalParCriteresRecherche(null));
        // Absence de critère
        verifierJournaux(journaldao.findOperationsJournalParCriteresRecherche(new CriteresRecherchesJournal()), true, mapJournal.get(1),
            mapJournal.get(2), mapJournal.get(3), mapJournal.get(4), mapJournal.get(5));
    }

    /**
     * Test de la méthode
     * fr.gouv.finances.lombok.journal.dao.JournalDao#findOperationsJournalParCriteresRecherche(fr.gouv.finances.lombok.journal.bean.CriteresRecherchesJournal).
     */
    @Test
    public void findOperationsJournalParIdentifiantStructure()
    {
        // Recherche sur identifiant structure vide
        CriteresRecherchesJournal criteresRecherchesJournal = new CriteresRecherchesJournal();
        criteresRecherchesJournal.setIdentifiantStructure("   ");
        verifierJournaux(journaldao.findOperationsJournalParCriteresRecherche(criteresRecherchesJournal), true, mapJournal.get(1),
            mapJournal.get(2), mapJournal.get(3), mapJournal.get(4), mapJournal.get(5));

        // Recherche sur identifiant structure inexistant
        criteresRecherchesJournal = new CriteresRecherchesJournal();
        criteresRecherchesJournal.setIdentifiantStructure(VALEUR_INEXISTANTE);
        ControleDonnees.verifierElements(
            journaldao.findOperationsJournalParCriteresRecherche(criteresRecherchesJournal));

        // Recherche sur 1er identifiant structure
        criteresRecherchesJournal = new CriteresRecherchesJournal();
        criteresRecherchesJournal.setIdentifiantStructure(PREFIX_VALEUR_REFERENCE + 1);
        verifierJournaux(journaldao.findOperationsJournalParCriteresRecherche(criteresRecherchesJournal), true, mapJournal.get(1),
            mapJournal.get(2));

        // Recherche sur 2ème identifiant structure
        criteresRecherchesJournal = new CriteresRecherchesJournal();
        criteresRecherchesJournal.setIdentifiantStructure(PREFIX_VALEUR_REFERENCE + 2);
        verifierJournaux(journaldao.findOperationsJournalParCriteresRecherche(criteresRecherchesJournal), true, mapJournal.get(4),
            mapJournal.get(5));
    }

    /**
     * Test de la méthode
     * fr.gouv.finances.lombok.journal.dao.JournalDao#findOperationsJournalParCriteresRecherche(fr.gouv.finances.lombok.journal.bean.CriteresRecherchesJournal).
     */
    @Test
    public void findOperationsJournalParNatureOperation()
    {
        // Recherche sur une liste de nature opération non initialisée
        CriteresRecherchesJournal criteresRecherchesJournal = new CriteresRecherchesJournal();
        criteresRecherchesJournal.setNaturesOperations(null);
        verifierJournaux(journaldao.findOperationsJournalParCriteresRecherche(criteresRecherchesJournal), true, mapJournal.get(1),
            mapJournal.get(2), mapJournal.get(3), mapJournal.get(4), mapJournal.get(5));

        // Recherche sur une liste de nature opération vide
        criteresRecherchesJournal = new CriteresRecherchesJournal();
        criteresRecherchesJournal.setNaturesOperations(new ArrayList<>());
        verifierJournaux(journaldao.findOperationsJournalParCriteresRecherche(criteresRecherchesJournal), true, mapJournal.get(1),
            mapJournal.get(2), mapJournal.get(3), mapJournal.get(4), mapJournal.get(5));

        // Recherche sur une nature opération vide
        criteresRecherchesJournal = new CriteresRecherchesJournal();
        criteresRecherchesJournal.setNatureOperation("   ");
        verifierJournaux(journaldao.findOperationsJournalParCriteresRecherche(criteresRecherchesJournal), true, mapJournal.get(1),
            mapJournal.get(2), mapJournal.get(3), mapJournal.get(4), mapJournal.get(5));

        // Recherche sur nature opération inexistante
        criteresRecherchesJournal = new CriteresRecherchesJournal();
        criteresRecherchesJournal.setNatureOperation(VALEUR_INEXISTANTE);
        ControleDonnees.verifierElements(
            journaldao.findOperationsJournalParCriteresRecherche(criteresRecherchesJournal));

        // Recherche sur une 1ère nature opération unique
        criteresRecherchesJournal = new CriteresRecherchesJournal();
        criteresRecherchesJournal.setNatureOperation(PREFIX_VALEUR_REFERENCE + 11);
        verifierJournaux(journaldao.findOperationsJournalParCriteresRecherche(criteresRecherchesJournal), true, mapJournal.get(1),
            mapJournal.get(2));

        // Recherche sur une liste d'une seule nature opération
        criteresRecherchesJournal = new CriteresRecherchesJournal();
        criteresRecherchesJournal.setNaturesOperations(Arrays.asList(PREFIX_VALEUR_REFERENCE + 12));
        verifierJournaux(journaldao.findOperationsJournalParCriteresRecherche(criteresRecherchesJournal), true, mapJournal.get(3));

        // Recherche sur une liste de plusieurs natures opérations
        criteresRecherchesJournal = new CriteresRecherchesJournal();
        criteresRecherchesJournal.setNaturesOperations(Arrays.asList(PREFIX_VALEUR_REFERENCE + 12, PREFIX_VALEUR_REFERENCE + 13));
        verifierJournaux(journaldao.findOperationsJournalParCriteresRecherche(criteresRecherchesJournal), true, mapJournal.get(3),
            mapJournal.get(5));

        // Recherche sur une nature puis une liste de natures = juste la liste
        criteresRecherchesJournal = new CriteresRecherchesJournal();
        criteresRecherchesJournal.setNatureOperation(PREFIX_VALEUR_REFERENCE + 11);
        criteresRecherchesJournal.setNaturesOperations(Arrays.asList(PREFIX_VALEUR_REFERENCE + 12, PREFIX_VALEUR_REFERENCE + 13));
        verifierJournaux(journaldao.findOperationsJournalParCriteresRecherche(criteresRecherchesJournal), true, mapJournal.get(3),
            mapJournal.get(5));

        // Recherche sur une liste de natures puis une nature = agrégation des natures
        criteresRecherchesJournal = new CriteresRecherchesJournal();
        List<String> liste = new ArrayList<>();
        liste.add(PREFIX_VALEUR_REFERENCE + 12);
        liste.add(PREFIX_VALEUR_REFERENCE + 13);
        criteresRecherchesJournal.setNaturesOperations(liste);
        criteresRecherchesJournal.setNatureOperation(PREFIX_VALEUR_REFERENCE + 11);
        verifierJournaux(journaldao.findOperationsJournalParCriteresRecherche(criteresRecherchesJournal), true, mapJournal.get(1),
            mapJournal.get(2), mapJournal.get(3), mapJournal.get(5));
    }

    /**
     * Test de la méthode
     * fr.gouv.finances.lombok.journal.dao.JournalDao#findOperationsJournalParCriteresRecherche(fr.gouv.finances.lombok.journal.bean.CriteresRecherchesJournal).
     */
    @Test
    public void findOperationsJournalParCodeOperation()
    {
        // Recherche sur une liste de code opération non initialisée
        CriteresRecherchesJournal criteresRecherchesJournal = new CriteresRecherchesJournal();
        criteresRecherchesJournal.setCodesOperation(null);
        verifierJournaux(journaldao.findOperationsJournalParCriteresRecherche(criteresRecherchesJournal), true, mapJournal.get(1),
            mapJournal.get(2), mapJournal.get(3), mapJournal.get(4), mapJournal.get(5));

        // Recherche sur une liste de code opération vide
        criteresRecherchesJournal = new CriteresRecherchesJournal();
        criteresRecherchesJournal.setCodesOperation(new ArrayList<>());
        verifierJournaux(journaldao.findOperationsJournalParCriteresRecherche(criteresRecherchesJournal), true, mapJournal.get(1),
            mapJournal.get(2), mapJournal.get(3), mapJournal.get(4), mapJournal.get(5));

        // Recherche sur un code opération vide
        criteresRecherchesJournal = new CriteresRecherchesJournal();
        criteresRecherchesJournal.setCodeOperation("   ");
        verifierJournaux(journaldao.findOperationsJournalParCriteresRecherche(criteresRecherchesJournal), true, mapJournal.get(1),
            mapJournal.get(2), mapJournal.get(3), mapJournal.get(4), mapJournal.get(5));

        // Rercherche sur un code opération inexistant
        criteresRecherchesJournal = new CriteresRecherchesJournal();
        criteresRecherchesJournal.setCodeOperation(VALEUR_INEXISTANTE);
        ControleDonnees.verifierElements(
            journaldao.findOperationsJournalParCriteresRecherche(criteresRecherchesJournal));

        // Rercherche sur un seul code opération
        criteresRecherchesJournal = new CriteresRecherchesJournal();
        criteresRecherchesJournal.setCodeOperation(PREFIX_VALEUR_REFERENCE + 21);
        verifierJournaux(journaldao.findOperationsJournalParCriteresRecherche(criteresRecherchesJournal), true, mapJournal.get(1),
            mapJournal.get(2));

        // Recherche sur une liste d'un seul code opération
        criteresRecherchesJournal = new CriteresRecherchesJournal();
        criteresRecherchesJournal.setCodesOperation(Arrays.asList(PREFIX_VALEUR_REFERENCE + 22));
        verifierJournaux(journaldao.findOperationsJournalParCriteresRecherche(criteresRecherchesJournal), true, mapJournal.get(3));

        // Recherche sur une liste de plusieurs code opérations
        criteresRecherchesJournal = new CriteresRecherchesJournal();
        criteresRecherchesJournal.setCodesOperation(Arrays.asList(PREFIX_VALEUR_REFERENCE + 22, PREFIX_VALEUR_REFERENCE + 23));
        verifierJournaux(journaldao.findOperationsJournalParCriteresRecherche(criteresRecherchesJournal), true, mapJournal.get(3),
            mapJournal.get(5));

        // Recherche sur un code puis une liste de code = juste la liste
        criteresRecherchesJournal = new CriteresRecherchesJournal();
        criteresRecherchesJournal.setCodeOperation(PREFIX_VALEUR_REFERENCE + 21);
        criteresRecherchesJournal.setCodesOperation(Arrays.asList(PREFIX_VALEUR_REFERENCE + 22, PREFIX_VALEUR_REFERENCE + 23));
        verifierJournaux(journaldao.findOperationsJournalParCriteresRecherche(criteresRecherchesJournal), true, mapJournal.get(3),
            mapJournal.get(5));

        // Recherche sur une liste de codes puis un code = juste la liste
        criteresRecherchesJournal = new CriteresRecherchesJournal();
        criteresRecherchesJournal.setCodesOperation(Arrays.asList(PREFIX_VALEUR_REFERENCE + 22, PREFIX_VALEUR_REFERENCE + 23));
        criteresRecherchesJournal.setCodeOperation(PREFIX_VALEUR_REFERENCE + 11);
        verifierJournaux(journaldao.findOperationsJournalParCriteresRecherche(criteresRecherchesJournal), true, mapJournal.get(3),
            mapJournal.get(5));

        // Recherche sur une liste d'un code et un code = la liste d'un code
        criteresRecherchesJournal = new CriteresRecherchesJournal();
        criteresRecherchesJournal.setCodesOperation(Arrays.asList(PREFIX_VALEUR_REFERENCE + 22));
        criteresRecherchesJournal.setCodeOperation(PREFIX_VALEUR_REFERENCE + 11);
        verifierJournaux(journaldao.findOperationsJournalParCriteresRecherche(criteresRecherchesJournal), true, mapJournal.get(3));
    }

    /**
     * Test de la méthode
     * fr.gouv.finances.lombok.journal.dao.JournalDao#findOperationsJournalParCriteresRecherche(fr.gouv.finances.lombok.journal.bean.CriteresRecherchesJournal).
     */
    @Test
    public void findOperationsJournalParIdentifiant()
    {
        // Recherche sur identifiant utilisateur ou batch inexistant
        CriteresRecherchesJournal criteres = new CriteresRecherchesJournal();
        criteres.setIdentifiantUtilisateurOuBatch(VALEUR_INEXISTANTE);
        ControleDonnees.verifierElements(journaldao.findOperationsJournalParCriteresRecherche(criteres));

        // Recherche sur chaque identifiant utilisateur ou batch
        mapJournal.entrySet().stream().forEach(entry -> {
            CriteresRecherchesJournal criteresIdUtilisateurOuBatch = new CriteresRecherchesJournal();
            criteresIdUtilisateurOuBatch.setIdentifiantUtilisateurOuBatch(String.valueOf(entry.getKey()));
            verifierJournaux(journaldao.findOperationsJournalParCriteresRecherche(criteresIdUtilisateurOuBatch), true,
                mapJournal.get(entry.getKey()));
        });

        // Recherche sur chaque date et heure d'opération
        mapJournal.entrySet().stream().forEach(entry -> {
            CriteresRecherchesJournal criteresDateHeureOperation = new CriteresRecherchesJournal();
            criteresDateHeureOperation.setDateHeureOperation(entry.getValue().getDateHeureOperation());
            verifierJournaux(journaldao.findOperationsJournalParCriteresRecherche(criteresDateHeureOperation), true,
                mapJournal.get(entry.getKey()));
        });
    }

    /**
     * Test de la méthode
     * fr.gouv.finances.lombok.journal.dao.JournalDao#findOperationsJournalParCriteresRecherche(fr.gouv.finances.lombok.journal.bean.CriteresRecherchesJournal).
     */
    @Test
    public void findOperationsJournalParDate()
    {
        CriteresRecherchesJournal criteresRecherchesJournal = new CriteresRecherchesJournal();
        criteresRecherchesJournal.setDateMax(TemporaliteUtils.getTimestamp(2018, 3, 2));
        verifierJournaux(journaldao.findOperationsJournalParCriteresRecherche(criteresRecherchesJournal), true, mapJournal.get(1),
            mapJournal.get(2), mapJournal.get(3));

        criteresRecherchesJournal = new CriteresRecherchesJournal();
        criteresRecherchesJournal.setDateMin(TemporaliteUtils.getTimestamp(2018, 3, 2));
        verifierJournaux(journaldao.findOperationsJournalParCriteresRecherche(criteresRecherchesJournal), true, mapJournal.get(2),
            mapJournal.get(3), mapJournal.get(4), mapJournal.get(5));

        criteresRecherchesJournal = new CriteresRecherchesJournal();
        criteresRecherchesJournal.setDateJJMMAAAA(TemporaliteUtils.getTimestamp(2018, 3, 2));
        verifierJournaux(journaldao.findOperationsJournalParCriteresRecherche(criteresRecherchesJournal), true, mapJournal.get(2),
            mapJournal.get(3));
    }

    /**
     * Test de la méthode
     * fr.gouv.finances.lombok.journal.dao.JournalDao#findOperationsJournalParCriteresRecherche(fr.gouv.finances.lombok.journal.bean.CriteresRecherchesJournal).
     */
    @Test
    public void findOperationsJournalParParamOper()
    {
        // Pas de nom, pas de valeur = pas de critère
        CriteresRecherchesJournal criteresRecherchesJournal = new CriteresRecherchesJournal();
        criteresRecherchesJournal.setValeurParamOper(null);
        criteresRecherchesJournal.setNomParamOper(null);
        verifierJournaux(journaldao.findOperationsJournalParCriteresRecherche(criteresRecherchesJournal), true, mapJournal.get(1),
            mapJournal.get(2), mapJournal.get(3), mapJournal.get(4), mapJournal.get(5));

        // Vide = pas de critère
        criteresRecherchesJournal = new CriteresRecherchesJournal();
        criteresRecherchesJournal.setValeurParamOper("");
        criteresRecherchesJournal.setNomParamOper("");
        verifierJournaux(journaldao.findOperationsJournalParCriteresRecherche(criteresRecherchesJournal), true, mapJournal.get(1),
            mapJournal.get(2), mapJournal.get(3), mapJournal.get(4), mapJournal.get(5));

        // Espace = pas de critère
        criteresRecherchesJournal = new CriteresRecherchesJournal();
        criteresRecherchesJournal.setValeurParamOper(" ");
        criteresRecherchesJournal.setNomParamOper(" ");
        verifierJournaux(journaldao.findOperationsJournalParCriteresRecherche(criteresRecherchesJournal), true, mapJournal.get(1),
            mapJournal.get(2), mapJournal.get(3), mapJournal.get(4), mapJournal.get(5));

        // Valeur non numérique = rien
        criteresRecherchesJournal = new CriteresRecherchesJournal();
        criteresRecherchesJournal.setValeurParamOper(VALEUR_INEXISTANTE);
        criteresRecherchesJournal.setNomParamOper(VALEUR_INEXISTANTE);
        ControleDonnees.verifierElements(
            journaldao.findOperationsJournalParCriteresRecherche(criteresRecherchesJournal));
        criteresRecherchesJournal = new CriteresRecherchesJournal();
        criteresRecherchesJournal.setValeurParamOperMax(VALEUR_INEXISTANTE);
        criteresRecherchesJournal.setNomParamOper(VALEUR_INEXISTANTE);
        ControleDonnees.verifierElements(
            journaldao.findOperationsJournalParCriteresRecherche(criteresRecherchesJournal));
        criteresRecherchesJournal = new CriteresRecherchesJournal();
        criteresRecherchesJournal.setValeurParamOperMin(VALEUR_INEXISTANTE);
        criteresRecherchesJournal.setNomParamOper(VALEUR_INEXISTANTE);
        ControleDonnees.verifierElements(
            journaldao.findOperationsJournalParCriteresRecherche(criteresRecherchesJournal));

        // Valeur inexistante
        criteresRecherchesJournal = new CriteresRecherchesJournal();
        criteresRecherchesJournal.setValeurParamOper("42");
        criteresRecherchesJournal.setNomParamOper(VALEUR_INEXISTANTE);
        ControleDonnees.verifierElements(
            journaldao.findOperationsJournalParCriteresRecherche(criteresRecherchesJournal));

        // Par de nom = pas de critère
        IntStream.rangeClosed(1, 5).forEach(i -> {
            CriteresRecherchesJournal crit = new CriteresRecherchesJournal();
            crit.setValeurParamOper(String.valueOf(i));
            crit.setNomParamOper(null);
            verifierJournaux(journaldao.findOperationsJournalParCriteresRecherche(crit), true, mapJournal.get(1), mapJournal.get(2),
                mapJournal.get(3), mapJournal.get(4), mapJournal.get(5));
            crit = new CriteresRecherchesJournal();
            crit.setValeurParamOperMax(String.valueOf(i));
            crit.setNomParamOper(null);
            verifierJournaux(journaldao.findOperationsJournalParCriteresRecherche(crit), true, mapJournal.get(1), mapJournal.get(2),
                mapJournal.get(3), mapJournal.get(4), mapJournal.get(5));
            crit = new CriteresRecherchesJournal();
            crit.setValeurParamOperMin(String.valueOf(i));
            crit.setNomParamOper(null);
            verifierJournaux(journaldao.findOperationsJournalParCriteresRecherche(crit), true, mapJournal.get(1), mapJournal.get(2),
                mapJournal.get(3), mapJournal.get(4), mapJournal.get(5));
        });

        // Nom sans valeur
        criteresRecherchesJournal = new CriteresRecherchesJournal();
        criteresRecherchesJournal.setValeurParamOper(null);
        criteresRecherchesJournal.setNomParamOper(ParametreOperationUtils.PREFIX_NOM + 1);
        verifierJournaux(journaldao.findOperationsJournalParCriteresRecherche(criteresRecherchesJournal), true, mapJournal.get(1),
            mapJournal.get(4));
        criteresRecherchesJournal = new CriteresRecherchesJournal();
        criteresRecherchesJournal.setValeurParamOper(null);
        criteresRecherchesJournal.setNomParamOper(ParametreOperationUtils.PREFIX_NOM + 2);
        verifierJournaux(journaldao.findOperationsJournalParCriteresRecherche(criteresRecherchesJournal), true, mapJournal.get(2),
            mapJournal.get(4));
        criteresRecherchesJournal = new CriteresRecherchesJournal();
        criteresRecherchesJournal.setValeurParamOper(null);
        criteresRecherchesJournal.setNomParamOper(ParametreOperationUtils.PREFIX_NOM + 3);
        ControleDonnees.verifierElements(
            journaldao.findOperationsJournalParCriteresRecherche(criteresRecherchesJournal));

        // Nom + valeur
        criteresRecherchesJournal = new CriteresRecherchesJournal();
        criteresRecherchesJournal.setValeurParamOper("1");
        criteresRecherchesJournal.setNomParamOper(ParametreOperationUtils.PREFIX_NOM + 1);
        verifierJournaux(journaldao.findOperationsJournalParCriteresRecherche(criteresRecherchesJournal), true, mapJournal.get(1),
            mapJournal.get(4));
        criteresRecherchesJournal = new CriteresRecherchesJournal();
        criteresRecherchesJournal.setValeurParamOper("2");
        criteresRecherchesJournal.setNomParamOper(ParametreOperationUtils.PREFIX_NOM + 2);
        verifierJournaux(journaldao.findOperationsJournalParCriteresRecherche(criteresRecherchesJournal), true, mapJournal.get(2),
            mapJournal.get(4));
        criteresRecherchesJournal = new CriteresRecherchesJournal();
        criteresRecherchesJournal.setValeurParamOper("3");
        criteresRecherchesJournal.setNomParamOper(null);
        verifierJournaux(journaldao.findOperationsJournalParCriteresRecherche(criteresRecherchesJournal), true, mapJournal.get(1),
            mapJournal.get(2), mapJournal.get(3), mapJournal.get(4), mapJournal.get(5));

        // Cas des opérations de journalisation sans paramètre
        final CriteresRecherchesJournal crit1 = new CriteresRecherchesJournal();
        IntStream.rangeClosed(5, 7).forEach(i -> {
            crit1.setValeurParamOper(String.valueOf(i));
            crit1.setNomParamOper(ParametreOperationUtils.PREFIX_NOM + i);
            ControleDonnees.verifierElements(journaldao.findOperationsJournalParCriteresRecherche(crit1));
        });

        // Toutes les valeurs max
        criteresRecherchesJournal = new CriteresRecherchesJournal();
        criteresRecherchesJournal.setValeurParamOperMax("1");
        criteresRecherchesJournal.setNomParamOper(ParametreOperationUtils.PREFIX_NOM + 1);
        verifierJournaux(journaldao.findOperationsJournalParCriteresRecherche(criteresRecherchesJournal), true, mapJournal.get(1),
            mapJournal.get(4));
        criteresRecherchesJournal = new CriteresRecherchesJournal();
        criteresRecherchesJournal.setValeurParamOperMax("1");
        criteresRecherchesJournal.setNomParamOper(ParametreOperationUtils.PREFIX_NOM + 2);
        ControleDonnees.verifierElements(
            journaldao.findOperationsJournalParCriteresRecherche(criteresRecherchesJournal));
        criteresRecherchesJournal = new CriteresRecherchesJournal();
        criteresRecherchesJournal.setValeurParamOper("2");
        criteresRecherchesJournal.setNomParamOper(ParametreOperationUtils.PREFIX_NOM + 2);
        verifierJournaux(journaldao.findOperationsJournalParCriteresRecherche(criteresRecherchesJournal), true, mapJournal.get(2),
            mapJournal.get(4));
        criteresRecherchesJournal = new CriteresRecherchesJournal();
        criteresRecherchesJournal.setValeurParamOper("3");
        criteresRecherchesJournal.setNomParamOper(ParametreOperationUtils.PREFIX_NOM + 3);
        ControleDonnees.verifierElements(
            journaldao.findOperationsJournalParCriteresRecherche(criteresRecherchesJournal));

        // Toutes les valeurs min
        criteresRecherchesJournal = new CriteresRecherchesJournal();
        criteresRecherchesJournal.setValeurParamOperMin("1");
        criteresRecherchesJournal.setNomParamOper(ParametreOperationUtils.PREFIX_NOM + 1);
        verifierJournaux(journaldao.findOperationsJournalParCriteresRecherche(criteresRecherchesJournal), true, mapJournal.get(1),
            mapJournal.get(4));
        IntStream.rangeClosed(2, 7).forEach(indice -> {
            CriteresRecherchesJournal crit = new CriteresRecherchesJournal();
            crit.setValeurParamOperMin(String.valueOf(indice));
            crit.setNomParamOper(ParametreOperationUtils.PREFIX_NOM + 1);
            ControleDonnees.verifierElements(journaldao.findOperationsJournalParCriteresRecherche(crit));
        });

        // Min et Max
        criteresRecherchesJournal = new CriteresRecherchesJournal();
        criteresRecherchesJournal.setValeurParamOperMax("1");
        criteresRecherchesJournal.setValeurParamOperMin("1");
        criteresRecherchesJournal.setNomParamOper(ParametreOperationUtils.PREFIX_NOM + 1);
        verifierJournaux(journaldao.findOperationsJournalParCriteresRecherche(criteresRecherchesJournal), true, mapJournal.get(1),
            mapJournal.get(4));
        criteresRecherchesJournal = new CriteresRecherchesJournal();
        criteresRecherchesJournal.setValeurParamOperMax("2");
        criteresRecherchesJournal.setValeurParamOperMin("2");
        criteresRecherchesJournal.setNomParamOper(ParametreOperationUtils.PREFIX_NOM + 2);
        verifierJournaux(journaldao.findOperationsJournalParCriteresRecherche(criteresRecherchesJournal), true, mapJournal.get(2),
            mapJournal.get(4));
    }

    /**
     * Test de la méthode
     * fr.gouv.finances.lombok.journal.dao.JournalDao#findOperationsJournalParCriteresRecherche(fr.gouv.finances.lombok.journal.bean.CriteresRecherchesJournal).
     */
    @Test
    public void findOperationsJournalParApurement()
    {
        // Recherche apuré
        CriteresRecherchesJournal criteresRecherchesJournal = new CriteresRecherchesJournal();
        criteresRecherchesJournal.setApurementPossible(true);
        verifierJournaux(journaldao.findOperationsJournalParCriteresRecherche(criteresRecherchesJournal), true, mapJournal.get(1),
            mapJournal.get(4), mapJournal.get(5));

        // Recherche non apuré
        criteresRecherchesJournal = new CriteresRecherchesJournal();
        criteresRecherchesJournal.setApurementPossible(false);
        verifierJournaux(journaldao.findOperationsJournalParCriteresRecherche(criteresRecherchesJournal), true, mapJournal.get(2));
    }

    /**
     * Test de la méthode
     * fr.gouv.finances.lombok.journal.dao.JournalDao#findOperationsJournalParIdentifiantMetierObjOperation(fr.gouv.finances.lombok.journal.bean.IdentifiantsMetierObjOperation).
     */
    @Test
    public void findOperationsJournalParIdentifiantMetierObjOperation()
    {
        VerificationExecution.verifierException(ProgrammationException.class,
            () -> journaldao.findOperationsJournalParIdentifiantMetierObjOperation(null));
        verifierJournaux(
            journaldao.findOperationsJournalParIdentifiantMetierObjOperation(new IdentifiantsMetierObjOperation()), true, mapJournal.get(1),
            mapJournal.get(2), mapJournal.get(3), mapJournal.get(4), mapJournal.get(5));

        IdentifiantsMetierObjOperation identifiantsMetierObjOperation = new IdentifiantsMetierObjOperation();
        identifiantsMetierObjOperation.setValeurIdentifiantMetier1(PREFIX_VALEUR_ID_METIER + 11);
        verifierIdentifiantMetierObjOperation(identifiantsMetierObjOperation, 1);

        identifiantsMetierObjOperation = new IdentifiantsMetierObjOperation();
        identifiantsMetierObjOperation.setValeurIdentifiantMetier2(PREFIX_VALEUR_ID_METIER + 12);
        verifierIdentifiantMetierObjOperation(identifiantsMetierObjOperation, 1);

        identifiantsMetierObjOperation = new IdentifiantsMetierObjOperation();
        identifiantsMetierObjOperation.setValeurIdentifiantMetier3(PREFIX_VALEUR_ID_METIER + 13);
        verifierIdentifiantMetierObjOperation(identifiantsMetierObjOperation, 1);

        identifiantsMetierObjOperation = new IdentifiantsMetierObjOperation();
        identifiantsMetierObjOperation.setValeurIdentifiantMetier4(PREFIX_VALEUR_ID_METIER + 14);
        verifierIdentifiantMetierObjOperation(identifiantsMetierObjOperation, 1);

        identifiantsMetierObjOperation = new IdentifiantsMetierObjOperation();
        identifiantsMetierObjOperation.setValeurIdentifiantMetier5(PREFIX_VALEUR_ID_METIER + 15);
        verifierIdentifiantMetierObjOperation(identifiantsMetierObjOperation, 1);

        identifiantsMetierObjOperation = new IdentifiantsMetierObjOperation();
        identifiantsMetierObjOperation.setValeurIdentifiantMetier1(PREFIX_VALEUR_ID_METIER + 21);
        verifierIdentifiantMetierObjOperation(identifiantsMetierObjOperation, 2);

        identifiantsMetierObjOperation = new IdentifiantsMetierObjOperation();
        identifiantsMetierObjOperation.setValeurIdentifiantMetier2(PREFIX_VALEUR_ID_METIER + 22);
        verifierIdentifiantMetierObjOperation(identifiantsMetierObjOperation, 2);

        identifiantsMetierObjOperation = new IdentifiantsMetierObjOperation();
        identifiantsMetierObjOperation.setValeurIdentifiantMetier3(PREFIX_VALEUR_ID_METIER + 23);
        verifierIdentifiantMetierObjOperation(identifiantsMetierObjOperation, 2);

        identifiantsMetierObjOperation = new IdentifiantsMetierObjOperation();
        identifiantsMetierObjOperation.setValeurIdentifiantMetier4(PREFIX_VALEUR_ID_METIER + 24);
        verifierIdentifiantMetierObjOperation(identifiantsMetierObjOperation, 2);

        identifiantsMetierObjOperation = new IdentifiantsMetierObjOperation();
        identifiantsMetierObjOperation.setValeurIdentifiantMetier5(PREFIX_VALEUR_ID_METIER + 25);
        verifierIdentifiantMetierObjOperation(identifiantsMetierObjOperation, 2);

        identifiantsMetierObjOperation = new IdentifiantsMetierObjOperation();
        identifiantsMetierObjOperation.setLibelleIdentifiantMetier1(PREFIX_LIBELLE_ID_METIER + 11);
        verifierIdentifiantMetierObjOperation(identifiantsMetierObjOperation, 1);

        identifiantsMetierObjOperation = new IdentifiantsMetierObjOperation();
        identifiantsMetierObjOperation.setLibelleIdentifiantMetier2(PREFIX_LIBELLE_ID_METIER + 12);
        verifierIdentifiantMetierObjOperation(identifiantsMetierObjOperation, 1);

        identifiantsMetierObjOperation = new IdentifiantsMetierObjOperation();
        identifiantsMetierObjOperation.setLibelleIdentifiantMetier3(PREFIX_LIBELLE_ID_METIER + 13);
        verifierIdentifiantMetierObjOperation(identifiantsMetierObjOperation, 1);

        identifiantsMetierObjOperation = new IdentifiantsMetierObjOperation();
        identifiantsMetierObjOperation.setLibelleIdentifiantMetier4(PREFIX_LIBELLE_ID_METIER + 14);
        verifierIdentifiantMetierObjOperation(identifiantsMetierObjOperation, 1);

        identifiantsMetierObjOperation = new IdentifiantsMetierObjOperation();
        identifiantsMetierObjOperation.setLibelleIdentifiantMetier5(PREFIX_LIBELLE_ID_METIER + 15);
        verifierIdentifiantMetierObjOperation(identifiantsMetierObjOperation, 1);

        identifiantsMetierObjOperation = new IdentifiantsMetierObjOperation();
        identifiantsMetierObjOperation.setLibelleIdentifiantMetier1(PREFIX_LIBELLE_ID_METIER + 21);
        verifierIdentifiantMetierObjOperation(identifiantsMetierObjOperation, 2);

        identifiantsMetierObjOperation = new IdentifiantsMetierObjOperation();
        identifiantsMetierObjOperation.setLibelleIdentifiantMetier2(PREFIX_LIBELLE_ID_METIER + 22);
        verifierIdentifiantMetierObjOperation(identifiantsMetierObjOperation, 2);

        identifiantsMetierObjOperation = new IdentifiantsMetierObjOperation();
        identifiantsMetierObjOperation.setLibelleIdentifiantMetier3(PREFIX_LIBELLE_ID_METIER + 23);
        verifierIdentifiantMetierObjOperation(identifiantsMetierObjOperation, 2);

        identifiantsMetierObjOperation = new IdentifiantsMetierObjOperation();
        identifiantsMetierObjOperation.setLibelleIdentifiantMetier4(PREFIX_LIBELLE_ID_METIER + 24);
        verifierIdentifiantMetierObjOperation(identifiantsMetierObjOperation, 2);

        identifiantsMetierObjOperation = new IdentifiantsMetierObjOperation();
        identifiantsMetierObjOperation.setLibelleIdentifiantMetier5(PREFIX_LIBELLE_ID_METIER + 25);
        verifierIdentifiantMetierObjOperation(identifiantsMetierObjOperation, 2);
    }

    /**
     * Méthode permettant de vérifier l'identifiant métier
     *
     * @param identifiantsMetierObjOperation identifiant à vérifier
     * @param indice indice de l'identifiant attendu
     */
    private void verifierIdentifiantMetierObjOperation(IdentifiantsMetierObjOperation identifiantsMetierObjOperation, int indice)
    {
        List<OperationJournal> journaux = journaldao.findOperationsJournalParIdentifiantMetierObjOperation(identifiantsMetierObjOperation);

        switch (indice)
        {
            case 1:
                verifierJournaux(journaux, true, mapJournal.get(1), mapJournal.get(2));
                break;
            default:
                verifierJournaux(journaux, true, mapJournal.get(4), mapJournal.get(5));
        }
    }

    /**
     * Test de la méthode
     * fr.gouv.finances.lombok.journal.dao.JournalDao#findUneOperationJournalParIdUtilEtDateHeure(java.lang.String,
     * java.util.Date).
     */
    @Test
    public void findUneOperationJournalParIdUtilEtDateHeure()
    {
        // Exécution de la méthode à tester sur des paramètres non renseignés ou vides
        assertNull(journaldao.findUneOperationJournalParIdUtilEtDateHeure(null, null));
        assertNull(journaldao.findUneOperationJournalParIdUtilEtDateHeure(
            mapJournal.get(1).getIdentifiantStructure(), mapJournal.get(2).getDateHeureOperation()));

        // Exécution de la méthode à tester sur toutes les données et vérification
        mapJournal.entrySet().stream().forEach(entry -> verifierJournal(journaldao.findUneOperationJournalParIdUtilEtDateHeure(
            entry.getValue().getIdentifiantUtilisateurOuBatch(), entry.getValue().getDateHeureOperation()), true, entry.getValue()));
    }

    /**
     * Test de la méthode fr.gouv.finances.lombok.journal.dao.JournalDao#findUneOperationParId(java.lang.Long).
     */
    @Test
    public void findUneOperationParId()
    {
        // Exécution de la méthode à tester sur des paramètres non renseignés ou vides
        assertNull(journaldao.findUneOperationParId(null));
        assertNull(journaldao.findUneOperationParId(10000L));

        // Exécution de la méthode à tester sur chaque élément en base et vérification
        mapJournal.entrySet().stream()
            .forEach(entry -> verifierJournal(journaldao.findUneOperationParId(entry.getValue().getId()), true, entry.getValue()));
    }

    /**
     * Test de la méthode
     * fr.gouv.finances.lombok.journal.dao.JournalDao#initialiseParametresOperation(fr.gouv.finances.lombok.journal.bean.OperationJournal).
     */
    @Test
    public void initialiseParametresOperation()
    {
        // Recherche sur une opération de journalisation non initialisée ou non persistée
        journaldao.initialiseParametresOperation(null);
        journaldao.initialiseParametresOperation(getJournal(10));

        // Chargement des paramètres d'opération de journalisation
        transactionTemplate.execute(status -> {
            // Chargement des opérations de journalisation, sans charger les paramètres, et vérification
            List<OperationJournal> journaux = journaldao.loadAllObjects(OperationJournal.class);
            verifierJournaux(journaux, false, mapJournal.get(1), mapJournal.get(2), mapJournal.get(3), mapJournal.get(4),
                mapJournal.get(5));

            // Chargement des paramètres
            journaux.stream().forEach(j -> journaldao.initialiseParametresOperation(j));

            // Vérification
            verifierJournaux(journaux, true, mapJournal.get(1), mapJournal.get(2), mapJournal.get(3), mapJournal.get(4), mapJournal.get(5));

            return null;
        });
    }

    /**
     * Test de la méthode fr.gouv.finances.lombok.journal.dao.JournalDao#findToutesLesOperationsJournal().
     */
    @Test
    public void findToutesLesOperationsJournal()
    {
        // Récupération de tous les journaux (sans leurs paramètres), en mode curseur
        List<OperationJournal> journaux = transactionTemplate.execute(j -> getOperationJournal(journaldao.findAllOperationsJournal()));

        // Vérification des éléments récupérés
        verifierJournaux(journaux, false, mapJournal.get(1), mapJournal.get(2), mapJournal.get(3), mapJournal.get(4), mapJournal.get(5));
    }

    /**
     * Test de la méthode
     * fr.gouv.finances.lombok.journal.dao.JournalDao#findOperationsPerimeesAUneDatePourUneDureeDeRetention(java.util.Date,
     * java.lang.String, int).
     */
    @Test
    public void findOperationsPerimeesAUneDatePourUneDureeDeRetention()
    {
        // Type de conservation inexistant
        mapJournal.entrySet().stream().forEach(entry -> ControleDonnees.verifierElements(
            transactionTemplate.execute(status -> getOperationJournal(journaldao.findOperationsPerimeesAUneDatePourUneDureeDeRetention(
                TemporaliteUtils.ajouterJour(entry.getValue().getDateHeureOperation(), 2), VALEUR_INEXISTANTE, 1)))));

        // Journal 1 OK
        verifierJournaux(
            transactionTemplate.execute(status -> getOperationJournal(journaldao.findOperationsPerimeesAUneDatePourUneDureeDeRetention(
                TemporaliteUtils.ajouterJour(mapJournal.get(1).getDateHeureOperation(), 2), TYPE_CONSERVATION_1, 1))),
            false, mapJournal.get(1));

        // Journal 1 KO à cause du type de conservation
        ControleDonnees.verifierElements(
            transactionTemplate.execute(status -> getOperationJournal(journaldao.findOperationsPerimeesAUneDatePourUneDureeDeRetention(
                TemporaliteUtils.ajouterJour(mapJournal.get(1).getDateHeureOperation(), 2), TYPE_CONSERVATION_2, 1))));

        // Journal 1 KO à cause de la date
        ControleDonnees.verifierElements(
            transactionTemplate.execute(status -> getOperationJournal(journaldao.findOperationsPerimeesAUneDatePourUneDureeDeRetention(
                mapJournal.get(1).getDateHeureOperation(), TYPE_CONSERVATION_1, 1))));

        // Journal 2 KO à cause de l'apurement
        ControleDonnees.verifierElements(
            transactionTemplate.execute(status -> getOperationJournal(journaldao.findOperationsPerimeesAUneDatePourUneDureeDeRetention(
                TemporaliteUtils.ajouterJour(mapJournal.get(2).getDateHeureOperation(), 2), TYPE_CONSERVATION_2, 1))));

        // Journal 3 OK
        verifierJournaux(
            transactionTemplate.execute(status -> getOperationJournal(journaldao.findOperationsPerimeesAUneDatePourUneDureeDeRetention(
                TemporaliteUtils.ajouterJour(mapJournal.get(3).getDateHeureOperation(), 2), TYPE_CONSERVATION_2, 1))),
            false, mapJournal.get(3));
    }

    /**
     * Méthode permettant de récupérer toutes les opérations de journalisation à partir d'un itérateur en mode curseur.
     *
     * @param scrollIterator itérateur en mode curseur
     * @return toutes les opérations de journalisation que permet de récupérer l'itérateur
     */
    private List<OperationJournal> getOperationJournal(ScrollIterator scrollIterator)
    {
        // Récupération des documents en mode curseur (possible par l'encapsulation dans une transaction)
        List<OperationJournal> journaux = new ArrayList<>();
        if (scrollIterator != null)
        {
            while (scrollIterator.hasNext())
            {
                journaux.add((OperationJournal) scrollIterator.nextObjetMetier());
            }
            // Fermeture du curseur après parcours de tous les éléments
            scrollIterator.close();
        }

        // Retour de tous les documents trouvés
        return journaux;
    }

    /**
     * Méthode permettant de vérifier des opérations de journalisation par rapport à celles attendues.
     *
     * @param journaux opérations de journalisation à vérifier
     * @param isParametreCharge les paramètres doivent-ils être chargés ?
     * @param journauxAttendus opérations de journalisation attendues
     */
    private void verifierJournaux(Collection<OperationJournal> journaux, boolean isParametreCharge, OperationJournal... journauxAttendus)
    {
        ControleDonnees.verifierElements(journaux, journauxAttendus);
        verifierParametres(journaux, isParametreCharge);
    }

    /**
     * Méthode permettant de vérifier une opération de journalisation par rapport à celle attendue.
     *
     * @param journal opération de journalisation à vérifier
     * @param isParametreCharge les paramètres doivent-ils être chargés ?
     * @param journalAttendu opération de journalisation attendue
     */
    private void verifierJournal(OperationJournal journal, boolean isParametreCharge, OperationJournal journalAttendu)
    {
        assertEquals(journalAttendu, journal);
        verifierParametres(journal, isParametreCharge);
    }

    /**
     * Méthode permettant de vérifier les paramètres d'opération de journalisation.
     *
     * @param journaux opérations de journalisation dont il faut vérifier les paramètres
     * @param isParametreCharge les paramètres doivent-ils être chargés ?
     */
    private void verifierParametres(Collection<OperationJournal> journaux, boolean isParametreCharge)
    {
        journaux.stream().forEach(j -> verifierParametres(j, isParametreCharge));
    }

    /**
     * Méthode permettant de vérifier les paramètres d'opération de journalisation.
     *
     * @param journal opération de journalisation dont il faut vérifier les paramètres
     * @param isParametreCharge les paramètres doivent-ils être chargés ?
     */
    private void verifierParametres(OperationJournal journal, boolean isParametreCharge)
    {
        switch (Integer.parseInt(journal.getIdentifiantUtilisateurOuBatch()))
        {
            case 1:
                verifierParam(journal, isParametreCharge, mapParam.get(1));
                break;
            case 2:
                verifierParam(journal, isParametreCharge, mapParam.get(2));
                break;
            case 3:
                verifierParam(journal, isParametreCharge, mapParam.get(3));
                break;
            case 4:
                verifierParam(journal, isParametreCharge, mapParam.get(1), mapParam.get(2));
                break;
            case 5:
                verifierParam(journal, isParametreCharge);
                break;
            default:
                fail(journal.getIdentifiantUtilisateurOuBatch() + " : identifiant non pris en compte");
        }
    }

    /**
     * Méthode permettant de vérifier les paramètres d'opération de journalisation.
     *
     * @param journal opération de journalisation dont il faut vérifier les paramètres
     * @param isParametreCharge les paramètres doivent-ils être chargés ?
     * @param parametresOperation paramètres attendus
     */
    private void verifierParam(OperationJournal operationJournal, boolean isParametreCharge, ParametreOperation... parametresOperation)
    {
        if (!isParametreCharge)
        {
            assertFalse("Paramètre(s) de journalisation chargé(s) à tort", journaldao.isCharged(operationJournal.getLesParamOperation()));
        }
        else
        {
            assertTrue("Paramètre(s) de journalisation non chargé(s)", journaldao.isCharged(operationJournal.getLesParamOperation()));
            ControleDonnees.verifierElements(operationJournal.getLesParamOperation(), parametresOperation);
        }
    }

    /**
     * Méthode permettant de générer une opération de journalisation.
     *
     * @param indice indice permettant d'instaurer de la généricité
     * @return l'opération de journalisation générée
     */
    private OperationJournal getJournal(Integer indice)
    {
        OperationJournal operationJournal = new OperationJournal();
        // L'identifiant utilisateur (ou batch) est l'indice du journal
        operationJournal.setIdentifiantUtilisateurOuBatch(String.valueOf(indice));
        // Le jour est l'indice du journal, sauf après 31 c'est fixé à 31
        int jour = 31;
        if (indice <= jour)
        {
            jour = indice;
        }
        operationJournal.setDateHeureOperation(TemporaliteUtils.getTimestamp(2018, 3, jour));

        switch (indice)
        {
            case 1:
                operationJournal.setIdentifiantStructure(PREFIX_VALEUR_REFERENCE + 1);
                operationJournal.setNatureOperation(PREFIX_VALEUR_REFERENCE + 11);
                operationJournal.setCodeOperation(PREFIX_VALEUR_REFERENCE + 21);
                operationJournal.setUnIdentifiantsMetierObjOperation(getIdentifiantsMetierObjOperation(1));
                operationJournal.setApurementPossible(true);
                operationJournal.setTypeDureeDeConservation(TYPE_CONSERVATION_1);
                break;
            case 2:
                operationJournal.setIdentifiantStructure(PREFIX_VALEUR_REFERENCE + 1);
                operationJournal.setNatureOperation(PREFIX_VALEUR_REFERENCE + 11);
                operationJournal.setCodeOperation(PREFIX_VALEUR_REFERENCE + 21);
                operationJournal.setUnIdentifiantsMetierObjOperation(getIdentifiantsMetierObjOperation(1));
                operationJournal.setApurementPossible(false);
                operationJournal.setTypeDureeDeConservation(TYPE_CONSERVATION_2);
                break;
            case 3:
                operationJournal.setNatureOperation(PREFIX_VALEUR_REFERENCE + 12);
                operationJournal.setCodeOperation(PREFIX_VALEUR_REFERENCE + 22);
                operationJournal.setTypeDureeDeConservation(TYPE_CONSERVATION_2);
                break;
            case 4:
                operationJournal.setIdentifiantStructure(PREFIX_VALEUR_REFERENCE + 2);
                operationJournal.setUnIdentifiantsMetierObjOperation(getIdentifiantsMetierObjOperation(2));
                operationJournal.setApurementPossible(true);
                break;
            default:
                operationJournal.setIdentifiantStructure(PREFIX_VALEUR_REFERENCE + 2);
                operationJournal.setNatureOperation(PREFIX_VALEUR_REFERENCE + 13);
                operationJournal.setCodeOperation(PREFIX_VALEUR_REFERENCE + 23);
                operationJournal.setUnIdentifiantsMetierObjOperation(getIdentifiantsMetierObjOperation(2));
                operationJournal.setApurementPossible(true);
        }

        /**
         * operation : 1 -> param1 2 -> param2 3 -> param3 4 -> param1 + param2 5 -> /
         */
        switch (indice)
        {
            case 1:
                operationJournal.setLesParamOperation(IntStream.rangeClosed(1, 1)
                    .mapToObj(mapParam::get).collect(Collectors.toSet()));
                break;
            case 2:
                operationJournal.setLesParamOperation(IntStream.rangeClosed(2, 2)
                    .mapToObj(mapParam::get).collect(Collectors.toSet()));
                break;
            case 3:
                operationJournal.setLesParamOperation(IntStream.rangeClosed(3, 3)
                    .mapToObj(mapParam::get).collect(Collectors.toSet()));
                break;
            case 4:
                operationJournal.setLesParamOperation(IntStream.rangeClosed(1, 2)
                    .mapToObj(mapParam::get).collect(Collectors.toSet()));
                break;
            default:
        }

        return operationJournal;
    }

    /**
     * Méthode permettant de générer un identifiant métier.
     *
     * @param indice indice permettant d'instaurer de la généricité
     * @return l'identifiant métier
     */
    private IdentifiantsMetierObjOperation getIdentifiantsMetierObjOperation(Integer indice)
    {
        IdentifiantsMetierObjOperation identifiantsMetierObjOperation = new IdentifiantsMetierObjOperation();
        identifiantsMetierObjOperation.setValeurIdentifiantMetier1(PREFIX_VALEUR_ID_METIER + (10 * indice + 1));
        identifiantsMetierObjOperation.setValeurIdentifiantMetier2(PREFIX_VALEUR_ID_METIER + (10 * indice + 2));
        identifiantsMetierObjOperation.setValeurIdentifiantMetier3(PREFIX_VALEUR_ID_METIER + (10 * indice + 3));
        identifiantsMetierObjOperation.setValeurIdentifiantMetier4(PREFIX_VALEUR_ID_METIER + (10 * indice + 4));
        identifiantsMetierObjOperation.setValeurIdentifiantMetier5(PREFIX_VALEUR_ID_METIER + (10 * indice + 5));
        identifiantsMetierObjOperation.setLibelleIdentifiantMetier1(PREFIX_LIBELLE_ID_METIER + (10 * indice + 1));
        identifiantsMetierObjOperation.setLibelleIdentifiantMetier2(PREFIX_LIBELLE_ID_METIER + (10 * indice + 2));
        identifiantsMetierObjOperation.setLibelleIdentifiantMetier3(PREFIX_LIBELLE_ID_METIER + (10 * indice + 3));
        identifiantsMetierObjOperation.setLibelleIdentifiantMetier4(PREFIX_LIBELLE_ID_METIER + (10 * indice + 4));
        identifiantsMetierObjOperation.setLibelleIdentifiantMetier5(PREFIX_LIBELLE_ID_METIER + (10 * indice + 5));
        return identifiantsMetierObjOperation;
    }

}