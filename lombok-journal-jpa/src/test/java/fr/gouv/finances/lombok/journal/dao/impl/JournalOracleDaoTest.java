/*
 * Copyright (c) 2016 DGFiP - Tous droits réservés
 * 
 */
package fr.gouv.finances.lombok.journal.dao.impl;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.data.jpa.JpaRepositoriesAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionTemplate;

import fr.gouv.finances.lombok.journal.bean.OperationJournal;
import fr.gouv.finances.lombok.journal.config.PersistenceJPAConfigTest;
import fr.gouv.finances.lombok.journal.dao.JournalDao;
import fr.gouv.finances.lombok.test.controle.ControleDonnees;
import fr.gouv.finances.lombok.test.controle.VerificationExecution;
import fr.gouv.finances.lombok.util.base.PurgeTablesDao;

/**
 * Tests unitaires automatisés du DAO JPA gérant les données des opérations de journalisation.
 *
 * @author Christophe Breheret-Girardin
 */
/*
 * @ContextConfiguration({"classpath*:/conf/applicationContext-core-persistance-test.xml",
 * "classpath*:/conf/applicationContext-jpa-orm-test.xml", "classpath*:/conf/applicationContext-journal-dao.xml",
 * "classpath*:/conf/applicationContext-commun-transaction.xml"})
 * @ActiveProfiles(profiles = {"journal", "jpa"})
 * @RunWith(SpringJUnit4ClassRunner.class)
 */
@ComponentScan(basePackages = {"fr.gouv.finances.lombok.jpa.dao", "fr.gouv.finances.lombok.journal.jpa.dao.impl"})
@EnableAutoConfiguration(exclude = JpaRepositoriesAutoConfiguration.class)
@SpringBootTest(classes = {PersistenceJPAConfigTest.class}, properties = {"lombok.composant.journal.inclus=true", "lombok.orm.jpa=true"})
@ActiveProfiles(profiles = {"embedded"})
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class JournalOracleDaoTest
{
    public JournalOracleDaoTest()
    {
        super();
    }

    protected static final Logger LOGGER = LoggerFactory.getLogger(JournalOracleDaoTest.class);

    /** Déclaration du DAO de gestion des données de journalisation. */
    @Autowired
    private JournalDao journaldao;

    /** Déclaration du DAO de gestion des purges de la base. */
    @Autowired
    private PurgeTablesDao purgetablesdao;

    /** Déclaration du template de transaction. */
    @Autowired
    private TransactionTemplate transactionTemplate;

    /**
     * Initialisation des données en base avant chaque test.
     */
    @Before
    public void creerDonnees()
    {
        // Purge des données de la base
        purgetablesdao.purgerTables("TJ_ZOPE_ZPAO_TJ", "ZOPERATIONJOURNAL_ZOPE", "ZPARAMETREOPERATION_ZPAO");

        // Vérification de l'absence de données
        ControleDonnees.verifierElements(journaldao.loadAllObjects(OperationJournal.class));

        // Suppression du cache pour forcer les requêtes des tests
        journaldao.clearPersistenceContext();
    }

    @Test
    public void testExistenceContrainteUniquePAOP()
    {
        VerificationExecution.verifierException(Throwable.class, () -> journaldao.testerExistenceContrainteUniquePAOP());
    }

    @Test
    public void sauvegardeTablesOperationJournalDansTablesTemporaires()
    {
        VerificationExecution.verifierException(Throwable.class, () -> journaldao.sauvegardeTablesOperationJournalDansTablesTemporaires());
    }

    @Test
    public void suppressionCopieTablesOperationJournal()
    {
        VerificationExecution.verifierException(Throwable.class, () -> journaldao.suppressionCopieTablesOperationJournal());
    }
}