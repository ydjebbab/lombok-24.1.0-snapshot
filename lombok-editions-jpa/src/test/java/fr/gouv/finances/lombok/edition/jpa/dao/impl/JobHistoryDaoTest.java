package fr.gouv.finances.lombok.edition.jpa.dao.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.when;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.data.jpa.JpaRepositoriesAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import fr.gouv.finances.lombok.edition.bean.ContenuEdition;
import fr.gouv.finances.lombok.edition.bean.FiltreEdition;
import fr.gouv.finances.lombok.edition.bean.FiltreValue;
import fr.gouv.finances.lombok.edition.bean.JobHistory;
import fr.gouv.finances.lombok.edition.bean.ProfilDesti;
import fr.gouv.finances.lombok.edition.config.PersistenceJPAConfigTest;
import fr.gouv.finances.lombok.edition.dao.JobHistoryDao;
import fr.gouv.finances.lombok.edition.util.FileUtils;
import fr.gouv.finances.lombok.edition.util.NombreMaxEdition;
import fr.gouv.finances.lombok.test.controle.ControleDonnees;
import fr.gouv.finances.lombok.test.controle.VerificationExecution;
import fr.gouv.finances.lombok.util.base.PurgeTablesDao;
import fr.gouv.finances.lombok.util.date.TemporaliteUtils;
import fr.gouv.finances.lombok.util.exception.ProgrammationException;
import fr.gouv.finances.lombok.util.exception.RegleGestionException;

/**
 * Tests unitaires automatisés du DAO JPA, gérant les données de l'historique des travaux d'édition.
 *
 * @author Christophe Breheret-Girardin
 * @author CF
 */
@ComponentScan(basePackages = {"fr.gouv.finances.lombok.edition.jpa.dao.impl, fr.gouv.finances.lombok.edition.config"})
@EnableAutoConfiguration(exclude = {JpaRepositoriesAutoConfiguration.class})
@SpringBootTest(classes = {PersistenceJPAConfigTest.class}, properties = {"lombok.orm.jpa=true"})
@ActiveProfiles(profiles = {"edition", "embedded"})
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional

public class JobHistoryDaoTest
{
    public JobHistoryDaoTest()
    {
        super();
    }

    protected static final Logger LOGGER = LoggerFactory.getLogger(JobHistoryDaoTest.class);

    /** Déclaration du DAO gérant les données historiques des travaux d'édition. */
    @Autowired
    private JobHistoryDao jobhistorydao;

    /** Déclaration du DAO de gestion des purges de la base. */
    @Autowired
    private PurgeTablesDao purgetableseditiondao;

    /** Initialisation d'une règle sur chaque test. */
    @Rule
    public TestName testUnitaire = new TestName();

    /** Constante donnant le suffixe d'une libellé d'une application d'origine. */
    private static final String SUFFIXE_CONTENU = "contenu ";

    /** Constante donnant le suffixe d'une libellé d'une application d'origine. */
    private static final String SUFFIXE_APPLICATION_ORIGINE = "APPLICATION ORIGINE ";

    /** Constante donnant le libellé d'une 1ère application d'origine. */
    private static final String APPLICATION_ORIGINE_1 = SUFFIXE_APPLICATION_ORIGINE + 1;

    /** Constante donnant le libellé d'une 2ème application d'origine. */
    private static final String APPLICATION_ORIGINE_2 = SUFFIXE_APPLICATION_ORIGINE + 2;

    /** Constante donnant le libellé d'une application d'origine non utilisée dans les jeux d'essai. */
    private static final String APPLICATION_ORIGINE_INCONNUE = SUFFIXE_APPLICATION_ORIGINE + 3;

    /** Constante d'un identifiant d'édition. */
    private static final String IDENTIFIANT_EDITION = "id";

    /** Constante d'un 2ème identifiant d'édition. */
    private static final String IDENTIFIANT_EDITION_2 = "id 2";

    /** Constante d'un identifiant d'édition non persisté. */
    private static final String IDENTIFIANT_EDITION_INCONNU = "id inconnu";

    /** Constante d'un propriétaire d'édition. */
    private static final String UID_PROPRIETAIRE = "UID proprietaire";

    /** Constante d'un 2ème propriétaire d'édition. */
    private static final String UID_PROPRIETAIRE_2 = "UID proprietaire 2";

    /** Constante d'un propriétaire d'édition non persisté. */
    private static final String UID_PROPRIETAIRE_INCONNU = "UID proprietaire inconnu";

    /** Constante du suffixe utilisé dans les libellés de profil. */
    private static final String SUFFIXE_PROFIL = "Profil ";

    /** Map contenant les historiques des travaux d'édition présents en base */
    private Map<Integer, JobHistory> mapJobHistory;

    /** Bouchon pour obtenir le comportement d'un fichier non lisible. */
    @Mock
    File fichierNonLisible = FileUtils.getFichierTemporaire("contenu temporaire");

    /**
     * Initialisation des données en base avant les tests.
     */
    @Before
    public void creerDonnees()
    {
        LOGGER.debug("Début d'initialisation du test '{}'", testUnitaire.getMethodName());

        // Initialisation des bouchons
        MockitoAnnotations.initMocks(this);

        // Purge des données de la base
        purgetableseditiondao.purgerTables("ZFILTREVALUE_ZFIV", "ZFILTREEDITION_ZFIE", "ZPROFILDESTI_ZPRD", "ZJOBHISTORY_ZJOH");

        // Vérification de l'absence de données suite à la purge
        verifierJobHistory(jobhistorydao.loadAllObjects(JobHistory.class));

        // Génération des historiques de travaux d'édition, insertion en base de données et sauvegarde dans une Map
        mapJobHistory = IntStream.rangeClosed(1, 15).mapToObj(this::getJobHistory)
            .peek(this::enregistrer)
            .collect(Collectors.toMap(j -> Integer.parseInt(j.getEditionUuid()), j -> j));

        // Insertion d'un profil non utilisé
        jobhistorydao.saveObject(getProfil(3));

        // Récupération des données insérées pour avoir une image de la base
        List<JobHistory> listeJobHistory = jobhistorydao.loadAllObjects(JobHistory.class);

        // Vérification des nouvelles données à partir de l'image de la base
        verifierJobHistory(listeJobHistory, mapJobHistory.get(1), mapJobHistory.get(2), mapJobHistory.get(3), mapJobHistory.get(4),
            mapJobHistory.get(5), mapJobHistory.get(6), mapJobHistory.get(7), mapJobHistory.get(8), mapJobHistory.get(9),
            mapJobHistory.get(10), mapJobHistory.get(11), mapJobHistory.get(12), mapJobHistory.get(13), mapJobHistory.get(14),
            mapJobHistory.get(15));

        LOGGER.debug("Fin d'initialisation du test '{}'", testUnitaire.getMethodName());
    }

    /**
     * Test de l'injection du DAO.
     */
    @Test
    public final void injectionDao()
    {
        // Vérification
        assertNotNull("DAO non injecté dans le test", jobhistorydao);
    }

    /**
     * Test de fr.gouv.finances.lombok.edition.dao.JobHistoryDao#saveJobHistory(
     * fr.gouv.finances.lombok.edition.bean.JobHistory).
     */
    @Test
    public void saveJobHistory()
    {
        LOGGER.debug("Methode saveJobHistory");
        // Création et sauvegarde d'un nouvel historique de travaux d'édition globalement vide
        JobHistory jobHistory19 = new JobHistory();
        jobHistory19.setEditionUuid(String.valueOf(19));
        jobHistory19.getStockageEdition().setTailleFichier(19L);
        jobhistorydao.saveJobHistory(jobHistory19);

        // Vérification de la sauvegarde
        verifierJobHistory(jobhistorydao.loadAllObjects(JobHistory.class), mapJobHistory.get(1), mapJobHistory.get(2), mapJobHistory.get(3),
            mapJobHistory.get(4), mapJobHistory.get(5), mapJobHistory.get(6), mapJobHistory.get(7), mapJobHistory.get(8),
            mapJobHistory.get(9), mapJobHistory.get(10), mapJobHistory.get(11), mapJobHistory.get(12), mapJobHistory.get(13),
            mapJobHistory.get(14), mapJobHistory.get(15), jobHistory19);

        // Purge des données de la base
        purgetableseditiondao.purgerTables("ZFILTREVALUE_ZFIV", "ZFILTREEDITION_ZFIE", "ZPROFILDESTI_ZPRD", "ZJOBHISTORY_ZJOH");

        // Génération des historiques de travaux d'édition, insertion en base de données
        mapJobHistory = IntStream.rangeClosed(1, 15).mapToObj(this::getJobHistory)
            .peek(jobhistorydao::saveJobHistory)
            .collect(Collectors.toMap(j -> Integer.parseInt(j.getEditionUuid()), j -> j));

        // Vérification de la sauvegarde
        verifierJobHistory(jobhistorydao.loadAllObjects(JobHistory.class), mapJobHistory.get(1), mapJobHistory.get(2), mapJobHistory.get(3),
            mapJobHistory.get(4), mapJobHistory.get(5), mapJobHistory.get(6), mapJobHistory.get(7), mapJobHistory.get(8),
            mapJobHistory.get(9), mapJobHistory.get(10), mapJobHistory.get(11), mapJobHistory.get(12), mapJobHistory.get(13),
            mapJobHistory.get(14), mapJobHistory.get(15));
    }

    /**
     * Test de fr.gouv.finances.lombok.edition.dao.JobHistoryDao#deleteJobHistory(
     * fr.gouv.finances.lombok.edition.bean.JobHistory).
     */
    @Test
    public void deleteJobHistory()
    {
        // Appel de la méthode à tester sur des paramétres non initialisés et/ou vides, et vérifications
        VerificationExecution.verifierException(ProgrammationException.class, () -> jobhistorydao.deleteJobHistory(null));
        VerificationExecution.verifierException(ProgrammationException.class, () -> jobhistorydao.deleteJobHistory(new JobHistory()));
        VerificationExecution.verifierException(ProgrammationException.class, () -> jobhistorydao.deleteJobHistory(getJobHistory(6)));

        // Appel de la méthode à tester, sur un élément chargé, et vérifications de la conformité
        jobhistorydao.deleteJobHistory(mapJobHistory.get(8));
        verifierJobHistory(jobhistorydao.loadAllObjects(JobHistory.class), mapJobHistory.get(1), mapJobHistory.get(2), mapJobHistory.get(3),
            mapJobHistory.get(4), mapJobHistory.get(5), mapJobHistory.get(6), mapJobHistory.get(7), mapJobHistory.get(9),
            mapJobHistory.get(10), mapJobHistory.get(11), mapJobHistory.get(12), mapJobHistory.get(13), mapJobHistory.get(14),
            mapJobHistory.get(15));
    }

    /**
     * Test de fr.gouv.finances.lombok.edition.dao.JobHistoryDao#deleteJobsHistories(java.util.List).
     */
    @Test
    public void deleteJobsHistories()
    {
        // Appel de la méthode à tester sur des paramétres non initialisés et/ou vides, et vérifications
        VerificationExecution.verifierException(ProgrammationException.class, () -> jobhistorydao.deleteJobsHistories(null));

        // Appel de la méthode à tester, sans résultat, et vérifications
        VerificationExecution.verifierException(ProgrammationException.class,
            () -> jobhistorydao.deleteJobsHistories(Arrays.asList(getJobHistory(4), getJobHistory(6))));

        // Appel de la méthode à tester, sur tous les éléments existants, et vérifications de leur conformité
        jobhistorydao.deleteJobsHistories(Arrays.asList(mapJobHistory.get(8), mapJobHistory.get(9)));
        verifierJobHistory(jobhistorydao.loadAllObjects(JobHistory.class), mapJobHistory.get(1), mapJobHistory.get(2), mapJobHistory.get(3),
            mapJobHistory.get(4), mapJobHistory.get(5), mapJobHistory.get(6), mapJobHistory.get(7), mapJobHistory.get(10),
            mapJobHistory.get(11), mapJobHistory.get(12), mapJobHistory.get(13), mapJobHistory.get(14), mapJobHistory.get(15));
    }

    /**
     * Test de fr.gouv.finances.lombok.edition.dao.JobHistoryDao#findBeanEditionIds(java.lang.String).
     */
    @Test
    public void findBeanEditionIds()
    {
        // Appel de la méthode à tester sur des paramétres non initialisés et/ou vides, et vérifications
        VerificationExecution.verifierException(ProgrammationException.class, () -> jobhistorydao.findBeanEditionIds(null));
        VerificationExecution.verifierException(ProgrammationException.class, () -> jobhistorydao.findBeanEditionIds(""));

        // Appel de la méthode à tester, sans résultat, et vérifications
        ControleDonnees.verifierElements(jobhistorydao.findBeanEditionIds(APPLICATION_ORIGINE_INCONNUE));

        // Appel de la méthode à tester, avec résultats, et vérifications de leur conformité
        ControleDonnees.verifierElements(jobhistorydao.findBeanEditionIds(APPLICATION_ORIGINE_1), IDENTIFIANT_EDITION,
            IDENTIFIANT_EDITION_2);
        ControleDonnees.verifierElements(jobhistorydao.findBeanEditionIds(APPLICATION_ORIGINE_2), IDENTIFIANT_EDITION);
    }

    /**
     * Test de fr.gouv.finances.lombok.edition.dao.JobHistoryDao#findEditionsLesPlusLongues( java.lang.String, int).
     */
    @Test
    public void findEditionsLesPlusLongues()
    {
        // Appel de la méthode à tester sur des paramétres non initialisés et/ou vides, et vérifications
        VerificationExecution.verifierException(ProgrammationException.class, () -> jobhistorydao.findEditionsLesPlusLongues(null, 0));
        VerificationExecution.verifierException(ProgrammationException.class, () -> jobhistorydao.findEditionsLesPlusLongues("", 0));

        // Appel de la méthode à tester, sans résultat, et vérifications
        verifierJobHistory(jobhistorydao.findEditionsLesPlusLongues(APPLICATION_ORIGINE_INCONNUE, 0));
        verifierJobHistory(jobhistorydao.findEditionsLesPlusLongues(APPLICATION_ORIGINE_2, 0), mapJobHistory.get(12));

        // Appel de la méthode à tester, sur tous les éléments existants, et vérifications de leur conformité
        verifierJobHistory(jobhistorydao.findEditionsLesPlusLongues(APPLICATION_ORIGINE_1, 0), true, mapJobHistory.get(15),
            mapJobHistory.get(14), mapJobHistory.get(13));
        verifierJobHistory(jobhistorydao.findEditionsLesPlusLongues(APPLICATION_ORIGINE_1, 2), true, mapJobHistory.get(15),
            mapJobHistory.get(14));
    }

    /**
     * Test de fr.gouv.finances.lombok.edition.dao.JobHistoryDao#findJobHistory(java.lang.String).
     */
    @Test
    public void findJobHistory()
    {
        // Appel de la méthode à tester sur des paramétres non initialisés et/ou vides, et vérifications
        VerificationExecution.verifierException(ProgrammationException.class, () -> jobhistorydao.findJobHistory(null));
        VerificationExecution.verifierException(ProgrammationException.class, () -> jobhistorydao.findJobHistory(""));

        // Appel de la méthode à tester, sans résultat, et vérifications
        assertNull(jobhistorydao.findJobHistory(IDENTIFIANT_EDITION_INCONNU));

        // Appel de la méthode à tester, sur tous les éléments existants, et vérifications de leur conformité
        mapJobHistory.entrySet().forEach(entry -> verifierJobHistory(
            jobhistorydao.findJobHistory(String.valueOf(entry.getKey())), entry.getValue()));
    }

    /**
     * Méthode permettant de vérifier la taille du fichier.
     *
     * @param jobHistory historique des travaux d'édition à vérifier
     * @param variation la taille est équivalent à l'UID, sauf si c'est une variation a été apportée
     */
    private void verifierTailleFichier(JobHistory jobHistory, long variation)
    {
        // Récupération de l'UUID
        Long editionUuid = Long.parseLong(jobHistory.getEditionUuid());
        // Récupération de la taille attendue (UUID + une éventuelle variation)
        Long taille = editionUuid + variation;
        // Il n'y a pas de stockage pour l'UUID "1"
        if (editionUuid != 1l)
        {
            // Vérification
            assertEquals(taille, jobHistory.getStockageEdition().getTailleFichier());
        }
    }

    /**
     * Test de fr.gouv.finances.lombok.edition.dao.JobHistoryDao#findJobHistoryParProfilStructureAffectationAppli(
     * java.lang.String, java.lang.String, java.lang.String).<br/>
     * Remarque : la méthode à tester n'est pas implémentée.
     */
    @Test
    public void findJobHistoryParProfilStructureAffectationAppli()
    {
        // Appel de la méthode à tester avec des paramètres renseignés
        VerificationExecution.verifierException(UnsupportedOperationException.class,
            () -> jobhistorydao.findJobHistoryParProfilStructureAffectationAppli(
                "Profil", "affectation", APPLICATION_ORIGINE_1));
    }

    /**
     * Test de fr.gouv.finances.lombok.edition.dao.JobHistoryDao#findLaDerniereEditionParBeanIdEtParAppliOrigine(
     * java.lang.String, java.lang.String).
     */
    @Test
    public void findLaDerniereEditionParBeanIdEtParAppliOrigine()
    {
        // Appel de la méthode à tester sur des paramétres non initialisés et/ou vides, et vérifications
        VerificationExecution.verifierException(ProgrammationException.class,
            () -> jobhistorydao.findLaDerniereEditionParBeanIdEtParAppliOrigine(null, null));
        VerificationExecution.verifierException(ProgrammationException.class,
            () -> jobhistorydao.findLaDerniereEditionParBeanIdEtParAppliOrigine("", ""));
        VerificationExecution.verifierException(ProgrammationException.class,
            () -> jobhistorydao.findLaDerniereEditionParBeanIdEtParAppliOrigine(IDENTIFIANT_EDITION, null));
        VerificationExecution.verifierException(ProgrammationException.class,
            () -> jobhistorydao.findLaDerniereEditionParBeanIdEtParAppliOrigine(null, APPLICATION_ORIGINE_1));

        // Appel de la méthode à tester, sans résultat, et vérifications
        assertNull(jobhistorydao.findLaDerniereEditionParBeanIdEtParAppliOrigine(
            IDENTIFIANT_EDITION_INCONNU, APPLICATION_ORIGINE_1));
        assertNull(jobhistorydao.findLaDerniereEditionParBeanIdEtParAppliOrigine(
            IDENTIFIANT_EDITION, APPLICATION_ORIGINE_INCONNUE));
        assertNull(jobhistorydao.findLaDerniereEditionParBeanIdEtParAppliOrigine(
            IDENTIFIANT_EDITION_2, APPLICATION_ORIGINE_2));

        // Appel de la méthode à tester, sur tous les éléments existants, et vérifications de leur conformité
        verifierJobHistory(jobhistorydao.findLaDerniereEditionParBeanIdEtParAppliOrigine(
            IDENTIFIANT_EDITION, APPLICATION_ORIGINE_1), mapJobHistory.get(14));
        verifierJobHistory(jobhistorydao.findLaDerniereEditionParBeanIdEtParAppliOrigine(
            IDENTIFIANT_EDITION, APPLICATION_ORIGINE_2), mapJobHistory.get(12));
        verifierJobHistory(jobhistorydao.findLaDerniereEditionParBeanIdEtParAppliOrigine(
            IDENTIFIANT_EDITION_2, APPLICATION_ORIGINE_1), mapJobHistory.get(15));
    }

    /**
     * Test de fr.gouv.finances.lombok.edition.dao.JobHistoryDao
     * #findLaDerniereEditionParBeanIdEtParAppliOrigineEtParUtilisateur( java.lang.String, java.lang.String,
     * java.lang.String).
     */
    @Test
    public void findLaDerniereEditionParBeanIdEtParAppliOrigineEtParUtilisateur()
    {
        // Appel de la méthode à tester avec des paramètres non renseignés et/ou vides, et vérifications
        VerificationExecution.verifierException(ProgrammationException.class,
            () -> jobhistorydao.findLaDerniereEditionParBeanIdEtParAppliOrigineEtParUtilisateur(
                null, null, null));
        VerificationExecution.verifierException(ProgrammationException.class,
            () -> jobhistorydao.findLaDerniereEditionParBeanIdEtParAppliOrigineEtParUtilisateur(
                "", "", ""));
        VerificationExecution.verifierException(ProgrammationException.class,
            () -> jobhistorydao.findLaDerniereEditionParBeanIdEtParAppliOrigineEtParUtilisateur(
                IDENTIFIANT_EDITION, null, null));
        VerificationExecution.verifierException(ProgrammationException.class,
            () -> jobhistorydao.findLaDerniereEditionParBeanIdEtParAppliOrigineEtParUtilisateur(
                null, APPLICATION_ORIGINE_1, null));
        VerificationExecution.verifierException(ProgrammationException.class,
            () -> jobhistorydao.findLaDerniereEditionParBeanIdEtParAppliOrigineEtParUtilisateur(
                null, null, UID_PROPRIETAIRE));

        // Appel de la méthode à tester, sans résultat, et vérifications
        assertNull(jobhistorydao.findLaDerniereEditionParBeanIdEtParAppliOrigineEtParUtilisateur(
            IDENTIFIANT_EDITION_INCONNU, APPLICATION_ORIGINE_1, UID_PROPRIETAIRE));
        assertNull(jobhistorydao.findLaDerniereEditionParBeanIdEtParAppliOrigineEtParUtilisateur(
            IDENTIFIANT_EDITION, APPLICATION_ORIGINE_INCONNUE, UID_PROPRIETAIRE));
        assertNull(jobhistorydao.findLaDerniereEditionParBeanIdEtParAppliOrigineEtParUtilisateur(
            IDENTIFIANT_EDITION, APPLICATION_ORIGINE_1, UID_PROPRIETAIRE_INCONNU));
        assertNull(jobhistorydao.findLaDerniereEditionParBeanIdEtParAppliOrigineEtParUtilisateur(
            IDENTIFIANT_EDITION_2, APPLICATION_ORIGINE_2, UID_PROPRIETAIRE_2));

        // Appel de la méthode à tester, sur tous les éléments existants, et vérifications de leur conformité
        verifierJobHistory(jobhistorydao.findLaDerniereEditionParBeanIdEtParAppliOrigineEtParUtilisateur(
            IDENTIFIANT_EDITION, APPLICATION_ORIGINE_1, UID_PROPRIETAIRE), mapJobHistory.get(14));
        verifierJobHistory(jobhistorydao.findLaDerniereEditionParBeanIdEtParAppliOrigineEtParUtilisateur(
            IDENTIFIANT_EDITION_2, APPLICATION_ORIGINE_1, UID_PROPRIETAIRE_2), mapJobHistory.get(15));
        verifierJobHistory(jobhistorydao.findLaDerniereEditionParBeanIdEtParAppliOrigineEtParUtilisateur(
            IDENTIFIANT_EDITION, APPLICATION_ORIGINE_2, UID_PROPRIETAIRE), mapJobHistory.get(12));
    }

    /**
     * Test de fr.gouv.finances.lombok.edition.dao.JobHistoryDao#findToutesEditionsParProfilFiltreAppli( java.util.Map,
     * java.lang.String).
     */
    @Test
    public void findToutesEditionsParProfilFiltreAppli()
    {
        // Initialisation de jeux d'essai
        Map<String, Set<FiltreEdition>> profilMapInconnu = new HashMap<>();
        profilMapInconnu.put(getProfil(4).getNomProfil(), new HashSet<>(Arrays.asList(getFiltreEdition(1))));

        Map<String, Set<FiltreEdition>> profilMapNonRenseigne = new HashMap<>();
        profilMapNonRenseigne.put(getProfil(1).getNomProfil(), null);

        Map<String, Set<FiltreEdition>> profilMapVide = new HashMap<>();
        profilMapVide.put(getProfil(1).getNomProfil(), new HashSet<>(Arrays.asList(new FiltreEdition())));

        Map<String, Set<FiltreEdition>> profilMap1 = new HashMap<>();
        profilMap1.put(getProfil(1).getNomProfil(), new HashSet<>(Arrays.asList(getFiltreEdition(3))));

        Map<String, Set<FiltreEdition>> profilMap2 = new HashMap<>();
        profilMap2.put(getProfil(1).getNomProfil(), new HashSet<>(Arrays.asList(getFiltreEdition(3))));
        profilMap2.put(getProfil(2).getNomProfil(), new HashSet<>(Arrays.asList(getFiltreEdition(3))));

        // Appel de la méthode à tester sur des paramétres non initialisés et/ou vides, et vérifications
        VerificationExecution.verifierException(ProgrammationException.class,
            () -> jobhistorydao.findToutesEditionsParProfilFiltreAppli(null, null));
        VerificationExecution.verifierException(ProgrammationException.class,
            () -> jobhistorydao.findToutesEditionsParProfilFiltreAppli(new HashMap<>(), ""));
        VerificationExecution.verifierException(ProgrammationException.class,
            () -> jobhistorydao.findToutesEditionsParProfilFiltreAppli(null, APPLICATION_ORIGINE_1));
        /*
         * VerificationExecution.verifierException(ProgrammationException.class , () ->
         * jobhistorydao.findToutesEditionsParProfilFiltreAppli(new HashMap<>(), APPLICATION_ORIGINE_1));
         */
        VerificationExecution.verifierException(ProgrammationException.class,
            () -> jobhistorydao.findToutesEditionsParProfilFiltreAppli(profilMap1, null));
        VerificationExecution.verifierException(ProgrammationException.class,
            () -> jobhistorydao.findToutesEditionsParProfilFiltreAppli(profilMapNonRenseigne, APPLICATION_ORIGINE_1));

        // Appel de la méthode à tester, sans résultat, et vérifications
        verifierJobHistory(jobhistorydao.findToutesEditionsParProfilFiltreAppli(
            profilMapInconnu, APPLICATION_ORIGINE_1));
        verifierJobHistory(jobhistorydao.findToutesEditionsParProfilFiltreAppli(
            profilMap1, APPLICATION_ORIGINE_INCONNUE));

        // Appel de la méthode à tester, sur tous les éléments existants, et vérifications de leur conformité
        verifierJobHistory(jobhistorydao.findToutesEditionsParProfilFiltreAppli(
            profilMap1, APPLICATION_ORIGINE_1), true, mapJobHistory.get(14));
        verifierJobHistory(jobhistorydao.findToutesEditionsParProfilFiltreAppli(
            profilMap2, APPLICATION_ORIGINE_1), true, mapJobHistory.get(14));
        verifierJobHistory(jobhistorydao.findToutesEditionsParProfilFiltreAppli(
            profilMapVide, APPLICATION_ORIGINE_1), true, mapJobHistory.get(14));
    }

    /**
     * Test de fr.gouv.finances.lombok.edition.dao.JobHistoryDao#findNouvellesEditionsParProfilFiltreAppli(
     * java.lang.String, java.util.Map, java.lang.String).
     */
    @Test
    public void findNouvellesEditionsParProfilFiltreAppli()
    {
        Map<String, Set<FiltreEdition>> profilMapInconnu = new HashMap<>();
        profilMapInconnu.put(getProfil(4).getNomProfil(), new HashSet<>(Arrays.asList(getFiltreEdition(1))));

        Map<String, Set<FiltreEdition>> profilMapNonRenseigne = new HashMap<>();
        profilMapNonRenseigne.put(getProfil(1).getNomProfil(), null);

        Map<String, Set<FiltreEdition>> profilMapVide = new HashMap<>();
        profilMapVide.put(getProfil(1).getNomProfil(), new HashSet<>(Arrays.asList(new FiltreEdition())));

        Map<String, Set<FiltreEdition>> profilMap = new HashMap<>();
        profilMap.put(getProfil(1).getNomProfil(), new HashSet<>(Arrays.asList(getFiltreEdition(3))));

        // Appel de la méthode à tester sur des paramétres non initialisés et/ou vides, et vérifications
        VerificationExecution.verifierException(ProgrammationException.class,
            () -> jobhistorydao.findNouvellesEditionsParProfilFiltreAppli(null, null, null));
        VerificationExecution.verifierException(ProgrammationException.class,
            () -> jobhistorydao.findNouvellesEditionsParProfilFiltreAppli("", new HashMap<>(), ""));
        VerificationExecution.verifierException(ProgrammationException.class,
            () -> jobhistorydao.findNouvellesEditionsParProfilFiltreAppli(UID_PROPRIETAIRE_2, null, null));
        VerificationExecution.verifierException(ProgrammationException.class, () -> jobhistorydao.findNouvellesEditionsParProfilFiltreAppli(
            UID_PROPRIETAIRE_2, null, APPLICATION_ORIGINE_1));
        /*
         * VerificationExecution.verifierException(ProgrammationException.class , () ->
         * jobhistorydao.findNouvellesEditionsParProfilFiltreAppli( UID_PROPRIETAIRE_2, new HashMap<>(),
         * APPLICATION_ORIGINE_1));
         */
        VerificationExecution.verifierException(ProgrammationException.class,
            () -> jobhistorydao.findNouvellesEditionsParProfilFiltreAppli(null, null, APPLICATION_ORIGINE_1));
        VerificationExecution.verifierException(ProgrammationException.class, () -> jobhistorydao.findNouvellesEditionsParProfilFiltreAppli(
            null, profilMap, APPLICATION_ORIGINE_1));
        VerificationExecution.verifierException(ProgrammationException.class,
            () -> jobhistorydao.findNouvellesEditionsParProfilFiltreAppli(null, profilMap, null));
        VerificationExecution.verifierException(ProgrammationException.class, () -> jobhistorydao.findNouvellesEditionsParProfilFiltreAppli(
            UID_PROPRIETAIRE_2, profilMap, null));
        VerificationExecution.verifierException(ProgrammationException.class, () -> jobhistorydao.findNouvellesEditionsParProfilFiltreAppli(
            UID_PROPRIETAIRE, profilMapNonRenseigne, APPLICATION_ORIGINE_1));

        // Appel de la méthode à tester, sans résultat, et vérifications
        verifierJobHistory(jobhistorydao.findNouvellesEditionsParProfilFiltreAppli(
            UID_PROPRIETAIRE, profilMap, APPLICATION_ORIGINE_2));
        verifierJobHistory(jobhistorydao.findNouvellesEditionsParProfilFiltreAppli(
            UID_PROPRIETAIRE_INCONNU, profilMapInconnu, APPLICATION_ORIGINE_1));
        verifierJobHistory(jobhistorydao.findNouvellesEditionsParProfilFiltreAppli(
            UID_PROPRIETAIRE_INCONNU, profilMap, APPLICATION_ORIGINE_INCONNUE));

        // Appel de la méthode à tester, sur tous les éléments existants, et vérifications de leur conformité
        verifierJobHistory(jobhistorydao.findNouvellesEditionsParProfilFiltreAppli(
            UID_PROPRIETAIRE, profilMap, APPLICATION_ORIGINE_1), true, mapJobHistory.get(14));
        verifierJobHistory(jobhistorydao.findNouvellesEditionsParProfilFiltreAppli(
            UID_PROPRIETAIRE, profilMapVide, APPLICATION_ORIGINE_1), true, mapJobHistory.get(14));
        verifierJobHistory(jobhistorydao.findNouvellesEditionsParProfilFiltreAppli(
            UID_PROPRIETAIRE_2, profilMapVide, APPLICATION_ORIGINE_1), true, mapJobHistory.get(14));
        verifierJobHistory(jobhistorydao.findNouvellesEditionsParProfilFiltreAppli(
            UID_PROPRIETAIRE_2, profilMapVide, APPLICATION_ORIGINE_2));
    }

    /**
     * Test de fr.gouv.finances.lombok.edition.dao.JobHistoryDao#findToutesEditionsParUidEtAppli( java.lang.String,
     * java.lang.String).
     */
    @Test
    public void findToutesEditionsParUidEtAppli()
    {
        // Appel de la méthode à tester sur des paramétres non initialisés et/ou vides, et vérifications
        VerificationExecution.verifierException(ProgrammationException.class,
            () -> jobhistorydao.findToutesEditionsParUidEtAppli(null, null));
        VerificationExecution.verifierException(ProgrammationException.class, () -> jobhistorydao.findToutesEditionsParUidEtAppli("", ""));
        VerificationExecution.verifierException(ProgrammationException.class,
            () -> jobhistorydao.findToutesEditionsParUidEtAppli(UID_PROPRIETAIRE, null));
        VerificationExecution.verifierException(ProgrammationException.class,
            () -> jobhistorydao.findToutesEditionsParUidEtAppli(null, APPLICATION_ORIGINE_1));

        // Appel de la méthode à tester, sans résultat, et vérifications
        verifierJobHistory(jobhistorydao.findToutesEditionsParUidEtAppli(
            UID_PROPRIETAIRE_INCONNU, APPLICATION_ORIGINE_1));
        verifierJobHistory(jobhistorydao.findToutesEditionsParUidEtAppli(
            UID_PROPRIETAIRE, APPLICATION_ORIGINE_INCONNUE));

        // Appel de la méthode à tester, sur tous les éléments existants, et vérifications de leur conformité
        verifierJobHistory(jobhistorydao.findToutesEditionsParUidEtAppli(
            UID_PROPRIETAIRE, APPLICATION_ORIGINE_1), true, mapJobHistory.get(13));
        verifierJobHistory(jobhistorydao.findToutesEditionsParUidEtAppli(
            UID_PROPRIETAIRE, APPLICATION_ORIGINE_2), true, mapJobHistory.get(12), mapJobHistory.get(11));
        verifierJobHistory(jobhistorydao.findToutesEditionsParUidEtAppli(
            UID_PROPRIETAIRE_2, APPLICATION_ORIGINE_1), true, mapJobHistory.get(15));
    }

    /**
     * Test de fr.gouv.finances.lombok.edition.dao.JobHistoryDao#isValidEdition(
     * fr.gouv.finances.lombok.edition.bean.JobHistory).
     */
    @Test
    public void isValidEdition()
    {
        // Appel de la méthode à tester sur des paramétres non initialisés et/ou vides, et vérifications
        VerificationExecution.verifierException(ProgrammationException.class, () -> jobhistorydao.isValidEdition(null));
        VerificationExecution.verifierException(ProgrammationException.class, () -> jobhistorydao.isValidEdition(new JobHistory()));

        // Appel de la méthode à tester, sans résultat, et vérifications
        assertFalse("L'édition ne devrait pas être trouvée", jobhistorydao.isValidEdition(getJobHistory(200)));

        // Appel de la méthode à tester, sur tous les éléments existants, et vérifications de leur conformité
        mapJobHistory.entrySet().forEach(
            entry -> assertTrue("L'édition n'aurait pas dû être trouvée", jobhistorydao.isValidEdition(getJobHistory(entry.getKey()))));
    }

    /**
     * Test de fr.gouv.finances.lombok.edition.dao.JobHistoryDao#findNouvellesEditionsParUidEtAppli( java.lang.String,
     * java.lang.String).
     */
    @Test
    public void findNouvellesEditionsParUidEtAppli()
    {
        // Appel de la méthode à tester sur des paramétres non initialisés et/ou vides, et vérifications
        VerificationExecution.verifierException(ProgrammationException.class,
            () -> jobhistorydao.findNouvellesEditionsParUidEtAppli(null, null));
        VerificationExecution.verifierException(ProgrammationException.class,
            () -> jobhistorydao.findNouvellesEditionsParUidEtAppli("", ""));
        VerificationExecution.verifierException(ProgrammationException.class,
            () -> jobhistorydao.findNouvellesEditionsParUidEtAppli(UID_PROPRIETAIRE, null));
        VerificationExecution.verifierException(ProgrammationException.class,
            () -> jobhistorydao.findNouvellesEditionsParUidEtAppli(null, APPLICATION_ORIGINE_1));

        // Appel de la méthode à tester, sans résultat, et vérifications
        verifierJobHistory(jobhistorydao.findNouvellesEditionsParUidEtAppli(
            UID_PROPRIETAIRE_INCONNU, APPLICATION_ORIGINE_1));
        verifierJobHistory(jobhistorydao.findNouvellesEditionsParUidEtAppli(
            UID_PROPRIETAIRE, APPLICATION_ORIGINE_INCONNUE));

        // Appel de la méthode à tester, sur tous les éléments existants, et vérifications de leur conformité
        verifierJobHistory(jobhistorydao.findNouvellesEditionsParUidEtAppli(
            UID_PROPRIETAIRE, APPLICATION_ORIGINE_1), true);
        verifierJobHistory(jobhistorydao.findNouvellesEditionsParUidEtAppli(
            UID_PROPRIETAIRE, APPLICATION_ORIGINE_2), true, mapJobHistory.get(12), mapJobHistory.get(11));
        verifierJobHistory(jobhistorydao.findNouvellesEditionsParUidEtAppli(
            UID_PROPRIETAIRE_2, APPLICATION_ORIGINE_1), true, mapJobHistory.get(15));
    }

    /**
     * Test de fr.gouv.finances.lombok.edition.dao.JobHistoryDao#findProfils(java.lang.String).
     */
    @Test
    public void findProfils()
    {
        // Appel de la méthode à tester sur des paramétres non initialisés et/ou vides, et vérifications
        VerificationExecution.verifierException(ProgrammationException.class, () -> jobhistorydao.findProfils(null));
        VerificationExecution.verifierException(ProgrammationException.class, () -> jobhistorydao.findProfils(""));

        // Appel de la méthode à tester, sans résultat, et vérifications
        ControleDonnees.verifierElements(jobhistorydao.findProfils(APPLICATION_ORIGINE_INCONNUE));

        // Appel de la méthode à tester, sur tous les éléments existants, et vérifications de leur conformité
        ControleDonnees.verifierElements(jobhistorydao.findProfils(APPLICATION_ORIGINE_1), getProfil(1), getProfil(2));
        ControleDonnees.verifierElements(jobhistorydao.findProfils(APPLICATION_ORIGINE_2), getProfil(1));
    }

    /**
     * Test de fr.gouv.finances.lombok.edition.dao.JobHistoryDao#findToutesEditionsParBeanEditionId( java.lang.String,
     * java.lang.String).
     */
    @Test
    public void findToutesEditionsParBeanEditionId()
    {
        // Appel de la méthode à tester sur des paramétres non initialisés et/ou vides, et vérifications
        VerificationExecution.verifierException(ProgrammationException.class,
            () -> jobhistorydao.findToutesEditionsParBeanEditionId(null, null));
        VerificationExecution.verifierException(ProgrammationException.class,
            () -> jobhistorydao.findToutesEditionsParBeanEditionId("", ""));
        VerificationExecution.verifierException(ProgrammationException.class,
            () -> jobhistorydao.findToutesEditionsParBeanEditionId(IDENTIFIANT_EDITION, null));
        VerificationExecution.verifierException(ProgrammationException.class,
            () -> jobhistorydao.findToutesEditionsParBeanEditionId(null, APPLICATION_ORIGINE_1));

        // Appel de la méthode à tester, sans résultat, et vérifications
        verifierJobHistory(jobhistorydao.findToutesEditionsParBeanEditionId(
            IDENTIFIANT_EDITION_INCONNU, APPLICATION_ORIGINE_1));
        verifierJobHistory(jobhistorydao.findToutesEditionsParBeanEditionId(
            IDENTIFIANT_EDITION, APPLICATION_ORIGINE_INCONNUE));

        // Appel de la méthode à tester, sur tous les éléments existants, et vérifications de leur conformité
        verifierJobHistory(jobhistorydao.findToutesEditionsParBeanEditionId(
            IDENTIFIANT_EDITION, APPLICATION_ORIGINE_1), true, mapJobHistory.get(14), mapJobHistory.get(13));
        verifierJobHistory(jobhistorydao.findToutesEditionsParBeanEditionId(
            IDENTIFIANT_EDITION, APPLICATION_ORIGINE_2), true, mapJobHistory.get(12), mapJobHistory.get(11));
        verifierJobHistory(jobhistorydao.findToutesEditionsParBeanEditionId(
            IDENTIFIANT_EDITION_2, APPLICATION_ORIGINE_1), true, mapJobHistory.get(15));

        // Test de la limite haute
        IntStream.rangeClosed(16, 150).forEach(indice -> enregistrer(getJobHistory(indice)));
        assertEquals(NombreMaxEdition.MAX_LIGNE_PAR_UID.getNbreMax(), jobhistorydao.findToutesEditionsParBeanEditionId(
            IDENTIFIANT_EDITION, APPLICATION_ORIGINE_1).size());
    }

    /**
     * Test de fr.gouv.finances.lombok.edition.dao.JobHistoryDao#findToutesEditionsParDateDemande( java.util.Date,
     * java.lang.String).
     */
    @Test
    public void findToutesEditionsParDateDemande()
    {
        // Appel de la méthode à tester sur des paramétres non initialisés et/ou vides, et vérifications
        VerificationExecution.verifierException(ProgrammationException.class,
            () -> jobhistorydao.findToutesEditionsParDateDemande(null, null));
        VerificationExecution.verifierException(ProgrammationException.class,
            () -> jobhistorydao.findToutesEditionsParDateDemande(null, ""));
        VerificationExecution.verifierException(ProgrammationException.class,
            () -> jobhistorydao.findToutesEditionsParDateDemande(null, APPLICATION_ORIGINE_1));
        VerificationExecution.verifierException(ProgrammationException.class,
            () -> jobhistorydao.findToutesEditionsParDateDemande(TemporaliteUtils.getDate(2018, 1, 8), null));

        // Appel de la méthode à tester, sans résultat, et vérifications
        verifierJobHistory(jobhistorydao.findToutesEditionsParDateDemande(
            TemporaliteUtils.getDate(2000, 1, 1), APPLICATION_ORIGINE_1));
        verifierJobHistory(jobhistorydao.findToutesEditionsParDateDemande(
            TemporaliteUtils.getDate(2018, 1, 8), APPLICATION_ORIGINE_INCONNUE));

        // Appel de la méthode à tester, sur tous les éléments existants, et vérifications de leur conformité
        verifierJobHistory(jobhistorydao.findToutesEditionsParDateDemande(
            TemporaliteUtils.getDate(2018, 1, 13), APPLICATION_ORIGINE_1), true, mapJobHistory.get(13));
        verifierJobHistory(jobhistorydao.findToutesEditionsParDateDemande(
            TemporaliteUtils.getDate(2018, 1, 11), APPLICATION_ORIGINE_2), true, mapJobHistory.get(11));

        // Vérification de la limitation des résultats
        IntStream.rangeClosed(16, 150).forEach(indice -> enregistrer(getJobHistory(indice)));
        assertEquals(NombreMaxEdition.MAX_LIGNE_PAR_UID.getNbreMax(), jobhistorydao.findToutesEditionsParDateDemande(
            TemporaliteUtils.getDate(2018, 1, 28), APPLICATION_ORIGINE_1).size());
    }

    /**
     * Test de fr.gouv.finances.lombok.edition.dao.JobHistoryDao#findToutesEditionsParEtat( java.lang.String,
     * java.lang.String).
     */
    @Test
    public void findToutesEditionsParEtat()
    {
        // Appel de la méthode à tester sur des paramétres non initialisés et/ou vides, et vérifications
        VerificationExecution.verifierException(ProgrammationException.class, () -> jobhistorydao.findToutesEditionsParEtat(null, null));
        VerificationExecution.verifierException(ProgrammationException.class, () -> jobhistorydao.findToutesEditionsParEtat("", ""));
        VerificationExecution.verifierException(ProgrammationException.class,
            () -> jobhistorydao.findToutesEditionsParEtat(JobHistory.STATUS_DISPONIBLE, null));
        VerificationExecution.verifierException(ProgrammationException.class,
            () -> jobhistorydao.findToutesEditionsParEtat(null, APPLICATION_ORIGINE_1));

        // Appel de la méthode à tester, sans résultat, et vérifications
        verifierJobHistory(jobhistorydao.findToutesEditionsParEtat("INCONNU", APPLICATION_ORIGINE_1));
        verifierJobHistory(jobhistorydao.findToutesEditionsParEtat(
            JobHistory.STATUS_DISPONIBLE, APPLICATION_ORIGINE_INCONNUE));

        // Appel de la méthode à tester, sur tous les éléments existants, et vérifications de leur conformité
        verifierJobHistory(jobhistorydao.findToutesEditionsParEtat(
            JobHistory.STATUS_DISPONIBLE, APPLICATION_ORIGINE_1), true, mapJobHistory.get(15), mapJobHistory.get(14),
            mapJobHistory.get(13));
        verifierJobHistory(jobhistorydao.findToutesEditionsParEtat(
            JobHistory.STATUS_DISPONIBLE, APPLICATION_ORIGINE_2), true, mapJobHistory.get(12));
        verifierJobHistory(jobhistorydao.findToutesEditionsParEtat(
            JobHistory.STATUS_DISPONIBLE_NON_ENVOYE, APPLICATION_ORIGINE_2), true, mapJobHistory.get(11));
        verifierJobHistory(jobhistorydao.findToutesEditionsParEtat(
            JobHistory.STATUS_ECHEC, APPLICATION_ORIGINE_1));
    }

    /**
     * Test de fr.gouv.finances.lombok.edition.dao.JobHistoryDao#findToutesEditionsParPeriodeDateDemande(
     * java.util.Date, java.util.Date, java.lang.String).
     */
    @Test
    public void findToutesEditionsParPeriodeDateDemande()
    {
        // Appel de la méthode à tester sur des paramétres non initialisés et/ou vides, et vérifications
        VerificationExecution.verifierException(ProgrammationException.class,
            () -> jobhistorydao.findToutesEditionsParPeriodeDateDemande(null, null, null));
        VerificationExecution.verifierException(ProgrammationException.class,
            () -> jobhistorydao.findToutesEditionsParPeriodeDateDemande(null, null, ""));
        VerificationExecution.verifierException(ProgrammationException.class,
            () -> jobhistorydao.findToutesEditionsParPeriodeDateDemande(null, null, APPLICATION_ORIGINE_1));
        VerificationExecution.verifierException(ProgrammationException.class,
            () -> jobhistorydao.findToutesEditionsParPeriodeDateDemande(new Date(), null, APPLICATION_ORIGINE_1));
        VerificationExecution.verifierException(ProgrammationException.class,
            () -> jobhistorydao.findToutesEditionsParPeriodeDateDemande(null, new Date(), APPLICATION_ORIGINE_1));

        // Appel de la méthode à tester, sans résultat, et vérifications
        verifierJobHistory(jobhistorydao.findToutesEditionsParPeriodeDateDemande(
            TemporaliteUtils.getDate(2000, 1, 1), TemporaliteUtils.getDate(2001, 1, 1), APPLICATION_ORIGINE_1));
        verifierJobHistory(jobhistorydao.findToutesEditionsParPeriodeDateDemande(
            TemporaliteUtils.getDate(2018, 1, 8), TemporaliteUtils.getDate(2018, 1, 8), APPLICATION_ORIGINE_INCONNUE));

        // Appel de la méthode à tester, sur tous les éléments existants, et vérifications de leur conformité
        verifierJobHistory(jobhistorydao.findToutesEditionsParPeriodeDateDemande(
            TemporaliteUtils.getDate(2018, 1, 1), TemporaliteUtils.getDate(2018, 1, 14), APPLICATION_ORIGINE_1), true,
            mapJobHistory.get(14), mapJobHistory.get(13));
        verifierJobHistory(jobhistorydao.findToutesEditionsParPeriodeDateDemande(
            TemporaliteUtils.getDate(2018, 1, 1), TemporaliteUtils.getDate(2018, 1, 14), APPLICATION_ORIGINE_2), true,
            mapJobHistory.get(12), mapJobHistory.get(11));

        // Vérification de la limitation des résultats
        IntStream.rangeClosed(16, 150).forEach(indice -> enregistrer(getJobHistory(indice)));
        assertEquals(NombreMaxEdition.MAX_LIGNE_PAR_UID.getNbreMax(), jobhistorydao.findToutesEditionsParPeriodeDateDemande(
            TemporaliteUtils.getDate(2018, 1, 28), TemporaliteUtils.getDate(2018, 2, 1), APPLICATION_ORIGINE_1).size());
    }

    /**
     * Test de fr.gouv.finances.lombok.edition.dao.JobHistoryDao#findAndLoadJobHistory(java.lang.String).
     */
    @Test
    public void findAndLoadJobHistory()
    {
        // Appel de la méthode à tester sur des paramétres non initialisés et/ou vides, et vérifications
        VerificationExecution.verifierException(ProgrammationException.class, () -> jobhistorydao.findAndLoadJobHistory(null));
        VerificationExecution.verifierException(ProgrammationException.class, () -> jobhistorydao.findAndLoadJobHistory(""));

        // Appel de la méthode à tester, sans résultat, et vérifications
        assertNull(jobhistorydao.findAndLoadJobHistory(IDENTIFIANT_EDITION_INCONNU));

        // Appel de la méthode à tester, sur tous les éléments existants, et vérifications de leur conformité
        mapJobHistory.entrySet().forEach(entry -> verifierJobHistory(
            jobhistorydao.findAndLoadJobHistory(String.valueOf(entry.getKey())), entry.getValue()));
    }

    /**
     * Test de fr.gouv.finances.lombok.edition.dao.JobHistoryDao#saveContenuEditionFromByteArray(
     * fr.gouv.finances.lombok.edition.bean.JobHistory, byte[]).
     */
    @Test
    public void saveContenuEditionFromByteArray()
    {
        // Appel de la méthode à tester sur des paramétres non initialisés et/ou vides, et vérifications
        VerificationExecution.verifierException(ProgrammationException.class,
            () -> jobhistorydao.saveContenuEditionFromByteArray(null, null));
        VerificationExecution.verifierException(ProgrammationException.class,
            () -> jobhistorydao.saveContenuEditionFromByteArray(new JobHistory(), null));
        VerificationExecution.verifierException(ProgrammationException.class,
            () -> jobhistorydao.saveContenuEditionFromByteArray(null, "test".getBytes()));
        VerificationExecution.verifierException(ProgrammationException.class,
            () -> jobhistorydao.saveContenuEditionFromByteArray(new JobHistory(), new byte[] {}));
        VerificationExecution.verifierException(ProgrammationException.class,
            () -> jobhistorydao.saveContenuEditionFromByteArray(new JobHistory(), "test 2".getBytes()));
        VerificationExecution.verifierException(ProgrammationException.class,
            () -> jobhistorydao.saveContenuEditionFromByteArray(mapJobHistory.get(4), new byte[] {}));
        VerificationExecution.verifierException(ProgrammationException.class,
            () -> jobhistorydao.saveContenuEditionFromByteArray(getJobHistory(11), new byte[] {}));

        // Appel de la méthode à tester, sur tous les éléments existants, et vérifications de leur conformité
        mapJobHistory.entrySet().forEach(entry -> {
            final int VARIATION_DONNEES = 10;
            sauvegarderContenuByteArray(entry.getValue(), VARIATION_DONNEES);
            verifierContenu(entry.getValue().getEditionUuid(), VARIATION_DONNEES);
        });
    }

    private void sauvegarderContenuByteArray(JobHistory jobHistory, int variationDonnees)
    {
        if (jobHistory.getStockageEdition() != null)
        {
            jobHistory.getStockageEdition().setTailleFichier(
                jobHistory.getStockageEdition().getTailleFichier() + variationDonnees);
        }

        // Sauvegarde d'un contenu au sein d'un historique de travaux d'édition
        jobhistorydao.saveContenuEditionFromByteArray(
            jobHistory, (SUFFIXE_CONTENU + variationDonnees + jobHistory.getEditionUuid()).getBytes());
    }

    private void verifierContenu(String editionUuid, int variationDonnees)
    {
        // Chargement du contenu de l'historique de travaux d'édition
        JobHistory jobHistory = jobhistorydao.chargerContenuEdition(jobhistorydao.findJobHistory(editionUuid));

        // Vérification du contenu + données modifiées
        assertNotNull(jobHistory.getStockageEdition());
        assertNotNull(jobHistory.getStockageEdition().getLeContenuEdition());
        assertNotNull(jobHistory.getStockageEdition().getLeContenuEdition().getData());
        assertEquals(SUFFIXE_CONTENU + variationDonnees + jobHistory.getEditionUuid(),
            new String(jobHistory.getStockageEdition().getLeContenuEdition().getData()));
        verifierTailleFichier(jobHistory, variationDonnees);
    }

    /**
     * Test de fr.gouv.finances.lombok.edition.dao.JobHistoryDao#saveContenuEditionFromFile(
     * fr.gouv.finances.lombok.edition.bean.JobHistory, java.io.File).
     */
    @Test
    public void saveContenuEditionFromFile()
    {
        // Paramétrage du comportement du bouchon pour rendre un fichier non lisible
        when(fichierNonLisible.canRead()).thenReturn(false);
        // Initialisation d'un fichier supprimé
        File fichierSupprime = FileUtils.getFichierTemporaire();
        fichierSupprime.delete();

        // Initialisation d'un fichier
        File fichier = FileUtils.getFichierTemporaire("test fichier");

        // Appel de la méthode à tester sur des paramétres non initialisés et/ou vides, et vérifications
        VerificationExecution.verifierException(ProgrammationException.class, () -> jobhistorydao.saveContenuEditionFromFile(null, null));
        VerificationExecution.verifierException(ProgrammationException.class,
            () -> jobhistorydao.saveContenuEditionFromFile(new JobHistory(), null));
        VerificationExecution.verifierException(ProgrammationException.class,
            () -> jobhistorydao.saveContenuEditionFromFile(null, fichier));
        VerificationExecution.verifierException(ProgrammationException.class, () -> jobhistorydao.saveContenuEditionFromFile(
            new JobHistory(), FileUtils.getFichierTemporaire()));
        VerificationExecution.verifierException(ProgrammationException.class,
            () -> jobhistorydao.saveContenuEditionFromFile(new JobHistory(), fichier));
        VerificationExecution.verifierException(ProgrammationException.class, () -> jobhistorydao.saveContenuEditionFromFile(
            mapJobHistory.get(4), FileUtils.getFichierTemporaire()));
        VerificationExecution.verifierException(ProgrammationException.class,
            () -> jobhistorydao.saveContenuEditionFromFile(mapJobHistory.get(4), fichierSupprime));
        VerificationExecution.verifierException(ProgrammationException.class,
            () -> jobhistorydao.saveContenuEditionFromFile(mapJobHistory.get(4), fichierNonLisible));
        VerificationExecution.verifierException(ProgrammationException.class,
            () -> jobhistorydao.saveContenuEditionFromFile(getJobHistory(11), fichier));

        // Appel de la méthode à tester, sur tous les éléments existants, et vérifications de leur conformité
        mapJobHistory.entrySet().forEach(entry -> {
            final int VARIATION_DONNEES = 10;
            sauvegarderContenuFile(entry.getValue(), VARIATION_DONNEES);
            verifierContenu(entry.getValue().getEditionUuid(), VARIATION_DONNEES);
        });
    }

    private void sauvegarderContenuFile(JobHistory jobHistory, int variationDonnees)
    {
        if (jobHistory.getStockageEdition() != null)
        {
            jobHistory.getStockageEdition().setTailleFichier(
                jobHistory.getStockageEdition().getTailleFichier() + variationDonnees);
        }

        // Sauvegarde d'un contenu au sein d'un historique de travaux d'édition
        jobhistorydao.saveContenuEditionFromFile(jobHistory,
            FileUtils.getFichierTemporaire(SUFFIXE_CONTENU + variationDonnees + jobHistory.getEditionUuid()));
    }

    /**
     * Test de fr.gouv.finances.lombok.edition.dao.JobHistoryDao#chargerContenuEdition(
     * fr.gouv.finances.lombok.edition.bean.JobHistory).
     */
    @Test
    public void chargerContenuEdition()
    {
        // Exécution de la méthode à tester, sans édition
        VerificationExecution.verifierException(ProgrammationException.class, () -> jobhistorydao.chargerContenuEdition(null));

        // Exécution de la méthode à tester, avec une édition sans aucune valeur
        VerificationExecution.verifierException(ProgrammationException.class, () -> jobhistorydao.chargerContenuEdition(new JobHistory()));

        // Exécution de la méthode à tester, avec une édition non chargée de la BDD
        VerificationExecution.verifierException(ProgrammationException.class, () -> jobhistorydao.chargerContenuEdition(getJobHistory(5)));

        // Exécution de la méthode à tester, avec une édition sans information de stockage
        VerificationExecution.verifierException(ProgrammationException.class,
            () -> jobhistorydao.chargerContenuEdition(mapJobHistory.get(1)));

        // Exécution de la méthode à tester, avec une édition présente en BDD mais sans contenu
        VerificationExecution.verifierException(ProgrammationException.class,
            () -> jobhistorydao.chargerContenuEdition(mapJobHistory.get(8)));

        // Exécution de la méthode à tester, avec une édition présente en BDD mais sans valeur de contenu
        VerificationExecution.verifierException(ProgrammationException.class,
            () -> jobhistorydao.chargerContenuEdition(mapJobHistory.get(9)));

        // Exécution de la méthode à tester
        IntStream.of(2, 3).forEach(indice -> verificationDonnees(indice));
        // Exécution de la méthode à tester, avec des éditions présentes en BDD possédant du contenu
        IntStream.rangeClosed(10, 15).forEach(indice -> verificationDonnees(indice));
    }

    private void verificationDonnees(int indice)
    {
        JobHistory jobHistory = mapJobHistory.get(indice);
        assertNotNull(jobHistory.getStockageEdition().getLeContenuEdition().getData());
        assertEquals(SUFFIXE_CONTENU + jobHistory.getEditionUuid(),
            new String(jobHistory.getStockageEdition().getLeContenuEdition().getData()));
    }

    /**
     * Test de fr.gouv.finances.lombok.edition.dao.JobHistoryDao#chargerContenuEditionToBytes(
     * fr.gouv.finances.lombok.edition.bean.JobHistory).
     */
    @Test
    public void chargerContenuEditionToBytes()
    {
        // Exécution de la méthode à tester, sans édition
        VerificationExecution.verifierException(ProgrammationException.class, () -> jobhistorydao.chargerContenuEditionToBytes(null));

        // Exécution de la méthode à tester, avec une édition sans aucune valeur
        VerificationExecution.verifierException(ProgrammationException.class,
            () -> jobhistorydao.chargerContenuEditionToBytes(new JobHistory()));

        // Exécution de la méthode à tester, avec une édition non chargée de la BDD
        VerificationExecution.verifierException(ProgrammationException.class,
            () -> jobhistorydao.chargerContenuEditionToBytes(getJobHistory(5)));

        // Exécution de la méthode à tester, avec une édition sans information de stockage
        VerificationExecution.verifierException(RegleGestionException.class,
            () -> jobhistorydao.chargerContenuEditionToBytes(mapJobHistory.get(1)));

        // Exécution de la méthode à tester, avec une édition possédant un mauvais statut
        VerificationExecution.verifierException(RegleGestionException.class,
            () -> jobhistorydao.chargerContenuEditionToBytes(mapJobHistory.get(2)));

        // Exécution de la méthode à tester, sans taille de fichier
        VerificationExecution.verifierException(RegleGestionException.class,
            () -> jobhistorydao.chargerContenuEditionToBytes(mapJobHistory.get(3)));

        // Exécution de la méthode à tester, avec une édition présente en BDD mais sans contenu
        VerificationExecution.verifierException(ProgrammationException.class,
            () -> jobhistorydao.chargerContenuEditionToBytes(mapJobHistory.get(8)));

        // Exécution de la méthode à tester, avec une édition présente en BDD mais sans valeur de contenu
        VerificationExecution.verifierException(ProgrammationException.class,
            () -> jobhistorydao.chargerContenuEditionToBytes(mapJobHistory.get(9)));

        // Exécution de la méthode à tester, avec des éditions présentes en BDD possédant du contenu
        IntStream.rangeClosed(10, 15).forEach(
            indice -> verifierChargerContenuEditionToBytes(mapJobHistory.get(indice)));
    }

    /**
     * Test de fr.gouv.finances.lombok.edition.dao.JobHistoryDao#chargerContenuEditionToFile(
     * fr.gouv.finances.lombok.edition.bean.JobHistory).
     */
    @Test
    public void chargerContenuEditionToFile()
    {
        // Exécution de la méthode à tester, sans édition
        VerificationExecution.verifierException(ProgrammationException.class, () -> jobhistorydao.chargerContenuEditionToFile(null));

        // Exécution de la méthode à tester, avec une édition sans aucune valeur
        VerificationExecution.verifierException(ProgrammationException.class,
            () -> jobhistorydao.chargerContenuEditionToFile(new JobHistory()));

        // Exécution de la méthode à tester, avec une édition non chargée de la BDD
        VerificationExecution.verifierException(ProgrammationException.class,
            () -> jobhistorydao.chargerContenuEditionToFile(getJobHistory(5)));

        // Exécution de la méthode à tester, avec une édition sans information de stockage
        VerificationExecution.verifierException(RegleGestionException.class,
            () -> jobhistorydao.chargerContenuEditionToFile(mapJobHistory.get(1)));

        // Exécution de la méthode à tester, avec une édition possédant un mauvais statut
        VerificationExecution.verifierException(RegleGestionException.class,
            () -> jobhistorydao.chargerContenuEditionToFile(mapJobHistory.get(2)));

        // Exécution de la méthode à tester, sans taille de fichier
        VerificationExecution.verifierException(RegleGestionException.class,
            () -> jobhistorydao.chargerContenuEditionToFile(mapJobHistory.get(3)));

        // Exécution de la méthode à tester, avec une édition présente en BDD mais sans contenu
        VerificationExecution.verifierException(ProgrammationException.class,
            () -> jobhistorydao.chargerContenuEditionToFile(mapJobHistory.get(8)));

        // Exécution de la méthode à tester, avec une édition présente en BDD mais sans valeur de contenu
        VerificationExecution.verifierException(ProgrammationException.class,
            () -> jobhistorydao.chargerContenuEditionToFile(mapJobHistory.get(9)));

        // Exécution de la méthode à tester, avec des éditions présentes en BDD possédant du contenu
        IntStream.rangeClosed(10, 15).forEach(
            indice -> verifierChargerContenuEditionToFile(mapJobHistory.get(indice)));
    }

    /**
     * Méthode permettant de vérifier le chargement du contenu (sous la forme tableau d'octet) de l'historique de
     * gestion de l'édition.
     *
     * @param jobHistory historique des travaux d'édition à vérifier
     */
    private void verifierChargerContenuEditionToBytes(JobHistory jobHistory)
    {
        // Appel de la méthode à tester
        jobhistorydao.chargerContenuEditionToBytes(jobHistory);
        // Le contenu est maintenant chargé
        verifierContenuByteCharge(jobHistory);
    }

    /**
     * Méthode permettant d'effectuer le chargement du contenu de l'historique (sous la forme fichier) de gestion de
     * l'édition.
     *
     * @param jobHistory historique des travaux d'édition dont on veut charger l'historique
     */
    private void verifierChargerContenuEditionToFile(JobHistory jobHistory)
    {
        // Appel de la méthode à tester
        jobhistorydao.chargerContenuEditionToFile(jobHistory);
        // Le contenu est maintenant chargé
        verifierContenuFileCharge(jobHistory);
    }

    /**
     * Méthode permettant de vérifier le chargement du contenu de l'historique (sous la forme fichier) de gestion de
     * l'édition.
     *
     * @param jobHistory historique des travaux d'édition à vérifier
     */
    private void verifierContenuFileCharge(JobHistory jobHistory)
    {
        // Vérification que le contenu est présent, avec la bonne valeur
        assertNotNull(jobHistory.getStockageEdition().getLeContenuEdition().getData());
        assertNotNull(jobHistory.getStockageEdition().getLeContenuEdition().getDataFile());
        assertTrue(jobHistory.getStockageEdition().getLeContenuEdition().getDataFile().exists());
        assertTrue(jobHistory.getStockageEdition().getLeContenuEdition().getDataFile().isFile());
        // Vérification du contenu du fichier temporaire
        try
        {
            assertEquals(SUFFIXE_CONTENU + jobHistory.getEditionUuid(), new String(Files.readAllBytes(
                jobHistory.getStockageEdition().getLeContenuEdition().getDataFile().toPath())));
        }
        catch (IOException ioe)
        {
            fail("Erreur de lecture du fichier '"
                + jobHistory.getStockageEdition().getLeContenuEdition().getDataFile().getAbsolutePath()
                + "' ");
        }

        // Vérification que le flux byte n'a été créé
        assertNull(jobHistory.getStockageEdition().getLeContenuEdition().getDataByte());
    }

    /**
     * Méthode permettant de vérifier le chargement du contenu de l'historique (sous la forme de tabeau d'octet) de
     * gestion de l'édition.
     *
     * @param jobHistory historique des travaux d'édition à vérifier
     */
    private void verifierContenuByteCharge(JobHistory jobHistory)
    {
        // Vérification que le contenu est présent, avec la bonne valeur
        assertNotNull(jobHistory.getStockageEdition().getLeContenuEdition().getData());
        assertNotNull(jobHistory.getStockageEdition().getLeContenuEdition().getDataByte());
        // Vérification du contenu du flux
        assertEquals(SUFFIXE_CONTENU + jobHistory.getEditionUuid(),
            new String(jobHistory.getStockageEdition().getLeContenuEdition().getDataByte()));

        // Vérification qu'aucun fichier n'a été créé
        assertNull(jobHistory.getStockageEdition().getLeContenuEdition().getDataFile());
    }

    /**
     * Méthode permettant de sauvegarder un historique des travaux d'édition.
     *
     * @param jobHistory historique à intégrer
     */
    private void enregistrer(JobHistory jobHistory)
    {
        if (jobHistory.getStockageEdition() != null
            && jobHistory.getStockageEdition().getLeContenuEdition() != null)
        {
            jobhistorydao.saveObject(jobHistory.getStockageEdition().getLeContenuEdition());
        }
        jobhistorydao.saveObject(jobHistory);
    }

    /**
     * Méthode permettant de comparer une liste d'historique des travaux d'édition avec un attendu.
     *
     * @param listeJobHistory liste à comparer
     * @param jobHistoryAttendu liste attendue
     */
    private void verifierJobHistory(Collection<JobHistory> listeJobHistory, JobHistory... jobHistoryAttendu)
    {
        verifierJobHistory(listeJobHistory, false, jobHistoryAttendu);
    }

    /**
     * Méthode permettant de comparer une liste d'historique des travaux d'édition avec un attendu, éventuellement
     * triée.
     *
     * @param listeJobHistory liste à comparer
     * @param isOrdre l'ordre doit-il être respectée ?
     * @param jobHistoryAttendu liste attendue
     */
    private void verifierJobHistory(Collection<JobHistory> listeJobHistory, boolean isOrdre, JobHistory... jobHistoryAttendu)
    {
        // Vérification des historiques des travaux d'édition par rapport à ceux attendus
        ControleDonnees.verifierElements(listeJobHistory, isOrdre, jobHistoryAttendu);

    }

    /**
     * Méthode permettant de comparer un historique des travaux d'édition avec un attendu.
     *
     * @param jobHistory historique à comparer
     * @param jobHistoryAttendu historique attendu
     */
    private void verifierJobHistory(JobHistory jobHistory, JobHistory jobHistoryAttendu)
    {
        // Vérification de l'historique des travaux d'édition par rapport à celui attendu
        assertEquals(jobHistory, jobHistoryAttendu);

    }

    /**
     * Méthode permettant de générer un historique de travaux d'édition.
     *
     * @param indice indice à utiliser dans les informations de l'historique
     * @return historique de travaux d'édition généré
     */
    private JobHistory getJobHistory(int indice)
    {
        JobHistory jobHistory = new JobHistory();
        String indiceString = String.valueOf(indice);

        // L'UID de l'édition est l'indice fourni
        jobHistory.setEditionUuid(indiceString);
        jobHistory.getOrdoEdition().setStartTime(TemporaliteUtils.getDate(2018, 2, 1));
        int jourMax = 28;
        if (indice < 29)
        {
            jourMax = indice;
        }
        jobHistory.getOrdoEdition().setEndTime(TemporaliteUtils.getDate(2018, 2, jourMax));

        jobHistory.getStockageEdition().setTailleFichier(new Long(indice));

        ContenuEdition contenuEdition = new ContenuEdition();
        contenuEdition.setData((SUFFIXE_CONTENU + indiceString).getBytes());
        jobHistory.getStockageEdition().setLeContenuEdition(contenuEdition);

        // Affectation de différentes valeurs suivant l'indice
        switch (indice)
        {
            case 1:
                // Création d'un historique sans information de stockage
                jobHistory.setStockageEdition(null);
                break;
            case 2:
                // Création d'un historique sans information de présentation
                jobHistory.setPresentationEdition(null);
                break;
            case 3:
                // Création d'un historique sans information d'archivage
                jobHistory.setArchivageEdition(null);
                break;
            case 4:
                // Création d'un historique sans information de courriel
                jobHistory.setMailEdition(null);
                break;
            case 5:
                // Création d'un historique sans information de destination
                jobHistory.setDestinationEdition(null);
                break;
            case 6:
                // Création d'un historique sans information d'ordonnancement
                jobHistory.setOrdoEdition(null);
                break;
            case 7:
                // Création d'un historique au statut échec, non lié à une application
                jobHistory.getOrdoEdition().setStatus(JobHistory.STATUS_ECHEC);
                break;
            case 8:
                // Création d'un historique au statut disponible, sans contenu, non lié à une application
                jobHistory.getOrdoEdition().setStatus(JobHistory.STATUS_DISPONIBLE);
                jobHistory.getStockageEdition().setLeContenuEdition(null);
                break;
            case 9:
                // Création d'un historique au statut disponible, sans contenu, non lié à une application
                jobHistory.getOrdoEdition().setStatus(JobHistory.STATUS_DISPONIBLE);
                jobHistory.getStockageEdition().setLeContenuEdition(new ContenuEdition());
                break;
            case 10:
                // Création d'un historique au statut disponible, non lié à une application
                jobHistory.getOrdoEdition().setStatus(JobHistory.STATUS_DISPONIBLE);
                break;
            case 11:
                jobHistory.getOrdoEdition().setStatus(JobHistory.STATUS_DISPONIBLE_NON_ENVOYE);
                jobHistory.getDestinationEdition().setApplicationOrigine(APPLICATION_ORIGINE_2);
                jobHistory.setBeanEditionId(IDENTIFIANT_EDITION);
                jobHistory.getDestinationEdition().setUidProprietaire(UID_PROPRIETAIRE);
                jobHistory.getOrdoEdition().setDateDemande(TemporaliteUtils.getDate(2018, 1, jourMax));
                break;
            case 12:
                jobHistory.getOrdoEdition().setStatus(JobHistory.STATUS_DISPONIBLE);
                jobHistory.getDestinationEdition().setApplicationOrigine(APPLICATION_ORIGINE_2);
                jobHistory.setBeanEditionId(IDENTIFIANT_EDITION);
                jobHistory.getDestinationEdition().setUidProprietaire(UID_PROPRIETAIRE);
                jobHistory.getOrdoEdition().setDateDemande(TemporaliteUtils.getDate(2018, 1, jourMax));
                jobHistory.getDestinationEdition().setProfilsDesti(
                    new HashSet<ProfilDesti>(Arrays.asList(getProfil(1))));
                break;
            case 13:
                jobHistory.getOrdoEdition().setStatus(JobHistory.STATUS_DISPONIBLE);
                jobHistory.getDestinationEdition().setApplicationOrigine(APPLICATION_ORIGINE_1);
                jobHistory.setBeanEditionId(IDENTIFIANT_EDITION);
                jobHistory.getDestinationEdition().setUidProprietaire(UID_PROPRIETAIRE);
                jobHistory.getDestinationEdition().setNouvelleEdition(false);
                jobHistory.getOrdoEdition().setDateDemande(TemporaliteUtils.getDate(2018, 1, jourMax));
                jobHistory.getDestinationEdition().getProfilsDesti().add(getProfil(2));
                break;
            case 14:
                jobHistory.getOrdoEdition().setStatus(JobHistory.STATUS_DISPONIBLE);
                jobHistory.getDestinationEdition().setApplicationOrigine(APPLICATION_ORIGINE_1);
                jobHistory.setBeanEditionId(IDENTIFIANT_EDITION);
                jobHistory.getDestinationEdition().setUidProprietaire(UID_PROPRIETAIRE);
                jobHistory.getDestinationEdition().setMonoDestinataire(false);
                jobHistory.getOrdoEdition().setDateDemande(TemporaliteUtils.getDate(2018, 1, jourMax));
                jobHistory.getDestinationEdition().setProfilsDesti(
                    new HashSet<ProfilDesti>(Arrays.asList(getProfil(1), getProfil(2))));
                break;
            case 15:
                jobHistory.getOrdoEdition().setStatus(JobHistory.STATUS_DISPONIBLE);
                jobHistory.getDestinationEdition().setApplicationOrigine(APPLICATION_ORIGINE_1);
                jobHistory.setBeanEditionId(IDENTIFIANT_EDITION_2);
                jobHistory.getDestinationEdition().setUidProprietaire(UID_PROPRIETAIRE_2);
                jobHistory.getOrdoEdition().setDateDemande(TemporaliteUtils.getDate(2018, 1, jourMax));
                break;
            default:
                jobHistory.getOrdoEdition().setStatus(JobHistory.STATUS_DISPONIBLE);
                jobHistory.getDestinationEdition().setApplicationOrigine(APPLICATION_ORIGINE_1);
                jobHistory.setBeanEditionId(IDENTIFIANT_EDITION);
                jobHistory.getDestinationEdition().setUidProprietaire(UID_PROPRIETAIRE);
                if (indice <= 12)
                {
                    jobHistory.getOrdoEdition().setDateDemande(
                        TemporaliteUtils.getDate(2018, 1, jourMax));
                }
                else
                {
                    jobHistory.getOrdoEdition().setDateDemande(TemporaliteUtils.getDate(2018, 1, 28, 10, 0, jourMax));
                    jobHistory.getDestinationEdition().setMonoDestinataire(false);
                }
        }

        return jobHistory;
    }

    /**
     * Méthode permettant de générer un profil.
     *
     * @param indice indice à utiliser dans les informations du profil
     * @return profil généré
     */
    private ProfilDesti getProfil(int indice)
    {
        ProfilDesti profilDesti = new ProfilDesti();

        switch (indice)
        {
            case 3:
            case 2:
                profilDesti.setNomProfil(SUFFIXE_PROFIL + indice);
                profilDesti.setListeFiltres(
                    new HashSet<>(Arrays.asList(getFiltreEdition(1), getFiltreEdition(2))));
                break;
            default:
                profilDesti.setNomProfil(SUFFIXE_PROFIL + 1);
                profilDesti.setListeFiltres(
                    new HashSet<>(Arrays.asList(getFiltreEdition(3))));
        }

        return profilDesti;
    }

    /**
     * Méthode permettant de générer un filtre d'édition.
     *
     * @param indice indice à utiliser dans les informations du filtre
     * @return filtre généré
     */
    private FiltreEdition getFiltreEdition(int indice)
    {
        FiltreEdition filtreEdition = new FiltreEdition();

        switch (indice)
        {
            default:
                filtreEdition.setNomDuFiltre("Nom " + indice);
                filtreEdition.setValsFiltre(new HashSet<FiltreValue>(Arrays.asList(getFiltreValue(1))));
        }

        return filtreEdition;
    }

    /**
     * Méthode permettant de générer la valeur d'un filtre d'édition.
     *
     * @param indice indice à utiliser dans les informations du filtre
     * @return valeur du filtre d'édition généré
     */
    private FiltreValue getFiltreValue(int indice)
    {
        FiltreValue filtreValue = new FiltreValue();

        switch (indice)
        {
            default:
                filtreValue.setValue("val " + indice);
        }

        return filtreValue;
    }

}