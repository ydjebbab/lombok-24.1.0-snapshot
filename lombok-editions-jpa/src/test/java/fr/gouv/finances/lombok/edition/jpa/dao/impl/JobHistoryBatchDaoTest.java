package fr.gouv.finances.lombok.edition.jpa.dao.impl;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.data.jpa.JpaRepositoriesAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import fr.gouv.finances.lombok.edition.bean.JobHistory;
import fr.gouv.finances.lombok.edition.config.PersistenceJPAConfigTest;
import fr.gouv.finances.lombok.edition.dao.JobHistoryBatchDao;
import fr.gouv.finances.lombok.test.controle.ControleDonnees;
import fr.gouv.finances.lombok.test.controle.VerificationExecution;
import fr.gouv.finances.lombok.util.base.PurgeTablesDao;
import fr.gouv.finances.lombok.util.date.TemporaliteUtils;
import fr.gouv.finances.lombok.util.exception.ProgrammationException;
import fr.gouv.finances.lombok.util.persistance.ScrollIterator;

/**
 * Tests unitaires automatisés du DAO JPA gérant les données des batchs liés aux historiques des travaux d'édition.
 *
 * @author Christophe Breheret-Girardin
 * @author CF
 */
@ComponentScan(basePackages = {"fr.gouv.finances.lombok.edition.jpa.dao.impl, fr.gouv.finances.lombok.edition.config"})
@EnableAutoConfiguration(exclude = {JpaRepositoriesAutoConfiguration.class})
@SpringBootTest(classes = {PersistenceJPAConfigTest.class}, properties = {"lombok.orm.jpa=true"})
@ActiveProfiles(profiles = {"edition", "embedded"})
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class JobHistoryBatchDaoTest
{
    public JobHistoryBatchDaoTest()
    {
        super();
    }

    protected static final Logger LOGGER = LoggerFactory.getLogger(JobHistoryBatchDaoTest.class);

    /** Déclaration du DAO gérant les données des batchs liés aux historiques des travaux d'édition. */
    @Autowired
    @Qualifier("jobhistorybatchdaoimpl")
    private JobHistoryBatchDao jobhistorybatchdao;

    /** Déclaration du DAO de gestion des purges de la base. */
    @Autowired
    private PurgeTablesDao purgetableseditiondao;

    /** Initialisation d'une règle sur chaque test. */
    @Rule
    public TestName testUnitaire = new TestName();

    /** Constante donnant le suffixe d'une libellé d'une application d'origine */
    private static final String SUFFIXE_APPLICATION_ORIGINE = "APPLICATION ORIGINE ";

    /** Constante donnant le libellé d'une 1ère application d'origine */
    private static final String APPLICATION_ORIGINE_1 = SUFFIXE_APPLICATION_ORIGINE + 1;

    /** Constante donnant le libellé d'une 2ème application d'origine */
    private static final String APPLICATION_ORIGINE_2 = SUFFIXE_APPLICATION_ORIGINE + 2;

    /** Constante donnant le libellé d'une application d'origine non utilisée dans les jeux d'essai */
    private static final String APPLICATION_ORIGINE_INCONNUE = SUFFIXE_APPLICATION_ORIGINE + 3;

    /** Map contenant les historiques des travaux d'édition présents en base */
    private Map<Integer, JobHistory> mapJobHistory;

    /** Taille de bloc à utiliser par défaut */
    private static final int TAILLE_BLOC = 20;

    /**
     * Initialisation des données en base avant les tests.
     */
    @Before
    public void creerDonnees()
    {
        LOGGER.debug("Début d'initialisation du test '{}'", testUnitaire.getMethodName());

        // Purge des données de la base
        purgetableseditiondao.purgerTables("ZFILTREVALUE_ZFIV", "ZFILTREEDITION_ZFIE", "ZPROFILDESTI_ZPRD", "ZJOBHISTORY_ZJOH");

        // Vérification de l'absence de données suite à la purge
        ControleDonnees.verifierElements(jobhistorybatchdao.loadAllObjects(JobHistory.class));

        // Génération des paramètres de journalisation, insertion en base de données et sauvegarde dans une Map
        mapJobHistory = IntStream.rangeClosed(1, 10).mapToObj(this::getJobHistory)
            .peek(jobhistorybatchdao::saveObject)
            .collect(Collectors.toMap(j -> Integer.parseInt(j.getEditionUuid()), j -> j));

        // Vérification des données à partir de l'image de la base
        ControleDonnees.verifierElements(jobhistorybatchdao.loadAllObjects(JobHistory.class), mapJobHistory.get(1), mapJobHistory.get(2),
            mapJobHistory.get(3), mapJobHistory.get(4), mapJobHistory.get(5), mapJobHistory.get(6), mapJobHistory.get(7),
            mapJobHistory.get(8), mapJobHistory.get(9), mapJobHistory.get(10));

        LOGGER.debug("Fin d'initialisation du test '{}'", testUnitaire.getMethodName());
    }

    /**
     * Méthode permettant de tester l'injection du DAO.
     */
    @Test
    // @Transactional("editionTransactionManager")
    public final void injectionDao()
    {
        // Vérification
        assertNotNull("DAO non injecté dans le test", jobhistorybatchdao);
    }

    /**
     * Vérification de l'appel à fr.gouv.finances.lombok.edition.dao.JobHistoryBatchDao#listeEditionAPurgerIterator(
     * int, java.util.Date, java.lang.String).
     */
    @Test
    public final void listeEditionAPurgerIterator()
    {
        // Appel de la méthode à tester sur des paramétres non initialisés et/ou vides, et vérifications
        VerificationExecution.verifierException(ProgrammationException.class,
            () -> jobhistorybatchdao.listeEditionAPurgerIterator(TAILLE_BLOC, null, null));
        VerificationExecution.verifierException(ProgrammationException.class,
            () -> jobhistorybatchdao.listeEditionAPurgerIterator(TAILLE_BLOC, TemporaliteUtils.getTimestamp(2017, 1, 9), null));
        VerificationExecution.verifierException(ProgrammationException.class,
            () -> jobhistorybatchdao.listeEditionAPurgerIterator(TAILLE_BLOC, TemporaliteUtils.getTimestamp(2017, 1, 9), ""));
        VerificationExecution.verifierException(ProgrammationException.class,
            () -> jobhistorybatchdao.listeEditionAPurgerIterator(TAILLE_BLOC, null, APPLICATION_ORIGINE_1));

        // Appel de la méthode à tester, sans résultat, et vérifications
        ScrollIterator scrollIterator = jobhistorybatchdao
            .listeEditionAPurgerIterator(TAILLE_BLOC, TemporaliteUtils.getTimestamp(2017, 1, 9), APPLICATION_ORIGINE_INCONNUE);
        assertFalse("Aucun élément ne devrait être trouvé", scrollIterator.hasNext());
        scrollIterator.close();

        scrollIterator = jobhistorybatchdao
            .listeEditionAPurgerIterator(TAILLE_BLOC, TemporaliteUtils.getTimestamp(2016, 1, 9), APPLICATION_ORIGINE_1);
        assertFalse("Aucun élément ne devrait être trouvé", scrollIterator.hasNext());
        scrollIterator.close();

        // Récupération des documents en mode curseur,
        // possible par l'encapsulation complète de la méthode dans une transaction (@Transactional).
        scrollIterator = jobhistorybatchdao
            .listeEditionAPurgerIterator(TAILLE_BLOC, TemporaliteUtils.getTimestamp(2017, 1, 9), APPLICATION_ORIGINE_1);

        // Récupération des élements
        List<JobHistory> liste = getJobHistory(scrollIterator);

        // Vérification de la cohérence des résultats
        ControleDonnees.verifierElements(liste, mapJobHistory.get(1), mapJobHistory.get(5), mapJobHistory.get(6), mapJobHistory.get(7),
            mapJobHistory.get(8), mapJobHistory.get(9));
    }

    /**
     * Vérification de l'appel à fr.gouv.finances.lombok.edition.dao.JobHistoryBatchDao#listeEditionAPurgerIterator(
     * int, java.util.Date, java.lang.String).
     */
    @Test
    public final void listeEditionAPurgerIteratorClose()
    {
        ScrollIterator scrollIterator = jobhistorybatchdao
            .listeEditionAPurgerIterator(TAILLE_BLOC, TemporaliteUtils.getTimestamp(2017, 1, 9), APPLICATION_ORIGINE_1);

        // Récupération des documents en mode curseur,
        // possible par l'encapsulation complète de la méthode dans une transaction (@Transactional).
        List<JobHistory> liste = new ArrayList<>();
        liste.add((JobHistory) scrollIterator.nextObjetMetier());

        // Fermeture après n'avoir récupéré qu'un seul élément
        scrollIterator.close();

        // Vérification de l'arrêt de la recherche
        verifierArretRecherche(scrollIterator);

        // Vérification de la cohérence des résultats
        ControleDonnees.verifierElements(liste, mapJobHistory.get(1));
    }

    /**
     * Vérification de l'appel à
     * fr.gouv.finances.lombok.edition.dao.JobHistoryBatchDao#listeEditionsDisponiblesIterator( int, java.lang.String).
     */
    @Test
    public final void listeEditionsDisponiblesIterator()
    {
        // Appel de la méthode à tester sur des paramétres non initialisés et/ou vides, et vérifications
        VerificationExecution.verifierException(ProgrammationException.class,
            () -> jobhistorybatchdao.listeEditionsDisponiblesIterator(TAILLE_BLOC, null));
        VerificationExecution.verifierException(ProgrammationException.class,
            () -> jobhistorybatchdao.listeEditionsDisponiblesIterator(TAILLE_BLOC, ""));

        // Appel de la méthode à tester, sans résultat, et vérifications
        ScrollIterator scrollIterator = jobhistorybatchdao
            .listeEditionsDisponiblesIterator(TAILLE_BLOC, APPLICATION_ORIGINE_INCONNUE);
        assertFalse("Aucun élément ne devrait être trouvé", scrollIterator.hasNext());
        scrollIterator.close();

        // Récupération des documents en mode curseur,
        // possible par l'encapsulation complète de la méthode dans une transaction (@Transactional).
        scrollIterator = jobhistorybatchdao
            .listeEditionsDisponiblesIterator(TAILLE_BLOC, APPLICATION_ORIGINE_1);

        // Récupération des élements
        List<JobHistory> liste = getJobHistory(scrollIterator);

        // Vérification de la cohérence des résultats
        ControleDonnees.verifierElements(liste, mapJobHistory.get(1), mapJobHistory.get(5), mapJobHistory.get(8), mapJobHistory.get(9),
            mapJobHistory.get(10));
    }

    /**
     * Vérification de l'appel à fr.gouv.finances.lombok.edition.dao.JobHistoryBatchDao#listeEditionsEnEchecIterator(
     * int, java.lang.String).
     */
    @Test
    public final void listeEditionsEnEchecIterator()
    {
        // Appel de la méthode à tester sur des paramétres non initialisés et/ou vides, et vérifications
        VerificationExecution.verifierException(ProgrammationException.class,
            () -> jobhistorybatchdao.listeEditionsEnEchecIterator(TAILLE_BLOC, null));
        VerificationExecution.verifierException(ProgrammationException.class,
            () -> jobhistorybatchdao.listeEditionsEnEchecIterator(TAILLE_BLOC, ""));

        // Appel de la méthode à tester, sans résultat, et vérifications
        ScrollIterator scrollIterator = jobhistorybatchdao
            .listeEditionsEnEchecIterator(TAILLE_BLOC, APPLICATION_ORIGINE_INCONNUE);
        assertFalse("Aucun élément ne devrait être trouvé", scrollIterator.hasNext());
        scrollIterator.close();

        // Récupération des documents en mode curseur,
        // possible par l'encapsulation complète de la méthode dans une transaction (@Transactional).
        scrollIterator = jobhistorybatchdao
            .listeEditionsEnEchecIterator(TAILLE_BLOC, APPLICATION_ORIGINE_1);

        // Récupération des élements
        List<JobHistory> liste = getJobHistory(scrollIterator);

        // Vérification de la cohérence des résultats
        ControleDonnees.verifierElements(liste, mapJobHistory.get(6), mapJobHistory.get(7));
    }

    /**
     * Méthode permettant de répérer les historiques de travaux d'édition à partir d'un itérateur en mode curseur.
     *
     * @param scrollIterator itérateur en mode curseur
     * @return les historiques de travaux d'édition
     */
    private List<JobHistory> getJobHistory(ScrollIterator scrollIterator)
    {
        // Récupération des documents en mode curseur,
        // possible par l'encapsulation complète de la méthode dans une transaction (@Transactional).
        List<JobHistory> liste = new ArrayList<>();
        while (scrollIterator.hasNext())
        {
            liste.add((JobHistory) scrollIterator.nextObjetMetier());
        }
        scrollIterator.close();

        // Vérification de l'arrêt de la recherche
        verifierArretRecherche(scrollIterator);

        // Retour de tous les documents trouvés
        return liste;
    }

    /**
     * Méthode permettant de vérifier que la recherche est arrêtée.
     *
     * @param scrollIterator iterateur en mode curseur
     */
    private void verifierArretRecherche(ScrollIterator scrollIterator)
    {
        assertTrue(scrollIterator.isClosed());
    }

    /**
     * Méthode permettant de générer un historique de travaux d'édition.
     *
     * @param indice indice à utiliser dans les informations de l'historique
     * @return historique de travaux d'édition généré
     */
    private JobHistory getJobHistory(int indice)
    {
        JobHistory jobHistory = new JobHistory();

        // L'UID de l'édition est l'indice fourni
        jobHistory.setEditionUuid(String.valueOf(indice));
        jobHistory.getArchivageEdition().setDatePurge(TemporaliteUtils.getTimestamp(2017, 1, indice));

        // Affectation de différentes valeurs suivant l'indice
        switch (indice)
        {
            case 1:
                jobHistory.getDestinationEdition().setApplicationOrigine(APPLICATION_ORIGINE_1);
                jobHistory.getOrdoEdition().setStatus(JobHistory.STATUS_DISPONIBLE);
                break;
            case 2:
                jobHistory.getDestinationEdition().setApplicationOrigine(APPLICATION_ORIGINE_2);
                jobHistory.getOrdoEdition().setStatus(JobHistory.STATUS_DISPONIBLE);
                break;
            case 3:
                jobHistory.getDestinationEdition().setApplicationOrigine(APPLICATION_ORIGINE_1);
                jobHistory.getOrdoEdition().setStatus(JobHistory.STATUS_EN_COURS_EXECUTION);
                break;
            case 4:
                jobHistory.getDestinationEdition().setApplicationOrigine(APPLICATION_ORIGINE_1);
                jobHistory.getOrdoEdition().setStatus(JobHistory.STATUS_EN_ATTENTE_EXECUTION);
                break;
            case 5:
                jobHistory.getDestinationEdition().setApplicationOrigine(APPLICATION_ORIGINE_1);
                jobHistory.getOrdoEdition().setStatus(JobHistory.STATUS_DISPONIBLE_NON_ENVOYE);
                break;
            case 6:
            case 7:
                jobHistory.getDestinationEdition().setApplicationOrigine(APPLICATION_ORIGINE_1);
                jobHistory.getOrdoEdition().setStatus(JobHistory.STATUS_ECHEC);
                break;
            default:
                jobHistory.getDestinationEdition().setApplicationOrigine(APPLICATION_ORIGINE_1);
                jobHistory.getOrdoEdition().setStatus(JobHistory.STATUS_DISPONIBLE);
        }

        return jobHistory;
    }
}