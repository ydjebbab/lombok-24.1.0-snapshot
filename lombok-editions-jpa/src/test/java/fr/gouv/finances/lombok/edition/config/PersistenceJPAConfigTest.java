/*
 * Copyright (c) 2020 DGFiP - Tous droits réservés
 * 
 */
package fr.gouv.finances.lombok.edition.config;

import java.util.Properties;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaDialect;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @author celfer Date: 11 dec. 2019
 */
@Configuration
@EnableTransactionManagement
// @ComponentScan({"fr.gouv.finances.*.*.dao"})
@ComponentScan({"fr.gouv.finances.lombok.edition.jpa.dao.impl"})
@EnableJpaRepositories(basePackages = "fr.gouv.finances.*.*.dao")
@ConditionalOnProperty(name = "lombok.orm.jpa", havingValue="true")
public class PersistenceJPAConfigTest
{
    protected static final Logger LOGGER = LoggerFactory.getLogger(PersistenceJPAConfigTest.class);

    //private static final String UNITE_PERSISTANCE_PRINCIPAL = "pu-principal";

    private static final String UNITE_PERSISTANCE_EDITION = "pu-edition";

    public PersistenceJPAConfigTest()
    {
        super();
    }

    /**
     * Configuration JPA pour une base embedded HSQL (profil embedded)
     *
     * @author celfer Date: 17 dec. 2019
     */
    @Configuration
    @Profile("embedded")
    public static class InnerEmbeddedConfig
    {
        /**
         * Construction de l'EMF à partir de la source de données et du choix d'hibernate.
         */
        // @Bean(name = "entityManagerJPA")

        /*
        @Bean(name = "entityManager")
        @Primary
        public LocalContainerEntityManagerFactoryBean entityManagerFactoryBean()
        {
            LOGGER.debug(">>> Instanciation du bean entityManager");
            final LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
            em.setDataSource(dataSource());
            em.setPersistenceUnitName(UNITE_PERSISTANCE_PRINCIPAL);
            em.setPackagesToScan(new String[] {"fr.gouv.finances.*.*.entite", "fr.gouv.finances.lombok.edition.bean"});
            em.setPersistenceXmlLocation("classpath:META-INF/persistence-test.xml");

            final HibernateJpaVendorAdapter hibernateJpaVendorAdapter = new HibernateJpaVendorAdapter();
            // CF: false par défaut
            // hibernateJpaVendorAdapter.setShowSql(false);
            hibernateJpaVendorAdapter.setGenerateDdl(false);
            hibernateJpaVendorAdapter.setDatabasePlatform("org.hibernate.dialect.HSQLDialect");

            em.setJpaVendorAdapter(hibernateJpaVendorAdapter);
            em.setJpaProperties(additionalProperties());

            return em;
        }
        */
        /**
         * Définition de la source de données
         */
        /*
        @Bean(name = "datasource")
        @Qualifier("dataSource")
        @Primary
        public DataSource dataSource()
        {
            LOGGER.debug(">>> Instanciation du bean datasource");
            EmbeddedDatabaseBuilder builder = new EmbeddedDatabaseBuilder();
            EmbeddedDatabase db = builder
                .setType(EmbeddedDatabaseType.HSQL)
                .setName("lombokdbtest")
                .addScript("classpath:" + "sql/create_schema_jasper_hsqldb.sql")
                .build();
            return db;
        }
        */

        /**
         * Construction d'un gestionnaire de transaction en liaison avec l'usine à EM.
         */
        // @Bean(name = "transactionManagerJPA")
        /*
        @Bean
        @Primary
        public PlatformTransactionManager transactionManager()
        {
            LOGGER.debug(">>> Instanciation du bean transactionManager");
            JpaTransactionManager transactionManager = new JpaTransactionManager();
            transactionManager.setEntityManagerFactory(entityManagerFactoryBean().getObject());
            transactionManager.setJpaDialect(new HibernateJpaDialect());
            return transactionManager;
        }

        @Bean
        public PersistenceExceptionTranslationPostProcessor exceptionTranslation()
        {
            return new PersistenceExceptionTranslationPostProcessor();
        }        

        @Bean
        @Profile("!batchdao")
        public TransactionTemplate transactionTemplate()
        {
            LOGGER.debug(">>> Instanciation du bean transactionTemplate");
            TransactionTemplate transactionTemplate = new TransactionTemplate();
            transactionTemplate.setTransactionManager(transactionManager());
            return transactionTemplate;
        }
        */

        private final Properties additionalProperties()
        {
            final Properties hibernateProperties = new Properties();
            hibernateProperties.setProperty("hibernate.hbm2ddl.auto", "create");
            // CF : false par défaut
            // hibernateProperties.setProperty("hibernate.use_sql_comments", "false");
            // TODO : use_sql_comments et show_sql à true ?
            return hibernateProperties;
        }

        // **********************
        // Profil edition
        // **********************

        @Bean(name = "entityManagerEdition")
        @Profile("edition")
        public LocalContainerEntityManagerFactoryBean entityManagerEditionFactoryBean()
        {
            LOGGER.debug(">>> Instanciation du bean entityManagerEditionFactoryBean");
            final LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
            em.setDataSource(editionDS());
            em.setPersistenceUnitName(UNITE_PERSISTANCE_EDITION);
            em.setPackagesToScan(new String[] {"fr.gouv.finances.*.*.entite", "fr.gouv.finances.lombok.edition.bean"});
            em.setPersistenceXmlLocation("classpath:META-INF/persistence-test.xml");

            final HibernateJpaVendorAdapter hibernateJpaVendorAdapter = new HibernateJpaVendorAdapter();
            // CF: false par défaut
            // hibernateJpaVendorAdapter.setShowSql(false);
            hibernateJpaVendorAdapter.setGenerateDdl(false);
            hibernateJpaVendorAdapter.setDatabasePlatform("org.hibernate.dialect.HSQLDialect");

            em.setJpaVendorAdapter(hibernateJpaVendorAdapter);
            em.setJpaProperties(additionalProperties());

            return em;
        }

        @Bean
        @Profile("edition")
        public PlatformTransactionManager editionTransactionManager()
        {
            LOGGER.debug(">>> Instanciation du bean transactionManager");
            JpaTransactionManager transactionManager = new JpaTransactionManager();
            transactionManager.setEntityManagerFactory(entityManagerEditionFactoryBean().getObject());
            transactionManager.setJpaDialect(new HibernateJpaDialect());
            return transactionManager;
        }

        /*
         * @Bean(name = "editionDatasource", destroyMethod = "close")
         * @Profile("edition") public DataSource editionDatasource() {
         * LOGGER.debug(">>> Instanciation du bean editionDatasource"); org.apache.tomcat.jdbc.pool.DataSource
         * dataSource = new org.apache.tomcat.jdbc.pool.DataSource(editionPool()); return dataSource; }
         * @Bean(name = "editionpool")
         * @Profile("edition") public org.apache.tomcat.jdbc.pool.PoolProperties editionPool() {
         * LOGGER.debug(">>> Instanciation du bean editionpool"); PoolProperties poolProperties = new PoolProperties();
         * //poolProperties.setInitialSize(initialSize); //poolProperties.setMaxActive(maxActive);
         * //poolProperties.setMaxIdle(maxIdle);
         * //poolProperties.setMinEvictableIdleTimeMillis(minEvictableIdleTimeMillis);
         * //poolProperties.setTimeBetweenEvictionRunsMillis(timeBetweenEvictionRunsMillis);
         * poolProperties.setTestWhileIdle(true); poolProperties.setTestOnReturn(true);
         * poolProperties.setTestOnBorrow(false);
         * poolProperties.setValidationQuery("select 1 from INFORMATION_SCHEMA.SYSTEM_USERS");
         * poolProperties.setDefaultReadOnly(false); poolProperties.setDefaultAutoCommit(true);
         * poolProperties.setDataSource(editionDS()); return poolProperties; }
         */

        @Bean
        @Profile("edition")
        public DataSource editionDS()
        {
            LOGGER.debug(">>> Instanciation du bean editionDS");
            EmbeddedDatabaseBuilder builder = new EmbeddedDatabaseBuilder();
            EmbeddedDatabase db = builder
                .setType(EmbeddedDatabaseType.HSQL)
                .addScript("classpath:" + "sql/create_schema_jasper_hsqldb.sql")
                .build();
            return db;
        }

    }
}
