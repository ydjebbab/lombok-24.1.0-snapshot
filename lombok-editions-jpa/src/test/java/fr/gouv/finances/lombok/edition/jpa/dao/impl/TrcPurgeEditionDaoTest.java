package fr.gouv.finances.lombok.edition.jpa.dao.impl;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.data.jpa.JpaRepositoriesAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import fr.gouv.finances.lombok.edition.bean.TrcPurgeEdition;
import fr.gouv.finances.lombok.edition.config.PersistenceJPAConfigTest;
import fr.gouv.finances.lombok.edition.dao.TrcPurgeEditionDao;
import fr.gouv.finances.lombok.test.controle.ControleDonnees;
import fr.gouv.finances.lombok.test.controle.VerificationExecution;
import fr.gouv.finances.lombok.util.base.PurgeTablesDao;
import fr.gouv.finances.lombok.util.date.TemporaliteUtils;
import fr.gouv.finances.lombok.util.exception.ProgrammationException;

/**
 * Tests unitaires automatisés du DAO JPA, gérant les données des traces de purge des éditions.
 *
 * @author Christophe Breheret-Girardin
 * @author CF
 */
@ComponentScan(basePackages = {"fr.gouv.finances.lombok.edition.jpa.dao.impl, fr.gouv.finances.lombok.edition.config"})
@EnableAutoConfiguration(exclude = {JpaRepositoriesAutoConfiguration.class})
@SpringBootTest(classes = {PersistenceJPAConfigTest.class}, properties = {"lombok.orm.jpa=true"})
@ActiveProfiles(profiles = {"edition", "embedded"})
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class TrcPurgeEditionDaoTest
{
    public TrcPurgeEditionDaoTest()
    {
        super();
    }

    protected static final Logger LOGGER = LoggerFactory.getLogger(TrcPurgeEditionDaoTest.class);

    /** Déclaration du DAO gérant les données des traces de purge des éditions. */
    @Autowired
    private TrcPurgeEditionDao trcpurgeeditiondao;

    /** Déclaration du DAO de gestion des purges de la base. */
    @Autowired
    private PurgeTablesDao purgetableseditiondao;

    /** Map contenant les traces de purge d'édition présentes en base */
    private Map<Integer, TrcPurgeEdition> mapTrcPurgeEdition = new HashMap<>();

    /** Constante donnant le suffixe d'une libellé d'une application d'origine */
    private static final String SUFFIXE_APPLICATION_ORIGINE = "APPLICATION ORIGINE ";

    /** Constante donnant le libellé d'une 1ère application d'origine */
    private static final String APPLICATION_ORIGINE_1 = SUFFIXE_APPLICATION_ORIGINE + 1;

    /** Constante donnant le libellé d'une 2ème application d'origine */
    private static final String APPLICATION_ORIGINE_2 = SUFFIXE_APPLICATION_ORIGINE + 2;

    /** Constante donnant le libellé d'une application d'origine non utilisée dans les jeux d'essai */
    private static final String APPLICATION_ORIGINE_INCONNUE = SUFFIXE_APPLICATION_ORIGINE + 3;

    /**
     * Initialisation des données en base avant les tests.
     */
    @Before
    public void creerDonnees()
    {
        // Purge des données de la base
        purgetableseditiondao.purgerTables("ZTRCPURGEEDITION_ZTPE");

        // Vérification de l'absence de données
        ControleDonnees.verifierElements(trcpurgeeditiondao.loadAllObjects(TrcPurgeEdition.class));

        // Génération des traces de purge d'édition, insertion en base de données et sauvegarde dans une Map
        mapTrcPurgeEdition = IntStream.rangeClosed(1, 9).mapToObj(this::getTracePurgeEdition)
            .peek(trcpurgeeditiondao::saveObject)
            .collect(Collectors.toMap(t -> Integer.parseInt(t.getMessageErreur()), t -> t));

        // Vérification de la présence des données avant exécution du test
        ControleDonnees.verifierElements(trcpurgeeditiondao.loadAllObjects(TrcPurgeEdition.class), mapTrcPurgeEdition.get(1),
            mapTrcPurgeEdition.get(2), mapTrcPurgeEdition.get(3), mapTrcPurgeEdition.get(4), mapTrcPurgeEdition.get(5),
            mapTrcPurgeEdition.get(6), mapTrcPurgeEdition.get(7), mapTrcPurgeEdition.get(8), mapTrcPurgeEdition.get(9));
    }

    /**
     * Méthode permettant de tester l'injection du DAO.
     */
    @Test
    public final void injectionDao()
    {
        // Vérification
        assertNotNull("DAO non injecté dans le test", trcpurgeeditiondao);
    }

    /**
     * Test de la méthode fr.gouv.finances.lombok.edition.dao.TrcPurgeEditionDao#deleteTrcPurgeEdition(
     * fr.gouv.finances.lombok.edition.bean.TrcPurgeEdition).
     */
    @Test
    public void deleteTrcPurgeEdition()
    {
        // Exécution de la méthode à tester sur un élément non persisté et vérifications
        trcpurgeeditiondao.deleteTrcPurgeEdition(getTracePurgeEdition(10));
        ControleDonnees.verifierElements(trcpurgeeditiondao.loadAllObjects(TrcPurgeEdition.class), mapTrcPurgeEdition.get(1),
            mapTrcPurgeEdition.get(2), mapTrcPurgeEdition.get(3), mapTrcPurgeEdition.get(4), mapTrcPurgeEdition.get(5),
            mapTrcPurgeEdition.get(6), mapTrcPurgeEdition.get(7), mapTrcPurgeEdition.get(8), mapTrcPurgeEdition.get(9));

        // Exécution de la méthode à tester sur un élément non chargé et vérifications
        trcpurgeeditiondao.deleteTrcPurgeEdition(getTracePurgeEdition(3));
        ControleDonnees.verifierElements(trcpurgeeditiondao.loadAllObjects(TrcPurgeEdition.class), mapTrcPurgeEdition.get(1),
            mapTrcPurgeEdition.get(2), mapTrcPurgeEdition.get(3), mapTrcPurgeEdition.get(4), mapTrcPurgeEdition.get(5),
            mapTrcPurgeEdition.get(6), mapTrcPurgeEdition.get(7), mapTrcPurgeEdition.get(8), mapTrcPurgeEdition.get(9));

        // Exécution de la méthode à tester sur un élément persisté + chargé et vérifications
        trcpurgeeditiondao.deleteTrcPurgeEdition(mapTrcPurgeEdition.get(3));
        ControleDonnees.verifierElements(trcpurgeeditiondao.loadAllObjects(TrcPurgeEdition.class), mapTrcPurgeEdition.get(1),
            mapTrcPurgeEdition.get(2), mapTrcPurgeEdition.get(4), mapTrcPurgeEdition.get(5), mapTrcPurgeEdition.get(6),
            mapTrcPurgeEdition.get(7), mapTrcPurgeEdition.get(8), mapTrcPurgeEdition.get(9));
    }

    /**
     * Test de la méthode fr.gouv.finances.lombok.edition.dao.TrcPurgeEditionDao#deleteTrcPurgeEditions(
     * java.util.Collection).
     */
    @Test
    public void deleteTrcPurgeEditions()
    {
        // 1ère exécution de la méthode à tester sans élément et vérifications
        trcpurgeeditiondao.deleteTrcPurgeEditions(null);
        ControleDonnees.verifierElements(trcpurgeeditiondao.loadAllObjects(TrcPurgeEdition.class), mapTrcPurgeEdition.get(1),
            mapTrcPurgeEdition.get(2), mapTrcPurgeEdition.get(3), mapTrcPurgeEdition.get(4), mapTrcPurgeEdition.get(5),
            mapTrcPurgeEdition.get(6), mapTrcPurgeEdition.get(7), mapTrcPurgeEdition.get(8), mapTrcPurgeEdition.get(9));

        // 2ème exécution de la méthode à tester sans élément et vérifications
        trcpurgeeditiondao.deleteTrcPurgeEditions(new ArrayList<>());
        ControleDonnees.verifierElements(trcpurgeeditiondao.loadAllObjects(TrcPurgeEdition.class), mapTrcPurgeEdition.get(1),
            mapTrcPurgeEdition.get(2), mapTrcPurgeEdition.get(3), mapTrcPurgeEdition.get(4), mapTrcPurgeEdition.get(5),
            mapTrcPurgeEdition.get(6), mapTrcPurgeEdition.get(7), mapTrcPurgeEdition.get(8), mapTrcPurgeEdition.get(9));

        // Exécution de la méthode à tester sur des éléments non persistés et vérifications
        trcpurgeeditiondao.deleteTrcPurgeEditions(Arrays.asList(
            getTracePurgeEdition(10), getTracePurgeEdition(11)));
        ControleDonnees.verifierElements(trcpurgeeditiondao.loadAllObjects(TrcPurgeEdition.class), mapTrcPurgeEdition.get(1),
            mapTrcPurgeEdition.get(2), mapTrcPurgeEdition.get(3), mapTrcPurgeEdition.get(4), mapTrcPurgeEdition.get(5),
            mapTrcPurgeEdition.get(6), mapTrcPurgeEdition.get(7), mapTrcPurgeEdition.get(8), mapTrcPurgeEdition.get(9));

        // Exécution de la méthode à tester sur des éléments non chargés et vérifications
        trcpurgeeditiondao.deleteTrcPurgeEditions(Arrays.asList(
            getTracePurgeEdition(1), getTracePurgeEdition(3)));
        ControleDonnees.verifierElements(trcpurgeeditiondao.loadAllObjects(TrcPurgeEdition.class), mapTrcPurgeEdition.get(1),
            mapTrcPurgeEdition.get(2), mapTrcPurgeEdition.get(3), mapTrcPurgeEdition.get(4), mapTrcPurgeEdition.get(5),
            mapTrcPurgeEdition.get(6), mapTrcPurgeEdition.get(7), mapTrcPurgeEdition.get(8), mapTrcPurgeEdition.get(9));

        // Exécution de la méthode à tester sur des éléments persités + chargés et vérifications
        trcpurgeeditiondao.deleteTrcPurgeEditions(Arrays.asList(
            mapTrcPurgeEdition.get(1), mapTrcPurgeEdition.get(3)));
        ControleDonnees.verifierElements(trcpurgeeditiondao.loadAllObjects(TrcPurgeEdition.class), mapTrcPurgeEdition.get(2),
            mapTrcPurgeEdition.get(4), mapTrcPurgeEdition.get(5), mapTrcPurgeEdition.get(6), mapTrcPurgeEdition.get(7),
            mapTrcPurgeEdition.get(8), mapTrcPurgeEdition.get(9));
    }

    /**
     * Test de la méthode fr.gouv.finances.lombok.edition.dao.TrcPurgeEditionDao#saveTrcPurgeEdition(
     * fr.gouv.finances.lombok.edition.bean.TrcPurgeEdition).
     */
    @Test
    public void saveTrcPurgeEdition()
    {
        // Exécution de la méthode à tester
        TrcPurgeEdition tracePurgeEdition = getTracePurgeEdition(10);
        trcpurgeeditiondao.saveTrcPurgeEdition(tracePurgeEdition);

        // Vérifications
        ControleDonnees.verifierElements(trcpurgeeditiondao.loadAllObjects(TrcPurgeEdition.class), mapTrcPurgeEdition.get(1),
            mapTrcPurgeEdition.get(2), mapTrcPurgeEdition.get(3), mapTrcPurgeEdition.get(4), mapTrcPurgeEdition.get(5),
            mapTrcPurgeEdition.get(6), mapTrcPurgeEdition.get(7), mapTrcPurgeEdition.get(8), mapTrcPurgeEdition.get(9), tracePurgeEdition);
    }

    /**
     * Test de la méthode fr.gouv.finances.lombok.edition.dao.TrcPurgeEditionDao#findTousLesTrcPurgeEdition(
     * java.lang.String, int).
     */
    @Test
    public void findTousLesTrcPurgeEdition()
    {
        // Exécution de la méthode à tester sur les cas sans résultat, et vérifications
        ControleDonnees.verifierElements(trcpurgeeditiondao.findTousLesTrcPurgeEdition(null, 0));
        ControleDonnees.verifierElements(trcpurgeeditiondao.findTousLesTrcPurgeEdition("", 0));
        ControleDonnees.verifierElements(trcpurgeeditiondao.findTousLesTrcPurgeEdition(APPLICATION_ORIGINE_INCONNUE, 0));

        // 1ère exécution de la méthode à tester sur des résultats et vérifications (pas de limitation)
        ControleDonnees.verifierElements(trcpurgeeditiondao.findTousLesTrcPurgeEdition(APPLICATION_ORIGINE_1, 0), true,
            mapTrcPurgeEdition.get(9), mapTrcPurgeEdition.get(8), mapTrcPurgeEdition.get(7), mapTrcPurgeEdition.get(6),
            mapTrcPurgeEdition.get(5), mapTrcPurgeEdition.get(4), mapTrcPurgeEdition.get(3), mapTrcPurgeEdition.get(1));
        ControleDonnees.verifierElements(trcpurgeeditiondao.findTousLesTrcPurgeEdition(APPLICATION_ORIGINE_2, 0), true,
            mapTrcPurgeEdition.get(2));

        // 2ème exécution de la méthode à tester sur des résultats et vérifications (limitation aux 2 premiers)
        ControleDonnees.verifierElements(trcpurgeeditiondao.findTousLesTrcPurgeEdition(APPLICATION_ORIGINE_1, 2), true,
            getTracePurgeEdition(9), getTracePurgeEdition(8));
    }

    /**
     * Test de la méthode fr.gouv.finances.lombok.edition.dao.TrcPurgeEditionDao#findLesTrcPurgeEditionPourUneDate(
     * java.util.Date, java.lang.String, int).
     */
    @Test
    public void findLesTrcPurgeEditionPourUneDate()
    {
        // Exécution de la méthode à tester sur les cas sans résultat, et vérifications
        ControleDonnees.verifierElements(trcpurgeeditiondao.findLesTrcPurgeEditionPourUneDate(null, null, 0));
        ControleDonnees.verifierElements(trcpurgeeditiondao.findLesTrcPurgeEditionPourUneDate(null, "", 0));
        ControleDonnees.verifierElements(trcpurgeeditiondao.findLesTrcPurgeEditionPourUneDate(
            null, APPLICATION_ORIGINE_INCONNUE, 0));
        ControleDonnees.verifierElements(trcpurgeeditiondao.findLesTrcPurgeEditionPourUneDate(
            TemporaliteUtils.getTimestamp(4000, 1, 1), null, 0));
        ControleDonnees.verifierElements(trcpurgeeditiondao.findLesTrcPurgeEditionPourUneDate(
            TemporaliteUtils.getTimestamp(4000, 1, 1), APPLICATION_ORIGINE_1, 0));
        ControleDonnees.verifierElements(
            trcpurgeeditiondao.findLesTrcPurgeEditionPourUneDate(TemporaliteUtils.getTimestamp(2017, 1, 5), APPLICATION_ORIGINE_2, 0));

        // 1ère exécution de la méthode à tester sur des résultats et vérifications (pas de limitation)
        ControleDonnees.verifierElements(
            trcpurgeeditiondao.findLesTrcPurgeEditionPourUneDate(TemporaliteUtils.getTimestamp(2017, 1, 5), APPLICATION_ORIGINE_1, 0), true,
            mapTrcPurgeEdition.get(6), mapTrcPurgeEdition.get(5));

        // 2ème exécution de la méthode à tester sur des résultats et vérifications (limitation aux 2 premiers)
        ControleDonnees.verifierElements(
            trcpurgeeditiondao.findLesTrcPurgeEditionPourUneDate(TemporaliteUtils.getTimestamp(2017, 1, 5), APPLICATION_ORIGINE_1, 1), true,
            mapTrcPurgeEdition.get(6));
    }

    /**
     * Test de la méthode
     * fr.gouv.finances.lombok.edition.dao.TrcPurgeEditionDao#findLesTrcPurgeEditionEnCoursAnterieuresAUneDate(
     * java.util.Date, java.lang.String).
     */
    @Test
    public void findLesTrcPurgeEditionEnCoursAnterieuresAUneDate()
    {
        // Exécution de la méthode à tester sur les cas sans résultat, et vérifications
        ControleDonnees.verifierElements(trcpurgeeditiondao.findLesTrcPurgeEditionEnCoursAnterieuresAUneDate(null, null));
        ControleDonnees.verifierElements(trcpurgeeditiondao.findLesTrcPurgeEditionEnCoursAnterieuresAUneDate(null, ""));
        ControleDonnees.verifierElements(trcpurgeeditiondao.findLesTrcPurgeEditionEnCoursAnterieuresAUneDate(
            null, APPLICATION_ORIGINE_INCONNUE));
        ControleDonnees.verifierElements(trcpurgeeditiondao.findLesTrcPurgeEditionEnCoursAnterieuresAUneDate(
            TemporaliteUtils.getTimestamp(1000, 1, 1), APPLICATION_ORIGINE_1));

        // Exécutions de la méthode à tester sur des résultats et vérifications
        ControleDonnees.verifierElements(trcpurgeeditiondao.findLesTrcPurgeEditionEnCoursAnterieuresAUneDate(
            TemporaliteUtils.getTimestamp(2017, 1, 4), APPLICATION_ORIGINE_1), mapTrcPurgeEdition.get(1), mapTrcPurgeEdition.get(3));
        ControleDonnees.verifierElements(trcpurgeeditiondao.findLesTrcPurgeEditionEnCoursAnterieuresAUneDate(
            TemporaliteUtils.getTimestamp(2017, 1, 4), APPLICATION_ORIGINE_2));
    }

    /**
     * Test de la méthode
     * fr.gouv.finances.lombok.edition.dao.TrcPurgeEditionDao#findLesTrcPurgeEditionPourUnInstantDemarrage(
     * java.util.Date, java.lang.String).
     */
    @Test
    public void findLesTrcPurgeEditionPourUnInstantDemarrage()
    {
        // Exécution de la méthode à tester sur les cas sans résultat, et vérifications
        ControleDonnees.verifierElements(trcpurgeeditiondao.findLesTrcPurgeEditionPourUnInstantDemarrage(null, null));
        ControleDonnees.verifierElements(trcpurgeeditiondao.findLesTrcPurgeEditionPourUnInstantDemarrage(null, ""));
        ControleDonnees.verifierElements(trcpurgeeditiondao.findLesTrcPurgeEditionPourUnInstantDemarrage(
            null, APPLICATION_ORIGINE_INCONNUE));
        ControleDonnees.verifierElements(trcpurgeeditiondao.findLesTrcPurgeEditionPourUnInstantDemarrage(
            TemporaliteUtils.getTimestamp(1000, 1, 1), APPLICATION_ORIGINE_1));
        ControleDonnees.verifierElements(trcpurgeeditiondao.findLesTrcPurgeEditionPourUnInstantDemarrage(
            TemporaliteUtils.getTimestamp(2017, 1, 5), APPLICATION_ORIGINE_2));

        // Exécution de la méthode à tester sur des résultats et vérifications
        ControleDonnees.verifierElements(trcpurgeeditiondao.findLesTrcPurgeEditionPourUnInstantDemarrage(
            TemporaliteUtils.getTimestamp(2017, 1, 5), APPLICATION_ORIGINE_1), mapTrcPurgeEdition.get(5), mapTrcPurgeEdition.get(6));
    }

    /**
     * Test de la méthode fr.gouv.finances.lombok.edition.dao.TrcPurgeEditionDao#findTrcPurgeEditionEnCours(
     * java.lang.String).
     */
    @Test
    public void findTrcPurgeEditionEnCours()
    {
        // Exécution de la méthode à tester sur les cas sans résultat, et vérifications
        ControleDonnees.verifierElements(trcpurgeeditiondao.findTrcPurgeEditionEnCours(null));
        ControleDonnees.verifierElements(trcpurgeeditiondao.findTrcPurgeEditionEnCours(""));
        ControleDonnees.verifierElements(trcpurgeeditiondao.findTrcPurgeEditionEnCours(APPLICATION_ORIGINE_INCONNUE));

        // Exécutions de la méthode à tester sur des résultats et vérifications
        ControleDonnees.verifierElements(trcpurgeeditiondao.findTrcPurgeEditionEnCours(APPLICATION_ORIGINE_1), mapTrcPurgeEdition.get(1),
            mapTrcPurgeEdition.get(3), mapTrcPurgeEdition.get(4), mapTrcPurgeEdition.get(5), mapTrcPurgeEdition.get(6),
            mapTrcPurgeEdition.get(7), mapTrcPurgeEdition.get(8), mapTrcPurgeEdition.get(9));
        ControleDonnees.verifierElements(trcpurgeeditiondao.findTrcPurgeEditionEnCours(APPLICATION_ORIGINE_2));
    }

    /**
     * Test de la méthode fr.gouv.finances.lombok.edition.dao.TrcPurgeEditionDao#checkExistencePurgeDateIdentique(
     * fr.gouv.finances.lombok.edition.bean.TrcPurgeEdition).
     */
    @Test
    public void checkExistencePurgeDateIdentique()
    {
        // Appel de la méthode à tester sur des paramétres non initialisés et/ou vides, et vérifications
        VerificationExecution.verifierException(ProgrammationException.class,
            () -> trcpurgeeditiondao.checkExistencePurgeDateIdentique(null));

        // Exécution de la méthode à tester sur les cas sans résultat, et vérifications
        assertFalse(trcpurgeeditiondao.checkExistencePurgeDateIdentique(mapTrcPurgeEdition.get(1)));
        assertFalse(trcpurgeeditiondao.checkExistencePurgeDateIdentique(mapTrcPurgeEdition.get(2)));
        assertFalse(trcpurgeeditiondao.checkExistencePurgeDateIdentique(mapTrcPurgeEdition.get(3)));
        assertFalse(trcpurgeeditiondao.checkExistencePurgeDateIdentique(mapTrcPurgeEdition.get(4)));
        assertFalse(trcpurgeeditiondao.checkExistencePurgeDateIdentique(mapTrcPurgeEdition.get(7)));
        assertFalse(trcpurgeeditiondao.checkExistencePurgeDateIdentique(mapTrcPurgeEdition.get(8)));
        assertFalse(trcpurgeeditiondao.checkExistencePurgeDateIdentique(mapTrcPurgeEdition.get(9)));
        assertFalse(trcpurgeeditiondao.checkExistencePurgeDateIdentique(getTracePurgeEdition(10)));

        // Exécutions de la méthode à tester sur des résultats et vérifications
        assertTrue(trcpurgeeditiondao.checkExistencePurgeDateIdentique(getTracePurgeEdition(1)));
        assertTrue(trcpurgeeditiondao.checkExistencePurgeDateIdentique(getTracePurgeEdition(5)));
        assertTrue(trcpurgeeditiondao.checkExistencePurgeDateIdentique(mapTrcPurgeEdition.get(5)));
        assertTrue(trcpurgeeditiondao.checkExistencePurgeDateIdentique(mapTrcPurgeEdition.get(6)));
    }

    /**
     * Test de la méthode fr.gouv.finances.lombok.edition.dao.TrcPurgeEditionDao#isUnBatchPurgeEnCoursExecution(
     * java.lang.String).
     */
    @Test
    public void isUnBatchPurgeEnCoursExecution()
    {
        // Exécution de la méthode à tester sur les cas sans résultat, et vérifications
        assertFalse(trcpurgeeditiondao.isUnBatchPurgeEnCoursExecution(null));
        assertFalse(trcpurgeeditiondao.isUnBatchPurgeEnCoursExecution(""));
        assertFalse(trcpurgeeditiondao.isUnBatchPurgeEnCoursExecution(APPLICATION_ORIGINE_INCONNUE));

        // Exécutions de la méthode à tester sur des résultats et vérifications
        assertTrue(trcpurgeeditiondao.isUnBatchPurgeEnCoursExecution(APPLICATION_ORIGINE_1));
        assertFalse(trcpurgeeditiondao.isUnBatchPurgeEnCoursExecution(APPLICATION_ORIGINE_2));
    }

    /**
     * Test de la méthode fr.gouv.finances.lombok.edition.dao.TrcPurgeEditionDao#findTrcPurgeEditionParPeriodePurge(
     * java.util.Date, java.util.Date, java.lang.String, int).
     */
    @Test
    public void findTrcPurgeEditionParPeriodePurge()
    {
        // Exécution de la méthode à tester sur les cas sans résultat, et vérifications
        ControleDonnees.verifierElements(trcpurgeeditiondao.findTrcPurgeEditionParPeriodePurge(null, null, null, 0));
        ControleDonnees.verifierElements(trcpurgeeditiondao.findTrcPurgeEditionParPeriodePurge(null, null, "", 0));
        ControleDonnees.verifierElements(trcpurgeeditiondao.findTrcPurgeEditionParPeriodePurge(
            null, null, APPLICATION_ORIGINE_INCONNUE, 0));
        ControleDonnees.verifierElements(trcpurgeeditiondao.findTrcPurgeEditionParPeriodePurge(
            null, null, APPLICATION_ORIGINE_1, 0));
        ControleDonnees.verifierElements(trcpurgeeditiondao.findTrcPurgeEditionParPeriodePurge(
            TemporaliteUtils.getTimestamp(2017, 1, 5), null, APPLICATION_ORIGINE_1, 0));
        ControleDonnees.verifierElements(trcpurgeeditiondao.findTrcPurgeEditionParPeriodePurge(
            null, TemporaliteUtils.getTimestamp(2017, 1, 5), APPLICATION_ORIGINE_1, 0));

        // Exécutions de la méthode à tester sur des résultats et vérifications (sans limitation)
        ControleDonnees.verifierElements(trcpurgeeditiondao.findTrcPurgeEditionParPeriodePurge(
            TemporaliteUtils.getTimestamp(2017, 1, 1), TemporaliteUtils.getTimestamp(2017, 1, 4), APPLICATION_ORIGINE_1, 0), true,
            mapTrcPurgeEdition.get(4), mapTrcPurgeEdition.get(3), mapTrcPurgeEdition.get(1));
        ControleDonnees.verifierElements(trcpurgeeditiondao.findTrcPurgeEditionParPeriodePurge(
            TemporaliteUtils.getTimestamp(2017, 1, 1), TemporaliteUtils.getTimestamp(2017, 1, 4), APPLICATION_ORIGINE_2, 0), true,
            mapTrcPurgeEdition.get(2));

        // Exécutions de la méthode à tester sur des résultats et vérifications (limitation aux 2 premiers)
        ControleDonnees.verifierElements(trcpurgeeditiondao.findTrcPurgeEditionParPeriodePurge(
            TemporaliteUtils.getTimestamp(2017, 1, 1), TemporaliteUtils.getTimestamp(2017, 1, 4), APPLICATION_ORIGINE_1, 2), true,
            mapTrcPurgeEdition.get(4), mapTrcPurgeEdition.get(3));
        ControleDonnees.verifierElements(trcpurgeeditiondao.findTrcPurgeEditionParPeriodePurge(
            TemporaliteUtils.getTimestamp(2017, 1, 1), TemporaliteUtils.getTimestamp(2017, 1, 4), APPLICATION_ORIGINE_2, 2), true,
            mapTrcPurgeEdition.get(2));
    }

    /**
     * Méthode permettant de générer une trace de purge d'édition.
     *
     * @param indice indice permettant d'instaurer de la généricité
     * @return la trace de purge d'édition générée
     */
    private TrcPurgeEdition getTracePurgeEdition(int indice)
    {
        TrcPurgeEdition tracePurgeEdition = new TrcPurgeEdition();

        // Valeurs communes
        tracePurgeEdition.setMessageErreur(String.valueOf(indice));
        tracePurgeEdition.setDateDebutPurge(TemporaliteUtils.getTimestamp(2017, 1, 0 + indice));
        tracePurgeEdition.setEtatPurge(TrcPurgeEdition.EN_COURS_EXECUTION);
        tracePurgeEdition.setApplicationOrigine(APPLICATION_ORIGINE_1);

        // Surcharge des valeurs communes pour certaines traces de purge des éditions
        switch (indice)
        {
            case 2:
                tracePurgeEdition.setEtatPurge(TrcPurgeEdition.TERMINE_SANS_ERREUR);
                tracePurgeEdition.setApplicationOrigine(APPLICATION_ORIGINE_2);
                break;
            case 6:
                tracePurgeEdition.setDateDebutPurge(TemporaliteUtils.getTimestamp(2017, 1, 5));
                break;
            default:
        }

        return tracePurgeEdition;
    }
}