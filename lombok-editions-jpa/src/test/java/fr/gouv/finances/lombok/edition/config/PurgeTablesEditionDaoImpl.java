package fr.gouv.finances.lombok.edition.config;

import java.util.Arrays;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import fr.gouv.finances.lombok.edition.jpa.dao.impl.BaseDaoEditionJpaImpl;
import fr.gouv.finances.lombok.util.base.PurgeTablesDao;

/**
 * 
 * @author celfer
 * Date: 11 déc. 2019
 */
@ConditionalOnProperty(name = "lombok.orm.jpa", havingValue="true")
@Repository("purgetableseditiondao")
@Transactional
public class PurgeTablesEditionDaoImpl extends BaseDaoEditionJpaImpl implements PurgeTablesDao
{

    /**
     * {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.util.base.PurgeTablesDao#purgerTables(java.lang.String[])
     */
    @Override
    public void purgerTables(String... nomsTable)
    {
        // Exécution d'un ordre pour tronquer chaque table
        // (instruction immédiate par rapport à une suppression, et qui ne nécessite pas d'instruction "commit")
        Arrays.asList(nomsTable).stream().forEach(
            nomTable -> entityManagerEdition.createNativeQuery("TRUNCATE TABLE " + nomTable).executeUpdate());
    }

}