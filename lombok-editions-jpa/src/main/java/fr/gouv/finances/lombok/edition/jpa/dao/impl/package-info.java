/*
 * Copyright (c) 2018 DGFiP - Tous droits réservés
 * 
 */
/**
 * Paquet lié aux implémentations JPA des DAO de gestion des données liées aux éditions.
 *
 * @author Christophe Breheret-Girardin
 */
package fr.gouv.finances.lombok.edition.jpa.dao.impl;