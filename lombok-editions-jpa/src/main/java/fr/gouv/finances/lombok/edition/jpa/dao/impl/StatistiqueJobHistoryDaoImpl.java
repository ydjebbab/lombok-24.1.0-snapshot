/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 */
package fr.gouv.finances.lombok.edition.jpa.dao.impl;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import fr.gouv.finances.lombok.edition.bean.JobHistory;
import fr.gouv.finances.lombok.edition.dao.ProprieteJobHistory;
import fr.gouv.finances.lombok.edition.dao.ProprieteStatistiqueJobHistoryEdition;
import fr.gouv.finances.lombok.edition.dao.StatistiqueJobHistoryDao;
import fr.gouv.finances.lombok.edition.techbean.StatJobHistory;
import fr.gouv.finances.lombok.util.exception.ProgrammationException;

/**
 * Implémentation JPA du DAO permettant de gérer les données liées aux statistiques des travaux d'édition.
 *
 * @author Christophe Breheret-Girardin
 */
@ConditionalOnProperty(name = "lombok.orm.jpa", havingValue="true")
@Repository("statistiquejobhistorydaoimpl")
@Transactional(transactionManager = "editionTransactionManager")
public class StatistiqueJobHistoryDaoImpl extends BaseDaoEditionJpaImpl implements StatistiqueJobHistoryDao
{

    protected static final Logger LOG = LoggerFactory.getLogger(StatistiqueJobHistoryDaoImpl.class);

    /**
     * {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.edition.dao.StatistiqueJobHistoryDao#compteJobHistoryParDatePurge(java.lang.String).
     */
    @Override
    public List<StatJobHistory> compteJobHistoryParDatePurge(String applicationOrigine)
    {
        LOG.debug(">>> Début méthode compteJobHistoryParDatePurge");

        // Cas des paramètres non fournis
        controlerParametres(applicationOrigine);

        // Initialisation de la recherche
        CriteriaBuilder criteriaBuilder = entityManagerEdition.getCriteriaBuilder();
        CriteriaQuery<StatJobHistory> query = criteriaBuilder.createQuery(StatJobHistory.class);
        Root<JobHistory> from = query.from(JobHistory.class);

        // Filtre sur l'application
        query.where(criteriaBuilder.equal(from.get(ProprieteJobHistory.DESTINATION_EDITION.getNom())
            .get(ProprieteJobHistory.APPLICATION_ORIGINE.getNom()), applicationOrigine));

        // Initialisation de la sélection
        Expression<Date> selectDate = from.get(ProprieteStatistiqueJobHistoryEdition.ARCHIVAGE_EDITION.getNom())
            .<Date> get(ProprieteStatistiqueJobHistoryEdition.DATE_PURGE.getNom());
        Expression<Long> count = criteriaBuilder.count(selectDate);

        // Regroupement des résultat par date de purge
        query.groupBy(selectDate);
        // Paramétrage du tri
        query.orderBy(criteriaBuilder.desc(selectDate));
        // Création des statistiques d'édition désirées
        query.select(criteriaBuilder.construct(StatJobHistory.class, selectDate, count));

        // Exécution de la requête
        List<StatJobHistory> liste = findAll(query);

        // Existe-t-il des dates de purge non renseignées ?
        Optional<StatJobHistory> isNullStatJobHistory = liste.stream().filter(session -> session.getDatePurge() == null).findAny();

        // S'il y en a, elles n'ont pas été comptées par la précédente requête (valeur 0)
        if (isNullStatJobHistory.isPresent())
        {
            // Création d'une Map à partir de la liste
            Map<Date, StatJobHistory> mapStatJobHistory = liste.stream()
                .filter(stat -> stat.getDatePurge() != null)
                .collect(Collectors.toMap(StatJobHistory::getDatePurge, stat -> stat));

            // Paramétrage de la recherche du nombre de jour demandé non renseigné
            criteriaBuilder = entityManagerEdition.getCriteriaBuilder();
            CriteriaQuery<Long> queryNbreNullDatePurge = criteriaBuilder.createQuery(Long.class);
            from = queryNbreNullDatePurge.from(JobHistory.class);
            CriteriaQuery<Long> selectNbreNullDatePurge = queryNbreNullDatePurge.select(criteriaBuilder.count(from));

            // Recherche des jours demandés non renseignés, pour une application donnée
            queryNbreNullDatePurge.where(criteriaBuilder.and(
                criteriaBuilder.equal(from.get(ProprieteJobHistory.DESTINATION_EDITION.getNom())
                    .get(ProprieteJobHistory.APPLICATION_ORIGINE.getNom()), applicationOrigine),
                criteriaBuilder.isNull(from.get(ProprieteStatistiqueJobHistoryEdition.ARCHIVAGE_EDITION.getNom())
                    .<Date> get(ProprieteStatistiqueJobHistoryEdition.DATE_PURGE.getNom()))));

            // Exécution de la requête sur le nombre de jour demandé non renseigné,
            // et remplacement du résultat précédent par celui-ci
            StatJobHistory statJobHistory = new StatJobHistory();
            statJobHistory.setNombreEditions(
                entityManagerEdition.createQuery(selectNbreNullDatePurge).getSingleResult().longValue());
            mapStatJobHistory.put(null, statJobHistory);

            return mapStatJobHistory.values().stream().collect(Collectors.toList());
        }

        return liste;
    }

    /**
     * {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.edition.dao.StatistiqueJobHistoryDao#compteJobHistoryParJourDemande(java.lang.String).
     */
    @Override
    public List<StatJobHistory> compteJobHistoryParJourDemande(String applicationOrigine)
    {
        LOG.debug(">>> Début méthode compteJobHistoryParJourDemande");
        // Cas des paramètres non fournis
        controlerParametres(applicationOrigine);

        // Initialisation de la recherche
        CriteriaBuilder criteriaBuilder = entityManagerEdition.getCriteriaBuilder();
        CriteriaQuery<StatJobHistory> query = criteriaBuilder.createQuery(StatJobHistory.class);
        Root<JobHistory> from = query.from(JobHistory.class);

        // Filtre sur l'application
        query.where(criteriaBuilder.equal(from.get(ProprieteJobHistory.DESTINATION_EDITION.getNom())
            .get(ProprieteJobHistory.APPLICATION_ORIGINE.getNom()), applicationOrigine));

        // Initialisation de la sélection
        Expression<Date> selectJour = from.<Date> get(ProprieteStatistiqueJobHistoryEdition.JOUR_DEMANDE_EDITION.getNom());
        Expression<Long> count = criteriaBuilder.count(selectJour);

        // Regroupement des résultat par jour de demande
        query.groupBy(selectJour);
        // Paramétrage du tri
        query.orderBy(criteriaBuilder.desc(selectJour));
        // Création des statistiques d'édition désirées
        query.select(criteriaBuilder.construct(StatJobHistory.class, count, selectJour));

        // Exécution de la requête
        List<StatJobHistory> liste = findAll(query);

        // Création d'une Map à partir de la liste
        Map<Date, StatJobHistory> mapStatJobHistory = liste.stream()
            .collect(Collectors.toMap(StatJobHistory::getJourDemande, stat -> stat));

        // S'il y a des jours non renseignés, ils n'ont pas été comptés par la précédente requête (valeur 0)
        if (mapStatJobHistory.containsKey(null))
        {
            // Paramétrage de la recherche du nombre de jour demandé non renseigné
            criteriaBuilder = entityManagerEdition.getCriteriaBuilder();
            CriteriaQuery<Long> queryNbreNullJourDemande = criteriaBuilder.createQuery(Long.class);
            from = queryNbreNullJourDemande.from(JobHistory.class);
            CriteriaQuery<Long> selectNbreNullJourDemande = queryNbreNullJourDemande.select(criteriaBuilder.count(from));

            // Recherche des jours demandés non renseignés, pour une application donnée
            queryNbreNullJourDemande.where(criteriaBuilder.and(
                criteriaBuilder.equal(from.get(ProprieteJobHistory.DESTINATION_EDITION.getNom())
                    .get(ProprieteJobHistory.APPLICATION_ORIGINE.getNom()), applicationOrigine),
                criteriaBuilder.isNull(from.get(
                    ProprieteStatistiqueJobHistoryEdition.JOUR_DEMANDE_EDITION.getNom()))));

            // Exécution de la requête sur le nombre de jour demandé non renseigné,
            // et remplacement du résultat précédent par celui-ci
            mapStatJobHistory.get(null).setNombreEditions(
                entityManagerEdition.createQuery(selectNbreNullJourDemande).getSingleResult().longValue());

            return mapStatJobHistory.values().stream().collect(Collectors.toList());
        }
        return liste;
    }

    /**
     * {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.edition.dao.StatistiqueJobHistoryDao#compteJobHistoryParStatus(java.lang.String).
     */
    @Override
    public List<StatJobHistory> compteJobHistoryParStatus(String applicationOrigine)
    {
        LOG.debug(">>> Début méthode compteJobHistoryParStatus");
        // Cas des paramètres non fournis
        controlerParametres(applicationOrigine);

        // Initialisation de la recherche
        CriteriaBuilder criteriaBuilder = entityManagerEdition.getCriteriaBuilder();
        CriteriaQuery<StatJobHistory> query = criteriaBuilder.createQuery(StatJobHistory.class);
        Root<JobHistory> from = query.from(JobHistory.class);

        // Filtre sur l'application
        query.where(criteriaBuilder.equal(from.get(ProprieteJobHistory.DESTINATION_EDITION.getNom())
            .get(ProprieteJobHistory.APPLICATION_ORIGINE.getNom()), applicationOrigine));

        // Initialisation de la sélection
        Expression<String> selectStatut = from.get(
            ProprieteJobHistory.ORDONNANCEMENT_EDITION.getNom()).get(ProprieteJobHistory.STATUT.getNom());
        Expression<Long> count = criteriaBuilder.count(selectStatut);

        // Regroupement des résultat par statut d'édition
        query.groupBy(selectStatut);
        // Création des statistiques d'édition désirées
        query.select(criteriaBuilder.construct(StatJobHistory.class, selectStatut, count));

        // Exécution de la requête
        List<StatJobHistory> liste = findAll(query);

        // Existe-t-il des statuts non renseignés ?
        Optional<StatJobHistory> isNullStatJobHistory = liste.stream().filter(stat -> stat.getStatus() == null).findAny();

        // S'il y en a, ils n'ont pas été comptées par la précédente requête (valeur 0)
        if (isNullStatJobHistory.isPresent())
        {
            // Création d'une Map à partir de la liste
            Map<String, StatJobHistory> mapStatJobHistory = liste.stream()
                .filter(stat -> stat.getStatus() != null)
                .collect(Collectors.toMap(StatJobHistory::getStatus, stat -> stat));

            // Paramétrage de la recherche du nombre de statut non renseigné
            criteriaBuilder = entityManagerEdition.getCriteriaBuilder();
            CriteriaQuery<Long> queryNbreNullStatut = criteriaBuilder.createQuery(Long.class);
            from = queryNbreNullStatut.from(JobHistory.class);
            CriteriaQuery<Long> selectNbreNullStatut = queryNbreNullStatut.select(criteriaBuilder.count(from));

            // Recherche des statuts non renseignés, pour une application donnée
            queryNbreNullStatut.where(criteriaBuilder.and(
                criteriaBuilder.equal(from.get(ProprieteJobHistory.DESTINATION_EDITION.getNom())
                    .get(ProprieteJobHistory.APPLICATION_ORIGINE.getNom()), applicationOrigine),
                criteriaBuilder.isNull(from.get(
                    ProprieteJobHistory.ORDONNANCEMENT_EDITION.getNom()).get(ProprieteJobHistory.STATUT.getNom()))));

            // Exécution de la requête sur le statut demandé non renseigné,
            // et ajout du résultat dans la Map précédemment calculée
            StatJobHistory statJobHistory = new StatJobHistory();
            statJobHistory.setNombreEditions(
                entityManagerEdition.createQuery(selectNbreNullStatut).getSingleResult().longValue());
            mapStatJobHistory.put(null, statJobHistory);

            return mapStatJobHistory.values().stream().collect(Collectors.toList());
        }

        return liste;
    }

    /**
     * {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.edition.dao.StatistiqueJobHistoryDao#compteJobHistoryParTypeEtStatus(java.lang.String).
     */
    @Override
    public List<StatJobHistory> compteJobHistoryParTypeEtStatus(String applicationOrigine)
    {
        LOG.debug(">>> Début méthode compteJobHistoryParTypeEtStatus");
        // Cas des paramètres non fournis
        controlerParametres(applicationOrigine);

        // Initialisation de la recherche
        CriteriaBuilder criteriaBuilder = entityManagerEdition.getCriteriaBuilder();
        CriteriaQuery<StatJobHistory> query = criteriaBuilder.createQuery(StatJobHistory.class);
        Root<JobHistory> from = query.from(JobHistory.class);

        // Filtre sur l'application
        query.where(criteriaBuilder
            .and(criteriaBuilder.equal(from
                .get(ProprieteStatistiqueJobHistoryEdition.DESTINATION_EDITION.getNom())
                .get(ProprieteStatistiqueJobHistoryEdition.APPLICATION_ORIGINE.getNom()), applicationOrigine)));

        // Initialisation de la sélection
        Expression<String> selectStatut = from.get(
            ProprieteJobHistory.ORDONNANCEMENT_EDITION.getNom()).get(ProprieteJobHistory.STATUT.getNom());
        Expression<String> selectId = from.get(ProprieteStatistiqueJobHistoryEdition.ID_EDITION.getNom());
        Expression<Long> count = criteriaBuilder
            .count(from.get(ProprieteStatistiqueJobHistoryEdition.UID.getNom()));

        // Regroupement des résultat par statut et identifiant
        query.groupBy(selectStatut, selectId);
        // Création des statistiques d'édition désirées
        query.select(criteriaBuilder.construct(StatJobHistory.class, selectStatut, selectId, count));

        // Exécution de la requête
        return findAll(query);
    }

    /**
     * {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.edition.dao.StatistiqueJobHistoryDao#compteTailleEditionParType(java.lang.String).
     */
    @Override
    public List<StatJobHistory> compteTailleEditionParType(String applicationOrigine)
    {
        LOG.debug(">>> Début méthode compteTailleEditionParType");
        // Cas des paramètres non fournis
        controlerParametres(applicationOrigine);

        // Initialisation de la recherche
        CriteriaBuilder criteriaBuilder = entityManagerEdition.getCriteriaBuilder();
        CriteriaQuery<StatJobHistory> query = criteriaBuilder.createQuery(StatJobHistory.class);
        Root<JobHistory> from = query.from(JobHistory.class);

        // Filtre sur l'application et le statut
        query.where(criteriaBuilder.and(criteriaBuilder.equal(from.get(ProprieteStatistiqueJobHistoryEdition.DESTINATION_EDITION.getNom())
            .get(ProprieteStatistiqueJobHistoryEdition.APPLICATION_ORIGINE.getNom()), applicationOrigine), criteriaBuilder.or(
                criteriaBuilder.equal(from.get(ProprieteStatistiqueJobHistoryEdition.ORDONNANCEMENT_EDITION.getNom())
                    .get(ProprieteStatistiqueJobHistoryEdition.STATUT_EDITION.getNom()), JobHistory.STATUS_DISPONIBLE),
                criteriaBuilder.equal(from.get(ProprieteStatistiqueJobHistoryEdition.ORDONNANCEMENT_EDITION.getNom())
                    .get(ProprieteStatistiqueJobHistoryEdition.STATUT_EDITION.getNom()), JobHistory.STATUS_DISPONIBLE_NON_ENVOYE))));

        // Initialisation de la sélection
        Expression<String> selectStatut = from.get(
            ProprieteJobHistory.ORDONNANCEMENT_EDITION.getNom()).get(ProprieteJobHistory.STATUT.getNom());
        Expression<String> selectId = from.get(ProprieteStatistiqueJobHistoryEdition.ID_EDITION.getNom());
        Expression<Long> count = criteriaBuilder
            .count(from.get(ProprieteStatistiqueJobHistoryEdition.UID.getNom()));
        Expression<Long> selectTotal = criteriaBuilder
            .sumAsLong(from.get(ProprieteStatistiqueJobHistoryEdition.STOCKAGE_EDITION.getNom())
                .get(ProprieteStatistiqueJobHistoryEdition.TAILLE_FICHIER_EDITION.getNom()));
        Expression<Double> selectMoyenne = criteriaBuilder
            .avg(from.get(ProprieteStatistiqueJobHistoryEdition.STOCKAGE_EDITION.getNom())
                .get(ProprieteStatistiqueJobHistoryEdition.TAILLE_FICHIER_EDITION.getNom()));

        // Regroupement des résultat par statut et identifiant
        query.groupBy(selectStatut, selectId);
        // Création des statistiques d'édition désirées
        query.select(criteriaBuilder
            .construct(StatJobHistory.class, selectStatut, selectId, count, selectTotal, selectMoyenne));

        // Exécution de la requête
        return findAll(query);
    }

    /**
     * Méthode permettant de contrôler les paramètres.
     *
     * @param applicationOrigine application liée aux éditions
     */
    private void controlerParametres(final String applicationOrigine)
    {
        if (StringUtils.isEmpty(applicationOrigine))
        {
            throw new ProgrammationException("Aucune application d'origine n'a été fournie");
        }
    }
}