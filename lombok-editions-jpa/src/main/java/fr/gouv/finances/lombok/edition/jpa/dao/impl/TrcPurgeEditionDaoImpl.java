/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 */
package fr.gouv.finances.lombok.edition.jpa.dao.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import fr.gouv.finances.lombok.edition.bean.TrcPurgeEdition;
import fr.gouv.finances.lombok.edition.dao.ProprieteTrcPurgeEdition;
import fr.gouv.finances.lombok.edition.dao.TrcPurgeEditionDao;
import fr.gouv.finances.lombok.edition.util.Control;
import fr.gouv.finances.lombok.util.persistance.ModeCritereRecherche;

/**
 * Implémentation JPA du DAO permettant de gérer les données des traces de purge d'édition.
 *
 * @author Christophe Breheret-Girardin
 */
@ConditionalOnProperty(name = "lombok.orm.jpa", havingValue="true")
@Repository("trcpurgeeditiondaoimpl")
@Transactional(transactionManager = "editionTransactionManager")
public class TrcPurgeEditionDaoImpl extends BaseDaoEditionJpaImpl implements TrcPurgeEditionDao
{
    protected static final Logger LOG = LoggerFactory.getLogger(TrcPurgeEditionDaoImpl.class);

    public TrcPurgeEditionDaoImpl()
    {
        super();
    }

    /** 
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.edition.dao.TrcPurgeEditionDao#checkExistencePurgeDateIdentique(
     * fr.gouv.finances.lombok.edition.bean.TrcPurgeEdition)
     */
    @Override
    public boolean checkExistencePurgeDateIdentique(TrcPurgeEdition trcPurgeEdition)
    {
        // Journalisation de la recherche
        LOG.debug("Recherche d'existence d'une trace de purge d'édition avec une date identique que '{}'"
            , trcPurgeEdition);

        // L'entité doit être renseignée
        Control.assertTrcPurgeEditionNotNull(trcPurgeEdition);

        // Paramétrage de la recherche
        CriteriaBuilder criteriaBuilder = entityManagerEdition.getCriteriaBuilder();
        CriteriaQuery<Long> query = criteriaBuilder.createQuery(Long.class);
        Root<TrcPurgeEdition> root = query.from(TrcPurgeEdition.class);

        // Paramétrage des critères
        List<Predicate> predicates = new ArrayList<>();
        predicates.add(criteriaBuilder.equal(
            root.<String>get(ProprieteTrcPurgeEdition.APPLICATION_ORIGINE.getNom())
                , trcPurgeEdition.getApplicationOrigine()));
        predicates.add(criteriaBuilder.equal(root.<Date>get(ProprieteTrcPurgeEdition.DATE_DEBUT_PURGE.getNom())
            , trcPurgeEdition.getDateDebutPurge()));
        if (trcPurgeEdition.getId() != null)
        {
            predicates.add(criteriaBuilder.notEqual(root.<String>get("id"), trcPurgeEdition.getId()));
        }
        query.where(predicates.toArray(new Predicate[]{}));

        // Paramétrage du comptage
        query.select(criteriaBuilder.count(root));

        // Exécution de la recherche
        return find(query) > 0;
    }

    /** 
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.edition.dao.TrcPurgeEditionDao#deleteTrcPurgeEdition(
     * fr.gouv.finances.lombok.edition.bean.TrcPurgeEdition)
     */
    @Override
    public void deleteTrcPurgeEdition(TrcPurgeEdition trcPurgeEdition)
    {
        // Journalisation de la suppression
        LOG.debug("Suppression de la trace de purge d'édition '{}'", trcPurgeEdition);

        // Exécution de la suppression
        deleteObject(trcPurgeEdition);
    }

    /** 
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.edition.dao.TrcPurgeEditionDao#deleteTrcPurgeEditions(
     * java.util.Collection)
     */
    @Override
    public void deleteTrcPurgeEditions(Collection<TrcPurgeEdition> lesTrcPurgesEditionSupp)
    {
        // Journalisation de la suppression
        LOG.debug("Suppression de plusieurs trace de purge d'édition");

        // La requête n'est exécutée que si le paramètre est renseigné
        if (lesTrcPurgesEditionSupp != null && !lesTrcPurgesEditionSupp.isEmpty())
        {
            // Paramétrage du cache
            entityManagerEdition.setProperty("javax.persistence.CacheRetrieveMode", "BYPASS");
            entityManagerEdition.setProperty("javax.persistence.CacheStoreMode", "BYPASS");

            // Exécution des suppressions
            lesTrcPurgesEditionSupp.stream().forEach(this::deleteObject);

            // Synchronisation avec la base de données
            entityManagerEdition.flush();
            entityManagerEdition.clear();
        }
    }

    /** 
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.edition.dao.TrcPurgeEditionDao
     * #findLesTrcPurgeEditionEnCoursAnterieuresAUneDate(java.util.Date, java.lang.String)
     */
    @Override
    public List<TrcPurgeEdition> findLesTrcPurgeEditionEnCoursAnterieuresAUneDate(
        Date dateDebutPurge, String applicationOrigine)
    {
        // Journalisation de la recherche
        LOG.debug("Recherche de trace de purge d'édition sur l'application '{}' antérieure à la date '{}'"
            , applicationOrigine, dateDebutPurge);

        // Paramétrage de la recherche
        CriteriaBuilder criteriaBuilder = entityManagerEdition.getCriteriaBuilder();
        CriteriaQuery<TrcPurgeEdition> query = criteriaBuilder.createQuery(TrcPurgeEdition.class);
        Root<TrcPurgeEdition> root = query.from(TrcPurgeEdition.class);

        // Paramétrage des critères
        query.where(criteriaBuilder.equal(
            root.<String>get(ProprieteTrcPurgeEdition.APPLICATION_ORIGINE.getNom()), applicationOrigine)
            , criteriaBuilder.equal(root.<String>get(ProprieteTrcPurgeEdition.ETAT_PURGE.getNom())
                , TrcPurgeEdition.EN_COURS_EXECUTION)
            , criteriaBuilder.lessThan(root.<Date>get(ProprieteTrcPurgeEdition.DATE_DEBUT_PURGE.getNom())
                , dateDebutPurge));

        // Exécution de la recherche
        return findAll(query);
    }

    /** 
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.edition.dao.TrcPurgeEditionDao#findLesTrcPurgeEditionPourUneDate(
     * java.util.Date, java.lang.String, int)
     */
    @Override
    public List<TrcPurgeEdition> findLesTrcPurgeEditionPourUneDate(Date dateDebutPurge, String applicationOrigine
        , int max)
    {
        // Journalisation de la recherche
        LOG.debug("Recherche de trace de purge d'édition sur l'application '{}' pour la date '{}' (max '{}' éditions)"
            , applicationOrigine, dateDebutPurge, max);

        // Si un des paramètres est manquant, la requête n'est pas exécutée
        if (dateDebutPurge == null || StringUtils.isEmpty(applicationOrigine))
        {
            return new ArrayList<>();
        }

        // Calcul de la borne supérieure à appliquer à la recherche
        Calendar cal = Calendar.getInstance(Locale.FRANCE);
        cal.setTime(dateDebutPurge);
        cal.add(Calendar.DATE, 1);
        Date dateDebutPurgePlusUn = cal.getTime();

        // Paramétrage de la recherche
        CriteriaBuilder criteriaBuilder = entityManagerEdition.getCriteriaBuilder();
        CriteriaQuery<TrcPurgeEdition> query = criteriaBuilder.createQuery(TrcPurgeEdition.class);
        Root<TrcPurgeEdition> root = query.from(TrcPurgeEdition.class);

        // Paramétrage des critères
        query.where(criteriaBuilder.equal(
            root.<String>get(ProprieteTrcPurgeEdition.APPLICATION_ORIGINE.getNom()), applicationOrigine)
            , criteriaBuilder.greaterThanOrEqualTo(root.<Date>get(ProprieteTrcPurgeEdition.DATE_DEBUT_PURGE.getNom())
                , dateDebutPurge)
            , criteriaBuilder.lessThan(root.<Date>get(ProprieteTrcPurgeEdition.DATE_DEBUT_PURGE.getNom())
                , dateDebutPurgePlusUn));

        // Paramétrage du tri
        query.orderBy(criteriaBuilder.desc(root.get(ProprieteTrcPurgeEdition.DATE_DEBUT_PURGE.getNom())));

        // Exécution de la recherche
        return findAll(query, max);
    }

    /** 
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.edition.dao.TrcPurgeEditionDao#findLesTrcPurgeEditionPourUnInstantDemarrage(
     * java.util.Date, java.lang.String)
     */
    @Override
    public List<TrcPurgeEdition> findLesTrcPurgeEditionPourUnInstantDemarrage(
        Date instantDemarrage, String applicationOrigine)
    {
        // Journalisation de la recherche
        LOG.debug("Recherche de trace de purge d'édition sur l'application '{}' pour un instant de démarrage '{}'"
            , applicationOrigine, instantDemarrage);

        // Paramétrage de la recherche
        CriteriaBuilder criteriaBuilder = entityManagerEdition.getCriteriaBuilder();
        CriteriaQuery<TrcPurgeEdition> query = criteriaBuilder.createQuery(TrcPurgeEdition.class);
        Root<TrcPurgeEdition> root = query.from(TrcPurgeEdition.class);

        // Paramétrage des critères
        query.where(criteriaBuilder.equal(
            root.<String>get(ProprieteTrcPurgeEdition.APPLICATION_ORIGINE.getNom()), applicationOrigine)
            , criteriaBuilder.equal(root.<Date>get(ProprieteTrcPurgeEdition.DATE_DEBUT_PURGE.getNom())
                , instantDemarrage));

        // Exécution de la recherche
        return findAll(query);
    }

    /** 
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.edition.dao.TrcPurgeEditionDao#findTousLesTrcPurgeEdition(
     * java.lang.String, int)
     */
    @Override
    public List<TrcPurgeEdition> findTousLesTrcPurgeEdition(String applicationOrigine, int max)
    {
        // Journalisation de la recherche
        LOG.debug("Recherche de toutes les traces de purge d'éditionn sur l'application '{}' "
            + "(max '{}' éditions)", applicationOrigine, max);

        // Si le paramètre est manquant, la requête n'est pas exécutée
        if (StringUtils.isEmpty(applicationOrigine))
        {
            return new ArrayList<>();
        }

        // Paramétrage de la recherche
        CriteriaBuilder criteriaBuilder = entityManagerEdition.getCriteriaBuilder();
        CriteriaQuery<TrcPurgeEdition> query = criteriaBuilder.createQuery(TrcPurgeEdition.class);
        Root<TrcPurgeEdition> root = query.from(TrcPurgeEdition.class);

        // Paramétrage des critères
        query.where(criteriaBuilder.equal(
            root.<String>get(ProprieteTrcPurgeEdition.APPLICATION_ORIGINE.getNom()), applicationOrigine));

        // Paramétrage du tri
        query.orderBy(criteriaBuilder.desc(root.get(ProprieteTrcPurgeEdition.DATE_DEBUT_PURGE.getNom())));

        // Exécution de la recherche
        return findAll(query, max);
    }

    /** 
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.edition.dao.TrcPurgeEditionDao#findTrcPurgeEditionEnCours(java.lang.String)
     */
    @Override
    public List<TrcPurgeEdition> findTrcPurgeEditionEnCours(String applicationOrigine)
    {
        // Journalisation de la recherche
        LOG.debug("Recherche de toutes les traces de purge d'édition sur l'application '{}' en cours"
            , applicationOrigine);

        // Préparation des critères
        Map<String, String> criteres = new HashMap<>();
        criteres.put(ProprieteTrcPurgeEdition.APPLICATION_ORIGINE.getNom(), applicationOrigine);
        criteres.put(ProprieteTrcPurgeEdition.ETAT_PURGE.getNom(), TrcPurgeEdition.EN_COURS_EXECUTION);

        // Recherche des traces de purge
        return findAllByCriterias(TrcPurgeEdition.class, criteres, ModeCritereRecherche.EQUAL);
    }

    /** 
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.edition.dao.TrcPurgeEditionDao#findTrcPurgeEditionParPeriodePurge(
     * java.util.Date, java.util.Date, java.lang.String, int)
     */
    @Override
    public List<TrcPurgeEdition> findTrcPurgeEditionParPeriodePurge(Date debutPeriodePurge, Date finPeriodePurge,
        String applicationOrigine, int max)
    {
        // Journalisation de la recherche
        LOG.debug("Recherche des traces de purge d'édition sur l'application '{}' entre '{}' et '{}' (max '{}' éditions)"
            , applicationOrigine, debutPeriodePurge, finPeriodePurge, max);

        // Paramétrage de la recherche
        CriteriaBuilder criteriaBuilder = entityManagerEdition.getCriteriaBuilder();
        CriteriaQuery<TrcPurgeEdition> query = criteriaBuilder.createQuery(TrcPurgeEdition.class);
        Root<TrcPurgeEdition> root = query.from(TrcPurgeEdition.class);

        // Paramétrage des critères
        query.where(criteriaBuilder.equal(
                root.<String>get(ProprieteTrcPurgeEdition.APPLICATION_ORIGINE.getNom()), applicationOrigine)
            , criteriaBuilder.greaterThanOrEqualTo(root.<Date>get(ProprieteTrcPurgeEdition.DATE_DEBUT_PURGE.getNom())
                , debutPeriodePurge)
            , criteriaBuilder.lessThanOrEqualTo(root.<Date>get(ProprieteTrcPurgeEdition.DATE_DEBUT_PURGE.getNom())
                , finPeriodePurge));

        // Paramétrage du tri
        query.orderBy(criteriaBuilder.desc(root.get(ProprieteTrcPurgeEdition.DATE_DEBUT_PURGE.getNom())));

        // Exécution de la recherche
        return findAll(query, max);
    }

    /** 
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.edition.dao.TrcPurgeEditionDao#isUnBatchPurgeEnCoursExecution(
     * java.lang.String)
     */
    @Override
    public boolean isUnBatchPurgeEnCoursExecution(String applicationOrigine)
    {
        // Journalisation de la recherche
        LOG.debug("Recherche si un  batch concernant les traces de purge d'édition sur l'application '{}'"
            + "est encore d'exécution", applicationOrigine);

        // Préparation des critères
        Map<String, String> criteres = new HashMap<>();
        criteres.put(ProprieteTrcPurgeEdition.APPLICATION_ORIGINE.getNom(), applicationOrigine);
        criteres.put(ProprieteTrcPurgeEdition.ETAT_PURGE.getNom(), TrcPurgeEdition.EN_COURS_EXECUTION);

        // Existe-il au moins un résultat ?
        return countTotalNumberOfLines(TrcPurgeEdition.class, criteres, ModeCritereRecherche.EQUAL) > 0;
    }

    /** 
     * {@inheritDoc}
     * @see fr.gouv.finances.lombok.edition.dao.TrcPurgeEditionDao#saveTrcPurgeEdition(
     * fr.gouv.finances.lombok.edition.bean.TrcPurgeEdition)
     */
    @Override
    public void saveTrcPurgeEdition(TrcPurgeEdition trcPurgeEdition)
    {
        // Journalisation de la mise à jour
        LOG.debug("Insertion de la trace de purge d'édition '{}'", trcPurgeEdition);

        // Exécution de la mise à jour
        modifyObject(trcPurgeEdition);
    }
}
