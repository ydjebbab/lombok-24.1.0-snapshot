/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 */
package fr.gouv.finances.lombok.edition.jpa.dao.impl;

import java.util.Date;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import fr.gouv.finances.lombok.edition.bean.JobHistory;
import fr.gouv.finances.lombok.edition.dao.JobHistoryBatchDao;
import fr.gouv.finances.lombok.edition.dao.ProprieteJobHistory;
import fr.gouv.finances.lombok.edition.util.Control;
import fr.gouv.finances.lombok.util.exception.ProgrammationException;
import fr.gouv.finances.lombok.util.persistance.ScrollIterator;

/**
 * Implémentation JPA du DAO permettant de gérer les données d'historique des travaux d'édition, en mode batch.
 *
 * @author Christophe Breheret-Girardin
 */
@ConditionalOnProperty(name = "lombok.orm.jpa", havingValue="true")
// CF : 18/10/2018 Modification du nom du bean dans le contexte Spring: 
// jobhistorybatchdaoimpl au lieu de jobhistorybatchdao
@Repository("jobhistorybatchdaoimpl")
@Transactional(transactionManager = "editionTransactionManager")
public class JobHistoryBatchDaoImpl extends BaseDaoEditionJpaImpl implements JobHistoryBatchDao
{
    /** Propriété du cache */
    private static final String PROPRIETE_CACHE = "javax.persistence.CacheStoreMode";

    /** Valeur du cache */
    private static final String VALEUR_CACHE = "BYPASS";

    /**
     * {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.edition.dao.JobHistoryBatchDao#listeEditionAPurgerIterator(int, java.util.Date,
     *      java.lang.String).
     */
    @Override
    public ScrollIterator listeEditionAPurgerIterator(int nombreOccurences, Date dateCourante, String applicationOrigine)
    {
        // Contrôle des paramètres
        Control.assertApplicationNonRenseignee(applicationOrigine);
        if (dateCourante == null)
        {
            throw new ProgrammationException("La date de recherche de l'historique des travaux d'édition"
                + " n'a pas été fournie");
        }

        // Paramètrage de la recherche
        CriteriaBuilder criteriaBuilder = entityManagerEdition.getCriteriaBuilder();
        CriteriaQuery<JobHistory> query = criteriaBuilder.createQuery(JobHistory.class);
        Root<JobHistory> root = query.from(JobHistory.class);

        // Paramétrage du cache
        entityManagerEdition.setProperty(PROPRIETE_CACHE, VALEUR_CACHE);

        // Paramètrage des critères
        // sur les éditions qui ne sont pas en cours ou en attente, dont la date de purge est antérieure à celle fournie
        // et liées à l'application fournie
        query.where(criteriaBuilder.equal(root.get(ProprieteJobHistory.DESTINATION_EDITION.getNom())
            .get(ProprieteJobHistory.APPLICATION_ORIGINE.getNom()), applicationOrigine),
            criteriaBuilder.lessThanOrEqualTo(root.get(ProprieteJobHistory.ARCHIVAGE_EDITION.getNom())
                .get(ProprieteJobHistory.DATE_PURGE.getNom()), dateCourante),
            criteriaBuilder.notEqual(root.get(ProprieteJobHistory.ORDONNANCEMENT_EDITION.getNom())
                .get(ProprieteJobHistory.STATUT.getNom()), JobHistory.STATUS_EN_COURS_EXECUTION),
            criteriaBuilder.notEqual(root.get(ProprieteJobHistory.ORDONNANCEMENT_EDITION.getNom())
                .get(ProprieteJobHistory.STATUT.getNom()), JobHistory.STATUS_EN_ATTENTE_EXECUTION));

        // Exécution de la recherche
        return getScrollIterator(query, nombreOccurences);
    }

    /**
     * {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.edition.dao.JobHistoryBatchDao#listeEditionsDisponiblesIterator(int,
     *      java.lang.String).
     */
    @Override
    public ScrollIterator listeEditionsDisponiblesIterator(int nombreOccurences, String applicationOrigine)
    {
        // Contrôle des paramètres
        Control.assertApplicationNonRenseignee(applicationOrigine);

        // Paramètrage de la recherche
        CriteriaBuilder criteriaBuilder = entityManagerEdition.getCriteriaBuilder();
        CriteriaQuery<JobHistory> query = criteriaBuilder.createQuery(JobHistory.class);
        Root<JobHistory> root = query.from(JobHistory.class);

        // Paramétrage du cache
        entityManagerEdition.setProperty(PROPRIETE_CACHE, VALEUR_CACHE);

        // Paramètrage des critères
        // sur les éditions en disponibles ou non envoyés, liées à l'application fournie
        query.where(criteriaBuilder.equal(root.get(ProprieteJobHistory.DESTINATION_EDITION.getNom())
            .get(ProprieteJobHistory.APPLICATION_ORIGINE.getNom()), applicationOrigine), criteriaBuilder.or(criteriaBuilder.equal(
                root.get(ProprieteJobHistory.ORDONNANCEMENT_EDITION.getNom())
                    .get(ProprieteJobHistory.STATUT.getNom()),
                JobHistory.STATUS_DISPONIBLE), criteriaBuilder.equal(
                    root.get(ProprieteJobHistory.ORDONNANCEMENT_EDITION.getNom())
                        .get(ProprieteJobHistory.STATUT.getNom()),
                    JobHistory.STATUS_DISPONIBLE_NON_ENVOYE)));

        // Exécution de la recherche
        return getScrollIterator(query, nombreOccurences);
    }

    /**
     * {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.edition.dao.JobHistoryBatchDao#listeEditionsEnEchecIterator(int, java.lang.String).
     */
    @Override
    public ScrollIterator listeEditionsEnEchecIterator(int nombreOccurences, String applicationOrigine)
    {
        // Contrôle des paramètres
        Control.assertApplicationNonRenseignee(applicationOrigine);

        // Paramètrage de la recherche
        CriteriaBuilder criteriaBuilder = entityManagerEdition.getCriteriaBuilder();
        CriteriaQuery<JobHistory> query = criteriaBuilder.createQuery(JobHistory.class);
        Root<JobHistory> root = query.from(JobHistory.class);

        // Paramétrage du cache
        entityManagerEdition.setProperty(PROPRIETE_CACHE, VALEUR_CACHE);

        // Paramètrage des critères
        // sur les éditions en échec, liées à l'application fournie
        query.where(criteriaBuilder.equal(root.get(ProprieteJobHistory.DESTINATION_EDITION.getNom())
            .get(ProprieteJobHistory.APPLICATION_ORIGINE.getNom()), applicationOrigine),
            criteriaBuilder.equal(root.get(ProprieteJobHistory.ORDONNANCEMENT_EDITION.getNom())
                .get(ProprieteJobHistory.STATUT.getNom()), JobHistory.STATUS_ECHEC));

        // Exécution de la recherche
        return getScrollIterator(query, nombreOccurences);
    }
}