/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 */
package fr.gouv.finances.lombok.edition.jpa.dao.impl;

import java.io.File;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import fr.gouv.finances.lombok.edition.bean.ContenuEdition;
import fr.gouv.finances.lombok.edition.bean.FiltreEdition;
import fr.gouv.finances.lombok.edition.bean.JobHistory;
import fr.gouv.finances.lombok.edition.bean.OrdoEdition;
import fr.gouv.finances.lombok.edition.bean.ProfilDesti;
import fr.gouv.finances.lombok.edition.dao.JobHistoryDao;
import fr.gouv.finances.lombok.edition.dao.ProprieteJobHistory;
import fr.gouv.finances.lombok.edition.util.Control;
import fr.gouv.finances.lombok.edition.util.FileUtils;
import fr.gouv.finances.lombok.edition.util.NombreMaxEdition;
import fr.gouv.finances.lombok.util.exception.ProgrammationException;
import fr.gouv.finances.lombok.util.persistance.ModeCritereRecherche;

/**
 * Implémentation JPA du DAO permettant de gérer les données d'historique des travaux d'édition.
 *
 * @author Christophe Breheret-Girardin
 */
@ConditionalOnProperty(name = "lombok.orm.jpa", havingValue="true")
@Repository("jobhistorydaoimpl")
@Transactional(transactionManager = "editionTransactionManager")
public class JobHistoryDaoImpl extends JobHistoryBatchDaoImpl implements JobHistoryDao
{

    protected static final Logger LOGGER = LoggerFactory.getLogger(JobHistoryDaoImpl.class);

    /** Constante de journalisation des recherches */
    protected static final String JOURNALISATION_RECHERCHE = "Recherche des historiques des travaux d'édition, ";

    /**
     * {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.edition.dao.JobHistoryDao#deleteJobHistory(fr.gouv.finances.lombok.edition.bean.JobHistory).
     */
    @Override
    public void deleteJobHistory(JobHistory jobHistory)
    {
        LOGGER.debug(">>> Debut methode deleteJobHistory()");
        // Le paramètre doit être présent
        Control.assertJobHistoryNotNull(jobHistory);

        // L'historique des travaux d'édition doit avoir été chargé
        Control.assertJobHistoryCharge(jobHistory);

        // Journalisation de la mise à jour
        LOGGER.debug("Suppression de l'historique des travaux d'édition (UUID #{})", jobHistory.getEditionUuid());

        // Exécution de la suppression
        deleteObject(jobHistory);
    }

    /**
     * {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.edition.dao.JobHistoryDao#deleteJobsHistories(java.util.List).
     */
    @Override
    public void deleteJobsHistories(List<JobHistory> historiques)
    {
        LOGGER.debug(">>> Debut methode deleteJobsHistories()");
        // La liste doit avoir été fournie
        Control.assertListJobHistoryNotNull(historiques);

        // Journalisation de la mise à jour
        LOGGER.debug("Suppression de {} historique(s) des travaux d'édition", historiques.size());

        // Chaque historique des travaux d'édition doit avoir été chargé
        historiques.stream().forEach(Control::assertJobHistoryCharge);

        // Paramétrage du cache
        entityManagerEdition.setProperty("javax.persistence.CacheRetrieveMode", "BYPASS");
        entityManagerEdition.setProperty("javax.persistence.CacheStoreMode", "BYPASS");

        // Demande de suppression
        historiques.stream().forEach(this::deleteObject);

        // Exécution des ordres SQL
        entityManagerEdition.flush();
        entityManagerEdition.clear();
    }

    /**
     * {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.edition.dao.JobHistoryDao#findBeanEditionIds(java.lang.String).
     */
    @Override
    public List<String> findBeanEditionIds(String applicationOrigine)
    {
        LOGGER.debug(">>> Debut methode findBeanEditionIds()");
        // Cas du paramétre absent
        Control.assertApplicationNonRenseignee(applicationOrigine);

        // Journalisation de la recherche
        LOGGER.debug(JOURNALISATION_RECHERCHE + " lié à l'application '{}'", applicationOrigine);

        // Paramétrage de la recherche
        CriteriaBuilder criteriaBuilder = entityManagerEdition.getCriteriaBuilder();
        CriteriaQuery<String> query = criteriaBuilder.createQuery(String.class);
        Root<JobHistory> root = query.from(JobHistory.class);
        query.select(root.get(ProprieteJobHistory.IDENTIFIANT_EDITION.getNom()));

        // Paramétrage des critères
        query.where(criteriaBuilder.equal(
            root.get(ProprieteJobHistory.DESTINATION_EDITION.getNom())
                .get(ProprieteJobHistory.APPLICATION_ORIGINE.getNom()),
            applicationOrigine));

        // Regroupement sur l'identifiant édition
        query.groupBy(root.get(ProprieteJobHistory.IDENTIFIANT_EDITION.getNom()));

        // Exécution de la recherche
        return findAll(query);
    }

    /**
     * {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.edition.dao.JobHistoryDao#findEditionsLesPlusLongues(java.lang.String, int).
     */
    @Override
    public List<JobHistory> findEditionsLesPlusLongues(String applicationOrigine, int nbeARetourner)
    {
        LOGGER.debug(">>> Debut methode findEditionsLesPlusLongues()");
        // Cas du paramétre absent
        Control.assertApplicationNonRenseignee(applicationOrigine);

        // Journalisation de la recherche
        LOGGER.debug(JOURNALISATION_RECHERCHE + " ayant les temps d'exécution les plus "
            + "longs, liés à l'application '{}' (max : {})", applicationOrigine, nbeARetourner);

        // Paramétrage de la recherche
        CriteriaBuilder criteriaBuilder = entityManagerEdition.getCriteriaBuilder();
        CriteriaQuery<JobHistory> query = criteriaBuilder.createQuery(JobHistory.class);
        Root<JobHistory> root = query.from(JobHistory.class);

        // Paramétrage des critères
        query.where(criteriaBuilder.equal(
            root.get(ProprieteJobHistory.ORDONNANCEMENT_EDITION.getNom())
                .get(ProprieteJobHistory.STATUT.getNom()),
            JobHistory.STATUS_DISPONIBLE), criteriaBuilder.equal(
                root.get(ProprieteJobHistory.DESTINATION_EDITION.getNom())
                    .get(ProprieteJobHistory.APPLICATION_ORIGINE.getNom()),
                applicationOrigine));

        // Paramétrage du tri
        query.orderBy(criteriaBuilder.desc(root.get(ProprieteJobHistory.ORDONNANCEMENT_EDITION.getNom())
            .get("tempsExecution")));

        // Exécution de la recherche
        return findAll(query, nbeARetourner);
    }

    /**
     * {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.edition.dao.JobHistoryDao#findJobHistory(java.lang.String)
     */
    @Override
    public JobHistory findJobHistory(String editionUuid)
    {
        LOGGER.debug(">>> Debut methode findJobHistory()");
        // Cas du paramétre absent
        Control.assertUuidNonRenseigne(editionUuid);

        // Journalisation de la recherche
        LOGGER.debug(JOURNALISATION_RECHERCHE + " d'UUID '{}'", editionUuid);

        // Préparation des critères
        Map<String, String> criteres = new HashMap<>();
        criteres.put(ProprieteJobHistory.UID_EDITION.getNom(), editionUuid);

        // Recherche des traces de purge
        return findByCriterias(JobHistory.class, criteres, ModeCritereRecherche.EQUAL);
    }

    /**
     * {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.edition.dao.JobHistoryDao#findJobHistoryParProfilStructureAffectationAppli(java.lang.String,
     *      java.lang.String, java.lang.String).
     */
    @Override
    public List<JobHistory> findJobHistoryParProfilStructureAffectationAppli(String profil, String structureAffectation,
        String applicationOrigine)
    {
        LOGGER.debug(">>> Debut methode findJobHistoryParProfilStructureAffectationAppli()");
        throw new UnsupportedOperationException("Fonctionnalité non disponible");
    }

    /**
     * {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.edition.dao.JobHistoryDao#findLaDerniereEditionParBeanIdEtParAppliOrigine(java.lang.String,
     *      java.lang.String).
     */
    @Override
    public JobHistory findLaDerniereEditionParBeanIdEtParAppliOrigine(String beanEditionId, String appliOrigine)
    {
        LOGGER.debug(">>> Debut methode findLaDerniereEditionParBeanIdEtParAppliOrigine()");
        // Cas des paramétres absents
        Control.assertIdNonRenseigne(beanEditionId);
        Control.assertApplicationNonRenseignee(appliOrigine);

        // Journalisation de la recherche
        LOGGER.debug(JOURNALISATION_RECHERCHE + " d'id '{}', lié à l'application '{}'", beanEditionId, appliOrigine);

        // Exécution de la recherche
        return findLaDerniereEdition(beanEditionId, appliOrigine);
    }

    /**
     * {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.edition.dao.JobHistoryDao#findLaDerniereEditionParBeanIdEtParAppliOrigineEtParUtilisateur(java.lang.String,
     *      java.lang.String, java.lang.String).
     */
    @Override
    public JobHistory findLaDerniereEditionParBeanIdEtParAppliOrigineEtParUtilisateur(String beanEditionId, String appliOrigine,
        String utilisateur)
    {
        LOGGER.debug(">>> Debut methode findLaDerniereEditionParBeanIdEtParAppliOrigineEtParUtilisateur()");
        // Cas des paramétres absents
        Control.assertIdNonRenseigne(beanEditionId);
        Control.assertApplicationNonRenseignee(appliOrigine);
        Control.assertUtilisateurNonRenseigne(utilisateur);

        // Journalisation de la recherche
        LOGGER.debug(JOURNALISATION_RECHERCHE + " d'identifiant '{}', par l'utilisateur '{}', "
            + "lié à l'application '{}'", beanEditionId, utilisateur, appliOrigine);

        // Exécution de la recherche
        return findLaDerniereEdition(beanEditionId, appliOrigine, utilisateur);
    }

    /**
     * Méthode permettant de récupérer le dernier historique de travaux d'édition
     *
     * @param beanEditionId identifiant de l'édition
     * @param appliOrigine application liée à l'édition
     * @param utilisateur propriétaire de l'édition
     * @return l'historique de travaux d'édition correspondant
     */
    private JobHistory findLaDerniereEdition(String beanEditionId, String appliOrigine, String utilisateur)
    {
        LOGGER.debug(">>> Debut methode findLaDerniereEdition()");
        // Initialisation du nombre de prédicat par requête et sous-requête
        int nbrePredicat = 3;
        int nbrePredicatSousRequete = 2;
        if (utilisateur != null)
        {
            nbrePredicat++;
            nbrePredicatSousRequete++;
        }

        // Paramétrage de la requête principale
        CriteriaBuilder criteriaBuilder = entityManagerEdition.getCriteriaBuilder();
        CriteriaQuery<JobHistory> query = criteriaBuilder.createQuery(JobHistory.class);
        Root<JobHistory> root = query.from(JobHistory.class);

        // Paramétrage de la sous-requête sur la date la plus récente
        Subquery<Date> maxSubQuery = query.subquery(Date.class);
        Root<JobHistory> rootSousRequete = maxSubQuery.from(JobHistory.class);
        maxSubQuery.select(criteriaBuilder.greatest(
            rootSousRequete.<OrdoEdition> get(ProprieteJobHistory.ORDONNANCEMENT_EDITION.getNom())
                .<Date> get(ProprieteJobHistory.DATE_DEMANDE.getNom())));

        // Initialisation des critères de la sous-requête
        Predicate[] predicatesSousRequete = new Predicate[nbrePredicatSousRequete];
        predicatesSousRequete[0] =
            criteriaBuilder.equal(rootSousRequete.get(ProprieteJobHistory.IDENTIFIANT_EDITION.getNom()), beanEditionId);
        predicatesSousRequete[1] = criteriaBuilder.equal(rootSousRequete.get(ProprieteJobHistory.DESTINATION_EDITION.getNom())
            .get(ProprieteJobHistory.APPLICATION_ORIGINE.getNom()), appliOrigine);
        if (utilisateur != null)
        {
            predicatesSousRequete[2] = criteriaBuilder.equal(rootSousRequete.get(ProprieteJobHistory.DESTINATION_EDITION.getNom())
                .get(ProprieteJobHistory.UID_PROPRIETAIRE.getNom()), utilisateur);
        }
        maxSubQuery.where(predicatesSousRequete);

        // Initialisation des critères de la requête principale
        Predicate[] predicates = new Predicate[nbrePredicat];
        predicates[0] = criteriaBuilder.equal(root.get(ProprieteJobHistory.IDENTIFIANT_EDITION.getNom()), beanEditionId);
        predicates[1] = criteriaBuilder.equal(root.get(ProprieteJobHistory.DESTINATION_EDITION.getNom())
            .get(ProprieteJobHistory.APPLICATION_ORIGINE.getNom()), appliOrigine);
        predicates[2] = criteriaBuilder.equal(root.get(ProprieteJobHistory.ORDONNANCEMENT_EDITION.getNom())
            .get(ProprieteJobHistory.DATE_DEMANDE.getNom()), maxSubQuery);
        if (utilisateur != null)
        {
            predicates[3] = criteriaBuilder.equal(root.get(ProprieteJobHistory.DESTINATION_EDITION.getNom())
                .get(ProprieteJobHistory.UID_PROPRIETAIRE.getNom()), utilisateur);
        }
        query.where(predicates);

        // Exécution de la recherche
        List<JobHistory> listeJobHistory = findAll(query);

        if (!listeJobHistory.isEmpty())
        {
            // Récupération du 1er des historiques existent pour la même date, le même identifiant,
            // voire le même propriétaire (même si c'est hautement improbable)
            return listeJobHistory.get(0);
        }

        return null;
    }

    /**
     * Méthode permettant de récupérer le dernier historique de travaux d'édition
     *
     * @param beanEditionId identifiant de l'édition
     * @param appliOrigine application liée à l'édition
     * @return l'historique de travaux d'édition correspondant
     */
    private JobHistory findLaDerniereEdition(String beanEditionId, String appliOrigine)
    {
        LOGGER.debug(">>> Debut methode findLaDerniereEdition()");
        return findLaDerniereEdition(beanEditionId, appliOrigine, null);
    }

    /**
     * {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.edition.dao.JobHistoryDao#findToutesEditionsParProfilFiltreAppli(java.util.Map,
     *      java.lang.String).
     */
    @Override
    public List<JobHistory> findToutesEditionsParProfilFiltreAppli(Map<String, Set<FiltreEdition>> mapFiltresEdition,
        String applicationOrigine)
    {
        LOGGER.debug(">>> Debut methode findToutesEditionsParProfilFiltreAppli()");
        return findToutesEditions(null, mapFiltresEdition, applicationOrigine);
    }

    /**
     * {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.edition.dao.JobHistoryDao#findNouvellesEditionsParProfilFiltreAppli(java.lang.String,
     *      java.util.Map, java.lang.String).
     */
    @Override
    public List<JobHistory> findNouvellesEditionsParProfilFiltreAppli(String uid, Map<String, Set<FiltreEdition>> mapFiltresEdition,
        String applicationOrigine)
    {
        LOGGER.debug(">>> Debut methode findNouvellesEditionsParProfilFiltreAppli()");
        // Cas du paramétre UID absent
        Control.assertUtilisateurNonRenseigne(uid);

        // Exécution de la recherche
        return findToutesEditions(uid, mapFiltresEdition, applicationOrigine);
    }

    /**
     * Méthode permettant de récupérer les éditions satisfaisant les critères.
     *
     * @param uid utilisateur lié aux éditions à rechercher
     * @param mapFiltresEdition Map des fitres d'édition à appliquer à la recherche
     * @param applicationOrigine application d'origine sur laquelle porte les éditions
     * @return la liste d'édition satisfaisant les critères
     */
    private List<JobHistory> findToutesEditions(String uid, Map<String, Set<FiltreEdition>> mapFiltresEdition, String applicationOrigine)
    {
        LOGGER.debug(">>> Debut methode findToutesEditions()");
        // Contrôle des paramétres
        Control.assertApplicationNonRenseignee(applicationOrigine);
        if (mapFiltresEdition == null)
        {
            throw new ProgrammationException("Les filtres de profil doivent être renseignés");
        }

        // Journalisation de la recherche
        LOGGER.debug(JOURNALISATION_RECHERCHE + " liés à l'application '{}' pour un certain "
            + "filtre de profil, non consultés par l'utilisateur '{}'", applicationOrigine, uid);

        // Passage en JPQL car bug JPA sur deux joins sur un embeddable (Cf. findProfils)
        StringBuilder requete = new StringBuilder(500);
        requete.append("SELECT distinct j FROM JobHistory j")
            .append(" inner join j.destinationEdition dest")
            .append(" inner join dest.profilsDesti p")
            .append(" left outer join p.listeFiltres f")
            .append(" left outer join f.valsFiltre v");

        if (uid != null)
        {
            requete.append(" left outer join dest.uidConsult u");
        }

        requete.append(" WHERE dest.applicationOrigine = :application AND dest.monoDestinataire = :monoDestinataire");

        Map<String, Object> param = new HashMap<>();

        AtomicInteger indexProfil = new AtomicInteger();
        AtomicInteger indexFiltre = new AtomicInteger();
        mapFiltresEdition.keySet().stream().forEach(key -> {
            final String variableProfil = "profil" + indexProfil.incrementAndGet();

            if (indexProfil.get() == 1)
            {
                requete.append(" AND (");
            }
            else
            {
                requete.append(" OR ");
            }

            requete.append(" (p.nomProfil = :" + variableProfil);
            param.put(variableProfil, key);

            // CF
            requete.append(" ) ");

            Set<FiltreEdition> filtresEditions = mapFiltresEdition.get(key);
            if (filtresEditions == null)
            {
                throw new ProgrammationException("Le filtre du profil '" + key + "' n'est pas renseigné");
            }

            filtresEditions.stream().forEach(filtre -> {
                if (StringUtils.isNotBlank(filtre.getNomDuFiltre()))
                {
                    final String variableNomFiltre = "nomFiltre" + indexFiltre.incrementAndGet();
                    final String variableValeurFiltre = "valeurFiltre" + indexFiltre.get();
                    requete.append(" AND f.nomDuFiltre = :").append(variableNomFiltre);
                    param.put(variableNomFiltre, filtre.getNomDuFiltre());
                    requete.append(" AND v.value in :").append(variableValeurFiltre);
                    param.put(variableValeurFiltre, filtre.getValsFiltreString());
                }
            });

            requete.append(" ) ");
        });

        // CF
        // requete.append(" ) ");

        if (uid != null)
        {
            requete.append(" AND (u.uidUtilisateur != :uid OR u.uidUtilisateur IS NULL)");
            param.put("uid", uid);
        }

        // Paramétrage de l'ordre
        requete.append(" ORDER BY j.ordoEdition.dateDemande DESC");

        // Initialisation de la recherche
        TypedQuery<JobHistory> query = entityManagerEdition.createQuery(requete.toString(), JobHistory.class)
            .setMaxResults(NombreMaxEdition.MAX_LIGNE_PAR_PROFIL.getNbreMax())
            .setParameter("application", applicationOrigine)
            .setParameter("monoDestinataire", Boolean.FALSE);
        param.entrySet().stream().forEach(entry -> {
            query.setParameter(entry.getKey(), entry.getValue());
        });

        // Exécution de la recherche
        return query.getResultList();
    }

    /**
     * {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.edition.dao.JobHistoryDao#findNouvellesEditionsParUidEtAppli(java.lang.String,
     *      java.lang.String).
     */
    @Override
    public List<JobHistory> findNouvellesEditionsParUidEtAppli(String uidProprietaire, String applicationOrigine)
    {
        LOGGER.debug(">>> Debut methode findNouvellesEditionsParUidEtAppli()");
        // Cas des paramétres absents
        Control.assertApplicationNonRenseignee(applicationOrigine);
        Control.assertUtilisateurNonRenseigne(uidProprietaire);

        // Journalisation de la recherche
        LOGGER.debug(JOURNALISATION_RECHERCHE + " du propriétaire '{}', liés à l'application '{}'", uidProprietaire, applicationOrigine);

        // Paramétrage de la recherche
        CriteriaBuilder criteriaBuilder = entityManagerEdition.getCriteriaBuilder();
        CriteriaQuery<JobHistory> query = criteriaBuilder.createQuery(JobHistory.class);
        Root<JobHistory> root = query.from(JobHistory.class);

        // Paramétrage des critères
        query.where(criteriaBuilder.equal(
            root.get(ProprieteJobHistory.DESTINATION_EDITION.getNom())
                .get(ProprieteJobHistory.UID_PROPRIETAIRE.getNom()),
            uidProprietaire), criteriaBuilder.equal(
                root.get(ProprieteJobHistory.DESTINATION_EDITION.getNom())
                    .get(ProprieteJobHistory.APPLICATION_ORIGINE.getNom()),
                applicationOrigine),
            criteriaBuilder.equal(
                root.get(ProprieteJobHistory.DESTINATION_EDITION.getNom())
                    .get(ProprieteJobHistory.MONO_DESTINATAIRE.getNom()),
                Boolean.TRUE),
            criteriaBuilder.equal(
                root.get(ProprieteJobHistory.DESTINATION_EDITION.getNom())
                    .get(ProprieteJobHistory.NOUVELLE_EDITION.getNom()),
                Boolean.TRUE));

        // Paramétrage du tri
        query.orderBy(criteriaBuilder.desc(root.get(ProprieteJobHistory.ORDONNANCEMENT_EDITION.getNom())
            .get(ProprieteJobHistory.DATE_DEMANDE.getNom())));

        // Exécution de la recherche
        return findAll(query, NombreMaxEdition.MAX_LIGNE_PAR_UID.getNbreMax());
    }

    /**
     * {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.edition.dao.JobHistoryDao#findProfils(java.lang.String)
     */
    @Override
    public List<ProfilDesti> findProfils(String applicationOrigine)
    {
        LOGGER.debug(">>> Debut methode findProfils()");
        // Cas du paramétre absent
        Control.assertApplicationNonRenseignee(applicationOrigine);

        // Journalisation de la recherche
        LOGGER.debug("Recherche des profils destinataires liés aux historiques des travaux d'édition, "
            + "liés à l'application '{}'", applicationOrigine);

        // Passage en JPQL car bug JPA sur deux joins sur un embeddable
        // CriteriaBuilder criteriaBuilder = entityManagerEdition.getCriteriaBuilder();
        // CriteriaQuery<ProfilDesti> queryProfil = criteriaBuilder.createQuery(ProfilDesti.class);
        // Root<ProfilDesti> from = queryProfil.from(ProfilDesti.class);
        // Subquery<JobHistory> subQuery = queryProfil.subquery(JobHistory.class);
        // Root<JobHistory> fromJobHistory = subQuery.from(JobHistory.class);
        // Join<DestinationEdition, ProfilDesti> join
        // = fromJobHistory.join("destinationEdition").join("profilsDesti");
        // subQuery.select(join.get("id"));
        // queryProfil.select(from).where(criteriaBuilder.equal(
        // from.get(ProprieteJobHistory.DESTINATION_EDITION.getNom())
        // .get(ProprieteJobHistory.APPLICATION_ORIGINE.getNom()), applicationOrigine));
        // return entityManagerEdition.createQuery(queryProfil).getResultList();

        // Initialisation de la requête
        StringBuilder requete = new StringBuilder(100);
        requete.append("SELECT distinct new fr.gouv.finances.lombok.edition.bean.ProfilDesti(p.nomProfil)")
            .append(" FROM JobHistory j")
            .append(" inner join j.destinationEdition dest")
            .append(" inner join dest.profilsDesti p")
            .append(" WHERE dest.applicationOrigine = :application");

        // Initialisation de la recherche
        TypedQuery<ProfilDesti> typedQuery = entityManagerEdition
            .createQuery(requete.toString(), ProfilDesti.class)
            .setParameter("application", applicationOrigine);

        // Exécution de la recherche
        return typedQuery.getResultList();
    }

    /**
     * {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.edition.dao.JobHistoryDao#findToutesEditionsParBeanEditionId(java.lang.String,
     *      java.lang.String).
     */
    @Override
    public List<JobHistory> findToutesEditionsParBeanEditionId(String beanEditionId, String applicationOrigine)
    {
        LOGGER.debug(">>> Debut methode findToutesEditionsParBeanEditionId()");
        // Cas des paramétres absents
        Control.assertIdNonRenseigne(beanEditionId);
        Control.assertApplicationNonRenseignee(applicationOrigine);

        // Journalisation de la recherche
        LOGGER.debug(JOURNALISATION_RECHERCHE + " d'identifiant '{}' liés à l'application '{}'", beanEditionId, applicationOrigine);

        // Paramétrage de la recherche
        CriteriaBuilder criteriaBuilder = entityManagerEdition.getCriteriaBuilder();
        CriteriaQuery<JobHistory> query = criteriaBuilder.createQuery(JobHistory.class);
        Root<JobHistory> root = query.from(JobHistory.class);

        // Paramétrage des critères
        query.where(criteriaBuilder.equal(
            root.get(ProprieteJobHistory.IDENTIFIANT_EDITION.getNom()), beanEditionId), criteriaBuilder.equal(
                root.get(ProprieteJobHistory.DESTINATION_EDITION.getNom())
                    .get(ProprieteJobHistory.APPLICATION_ORIGINE.getNom()),
                applicationOrigine));

        // Paramétrage du tri
        query.orderBy(criteriaBuilder.desc(root.get(ProprieteJobHistory.ORDONNANCEMENT_EDITION.getNom())
            .get(ProprieteJobHistory.DATE_DEMANDE.getNom())));

        // Exécution de la recherche
        return findAll(query, NombreMaxEdition.MAX_LIGNE_PAR_UID.getNbreMax());
    }

    /**
     * {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.edition.dao.JobHistoryDao#findToutesEditionsParDateDemande(java.util.Date,
     *      java.lang.String).
     */
    @Override
    public List<JobHistory> findToutesEditionsParDateDemande(Date dateDemande, String applicationOrigine)
    {
        LOGGER.debug(">>> Debut methode findToutesEditionsParDateDemande()");
        // Cas des paramètres non renseignés
        Control.assertApplicationNonRenseignee(applicationOrigine);
        if (dateDemande == null)
        {
            throw new ProgrammationException("La date de demande doit être renseignée");
        }

        // Journalisation de la recherche
        LOGGER.debug(JOURNALISATION_RECHERCHE + "liés à l'application '{}' pour la date '{}'", applicationOrigine, dateDemande);

        // Détermination de la borne fin de la date de demande
        Calendar cal = Calendar.getInstance(Locale.FRANCE);
        cal.setTime(dateDemande);
        cal.add(Calendar.DATE, 1);
        Date dateDemandePlusUn = cal.getTime();

        // Paramétrage de la recherche
        CriteriaBuilder criteriaBuilder = entityManagerEdition.getCriteriaBuilder();
        CriteriaQuery<JobHistory> query = criteriaBuilder.createQuery(JobHistory.class);
        Root<JobHistory> root = query.from(JobHistory.class);

        // Paramétrage des critères
        query.where(criteriaBuilder.equal(
            root.get(ProprieteJobHistory.DESTINATION_EDITION.getNom())
                .get(ProprieteJobHistory.APPLICATION_ORIGINE.getNom()),
            applicationOrigine), criteriaBuilder.greaterThanOrEqualTo(
                root.get(ProprieteJobHistory.ORDONNANCEMENT_EDITION.getNom())
                    .<Date> get(ProprieteJobHistory.DATE_DEMANDE.getNom()),
                dateDemande),
            criteriaBuilder.lessThan(
                root.get(ProprieteJobHistory.ORDONNANCEMENT_EDITION.getNom())
                    .<Date> get(ProprieteJobHistory.DATE_DEMANDE.getNom()),
                dateDemandePlusUn));

        // Paramétrage du tri
        query.orderBy(criteriaBuilder.desc(root.get(ProprieteJobHistory.ORDONNANCEMENT_EDITION.getNom())
            .get(ProprieteJobHistory.DATE_DEMANDE.getNom())));

        // Exécution de la recherche
        return findAll(query, NombreMaxEdition.MAX_LIGNE_PAR_UID.getNbreMax());
    }

    /**
     * {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.edition.dao.JobHistoryDao#findToutesEditionsParEtat(java.lang.String,
     *      java.lang.String).
     */
    @Override
    public List<JobHistory> findToutesEditionsParEtat(String etat, String applicationOrigine)
    {
        LOGGER.debug(">>> Debut methode findToutesEditionsParEtat()");
        // Cas des paramètres non renseignés
        Control.assertApplicationNonRenseignee(applicationOrigine);
        if (StringUtils.isEmpty(etat))
        {
            throw new ProgrammationException("L'état doit être renseignée");
        }

        // Journalisation de la recherche
        LOGGER.debug(JOURNALISATION_RECHERCHE + " liés à l'application '{}' pour l'état '{}'", applicationOrigine, etat);

        // Paramétrage de la recherche
        CriteriaBuilder criteriaBuilder = entityManagerEdition.getCriteriaBuilder();
        CriteriaQuery<JobHistory> query = criteriaBuilder.createQuery(JobHistory.class);
        Root<JobHistory> root = query.from(JobHistory.class);

        // Paramétrage des critères
        query.where(criteriaBuilder.equal(
            root.get(ProprieteJobHistory.ORDONNANCEMENT_EDITION.getNom())
                .get(ProprieteJobHistory.STATUT.getNom()),
            etat), criteriaBuilder.equal(
                root.get(ProprieteJobHistory.DESTINATION_EDITION.getNom())
                    .get(ProprieteJobHistory.APPLICATION_ORIGINE.getNom()),
                applicationOrigine));

        // Paramétrage du tri
        query.orderBy(criteriaBuilder.desc(root.get(ProprieteJobHistory.ORDONNANCEMENT_EDITION.getNom())
            .get(ProprieteJobHistory.DATE_DEMANDE.getNom())));

        // Exécution de la recherche
        return findAll(query, NombreMaxEdition.MAX_LIGNE_PAR_UID.getNbreMax());
    }

    /**
     * {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.edition.dao.JobHistoryDao#findToutesEditionsParPeriodeDateDemande(java.util.Date,
     *      java.util.Date, java.lang.String).
     */
    @Override
    public List<JobHistory> findToutesEditionsParPeriodeDateDemande(Date debutPeriodeDemande, Date finPeriodeDemande,
        String applicationOrigine)
    {
        LOGGER.debug(">>> Debut methode findToutesEditionsParPeriodeDateDemande()");
        // Cas des paramètres non renseignés
        Control.assertApplicationNonRenseignee(applicationOrigine);
        if (debutPeriodeDemande == null || finPeriodeDemande == null)
        {
            throw new ProgrammationException("La date de début et fin de période doit être renseignée");
        }

        // Journalisation de la recherche
        LOGGER.debug(JOURNALISATION_RECHERCHE + "liés à l'application '{}' pour période '{}' -> '{}'", applicationOrigine,
            debutPeriodeDemande, finPeriodeDemande);

        // Paramétrage de la recherche
        CriteriaBuilder criteriaBuilder = entityManagerEdition.getCriteriaBuilder();
        CriteriaQuery<JobHistory> query = criteriaBuilder.createQuery(JobHistory.class);
        Root<JobHistory> root = query.from(JobHistory.class);

        // Paramétrage des critères
        query.where(criteriaBuilder.greaterThanOrEqualTo(
            root.get(ProprieteJobHistory.ORDONNANCEMENT_EDITION.getNom())
                .get(ProprieteJobHistory.DATE_DEMANDE.getNom()),
            debutPeriodeDemande), criteriaBuilder.lessThanOrEqualTo(
                root.get(ProprieteJobHistory.ORDONNANCEMENT_EDITION.getNom())
                    .get(ProprieteJobHistory.DATE_DEMANDE.getNom()),
                finPeriodeDemande),
            criteriaBuilder.equal(
                root.get(ProprieteJobHistory.DESTINATION_EDITION.getNom())
                    .get(ProprieteJobHistory.APPLICATION_ORIGINE.getNom()),
                applicationOrigine));

        // Paramétrage du tri
        query.orderBy(criteriaBuilder.desc(root.get(ProprieteJobHistory.ORDONNANCEMENT_EDITION.getNom())
            .get(ProprieteJobHistory.DATE_DEMANDE.getNom())));

        // Exécution de la recherche
        return findAll(query, NombreMaxEdition.MAX_LIGNE_PAR_UID.getNbreMax());
    }

    /**
     * {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.edition.dao.JobHistoryDao#findToutesEditionsParUidEtAppli(java.lang.String,
     *      java.lang.String).
     */
    @Override
    public List<JobHistory> findToutesEditionsParUidEtAppli(String uidProprietaire, String applicationOrigine)
    {
        LOGGER.debug(">>> Debut methode findToutesEditionsParUidEtAppli()");
        // Contrôle des paramétres
        Control.assertApplicationNonRenseignee(applicationOrigine);
        Control.assertUtilisateurNonRenseigne(uidProprietaire);

        // Journalisation de la recherche
        LOGGER.debug(JOURNALISATION_RECHERCHE + " liés à l'application '{}' pour l'utilisateur '{}' ", applicationOrigine, uidProprietaire);

        // Paramétrage de la recherche
        CriteriaBuilder criteriaBuilder = entityManagerEdition.getCriteriaBuilder();
        CriteriaQuery<JobHistory> query = criteriaBuilder.createQuery(JobHistory.class);
        Root<JobHistory> root = query.from(JobHistory.class);

        // Paramétrage des critères
        query.where(criteriaBuilder.equal(
            root.get(ProprieteJobHistory.DESTINATION_EDITION.getNom())
                .get(ProprieteJobHistory.UID_PROPRIETAIRE.getNom()),
            uidProprietaire), criteriaBuilder.equal(
                root.get(ProprieteJobHistory.DESTINATION_EDITION.getNom())
                    .get(ProprieteJobHistory.APPLICATION_ORIGINE.getNom()),
                applicationOrigine),
            criteriaBuilder.equal(
                root.get(ProprieteJobHistory.DESTINATION_EDITION.getNom())
                    .get(ProprieteJobHistory.MONO_DESTINATAIRE.getNom()),
                Boolean.TRUE));

        // Paramétrage du tri
        query.orderBy(criteriaBuilder.desc(root.get(ProprieteJobHistory.ORDONNANCEMENT_EDITION.getNom())
            .get(ProprieteJobHistory.DATE_DEMANDE.getNom())));

        // Exécution de la recherche
        return findAll(query, NombreMaxEdition.MAX_LIGNE_PAR_UID.getNbreMax());
    }

    /**
     * {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.edition.dao.JobHistoryDao#isValidEdition(fr.gouv.finances.lombok.edition.bean.JobHistory).
     */
    @Override
    public boolean isValidEdition(JobHistory jobHistory)
    {
        LOGGER.debug(">>> Debut methode isValidEdition()");
        Control.assertJobHistoryNotNull(jobHistory);
        Control.assertUuidNonRenseigne(jobHistory.getEditionUuid());

        // Journalisation de la recherche
        LOGGER.debug("Détermination si un historique de travaux d'édition, d'identifiant '{}', existe ou non", jobHistory.getEditionUuid());

        // Préparation des critères
        Map<String, String> criteres = new HashMap<>();
        criteres.put(ProprieteJobHistory.UID_EDITION.getNom(), jobHistory.getEditionUuid());

        // Recherche des traces de purge
        return countTotalNumberOfLines(JobHistory.class, criteres, ModeCritereRecherche.EQUAL) > 0;
    }

    /**
     * {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.edition.dao.JobHistoryDao#saveContenuEditionFromByteArray(fr.gouv.finances.lombok.edition.bean.JobHistory,
     *      byte[]).
     */
    @Override
    public void saveContenuEditionFromByteArray(JobHistory jobHistory, byte[] dataBytes)
    {
        LOGGER.debug(">>> Debut methode saveContenuEditionFromByteArray()");
        // Contrôles des paramétres
        Control.assertJobHistoryNotNull(jobHistory);
        Control.assertJobHistoryCharge(jobHistory);
        Control.assertdatabytesNotNull(dataBytes);

        // Journalisation de la mise à jour
        LOGGER.debug("Sauvegarde du contenu au sein de l'historique de travaux d'édition d'identifiant '{}'", jobHistory.getEditionUuid());

        // Mise à jour des éventuelles modifications
        modifyObject(jobHistory);

        // Chargement du contenu de l'édition, avec initialisation en cas d'absence
        loadSafeData(jobHistory);

        // Association du nouveau contenu de l'édition avec l'historique
        jobHistory.getStockageEdition().getLeContenuEdition().setData(dataBytes);

        // Sauvegarde en base
        if (jobHistory.getStockageEdition().getLeContenuEdition().getId() != null)
        {
            modifyObject(jobHistory.getStockageEdition().getLeContenuEdition());
        }
        else
        {
            saveObject(jobHistory.getStockageEdition().getLeContenuEdition());
        }

        modifyObject(jobHistory);
    }

    /**
     * {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.edition.dao.JobHistoryDao#saveContenuEditionFromFile(fr.gouv.finances.lombok.edition.bean.JobHistory,
     *      java.io.File).
     */
    @Override
    public void saveContenuEditionFromFile(JobHistory jobHistory, File fichierTemporaire)
    {
        LOGGER.debug(">>> Debut methode saveContenuEditionFromFile()");
        Control.assertJobHistoryNotNull(jobHistory);
        Control.assertJobHistoryCharge(jobHistory);
        Control.assertFichierAccessibleEnLecture(fichierTemporaire);
        if (fichierTemporaire.length() == 0)
        {
            throw new ProgrammationException("Le fichier '" + fichierTemporaire.getName() + "' est vide");
        }

        // Journalisation de la mise à jour
        LOGGER.debug("Sauvegarde du contenu au sein de l'historique de travaux d'édition d'identifiant '{}'", jobHistory.getEditionUuid());

        // Mise à jour des éventuelles modifications
        modifyObject(jobHistory);
        // Chargement du contenu de l'édition, avec initialisation en cas d'absence
        loadSafeData(jobHistory);

        // Association du nouveau contenu de l'édition avec l'historique
        jobHistory.getStockageEdition().getLeContenuEdition().setData(
            FileUtils.getFichier(fichierTemporaire));

        // Sauvegarde en base
        if (jobHistory.getStockageEdition().getLeContenuEdition().getId() != null)
        {
            modifyObject(jobHistory.getStockageEdition().getLeContenuEdition());
        }
        else
        {
            saveObject(jobHistory.getStockageEdition().getLeContenuEdition());
        }
        modifyObject(jobHistory);
    }

    /**
     * {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.edition.dao.JobHistoryDao#saveJobHistory(fr.gouv.finances.lombok.edition.bean.JobHistory).
     */
    @Override
    public void saveJobHistory(JobHistory jobHistory)
    {
        // Journalisation de la sauvegarde
        LOGGER.trace("Sauvegarde d'un historique des travaux d'édition");

        // Création du JobHistory (et associations associées)
        if (jobHistory.getId() == null)
        {
            saveObject(jobHistory);
        }
        else
        {
            // OK en mode WEB
            // KO en mode batch
            // modifyObject(jobHistory);

            // CF: rustine provisoire pour éviter l'erreur suivante :
            // "Caused by: org.hibernate.StaleObjectStateException:
            // Row was updated or deleted by another transaction (or unsaved-value mapping was incorrect) :
            // [fr.gouv.finances.lombok.edition.bean.ProfilDesti#750]"
            JobHistory jobHistoryTrouve = entityManagerEdition.find(JobHistory.class, jobHistory.getId());

            Set<ProfilDesti> setProfilDesti = jobHistory.getDestinationEdition().getProfilsDesti();
            Set<ProfilDesti> setProfilDestiTrouves = jobHistoryTrouve.getDestinationEdition().getProfilsDesti();
            Set<ProfilDesti> setProfilDestiModifies = new HashSet<>();

            if (null != setProfilDesti && null != setProfilDestiTrouves)
            {
                Iterator<ProfilDesti> itrSetProfilDesti = setProfilDesti.iterator();
                Iterator<ProfilDesti> itrSetProfilDestiTrouves = setProfilDestiTrouves.iterator();

                while (itrSetProfilDesti.hasNext() && itrSetProfilDestiTrouves.hasNext())
                {
                    ProfilDesti profilDesti = itrSetProfilDesti.next();
                    ProfilDesti profilDestiTrouve = itrSetProfilDestiTrouves.next();

                    if (profilDesti.getVersion() < profilDestiTrouve.getVersion())
                    {
                        profilDesti.setVersion(profilDestiTrouve.getVersion());
                    }
                    setProfilDestiModifies.add(profilDesti);
                }

                jobHistory.getDestinationEdition().setProfilsDesti(setProfilDestiModifies);
            }

            modifyObject(jobHistory);
        }
    }

    /**
     * {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.edition.dao.JobHistoryDao#findAndLoadJobHistory(java.lang.String).
     */
    @Override
    public JobHistory findAndLoadJobHistory(String editionUuid)
    {
        LOGGER.debug(">>> Debut methode findAndLoadJobHistory()");
        // Cas du paramétre absent
        Control.assertUuidNonRenseigne(editionUuid);

        // Journalisation de la recherche
        LOGGER.debug("Chargement de l'historique de travaux d'édition d'identifiant '{}'", editionUuid);

        // Préparation des critères
        Map<String, String> criteres = new HashMap<>();
        criteres.put(ProprieteJobHistory.UID_EDITION.getNom(), editionUuid);

        Map<String, JoinType> jointure = new HashMap<>();
        jointure.put(ProprieteJobHistory.CONTENU_EDITION.getNom(), JoinType.INNER);
        jointure.put(ProprieteJobHistory.PROFILS_DESTINATAIRES.getNom(), JoinType.INNER);
        jointure.put(ProprieteJobHistory.LISTE_FILTRE.getNom(), JoinType.INNER);

        // Recherche des traces de purge
        return findByCriterias(JobHistory.class, criteres, ModeCritereRecherche.EQUAL);
    }

    /**
     * {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.edition.dao.JobHistoryDao#chargerContenuEditionToBytes(fr.gouv.finances.lombok.edition.bean.JobHistory).
     */
    @Override
    public JobHistory chargerContenuEditionToBytes(JobHistory jobHistory)
    {
        LOGGER.debug(">>> Debut methode chargerContenuEditionToBytes()");
        // L'historique des travaux d'édition doit avoir été fourni
        Control.assertJobHistoryNotNull(jobHistory);

        // L'historique des travaux d'édition doit avoir été chargé
        Control.assertJobHistoryCharge(jobHistory);

        // L'historique des travaux d'édition doit être au statut disponible
        Control.assertEditionDisponible(jobHistory);

        // Journalisation du chargement
        LOGGER.debug("Chargement du contenu de l'historique des travaux d'édition (UUID #{})", jobHistory.getEditionUuid());

        // Exécution du chargement du contenu de l'édition
        jobHistory = chargerContenuEdition(jobHistory);

        // Récupération du contenu sous forme de tableau d'octet et alimentation de l'historique
        jobHistory.getStockageEdition().getLeContenuEdition().setDataByte(
            jobHistory.getStockageEdition().getLeContenuEdition().getData());

        return jobHistory;
    }

    /**
     * {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.edition.dao.JobHistoryDao#chargerContenuEditionToFile(fr.gouv.finances.lombok.edition.bean.JobHistory).
     */
    @Override
    public void chargerContenuEditionToFile(JobHistory jobHistory)
    {
        LOGGER.debug(">>> Debut methode chargerContenuEditionToFile()");
        // L'historique des travaux d'édition doit avoir été fourni
        Control.assertJobHistoryNotNull(jobHistory);

        // L'historique des travaux d'édition doit avoir été chargé
        Control.assertJobHistoryCharge(jobHistory);

        // L'historique des travaux d'édition doit être au statut disponible
        Control.assertEditionDisponible(jobHistory);

        // Journalisation du chargement
        LOGGER.debug("Chargement du contenu de l'historique des travaux d'édition (UUID #{})", jobHistory.getEditionUuid());

        // Exécution du chargement du contenu de l'édition
        jobHistory = chargerContenuEdition(jobHistory);

        // Récupération du contenu sous forme de fichier et alimentation de l'historique
        jobHistory.getStockageEdition().getLeContenuEdition().setDataFile(
            FileUtils.getFichier(jobHistory.getStockageEdition().getLeContenuEdition().getData()));
    }

    /**
     * {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.edition.dao.JobHistoryDao#chargerContenuEdition(fr.gouv.finances.lombok.edition.bean.JobHistory).
     */
    @Override
    public JobHistory chargerContenuEdition(JobHistory jobHistory)
    {
        // Journalisation du chargement
        LOGGER.trace("Chargement du contenu de l'historique des travaux d'édition");

        // L'historique des travaux d'édition doit avoir été fourni
        Control.assertJobHistoryNotNull(jobHistory);

        // L'historique des travaux d'édition doit avoir été chargé
        Control.assertJobHistoryCharge(jobHistory);

        // Des informations de stockage doivent être présentes
        if (jobHistory.getStockageEdition() == null)
        {
            throw new ProgrammationException("Pas d'information de stockage liée à "
                + "l'historique d'édition fournie (UUID #" + jobHistory.getEditionUuid() + ")");
        }

        // Récupération du contenu
        ContenuEdition contenu = jobHistory.getStockageEdition().getLeContenuEdition();
        // entityManagerEdition.lock(jobHistory, LockModeType.READ);

        if (!isCharged(contenu))
        {
            jobHistory = entityManagerEdition.merge(jobHistory);
            contenu = jobHistory.getStockageEdition().getLeContenuEdition();
        }

        if (contenu == null || contenu.getData() == null)
        {
            throw new ProgrammationException("Le contenu de l'historique d'édition fournie "
                + "(UUID #" + jobHistory.getEditionUuid() + ") est vide");
        }

        return jobHistory;
    }
}