package fr.gouv.finances.lombok.autoconfig;

import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

/**
 * Classe de configuration qui permet de conditionner les ressources à importer
 * 
 * @author celfer Date: 20 févr. 2020
 */
@Configuration
//@ConditionalOnProperty(name = "lombok.orm.jpa", havingValue = "true")
@ConditionalOnExpression("'${lombok.orm.jpa}' == 'true' && '${lombok.composant.edition.inclus}' == 'true'")
@ImportResource({"classpath:conf/applicationContext-edition-dao.xml"})
public class EditionsJpaApplicationConfig
{

}
