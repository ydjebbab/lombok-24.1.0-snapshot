/*
 * Copyright (c) 2020 DGFiP - Tous droits réservés
 * 
 */
package fr.gouv.finances.lombok.adresse.jpa.dao.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.data.jpa.JpaRepositoriesAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import fr.gouv.finances.lombok.adresse.bean.Adresse;
import fr.gouv.finances.lombok.adresse.bean.CodePostal;
import fr.gouv.finances.lombok.adresse.bean.Commune;
import fr.gouv.finances.lombok.adresse.bean.Departement;
import fr.gouv.finances.lombok.adresse.bean.PaysInsee;
import fr.gouv.finances.lombok.adresse.bean.PaysIso;
import fr.gouv.finances.lombok.adresse.bean.Region;
import fr.gouv.finances.lombok.adresse.config.PersistenceJPAConfigTest;
import fr.gouv.finances.lombok.adresse.dao.AdresseDao;
import fr.gouv.finances.lombok.test.controle.ControleDonnees;
import fr.gouv.finances.lombok.test.controle.VerificationExecution;
import fr.gouv.finances.lombok.util.exception.ProgrammationException;

/**
 * Tests de l'implémentation JPA du DAO adresse.
 * 
 * @author Christophe Breheret-Girardin
 * @author CF
 */
@ComponentScan(basePackages = {"fr.gouv.finances.lombok.jpa.dao", "fr.gouv.finances.lombok.adresse.jpa.dao.impl"})
@EnableAutoConfiguration(exclude = JpaRepositoriesAutoConfiguration.class)
@SpringBootTest(classes = {PersistenceJPAConfigTest.class}, properties = {"lombok.composant.adresse.inclus=true", "lombok.orm.jpa=true"})
// @ActiveProfiles(profiles = {"adresse", "jpa", "embedded"})
@ActiveProfiles(profiles = {"embedded"})
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class AdresseDaoImplTest
{
    public AdresseDaoImplTest()
    {
        super();
    }

    protected static final Logger LOGGER = LoggerFactory.getLogger(AdresseDaoImplTest.class);

    /** Déclaration du DAO gérant les données liées aux adresses. */
    @Autowired
    private AdresseDao adressedao;

    /** Code de département */
    private static final String VALEUR_INCONNUE = "kamoulox";

    /** Map contenant les codes postaux présents en base */
    protected Map<Integer, CodePostal> mapCodePostal = new HashMap<>();

    /** Map contenant les départements présents en base */
    protected Map<Integer, Departement> mapDepartement = new HashMap<>();

    /** Map contenant les régions présentes en base */
    protected Map<Integer, Region> mapRegion = new HashMap<>();

    /** Map contenant les communes présentes en base */
    protected Map<Integer, Commune> mapCommune = new HashMap<>();

    /** Map contenant les pays INSEE présents en base */
    protected Map<Integer, PaysInsee> mapPaysInsee = new HashMap<>();

    /** Map contenant les pays ISO présents en base */
    protected Map<Integer, PaysIso> mapPaysIso = new HashMap<>();

    /**
     * Vérification de la base avant les tests.
     */
    @Before
    public void verifierBdd()
    {
        // Lecture des éléments en base de données (les élements sont insérés via un script SQL externe)
        // et sauvegarde dans une Map
        mapCodePostal = adressedao.loadAllObjects(CodePostal.class).stream().collect(Collectors.toMap(c -> (int) c.getId(), c -> c));
        mapDepartement = adressedao.loadAllObjects(Departement.class).stream().collect(Collectors.toMap(c -> (int) c.getId(), c -> c));
        mapRegion = adressedao.loadAllObjects(Region.class).stream().collect(Collectors.toMap(r -> (int) r.getId(), r -> r));
        mapCommune = adressedao.loadAllObjects(Commune.class).stream().collect(Collectors.toMap(c -> (int) c.getId(), c -> c));
        mapPaysInsee =
            adressedao.loadAllObjects(PaysInsee.class).stream().collect(Collectors.toMap(pinsee -> (int) pinsee.getId(), pinsee -> pinsee));
        mapPaysIso = adressedao.loadAllObjects(PaysIso.class).stream()
            .collect(Collectors.toMap(piso -> Integer.parseInt(piso.getId()), piso -> piso));

        // Vérification du nombre d'élément
        assertEquals(3, mapCodePostal.size());
        assertEquals(2, mapDepartement.size());
        assertEquals(2, mapRegion.size());

        // Suppression des informations en cache pour forcer les requêtes
        adressedao.clearPersistenceContext();
    }

    /**
     * Méthode permettant de tester l'injection du DAO.
     */
    @Test
    public void testInjectionDao()
    {
        assertNotNull("DAO non injecté dans le test", adressedao);
    }

    /**
     * Méthode de test pour
     * {@link fr.gouv.finances.lombok.adresse.dao.impl.AdresseDaoImpl#checkExistCodePostal(java.lang.String)}.
     */
    @Test
    public void testCheckExistCodePostal()
    {
        // Exécution de la méthode à tester sur des paramètres non renseignés ou vides
        assertFalse("Code postal existant", adressedao.checkExistCodePostal(null));
        assertFalse("Code postal existant", adressedao.checkExistCodePostal(""));

        // Exécution de la méthode à tester sans résultat et vérification
        assertFalse("Code postal non prévu", adressedao.checkExistCodePostal(VALEUR_INCONNUE));

        // Exécution de la méthode à tester sur toutes les données et vérifications
        mapCodePostal.entrySet().stream()
            .forEach(e -> assertTrue("Code postal non trouvé", adressedao.checkExistCodePostal(e.getValue().getCode())));
    }

    /**
     * Méthode de test si un code postal existe pour une ville.
     * {@link fr.gouv.finances.lombok.adresse.dao.impl.AdresseDaoImpl#checkExistCodePostalXVille(java.lang.String, java.lang.String)}
     */
    @Test
    public void testCheckExistCodePostalXVille()
    {
        // Exécution de la méthode à tester sur des paramètres non renseignés ou vides
        assertFalse("Un élément sans code postal, ni ville ne devrait pas être trouvé", adressedao.checkExistCodePostalXVille(null, null));
        assertFalse("Un élément sans code postal, ni ville ne devrait pas être trouvé", adressedao.checkExistCodePostalXVille("", ""));
        assertFalse("Une ville sans code ne devrait pas être trouvée",
            adressedao.checkExistCodePostalXVille(null, mapCodePostal.get(1).getVille()));
        assertFalse("Le code postal sans ville ne devrait pas être trouvé",
            adressedao.checkExistCodePostalXVille(mapCodePostal.get(1).getCode(), null));

        // Exécution de la méthode à tester sans résultat et vérification
        assertFalse("Code postal non prévu", adressedao.checkExistCodePostalXVille(
            mapCodePostal.get(3).getCode(), mapCodePostal.get(2).getVille()));

        // Exécution de la méthode à tester sur toutes les données et vérifications
        mapCodePostal.entrySet().stream().forEach(e -> {
            assertTrue("Code postal non trouvé", adressedao.checkExistCodePostalXVille(e.getValue().getCode(), e.getValue().getVille()));
            assertTrue("Code postal non trouvé", adressedao.checkExistCodePostalXVille(
                StringUtils.substring(e.getValue().getCode(), 0, 2), StringUtils.substring(e.getValue().getVille(), 0, 2)));
            assertTrue("Code postal non trouvé", adressedao.checkExistCodePostalXVille(
                StringUtils.substring(e.getValue().getCode(), 0, 2), StringUtils.substring(e.getValue().getVille(), 0, 2).toLowerCase()));
            assertTrue("Code postal non trouvé", adressedao.checkExistCodePostalXVille(
                StringUtils.substring(e.getValue().getCode(), 0, 2), StringUtils.substring(e.getValue().getVille(), 0, 2).toUpperCase()));
        });
    }

    /**
     * Méthode de test pour {@link fr.gouv.finances.lombok.adresse.dao.impl.AdresseDaoImpl#deleteTousLesCodesPostaux()}.
     * Cette méthode possède une instruction de "rollback", pour éviter ne pas supprimer tous les codes postaux en base de
     * données.
     */
    @Test
    @Transactional
    @Rollback(true)
    public void testDeleteTousLesCodesPostaux()
    {
        // Suppression des codes postaux
        adressedao.deleteTousLesCodesPostaux();

        // Vérification
        ControleDonnees.verifierElements(adressedao.loadAllObjects(CodePostal.class));
    }

    /**
     * Méthode de test pour {@link fr.gouv.finances.lombok.adresse.dao.impl.AdresseDaoImpl#findCodePostaux()}.
     */
    @Test
    public void testFindCodePostaux()
    {
        // Vérification des données et du tri
        ControleDonnees.verifierElements(adressedao.findCodePostaux(), true, mapCodePostal.get(3), mapCodePostal.get(2),
            mapCodePostal.get(1));
    }

    /**
     * Méthode de test pour
     * {@link fr.gouv.finances.lombok.adresse.dao.impl.AdresseDaoImpl#findCodesPostauxParCode(java.lang.String)}.
     */
    @Test
    public void testFindCodesPostauxParCode()
    {
        // Exécution de la méthode à tester sur des paramètres non renseignés ou vides
        ControleDonnees.verifierElements(adressedao.findCodesPostauxParCode(null));

        // Exécution de la méthode à tester sans résultat et vérification
        ControleDonnees.verifierElements(adressedao.findCodesPostauxParCode(VALEUR_INCONNUE));

        // Exécution de la méthode à tester sur toutes les données et vérifications
        ControleDonnees.verifierElements(adressedao.findCodesPostauxParCode(mapCodePostal.get(1).getCode()), true, mapCodePostal.get(2),
            mapCodePostal.get(1));
        ControleDonnees.verifierElements(adressedao.findCodesPostauxParCode(mapCodePostal.get(2).getCode()), true, mapCodePostal.get(2),
            mapCodePostal.get(1));
        ControleDonnees.verifierElements(adressedao.findCodesPostauxParCode(mapCodePostal.get(3).getCode()), true, mapCodePostal.get(3));
    }

    /**
     * Méthode de test pour
     * {@link fr.gouv.finances.lombok.adresse.dao.impl.AdresseDaoImpl#findCodesPostauxParCodeEtOuVille(java.lang.String, java.lang.String)}
     * .
     */
    @Test
    public void testFindCodesPostauxParCodeEtOuVille()
    {
        // Exécution de la méthode à tester sur des paramètres non renseignés ou vides
        ControleDonnees.verifierElements(adressedao.findCodesPostauxParCodeEtOuVille(null, null));
        ControleDonnees.verifierElements(adressedao.findCodesPostauxParCodeEtOuVille("", ""));

        // Exécution de la méthode à tester avec résultat et vérification
        ControleDonnees.verifierElements(adressedao.findCodesPostauxParCodeEtOuVille(VALEUR_INCONNUE, VALEUR_INCONNUE));

        // Exécution de la méthode à tester sur toutes les données et vérification
        ControleDonnees.verifierElements(adressedao.findCodesPostauxParCodeEtOuVille(mapCodePostal.get(1).getCode(), null),
            mapCodePostal.get(1), mapCodePostal.get(2));
        ControleDonnees.verifierElements(adressedao.findCodesPostauxParCodeEtOuVille(mapCodePostal.get(2).getCode(), null),
            mapCodePostal.get(1), mapCodePostal.get(2));
        ControleDonnees.verifierElements(adressedao.findCodesPostauxParCodeEtOuVille(mapCodePostal.get(3).getCode(), null),
            mapCodePostal.get(3));
        ControleDonnees.verifierElements(adressedao.findCodesPostauxParCodeEtOuVille(null, mapCodePostal.get(1).getVille()),
            mapCodePostal.get(1), mapCodePostal.get(3));
        ControleDonnees.verifierElements(adressedao.findCodesPostauxParCodeEtOuVille(null, mapCodePostal.get(2).getVille()),
            mapCodePostal.get(2));
        ControleDonnees.verifierElements(adressedao.findCodesPostauxParCodeEtOuVille(null, mapCodePostal.get(3).getVille()),
            mapCodePostal.get(1), mapCodePostal.get(3));
        ControleDonnees.verifierElements(adressedao.findCodesPostauxParCodeEtOuVille(
            StringUtils.substring(mapCodePostal.get(1).getCode(), 0, 2), null), mapCodePostal.get(1), mapCodePostal.get(2),
            mapCodePostal.get(3));
        ControleDonnees.verifierElements(adressedao.findCodesPostauxParCodeEtOuVille(
            StringUtils.substring(mapCodePostal.get(2).getCode(), 0, 2), null), mapCodePostal.get(1), mapCodePostal.get(2),
            mapCodePostal.get(3));
        ControleDonnees.verifierElements(adressedao.findCodesPostauxParCodeEtOuVille(
            StringUtils.substring(mapCodePostal.get(3).getCode(), 0, 2), null), mapCodePostal.get(1), mapCodePostal.get(2),
            mapCodePostal.get(3));
        ControleDonnees.verifierElements(adressedao.findCodesPostauxParCodeEtOuVille(
            null, StringUtils.substring(mapCodePostal.get(1).getVille(), 0, 2)), mapCodePostal.get(1), mapCodePostal.get(3));
        ControleDonnees.verifierElements(
            adressedao.findCodesPostauxParCodeEtOuVille(null, StringUtils.substring(mapCodePostal.get(2).getVille(), 0, 2)),
            mapCodePostal.get(2));
        ControleDonnees.verifierElements(
            adressedao.findCodesPostauxParCodeEtOuVille(null, StringUtils.substring(mapCodePostal.get(3).getVille(), 0, 2)),
            mapCodePostal.get(1), mapCodePostal.get(3));
    }

    /**
     * Méthode de test pour
     * {@link fr.gouv.finances.lombok.adresse.dao.impl.AdresseDaoImpl#findCodesPostauxParVille(java.lang.String)}.
     */
    @Test
    public void testFindCodesPostauxParVille()
    {
        // Exécution de la méthode à tester sur des paramètres non renseignés ou vides
        ControleDonnees.verifierElements(adressedao.findCodesPostauxParVille(null));

        // Exécution de la méthode à tester sans résultat et vérification
        ControleDonnees.verifierElements(adressedao.findCodesPostauxParVille(VALEUR_INCONNUE));

        // Exécution de la méthode à tester sur toutes les données et vérification
        ControleDonnees.verifierElements(adressedao.findCodesPostauxParVille(mapCodePostal.get(1).getVille()), mapCodePostal.get(1),
            mapCodePostal.get(3));
        ControleDonnees.verifierElements(adressedao.findCodesPostauxParVille(mapCodePostal.get(3).getVille()), mapCodePostal.get(1),
            mapCodePostal.get(3));
        ControleDonnees.verifierElements(adressedao.findCodesPostauxParVille(mapCodePostal.get(2).getVille()), mapCodePostal.get(2));
    }

    /**
     * Méthode de test pour
     * {@link fr.gouv.finances.lombok.adresse.dao.impl.AdresseDaoImpl#findTousLesDepartementsAvecRegion()}.
     */
    @Test
    public void testFindTousLesDepartementsAvecRegion()
    {
        // Exécution de la méthode à tester et vérification
        List<Departement> departements = adressedao.findTousLesDepartementsAvecRegion();
        ControleDonnees.verifierElements(departements, mapDepartement.get(1), mapDepartement.get(2));

        // Vérification des associations
        departements.stream().forEach(d -> {
            // Les communes ne doivent pas avoir été chargées
            assertFalse("Les communes ne doivent pas avoir été chargées", adressedao.isCharged(d.getLesCommunes()));

            // Les régions du département doivent avoir été chargées
            assertTrue("Les régions doivent avoir été chargées", adressedao.isCharged(d.getLaRegion()));

            // Vérification des données
            switch ((int) d.getId())
            {
                case 1:
                    // Vérification de la région
                    assertEquals(mapRegion.get(1), d.getLaRegion());
                    break;
                case 2:
                    // Pas de région pour ce département
                    assertNull(d.getLaRegion());
                    break;
                default:
                    fail("département non prévu");
            }
        });
    }

    /**
     * Méthode de test pour {@link fr.gouv.finances.lombok.adresse.dao.impl.AdresseDaoImpl#findTousLesPaysInsee()}.
     */
    @Test
    public void testFindTousLesPaysInsee()
    {
        // Exécution de la méthode à tester puis vérification des données et du tri
        ControleDonnees.verifierElements(
            adressedao.findTousLesPaysInsee(), true, mapPaysInsee.get(2), mapPaysInsee.get(1));
    }

    /**
     * Méthode de test pour {@link fr.gouv.finances.lombok.adresse.dao.impl.AdresseDaoImpl#findTousLesPaysIso()}.
     */
    @Test
    public void testFindTousLesPaysIso()
    {
        // Exécution de la méthode à tester puis vérification des données et du tri
        ControleDonnees.verifierElements(
            adressedao.findTousLesPaysIso(), true, mapPaysIso.get(2), mapPaysIso.get(1));
    }

    /**
     * Méthode de test pour
     * {@link fr.gouv.finances.lombok.adresse.dao.impl.AdresseDaoImpl#findToutesLesRegionsAvecDepartements()}.
     */
    @Test
    public void testFindToutesLesRegionsAvecDepartements()
    {
        // Exécution de la méthode à tester et vérification
        List<Region> regions = adressedao.findToutesLesRegionsAvecDepartements();
        ControleDonnees.verifierElements(regions, true, mapRegion.get(2), mapRegion.get(1));

        // Vérification des associations
        regions.stream().forEach(r -> {
            // Les départements doivent avoir été chargés
            assertTrue("Les départements ne doivent pas avoir été chargés", adressedao.isCharged(r.getLesDepartements()));

            // Vérification des données
            switch ((int) r.getId())
            {
                case 1:
                    ControleDonnees.verifierElements(r.getLesDepartements(), mapDepartement.get(1));
                    break;
                case 2:
                    ControleDonnees.verifierElements(r.getLesDepartements());
                    break;
                default:
                    fail("département non prévu");
            }
        });
    }

    /**
     * Méthode de test pour
     * {@link fr.gouv.finances.lombok.adresse.dao.impl.AdresseDaoImpl#findUnDepartementParCodeDepartementINSEE( java.lang.String)}.
     */
    @Test
    public void testFindUnDepartementParCodeDepartementINSEE()
    {
        // Exécution de la méthode à tester sur des paramètres non renseignés ou vides
        VerificationExecution.verifierException(ProgrammationException.class,
            () -> adressedao.findUnDepartementParCodeDepartementINSEE(null));
        VerificationExecution.verifierException(ProgrammationException.class,
            () -> adressedao.findUnDepartementParCodeDepartementINSEE(""));

        // Exécution de la méthode à tester sans résultat et vérification
        assertNull(adressedao.findUnDepartementParCodeDepartementINSEE(VALEUR_INCONNUE));

        // Exécution de la méthode à tester sur toutes les données et vérification
        mapDepartement.entrySet().stream().forEach(e -> {
            Departement departement = adressedao.findUnDepartementParCodeDepartementINSEE(e.getValue().getCodedepartementINSEE());
            assertEquals(e.getValue(), departement);

            // Vérification des associations
            assertFalse("Les communes ne doivent pas avoir été chargées", adressedao.isCharged(e.getValue().getLesCommunes()));
            switch ((int) e.getValue().getId())
            {
                case 1:
                    assertFalse("La région ne doit pas avoir été chargée", adressedao.isCharged(departement.getLaRegion()));
                    break;
                case 2:
                    assertTrue("La région doit être chargée car vide", adressedao.isCharged(departement.getLaRegion()));
                    assertNull("Pas de région pour ce département", departement.getLaRegion());
                    break;
                default:
                    fail("département non prévu");
            }
        });
    }

    /**
     * Méthode de test pour
     * {@link fr.gouv.finances.lombok.adresse.dao.impl.AdresseDaoImpl #findUnDepartementParCodeDepartementINSEEAvecRegion(java.lang.String)}.
     */
    @Test
    public void testFindUnDepartementParCodeDepartementINSEEAvecRegion()
    {
        // Exécution de la méthode à tester sur des paramètres non renseignés ou vides
        VerificationExecution.verifierException(ProgrammationException.class,
            () -> adressedao.findUnDepartementParCodeDepartementINSEEAvecRegion(null));
        VerificationExecution.verifierException(ProgrammationException.class,
            () -> adressedao.findUnDepartementParCodeDepartementINSEEAvecRegion(""));

        // Exécution de la méthode à tester sans résultat et vérification
        assertNull(adressedao.findUnDepartementParCodeDepartementINSEEAvecRegion(VALEUR_INCONNUE));

        // Exécution de la méthode à tester sur toutes les données et vérification
        mapDepartement.entrySet().stream().forEach(e -> {
            Departement departement = adressedao.findUnDepartementParCodeDepartementINSEEAvecRegion(e.getValue().getCodedepartementINSEE());
            assertEquals(e.getValue(), departement);

            // Vérification des associations
            assertTrue("La région doit avoir été chargée", adressedao.isCharged(departement.getLaRegion()));
            assertFalse("Les communes ne doivent pas avoir été chargées", adressedao.isCharged(departement.getLesCommunes()));
            switch ((int) departement.getId())
            {
                case 1:
                    assertEquals(mapRegion.get(1), departement.getLaRegion());
                    break;
                case 2:
                    assertNull("Pas de région pour ce département", departement.getLaRegion());
                    break;
                default:
                    fail("département non prévu");
            }
        });
    }

    /**
     * Méthode de test pour
     * {@link fr.gouv.finances.lombok.adresse.dao.impl.AdresseDaoImpl #findUnDepartementParCodeDepartementINSEEAvecRegionEtCommunes(java.lang.String)}.
     */
    @Test
    public void testFindUnDepartementParCodeDepartementINSEEAvecRegionEtCommunes()
        throws InterruptedException, ExecutionException
    {
        // Exécution de la méthode à tester sur des paramètres non renseignés ou vides
        VerificationExecution.verifierException(ProgrammationException.class,
            () -> adressedao.findUnDepartementParCodeDepartementINSEEAvecRegionEtCommunes(null));
        VerificationExecution.verifierException(ProgrammationException.class,
            () -> adressedao.findUnDepartementParCodeDepartementINSEEAvecRegionEtCommunes(""));

        // Exécution de la méthode à tester sans résultat et vérification
        assertNull(adressedao.findUnDepartementParCodeDepartementINSEEAvecRegionEtCommunes(VALEUR_INCONNUE));

        // Exécution de la méthode à tester sur toutes les données et vérification
        mapDepartement.entrySet().stream().forEach(e -> {
            Departement departement = adressedao
                .findUnDepartementParCodeDepartementINSEEAvecRegionEtCommunes(
                    e.getValue().getCodedepartementINSEE());
            assertEquals(e.getValue(), departement);

            // Vérification des associations
            assertTrue("La région doit avoir été chargée", adressedao.isCharged(departement.getLaRegion()));
            assertTrue("Les communes doivent avoir été chargées", adressedao.isCharged(departement.getLesCommunes()));
            switch ((int) e.getValue().getId())
            {
                case 1:
                    assertEquals(mapRegion.get(1), departement.getLaRegion());
                    ControleDonnees.verifierElements(departement.getLesCommunes(), mapCommune.get(1));
                    break;
                case 2:
                    assertNull("Pas de région pour ce département", departement.getLaRegion());
                    ControleDonnees.verifierElements(departement.getLesCommunes());
                    break;
                default:
                    fail("département non prévu");
            }
        });
    }

    /**
     * Méthode de test pour
     * {@link fr.gouv.finances.lombok.adresse.dao.impl.AdresseDaoImpl#findUneCommuneParCodeCommuneINSEE( java.lang.String)}.
     */
    @Test
    public void testFindUneCommuneParCodeCommuneINSEE()
    {
        // Exécution de la méthode à tester sur des paramètres non renseignés ou vides
        VerificationExecution.verifierException(ProgrammationException.class, () -> adressedao.findUneCommuneParCodeCommuneINSEE(null));
        VerificationExecution.verifierException(ProgrammationException.class, () -> adressedao.findUneCommuneParCodeCommuneINSEE(""));

        // Exécution de la méthode à tester sans résultat et vérification
        assertNull(adressedao.findUneCommuneParCodeCommuneINSEE(VALEUR_INCONNUE));

        // Exécution de la méthode à tester sur toutes les données et vérification
        mapCommune.entrySet().stream()
            .forEach(e -> assertEquals(e.getValue(), adressedao.findUneCommuneParCodeCommuneINSEE(e.getValue().getCodecommuneINSEE())));
    }

    /**
     * Méthode de test pour
     * {@link fr.gouv.finances.lombok.adresse.dao.impl.AdresseDaoImpl#rechercherUneCommuneParCodeCommuneINSEE( java.lang.String)}.
     */
    @Test
    public void testRechercherUneCommuneParCodeCommuneINSEE()
    {
        testFindUneCommuneParCodeCommuneINSEE();
    }

    /**
     * Méthode de test pour
     * {@link fr.gouv.finances.lombok.adresse.dao.impl.AdresseDaoImpl#findUneRegionParCodeRegionINSEE( java.lang.String)}.
     */
    @Test
    public void testFindUneRegionParCodeRegionINSEE()
    {
        // Exécution de la méthode à tester sur des paramètres non renseignés ou vides
        VerificationExecution.verifierException(ProgrammationException.class, () -> adressedao.findUneRegionParCodeRegionINSEE(null));
        VerificationExecution.verifierException(ProgrammationException.class, () -> adressedao.findUneRegionParCodeRegionINSEE(""));

        // Exécution de la méthode à tester sans résultat et vérification
        assertNull(adressedao.findUneRegionParCodeRegionINSEE(VALEUR_INCONNUE));

        // Exécution de la méthode à tester sur toutes les données et vérification
        mapRegion.entrySet().stream()
            .forEach(e -> assertEquals(e.getValue(), adressedao.findUneRegionParCodeRegionINSEE(e.getValue().getCoderegionINSEE())));
    }

    /**
     * Méthode de test pour
     * {@link fr.gouv.finances.lombok.adresse.dao.impl.AdresseDaoImpl#findUnPaysInseeParCodePaysInsee( java.lang.String)}.
     * 
     * @throws ExecutionException   erreur en cas d'exécution d'un Future
     * @throws InterruptedException en cas d'interruption d'un Future
     */
    @Test
    public void testFindUnPaysInseeParCodePaysInsee() throws InterruptedException, ExecutionException
    {
        // Exécution de la méthode à tester sur des paramètres non renseignés ou vides
        VerificationExecution.verifierException(ProgrammationException.class, () -> adressedao.findUnPaysInseeParCodePaysInsee(null));
        VerificationExecution.verifierException(ProgrammationException.class, () -> adressedao.findUnPaysInseeParCodePaysInsee(""));

        // Exécution de la méthode à tester sans résultat et vérification
        assertNull(adressedao.findUnPaysInseeParCodePaysInsee(VALEUR_INCONNUE));

        // Exécution de la méthode à tester sur toutes les données et vérification
        mapPaysInsee.entrySet().stream()
            .forEach(e -> assertEquals(e.getValue(), adressedao.findUnPaysInseeParCodePaysInsee(e.getValue().getCodePaysInsee())));
    }

    /**
     * Méthode de test pour
     * {@link fr.gouv.finances.lombok.adresse.dao.impl.AdresseDaoImpl#findUnPaysIsoParCodePaysIso( java.lang.String)}.
     */
    @Test
    public void testFindUnPaysIsoParCodePaysIso()
    {
        // Exécution de la méthode à tester sur des paramètres non renseignés ou vides
        VerificationExecution.verifierException(ProgrammationException.class, () -> adressedao.findUnPaysIsoParCodePaysIso(null));
        VerificationExecution.verifierException(ProgrammationException.class, () -> adressedao.findUnPaysIsoParCodePaysIso(""));

        // Exécution de la méthode à tester sans résultat et vérification
        assertNull(adressedao.findUnPaysIsoParCodePaysIso(VALEUR_INCONNUE));

        // Exécution de la méthode à tester sur toutes les données et vérification
        mapPaysIso.entrySet().stream()
            .forEach(e -> assertEquals(e.getValue(), adressedao.findUnPaysIsoParCodePaysIso(e.getValue().getCodeIsoAlpha2())));
    }

    /**
     * Méthode de test pour
     * {@link fr.gouv.finances.lombok.adresse.dao.impl.AdresseDaoImpl#testExistenceCommune(java.lang.String)}.
     */
    @Test
    public void testExistenceCommune()
    {
        // Exécution de la méthode à tester sur des paramètres non renseignés ou vides
        assertFalse("Aucune commune ne devrait être trouvée", adressedao.testExistenceCommune(null));
        assertFalse("Aucune commune ne devrait être trouvée", adressedao.testExistenceCommune(""));

        // Exécution de la méthode à tester sans résultat et vérification
        assertFalse("Aucune commune ne devrait être trouvée", adressedao.testExistenceCommune(VALEUR_INCONNUE));

        // Exécution de la méthode à tester sur toutes les données et vérification
        mapCommune.entrySet().stream().forEach(e -> assertTrue(adressedao.testExistenceCommune(e.getValue().getCodecommuneINSEE())));
    }

    /**
     * Méthode de test pour
     * {@link fr.gouv.finances.lombok.adresse.dao.impl.AdresseDaoImpl#testExistenceDepartement(java.lang.String)}.
     */
    @Test
    public void testExistenceDepartement()
    {
        // Exécution de la méthode à tester sur des paramètres non renseignés ou vides
        assertFalse("Aucun département ne devrait être trouvé", adressedao.testExistenceDepartement(null));
        assertFalse("Aucun département ne devrait être trouvé", adressedao.testExistenceDepartement(""));

        // Exécution de la méthode à tester sans résultat et vérification
        assertFalse("Aucun département ne devrait être trouvé", adressedao.testExistenceDepartement(VALEUR_INCONNUE));

        // Exécution de la méthode à tester sur toutes les données et vérification
        mapDepartement.entrySet().stream()
            .forEach(e -> assertTrue(adressedao.testExistenceDepartement(e.getValue().getCodedepartementINSEE())));
    }

    /**
     * Méthode de test pour
     * {@link fr.gouv.finances.lombok.adresse.dao.impl.AdresseDaoImpl#testExistenceRegion(java.lang.String)}.
     */
    @Test
    public void testExistenceRegion()
    {
        // Exécution de la méthode à tester sur des paramètres non renseignés ou vides
        assertFalse("Aucune région ne devrait être trouvée", adressedao.testExistenceRegion(null));
        assertFalse("Aucune région ne devrait être trouvée", adressedao.testExistenceRegion(""));

        // Exécution de la méthode à tester sans résultat et vérification
        assertFalse("Aucune région ne devrait être trouvée", adressedao.testExistenceRegion(VALEUR_INCONNUE));

        // Exécution de la méthode à tester sur toutes les données et vérification
        mapRegion.entrySet().stream().forEach(e -> assertTrue(adressedao.testExistenceRegion(e.getValue().getCoderegionINSEE())));
    }

    /**
     * Tests du paramétrage ORM des contraintes du code postal.
     */
    @Test
    public void testContraintesCodePostal()
    {
        // Insertion d'une valeur de couple (code + ville) déjà existante
        CodePostal codePostal = new CodePostal();
        codePostal.setCode(mapCodePostal.get(1).getCode());
        codePostal.setVille(mapCodePostal.get(1).getVille());

        VerificationExecution.verifierException(DataIntegrityViolationException.class, () -> adressedao.saveObject(codePostal));
    }

    /**
     * Tests du paramétrage ORM des contraintes de la commune.
     */
    @Test
    public void testContraintesCommune()
    {
        LOGGER.debug("Tests du paramétrage ORM des contraintes de la commune");
        // Insertion d'une commune déjà existante
        Commune commune = new Commune();
        commune.setCodecommuneINSEE(mapCommune.get(1).getCodecommuneINSEE());
        VerificationExecution.verifierException(DataIntegrityViolationException.class, () -> adressedao.saveObject(commune));
    }

    /**
     * Tests du paramétrage ORM des contraintes du département.
     */
    @Test
    public void testContraintesDepartement()
    {
        // Insertion d'un département déjà existant
        Departement departement = new Departement();
        departement.setCodedepartementINSEE(mapDepartement.get(1).getCodedepartementINSEE());

        VerificationExecution.verifierException(DataIntegrityViolationException.class, () -> adressedao.saveObject(departement));
    }

    /**
     * Tests du paramétrage ORM des contraintes du pays INSEE.
     */
    @Test
    public void testContraintesPaysInsee()
    {
        // Insertion d'un département déjà existant
        PaysInsee pays = new PaysInsee();
        pays.setCodePaysInsee(mapPaysInsee.get(1).getCodePaysInsee());

        VerificationExecution.verifierException(DataIntegrityViolationException.class, () -> adressedao.saveObject(pays));
    }

    /**
     * Tests du paramétrage ORM des contraintes de la région.
     */
    @Test
    public void testContraintesRegion()
    {
        // Insertion d'une région déjà existante
        Region region = new Region();
        region.setCoderegionINSEE(mapRegion.get(1).getCoderegionINSEE());

        VerificationExecution.verifierException(DataIntegrityViolationException.class, () -> adressedao.saveObject(region));
    }

    /**
     * Tests du paramétrage ORM des contraintes de l'adresse.
     */
    @Test
    public void testContraintesAdresse()
    {
        // Insertion d'une adresse
        adressedao.saveObject(getAdresse());

        // Insertion de la même adresse sans problème puisque pas de contrainte
        adressedao.saveObject(getAdresse());
    }

    /**
     * Méthode permettant de générer une adresse
     *
     * @return l'adresse générée
     */
    private Adresse getAdresse()
    {
        Adresse adresse = new Adresse();
        adresse.setCodePostalEtCedex("cp");
        adresse.setPays("pays");
        adresse.setVille("ville");
        adresse.setLigne2ServiceAppartEtgEsc("appart");
        adresse.setLigne3BatimentImmeubleResid("bat");
        adresse.setLigne4NumeroLibelleVoie("num");
        adresse.setLigne5LieuDitMentionSpeciale("lieu");
        return adresse;
    }
}
