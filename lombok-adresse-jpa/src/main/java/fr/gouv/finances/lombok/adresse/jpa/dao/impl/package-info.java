/*
 * Copyright (c) 2017 DGFiP - Tous droits réservés
 * 
 */
/**
 * Paquet lié aux implémentation JPA des DAO
 *
 * @author Christophe Breheret-Girardin
 */
package fr.gouv.finances.lombok.adresse.jpa.dao.impl;