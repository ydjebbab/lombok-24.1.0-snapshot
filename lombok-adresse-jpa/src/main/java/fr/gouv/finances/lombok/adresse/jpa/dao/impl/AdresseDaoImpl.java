/*
 * Copyright (c) 2014 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.adresse.jpa.dao.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import fr.gouv.finances.lombok.adresse.bean.CodePostal;
import fr.gouv.finances.lombok.adresse.bean.Commune;
import fr.gouv.finances.lombok.adresse.bean.Departement;
import fr.gouv.finances.lombok.adresse.bean.PaysInsee;
import fr.gouv.finances.lombok.adresse.bean.PaysIso;
import fr.gouv.finances.lombok.adresse.bean.Region;
import fr.gouv.finances.lombok.adresse.dao.AdresseDao;
import fr.gouv.finances.lombok.adresse.dao.ProprieteAdresse;
import fr.gouv.finances.lombok.jpa.dao.BaseDaoJpaImpl;
import fr.gouv.finances.lombok.util.persistance.ModeCritereRecherche;

/**
 * Implémentation JPA du DAO de gestion des adresses
 *
 * @author CBRE
 * @author CF : migration vers JPA
 */
@ConditionalOnProperty(name = "lombok.orm.jpa", havingValue="true")
@Repository("adressedaoimpl")
@Transactional(transactionManager="transactionManager")
public class AdresseDaoImpl extends BaseDaoJpaImpl implements AdresseDao
{
    protected static final Logger LOGGER = LoggerFactory.getLogger(AdresseDaoImpl.class);

    public AdresseDaoImpl()
    {
        super();
    }

    /** {@inheritDoc} */
    @Override
    public boolean checkExistCodePostal(String codePostal)
    {
        // Journalisation de la recherche
        LOGGER.debug("Recherche de l'existence du code postal '{}'", codePostal);

        // Si le paramètre n'est pas renseigné, la recherche n'est pas exécutée
        if (StringUtils.isEmpty(codePostal))
        {
            return false;
        }

        // Préparation des critères
        Map<String, String> criteres = new HashMap<>();
        criteres.put(ProprieteAdresse.CODE_POSTAL.getNom(), codePostal);

        // Existe-il au moins un résultat ?
        return countTotalNumberOfLines(CodePostal.class, criteres, ModeCritereRecherche.EQUAL) > 0;
    }

    /** {@inheritDoc} */
    @Override
    public boolean checkExistCodePostalXVille(String codePostal, String ville)
    {
        // Si l'un des paramètres n'est pas renseigné, la recherche n'est pas exécutée
        if (StringUtils.isEmpty(codePostal) || StringUtils.isEmpty(ville))
        {
            return false;
        }

        // Préparation des critères
        Map<String, String> criteres = new HashMap<>();
        criteres.put(ProprieteAdresse.CODE_POSTAL.getNom(), codePostal.trim());
        criteres.put(ProprieteAdresse.VILLE.getNom(), ville.trim());

        // Existe-il au moins un résultat ?
        return countTotalNumberOfLines(CodePostal.class, criteres, ModeCritereRecherche.LIKE_DEBUT_IGNORE_CASSE) > 0;
    }

    /** {@inheritDoc} */
    @Override
    public void deleteTousLesCodesPostaux()
    {
        // Journalisation de la mise à jour
        LOGGER.trace("Suppression de tous les codes postaux");

        // Suppression ensembliste des codes postaux
        entityManager.createQuery("delete from " + CodePostal.class.getSimpleName()).executeUpdate();
    }

    /** {@inheritDoc} */
    @Override
    public List<CodePostal> findCodePostaux()
    {
        // Journalisation de la recherche
        LOGGER.trace("Recherche de tous les codes postaux");

        // Paramètrage de la recherche
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<CodePostal> query = criteriaBuilder.createQuery(CodePostal.class);
        Root<CodePostal> root = query.from(CodePostal.class);

        // Paramétrage du tri
        query.orderBy(criteriaBuilder.asc(root.get(ProprieteAdresse.CODE_POSTAL.getNom())));

        // Exécution de la recherche
        return findAll(query);
    }

    /** {@inheritDoc} */
    @Override
    public List<CodePostal> findCodesPostauxParCode(String codePostal)
    {
        // Journalisation de la recherche
        LOGGER.debug("Recherche des codes postaux '{}'", codePostal);

        // Si le paramètre n'est pas renseigné, la recherche n'est pas exécutée
        if (StringUtils.isEmpty(codePostal))
        {
            return new ArrayList<>();
        }

        // Paramètrage de la recherche
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<CodePostal> query = criteriaBuilder.createQuery(CodePostal.class);
        Root<CodePostal> root = query.from(CodePostal.class);

        // Paramètrage des critères
        query.where(criteriaBuilder.equal(root.<String> get(ProprieteAdresse.CODE_POSTAL.getNom()), codePostal));

        // Paramétrage du tri
        query.orderBy(criteriaBuilder.asc(root.get(ProprieteAdresse.VILLE.getNom())));

        // Exécution de la recherche
        return findAll(query);
    }

    /** {@inheritDoc} */
    @Override
    public List<CodePostal> findCodesPostauxParCodeEtOuVille(String codePostal, String ville)
    {
        // Journalisation de la recherche
        LOGGER.debug("Recherche des codes postaux '{}' liés à la ville '{}'", codePostal, ville);

        // L'un des deux critères doit obligatoirement être renseigné
        if (StringUtils.isEmpty(codePostal) && StringUtils.isEmpty(ville))
        {
            return new ArrayList<>();
        }

        // Préparation des critères
        Map<String, String> criteres = new HashMap<>();
        // Si une recherche doit être effectuée sur le critère du code postal
        if (!StringUtils.isEmpty(codePostal))
        {
            // Ajout du critère de recherche sur le code postal
            criteres.put(ProprieteAdresse.CODE_POSTAL.getNom(), codePostal.trim());
        }
        // Si une recherche doit être effectuée sur le critère ville
        if (!StringUtils.isEmpty(ville))
        {
            // Ajout du critère de recherche sur la ville
            criteres.put(ProprieteAdresse.VILLE.getNom(), ville.trim());
        }

        // Recherche des codes postaux par égalité large
        return findAllByCriterias(CodePostal.class, criteres, ModeCritereRecherche.LIKE_DEBUT);
    }

    /** {@inheritDoc} */
    @Override
    public List<CodePostal> findCodesPostauxParVille(String ville)
    {
        // Journalisation de la recherche
        LOGGER.debug("Recherche des codes postaux liés à la ville '{}'", ville);

        // Si le paramètre n'est pas renseigné, la recherche n'est pas exécutée
        if (StringUtils.isEmpty(ville))
        {
            return new ArrayList<>();
        }

        // Paramètrage de la recherche
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<CodePostal> query = criteriaBuilder.createQuery(CodePostal.class);
        Root<CodePostal> root = query.from(CodePostal.class);

        // Paramètrage des critères
        query.where(criteriaBuilder.equal(criteriaBuilder.lower(root.<String> get(ProprieteAdresse.VILLE.getNom())),
            ville.toLowerCase(Locale.FRANCE)));

        // Paramétrage du tri
        query.orderBy(criteriaBuilder.asc(root.get(ProprieteAdresse.VILLE.getNom())));

        // Exécution de la recherche
        return findAll(query);
    }

    /** {@inheritDoc} */
    @Override
    public List<Departement> findTousLesDepartementsAvecRegion()
    {
        // Journalisation de la recherche
        LOGGER.trace("Recherche de tous les départements, avec les régions associées");

        // Paramètrage de la recherche
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Departement> query = criteriaBuilder.createQuery(Departement.class);
        Root<Departement> root = query.from(Departement.class);

        // Paramètrage des jointures
        root.fetch(ProprieteAdresse.REGION.getNom(), JoinType.LEFT);

        // Paramètrage du tri
        query.orderBy(criteriaBuilder.asc(root.get(ProprieteAdresse.CODE_DEPARTEMENT.getNom())));

        // Recherche des départements
        return findAll(query);
    }

    /** {@inheritDoc} */
    @Override
    public List<PaysInsee> findTousLesPaysInsee()
    {
        // Journalisation de la recherche
        LOGGER.trace("Recherche de tous les pays selon la codification INSEE");

        // Paramètrage de la recherche
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<PaysInsee> query = criteriaBuilder.createQuery(PaysInsee.class);
        Root<PaysInsee> root = query.from(PaysInsee.class);

        // Paramétrage du tri
        query.orderBy(criteriaBuilder.asc(root.get(ProprieteAdresse.NOM_PAYS.getNom())));

        // Exécution de la recherche
        return findAll(query);
    }

    /** {@inheritDoc} */
    @Override
    public List<PaysIso> findTousLesPaysIso()
    {
        // Journalisation de la recherche
        LOGGER.trace("Recherche de tous les pays selon la codification ISO");

        // Paramètrage de la recherche
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<PaysIso> query = criteriaBuilder.createQuery(PaysIso.class);
        Root<PaysIso> root = query.from(PaysIso.class);

        // Paramétrage du tri
        query.orderBy(criteriaBuilder.asc(root.get(ProprieteAdresse.NOM_PAYS.getNom())));

        // Exécution de la recherche
        return findAll(query);
    }

    /** {@inheritDoc} */
    @Override
    public List<Region> findToutesLesRegionsAvecDepartements()
    {
        // Journalisation de la recherche
        LOGGER.trace("Recherche de toutes les régions, et les départements associés");

        // Paramètrage de la recherche
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Region> query = criteriaBuilder.createQuery(Region.class);
        Root<Region> root = query.from(Region.class);

        // Paramètrage des jointures
        root.fetch(ProprieteAdresse.DEPARTEMENTS.getNom(), JoinType.LEFT);

        // Paramétrage du tri
        query.orderBy(criteriaBuilder.asc(root.get(ProprieteAdresse.LIBELLE_REGION.getNom())));

        // Exécution de la recherche
        return findAll(query);
    }

    /** {@inheritDoc} */
    @Override
    public Departement findUnDepartementParCodeDepartementINSEE(String codedepartementINSEE)
    {
        // Journalisation de la recherche
        LOGGER.trace("Recherche du département de code INSEE '{}'", codedepartementINSEE);

        // Recherche du département
        return findbyUniqueCriteria(Departement.class, ProprieteAdresse.CODE_DEPARTEMENT.getNom(), codedepartementINSEE);
    }

    /** {@inheritDoc} */
    @Override
    public Departement findUnDepartementParCodeDepartementINSEEAvecRegion(String codedepartementINSEE)
    {
        // Journalisation de la recherche
        LOGGER.trace("Recherche du département de code INSEE '{}', avec la région associée", codedepartementINSEE);

        // Paramétrage de la jointure
        Map<String, JoinType> jointures = new HashMap<>();
        jointures.put(ProprieteAdresse.REGION.getNom(), JoinType.LEFT);

        // Recherche du département
        return findByUniqueCriteriaWithJoins(Departement.class, ProprieteAdresse.CODE_DEPARTEMENT.getNom(), codedepartementINSEE,
            jointures);
    }

    /** {@inheritDoc} */
    @Override
    public Departement findUnDepartementParCodeDepartementINSEEAvecRegionEtCommunes(
        String codedepartementINSEE)
    {
        // Journalisation de la recherche
        LOGGER.trace("Recherche du département de code INSEE '{}', avec la région et la commune associées", codedepartementINSEE);

        // Paramétrage des jointures
        Map<String, JoinType> jointures = new HashMap<>();
        jointures.put(ProprieteAdresse.REGION.getNom(), JoinType.LEFT);
        jointures.put(ProprieteAdresse.COMMUNES.getNom(), JoinType.LEFT);

        // Recherche du département
        return findByUniqueCriteriaWithJoins(Departement.class, ProprieteAdresse.CODE_DEPARTEMENT.getNom(), codedepartementINSEE,
            jointures);
    }

    /** {@inheritDoc} */
    @Override
    public Commune findUneCommuneParCodeCommuneINSEE(String codecommuneINSEE)
    {
        // Journalisation de la recherche
        LOGGER.trace("Recherche de la commune de code INSEE '{}'", codecommuneINSEE);

        // Recherche d'une commune, filtrée sur la propriété du code commune
        return findbyUniqueCriteria(Commune.class, ProprieteAdresse.CODE_COMMUNE.getNom(), codecommuneINSEE);
    }

    /** {@inheritDoc} */
    @Override
    public Region findUneRegionParCodeRegionINSEE(String coderegionINSEE)
    {
        // Journalisation de la recherche
        LOGGER.trace("Recherche de la région de code INSEE '{}'", coderegionINSEE);

        // Recherche d'une région, filtrée sur la propriété du code région
        return findbyUniqueCriteria(Region.class, ProprieteAdresse.CODE_REGION.getNom(), coderegionINSEE);
    }

    /** {@inheritDoc} */
    @Override
    public PaysInsee findUnPaysInseeParCodePaysInsee(String codePaysInsee)
    {
        // Journalisation de la recherche
        LOGGER.trace("Recherche du pays de code INSEE '{}'", codePaysInsee);

        // Recherche d'un pays INSEE, filtré sur la propriété du code INSEE du pays
        return findbyUniqueCriteria(PaysInsee.class, ProprieteAdresse.CODE_PAYS_INSEE.getNom(), codePaysInsee);
    }

    /** {@inheritDoc} */
    @Override
    public PaysIso findUnPaysIsoParCodePaysIso(String codeIsoAlpha2)
    {
        // Journalisation de la recherche
        LOGGER.trace("Recherche du pays de code ISO '{}'", codeIsoAlpha2);

        // Recherche d'un pays ISO, filtré sur la propriété code ISO du pays
        return findbyUniqueCriteria(PaysIso.class, ProprieteAdresse.CODE_PAYS_ISO.getNom(), codeIsoAlpha2);
    }

    /** {@inheritDoc} */
    @Override
    public boolean testExistenceCommune(String codeCommuneINSEE)
    {
        // Journalisation de la recherche
        LOGGER.debug("Recherche de l'existence de la commune de code INSEE '{}'", codeCommuneINSEE);

        // Si le paramètre n'est pas renseigné, la recherche n'est pas exécutée
        if (StringUtils.isEmpty(codeCommuneINSEE))
        {
            return false;
        }

        // Préparation des critères
        Map<String, String> criteres = new HashMap<>();
        criteres.put(ProprieteAdresse.CODE_COMMUNE.getNom(), codeCommuneINSEE);

        // Existe-il au moins un résultat ?
        return countTotalNumberOfLines(Commune.class, criteres, ModeCritereRecherche.EQUAL_IGNORE_CASSE) > 0;
    }

    /** {@inheritDoc} */
    @Override
    public boolean testExistenceDepartement(String codeDepartement)
    {
        // Journalisation de la recherche
        LOGGER.debug("Recherche de l'existence d'un département de code '{}'", codeDepartement);

        // Si le paramètre n'est pas renseigné, la recherche n'est pas exécutée
        if (StringUtils.isEmpty(codeDepartement))
        {
            return false;
        }

        // Préparation des critères
        Map<String, String> criteres = new HashMap<>();
        criteres.put(ProprieteAdresse.CODE_DEPARTEMENT.getNom(), codeDepartement);

        // Existe-il au moins un résultat ?
        return countTotalNumberOfLines(Departement.class, criteres, ModeCritereRecherche.EQUAL_IGNORE_CASSE) > 0;
    }

    /** {@inheritDoc} */
    @Override
    public boolean testExistenceRegion(String codeRegion)
    {
        // Journalisation de la recherche
        LOGGER.debug("Recherche de l'existence d'une région de code INSEE '{}'", codeRegion);

        // Si le paramètre n'est pas renseigné, la recherche n'est pas exécutée
        if (StringUtils.isEmpty(codeRegion))
        {
            return false;
        }

        // Préparation des critères
        Map<String, String> criteres = new HashMap<>();
        criteres.put(ProprieteAdresse.CODE_REGION.getNom(), codeRegion);

        // Existe-il au moins un résultat ?
        return countTotalNumberOfLines(Region.class, criteres, ModeCritereRecherche.EQUAL_IGNORE_CASSE) > 0;
    }

    /**
     * pour autocompletion communes findLesCommunesParDebutLibelleCommune
     *
     * @param nomcommune
     * @return Les communes
     */
    @Override
    public List<String> findLesCommunesParDebutLibelleCommune(String nomcommune)
    {
        LOGGER.debug(">>> Debut findLesCommunesParDebutLibelleCommune");

        List<String> lesCommunes = new ArrayList<>();
        CriteriaBuilder criteriabuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Commune> criteriaQuery = criteriabuilder.createQuery(Commune.class);
        Root<Commune> communeRoot = criteriaQuery.from(Commune.class);

        Predicate predicate =
            criteriabuilder.like(criteriabuilder.lower(communeRoot.get("libellecommune")), nomcommune.trim().toLowerCase() + '%');
        criteriaQuery.where(predicate);
        criteriaQuery.select(communeRoot);
        // Pour debug seulement
        TypedQuery<Commune> communeTypedQuery = entityManager.createQuery(criteriaQuery);
        LOGGER.debug("communeTypedQuery : " + communeTypedQuery.unwrap(org.hibernate.Query.class).getQueryString());

        List<Commune> communeListeResultat = communeTypedQuery.getResultList();
        LOGGER.debug("communeListeResultat : " + communeListeResultat);

        if (null != communeListeResultat)
        {
            for (Commune commune : communeListeResultat)
            {
                lesCommunes.add(commune.getLibellecommune());
            }
        }
        return lesCommunes;
    }

    /**
     * pour autocompletion pays
     *
     * @param pays
     * @return Les pays
     */
    @Override
    public List<String> findLesPaysParDebutLibellePays(String pays)
    {
        LOGGER.debug(">>> Debut findLesPaysParDebutLibellePays");

        List<String> lesPays = new ArrayList<>();
        CriteriaBuilder criteriabuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<PaysIso> criteriaQuery = criteriabuilder.createQuery(PaysIso.class);
        Root<PaysIso> paysIsoRoot = criteriaQuery.from(PaysIso.class);

        Predicate predicate =
            criteriabuilder.like(criteriabuilder.lower(paysIsoRoot.get("nom")), pays.trim().toLowerCase() + '%');
        criteriaQuery.where(predicate);
        criteriaQuery.select(paysIsoRoot);
        // Pour debug seulement
        TypedQuery<PaysIso> paysIsoTypedQuery = entityManager.createQuery(criteriaQuery);
        LOGGER.debug("paysIsoTypedQuery : " + paysIsoTypedQuery.unwrap(org.hibernate.Query.class).getQueryString());

        List<PaysIso> paysIsoListeResultat = paysIsoTypedQuery.getResultList();
        LOGGER.debug("paysIsoListeResultat : " + paysIsoListeResultat);

        if (null != paysIsoListeResultat)
        {
            for (PaysIso paysIso : paysIsoListeResultat)
            {
                lesPays.add(paysIso.getNom());
            }
        }
        return lesPays;
    }

}
