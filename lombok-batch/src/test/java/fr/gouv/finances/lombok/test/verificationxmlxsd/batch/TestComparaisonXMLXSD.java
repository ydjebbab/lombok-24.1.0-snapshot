/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : testComparaisonXMLXSD.java
 *
 */
package fr.gouv.finances.lombok.test.verificationxmlxsd.batch;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.gouv.finances.lombok.batch.ServiceBatchCommun;

/**
 * Class de test TestComparaisonXMLXSD
 * 
 * @version $Revision: 1.2 $ Date: 11 déc. 2009
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles(profiles = {"embedded", "hibernate", "edition", "atlas", "journal", "structure", "bancaire", "jdbc"})
@ContextConfiguration(name = "application", locations = {"classpath*:/conf/app-config.xml",
        "classpath:testtraitementverificationxmlxsdContext-service.xml"})
// pas sur que l'ajout de la config marche comme ceci (car ça vient pas par héritage mais par annotation)

public class TestComparaisonXMLXSD
{
    @Autowired
    protected ApplicationContext context;

    /**
     * methode Test traiter batch
     */
    @Test
    public void testTraiterBatch()
    {
        ServiceBatchCommun sbc = (ServiceBatchCommun) context.getBean("traitementverificationxmlxsdimpl");
        int statuscode = sbc.demarrer();
        Assert.assertEquals(0, statuscode);
    }

}
