/*
 * Copyright (c) 2017 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.batch;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.jfree.util.Log;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.ExitCodeEvent;
import org.springframework.boot.ExitCodeExceptionMapper;
import org.springframework.boot.ExitCodeGenerator;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.event.EventListener;
import org.springframework.core.env.AbstractEnvironment;

import fr.gouv.finances.lombok.util.exception.ApplicationException;
import fr.gouv.finances.lombok.util.exception.ExploitationException;
import fr.gouv.finances.lombok.util.exception.ProgrammationException;

/**
 * <pre>
 * Super classe permettant de lancer des batchs avec SPRING BOOT. 
 * Cette classe est de type CommandLineRunner, elle sera exécutée après le démarrage du contexte. 
 * Cette classe génère des codes erreur en sortie.
 * Elle définit un bean qui remappe les exceptions ingérables Lombok en codes erreur 
 * (voir le bean ExitCodeExceptionMapper)
 * </pre>
 */
public abstract class AbstractBootBatchStarter implements CommandLineRunner, ExitCodeGenerator
{
    /** fatal utilisation d'un marqueur pour reproduire log.fata avec slf4j */
    private static Marker fatal = MarkerFactory.getMarker("FATAL");

    /** Constant : LOG. */
    protected static final Logger LOG = LoggerFactory.getLogger(AbstractBootBatchStarter.class);

    /** exit code. code retour par défaut */
    private int exitCode = 1;

    /** batch context. le contexte sera injecté suite à la construction de ce contexte */
    @Autowired
    ConfigurableApplicationContext batchContext;

    /**
     * Constructeur
     */
    public AbstractBootBatchStarter()
    {
        super();
    }
    
    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see org.springframework.boot.ExitCodeGenerator#getExitCode()
     */
    @Override
    public int getExitCode()
    {
        return exitCode;
    }


    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see org.springframework.boot.CommandLineRunner#run(java.lang.String[])
     */
    @Override
    public void run(String... args) throws Exception
    {
        LOG.info("--> Fin initialisation du contexte du batch avec les profils : " + batchContext.getEnvironment().getActiveProfiles());
        // verification des paramètres passés
        verifyArgs(args);

        String batchToLoad = args[0];
        ServiceBatchCommun traitementBatch;

        LOG.info("--> Lancement de la méthode demarrer du batch :" + batchToLoad);

        try
        {
            traitementBatch = (ServiceBatchCommun) batchContext.getBean(
                batchToLoad.toLowerCase(Locale.FRANCE));

            traitementBatch.setArguments(extraireargumentsbatch(args));

            exitCode = traitementBatch.demarrer();

            if (exitCode == 0)
            {
                LOG.info("Le batch s'est terminé normalement (code 0)");
            }
            else
            {
                LOG.error(fatal, "!!! Le batch s'est terminé avec le code " + exitCode + " (différent de 0) !!!");
                LOG.error(fatal, "!!! Consultez les consignes du guide d'exploitation pour ce code erreur !!!");
            }
        }
        catch (BeansException beansExc)
        {
            LOG.error(fatal, "Le batch " + batchToLoad + " n'existe pas dans le contexte démarré.", beansExc);
            exitCode = 1;

        }
    }
    
    public enum ExceptionLombokErrorCode
    {
        ERR_7701(7701), ERR_7702(7702), ERR_7703(7703);
        private final int codevalue;

        ExceptionLombokErrorCode(int code)
        {
            this.codevalue = code;
        }
        
        public int getCodevalue()
        {
            return codevalue;
        }

        @Override
        public String toString()
        {
            return Integer.toString(codevalue);
        }
    }

    /**
     * methode Exit event : listener pour les codes erreurs.
     *
     * @param event evenement ecouté
     */
    @EventListener
    public void exitEvent(ExitCodeEvent event)
    {
        LOG.info("--> Information sur l'evenement de sortie (ExitCodeEvent) --");
        LOG.info("exit code: " + event.getExitCode());
        LOG.info("timestamp: " + event.getTimestamp());
    }

    /**
     * methode Extraireargumentsbatch
     *
     * @param args --
     * @return map
     */
    private Map<String, String> extraireargumentsbatch(String[] args)
    {

        Map<String, String> argumentsbatch = null;
        if (args.length > 1)
        {
            argumentsbatch = new HashMap<>();
            for (int i = 1; i < args.length; i++)
            {
                argumentsbatch.put(extrairecle(args[i]), extrairevaleur(args[i]));
            }
            LOG.debug("Arguments passés au batch : " + argumentsbatch.toString());
        }
        return argumentsbatch;
    }

    /**
     * methode Extrairecle 
     * 
     * @param arg --
     * @return string
     */
    private String extrairecle(String arg)
    {
        String cle;
        if (arg.indexOf('=') == -1)
        {
            LOG.error(fatal, "Les arguments doivent etre entres sous la forme de cle=valeur");
            throw new ExploitationException("Les arguments doivent etre entres sous la forme de cle=valeur");
        }

        cle = arg.substring(0, arg.indexOf('='));
        LOG.debug("clé lue :" + cle);
        return cle;
    }

    /**
     * methode Extrairevaleur 
     * 
     * @param arg --
     * @return string
     */
    private String extrairevaleur(String arg)
    {
        String valeur;
        if (arg.indexOf('=') == -1)
        {
            LOG.error(fatal, "les arguments doivent etre entres sous la forme de cle=valeur");
            throw new ExploitationException("les arguments doivent etre entres sous la forme de cle=valeur");
        }

        valeur = arg.substring(arg.indexOf('=') + 1);
        LOG.debug("valeur lue :" + valeur);
        return valeur;
    }

  
    /**
     * Modificateur de l attribut active profiles.
     *
     * @param batchProfils le nouveau active profiles
     */
    protected static void setActiveProfiles(String batchProfils)
    {
        // activation des profils sous forme de variable d'environnement.
        Log.debug("Profils actives dans la classe applicative:" + batchProfils);
        System.setProperty(AbstractEnvironment.ACTIVE_PROFILES_PROPERTY_NAME, batchProfils);
    }

    /**
     * methode Verify args : rejette le lancement si aucun paramètre est passé.
     *
     * @param args arguments du batch (le premier étant le nom du batch à lancer)
     */
    private static void verifyArgs(String[] args)
    {
        LOG.info("--> Vérification si le batch comporte au moins un argument qui correspond au nom du batch à executer.");
        if (args == null || args.length == 0)
        {
            LOG.error(fatal, "Pas de nom de batch passé en argument !");
            LOG.error(fatal, "Le premier argument doit correspondre au nom du batch à exécuter.");
            LOG.error(fatal, "Les autres arguments, optionnels, seront transmis en paramètres au batch");
            LOG.error(fatal, "--> Arrêt du batch demandé pour une exception d'exploitation.");
            throw new ExploitationException("Pas de nom de batch passé en argument");
        }
        else
        {
            LOG.info("Le batch comporte au moins un argument.");
        }
    }

    /**
     * methode Exit code exception mapper : mapper pour les exceptions lombok de type non gérable.
     *
     * @return exit code exception mapper
     */
    @Bean
    ExitCodeExceptionMapper exitCodeExceptionMapper()
    {
        return exception -> {
            // plutôt que de modifier les classes d'erreur ingérable de Lombok,
            // on définit ici les codes erreur
            if (exception.getCause() instanceof ProgrammationException)
            {
                return ExceptionLombokErrorCode.ERR_7702.codevalue;
            }
            else if (exception.getCause() instanceof ApplicationException)
            {
                return ExceptionLombokErrorCode.ERR_7701.codevalue;
            }
            else if (exception.getCause() instanceof ExploitationException)
            {
                return ExceptionLombokErrorCode.ERR_7703.codevalue;
            }
            return 1;
        };
    }

   

}