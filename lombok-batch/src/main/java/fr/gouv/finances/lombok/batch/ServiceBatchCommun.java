/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : ServiceBatchCommun.java
 *
 */
package fr.gouv.finances.lombok.batch;

import java.util.Map;

/**
 * Interface à usage interne permettant à la classe BatchStarter d'interagir sur la classe d'implementation des batchs.
 * 
 * @author chouard-cp
 * @version $Revision: 1.2 $ Date: 15 déc. 2009
 */
public interface ServiceBatchCommun
{

    /**
     * methode Demarrer
     * 
     * @return int
     */
    public int demarrer();

    /**
     * Modificateur de l attribut arguments.
     * 
     * @param args le nouveau arguments
     */
    public void setArguments(Map args);

    /**
     * Modificateur de l attribut version application.
     * 
     * @param versionApplication le nouveau version application
     */
    public void setVersionApplication(String versionApplication);

}
