/*
 * Copyright (c) 2017 DGFiP - Tous droits réservés
 * 
*/
// Projet : lombok-batch
/**
 * Documentation du paquet fr.gouv.finances.lombok.batch
 * @author chouard
 * @version 1.0
 */
package fr.gouv.finances.lombok.batch;