/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : ServiceBatchCommunImpl.java
 *
 */
package fr.gouv.finances.lombok.batch;

import java.util.Calendar;
import java.util.Date;
import java.util.Map;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;

/**
 * <pre>
 * Classe abstraite dont doivent heriter toutes les classes d'implementation de batch. 
 * Permet de gérer des messages de debut et de fin standards et d'envoyer un code retour 
 * pour prise en compte dans l'outil d'ordonancement en production. 
 * Le code du batch proprement dit doit être ecrit à partir de la methode traiterBatch() à surcharger.
 * </pre>
 * 
 * @author chouard-cp
 * @version $Revision: 1.5 $ Date: 15 déc. 2009
 */
public abstract class ServiceBatchCommunImpl implements ServiceBatchCommun
{
    protected Logger log = LoggerFactory.getLogger(getClass());

    private String nomBatch;

    private String versionBatch = null;

    @Value("${appli.version}")
    private String versionApplication;

    private Map arguments = null;

    public ServiceBatchCommunImpl()
    {
        super();
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @return int
     * @see fr.gouv.finances.lombok.batch.ServiceBatchCommun#demarrer()
     */
    @Override
    public int demarrer()
    {
        int exitStatus = 1;
        long start = 0;
        StringBuilder unMsg = new StringBuilder(80);
        Date uneDateCourante = null;

        try
        {
            // ----------------------
            // -- Entête section 1 --
            // ----------------------
            unMsg.append("\n============================== SECTION 1 : INFORMATIONS DU BATCH ==============================\n");
            log.info(unMsg.toString());

            // ------------------
            // -- Nom du BATCH --
            // ------------------
            unMsg = new StringBuilder("Nom du BATCH: ");
            unMsg.append(this.getNomBatch());
            log.info(unMsg.toString());

            // -------------------------------------
            // -- Version de l'architecture BATCH --
            // -------------------------------------
            unMsg = new StringBuilder("Version Architecture BATCH: ");
            unMsg.append(BatchStarter.TECH_BATCH_VERSION_ARCHI);
            log.info(unMsg.toString());

            // ------------------------------
            // -- Version du Application --
            // ------------------------------
            unMsg = new StringBuilder("Version Application: ");
            unMsg.append(this.getVersionApplication());
            log.info(unMsg.toString());

            // ---------------------------------------
            // -- Version du BATCH courant (option) --
            // ---------------------------------------
            if (this.getVersionBatch() != null)
            {
                unMsg = new StringBuilder("Numero de version specifique au batch : ");
                unMsg.append(this.getVersionBatch());
                log.info(unMsg.toString());
            }

            // --------------------------------
            // -- Date du lancement du BATCH --
            // --------------------------------
            Calendar cal = new DateTime().toGregorianCalendar();
            start = cal.getTimeInMillis();

            unMsg = new StringBuilder("Début du Batch : ");
            unMsg.append(cal.getTime().toString());
            log.info(unMsg.toString());

            // ----------------------
            // -- Entête section 2 --
            // ----------------------
            unMsg = new StringBuilder("\n============================== SECTION 2 : DEROULEMENT DU BATCH ==============================\n");
            log.info(unMsg.toString());

            // -----------------------------------------------
            // -- Traitement du BATCH: méthode à surcharger --
            // -----------------------------------------------
            this.traiterBatch();

            // ----------------------
            // -- Entête section 3 --
            // ----------------------
            unMsg = new StringBuilder("\n============================== SECTION 3 : FIN DU BATCH ==============================");
            log.info(unMsg.toString());

            // --------------------------------------
            // -- Date de fin d'exécution du BATCH --
            // --------------------------------------
            unMsg = new StringBuilder("Fin du Batch: ");
            uneDateCourante = new DateTime().toDate();
            unMsg.append(uneDateCourante.toString());
            log.info(unMsg.toString());

            // -----------------------------------
            // -- Duree de l'exécution du BATCH --
            // -----------------------------------
            unMsg = new StringBuilder("Duree du Batch: ");
            unMsg.append(new DateTime().getMillis() - start);
            log.info(unMsg + " milli secondes");

            // this.fichierEntreeSortieCourant = null;
            // this.fichierParametresCourant = null;

            // -------------------
            // -- Fin section 3 --
            // -------------------
            unMsg = new StringBuilder("\n=======================================================================\n");
            unMsg.append("=======================================================================\n");
            unMsg.append("=======================================================================");
            log.info(unMsg.toString());
            exitStatus = 0;
        }

        catch (RuntimeException e)
        {
            unMsg = new StringBuilder("Erreur lors de l'execution du batch ");
            unMsg.append(this.getNomBatch());
            log.error(unMsg.toString());
            log.error(e.getMessage());

            // ajout lc pil d'exception
            log.error("Pile d'exception", e);
            log.error("Fin pile d'exception");

            // -----------------------------------
            // -- Duree de l'exécution du BATCH --
            // -----------------------------------
            unMsg = new StringBuilder("Duree du Batch: ");
            unMsg.append(new DateTime().getMillis() - start);
            log.info(unMsg + " milli secondes");
        }
        return exitStatus;
    }

    public Map getArguments()
    {
        return arguments;
    }

    public String getNomBatch()
    {
        return nomBatch;
    }

    public String getVersionApplication()
    {
        return versionApplication;
    }

    public String getVersionBatch()
    {
        return versionBatch;
    }

    @Override
    public void setArguments(Map arguments)
    {
        this.arguments = arguments;
    }

    public void setNomBatch(String nomBatch)
    {
        this.nomBatch = nomBatch;
    }

    @Override
    public void setVersionApplication(String versionApplication)
    {
        this.versionApplication = versionApplication;
    }

    public void setVersionBatch(String versionBatch)
    {
        this.versionBatch = versionBatch;
    }

    /**
     * methode Traiter batch
     */
    public abstract void traiterBatch();

}
