/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 *
 */
package fr.gouv.finances.lombok.batch;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Locale;

import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.core.util.Loader;
import org.apache.logging.log4j.util.LoaderUtil;

import fr.gouv.finances.lombok.parsertexte.integration.GestionnaireImportationFichiers;
import fr.gouv.finances.lombok.parsertexte.integration.GestionnaireObjetExtrait;
import fr.gouv.finances.lombok.parsertexte.integration.IntegrationException;
import fr.gouv.finances.lombok.parsertexte.integration.LigneFichierSource;
import fr.gouv.finances.lombok.parsertexte.parametrage.DefinitionCustomPropertyEditor;
import fr.gouv.finances.lombok.util.exception.ApplicationExceptionTransformateur;
import fr.gouv.finances.lombok.util.exception.ProgrammationException;

/**
 * <pre>
 * Batch utilitaire derivee de ServiceBatchCommun 
 * Usage :
 * 1) Faire heriter la classe de traitement batch de cette classe (voir usage de ServiceBatchCommunOmpl) 
 * 2) Surcharger les méthodes qui seront appelées par le callback. 
 * 3) Principales méthodes à surcharger : 
 * - extractionObjet (obligatoire) : permet de récupérer une ligne parsée sous forme d'objet 
 * - anomalieExtraction : permet de traiter les anomalies si on ne souhaite pas arréter le batch en erreur 
 * - extractionLigneInconnue : idem ci-dessus dans le cas ou le parser ne parvient pas à déterminer le type de la ligne
 * dans les fichiers à structures multiples 
 * - la méthode suivante est appelée lors de l’instanciation des custom property editor déclarés.
 * Elle doit retourner l’instance de l’editor correspondant l’instance de DefinitionCustomPropertyEditor passée en paramètre. 
 * Cette instance possède une propriété « id » donnant l’Id de l’editor spécifié dans le fichier format. 
 * Voir la doc du parseur texte pour plus d'informations {ftp://ftp.bercy.cp/Bureau_3D/Support_forteUDS/J2EE/Documentation%20Parseur%20de%20fichier%20texte.doc} 
 * 4) Surcharger la methode traiterBatch() pour declencher le parse en autoreferençant le batch comme gestionnaire du parsing 
 * final GestionnaireImportationFichiers unGestionnaireImportationFichiers = this.construireUnGestionnaireImportationFichiers(); 
 * final TraitementChargementCodesPostauxImpl batchSelfRef=this;
 * unGestionnaireImportationFichiers.lectureEtDecodageFichier(monFichierTexte,batchSelfRef);
 * </pre>
 * 
 * @author L.Continsouzas
 * @author chouard-cp
 * @author Christophe Breheret-Girardin
 */
public abstract class ServiceBatchCommunParserTexteImpl extends ServiceBatchCommunImpl implements
    GestionnaireObjetExtrait
{

    /** SUFFIX e_ nomstandar d_ fichie r_ forma t_ parsertexte. */
    private static String SUFFIXE_NOMSTANDARD_FICHIER_FORMAT_PARSERTEXTE = "-format.ptx.xml";

    /** SUFFIX e_ nomstandar d_ fichie r_ classe s_ parsertexte. */
    private static String SUFFIXE_NOMSTANDARD_FICHIER_CLASSES_PARSERTEXTE = "-classes.ptx.xml";

    /** texte url fichier param parseur texte classes. */
    private String texteUrlFichierParamParseurTexteClasses = null;

    /** texte url fichier param parseur texte format. */
    private String texteUrlFichierParamParseurTexteFormat = null;

    /** Déclaration du gestionnaire d'importation de fichier */
    private GestionnaireImportationFichiers gestionnaireImportationFichiers;

    /**
     * Constructeur 
     */
    public ServiceBatchCommunParserTexteImpl()
    {
        super();
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param pIdLigne le id ligne
     * @param pintNumLigne
     * @param pAttribut le attribut
     * @param pMessage le message
     * @param pContenuLigne le contenu ligne
     * @return int
     * @see fr.gouv.finances.lombok.parsertexte.integration.GestionnaireObjetExtrait#anomalieExtraction(java.lang.String,
     *      int, java.lang.String, java.lang.String, java.lang.String)
     */
    @Override
    public int anomalieExtraction(String pIdLigne, int pintNumLigne, String pAttribut, String pMessage,
        String pContenuLigne)
    {
        throw new ProgrammationException("Anomalie dans l'intégration du fichier id ligne:" + pIdLigne + "NumLigne:"
            + pintNumLigne + "Attribut:" + pAttribut + "Message" + pMessage + "ContenuLigne" + pContenuLigne);
    }

    /**
     * methode Construire un gestionnaire importation fichiers : --.
     * 
     * @return gestionnaire importation fichiers
     */
    public GestionnaireImportationFichiers construireUnGestionnaireImportationFichiers()
    {
        // Si le gestionnaire n'a pas été injecté, instanciation manuelle
        // (permet la compatibilité avec les anciens projets)
        if (gestionnaireImportationFichiers == null)
        {
            gestionnaireImportationFichiers = new GestionnaireImportationFichiers();
        }
        URL urlFichierParamParseurTexteClasses = null;
        URL urlFichierParamParseurTexteFormat = null;

        // cas ou les URLs des fichiers de paramètres ne sont pas spécifiés explicitement
        if (this.getTexteUrlFichierParamParseurTexteClasses() == null)
        {
            urlFichierParamParseurTexteClasses = this.calculerURLFichierParamClassesParDefaut();
        }
        else
        {
            try
            {
                urlFichierParamParseurTexteClasses = new URL(this.getTexteUrlFichierParamParseurTexteClasses());
            }
            catch (MalformedURLException e)
            {
                throw ApplicationExceptionTransformateur.transformer(e);
            }
        }

        if (this.getTexteUrlFichierParamParseurTexteFormat() == null)
        {
            urlFichierParamParseurTexteFormat = this.calculerURLFichierParamFormatParDefaut();
        }
        else
        {
            try
            {
                urlFichierParamParseurTexteFormat = new URL(this.getTexteUrlFichierParamParseurTexteFormat());
            }
            catch (MalformedURLException e)
            {
                throw ApplicationExceptionTransformateur.transformer(e);
            }
        }

        try
        {
            gestionnaireImportationFichiers.lectureParametrage(urlFichierParamParseurTexteFormat,
                urlFichierParamParseurTexteClasses);
        }
        catch (IntegrationException e)
        {
            throw ApplicationExceptionTransformateur.transformer(e);
        }

        return gestionnaireImportationFichiers;
    }

    /**
     * methode Construire un gestionnaire importation fichiers : --.
     * 
     * @param pEncoding
     * @return gestionnaire importation fichiers
     */
    public GestionnaireImportationFichiers construireUnGestionnaireImportationFichiers(String pEncoding)
    {
        GestionnaireImportationFichiers gestionnaireImportation = new GestionnaireImportationFichiers(pEncoding);
        URL urlFichierParamParseurTexteClasses = null;
        URL urlFichierParamParseurTexteFormat = null;

        // cas ou les url des fichiers de paramètres ne sont spécifiés
        // explicitement
        if (this.getTexteUrlFichierParamParseurTexteClasses() == null)
        {
            urlFichierParamParseurTexteClasses = this.calculerURLFichierParamClassesParDefaut();
        }
        else
        {
            try
            {
                urlFichierParamParseurTexteClasses = new URL(this.getTexteUrlFichierParamParseurTexteClasses());
            }
            catch (MalformedURLException e)
            {
                throw ApplicationExceptionTransformateur.transformer(e);
            }
        }

        if (this.getTexteUrlFichierParamParseurTexteFormat() == null)
        {
            urlFichierParamParseurTexteFormat = this.calculerURLFichierParamFormatParDefaut();
        }
        else
        {
            try
            {
                urlFichierParamParseurTexteFormat = new URL(this.getTexteUrlFichierParamParseurTexteFormat());
            }
            catch (MalformedURLException e)
            {
                throw ApplicationExceptionTransformateur.transformer(e);
            }
        }

        try
        {
            gestionnaireImportation.lectureParametrage(urlFichierParamParseurTexteFormat,
                urlFichierParamParseurTexteClasses);
        }
        catch (IntegrationException e)
        {
            throw ApplicationExceptionTransformateur.transformer(e);
        }

        return gestionnaireImportation;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param pLigne le ligne
     * @param pintNumLigne
     * @return string
     * @see fr.gouv.finances.lombok.parsertexte.integration.GestionnaireObjetExtrait#extractionLigneInconnue(fr.gouv.finances.lombok.parsertexte.integration.LigneFichierSource,
     *      int)
     */
    @Override
    public String extractionLigneInconnue(LigneFichierSource pLigne, int pintNumLigne)
    {
        throw new ProgrammationException("Ligne Inconnue :" + pLigne + "NumLigne:" + pintNumLigne);
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param pPresenceAnomalieDansFichier le presence anomalie dans fichier
     * @see fr.gouv.finances.lombok.parsertexte.integration.GestionnaireObjetExtrait#finTraitementFichier(boolean)
     */
    @Override
    public void finTraitementFichier(boolean pPresenceAnomalieDansFichier)
    {

    }

    /**
     * Accesseur de l attribut texte url fichier param parseur texte classes.
     * 
     * @return texte url fichier param parseur texte classes
     */
    public String getTexteUrlFichierParamParseurTexteClasses()
    {
        return texteUrlFichierParamParseurTexteClasses;
    }

    /**
     * Accesseur de l attribut texte url fichier param parseur texte format.
     * 
     * @return texte url fichier param parseur texte format
     */
    public String getTexteUrlFichierParamParseurTexteFormat()
    {
        return texteUrlFichierParamParseurTexteFormat;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param pDefinition le definition
     * @return object
     * @see fr.gouv.finances.lombok.parsertexte.integration.GestionnaireObjetExtrait#instanciationCustomPropertyEditor(fr.gouv.finances.lombok.parsertexte.parametrage.DefinitionCustomPropertyEditor)
     */
    @Override
    public Object instanciationCustomPropertyEditor(DefinitionCustomPropertyEditor pDefinition)
    {
        // Auto-generated method stub
        return null;
    }

    /**
     * Modificateur de l attribut texte url fichier param parseur texte classes.
     * 
     * @param urlFichierParamParseurTexteClasses le nouveau texte url fichier param parseur texte classes
     */
    public void setTexteUrlFichierParamParseurTexteClasses(String urlFichierParamParseurTexteClasses)
    {
        this.texteUrlFichierParamParseurTexteClasses = urlFichierParamParseurTexteClasses;
    }

    /**
     * Modificateur de l attribut texte url fichier param parseur texte format.
     * 
     * @param urlFichierParamParseurTexteFormat le nouveau texte url fichier param parseur texte format
     */
    public void setTexteUrlFichierParamParseurTexteFormat(String urlFichierParamParseurTexteFormat)
    {
        this.texteUrlFichierParamParseurTexteFormat = urlFichierParamParseurTexteFormat;
    }

    /**
     * Mutateur de gestionnaireImportationFichiers
     *
     * @param gestionnaireImportationFichiers gestionnaireImportationFichiers
     */
    @Override
    public void setGestionnaireImportationFichiers(GestionnaireImportationFichiers gestionnaireImportationFichiers)
    {
        this.gestionnaireImportationFichiers = gestionnaireImportationFichiers;
    }

    /**
     * methode Calculer prefixe defaut url fichiers param parseur : --.
     * 
     * @return string
     */
    private String calculerPrefixeDefautURLFichiersParamParseur()
    {

        // recherche des url de fichiers de config dans le classpath si non spécifiés

        String prefixeNomFichierConf = this.getClass().getName();
        String prefixeNomFichiersConfSansImpl;

        if (prefixeNomFichierConf.toLowerCase(Locale.FRANCE).endsWith("impl"))
        {
            prefixeNomFichiersConfSansImpl = prefixeNomFichierConf.substring(0, prefixeNomFichierConf.length() - 4);
        }
        else
        {
            prefixeNomFichiersConfSansImpl = prefixeNomFichierConf;
        }

        return StringUtils.replace(prefixeNomFichiersConfSansImpl, ".", "/").toLowerCase(Locale.FRANCE);
    }

    /**
     * methode Calculer url fichier param classes par defaut : --.
     * 
     * @return uRL
     */
    private URL calculerURLFichierParamClassesParDefaut()
    {
        String nomStandardFichierClasses =
            this.calculerPrefixeDefautURLFichiersParamParseur() + SUFFIXE_NOMSTANDARD_FICHIER_CLASSES_PARSERTEXTE;
        return Loader.getResource(nomStandardFichierClasses, LoaderUtil.getThreadContextClassLoader());
    }

    /**
     * methode Calculer url fichier param format par defaut : --.
     * 
     * @return uRL
     */
    private URL calculerURLFichierParamFormatParDefaut()
    {
        String nomStandardFichierFormat =
            this.calculerPrefixeDefautURLFichiersParamParseur() + SUFFIXE_NOMSTANDARD_FICHIER_FORMAT_PARSERTEXTE;
        return Loader.getResource(nomStandardFichierFormat, LoaderUtil.getThreadContextClassLoader());
    }

}
