/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : ValidateurXMLXSDAvecFabriqueParseur.java
 *
 */
package fr.gouv.finances.lombok.verificationxmlxsd.batch;

import java.io.IOException;
import java.util.ArrayList;

import javax.xml.XMLConstants;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.XMLReader;

import fr.gouv.finances.lombok.util.xml.FabriqueDeParseurSAXAvecOptionsCommunes;

/**
 * Validateur XML XSD avec SAX. Avantage par rapport à DOM : est plus rapide.
 * 
 * @author amleplatinec-cp
 * @version $Revision: 1.2 $ Date: 15 déc. 2009
 */
public class ValidateurXMLXSDAvecFabriqueParseur implements ErrorHandler
{

    /** Constant : CONTINUE_AFTER_FATAL_ERROR. */
    protected static final String CONTINUE_AFTER_FATAL_ERROR =
        "http://apache.org/xml/features/continue-after-fatal-error";

    /** Constant : JAXP_SCHEMA_SOURCE. */
    protected static final String JAXP_SCHEMA_SOURCE = "http://java.sun.com/xml/jaxp/properties/schemaSource";

    /** Constant : JAXP_SCHEMA_LANGUAGE. */
    private static final String JAXP_SCHEMA_LANGUAGE = "http://java.sun.com/xml/jaxp/properties/schemaLanguage";

    /** Constant : W3C_XML_SCHEMA. */
    private static final String W3C_XML_SCHEMA = "http://www.w3.org/2001/XMLSchema";

    /** Memo erreur validation. */
    public boolean validationError = false;

    /** sax parse exception list. */
    public ArrayList saxParseExceptionList = new ArrayList();

    /** La réponse de la validation. */
    private String response = null;

    /** Le logger. */
    private final Log log = LogFactory.getLog(this.getClass());

    /**
     * Constructeur
     */
    public ValidateurXMLXSDAvecFabriqueParseur()
    {
        super();
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param arg0
     * @throws SAXException the SAX exception
     * @see org.xml.sax.ErrorHandler#error(org.xml.sax.SAXParseException)
     */
    @Override
    public void error(SAXParseException arg0) throws SAXException
    {
        validationError = true;
        saxParseExceptionList.add(arg0);
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param arg0
     * @throws SAXException the SAX exception
     * @see org.xml.sax.ErrorHandler#fatalError(org.xml.sax.SAXParseException)
     */
    @Override
    public void fatalError(SAXParseException arg0) throws SAXException
    {
        validationError = true;
        saxParseExceptionList.add(arg0);
    }

    /**
     * on passe juste les adresses du xml et du xsd.
     * 
     * @param xmlFile --
     * @param xsdFile --
     * @return the string
     */
    public String validerXMLParRapportXSD(String xmlFile, String xsdFile)
    {

        try
        {
            XMLReader parser = FabriqueDeParseurSAXAvecOptionsCommunes.createXMLReader();
            parser.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
            parser.setErrorHandler(this);
            parser.setProperty(JAXP_SCHEMA_LANGUAGE, W3C_XML_SCHEMA);
            parser.setFeature(CONTINUE_AFTER_FATAL_ERROR, true);
            parser.setProperty(JAXP_SCHEMA_SOURCE, xsdFile);

            parser.parse(xmlFile);
            if (validationError == true)
            {
                response = calculerReponse(saxParseExceptionList);
                return response;
            }
            else
            {
                return null;
            }
        }

        catch (SAXException exc)
        {
            log.debug("SAXException détectée ", exc);
            if (this.validationError)
            {
                response = calculerReponse(this.saxParseExceptionList);
            }
            StringBuilder sbuild;
            if (response != null && response.compareTo("") != 0)
            {
                sbuild = new StringBuilder(response);
            }
            else
            {
                sbuild = new StringBuilder("");
            }
            sbuild.append("\n   *** ").append(exc.getMessage());
            return sbuild.toString();

        }
        catch (IOException exception)
        {
            log.debug("Exception détectée", exception);
            if (this.validationError)
            {
                response = calculerReponse(this.saxParseExceptionList);
            }
            StringBuilder sbuild;
            if (response != null && response.compareTo("") != 0)
            {
                sbuild = new StringBuilder(response);
            }
            else
            {
                sbuild = new StringBuilder("");
            }
            sbuild.append("\n   *** ").append(exception.getMessage());
            return sbuild.toString();
        }

    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param arg0
     * @throws SAXException the SAX exception
     * @see org.xml.sax.ErrorHandler#warning(org.xml.sax.SAXParseException)
     */
    @Override
    public void warning(SAXParseException arg0) throws SAXException
    {
        validationError = true;
        saxParseExceptionList.add(arg0);
    }

    /**
     * renvoie toutes les erreurs sous forme de string.
     * 
     * @param saxParseExceptionList --
     * @return the string
     */
    private String calculerReponse(ArrayList saxParseExceptionList)
    {
        StringBuilder sbuild = new StringBuilder();
        sbuild.append("les erreurs suivantes ont ete detectees");
        for (int i = 0; i < saxParseExceptionList.size(); i++)
        {
            SAXParseException xcpt = (SAXParseException) saxParseExceptionList.get(i);

            sbuild.append("\n  ").append(xcpt.getLineNumber());
            sbuild.append(':').append(xcpt.getColumnNumber());
            sbuild.append(" *** ").append(xcpt.getMessage());
        }
        return sbuild.toString();
    }

}
