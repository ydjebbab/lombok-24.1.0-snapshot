/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : BatchStarter.java
 *
 */
package fr.gouv.finances.lombok.batch;

import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import fr.gouv.finances.lombok.util.cle.ApplicationPropertiesUtil;
import fr.gouv.finances.lombok.util.exception.ExploitationException;
import fr.gouv.finances.lombok.util.exception.ProgrammationException;

/**
 * <pre>
 * Classe servant au lancement de l'ensemble des batchs.
 * On doit passer en paramètre le nom du batch qui permettra de charger le contexte et le bean spring correspondant 
 * si les règles de nommage ont bien été respectées : 
 * - le contexte doit s'appeler nomdubatchcontext 
 * - Le bean instanciant la classe implementant le batch doit s'appeler nomdubatchimpl 
 * La méthode demarrer() est ensuite appelee sur le bean trouvé.
 * La classe d'implémentation des batchs applicatifs doit heriter de ServiceBatchCommunImpl qui porte la methode demarrer().
 * </pre>
 *
 * @author chouard-cp
 * @version $Revision: 1.3 $ Date: 14 déc. 2009
 * @see fr.gouv.finances.lombok.batch.ServiceBatchCommun
 * @see fr.gouv.finances.lombok.batch.ServiceBatchCommunImpl
 */
@Deprecated
public final class BatchStarter
{
    public static final String TECH_BATCH_VERSION_ARCHI = "24";

    private static Map<Object, Object> argumentsbatch = null;

    private static ServiceBatchCommun serviceBatch = null;

    private static final String BATCH_FACTORY = "conf/batch/batchFactory.xml";

    private static final Logger LOG = LoggerFactory.getLogger(BatchStarter.class);

    private final SystemExitWrapper systemExit;

    /**
     * Constructeur
     *
     * @param systemExit constructeur avec le wrapper systemExit (qui sera mocké pour les tests)
     */
    public BatchStarter(SystemExitWrapper systemExit)
    {
        this.systemExit = systemExit;
    }

    /**
     * The main method.
     * 
     * @param args --
     */
    public static void main(String[] args)
    {
        SystemExitWrapper aSystemExit = new SystemExitWrapper();
        BatchStarter batchStarter = new BatchStarter(aSystemExit);
        batchStarter.executeAndExit(args);
    }

    /**
     * Méthode principale exécutant le batch. La sortie via System.exit est wrappée dans la classe SystemExitWrapper
     *
     * @param args un argument nécessaire: le nom du batch (sans suffixe impl ou context)
     */
    public void executeAndExit(String[] args)
    {
        String batchBeanNameToLoad;
        String contextFactory;

        int exitStatus = 0;
        ClassPathXmlApplicationContext context = null;

        try
        {
            // log4jurl = "classpath:log4j2.properties";
            // configurerLog4j(log4jurl);

            String implSuffixe = "impl";
            String contexteSuffixe = "context";

            if (args != null && args.length >= 1 && args[0] != null)
            {
                batchBeanNameToLoad = args[0] + implSuffixe;
                contextFactory = args[0] + contexteSuffixe;
            }
            else
            {
                LOG.error("Pas de nom de batch passé en argument!");
                LOG.error("On doit passer en premier argument le nom du batch");
                LOG.error("Le nom du bean utilisé sera nom-du-batch + " + implSuffixe);
                LOG.error("le nom de contexte utilisé sera nom-du-batch + " + contexteSuffixe);
                LOG.error("Les autres arguments sontpassés en paramètre au batch");
                LOG.error("Arrêt.");
                throw new ProgrammationException("Pas de nom de batch passé en argument");
            }

            extraireargumentsbatch(args);

            LOG.info("Lancement du batch :" + batchBeanNameToLoad);

            afficherProprietesSystemeDansLaLogSiDebug();
            Properties prop = new Properties();
            prop.load(BatchStarter.class.getClassLoader().getResourceAsStream("application.properties"));
            String profilesActives = prop.getProperty("batch.profils.actifs");
            LOG.info("profils activés (application.properties, clé batch.profils.actifs): " + profilesActives);
            System.setProperty("spring.profiles.active", profilesActives);
            context = new ClassPathXmlApplicationContext(BATCH_FACTORY);

            ClassPathXmlApplicationContext contextBatch = (ClassPathXmlApplicationContext) context.getBean(
                contextFactory.toLowerCase(Locale.FRANCE));
            serviceBatch = (ServiceBatchCommun) contextBatch.getBean(
                batchBeanNameToLoad.toLowerCase(Locale.FRANCE));
            passerNumeroVersionApplicationAuServiceBatch();
            passerArgumentsAuServiceBatch(argumentsbatch);

            exitStatus = serviceBatch.demarrer();
            LOG.info("Fin du batch :" + batchBeanNameToLoad);
        }
        catch (RuntimeException | IOException exc)
        {
            LOG.error("Affichage de la pile d'exception reçue au plus bas niveau", exc);
            exitStatus = 1;
        }
        finally
        {
            if (context != null && context.isActive())
            {
                context.close();
            }
        }

        if (exitStatus == 0)
        {
            LOG.info("Le batch s'est terminé normalement (code 0)");
        }
        else
        {
            LOG.error("!!! Le batch ne s'est pas terminé normalement (code différent de 0) !!!");
            LOG.error("!!! Consultez les consignes de relance sur le guide d'exploitation !!!");
        }

        systemExit.exit(exitStatus);
    }

    /**
     * methode Afficher proprietes systeme dans la log si debug
     */
    private static void afficherProprietesSystemeDansLaLogSiDebug()
    {
        if (LOG.isDebugEnabled())
        {
            Properties props = System.getProperties();
            Enumeration<?> enumKeys = props.keys();
            Object obj;
            Object prop;

            while (enumKeys.hasMoreElements())
            {
                obj = enumKeys.nextElement();
                prop = props.get(obj);
                LOG.debug(obj.toString() + "=" + prop.toString());
            }
        }
    }

    /**
     * methode extraireargumentsbatch
     * 
     * @param args --
     */
    private static void extraireargumentsbatch(String[] args)
    {

        if (args.length > 1)
        {
            argumentsbatch = new HashMap<Object, Object>();
            for (int i = 1; i < args.length; i++)
            {
                argumentsbatch.put(extrairecle(args[i]), extrairevaleur(args[i]));
            }
            LOG.debug("Arguments passés au batch : " + argumentsbatch.toString());
        }
    }

    /**
     * methode extrairecle
     * 
     * @param arg --
     * @return string
     */
    private static String extrairecle(String arg)
    {
        String cle = null;
        if (arg.indexOf('=') == -1)
        {
            LOG.error("les arguments doivent etre entres sous la forme de cle=valeur");
            throw new ExploitationException("les arguments doivent etre entres sous la forme de cle=valeur");
        }

        cle = arg.substring(0, arg.indexOf('='));
        LOG.debug("clé lue :" + cle);
        return cle;
    }

    /**
     * methode extrairevaleur
     * 
     * @param arg --
     * @return string
     */
    private static String extrairevaleur(String arg)
    {
        String valeur = null;
        if (arg.indexOf('=') == -1)
        {
            LOG.error("les arguments doivent etre entres sous la forme de cle=valeur");
            throw new ExploitationException("les arguments doivent etre entres sous la forme de cle=valeur");
        }

        valeur = arg.substring(arg.indexOf('=') + 1);
        LOG.debug("valeur lue :" + valeur);
        return valeur;
    }

    /**
     * methode Passer arguments au service batch
     * 
     * @param args --
     */
    private static void passerArgumentsAuServiceBatch(Map<Object, Object> args)
    {
        // recuperation du numero de version de l'appli pour mise dans les logs
        serviceBatch.setArguments(args);
        LOG.debug("Arguments lu : " + args);
    }

    /**
     * methode Passer numero version application au service batch
     */
    private static void passerNumeroVersionApplicationAuServiceBatch()
    {
        String versionApplication = ApplicationPropertiesUtil.getProperty("appli.version");
        serviceBatch.setVersionApplication(versionApplication);
        LOG.debug("Numero de version (appli.version) lu : " + versionApplication);
    }

}
