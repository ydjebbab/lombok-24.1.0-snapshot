/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : ServiceBatchCommunParserSaxImpl.java
 *
 */
package fr.gouv.finances.lombok.batch;

import java.io.IOException;

import javax.xml.XMLConstants;

import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.ErrorHandler;
import org.xml.sax.InputSource;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.XMLReader;

import fr.gouv.finances.lombok.util.exception.ProgrammationException;
import fr.gouv.finances.lombok.util.xml.FabriqueDeParseurSAXAvecOptionsCommunes;

/**
 * <pre>
 * Classe batch utilitaire derivee de ServiceBatchCommun 
 * Usage : 
 * 1) Faire heriter la classe de traitement batch de cette classe (voir usage de ServiceBatchCommunOmpl) 
 * 2) Surcharger les méthodes qui seront appelées par le callback. 
 * Faites appel à la super méthode si vous voulez bénéficier de la possibilité d'activer des logs 
 * 3) Principales méthodes à surcharger : 
 * startDocument/endDocument : pour initier des actions en début/fin de document 
 * startElement/endElement : pour initier des actions en debut/fin d'element (ie balise ouvrante / fermante) pour récupérer 
 * les valeurs d'attributs dans les balises Voir la doc de l'api SAX pour l'usage des autre méthodes 
 * 4) Surcharger la methode traiterBatch()
 * A l'intérieur appeler la methode parserDocument() dans la classe fille pour déclencher le parse.
 * Vous pouvez ajouter des attributs à la classe fille de manière à stocker des éléments intermédiaires : 
 * par exemple stocker tous les éléments lus entre une balise ouvrante et une balise fermante de manière à pouvoir 
 * constituer un objet complet à envoyer vers la base dès que l'on rencontre la balise fermante.
 * </pre>
 * 
 * @author L.Continsouzas
 * @author chouard-cp
 * @version $Revision: 1.2 $ Date: 15 déc. 2009
 */
public abstract class ServiceBatchCommunParserSaxImpl extends ServiceBatchCommunImpl implements ContentHandler, ErrorHandler
{
    /**
     * Constructeur
     */
    public ServiceBatchCommunParserSaxImpl()
    {
        super();
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param chh
     * @param offset
     * @param length
     * @throws SAXException the SAX exception
     * @see org.xml.sax.ContentHandler#characters(char[], int, int)
     */
    @Override
    public void characters(char[] chh, int offset, int length) throws SAXException
    {
        log.trace("characters (ch=" + String.valueOf(chh) + ",offset=" + offset + ",length=" + length + ")");
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @throws SAXException the SAX exception
     * @see org.xml.sax.ContentHandler#endDocument()
     */
    @Override
    public void endDocument() throws SAXException
    {
        log.trace("endDocument");
    }

    /**
     * End element.
     * 
     * @param uri --
     * @param localName --
     * @param qname --
     * @throws SAXException the SAX exception
     */
    @Override
    public void endElement(String uri, String localName, String qname) throws SAXException
    {
        log.trace("endElement (uri=" + uri + ",localName=" + localName + ",qname=" + qname + ")");
    }

    /**
     * End prefix mapping.
     * 
     * @param prefix --
     * @throws SAXException the SAX exception
     */
    @Override
    public void endPrefixMapping(String prefix) throws SAXException
    {
        log.trace("startPrefixMapping (prefix=" + prefix + ")");
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param exc
     * @throws SAXException the SAX exception
     * @see org.xml.sax.ErrorHandler#error(org.xml.sax.SAXParseException)
     */
    @Override
    public void error(SAXParseException exc) throws SAXException
    {
        log.error("SAX Parser Error : " + exc.getMessage());
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param exc
     * @throws SAXException the SAX exception
     * @see org.xml.sax.ErrorHandler#fatalError(org.xml.sax.SAXParseException)
     */
    @Override
    public void fatalError(SAXParseException exc) throws SAXException
    {
        log.error("SAX Fatal Error", exc);
        throw new ProgrammationException(exc);
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param chh
     * @param offset
     * @param length
     * @throws SAXException the SAX exception
     * @see org.xml.sax.ContentHandler#ignorableWhitespace(char[], int, int)
     */
    @Override
    public void ignorableWhitespace(char[] chh, int offset, int length) throws SAXException
    {
        log.trace("ignorableWhitespace (ch=" + String.valueOf(chh) + ",offset=" + offset + ",length=" + length + ")");
    }

    /**
     * Methode à appeler pour declencher le parse - Instancie un parseur SAX validant - Demarre le parse (le callback
     * est ensuite effectue sur la classe fille si les methodes ont été surchargées).
     * 
     * @param source fichier xml à parser
     */

    public synchronized void parserDocument(InputSource source)
    {

        // set handlers
        XMLReader parser = FabriqueDeParseurSAXAvecOptionsCommunes.createXMLReader();
        parser.setErrorHandler(this);
        parser.setContentHandler(this);

        try
        {
            parser.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
            parser.parse(source);
        }
        catch (SAXParseException e)
        {
            throw new ProgrammationException(e);
        }
        catch (IOException e)
        {
            new ProgrammationException(e);
        }
        catch (SAXException e)
        {
            new ProgrammationException(e);
        }
    }

    /**
     * Processing instruction.
     * 
     * @param target --
     * @param data --
     * @throws SAXException the SAX exception
     */
    @Override
    public void processingInstruction(String target, String data) throws SAXException
    {
        log.trace("processingInstruction (target=" + target + ", data=" + data + ")");
    }

    // endPrefixMapping(String)

    /**
     * Set document locator.
     * 
     * @param locator --
     */
    @Override
    public void setDocumentLocator(Locator locator)
    {
        log.trace("setDocumentLocator(locator=" + locator + ")");
    }

    // startElement(String,String,String,Attributes)

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param name
     * @throws SAXException the SAX exception
     * @see org.xml.sax.ContentHandler#skippedEntity(java.lang.String)
     */
    @Override
    public void skippedEntity(String name) throws SAXException
    {
        log.trace("skippedEntity (name=" + name + ")");
    }

    /**
     * Start document.
     * 
     * @throws SAXException the SAX exception
     */
    @Override
    public void startDocument() throws SAXException
    {
        log.trace("startDocument");
    }

    /*
     * skippedEntity(String) ErrorHandler methods
     */

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param uri
     * @param localName
     * @param qname
     * @param attributes
     * @throws SAXException the SAX exception
     * @see org.xml.sax.ContentHandler#startElement(java.lang.String, java.lang.String, java.lang.String,
     *      org.xml.sax.Attributes)
     */
    @Override
    public void startElement(String uri, String localName, String qname, Attributes attributes) throws SAXException
    {
        log.trace("startElement (uri=" + uri + ",localName=" + localName + ",qname=" + qname + ")");
        log.trace(">liste attributs associzes a l'element");
        if (attributes == null)
        {
            log.trace("pas d'attribut");
        }
        else
        {
            int length = attributes.getLength();
            for (int i = 0; i < length; i++)
            {
                log.trace(">  localname=" + attributes.getLocalName(i));
                log.trace(">  attrqName=" + attributes.getQName(i));
                log.trace(">  attrqURI=" + attributes.getURI(i));
                log.trace(">  attrqType=" + attributes.getType(i));
                log.trace(">  attrqValue=" + attributes.getValue(i));
                log.trace("---------------");
            }
        }
        log.trace(">fin liste attributs associes a l'element");
    }

    // warning(SAXParseException)

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param prefix
     * @param uri
     * @throws SAXException the SAX exception
     * @see org.xml.sax.ContentHandler#startPrefixMapping(java.lang.String, java.lang.String)
     */
    @Override
    public void startPrefixMapping(String prefix, String uri) throws SAXException
    {
        log.trace("startPrefixMapping (prefix=" + prefix + ",uri=" + uri + ")");
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param exc
     * @throws SAXException the SAX exception
     * @see org.xml.sax.ErrorHandler#warning(org.xml.sax.SAXParseException)
     */
    @Override
    public void warning(SAXParseException exc) throws SAXException
    {
        log.warn("SAX Parser Warning : " + exc.getMessage());
    }

}
