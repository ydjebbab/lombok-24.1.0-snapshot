/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 *
 */
package fr.gouv.finances.lombok.verificationxmlxsd.batch;

import java.io.IOException;
import java.util.ArrayList;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.DefaultHandler;

/**
 * Utilitaire de validation XSD.
 * 
 * @author chouard-cp
 */
public class XSDValidator
{

    /**
     * Bean de stockage d'information utilisé dans la validation XSD.
     *
     * @author amleplatinec
     */
    private class Validator extends DefaultHandler
    {

        /**
         * Constructeur.
         */
        public Validator()
        {
            super();
        }

        /** Mémorisation des erreurs de validation. */
        public boolean validationError = false;

        /** Mémorisation des exception SAX. */
        public ArrayList<SAXParseException> saxParseExceptionList = new ArrayList<>();

        /**
         * (methode de remplacement) {@inheritDoc}.
         * 
         * @param exception
         * @see org.xml.sax.helpers.DefaultHandler#error(org.xml.sax.SAXParseException)
         */
        @Override
        public void error(final SAXParseException exception)
        {
            validationError = true;
            saxParseExceptionList.add(exception);
        }

        /**
         * (methode de remplacement) {@inheritDoc}.
         * 
         * @param exception
         * @see org.xml.sax.helpers.DefaultHandler#fatalError(org.xml.sax.SAXParseException)
         */
        @Override
        public void fatalError(final SAXParseException exception)
        {
            validationError = true;
            saxParseExceptionList.add(exception);
        }

        /**
         * (methode de remplacement) {@inheritDoc}.
         * 
         * @param exception
         * @see org.xml.sax.helpers.DefaultHandler#warning(org.xml.sax.SAXParseException)
         */
        @Override
        public void warning(final SAXParseException exception)
        {
            validationError = true;
            saxParseExceptionList.add(exception);
        }
    }

    /** Constante : JAXP_SCHEMA_LANGUAGE. */
    private static final String JAXP_SCHEMA_LANGUAGE = "http://java.sun.com/xml/jaxp/properties/schemaLanguage";

    /** Constante : W3C_XML_SCHEMA. */
    private static final String W3C_XML_SCHEMA = "http://www.w3.org/2001/XMLSchema";

    /** Constante : JAXP_SCHEMA_SOURCE. */
    private static final String JAXP_SCHEMA_SOURCE = "http://java.sun.com/xml/jaxp/properties/schemaSource";

    /** Constante : CONTINUE_AFTER_FATAL_ERROR. */
    private static final String CONTINUE_AFTER_FATAL_ERROR =
        "http://apache.org/xml/features/continue-after-fatal-error";

    /** Mémorisation des erreurs de validation. */
    public boolean validationError = false;

    /** La réponse de la validation. */
    private String response = null;

    /** Initialisation de la journalisation. */
    private final Log log = LogFactory.getLog(this.getClass());

    /** Résultat de validation *. */
    private boolean isValid = false;

    /**
     * Verifie si valide.
     * 
     * @return true si valide, false sinon
     */
    public boolean isValid()
    {
        return isValid;
    }

    /**
     * on passe juste les noms de fichiers en entree méthode principale qui fait la validation.
     * 
     * @param xmlFile (nom du fichier)
     * @param xsdFile (nom du fichier)
     * @param recuperertoutesleserreurs --
     * @return XMLParseError[]
     */
    public String validerXMLParRapportXSD(String xmlFile, String xsdFile, String recuperertoutesleserreurs)
    {
        Validator handler = new Validator();

        try
        {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            factory.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
            factory.setNamespaceAware(true);
            factory.setValidating(true);
            factory.setAttribute(JAXP_SCHEMA_LANGUAGE, W3C_XML_SCHEMA);
            factory.setAttribute(JAXP_SCHEMA_SOURCE, xsdFile);
            if (recuperertoutesleserreurs != null
                && recuperertoutesleserreurs.compareTo("recuperertoutesleserreurs") == 0)
            {
                factory.setAttribute(CONTINUE_AFTER_FATAL_ERROR, Boolean.TRUE);
            }

            DocumentBuilder builder = factory.newDocumentBuilder();

            builder.setErrorHandler(handler);
            builder.parse(xmlFile);

            isValid = !handler.validationError;
            if (handler.validationError)
            {
                response = calculerReponse(handler.saxParseExceptionList);
                return response;
            }
        }
        catch (ParserConfigurationException | IOException | SAXException exception)
        {
            log.debug("ParserConfigurationException détectée", exception);

            if (handler.validationError)
            {
                response = calculerReponse(handler.saxParseExceptionList);
            }
            StringBuilder sbuild;
            if (response != null && response.compareTo("") != 0)
            {
                sbuild = new StringBuilder(response);
            }
            else
            {
                sbuild = new StringBuilder("");
            }
            sbuild.append("\n   *** ").append(exception.getMessage());

        }

        return response;
    }

    /**
     * Méthode de calcul de la réponse.
     * 
     * @param saxParseExceptionList --
     * @return string
     */
    private String calculerReponse(ArrayList<SAXParseException> saxParseExceptionList)
    {
        StringBuilder sbuild = new StringBuilder();
        sbuild.append("les erreurs suivantes ont ete detectees");
        for (int i = 0; i < saxParseExceptionList.size(); i++)
        {
            SAXParseException xcpt = (SAXParseException) saxParseExceptionList.get(i);
            sbuild.append("\n  ").append(xcpt.getLineNumber());
            sbuild.append(':').append(xcpt.getColumnNumber());
            sbuild.append(" *** ").append(xcpt.getMessage());
        }
        return sbuild.toString();
    }

}
