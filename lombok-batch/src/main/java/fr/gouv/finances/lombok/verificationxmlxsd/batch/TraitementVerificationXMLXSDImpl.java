/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : TraitementVerificationXMLXSDImpl.java
 *
 */
package fr.gouv.finances.lombok.verificationxmlxsd.batch;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.gouv.finances.lombok.batch.ServiceBatchCommunImpl;

/**
 * Class TraitementVerificationXMLXSDImpl --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.2 $ Date: 11 déc. 2009
 */
public class TraitementVerificationXMLXSDImpl extends ServiceBatchCommunImpl
{
    protected Logger log = LoggerFactory.getLogger(getClass());

    private String urlfichierxml;

    private String urlfichierxsd;

    private String urlfichiersortie;

    private String fichiersortieavecurl;

    private String recuperertoutesleserreurs;

    public TraitementVerificationXMLXSDImpl()
    {
        super();
    }

    public String getFichiersortieavecurl()
    {
        return fichiersortieavecurl;
    }

    public String getRecuperertoutesleserreurs()
    {
        return recuperertoutesleserreurs;
    }

    public String getUrlfichiersortie()
    {
        return urlfichiersortie;
    }

    public String getUrlfichierxml()
    {
        return urlfichierxml;
    }

    public String getUrlfichierxsd()
    {
        return urlfichierxsd;
    }

    public void setFichiersortieavecurl(String fichiersortieavecurl)
    {
        this.fichiersortieavecurl = fichiersortieavecurl;
    }

    public void setRecuperertoutesleserreurs(String recuperertoutesleserreurs)
    {
        this.recuperertoutesleserreurs = recuperertoutesleserreurs;
    }

    public void setUrlfichiersortie(String urlfichiersortie)
    {
        this.urlfichiersortie = urlfichiersortie;
    }

    public void setUrlfichierxml(String urlfichierxml)
    {
        this.urlfichierxml = urlfichierxml;
    }

    public void setUrlfichierxsd(String urlfichierxsd)
    {
        this.urlfichierxsd = urlfichierxsd;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @see fr.gouv.finances.lombok.batch.ServiceBatchCommunImpl#traiterBatch()
     */
    @Override
    public void traiterBatch()
    {
        Map<?, ?> lesArgumentsPasses = getArguments();
        XSDValidator xsdValidator = new XSDValidator();

        // on va regarder pour chaque clé du fichier propriété si clé avec
        // meme nom existe dans les arguments passés
        if (lesArgumentsPasses != null)
        {
            if (lesArgumentsPasses.containsKey("urlfichierxml"))
            {
                setUrlfichierxml((String) lesArgumentsPasses.get("urlfichierxml"));
                log.debug("valeur Urlfichierxml:" + (String) lesArgumentsPasses.get("urlfichierxml"));
            }
        }

        if (lesArgumentsPasses != null)
        {
            if (lesArgumentsPasses.containsKey("urlfichierxsd"))
            {
                setUrlfichierxsd((String) lesArgumentsPasses.get("urlfichierxsd"));
                log.debug("valeur Urlfichierxsd:" + (String) lesArgumentsPasses.get("urlfichierxsd"));
            }
        }

        if (lesArgumentsPasses != null)
        {
            if (lesArgumentsPasses.containsKey("urlfichiersortie"))
            {
                setUrlfichiersortie((String) lesArgumentsPasses.get("urlfichiersortie"));
                log.debug("valeur fichiersortieavecurl:" + (String) lesArgumentsPasses.get("urlfichiersortie"));

            }
            else
            {
                // le fichier sortie sera de la forme file://..
                setFichiersortieavecurl(this.getUrlfichiersortie());
                log.debug("valeur fichiersortieavecurl:" + getUrlfichiersortie());
            }
        }
        else
        {
            // le fichier sortie sera de la forme file://..
            setFichiersortieavecurl(getUrlfichiersortie());
            log.debug("valeur fichiersortieavecurl:" + getUrlfichiersortie());
        }

        log.info("valeur Urlfichierxml:" + urlfichierxml);
        log.info("valeur Urlfichierxsd:" + urlfichierxsd);
        log.info("valeur Urlfichiersortie:" + urlfichiersortie);
        log.info("entrée traiterBatch TraitementVerificationXMLXSD");
        log.info("valeur fichiersortieavecurl:" + getFichiersortieavecurl());

        String lesResponses =
            xsdValidator.validerXMLParRapportXSD(urlfichierxml, urlfichierxsd, recuperertoutesleserreurs);
        if (lesResponses != null)
        {
            log.info("des erreurs ont été détectées au cours de la validation");
            if (getFichiersortieavecurl() != null && getFichiersortieavecurl().compareTo("") != 0)
            {
                // cas ou fichier sortie est une url
                log.debug(" avant genererFichierSortieBatch valeur fichiersortieavecurl:"
                    + getFichiersortieavecurl());
                genererFichierSortieBatch(lesResponses, getFichiersortieavecurl());
            }
            else
            {
                // autre cas
                genererFichierSortieBatch(lesResponses);
            }

            throw new RuntimeException(
                "Des erreurs ont été détectées lors de la validation du xml par rapport au xsd.Le fichier xml n'est pas conforme au fichier xsd");

        }
        else
        {
            log.info("aucune erreur détectée lors de la validation");
            if (getFichiersortieavecurl() != null && getFichiersortieavecurl().compareTo("") != 0)
            {
                // cas ou fichier sortie est une url
                log.debug(" avant genererFichierSortieBatch valeur fichiersortieavecurl:"
                    + getFichiersortieavecurl());
                genererFichierSortieBatch("fichier valide", getFichiersortieavecurl());
            }
            else
            {
                // autre cas
                genererFichierSortieBatch("fichier valide");
            }
        }

    }

    /**
     * methode Generer fichier sortie batch : --.
     * 
     * @param lesResponses --
     */
    private void genererFichierSortieBatch(String lesResponses)
    {
        try
        {
            File fichiersortie = new File(urlfichiersortie);
            FileUtils.writeByteArrayToFile(fichiersortie, lesResponses.getBytes());
        }
        catch (FileNotFoundException fnfe)
        {
            log.error("Erreur : ", fnfe);
        }
        catch (IOException ioe)
        {
            log.error("Erreur : ", ioe);
        }
    }

    // cas ou le nom du fichier sortie est de la forme file://..
    /**
     * methode Generer fichier sortie batch : --.
     * 
     * @param lesResponses --
     * @param nomFichier --
     */
    private void genererFichierSortieBatch(String lesResponses, String nomFichier)
    {
        try
        {
            String nomurl = new URL(nomFichier).getFile();
            File fichiersortie = new File(nomurl);
            FileUtils.writeByteArrayToFile(fichiersortie, lesResponses.getBytes());
        }
        catch (FileNotFoundException fnfe)
        {
            log.error("Erreur : ", fnfe);
        }
        catch (IOException ioe)
        {
            log.error("Erreur : ", ioe);
        }
    }

}
