/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 *
 */
package fr.gouv.finances.lombok.batch.config;

import javax.sql.DataSource;

import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.launch.support.SimpleJobLauncher;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.core.repository.support.JobRepositoryFactoryBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.env.Environment;
import org.springframework.transaction.PlatformTransactionManager;

/**
 * Classe de configuration. Elle reprend les éléments de batchContext-resources.xml
 * 
 * @author pphilibert Date: 2 août 2018
 */
@Configuration
@Profile({"integration", "springbatch"})
public class LombokBatchResourcesConfiguration
{
    @Autowired
    @Qualifier("datasource")
    DataSource datasource;

    @Autowired
    @Qualifier("transactionManager")
    PlatformTransactionManager transactionManager;

    @Autowired
    private Environment environment;

    /**
     * Constructeur
     */
    public LombokBatchResourcesConfiguration()
    {
        super();
    }

    /**
     * Création d'un bean jobLauncher
     *
     * @return JobLauncher
     * @throws Exception
     */
    @Bean
    public JobLauncher jobLauncher() throws Exception
    {
        SimpleJobLauncher jobLauncher = new SimpleJobLauncher();
        jobLauncher.setJobRepository(jobRepository());
        jobLauncher.afterPropertiesSet();
        return jobLauncher;
    }

    /**
     * Création d'un bean jobRepository
     *
     * @return JobRepository
     * @throws Exception
     */
    @Bean
    public JobRepository jobRepository() throws Exception
    {
        JobRepositoryFactoryBean factory = new JobRepositoryFactoryBean();
        factory.setDataSource(datasource);
        factory.setTransactionManager(transactionManager);
        // type de datasource défini par un profil : oracle, postgre, embedded
        String type = null;
        if (environment.getActiveProfiles() != null)
        {
            for (String profil : environment.getActiveProfiles())
            {
                if ("oracle".equals(profil))
                {
                    type = "oracle";
                }
                else if ("postgre".equals(profil))
                {
                    type = "postgres";
                }
                else if ("embedded".equals(profil))
                {
                    type = "hsql";
                }
            }
        }
        if (type != null)
        {
            factory.setDatabaseType(type);
        }
        factory.afterPropertiesSet();
        return factory.getObject();
    }
}
