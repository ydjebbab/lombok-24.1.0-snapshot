/*
 * Copyright (c) 2017 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.batch;

/**
 * Classe wrappant l'appel System.exit.
 */
public class SystemExitWrapper
{

    /**
     * <pre>
     * Méthode appelée depuis BatchStarter. 
     * Elle va permettre de créer des tests en mockant ce wrapper avec mockito.
     * </pre>
     *
     * @param status le status qui sera retourné
     */
    public void exit(int status)
    {
        System.exit(status);
    }

}
