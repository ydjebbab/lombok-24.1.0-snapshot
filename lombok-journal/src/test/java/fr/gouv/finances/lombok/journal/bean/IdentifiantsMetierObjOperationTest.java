/*
 * Copyright (c) 2016 DGFiP - Tous droits réservés
 * 
 */
package fr.gouv.finances.lombok.journal.bean;

import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;

import org.junit.Test;

import fr.gouv.finances.lombok.test.pojo.AbstractCorePojoTest;

/**
 * Tests unitaires du POJO IdentifiantsMetierObjOperation.
 * 
 * @author Christophe Breheret-Girardin
 */
public class IdentifiantsMetierObjOperationTest extends AbstractCorePojoTest<IdentifiantsMetierObjOperation>
{
    /**
     * Constructeur.
     */
    public IdentifiantsMetierObjOperationTest()
    {
        super();
    }

    /**
     * Méthode permettant de tester l'égalité du POJO.
     */
    @Test
    public final void testEqualsHashCode()
    {
        EqualsVerifier.forClass(getParameterizedType())
            .suppress(Warning.NONFINAL_FIELDS, Warning.STRICT_INHERITANCE)
            .withOnlyTheseFields("libelleIdentifiantMetier1", "libelleIdentifiantMetier2"
                , "libelleIdentifiantMetier3", "libelleIdentifiantMetier4", "libelleIdentifiantMetier5"
                , "valeurIdentifiantMetier1", "valeurIdentifiantMetier2", "valeurIdentifiantMetier3"
                , "valeurIdentifiantMetier4", "valeurIdentifiantMetier5")
            .usingGetClass()
            .verify();
    }
}
