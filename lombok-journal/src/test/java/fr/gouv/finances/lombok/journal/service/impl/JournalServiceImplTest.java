/*
 * Copyright (c) 2016 DGFiP - Tous droits réservés
 * 
 */
package fr.gouv.finances.lombok.journal.service.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionException;
import org.springframework.transaction.support.AbstractPlatformTransactionManager;
import org.springframework.transaction.support.DefaultTransactionStatus;
import org.springframework.transaction.support.TransactionTemplate;

import fr.gouv.finances.lombok.journal.bean.CriteresRecherchesJournal;
import fr.gouv.finances.lombok.journal.bean.IdentifiantsMetierObjOperation;
import fr.gouv.finances.lombok.journal.bean.OperationJournal;
import fr.gouv.finances.lombok.journal.bean.ParametreOperation;
import fr.gouv.finances.lombok.journal.dao.JournalDao;
import fr.gouv.finances.lombok.journal.dao.ParametreOperationDao;
import fr.gouv.finances.lombok.journal.dao.JournalDao;
import fr.gouv.finances.lombok.journal.dao.ParametreOperationDao;
import fr.gouv.finances.lombok.util.exception.ProgrammationException;
import fr.gouv.finances.lombok.util.persistance.ScrollIterator;

/**
 * JournalServiceImplTest.java Class de test unitaire du service journal
 * 
 * @author christian.houard Date: 25 oct. 2016
 */
public class JournalServiceImplTest
{

    /** journal service. */
    private JournalServiceImpl journalService;

    /** journal dao. */
    private JournalDao journalDao;

    /** paramoper dao. */
    private ParametreOperationDao paramoperDao;

    /**
     * methode Setup mock.
     */
    @Before
    public void setupMock()
    {
        journalService = new JournalServiceImpl();
        journalDao = mock(JournalDao.class);
        paramoperDao = mock(ParametreOperationDao.class);
        journalService.setJournaldao(journalDao);
        journalService.setParamoperdao(paramoperDao);
        TransactionTemplate txTemplate = new TransactionTemplate(new AbstractPlatformTransactionManager()
        {

            /**
             * serialVersionUID - long,
             */
            private static final long serialVersionUID = 1L;

            @Override
            protected Object doGetTransaction() throws TransactionException
            {
                return new Object();
            }

            @Override
            protected void doCommit(DefaultTransactionStatus status) throws TransactionException
            {
                // Raccord de méthode auto-généré
            }

            @Override
            protected void doRollback(DefaultTransactionStatus status) throws TransactionException
            {
                // Raccord de méthode auto-généré
            }

            @Override
            protected void doBegin(Object transaction, TransactionDefinition definition) throws TransactionException
            {
                // Raccord de méthode auto-généré
            }
        });

        journalService.setTransactionTemplate(txTemplate);

    }

    /**
     * Test method for {@link fr.gouv.finances.lombok.journal.service.impl.JournalServiceImpl#JournalServiceImpl()}.
     *
     * @throws Exception the exception
     */
    @Test
    public final void testJournalServiceImpl() throws Exception
    {
        assertNotNull(journalService);

    }

    /**
     * Test method for
     * {@link fr.gouv.finances.lombok.journal.service.impl.JournalServiceImpl#ajouterUneOperationAuJournal(java.lang.String, java.util.Set, java.lang.String, java.lang.String, java.lang.String, fr.gouv.finances.lombok.journal.bean.IdentifiantsMetierObjOperation, java.lang.String, boolean)}
     * .
     *
     * @throws Exception the exception
     */
    @Test
    public final void testAjouterUneOperationAuJournal() throws Exception
    {
        // ajout d' une operation 1
        String idutiloubatch = "idutiloubatch1";
        String natureOperation = "natureOperation1";
        String identifiantStructure = "identifiantStructure1";
        String dureedeconservation = "COURT";
        String codeOperation = "codeOperation1";
        boolean apurementPossible = false;

        Set<ParametreOperation> lesParamOperation = new HashSet<ParametreOperation>();
        ParametreOperation paramOper = new ParametreOperation();

        paramOper.setNom("Nom1");
        paramOper.setValeur("Valeur1");
        lesParamOperation.add(paramOper);

        IdentifiantsMetierObjOperation unIdentifiantsMetierObjOperation = new IdentifiantsMetierObjOperation();
        unIdentifiantsMetierObjOperation.setLibelleIdentifiantMetier1("libelleIdentifiantMetier1");
        unIdentifiantsMetierObjOperation.setValeurIdentifiantMetier1("valeurIdentifiantMetier1");
        OperationJournal opeJournal;
        opeJournal = journalService.ajouterUneOperationAuJournal(idutiloubatch, lesParamOperation, natureOperation, identifiantStructure,
            dureedeconservation, unIdentifiantsMetierObjOperation, codeOperation, apurementPossible);

        assertNotNull(opeJournal);
        assertEquals(idutiloubatch, opeJournal.getIdentifiantUtilisateurOuBatch());

        // ajout d' une operation 2
        idutiloubatch = "idutiloubatch2";
        natureOperation = "natureOperation2";
        identifiantStructure = "identifiantStructure2";
        dureedeconservation = "COURT";
        codeOperation = "codeOperation2";
        apurementPossible = false;

        lesParamOperation = new HashSet<ParametreOperation>();
        paramOper = new ParametreOperation();
        paramOper.setNom("Nom2");
        paramOper.setValeur("Valeur2");
        lesParamOperation.add(paramOper);

        unIdentifiantsMetierObjOperation = new IdentifiantsMetierObjOperation();
        unIdentifiantsMetierObjOperation.setLibelleIdentifiantMetier1("libelleIdentifiantMetier2");
        unIdentifiantsMetierObjOperation.setValeurIdentifiantMetier1("valeurIdentifiantMetier2");

        opeJournal = journalService.ajouterUneOperationAuJournal(idutiloubatch, lesParamOperation, natureOperation,
            identifiantStructure, dureedeconservation, unIdentifiantsMetierObjOperation, codeOperation,
            apurementPossible);

        assertNotNull(opeJournal);
        assertEquals(idutiloubatch, opeJournal.getIdentifiantUtilisateurOuBatch());

        // ajout d' une operation 3
        idutiloubatch = "idutiloubatch3";
        natureOperation = "natureOperation3";
        identifiantStructure = "identifiantStructure3";
        dureedeconservation = "COURT";
        codeOperation = "codeOperation3";
        apurementPossible = true;

        lesParamOperation = new HashSet<ParametreOperation>();
        paramOper = new ParametreOperation();
        paramOper.setNom("Nom3");
        paramOper.setValeur("Valeur3");
        lesParamOperation.add(paramOper);

        unIdentifiantsMetierObjOperation = new IdentifiantsMetierObjOperation();
        unIdentifiantsMetierObjOperation.setLibelleIdentifiantMetier1("libelleIdentifiantMetier3");
        unIdentifiantsMetierObjOperation.setValeurIdentifiantMetier1("valeurIdentifiantMetier3");

        opeJournal = journalService.ajouterUneOperationAuJournal(idutiloubatch, lesParamOperation, natureOperation,
            identifiantStructure, dureedeconservation, unIdentifiantsMetierObjOperation, codeOperation,
            apurementPossible);

        assertNotNull(opeJournal);
        assertEquals(idutiloubatch, opeJournal.getIdentifiantUtilisateurOuBatch());

        // ajout d' une operation 4
        idutiloubatch = "idutiloubatch4";
        natureOperation = "natureOperation4";
        identifiantStructure = "identifiantStructure4";
        dureedeconservation = "COURT";
        codeOperation = "codeOperation4";
        apurementPossible = true;

        lesParamOperation = new HashSet<ParametreOperation>();
        paramOper = new ParametreOperation();
        paramOper.setNom("Nom4");
        paramOper.setValeur("Valeur4");
        lesParamOperation.add(paramOper);

        unIdentifiantsMetierObjOperation = new IdentifiantsMetierObjOperation();
        unIdentifiantsMetierObjOperation.setLibelleIdentifiantMetier1("libelleIdentifiantMetier4");
        unIdentifiantsMetierObjOperation.setValeurIdentifiantMetier1("valeurIdentifiantMetier4");

        opeJournal = journalService.ajouterUneOperationAuJournal(idutiloubatch, lesParamOperation, natureOperation,
            identifiantStructure, dureedeconservation, unIdentifiantsMetierObjOperation, codeOperation,
            apurementPossible);

        assertNotNull(opeJournal);
        assertEquals(idutiloubatch, opeJournal.getIdentifiantUtilisateurOuBatch());

    }

    /**
     * Test method for
     * {@link fr.gouv.finances.lombok.journal.service.impl.JournalServiceImpl#ajusteSequencesOperationJournal()}.
     *
     * @throws Exception the exception
     */
    @Test
    public final void testAjusteSequencesOperationJournal() throws Exception
    {
        journalService.ajusteSequencesOperationJournal();

    }

    /**
     * Test method for
     * {@link fr.gouv.finances.lombok.journal.service.impl.JournalServiceImpl#effacerLesOperationPerimees()}.
     *
     * @throws Exception the exception
     */
    @Test
    public final void testEffacerLesOperationPerimees() throws Exception
    {
        ScrollIterator stubScroll = mock(ScrollIterator.class);
        when(stubScroll.hasNext()).thenReturn(true).thenReturn(false);
        when(stubScroll.nextObjetMetier()).thenReturn(mock(OperationJournal.class));
        
        when(journalDao.findOperationsPerimeesAUneDatePourUneDureeDeRetention(any(Date.class),
            anyString(), anyInt())).thenReturn(stubScroll);
        journalService.effacerLesOperationPerimees();

    }

    /**
     * Test method for
     * {@link fr.gouv.finances.lombok.journal.service.impl.JournalServiceImpl#effacerLesOperationPerimees(java.lang.String, int)}
     * .
     *
     * @throws Exception the exception
     */
    @Test
    public final void testEffacerLesOperationPerimeesStringInt() throws Exception
    {
        when(journalDao.findOperationsPerimeesAUneDatePourUneDureeDeRetention(any(Date.class),
            anyString(), anyInt())).thenReturn(mock(ScrollIterator.class));
        journalService.effacerLesOperationPerimees("toto", 10);

    }

    /**
     * Test method for
     * {@link fr.gouv.finances.lombok.journal.service.impl.JournalServiceImpl#rechercherLesOperationsJournalParCriteresRecherche(fr.gouv.finances.lombok.journal.bean.CriteresRecherchesJournal)}
     * .
     *
     * @throws Exception the exception
     */
    @Test
    public final void testRechercherLesOperationsJournalParCriteresRecherche() throws Exception
    {
        assertTrue(journalService.rechercherLesOperationsJournalParCriteresRecherche(mock(CriteresRecherchesJournal.class)).isEmpty());

    }

    /**
     * Test method for
     * {@link fr.gouv.finances.lombok.journal.service.impl.JournalServiceImpl#rechercherOperationsJournalParIdentifiantMetierObjOperation(fr.gouv.finances.lombok.journal.bean.IdentifiantsMetierObjOperation)}
     * .
     *
     * @throws Exception the exception
     */
    @Test
    public final void testRechercherOperationsJournalParIdentifiantMetierObjOperation() throws Exception
    {
        assertTrue(journalService.rechercherOperationsJournalParIdentifiantMetierObjOperation(mock(IdentifiantsMetierObjOperation.class))
            .isEmpty());

    }

    /**
     * Test method for
     * {@link fr.gouv.finances.lombok.journal.service.impl.JournalServiceImpl#rechercheUneOperationJournalParIdUtilEtDateHeure(java.lang.String, java.util.Date)}
     * .
     *
     * @throws Exception the exception
     */
    @Test
    public final void testRechercheUneOperationJournalParIdUtilEtDateHeure() throws Exception
    {
        when(journalDao.findUneOperationJournalParIdUtilEtDateHeure(anyString(), any(Date.class))).thenReturn(mock(OperationJournal.class));
        assertNotNull(journalService.rechercheUneOperationJournalParIdUtilEtDateHeure("lol", mock(Date.class)));

    }

    /** thrown. */
    @Rule
    public ExpectedException thrown = ExpectedException.none();

    /**
     * Test method for
     * {@link fr.gouv.finances.lombok.journal.service.impl.JournalServiceImpl#recreationParametresOperations()}.
     *
     * @throws Exception the exception
     */
    @Test
    public final void testRecreationParametresOperationsWithException() throws Exception
    {
        thrown.expect(ProgrammationException.class);
        journalService.recreationParametresOperations();

    }

    /**
     * methode Test recreation parametres operations without exception : DOCUMENTEZ_MOI.
     *
     * @throws Exception the exception
     */
    @Test
    public final void testRecreationParametresOperationsWithoutException() throws Exception
    {
        when(journalDao.testerExistenceContrainteUniquePAOP()).thenReturn(false);
        when(journalDao.findAllOperationsJournal()).thenReturn(mock(ScrollIterator.class));
        journalService.recreationParametresOperations();

    }

    /**
     * Test method for
     * {@link fr.gouv.finances.lombok.journal.service.impl.JournalServiceImpl#supprimerUneOperationJournal(fr.gouv.finances.lombok.journal.bean.OperationJournal)}
     * .
     *
     * @throws Exception the exception
     */
    @Test
    public final void testSupprimerUneOperationJournal() throws Exception
    {
        journalService.supprimerUneOperationJournal(mock(OperationJournal.class));

    }

    /**
     * Test method for
     * {@link fr.gouv.finances.lombok.journal.service.impl.JournalServiceImpl#retourneDureeConservationAssocieeAuCode(fr.gouv.finances.lombok.journal.bean.OperationJournal)}
     * .
     *
     * @throws Exception the exception
     */
    @Test
    public final void testRetourneDureeConservationAssocieeAuCode() throws Exception
    {
        // DOCUMENTEZ_MOI

    }

    /**
     * Test method for
     * {@link fr.gouv.finances.lombok.journal.service.impl.JournalServiceImpl#tranfererToutesLesOperationsJournalDansITM()}
     * .
     *
     * @throws Exception the exception
     */
    @Test
    public final void testTranfererToutesLesOperationsJournalDansITM() throws Exception
    {
        // DOCUMENTEZ_MOI

    }

}
