/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : JournalServiceTest.java
 *
 */
package fr.gouv.finances.lombok.test.journal;

import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import fr.gouv.finances.lombok.journal.bean.CriteresRecherchesJournal;
import fr.gouv.finances.lombok.journal.bean.IdentifiantsMetierObjOperation;
import fr.gouv.finances.lombok.journal.bean.OperationJournal;
import fr.gouv.finances.lombok.journal.bean.ParametreOperation;
import fr.gouv.finances.lombok.journal.service.JournalService;
import fr.gouv.finances.lombok.util.base.LombokServiceTest;

/**
 * Class JournalServiceTest
 * 
 * 
 * @version $Revision: 1.3 $ Date: 11 déc. 2009
 */
//@RunWith(LombokJunit4ClassRunner.class)
@LombokServiceTest
public class JournalServiceTest
{
    private Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    protected JournalService journalserviceimpl;

    /**
     * methode Ajouter operation au journal avec plusieurs param et valeurs numeriques : --.
     */
    public void ajouterOperationAuJournalAvecPlusieursParamEtValeursNumeriques()
    {

        /* créer les attributs de operationjournal */
        Set<ParametreOperation> lesParamOperation = new HashSet<ParametreOperation>();
        ParametreOperation paramOper = new ParametreOperation();
        ParametreOperation paramOper1 = new ParametreOperation();
        ParametreOperation paramOper2 = new ParametreOperation();

        paramOper.setNom("401");
        paramOper.setValeur("100");
        lesParamOperation.add(paramOper);
        paramOper1.setNom("404");
        paramOper1.setValeur("100");
        lesParamOperation.add(paramOper1);
        paramOper2.setNom("512");
        paramOper2.setValeur("200");
        lesParamOperation.add(paramOper2);
        journalserviceimpl.ajouterUneOperationAuJournal("idutiloubatctcqq", lesParamOperation, "natureoperationqq",
            " identifiantStructureqq", "COURT", null, "", true);
        log.info("aprés ajouter nouvelle operation avec  * valeurs");

    }

    /**
     * methode Ajouter operation au journal avec test ordre insertion pram oper :   .
     */
    public void ajouterOperationAuJournalAvecTestOrdreInsertionPramOper()
    {

        /* créer les attributs de operationjournal */
        Set<ParametreOperation> lesParamOperation = new HashSet<ParametreOperation>();
        ParametreOperation paramOper = new ParametreOperation();
        ParametreOperation paramOper1 = new ParametreOperation();
        ParametreOperation paramOper2 = new ParametreOperation();

        paramOper.setNom("P01");
        paramOper.setValeur("100");
        paramOper.setNumIncrement(new Long(1));
        lesParamOperation.add(paramOper);
        paramOper1.setNom("P02");
        paramOper1.setValeur("100");
        paramOper1.setNumIncrement(new Long(2));
        lesParamOperation.add(paramOper1);
        paramOper2.setNom("P03");
        paramOper2.setValeur("200");
        paramOper2.setNumIncrement(new Long(3));
        lesParamOperation.add(paramOper2);
        journalserviceimpl.ajouterUneOperationAuJournal("idutiloubatctcqq", lesParamOperation, "natureoperationoRDREiNSERTIONpARAM",
            " identifiantStructureoRDREiNSERTIONpARAM", "COURT", null, "oRDREiNSERTIONpARAM", true);
        log.info("aprés ajouter nouvelle operation avec  * valeurs");

        CriteresRecherchesJournal crit = new CriteresRecherchesJournal();
        crit.ajouterUnCodeOperation("oRDREiNSERTIONpARAM");
        @SuppressWarnings("rawtypes")
        List lesoperations = journalserviceimpl.rechercherLesOperationsJournalParCriteresRecherche(crit);

   
        @SuppressWarnings("rawtypes")
        Iterator iter = lesoperations.iterator();
        while (iter.hasNext())
        {
            OperationJournal uneOpJOurnal = (OperationJournal) iter.next();
            Iterator<ParametreOperation> iterOer = uneOpJOurnal.getLesParamOperation().iterator();
            {
                while (iterOer.hasNext())
                {
                    ParametreOperation paramOper3 = (ParametreOperation) iterOer.next();
                    if (paramOper3.getNom().compareTo("P02") == 0)
                    {
                        Assert.assertTrue(paramOper3.getNumIncrement().intValue() == 2);
                    }
                }
            }
        }
    }

    /**
     * methode Ajouter operations au journal : --.
     */
    public void ajouterOperationsAuJournal()
    {

        /* créer les attributs de operationjournal */
        Set<ParametreOperation> lesParamOperation = new HashSet<ParametreOperation>();
        ParametreOperation paramOper = new ParametreOperation();

        paramOper.setNom("C2");
        paramOper.setValeur("V2");
        lesParamOperation.add(paramOper);

        journalserviceimpl.ajouterUneOperationAuJournal("idutiloubatc2h", lesParamOperation, "natureoperation2",
            " identifiantStructure", "COURT", null, "", true);
        log.info("aprés ajouter nouvelle operation 1");

        lesParamOperation = new HashSet<ParametreOperation>();
        paramOper = new ParametreOperation();
        paramOper.setNom("C1");
        paramOper.setValeur("V1");
        lesParamOperation.add(paramOper);
        journalserviceimpl.ajouterUneOperationAuJournal("idutiloubatch1", lesParamOperation, "natureoperation1",
            " identifiantStructure1", "COURT", null, "", true);
        log.info("aprés ajouter nouvelle operation 2");

        lesParamOperation = new HashSet<ParametreOperation>();
        paramOper = new ParametreOperation();
        paramOper.setNom("C0");
        paramOper.setValeur("V0");
        lesParamOperation.add(paramOper);
        journalserviceimpl.ajouterUneOperationAuJournal("idutiloubatch1", lesParamOperation, "natureoperation1",
            " identifiantStructure1", "COURT", null, "", true);
        log.info("aprés ajouter nouvelle operation 3");

        // opération avec param oper exisatnt (nom valeur )
        lesParamOperation = new HashSet<ParametreOperation>();
        paramOper = new ParametreOperation();
        paramOper.setNom("C2");
        paramOper.setValeur("V2");
        lesParamOperation.add(paramOper);
        journalserviceimpl.ajouterUneOperationAuJournal("idutiloubatc2hbis", lesParamOperation, "natureoperation2",
            " identifiantStructure", "COURT", null, "", true);
        log.info("aprés ajouter nouvelle operation 4");

        // opération avec param oper exisatnt (nom uniquement )
        lesParamOperation = new HashSet<ParametreOperation>();
        paramOper = new ParametreOperation();
        paramOper.setNom("C2");
        paramOper.setValeur("V3");
        lesParamOperation.add(paramOper);
        journalserviceimpl.ajouterUneOperationAuJournal("idutiloubatc2hbis", lesParamOperation, "natureoperation2",
            " identifiantStructure", "COURT", null, "", true);
        log.info("aprés ajouter nouvelle operation 5");

    }

    /**
     * methode Ajouter p lusieurs operation neptune au journal : --.
     */
    public void ajouterPLusieursOperationNeptuneAuJournal()
    {

        // ajout d' une operation 1
        String idutiloubatch = "idutiloubatch1";
        String natureOperation = "natureOperation1";
        String identifiantStructure = "identifiantStructure1";
        String dureedeconservation = "COURT";
        String codeOperation = "codeOperation1";
        boolean apurementPossible = false;

        Set<ParametreOperation> lesParamOperation = new HashSet<ParametreOperation>();
        ParametreOperation paramOper = new ParametreOperation();

        paramOper.setNom("Nom1");
        paramOper.setValeur("Valeur1");
        lesParamOperation.add(paramOper);

        IdentifiantsMetierObjOperation unIdentifiantsMetierObjOperation = new IdentifiantsMetierObjOperation();
        unIdentifiantsMetierObjOperation.setLibelleIdentifiantMetier1("libelleIdentifiantMetier1");
        unIdentifiantsMetierObjOperation.setValeurIdentifiantMetier1("valeurIdentifiantMetier1");

        journalserviceimpl.ajouterUneOperationAuJournal(idutiloubatch, lesParamOperation, natureOperation,
            identifiantStructure, dureedeconservation, unIdentifiantsMetierObjOperation, codeOperation,
            apurementPossible);

        // ajout d' une operation 2
        idutiloubatch = "idutiloubatch2";
        natureOperation = "natureOperation2";
        identifiantStructure = "identifiantStructure2";
        dureedeconservation = "COURT";
        codeOperation = "codeOperation2";
        apurementPossible = false;

        lesParamOperation = new HashSet<ParametreOperation>();
        paramOper = new ParametreOperation();
        paramOper.setNom("Nom2");
        paramOper.setValeur("Valeur2");
        lesParamOperation.add(paramOper);

        unIdentifiantsMetierObjOperation = new IdentifiantsMetierObjOperation();
        unIdentifiantsMetierObjOperation.setLibelleIdentifiantMetier1("libelleIdentifiantMetier2");
        unIdentifiantsMetierObjOperation.setValeurIdentifiantMetier1("valeurIdentifiantMetier2");

        journalserviceimpl.ajouterUneOperationAuJournal(idutiloubatch, lesParamOperation, natureOperation,
            identifiantStructure, dureedeconservation, unIdentifiantsMetierObjOperation, codeOperation,
            apurementPossible);

        // ajout d' une operation 3
        idutiloubatch = "idutiloubatch3";
        natureOperation = "natureOperation3";
        identifiantStructure = "identifiantStructure3";
        dureedeconservation = "COURT";
        codeOperation = "codeOperation3";
        apurementPossible = true;

        lesParamOperation = new HashSet<ParametreOperation>();
        paramOper = new ParametreOperation();
        paramOper.setNom("Nom3");
        paramOper.setValeur("Valeur3");
        lesParamOperation.add(paramOper);

        unIdentifiantsMetierObjOperation = new IdentifiantsMetierObjOperation();
        unIdentifiantsMetierObjOperation.setLibelleIdentifiantMetier1("libelleIdentifiantMetier3");
        unIdentifiantsMetierObjOperation.setValeurIdentifiantMetier1("valeurIdentifiantMetier3");

        journalserviceimpl.ajouterUneOperationAuJournal(idutiloubatch, lesParamOperation, natureOperation,
            identifiantStructure, dureedeconservation, unIdentifiantsMetierObjOperation, codeOperation,
            apurementPossible);

        // ajout d' une operation 4
        idutiloubatch = "idutiloubatch4";
        natureOperation = "natureOperation4";
        identifiantStructure = "identifiantStructure4";
        dureedeconservation = "COURT";
        codeOperation = "codeOperation4";
        apurementPossible = true;

        lesParamOperation = new HashSet<ParametreOperation>();
        paramOper = new ParametreOperation();
        paramOper.setNom("Nom4");
        paramOper.setValeur("Valeur4");
        lesParamOperation.add(paramOper);

        unIdentifiantsMetierObjOperation = new IdentifiantsMetierObjOperation();
        unIdentifiantsMetierObjOperation.setLibelleIdentifiantMetier1("libelleIdentifiantMetier4");
        unIdentifiantsMetierObjOperation.setValeurIdentifiantMetier1("valeurIdentifiantMetier4");

        journalserviceimpl.ajouterUneOperationAuJournal(idutiloubatch, lesParamOperation, natureOperation,
            identifiantStructure, dureedeconservation, unIdentifiantsMetierObjOperation, codeOperation,
            apurementPossible);

    }

    /**
     * methode Ajouter une nouvelle operation neptune au journal : --.
     */
    public void ajouterUneNouvelleOperationNeptuneAuJournal()
    {

        // ajout d' une operation 1
        String idutiloubatch = "idutiloubatch1";
        String natureOperation = "natureOperation1";
        String identifiantStructure = "identifiantStructure1";
        String dureedeconservation = "COURT";
        String codeOperation = "codeOperation1";
        boolean apurementPossible = false;

        Set<ParametreOperation> lesParamOperation = new HashSet<ParametreOperation>();
        ParametreOperation paramOper = new ParametreOperation();

        paramOper.setNom("Nom1");
        paramOper.setValeur("Valeur1");
        lesParamOperation.add(paramOper);

        IdentifiantsMetierObjOperation unIdentifiantsMetierObjOperation = new IdentifiantsMetierObjOperation();
        unIdentifiantsMetierObjOperation.setLibelleIdentifiantMetier1("libelleIdentifiantMetier1");
        unIdentifiantsMetierObjOperation.setValeurIdentifiantMetier1("valeurIdentifiantMetier1");

        journalserviceimpl.ajouterUneOperationAuJournal(idutiloubatch, lesParamOperation, natureOperation,
            identifiantStructure, dureedeconservation, unIdentifiantsMetierObjOperation, codeOperation,
            apurementPossible);

    }

    /**
     * methode Test ajouter o peration ancien format : --.
     */
    //@Test
    public void testAjouterOPerationAncienFormat()
    {

        journalserviceimpl.ajouterUneOperationAuJournal("idutiloubatch", null, "natureOperation1", "075000", "COURT",
            null, "", true);
    }

    /**
     * methode Test ajouter operation au journal : --.
     * 
     * @throws Exception the exception
     */
    //@Test
    public void testAjouterOperationAuJournal() throws Exception
    {

        // créer les attributs de operationjournal
        Set<ParametreOperation> lesParamOperation = new HashSet<ParametreOperation>();
        ParametreOperation paramOper = new ParametreOperation();

        paramOper.setNom("C0");
        paramOper.setValeur("V0");
        lesParamOperation.add(paramOper);

        ParametreOperation paramOper1 = new ParametreOperation();
        paramOper1.setNom("C0");
        paramOper1.setValeur("");
        lesParamOperation.add(paramOper1);
        ParametreOperation paramOper2 = new ParametreOperation();
        paramOper2.setNom("C0");
        paramOper2.setValeur("");
        lesParamOperation.add(paramOper2);

        ParametreOperation paramOper3 = new ParametreOperation();
        paramOper3.setNom("C0");
        paramOper3.setValeur("  ");
        lesParamOperation.add(paramOper3);
        ParametreOperation paramOper4 = new ParametreOperation();
        paramOper4.setNom("C0");
        paramOper4.setValeur("  ");
        lesParamOperation.add(paramOper4);

        ParametreOperation paramOper5 = new ParametreOperation();
        paramOper5.setNom("C0");
        paramOper5.setValeur("     ");
        lesParamOperation.add(paramOper5);
        ParametreOperation paramOper6 = new ParametreOperation();
        paramOper6.setNom("C0");
        paramOper6.setValeur("     ");
        lesParamOperation.add(paramOper6);

        ParametreOperation paramOper7 = new ParametreOperation();
        paramOper7.setNom("C0");
        paramOper7.setValeur(null);
        lesParamOperation.add(paramOper7);
        ParametreOperation paramOper8 = new ParametreOperation();
        paramOper8.setNom("C0");
        paramOper8.setValeur(null);
        lesParamOperation.add(paramOper8);

        journalserviceimpl.ajouterUneOperationAuJournal("idutiloubatch", lesParamOperation, "Création d'un orc",
            " identifiantStructure", "TRESCOURT", null, "Code1", true);
        System.out.println("aprés ajouter nouvelle operation");

    }

    /**
     * methode Test ajouter operation au journal bis : --.
     * 
     * @throws Exception the exception
     */
    //@Test
    public void testAjouterOperationAuJournalBis() throws Exception
    {

        // créer les attributs de operationjournal
        Set<ParametreOperation> lesParamOperation = new HashSet<ParametreOperation>();
        ParametreOperation paramOper = new ParametreOperation();

        paramOper.setNom("C0");
        paramOper.setValeur("V0");
        lesParamOperation.add(paramOper);
        journalserviceimpl.ajouterUneOperationAuJournal("idutiloubatch1", lesParamOperation, "Modification d'un orc",
            " identifiantStructure", "TRESCOURT", null, "Code2", true);
        System.out.println("aprés ajouter nouvelle operation");

    }

    /**
     * methode Test rechercher les operations par liste codes operation : --.
     */
    //@Test
    public void testRechercherLesOperationsParListeCodesOperation()
    {
        // Creation des opérations journal qui seront recherchées dans la suite du test

        journalserviceimpl.ajouterUneOperationAuJournal("idutiloubatch", null, "CU1", " identifiantStructure",
            "TRESCOURT", null, "Code3", true);

        journalserviceimpl.ajouterUneOperationAuJournal("idutiloubatch", null, "CU2", " identifiantStructure",
            "TRESCOURT", null, "Code4", true);

        journalserviceimpl.ajouterUneOperationAuJournal("idutiloubatch", null, "CU3", " identifiantStructure",
            "TRESCOURT", null, "Code5", true);

        CriteresRecherchesJournal crit = new CriteresRecherchesJournal();
        crit.ajouterUnCodeOperation("Code1");
        crit.ajouterUnCodeOperation("Code2");
        crit.ajouterUnCodeOperation("Code3");

        @SuppressWarnings("rawtypes")
        List lesoperations = journalserviceimpl.rechercherLesOperationsJournalParCriteresRecherche(crit);

        Assert.assertTrue(lesoperations.size() == 1);
    }

    /**
     * methode Test rechercher les operations par liste natures operation : --.
     */
    //@Test
    public void testRechercherLesOperationsParListeNaturesOperation()
    {
        // Creation des opérations journal qui seront recherchées dans la suite du test

        journalserviceimpl.ajouterUneOperationAuJournal("idutiloubatch", null, "CU1", " identifiantStructure",
            "TRESCOURT", null, "Code3", true);

        journalserviceimpl.ajouterUneOperationAuJournal("idutiloubatch", null, "CU2", " identifiantStructure",
            "TRESCOURT", null, "Code4", true);

        journalserviceimpl.ajouterUneOperationAuJournal("idutiloubatch", null, "CU3", " identifiantStructure",
            "TRESCOURT", null, "Code5", true);

        CriteresRecherchesJournal crit = new CriteresRecherchesJournal();
        crit.ajouterUneNatureOperation("CU1");
        crit.ajouterUneNatureOperation("CU2");
        crit.ajouterUneNatureOperation("CU3");

        @SuppressWarnings("rawtypes")
        List lesoperations = journalserviceimpl.rechercherLesOperationsJournalParCriteresRecherche(crit);

        Assert.assertTrue(lesoperations.size() == 3);
    }

    /**
     * methode Test rechercher les operations par nature operation : --.
     * 
     * @throws Exception the exception
     */
    public void testRechercherLesOperationsParNatureOperation() throws Exception
    {
        @SuppressWarnings("rawtypes")
        List lesoperations;

        Set<ParametreOperation> lesParamOperation = new HashSet<ParametreOperation>();
        ParametreOperation paramOper = new ParametreOperation();

        paramOper.setNom("C0");
        paramOper.setValeur("V0");
        lesParamOperation.add(paramOper);

        ParametreOperation paramOper1 = new ParametreOperation();
        paramOper1.setNom("C0");
        paramOper1.setValeur("");
        lesParamOperation.add(paramOper1);
        ParametreOperation paramOper2 = new ParametreOperation();
        paramOper2.setNom("C0");
        paramOper2.setValeur("");
        lesParamOperation.add(paramOper2);

        ParametreOperation paramOper3 = new ParametreOperation();
        paramOper3.setNom("C0");
        paramOper3.setValeur("  ");
        lesParamOperation.add(paramOper3);
        ParametreOperation paramOper4 = new ParametreOperation();
        paramOper4.setNom("C0");
        paramOper4.setValeur("  ");
        lesParamOperation.add(paramOper4);

        ParametreOperation paramOper5 = new ParametreOperation();
        paramOper5.setNom("C0");
        paramOper5.setValeur("     ");
        lesParamOperation.add(paramOper5);
        ParametreOperation paramOper6 = new ParametreOperation();
        paramOper6.setNom("C0");
        paramOper6.setValeur("     ");
        lesParamOperation.add(paramOper6);

        ParametreOperation paramOper7 = new ParametreOperation();
        paramOper7.setNom("C0");
        paramOper7.setValeur(null);
        lesParamOperation.add(paramOper7);
        ParametreOperation paramOper8 = new ParametreOperation();
        paramOper8.setNom("C0");
        paramOper8.setValeur(null);
        lesParamOperation.add(paramOper8);

        journalserviceimpl.ajouterUneOperationAuJournal("idutiloubatch", lesParamOperation, "Création d'un orc",
            " identifiantStructure", "TRESCOURT", null, "Code1", true);
        CriteresRecherchesJournal crit = new CriteresRecherchesJournal();
        crit.setNatureOperation("Création d'un orc");

        lesoperations = journalserviceimpl.rechercherLesOperationsJournalParCriteresRecherche(crit);
        Assert.assertTrue(lesoperations.size() == 1);

    }

    /**
     * methode Test rechercher les operations par un code operation : --.
     */
    //@Test
    public void testRechercherLesOperationsParUnCodeOperation()
    {
        // Creation des opérations journal qui seront recherchées dans la suite du test

        journalserviceimpl.ajouterUneOperationAuJournal("idutiloubatch", null, "CU1ZF1", " identifiantStructure",
            "TRESCOURT", null, "Code1", true);

        CriteresRecherchesJournal crit = new CriteresRecherchesJournal();
        crit.ajouterUnCodeOperation("Code1");

        @SuppressWarnings("rawtypes")
        List lesoperations = journalserviceimpl.rechercherLesOperationsJournalParCriteresRecherche(crit);

        Assert.assertTrue(lesoperations.size() == 1);
    }

    /**
     * methode Test rechercher les operations par une nature operation : --.
     */
    //@Test
    public void testRechercherLesOperationsParUneNatureOperation()
    {
        // Creation des opérations journal qui seront recherchées dans la suite du test

        journalserviceimpl.ajouterUneOperationAuJournal("idutiloubatch", null, "CU1ZF1", " identifiantStructure",
            "TRESCOURT", null, "Code1", true);

        CriteresRecherchesJournal crit = new CriteresRecherchesJournal();
        crit.ajouterUneNatureOperation("CU1ZF1");

        @SuppressWarnings("rawtypes")
        List lesoperations = journalserviceimpl.rechercherLesOperationsJournalParCriteresRecherche(crit);

        Assert.assertTrue(lesoperations.size() == 1);
    }

    /**
     * methode Test rechercher operation journal aucune o peration date : --.
     */
    //@Test
    public void testRechercherOperationJournalAucuneOPerationDate()
    {
        ajouterOperationsAuJournal();
        CriteresRecherchesJournal crit = new CriteresRecherchesJournal();
        DateTimeZone dtz = DateTimeZone.forID("UTC");
        DateTime date = new DateTime(1974, 10, 28, 0, 0, 0, 0, dtz);

        crit.setDateJJMMAAAA(date.toDate());
        @SuppressWarnings("rawtypes")
        List lesoperations = journalserviceimpl.rechercherLesOperationsJournalParCriteresRecherche(crit);
        Assert.assertEquals(lesoperations.size(), 0);

    }

    /**
     * methode Test rechercher operation journalavec nom et valeur max : --.
     */
    //@Test
    public void testRechercherOperationJournalavecNomEtValeurMax()
    {
        ajouterOperationAuJournalAvecPlusieursParamEtValeursNumeriques();
        CriteresRecherchesJournal crit = new CriteresRecherchesJournal();
        crit.setValeurParamOperMax("150");
        crit.setNomParamOper("401");
        @SuppressWarnings("rawtypes")
        List lesoperations = journalserviceimpl.rechercherLesOperationsJournalParCriteresRecherche(crit);
        Assert.assertEquals(lesoperations.size(), 1);

    }

    /**
     * methode Test rechercher operation journalavec valeur max : --.
     */
    //@Test
    public void testRechercherOperationJournalavecValeurMax()
    {
        ajouterOperationAuJournalAvecPlusieursParamEtValeursNumeriques();
        CriteresRecherchesJournal crit = new CriteresRecherchesJournal();
        crit.setValeurParamOperMax("150.6");
        crit.setNomParamOper("401");
        @SuppressWarnings("rawtypes")
        List lesoperations = journalserviceimpl.rechercherLesOperationsJournalParCriteresRecherche(crit);
        Assert.assertEquals(lesoperations.size(), 1);

    }

    /**
     * methode Test rechercher operation journal par nature : --.
     */
    //@Test
    public void testRechercherOperationJournalParNature()
    {
        ajouterPLusieursOperationNeptuneAuJournal();
        CriteresRecherchesJournal crit = new CriteresRecherchesJournal();

        crit.setNatureOperation("natureoperation1");

        @SuppressWarnings("rawtypes")
        List lesoperations = journalserviceimpl.rechercherLesOperationsJournalParCriteresRecherche(crit);
        Assert.assertNotNull(lesoperations.size());
    }

    /**
     * methode Test rechercher operation journaltest par date max : --.
     */
    //@Test
    public void testRechercherOperationJournaltestParDateMax()
    {
        ajouterOperationsAuJournal();
        CriteresRecherchesJournal crit = new CriteresRecherchesJournal();
        crit.setDateMax(new Date());
        @SuppressWarnings("rawtypes")
        List lesoperations = journalserviceimpl.rechercherLesOperationsJournalParCriteresRecherche(crit);
        Assert.assertNotNull(lesoperations.size());

    }

    /**
     * methode Test rechercher operation journaltest par nom param oper : --.
     */
    //@Test
    public void testRechercherOperationJournaltestParNomParamOper()
    {
        ajouterOperationsAuJournal();
        CriteresRecherchesJournal crit = new CriteresRecherchesJournal();
        crit.setNomParamOper("C2");
        @SuppressWarnings("rawtypes")
        List lesoperations = journalserviceimpl.rechercherLesOperationsJournalParCriteresRecherche(crit);
        Assert.assertEquals(lesoperations.size(), 5);
    }

    /**
     * methode Test rechercher operation journaltest par valeur param oper : --.
     */
    //@Test
    public void testRechercherOperationJournaltestParValeurParamOper()
    {
        ajouterOperationsAuJournal();
        CriteresRecherchesJournal crit = new CriteresRecherchesJournal();
        crit.setNomParamOper("C2");
        crit.setValeurParamOper("V2");
        @SuppressWarnings("rawtypes")
        List lesoperations = journalserviceimpl.rechercherLesOperationsJournalParCriteresRecherche(crit);
        Assert.assertEquals(lesoperations.size(), 2);

    }

    /**
     * methode Test rechercher operations journal par libelle et valuer identifiant metier obj operation : --.
     */
    //@Test
    public void testRechercherOperationsJournalParLibelleEtValuerIdentifiantMetierObjOperation()
    {
        ajouterPLusieursOperationNeptuneAuJournal();
        IdentifiantsMetierObjOperation unIdentifiantsMetierObjOperation = new IdentifiantsMetierObjOperation();
        unIdentifiantsMetierObjOperation.setLibelleIdentifiantMetier1("libelleIdentifiantMetier1");
        unIdentifiantsMetierObjOperation.setValeurIdentifiantMetier1("valeurIdentifiantMetier1");
        List<OperationJournal> lesOperations =
            journalserviceimpl
                .rechercherOperationsJournalParIdentifiantMetierObjOperation(unIdentifiantsMetierObjOperation);
        Assert.assertNotNull(lesOperations);
    }

    /**
     * methode Test rechercher operations journal par l ibelle identifiant metier obj operation : --.
     */
    //@Test
    public void testRechercherOperationsJournalParLIbelleIdentifiantMetierObjOperation()
    {
        ajouterPLusieursOperationNeptuneAuJournal();
        IdentifiantsMetierObjOperation unIdentifiantsMetierObjOperation = new IdentifiantsMetierObjOperation();
        unIdentifiantsMetierObjOperation.setLibelleIdentifiantMetier1("libelleIdentifiantMetier1");
        List<OperationJournal> lesOperations =
            journalserviceimpl
                .rechercherOperationsJournalParIdentifiantMetierObjOperation(unIdentifiantsMetierObjOperation);
        Assert.assertNotNull(lesOperations);
    }

}