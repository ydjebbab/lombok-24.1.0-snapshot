/*
 * Copyright (c) 2016 DGFiP - Tous droits réservés
 * 
 */
package fr.gouv.finances.lombok.journal.mvc.form;

import fr.gouv.finances.lombok.test.pojo.AbstractCorePojoTest;

/**
 * Tests unitaires du POJO RechercheOperationsJournalForm.
 *
 * @param  RechercheOperationsJournalForm formulaire lié à la recherche d'opération dans le journal
 *
 * @author Christophe Breheret-Girardin
 */
public class RechercheOperationsJournalFormTest extends AbstractCorePojoTest< RechercheOperationsJournalForm>
{   
    /**
     * Constructeur.
     */
    public RechercheOperationsJournalFormTest()
    {
        super();
    }

}
