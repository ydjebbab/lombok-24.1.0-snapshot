/*
 * Copyright (c) 2016 DGFiP - Tous droits réservés
 * 
 */
package fr.gouv.finances.lombok.journal.bean;

import fr.gouv.finances.lombok.test.pojo.AbstractCorePojoTest;

/**
 * Tests unitaires du POJO CriteresRecherchesJournal.
 * 
 * @author Christophe Breheret-Girardin
 */
public class CriteresRecherchesJournalTest extends AbstractCorePojoTest<CriteresRecherchesJournal>
{
    /**
     * Constructeur.
     */
    public CriteresRecherchesJournalTest()
    {
        super();
    }

}
