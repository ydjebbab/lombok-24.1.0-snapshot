/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : TraitementEffacerOperationsJournalTest.java
 *
 */
package fr.gouv.finances.lombok.test.journal;

import org.junit.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.gouv.finances.lombok.batch.ServiceBatchCommun;
import fr.gouv.finances.lombok.journal.dao.JournalDao;
import fr.gouv.finances.lombok.journal.service.JournalService;
import fr.gouv.finances.lombok.util.base.LombokServiceTest;

/**
 * Class TraitementEffacerOperationsJournalTest --.
 * 
 * 
 * @version $Revision: 1.2 $ Date: 11 déc. 2009
 */
//@RunWith(SpringJUnit4ClassRunner.class)
@LombokServiceTest
public class TraitementEffacerOperationsJournalTest
{

    @Autowired
    protected ApplicationContext context;
    
    /** journalserviceso. */
    @Autowired
    protected JournalService journalserviceso;

    /** journaldao. */
    @Autowired
    protected JournalDao journaldao;

    /**
     * methode Test migration operation journal : --.
     */
    //@Test
    public void testMigrationOperationJournal()
    {
        journalserviceso.recreationParametresOperations();
    }

    /**
     * methode Test traiter batch : --.
     */
    //@Test
    public void testTraiterBatch()
    {
        ServiceBatchCommun sbc =
            (ServiceBatchCommun) context.getBean("traitementeffaceroperationsjournalimpl");
        int statuscode = sbc.demarrer();
        Assert.assertEquals(0, statuscode);
    }

    /*
     * public void testInsererOperationsJournal() { for (int i = 0;i<2;i++) { Set lesParamOper = new HashSet();
     * ParametreOperation paramOperation1 = new ParametreOperation(); paramOperation1.setNom("nomParam1" + i);
     * paramOperation1.setValeur("valeurParam1" + i); lesParamOper.add(paramOperation1); ParametreOperation
     * paramOperation2 = new ParametreOperation(); paramOperation2.setNom("nomParam2" + i);
     * paramOperation2.setValeur("valeurParam2" + i); lesParamOper.add(paramOperation2); String idutiloubatch = "junit";
     * String natureOperation = "Insertion d'opération journal pour test du batch de purge"; String identifiantStructure
     * = "" ; String dureedeconservation = "TRESCOURT"; journalserviceso.ajouterOperationAuJournal(idutiloubatch,
     * lesParamOper, natureOperation, identifiantStructure, dureedeconservation); } CriteresRecherchesJournal crit = new
     * CriteresRecherchesJournal(); crit.setIdentifiantUtilisateurOuBatch("junit"); List operationsJournalTresCourt =
     * journaldao.findOperationsJournalParCriteresRecherche(crit); for (Iterator iter =
     * operationsJournalTresCourt.iterator(); iter.hasNext();) { OperationJournal operationJournal = (OperationJournal)
     * iter.next(); Date dateop = operationJournal.getDateHeureOperation(); Calendar cal = Calendar.getInstance();
     * cal.setTime(dateop); cal.add(Calendar.DAY_OF_YEAR, -7); Date newDate = cal.getTime();
     * operationJournal.setDateHeureOperation(newDate); journaldao.saveObject(operationJournal); }
     * log.info("Nombre d'op de durée de rétention trés courte " + operationsJournalTresCourt.size());
     * ServiceBatchCommun sbc=(ServiceBatchCommun) applicationContext.getBean("traitementeffaceroperationsjournalimpl");
     * int statuscode=sbc.demarrer(); assertEquals(0,statuscode); }
     */

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @return string
     * @see fr.gouv.finances.lombok.test.BaseServiceNonTransactionnelTestCase#ajouterConfigLocationSpecifique()
     */
    protected String ajouterConfigLocationSpecifique()
    {
        return "classpath:fr/gouv/finances/lombok/test/journal/traitementEffacerOperationsJournalContext-service.xml";
    }

}