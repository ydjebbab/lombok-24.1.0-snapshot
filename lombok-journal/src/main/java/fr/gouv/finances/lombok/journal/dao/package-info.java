/*
 * Copyright (c) 2018 DGFiP - Tous droits réservés
 * 
 */
/**
 * Paquet lié aux implémentations Hibernate des DAO de gestion des données de journalisation.
 *
 * @author Christophe Breheret-Girardin
 */
package fr.gouv.finances.lombok.journal.dao;