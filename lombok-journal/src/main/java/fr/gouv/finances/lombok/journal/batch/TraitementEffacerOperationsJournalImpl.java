/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : TraitementEffacerOperationsJournalImpl.java
 *
 */
package fr.gouv.finances.lombok.journal.batch;

import java.util.Map;
import fr.gouv.finances.lombok.batch.ServiceBatchCommunImpl;
import fr.gouv.finances.lombok.journal.service.JournalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

/**
 * <pre>
 * Batch permettant d'apurer les operations dans le journal.
 * Chaque mouvement journal a un type de duree de retention.
 * Les operations journal sont apurées en fonction de leur type de durée de rétention 
 * et de lu nombre de jour associe dans le parametrage (fichier application.properties)
 * </pre>
 *
 * @author amleplatinec-cp
 * @version $Revision: 1.2 $ Date: 15 déc. 2009
 */
@Service("traitementeffaceroperationsjournal")
@Profile("batch")
@Lazy(true)
public class TraitementEffacerOperationsJournalImpl extends ServiceBatchCommunImpl {

    /**
     * journalserviceso.
     */
    @Autowired()
    @Qualifier("journalserviceimpl")
    private JournalService journalserviceso;

    /**
     * Constructeur de la classe TraitementEffacerOperationsJournalImpl.java
     */
    public TraitementEffacerOperationsJournalImpl()
    {
        super();         
    }

    /**
     * Accesseur de l attribut journalserviceso.
     *
     * @return journalserviceso
     */
    public JournalService getJournalserviceso() {
        return journalserviceso;
    }

    /**
     * Modificateur de l attribut journalserviceso.
     *
     * @param journalserviceso le nouveau journalserviceso
     */
    public void setJournalserviceso(JournalService journalserviceso) {
        this.journalserviceso = journalserviceso;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     *
     * @see fr.gouv.finances.lombok.batch.ServiceBatchCommunImpl#traiterBatch()
     */
    @Override
    public void traiterBatch()
    {
        Map args = this.getArguments();
        if (args != null && "OUI".equalsIgnoreCase((String) args.get("MIGRATION"))) {
            journalserviceso.ajusteSequencesOperationJournal();
            journalserviceso.recreationParametresOperations();
        } else {
            journalserviceso.effacerLesOperationPerimees();
        }
    }

    @Value("${traitementeffaceroperationsjournal.versionbatch}")
    @Override()
    public void setVersionBatch(String versionBatch) {
        super.setVersionBatch(versionBatch);
    }

    @Value("${traitementeffaceroperationsjournal.nombatch}")
    @Override()
    public void setNomBatch(String nomBatch) {
        super.setNomBatch(nomBatch);
    }
}
