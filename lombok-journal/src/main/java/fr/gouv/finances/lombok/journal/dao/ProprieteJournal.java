/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 */
package fr.gouv.finances.lombok.journal.dao;

/**
 * Propriétés liées aux données des adresses.
 * @see fr.gouv.finances.lombok.journal.bean.ParametreOperation
 *
 * @author Christophe Breheret-Girardin
 */
public enum ProprieteJournal
{
    /** Propriété des paramètres d'opération de journalisation. */
    PARAMETRES("lesParamOperation"),

    /** Propriété de l'identifiant structure. */
    IDENTIFIANT_STRUCTURE("identifiantStructure"),

    /** Propriété de la nature de l'opération de journalisation. */
    NATURE("natureOperation"),

    /** Propriété du code de l'opération de journalisation. */
    CODE("codeOperation"),

    /** Propriété de l'identifiant de l'opération de journalisation. */
    IDENTIFIANT("identifiantUtilisateurOuBatch"),

    /** Propriété de temporalité de l'opération de journalisation. */
    TEMPORALITE("dateHeureOperation"),

    /** Propriété de l'apurement de l'opération de journalisation. */
    APUREMENT("apurementPossible"),

    /** Propriété du nom d'un paramètre de l'opération de journalisation. */
    NOM_PARAM("nom"),

    /**  Propriété de la valeur d'un paramètre de l'opération de journalisation. */
    VALEUR_PARAM("valeur"),
    
    /** Propriété de l'identifiant métier de l'opération de journalisation. */
    IDENTIFIANT_METIER("unIdentifiantsMetierObjOperation"),

    /** Propriété de la durée de conservation d'une opération de journalisation. */
    TYPE_DUREE_CONSERVATION("typeDureeDeConservation");

    /** Nom de la propriété */
    private String nom;

    /**
     * Constructeur.
     *
     * @param libelleNom libellé du nom de la propriété
     */
    private ProprieteJournal(String libelleNom)
    {
        nom = libelleNom;
    }

    /**
     * Accesseur de nom.
     *
     * @return nom
     */
    public String getNom()
    {
        return nom;
    }
    
}
