/*
 * Copyright (c) 2011 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.journal.bean;

import javax.xml.bind.annotation.XmlRegistry;

/**
 * This object contains factory methods for each Java content interface and Java element interface generated in the
 * fr.gouv.finances.lombok.journal.bean package.
 * <p>
 * An ObjectFactory allows you to programatically construct new instances of the Java representation for XML content.
 * The Java representation of XML content can consist of schema derived interfaces and classes representing the binding
 * of schema type definitions, element declarations and model groups. Factory methods for each of these are provided in
 * this class.
 */
@XmlRegistry
public class ObjectFactory
{

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package:
     * fr.gouv.finances.lombok.journal.bean
     */
    public ObjectFactory()
    {
    }

    /**
     * Create an instance of {@link CriteresRecherchesJournal }.
     * 
     * @return the criteres recherches journal
     */
    public CriteresRecherchesJournal createCriteresRecherchesJournal()
    {
        return new CriteresRecherchesJournal();
    }

    /**
     * Create an instance of {@link IdentifiantsMetierObjOperation }.
     * 
     * @return the identifiants metier obj operation
     */
    public IdentifiantsMetierObjOperation createIdentifiantsMetierObjOperation()
    {
        return new IdentifiantsMetierObjOperation();
    }

    /**
     * Create an instance of {@link OperationJournal }.
     * 
     * @return the operation journal
     */
    public OperationJournal createOperationJournal()
    {
        return new OperationJournal();
    }

    /**
     * Create an instance of {@link ParametreOperation }.
     * 
     * @return the parametre operation
     */
    public ParametreOperation createParametreOperation()
    {
        return new ParametreOperation();
    }

}
