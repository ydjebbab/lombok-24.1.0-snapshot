/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 */
package fr.gouv.finances.lombok.journal.dao;

/**
 * Interface fournissant des services de persistances pour les operations dans le journal.
 * Utiliser de preference l'interface du service metier JournalService
 * 
 * @see fr.gouv.finances.lombok.journal.service.JournalService
 * @author amleplatinec-cp
 *
 */

import java.util.Date;
import java.util.List;
import java.util.Set;

import fr.gouv.finances.lombok.journal.bean.CriteresRecherchesJournal;
import fr.gouv.finances.lombok.journal.bean.IdentifiantsMetierObjOperation;
import fr.gouv.finances.lombok.journal.bean.OperationJournal;
import fr.gouv.finances.lombok.journal.bean.ParametreOperation;
import fr.gouv.finances.lombok.util.base.CoreBaseDao;
import fr.gouv.finances.lombok.util.persistance.ScrollIterator;

/**
 * Interface JournalDao --.
 * 
 * @author amleplatinec
 * @version $Revision: 1.2 $ Date: 11 déc. 2009
 */
public interface JournalDao extends CoreBaseDao
{
    /**
     * Positionne les séquences du composant journal / max des id dans les tables.
     */
    public void ajusterSequencesOracle();

    /**
     * Comptage du nombre de paramètres opérations orphelins dans la tabe PARAMETREOPERATION_PAOP.
     * 
     * @return the long
     */
    public Long countNombreParametresOrphelins();

    /**
     * Compte le nombre d'OperationJournal associées à un ParametreOperation.
     * 
     * @param paramOper --
     * @return the int
     */
    public int countOperationJournalAssocieAUnParametreOperation(ParametreOperation paramOper);

    /**
     * Supprime une opération du journal.
     * 
     * @param operationjournal --
     */
    public void deleteOperationJournal(OperationJournal operationjournal);

    /**
     * Recherche les opérations journal en fonction des critères de recherche.
     * 
     * @param crit --
     * @return the list< operation journal>
     */
    public List<OperationJournal> findOperationsJournalParCriteresRecherche(final CriteresRecherchesJournal crit);

    /**
     * retourne les opérations journal associées à unIdentifiantMetierObjOperation.
     * 
     * @param unIdentifiantMetierObjOperation --
     * @return the list< operation journal>
     */
    public List<OperationJournal> findOperationsJournalParIdentifiantMetierObjOperation(
        IdentifiantsMetierObjOperation unIdentifiantMetierObjOperation);

    /**
     * Retourne les opérations du journal qui sont périmées à une date passée en paramètre.
     * 
     * @param datelimite --
     * @return the list< operation journal>
     */
    public List<OperationJournal> findOperationsPerimees(String datelimite);

    /**
     * Retourne l'ensemble des ParametreOperation d'une OperationJournal.
     *
     * @param operationJournal --
     * @return the set< parametre operation>
     */
    public Set<ParametreOperation> findParametresOperations(OperationJournal operationJournal);

    /**
     * Recherche une opération du journal par l'identifiant de l'utilisateur et le moment d'enregistrement de
     * l'opération dans le journal.
     *
     * @param idUtilOuBatch --
     * @param dateHeureOperation --
     * @return une operationJournal ou null
     */
    public OperationJournal findUneOperationJournalParIdUtilEtDateHeure(String idUtilOuBatch, Date dateHeureOperation);

    /**
     * Recherche une opération journal par son id Hibernate.
     *
     * @param identifiant identiant
     * @return l'opérateur de journalisation
     */
    public OperationJournal findUneOperationParId(Long identifiant);

    /**
     * Initialise les ParametresOperation d'un OperationJournal.
     *
     * @param operationJournal --
     */
    public void initialiseParametresOperation(OperationJournal operationJournal);

    /**
     * Copie les tables du composant journal dans des tables temporaires.
     */
    public void sauvegardeTablesOperationJournalDansTablesTemporaires();

    /**
     * Sauvegarge un objet OperationJournal.
     * 
     * @param uneOperationJournal --
     */
    public void saveNouvelleOperationAuJournal(OperationJournal uneOperationJournal);

    /**
     * Suppression des copies des tables du composant journal.
     */
    public void suppressionCopieTablesOperationJournal();

    /**
     * Suppression des parametres opérations qui ne sont rattachés à aucune opération journal.
     */
    public void supprimerParametresOperationsOrphelins();

    /**
     * Retourne un iterateur sur toutes les éléments OperationJournal périmées.
     *
     * @param dateCourante --
     * @param typeDureeDeConservation --
     * @param joursDeRetention --
     * @return the dgcp scroll iterator
     */
    public ScrollIterator findOperationsPerimeesAUneDatePourUneDureeDeRetention(final Date dateCourante,
        final String typeDureeDeConservation, final int joursDeRetention);

    /**
     * Retourne un iterateur sur toutes les éléments OperationJournal.
     *
     * @return the dgcp scroll iterator
     */
    public ScrollIterator findAllOperationsJournal();

    /**
     * Teste si la table PARAMETREOPERATION_PAOP possède une contrainte unique.
     *
     * @return true, if test existence contrainte unique paop
     */
    public boolean testerExistenceContrainteUniquePAOP();

}
