/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : RechercheOperationsJournalForm.java
 *
 */
package fr.gouv.finances.lombok.journal.mvc.form;

import java.io.Serializable;
import java.util.Date;

/**
 * Class RechercheOperationsJournalForm
 * 
 * @author amleplatinec
 * @version $Revision: 1.2 $ Date: 11 déc. 2009
 */
public class RechercheOperationsJournalForm implements Serializable
{
    private static final long serialVersionUID = 1L;

    private Date dateDeRecherche;

    /** valeur identifiant metier obj operations. */
    private String valeurIdentifiantMetierObjOperations;

    public RechercheOperationsJournalForm()
    {
        super();
    }

    /**
     * Accesseur de l attribut date de recherche.
     * 
     * @return date de recherche
     */
    public Date getDateDeRecherche()
    {
        return dateDeRecherche;
    }

    /**
     * Modificateur de l attribut date de recherche.
     * 
     * @param dateDeRecherche le nouveau date de recherche
     */
    public void setDateDeRecherche(Date dateDeRecherche)
    {
        this.dateDeRecherche = dateDeRecherche;
    }

    /**
     * Accesseur de l attribut valeur identifiant metier obj operations.
     * 
     * @return valeur identifiant metier obj operations
     */
    public String getValeurIdentifiantMetierObjOperations()
    {
        return valeurIdentifiantMetierObjOperations;
    }

    /**
     * Modificateur de l attribut valeur identifiant metier obj operations.
     * 
     * @param valeurIdentifiantMetierObjOperations le nouveau valeur identifiant metier obj operations
     */
    public void setValeurIdentifiantMetierObjOperations(String valeurIdentifiantMetierObjOperations)
    {
        this.valeurIdentifiantMetierObjOperations = valeurIdentifiantMetierObjOperations;
    }

}
