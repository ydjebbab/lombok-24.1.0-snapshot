/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 */
package fr.gouv.finances.lombok.journal.bean;

import java.util.Objects;

import javax.xml.bind.annotation.XmlType;

import fr.gouv.finances.lombok.util.base.BaseBean;

/**
 * Entité du paramétrage des opérations de journalisation.
 * 
 * @author amleplatinec
 * @author Christophe Breheret-Girardin
 */
@XmlType(namespace = "http://journal.lombok.finances.gouv.fr/bean")
public class ParametreOperation extends BaseBean
{

    /** Initialisation de l'UID. */
    private static final long serialVersionUID = 1L;

    /** Identifiant. */
    private Long id;

    /** Version. */
    private int version;

    /** Nom du paramètre. */
    private String nom;

    /** Valeur du paramètre. */
    private String valeur;

    /** Valeur précédente du paramètre. */
    private String valeurPrecedente;

    /** Numéro d'incrément : ordre d'insertion dans operation journal. */
    private Long numIncrement;

    /**
     * Constructeur.
     */
    public ParametreOperation()
    {
        super();
    }

    /**
     * Constructeur.
     * 
     * @param nom Nom du paramètre
     * @param valeur Valeur du paramètre
     */
    public ParametreOperation(String nom, String valeur)
    {
        super();
        this.nom = nom;
        this.valeur = valeur;
    }

    /**
     * Accesseur de l'attribut id.
     * 
     * @return id
     */
    public Long getId()
    {
        return id;
    }

    /**
     * Accesseur de l'attribut nom.
     * 
     * @return nom
     */
    public String getNom()
    {
        return nom;
    }

    /**
     * Accesseur de numIncrement
     * 
     * @return the numIncrement - Long, ordre d'insertion dans operation journal
     */
    public Long getNumIncrement()
    {
        return numIncrement;
    }

    /**
     * Accesseur de l'attribut valeur.
     * 
     * @return valeur
     */
    public String getValeur()
    {
        return valeur;
    }

    /**
     * Accesseur de l'attribut valeur precedente.
     * 
     * @return valeur precedente
     */
    public String getValeurPrecedente()
    {
        return valeurPrecedente;
    }

    /**
     * Accesseur de l'attribut version.
     * 
     * @return version
     */
    public int getVersion()
    {
        return version;
    }

    /**
     * Modificateur de l'attribut id.
     * 
     * @param id le nouveau id
     */
    public void setId(Long id)
    {
        this.id = id;
    }

    /**
     * Modificateur de l'attribut nom.
     * 
     * @param nom le nouveau nom
     */
    public void setNom(String nom)
    {
        this.nom = nom;
    }

    /**
     * Modificateur de numIncrement
     * 
     * @param numIncrement the new numIncrement - Long, ordre d'insertion dans operation journal
     */
    public void setNumIncrement(Long numIncrement)
    {
        this.numIncrement = numIncrement;
    }

    /**
     * Modificateur de l'attribut valeur.
     * 
     * @param valeur le nouveau valeur
     */
    public void setValeur(String valeur)
    {
        this.valeur = valeur;
    }

    /**
     * Modificateur de l'attribut valeur precedente.
     * 
     * @param valeurPrecedente le nouveau valeur precedente
     */
    public void setValeurPrecedente(String valeurPrecedente)
    {
        this.valeurPrecedente = valeurPrecedente;
    }

    /**
     * Modificateur de l'attribut version.
     * 
     * @param version le nouveau version
     */
    public void setVersion(int version)
    {
        this.version = version;
    }

    /**
     * {@inheritDoc}
     * 
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode()
    {
        return Objects.hash(nom, valeur, valeurPrecedente);
    }

    /**
     * {@inheritDoc}
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object object)
    {
        if (object == null)
        {
            return false;
        }

        if (this.getClass() != object.getClass())
        {
            return false;
        }

        ParametreOperation parametreOperation = (ParametreOperation) object;
        return Objects.equals(nom, parametreOperation.nom)
            && Objects.equals(valeur, parametreOperation.valeur)
            && Objects.equals(valeurPrecedente, parametreOperation.valeurPrecedente);
    }

}
