package fr.gouv.finances.lombok.autoconfig;

/**
 * 
 * @author celfer
 * Date: Feb 19, 2020
 */
public enum DureeConservation
{

    TRESCOURT, COURT, NORMAL, LONG, TRESLONG;
}
