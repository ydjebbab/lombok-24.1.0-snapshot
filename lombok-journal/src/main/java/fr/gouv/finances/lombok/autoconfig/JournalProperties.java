package fr.gouv.finances.lombok.autoconfig;

import org.springframework.boot.context.properties.ConfigurationProperties;

import fr.gouv.finances.lombok.autoconfig.LombokComposantProperties;

/**
 * @author celinio fernandes Date: Feb 19, 2020
 */
@ConfigurationProperties(prefix = "lombok.composant.journal")
public class JournalProperties extends LombokComposantProperties
{

    private int nombrejours;

    private String administrateur;

    private DureeConservation trescourt;

    private DureeConservation court;

    private DureeConservation normal;

    private DureeConservation longu;

    private DureeConservation treslong;

    private int inttrescourt;

    private int intcourt;

    private int intnormal;

    private int intlongu;

    private int inttreslong;

    public int getNombrejours()
    {
        return nombrejours;
    }

    public void setNombrejours(int nombrejours)
    {
        this.nombrejours = nombrejours;
    }

    public String getAdministrateur()
    {
        return administrateur;
    }

    public void setAdministrateur(String administrateur)
    {
        this.administrateur = administrateur;
    }

    public DureeConservation getTrescourt()
    {
        return trescourt;
    }

    public void setTrescourt(DureeConservation trescourt)
    {
        this.trescourt = trescourt;
    }

    public DureeConservation getCourt()
    {
        return court;
    }

    public void setCourt(DureeConservation court)
    {
        this.court = court;
    }

    public DureeConservation getNormal()
    {
        return normal;
    }

    public void setNormal(DureeConservation normal)
    {
        this.normal = normal;
    }

    public DureeConservation getLongu()
    {
        return longu;
    }

    public void setLongu(DureeConservation longu)
    {
        this.longu = longu;
    }

    public DureeConservation getTreslong()
    {
        return treslong;
    }

    public void setTreslong(DureeConservation treslong)
    {
        this.treslong = treslong;
    }

    public int getInttrescourt()
    {
        return inttrescourt;
    }

    public void setInttrescourt(int inttrescourt)
    {
        this.inttrescourt = inttrescourt;
    }

    public int getIntcourt()
    {
        return intcourt;
    }

    public void setIntcourt(int intcourt)
    {
        this.intcourt = intcourt;
    }

    public int getIntnormal()
    {
        return intnormal;
    }

    public void setIntnormal(int intnormal)
    {
        this.intnormal = intnormal;
    }

    public int getIntlongu()
    {
        return intlongu;
    }

    public void setIntlongu(int intlongu)
    {
        this.intlongu = intlongu;
    }

    public int getInttreslong()
    {
        return inttreslong;
    }

    public void setInttreslong(int inttreslong)
    {
        this.inttreslong = inttreslong;
    }

    @Override
    public String toString()
    {
        return "JournalProperties [inclus=" + inclus + ", nombrejours=" + nombrejours + ", administrateur=" + administrateur
            + ", trescourt="
            + trescourt + ", court=" + court + ", normal=" + normal + ", longu=" + longu + ", treslong=" + treslong + ", inttrescourt="
            + inttrescourt + ", intcourt=" + intcourt + ", intnormal=" + intnormal + ", intlongu=" + intlongu + ", inttreslong="
            + inttreslong + "]";
    }

}
