/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : ParametreOperationDao.java
 *
 */
package fr.gouv.finances.lombok.journal.dao;

import fr.gouv.finances.lombok.journal.bean.ParametreOperation;
import fr.gouv.finances.lombok.util.base.CoreBaseDao;

/**
 * <pre>
 * Interface fournissant des services de persistance pour les parametres associes aux
 * operations dans le journal.
 * Utiliser de preference l'interface du service metier JournalService.
 * </pre>
 * 
 * @see fr.gouv.finances.lombok.journal.service.JournalService
 * @author amleplatinec-cp
 * @version $Revision: 1.2 $ Date: 15 déc. 2009
 */
public interface ParametreOperationDao extends CoreBaseDao
{

    /**
     * suppresion du parametre operation.
     * 
     * @param unParametreOperation --
     */
    public void deleteParametreOperation(ParametreOperation unParametreOperation);

    /**
     * recherche du parametre à partir de son nom.
     * 
     * @param nom --
     * @return the parametre operation
     */
    public ParametreOperation findParametreOperationParNom(String nom);

    /**
     * recherche du parametre à partir de son nom et de sa valeur.
     * 
     * @param nom --
     * @param valeur --
     * @return the parametre operation
     */
    public ParametreOperation findParametreOperationParNomEtValeur(String nom, String valeur);

    /**
     * sauveagrde du parametre.
     * 
     * @param unParametreOperation --
     */
    public void saveParametreOperation(ParametreOperation unParametreOperation);

}
