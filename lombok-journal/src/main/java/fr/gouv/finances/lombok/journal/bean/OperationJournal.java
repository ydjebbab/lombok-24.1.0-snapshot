/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 *
 */
package fr.gouv.finances.lombok.journal.bean;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.apache.commons.beanutils.BeanComparator;
import org.apache.commons.collections.comparators.ComparatorChain;

import fr.gouv.finances.lombok.util.base.BaseBean;
import fr.gouv.finances.lombok.util.comparators.NullSafeBeanComparator;
import fr.gouv.finances.lombok.util.comparators.NullorNotComparableComparator;
import fr.gouv.finances.lombok.util.ws.DateAdapter;

/**
 * Entité d'une opération de journalisation.
 * 
 * @author amleplatinec
 * @author Christophe Breheret-Girardin
 */
@XmlType(namespace = "http://journal.lombok.finances.gouv.fr/bean")
public class OperationJournal extends BaseBean
{

    /** Initialisation de l'UID. */
    private static final long serialVersionUID = 1L;

    /** Identifiant. */
    private Long id;

    /** VIersion. */
    private int version;

    /** Date heure de création de l'operation. */
    private Date dateHeureOperation;

    /** Identifiant créant l'opération. */
    private String identifiantUtilisateurOuBatch;

    /** Nature de l'opération (exemple : création). */
    private String natureOperation;

    /** Identifiant impacté (exemple : un codique). */
    private String identifiantStructure;

    /** Structure d'affectation de l'utilisateur à l'origine de l'opération. */
    private String structureUtilisateur;

    /** Liste des paramètres rattachés à l'opération. */
    private Set<ParametreOperation> lesParamOperation = new HashSet<>();

    /** Durée de conservation avant effacement de l'opération. */
    private String typeDureeDeConservation;

    /** Indique la possibilité d'apurer (même si delai de conservation est expiré). */
    private Boolean apurementPossible;

    /** Code de l'opération(est complémentaire de la nature). */
    private String codeOperation;

    /** Identifiants métiers de l'operation. */
    private IdentifiantsMetierObjOperation unIdentifiantsMetierObjOperation;

    /**
     * Constructeur.
     */
    public OperationJournal()
    {
        super();
    }

    /**
     * Constructeur.
     *
     * @param dateHeureOperation date et heure de l'opération
     * @param identifiantUtilisateurOuBatch identifiant de l'utilisateur ou du batch
     */
    public OperationJournal(Date dateHeureOperation, String identifiantUtilisateurOuBatch)
    {
        super();
        this.dateHeureOperation = dateHeureOperation;
        this.identifiantUtilisateurOuBatch = identifiantUtilisateurOuBatch;
    }

   

    /**
     * Accesseur de l attribut apurement possible.
     * 
     * @return apurement possible
     */
    public Boolean getApurementPossible()
    {
        return apurementPossible;
    }

    /**
     * Accesseur de l attribut code operation.
     * 
     * @return code operation
     */
    public String getCodeOperation()
    {
        return codeOperation;
    }

    /**
     * Accesseur de l attribut date heure operation.
     * 
     * @return date heure operation
     */
    @XmlJavaTypeAdapter(value = DateAdapter.class)
    @XmlSchemaType(name = "dateTime")
    public Date getDateHeureOperation()
    {
        return dateHeureOperation;
    }

    /**
     * Accesseur de l attribut id.
     * 
     * @return id
     */
    public Long getId()
    {
        return id;
    }

    /**
     * Accesseur de l attribut identifiant structure.
     * 
     * @return identifiant structure
     */
    public String getIdentifiantStructure()
    {
        return identifiantStructure;
    }

    /**
     * Accesseur de l attribut structure utilisateur.
     * 
     * @return structure utilisateur
     */
    public String getStructureUtilisateur()
    {
        return structureUtilisateur;
    }

    /**
     * Accesseur de l attribut identifiant utilisateur ou batch.
     * 
     * @return identifiant utilisateur ou batch
     */
    public String getIdentifiantUtilisateurOuBatch()
    {
        return identifiantUtilisateurOuBatch;
    }

    /**
     * Gets the les param operation.
     * 
     * @return the lesParamOperation
     */
    public Set<ParametreOperation> getLesParamOperation()
    {
        return lesParamOperation;
    }

    /**
     * Accesseur de l attribut nature operation.
     * 
     * @return nature operation
     */
    public String getNatureOperation()
    {
        return natureOperation;
    }

    /**
     * Accesseur de l attribut type duree de conservation.
     * 
     * @return type duree de conservation
     */
    public String getTypeDureeDeConservation()
    {
        return typeDureeDeConservation;
    }

    /**
     * Accesseur de l attribut un identifiants metier obj operation.
     * 
     * @return un identifiants metier obj operation
     */
    public IdentifiantsMetierObjOperation getUnIdentifiantsMetierObjOperation()
    {
        return unIdentifiantsMetierObjOperation;
    }

    /**
     * Accesseur de l attribut version.
     * 
     * @return version
     */
    public int getVersion()
    {
        return version;
    }

    /**
     * Sets the apurement possible.
     * 
     * @param apurementPossible --
     */
    public void setApurementPossible(Boolean apurementPossible)
    {
        this.apurementPossible = apurementPossible;
    }

    /**
     * Sets the code operation.
     * 
     * @param codeOperation --
     */
    public void setCodeOperation(String codeOperation)
    {
        this.codeOperation = codeOperation;
    }

    /**
     * Modificateur de l attribut date heure operation.
     * 
     * @param dateheureoperation le nouveau date heure operation
     */
    public void setDateHeureOperation(Date dateheureoperation)
    {
        this.dateHeureOperation = dateheureoperation;
    }

    /**
     * Modificateur de l attribut id.
     * 
     * @param id le nouveau id
     */
    public void setId(Long id)
    {
        this.id = id;
    }

    /**
     * Sets the identifiant structure.
     * 
     * @param identifiantStructure --
     */
    public void setIdentifiantStructure(String identifiantStructure)
    {
        this.identifiantStructure = identifiantStructure;
    }

    /**
     * Modificateur de l'attribut structureUtilisateur
     * 
     * @param structureUtilisateur --
     */
    public void setStructureUtilisateur(String structureUtilisateur)
    {
        this.structureUtilisateur = structureUtilisateur;
    }

    /**
     * Modificateur de l attribut identifiant utilisateur ou batch.
     * 
     * @param identifiantutilisateuroubatch le nouveau identifiant utilisateur ou batch
     */
    public void setIdentifiantUtilisateurOuBatch(String identifiantutilisateuroubatch)
    {
        this.identifiantUtilisateurOuBatch = identifiantutilisateuroubatch;
    }

    /**
     * Sets the les param operation.
     * 
     * @param lesParamOperation the lesParamOperation to set
     */
    public void setLesParamOperation(Set<ParametreOperation> lesParamOperation)
    {
        this.lesParamOperation = lesParamOperation;
    }

    /**
     * Modificateur de l attribut nature operation.
     * 
     * @param natureoperation le nouveau nature operation
     */
    public void setNatureOperation(String natureoperation)
    {
        this.natureOperation = natureoperation;
    }

    /**
     * Modificateur de l attribut type duree de conservation.
     * 
     * @param typedureedeconservation le nouveau type duree de conservation
     */
    public void setTypeDureeDeConservation(String typedureedeconservation)
    {
        this.typeDureeDeConservation = typedureedeconservation;
    }

    /**
     * Sets the un identifiants metier obj operation.
     * 
     * @param unIdentifiantsMetierObjOperation --
     */
    public void setUnIdentifiantsMetierObjOperation(IdentifiantsMetierObjOperation unIdentifiantsMetierObjOperation)
    {
        this.unIdentifiantsMetierObjOperation = unIdentifiantsMetierObjOperation;
    }

    /**
     * Modificateur de l attribut version.
     * 
     * @param version le nouveau version
     */
    public void setVersion(int version)
    {
        this.version = version;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @return string
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        Map<String, Object> hashT = new HashMap<>();
        Class cls = getClass();
        java.lang.reflect.Field[] field = cls.getDeclaredFields();
        // jdk1.2
        java.lang.reflect.AccessibleObject.setAccessible(field, true);
        for (int i = 0; i < field.length; i++)
        {
            try
            {
                Object attr = field[i].get(this);
                if (attr instanceof Number || attr instanceof String || attr instanceof Boolean || attr instanceof Date)
                {
                    if (field[i].get(this) != null)
                    {
                        hashT.put(field[i].getName(), field[i].get(this));
                    }
                    else
                    {
                        hashT.put(field[i].getName(), "null");
                    }
                }
            }
            catch (IllegalArgumentException | IllegalAccessException exception)
            {
                log.error("ERREUR : ", exception);
            }
        }

        return cls.getName() + hashT;
    }

    /**
     * Méthode qui permettra de trier les parametres operations de l'operation journal selon le numIncrement croissant.
     * 
     * @return list
     */
    public List<ParametreOperation> triLesParamOperDansOperationJournalSurNumIncrement()
    {

        List<ParametreOperation> lesParaOperEnList = new ArrayList<>();
        for (Iterator<ParametreOperation> iter = lesParamOperation.iterator(); iter.hasNext();)
        {
            lesParaOperEnList.add((ParametreOperation) iter.next());
            // Comparator.

        }
        List<BeanComparator> sortFields = new ArrayList<>();
        sortFields.add(new NullSafeBeanComparator("numIncrement",
            new NullorNotComparableComparator()));
        ComparatorChain multiSort = new ComparatorChain(sortFields);
        Collections.sort(lesParaOperEnList, multiSort);
        return lesParaOperEnList;
    }
    
    /** 
     * {@inheritDoc}
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode()
    {
        return Objects.hash(dateHeureOperation, identifiantUtilisateurOuBatch);
    }
    
    /** 
     * {@inheritDoc}
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object object)
    {
        if (object == null)
        {
            return false;
        }

        if (this.getClass() != object.getClass())
        {
            return false;
        }

        OperationJournal operationJournal = (OperationJournal) object;
        return Objects.equals(identifiantUtilisateurOuBatch, operationJournal.identifiantUtilisateurOuBatch)
            && Objects.equals(dateHeureOperation, operationJournal.dateHeureOperation);
    }

}
