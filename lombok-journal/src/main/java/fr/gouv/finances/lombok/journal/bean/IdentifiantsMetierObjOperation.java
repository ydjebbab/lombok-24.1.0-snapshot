/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 *
 */
package fr.gouv.finances.lombok.journal.bean;

import java.util.Objects;

import javax.xml.bind.annotation.XmlType;

import fr.gouv.finances.lombok.util.base.BaseBean;

/**
 * Identifiant métier lié aux opérations de journalisation.
 *
 * @author amleplatinec
 * @author Christophe Breheret-Girardin
 */
@XmlType(namespace = "http://journal.lombok.finances.gouv.fr/bean")
public class IdentifiantsMetierObjOperation extends BaseBean
{

    /** Initialisation de l'UID. */
    private static final long serialVersionUID = 1L;

    /** Valeur identifiant métier 1. */
    private String valeurIdentifiantMetier1;

    /** Valeur identifiant métier 2. */
    private String valeurIdentifiantMetier2;

    /** Valeur identifiant métier 3. */
    private String valeurIdentifiantMetier3;

    /** Valeur identifiant métier 4. */
    private String valeurIdentifiantMetier4;

    /** Valeur identifiant métier 5. */
    private String valeurIdentifiantMetier5;

    /** Libellé identifiant métier 1. */
    private String libelleIdentifiantMetier1;

    /** Libellé identifiant métier 2. */
    private String libelleIdentifiantMetier2;

    /** Libellé identifiant métier 3. */
    private String libelleIdentifiantMetier3;

    /** Libellé identifiant métier 4. */
    private String libelleIdentifiantMetier4;

    /** Libellé identifiant métier 5. */
    private String libelleIdentifiantMetier5;

    /**
     * Constructeur.
     */
    public IdentifiantsMetierObjOperation()
    {
        super();
    }

    /**
     * Accesseur de l'attribut libellé identifiant métier 1.
     * 
     * @return libellé identifiant métier 1
     */
    public String getLibelleIdentifiantMetier1()
    {
        return libelleIdentifiantMetier1;
    }

    /**
     * Accesseur de l'attribut libellé identifiant métier 2.
     * 
     * @return libellé identifiant métier 2
     */
    public String getLibelleIdentifiantMetier2()
    {
        return libelleIdentifiantMetier2;
    }

    /**
     * Accesseur de l'attribut libellé identifiant métier 3.
     * 
     * @return libellé identifiant métier 3
     */
    public String getLibelleIdentifiantMetier3()
    {
        return libelleIdentifiantMetier3;
    }

    /**
     * Accesseur de l'attribut libellé identifiant métier 4.
     * 
     * @return libellé identifiant métier 4
     */
    public String getLibelleIdentifiantMetier4()
    {
        return libelleIdentifiantMetier4;
    }

    /**
     * Accesseur de l'attribut libellé identifiant métier 5.
     * 
     * @return libellé identifiant métier 5
     */
    public String getLibelleIdentifiantMetier5()
    {
        return libelleIdentifiantMetier5;
    }

    /**
     * Accesseur de l'attribut valeur identifiant métier 1.
     * 
     * @return valeur identifiant métier 1
     */
    public String getValeurIdentifiantMetier1()
    {
        return valeurIdentifiantMetier1;
    }

    /**
     * Accesseur de l'attribut valeur identifiant métier 2.
     * 
     * @return valeur identifiant métier 2
     */
    public String getValeurIdentifiantMetier2()
    {
        return valeurIdentifiantMetier2;
    }

    /**
     * Accesseur de l'attribut valeur identifiant métier 3.
     * 
     * @return valeur identifiant métier 3
     */
    public String getValeurIdentifiantMetier3()
    {
        return valeurIdentifiantMetier3;
    }

    /**
     * Accesseur de l'attribut valeur identifiant métier 4.
     * 
     * @return valeur identifiant métier 4
     */
    public String getValeurIdentifiantMetier4()
    {
        return valeurIdentifiantMetier4;
    }

    /**
     * Accesseur de l'attribut valeur identifiant métier 5.
     * 
     * @return valeur identifiant métier 5
     */
    public String getValeurIdentifiantMetier5()
    {
        return valeurIdentifiantMetier5;
    }

    /**
     * Modificateur de l'attribut libellé identifiant métier 1.
     * 
     * @param libelleIdentifiantMetier1 le nouveau libellé identifiant métier 1
     */
    public void setLibelleIdentifiantMetier1(String libelleIdentifiantMetier1)
    {
        this.libelleIdentifiantMetier1 = libelleIdentifiantMetier1;
    }

    /**
     * Modificateur de l'attribut libellé identifiant métier 2.
     * 
     * @param libelleIdentifiantMetier2 le nouveau libellé identifiant métier 2
     */
    public void setLibelleIdentifiantMetier2(String libelleIdentifiantMetier2)
    {
        this.libelleIdentifiantMetier2 = libelleIdentifiantMetier2;
    }

    /**
     * Modificateur de l'attribut libellé identifiant métier 3.
     * 
     * @param libelleIdentifiantMetier3 le nouveau libellé identifiant métier 3
     */
    public void setLibelleIdentifiantMetier3(String libelleIdentifiantMetier3)
    {
        this.libelleIdentifiantMetier3 = libelleIdentifiantMetier3;
    }

    /**
     * Modificateur de l'attribut libellé identifiant métier 4.
     * 
     * @param libelleIdentifiantMetier4 le nouveau libellé identifiant métier 4
     */
    public void setLibelleIdentifiantMetier4(String libelleIdentifiantMetier4)
    {
        this.libelleIdentifiantMetier4 = libelleIdentifiantMetier4;
    }

    /**
     * Modificateur de l'attribut libellé identifiant métier 5.
     * 
     * @param libelleIdentifiantMetier5 le nouveau libellé identifiant métier 5
     */
    public void setLibelleIdentifiantMetier5(String libelleIdentifiantMetier5)
    {
        this.libelleIdentifiantMetier5 = libelleIdentifiantMetier5;
    }

    /**
     * Modificateur de l'attribut valeur identifiant métier 1.
     * 
     * @param valeurIdentifiantMetier1 le nouveau valeur identifiant métier 1
     */
    public void setValeurIdentifiantMetier1(String valeurIdentifiantMetier1)
    {
        this.valeurIdentifiantMetier1 = valeurIdentifiantMetier1;
    }

    /**
     * Modificateur de l'attribut valeur identifiant métier 2.
     * 
     * @param valeurIdentifiantMetier2 le nouveau valeur identifiant métier 2
     */
    public void setValeurIdentifiantMetier2(String valeurIdentifiantMetier2)
    {
        this.valeurIdentifiantMetier2 = valeurIdentifiantMetier2;
    }

    /**
     * Modificateur de l'attribut valeur identifiant métier 3.
     * 
     * @param valeurIdentifiantMetier3 le nouveau valeur identifiant métier 3
     */
    public void setValeurIdentifiantMetier3(String valeurIdentifiantMetier3)
    {
        this.valeurIdentifiantMetier3 = valeurIdentifiantMetier3;
    }

    /**
     * Modificateur de l'attribut valeur identifiant métier 4.
     * 
     * @param valeurIdentifiantMetier4 le nouveau valeur identifiant métier 4
     */
    public void setValeurIdentifiantMetier4(String valeurIdentifiantMetier4)
    {
        this.valeurIdentifiantMetier4 = valeurIdentifiantMetier4;
    }

    /**
     * Modificateur de l'attribut valeur identifiant métier 5.
     * 
     * @param valeurIdentifiantMetier5 le nouveau valeur identifiant métier 5
     */
    public void setValeurIdentifiantMetier5(String valeurIdentifiantMetier5)
    {
        this.valeurIdentifiantMetier5 = valeurIdentifiantMetier5;
    }

    /**
     * {@inheritDoc}
     * 
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode()
    {
        return Objects.hash(libelleIdentifiantMetier1, libelleIdentifiantMetier2, libelleIdentifiantMetier3, libelleIdentifiantMetier4,
            libelleIdentifiantMetier5, valeurIdentifiantMetier1, valeurIdentifiantMetier2, valeurIdentifiantMetier3,
            valeurIdentifiantMetier4, valeurIdentifiantMetier5);
    }

    /**
     * {@inheritDoc}
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object object)
    {
        if (object == null)
        {
            return false;
        }

        if (this.getClass() != object.getClass())
        {
            return false;
        }

        IdentifiantsMetierObjOperation identifiantsMetierObjOperation = (IdentifiantsMetierObjOperation) object;
        return Objects.equals(libelleIdentifiantMetier1, identifiantsMetierObjOperation.libelleIdentifiantMetier1)
            && Objects.equals(libelleIdentifiantMetier2, identifiantsMetierObjOperation.libelleIdentifiantMetier2)
            && Objects.equals(libelleIdentifiantMetier3, identifiantsMetierObjOperation.libelleIdentifiantMetier3)
            && Objects.equals(libelleIdentifiantMetier4, identifiantsMetierObjOperation.libelleIdentifiantMetier4)
            && Objects.equals(libelleIdentifiantMetier5, identifiantsMetierObjOperation.libelleIdentifiantMetier5)
            && Objects.equals(valeurIdentifiantMetier1, identifiantsMetierObjOperation.valeurIdentifiantMetier1)
            && Objects.equals(valeurIdentifiantMetier2, identifiantsMetierObjOperation.valeurIdentifiantMetier2)
            && Objects.equals(valeurIdentifiantMetier3, identifiantsMetierObjOperation.valeurIdentifiantMetier3)
            && Objects.equals(valeurIdentifiantMetier4, identifiantsMetierObjOperation.valeurIdentifiantMetier4)
            && Objects.equals(valeurIdentifiantMetier5, identifiantsMetierObjOperation.valeurIdentifiantMetier5);
    }

}
