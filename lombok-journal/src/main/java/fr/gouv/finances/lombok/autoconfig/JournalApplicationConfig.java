package fr.gouv.finances.lombok.autoconfig;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

/**
 * Classe de configuration qui permet de conditionner les ressources à importer
 * 
 * @author celfer Date: 6 févr. 2020
 */
@Configuration
@EnableConfigurationProperties(JournalProperties.class)
public class JournalApplicationConfig
{

    private static final Logger log = LoggerFactory.getLogger(JournalApplicationConfig.class);

    @Autowired
    private JournalProperties journalProperties;

    @PostConstruct
    private void afficherProprietes()
    {
       // log.info("Valeurs des propriétés du composant journal :  " + journalProperties.toString());      
        if (log.isInfoEnabled())
        {
            log.info("Valeurs des propriétés du composant journal : {}  ", journalProperties);
        }
    }

    // matchIfMissing : (defaut : false)
    // si true alors si la propriété lombok.composant.journal n'est pas définie, le bean est chargé quand même, les
    // imports sont fait
    // si false alors si la propriété lombok.composant.journal n'est pas définie, le bean n'est pas chargé et les
    // imports ne sont pas fait
    // @ConditionalOnProperty(value = "lombok.composant.journal", havingValue = "true")
    @Configuration
    @ConditionalOnExpression("'${lombok.composant.journal.inclus}' == 'true' and '${lombok.composant.itm.inclus}' == 'false'")
    @ImportResource({"classpath*:conf/applicationContext-journal-dao.xml",
            "classpath:conf/applicationContext-journal-service.xml"})
    @ComponentScan(basePackages = {"fr.gouv.finances.lombok.journal.jpa.dao.impl"})
    public static class JournalSansItmApplicationConfig
    {
        private static final Logger log = LoggerFactory.getLogger(JournalSansItmApplicationConfig.class);

        @PostConstruct
        public void init()
        {
            log.debug(">>> Après initialisation de JournalSansItmApplicationConfig");
        }

    }

    @Configuration
    @ConditionalOnProperty(name = "lombok.composant.journalws.inclus", havingValue = "true")
    @ImportResource({"classpath:conf/applicationContext-journal-ws-client.xml", "classpath:conf/applicationContext-journal-ws.xml"})
    @ComponentScan(basePackages = {"fr.gouv.finances.lombok.journal.jpa.dao.impl"})
    public static class JournalWSApplicationConfig
    {
        private static final Logger log = LoggerFactory.getLogger(JournalWSApplicationConfig.class);

        @PostConstruct
        public void init()
        {
            log.debug(">>> Après initialisation de JournalWSApplicationConfig");
        }

    }

    @Configuration
    @ConditionalOnExpression("'${lombok.composant.journal.inclus}' == 'true' and '${lombok.composant.itm.inclus}' == 'true'")
    @ImportResource({"classpath*:conf/applicationContext-journal-dao.xml", "classpath:conf/applicationContext-journal-service-itm.xml",
            "classpath*:conf/applicationContext-itm-service.xml"})
    @ComponentScan(basePackages = {"fr.gouv.finances.lombok.journal.jpa.dao.impl"})
    public static class JournalAvecItmApplicationConfig
    {
        private static final Logger log = LoggerFactory.getLogger(JournalAvecItmApplicationConfig.class);

        @PostConstruct
        public void init()
        {
            log.debug(">>> Après initialisation de JournalAvecItmApplicationConfig");
        }
    }

}
