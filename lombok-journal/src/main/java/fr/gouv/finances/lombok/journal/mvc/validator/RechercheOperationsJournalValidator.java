/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : RechercheOperationsJournalValidator.java
 *
 *
 * Copyright 2002-2004 the original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package fr.gouv.finances.lombok.journal.mvc.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import fr.gouv.finances.lombok.journal.mvc.form.RechercheOperationsJournalForm;

/**
 * Class RechercheOperationsJournalValidator
 * 
 * @author amleplatinec
 * @version $Revision: 1.2 $ Date: 11 déc. 2009
 */
public class RechercheOperationsJournalValidator implements Validator
{

    public RechercheOperationsJournalValidator()
    {
        super();
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param clazz
     * @return true, si c'est vrai
     * @see org.springframework.validation.Validator#supports(java.lang.Class)
     */
    @Override
    public boolean supports(Class clazz)
    {
        return clazz.equals(RechercheOperationsJournalForm.class);
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param obj
     * @param errors
     * @see org.springframework.validation.Validator#validate(java.lang.Object, org.springframework.validation.Errors)
     */
    @Override
    public void validate(Object obj, Errors errors)
    {
        // RAS
    }

    /**
     * methode Validate surface critere recherche
     * 
     * @param command --
     * @param errors --
     */
    public void validateSurfaceCritereRecherche(RechercheOperationsJournalForm command, Errors errors)
    {
        /**
         * à compléter si on veut rendre des critères de recherche obligatoires
         */

    }
}