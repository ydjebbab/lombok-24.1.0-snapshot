/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 */
package fr.gouv.finances.lombok.journal.bean;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import fr.gouv.finances.lombok.util.base.BaseTechBean;
import fr.gouv.finances.lombok.util.ws.DateAdapter;

/**
 * Critères de recherche des opérations de journalisation.
 * 
 * @author amleplatinec
 * @author Christophe Breheret-Girardin
 */
@XmlType(namespace = "http://journal.lombok.finances.gouv.fr/bean")
public class CriteresRecherchesJournal extends BaseTechBean
{

    /**
     * Initialisation de l'UID.
     */
    private static final long serialVersionUID = -2814338910312051298L;

    /** Date et heure exacte de l'opération recherchée. */
    private Date dateHeureOperation;

    /** Date des opérations recherchées. */
    // sert pour trouver les opération au 24/11/2006 par exemple
    private Date dateJJMMAAAA;

    /** Date maximum des opérations recherchées. */
    private Date dateMin;

    /** Date minimum des opérations recherchées. */
    private Date dateMax;

    /** Identifiant de l'utilisateur ou du batch. */
    private String identifiantUtilisateurOuBatch;

    /** Nature de l'opération. */
    private String natureOperation;

    /** Identifiant Structure associé à l'operation. */
    private String identifiantStructure;

    /** Nom du paramètres opération des opérations recherchées. */
    private String nomParamOper;

    /** Valeur du paramètre opération des opérations recherchées. */
    private String valeurParamOper;

    /**
     * Valeur minimum d'un paramètre associé aux opérations recherchés.<br/>
     * Attention, il est necessaire de renseigner également le critère "nomParamOper" pour que ce critère soit pris en
     * compte.
     */
    private String valeurParamOperMin;

    /**
     * Valeur maximum d'un paramètre associé aux opérations recherchés.<br/>
     * Attention, il est necessaire de renseigner également le critère "nomParamOper" pour que ce critère soit pris en
     * compte.
     */
    private String valeurParamOperMax;

    /** Liste des natures opérations des opérations recherchées. */
    private List<String> naturesOperations = new ArrayList<>();

    /** Code operation de l 'opération recherchée. */
    private String codeOperation;

    /** Liste des codes opérations des opérations recherchées. */
    private List<String> codesOperation = new ArrayList<>();

    /** Recherche si l'apurement est possible ou non. */
    private Boolean apurementPossible;

    /**
     * Constructeur.
     */
    public CriteresRecherchesJournal()
    {
        super();
    }

    /**
     * Ajoute un code opération à codesOperation.
     * 
     * @param codeOperation --
     */
    public void ajouterUnCodeOperation(String codeOperation)
    {
        if (codeOperation != null)
        {
            if (codesOperation.isEmpty())
            {
                this.codeOperation = codeOperation;
            }
            this.codesOperation.add(codeOperation);
        }
    }

    /**
     * Ajoute une nature opération à.
     * 
     * @param natureOperation --
     */
    public void ajouterUneNatureOperation(String natureOperation)
    {
        if (natureOperation != null)
        {
            if (naturesOperations.isEmpty())
            {
                this.natureOperation = natureOperation;
            }
            this.naturesOperations.add(natureOperation);
        }
    }

    /**
     * Accesseur de l attribut apurement possible.
     * 
     * @return apurement possible
     */
    public Boolean getApurementPossible()
    {
        return apurementPossible;
    }

    /**
     * Accesseur de l attribut code operation.
     * 
     * @return code operation
     */
    public String getCodeOperation()
    {
        return codeOperation;
    }

    /**
     * Accesseur de l attribut codes operation.
     * 
     * @return codes operation
     */
    public List<String> getCodesOperation()
    {
        return codesOperation;
    }

    /**
     * Accesseur de l attribut date heure operation.
     * 
     * @return date heure operation
     */
    @XmlJavaTypeAdapter(value = DateAdapter.class)
    @XmlSchemaType(name = "dateTime")
    public Date getDateHeureOperation()
    {
        return dateHeureOperation;
    }

    /**
     * Accesseur de l attribut date jjmmaaaa.
     * 
     * @return date jjmmaaaa
     */
    @XmlJavaTypeAdapter(value = DateAdapter.class)
    @XmlSchemaType(name = "dateTime")
    public Date getDateJJMMAAAA()
    {
        return dateJJMMAAAA;
    }

    /**
     * Accesseur de l attribut date max.
     * 
     * @return date max
     */
    @XmlJavaTypeAdapter(value = DateAdapter.class)
    @XmlSchemaType(name = "dateTime")
    public Date getDateMax()
    {
        return dateMax;
    }

    /**
     * Accesseur de l attribut date min.
     * 
     * @return date min
     */
    @XmlJavaTypeAdapter(value = DateAdapter.class)
    @XmlSchemaType(name = "dateTime")
    public Date getDateMin()
    {
        return dateMin;
    }

    /**
     * Accesseur de l attribut identifiant structure.
     * 
     * @return identifiant structure
     */
    public String getIdentifiantStructure()
    {
        return identifiantStructure;
    }

    /**
     * Accesseur de l attribut identifiant utilisateur ou batch.
     * 
     * @return identifiant utilisateur ou batch
     */
    public String getIdentifiantUtilisateurOuBatch()
    {
        return identifiantUtilisateurOuBatch;
    }

    /**
     * Accesseur de l attribut nature operation.
     * 
     * @return nature operation
     */
    public String getNatureOperation()
    {
        return natureOperation;
    }

    /**
     * liste de natures operation.
     * 
     * @return the natures operations
     */
    public List<String> getNaturesOperations()
    {
        return naturesOperations;
    }

    /**
     * Accesseur de l attribut nom param oper.
     * 
     * @return nom param oper
     */
    public String getNomParamOper()
    {
        return nomParamOper;
    }

    /**
     * Accesseur de l attribut valeur param oper.
     * 
     * @return valeur param oper
     */
    public String getValeurParamOper()
    {
        return valeurParamOper;
    }

    /**
     * Accesseur de l attribut valeur param oper max.
     * 
     * @return valeur param oper max
     */
    public String getValeurParamOperMax()
    {
        return valeurParamOperMax;
    }

    /**
     * Accesseur de l attribut valeur param oper min.
     * 
     * @return valeur param oper min
     */
    public String getValeurParamOperMin()
    {
        return valeurParamOperMin;
    }

    /**
     * Modificateur de l attribut apurement possible.
     * 
     * @param apurementPossible le nouveau apurement possible
     */
    public void setApurementPossible(Boolean apurementPossible)
    {
        this.apurementPossible = apurementPossible;
    }

    /**
     * Modificateur de l attribut code operation.
     * 
     * @param codeOperation le nouveau code operation
     */
    public void setCodeOperation(String codeOperation)
    {
        this.codeOperation = codeOperation;
    }

    /**
     * Modificateur de l attribut codes operation.
     * 
     * @param codesOperation le nouveau codes operation
     */
    public void setCodesOperation(List<String> codesOperation)
    {
        this.codesOperation = codesOperation;
    }

    /**
     * Modificateur de l attribut date heure operation.
     * 
     * @param dateHeureOperation le nouveau date heure operation
     */
    public void setDateHeureOperation(Date dateHeureOperation)
    {
        this.dateHeureOperation = dateHeureOperation;
    }

    /**
     * Modificateur de l attribut date jjmmaaaa.
     * 
     * @param dateJJMMAAAA le nouveau date jjmmaaaa
     */
    public void setDateJJMMAAAA(Date dateJJMMAAAA)
    {
        this.dateJJMMAAAA = dateJJMMAAAA;
    }

    /**
     * Modificateur de l attribut date max.
     * 
     * @param dateMax le nouveau date max
     */
    public void setDateMax(Date dateMax)
    {
        this.dateMax = dateMax;
    }

    /**
     * Modificateur de l attribut date min.
     * 
     * @param dateMin le nouveau date min
     */
    public void setDateMin(Date dateMin)
    {
        this.dateMin = dateMin;
    }

    /**
     * Modificateur de l attribut identifiant structure.
     * 
     * @param identifiantStructure le nouveau identifiant structure
     */
    public void setIdentifiantStructure(String identifiantStructure)
    {
        this.identifiantStructure = identifiantStructure;
    }

    /**
     * Modificateur de l attribut identifiant utilisateur ou batch.
     * 
     * @param identifiantUtilisateurOuBatch le nouveau identifiant utilisateur ou batch
     */
    public void setIdentifiantUtilisateurOuBatch(String identifiantUtilisateurOuBatch)
    {
        this.identifiantUtilisateurOuBatch = identifiantUtilisateurOuBatch;
    }

    /**
     * Modificateur de l attribut nature operation.
     * 
     * @param natureOperation le nouveau nature operation
     */
    public void setNatureOperation(String natureOperation)
    {
        if (natureOperation != null)
        {
            this.natureOperation = natureOperation;
            this.naturesOperations.add(natureOperation);
        }
    }

    /**
     * Modificateur de l attribut natures operations.
     * 
     * @param naturesOperations le nouveau natures operations
     */
    public void setNaturesOperations(List<String> naturesOperations)
    {
        this.naturesOperations = naturesOperations;
    }

    /**
     * Modificateur de l attribut nom param oper.
     * 
     * @param nomParamOper le nouveau nom param oper
     */
    public void setNomParamOper(String nomParamOper)
    {
        this.nomParamOper = nomParamOper;
    }

    /**
     * Modificateur de l attribut valeur param oper.
     * 
     * @param valeurParamOper le nouveau valeur param oper
     */
    public void setValeurParamOper(String valeurParamOper)
    {
        this.valeurParamOper = valeurParamOper;
    }

    /**
     * Modificateur de l attribut valeur param oper max.
     * 
     * @param valeurParamOperMax le nouveau valeur param oper max
     */
    public void setValeurParamOperMax(String valeurParamOperMax)
    {
        this.valeurParamOperMax = valeurParamOperMax;
    }

    /**
     * Modificateur de l attribut valeur param oper min.
     * 
     * @param valeurParamOperMin le nouveau valeur param oper min
     */
    public void setValeurParamOperMin(String valeurParamOperMin)
    {
        this.valeurParamOperMin = valeurParamOperMin;
    }

}