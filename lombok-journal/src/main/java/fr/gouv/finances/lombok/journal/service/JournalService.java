/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : JournalService.java
 *
 */
package fr.gouv.finances.lombok.journal.service;

import java.util.Date;
import java.util.List;
import java.util.Set;

import fr.gouv.finances.lombok.journal.bean.CriteresRecherchesJournal;
import fr.gouv.finances.lombok.journal.bean.IdentifiantsMetierObjOperation;
import fr.gouv.finances.lombok.journal.bean.OperationJournal;
import fr.gouv.finances.lombok.journal.bean.ParametreOperation;

/**
 * Interface fournissant des services metiers pour les operations dans le journal.
 * 
 * @author amleplatinec-cp
 * @version $Revision: 1.2 $ Date: 15 déc. 2009
 */
public interface JournalService
{

    /**
     * Ajouter toute une série d'attributs à l'opération journal.
     * 
     * @param idutiloubatch --
     * @param lesParamOperation --
     * @param natureOperation --
     * @param identifiantStructure --
     * @param dureedeconservation --
     * @param unIdentifiantsMetierObjOperation --
     * @param codeOperation --
     * @param apurementPossible --
     * @return the operation journal
     */
    public OperationJournal ajouterUneOperationAuJournal(String idutiloubatch,
        Set<ParametreOperation> lesParamOperation, String natureOperation, String identifiantStructure,
        String dureedeconservation, IdentifiantsMetierObjOperation unIdentifiantsMetierObjOperation,
        String codeOperation, boolean apurementPossible);

    /**
     * Migration du schéma du composant OpérationJournal V15.
     */
    public void ajusteSequencesOperationJournal();

    /**
     * Efface les opérations journal périmées.
     */
    public void effacerLesOperationPerimees();

    /**
     * Retourne une chaîne désignant une durée de conservation des opérations courte.
     * 
     * @return the court
     */
    public String getCourt();

    /**
     * Retourne une chaîne désignant une durée de conservation des opérations longue.
     * 
     * @return the longu
     */
    public String getLongu();

    /**
     * Retourne une chaîne désignant une durée de conservation des opérations normale.
     * 
     * @return the normal
     */
    public String getNormal();

    /**
     * Retourne une chaîne désignant une durée de conservation des opérations très courte.
     * 
     * @return the trescourt
     */
    public String getTrescourt();

    /**
     * Retourne une chaîne désignant une durée de conservation des opérations très longue.
     * 
     * @return the treslong
     */
    public String getTreslong();

    /**
     * Permet de rechercher des operations dans le journal par criteres multiples en passant un objet de type
     * CriteresRecherchesJournal Voir la documentation sur cette classe.
     * 
     * @param crit --
     * @return the list
     * @see fr.gouv.finances.lombok.journal.bean.CriteresRecherchesJournal
     */
    public List rechercherLesOperationsJournalParCriteresRecherche(CriteresRecherchesJournal crit);

    /**
     * retourne les opérations journal associées à unIdentifiantMetierObjOperation.
     * 
     * @param unIdentifiantMetierObjOperation --
     * @return the list< operation journal>
     */
    public List<OperationJournal> rechercherOperationsJournalParIdentifiantMetierObjOperation(
        IdentifiantsMetierObjOperation unIdentifiantMetierObjOperation);

    /**
     * Recherche une opération jounral par son identifiantUtilisateur et le moment d'enregistrement de l'opération dans
     * le journal.
     * 
     * @param idUtilOuBatch --
     * @param dateHeureOperation --
     * @return the operation journal
     */
    public OperationJournal rechercheUneOperationJournalParIdUtilEtDateHeure(String idUtilOuBatch,
        Date dateHeureOperation);

    /**
     * Migration des données du journal V15.
     */
    public void recreationParametresOperations();

    /**
     * Supprime une opération du journal.
     * 
     * @param operationJournal --
     */
    public void supprimerUneOperationJournal(OperationJournal operationJournal);

    /**
     * transfert des oprations journal dans ITM
     */
    public int tranfererToutesLesOperationsJournalDansITM();

}
