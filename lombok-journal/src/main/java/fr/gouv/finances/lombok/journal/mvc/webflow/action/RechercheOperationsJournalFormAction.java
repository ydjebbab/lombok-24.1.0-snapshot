/*
 * Copyright : Ministère de l'Economie, des Finances et de l'Industrie - France
 * Contributeur(s) : 
 * - amleplatinec
 *
 * Projet ${projet} - lombok.framework
 *
 * fichier : RechercheOperationsJournalFormAction.java
 *
 */
package fr.gouv.finances.lombok.journal.mvc.webflow.action;

import java.beans.PropertyEditor;
import java.util.Date;
import java.util.List;

import org.springframework.beans.PropertyEditorRegistry;
import org.springframework.webflow.action.FormAction;
import org.springframework.webflow.execution.Event;
import org.springframework.webflow.execution.RequestContext;

import fr.gouv.finances.lombok.apptags.util.CheckboxSelectUtil;
import fr.gouv.finances.lombok.journal.bean.CriteresRecherchesJournal;
import fr.gouv.finances.lombok.journal.bean.IdentifiantsMetierObjOperation;
import fr.gouv.finances.lombok.journal.bean.OperationJournal;
import fr.gouv.finances.lombok.journal.mvc.form.RechercheOperationsJournalForm;
import fr.gouv.finances.lombok.journal.service.JournalService;
import fr.gouv.finances.lombok.util.propertyeditor.CpCustomDateEditor;
import fr.gouv.finances.lombok.util.propertyeditor.SelectEditor;

/**
 * Class RechercheOperationsJournalFormAction
 * 
 * @author amleplatinec
 * @version $Revision: 1.4 $ Date: 11 déc. 2009
 */
public class RechercheOperationsJournalFormAction extends FormAction
{

    /** Constant : COLLECTION_OPERATIONSJOURNAL. */
    public static final String COLLECTION_OPERATIONSJOURNAL = "listeoperationsjournal";

    /** journalserviceso. */
    private JournalService journalserviceso;

    /**
     * Constructeur
     */
    public RechercheOperationsJournalFormAction()
    {
        super();
    }

    /**
     * Constructeur
     *
     * @param formObjectClass
     */
    public RechercheOperationsJournalFormAction(Class<?> formObjectClass)
    {
        super(formObjectClass);

    }

    /**
     * methode Executer rechercher operations journal
     * 
     * @param rc --
     * @return event
     * @throws Exception the exception
     */
    public Event executerRechercherOperationsJournal(RequestContext rc)
    {

        // récupérationde la date
        RechercheOperationsJournalForm dateform =
            (RechercheOperationsJournalForm) rc.getFlowScope().get(getFormObjectName());
        Date date = dateform.getDateDeRecherche();
        IdentifiantsMetierObjOperation unIdentifiantsMetierObjOperation = null;
        String valIdsMetierObjOperation = dateform.getValeurIdentifiantMetierObjOperations();

        if (valIdsMetierObjOperation != null
            && valIdsMetierObjOperation.compareToIgnoreCase("") != 0)
        {
            unIdentifiantsMetierObjOperation = new IdentifiantsMetierObjOperation();
            unIdentifiantsMetierObjOperation.setValeurIdentifiantMetier1(valIdsMetierObjOperation);
        }
        // à compléter des autres critéres de recherche éventuellment rajoutés
        // + les inscrire dans les CriteresRecherchesJournal

        List listeoperationsjournal = null;
        CriteresRecherchesJournal crit = new CriteresRecherchesJournal();

        if (date != null)
        {
            crit.setDateJJMMAAAA(date);
            listeoperationsjournal = journalserviceso.rechercherLesOperationsJournalParCriteresRecherche(crit);
        }
        else if (unIdentifiantsMetierObjOperation != null)
        {
            listeoperationsjournal =
                journalserviceso
                    .rechercherOperationsJournalParIdentifiantMetierObjOperation(unIdentifiantsMetierObjOperation);
        }

        rc.getFlowScope().put(COLLECTION_OPERATIONSJOURNAL, listeoperationsjournal);
        return success();
    }

    /**
     * Accesseur de l attribut journalserviceso.
     * 
     * @return journalserviceso
     */
    public JournalService getJournalserviceso()
    {
        return journalserviceso;
    }

    /**
     * methode Parametrer checkboxes
     * 
     * @param request --
     * @return event
     * @throws Exception the exception
     */
    public Event parametrerCheckboxes(RequestContext request)
    {
        // Sauvegarde de l'objet de paramétrage des checkboxes dans le FlowScope
        CheckboxSelectUtil.parametrerCheckboxes(request, "opjour").utiliserRowid("id").utiliserListeElements(
            COLLECTION_OPERATIONSJOURNAL);
        return success();
    }

    /**
     * methode Selectionner par un lien
     * 
     * @param rc --
     * @return event
     * @throws Exception the exception
     */
    public Event selectionnerParUnLien(RequestContext rc)
    {
        Event result;

        @SuppressWarnings("unchecked")
        List<OperationJournal> lesopjour = (List<OperationJournal>) rc.getFlowScope().get(COLLECTION_OPERATIONSJOURNAL);
        PropertyEditor propertyEditor = new SelectEditor(lesopjour, "id");

        propertyEditor.setAsText(rc.getRequestParameters().get("id"));
        OperationJournal uneOpJourSelectionne = (OperationJournal) propertyEditor.getValue();
        if (uneOpJourSelectionne != null)
        {
            rc.getFlowScope().put("lesParamOperation", uneOpJourSelectionne.triLesParamOperDansOperationJournalSurNumIncrement());

            result = success();
        }
        else
        {
            result = error();
        }

        return result;
    }

    /**
     * methode Executer rechercher operations journal
     * 
     * @param rc --
     * @return event
     * @throws Exception the exception
     */
    public Event executerTransfererOperationsJournalDansITM(RequestContext rc) throws Exception
    {
        journalserviceso.tranfererToutesLesOperationsJournalDansITM();
        return success();

    }

    /**
     * Modificateur de l attribut journalserviceso.
     * 
     * @param journalserviceso le nouveau journalserviceso
     */
    public void setJournalserviceso(JournalService journalserviceso)
    {
        this.journalserviceso = journalserviceso;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param context
     * @param registry
     * @see org.springframework.webflow.action.FormAction#registerPropertyEditors(org.springframework.webflow.execution.RequestContext,
     *      org.springframework.beans.PropertyEditorRegistry)
     */
    @Override
    protected void registerPropertyEditors(RequestContext context, PropertyEditorRegistry registry)
    {
        super.registerPropertyEditors(context, registry);
        registry.registerCustomEditor(Date.class, new CpCustomDateEditor());
    }

}
