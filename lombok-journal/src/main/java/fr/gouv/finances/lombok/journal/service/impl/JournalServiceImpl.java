/*
 * Copyright (c) 2017 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.journal.service.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import fr.gouv.finances.lombok.itm.service.ItmService;
import fr.gouv.finances.lombok.journal.bean.CriteresRecherchesJournal;
import fr.gouv.finances.lombok.journal.bean.IdentifiantsMetierObjOperation;
import fr.gouv.finances.lombok.journal.bean.OperationJournal;
import fr.gouv.finances.lombok.journal.bean.ParametreOperation;
import fr.gouv.finances.lombok.journal.dao.JournalDao;
import fr.gouv.finances.lombok.journal.dao.ParametreOperationDao;
import fr.gouv.finances.lombok.journal.service.JournalService;
import fr.gouv.finances.lombok.util.base.BaseServiceImpl;
import fr.gouv.finances.lombok.util.exception.ApplicationExceptionTransformateur;
import fr.gouv.finances.lombok.util.exception.ProgrammationException;
import fr.gouv.finances.lombok.util.persistance.ScrollIterator;
import fr.gouv.impots.appli.itmtgenmas.services.objetsvaleurs.metier.UtilisateurTypeSrvValeur;
import fr.gouv.impots.appli.itmtgenmas.services.objetsvaleurs.metier.enregistrementtraces.ExploitationSrvValeur;
import fr.gouv.impots.appli.itmtgenmas.services.offresservices.Erreur;
import fr.gouv.impots.appli.itmtgenmas.services.offresservices.objetsvaleurs.service.enregistrementtraces.reponse.ErreurReponseSrvValeur;
import fr.gouv.impots.appli.itmtgenmas.services.offresservices.objetsvaleurs.service.enregistrementtraces.requete.NtcSRequeteSrvValeur;

/**
 * Class JournalServiceImpl
 * 
 * @author amleplatinec
 * @author CF
 * @version $Revision: 1.5 $ Date: 11 déc. 2009
 */
public class JournalServiceImpl extends BaseServiceImpl implements JournalService
{
    private static final Logger log = LoggerFactory.getLogger(JournalServiceImpl.class);

    /** Constant : C1000. */
    private static final int C1000 = 1000;

    /*
     * Chaîne de caractères par défaut désignant une durée très courte de conservation d'une opération dans le journal.
     */
    /** Constant : TRESCOURT. */
    private static final String S_TRESCOURT = "TRESCOURT";

    /** Chaîne de caractères par défaut désignant une durée courte de conservation d'une opération dans le journal. */
    private static final String S_COURT = "COURT";

    /** Chaîne de caractères par défaut désignant une durée normale de conservation d'une opération dans le journal. */
    private static final String S_NORMAL = "NORMAL";

    /** Chaîne de caractères par défaut désignant une durée longue de conservation d'une opération dans le journal. */
    private static final String S_LONG = "LONG";

    /*
     * Chaîne de caractères par défaut désignant une durée très longue de conservation d'une opération dans le journal.
     */
    /** Constant : TRESLONG. */
    private static final String S_TRESLONG = "TRESLONG";

    /**
     * Nombre de jours par défaut de conservation d'une opération dans le journal lorsque la durée de conservation est
     * très courte.
     */
    private static final int DUREE_RETENTION_TRESCOURT = 7;

    /**
     * Nombre de jours par défaut de conservation d'une opération dans le journal lorsque la durée de conservation est
     * courte.
     */
    private static final int DUREE_RETENTION_COURT = 14;

    /**
     * Nombre de jours par défaut de conservation d'une opération dans le journal lorsque la durée de conservation est
     * normale.
     */
    private static final int DUREE_RETENTION_NORMAL = 30;

    /**
     * Nombre de jours par défaut de conservation d'une opération dans le journal lorsque la durée de conservation est
     * longue.
     */
    private static final int DUREE_RETENTION_LONG = 90;

    /**
     * Nombre de jours par défaut de conservation d'une opération dans le journal lorsque la durée de conservation est
     * très longue.
     */
    private static final int DUREE_RETENTION_TRESLONG = 360;

    /**
     * Valeur affectée à la propriété OperationJournal.identifiantUtilisateur lorsqu'aucune valeur n'est fournie au
     * service pour le paramètre idutiloubatch
     */
    private static final String IDENTIFIANT_UTILISATEUR_NON_SERVI = "identifiant utilisateur non servi";

    /**
     * Valeur affectée à la propriété OperationJournal.identifiantStructure lorsqu'aucune valeur n'est fournie au
     * service pour le paramètre idutiloubatch
     */
    private static final String IDENTIFIANT_STRUCTURE_BATCH = "identifiantStructureBatch";

    /**
     * Valeur affectée à la propriété OperationJournal.codeOperation lorsqu'aucune valeur n'est fournie au service pour
     * le paramètre codeOperation
     */
    private static final String CODE_OPERATION_NON_SERVI = "code operation non servi";

    /** journaldao. */
    private JournalDao journaldao;

    /** paramoperdao. */
    private ParametreOperationDao paramoperdao;

    /** transaction template. */
    private TransactionTemplate transactionTemplate;

    /** nombrejours. */
    private int nombrejours;

    /** administrateur. */
    private String administrateur;

    /** Chaîne de caractères désignant une durée très courte de conservation d'une opération dans le journal. */
    private String trescourt;

    /** Chaîne de caractères désignant une durée courte de conservation d'une opération dans le journal. */
    private String court;

    /** Chaîne de caractères désignant une durée normale de conservation d'une opération dans le journal. */
    private String normal;

    /** Chaîne de caractères désignant une durée longue de conservation d'une opération dans le journal. */
    private String longu;

    /** Chaîne de caractères désignant une durée très longue de conservation d'une opération dans le journal. */
    private String treslong;

    /**
     * Nombre de jours de conservation d'une opération dans le journal lorsque la durée de conservation est très courte.
     */
    private int inttrescourt;

    /** Nombre de jours de conservation d'une opération dans le journal lorsque la durée de conservation est courte. */
    private int intcourt;

    /** Nombre de jours de conservation d'une opération dans le journal lorsque la durée de conservation est normale. */
    private int intnormal;

    /** Nombre de jours de conservation d'une opération dans le journal lorsque la durée de conservation est longue. */
    private int intlongu;

    /**
     * Nombre de jours de conservation d'une opération dans le journal lorsque la durée de conservation est trsè longue.
     */
    private int inttreslong;

    /**
     * Etat sur le service permettant de garantir que chaque objet OperationJournal possède une propriété
     * dateheureOperation distincte.
     */
    private int nanoincrement = 1;

    /**
     * Constructeur
     */
    public JournalServiceImpl()
    {
        super();
        // Affectation des valeurs par défaut
        this.trescourt = S_TRESCOURT;
        this.court = S_COURT;
        this.normal = S_NORMAL;
        this.longu = S_LONG;
        this.treslong = S_TRESLONG;

        this.inttrescourt = DUREE_RETENTION_TRESCOURT;
        this.intcourt = DUREE_RETENTION_COURT;
        this.intnormal = DUREE_RETENTION_NORMAL;
        this.intlongu = DUREE_RETENTION_LONG;
        this.inttreslong = DUREE_RETENTION_TRESLONG;
    }

    /** itmserviceso. */
    private ItmService itmserviceso;

    /**
     * Gets the itmserviceso.
     *
     * @return the itmserviceso
     */
    public ItmService getItmserviceso()
    {
        return itmserviceso;
    }

    /**
     * Sets the itmserviceso.
     *
     * @param itmserviceso the new itmserviceso
     */
    public void setItmserviceso(ItmService itmserviceso)
    {
        this.itmserviceso = itmserviceso;
    }

    /**
     * <pre>
     * Permet d'ajouter une operation dans le journal en prenant en compte de nouveaux attributs (code
     * OPeration,apurementPOssible,unIdentifiantsMetierObjOperation).
     * La journalisation d'operation sert la plupart du temps à pouvoir retracer qui a fait quoi.
     * Elle est donc principalement utilisée sur les mises à jour ou sur la consultation de donnéees confidentielles ou soumises au controle de la CNIL.
     * </pre>
     * 
     * @param idutiloubatch identifiant de l'utilisateur (uid) ou du batch (nomdebatch)
     * @param lesParamOperation --
     * @param natureOperation --
     * @param identifiantStructure permet d'identifier une structure associee a l'operation (par exemple codique
     *        NOMINOE)
     * @param dureedeconservation type de duree de conservation parametrable par fichier properties cles
     *        journal.trescourt,journal.court,journal.normal,journal.longu, journal.treslong valeurs en principe :
     *        TRESCOURT, COURT, NORMAL, LONG ou TRESLONG
     * @param unIdentifiantsMetierObjOperation : propriétes pour identifier de soobjets métiers tracés dans le journal
     * @param codeOperation : attribut complémentaire de nature operation
     * @param apurementPossible :indique si l operation peut être supprimée par le batch d'apurement quand le délai de
     *        conservation est dépasé
     * @return the operation journal
     * @see fr.gouv.finances.lombok.journal.bean.ParametreOperation
     */
    @Override
    public OperationJournal ajouterUneOperationAuJournal(String idutiloubatch,
        Set<ParametreOperation> lesParamOperation, String natureOperation, String identifiantStructure,
        String dureedeconservation, IdentifiantsMetierObjOperation unIdentifiantsMetierObjOperation,
        String codeOperation, boolean apurementPossible)
    {
        log.debug(">>> Debut methode ajouterUneOperationAuJournal");

        OperationJournal operationJournal = new OperationJournal();
        affecterDureeDeConservation(dureedeconservation, operationJournal);
        affecterOperationNature(natureOperation, operationJournal);
        affecterDateHeureOperation(operationJournal);
        affecterIdentifiants(idutiloubatch, identifiantStructure, operationJournal);
        affecterParametresOperation(operationJournal, lesParamOperation);
        affecterIdentifiantsMetierObjOperation(unIdentifiantsMetierObjOperation, operationJournal);
        affecterCodeOperationEtApurementPossible(codeOperation, apurementPossible, operationJournal);
        journaldao.saveNouvelleOperationAuJournal(operationJournal);
        return operationJournal;
    }

    /**
     * Migration du schéma du composant OpérationJournal V15.
     */
    @Override
    public void ajusteSequencesOperationJournal()
    {
        log.debug(">>> Debut methode ajusteSequencesOperationJournal");
        log.warn("-----------------------------------------------------------------------------------");
        log.warn("RECREATION DES SEQUENCES ORACLE ET AJUSTEMENT DE LEUR VALEUR INITIALE");
        log.warn("-----------------------------------------------------------------------------------");
        journaldao.ajusterSequencesOracle();
    }

    /**
     * Efface les opérations journal périmées.
     */
    @Override
    public void effacerLesOperationPerimees()
    {
        log.debug(">>> Debut methode effacerLesOperationPerimees");
        effacerOperationPourTypeDureeConservation(trescourt, inttrescourt);
        effacerOperationPourTypeDureeConservation(court, intcourt);
        effacerOperationPourTypeDureeConservation(normal, intnormal);
        effacerOperationPourTypeDureeConservation(longu, intlongu);
        effacerOperationPourTypeDureeConservation(treslong, inttreslong);
    }

    /**
     * Supprime les operations périmées.
     * 
     * @param typeDureeDeConservation --
     * @param dureeDeConservation --
     * @return the int
     */
    public int effacerLesOperationPerimees(final String typeDureeDeConservation, final int dureeDeConservation)
    {
        log.debug(">>> Debut methode effacerLesOperationPerimees");
        Object retour = null;
        try
        {
            retour = transactionTemplate.execute(new TransactionCallback<Object>()
            {
                public Integer doInTransaction(TransactionStatus arg0)
                {
                    int nbOpSupp = 0;
                    Date dateCourante = new Date();
                    ScrollIterator structureIterator =
                        journaldao.findOperationsPerimeesAUneDatePourUneDureeDeRetention(dateCourante,
                            typeDureeDeConservation, dureeDeConservation);

                    while (structureIterator.hasNext())
                    {
                        OperationJournal operationJournal = (OperationJournal) structureIterator.nextObjetMetier();

                        log.debug("Operation journal supprimée : " + operationJournal.getId());

                        // La session étant vidée, on lit en base les ParametreOperation
                        Set lesParamOper = journaldao.findParametresOperations(operationJournal);
                        Iterator unparamoperterator = lesParamOper.iterator();

                        journaldao.deleteOperationJournal(operationJournal);
                        while (unparamoperterator.hasNext())
                        {
                            ParametreOperation paramOper = ((ParametreOperation) unparamoperterator.next());
                            int nbop = journaldao.countOperationJournalAssocieAUnParametreOperation(paramOper);

                            if (nbop == 0)
                            {
                                log.debug("Paramètre opération supprimé (nom = " + paramOper.getNom() + ",valeur = "
                                    + paramOper.getValeur());
                                paramoperdao.deleteParametreOperation(paramOper);
                            }
                        }

                        nbOpSupp++;
                        if (nbOpSupp % 100 == 0)
                        {
                            journaldao.flush();
                            journaldao.clearPersistenceContext();
                            log.info("flush : " + nbOpSupp);
                        }
                    }

                    return nbOpSupp;

                }
            });

        }
        catch (RuntimeException e)
        {
            throw ApplicationExceptionTransformateur.transformer("Erreur lors de la suppression des opérations périmées", e);
        }

        return (Integer) retour;
    }

    /**
     * Gets the administrateur.
     *
     * @return the administrateur
     */
    public String getAdministrateur()
    {
        return administrateur;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @return the chaîne de caractères désignant une durée courte de conservation d'une opération dans le journal
     * @see fr.gouv.finances.lombok.journal.service.JournalService#getCourt()
     */
    @Override
    public String getCourt()
    {
        return court;
    }

    /**
     * Gets the nombre de jours de conservation d'une opération dans le journal lorsque la durée de conservation est
     * courte.
     *
     * @return the nombre de jours de conservation d'une opération dans le journal lorsque la durée de conservation est
     *         courte
     */
    public int getIntcourt()
    {
        return intcourt;
    }

    /**
     * Gets the nombre de jours de conservation d'une opération dans le journal lorsque la durée de conservation est
     * longue.
     *
     * @return the nombre de jours de conservation d'une opération dans le journal lorsque la durée de conservation est
     *         longue
     */
    public int getIntlongu()
    {
        return intlongu;
    }

    /**
     * Gets the nombre de jours de conservation d'une opération dans le journal lorsque la durée de conservation est
     * normale.
     *
     * @return the nombre de jours de conservation d'une opération dans le journal lorsque la durée de conservation est
     *         normale
     */
    public int getIntnormal()
    {
        return intnormal;
    }

    /**
     * Gets the nombre de jours de conservation d'une opération dans le journal lorsque la durée de conservation est
     * très courte.
     *
     * @return the nombre de jours de conservation d'une opération dans le journal lorsque la durée de conservation est
     *         très courte
     */
    public int getInttrescourt()
    {
        return inttrescourt;
    }

    /**
     * Gets the nombre de jours de conservation d'une opération dans le journal lorsque la durée de conservation est
     * trsè longue.
     *
     * @return the nombre de jours de conservation d'une opération dans le journal lorsque la durée de conservation est
     *         trsè longue
     */
    public int getInttreslong()
    {
        return inttreslong;
    }

    /**
     * Gets the journaldao.
     *
     * @return the journaldao
     */
    public JournalDao getJournaldao()
    {
        return journaldao;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @return the chaîne de caractères désignant une durée longue de conservation d'une opération dans le journal
     * @see fr.gouv.finances.lombok.journal.service.JournalService#getLongu()
     */
    @Override
    public String getLongu()
    {
        return longu;
    }

    /**
     * Gets the nombrejours.
     *
     * @return the nombrejours
     */
    public int getNombrejours()
    {
        return nombrejours;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @return the chaîne de caractères désignant une durée normale de conservation d'une opération dans le journal
     * @see fr.gouv.finances.lombok.journal.service.JournalService#getNormal()
     */
    @Override
    public String getNormal()
    {
        return normal;
    }

    /**
     * Gets the paramoperdao.
     *
     * @return the paramoperdao
     */
    public ParametreOperationDao getParamoperdao()
    {
        return paramoperdao;
    }

    // méthodes nécessaires à cause de la méthode effacerLesOperationsJournalPerimees
    /**
     * Gets the transaction template.
     *
     * @return the transaction template
     */
    public TransactionTemplate getTransactionTemplate()
    {
        return transactionTemplate;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @return the chaîne de caractères désignant une durée très courte de conservation d'une opération dans le journal
     * @see fr.gouv.finances.lombok.journal.service.JournalService#getTrescourt()
     */
    @Override
    public String getTrescourt()
    {
        return trescourt;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @return the chaîne de caractères désignant une durée très longue de conservation d'une opération dans le journal
     * @see fr.gouv.finances.lombok.journal.service.JournalService#getTreslong()
     */
    @Override
    public String getTreslong()
    {
        return treslong;
    }

    /**
     * Permet de rechercher des operations dans le journal par criteres multiples en passant un objet de type
     * CriteresRecherchesJournal Voir la documentation sur cette classe.
     * 
     * @param crit --
     * @return the list
     * @see fr.gouv.finances.lombok.journal.bean.CriteresRecherchesJournal
     */
    @Override
    public List rechercherLesOperationsJournalParCriteresRecherche(CriteresRecherchesJournal crit)
    {
        log.debug(">>> Debut methode rechercherLesOperationsJournalParCriteresRecherche");
        List listeoperations = new ArrayList();
        listeoperations = journaldao.findOperationsJournalParCriteresRecherche(crit);
        return listeoperations;
    }

    /**
     * retourne les opérations journal associées à unIdentifiantMetierObjOperation.
     * 
     * @param unIdentifiantMetierObjOperation --
     * @return the list< operation journal>
     */
    @Override
    public List<OperationJournal> rechercherOperationsJournalParIdentifiantMetierObjOperation(
        IdentifiantsMetierObjOperation unIdentifiantMetierObjOperation)
    {
        log.debug(">>> Debut methode rechercherOperationsJournalParIdentifiantMetierObjOperation");
        return journaldao.findOperationsJournalParIdentifiantMetierObjOperation(unIdentifiantMetierObjOperation);
    }

    /**
     * Recherche une opération jounral par son identifiantUtilisateur et le moment d'enregistrement de l'opération dans
     * le journal.
     * 
     * @param idUtilOuBatch --
     * @param dateHeureOperation --
     * @return the operation journal
     */
    @Override
    public OperationJournal rechercheUneOperationJournalParIdUtilEtDateHeure(String idUtilOuBatch,
        Date dateHeureOperation)
    {
        log.debug(">>> Debut methode rechercheUneOperationJournalParIdUtilEtDateHeure");
        return journaldao.findUneOperationJournalParIdUtilEtDateHeure(idUtilOuBatch, dateHeureOperation);
    }

    /**
     * Suite à la modification du shéma introduit dansla V15R du composant journal duplication des ParametreOperation.
     */
    @Override
    public void recreationParametresOperations()
    {
        log.debug(">>> Debut methode recreationParametresOperations");
        // Teste si la contrainte unique sur la table parametre_operation a été supprimée
        log.warn("-----------------------------------------------------------------------------------");
        log.warn("TEST DE LA DESACTIVATION DE LA CONTRAINTE UNIQUE SUR LA TABLE PARAMETREOPERATION_PAOP");
        log.warn("-----------------------------------------------------------------------------------");
        if (journaldao.testerExistenceContrainteUniquePAOP())
        {
            StringBuilder msg = new StringBuilder();
            // ancien code
            // msg.append("UNE CONTRAINTE UNIQUE EST ENCORE ACTIVE SUR LA TABLE PARAMETREOPERATION_PAOP.");
            // msg.append("VERIFIER QUE LE SCHEMA DU COMPOSANT JOURNAL EST A JOUR POUR LES VERSION V15 ET PLUS");
            // nouveau code
            msg.append(
                "UNE CONTRAINTE UNIQUE EST ENCORE ACTIVE SUR LA TABLE PARAMETREOPERATION_PAOP.VERIFIER QUE LE SCHEMA DU COMPOSANT JOURNAL EST A JOUR POUR LES VERSION V15 ET PLUS");
            log.warn(msg.toString());
            throw new ProgrammationException(msg.toString());
        }

        // Copie des tables dans des tables temporaires
        log.warn("-----------------------------------------------------------------------------------");
        log.warn("SAUVEGARDES DES TABLES DU COMPOSANT JOURNAL DANS DES TABLES TEMPORAIRES");
        log.warn("-----------------------------------------------------------------------------------");
        journaldao.sauvegardeTablesOperationJournalDansTablesTemporaires();

        log.warn("-----------------------------------------------------------------------------------");
        log.warn("RECREATION DES PARAMETRES OPERATIONS");
        log.warn("-----------------------------------------------------------------------------------");
        log.warn("COPIE DES PARAMETRES ..... ");

        Object retour = null;
        try
        {
            retour = transactionTemplate.execute(new TransactionCallbackWithoutResult()
            {
                public void doInTransactionWithoutResult(TransactionStatus arg0)
                {

                    ScrollIterator structureIterator = journaldao.findAllOperationsJournal();
                    long nombreParametreOpCopie = 0;

                    while (structureIterator.hasNext())
                    {
                        OperationJournal operationJournal = (OperationJournal) structureIterator.nextObjetMetier();
                        // On vide la session une fois l'objet lu pour éviter une
                        // exception levée
                        // par Hibernate lorsqu'un collection est associée à deux
                        // sessions
                        journaldao.clearPersistenceContext();
                        log.debug("--------------------------");
                        log.debug(operationJournal.toString());

                        // La session étant vidée, on lit en base les ParametreOperation
                        Set lesParamOper = journaldao.findParametresOperations(operationJournal);
                        Iterator unparamoperterator = lesParamOper.iterator();

                        // Recréation des ParametreOperation de façon à ce qu'ils
                        // ne soient plus partagés entre les OperationJournal
                        Set<ParametreOperation> lesNewParamOper = new HashSet<ParametreOperation>();

                        while (unparamoperterator.hasNext())
                        {

                            ParametreOperation paramOper = ((ParametreOperation) unparamoperterator.next());

                            log.debug(paramOper.toString());

                            ParametreOperation newParamOper = new ParametreOperation();
                            newParamOper.setNom(paramOper.getNom());
                            newParamOper.setValeur(paramOper.getValeur());

                            lesNewParamOper.add(newParamOper);

                            paramoperdao.saveParametreOperation(newParamOper);
                            nombreParametreOpCopie++;
                            log.debug("Paramètre opération copié " + newParamOper.toString());
                        }

                        // On rattache les nouveaux paramètres à l'objet OperationJournal en cours
                        operationJournal.setLesParamOperation(lesNewParamOper);

                        journaldao.saveNouvelleOperationAuJournal(operationJournal);

                        journaldao.flush();

                        log.debug("--------------------------");

                    }

                    log.warn("NOMBRE DE PARAMETRES OPERATION COPIES = " + nombreParametreOpCopie);

                    log.warn("-----------------------------------------------------------------------------------");
                    log.warn("SUPPRESSION DES PARAMETRES OPERATIONS ORPHELINS");
                    log.warn("-----------------------------------------------------------------------------------");
                    Long nbLignes = journaldao.countNombreParametresOrphelins();
                    log.warn("NOMBRE DE PARAMETREOPERATIONS ORPHELINS A SUPPRIMER = " + nbLignes);
                    log.warn("SUPPRESSION DES PARAMETRES OPERATIONS ......");
                    journaldao.supprimerParametresOperationsOrphelins();
                    nbLignes = journaldao.countNombreParametresOrphelins();
                    log.warn("NOMBRE DE PARAMETREOPERATIONS ORPHELINS RESTANTS DANS LA TABLE = " + nbLignes);
                    // Si tout est ok, on supprime les copies des tables
                    log.warn("-----------------------------------------------------------------------------------");
                    log.warn("SUPPRESSION DES TABLES TEMPORAIRES");
                    log.warn("-----------------------------------------------------------------------------------");
                    journaldao.suppressionCopieTablesOperationJournal();
                }
            });
        }
        catch (RuntimeException e)
        {
            throw ApplicationExceptionTransformateur.transformer("Erreur lors de la recréation des paramètres opérations", e);
        }

    }

    /**
     * Sets the administrateur.
     *
     * @param administrateur the new administrateur
     */
    public void setAdministrateur(String administrateur)
    {
        this.administrateur = administrateur;
    }

    /**
     * Sets the chaîne de caractères désignant une durée courte de conservation d'une opération dans le journal.
     *
     * @param court the new chaîne de caractères désignant une durée courte de conservation d'une opération dans le
     *        journal
     */
    public void setCourt(String court)
    {
        this.court = court;
    }

    /**
     * Sets the nombre de jours de conservation d'une opération dans le journal lorsque la durée de conservation est
     * courte.
     *
     * @param intcourt the new nombre de jours de conservation d'une opération dans le journal lorsque la durée de
     *        conservation est courte
     */
    public void setIntcourt(int intcourt)
    {
        this.intcourt = intcourt;
    }

    /**
     * Sets the nombre de jours de conservation d'une opération dans le journal lorsque la durée de conservation est
     * longue.
     *
     * @param intlongu the new nombre de jours de conservation d'une opération dans le journal lorsque la durée de
     *        conservation est longue
     */
    public void setIntlongu(int intlongu)
    {
        this.intlongu = intlongu;
    }

    /**
     * Sets the nombre de jours de conservation d'une opération dans le journal lorsque la durée de conservation est
     * normale.
     *
     * @param intnormal the new nombre de jours de conservation d'une opération dans le journal lorsque la durée de
     *        conservation est normale
     */
    public void setIntnormal(int intnormal)
    {
        this.intnormal = intnormal;
    }

    /**
     * Sets the nombre de jours de conservation d'une opération dans le journal lorsque la durée de conservation est
     * très courte.
     *
     * @param inttrescourt the new nombre de jours de conservation d'une opération dans le journal lorsque la durée de
     *        conservation est très courte
     */
    public void setInttrescourt(int inttrescourt)
    {
        this.inttrescourt = inttrescourt;
    }

    /**
     * Sets the nombre de jours de conservation d'une opération dans le journal lorsque la durée de conservation est
     * trsè longue.
     *
     * @param inttreslong the new nombre de jours de conservation d'une opération dans le journal lorsque la durée de
     *        conservation est trsè longue
     */
    public void setInttreslong(int inttreslong)
    {
        this.inttreslong = inttreslong;
    }

    /**
     * Sets the journaldao.
     *
     * @param journaldao the new journaldao
     */
    public void setJournaldao(JournalDao journaldao)
    {
        this.journaldao = journaldao;
    }

    /**
     * Sets the chaîne de caractères désignant une durée longue de conservation d'une opération dans le journal.
     *
     * @param longu the new chaîne de caractères désignant une durée longue de conservation d'une opération dans le
     *        journal
     */
    public void setLongu(String longu)
    {
        this.longu = longu;
    }

    /**
     * Sets the nombrejours.
     *
     * @param nombrejours the new nombrejours
     */
    public void setNombrejours(int nombrejours)
    {
        this.nombrejours = nombrejours;
    }

    /**
     * Sets the chaîne de caractères désignant une durée normale de conservation d'une opération dans le journal.
     *
     * @param normal the new chaîne de caractères désignant une durée normale de conservation d'une opération dans le
     *        journal
     */
    public void setNormal(String normal)
    {
        this.normal = normal;
    }

    /**
     * Sets the paramoperdao.
     *
     * @param paramoperdao the new paramoperdao
     */
    public void setParamoperdao(ParametreOperationDao paramoperdao)
    {
        this.paramoperdao = paramoperdao;
    }

    /**
     * Sets the transaction template.
     *
     * @param transactionTemplate the new transaction template
     */
    public void setTransactionTemplate(TransactionTemplate transactionTemplate)
    {
        this.transactionTemplate = transactionTemplate;
    }

    /**
     * Sets the chaîne de caractères désignant une durée très courte de conservation d'une opération dans le journal.
     *
     * @param trescourt the new chaîne de caractères désignant une durée très courte de conservation d'une opération
     *        dans le journal
     */
    public void setTrescourt(String trescourt)
    {
        this.trescourt = trescourt;
    }

    /**
     * Sets the chaîne de caractères désignant une durée très longue de conservation d'une opération dans le journal.
     *
     * @param treslong the new chaîne de caractères désignant une durée très longue de conservation d'une opération dans
     *        le journal
     */
    public void setTreslong(String treslong)
    {
        this.treslong = treslong;
    }

    /**
     * Supprime une opération du journal.
     * 
     * @param operationJournal --
     */
    @Override
    public void supprimerUneOperationJournal(OperationJournal operationJournal)
    {
        log.debug(">>> Debut methode supprimerUneOperationJournal");
        journaldao.deleteOperationJournal(operationJournal);
    }

    /**
     * methode Retourne duree conservation associee au code : DOCUMENTEZ_MOI.
     *
     * @param uneOperationJournal DOCUMENTEZ_MOI
     * @return int
     */
    public int retourneDureeConservationAssocieeAuCode(OperationJournal uneOperationJournal)
    {
        log.debug(">>> Debut methode retourneDureeConservationAssocieeAuCode");
        int retour = 0;

        if (uneOperationJournal.getTypeDureeDeConservation().equals(S_TRESCOURT))
        {
            retour = 7;
        }
        else if (uneOperationJournal.getTypeDureeDeConservation().equals(S_COURT))
        {
            retour = 14;
        }
        else if (uneOperationJournal.getTypeDureeDeConservation().equals(S_LONG))
        {
            retour = 90;
        }
        else if (uneOperationJournal.getTypeDureeDeConservation().equals(S_TRESLONG))
        {
            retour = 360;
        }
        else
        // si "S_NORMAL" ou par défaut
        {
            retour = 30;
        }

        return retour;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.journal.service.JournalService#tranfererToutesLesOperationsJournalDansITM()
     */
    @Override
    public int tranfererToutesLesOperationsJournalDansITM()
    {
        log.debug(">>> Debut methode tranfererToutesLesOperationsJournalDansITM");
        Object retour = null;
        try
        {
            retour = transactionTemplate.execute(new TransactionCallback<Object>()
            {
                public Integer doInTransaction(TransactionStatus arg0)
                {
                    int nbOpTransferes = 0;
                    ScrollIterator journalIterator =
                        journaldao.findAllOperationsJournal();

                    while (journalIterator.hasNext())
                    {
                        OperationJournal operationJournal = (OperationJournal) journalIterator.nextObjetMetier();
                        // On vide la session une fois l'objet lu pour éviter une exception levée
                        // par Hibernate lorsqu'une collection est associée à deux sessions
                        journaldao.clearPersistenceContext();

                        log.debug("Operation journal transférée : " + operationJournal.getId());

                        try
                        {
                            // La session étant vidée, on lit en base les ParametreOperation
                            NtcSRequeteSrvValeur ntcSRequeteSrvValeur = new NtcSRequeteSrvValeur();

                            // Horodatage
                            log.debug("debut horodatage   ");
                            GregorianCalendar gCalendar = new GregorianCalendar();
                            if (operationJournal.getDateHeureOperation() != null)
                            {
                                gCalendar.setTime(operationJournal.getDateHeureOperation());
                                XMLGregorianCalendar xmlGregorianCalendar =
                                    DatatypeFactory.newInstance().newXMLGregorianCalendar(gCalendar);
                                ntcSRequeteSrvValeur.setHorodatage(xmlGregorianCalendar);

                                // Type trace doit être forcement nomTrace
                                ntcSRequeteSrvValeur.setTypeTrace("JOURNAL.VISU.TRACES.01");
                                ntcSRequeteSrvValeur.setTypeTraceInitiateur(operationJournal.getIdentifiantUtilisateurOuBatch());
                                xmlGregorianCalendar = null;
                            }
                            gCalendar = null;
                            log.debug("fin horodatage   ");

                            // TypeTraceInitiateur optionnel dans itm non servi dans operation journal
                            // ntcSRequeteSrvValeurEmetteur.setTypeTraceInitiateur(value);

                            // Duree de conservation et d'exploitation
                            log.debug("debut Duree de conservation et d'exploitation   ");
                            ExploitationSrvValeur exploitationSrvValeur = new ExploitationSrvValeur();
                            GregorianCalendar gCalendar1 = new GregorianCalendar();
                            gCalendar1.setTimeInMillis(retourneDureeConservationAssocieeAuCode(operationJournal) * 3600l);
                            XMLGregorianCalendar xmlGregorianCalendar1 = DatatypeFactory.newInstance().newXMLGregorianCalendar(gCalendar1);
                            exploitationSrvValeur.setDureeConservation(xmlGregorianCalendar1);
                            // optionnekl dans itm non servi dans operation journal on va la mettre égale à
                            // setDureeConservation
                            exploitationSrvValeur.setDureeExploit(xmlGregorianCalendar1);
                            ntcSRequeteSrvValeur.setExploitation(exploitationSrvValeur);
                            xmlGregorianCalendar1 = null;
                            gCalendar1 = null;
                            exploitationSrvValeur = null;
                            log.debug("fin Duree de conservation et d'exploitation   ");

                            // Emetteur
                            log.debug("debut Emetteur   ");
                            if (operationJournal.getIdentifiantUtilisateurOuBatch() != null)
                            {
                                UtilisateurTypeSrvValeur unUtilisateurTypeSrvValeurEmetteur = new UtilisateurTypeSrvValeur();
                                unUtilisateurTypeSrvValeurEmetteur.setNom("UID");
                                unUtilisateurTypeSrvValeurEmetteur.setValeur(operationJournal.getIdentifiantUtilisateurOuBatch());
                                ntcSRequeteSrvValeur.getEmetteur().add(unUtilisateurTypeSrvValeurEmetteur);
                                unUtilisateurTypeSrvValeurEmetteur = null;
                            }
                            log.debug("fin Emetteur   ");

                            // IDENTIFIANT STRUCTURE
                            log.debug("debut identifiantstructure  ");
                            if (operationJournal.getIdentifiantStructure() != null)
                            {
                                UtilisateurTypeSrvValeur unUtilisateurTypeSrvValeurStructure = new UtilisateurTypeSrvValeur();
                                unUtilisateurTypeSrvValeurStructure.setNom("Codique");
                                unUtilisateurTypeSrvValeurStructure.setValeur(operationJournal.getIdentifiantStructure());
                                ntcSRequeteSrvValeur.getEmetteur().add(unUtilisateurTypeSrvValeurStructure);
                                unUtilisateurTypeSrvValeurStructure = null;
                            }
                            log.debug("fin identifiantstructure   ");

                            // Metier / traitement des parametres operation
                            log.debug("debut traitement des parametres operations   ");
                            // Set lesParamOperation =operationJournal.getLesParamOperation();
                            Set<ParametreOperation> lesParamOperation = journaldao.findParametresOperations(operationJournal);
                            Iterator paramOperationIterator = lesParamOperation.iterator();

                            while (paramOperationIterator.hasNext())
                            {
                                ParametreOperation paramOperation = (ParametreOperation) paramOperationIterator.next();

                                UtilisateurTypeSrvValeur unUtilisateurTypeSrvValeurParamOper = new UtilisateurTypeSrvValeur();
                                if (paramOperation.getNom().length() > 20)
                                    unUtilisateurTypeSrvValeurParamOper.setNom(paramOperation.getNom().substring(0, 20));
                                else
                                    unUtilisateurTypeSrvValeurParamOper.setNom(paramOperation.getNom());
                                unUtilisateurTypeSrvValeurParamOper.setValeur(paramOperation.getValeur());
                                ntcSRequeteSrvValeur.getMetier().add(unUtilisateurTypeSrvValeurParamOper);
                                paramOperation = null;

                            }
                            log.debug("fin traitement des parametres operations   ");

                            // Metier / traitement des IdentifiantsMetiersObjOperation
                            log.debug("debut traitement des IdentifiantsMetierObjOperation   ");
                            IdentifiantsMetierObjOperation unIdentifiantsMetierObjOperation =
                                operationJournal.getUnIdentifiantsMetierObjOperation();
                            if (unIdentifiantsMetierObjOperation.getLibelleIdentifiantMetier1() != null)
                            {
                                UtilisateurTypeSrvValeur unUtilisateurTypeSrvValeurIdentifiantsMetierObjOperation1 =
                                    new UtilisateurTypeSrvValeur();
                                unUtilisateurTypeSrvValeurIdentifiantsMetierObjOperation1.setNom(unIdentifiantsMetierObjOperation
                                    .getLibelleIdentifiantMetier1());
                                unUtilisateurTypeSrvValeurIdentifiantsMetierObjOperation1.setValeur(unIdentifiantsMetierObjOperation
                                    .getValeurIdentifiantMetier1());
                                ntcSRequeteSrvValeur.getMetier().add(unUtilisateurTypeSrvValeurIdentifiantsMetierObjOperation1);
                            }
                            if (unIdentifiantsMetierObjOperation.getLibelleIdentifiantMetier2() != null)
                            {
                                UtilisateurTypeSrvValeur unUtilisateurTypeSrvValeurIdentifiantsMetierObjOperation2 =
                                    new UtilisateurTypeSrvValeur();
                                unUtilisateurTypeSrvValeurIdentifiantsMetierObjOperation2.setNom(unIdentifiantsMetierObjOperation
                                    .getLibelleIdentifiantMetier2());
                                unUtilisateurTypeSrvValeurIdentifiantsMetierObjOperation2.setValeur(unIdentifiantsMetierObjOperation
                                    .getValeurIdentifiantMetier2());
                                ntcSRequeteSrvValeur.getMetier().add(unUtilisateurTypeSrvValeurIdentifiantsMetierObjOperation2);
                            }
                            if (unIdentifiantsMetierObjOperation.getLibelleIdentifiantMetier3() != null)
                            {
                                UtilisateurTypeSrvValeur unUtilisateurTypeSrvValeurIdentifiantsMetierObjOperation3 =
                                    new UtilisateurTypeSrvValeur();
                                unUtilisateurTypeSrvValeurIdentifiantsMetierObjOperation3.setNom(unIdentifiantsMetierObjOperation
                                    .getLibelleIdentifiantMetier3());
                                unUtilisateurTypeSrvValeurIdentifiantsMetierObjOperation3.setValeur(unIdentifiantsMetierObjOperation
                                    .getValeurIdentifiantMetier3());
                                ntcSRequeteSrvValeur.getMetier().add(unUtilisateurTypeSrvValeurIdentifiantsMetierObjOperation3);
                            }
                            if (unIdentifiantsMetierObjOperation.getLibelleIdentifiantMetier4() != null)
                            {
                                UtilisateurTypeSrvValeur unUtilisateurTypeSrvValeurIdentifiantsMetierObjOperation4 =
                                    new UtilisateurTypeSrvValeur();
                                unUtilisateurTypeSrvValeurIdentifiantsMetierObjOperation4.setNom(unIdentifiantsMetierObjOperation
                                    .getLibelleIdentifiantMetier4());
                                unUtilisateurTypeSrvValeurIdentifiantsMetierObjOperation4.setValeur(unIdentifiantsMetierObjOperation
                                    .getValeurIdentifiantMetier4());
                                ntcSRequeteSrvValeur.getMetier().add(unUtilisateurTypeSrvValeurIdentifiantsMetierObjOperation4);
                            }
                            if (unIdentifiantsMetierObjOperation.getLibelleIdentifiantMetier5() != null)
                            {
                                UtilisateurTypeSrvValeur unUtilisateurTypeSrvValeurIdentifiantsMetierObjOperation5 =
                                    new UtilisateurTypeSrvValeur();
                                unUtilisateurTypeSrvValeurIdentifiantsMetierObjOperation5.setNom(unIdentifiantsMetierObjOperation
                                    .getLibelleIdentifiantMetier5());
                                unUtilisateurTypeSrvValeurIdentifiantsMetierObjOperation5.setValeur(unIdentifiantsMetierObjOperation
                                    .getValeurIdentifiantMetier5());
                                ntcSRequeteSrvValeur.getMetier().add(unUtilisateurTypeSrvValeurIdentifiantsMetierObjOperation5);
                            }
                            log.debug("debut traitement des IdentifiantsMetierObjOperation   ");
                            // Metier traitement de apurementPossible et codeOperation
                            log.debug("debut traitement apurementPossible   ");
                            if (operationJournal.getApurementPossible() != null)
                            {
                                UtilisateurTypeSrvValeur unUtilisateurTypeSrvValeurApurementPossible = new UtilisateurTypeSrvValeur();
                                unUtilisateurTypeSrvValeurApurementPossible.setNom("apurement possible");
                                unUtilisateurTypeSrvValeurApurementPossible.setValeur(operationJournal.getApurementPossible().toString());
                                ntcSRequeteSrvValeur.getMetier().add(unUtilisateurTypeSrvValeurApurementPossible);
                                unUtilisateurTypeSrvValeurApurementPossible = null;
                            }
                            log.debug("fin traitement apurementPossible   ");
                            log.debug("debut traitement NatureOperation   ");
                            if (operationJournal.getCodeOperation() != null)
                            {
                                UtilisateurTypeSrvValeur unUtilisateurTypeSrvValeurNatureOperation = new UtilisateurTypeSrvValeur();
                                unUtilisateurTypeSrvValeurNatureOperation.setNom("nature operation");
                                unUtilisateurTypeSrvValeurNatureOperation.setValeur(operationJournal.getNatureOperation());
                                ntcSRequeteSrvValeur.getMetier().add(unUtilisateurTypeSrvValeurNatureOperation);
                                unUtilisateurTypeSrvValeurNatureOperation = null;
                            }
                            log.debug("fin traitement NatureOperation   ");
                            log.debug("debut traitement CodeOperation   ");
                            if (operationJournal.getCodeOperation() != null)
                            {
                                UtilisateurTypeSrvValeur unUtilisateurTypeSrvValeurCodeOperation = new UtilisateurTypeSrvValeur();
                                unUtilisateurTypeSrvValeurCodeOperation.setNom("code operation");
                                unUtilisateurTypeSrvValeurCodeOperation.setValeur(operationJournal.getCodeOperation());
                                ntcSRequeteSrvValeur.getMetier().add(unUtilisateurTypeSrvValeurCodeOperation);
                                unUtilisateurTypeSrvValeurCodeOperation = null;
                            }
                            log.debug("fin traitement CodeOperation   ");
                            // appel du web service et enregistrement
                            log.debug("debut traitement creerItmWebServiceClient   ");

                            log.debug("fin traitement creerItmWebServiceClient   ");
                            log.debug("debut traitement enregistrerTrace   ");

                            String nomTrace = "JOURNAL.VISU.TRACES.01";
                            byte[] deftraces = new byte[22];
                            deftraces = nomTrace.getBytes();

                            ErreurReponseSrvValeur reponse = itmserviceso.enregistrerTrace(deftraces, ntcSRequeteSrvValeur);
                            if (reponse.getErreur() != null)
                            {
                                Erreur erreur = reponse.getErreur();
                                log.error(
                                    erreur.getCode() + "\n" +
                                        erreur.getSeverite() + "\n" +
                                        erreur.getLibelle() + "\n" +
                                        erreur.getDescriptif() + "\n" +
                                        erreur.getId() + "\n");
                            }
                            log.debug("debut traitement enregistrerTrace   ");

                            nbOpTransferes++;
                        }
                        catch (DatatypeConfigurationException dce)
                        {
                            log.warn("erreur", dce);
                            throw ApplicationExceptionTransformateur.transformer(dce);
                        }
                    }

                    return nbOpTransferes;

                }
            });

        }
        catch (RuntimeException e)
        {
            throw ApplicationExceptionTransformateur.transformer("Erreur lors du transfert des opérations journal dans ITM", e);
        }
        return (Integer) retour;
    }

    /**
     * Affecte codeOperation et apurementPossible à une OperationJournal.
     * 
     * @param unCodeOperation --
     * @param unApurementPossible --
     * @param operationJournal --
     */
    private void affecterCodeOperationEtApurementPossible(String unCodeOperation, boolean unApurementPossible,
        OperationJournal operationJournal)
    {
        log.debug(">>> Debut methode affecterCodeOperationEtApurementPossible");
        if (unCodeOperation != null)
        {
            operationJournal.setCodeOperation(unCodeOperation);
        }
        else
        {
            operationJournal.setCodeOperation(CODE_OPERATION_NON_SERVI);
        }
        operationJournal.setApurementPossible(unApurementPossible);
    }

    /**
     * Fournit un instant unique pour les objets OperationJournal.
     * 
     * @param operationJournal --
     */
    private synchronized void affecterDateHeureOperation(OperationJournal operationJournal)
    {
        log.debug(">>> Debut methode affecterDateHeureOperation");
        Timestamp timestamp = new Timestamp((new Date()).getTime());

        // Pour garantir que deux éléments du journal n'auront pas la même clé
        // (dateheureoperation < identifiantutilisateuroubatch), on utilise un etat

        nanoincrement++;
        if (nanoincrement > C1000)
        {
            nanoincrement = 1;
        }
        timestamp.setNanos(timestamp.getNanos() + nanoincrement * C1000);
        operationJournal.setDateHeureOperation(timestamp);
    }

    /**
     * Test si la durée de conservation passée en paramètre est autorisée.
     * 
     * @param dureeDeConservation --
     * @param operationJournal --
     */
    private void affecterDureeDeConservation(String dureeDeConservation, OperationJournal operationJournal)
    {
        log.debug(">>> Debut methode affecterDureeDeConservation");
        if (!(trescourt.equals(dureeDeConservation) || court.equals(dureeDeConservation)
            || normal.equals(dureeDeConservation) || longu.equals(dureeDeConservation) || treslong
                .equals(dureeDeConservation)))
        {
            StringBuilder msg = new StringBuilder();
            msg.append("Le parametre 'dureedeconservation' ( ");
            msg.append(dureeDeConservation);
            msg.append(" ) doit prendre une des valeurs suivante :");
            msg.append(S_TRESCOURT);
            msg.append(',');
            msg.append(S_COURT);
            msg.append(',');
            msg.append(S_NORMAL);
            msg.append(',');
            msg.append(S_LONG);
            msg.append(',');
            msg.append(S_TRESLONG);

            throw new ProgrammationException(msg.toString());
        }
        else
        {
            operationJournal.setTypeDureeDeConservation(dureeDeConservation);
        }
    }

    /**
     * Affecte les attributs idUtilOuBatch et identifiantStructure.
     * 
     * @param idUtilOuBatch --
     * @param identifiantStructure --
     * @param operationJournal --
     */
    private void affecterIdentifiants(String idUtilOuBatch, String identifiantStructure, OperationJournal operationJournal)
    {
        log.debug(">>> Debut methode affecterIdentifiants");
        if (idUtilOuBatch != null)
        {
            operationJournal.setIdentifiantUtilisateurOuBatch(tronquerA255Caracteres(idUtilOuBatch, "idUtilOuBatch"));
            operationJournal.setIdentifiantStructure(tronquerA255Caracteres(identifiantStructure, "identifiantStructure"));
        }
        else
        {
            operationJournal.setIdentifiantUtilisateurOuBatch(IDENTIFIANT_UTILISATEUR_NON_SERVI);
            operationJournal.setIdentifiantStructure(IDENTIFIANT_STRUCTURE_BATCH);
        }
    }

    /**
     * Affecte les IdentifiantsMetierObjOperation à une OperationJournal.
     * 
     * @param unIdentifiantsMetierObjOperation --
     * @param operationJournal --
     */
    private void affecterIdentifiantsMetierObjOperation(
        IdentifiantsMetierObjOperation unIdentifiantsMetierObjOperation, OperationJournal operationJournal)
    {
        log.debug(">>> Debut methode affecterIdentifiantsMetierObjOperation");
        if (unIdentifiantsMetierObjOperation != null)
        {
            operationJournal.setUnIdentifiantsMetierObjOperation(unIdentifiantsMetierObjOperation);
        }
    }

    /**
     * Affecte les ParametreOperation à une OperationJournal.
     * 
     * @param lesParamOperation --
     * @param operationJournal --
     */
    private void affecterParametresOperation(OperationJournal operationJournal, Set<ParametreOperation> parametreOperationSet)
    {
        log.debug(">>> Debut methode affecterParametresOperation");
        Set<ParametreOperation> parametreOperationsSauveesSet = operationJournal.getLesParamOperation();
        // CF : passage en Java 8
        if ((parametreOperationSet != null) && (!parametreOperationSet.isEmpty()))
        {
            parametreOperationSet.forEach(parametreOperation -> {
                parametreOperation.setNumIncrement(parametreOperation.getNumIncrement());
                parametreOperation.setNom(tronquerA255Caracteres(parametreOperation.getNom(), "ParametreOperation.nom"));
                parametreOperation.setValeur(tronquerA255Caracteres(parametreOperation.getValeur(), "ParametreOperation.valeur"));
                paramoperdao.saveParametreOperation(parametreOperation);
                parametreOperationsSauveesSet.add(parametreOperation);
            });
        }
        operationJournal.setLesParamOperation(parametreOperationsSauveesSet);
    }

    /**
     * Affecte l'attribut NatureOperation à une OperationJournal Si la nature de l'opération comporte plus de 255
     * caractères seuls les 255 premiers sont conservés.
     * 
     * @param natureOperation --
     * @param operationJournal --
     */
    private void affecterOperationNature(String natureOperation, OperationJournal operationJournal)
    {
        log.debug(">>> Debut methode affecterOperationNature");
        operationJournal.setNatureOperation(tronquerA255Caracteres(natureOperation, "natureOperation"));
    }

    /**
     * Supprime les OperationJournal périmées pour une catégorie de durée de conservation et une durée de conservation
     * donnée.
     * 
     * @param typeDureeConservation --
     * @param dureeConservation --
     */
    private void effacerOperationPourTypeDureeConservation(String typeDureeConservation, int dureeConservation)
    {
        log.debug(">>> Debut methode effaceOperationPourTypeDureeConservation");
        log.warn("------------------------------------------------------------------------------------");
        log.warn("Durée de conservation : " + typeDureeConservation);
        int nbOpTresCourt = effacerLesOperationPerimees(typeDureeConservation, dureeConservation);
        log.warn(nbOpTresCourt + " OperationJournal supprimées");
        log.warn("------------------------------------------------------------------------------------");
    }

    /**
     * Tronque une chaîne de caractères si elle comporte plus de 255 caractères.
     * 
     * @param attr --
     * @param attrName --
     * @return the string
     */
    private String tronquerA255Caracteres(String attr, String attrName)
    {
        String result = attr;
        // On tronque la nature de l'opération si > 255 + warning
        if (attr != null && attr.length() > 255)
        {
            result = attr.substring(0, 254);
            StringBuilder msg = new StringBuilder();
            msg.append("L'attribut '" + attrName + "' comporte plus de 255 caractères.");
            msg.append("Il a été tronqué.");
            msg.append(attr);
            msg.append(" devient ");
            msg.append(result);
            log.warn(msg.toString());
        }

        return result;
    }

}
