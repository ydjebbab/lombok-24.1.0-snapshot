/*
 * Copyright (c) 2014 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.srm.jpa.dao.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.criteria.CriteriaDelete;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.orm.ObjectRetrievalFailureException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import fr.gouv.finances.cp.srm.dao.MessageTypeDao;
import fr.gouv.finances.cp.srm.dao.SrmDaoHelper;
import fr.gouv.finances.lombok.jpa.dao.BaseDaoJpaImpl;
import fr.gouv.finances.lombok.srm.bean.MessageType;
import fr.gouv.finances.lombok.util.persistance.ModeCritereRecherche;

/**
 * Implémentation JPA du DAO de gestion des données du type de message SIREME.
 *
 * @author Christophe Breheret-Girardin
 */
@ConditionalOnProperty(name = "lombok.orm.jpa", havingValue="true")
@Repository("messagetypedaoimpl")
@Transactional(transactionManager="transactionManager")
public class MessageTypeDaoImpl extends BaseDaoJpaImpl implements MessageTypeDao
{
    /** Initialisation de la journalisation. */
    protected static final Logger LOG = LoggerFactory.getLogger(MessageTypeDaoImpl.class);

    /**
     * Constructeur.
     */
    public MessageTypeDaoImpl()
    {
        super();
    }

    /**
     * {@inheritDoc}
     *
     * @see fr.gouv.finances.cp.srm.dao.MessageTypeDao#deleteMessageType(fr.gouv.finances.lombok.srm.bean.MessageType)
     */
    @Override
    public void deleteMessageType(MessageType messagetype)
    {
        // Journalisation de la suppression
        LOG.debug("Suppression du type de message SIREME '{}'", messagetype.getLibelleMessageType());

        // Suppression du type de message demandé
        deleteObject(messagetype);
    }

    /**
     * {@inheritDoc}
     *
     * @see fr.gouv.finances.cp.srm.dao.MessageTypeDao#deleteTousMessagesType()
     */
    @Override
    public void deleteTousMessagesType()
    {
        // Journalisation de la suppression
        LOG.trace("Suppression de tous les types de message SIREME");

        // Initialisation de la suppression sur l'entité MessageType
        CriteriaDelete<MessageType> criteriaDelete = entityManager.getCriteriaBuilder()
            .createCriteriaDelete(MessageType.class);
        criteriaDelete.from(MessageType.class);

        // Exécution de la mise à jour
        entityManager.createQuery(criteriaDelete).executeUpdate();
    }

    /**
     * {@inheritDoc}
     *
     * @see fr.gouv.finances.cp.srm.dao.MessageTypeDao#findLibelleAssocieAMessageTypeId(java.lang.String)
     */
    @Override
    public String findLibelleAssocieAMessageTypeId(String messageTypeId)
    {
        // Journalisation de la recherche
        LOG.debug("Recherche d'un type de message correspondant à l'identifiant '{}'", messageTypeId);

        // Récupération de l'identifiant sous forme numérique
        Long identifiant = SrmDaoHelper.getIdentifiant(messageTypeId);

        // Si le paramètre n'est pas renseigné, la requête n'est pas exécutée
        if (identifiant == null)
        {
            return null;
        }

        // Exécution de la recherche
        try
        {
            // Récupération du libellé si un type de message correspond au critère
            MessageType messageType = getObject(MessageType.class, identifiant);
            return messageType.getLibelleMessageType();
        }
        catch (ObjectRetrievalFailureException orfe)
        {
            // Type de message non trouvé
            LOG.info("Type de message SIREME d'identifiant '{}' non trouvé : {}", messageTypeId, orfe);
            return null;
        }
    }

    /**
     * {@inheritDoc}
     *
     * @see fr.gouv.finances.cp.srm.dao.MessageTypeDao#findListeMessagesTypes()
     */
    @Override
    public List<MessageType> findListeMessagesTypes()
    {
        // Journalisation de la recherche
        LOG.trace("Recherche de tous les types de message SIREME");

        // Récupération de tous les types de message sireme
        return loadAllObjects(MessageType.class);
    }

    /**
     * {@inheritDoc}
     *
     * @see fr.gouv.finances.cp.srm.dao.MessageTypeDao#findMessageTypeParLibelle(java.lang.String)
     */
    @Override
    public MessageType findMessageTypeParLibelle(String libelle)
    {
        // Journalisation de la recherche
        LOG.debug("Recherche d'un type de message correspondant au libellé '{}'", libelle);

        // Si le paramètre n'est pas renseigné, la requête n'est pas exécutée
        if (SrmDaoHelper.parametreNonFourni(libelle))
        {
            return null;
        }

        // Préparation des critères
        Map<String, String> criteres = new HashMap<>();
        criteres.put("libelleMessageType", libelle);

        // Recherche des types de message sireme
        return findByCriterias(MessageType.class, criteres, ModeCritereRecherche.EQUAL);
    }

    /**
     * {@inheritDoc}
     *
     * @see fr.gouv.finances.cp.srm.dao.MessageTypeDao#findMessageTypeParPartieParLibelle(java.lang.String)
     */
    @Override
    public List<MessageType> findMessageTypeParPartieParLibelle(String libelle)
    {
        // Journalisation de la recherche
        LOG.debug("Recherche d'un type de message dont le libellé contient '{}'", libelle);

        // Si le paramètre n'est pas renseigné, la requête n'est pas exécutée
        if (SrmDaoHelper.parametreNonFourni(libelle))
        {
            return new ArrayList<>();
        }

        // Préparation des critères
        Map<String, String> criteres = new HashMap<>();
        criteres.put("libelleMessageType", libelle);

        // Recherche des types de message sireme
        return findAllByCriterias(MessageType.class, criteres, ModeCritereRecherche.LIKE_ANYWHERE);
    }

    /**
     * {@inheritDoc}
     *
     * @see fr.gouv.finances.cp.srm.dao.MessageTypeDao#saveMessageType(fr.gouv.finances.lombok.srm.bean.MessageType)
     */
    @Override
    public void saveMessageType(MessageType messageType)
    {
        // Journalisation de la mise à jour
        LOG.debug("Sauvegarde du type de message '{}'", messageType.getLibelleMessageType());

        // Exécution de la mise à jour
        saveObject(messageType);
    }

}
