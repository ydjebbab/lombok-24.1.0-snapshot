/*
 * Copyright (c) 2017 DGFiP - Tous droits réservés
 * 
*/
/**
 * Paquet contenant les implémentations JPA des DAO liés à la gestion des messages sireme
 * @author Christophe Breheret-Girardin
 */
package fr.gouv.finances.lombok.srm.jpa.dao.impl;