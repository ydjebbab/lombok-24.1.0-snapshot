/*
 * Copyright (c) 2014 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.srm.jpa.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import fr.gouv.finances.cp.srm.dao.ProfilDao;
import fr.gouv.finances.cp.srm.dao.ProprieteProfil;
import fr.gouv.finances.cp.srm.dao.SrmDaoHelper;
import fr.gouv.finances.lombok.jpa.dao.BaseDaoJpaImpl;
import fr.gouv.finances.lombok.srm.bean.Profil;
import fr.gouv.finances.lombok.util.persistance.ModeCritereRecherche;

/**
 * Implémentation JPA du DAO de gestion des données du profil.
 *
 * @author Christophe Breheret-Girardin
 */
@ConditionalOnProperty(name = "lombok.orm.jpa", havingValue="true")
@Repository("profildaoimpl")
@Transactional(transactionManager="transactionManager")
public class ProfilDaoImpl extends BaseDaoJpaImpl implements ProfilDao
{
    protected static final Logger LOG = LoggerFactory.getLogger(ProfilDaoImpl.class);

    public ProfilDaoImpl()
    {
        super();
    }

    /**
     * {@inheritDoc}
     * 
     * @see fr.gouv.finances.cp.srm.dao.ProfilDao#findProfilParCode(java.lang.String)
     */
    @Override
    public Profil findProfilParCode(String code)
    {
        LOG.debug("Recherche du profil correspondant au code '{}'", code);

        // Si aucun paramètre, la requête n'est pas exécutée
        if (SrmDaoHelper.parametreNonFourni(code))
        {
            return null;
        }

        // Préparation des critères
        Map<String, String> criteres = new HashMap<>();
        criteres.put(ProprieteProfil.PROPRIETE_CODE.getNom(), code);

        // Recherche des profils selon le code profil
        // (égalité stricte).
        List<Profil> profils = findAllByCriterias(Profil.class, criteres, ModeCritereRecherche.EQUAL);

        // Récupération d'un profil s'il existe
        Profil profil = null;
        if (!profils.isEmpty())
        {
            // Récupération du 1er
            profil = profils.get(0);
        }
        return profil;
    }

    /**
     * {@inheritDoc}
     * 
     * @see fr.gouv.finances.cp.srm.dao.ProfilDao#findProfilParCode(java.lang.String)
     */
    @Override
    public void saveProfil(Profil profil)
    {
        // Journalisation de la mise à jour
        LOG.debug("Enregistrement du profil '{}'", profil.getCodeProfil());

        // Exécution de la mise à jour
        saveObject(profil);
    }

    /**
     * {@inheritDoc}
     * 
     * @see fr.gouv.finances.cp.srm.dao.ProfilDao#findProfilParCode(java.lang.String)
     */
    @Override
    public List<Profil> findTousLesProfils()
    {
        LOG.trace("Recherche de tous les profils");

        // Exécution du chargement
        return loadAllObjects(Profil.class);
    }
}
