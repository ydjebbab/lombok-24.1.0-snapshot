/*
 * Copyright (c) 2014 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.srm.jpa.dao.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import fr.gouv.finances.cp.srm.dao.MessageDao;
import fr.gouv.finances.cp.srm.dao.ProprieteMessage;
import fr.gouv.finances.cp.srm.techbean.CriteresRechercheMessages;
import fr.gouv.finances.lombok.jpa.dao.BaseDaoJpaImpl;
import fr.gouv.finances.lombok.srm.bean.Codique;
import fr.gouv.finances.lombok.srm.bean.Message;
import fr.gouv.finances.lombok.srm.bean.Profil;
import fr.gouv.finances.lombok.util.persistance.ModeCritereRecherche;

/**
 * Implémentation JPA du DAO de gestion des données du message SIREME.
 *
 * @author Christophe Breheret-Girardin
 */
@ConditionalOnProperty(name = "lombok.orm.jpa", havingValue="true")
@Repository("messagedaoimpl")
@Transactional(transactionManager="transactionManager")
public class MessageDaoImpl extends BaseDaoJpaImpl implements MessageDao
{
    /** Initialisation de la journalisation. */
    protected static final Logger LOG = LoggerFactory.getLogger(MessageDaoImpl.class);

    /**
     * Constructeur.
     */
    public MessageDaoImpl()
    {
        super();
    }

    /**
     * {@inheritDoc}
     *
     * @see fr.gouv.finances.cp.srm.dao.MessageDao#deleteMessage(fr.gouv.finances.lombok.srm.bean.Message)
     */
    @Override
    public void deleteMessage(Message message)
    {
        // Journalisation de la suppression
        LOG.debug("Suppression du message SIREME '{}'", message.getLibelleMessage());

        // Suppression du type de message
        deleteObject(message);
    }

    /**
     * 
     * (methode de remplacement)
     * {@inheritDoc}
     * @see fr.gouv.finances.cp.srm.dao.MessageDao#findMessageExisteDeja(fr.gouv.finances.lombok.srm.bean.Message)
     */
    @Override
    public Message findMessageExisteDeja(Message message)
    {
        // Si aucun critère, la requête n'est pas exécutée
        if (message == null || StringUtils.isEmpty(message.getLibelleMessage()))
        {
            LOG.info("Le message fourni n'est pas complet pour être un critère de recherche");
            return null;
        }

        // Journalisation de la recherche
        LOG.debug("Recherche si un message SIREME existe déjà avec le libellé '{}'", message.getLibelleMessage());

        // Préparation des critères
        Map<String, String> criteres = new HashMap<>();
        criteres.put(ProprieteMessage.PROPRIETE_LIBELLE.getNom(), message.getLibelleMessage());

        // Recherche des types de message sireme
        List<Message> messages = findAllByCriterias(Message.class, criteres, ModeCritereRecherche.EQUAL);

        // Analyse des résultats
        Message unMessage = null;
        if (!messages.isEmpty())
        {
            unMessage = messages.iterator().next();
        }
        return unMessage;
    }

    /**
     * 
     * (methode de remplacement)
     * {@inheritDoc}
     * @see fr.gouv.finances.cp.srm.dao.MessageDao#findMessageExisteDejaEnModif(fr.gouv.finances.lombok.srm.bean.Message)
     */
    @Override
    public Message findMessageExisteDejaEnModif(Message message)
    {
        // Si aucun critère de recherche, la requête n'est pas exécutée
        if (message == null || StringUtils.isEmpty(message.getLibelleMessage())
            || message.getId() == null)
        {
            LOG.info("Le message fourni n'est pas complet pour être un critère de recherche");
            return null;
        }

        // Journalisation de la recherche
        LOG.debug("Recherche si un message SIREME existe déjà en modification avec le libellé '{}'", message.getLibelleMessage());

        // Paramètrage de la recherche
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Message> query = criteriaBuilder.createQuery(Message.class);
        Root<Message> root = query.from(Message.class);

        // Paramètrage des critères
        Predicate idPredicate = criteriaBuilder.notEqual(root.<Long> get("id"), message.getId());
        Predicate libellePredicate = criteriaBuilder.equal(
            root.<String> get(ProprieteMessage.PROPRIETE_LIBELLE.getNom()), message.getLibelleMessage());
        query.where(idPredicate, libellePredicate);

        // Exécution de la requête
        Iterator<Message> resultat = findAll(query).iterator();

        // Analyse des résultats
        Message unMessage = null;
        if (resultat.hasNext())
        {
            unMessage = resultat.next();
        }

        return unMessage;
    }

    /**
     * 
     * (methode de remplacement)
     * {@inheritDoc}
     * @see fr.gouv.finances.cp.srm.dao.MessageDao#findMessages(fr.gouv.finances.cp.srm.techbean.CriteresRechercheMessages)
     */
    @Override
    public List<Message> findMessages(CriteresRechercheMessages criteresRechercheMessages)
    {
        // Journalisation de la recherche
        LOG.debug(">>> Recherche des messages SIREME correspondants aux critères '{}'", criteresRechercheMessages);

        // Si aucun critère, la requête n'est pas exécutée
        if (criteresRechercheMessages == null)
        {
            LOG.info("Aucun critère, récupération de tous les messages");
            return findTousLesMessages();
        }

        LOG.info("Des critères existent");
        // Paramètrage de la recherche
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Message> query = criteriaBuilder.createQuery(Message.class);
        Root<Message> root = query.from(Message.class);
        query.distinct(true);

        // Récupération des critères de recherche
        List<Predicate> predicates = recupererCriteres(criteresRechercheMessages, criteriaBuilder, root);
        query.where(predicates.toArray(new Predicate[] {}));

        // Récupération des informations associées
        ajouterAssociations(root);

        // Initialisation de l'ordre à appliquer aux résultats
        query.orderBy(criteriaBuilder.asc(root.get(ProprieteMessage.PROPRIETE_DATE_DEBUT.getNom())));

        // Récupération des messages correspondants
        return findAll(query);
    }

   
    /**
     * 
     * (methode de remplacement)
     * {@inheritDoc}
     * @see fr.gouv.finances.cp.srm.dao.MessageDao#findMessagesAyantLibelleCodiqueRattacheAAutreMessage(java.lang.String, fr.gouv.finances.lombok.srm.bean.Message)
     */
    @Override
    public List<Message> findMessagesAyantLibelleCodiqueRattacheAAutreMessage(
        String libelleCodique, Message message)
    {
        // Journalisation de la recherche
        LOG.debug("Recherche des messages SIREME ayant le codique '{}' rattaché à un autre message", libelleCodique);

        // Si aucun critère, la requête n'est pas exécutée
        if (message == null || message.getId() == null || StringUtils.isEmpty(libelleCodique))
        {
            LOG.info("Le message et le libellé fournis ne sont pas complet pour être des critères"
                + " de recherche");
            return new ArrayList<>();
        }

        // Paramètrage de la recherche
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Message> query = criteriaBuilder.createQuery(Message.class);
        Root<Message> root = query.from(Message.class);

        // Paramètrage de la jointure
        Join<Message, Codique> codiqueJoin = root.join(
            ProprieteMessage.PROPRIETE_CODIQUES_DESTINATAIRES.getNom(), JoinType.INNER);

        // Paramètrage des critères
        Predicate idPredicate = criteriaBuilder.notEqual(root.<Long> get("id"), message.getId());
        Predicate codiquePredicate = criteriaBuilder.equal(
            codiqueJoin.<Codique> get("libelleCodique"), libelleCodique);
        query.where(idPredicate, codiquePredicate);

        // Exécution de la recherche
        return findAll(query);
    }

    /**
     * {@inheritDoc}
     *
     * @see fr.gouv.finances.cp.srm.dao.MessageDao#findMessagesPerimes()
     */
    @Override
    public List<Message> findMessagesPerimes()
    {
        // Journalisation de la recherche
        LOG.trace("Recherche des messages SIREME périmés");

        // Paramètrage de la recherche
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Message> query = criteriaBuilder.createQuery(Message.class);
        Root<Message> root = query.from(Message.class);
        query.distinct(true);

        // si message simple d'information, on cherche messages dont date de fin message < date du jour
        // si message qui desactive un profil, on cherche messages dont date de fin de desactivation
        // du profil < date du jour
        Date dayDate = new Date();
        Predicate dateFinPredicate = criteriaBuilder.and(
            criteriaBuilder.isNull(root.<Date> get(ProprieteMessage.PROPRIETE_DATE_FIN_INACTIVITE.getNom())),
            criteriaBuilder.lessThan(root.<Date> get(ProprieteMessage.PROPRIETE_DATE_FIN.getNom()), dayDate));
        Predicate dateFinActivitePredicate = criteriaBuilder.and(
            criteriaBuilder.isNotNull(root.<Date> get(ProprieteMessage.PROPRIETE_DATE_FIN_INACTIVITE.getNom())),
            criteriaBuilder.lessThan(root.<Date> get(ProprieteMessage.PROPRIETE_DATE_FIN_INACTIVITE.getNom()), dayDate));
        query.where(criteriaBuilder.or(dateFinPredicate, dateFinActivitePredicate));

        // Récupération des informations associées
        ajouterAssociations(root);

        return findAll(query);
    }

    /**
     * {@inheritDoc}
     *
     * @see fr.gouv.finances.cp.srm.dao.MessageDao#findTousLesMessages()
     */
    @Override
    public List<Message> findTousLesMessages()
    {
        // Journalisation de la recherche
        LOG.trace("Récupération de tous les messages SIREME (+ profils + codiques)");

        // Paramètrage de la recherche
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Message> query = criteriaBuilder.createQuery(Message.class);
        Root<Message> root = query.from(Message.class);
        query.distinct(true);

        // Récupération des informations associées
        ajouterAssociations(root);

        // Exécution de la requête
        return findAll(query);
    }

    @Override
    public void saveMessage(Message message)
    {
        // Journalisation de la mise à jour
        LOG.debug("Création du message SIREME '{}'", message.getLibelleMessage());

        // Création d'un message
        saveObject(message);
    }

    /**
     * Méthode permettant d'ajouter la récupération des associations du message
     *
     * @param root entité de sélection utilisée dans la requête
     */
    private void ajouterAssociations(Root<Message> root)
    {
        // Journalisation de l'ajout des associations à récupérer
        LOG.trace("Ajout des associations à récupérer lors de la recherche");

        // Ajout des jointures
        root.fetch(ProprieteMessage.PROPRIETE_PROFILS_DESACTIVES.getNom(), JoinType.LEFT);
        root.fetch(ProprieteMessage.PROPRIETE_PROFILS_DESTINATAIRES.getNom(), JoinType.LEFT);
        root.fetch(ProprieteMessage.PROPRIETE_CODIQUES_DESTINATAIRES.getNom(), JoinType.LEFT);
    }
    
    /**
     * Méthode permettant de récupérer les critères à appliquer à la recherche de message
     *
     * @param criteres Critères à appliquer
     * @param criteriaBuilder Utilitaire de construction de requêtes JPA
     * @param root Entité de sélection utilisée dans la requête
     * @return La liste des prédicats de critères à appliquer
     */
    private List<Predicate> recupererCriteres(final CriteresRechercheMessages criteresRechercheMessages,
        CriteriaBuilder criteriaBuilder, Root<Message> root)
    {
        // Journalisation de la récupération des critères
        LOG.trace("Récupération des critères de recherche");

        List<Predicate> predicates = new ArrayList<>();

        // Cas de recherche sur les libellés de message
        if (!StringUtils.isEmpty(criteresRechercheMessages.getLibelleMessage()))
        {
            predicates
                .add(criteriaBuilder.equal(root.<Long> get(ProprieteMessage.PROPRIETE_LIBELLE.getNom()), criteresRechercheMessages.getLibelleMessage()));
        }

        // Cas de recherche sur les types de message
        if (!StringUtils.isEmpty(criteresRechercheMessages.getTypeMessages()))
        {
            predicates.add(criteriaBuilder.equal(root.<Long> get("typeMessage"), criteresRechercheMessages.getTypeMessages()));
        }

        // Cas de recherche sur les dates
        if (criteresRechercheMessages.getDateDebutMessage() != null)
        {
            predicates.add(criteriaBuilder.greaterThanOrEqualTo(root.<Date> get(
                ProprieteMessage.PROPRIETE_DATE_DEBUT.getNom()), criteresRechercheMessages.getDateDebutMessage()));
        }

        if (criteresRechercheMessages.getDateFinMessage() != null)
        {
            predicates.add(criteriaBuilder.lessThanOrEqualTo(root.<Date> get(
                ProprieteMessage.PROPRIETE_DATE_FIN.getNom()), criteresRechercheMessages.getDateFinMessage()));
        }

        // Cas de recherche sur le créateur
        if (!StringUtils.isEmpty(criteresRechercheMessages.getUidCreateurMessage()))
        {
            predicates.add(criteriaBuilder.equal(criteriaBuilder.upper(
                root.<String> get("UIDCreateurMessage")), criteresRechercheMessages.getUidCreateurMessage().trim().toUpperCase()));
        }

        // Cas de recherche des messages actifs
        if (criteresRechercheMessages.isRechOnlyMessagesActifsNow())
        {
            predicates.add(criteriaBuilder.lessThanOrEqualTo(root.<Date> get(
                ProprieteMessage.PROPRIETE_DATE_DEBUT.getNom()), criteresRechercheMessages.getDateHeureCourante()));
            predicates.add(criteriaBuilder.greaterThanOrEqualTo(root.<Date> get(
                ProprieteMessage.PROPRIETE_DATE_FIN.getNom()), criteresRechercheMessages.getDateHeureCourante()));
        }

        // Cas de recherche des messages liés aux profils à désactiver à une date donnée
        if (criteresRechercheMessages.isRechOnlyMessProfilsADesactNow())
        {
            predicates.add(criteriaBuilder.isNotNull(root.<Date> get(
                ProprieteMessage.PROPRIETE_DATE_DEBUT_INACTIVITE.getNom())));
            predicates.add(criteriaBuilder.isNotNull(root.<Date> get(
                ProprieteMessage.PROPRIETE_DATE_FIN_INACTIVITE.getNom())));
            predicates.add(criteriaBuilder.lessThanOrEqualTo(root.<Date> get(
                ProprieteMessage.PROPRIETE_DATE_DEBUT_INACTIVITE.getNom()), criteresRechercheMessages.getDateHeureCourante()));
            predicates.add(criteriaBuilder.greaterThanOrEqualTo(root.<Date> get(
                ProprieteMessage.PROPRIETE_DATE_FIN_INACTIVITE.getNom()), criteresRechercheMessages.getDateHeureCourante()));
        }

        // Cas de recherche des messages liés aux profils à désactiver à la date du jour
        if (criteresRechercheMessages.isRechOnlyMessAvecProfilsADesact())
        {
            predicates.add(
                criteriaBuilder.isNotEmpty(root.<Collection<Profil>> get(
                    ProprieteMessage.PROPRIETE_PROFILS_DESACTIVES.getNom())));
        }

        return predicates;
    }

}
