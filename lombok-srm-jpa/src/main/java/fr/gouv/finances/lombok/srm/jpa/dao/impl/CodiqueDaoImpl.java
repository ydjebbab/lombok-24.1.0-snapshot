/*
 * Copyright (c) 2014 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.lombok.srm.jpa.dao.impl;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import fr.gouv.finances.cp.srm.dao.CodiqueDao;
import fr.gouv.finances.cp.srm.dao.ProprieteCodique;
import fr.gouv.finances.cp.srm.dao.SrmDaoHelper;
import fr.gouv.finances.lombok.jpa.dao.BaseDaoJpaImpl;
import fr.gouv.finances.lombok.srm.bean.Codique;
import fr.gouv.finances.lombok.util.persistance.ModeCritereRecherche;

/**
 * Implémentation JPA du DAO de gestion des données du codique.
 *
 * @author Christophe Breheret-Girardin
 */
@ConditionalOnProperty(name = "lombok.orm.jpa", havingValue="true")
@Repository("codiquedaoimpl")
@Transactional(transactionManager="transactionManager")
public class CodiqueDaoImpl extends BaseDaoJpaImpl implements CodiqueDao
{
    protected static final Logger LOG = LoggerFactory.getLogger(CodiqueDaoImpl.class);

    public CodiqueDaoImpl()
    {
        super();
    }

    /**
     * {@inheritDoc}
     *
     * @see fr.gouv.finances.cp.srm.dao.CodiqueDao#findCodique(java.lang.String)
     */
    @Override
    public Codique findCodique(String libelle)
    {
        // Journalisation de la recherche
        LOG.debug("Recherche du codique correspondant au libellé '{}'", libelle);

        // Si aucun paramètre, la requête n'est pas exécutée
        if (SrmDaoHelper.parametreNonFourni(libelle))
        {
            return null;
        }

        // Préparation des critères
        Map<String, String> criteres = new HashMap<>();
        criteres.put(ProprieteCodique.PROPRIETE_LIBELLE.getNom(), libelle);

        // Recherche des codiques correspondant au libellé
        // (égalité stricte).
        return findByCriterias(Codique.class, criteres, ModeCritereRecherche.EQUAL);
    }

    /**
     * {@inheritDoc}
     *
     * @see fr.gouv.finances.cp.srm.dao.CodiqueDao#saveCodique(fr.gouv.finances.lombok.srm.bean.Codique)
     */
    @Override
    public void saveCodique(Codique codique)
    {
        // Journalisation de la mise à jour
        LOG.debug("Sauvegarde du codique '{}'", codique.getLibelleCodique());

        // Exécution de la mise à jour
        saveObject(codique);
    }

}
