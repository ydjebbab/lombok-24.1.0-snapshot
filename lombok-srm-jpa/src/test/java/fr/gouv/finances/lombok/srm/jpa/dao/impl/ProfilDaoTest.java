/*
 * Copyright (c) 2020 DGFiP - Tous droits réservés
 * 
 */
package fr.gouv.finances.lombok.srm.jpa.dao.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.data.jpa.JpaRepositoriesAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import fr.gouv.finances.cp.srm.dao.ProfilDao;
import fr.gouv.finances.cp.srm.dao.impl.SrmUtils;
import fr.gouv.finances.lombok.srm.bean.Profil;
import fr.gouv.finances.lombok.srm.config.PersistenceJPAConfigTest;
import fr.gouv.finances.lombok.test.controle.ControleDonnees;
import fr.gouv.finances.lombok.test.controle.VerificationExecution;
import fr.gouv.finances.lombok.util.base.PurgeTablesDao;

/**
 * Tests unitaires automatisés du DAO JPA de gestion des données du profil.
 *
 * @author Christophe Breheret-Girardin
 * @author CF
 */
@ComponentScan(basePackages = {"fr.gouv.finances.lombok.jpa.dao", "fr.gouv.finances.lombok.srm.jpa.dao.impl"})
@EnableAutoConfiguration(exclude = JpaRepositoriesAutoConfiguration.class)
@SpringBootTest(classes = {PersistenceJPAConfigTest.class}, properties = {"lombok.orm.jpa=true", "lombok.composant.sireme.inclus=true"})
@ActiveProfiles(profiles = {"embedded"})
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class ProfilDaoTest
{
    public ProfilDaoTest()
    {
        super();
    }

    /** Initialisation de la journalisation. */
    protected static final Logger LOGGER = LoggerFactory.getLogger(ProfilDaoTest.class);

    /** Déclaration du DAO de gestion des données du profil. */
    @Autowired
    private ProfilDao profildao;

    /** Déclaration du DAO de gestion des purges de la base. */
    @Autowired
    private PurgeTablesDao purgetablesdao;

    /** Map contenant les profils présents en base */
    private Map<Integer, Profil> mapProfil = new HashMap<>();

    /**
     * Initialisation des données en base avant chaque test.
     */
    @Before
    public void creerDonnees()
    {
        // Purge des données de la base
        purgetablesdao.purgerTables("ZTJ_MESS_PROF_TJ", "ZPROFIL_PROF", "ZTJ_MESS_CODI_TJ", "ZCODIQUE_CODI", "ZMESSAGETYPE_METY",
            "ZMESSAGE_MESS");

        // Vérification de l'absence de données
        ControleDonnees.verifierElements(profildao.loadAllObjects(Profil.class));

        // Génération des profils, insertion en base de données et sauvegarde dans une Map
        mapProfil = IntStream.rangeClosed(1, 2).mapToObj(SrmUtils::getProfil).peek(profildao::saveObject)
            .collect(Collectors.toMap(profil -> Integer.parseInt(profil.getCodeProfil()), profil -> profil));

        // Vérification des données à partir de l'image de la base
        ControleDonnees.verifierElements(profildao.loadAllObjects(Profil.class), mapProfil.get(1), mapProfil.get(2));

        // Suppression du cache pour forcer les requêtes des tests
        profildao.clearPersistenceContext();
    }

    /**
     * Méthode permettant de tester l'injection du DAO.
     */
    @Test
    public final void testInjectionDao()
    {
        assertNotNull("DAO non injecté dans le test", profildao);
    }

    /**
     * Test de la méthode {@link fr.gouv.finances.cp.srm.dao.ProfilDao#findProfilParCode(java.lang.String)}.
     */
    @Test
    public void testFindProfilParCode()
    {
        // Exécution de la méthode à tester sur un paramètre non renseigné ou vide
        assertNull(profildao.findProfilParCode(null));
        assertNull(profildao.findProfilParCode(""));

        // Exécution de la méthode à tester sur des données inexistantes
        assertNull(profildao.findProfilParCode("kamoulox"));

        // Exécution de la méthode à tester sur toutes les données existantes
        mapProfil.entrySet().stream()
            .forEach(entry -> assertEquals(entry.getValue(), profildao.findProfilParCode(entry.getValue().getCodeProfil())));
    }

    /**
     * Test de la méthode
     * {@link fr.gouv.finances.cp.srm.dao.ProfilDao#saveProfil(fr.gouv.finances.lombok.srm.bean.Profil)}.
     */
    @Test
    public void testSaveProfil()
    {
        // Exécution de la méthode à tester
        Profil profil = SrmUtils.getProfil(3);
        profildao.saveProfil(profil);

        // Vérifications
        ControleDonnees.verifierElements(profildao.loadAllObjects(Profil.class), mapProfil.get(1), mapProfil.get(2), profil);
    }

    /**
     * Test de la méthode {@link fr.gouv.finances.cp.srm.dao.ProfilDao#findTousLesProfils()}.
     */
    @Test
    public void testFindTousLesProfils()
    {
        // Exécution de la méthode à tester et vérifications
        ControleDonnees.verifierElements(profildao.findTousLesProfils(), mapProfil.get(1), mapProfil.get(2));
    }

    /**
     * Test des contraintes.
     */
    public void testContraintes()
    {
        // Exécution de la méthode à tester
        VerificationExecution.verifierException(DataIntegrityViolationException.class, () -> profildao.saveObject(SrmUtils.getProfil(1)));
    }

}
