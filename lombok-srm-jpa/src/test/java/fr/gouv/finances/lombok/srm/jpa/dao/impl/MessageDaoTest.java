/*
 * Copyright (c) 2020 DGFiP - Tous droits réservés
 * 
 */
package fr.gouv.finances.lombok.srm.jpa.dao.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.data.jpa.JpaRepositoriesAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import fr.gouv.finances.cp.srm.dao.MessageDao;
import fr.gouv.finances.cp.srm.dao.impl.SrmUtils;
import fr.gouv.finances.cp.srm.techbean.CriteresRechercheMessages;
import fr.gouv.finances.lombok.srm.bean.Codique;
import fr.gouv.finances.lombok.srm.bean.Message;
import fr.gouv.finances.lombok.srm.bean.Profil;
import fr.gouv.finances.lombok.srm.config.PersistenceJPAConfigTest;
import fr.gouv.finances.lombok.test.controle.ControleDonnees;
import fr.gouv.finances.lombok.util.base.PurgeTablesDao;
import fr.gouv.finances.lombok.util.date.TemporaliteUtils;

/**
 * Tests unitaires automatisés du DAO JPA, sur la gestion des données des messages SIREME.
 *
 * @author Christophe Breheret-Girardin
 * @author CF
 */
@ComponentScan(basePackages = {"fr.gouv.finances.lombok.jpa.dao", "fr.gouv.finances.lombok.srm.jpa.dao.impl"})
@EnableAutoConfiguration(exclude = JpaRepositoriesAutoConfiguration.class)
@SpringBootTest(classes = {PersistenceJPAConfigTest.class}, properties = {"lombok.orm.jpa=true", "lombok.composant.sireme.inclus=true"})
@ActiveProfiles(profiles = {"embedded"})
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class MessageDaoTest
{

    public MessageDaoTest()
    {
        super();
    }

    protected static final Logger LOGGER = LoggerFactory.getLogger(MessageDaoTest.class);

    /** Déclaration du DAO de gestion des données du message. */
    @Autowired
    private MessageDao messagedao;

    /** Déclaration du DAO de gestion des purges de la base. */
    @Autowired
    private PurgeTablesDao purgetablesdao;

    /** Map contenant les profils présents en base */
    private Map<Integer, Profil> mapProfil = new HashMap<>();

    /** Map contenant les codiques présents en base */
    private Map<Integer, Codique> mapCodique = new HashMap<>();

    /** Map contenant les messages présents en base */
    private Map<Integer, Message> mapMessage = new HashMap<>();

    /** Date du jour. */
    private Timestamp DATE = TemporaliteUtils.getTimestamp();

    /** Déclaration d'une constante contenant un libellé de message inconnu. */
    private static final String LIBELLE_INCONNU = "kamoulox";

    /** Déclaration d'une constante contenant un créateur de message. */
    private static final String CREATEUR = "UID créateur";

    /**
     * Initialisation des données en base avant chaque test.
     */
    @Before
    public void creerDonnees()
    {
        // Purge des données de la base
        purgetablesdao.purgerTables("ZTJ_MESS_PROF_TJ", "ZPROFIL_PROF", "ZTJ_MESS_CODI_TJ", "ZCODIQUE_CODI", "ZMESSAGETYPE_METY",
            "ZMESSAGE_MESS");

        // Vérification de l'absence de données
        ControleDonnees.verifierElements(messagedao.loadAllObjects(Message.class));
        ControleDonnees.verifierElements(messagedao.loadAllObjects(Codique.class));
        ControleDonnees.verifierElements(messagedao.loadAllObjects(Profil.class));

        // Génération des profils, insertion en base de données et sauvegarde dans une Map
        mapProfil = IntStream.rangeClosed(1, 4).mapToObj(SrmUtils::getProfil).peek(messagedao::saveObject)
            .collect(Collectors.toMap(profil -> Integer.parseInt(profil.getCodeProfil()), profil -> profil));

        // Génération des codiques, insertion en base de données et sauvegarde dans une Map
        mapCodique = IntStream.rangeClosed(1, 2).mapToObj(SrmUtils::getCodique).peek(messagedao::saveObject)
            .collect(Collectors.toMap(c -> Integer.parseInt(c.getLibelleCodique()), c -> c));

        // Génération des messages, insertion en base de données et sauvegarde dans une Map
        mapMessage = IntStream.rangeClosed(1, 3).mapToObj(this::getMessage).peek(messagedao::saveObject)
            .collect(Collectors.toMap(m -> Integer.parseInt(m.getLibelleMessage()), m -> m));

        // Vérification des données à partir de l'image de la base (sans association)
        ControleDonnees.verifierElements(messagedao.loadAllObjects(Profil.class), mapProfil.get(1), mapProfil.get(2), mapProfil.get(3),
            mapProfil.get(4));
        ControleDonnees.verifierElements(messagedao.loadAllObjects(Codique.class), mapCodique.get(1), mapCodique.get(2));
        verifierMessages(messagedao.loadAllObjects(Message.class), false, mapMessage.get(1), mapMessage.get(2), mapMessage.get(3));

        // Suppression du cache pour forcer les requêtes des tests
        messagedao.clearPersistenceContext();
    }

    /**
     * Méthode permettant de tester l'injection du DAO.
     */
    @Test
    public final void testInjectionDao()
    {
        assertNotNull("DAO non injecté dans le test", messagedao);
    }

    /**
     * Test de la méthode {@link fr.gouv.finances.cp.srm.dao.MessageDao#findTousLesMessages()}.
     */
    @Test
    public void findTousLesMessages()
    {
        LOGGER.info(">>> Debut methode findTousLesMessages");
        // Exécution de la méthode à tester et vérifications des messages et de ses associations
        verifierMessages(messagedao.findTousLesMessages(), true, mapMessage.get(1), mapMessage.get(2), mapMessage.get(3));
    }

    /**
     * Test de la méthode
     * {@link fr.gouv.finances.cp.srm.dao.MessageDao#saveMessage(fr.gouv.finances.lombok.srm.bean.Message)}.
     */
    @Test
    public void saveMessage()
    {
        // Création d'un message avec la méthode à tester
        Message message4 = getMessage(4);
        messagedao.saveMessage(message4);

        // Vérifications
        ControleDonnees.verifierElements(messagedao.loadAllObjects(Message.class), mapMessage.get(1), mapMessage.get(2), mapMessage.get(3),
            message4);
    }

    /**
     * Test de la méthode
     * {@link fr.gouv.finances.cp.srm.dao.MessageDao#deleteMessage(fr.gouv.finances.lombok.srm.bean.Message)}.
     */
    @Test
    public void testDeleteMessage()
    {
        // Exécution de la méthode à tester
        messagedao.deleteMessage(mapMessage.get(2));

        // Vérifications
        ControleDonnees.verifierElements(messagedao.loadAllObjects(Message.class), mapMessage.get(1), mapMessage.get(3));
    }

    /**
     * Test de la méthode
     * {@link fr.gouv.finances.cp.srm.dao.MessageDao#findMessageExisteDeja(fr.gouv.finances.lombok.srm.bean.Message)}.
     */
    @Test
    public void testFindMessageExisteDeja()
    {
        // Exécution de la méthode à tester sur des paramètres non renseignés ou vides
        assertNull("Aucun message ne doit être trouvé", messagedao.findMessageExisteDeja(null));
        assertNull("Aucun message ne doit être trouvé", messagedao.findMessageExisteDeja(
            new Message(new Date(), new Date(), "", "", CREATEUR, new Date())));
        assertNull("Aucun message ne doit être trouvé", messagedao.findMessageExisteDeja(
            new Message(new Date(), new Date(), "", null, CREATEUR, new Date())));
        assertNull("Aucun message ne doit être trouvé", messagedao.findMessageExisteDeja(
            new Message(new Date(), new Date(), LIBELLE_INCONNU, "", CREATEUR, new Date())));

        // Exécution de la méthode à tester sans résultat et vérification
        assertNull(messagedao.findMessageExisteDeja(getMessage(4)));

        // Exécution de la méthode à tester sur des résultats et vérification
        assertEquals(messagedao.findMessageExisteDeja(mapMessage.get(2)), getMessage(2));
        assertEquals(messagedao.findMessageExisteDeja(mapMessage.get(2)), mapMessage.get(2));
    }

    /**
     * Test de la méthode
     * {@link fr.gouv.finances.cp.srm.dao.MessageDao#findMessageExisteDejaEnModif(fr.gouv.finances.lombok.srm.bean.Message)}.
     */
    @Test
    public void testFindMessageExisteDejaEnModif()
    {
        // Exécution de la méthode à tester sur des paramètres non renseignés ou vides
        assertNull("Aucun message ne doit être trouvé", messagedao.findMessageExisteDejaEnModif(null));
        assertNull("Aucun message ne doit être trouvé", messagedao.findMessageExisteDejaEnModif(
            new Message(new Date(), new Date(), "", "", CREATEUR, new Date())));
        assertNull("Aucun message ne doit être trouvé", messagedao.findMessageExisteDejaEnModif(
            new Message(new Date(), new Date(), "", null, CREATEUR, new Date())));
        assertNull("Aucun message ne doit être trouvé", messagedao.findMessageExisteDejaEnModif(
            new Message(new Date(), new Date(), LIBELLE_INCONNU, "", CREATEUR, new Date())));

        // Exécution de la méthode à tester sans résultat et vérification
        assertNull(messagedao.findMessageExisteDejaEnModif(mapMessage.get(1)));

        // Exécution de la méthode à tester sur des résultats et vérification
        Message message1bis = new Message(new Date(), new Date(), mapMessage.get(1).getLibelleMessage(), "", CREATEUR, new Date());
        messagedao.saveObject(message1bis);
        verifierMessage(messagedao.findMessageExisteDejaEnModif(message1bis), false, mapMessage.get(1));
    }

    /**
     * Test de la méthode suivante sur les attributs vides :
     * {@link fr.gouv.finances.cp.srm.dao.MessageDao#findMessages(fr.gouv.finances.cp.srm.techbean.CriteresRechercheMessages)}.
     */
    @Test
    public void testFindMessagesAttributsVide()
    {
        // Exécution de la méthode à tester sur des critères non initialisés et vérifications
        verifierMessages(messagedao.findMessages(null), true, mapMessage.get(1), mapMessage.get(2), mapMessage.get(3));

        // Exécution de la méthode à tester sur des critères vides et vérifications
        CriteresRechercheMessages criteres = new CriteresRechercheMessages();
        verifierMessages(messagedao.findMessages(criteres), true, mapMessage.get(1), mapMessage.get(2), mapMessage.get(3));
    }

    /**
     * Test de la méthode suivante sur les attributs renseignés, avec profils retournés :
     * {@link fr.gouv.finances.cp.srm.dao.MessageDao#findMessages(fr.gouv.finances.cp.srm.techbean.CriteresRechercheMessages)}.
     */
    @Test
    public void testFindMessagesAttributsRecuperationProfil()
    {
        // Initialisation des critères de recherche sur les attributs du message 2
        CriteresRechercheMessages criteres = new CriteresRechercheMessages();
        criteres.setLibelleMessage(mapMessage.get(2).getLibelleMessage());
        Calendar calendar = Calendar.getInstance();
        calendar.set(2017, 11, 19);
        criteres.setDateHeureCourante(calendar.getTime());
        criteres.setTypeMessages(mapMessage.get(2).getTypeMessage());
        criteres.setUidCreateurMessage(CREATEUR);

        // Exécution de la méthode à tester et vérifications
        verifierMessages(messagedao.findMessages(criteres), true, mapMessage.get(2));
    }

    /**
     * Test de la méthode suivante sur les attributs renseignés, sans profil retourné :
     * {@link fr.gouv.finances.cp.srm.dao.MessageDao#findMessages(fr.gouv.finances.cp.srm.techbean.CriteresRechercheMessages)}.
     */
    @Test
    public void testFindMessagesAttributsSansProfil()
    {
        // Initialisation des critères de recherche sur les attributs du message 1
        CriteresRechercheMessages criteres = new CriteresRechercheMessages();
        criteres.setLibelleMessage(mapMessage.get(1).getLibelleMessage());
        Calendar calendar = Calendar.getInstance();
        calendar.set(2017, 11, 19);
        criteres.setDateHeureCourante(calendar.getTime());
        criteres.setTypeMessages(mapMessage.get(1).getTypeMessage());
        criteres.setUidCreateurMessage(CREATEUR);

        // Exécution de la méthode à tester et vérifications
        verifierMessages(messagedao.findMessages(criteres), true, mapMessage.get(1));
    }

    /**
     * Test de la méthode suivante sur les attributs renseignés sur les dates :
     * {@link fr.gouv.finances.cp.srm.dao.MessageDao#findMessages( fr.gouv.finances.cp.srm.techbean.CriteresRechercheMessages)}.
     */
    @Test
    public void testFindMessagesAttributsAvecDate()
    {
        // Initialisation des critères de recherche sur les dates
        CriteresRechercheMessages criteres = new CriteresRechercheMessages();
        criteres.setDateDebutMessage(TemporaliteUtils.getTimestamp(2017, 11, 18));
        criteres.setDateFinMessage(TemporaliteUtils.getTimestamp(2017, 11, 21));

        // Exécution de la méthode à tester et vérification qu'il n'y a aucun résultat
        verifierMessages(messagedao.findMessages(criteres), true);

        // Paramétrage d'une borne inférieure non conforme au test
        modifierDate(mapMessage.get(1), 2017, 11, 17, 2017, 11, 20);
        Message message1 = messagedao.modifyObject(mapMessage.get(1));
        // Exécution de la méthode à tester et vérification qu'il n'y a aucun résultat
        ControleDonnees.verifierElements(messagedao.findMessages(criteres));

        // Paramétrage d'une borne supérieure non conforme au test
        modifierDate(message1, 2017, 11, 19, 2017, 11, 22);
        message1 = messagedao.modifyObject(message1);
        // Exécution de la méthode à tester et vérification qu'il n'y a aucun résultat
        ControleDonnees.verifierElements(messagedao.findMessages(criteres));

        // Paramétrage de bornes conformes au test
        modifierDate(message1, 2017, 11, 19, 2017, 11, 20);
        message1 = messagedao.modifyObject(message1);
        // Exécution de la méthode à tester et vérification du bon résultat
        verifierMessages(messagedao.findMessages(criteres), true, message1);

        // Paramétrage d'une borne inférieure non renseignée
        message1.setDateDebutMessage(null);
        // Exécution de la méthode à tester et vérification du bon résultat
        verifierMessages(messagedao.findMessages(criteres), true, message1);

        // Paramétrage d'une borne supérieure non renseignée
        message1.setDateDebutMessage(TemporaliteUtils.getTimestamp(2017, 11, 19));
        message1.setDateFinMessage(null);
        // Exécution de la méthode à tester et vérification du bon résultat
        verifierMessages(messagedao.findMessages(criteres), true, message1);
    }

    /**
     * Test de la méthode suivante sur les messages actifs à la date demandée :
     * {@link fr.gouv.finances.cp.srm.dao.MessageDao#findMessages(fr.gouv.finances.cp.srm.techbean.CriteresRechercheMessages)}.
     */
    @Test
    public void testFindMessagesActifsDateDemandee()
    {
        // Initialisation des critères de recherche sur les messages actifs à la date définie
        CriteresRechercheMessages criteres = new CriteresRechercheMessages();
        criteres.setDateHeureCourante(TemporaliteUtils.getTimestamp(2018, 1, 1));
        criteres.setRechOnlyMessagesActifsNow(true);

        // Exécution de la méthode à tester et vérification du bon résultat
        verifierMessages(messagedao.findMessages(criteres), true, mapMessage.get(2));
    }

    /**
     * Test de la méthode suivante sur les profils à désactiver à la date choisie :
     * {@link fr.gouv.finances.cp.srm.dao.MessageDao#findMessages(fr.gouv.finances.cp.srm.techbean.CriteresRechercheMessages)}.
     */
    @Test
    public void testFindMessagesProfilADesactiverADate()
    {
        // Initialisation des critères de recherche sur les messages actifs
        CriteresRechercheMessages criteres = new CriteresRechercheMessages();
        criteres.setDateHeureCourante(TemporaliteUtils.getTimestamp(2018, 1, 1));
        criteres.setRechOnlyMessProfilsADesactNow(true);

        // Exécution de la méthode à tester et vérification du bon résultat
        verifierMessages(messagedao.findMessages(criteres), true, mapMessage.get(2));

        // Paramétrage d'une absence de borne inférieure
        Message message2 = mapMessage.get(2);
        message2.setDateDebutInactiviteProfil(null);
        message2 = messagedao.modifyObject(message2);
        // Exécution de la méthode à tester et vérification d'aucun résultat
        verifierMessages(messagedao.findMessages(criteres), true);

        // Paramétrage d'une absence de borne supérieure
        message2.setDateDebutInactiviteProfil(TemporaliteUtils.getTimestamp(1999, 1, 1));
        message2.setDateDebutInactiviteProfil(null);
        message2 = messagedao.modifyObject(message2);
        // Exécution de la méthode à tester et vérification d'aucun résultat
        verifierMessages(messagedao.findMessages(criteres), true);
    }

    /**
     * Test de la méthode suivante sur les profils à désactiver :
     * {@link fr.gouv.finances.cp.srm.dao.MessageDao#findMessages(fr.gouv.finances.cp.srm.techbean.CriteresRechercheMessages)}.
     */
    @Test
    public void testFindMessagesAvecProfilADesactiver()
    {
        // Initialisation des critères de recherche sur les messages actifs
        CriteresRechercheMessages criteres = new CriteresRechercheMessages();
        criteres.setRechOnlyMessAvecProfilsADesact(true);

        // Exécution de la méthode à tester et vérification du bon résultat
        verifierMessages(messagedao.findMessages(criteres), true, mapMessage.get(1), mapMessage.get(3));
    }

    /**
     * Test de la méthode
     * {@link fr.gouv.finances.cp.srm.dao.MessageDao#findMessagesAyantLibelleCodiqueRattacheAAutreMessage( java.lang.String, fr.gouv.finances.lombok.srm.bean.Message)}.
     */
    @Test
    public void testFindMessagesAyantLibelleCodiqueRattacheAAutreMessage()
    {
        // 1ères exécutions de la méthode et vérifications
        verifierMessages(messagedao.findMessagesAyantLibelleCodiqueRattacheAAutreMessage(null, null), false);
        verifierMessages(messagedao.findMessagesAyantLibelleCodiqueRattacheAAutreMessage("", null), false);
        verifierMessages(messagedao.findMessagesAyantLibelleCodiqueRattacheAAutreMessage(
            null, new Message()), false);
        verifierMessages(messagedao.findMessagesAyantLibelleCodiqueRattacheAAutreMessage(
            "", new Message()), false);
        verifierMessages(messagedao.findMessagesAyantLibelleCodiqueRattacheAAutreMessage(
            LIBELLE_INCONNU, new Message()), false);
        verifierMessages(messagedao.findMessagesAyantLibelleCodiqueRattacheAAutreMessage(
            LIBELLE_INCONNU, null), false);
        verifierMessages(messagedao.findMessagesAyantLibelleCodiqueRattacheAAutreMessage(
            mapCodique.get(1).getLibelleCodique(), null), false);
        verifierMessages(messagedao.findMessagesAyantLibelleCodiqueRattacheAAutreMessage(
            mapCodique.get(1).getLibelleCodique(), new Message()), false);
        verifierMessages(messagedao.findMessagesAyantLibelleCodiqueRattacheAAutreMessage(
            null, mapMessage.get(2)), false);

        // Exécution de la méthode sur un libellé de codique seulement lié au message 2
        // et vérification qu'aucun message n'est ramené
        verifierMessages(messagedao.findMessagesAyantLibelleCodiqueRattacheAAutreMessage(
            mapCodique.get(2).getLibelleCodique(), mapMessage.get(2)), false);

        // Exécution de la méthode sur un libellé de codique lié aux messages 2 et 3
        // et vérification que seul le message 3 est ramené
        verifierMessages(messagedao.findMessagesAyantLibelleCodiqueRattacheAAutreMessage(
            mapCodique.get(1).getLibelleCodique(), mapMessage.get(2)), false, mapMessage.get(3));
    }

    /**
     * Test de la méthode {@link fr.gouv.finances.cp.srm.dao.MessageDao#findMessagesPerimes()}.
     */
    @Test
    public void testFindMessagesPerimes()
    {
        // Création d'un message
        Message message4 = getMessage(4);
        messagedao.saveObject(message4);

        // Exécution de la méthode à tester et vérification du bon résultat (2 messages périmés)
        verifierMessages(messagedao.findMessagesPerimes(), true, mapMessage.get(1), message4);
    }

    /**
     * Test des contraintes.
     */
    @Test(expected = DataIntegrityViolationException.class)
    public void testContraintes()
    {
        // Exécution de la méthode
        messagedao.saveObject(getMessage(1));
    }

    /**
     * Méthode permettant de modifier un message.
     *
     * @param message message à modifier
     * @param anneeDebut année de début du message à modifier
     * @param moisDebut mois de début du message à modifier
     * @param jourDebut jour de début du message à modifier
     * @param anneeFin année de fin du message à modifier
     * @param moisFin mois de fin du message à modifier
     * @param jourFin jour de fin du message à modifier
     */
    private void modifierDate(Message message, int anneeDebut, int moisDebut, int jourDebut, int anneeFin, int moisFin, int jourFin)
    {
        message.setDateCreationMessage(TemporaliteUtils.getTimestamp());
        message.setDateDebutMessage(TemporaliteUtils.getTimestamp(anneeDebut, moisDebut, jourDebut));
        message.setDateFinMessage(TemporaliteUtils.getTimestamp(anneeFin, moisFin, jourFin));
    }

    /**
     * Méthode permettant de vérifier l'absence d'association chargée sur un message.
     *
     * @param message le message à vérifier
     */
    private void verifierAbsenceAssociation(Message message)
    {
        assertFalse(messagedao.isCharged(message.getLesProfilsDesactives()));
        assertFalse(messagedao.isCharged(message.getLesProfilsDestinataires()));
        assertFalse(messagedao.isCharged(message.getLesCodiquesDestinataires()));
    }

    /**
     * Méthode permettant de vérifier la présence d'association chargée sur un message.
     *
     * @param message le message à vérifier
     */
    private void verifierPresenceAssociation(Message message)
    {
        // Contrôles du chargement
        assertTrue(messagedao.isCharged(message.getLesProfilsDesactives()));
        assertTrue(messagedao.isCharged(message.getLesProfilsDestinataires()));
        assertTrue(messagedao.isCharged(message.getLesCodiquesDestinataires()));

        // Contrôles des données
        if (Integer.parseInt(message.getLibelleMessage()) <= 3)
        {
            ControleDonnees.verifierElements(message.getLesProfilsDesactives(),
                mapMessage.get(Integer.parseInt(message.getLibelleMessage()))
                    .getLesProfilsDesactives().stream().toArray(Profil[]::new));
            ControleDonnees.verifierElements(message.getLesProfilsDestinataires(),
                mapMessage.get(Integer.parseInt(message.getLibelleMessage()))
                    .getLesProfilsDestinataires().stream().toArray(Profil[]::new));
            ControleDonnees.verifierElements(message.getLesCodiquesDestinataires(),
                mapMessage.get(Integer.parseInt(message.getLibelleMessage()))
                    .getLesCodiquesDestinataires().stream().toArray(Codique[]::new));
        }
        else
        {
            // Pas de profil / codique associé à ces messages
            assertTrue(message.getLesProfilsDesactives().isEmpty());
            assertTrue(message.getLesProfilsDestinataires().isEmpty());
            assertTrue(message.getLesCodiquesDestinataires().isEmpty());
        }
    }

    /**
     * Méthode permettant de contrôler des messages sireme.
     *
     * @param messages liste des messages à vérifier
     * @param isAssociationsChargees les associations profil et codique doivent-elles chargées ?
     * @param messagesAttendus les messages attendus
     */
    private void verifierMessages(List<Message> messages, boolean isAssociationsChargees, Message... messagesAttendus)
    {
        // Contrôle des messages
        ControleDonnees.verifierElements(messages, messagesAttendus);

        // Les associations doivent-elles chargées ?
        if (!isAssociationsChargees)
        {
            // Vérification de l'absence des associations dans chaque message
            messages.stream().forEach(this::verifierAbsenceAssociation);
        }
        else
        {
            // Vérification de la présence et de la valeur des associations de chaque message
            messages.stream().forEach(this::verifierPresenceAssociation);
        }
    }

    /**
     * Méthode permettant de contrôler un message sireme.
     *
     * @param message message à contrôler
     * @param isAssociationsChargees les associations sont-elles chargées ?
     * @param messagesAttendus les messages attendus
     */
    private void verifierMessage(Message message, boolean isAssociationsChargees, Message... messagesAttendus)
    {
        List<Message> messages = new ArrayList<>();
        if (message != null)
        {
            messages = Arrays.asList(message);
        }
        verifierMessages(messages, isAssociationsChargees, messagesAttendus);
    }

    /**
     * Méthode permettant de générer un message.
     *
     * @param indice indice du message
     */
    private Message getMessage(int indice)
    {
        Message message = new Message(DATE, DATE, String.valueOf(indice), "type message", CREATEUR, DATE);

        Date dateFinActiviteAncienne = TemporaliteUtils.getTimestamp(1999, 1, 1);
        Date dateFinActiviteFuture = TemporaliteUtils.getTimestamp(9002, 1, 1);
        Date dateMessageAncienne = TemporaliteUtils.getTimestamp(2000, 1, 1);
        Date dateFinMessageFutur = TemporaliteUtils.getTimestamp(9001, 1, 1);

        switch (indice)
        {
            case 1:
                message.setDateDebutInactiviteProfil(TemporaliteUtils.getTimestamp(1999, 1, 1));
                message.setDateFinInactiviteProfil(dateFinActiviteAncienne);
                message.setDateDebutMessage(dateMessageAncienne);
                message.setDateFinMessage(dateMessageAncienne);
                message.getLesProfilsDesactives().add(mapProfil.get(1));
                message.getLesProfilsDesactives().add(mapProfil.get(2));
                message.getLesProfilsDestinataires().add(mapProfil.get(3));
                break;
            case 2:
                message.setDateDebutInactiviteProfil(TemporaliteUtils.getTimestamp(1999, 1, 1));
                message.setDateFinInactiviteProfil(dateFinActiviteFuture);
                message.setDateDebutMessage(TemporaliteUtils.getTimestamp(2017, 11, 17));
                message.setDateFinMessage(dateFinMessageFutur);
                message.getLesCodiquesDestinataires().add(mapCodique.get(1));
                message.getLesCodiquesDestinataires().add(mapCodique.get(2));
                break;
            case 3:
                message.setDateFinInactiviteProfil(null);
                message.setDateFinMessage(dateFinMessageFutur);
                message.setDateDebutMessage(TemporaliteUtils.getTimestamp(9000, 1, 1));
                message.getLesProfilsDesactives().add(mapProfil.get(4));
                message.getLesCodiquesDestinataires().add(mapCodique.get(1));
                break;
            default:
                message.setDateFinInactiviteProfil(null);
                message.setDateFinMessage(dateMessageAncienne);
                // Pas de profil, pas de codique
        }

        return message;
    }

}
